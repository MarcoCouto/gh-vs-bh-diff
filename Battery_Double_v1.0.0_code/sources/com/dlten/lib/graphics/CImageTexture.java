package com.dlten.lib.graphics;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.opengl.GLES10;
import android.opengl.GLUtils;
import com.dlten.lib.STD;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

public class CImageTexture extends CImage {
    private int SC_RES_HEIGHT;
    private int SC_RES_WIDTH;
    private FloatBuffer mColorBuffer0;
    private FloatBuffer mFVertexBuffer;
    private FloatBuffer mFVertexBuffer0;
    private ShortBuffer mIndexBuffer;
    private FloatBuffer mTexBuffer;
    private ByteBuffer m_cbb0;
    private ByteBuffer m_ibb;
    private CRect m_rectTexture;
    private ByteBuffer m_tbb;
    private int m_textureID;
    private ByteBuffer m_vbb;
    private ByteBuffer m_vbb0;

    public CImageTexture() {
        this.m_textureID = -1;
        this.m_rectTexture = new CRect();
        this.m_vbb = ByteBuffer.allocateDirect(48);
        this.m_tbb = ByteBuffer.allocateDirect(32);
        this.m_ibb = ByteBuffer.allocateDirect(8);
        this.m_vbb0 = ByteBuffer.allocateDirect(48);
        this.m_cbb0 = ByteBuffer.allocateDirect(64);
        this.m_textureID = -1;
        CDCView dCView = CImage.getDCView();
        this.SC_RES_WIDTH = CDCView.getResWidth();
        this.SC_RES_HEIGHT = CDCView.getResHeight();
    }

    public CRect getTextureRect() {
        return this.m_rectTexture;
    }

    public int getTextureID() {
        return this.m_textureID;
    }

    /* access modifiers changed from: protected */
    public int loadResource(Bitmap img) {
        Config config;
        unloadResource();
        super.loadResource(img);
        int width = adjustValue(this.m_totalWidth);
        int height = adjustValue(this.m_totalHeight);
        if (!(this.m_totalWidth == width && this.m_totalHeight == height)) {
            try {
                if (img.hasAlpha()) {
                    config = Config.ARGB_8888;
                } else {
                    config = Config.RGB_565;
                }
                Bitmap bmpTexture = Bitmap.createBitmap(width, height, config);
                new Canvas(bmpTexture).drawBitmap(img, BitmapDescriptorFactory.HUE_RED, BitmapDescriptorFactory.HUE_RED, null);
                img.recycle();
                img = bmpTexture;
            } catch (Exception e) {
                int n = 1 + 1;
                return -1;
            }
        }
        STD.ASSERT(this.m_textureID == -1);
        int textureID = createTexture(img);
        this.m_textureID = textureID;
        initTexture();
        return textureID;
    }

    /* access modifiers changed from: protected */
    public void unloadResource() {
        if (this.m_textureID != -1) {
            deleteTexture(this.m_textureID);
            this.m_textureID = -1;
        }
        super.unloadResource();
    }

    public void drawImage(float fPosX, float fPosY, float fScaleX, float fScaleY, int nAlpha, Matrix matrix) {
        drawImage(this.m_textureID, fPosX, fPosY, ((float) this.m_totalWidth) * fScaleX, ((float) this.m_totalHeight) * fScaleY, nAlpha);
    }

    private int adjustValue(int val) {
        if (val <= 2) {
            return 2;
        }
        if (val <= 4) {
            return 4;
        }
        if (val <= 8) {
            return 8;
        }
        if (val <= 16) {
            return 16;
        }
        if (val <= 32) {
            return 32;
        }
        if (val <= 64) {
            return 64;
        }
        if (val <= 128) {
            return 128;
        }
        if (val <= 256) {
            return 256;
        }
        if (val <= 512) {
            return 512;
        }
        if (val <= 1024) {
            return 1024;
        }
        return val;
    }

    private int createTexture(Bitmap img) {
        int width = img.getWidth();
        int height = img.getHeight();
        this.m_rectTexture.left = BitmapDescriptorFactory.HUE_RED;
        this.m_rectTexture.top = BitmapDescriptorFactory.HUE_RED;
        this.m_rectTexture.width = ((float) this.m_totalWidth) / ((float) width);
        this.m_rectTexture.height = ((float) this.m_totalHeight) / ((float) height);
        GLES10.glEnable(3553);
        int[] textures = new int[1];
        GLES10.glGenTextures(1, textures, 0);
        GLES10.glBindTexture(3553, textures[0]);
        GLES10.glTexParameterf(3553, 10241, 9729.0f);
        GLES10.glTexParameterf(3553, 10240, 9729.0f);
        GLES10.glTexParameterf(3553, 10242, 33071.0f);
        GLES10.glTexParameterf(3553, 10243, 33071.0f);
        GLUtils.texImage2D(3553, 0, img, 0);
        return textures[0];
    }

    private void deleteTexture(int textureID) {
        GLES10.glDeleteTextures(1, new int[]{textureID}, 0);
    }

    private void initTexture() {
        this.m_vbb.order(ByteOrder.nativeOrder());
        this.m_tbb.order(ByteOrder.nativeOrder());
        this.m_ibb.order(ByteOrder.nativeOrder());
        this.mFVertexBuffer = this.m_vbb.asFloatBuffer();
        this.mTexBuffer = this.m_tbb.asFloatBuffer();
        this.mIndexBuffer = this.m_ibb.asShortBuffer();
        float left = this.m_rectTexture.left;
        float top = this.m_rectTexture.top;
        float right = this.m_rectTexture.Right();
        float bottom = this.m_rectTexture.Bottom();
        this.mTexBuffer.put(left);
        this.mTexBuffer.put(top);
        this.mTexBuffer.put(left);
        this.mTexBuffer.put(bottom);
        this.mTexBuffer.put(right);
        this.mTexBuffer.put(bottom);
        this.mTexBuffer.put(right);
        this.mTexBuffer.put(top);
        this.mTexBuffer.position(0);
        this.mIndexBuffer.put(0);
        this.mIndexBuffer.put(1);
        this.mIndexBuffer.put(2);
        this.mIndexBuffer.put(3);
        this.mIndexBuffer.position(0);
        this.m_vbb0.order(ByteOrder.nativeOrder());
        this.m_cbb0.order(ByteOrder.nativeOrder());
        this.mFVertexBuffer0 = this.m_vbb0.asFloatBuffer();
        this.mFVertexBuffer0.put(-0.5f);
        this.mFVertexBuffer0.put(0.5f);
        this.mFVertexBuffer0.put(BitmapDescriptorFactory.HUE_RED);
        this.mFVertexBuffer0.put(-0.5f);
        this.mFVertexBuffer0.put(-0.5f);
        this.mFVertexBuffer0.put(BitmapDescriptorFactory.HUE_RED);
        this.mFVertexBuffer0.put(0.5f);
        this.mFVertexBuffer0.put(-0.5f);
        this.mFVertexBuffer0.put(BitmapDescriptorFactory.HUE_RED);
        this.mFVertexBuffer0.put(0.5f);
        this.mFVertexBuffer0.put(0.5f);
        this.mFVertexBuffer0.put(BitmapDescriptorFactory.HUE_RED);
        this.mFVertexBuffer0.position(0);
    }

    public void drawImage(int textureID, float x, float y, float w, float h, int nAlpha) {
        GLES10.glBindTexture(3553, textureID);
        float left = (x / ((float) this.SC_RES_WIDTH)) - 0.5f;
        float top = ((y / ((float) this.SC_RES_HEIGHT)) - 0.5f) * -1.0f;
        float right = ((x + w) / ((float) this.SC_RES_WIDTH)) - 0.5f;
        float bottom = (((y + h) / ((float) this.SC_RES_HEIGHT)) - 0.5f) * -1.0f;
        this.mFVertexBuffer.put(left);
        this.mFVertexBuffer.put(top);
        this.mFVertexBuffer.put(BitmapDescriptorFactory.HUE_RED);
        this.mFVertexBuffer.put(left);
        this.mFVertexBuffer.put(bottom);
        this.mFVertexBuffer.put(BitmapDescriptorFactory.HUE_RED);
        this.mFVertexBuffer.put(right);
        this.mFVertexBuffer.put(bottom);
        this.mFVertexBuffer.put(BitmapDescriptorFactory.HUE_RED);
        this.mFVertexBuffer.put(right);
        this.mFVertexBuffer.put(top);
        this.mFVertexBuffer.put(BitmapDescriptorFactory.HUE_RED);
        this.mFVertexBuffer.position(0);
        GLES10.glEnable(3553);
        GLES10.glEnableClientState(32884);
        GLES10.glVertexPointer(3, 5126, 0, this.mFVertexBuffer);
        GLES10.glEnableClientState(32888);
        GLES10.glTexCoordPointer(2, 5126, 0, this.mTexBuffer);
        GLES10.glDrawElements(6, 4, 5123, this.mIndexBuffer);
        if (nAlpha < 255) {
            float fAlpha = ((float) nAlpha) / 255.0f;
            this.mColorBuffer0 = this.m_cbb0.asFloatBuffer();
            for (int i = 0; i < 4; i++) {
                this.mColorBuffer0.put(BitmapDescriptorFactory.HUE_RED);
                this.mColorBuffer0.put(BitmapDescriptorFactory.HUE_RED);
                this.mColorBuffer0.put(BitmapDescriptorFactory.HUE_RED);
                this.mColorBuffer0.put(1.0f - fAlpha);
            }
            this.mColorBuffer0.position(0);
            GLES10.glEnableClientState(32886);
            GLES10.glColorPointer(4, 5126, 0, this.mColorBuffer0);
            GLES10.glDrawElements(6, 4, 5123, this.mIndexBuffer);
            GLES10.glDisableClientState(32886);
        }
        GLES10.glDisableClientState(32888);
        GLES10.glDisableClientState(32884);
    }
}
