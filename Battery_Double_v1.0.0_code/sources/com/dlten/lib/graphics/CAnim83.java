package com.dlten.lib.graphics;

import com.dlten.lib.STD;
import com.dlten.lib.frmWork.CAnimation;
import com.dlten.lib.frmWork.CEventWnd;

public class CAnim83 extends CAnimation {
    private static final int ANIM_OBJ_COUNT = 5;
    private static final int ANIM_OBJ_GAME = 0;
    private static final int ANIM_OBJ_GTFDATA = 2;
    private static final int ANIM_OBJ_GTFPICTURE = 1;
    private static final int ANIM_OBJ_IMG_OBJ = 4;
    private static final int ANIM_OBJ_PANE = 3;
    public static final int CREATE_ANIM_COUNT = 2;
    public static final int CREATE_ANIM_LINE_ACCELER = 1;
    public static final int CREATE_ANIM_LINE_UNIFORM = 0;
    private int m_nFrameCount = 0;
    private ANIM_ELEMENT[] m_pAnimation = null;

    class ANIM_ELEMENT {
        int animObjType;
        int nParam1;
        int nParam2;
        Object pAnimObj;
        int x;
        int y;

        public ANIM_ELEMENT() {
            Reset();
        }

        public void Reset() {
            this.pAnimObj = null;
            this.animObjType = 0;
            this.x = 0;
            this.y = 0;
            this.nParam1 = 0;
            this.nParam2 = 0;
        }
    }

    class CGtfPicture {
        CGtfPicture() {
        }

        /* access modifiers changed from: 0000 */
        public void SetPos(int x, int y) {
        }

        /* access modifiers changed from: 0000 */
        public void Draw(int x, int y) {
        }

        /* access modifiers changed from: 0000 */
        public void Draw() {
        }
    }

    public CAnim83() {
        ResetAnimation();
    }

    public void ResetAnimation() {
    }

    public void SetFrame(int xx) {
        int nCurFrame = GetCurFrameNum();
        if (nCurFrame >= 0 && nCurFrame < this.m_nFrameCount) {
            Object pObj = this.m_pAnimation[nCurFrame].pAnimObj;
            int x = this.m_pAnimation[nCurFrame].x;
            int y = this.m_pAnimation[nCurFrame].y;
            if (pObj != null) {
                switch (this.m_pAnimation[nCurFrame].animObjType) {
                    case 1:
                        ((CGtfPicture) pObj).SetPos(x, y);
                        return;
                    case 4:
                        ((CImgObj) pObj).moveTo((float) x, (float) y);
                        return;
                    default:
                        return;
                }
            }
        }
    }

    public void DrawProc() {
        int nCurFrame = GetCurFrameNum();
        if (nCurFrame >= 0 && nCurFrame < this.m_nFrameCount) {
            Object pObj = this.m_pAnimation[nCurFrame].pAnimObj;
            if (pObj != null) {
                switch (this.m_pAnimation[nCurFrame].animObjType) {
                    case 1:
                        ((CGtfPicture) pObj).Draw();
                        return;
                    case 4:
                        ((CImgObj) pObj).draw();
                        return;
                    default:
                        return;
                }
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public int[] GetPos() {
        int nCurFrame = GetCurFrameNum();
        return new int[]{this.m_pAnimation[nCurFrame].x, this.m_pAnimation[nCurFrame].y};
    }

    public void Create(CEventWnd pParent, int animType, CGtfPicture pImg, int[][] pPtList, int nPtCount, int nStartGapFrames, int nFrames, int nEndGapFrames) {
        Create(pParent, animType, pImg, 1, 0, 0, pPtList, nPtCount, nStartGapFrames, nFrames, nEndGapFrames);
    }

    public void Create(CEventWnd pParent, int animType, CImgObj pImg, int[][] pPtList, int nPtCount, int nStartGapFrames, int nFrames, int nEndGapFrames) {
        Create(pParent, animType, pImg, 4, 0, 0, pPtList, nPtCount, nStartGapFrames, nFrames, nEndGapFrames);
    }

    public void Create(CEventWnd pParent, int animType, Object pAnimObj, int animObjType, int nParam1, int nParam2, int[][] pPtList, int nPtCount, int nStartGapFrames, int nFrames, int nEndGapFrames) {
        STD.ASSERT(nPtCount > 1);
        ResetAnimation();
        CreateAnimElements(animType, pAnimObj, animObjType, nParam1, nParam2, pPtList, nPtCount, nStartGapFrames, nFrames, nEndGapFrames);
        switch (animType) {
            case 0:
                SetLineAnimProps_Uniform(this.m_pAnimation, nStartGapFrames, pPtList, nPtCount, nFrames);
                break;
            case 1:
                SetLineAnimProps_Accelerated(this.m_pAnimation, nStartGapFrames, pPtList, nPtCount, nFrames);
                break;
        }
        super.Create(pParent, 0, this.m_nFrameCount, false, true);
    }

    public void CreateAnimElements(int animType, Object pAnimObj, int animObjType, int nParam1, int nParam2, int[][] pPtList, int nPtCount, int nStartGapFrames, int nFrames, int nEndGapFrames) {
        this.m_nFrameCount = nStartGapFrames + nFrames + nEndGapFrames;
        this.m_pAnimation = null;
        this.m_pAnimation = new ANIM_ELEMENT[this.m_nFrameCount];
        for (int i = 0; i < this.m_nFrameCount; i++) {
            this.m_pAnimation[i] = new ANIM_ELEMENT();
        }
        for (int i2 = 0; i2 < nStartGapFrames; i2++) {
            this.m_pAnimation[i2].pAnimObj = pAnimObj;
            this.m_pAnimation[i2].animObjType = animObjType;
            this.m_pAnimation[i2].x = pPtList[0][0];
            this.m_pAnimation[i2].y = pPtList[0][1];
            this.m_pAnimation[i2].nParam1 = nParam1;
            this.m_pAnimation[i2].nParam2 = nParam2;
        }
        for (int i3 = nStartGapFrames; i3 < nFrames + nStartGapFrames; i3++) {
            this.m_pAnimation[i3].pAnimObj = pAnimObj;
            this.m_pAnimation[i3].animObjType = animObjType;
            this.m_pAnimation[i3].x = pPtList[0][0];
            this.m_pAnimation[i3].y = pPtList[0][1];
            this.m_pAnimation[i3].nParam1 = nParam1;
            this.m_pAnimation[i3].nParam2 = nParam2;
        }
        for (int i4 = nFrames + nStartGapFrames; i4 < this.m_nFrameCount; i4++) {
            this.m_pAnimation[i4].pAnimObj = pAnimObj;
            this.m_pAnimation[i4].animObjType = animObjType;
            this.m_pAnimation[i4].x = pPtList[nPtCount - 1][0];
            this.m_pAnimation[i4].y = pPtList[nPtCount - 1][1];
            this.m_pAnimation[i4].nParam1 = nParam1;
            this.m_pAnimation[i4].nParam2 = nParam2;
        }
    }

    /* access modifiers changed from: 0000 */
    public void SetLineAnimProps_Uniform(ANIM_ELEMENT[] pAnim, int nStartIdx, int[][] pPtList, int nPtCount, int nFrames) {
        int[] nArrDistances = new int[(nPtCount - 1)];
        int[] nArrFrames = new int[(nPtCount - 1)];
        int nTotalDistance = 0;
        for (int i = 0; i < nPtCount - 1; i++) {
            int nDistX = pPtList[i + 1][0] - pPtList[i][0];
            int nDistY = pPtList[i + 1][1] - pPtList[i][1];
            nArrDistances[i] = (nDistX * nDistX) + (nDistY * nDistY);
            nTotalDistance += nArrDistances[i];
        }
        int nFrameSum = 0;
        for (int i2 = nPtCount - 2; i2 > 0; i2--) {
            if (nTotalDistance == 0) {
                nArrFrames[i2] = 0;
            } else {
                nArrFrames[i2] = (nArrDistances[i2] * nFrames) / nTotalDistance;
            }
            nFrameSum += nArrFrames[i2];
        }
        nArrFrames[0] = nFrames - nFrameSum;
        int nFrameSum2 = 0;
        for (int i3 = 0; i3 < nPtCount - 1; i3++) {
            for (int j = 0; j < nArrFrames[i3]; j++) {
                if (nFrameSum2 >= nFrames) {
                    STD.ASSERT(false);
                } else {
                    pAnim[nStartIdx + nFrameSum2].x = pPtList[i3][0] + (((pPtList[i3 + 1][0] - pPtList[i3][0]) * j) / nArrFrames[i3]);
                    pAnim[nStartIdx + nFrameSum2].y = pPtList[i3][1] + (((pPtList[i3 + 1][1] - pPtList[i3][1]) * j) / nArrFrames[i3]);
                    nFrameSum2++;
                }
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void SetLineAnimProps_Accelerated(ANIM_ELEMENT[] pAnim, int nStartIdx, int[][] pPtList, int nPtCount, int nFrames) {
        int[] nArrFrames = new int[(nPtCount - 1)];
        int nCount = nPtCount - 1;
        int nFrameSum = 0;
        for (int i = nCount - 1; i > 0; i--) {
            nArrFrames[i] = ((nFrames * i) / nCount) - (((i - 1) * nFrames) / nCount);
            nFrameSum += nArrFrames[i];
        }
        nArrFrames[0] = nFrames - nFrameSum;
        int nFrameSum2 = 0;
        for (int i2 = 0; i2 < nPtCount - 1; i2++) {
            for (int j = 0; j < nArrFrames[i2]; j++) {
                if (nFrameSum2 >= nFrames) {
                    STD.ASSERT(false);
                } else {
                    pAnim[nStartIdx + nFrameSum2].x = pPtList[i2][0] + (((pPtList[i2 + 1][0] - pPtList[i2][0]) * j) / nArrFrames[i2]);
                    pAnim[nStartIdx + nFrameSum2].y = pPtList[i2][1] + (((pPtList[i2 + 1][1] - pPtList[i2][1]) * j) / nArrFrames[i2]);
                    nFrameSum2++;
                }
            }
        }
    }
}
