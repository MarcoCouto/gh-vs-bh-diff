package com.dlten.lib.graphics;

import android.content.Context;
import android.opengl.GLSurfaceView;
import android.util.AttributeSet;
import com.dlten.lib.STD;

public class CGLDCView extends GLSurfaceView {
    public static int HALF_HEIGHT;
    public static int HALF_WIDTH;
    public static int REAL_HEIGHT;
    public static int REAL_WIDTH = 480;
    private int m_nDblOffsetX;
    private int m_nDblOffsetY;
    private int m_nWndOffsetX;
    private int m_nWndOffsetY;
    private CRenderer m_renderer = null;

    public CGLDCView(Context context) {
        super(context);
        initCBaseView();
    }

    public CGLDCView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initCBaseView();
    }

    private void initCBaseView() {
        this.m_renderer = new CRenderer();
        setRenderer(this.m_renderer);
    }

    public void prepareDC() {
        int nBufWidth = getWidth();
        int nBufHeight = getHeight();
        int nHeightLimit = (REAL_WIDTH * 854) / 480;
        REAL_HEIGHT = (REAL_WIDTH * nBufHeight) / nBufWidth;
        if (REAL_HEIGHT > nHeightLimit) {
            REAL_HEIGHT = nHeightLimit;
        }
        STD.logout("init(" + nBufWidth + ", " + nBufHeight + ")");
        STD.logout("Drawing(" + REAL_WIDTH + ", " + REAL_HEIGHT + ")");
        HALF_WIDTH = REAL_WIDTH / 2;
        HALF_HEIGHT = REAL_HEIGHT / 2;
        this.m_nWndOffsetX = 0;
        this.m_nWndOffsetY = 0;
        this.m_nDblOffsetX = 0;
        this.m_nDblOffsetY = 0;
    }

    public static void setResWidth(int nResWidth) {
        REAL_WIDTH = nResWidth;
    }
}
