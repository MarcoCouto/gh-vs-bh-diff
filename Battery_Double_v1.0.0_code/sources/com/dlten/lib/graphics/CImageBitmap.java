package com.dlten.lib.graphics;

import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.support.v4.view.MotionEventCompat;
import com.dlten.lib.STD;

public class CImageBitmap extends CImage {
    private Bitmap m_img = null;

    /* access modifiers changed from: protected */
    public int loadResource(Bitmap img) {
        super.loadResource(img);
        if (CDCView.m_fScaleRes != 1.0f) {
            Bitmap rotateBitmap = null;
            try {
                rotateBitmap = Bitmap.createBitmap(img, 0, 0, img.getWidth(), img.getHeight(), CImage.getDCView().m_matrix, true);
            } catch (Exception e) {
                STD.printStackTrace(e);
            }
            this.m_img = rotateBitmap;
        } else {
            this.m_img = img;
        }
        return 1;
    }

    /* access modifiers changed from: protected */
    public int changeWithPer(int nPer) {
        Bitmap changeBitmap = null;
        try {
            int h = (this.m_img.getHeight() * nPer) / 100;
            int y = this.m_img.getHeight() - h;
            changeBitmap = Bitmap.createBitmap(this.m_img, 0, y, this.m_img.getWidth(), h);
        } catch (Exception e) {
            STD.printStackTrace(e);
        }
        this.m_img = changeBitmap;
        return 1;
    }

    /* access modifiers changed from: protected */
    public void unloadResource() {
        if (this.m_img != null) {
            this.m_img.recycle();
            this.m_img = null;
        }
        super.unloadResource();
    }

    public void drawImage(float fPosX, float fPosY, float fScaleX, float fScaleY, int nAlpha, Matrix matrix) {
        CDCView dc = CImage.getDCView();
        if (nAlpha != 255) {
            dc.setAlpha(nAlpha);
        }
        dc.drawImage(this.m_img, matrix);
        if (nAlpha != 255) {
            dc.setAlpha(MotionEventCompat.ACTION_MASK);
        }
    }

    /* access modifiers changed from: protected */
    public Bitmap getImage() {
        return this.m_img;
    }

    public int[] getRGBData() {
        if (this.m_img == null) {
            return null;
        }
        return getRGBData(0, 0, this.m_img.getWidth(), this.m_img.getHeight());
    }

    public int[] getRGBData(int x, int y, int w, int h) {
        return CBmpManager.getRGBData(this.m_img, x, y, w, h);
    }
}
