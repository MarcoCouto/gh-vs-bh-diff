package com.dlten.lib.graphics;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Region.Op;
import android.graphics.Typeface;
import android.util.AttributeSet;
import com.dlten.lib.Common;
import com.dlten.lib.STD;
import com.dlten.lib.frmWork.CEventWnd;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;

public abstract class CBmpDCView extends CDCView {
    private static Paint m_paintColor = new Paint();
    private Bitmap bmpBackup = null;
    private Paint m_font;
    private Typeface m_fontStyle;
    private RectF m_rtDst = new RectF();
    private Rect m_rtSrc = new Rect();
    protected Rect m_rtTDst = new Rect();
    protected Rect m_rtTSrc = new Rect();

    public CBmpDCView(Context context) {
        super(context);
        initBmpDCView();
    }

    public CBmpDCView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initBmpDCView();
    }

    private void initBmpDCView() {
        initFont();
        CImgObj.SetMode(0);
    }

    public void prepareDC() {
        super.prepareDC();
        clear(0);
        m_paintColor.setAntiAlias(false);
        m_paintColor.setDither(true);
    }

    public synchronized void update(CEventWnd pWnd) {
        if (pWnd != null) {
            Canvas canvasTemp = this.m_holder.lockCanvas();
            if (canvasTemp == null) {
                STD.ASSERT(false);
            } else {
                try {
                    clear();
                    pWnd.DrawPrevProc();
                    pWnd.DrawProcess();
                } catch (Exception e) {
                    STD.printStackTrace(e);
                }
                updateGraphics(canvasTemp);
                this.m_holder.unlockCanvasAndPost(canvasTemp);
            }
        }
    }

    public final void updateGraphics(Canvas canvasUpdate) {
        canvasUpdate.drawBitmap(this.m_bmpDblBuffer, (float) m_nDblOffsetX, (float) m_nDblOffsetY, m_paintColor);
    }

    public final int[] getRGBData(int x, int y, int w, int h) {
        try {
            return CBmpManager.getRGBData(this.m_bmpDblBuffer, x, y, w, h);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public final void captureDisp() {
        this.bmpBackup = Bitmap.createBitmap(this.m_bmpDblBuffer);
    }

    public final void restoreDisp() {
        if (this.bmpBackup != null) {
            this.m_canvas.drawBitmap(this.bmpBackup, BitmapDescriptorFactory.HUE_RED, BitmapDescriptorFactory.HUE_RED, null);
        }
    }

    public final void drawImage(Bitmap img, float nPosX, float nPosY) {
        if (img != null) {
            this.m_canvas.drawBitmap(img, nPosX, nPosY, m_paintColor);
        }
    }

    public final void drawImage(Bitmap img, Rect rtSrc, RectF rtDest) {
        if (img != null) {
            this.m_canvas.drawBitmap(img, rtSrc, rtDest, m_paintColor);
        }
    }

    public void drawImage(Bitmap img, Matrix matrix) {
        this.m_canvas.drawBitmap(img, matrix, m_paintColor);
    }

    public final void drawImage(Bitmap img, CRect rtSrc, CRect rtDest) {
        if (img != null && rtSrc != null && rtDest != null) {
            this.m_rtSrc.set((int) rtSrc.left, (int) rtSrc.top, (int) (rtSrc.left + rtSrc.width), (int) (rtSrc.top + rtSrc.height));
            this.m_rtDst.set(rtDest.left, rtDest.top, rtDest.left + rtDest.width, rtDest.top + rtDest.height);
            this.m_canvas.drawBitmap(img, this.m_rtSrc, this.m_rtDst, m_paintColor);
        }
    }

    public void drawString(String str, float nPosX, float nPosY, int nAnchor) {
        Align newAlign;
        int nGap;
        if (str != null) {
            int nHAnchor = nAnchor & 112;
            if (nHAnchor == 64) {
                newAlign = Align.RIGHT;
            } else if (nHAnchor == 32) {
                newAlign = Align.CENTER;
            } else {
                newAlign = Align.LEFT;
            }
            this.m_font.setTextAlign(newAlign);
            if ((nAnchor & 2) != 0) {
                int nAnchor2 = (nAnchor & -3) | 4;
                nGap = (-this.FONT_BASELINE) - (this.FONT_H / 2);
            } else if ((nAnchor & 4) != 0) {
                int nAnchor3 = (nAnchor & -5) | 4;
                nGap = (-this.FONT_BASELINE) - this.FONT_H;
            } else {
                int nAnchor4 = (nAnchor & -2) | 4;
                nGap = -this.FONT_BASELINE;
            }
            float nPosY2 = nPosY + ((float) nGap);
            this.m_font.setColor(m_paintColor.getColor());
            this.m_canvas.drawText(str, ((float) m_nWndOffsetX) + nPosX, ((float) m_nWndOffsetY) + nPosY2, this.m_font);
        }
    }

    /* access modifiers changed from: protected */
    public final void setClip(int[] rect) {
        setClip(rect[0], rect[1], rect[2], rect[3]);
    }

    /* access modifiers changed from: protected */
    public final void setClip(int x, int y, int width, int height) {
        this.m_canvas.clipRect((float) (m_nWndOffsetX + x), (float) (m_nWndOffsetY + y), (float) (m_nWndOffsetX + x + width), (float) (m_nWndOffsetY + y + height), Op.REPLACE);
    }

    /* access modifiers changed from: protected */
    public final void clearClip() {
        this.m_canvas.clipRect((float) m_nWndOffsetX, (float) m_nWndOffsetY, (float) (m_nWndOffsetX + m_drawWidth), (float) (m_nWndOffsetY + m_drawHeight), Op.REPLACE);
    }

    /* access modifiers changed from: protected */
    public final void copyArea(int x, int y, int width, int height, int dx, int dy) {
        copyArea(x, y, width, height, dx, dy, 17);
    }

    /* access modifiers changed from: protected */
    public final void copyArea(int x, int y, int width, int height, int dx, int dy, int nAnchor) {
        int dx2 = dx + m_nWndOffsetX;
        int dy2 = dy + m_nWndOffsetY;
        int[] imgData = getRGBData(x + m_nWndOffsetX, y + m_nWndOffsetY, width, height);
        float[] offset = CBmpManager.getLeftTopPos((float) width, (float) height, nAnchor);
        this.m_canvas.drawBitmap(imgData, 0, width, offset[0] + ((float) dx2), offset[1] + ((float) dy2), width, height, true, null);
    }

    public final void setLineWidth(float nWidth) {
        m_paintColor.setStrokeWidth(nWidth);
    }

    public final void drawLine(float nLeft, float nTop, float nRight, float nBottom) {
        this.m_canvas.drawLine(((float) m_nWndOffsetX) + nLeft, ((float) m_nWndOffsetY) + nTop, ((float) m_nWndOffsetX) + nRight, ((float) m_nWndOffsetY) + nBottom, m_paintColor);
    }

    public final void drawRect(int[] rect) {
        drawRect(rect[0], rect[1], rect[2], rect[3]);
    }

    public final void drawRect(int nLeft, int nTop, int nWidth, int nHeight) {
        m_paintColor.setStyle(Style.STROKE);
        m_paintColor.setStrokeWidth(1.0f);
        this.m_canvas.drawRect((float) (m_nWndOffsetX + nLeft), (float) (m_nWndOffsetY + nTop), (float) (m_nWndOffsetX + nLeft + nWidth), (float) (m_nWndOffsetY + nTop + nHeight), m_paintColor);
    }

    public void fillRect(int nLeft, int nTop, int nWidth, int nHeight) {
        m_paintColor.setStyle(Style.FILL);
        this.m_canvas.drawRect(Common.code2screen((float) (m_nWndOffsetX + nLeft)), Common.code2screen((float) (m_nWndOffsetY + nTop)), Common.code2screen((float) (m_nWndOffsetX + nLeft + nWidth)), Common.code2screen((float) (m_nWndOffsetY + nTop + nHeight)), m_paintColor);
    }

    public void setARGB(int nAlpha, int nR, int nG, int nB) {
        m_paintColor.setARGB(nAlpha, nR, nG, nB);
    }

    public void setAlpha(int nAlpha) {
        m_paintColor.setAlpha(nAlpha);
    }

    public void setRotate(float fDegress, int x, int y) {
        this.m_canvas.rotate(fDegress, (float) x, (float) y);
    }

    private void initFont() {
        this.m_fontStyle = Typeface.create(Typeface.DEFAULT, 0);
        this.m_font = new Paint();
        this.m_font.setTypeface(this.m_fontStyle);
        this.m_font.setTextSize((float) this.m_nFontSize);
        this.m_font.setAntiAlias(false);
        this.m_font.setDither(true);
        this.FONT_BASELINE = (int) this.m_font.getFontMetrics().descent;
        this.FONT_H = (int) (this.m_font.getFontMetrics().top - this.m_font.getFontMetrics().bottom);
        setFont(this.m_font);
        setFontSize(this.m_nFontSize);
    }

    /* access modifiers changed from: protected */
    public final void setFont(Paint font) {
        this.m_font = font;
        if (this.m_font != null) {
            this.FONT_H = (int) this.m_font.getTextSize();
            this.FONT_BASELINE = (int) this.m_font.ascent();
        }
    }

    /* access modifiers changed from: protected */
    public final void setFont() {
        this.m_font.setTextSize((float) this.m_nFontSize);
        setFont(this.m_font);
    }

    /* access modifiers changed from: protected */
    public final Paint getFont() {
        return this.m_font;
    }

    /* access modifiers changed from: protected */
    public final int getFontWidth(Paint font) {
        float[] fWidth = new float[1];
        font.getTextWidths("W", fWidth);
        return (int) fWidth[0];
    }

    /* access modifiers changed from: protected */
    public final int getStrWidth(String str) {
        if (this.m_font == null || str == null || str.length() <= 0) {
            return 0;
        }
        float fRet = BitmapDescriptorFactory.HUE_RED;
        float[] fWidth = new float[str.length()];
        this.m_font.getTextWidths(str, fWidth);
        for (float f : fWidth) {
            fRet += f;
        }
        return (int) fRet;
    }
}
