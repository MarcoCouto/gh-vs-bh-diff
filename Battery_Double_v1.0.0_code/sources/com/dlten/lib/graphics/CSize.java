package com.dlten.lib.graphics;

import com.google.android.gms.maps.model.BitmapDescriptorFactory;

public class CSize {
    public float h;
    public float w;

    public CSize() {
    }

    public CSize(float initCX, float initCY) {
        this.w = initCX;
        this.h = initCY;
    }

    public CSize(CSize initSize) {
        this.w = initSize.w;
        this.h = initSize.h;
    }

    public CSize(CPoint initPt) {
        this.w = initPt.x;
        this.h = initPt.y;
    }

    public float getWidth() {
        return this.w;
    }

    public float getHeight() {
        return this.h;
    }

    public String toString() {
        return "<" + this.w + ", " + this.h + ">";
    }

    public static CSize make(float w2, float h2) {
        return new CSize(w2, h2);
    }

    public static CSize zero() {
        return new CSize(BitmapDescriptorFactory.HUE_RED, BitmapDescriptorFactory.HUE_RED);
    }

    public static boolean equalToSize(CSize s1, CSize s2) {
        return s1.w == s2.w && s1.h == s2.h;
    }
}
