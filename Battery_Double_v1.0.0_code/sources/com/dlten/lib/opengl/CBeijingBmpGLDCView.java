package com.dlten.lib.opengl;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.opengl.GLES10;
import android.opengl.GLU;
import android.support.v4.view.MotionEventCompat;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import com.dlten.lib.STD;
import com.dlten.lib.frmWork.CEventWnd;
import com.dlten.lib.graphics.CBmpDCView;
import com.dlten.lib.graphics.CImageTexture;
import com.dlten.lib.graphics.CRect;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;
import java.util.concurrent.Semaphore;
import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLContext;
import javax.microedition.khronos.egl.EGLDisplay;
import javax.microedition.khronos.egl.EGLSurface;
import javax.microedition.khronos.opengles.GL;
import javax.microedition.khronos.opengles.GL10;

public class CBeijingBmpGLDCView extends CBmpDCView {
    private static final int MAX_RECT_COUNT = 200;
    private static final Semaphore sEglSemaphore = new Semaphore(1);
    private GL10 GLThread_local_gl = null;
    private boolean GLThread_local_tellRendererSurfaceChanged;
    private boolean GLThread_local_tellRendererSurfaceCreated;
    private FloatBuffer mColorBuffer0;
    private boolean mContextLost;
    private volatile boolean mDone;
    private EglHelper mEglHelper;
    private FloatBuffer mFVertexBuffer;
    private FloatBuffer mFVertexBuffer0;
    private boolean mHasFocus;
    private boolean mHasSurface;
    private int mHeight;
    private ShortBuffer mIndexBuffer;
    private ShortBuffer mIndexBuffer0;
    private boolean mPaused;
    private boolean mSizeChanged = true;
    private FloatBuffer mTexBuffer;
    private int mWidth;
    private Bitmap m_CurTextureBitmap;
    private Canvas m_CurTextureCanvas;
    private ByteBuffer m_cbb0 = ByteBuffer.allocateDirect(64);
    private float m_fAlpha;
    private ByteBuffer m_ibb = ByteBuffer.allocateDirect(1600);
    private ByteBuffer m_ibb0 = ByteBuffer.allocateDirect(8);
    CImageTexture m_img = new CImageTexture();
    private ByteBuffer m_tbb = ByteBuffer.allocateDirect(6400);
    private ByteBuffer m_vbb = ByteBuffer.allocateDirect(9600);
    private ByteBuffer m_vbb0 = ByteBuffer.allocateDirect(48);

    private class EglHelper {
        EGL10 mEgl;
        EGLConfig mEglConfig;
        EGLContext mEglContext;
        EGLDisplay mEglDisplay;
        EGLSurface mEglSurface;

        public EglHelper() {
        }

        public void start(int[] configSpec) {
            this.mEgl = (EGL10) EGLContext.getEGL();
            this.mEglDisplay = this.mEgl.eglGetDisplay(EGL10.EGL_DEFAULT_DISPLAY);
            this.mEgl.eglInitialize(this.mEglDisplay, new int[2]);
            EGLConfig[] configs = new EGLConfig[1];
            this.mEgl.eglChooseConfig(this.mEglDisplay, configSpec, configs, 1, new int[1]);
            this.mEglConfig = configs[0];
            this.mEglContext = this.mEgl.eglCreateContext(this.mEglDisplay, this.mEglConfig, EGL10.EGL_NO_CONTEXT, null);
            this.mEglSurface = null;
        }

        public GL createSurface(SurfaceHolder holder) {
            if (this.mEglSurface != null) {
                this.mEgl.eglMakeCurrent(this.mEglDisplay, EGL10.EGL_NO_SURFACE, EGL10.EGL_NO_SURFACE, EGL10.EGL_NO_CONTEXT);
                this.mEgl.eglDestroySurface(this.mEglDisplay, this.mEglSurface);
            }
            this.mEglSurface = this.mEgl.eglCreateWindowSurface(this.mEglDisplay, this.mEglConfig, holder, null);
            this.mEgl.eglMakeCurrent(this.mEglDisplay, this.mEglSurface, this.mEglSurface, this.mEglContext);
            return this.mEglContext.getGL();
        }

        public boolean swap() {
            this.mEgl.eglSwapBuffers(this.mEglDisplay, this.mEglSurface);
            return this.mEgl.eglGetError() != 12302;
        }

        public void finish() {
            if (this.mEglSurface != null) {
                this.mEgl.eglMakeCurrent(this.mEglDisplay, EGL10.EGL_NO_SURFACE, EGL10.EGL_NO_SURFACE, EGL10.EGL_NO_CONTEXT);
                this.mEgl.eglDestroySurface(this.mEglDisplay, this.mEglSurface);
                this.mEglSurface = null;
            }
            if (this.mEglContext != null) {
                this.mEgl.eglDestroyContext(this.mEglDisplay, this.mEglContext);
                this.mEglContext = null;
            }
            if (this.mEglDisplay != null) {
                this.mEgl.eglTerminate(this.mEglDisplay);
                this.mEglDisplay = null;
            }
        }
    }

    public CBeijingBmpGLDCView(Context context) {
        super(context);
        initCBaseView();
    }

    public CBeijingBmpGLDCView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initCBaseView();
    }

    private void initCBaseView() {
        GLThread_Constructor();
        this.m_vbb.order(ByteOrder.nativeOrder());
        this.m_tbb.order(ByteOrder.nativeOrder());
        this.m_ibb.order(ByteOrder.nativeOrder());
        this.m_vbb0.order(ByteOrder.nativeOrder());
        this.m_cbb0.order(ByteOrder.nativeOrder());
        this.m_ibb0.order(ByteOrder.nativeOrder());
        this.mFVertexBuffer0 = this.m_vbb0.asFloatBuffer();
        this.mIndexBuffer0 = this.m_ibb0.asShortBuffer();
        this.mFVertexBuffer0.put(-0.5f);
        this.mFVertexBuffer0.put(0.5f);
        this.mFVertexBuffer0.put(BitmapDescriptorFactory.HUE_RED);
        this.mFVertexBuffer0.put(-0.5f);
        this.mFVertexBuffer0.put(-0.5f);
        this.mFVertexBuffer0.put(BitmapDescriptorFactory.HUE_RED);
        this.mFVertexBuffer0.put(0.5f);
        this.mFVertexBuffer0.put(-0.5f);
        this.mFVertexBuffer0.put(BitmapDescriptorFactory.HUE_RED);
        this.mFVertexBuffer0.put(0.5f);
        this.mFVertexBuffer0.put(0.5f);
        this.mFVertexBuffer0.put(BitmapDescriptorFactory.HUE_RED);
        this.mFVertexBuffer0.position(0);
        this.mIndexBuffer0.put(0);
        this.mIndexBuffer0.put(1);
        this.mIndexBuffer0.put(2);
        this.mIndexBuffer0.put(3);
        this.mIndexBuffer0.position(0);
    }

    public void prepareDC() {
        super.prepareDC();
        GLThread_run_start();
    }

    public void releaseDC() {
        GLThread_run_end();
    }

    public final synchronized void update(CEventWnd pWnd) {
        if (pWnd != null) {
            try {
                GLThread_run_loop();
                if (surfaceInitialized()) {
                    clear();
                    GLES10.glActiveTexture(33984);
                    pWnd.DrawPrevProc();
                    pWnd.DrawProcess();
                    this.m_img.load(this.m_bmpDblBuffer);
                    this.m_img.drawImage((float) BitmapDescriptorFactory.HUE_RED, (float) BitmapDescriptorFactory.HUE_RED, 1.0f, 1.0f, (int) MotionEventCompat.ACTION_MASK, (Matrix) null);
                }
            } catch (Exception e) {
                STD.printStackTrace(e);
            }
        }
        return;
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
        GLThread_onWindowResize(w, h);
    }

    public void surfaceCreated(SurfaceHolder holder) {
        GLThread_surfaceCreated();
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        GLThread_surfaceDestroyed();
    }

    public void onWindowFocusChanged(boolean hasWindowFocus) {
        super.onWindowFocusChanged(hasWindowFocus);
        GLThread_onWindowFocusChanged(hasWindowFocus);
    }

    public void GLThread_surfaceCreated() {
        synchronized (this) {
            this.mHasSurface = true;
            this.mContextLost = false;
            notify();
        }
    }

    public void GLThread_surfaceDestroyed() {
        synchronized (this) {
            this.mHasSurface = false;
            notify();
        }
    }

    public void GLThread_onWindowResize(int w, int h) {
        synchronized (this) {
            this.mWidth = w;
            this.mHeight = h;
            this.mSizeChanged = true;
        }
    }

    public void GLThread_onWindowFocusChanged(boolean hasFocus) {
        this.mHasFocus = true;
    }

    public void GLThread_Constructor() {
        this.mDone = false;
        this.mWidth = 0;
        this.mHeight = 0;
    }

    private boolean GLThread_needToWait() {
        return (this.mPaused || !this.mHasFocus || !this.mHasSurface || this.mContextLost) && !this.mDone;
    }

    public void GLThread_run_start() {
        try {
            sEglSemaphore.acquire();
            this.mEglHelper = new EglHelper();
            this.mEglHelper.start(null);
            this.GLThread_local_gl = null;
            this.GLThread_local_tellRendererSurfaceCreated = true;
            this.GLThread_local_tellRendererSurfaceChanged = true;
        } catch (Exception | InterruptedException e) {
        }
    }

    public void GLThread_run_end() {
        this.mEglHelper.finish();
        sEglSemaphore.release();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0036, code lost:
        if (r2 == false) goto L_0x0041;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0038, code lost:
        r8.mEglHelper.start(null);
        r8.GLThread_local_tellRendererSurfaceCreated = true;
        r0 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0041, code lost:
        if (r0 == false) goto L_0x0051;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0043, code lost:
        r8.GLThread_local_gl = (javax.microedition.khronos.opengles.GL10) r8.mEglHelper.createSurface(r8.m_holder);
        r8.GLThread_local_tellRendererSurfaceChanged = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0053, code lost:
        if (r8.GLThread_local_tellRendererSurfaceCreated == false) goto L_0x005a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0055, code lost:
        setCamera();
        r8.GLThread_local_tellRendererSurfaceCreated = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x005c, code lost:
        if (r8.GLThread_local_tellRendererSurfaceChanged == false) goto L_0x0060;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x005e, code lost:
        r8.GLThread_local_tellRendererSurfaceChanged = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0060, code lost:
        if (r3 <= 0) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0062, code lost:
        if (r1 <= 0) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0064, code lost:
        r8.mEglHelper.swap();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:?, code lost:
        return;
     */
    public void GLThread_run_loop() throws InterruptedException {
        if (!this.mDone) {
            boolean needStart = false;
            synchronized (this) {
                if (this.mPaused) {
                    this.mEglHelper.finish();
                    needStart = true;
                }
                if (GLThread_needToWait()) {
                    while (GLThread_needToWait()) {
                        wait();
                    }
                }
                if (!this.mDone) {
                    boolean changed = this.mSizeChanged;
                    int w = this.mWidth;
                    int h = this.mHeight;
                    this.mSizeChanged = false;
                }
            }
        }
    }

    public boolean surfaceInitialized() {
        return this.GLThread_local_gl != null;
    }

    public int createTexture(int width, int height) {
        try {
            this.m_CurTextureBitmap = Bitmap.createBitmap(width, height, Config.ARGB_4444);
        } catch (Exception e) {
            int n = 1 + 1;
        }
        this.m_CurTextureCanvas = new Canvas(this.m_CurTextureBitmap);
        GLES10.glDisable(3024);
        GLES10.glHint(3152, 4353);
        GLES10.glClearColor(BitmapDescriptorFactory.HUE_RED, BitmapDescriptorFactory.HUE_RED, BitmapDescriptorFactory.HUE_RED, 1.0f);
        GLES10.glShadeModel(7425);
        GLES10.glEnable(2929);
        GLES10.glEnable(3553);
        int[] textures = new int[1];
        GLES10.glGenTextures(1, textures, 0);
        int TextureID = textures[0];
        GLES10.glBindTexture(3553, TextureID);
        GLES10.glTexParameterf(3553, 10241, 9729.0f);
        GLES10.glTexParameterf(3553, 10240, 9729.0f);
        GLES10.glTexParameterf(3553, 10242, 33071.0f);
        GLES10.glTexParameterf(3553, 10243, 33071.0f);
        GLES10.glTexEnvf(8960, 8704, 7681.0f);
        return TextureID;
    }

    public void deleteTexture(int textureID) {
        GLES10.glDeleteTextures(1, new int[]{textureID}, 0);
    }

    private void setCamera() {
        GLES10.glViewport(m_nDblOffsetX, m_nDblOffsetY, SC_WIDTH - (m_nDblOffsetX * 2), SC_HEIGHT - (m_nDblOffsetY * 2));
        GLES10.glMatrixMode(5889);
        GLES10.glLoadIdentity();
        GLES10.glFrustumf(-0.25f, 0.25f, -0.25f, 0.25f, 2.5f, 5.5f);
    }

    public void setTexture(int nTextureID) {
        GLES10.glBindTexture(3553, nTextureID);
    }

    public void clear() {
        GLES10.glDisable(3024);
        GLES10.glTexEnvx(8960, 8704, 8448);
        GLES10.glClear(16640);
        GLES10.glMatrixMode(5888);
        GLES10.glLoadIdentity();
        GLU.gluLookAt(this.GLThread_local_gl, BitmapDescriptorFactory.HUE_RED, BitmapDescriptorFactory.HUE_RED, 5.0f, BitmapDescriptorFactory.HUE_RED, BitmapDescriptorFactory.HUE_RED, BitmapDescriptorFactory.HUE_RED, BitmapDescriptorFactory.HUE_RED, 1.0f, BitmapDescriptorFactory.HUE_RED);
        GLES10.glEnableClientState(32884);
        GLES10.glEnableClientState(32888);
        this.mFVertexBuffer = this.m_vbb.asFloatBuffer();
        this.mTexBuffer = this.m_tbb.asFloatBuffer();
        this.mIndexBuffer = this.m_ibb.asShortBuffer();
        GLES10.glEnable(3042);
        GLES10.glBlendFunc(770, 771);
        this.m_fAlpha = 1.0f;
    }

    public void drawImage(int textureID, float x, float y, float w, float h, float scaleX, float scaleY, CRect rectTexture) {
        GLES10.glBindTexture(3553, textureID);
        float left = (x / ((float) RES_WIDTH)) - 0.5f;
        float top = 0.5f - (y / ((float) RES_HEIGHT));
        float right = (((w * scaleX) + x) / ((float) RES_WIDTH)) - 0.5f;
        float bottom = 0.5f - (((h * scaleY) + y) / ((float) RES_HEIGHT));
        this.mFVertexBuffer.put(left);
        this.mFVertexBuffer.put(top);
        this.mFVertexBuffer.put(BitmapDescriptorFactory.HUE_RED);
        this.mFVertexBuffer.put(left);
        this.mFVertexBuffer.put(bottom);
        this.mFVertexBuffer.put(BitmapDescriptorFactory.HUE_RED);
        this.mFVertexBuffer.put(right);
        this.mFVertexBuffer.put(bottom);
        this.mFVertexBuffer.put(BitmapDescriptorFactory.HUE_RED);
        this.mFVertexBuffer.put(right);
        this.mFVertexBuffer.put(top);
        this.mFVertexBuffer.put(BitmapDescriptorFactory.HUE_RED);
        float left2 = rectTexture.left;
        float top2 = rectTexture.top;
        float right2 = rectTexture.Right();
        float bottom2 = rectTexture.Bottom();
        this.mTexBuffer.put(left2);
        this.mTexBuffer.put(top2);
        this.mTexBuffer.put(left2);
        this.mTexBuffer.put(bottom2);
        this.mTexBuffer.put(right2);
        this.mTexBuffer.put(bottom2);
        this.mTexBuffer.put(right2);
        this.mTexBuffer.put(top2);
        this.mIndexBuffer.put(0);
        this.mIndexBuffer.put(1);
        this.mIndexBuffer.put(2);
        this.mIndexBuffer.put(3);
        this.mFVertexBuffer.position(0);
        this.mTexBuffer.position(0);
        GLES10.glFrontFace(2304);
        GLES10.glVertexPointer(3, 5126, 0, this.mFVertexBuffer);
        GLES10.glEnable(3553);
        GLES10.glTexCoordPointer(2, 5126, 0, this.mTexBuffer);
        this.mIndexBuffer.position(0);
        GLES10.glDrawElements(6, 4, 5123, this.mIndexBuffer);
        if (this.m_fAlpha != 1.0f) {
            this.mColorBuffer0 = this.m_cbb0.asFloatBuffer();
            for (int i = 0; i < 4; i++) {
                this.mColorBuffer0.put(BitmapDescriptorFactory.HUE_RED);
                this.mColorBuffer0.put(BitmapDescriptorFactory.HUE_RED);
                this.mColorBuffer0.put(BitmapDescriptorFactory.HUE_RED);
                this.mColorBuffer0.put(1.0f - this.m_fAlpha);
            }
            this.mColorBuffer0.position(0);
            GLES10.glDisableClientState(32888);
            GLES10.glEnableClientState(32886);
            GLES10.glDisable(3553);
            GLES10.glVertexPointer(3, 5126, 0, this.mFVertexBuffer0);
            GLES10.glColorPointer(4, 5126, 0, this.mColorBuffer0);
            GLES10.glDrawElements(6, 4, 5123, this.mIndexBuffer0);
            GLES10.glEnableClientState(32888);
            GLES10.glDisableClientState(32886);
            GLES10.glEnable(3553);
        }
    }

    public void setAlpha(int nAlpha) {
        this.m_fAlpha = ((float) nAlpha) / 255.0f;
    }
}
