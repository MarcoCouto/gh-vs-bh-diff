package com.dlten.lib.opengl;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.opengl.GLUtils;
import android.util.Log;
import com.dlten.lib.graphics.CDCView;
import com.dlten.lib.opengl.CMyGLDCView.Renderer;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import java.io.IOException;
import java.io.InputStream;
import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.opengles.GL11;

public class SimpleGLRenderer implements Renderer {
    private static Options sBitmapOptions = new Options();
    private Context mContext;
    private int[] mCropWorkspace;
    private GLSprite[] mSprites;
    private int[] mTextureNameWorkspace;
    private boolean mUseHardwareBuffers;
    private boolean mUseVerts;
    private CMyGLDCView m_parent;

    public SimpleGLRenderer(Context context, CMyGLDCView parent) {
        this.m_parent = null;
        this.mTextureNameWorkspace = new int[1];
        this.mCropWorkspace = new int[4];
        sBitmapOptions.inPreferredConfig = Config.RGB_565;
        this.mContext = context;
        this.m_parent = parent;
    }

    public int[] getConfigSpec() {
        int[] configSpec = new int[3];
        configSpec[0] = 12325;
        configSpec[2] = 12344;
        return configSpec;
    }

    public void setSprites(GLSprite[] sprites) {
        this.mSprites = sprites;
    }

    public void setVertMode(boolean useVerts, boolean useHardwareBuffers) {
        this.mUseVerts = useVerts;
        if (!useVerts) {
            useHardwareBuffers = false;
        }
        this.mUseHardwareBuffers = useHardwareBuffers;
    }

    public void drawFrame(GL10 gl) {
        if (this.mSprites != null) {
            deleteSprites(gl);
            createSprites(gl);
            gl.glMatrixMode(5888);
            if (this.mUseVerts) {
                Grid.beginDrawing(gl, true, false);
            }
            for (GLSprite draw : this.mSprites) {
                draw.draw(gl);
            }
            if (this.mUseVerts) {
                Grid.endDrawing(gl);
            }
        }
    }

    public void sizeChanged(GL10 gl, int width, int height) {
        int resWidth = CDCView.getResWidth();
        int resHeight = CDCView.getResHeight();
        int bufWidth = CDCView.getBufWidth();
        int bufHeight = CDCView.getBufHeight();
        gl.glViewport(0, (height - bufHeight) - CDCView.getDblOffsetY(), resWidth, bufHeight);
        gl.glMatrixMode(5889);
        gl.glLoadIdentity();
        gl.glOrthof(BitmapDescriptorFactory.HUE_RED, (float) resWidth, BitmapDescriptorFactory.HUE_RED, (float) bufHeight, BitmapDescriptorFactory.HUE_RED, 1.0f);
        gl.glShadeModel(7424);
        gl.glEnable(3042);
        gl.glBlendFunc(770, 771);
        gl.glColor4x(65536, 65536, 65536, 65536);
        gl.glEnable(3553);
    }

    public void surfaceCreated(GL10 gl) {
        gl.glHint(3152, 4353);
        gl.glClearColor(BitmapDescriptorFactory.HUE_RED, BitmapDescriptorFactory.HUE_RED, BitmapDescriptorFactory.HUE_RED, 1.0f);
        gl.glShadeModel(7424);
        gl.glDisable(2929);
        gl.glEnable(3553);
        gl.glDisable(3024);
        gl.glDisable(2896);
        gl.glClear(16640);
        createSprites(gl);
    }

    public void shutdown(GL10 gl) {
        deleteSprites(gl);
    }

    /* access modifiers changed from: protected */
    public int loadBitmap(Context context, GL10 gl, Bitmap bitmap) {
        int textureName = -1;
        if (!(context == null || gl == null || bitmap == null)) {
            gl.glGenTextures(1, this.mTextureNameWorkspace, 0);
            textureName = this.mTextureNameWorkspace[0];
            gl.glBindTexture(3553, textureName);
            gl.glTexParameterf(3553, 10241, 9729.0f);
            gl.glTexParameterf(3553, 10240, 9729.0f);
            gl.glTexEnvf(8960, 8704, 7681.0f);
            GLUtils.texImage2D(3553, 0, bitmap, 0);
            this.mCropWorkspace[0] = 0;
            this.mCropWorkspace[1] = bitmap.getHeight();
            this.mCropWorkspace[2] = bitmap.getWidth();
            this.mCropWorkspace[3] = -bitmap.getHeight();
            ((GL11) gl).glTexParameteriv(3553, 35741, this.mCropWorkspace, 0);
            int error = gl.glGetError();
            if (error != 0) {
                Log.e("SpriteMethodTest", "Texture Load GLError: " + error);
            }
        }
        return textureName;
    }

    /* access modifiers changed from: protected */
    public int loadBitmap(Context context, GL10 gl, int resourceId) {
        InputStream is = context.getResources().openRawResource(resourceId);
        try {
            Bitmap bitmap = BitmapFactory.decodeStream(is, null, sBitmapOptions);
            int textureName = loadBitmap(context, gl, bitmap);
            bitmap.recycle();
            return textureName;
        } finally {
            try {
                is.close();
            } catch (IOException e) {
            }
        }
    }

    /* access modifiers changed from: protected */
    public void changeBitmap(GL10 gl, int textureName, Bitmap bitmap) {
        if (gl != null && textureName != -1 && bitmap != null) {
            gl.glBindTexture(3553, textureName);
            gl.glCopyTexImage2D(3553, 0, 6408, 0, 0, bitmap.getWidth(), bitmap.getHeight(), 0);
        }
    }

    private void createSprites(GL10 gl) {
        int lastTextureId;
        if (this.mSprites != null) {
            if (this.mUseHardwareBuffers) {
                for (GLSprite grid : this.mSprites) {
                    grid.getGrid().invalidateHardwareBuffers();
                }
            }
            for (int x = 0; x < this.mSprites.length; x++) {
                int resource = this.mSprites[x].getResourceId();
                Bitmap bitmap = this.mSprites[x].getBmp();
                synchronized (bitmap) {
                    lastTextureId = loadBitmap(this.mContext, gl, bitmap);
                }
                int lastLoadedResource = resource;
                this.mSprites[x].setTextureName(lastTextureId);
                if (this.mUseHardwareBuffers) {
                    Grid currentGrid = this.mSprites[x].getGrid();
                    if (!currentGrid.usingHardwareBuffers()) {
                        currentGrid.generateHardwareBuffers(gl);
                    }
                }
            }
        }
    }

    private void changeSprites(GL10 gl) {
        if (this.mSprites != null) {
            for (int x = 0; x < this.mSprites.length; x++) {
                int lastTextureId = this.mSprites[x].getTextureName();
                if (lastTextureId != -1) {
                    Bitmap bitmap = this.mSprites[x].getBmp();
                    synchronized (bitmap) {
                        changeBitmap(gl, lastTextureId, bitmap);
                    }
                }
            }
        }
    }

    private void deleteSprites(GL10 gl) {
        if (this.mSprites != null) {
            int[] textureToDelete = new int[1];
            for (int x = 0; x < this.mSprites.length; x++) {
                int resourceId = this.mSprites[x].getResourceId();
                textureToDelete[0] = this.mSprites[x].getTextureName();
                gl.glDeleteTextures(1, textureToDelete, 0);
                this.mSprites[x].setTextureName(0);
                if (this.mUseHardwareBuffers) {
                    this.mSprites[x].getGrid().releaseHardwareBuffers(gl);
                }
            }
        }
    }
}
