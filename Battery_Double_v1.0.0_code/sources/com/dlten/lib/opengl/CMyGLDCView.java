package com.dlten.lib.opengl;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import com.dlten.lib.STD;
import com.dlten.lib.frmWork.CEventWnd;
import com.dlten.lib.graphics.CBmpDCView;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import java.util.concurrent.Semaphore;
import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLContext;
import javax.microedition.khronos.egl.EGLDisplay;
import javax.microedition.khronos.egl.EGLSurface;
import javax.microedition.khronos.opengles.GL;
import javax.microedition.khronos.opengles.GL10;

public class CMyGLDCView extends CBmpDCView {
    /* access modifiers changed from: private */
    public static final Semaphore sEglSemaphore = new Semaphore(1);
    private GLThread mGLThread;
    /* access modifiers changed from: private */
    public GLWrapper mGLWrapper;
    /* access modifiers changed from: private */
    public boolean mSizeChanged = true;
    private SimpleGLRenderer m_renderer = null;

    private class EglHelper {
        EGL10 mEgl;
        EGLConfig mEglConfig;
        EGLContext mEglContext;
        EGLDisplay mEglDisplay;
        EGLSurface mEglSurface;

        public EglHelper() {
        }

        public void start(int[] configSpec) {
            this.mEgl = (EGL10) EGLContext.getEGL();
            this.mEglDisplay = this.mEgl.eglGetDisplay(EGL10.EGL_DEFAULT_DISPLAY);
            this.mEgl.eglInitialize(this.mEglDisplay, new int[2]);
            EGLConfig[] configs = new EGLConfig[1];
            this.mEgl.eglChooseConfig(this.mEglDisplay, configSpec, configs, 1, new int[1]);
            this.mEglConfig = configs[0];
            this.mEglContext = this.mEgl.eglCreateContext(this.mEglDisplay, this.mEglConfig, EGL10.EGL_NO_CONTEXT, null);
            this.mEglSurface = null;
        }

        public GL createSurface(SurfaceHolder holder) {
            if (this.mEglSurface != null) {
                this.mEgl.eglMakeCurrent(this.mEglDisplay, EGL10.EGL_NO_SURFACE, EGL10.EGL_NO_SURFACE, EGL10.EGL_NO_CONTEXT);
                this.mEgl.eglDestroySurface(this.mEglDisplay, this.mEglSurface);
            }
            this.mEglSurface = this.mEgl.eglCreateWindowSurface(this.mEglDisplay, this.mEglConfig, holder, null);
            this.mEgl.eglMakeCurrent(this.mEglDisplay, this.mEglSurface, this.mEglSurface, this.mEglContext);
            GL gl = this.mEglContext.getGL();
            if (CMyGLDCView.this.mGLWrapper != null) {
                return CMyGLDCView.this.mGLWrapper.wrap(gl);
            }
            return gl;
        }

        public boolean swap() {
            this.mEgl.eglSwapBuffers(this.mEglDisplay, this.mEglSurface);
            return this.mEgl.eglGetError() != 12302;
        }

        public void finish() {
            if (this.mEglSurface != null) {
                this.mEgl.eglMakeCurrent(this.mEglDisplay, EGL10.EGL_NO_SURFACE, EGL10.EGL_NO_SURFACE, EGL10.EGL_NO_CONTEXT);
                this.mEgl.eglDestroySurface(this.mEglDisplay, this.mEglSurface);
                this.mEglSurface = null;
            }
            if (this.mEglContext != null) {
                this.mEgl.eglDestroyContext(this.mEglDisplay, this.mEglContext);
                this.mEglContext = null;
            }
            if (this.mEglDisplay != null) {
                this.mEgl.eglTerminate(this.mEglDisplay);
                this.mEglDisplay = null;
            }
        }
    }

    class GLThread extends Thread {
        private boolean mContextLost;
        private boolean mDone = false;
        private EglHelper mEglHelper;
        private Runnable mEvent;
        private boolean mHasFocus;
        private boolean mHasSurface;
        private int mHeight = 0;
        private boolean mPaused;
        private Renderer mRenderer;
        private int mWidth = 0;

        GLThread(Renderer renderer) {
            this.mRenderer = renderer;
            setName("GLThread");
        }

        /* JADX WARNING: Code restructure failed: missing block: B:10:0x0025, code lost:
            com.dlten.lib.opengl.CMyGLDCView.access$1().release();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:11:0x002c, code lost:
            throw r1;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:14:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:8:0x001c, code lost:
            com.dlten.lib.opengl.CMyGLDCView.access$1().release();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:9:0x0024, code lost:
            r1 = move-exception;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Removed duplicated region for block: B:9:0x0024 A[ExcHandler:  FINALLY, Splitter:B:0:0x0000] */
        public void run() {
            try {
                CMyGLDCView.sEglSemaphore.acquire();
                guardedRun();
            } catch (InterruptedException e) {
            } finally {
            }
        }

        /* JADX WARNING: Code restructure failed: missing block: B:31:0x0067, code lost:
            if (r4 == false) goto L_0x0070;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:32:0x0069, code lost:
            r10.mEglHelper.start(r1);
            r6 = true;
            r0 = true;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:33:0x0070, code lost:
            if (r0 == false) goto L_0x0081;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:34:0x0072, code lost:
            r2 = (javax.microedition.khronos.opengles.GL10) r10.mEglHelper.createSurface(com.dlten.lib.opengl.CMyGLDCView.access$4(r10.this$0));
            r5 = true;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:35:0x0081, code lost:
            if (r6 == false) goto L_0x0089;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:36:0x0083, code lost:
            r10.mRenderer.surfaceCreated(r2);
            r6 = false;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:37:0x0089, code lost:
            if (r5 == false) goto L_0x0091;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:38:0x008b, code lost:
            r10.mRenderer.sizeChanged(r2, r7, r3);
            r5 = false;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:39:0x0091, code lost:
            if (r7 <= 0) goto L_0x0017;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:40:0x0093, code lost:
            if (r3 <= 0) goto L_0x0017;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:41:0x0095, code lost:
            r10.mRenderer.drawFrame(r2);
            r10.mEglHelper.swap();
         */
        private void guardedRun() throws InterruptedException {
            this.mEglHelper = new EglHelper();
            int[] configSpec = this.mRenderer.getConfigSpec();
            this.mEglHelper.start(configSpec);
            GL10 gl = null;
            boolean tellRendererSurfaceCreated = true;
            boolean tellRendererSurfaceChanged = true;
            while (true) {
                if (this.mDone) {
                    break;
                }
                boolean needStart = false;
                synchronized (this) {
                    if (this.mEvent != null) {
                        this.mEvent.run();
                    }
                    if (this.mPaused) {
                        this.mEglHelper.finish();
                        needStart = true;
                    }
                    if (needToWait()) {
                        while (needToWait()) {
                            wait();
                        }
                    }
                    if (!this.mDone) {
                        boolean changed = CMyGLDCView.this.mSizeChanged;
                        int w = this.mWidth;
                        int h = this.mHeight;
                        CMyGLDCView.this.mSizeChanged = false;
                    }
                }
            }
            if (gl != null) {
                this.mRenderer.shutdown(gl);
            }
            this.mEglHelper.finish();
        }

        private boolean needToWait() {
            return (this.mPaused || !this.mHasFocus || !this.mHasSurface || this.mContextLost) && !this.mDone;
        }

        public void surfaceCreated() {
            synchronized (this) {
                this.mHasSurface = true;
                this.mContextLost = false;
                notify();
            }
        }

        public void surfaceDestroyed() {
            synchronized (this) {
                this.mHasSurface = false;
                notify();
            }
        }

        public void onPause() {
            synchronized (this) {
                this.mPaused = true;
            }
        }

        public void onResume() {
            synchronized (this) {
                this.mPaused = false;
                notify();
            }
        }

        public void onWindowFocusChanged(boolean hasFocus) {
            synchronized (this) {
                this.mHasFocus = hasFocus;
                if (this.mHasFocus) {
                    notify();
                }
            }
        }

        public void onWindowResize(int w, int h) {
            synchronized (this) {
                this.mWidth = w;
                this.mHeight = h;
                CMyGLDCView.this.mSizeChanged = true;
            }
        }

        public void requestExitAndWait() {
            synchronized (this) {
                this.mDone = true;
                notify();
            }
            try {
                join();
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }

        public void setEvent(Runnable r) {
            synchronized (this) {
                this.mEvent = r;
            }
        }

        public void clearEvent() {
            synchronized (this) {
                this.mEvent = null;
            }
        }
    }

    public interface GLWrapper {
        GL wrap(GL gl);
    }

    public interface Renderer {
        void drawFrame(GL10 gl10);

        int[] getConfigSpec();

        void shutdown(GL10 gl10);

        void sizeChanged(GL10 gl10, int i, int i2);

        void surfaceCreated(GL10 gl10);
    }

    public void prepareDC() {
        super.prepareDC();
        createBackgroundSprite();
    }

    private void createBackgroundSprite() {
        GLSprite[] spriteArray = new GLSprite[1];
        GLSprite sprt = new GLSprite(-1);
        Bitmap bmp = this.m_bmpDblBuffer;
        sprt.width = (float) bmp.getWidth();
        sprt.height = (float) bmp.getHeight();
        sprt.setBmp(bmp);
        if (1 != 0) {
            float fScale = getScaleRes();
            Grid backgroundGrid = new Grid(2, 2, false);
            backgroundGrid.set(0, 0, BitmapDescriptorFactory.HUE_RED, BitmapDescriptorFactory.HUE_RED, BitmapDescriptorFactory.HUE_RED, BitmapDescriptorFactory.HUE_RED / fScale, 1.0f / fScale, null);
            backgroundGrid.set(1, 0, sprt.width, BitmapDescriptorFactory.HUE_RED, BitmapDescriptorFactory.HUE_RED, 1.0f / fScale, 1.0f / fScale, null);
            backgroundGrid.set(0, 1, BitmapDescriptorFactory.HUE_RED, sprt.height, BitmapDescriptorFactory.HUE_RED, BitmapDescriptorFactory.HUE_RED / fScale, BitmapDescriptorFactory.HUE_RED / fScale, null);
            backgroundGrid.set(1, 1, sprt.width, sprt.height, BitmapDescriptorFactory.HUE_RED, 1.0f / fScale, BitmapDescriptorFactory.HUE_RED / fScale, null);
            sprt.setGrid(backgroundGrid);
        }
        spriteArray[0] = sprt;
        this.m_renderer.setSprites(spriteArray);
        this.m_renderer.setVertMode(true, false);
    }

    public synchronized void update(CEventWnd pWnd) {
        if (pWnd != null) {
            try {
                synchronized (this.m_bmpDblBuffer) {
                    clear();
                    pWnd.DrawPrevProc();
                    pWnd.DrawProcess();
                }
            } catch (Exception e) {
                STD.printStackTrace(e);
            }
        }
        return;
    }

    public CMyGLDCView(Context context) {
        super(context);
        init(context);
    }

    public CMyGLDCView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    private void init(Context context) {
        SimpleGLRenderer render = new SimpleGLRenderer(context, this);
        setRenderer(render);
        this.m_renderer = render;
    }

    public void setGLWrapper(GLWrapper glWrapper) {
        this.mGLWrapper = glWrapper;
    }

    public void setRenderer(Renderer renderer) {
        this.mGLThread = new GLThread(renderer);
        this.mGLThread.start();
    }

    public void surfaceCreated(SurfaceHolder holder) {
        this.mGLThread.surfaceCreated();
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        this.mGLThread.surfaceDestroyed();
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
        this.mGLThread.onWindowResize(w, h);
    }

    public void onPause() {
        this.mGLThread.onPause();
    }

    public void onResume() {
        this.mGLThread.onResume();
    }

    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        this.mGLThread.onWindowFocusChanged(hasFocus);
    }

    public void setEvent(Runnable r) {
        this.mGLThread.setEvent(r);
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.mGLThread.requestExitAndWait();
    }
}
