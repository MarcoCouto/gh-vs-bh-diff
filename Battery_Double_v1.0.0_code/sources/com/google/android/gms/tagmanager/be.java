package com.google.android.gms.tagmanager;

import com.google.android.gms.internal.a;
import com.google.android.gms.internal.d;
import java.util.Map;

class be extends by {
    private static final String ID = a.LESS_THAN.toString();

    public be() {
        super(ID);
    }

    /* access modifiers changed from: protected */
    public boolean a(dh dhVar, dh dhVar2, Map<String, d.a> map) {
        return dhVar.compareTo(dhVar2) < 0;
    }
}
