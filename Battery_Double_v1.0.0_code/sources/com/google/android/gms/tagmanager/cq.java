package com.google.android.gms.tagmanager;

import android.content.Context;
import android.content.res.Resources.NotFoundException;
import com.google.android.gms.internal.c.f;
import com.google.android.gms.internal.pu.a;
import com.google.android.gms.internal.qv;
import com.google.android.gms.internal.qw;
import com.google.android.gms.tagmanager.cr.c;
import com.google.android.gms.tagmanager.cr.g;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.json.JSONException;

class cq implements f {
    private final String aqm;
    private bg<a> asC;
    private final ExecutorService asJ = Executors.newSingleThreadExecutor();
    private final Context mContext;

    cq(Context context, String str) {
        this.mContext = context;
        this.aqm = str;
    }

    private c a(ByteArrayOutputStream byteArrayOutputStream) {
        boolean z = false;
        try {
            return ba.cI(byteArrayOutputStream.toString("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            bh.S("Failed to convert binary resource to string for JSON parsing; the file format is not UTF-8 format.");
            return z;
        } catch (JSONException e2) {
            bh.W("Failed to extract the container from the resource file. Resource is a UTF-8 encoded string but doesn't contain a JSON container");
            return z;
        }
    }

    private void d(a aVar) throws IllegalArgumentException {
        if (aVar.gs == null && aVar.auC == null) {
            throw new IllegalArgumentException("Resource and SupplementedResource are NULL.");
        }
    }

    private c k(byte[] bArr) {
        try {
            c b = cr.b(f.a(bArr));
            if (b == null) {
                return b;
            }
            bh.V("The container was successfully loaded from the resource (using binary file)");
            return b;
        } catch (qv e) {
            bh.T("The resource file is corrupted. The container cannot be extracted from the binary file");
            return null;
        } catch (g e2) {
            bh.W("The resource file is invalid. The container from the binary file is invalid");
            return null;
        }
    }

    public void a(bg<a> bgVar) {
        this.asC = bgVar;
    }

    public void b(final a aVar) {
        this.asJ.execute(new Runnable() {
            public void run() {
                cq.this.c(aVar);
            }
        });
    }

    /* access modifiers changed from: 0000 */
    public boolean c(a aVar) {
        boolean z = false;
        File qj = qj();
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(qj);
            try {
                fileOutputStream.write(qw.f(aVar));
                z = true;
                try {
                } catch (IOException e) {
                    bh.W("error closing stream for writing resource to disk");
                }
            } catch (IOException e2) {
                bh.W("Error writing resource to disk. Removing resource from disk.");
                qj.delete();
                try {
                } catch (IOException e3) {
                    bh.W("error closing stream for writing resource to disk");
                }
            } finally {
                try {
                    fileOutputStream.close();
                } catch (IOException e4) {
                    bh.W("error closing stream for writing resource to disk");
                }
            }
        } catch (FileNotFoundException e5) {
            bh.T("Error opening resource file for writing");
        }
        return z;
    }

    public c fH(int i) {
        try {
            InputStream openRawResource = this.mContext.getResources().openRawResource(i);
            bh.V("Attempting to load a container from the resource ID " + i + " (" + this.mContext.getResources().getResourceName(i) + ")");
            try {
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                cr.b(openRawResource, byteArrayOutputStream);
                c a = a(byteArrayOutputStream);
                if (a == null) {
                    return k(byteArrayOutputStream.toByteArray());
                }
                bh.V("The container was successfully loaded from the resource (using JSON file format)");
                return a;
            } catch (IOException e) {
                bh.W("Error reading the default container with resource ID " + i + " (" + this.mContext.getResources().getResourceName(i) + ")");
                return null;
            }
        } catch (NotFoundException e2) {
            bh.W("Failed to load the container. No default container resource found with the resource ID " + i);
            return null;
        }
    }

    public void pt() {
        this.asJ.execute(new Runnable() {
            public void run() {
                cq.this.qi();
            }
        });
    }

    /* access modifiers changed from: 0000 */
    public void qi() {
        if (this.asC == null) {
            throw new IllegalStateException("Callback must be set before execute");
        }
        this.asC.ps();
        bh.V("Attempting to load resource from disk");
        if ((ce.qa().qb() == a.CONTAINER || ce.qa().qb() == a.CONTAINER_DEBUG) && this.aqm.equals(ce.qa().getContainerId())) {
            this.asC.a(bg.a.NOT_AVAILABLE);
            return;
        }
        try {
            FileInputStream fileInputStream = new FileInputStream(qj());
            try {
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                cr.b(fileInputStream, byteArrayOutputStream);
                a l = a.l(byteArrayOutputStream.toByteArray());
                d(l);
                this.asC.l(l);
                try {
                } catch (IOException e) {
                    bh.W("Error closing stream for reading resource from disk");
                }
            } catch (IOException e2) {
                this.asC.a(bg.a.IO_ERROR);
                bh.W("Failed to read the resource from disk");
                try {
                } catch (IOException e3) {
                    bh.W("Error closing stream for reading resource from disk");
                }
            } catch (IllegalArgumentException e4) {
                this.asC.a(bg.a.IO_ERROR);
                bh.W("Failed to read the resource from disk. The resource is inconsistent");
                try {
                } catch (IOException e5) {
                    bh.W("Error closing stream for reading resource from disk");
                }
            } finally {
                try {
                    fileInputStream.close();
                } catch (IOException e6) {
                    bh.W("Error closing stream for reading resource from disk");
                }
            }
            bh.V("The Disk resource was successfully read.");
        } catch (FileNotFoundException e7) {
            bh.S("Failed to find the resource in the disk");
            this.asC.a(bg.a.NOT_AVAILABLE);
        }
    }

    /* access modifiers changed from: 0000 */
    public File qj() {
        return new File(this.mContext.getDir("google_tagmanager", 0), "resource_" + this.aqm);
    }

    public synchronized void release() {
        this.asJ.shutdown();
    }
}
