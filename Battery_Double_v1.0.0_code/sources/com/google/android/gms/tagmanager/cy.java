package com.google.android.gms.tagmanager;

import android.content.Context;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Message;

class cy extends cx {
    private static cy atJ;
    /* access modifiers changed from: private */
    public static final Object yT = new Object();
    /* access modifiers changed from: private */
    public at atA;
    private volatile ar atB;
    /* access modifiers changed from: private */
    public int atC = 1800000;
    private boolean atD = true;
    private boolean atE = false;
    private boolean atF = true;
    private au atG = new au() {
        public void B(boolean z) {
            cy.this.a(z, cy.this.connected);
        }
    };
    private bo atH;
    /* access modifiers changed from: private */
    public boolean atI = false;
    private Context atz;
    /* access modifiers changed from: private */
    public boolean connected = true;
    /* access modifiers changed from: private */
    public Handler handler;

    private cy() {
    }

    private void ev() {
        this.atH = new bo(this);
        this.atH.z(this.atz);
    }

    private void ew() {
        this.handler = new Handler(this.atz.getMainLooper(), new Callback() {
            public boolean handleMessage(Message msg) {
                if (1 == msg.what && cy.yT.equals(msg.obj)) {
                    cy.this.dispatch();
                    if (cy.this.atC > 0 && !cy.this.atI) {
                        cy.this.handler.sendMessageDelayed(cy.this.handler.obtainMessage(1, cy.yT), (long) cy.this.atC);
                    }
                }
                return true;
            }
        });
        if (this.atC > 0) {
            this.handler.sendMessageDelayed(this.handler.obtainMessage(1, yT), (long) this.atC);
        }
    }

    public static cy qN() {
        if (atJ == null) {
            atJ = new cy();
        }
        return atJ;
    }

    /* access modifiers changed from: 0000 */
    public synchronized void C(boolean z) {
        a(this.atI, z);
    }

    /* access modifiers changed from: 0000 */
    public synchronized void a(Context context, ar arVar) {
        if (this.atz == null) {
            this.atz = context.getApplicationContext();
            if (this.atB == null) {
                this.atB = arVar;
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public synchronized void a(boolean z, boolean z2) {
        if (!(this.atI == z && this.connected == z2)) {
            if (z || !z2) {
                if (this.atC > 0) {
                    this.handler.removeMessages(1, yT);
                }
            }
            if (!z && z2 && this.atC > 0) {
                this.handler.sendMessageDelayed(this.handler.obtainMessage(1, yT), (long) this.atC);
            }
            bh.V("PowerSaveMode " + ((z || !z2) ? "initiated." : "terminated."));
            this.atI = z;
            this.connected = z2;
        }
    }

    public synchronized void dispatch() {
        if (!this.atE) {
            bh.V("Dispatch call queued. Dispatch will run once initialization is complete.");
            this.atD = true;
        } else {
            this.atB.b(new Runnable() {
                public void run() {
                    cy.this.atA.dispatch();
                }
            });
        }
    }

    /* access modifiers changed from: 0000 */
    public synchronized void ey() {
        if (!this.atI && this.connected && this.atC > 0) {
            this.handler.removeMessages(1, yT);
            this.handler.sendMessage(this.handler.obtainMessage(1, yT));
        }
    }

    /* access modifiers changed from: 0000 */
    public synchronized at qO() {
        if (this.atA == null) {
            if (this.atz == null) {
                throw new IllegalStateException("Cant get a store unless we have a context");
            }
            this.atA = new cb(this.atG, this.atz);
        }
        if (this.handler == null) {
            ew();
        }
        this.atE = true;
        if (this.atD) {
            dispatch();
            this.atD = false;
        }
        if (this.atH == null && this.atF) {
            ev();
        }
        return this.atA;
    }
}
