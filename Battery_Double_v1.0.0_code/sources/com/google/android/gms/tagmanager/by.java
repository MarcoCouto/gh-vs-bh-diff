package com.google.android.gms.tagmanager;

import com.google.android.gms.internal.d.a;
import java.util.Map;

abstract class by extends cd {
    public by(String str) {
        super(str);
    }

    /* access modifiers changed from: protected */
    public boolean a(a aVar, a aVar2, Map<String, a> map) {
        dh k = di.k(aVar);
        dh k2 = di.k(aVar2);
        if (k == di.qZ() || k2 == di.qZ()) {
            return false;
        }
        return a(k, k2, map);
    }

    /* access modifiers changed from: protected */
    public abstract boolean a(dh dhVar, dh dhVar2, Map<String, a> map);
}
