package com.google.android.gms.tagmanager;

import android.content.Context;
import android.os.Looper;
import com.google.android.gms.common.api.BaseImplementation.AbstractPendingResult;
import com.google.android.gms.common.api.Releasable;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.internal.c.j;
import com.google.android.gms.internal.ld;
import com.google.android.gms.internal.lf;

class o extends AbstractPendingResult<ContainerHolder> {
    private final Looper JF;
    private final d aqB;
    /* access modifiers changed from: private */
    public final cg aqC;
    private final int aqD;
    private f aqE;
    /* access modifiers changed from: private */
    public volatile n aqF;
    /* access modifiers changed from: private */
    public volatile boolean aqG;
    /* access modifiers changed from: private */
    public j aqH;
    private String aqI;
    private e aqJ;
    private a aqK;
    private final String aqm;
    /* access modifiers changed from: private */
    public long aqr;
    private final TagManager aqy;
    private final Context mContext;
    /* access modifiers changed from: private */
    public final ld wb;

    interface a {
        boolean b(Container container);
    }

    private class b implements bg<com.google.android.gms.internal.pu.a> {
        private b() {
        }

        /* renamed from: a */
        public void l(com.google.android.gms.internal.pu.a aVar) {
            j jVar;
            if (aVar.auC != null) {
                jVar = aVar.auC;
            } else {
                com.google.android.gms.internal.c.f fVar = aVar.gs;
                jVar = new j();
                jVar.gs = fVar;
                jVar.gr = null;
                jVar.gt = fVar.version;
            }
            o.this.a(jVar, aVar.auB, true);
        }

        public void a(com.google.android.gms.tagmanager.bg.a aVar) {
            if (!o.this.aqG) {
                o.this.w(0);
            }
        }

        public void ps() {
        }
    }

    private class c implements bg<j> {
        private c() {
        }

        public void a(com.google.android.gms.tagmanager.bg.a aVar) {
            if (o.this.aqF != null) {
                o.this.b(o.this.aqF);
            } else {
                o.this.b(o.this.c(Status.Kz));
            }
            o.this.w(3600000);
        }

        /* JADX WARNING: Code restructure failed: missing block: B:19:?, code lost:
            return;
         */
        /* renamed from: b */
        public void l(j jVar) {
            synchronized (o.this) {
                if (jVar.gs == null) {
                    if (o.this.aqH.gs == null) {
                        bh.T("Current resource is null; network resource is also null");
                        o.this.w(3600000);
                        return;
                    }
                    jVar.gs = o.this.aqH.gs;
                }
                o.this.a(jVar, o.this.wb.currentTimeMillis(), false);
                bh.V("setting refresh time to current time: " + o.this.aqr);
                if (!o.this.pr()) {
                    o.this.a(jVar);
                }
            }
        }

        public void ps() {
        }
    }

    private class d implements com.google.android.gms.tagmanager.n.a {
        private d() {
        }

        public void ct(String str) {
            o.this.ct(str);
        }

        public String pl() {
            return o.this.pl();
        }

        public void pn() {
            if (o.this.aqC.fe()) {
                o.this.w(0);
            }
        }
    }

    interface e extends Releasable {
        void a(bg<j> bgVar);

        void cw(String str);

        void e(long j, String str);
    }

    interface f extends Releasable {
        void a(bg<com.google.android.gms.internal.pu.a> bgVar);

        void b(com.google.android.gms.internal.pu.a aVar);

        com.google.android.gms.tagmanager.cr.c fH(int i);

        void pt();
    }

    o(Context context, TagManager tagManager, Looper looper, String str, int i, f fVar, e eVar, ld ldVar, cg cgVar) {
        super(looper == null ? Looper.getMainLooper() : looper);
        this.mContext = context;
        this.aqy = tagManager;
        if (looper == null) {
            looper = Looper.getMainLooper();
        }
        this.JF = looper;
        this.aqm = str;
        this.aqD = i;
        this.aqE = fVar;
        this.aqJ = eVar;
        this.aqB = new d();
        this.aqH = new j();
        this.wb = ldVar;
        this.aqC = cgVar;
        if (pr()) {
            ct(ce.qa().qc());
        }
    }

    public o(Context context, TagManager tagManager, Looper looper, String str, int i, r rVar) {
        this(context, tagManager, looper, str, i, new cq(context, str), new cp(context, str, rVar), lf.m2if(), new bf(30, 900000, 5000, "refreshing", lf.m2if()));
    }

    private void V(final boolean z) {
        this.aqE.a(new b());
        this.aqJ.a(new c());
        com.google.android.gms.tagmanager.cr.c fH = this.aqE.fH(this.aqD);
        if (fH != null) {
            this.aqF = new n(this.aqy, this.JF, new Container(this.mContext, this.aqy.getDataLayer(), this.aqm, 0, fH), this.aqB);
        }
        this.aqK = new a() {
            public boolean b(Container container) {
                return z ? container.getLastRefreshTime() + 43200000 >= o.this.wb.currentTimeMillis() : !container.isDefault();
            }
        };
        if (pr()) {
            this.aqJ.e(0, "");
        } else {
            this.aqE.pt();
        }
    }

    /* access modifiers changed from: private */
    public synchronized void a(j jVar) {
        if (this.aqE != null) {
            com.google.android.gms.internal.pu.a aVar = new com.google.android.gms.internal.pu.a();
            aVar.auB = this.aqr;
            aVar.gs = new com.google.android.gms.internal.c.f();
            aVar.auC = jVar;
            this.aqE.b(aVar);
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0008, code lost:
        if (r8.aqG != false) goto L_0x000a;
     */
    public synchronized void a(j jVar, long j, boolean z) {
        if (z) {
        }
        if (!isReady() || this.aqF == null) {
        }
        this.aqH = jVar;
        this.aqr = j;
        w(Math.max(0, Math.min(43200000, (this.aqr + 43200000) - this.wb.currentTimeMillis())));
        Container container = new Container(this.mContext, this.aqy.getDataLayer(), this.aqm, j, jVar);
        if (this.aqF == null) {
            this.aqF = new n(this.aqy, this.JF, container, this.aqB);
        } else {
            this.aqF.a(container);
        }
        if (!isReady() && this.aqK.b(container)) {
            b(this.aqF);
        }
    }

    /* access modifiers changed from: private */
    public boolean pr() {
        ce qa = ce.qa();
        return (qa.qb() == a.CONTAINER || qa.qb() == a.CONTAINER_DEBUG) && this.aqm.equals(qa.getContainerId());
    }

    /* access modifiers changed from: private */
    public synchronized void w(long j) {
        if (this.aqJ == null) {
            bh.W("Refresh requested, but no network load scheduler.");
        } else {
            this.aqJ.e(j, this.aqH.gt);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: aD */
    public ContainerHolder c(Status status) {
        if (this.aqF != null) {
            return this.aqF;
        }
        if (status == Status.Kz) {
            bh.T("timer expired: setting result to failure");
        }
        return new n(status);
    }

    /* access modifiers changed from: 0000 */
    public synchronized void ct(String str) {
        this.aqI = str;
        if (this.aqJ != null) {
            this.aqJ.cw(str);
        }
    }

    /* access modifiers changed from: 0000 */
    public synchronized String pl() {
        return this.aqI;
    }

    public void po() {
        com.google.android.gms.tagmanager.cr.c fH = this.aqE.fH(this.aqD);
        if (fH != null) {
            b(new n(this.aqy, this.JF, new Container(this.mContext, this.aqy.getDataLayer(), this.aqm, 0, fH), new com.google.android.gms.tagmanager.n.a() {
                public void ct(String str) {
                    o.this.ct(str);
                }

                public String pl() {
                    return o.this.pl();
                }

                public void pn() {
                    bh.W("Refresh ignored: container loaded as default only.");
                }
            }));
        } else {
            String str = "Default was requested, but no default container was found";
            bh.T(str);
            b(c(new Status(10, str, null)));
        }
        this.aqJ = null;
        this.aqE = null;
    }

    public void pp() {
        V(false);
    }

    public void pq() {
        V(true);
    }
}
