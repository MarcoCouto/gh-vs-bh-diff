package com.google.android.gms.tagmanager;

import com.google.android.gms.internal.d.a;
import java.util.Map;

abstract class dd extends cd {
    public dd(String str) {
        super(str);
    }

    /* access modifiers changed from: protected */
    public boolean a(a aVar, a aVar2, Map<String, a> map) {
        String j = di.j(aVar);
        String j2 = di.j(aVar2);
        if (j == di.ra() || j2 == di.ra()) {
            return false;
        }
        return a(j, j2, map);
    }

    /* access modifiers changed from: protected */
    public abstract boolean a(String str, String str2, Map<String, a> map);
}
