package com.google.android.gms.tagmanager;

import com.google.android.gms.internal.a;
import com.google.android.gms.internal.d;
import java.util.Map;

class q extends dd {
    private static final String ID = a.CONTAINS.toString();

    public q() {
        super(ID);
    }

    /* access modifiers changed from: protected */
    public boolean a(String str, String str2, Map<String, d.a> map) {
        return str.contains(str2);
    }
}
