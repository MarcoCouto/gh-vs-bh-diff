package com.google.android.gms.tagmanager;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteCursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build.VERSION;
import android.text.TextUtils;
import com.google.android.gms.internal.ld;
import com.google.android.gms.internal.lf;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import org.apache.http.impl.client.DefaultHttpClient;

class cb implements at {
    /* access modifiers changed from: private */
    public static final String BS = String.format("CREATE TABLE IF NOT EXISTS %s ( '%s' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, '%s' INTEGER NOT NULL, '%s' TEXT NOT NULL,'%s' INTEGER NOT NULL);", new Object[]{"gtm_hits", "hit_id", "hit_time", "hit_url", "hit_first_send_time"});
    /* access modifiers changed from: private */
    public final String BV;
    private long BX;
    private final int BY;
    private final b asf;
    private volatile ab asg;
    private final au ash;
    /* access modifiers changed from: private */
    public final Context mContext;
    /* access modifiers changed from: private */
    public ld wb;

    class a implements com.google.android.gms.tagmanager.db.a {
        a() {
        }

        public void a(ap apVar) {
            cb.this.y(apVar.fb());
        }

        public void b(ap apVar) {
            cb.this.y(apVar.fb());
            bh.V("Permanent failure dispatching hitId: " + apVar.fb());
        }

        public void c(ap apVar) {
            long pK = apVar.pK();
            if (pK == 0) {
                cb.this.c(apVar.fb(), cb.this.wb.currentTimeMillis());
            } else if (pK + 14400000 < cb.this.wb.currentTimeMillis()) {
                cb.this.y(apVar.fb());
                bh.V("Giving up on failed hitId: " + apVar.fb());
            }
        }
    }

    class b extends SQLiteOpenHelper {
        private boolean BZ;
        private long Ca = 0;

        b(Context context, String str) {
            super(context, str, null, 1);
        }

        /* JADX INFO: finally extract failed */
        private void a(SQLiteDatabase sQLiteDatabase) {
            Cursor rawQuery = sQLiteDatabase.rawQuery("SELECT * FROM gtm_hits WHERE 0", null);
            HashSet hashSet = new HashSet();
            try {
                String[] columnNames = rawQuery.getColumnNames();
                for (String add : columnNames) {
                    hashSet.add(add);
                }
                rawQuery.close();
                if (!hashSet.remove("hit_id") || !hashSet.remove("hit_url") || !hashSet.remove("hit_time") || !hashSet.remove("hit_first_send_time")) {
                    throw new SQLiteException("Database column missing");
                } else if (!hashSet.isEmpty()) {
                    throw new SQLiteException("Database has extra columns");
                }
            } catch (Throwable th) {
                rawQuery.close();
                throw th;
            }
        }

        /* JADX WARNING: Removed duplicated region for block: B:16:0x0048  */
        private boolean a(String str, SQLiteDatabase sQLiteDatabase) {
            Cursor cursor;
            Cursor cursor2 = null;
            try {
                SQLiteDatabase sQLiteDatabase2 = sQLiteDatabase;
                Cursor query = sQLiteDatabase2.query("SQLITE_MASTER", new String[]{"name"}, "name=?", new String[]{str}, null, null, null);
                try {
                    boolean moveToFirst = query.moveToFirst();
                    if (query == null) {
                        return moveToFirst;
                    }
                    query.close();
                    return moveToFirst;
                } catch (SQLiteException e) {
                    cursor = query;
                } catch (Throwable th) {
                    th = th;
                    cursor2 = query;
                    if (cursor2 != null) {
                    }
                    throw th;
                }
            } catch (SQLiteException e2) {
                cursor = null;
            } catch (Throwable th2) {
                th = th2;
                if (cursor2 != null) {
                    cursor2.close();
                }
                throw th;
            }
            try {
                bh.W("Error querying for table " + str);
                if (cursor != null) {
                    cursor.close();
                }
                return false;
            } catch (Throwable th3) {
                cursor2 = cursor;
                th = th3;
                if (cursor2 != null) {
                }
                throw th;
            }
        }

        public SQLiteDatabase getWritableDatabase() {
            if (!this.BZ || this.Ca + 3600000 <= cb.this.wb.currentTimeMillis()) {
                SQLiteDatabase sQLiteDatabase = null;
                this.BZ = true;
                this.Ca = cb.this.wb.currentTimeMillis();
                try {
                    sQLiteDatabase = super.getWritableDatabase();
                } catch (SQLiteException e) {
                    cb.this.mContext.getDatabasePath(cb.this.BV).delete();
                }
                if (sQLiteDatabase == null) {
                    sQLiteDatabase = super.getWritableDatabase();
                }
                this.BZ = false;
                return sQLiteDatabase;
            }
            throw new SQLiteException("Database creation failed");
        }

        public void onCreate(SQLiteDatabase db) {
            ak.ag(db.getPath());
        }

        public void onOpen(SQLiteDatabase db) {
            if (VERSION.SDK_INT < 15) {
                Cursor rawQuery = db.rawQuery("PRAGMA journal_mode=memory", null);
                try {
                    rawQuery.moveToFirst();
                } finally {
                    rawQuery.close();
                }
            }
            if (!a("gtm_hits", db)) {
                db.execSQL(cb.BS);
            } else {
                a(db);
            }
        }

        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        }
    }

    cb(au auVar, Context context) {
        this(auVar, context, "gtm_urls.db", 2000);
    }

    cb(au auVar, Context context, String str, int i) {
        this.mContext = context.getApplicationContext();
        this.BV = str;
        this.ash = auVar;
        this.wb = lf.m2if();
        this.asf = new b(this.mContext, this.BV);
        this.asg = new db(new DefaultHttpClient(), this.mContext, new a());
        this.BX = 0;
        this.BY = i;
    }

    private SQLiteDatabase al(String str) {
        try {
            return this.asf.getWritableDatabase();
        } catch (SQLiteException e) {
            bh.W(str);
            return null;
        }
    }

    /* access modifiers changed from: private */
    public void c(long j, long j2) {
        SQLiteDatabase al = al("Error opening database for getNumStoredHits.");
        if (al != null) {
            ContentValues contentValues = new ContentValues();
            contentValues.put("hit_first_send_time", Long.valueOf(j2));
            try {
                al.update("gtm_hits", contentValues, "hit_id=?", new String[]{String.valueOf(j)});
            } catch (SQLiteException e) {
                bh.W("Error setting HIT_FIRST_DISPATCH_TIME for hitId: " + j);
                y(j);
            }
        }
    }

    private void fh() {
        int fj = (fj() - this.BY) + 1;
        if (fj > 0) {
            List G = G(fj);
            bh.V("Store full, deleting " + G.size() + " hits to make room.");
            b((String[]) G.toArray(new String[0]));
        }
    }

    private void g(long j, String str) {
        SQLiteDatabase al = al("Error opening database for putHit");
        if (al != null) {
            ContentValues contentValues = new ContentValues();
            contentValues.put("hit_time", Long.valueOf(j));
            contentValues.put("hit_url", str);
            contentValues.put("hit_first_send_time", Integer.valueOf(0));
            try {
                al.insert("gtm_hits", null, contentValues);
                this.ash.B(false);
            } catch (SQLiteException e) {
                bh.W("Error storing hit");
            }
        }
    }

    /* access modifiers changed from: private */
    public void y(long j) {
        b(new String[]{String.valueOf(j)});
    }

    /* access modifiers changed from: 0000 */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0082  */
    public List<String> G(int i) {
        Cursor cursor;
        ArrayList arrayList = new ArrayList();
        if (i <= 0) {
            bh.W("Invalid maxHits specified. Skipping");
            return arrayList;
        }
        SQLiteDatabase al = al("Error opening database for peekHitIds.");
        if (al == null) {
            return arrayList;
        }
        try {
            cursor = al.query("gtm_hits", new String[]{"hit_id"}, null, null, null, null, String.format("%s ASC", new Object[]{"hit_id"}), Integer.toString(i));
            try {
                if (cursor.moveToFirst()) {
                    do {
                        arrayList.add(String.valueOf(cursor.getLong(0)));
                    } while (cursor.moveToNext());
                }
                if (cursor != null) {
                    cursor.close();
                }
            } catch (SQLiteException e) {
                e = e;
                try {
                    bh.W("Error in peekHits fetching hitIds: " + e.getMessage());
                    if (cursor != null) {
                        cursor.close();
                    }
                    return arrayList;
                } catch (Throwable th) {
                    th = th;
                    if (cursor != null) {
                        cursor.close();
                    }
                    throw th;
                }
            }
        } catch (SQLiteException e2) {
            e = e2;
            cursor = null;
        } catch (Throwable th2) {
            th = th2;
            cursor = null;
            if (cursor != null) {
            }
            throw th;
        }
        return arrayList;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:35:0x00ea, code lost:
        r4.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00f2, code lost:
        r12.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x016b, code lost:
        r2 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x016c, code lost:
        r12 = r13;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x0171, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x0172, code lost:
        r3 = r2;
        r4 = r13;
        r2 = r11;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:80:?, code lost:
        return r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:?, code lost:
        return r2;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00ea  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00f2  */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x016b A[ExcHandler: all (th java.lang.Throwable), Splitter:B:6:0x0040] */
    /* JADX WARNING: Removed duplicated region for block: B:80:? A[RETURN, SYNTHETIC] */
    public List<ap> H(int i) {
        Cursor cursor;
        SQLiteException sQLiteException;
        ArrayList arrayList;
        Cursor query;
        ArrayList<ap> arrayList2;
        ArrayList arrayList3 = new ArrayList();
        SQLiteDatabase al = al("Error opening database for peekHits");
        if (al == null) {
            return arrayList3;
        }
        Cursor cursor2 = null;
        try {
            query = al.query("gtm_hits", new String[]{"hit_id", "hit_time", "hit_first_send_time"}, null, null, null, null, String.format("%s ASC", new Object[]{"hit_id"}), Integer.toString(i));
            try {
                arrayList2 = new ArrayList<>();
                if (query.moveToFirst()) {
                    do {
                        arrayList2.add(new ap(query.getLong(0), query.getLong(1), query.getLong(2)));
                    } while (query.moveToNext());
                }
                if (query != null) {
                    query.close();
                }
                try {
                    Cursor query2 = al.query("gtm_hits", new String[]{"hit_id", "hit_url"}, null, null, null, null, String.format("%s ASC", new Object[]{"hit_id"}), Integer.toString(i));
                    try {
                        if (query2.moveToFirst()) {
                            int i2 = 0;
                            while (true) {
                                if (((SQLiteCursor) query2).getWindow().getNumRows() > 0) {
                                    ((ap) arrayList2.get(i2)).ak(query2.getString(1));
                                } else {
                                    bh.W(String.format("HitString for hitId %d too large.  Hit will be deleted.", new Object[]{Long.valueOf(((ap) arrayList2.get(i2)).fb())}));
                                }
                                int i3 = i2 + 1;
                                if (!query2.moveToNext()) {
                                    break;
                                }
                                i2 = i3;
                            }
                        }
                        if (query2 != null) {
                            query2.close();
                        }
                        return arrayList2;
                    } catch (SQLiteException e) {
                        e = e;
                        query = query2;
                    } catch (Throwable th) {
                        th = th;
                        query = query2;
                        if (query != null) {
                            query.close();
                        }
                        throw th;
                    }
                } catch (SQLiteException e2) {
                    e = e2;
                }
            } catch (SQLiteException e3) {
                sQLiteException = e3;
                cursor = query;
                arrayList = arrayList2;
                try {
                    bh.W("Error in peekHits fetching hitIds: " + sQLiteException.getMessage());
                    if (cursor == null) {
                    }
                } catch (Throwable th2) {
                    th = th2;
                    cursor2 = cursor;
                    if (cursor2 != null) {
                    }
                    throw th;
                }
            } catch (Throwable th3) {
            }
        } catch (SQLiteException e4) {
            sQLiteException = e4;
            cursor = null;
            arrayList = arrayList3;
        } catch (Throwable th4) {
            th = th4;
            if (cursor2 != null) {
            }
            throw th;
        }
        try {
            bh.W("Error in peekHits fetching hit url: " + e.getMessage());
            ArrayList arrayList4 = new ArrayList();
            boolean z = false;
            for (ap apVar : arrayList2) {
                if (TextUtils.isEmpty(apVar.pL())) {
                    if (z) {
                        break;
                    }
                    z = true;
                }
                arrayList4.add(apVar);
            }
            if (query != null) {
                query.close();
            }
            return arrayList4;
        } catch (Throwable th5) {
            th = th5;
        }
    }

    /* access modifiers changed from: 0000 */
    public void b(String[] strArr) {
        boolean z = true;
        if (strArr != null && strArr.length != 0) {
            SQLiteDatabase al = al("Error opening database for deleteHits.");
            if (al != null) {
                try {
                    al.delete("gtm_hits", String.format("HIT_ID in (%s)", new Object[]{TextUtils.join(",", Collections.nCopies(strArr.length, "?"))}), strArr);
                    au auVar = this.ash;
                    if (fj() != 0) {
                        z = false;
                    }
                    auVar.B(z);
                } catch (SQLiteException e) {
                    bh.W("Error deleting hits");
                }
            }
        }
    }

    public void dispatch() {
        bh.V("GTM Dispatch running...");
        if (this.asg.ea()) {
            List H = H(40);
            if (H.isEmpty()) {
                bh.V("...nothing to dispatch");
                this.ash.B(true);
                return;
            }
            this.asg.k(H);
            if (pY() > 0) {
                cy.qN().dispatch();
            }
        }
    }

    public void f(long j, String str) {
        fi();
        fh();
        g(j, str);
    }

    /* access modifiers changed from: 0000 */
    public int fi() {
        boolean z = true;
        long currentTimeMillis = this.wb.currentTimeMillis();
        if (currentTimeMillis <= this.BX + 86400000) {
            return 0;
        }
        this.BX = currentTimeMillis;
        SQLiteDatabase al = al("Error opening database for deleteStaleHits.");
        if (al == null) {
            return 0;
        }
        int delete = al.delete("gtm_hits", "HIT_TIME < ?", new String[]{Long.toString(this.wb.currentTimeMillis() - 2592000000L)});
        au auVar = this.ash;
        if (fj() != 0) {
            z = false;
        }
        auVar.B(z);
        return delete;
    }

    /* access modifiers changed from: 0000 */
    public int fj() {
        Cursor cursor = null;
        int i = 0;
        SQLiteDatabase al = al("Error opening database for getNumStoredHits.");
        if (al != null) {
            try {
                Cursor rawQuery = al.rawQuery("SELECT COUNT(*) from gtm_hits", null);
                if (rawQuery.moveToFirst()) {
                    i = (int) rawQuery.getLong(0);
                }
                if (rawQuery != null) {
                    rawQuery.close();
                }
            } catch (SQLiteException e) {
                bh.W("Error getting numStoredHits");
                if (cursor != null) {
                    cursor.close();
                }
            } catch (Throwable th) {
                if (cursor != null) {
                    cursor.close();
                }
                throw th;
            }
        }
        return i;
    }

    /* access modifiers changed from: 0000 */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0040  */
    public int pY() {
        Cursor cursor;
        int i;
        Cursor cursor2 = null;
        SQLiteDatabase al = al("Error opening database for getNumStoredHits.");
        if (al == null) {
            return 0;
        }
        try {
            Cursor query = al.query("gtm_hits", new String[]{"hit_id", "hit_first_send_time"}, "hit_first_send_time=0", null, null, null, null);
            try {
                i = query.getCount();
                if (query != null) {
                    query.close();
                }
            } catch (SQLiteException e) {
                cursor = query;
            } catch (Throwable th) {
                th = th;
                cursor2 = query;
                if (cursor2 != null) {
                }
                throw th;
            }
        } catch (SQLiteException e2) {
            cursor = null;
        } catch (Throwable th2) {
            th = th2;
            if (cursor2 != null) {
                cursor2.close();
            }
            throw th;
        }
        return i;
        try {
            bh.W("Error getting num untried hits");
            if (cursor != null) {
                cursor.close();
                i = 0;
            } else {
                i = 0;
            }
            return i;
        } catch (Throwable th3) {
            cursor2 = cursor;
            th = th3;
            if (cursor2 != null) {
            }
            throw th;
        }
    }
}
