package com.google.android.gms.tagmanager;

import android.text.TextUtils;

class ap {
    private final long Bv;
    private final long Bw;
    private String By;
    private final long arw;

    ap(long j, long j2, long j3) {
        this.Bv = j;
        this.Bw = j2;
        this.arw = j3;
    }

    /* access modifiers changed from: 0000 */
    public void ak(String str) {
        if (str != null && !TextUtils.isEmpty(str.trim())) {
            this.By = str;
        }
    }

    /* access modifiers changed from: 0000 */
    public long fb() {
        return this.Bv;
    }

    /* access modifiers changed from: 0000 */
    public long pK() {
        return this.arw;
    }

    /* access modifiers changed from: 0000 */
    public String pL() {
        return this.By;
    }
}
