package com.google.android.gms.tagmanager;

import com.google.android.gms.internal.d.a;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

class dm {
    private static bz<a> a(bz<a> bzVar) {
        try {
            return new bz(di.u(dg(di.j((a) bzVar.getObject()))), bzVar.pX());
        } catch (UnsupportedEncodingException e) {
            bh.b("Escape URI: unsupported encoding", e);
            return bzVar;
        }
    }

    private static bz<a> a(bz<a> bzVar, int i) {
        if (!q((a) bzVar.getObject())) {
            bh.T("Escaping can only be applied to strings.");
            return bzVar;
        }
        switch (i) {
            case 12:
                return a(bzVar);
            default:
                bh.T("Unsupported Value Escaping: " + i);
                return bzVar;
        }
    }

    static bz<a> a(bz<a> bzVar, int... iArr) {
        for (int a : iArr) {
            bzVar = a(bzVar, a);
        }
        return bzVar;
    }

    static String dg(String str) throws UnsupportedEncodingException {
        return URLEncoder.encode(str, "UTF-8").replaceAll("\\+", "%20");
    }

    private static boolean q(a aVar) {
        return di.o(aVar) instanceof String;
    }
}
