package com.google.android.gms.tagmanager;

import android.content.Context;
import android.support.v4.view.accessibility.AccessibilityEventCompat;
import com.google.android.gms.internal.c.i;
import com.google.android.gms.tagmanager.cr.e;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

class ct {
    private static final bz<com.google.android.gms.internal.d.a> atb = new bz<>(di.rb(), true);
    private final DataLayer aqn;
    private final com.google.android.gms.tagmanager.cr.c atc;
    private final ag atd;
    private final Map<String, aj> ate;
    private final Map<String, aj> atf;
    private final Map<String, aj> atg;
    private final k<com.google.android.gms.tagmanager.cr.a, bz<com.google.android.gms.internal.d.a>> ath;
    private final k<String, b> ati;
    private final Set<e> atj;
    private final Map<String, c> atk;
    private volatile String atl;
    private int atm;

    interface a {
        void a(e eVar, Set<com.google.android.gms.tagmanager.cr.a> set, Set<com.google.android.gms.tagmanager.cr.a> set2, cn cnVar);
    }

    private static class b {
        private com.google.android.gms.internal.d.a asN;
        private bz<com.google.android.gms.internal.d.a> ats;

        public b(bz<com.google.android.gms.internal.d.a> bzVar, com.google.android.gms.internal.d.a aVar) {
            this.ats = bzVar;
            this.asN = aVar;
        }

        public int getSize() {
            return (this.asN == null ? 0 : this.asN.rY()) + ((com.google.android.gms.internal.d.a) this.ats.getObject()).rY();
        }

        public bz<com.google.android.gms.internal.d.a> qG() {
            return this.ats;
        }

        public com.google.android.gms.internal.d.a qm() {
            return this.asN;
        }
    }

    private static class c {
        private final Set<e> atj = new HashSet();
        private final Map<e, List<com.google.android.gms.tagmanager.cr.a>> att = new HashMap();
        private final Map<e, List<com.google.android.gms.tagmanager.cr.a>> atu = new HashMap();
        private final Map<e, List<String>> atv = new HashMap();
        private final Map<e, List<String>> atw = new HashMap();
        private com.google.android.gms.tagmanager.cr.a atx;

        public void a(e eVar, com.google.android.gms.tagmanager.cr.a aVar) {
            List list = (List) this.att.get(eVar);
            if (list == null) {
                list = new ArrayList();
                this.att.put(eVar, list);
            }
            list.add(aVar);
        }

        public void a(e eVar, String str) {
            List list = (List) this.atv.get(eVar);
            if (list == null) {
                list = new ArrayList();
                this.atv.put(eVar, list);
            }
            list.add(str);
        }

        public void b(e eVar) {
            this.atj.add(eVar);
        }

        public void b(e eVar, com.google.android.gms.tagmanager.cr.a aVar) {
            List list = (List) this.atu.get(eVar);
            if (list == null) {
                list = new ArrayList();
                this.atu.put(eVar, list);
            }
            list.add(aVar);
        }

        public void b(e eVar, String str) {
            List list = (List) this.atw.get(eVar);
            if (list == null) {
                list = new ArrayList();
                this.atw.put(eVar, list);
            }
            list.add(str);
        }

        public void i(com.google.android.gms.tagmanager.cr.a aVar) {
            this.atx = aVar;
        }

        public Set<e> qH() {
            return this.atj;
        }

        public Map<e, List<com.google.android.gms.tagmanager.cr.a>> qI() {
            return this.att;
        }

        public Map<e, List<String>> qJ() {
            return this.atv;
        }

        public Map<e, List<String>> qK() {
            return this.atw;
        }

        public Map<e, List<com.google.android.gms.tagmanager.cr.a>> qL() {
            return this.atu;
        }

        public com.google.android.gms.tagmanager.cr.a qM() {
            return this.atx;
        }
    }

    public ct(Context context, com.google.android.gms.tagmanager.cr.c cVar, DataLayer dataLayer, com.google.android.gms.tagmanager.s.a aVar, com.google.android.gms.tagmanager.s.a aVar2, ag agVar) {
        if (cVar == null) {
            throw new NullPointerException("resource cannot be null");
        }
        this.atc = cVar;
        this.atj = new HashSet(cVar.qp());
        this.aqn = dataLayer;
        this.atd = agVar;
        this.ath = new l().a(AccessibilityEventCompat.TYPE_TOUCH_INTERACTION_START, new com.google.android.gms.tagmanager.l.a<com.google.android.gms.tagmanager.cr.a, bz<com.google.android.gms.internal.d.a>>() {
            /* renamed from: a */
            public int sizeOf(com.google.android.gms.tagmanager.cr.a aVar, bz<com.google.android.gms.internal.d.a> bzVar) {
                return ((com.google.android.gms.internal.d.a) bzVar.getObject()).rY();
            }
        });
        this.ati = new l().a(AccessibilityEventCompat.TYPE_TOUCH_INTERACTION_START, new com.google.android.gms.tagmanager.l.a<String, b>() {
            /* renamed from: a */
            public int sizeOf(String str, b bVar) {
                return str.length() + bVar.getSize();
            }
        });
        this.ate = new HashMap();
        b(new i(context));
        b(new s(aVar2));
        b(new w(dataLayer));
        b(new dj(context, dataLayer));
        this.atf = new HashMap();
        c(new q());
        c(new ad());
        c(new ae());
        c(new al());
        c(new am());
        c(new bd());
        c(new be());
        c(new ci());
        c(new dc());
        this.atg = new HashMap();
        a(new b(context));
        a(new c(context));
        a(new e(context));
        a(new f(context));
        a(new g(context));
        a(new h(context));
        a(new m());
        a(new p(this.atc.getVersion()));
        a(new s(aVar));
        a(new u(dataLayer));
        a(new z(context));
        a(new aa());
        a(new ac());
        a(new ah(this));
        a(new an());
        a(new ao());
        a(new ax(context));
        a(new az());
        a(new bc());
        a(new bj());
        a(new bl(context));
        a(new ca());
        a(new cc());
        a(new cf());
        a(new ch());
        a(new cj(context));
        a(new cu());
        a(new cv());
        a(new de());
        a(new dk());
        this.atk = new HashMap();
        for (e eVar : this.atj) {
            if (agVar.pH()) {
                a(eVar.qx(), eVar.qy(), "add macro");
                a(eVar.qC(), eVar.qz(), "remove macro");
                a(eVar.qv(), eVar.qA(), "add tag");
                a(eVar.qw(), eVar.qB(), "remove tag");
            }
            for (int i = 0; i < eVar.qx().size(); i++) {
                com.google.android.gms.tagmanager.cr.a aVar3 = (com.google.android.gms.tagmanager.cr.a) eVar.qx().get(i);
                String str = "Unknown";
                if (agVar.pH() && i < eVar.qy().size()) {
                    str = (String) eVar.qy().get(i);
                }
                c e = e(this.atk, h(aVar3));
                e.b(eVar);
                e.a(eVar, aVar3);
                e.a(eVar, str);
            }
            for (int i2 = 0; i2 < eVar.qC().size(); i2++) {
                com.google.android.gms.tagmanager.cr.a aVar4 = (com.google.android.gms.tagmanager.cr.a) eVar.qC().get(i2);
                String str2 = "Unknown";
                if (agVar.pH() && i2 < eVar.qz().size()) {
                    str2 = (String) eVar.qz().get(i2);
                }
                c e2 = e(this.atk, h(aVar4));
                e2.b(eVar);
                e2.b(eVar, aVar4);
                e2.b(eVar, str2);
            }
        }
        for (Entry entry : this.atc.qq().entrySet()) {
            for (com.google.android.gms.tagmanager.cr.a aVar5 : (List) entry.getValue()) {
                if (!di.n((com.google.android.gms.internal.d.a) aVar5.ql().get(com.google.android.gms.internal.b.NOT_DEFAULT_MACRO.toString())).booleanValue()) {
                    e(this.atk, (String) entry.getKey()).i(aVar5);
                }
            }
        }
    }

    private bz<com.google.android.gms.internal.d.a> a(com.google.android.gms.internal.d.a aVar, Set<String> set, dl dlVar) {
        if (!aVar.gF) {
            return new bz<>(aVar, true);
        }
        switch (aVar.type) {
            case 2:
                com.google.android.gms.internal.d.a g = cr.g(aVar);
                g.gw = new com.google.android.gms.internal.d.a[aVar.gw.length];
                for (int i = 0; i < aVar.gw.length; i++) {
                    bz<com.google.android.gms.internal.d.a> a2 = a(aVar.gw[i], set, dlVar.fK(i));
                    if (a2 == atb) {
                        return atb;
                    }
                    g.gw[i] = (com.google.android.gms.internal.d.a) a2.getObject();
                }
                return new bz<>(g, false);
            case 3:
                com.google.android.gms.internal.d.a g2 = cr.g(aVar);
                if (aVar.gx.length != aVar.gy.length) {
                    bh.T("Invalid serving value: " + aVar.toString());
                    return atb;
                }
                g2.gx = new com.google.android.gms.internal.d.a[aVar.gx.length];
                g2.gy = new com.google.android.gms.internal.d.a[aVar.gx.length];
                for (int i2 = 0; i2 < aVar.gx.length; i2++) {
                    bz<com.google.android.gms.internal.d.a> a3 = a(aVar.gx[i2], set, dlVar.fL(i2));
                    bz<com.google.android.gms.internal.d.a> a4 = a(aVar.gy[i2], set, dlVar.fM(i2));
                    if (a3 == atb || a4 == atb) {
                        return atb;
                    }
                    g2.gx[i2] = (com.google.android.gms.internal.d.a) a3.getObject();
                    g2.gy[i2] = (com.google.android.gms.internal.d.a) a4.getObject();
                }
                return new bz<>(g2, false);
            case 4:
                if (set.contains(aVar.gz)) {
                    bh.T("Macro cycle detected.  Current macro reference: " + aVar.gz + "." + "  Previous macro references: " + set.toString() + ".");
                    return atb;
                }
                set.add(aVar.gz);
                bz<com.google.android.gms.internal.d.a> a5 = dm.a(a(aVar.gz, set, dlVar.pW()), aVar.gE);
                set.remove(aVar.gz);
                return a5;
            case 7:
                com.google.android.gms.internal.d.a g3 = cr.g(aVar);
                g3.gD = new com.google.android.gms.internal.d.a[aVar.gD.length];
                for (int i3 = 0; i3 < aVar.gD.length; i3++) {
                    bz<com.google.android.gms.internal.d.a> a6 = a(aVar.gD[i3], set, dlVar.fN(i3));
                    if (a6 == atb) {
                        return atb;
                    }
                    g3.gD[i3] = (com.google.android.gms.internal.d.a) a6.getObject();
                }
                return new bz<>(g3, false);
            default:
                bh.T("Unknown type: " + aVar.type);
                return atb;
        }
    }

    private bz<com.google.android.gms.internal.d.a> a(String str, Set<String> set, bk bkVar) {
        com.google.android.gms.tagmanager.cr.a aVar;
        this.atm++;
        b bVar = (b) this.ati.get(str);
        if (bVar == null || this.atd.pH()) {
            c cVar = (c) this.atk.get(str);
            if (cVar == null) {
                bh.T(qF() + "Invalid macro: " + str);
                this.atm--;
                return atb;
            }
            bz a2 = a(str, cVar.qH(), cVar.qI(), cVar.qJ(), cVar.qL(), cVar.qK(), set, bkVar.py());
            if (((Set) a2.getObject()).isEmpty()) {
                aVar = cVar.qM();
            } else {
                if (((Set) a2.getObject()).size() > 1) {
                    bh.W(qF() + "Multiple macros active for macroName " + str);
                }
                aVar = (com.google.android.gms.tagmanager.cr.a) ((Set) a2.getObject()).iterator().next();
            }
            if (aVar == null) {
                this.atm--;
                return atb;
            }
            bz<com.google.android.gms.internal.d.a> a3 = a(this.atg, aVar, set, bkVar.pN());
            bz bzVar = a3 == atb ? atb : new bz(a3.getObject(), a2.pX() && a3.pX());
            com.google.android.gms.internal.d.a qm = aVar.qm();
            if (bzVar.pX()) {
                this.ati.e(str, new b(bzVar, qm));
            }
            a(qm, set);
            this.atm--;
            return bzVar;
        }
        a(bVar.qm(), set);
        this.atm--;
        return bVar.qG();
    }

    private bz<com.google.android.gms.internal.d.a> a(Map<String, aj> map, com.google.android.gms.tagmanager.cr.a aVar, Set<String> set, ck ckVar) {
        boolean z;
        boolean z2 = true;
        com.google.android.gms.internal.d.a aVar2 = (com.google.android.gms.internal.d.a) aVar.ql().get(com.google.android.gms.internal.b.FUNCTION.toString());
        if (aVar2 == null) {
            bh.T("No function id in properties");
            return atb;
        }
        String str = aVar2.gA;
        aj ajVar = (aj) map.get(str);
        if (ajVar == null) {
            bh.T(str + " has no backing implementation.");
            return atb;
        }
        bz<com.google.android.gms.internal.d.a> bzVar = (bz) this.ath.get(aVar);
        if (bzVar != null && !this.atd.pH()) {
            return bzVar;
        }
        HashMap hashMap = new HashMap();
        boolean z3 = true;
        for (Entry entry : aVar.ql().entrySet()) {
            bz<com.google.android.gms.internal.d.a> a2 = a((com.google.android.gms.internal.d.a) entry.getValue(), set, ckVar.cJ((String) entry.getKey()).e((com.google.android.gms.internal.d.a) entry.getValue()));
            if (a2 == atb) {
                return atb;
            }
            if (a2.pX()) {
                aVar.a((String) entry.getKey(), (com.google.android.gms.internal.d.a) a2.getObject());
                z = z3;
            } else {
                z = false;
            }
            hashMap.put(entry.getKey(), a2.getObject());
            z3 = z;
        }
        if (!ajVar.a(hashMap.keySet())) {
            bh.T("Incorrect keys for function " + str + " required " + ajVar.pJ() + " had " + hashMap.keySet());
            return atb;
        }
        if (!z3 || !ajVar.pe()) {
            z2 = false;
        }
        bz<com.google.android.gms.internal.d.a> bzVar2 = new bz<>(ajVar.B(hashMap), z2);
        if (z2) {
            this.ath.e(aVar, bzVar2);
        }
        ckVar.d((com.google.android.gms.internal.d.a) bzVar2.getObject());
        return bzVar2;
    }

    private bz<Set<com.google.android.gms.tagmanager.cr.a>> a(Set<e> set, Set<String> set2, a aVar, cs csVar) {
        HashSet hashSet = new HashSet();
        HashSet hashSet2 = new HashSet();
        boolean z = true;
        for (e eVar : set) {
            cn pV = csVar.pV();
            bz a2 = a(eVar, set2, pV);
            if (((Boolean) a2.getObject()).booleanValue()) {
                aVar.a(eVar, hashSet, hashSet2, pV);
            }
            z = z && a2.pX();
        }
        hashSet.removeAll(hashSet2);
        csVar.b(hashSet);
        return new bz<>(hashSet, z);
    }

    private void a(com.google.android.gms.internal.d.a aVar, Set<String> set) {
        if (aVar != null) {
            bz<com.google.android.gms.internal.d.a> a2 = a(aVar, set, (dl) new bx());
            if (a2 != atb) {
                Object o = di.o((com.google.android.gms.internal.d.a) a2.getObject());
                if (o instanceof Map) {
                    this.aqn.push((Map) o);
                } else if (o instanceof List) {
                    for (Object next : (List) o) {
                        if (next instanceof Map) {
                            this.aqn.push((Map) next);
                        } else {
                            bh.W("pushAfterEvaluate: value not a Map");
                        }
                    }
                } else {
                    bh.W("pushAfterEvaluate: value not a Map or List");
                }
            }
        }
    }

    private static void a(List<com.google.android.gms.tagmanager.cr.a> list, List<String> list2, String str) {
        if (list.size() != list2.size()) {
            bh.U("Invalid resource: imbalance of rule names of functions for " + str + " operation. Using default rule name instead");
        }
    }

    private static void a(Map<String, aj> map, aj ajVar) {
        if (map.containsKey(ajVar.pI())) {
            throw new IllegalArgumentException("Duplicate function type name: " + ajVar.pI());
        }
        map.put(ajVar.pI(), ajVar);
    }

    private static c e(Map<String, c> map, String str) {
        c cVar = (c) map.get(str);
        if (cVar != null) {
            return cVar;
        }
        c cVar2 = new c();
        map.put(str, cVar2);
        return cVar2;
    }

    private static String h(com.google.android.gms.tagmanager.cr.a aVar) {
        return di.j((com.google.android.gms.internal.d.a) aVar.ql().get(com.google.android.gms.internal.b.INSTANCE_NAME.toString()));
    }

    private String qF() {
        if (this.atm <= 1) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        sb.append(Integer.toString(this.atm));
        for (int i = 2; i < this.atm; i++) {
            sb.append(' ');
        }
        sb.append(": ");
        return sb.toString();
    }

    /* access modifiers changed from: 0000 */
    public bz<Boolean> a(com.google.android.gms.tagmanager.cr.a aVar, Set<String> set, ck ckVar) {
        bz a2 = a(this.atf, aVar, set, ckVar);
        Boolean n = di.n((com.google.android.gms.internal.d.a) a2.getObject());
        ckVar.d(di.u(n));
        return new bz<>(n, a2.pX());
    }

    /* access modifiers changed from: 0000 */
    public bz<Boolean> a(e eVar, Set<String> set, cn cnVar) {
        boolean z;
        boolean z2 = true;
        for (com.google.android.gms.tagmanager.cr.a a2 : eVar.qu()) {
            bz a3 = a(a2, set, cnVar.pP());
            if (((Boolean) a3.getObject()).booleanValue()) {
                cnVar.f(di.u(Boolean.valueOf(false)));
                return new bz<>(Boolean.valueOf(false), a3.pX());
            }
            z2 = z && a3.pX();
        }
        for (com.google.android.gms.tagmanager.cr.a a4 : eVar.qt()) {
            bz a5 = a(a4, set, cnVar.pQ());
            if (!((Boolean) a5.getObject()).booleanValue()) {
                cnVar.f(di.u(Boolean.valueOf(false)));
                return new bz<>(Boolean.valueOf(false), a5.pX());
            }
            z = z && a5.pX();
        }
        cnVar.f(di.u(Boolean.valueOf(true)));
        return new bz<>(Boolean.valueOf(true), z);
    }

    /* access modifiers changed from: 0000 */
    public bz<Set<com.google.android.gms.tagmanager.cr.a>> a(String str, Set<e> set, Map<e, List<com.google.android.gms.tagmanager.cr.a>> map, Map<e, List<String>> map2, Map<e, List<com.google.android.gms.tagmanager.cr.a>> map3, Map<e, List<String>> map4, Set<String> set2, cs csVar) {
        final Map<e, List<com.google.android.gms.tagmanager.cr.a>> map5 = map;
        final Map<e, List<String>> map6 = map2;
        final Map<e, List<com.google.android.gms.tagmanager.cr.a>> map7 = map3;
        final Map<e, List<String>> map8 = map4;
        return a(set, set2, (a) new a() {
            public void a(e eVar, Set<com.google.android.gms.tagmanager.cr.a> set, Set<com.google.android.gms.tagmanager.cr.a> set2, cn cnVar) {
                List list = (List) map5.get(eVar);
                List list2 = (List) map6.get(eVar);
                if (list != null) {
                    set.addAll(list);
                    cnVar.pR().c(list, list2);
                }
                List list3 = (List) map7.get(eVar);
                List list4 = (List) map8.get(eVar);
                if (list3 != null) {
                    set2.addAll(list3);
                    cnVar.pS().c(list3, list4);
                }
            }
        }, csVar);
    }

    /* access modifiers changed from: 0000 */
    public bz<Set<com.google.android.gms.tagmanager.cr.a>> a(Set<e> set, cs csVar) {
        return a(set, (Set<String>) new HashSet<String>(), (a) new a() {
            public void a(e eVar, Set<com.google.android.gms.tagmanager.cr.a> set, Set<com.google.android.gms.tagmanager.cr.a> set2, cn cnVar) {
                set.addAll(eVar.qv());
                set2.addAll(eVar.qw());
                cnVar.pT().c(eVar.qv(), eVar.qA());
                cnVar.pU().c(eVar.qw(), eVar.qB());
            }
        }, csVar);
    }

    /* access modifiers changed from: 0000 */
    public void a(aj ajVar) {
        a(this.atg, ajVar);
    }

    /* access modifiers changed from: 0000 */
    public void b(aj ajVar) {
        a(this.ate, ajVar);
    }

    /* access modifiers changed from: 0000 */
    public void c(aj ajVar) {
        a(this.atf, ajVar);
    }

    public bz<com.google.android.gms.internal.d.a> cT(String str) {
        this.atm = 0;
        af cC = this.atd.cC(str);
        bz<com.google.android.gms.internal.d.a> a2 = a(str, (Set<String>) new HashSet<String>(), cC.pE());
        cC.pG();
        return a2;
    }

    /* access modifiers changed from: 0000 */
    public synchronized void cU(String str) {
        this.atl = str;
    }

    public synchronized void cr(String str) {
        cU(str);
        af cD = this.atd.cD(str);
        t pF = cD.pF();
        for (com.google.android.gms.tagmanager.cr.a a2 : (Set) a(this.atj, pF.py()).getObject()) {
            a(this.ate, a2, (Set<String>) new HashSet<String>(), pF.px());
        }
        cD.pG();
        cU(null);
    }

    public synchronized void l(List<i> list) {
        for (i iVar : list) {
            if (iVar.name == null || !iVar.name.startsWith("gaExperiment:")) {
                bh.V("Ignored supplemental: " + iVar);
            } else {
                ai.a(this.aqn, iVar);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public synchronized String qE() {
        return this.atl;
    }
}
