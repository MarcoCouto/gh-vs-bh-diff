package com.google.android.gms.tagmanager;

import android.os.Build;
import com.google.android.gms.fitness.FitnessActivities;
import com.google.android.gms.internal.a;
import com.google.android.gms.internal.d;
import java.util.Map;

class aa extends aj {
    private static final String ID = a.DEVICE_NAME.toString();

    public aa() {
        super(ID, new String[0]);
    }

    public d.a B(Map<String, d.a> map) {
        String str = Build.MANUFACTURER;
        String str2 = Build.MODEL;
        if (!str2.startsWith(str) && !str.equals(FitnessActivities.UNKNOWN)) {
            str2 = str + " " + str2;
        }
        return di.u(str2);
    }

    public boolean pe() {
        return true;
    }
}
