package com.google.android.gms.tagmanager;

import android.content.Context;
import android.net.Uri;
import android.net.Uri.Builder;
import com.google.android.gms.internal.b;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

class i extends dg {
    private static final String ID = com.google.android.gms.internal.a.ARBITRARY_PIXEL.toString();
    private static final String URL = b.URL.toString();
    private static final String aqf = b.ADDITIONAL_PARAMS.toString();
    private static final String aqg = b.UNREPEATABLE.toString();
    static final String aqh = ("gtm_" + ID + "_unrepeatable");
    private static final Set<String> aqi = new HashSet();
    private final a aqj;
    private final Context mContext;

    public interface a {
        aq pf();
    }

    public i(final Context context) {
        this(context, new a() {
            public aq pf() {
                return y.X(context);
            }
        });
    }

    i(Context context, a aVar) {
        super(ID, URL);
        this.aqj = aVar;
        this.mContext = context;
    }

    private synchronized boolean cl(String str) {
        boolean z = true;
        synchronized (this) {
            if (!cn(str)) {
                if (cm(str)) {
                    aqi.add(str);
                } else {
                    z = false;
                }
            }
        }
        return z;
    }

    public void D(Map<String, com.google.android.gms.internal.d.a> map) {
        String str = map.get(aqg) != null ? di.j((com.google.android.gms.internal.d.a) map.get(aqg)) : null;
        if (str == null || !cl(str)) {
            Builder buildUpon = Uri.parse(di.j((com.google.android.gms.internal.d.a) map.get(URL))).buildUpon();
            com.google.android.gms.internal.d.a aVar = (com.google.android.gms.internal.d.a) map.get(aqf);
            if (aVar != null) {
                Object o = di.o(aVar);
                if (!(o instanceof List)) {
                    bh.T("ArbitraryPixel: additional params not a list: not sending partial hit: " + buildUpon.build().toString());
                    return;
                }
                for (Object next : (List) o) {
                    if (!(next instanceof Map)) {
                        bh.T("ArbitraryPixel: additional params contains non-map: not sending partial hit: " + buildUpon.build().toString());
                        return;
                    }
                    for (Entry entry : ((Map) next).entrySet()) {
                        buildUpon.appendQueryParameter(entry.getKey().toString(), entry.getValue().toString());
                    }
                }
            }
            String uri = buildUpon.build().toString();
            this.aqj.pf().cB(uri);
            bh.V("ArbitraryPixel: url = " + uri);
            if (str != null) {
                synchronized (i.class) {
                    aqi.add(str);
                    cz.a(this.mContext, aqh, str, "true");
                }
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public boolean cm(String str) {
        return this.mContext.getSharedPreferences(aqh, 0).contains(str);
    }

    /* access modifiers changed from: 0000 */
    public boolean cn(String str) {
        return aqi.contains(str);
    }
}
