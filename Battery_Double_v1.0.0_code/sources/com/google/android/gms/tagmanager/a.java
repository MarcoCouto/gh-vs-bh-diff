package com.google.android.gms.tagmanager;

import android.content.Context;
import android.os.Process;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.ads.identifier.AdvertisingIdClient.Info;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.internal.ld;
import com.google.android.gms.internal.lf;
import java.io.IOException;

class a {
    private static a aqa;
    private static Object xO = new Object();
    private volatile long apW;
    private volatile long apX;
    private volatile long apY;
    private C0139a apZ;
    private volatile boolean mClosed;
    /* access modifiers changed from: private */
    public final Context mContext;
    private final ld wb;
    private final Thread wu;
    private volatile Info xQ;

    /* renamed from: com.google.android.gms.tagmanager.a$a reason: collision with other inner class name */
    public interface C0139a {
        Info pd();
    }

    private a(Context context) {
        this(context, null, lf.m2if());
    }

    a(Context context, C0139a aVar, ld ldVar) {
        this.apW = 900000;
        this.apX = 30000;
        this.mClosed = false;
        this.apZ = new C0139a() {
            public Info pd() {
                boolean z = false;
                try {
                    return AdvertisingIdClient.getAdvertisingIdInfo(a.this.mContext);
                } catch (IllegalStateException e) {
                    bh.W("IllegalStateException getting Advertising Id Info");
                    return z;
                } catch (GooglePlayServicesRepairableException e2) {
                    bh.W("GooglePlayServicesRepairableException getting Advertising Id Info");
                    return z;
                } catch (IOException e3) {
                    bh.W("IOException getting Ad Id Info");
                    return z;
                } catch (GooglePlayServicesNotAvailableException e4) {
                    bh.W("GooglePlayServicesNotAvailableException getting Advertising Id Info");
                    return z;
                } catch (Exception e5) {
                    bh.W("Unknown exception. Could not get the Advertising Id Info.");
                    return z;
                }
            }
        };
        this.wb = ldVar;
        if (context != null) {
            this.mContext = context.getApplicationContext();
        } else {
            this.mContext = context;
        }
        if (aVar != null) {
            this.apZ = aVar;
        }
        this.wu = new Thread(new Runnable() {
            public void run() {
                a.this.pb();
            }
        });
    }

    static a W(Context context) {
        if (aqa == null) {
            synchronized (xO) {
                if (aqa == null) {
                    aqa = new a(context);
                    aqa.start();
                }
            }
        }
        return aqa;
    }

    /* access modifiers changed from: private */
    public void pb() {
        Process.setThreadPriority(10);
        while (!this.mClosed) {
            try {
                this.xQ = this.apZ.pd();
                Thread.sleep(this.apW);
            } catch (InterruptedException e) {
                bh.U("sleep interrupted in AdvertiserDataPoller thread; continuing");
            }
        }
    }

    private void pc() {
        if (this.wb.currentTimeMillis() - this.apY >= this.apX) {
            interrupt();
            this.apY = this.wb.currentTimeMillis();
        }
    }

    /* access modifiers changed from: 0000 */
    public void interrupt() {
        this.wu.interrupt();
    }

    public boolean isLimitAdTrackingEnabled() {
        pc();
        if (this.xQ == null) {
            return true;
        }
        return this.xQ.isLimitAdTrackingEnabled();
    }

    public String pa() {
        pc();
        if (this.xQ == null) {
            return null;
        }
        return this.xQ.getId();
    }

    /* access modifiers changed from: 0000 */
    public void start() {
        this.wu.start();
    }
}
