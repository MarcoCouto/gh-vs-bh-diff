package com.google.android.gms.tagmanager;

import android.content.Context;
import com.google.android.gms.internal.c.j;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

class cp implements e {
    private String aqI;
    /* access modifiers changed from: private */
    public final String aqm;
    private bg<j> asC;
    private r asD;
    private final ScheduledExecutorService asF;
    private final a asG;
    private ScheduledFuture<?> asH;
    private boolean mClosed;
    /* access modifiers changed from: private */
    public final Context mContext;

    interface a {
        co a(r rVar);
    }

    interface b {
        ScheduledExecutorService qh();
    }

    public cp(Context context, String str, r rVar) {
        this(context, str, rVar, null, null);
    }

    cp(Context context, String str, r rVar, b bVar, a aVar) {
        this.asD = rVar;
        this.mContext = context;
        this.aqm = str;
        if (bVar == null) {
            bVar = new b() {
                public ScheduledExecutorService qh() {
                    return Executors.newSingleThreadScheduledExecutor();
                }
            };
        }
        this.asF = bVar.qh();
        if (aVar == null) {
            this.asG = new a() {
                public co a(r rVar) {
                    return new co(cp.this.mContext, cp.this.aqm, rVar);
                }
            };
        } else {
            this.asG = aVar;
        }
    }

    private co cM(String str) {
        co a2 = this.asG.a(this.asD);
        a2.a(this.asC);
        a2.cw(this.aqI);
        a2.cL(str);
        return a2;
    }

    private synchronized void qg() {
        if (this.mClosed) {
            throw new IllegalStateException("called method after closed");
        }
    }

    public synchronized void a(bg<j> bgVar) {
        qg();
        this.asC = bgVar;
    }

    public synchronized void cw(String str) {
        qg();
        this.aqI = str;
    }

    public synchronized void e(long j, String str) {
        bh.V("loadAfterDelay: containerId=" + this.aqm + " delay=" + j);
        qg();
        if (this.asC == null) {
            throw new IllegalStateException("callback must be set before loadAfterDelay() is called.");
        }
        if (this.asH != null) {
            this.asH.cancel(false);
        }
        this.asH = this.asF.schedule(cM(str), j, TimeUnit.MILLISECONDS);
    }

    public synchronized void release() {
        qg();
        if (this.asH != null) {
            this.asH.cancel(false);
        }
        this.asF.shutdown();
        this.mClosed = true;
    }
}
