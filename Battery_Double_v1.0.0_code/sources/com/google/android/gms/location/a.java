package com.google.android.gms.location;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.a.C0004a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.internal.nn;
import java.util.ArrayList;
import java.util.List;

public class a implements Creator<GeofencingRequest> {
    static void a(GeofencingRequest geofencingRequest, Parcel parcel, int i) {
        int H = b.H(parcel);
        b.c(parcel, 1, geofencingRequest.ng(), false);
        b.c(parcel, 1000, geofencingRequest.getVersionCode());
        b.c(parcel, 2, geofencingRequest.getInitialTrigger());
        b.H(parcel, H);
    }

    /* renamed from: cI */
    public GeofencingRequest createFromParcel(Parcel parcel) {
        int i = 0;
        int G = com.google.android.gms.common.internal.safeparcel.a.G(parcel);
        ArrayList arrayList = null;
        int i2 = 0;
        while (parcel.dataPosition() < G) {
            int F = com.google.android.gms.common.internal.safeparcel.a.F(parcel);
            switch (com.google.android.gms.common.internal.safeparcel.a.aH(F)) {
                case 1:
                    arrayList = com.google.android.gms.common.internal.safeparcel.a.c(parcel, F, nn.CREATOR);
                    break;
                case 2:
                    i = com.google.android.gms.common.internal.safeparcel.a.g(parcel, F);
                    break;
                case 1000:
                    i2 = com.google.android.gms.common.internal.safeparcel.a.g(parcel, F);
                    break;
                default:
                    com.google.android.gms.common.internal.safeparcel.a.b(parcel, F);
                    break;
            }
        }
        if (parcel.dataPosition() == G) {
            return new GeofencingRequest(i2, (List<nn>) arrayList, i);
        }
        throw new C0004a("Overread allowed size end=" + G, parcel);
    }

    /* renamed from: ex */
    public GeofencingRequest[] newArray(int i) {
        return new GeofencingRequest[i];
    }
}
