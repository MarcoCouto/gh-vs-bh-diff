package com.google.android.gms.games.snapshot;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.a.C0004a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.drive.Contents;

public class SnapshotContentsEntityCreator implements Creator<SnapshotContentsEntity> {
    static void a(SnapshotContentsEntity snapshotContentsEntity, Parcel parcel, int i) {
        int H = b.H(parcel);
        b.a(parcel, 1, (Parcelable) snapshotContentsEntity.ir(), i, false);
        b.c(parcel, 1000, snapshotContentsEntity.getVersionCode());
        b.H(parcel, H);
    }

    /* renamed from: cD */
    public SnapshotContentsEntity createFromParcel(Parcel parcel) {
        int G = a.G(parcel);
        int i = 0;
        Contents contents = null;
        while (parcel.dataPosition() < G) {
            int F = a.F(parcel);
            switch (a.aH(F)) {
                case 1:
                    contents = (Contents) a.a(parcel, F, Contents.CREATOR);
                    break;
                case 1000:
                    i = a.g(parcel, F);
                    break;
                default:
                    a.b(parcel, F);
                    break;
            }
        }
        if (parcel.dataPosition() == G) {
            return new SnapshotContentsEntity(i, contents);
        }
        throw new C0004a("Overread allowed size end=" + G, parcel);
    }

    /* renamed from: eq */
    public SnapshotContentsEntity[] newArray(int i) {
        return new SnapshotContentsEntity[i];
    }
}
