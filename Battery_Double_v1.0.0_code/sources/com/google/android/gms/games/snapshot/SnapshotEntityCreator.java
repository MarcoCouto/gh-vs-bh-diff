package com.google.android.gms.games.snapshot;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.a.C0004a;
import com.google.android.gms.common.internal.safeparcel.b;

public class SnapshotEntityCreator implements Creator<SnapshotEntity> {
    public static final int CONTENT_DESCRIPTION = 0;

    static void a(SnapshotEntity snapshotEntity, Parcel parcel, int i) {
        int H = b.H(parcel);
        b.a(parcel, 1, (Parcelable) snapshotEntity.getMetadata(), i, false);
        b.c(parcel, 1000, snapshotEntity.getVersionCode());
        b.a(parcel, 3, (Parcelable) snapshotEntity.getSnapshotContents(), i, false);
        b.H(parcel, H);
    }

    public SnapshotEntity createFromParcel(Parcel parcel) {
        SnapshotContentsEntity snapshotContentsEntity;
        SnapshotMetadata snapshotMetadata;
        int i;
        SnapshotContentsEntity snapshotContentsEntity2 = null;
        int G = a.G(parcel);
        int i2 = 0;
        SnapshotMetadata snapshotMetadata2 = null;
        while (parcel.dataPosition() < G) {
            int F = a.F(parcel);
            switch (a.aH(F)) {
                case 1:
                    i = i2;
                    SnapshotMetadata snapshotMetadata3 = (SnapshotMetadataEntity) a.a(parcel, F, (Creator<T>) SnapshotMetadataEntity.CREATOR);
                    snapshotContentsEntity = snapshotContentsEntity2;
                    snapshotMetadata = snapshotMetadata3;
                    break;
                case 3:
                    snapshotContentsEntity = (SnapshotContentsEntity) a.a(parcel, F, (Creator<T>) SnapshotContentsEntity.CREATOR);
                    snapshotMetadata = snapshotMetadata2;
                    i = i2;
                    break;
                case 1000:
                    SnapshotContentsEntity snapshotContentsEntity3 = snapshotContentsEntity2;
                    snapshotMetadata = snapshotMetadata2;
                    i = a.g(parcel, F);
                    snapshotContentsEntity = snapshotContentsEntity3;
                    break;
                default:
                    a.b(parcel, F);
                    snapshotContentsEntity = snapshotContentsEntity2;
                    snapshotMetadata = snapshotMetadata2;
                    i = i2;
                    break;
            }
            i2 = i;
            snapshotMetadata2 = snapshotMetadata;
            snapshotContentsEntity2 = snapshotContentsEntity;
        }
        if (parcel.dataPosition() == G) {
            return new SnapshotEntity(i2, snapshotMetadata2, snapshotContentsEntity2);
        }
        throw new C0004a("Overread allowed size end=" + G, parcel);
    }

    public SnapshotEntity[] newArray(int size) {
        return new SnapshotEntity[size];
    }
}
