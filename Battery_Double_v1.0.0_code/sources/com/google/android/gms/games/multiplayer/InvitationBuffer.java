package com.google.android.gms.games.multiplayer;

import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.data.g;

public final class InvitationBuffer extends g<Invitation> {
    public InvitationBuffer(DataHolder dataHolder) {
        super(dataHolder);
    }

    /* access modifiers changed from: protected */
    public String ha() {
        return "external_invitation_id";
    }

    /* access modifiers changed from: protected */
    /* renamed from: k */
    public Invitation f(int i, int i2) {
        return new InvitationRef(this.JG, i, i2);
    }
}
