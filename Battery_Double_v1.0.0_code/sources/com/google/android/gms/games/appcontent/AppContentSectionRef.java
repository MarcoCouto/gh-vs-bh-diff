package com.google.android.gms.games.appcontent;

import android.net.Uri;
import android.os.Bundle;
import android.os.Parcel;
import com.google.android.gms.common.data.DataHolder;
import java.util.ArrayList;

public final class AppContentSectionRef extends MultiDataBufferRef implements AppContentSection {
    private final int Ya;

    AppContentSectionRef(ArrayList<DataHolder> dataHolderCollection, int dataRow, int numChildren) {
        super(dataHolderCollection, 0, dataRow);
        this.Ya = numChildren;
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        return AppContentSectionEntity.a(this, obj);
    }

    public String getTitle() {
        return getString("section_title");
    }

    public String getType() {
        return getString("section_type");
    }

    public int hashCode() {
        return AppContentSectionEntity.a(this);
    }

    public String kI() {
        return getString("section_content_description");
    }

    public Bundle kP() {
        return AppContentUtils.d(this.JG, this.XX, "section_data", this.KZ);
    }

    public String kQ() {
        return getString("section_subtitle");
    }

    public Uri kY() {
        return aR("section_background_image_uri");
    }

    /* renamed from: la */
    public AppContentSection freeze() {
        return new AppContentSectionEntity(this);
    }

    /* renamed from: lb */
    public ArrayList<AppContentAction> getActions() {
        return AppContentUtils.a(this.JG, this.XX, "section_actions", this.KZ);
    }

    /* renamed from: lc */
    public ArrayList<AppContentCard> kZ() {
        ArrayList<AppContentCard> arrayList = new ArrayList<>(this.Ya);
        for (int i = 0; i < this.Ya; i++) {
            arrayList.add(new AppContentCardRef(this.XX, this.KZ + i));
        }
        return arrayList;
    }

    public String toString() {
        return AppContentSectionEntity.b(this);
    }

    public void writeToParcel(Parcel dest, int flags) {
        ((AppContentSectionEntity) freeze()).writeToParcel(dest, flags);
    }
}
