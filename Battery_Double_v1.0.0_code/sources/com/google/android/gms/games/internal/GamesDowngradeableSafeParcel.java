package com.google.android.gms.games.internal;

import com.google.android.gms.internal.ji;
import com.google.android.gms.internal.lg;

public abstract class GamesDowngradeableSafeParcel extends ji {
    /* access modifiers changed from: protected */
    public static boolean c(Integer num) {
        if (num == null) {
            return false;
        }
        return lg.aV(num.intValue());
    }
}
