package com.google.android.gms.games.internal.api;

import android.content.Intent;
import com.google.android.gms.common.api.BaseImplementation.b;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.Games.BaseGamesApiMethodImpl;
import com.google.android.gms.games.achievement.AchievementBuffer;
import com.google.android.gms.games.achievement.Achievements;
import com.google.android.gms.games.achievement.Achievements.LoadAchievementsResult;
import com.google.android.gms.games.achievement.Achievements.UpdateAchievementResult;
import com.google.android.gms.games.internal.GamesClientImpl;

public final class AchievementsImpl implements Achievements {

    /* renamed from: com.google.android.gms.games.internal.api.AchievementsImpl$10 reason: invalid class name */
    class AnonymousClass10 extends LoadImpl {
        final /* synthetic */ boolean ZW;
        final /* synthetic */ String ZY;
        final /* synthetic */ String ZZ;

        public void a(GamesClientImpl gamesClientImpl) {
            gamesClientImpl.b((b<LoadAchievementsResult>) this, this.ZY, this.ZZ, this.ZW);
        }
    }

    private static abstract class LoadImpl extends BaseGamesApiMethodImpl<LoadAchievementsResult> {
        private LoadImpl(GoogleApiClient googleApiClient) {
            super(googleApiClient);
        }

        /* renamed from: I */
        public LoadAchievementsResult c(final Status status) {
            return new LoadAchievementsResult() {
                public AchievementBuffer getAchievements() {
                    return new AchievementBuffer(DataHolder.av(14));
                }

                public Status getStatus() {
                    return status;
                }

                public void release() {
                }
            };
        }
    }

    private static abstract class UpdateImpl extends BaseGamesApiMethodImpl<UpdateAchievementResult> {
        /* access modifiers changed from: private */
        public final String CE;

        public UpdateImpl(String id, GoogleApiClient googleApiClient) {
            super(googleApiClient);
            this.CE = id;
        }

        /* renamed from: J */
        public UpdateAchievementResult c(final Status status) {
            return new UpdateAchievementResult() {
                public String getAchievementId() {
                    return UpdateImpl.this.CE;
                }

                public Status getStatus() {
                    return status;
                }
            };
        }
    }

    public Intent getAchievementsIntent(GoogleApiClient apiClient) {
        return Games.d(apiClient).lm();
    }

    public void increment(GoogleApiClient apiClient, String id, int numSteps) {
        final String str = id;
        final int i = numSteps;
        apiClient.b(new UpdateImpl(id, apiClient) {
            public void a(GamesClientImpl gamesClientImpl) {
                gamesClientImpl.a(null, str, i);
            }
        });
    }

    public PendingResult<UpdateAchievementResult> incrementImmediate(GoogleApiClient apiClient, String id, int numSteps) {
        final String str = id;
        final int i = numSteps;
        return apiClient.b(new UpdateImpl(id, apiClient) {
            public void a(GamesClientImpl gamesClientImpl) {
                gamesClientImpl.a((b<UpdateAchievementResult>) this, str, i);
            }
        });
    }

    public PendingResult<LoadAchievementsResult> load(GoogleApiClient apiClient, final boolean forceReload) {
        return apiClient.a(new LoadImpl(apiClient) {
            public void a(GamesClientImpl gamesClientImpl) {
                gamesClientImpl.c((b<LoadAchievementsResult>) this, forceReload);
            }
        });
    }

    public void reveal(GoogleApiClient apiClient, final String id) {
        apiClient.b(new UpdateImpl(apiClient, id) {
            public void a(GamesClientImpl gamesClientImpl) {
                gamesClientImpl.a(null, id);
            }
        });
    }

    public PendingResult<UpdateAchievementResult> revealImmediate(GoogleApiClient apiClient, final String id) {
        return apiClient.b(new UpdateImpl(apiClient, id) {
            public void a(GamesClientImpl gamesClientImpl) {
                gamesClientImpl.a((b<UpdateAchievementResult>) this, id);
            }
        });
    }

    public void setSteps(GoogleApiClient apiClient, String id, int numSteps) {
        final String str = id;
        final int i = numSteps;
        apiClient.b(new UpdateImpl(id, apiClient) {
            public void a(GamesClientImpl gamesClientImpl) {
                gamesClientImpl.b(null, str, i);
            }
        });
    }

    public PendingResult<UpdateAchievementResult> setStepsImmediate(GoogleApiClient apiClient, String id, int numSteps) {
        final String str = id;
        final int i = numSteps;
        return apiClient.b(new UpdateImpl(id, apiClient) {
            public void a(GamesClientImpl gamesClientImpl) {
                gamesClientImpl.b((b<UpdateAchievementResult>) this, str, i);
            }
        });
    }

    public void unlock(GoogleApiClient apiClient, final String id) {
        apiClient.b(new UpdateImpl(apiClient, id) {
            public void a(GamesClientImpl gamesClientImpl) {
                gamesClientImpl.b(null, id);
            }
        });
    }

    public PendingResult<UpdateAchievementResult> unlockImmediate(GoogleApiClient apiClient, final String id) {
        return apiClient.b(new UpdateImpl(apiClient, id) {
            public void a(GamesClientImpl gamesClientImpl) {
                gamesClientImpl.b((b<UpdateAchievementResult>) this, id);
            }
        });
    }
}
