package com.google.android.gms.games.internal.api;

import com.google.android.gms.common.api.BaseImplementation.b;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.games.Games.BaseGamesApiMethodImpl;
import com.google.android.gms.games.appcontent.AppContents;
import com.google.android.gms.games.appcontent.AppContents.LoadAppContentResult;
import com.google.android.gms.games.internal.GamesClientImpl;

public final class AppContentsImpl implements AppContents {

    /* renamed from: com.google.android.gms.games.internal.api.AppContentsImpl$1 reason: invalid class name */
    class AnonymousClass1 extends LoadsImpl {
        final /* synthetic */ boolean ZW;
        final /* synthetic */ int aaf;
        final /* synthetic */ String aag;
        final /* synthetic */ String[] aah;

        /* access modifiers changed from: protected */
        public void a(GamesClientImpl gamesClientImpl) {
            gamesClientImpl.a((b<LoadAppContentResult>) this, this.aaf, this.aag, this.aah, this.ZW);
        }
    }

    private static abstract class LoadsImpl extends BaseGamesApiMethodImpl<LoadAppContentResult> {
        /* renamed from: N */
        public LoadAppContentResult c(final Status status) {
            return new LoadAppContentResult() {
                public Status getStatus() {
                    return status;
                }

                public void release() {
                }
            };
        }
    }
}
