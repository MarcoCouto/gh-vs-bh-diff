package com.google.android.gms.games.internal.api;

import android.os.Bundle;
import com.google.android.gms.common.api.BaseImplementation.b;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.Games.BaseGamesApiMethodImpl;
import com.google.android.gms.games.Notifications;
import com.google.android.gms.games.Notifications.ContactSettingLoadResult;
import com.google.android.gms.games.Notifications.GameMuteStatusChangeResult;
import com.google.android.gms.games.Notifications.GameMuteStatusLoadResult;
import com.google.android.gms.games.Notifications.InboxCountResult;
import com.google.android.gms.games.internal.GamesClientImpl;

public final class NotificationsImpl implements Notifications {

    /* renamed from: com.google.android.gms.games.internal.api.NotificationsImpl$1 reason: invalid class name */
    class AnonymousClass1 extends BaseGamesApiMethodImpl<GameMuteStatusChangeResult> {
        final /* synthetic */ String aaR;

        /* renamed from: Y */
        public GameMuteStatusChangeResult c(final Status status) {
            return new GameMuteStatusChangeResult() {
                public Status getStatus() {
                    return status;
                }
            };
        }

        /* access modifiers changed from: protected */
        public void a(GamesClientImpl gamesClientImpl) {
            gamesClientImpl.e((b<GameMuteStatusChangeResult>) this, this.aaR, true);
        }
    }

    /* renamed from: com.google.android.gms.games.internal.api.NotificationsImpl$2 reason: invalid class name */
    class AnonymousClass2 extends BaseGamesApiMethodImpl<GameMuteStatusChangeResult> {
        final /* synthetic */ String aaR;

        /* renamed from: Y */
        public GameMuteStatusChangeResult c(final Status status) {
            return new GameMuteStatusChangeResult() {
                public Status getStatus() {
                    return status;
                }
            };
        }

        /* access modifiers changed from: protected */
        public void a(GamesClientImpl gamesClientImpl) {
            gamesClientImpl.e((b<GameMuteStatusChangeResult>) this, this.aaR, false);
        }
    }

    /* renamed from: com.google.android.gms.games.internal.api.NotificationsImpl$3 reason: invalid class name */
    class AnonymousClass3 extends BaseGamesApiMethodImpl<GameMuteStatusLoadResult> {
        final /* synthetic */ String aaR;

        /* renamed from: Z */
        public GameMuteStatusLoadResult c(final Status status) {
            return new GameMuteStatusLoadResult() {
                public Status getStatus() {
                    return status;
                }
            };
        }

        /* access modifiers changed from: protected */
        public void a(GamesClientImpl gamesClientImpl) {
            gamesClientImpl.p((b<GameMuteStatusLoadResult>) this, this.aaR);
        }
    }

    /* renamed from: com.google.android.gms.games.internal.api.NotificationsImpl$4 reason: invalid class name */
    class AnonymousClass4 extends ContactSettingLoadImpl {
        final /* synthetic */ boolean ZW;

        /* access modifiers changed from: protected */
        public void a(GamesClientImpl gamesClientImpl) {
            gamesClientImpl.h((b<ContactSettingLoadResult>) this, this.ZW);
        }
    }

    /* renamed from: com.google.android.gms.games.internal.api.NotificationsImpl$5 reason: invalid class name */
    class AnonymousClass5 extends ContactSettingUpdateImpl {
        final /* synthetic */ boolean aaV;
        final /* synthetic */ Bundle aaW;

        /* access modifiers changed from: protected */
        public void a(GamesClientImpl gamesClientImpl) {
            gamesClientImpl.a((b<Status>) this, this.aaV, this.aaW);
        }
    }

    /* renamed from: com.google.android.gms.games.internal.api.NotificationsImpl$6 reason: invalid class name */
    class AnonymousClass6 extends InboxCountImpl {
        /* access modifiers changed from: protected */
        public void a(GamesClientImpl gamesClientImpl) {
            gamesClientImpl.i(this);
        }
    }

    private static abstract class ContactSettingLoadImpl extends BaseGamesApiMethodImpl<ContactSettingLoadResult> {
        /* renamed from: aa */
        public ContactSettingLoadResult c(final Status status) {
            return new ContactSettingLoadResult() {
                public Status getStatus() {
                    return status;
                }
            };
        }
    }

    private static abstract class ContactSettingUpdateImpl extends BaseGamesApiMethodImpl<Status> {
        /* renamed from: b */
        public Status c(Status status) {
            return status;
        }
    }

    private static abstract class InboxCountImpl extends BaseGamesApiMethodImpl<InboxCountResult> {
        /* renamed from: ab */
        public InboxCountResult c(final Status status) {
            return new InboxCountResult() {
                public Status getStatus() {
                    return status;
                }
            };
        }
    }

    public void clear(GoogleApiClient apiClient, int notificationTypes) {
        Games.d(apiClient).dT(notificationTypes);
    }

    public void clearAll(GoogleApiClient apiClient) {
        clear(apiClient, 31);
    }
}
