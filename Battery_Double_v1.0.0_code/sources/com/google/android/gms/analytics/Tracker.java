package com.google.android.gms.analytics;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import com.google.android.gms.internal.jx;
import com.google.android.gms.internal.ld;
import com.google.android.gms.internal.lf;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Random;

public class Tracker {
    private final TrackerHandler Ce;
    private final Map<String, String> Cf;
    private ah Cg;
    private final k Ch;
    private final ai Ci;
    private final g Cj;
    private boolean Ck;
    /* access modifiers changed from: private */
    public a Cl;
    /* access modifiers changed from: private */
    public am Cm;
    private ExceptionReporter Cn;
    private Context mContext;
    private final Map<String, String> rd;

    private class a implements a {
        private boolean Co = false;
        private int Cp = 0;
        private long Cq = -1;
        private boolean Cr = false;
        private long Cs;
        private ld wb = lf.m2if();

        public a() {
        }

        private void fq() {
            GoogleAnalytics eY = GoogleAnalytics.eY();
            if (eY == null) {
                ae.T("GoogleAnalytics isn't initialized for the Tracker!");
            } else if (this.Cq >= 0 || this.Co) {
                eY.a((a) Tracker.this.Cl);
            } else {
                eY.b(Tracker.this.Cl);
            }
        }

        public void enableAutoActivityTracking(boolean enabled) {
            this.Co = enabled;
            fq();
        }

        public long fn() {
            return this.Cq;
        }

        public boolean fo() {
            return this.Co;
        }

        public boolean fp() {
            boolean z = this.Cr;
            this.Cr = false;
            return z;
        }

        /* access modifiers changed from: 0000 */
        public boolean fr() {
            return this.wb.elapsedRealtime() >= this.Cs + Math.max(1000, this.Cq);
        }

        public void i(Activity activity) {
            y.eK().a(com.google.android.gms.analytics.y.a.EASY_TRACKER_ACTIVITY_START);
            if (this.Cp == 0 && fr()) {
                this.Cr = true;
            }
            this.Cp++;
            if (this.Co) {
                Intent intent = activity.getIntent();
                if (intent != null) {
                    Tracker.this.setCampaignParamsOnNextHit(intent.getData());
                }
                HashMap hashMap = new HashMap();
                hashMap.put("&t", "screenview");
                y.eK().D(true);
                Tracker.this.set("&cd", Tracker.this.Cm != null ? Tracker.this.Cm.k(activity) : activity.getClass().getCanonicalName());
                Tracker.this.send(hashMap);
                y.eK().D(false);
            }
        }

        public void j(Activity activity) {
            y.eK().a(com.google.android.gms.analytics.y.a.EASY_TRACKER_ACTIVITY_STOP);
            this.Cp--;
            this.Cp = Math.max(0, this.Cp);
            if (this.Cp == 0) {
                this.Cs = this.wb.elapsedRealtime();
            }
        }

        public void setSessionTimeout(long sessionTimeout) {
            this.Cq = sessionTimeout;
            fq();
        }
    }

    Tracker(String trackingId, TrackerHandler handler, Context context) {
        this(trackingId, handler, k.el(), ai.fl(), g.dZ(), new ad("tracking"), context);
    }

    Tracker(String trackingId, TrackerHandler handler, k clientIdDefaultProvider, ai screenResolutionDefaultProvider, g appFieldsDefaultProvider, ah rateLimiter, Context context) {
        this.rd = new HashMap();
        this.Cf = new HashMap();
        this.Ce = handler;
        if (context != null) {
            this.mContext = context.getApplicationContext();
        }
        if (trackingId != null) {
            this.rd.put("&tid", trackingId);
        }
        this.rd.put("useSecure", "1");
        this.Ch = clientIdDefaultProvider;
        this.Ci = screenResolutionDefaultProvider;
        this.Cj = appFieldsDefaultProvider;
        this.rd.put("&a", Integer.toString(new Random().nextInt(Integer.MAX_VALUE) + 1));
        this.Cg = rateLimiter;
        this.Cl = new a();
        enableAdvertisingIdCollection(false);
    }

    /* access modifiers changed from: 0000 */
    public void a(am amVar) {
        ae.V("Loading Tracker config values.");
        this.Cm = amVar;
        if (this.Cm.ft()) {
            String fu = this.Cm.fu();
            set("&tid", fu);
            ae.V("[Tracker] trackingId loaded: " + fu);
        }
        if (this.Cm.fv()) {
            String d = Double.toString(this.Cm.fw());
            set("&sf", d);
            ae.V("[Tracker] sample frequency loaded: " + d);
        }
        if (this.Cm.fx()) {
            setSessionTimeout((long) this.Cm.getSessionTimeout());
            ae.V("[Tracker] session timeout loaded: " + fn());
        }
        if (this.Cm.fy()) {
            enableAutoActivityTracking(this.Cm.fz());
            ae.V("[Tracker] auto activity tracking loaded: " + fo());
        }
        if (this.Cm.fA()) {
            if (this.Cm.fB()) {
                set("&aip", "1");
                ae.V("[Tracker] anonymize ip loaded: true");
            }
            ae.V("[Tracker] anonymize ip loaded: false");
        }
        enableExceptionReporting(this.Cm.fC());
    }

    public void enableAdvertisingIdCollection(boolean enabled) {
        if (!enabled) {
            this.rd.put("&ate", null);
            this.rd.put("&adid", null);
            return;
        }
        if (this.rd.containsKey("&ate")) {
            this.rd.remove("&ate");
        }
        if (this.rd.containsKey("&adid")) {
            this.rd.remove("&adid");
        }
    }

    public void enableAutoActivityTracking(boolean enabled) {
        this.Cl.enableAutoActivityTracking(enabled);
    }

    public void enableExceptionReporting(boolean enabled) {
        if (this.Ck != enabled) {
            this.Ck = enabled;
            if (enabled) {
                this.Cn = new ExceptionReporter(this, Thread.getDefaultUncaughtExceptionHandler(), this.mContext);
                Thread.setDefaultUncaughtExceptionHandler(this.Cn);
                ae.V("Uncaught exceptions will be reported to Google Analytics.");
                return;
            }
            if (this.Cn != null) {
                Thread.setDefaultUncaughtExceptionHandler(this.Cn.et());
            } else {
                Thread.setDefaultUncaughtExceptionHandler(null);
            }
            ae.V("Uncaught exceptions will not be reported to Google Analytics.");
        }
    }

    /* access modifiers changed from: 0000 */
    public long fn() {
        return this.Cl.fn();
    }

    /* access modifiers changed from: 0000 */
    public boolean fo() {
        return this.Cl.fo();
    }

    public String get(String key) {
        y.eK().a(com.google.android.gms.analytics.y.a.GET);
        if (TextUtils.isEmpty(key)) {
            return null;
        }
        if (this.rd.containsKey(key)) {
            return (String) this.rd.get(key);
        }
        if (key.equals("&ul")) {
            return an.a(Locale.getDefault());
        }
        if (this.Ch != null && this.Ch.ac(key)) {
            return this.Ch.getValue(key);
        }
        if (this.Ci != null && this.Ci.ac(key)) {
            return this.Ci.getValue(key);
        }
        if (this.Cj == null || !this.Cj.ac(key)) {
            return null;
        }
        return this.Cj.getValue(key);
    }

    public void send(Map<String, String> params) {
        y.eK().a(com.google.android.gms.analytics.y.a.SEND);
        HashMap hashMap = new HashMap();
        hashMap.putAll(this.rd);
        if (params != null) {
            hashMap.putAll(params);
        }
        for (String str : this.Cf.keySet()) {
            if (!hashMap.containsKey(str)) {
                hashMap.put(str, this.Cf.get(str));
            }
        }
        this.Cf.clear();
        if (TextUtils.isEmpty((CharSequence) hashMap.get("&tid"))) {
            ae.W(String.format("Missing tracking id (%s) parameter.", new Object[]{"&tid"}));
        }
        String str2 = (String) hashMap.get("&t");
        if (TextUtils.isEmpty(str2)) {
            ae.W(String.format("Missing hit type (%s) parameter.", new Object[]{"&t"}));
            str2 = "";
        }
        if (this.Cl.fp()) {
            hashMap.put("&sc", "start");
        }
        String lowerCase = str2.toLowerCase();
        if ("screenview".equals(lowerCase) || "pageview".equals(lowerCase) || "appview".equals(lowerCase) || TextUtils.isEmpty(lowerCase)) {
            int parseInt = Integer.parseInt((String) this.rd.get("&a")) + 1;
            if (parseInt >= Integer.MAX_VALUE) {
                parseInt = 1;
            }
            this.rd.put("&a", Integer.toString(parseInt));
        }
        if (lowerCase.equals("transaction") || lowerCase.equals("item") || this.Cg.fe()) {
            this.Ce.u(hashMap);
        } else {
            ae.W("Too many hits sent too quickly, rate limiting invoked.");
        }
    }

    public void set(String key, String value) {
        jx.b(key, (Object) "Key should be non-null");
        y.eK().a(com.google.android.gms.analytics.y.a.SET);
        this.rd.put(key, value);
    }

    public void setAnonymizeIp(boolean anonymize) {
        set("&aip", an.E(anonymize));
    }

    public void setAppId(String appId) {
        set("&aid", appId);
    }

    public void setAppInstallerId(String appInstallerId) {
        set("&aiid", appInstallerId);
    }

    public void setAppName(String appName) {
        set("&an", appName);
    }

    public void setAppVersion(String appVersion) {
        set("&av", appVersion);
    }

    public void setCampaignParamsOnNextHit(Uri uri) {
        if (uri != null) {
            String queryParameter = uri.getQueryParameter("referrer");
            if (!TextUtils.isEmpty(queryParameter)) {
                Uri parse = Uri.parse("http://hostname/?" + queryParameter);
                String queryParameter2 = parse.getQueryParameter("utm_id");
                if (queryParameter2 != null) {
                    this.Cf.put("&ci", queryParameter2);
                }
                String queryParameter3 = parse.getQueryParameter("utm_campaign");
                if (queryParameter3 != null) {
                    this.Cf.put("&cn", queryParameter3);
                }
                String queryParameter4 = parse.getQueryParameter("utm_content");
                if (queryParameter4 != null) {
                    this.Cf.put("&cc", queryParameter4);
                }
                String queryParameter5 = parse.getQueryParameter("utm_medium");
                if (queryParameter5 != null) {
                    this.Cf.put("&cm", queryParameter5);
                }
                String queryParameter6 = parse.getQueryParameter("utm_source");
                if (queryParameter6 != null) {
                    this.Cf.put("&cs", queryParameter6);
                }
                String queryParameter7 = parse.getQueryParameter("utm_term");
                if (queryParameter7 != null) {
                    this.Cf.put("&ck", queryParameter7);
                }
                String queryParameter8 = parse.getQueryParameter("dclid");
                if (queryParameter8 != null) {
                    this.Cf.put("&dclid", queryParameter8);
                }
                String queryParameter9 = parse.getQueryParameter("gclid");
                if (queryParameter9 != null) {
                    this.Cf.put("&gclid", queryParameter9);
                }
            }
        }
    }

    public void setClientId(String clientId) {
        set("&cid", clientId);
    }

    public void setEncoding(String encoding) {
        set("&de", encoding);
    }

    public void setHostname(String hostname) {
        set("&dh", hostname);
    }

    public void setLanguage(String language) {
        set("&ul", language);
    }

    public void setLocation(String location) {
        set("&dl", location);
    }

    public void setPage(String page) {
        set("&dp", page);
    }

    public void setReferrer(String referrer) {
        set("&dr", referrer);
    }

    public void setSampleRate(double sampleRate) {
        set("&sf", Double.toHexString(sampleRate));
    }

    public void setScreenColors(String screenColors) {
        set("&sd", screenColors);
    }

    public void setScreenName(String screenName) {
        set("&cd", screenName);
    }

    public void setScreenResolution(int width, int height) {
        if (width >= 0 || height >= 0) {
            set("&sr", width + "x" + height);
        } else {
            ae.W("Invalid width or height. The values should be non-negative.");
        }
    }

    public void setSessionTimeout(long sessionTimeout) {
        this.Cl.setSessionTimeout(1000 * sessionTimeout);
    }

    public void setTitle(String title) {
        set("&dt", title);
    }

    public void setUseSecure(boolean useSecure) {
        set("useSecure", an.E(useSecure));
    }

    public void setViewportSize(String viewportSize) {
        set("&vp", viewportSize);
    }
}
