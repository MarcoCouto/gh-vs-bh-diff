package com.google.android.gms.analytics;

import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;

interface f {
    void dQ();

    void dW();

    LinkedBlockingQueue<Runnable> dX();

    void dY();

    void dispatch();

    Thread getThread();

    void u(Map<String, String> map);
}
