package com.google.android.gms.analytics;

import com.google.android.gms.internal.ha;
import java.util.List;
import java.util.Map;

interface b {
    void a(Map<String, String> map, long j, String str, List<ha> list);

    void connect();

    void dQ();

    void disconnect();
}
