package com.google.android.gms.analytics;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteCursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build.VERSION;
import android.text.TextUtils;
import com.google.android.gms.internal.ha;
import com.google.android.gms.internal.ld;
import com.google.android.gms.internal.lf;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import org.apache.http.impl.client.DefaultHttpClient;

class ag implements d {
    /* access modifiers changed from: private */
    public static final String BS = String.format("CREATE TABLE IF NOT EXISTS %s ( '%s' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, '%s' INTEGER NOT NULL, '%s' TEXT NOT NULL, '%s' TEXT NOT NULL, '%s' INTEGER);", new Object[]{"hits2", "hit_id", "hit_time", "hit_url", "hit_string", "hit_app_id"});
    private final a BT;
    private volatile r BU;
    /* access modifiers changed from: private */
    public final String BV;
    private af BW;
    private long BX;
    private final int BY;
    /* access modifiers changed from: private */
    public final Context mContext;
    /* access modifiers changed from: private */
    public ld wb;
    private o ys;
    private volatile boolean yt;
    private final e zc;

    class a extends SQLiteOpenHelper {
        private boolean BZ;
        private long Ca = 0;

        a(Context context, String str) {
            super(context, str, null, 1);
        }

        /* JADX INFO: finally extract failed */
        private void a(SQLiteDatabase sQLiteDatabase) {
            boolean z = false;
            Cursor rawQuery = sQLiteDatabase.rawQuery("SELECT * FROM hits2 WHERE 0", null);
            HashSet hashSet = new HashSet();
            try {
                String[] columnNames = rawQuery.getColumnNames();
                for (String add : columnNames) {
                    hashSet.add(add);
                }
                rawQuery.close();
                if (!hashSet.remove("hit_id") || !hashSet.remove("hit_url") || !hashSet.remove("hit_string") || !hashSet.remove("hit_time")) {
                    throw new SQLiteException("Database column missing");
                }
                if (!hashSet.remove("hit_app_id")) {
                    z = true;
                }
                if (!hashSet.isEmpty()) {
                    throw new SQLiteException("Database has extra columns");
                } else if (z) {
                    sQLiteDatabase.execSQL("ALTER TABLE hits2 ADD COLUMN hit_app_id");
                }
            } catch (Throwable th) {
                rawQuery.close();
                throw th;
            }
        }

        /* JADX WARNING: Removed duplicated region for block: B:16:0x0048  */
        private boolean a(String str, SQLiteDatabase sQLiteDatabase) {
            Cursor cursor;
            Cursor cursor2 = null;
            try {
                SQLiteDatabase sQLiteDatabase2 = sQLiteDatabase;
                Cursor query = sQLiteDatabase2.query("SQLITE_MASTER", new String[]{"name"}, "name=?", new String[]{str}, null, null, null);
                try {
                    boolean moveToFirst = query.moveToFirst();
                    if (query == null) {
                        return moveToFirst;
                    }
                    query.close();
                    return moveToFirst;
                } catch (SQLiteException e) {
                    cursor = query;
                } catch (Throwable th) {
                    th = th;
                    cursor2 = query;
                    if (cursor2 != null) {
                    }
                    throw th;
                }
            } catch (SQLiteException e2) {
                cursor = null;
            } catch (Throwable th2) {
                th = th2;
                if (cursor2 != null) {
                    cursor2.close();
                }
                throw th;
            }
            try {
                ae.W("Error querying for table " + str);
                if (cursor != null) {
                    cursor.close();
                }
                return false;
            } catch (Throwable th3) {
                cursor2 = cursor;
                th = th3;
                if (cursor2 != null) {
                }
                throw th;
            }
        }

        public SQLiteDatabase getWritableDatabase() {
            if (!this.BZ || this.Ca + 3600000 <= ag.this.wb.currentTimeMillis()) {
                SQLiteDatabase sQLiteDatabase = null;
                this.BZ = true;
                this.Ca = ag.this.wb.currentTimeMillis();
                try {
                    sQLiteDatabase = super.getWritableDatabase();
                } catch (SQLiteException e) {
                    ag.this.mContext.getDatabasePath(ag.this.BV).delete();
                }
                if (sQLiteDatabase == null) {
                    sQLiteDatabase = super.getWritableDatabase();
                }
                this.BZ = false;
                return sQLiteDatabase;
            }
            throw new SQLiteException("Database creation failed");
        }

        public void onCreate(SQLiteDatabase db) {
            t.ag(db.getPath());
        }

        public void onOpen(SQLiteDatabase db) {
            if (VERSION.SDK_INT < 15) {
                Cursor rawQuery = db.rawQuery("PRAGMA journal_mode=memory", null);
                try {
                    rawQuery.moveToFirst();
                } finally {
                    rawQuery.close();
                }
            }
            if (!a("hits2", db)) {
                db.execSQL(ag.BS);
            } else {
                a(db);
            }
        }

        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        }
    }

    ag(e eVar, Context context, o oVar) {
        this(eVar, context, "google_analytics_v4.db", 2000, oVar);
    }

    ag(e eVar, Context context, String str, int i, o oVar) {
        this.yt = true;
        this.mContext = context.getApplicationContext();
        this.ys = oVar;
        this.BV = str;
        this.zc = eVar;
        this.wb = lf.m2if();
        this.BT = new a(this.mContext, this.BV);
        this.BU = new h(new DefaultHttpClient(), this.mContext, this.ys);
        this.BX = 0;
        this.BY = i;
    }

    static String A(Map<String, String> map) {
        ArrayList arrayList = new ArrayList(map.size());
        for (Entry entry : map.entrySet()) {
            arrayList.add(ac.encode((String) entry.getKey()) + "=" + ac.encode((String) entry.getValue()));
        }
        return TextUtils.join("&", arrayList);
    }

    private void a(Map<String, String> map, long j, String str) {
        long j2;
        SQLiteDatabase al = al("Error opening database for putHit");
        if (al != null) {
            ContentValues contentValues = new ContentValues();
            contentValues.put("hit_string", A(map));
            contentValues.put("hit_time", Long.valueOf(j));
            if (map.containsKey("AppUID")) {
                try {
                    j2 = Long.parseLong((String) map.get("AppUID"));
                } catch (NumberFormatException e) {
                    j2 = 0;
                }
            } else {
                j2 = 0;
            }
            contentValues.put("hit_app_id", Long.valueOf(j2));
            if (str == null) {
                str = "http://www.google-analytics.com/collect";
            }
            if (str.length() == 0) {
                ae.W("Empty path: not sending hit");
                return;
            }
            contentValues.put("hit_url", str);
            try {
                al.insert("hits2", null, contentValues);
                this.zc.B(false);
            } catch (SQLiteException e2) {
                ae.W("Error storing hit");
            }
        }
    }

    private void a(Map<String, String> map, Collection<ha> collection) {
        String substring = "&_v".substring(1);
        if (collection != null) {
            for (ha haVar : collection) {
                if ("appendVersion".equals(haVar.getId())) {
                    map.put(substring, haVar.getValue());
                    return;
                }
            }
        }
    }

    private SQLiteDatabase al(String str) {
        try {
            return this.BT.getWritableDatabase();
        } catch (SQLiteException e) {
            ae.W(str);
            return null;
        }
    }

    private void fh() {
        int fj = (fj() - this.BY) + 1;
        if (fj > 0) {
            List G = G(fj);
            ae.V("Store full, deleting " + G.size() + " hits to make room.");
            b((String[]) G.toArray(new String[0]));
        }
    }

    /* access modifiers changed from: 0000 */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0082  */
    public List<String> G(int i) {
        Cursor cursor;
        ArrayList arrayList = new ArrayList();
        if (i <= 0) {
            ae.W("Invalid maxHits specified. Skipping");
            return arrayList;
        }
        SQLiteDatabase al = al("Error opening database for peekHitIds.");
        if (al == null) {
            return arrayList;
        }
        try {
            cursor = al.query("hits2", new String[]{"hit_id"}, null, null, null, null, String.format("%s ASC", new Object[]{"hit_id"}), Integer.toString(i));
            try {
                if (cursor.moveToFirst()) {
                    do {
                        arrayList.add(String.valueOf(cursor.getLong(0)));
                    } while (cursor.moveToNext());
                }
                if (cursor != null) {
                    cursor.close();
                }
            } catch (SQLiteException e) {
                e = e;
                try {
                    ae.W("Error in peekHits fetching hitIds: " + e.getMessage());
                    if (cursor != null) {
                        cursor.close();
                    }
                    return arrayList;
                } catch (Throwable th) {
                    th = th;
                    if (cursor != null) {
                        cursor.close();
                    }
                    throw th;
                }
            }
        } catch (SQLiteException e2) {
            e = e2;
            cursor = null;
        } catch (Throwable th2) {
            th = th2;
            cursor = null;
            if (cursor != null) {
            }
            throw th;
        }
        return arrayList;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:35:0x00f6, code lost:
        r4.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00fe, code lost:
        r12.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x0177, code lost:
        r2 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x0178, code lost:
        r12 = r13;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x017d, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x017e, code lost:
        r3 = r2;
        r4 = r13;
        r2 = r11;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:80:?, code lost:
        return r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:?, code lost:
        return r2;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00f6  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00fe  */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x0177 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:6:0x003b] */
    /* JADX WARNING: Removed duplicated region for block: B:80:? A[RETURN, SYNTHETIC] */
    public List<ab> H(int i) {
        Cursor cursor;
        SQLiteException sQLiteException;
        ArrayList arrayList;
        Cursor query;
        ArrayList<ab> arrayList2;
        ArrayList arrayList3 = new ArrayList();
        SQLiteDatabase al = al("Error opening database for peekHits");
        if (al == null) {
            return arrayList3;
        }
        Cursor cursor2 = null;
        try {
            query = al.query("hits2", new String[]{"hit_id", "hit_time"}, null, null, null, null, String.format("%s ASC", new Object[]{"hit_id"}), Integer.toString(i));
            try {
                arrayList2 = new ArrayList<>();
                if (query.moveToFirst()) {
                    do {
                        arrayList2.add(new ab(null, query.getLong(0), query.getLong(1), ""));
                    } while (query.moveToNext());
                }
                if (query != null) {
                    query.close();
                }
                try {
                    Cursor query2 = al.query("hits2", new String[]{"hit_id", "hit_string", "hit_url"}, null, null, null, null, String.format("%s ASC", new Object[]{"hit_id"}), Integer.toString(i));
                    try {
                        if (query2.moveToFirst()) {
                            int i2 = 0;
                            while (true) {
                                if (((SQLiteCursor) query2).getWindow().getNumRows() > 0) {
                                    ((ab) arrayList2.get(i2)).aj(query2.getString(1));
                                    ((ab) arrayList2.get(i2)).ak(query2.getString(2));
                                } else {
                                    ae.W(String.format("HitString for hitId %d too large.  Hit will be deleted.", new Object[]{Long.valueOf(((ab) arrayList2.get(i2)).fb())}));
                                }
                                int i3 = i2 + 1;
                                if (!query2.moveToNext()) {
                                    break;
                                }
                                i2 = i3;
                            }
                        }
                        if (query2 != null) {
                            query2.close();
                        }
                        return arrayList2;
                    } catch (SQLiteException e) {
                        e = e;
                        query = query2;
                    } catch (Throwable th) {
                        th = th;
                        query = query2;
                        if (query != null) {
                            query.close();
                        }
                        throw th;
                    }
                } catch (SQLiteException e2) {
                    e = e2;
                }
            } catch (SQLiteException e3) {
                sQLiteException = e3;
                cursor = query;
                arrayList = arrayList2;
                try {
                    ae.W("Error in peekHits fetching hitIds: " + sQLiteException.getMessage());
                    if (cursor == null) {
                    }
                } catch (Throwable th2) {
                    th = th2;
                    cursor2 = cursor;
                    if (cursor2 != null) {
                    }
                    throw th;
                }
            } catch (Throwable th3) {
            }
        } catch (SQLiteException e4) {
            sQLiteException = e4;
            cursor = null;
            arrayList = arrayList3;
        } catch (Throwable th4) {
            th = th4;
            if (cursor2 != null) {
            }
            throw th;
        }
        try {
            ae.W("Error in peekHits fetching hitString: " + e.getMessage());
            ArrayList arrayList4 = new ArrayList();
            boolean z = false;
            for (ab abVar : arrayList2) {
                if (TextUtils.isEmpty(abVar.fa())) {
                    if (z) {
                        break;
                    }
                    z = true;
                }
                arrayList4.add(abVar);
            }
            if (query != null) {
                query.close();
            }
            return arrayList4;
        } catch (Throwable th5) {
            th = th5;
        }
    }

    public void a(Map<String, String> map, long j, String str, Collection<ha> collection) {
        fi();
        fh();
        a(map, collection);
        a(map, j, str);
    }

    /* access modifiers changed from: 0000 */
    @Deprecated
    public void b(Collection<ab> collection) {
        if (collection == null || collection.isEmpty()) {
            ae.W("Empty/Null collection passed to deleteHits.");
            return;
        }
        String[] strArr = new String[collection.size()];
        int i = 0;
        for (ab fb : collection) {
            int i2 = i + 1;
            strArr[i] = String.valueOf(fb.fb());
            i = i2;
        }
        b(strArr);
    }

    /* access modifiers changed from: 0000 */
    public void b(String[] strArr) {
        boolean z = true;
        if (strArr == null || strArr.length == 0) {
            ae.W("Empty hitIds passed to deleteHits.");
            return;
        }
        SQLiteDatabase al = al("Error opening database for deleteHits.");
        if (al != null) {
            try {
                al.delete("hits2", String.format("HIT_ID in (%s)", new Object[]{TextUtils.join(",", Collections.nCopies(strArr.length, "?"))}), strArr);
                e eVar = this.zc;
                if (fj() != 0) {
                    z = false;
                }
                eVar.B(z);
            } catch (SQLiteException e) {
                ae.W("Error deleting hits " + TextUtils.join(",", strArr));
            }
        }
    }

    public r dV() {
        return this.BU;
    }

    public void dispatch() {
        boolean z = true;
        ae.V("Dispatch running...");
        if (this.BU.ea()) {
            List H = H(20);
            if (H.isEmpty()) {
                ae.V("...nothing to dispatch");
                this.zc.B(true);
                return;
            }
            if (this.BW == null) {
                this.BW = new af("_t=dispatch&_v=ma4.0.4", false);
            }
            if (fj() > H.size()) {
                z = false;
            }
            int a2 = this.BU.a(H, this.BW, z);
            ae.V("sent " + a2 + " of " + H.size() + " hits");
            b((Collection<ab>) H.subList(0, Math.min(a2, H.size())));
            if (a2 != H.size() || fj() <= 0) {
                this.BW = null;
            } else {
                GoogleAnalytics.getInstance(this.mContext).dispatchLocalHits();
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public int fi() {
        boolean z = true;
        long currentTimeMillis = this.wb.currentTimeMillis();
        if (currentTimeMillis <= this.BX + 86400000) {
            return 0;
        }
        this.BX = currentTimeMillis;
        SQLiteDatabase al = al("Error opening database for deleteStaleHits.");
        if (al == null) {
            return 0;
        }
        int delete = al.delete("hits2", "HIT_TIME < ?", new String[]{Long.toString(this.wb.currentTimeMillis() - 2592000000L)});
        e eVar = this.zc;
        if (fj() != 0) {
            z = false;
        }
        eVar.B(z);
        return delete;
    }

    /* access modifiers changed from: 0000 */
    public int fj() {
        Cursor cursor = null;
        int i = 0;
        SQLiteDatabase al = al("Error opening database for getNumStoredHits.");
        if (al != null) {
            try {
                Cursor rawQuery = al.rawQuery("SELECT COUNT(*) from hits2", null);
                if (rawQuery.moveToFirst()) {
                    i = (int) rawQuery.getLong(0);
                }
                if (rawQuery != null) {
                    rawQuery.close();
                }
            } catch (SQLiteException e) {
                ae.W("Error getting numStoredHits");
                if (cursor != null) {
                    cursor.close();
                }
            } catch (Throwable th) {
                if (cursor != null) {
                    cursor.close();
                }
                throw th;
            }
        }
        return i;
    }

    public void l(long j) {
        boolean z = true;
        SQLiteDatabase al = al("Error opening database for clearHits");
        if (al != null) {
            if (j == 0) {
                al.delete("hits2", null, null);
            } else {
                al.delete("hits2", "hit_app_id = ?", new String[]{Long.valueOf(j).toString()});
            }
            e eVar = this.zc;
            if (fj() != 0) {
                z = false;
            }
            eVar.B(z);
        }
    }

    public void setDryRun(boolean dryRun) {
        this.yt = dryRun;
        if (this.BU != null) {
            this.BU.setDryRun(dryRun);
        }
    }
}
