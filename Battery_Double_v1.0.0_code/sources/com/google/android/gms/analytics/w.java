package com.google.android.gms.analytics;

import android.content.Context;
import android.content.Intent;
import com.google.android.gms.analytics.c.C0000c;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.internal.ha;
import com.google.android.gms.internal.ld;
import com.google.android.gms.internal.lf;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Queue;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentLinkedQueue;

class w implements ak, com.google.android.gms.analytics.c.b, C0000c {
    private final Context mContext;
    /* access modifiers changed from: private */
    public ld wb;
    private d yU;
    private final f yV;
    private boolean yX;
    /* access modifiers changed from: private */
    public volatile long zh;
    /* access modifiers changed from: private */
    public volatile a zi;
    private volatile b zj;
    private d zk;
    private final GoogleAnalytics zl;
    /* access modifiers changed from: private */
    public final Queue<d> zm;
    private volatile int zn;
    private volatile Timer zo;
    private volatile Timer zp;
    /* access modifiers changed from: private */
    public volatile Timer zq;
    private boolean zr;
    private boolean zs;
    private boolean zt;
    /* access modifiers changed from: private */
    public long zu;

    private enum a {
        CONNECTING,
        CONNECTED_SERVICE,
        CONNECTED_LOCAL,
        BLOCKED,
        PENDING_CONNECTION,
        PENDING_DISCONNECT,
        DISCONNECTED
    }

    private class b extends TimerTask {
        private b() {
        }

        public void run() {
            if (w.this.zi != a.CONNECTED_SERVICE || !w.this.zm.isEmpty() || w.this.zh + w.this.zu >= w.this.wb.elapsedRealtime()) {
                w.this.zq.schedule(new b(), w.this.zu);
                return;
            }
            ae.V("Disconnecting due to inactivity");
            w.this.cJ();
        }
    }

    private class c extends TimerTask {
        private c() {
        }

        public void run() {
            if (w.this.zi == a.CONNECTING) {
                w.this.eE();
            }
        }
    }

    private static class d {
        private final Map<String, String> zF;
        private final long zG;
        private final String zH;
        private final List<ha> zI;

        public d(Map<String, String> map, long j, String str, List<ha> list) {
            this.zF = map;
            this.zG = j;
            this.zH = str;
            this.zI = list;
        }

        public Map<String, String> eH() {
            return this.zF;
        }

        public long eI() {
            return this.zG;
        }

        public List<ha> eJ() {
            return this.zI;
        }

        public String getPath() {
            return this.zH;
        }

        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append("PATH: ");
            sb.append(this.zH);
            if (this.zF != null) {
                sb.append("  PARAMS: ");
                for (Entry entry : this.zF.entrySet()) {
                    sb.append((String) entry.getKey());
                    sb.append("=");
                    sb.append((String) entry.getValue());
                    sb.append(",  ");
                }
            }
            return sb.toString();
        }
    }

    private class e extends TimerTask {
        private e() {
        }

        public void run() {
            w.this.eF();
        }
    }

    w(Context context, f fVar) {
        this(context, fVar, null, GoogleAnalytics.getInstance(context));
    }

    w(Context context, f fVar, d dVar, GoogleAnalytics googleAnalytics) {
        this.zm = new ConcurrentLinkedQueue();
        this.zu = 300000;
        this.zk = dVar;
        this.mContext = context;
        this.yV = fVar;
        this.zl = googleAnalytics;
        this.wb = lf.m2if();
        this.zn = 0;
        this.zi = a.DISCONNECTED;
    }

    private Timer a(Timer timer) {
        if (timer != null) {
            timer.cancel();
        }
        return null;
    }

    /* access modifiers changed from: private */
    public synchronized void cJ() {
        if (this.zj != null && this.zi == a.CONNECTED_SERVICE) {
            this.zi = a.PENDING_DISCONNECT;
            this.zj.disconnect();
        }
    }

    private void eA() {
        this.zo = a(this.zo);
        this.zp = a(this.zp);
        this.zq = a(this.zq);
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0084, code lost:
        if (r8.yX == false) goto L_0x001f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0086, code lost:
        eD();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x00db, code lost:
        r8.zh = r8.wb.elapsedRealtime();
     */
    public synchronized void eC() {
        if (Thread.currentThread().equals(this.yV.getThread())) {
            if (this.zr) {
                dQ();
            }
            switch (this.zi) {
                case CONNECTED_LOCAL:
                    while (!this.zm.isEmpty()) {
                        d dVar = (d) this.zm.poll();
                        ae.V("Sending hit to store  " + dVar);
                        this.yU.a(dVar.eH(), dVar.eI(), dVar.getPath(), dVar.eJ());
                    }
                    break;
                case CONNECTED_SERVICE:
                    while (!this.zm.isEmpty()) {
                        d dVar2 = (d) this.zm.peek();
                        ae.V("Sending hit to service   " + dVar2);
                        if (!this.zl.isDryRunEnabled()) {
                            this.zj.a(dVar2.eH(), dVar2.eI(), dVar2.getPath(), dVar2.eJ());
                        } else {
                            ae.V("Dry run enabled. Hit not actually sent to service.");
                        }
                        this.zm.poll();
                    }
                    break;
                case DISCONNECTED:
                    ae.V("Need to reconnect");
                    if (!this.zm.isEmpty()) {
                        eF();
                        break;
                    }
                    break;
                case BLOCKED:
                    ae.V("Blocked. Dropping hits.");
                    this.zm.clear();
                    break;
            }
        } else {
            this.yV.dX().add(new Runnable() {
                public void run() {
                    w.this.eC();
                }
            });
        }
    }

    private void eD() {
        this.yU.dispatch();
        this.yX = false;
    }

    /* access modifiers changed from: private */
    public synchronized void eE() {
        if (this.zi != a.CONNECTED_LOCAL) {
            if (this.mContext == null || !GooglePlayServicesUtil.GOOGLE_PLAY_SERVICES_PACKAGE.equals(this.mContext.getPackageName())) {
                eA();
                ae.V("falling back to local store");
                if (this.zk != null) {
                    this.yU = this.zk;
                } else {
                    v eu = v.eu();
                    eu.a(this.mContext, this.yV);
                    this.yU = eu.ex();
                }
                this.zi = a.CONNECTED_LOCAL;
                eC();
            } else {
                this.zi = a.BLOCKED;
                this.zj.disconnect();
                ae.W("Attempted to fall back to local store from service.");
            }
        }
    }

    /* access modifiers changed from: private */
    public synchronized void eF() {
        if (this.zt || this.zj == null || this.zi == a.CONNECTED_LOCAL) {
            ae.W("client not initialized.");
            eE();
        } else {
            try {
                this.zn++;
                a(this.zp);
                this.zi = a.CONNECTING;
                this.zp = new Timer("Failed Connect");
                this.zp.schedule(new c(), 3000);
                ae.V("connecting to Analytics service");
                this.zj.connect();
            } catch (SecurityException e2) {
                ae.W("security exception on connectToService");
                eE();
            }
        }
        return;
    }

    private void eG() {
        this.zo = a(this.zo);
        this.zo = new Timer("Service Reconnect");
        this.zo.schedule(new e(), 5000);
    }

    public synchronized void a(int i, Intent intent) {
        this.zi = a.PENDING_CONNECTION;
        if (this.zn < 2) {
            ae.W("Service unavailable (code=" + i + "), will retry.");
            eG();
        } else {
            ae.W("Service unavailable (code=" + i + "), using local store.");
            eE();
        }
    }

    public void b(Map<String, String> map, long j, String str, List<ha> list) {
        ae.V("putHit called");
        this.zm.add(new d(map, j, str, list));
        eC();
    }

    public void dQ() {
        ae.V("clearHits called");
        this.zm.clear();
        switch (this.zi) {
            case CONNECTED_LOCAL:
                this.yU.l(0);
                this.zr = false;
                return;
            case CONNECTED_SERVICE:
                this.zj.dQ();
                this.zr = false;
                return;
            default:
                this.zr = true;
                return;
        }
    }

    public synchronized void dW() {
        if (!this.zt) {
            ae.V("setForceLocalDispatch called.");
            this.zt = true;
            switch (this.zi) {
                case CONNECTED_LOCAL:
                case PENDING_CONNECTION:
                case PENDING_DISCONNECT:
                case DISCONNECTED:
                    break;
                case CONNECTED_SERVICE:
                    cJ();
                    break;
                case CONNECTING:
                    this.zs = true;
                    break;
            }
        }
    }

    public void dispatch() {
        switch (this.zi) {
            case CONNECTED_LOCAL:
                eD();
                return;
            case CONNECTED_SERVICE:
                return;
            default:
                this.yX = true;
                return;
        }
    }

    public void eB() {
        if (this.zj == null) {
            this.zj = new c(this.mContext, this, this);
            eF();
        }
    }

    public synchronized void onConnected() {
        this.zp = a(this.zp);
        this.zn = 0;
        ae.V("Connected to service");
        this.zi = a.CONNECTED_SERVICE;
        if (this.zs) {
            cJ();
            this.zs = false;
        } else {
            eC();
            this.zq = a(this.zq);
            this.zq = new Timer("disconnect check");
            this.zq.schedule(new b(), this.zu);
        }
    }

    public synchronized void onDisconnected() {
        if (this.zi == a.BLOCKED) {
            ae.V("Service blocked.");
            eA();
        } else if (this.zi == a.PENDING_DISCONNECT) {
            ae.V("Disconnected from service");
            eA();
            this.zi = a.DISCONNECTED;
        } else {
            ae.V("Unexpected disconnect.");
            this.zi = a.PENDING_CONNECTION;
            if (this.zn < 2) {
                eG();
            } else {
                eE();
            }
        }
    }
}
