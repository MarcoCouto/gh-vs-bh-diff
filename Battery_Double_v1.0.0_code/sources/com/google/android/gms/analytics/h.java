package com.google.android.gms.analytics;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Build.VERSION;
import android.text.TextUtils;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.zip.GZIPOutputStream;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpEntityEnclosingRequest;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.message.BasicHttpEntityEnclosingRequest;

public class h implements r {
    private final Context mContext;
    private final String wl;
    private final HttpClient yf;
    private URL yg;
    private int yh;
    private int yi;
    private int yj;
    private String yk;
    private String yl;
    private i ym;
    private l yn;
    private Set<Integer> yo = new HashSet();
    private boolean yp = false;
    private long yq;
    private long yr;
    private o ys;
    private volatile boolean yt = false;

    h(HttpClient httpClient, Context context, o oVar) {
        this.mContext = context.getApplicationContext();
        this.ys = oVar;
        this.wl = a("GoogleAnalytics", "4.0", VERSION.RELEASE, an.a(Locale.getDefault()), Build.MODEL, Build.ID);
        this.yf = httpClient;
    }

    private String a(ab abVar, List<String> list, i iVar) {
        if (iVar == i.NONE) {
            return TextUtils.isEmpty((abVar.fa() == null || abVar.fa().length() == 0) ? "" : abVar.fa()) ? "" : ac.a(abVar, System.currentTimeMillis());
        }
        String str = "";
        for (String str2 : list) {
            if (str2.length() != 0) {
                if (str.length() != 0) {
                    str = str + "\n";
                }
                str = str + str2;
            }
        }
        return str;
    }

    private URL a(ab abVar) {
        if (this.yg != null) {
            return this.yg;
        }
        try {
            return new URL("http:".equals(abVar.fd()) ? "http://www.google-analytics.com/collect" : "https://ssl.google-analytics.com/collect");
        } catch (MalformedURLException e) {
            ae.T("Error trying to parse the hardcoded host url. This really shouldn't happen.");
            return null;
        }
    }

    private void a(af afVar, HttpHost httpHost, i iVar, l lVar) {
        HttpHost httpHost2;
        afVar.g("_bs", iVar.toString());
        afVar.g("_cs", lVar.toString());
        String fg = afVar.fg();
        if (!TextUtils.isEmpty(fg)) {
            if (httpHost == null) {
                try {
                    URL url = new URL("https://ssl.google-analytics.com");
                    httpHost2 = new HttpHost(url.getHost(), url.getPort(), url.getProtocol());
                } catch (MalformedURLException e) {
                    return;
                }
            } else {
                httpHost2 = httpHost;
            }
            a(fg, httpHost2, 1, afVar, l.NONE);
        }
    }

    private void a(HttpEntityEnclosingRequest httpEntityEnclosingRequest) {
        StringBuffer stringBuffer = new StringBuffer();
        for (Header obj : httpEntityEnclosingRequest.getAllHeaders()) {
            stringBuffer.append(obj.toString()).append("\n");
        }
        stringBuffer.append(httpEntityEnclosingRequest.getRequestLine().toString()).append("\n");
        if (httpEntityEnclosingRequest.getEntity() != null) {
            try {
                InputStream content = httpEntityEnclosingRequest.getEntity().getContent();
                if (content != null) {
                    int available = content.available();
                    if (available > 0) {
                        byte[] bArr = new byte[available];
                        content.read(bArr);
                        stringBuffer.append("POST:\n");
                        stringBuffer.append(new String(bArr)).append("\n");
                    }
                }
            } catch (IOException e) {
                ae.W("Error Writing hit to log...");
            }
        }
        ae.U(stringBuffer.toString());
    }

    private boolean a(String str, HttpHost httpHost, int i, af afVar, l lVar) {
        int length;
        int length2;
        boolean z = i > 1;
        if (str.getBytes().length > this.yj || str.getBytes().length > this.yi) {
            ae.W("Request too long (> " + Math.min(this.yi, this.yj) + " bytes)--not sent");
            return true;
        } else if (this.yt) {
            ae.U("Dry run enabled. Hit not actually sent.");
            return true;
        } else {
            HttpEntityEnclosingRequest d = d(str, z);
            if (d == null) {
                return true;
            }
            if (d.getRequestLine().getMethod().equals("GET")) {
                length2 = str.getBytes().length;
                length = str.getBytes().length;
            } else {
                try {
                    switch (lVar) {
                        case GZIP:
                            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                            GZIPOutputStream gZIPOutputStream = new GZIPOutputStream(byteArrayOutputStream);
                            gZIPOutputStream.write(str.getBytes());
                            gZIPOutputStream.close();
                            byte[] byteArray = byteArrayOutputStream.toByteArray();
                            length = byteArray.length + 0;
                            d.setEntity(new ByteArrayEntity(byteArray));
                            d.addHeader("Content-Encoding", "gzip");
                            break;
                        default:
                            length = str.getBytes().length;
                            d.setEntity(new StringEntity(str));
                            break;
                    }
                    length2 = str.getBytes().length;
                } catch (UnsupportedEncodingException e) {
                    ae.T("Encoding error, hit will be discarded");
                    return true;
                } catch (IOException e2) {
                    ae.T("Unexpected IOException: " + e2.getMessage());
                    ae.T("Request will be discarded");
                    return true;
                }
            }
            d.addHeader("Host", httpHost.toHostString());
            if (ae.ff()) {
                a(d);
            }
            try {
                HttpResponse execute = this.yf.execute(httpHost, d);
                afVar.e("_td", length2);
                afVar.e("_cd", length);
                int statusCode = execute.getStatusLine().getStatusCode();
                HttpEntity entity = execute.getEntity();
                if (entity != null) {
                    entity.consumeContent();
                }
                if (statusCode == 200) {
                    return true;
                }
                if (!z || !this.yo.contains(Integer.valueOf(statusCode))) {
                    ae.W("Bad response: " + execute.getStatusLine().getStatusCode());
                    return true;
                }
                ae.U("Falling back to single hit per request mode.");
                this.yp = true;
                this.yq = System.currentTimeMillis();
                return false;
            } catch (ClientProtocolException e3) {
                ae.W("ClientProtocolException sending hit; discarding hit...");
                return true;
            } catch (IOException e4) {
                ae.W("Exception sending hit: " + e4.getClass().getSimpleName());
                ae.W(e4.getMessage());
                return false;
            }
        }
    }

    private HttpEntityEnclosingRequest d(String str, boolean z) {
        if (TextUtils.isEmpty(str)) {
            System.out.println("Empty hit, discarding.");
            return null;
        }
        String str2 = this.yk + "?" + str;
        BasicHttpEntityEnclosingRequest basicHttpEntityEnclosingRequest = (str2.length() >= this.yh || z) ? z ? new BasicHttpEntityEnclosingRequest("POST", this.yl) : new BasicHttpEntityEnclosingRequest("POST", this.yk) : new BasicHttpEntityEnclosingRequest("GET", str2);
        basicHttpEntityEnclosingRequest.addHeader("User-Agent", this.wl);
        return basicHttpEntityEnclosingRequest;
    }

    /* access modifiers changed from: 0000 */
    public int a(List<ab> list, int i) {
        long j;
        int i2;
        if (list.isEmpty()) {
            return 0;
        }
        if (i > list.size()) {
            i = list.size();
        }
        int i3 = i - 1;
        long j2 = 0;
        int i4 = i;
        while (i3 > 0) {
            ab abVar = (ab) list.get(i3);
            long fc = ((ab) list.get(i3 - 1)).fc();
            long fc2 = abVar.fc();
            if (fc == 0 || fc2 == 0 || fc2 - fc <= j2) {
                j = j2;
                i2 = i4;
            } else {
                j = fc2 - fc;
                i2 = i3;
            }
            i3--;
            i4 = i2;
            j2 = j;
        }
        return i4;
    }

    /* JADX WARNING: Removed duplicated region for block: B:54:0x01c3  */
    /* JADX WARNING: Removed duplicated region for block: B:86:0x02b1  */
    public int a(List<ab> list, af afVar, boolean z) {
        l lVar;
        i iVar;
        int i;
        int i2;
        List<String> list2;
        int i3;
        int i4;
        int i5;
        this.yh = this.ys.eb();
        this.yi = this.ys.ec();
        this.yj = this.ys.ed();
        int ee = this.ys.ee();
        this.yk = this.ys.eg();
        this.yl = this.ys.eh();
        this.ym = this.ys.ei();
        this.yn = this.ys.ej();
        this.yo.clear();
        this.yo.addAll(this.ys.ek());
        this.yr = this.ys.ef();
        if (!this.yp && this.yo.isEmpty()) {
            this.yp = true;
            this.yq = System.currentTimeMillis();
        }
        if (this.yp && System.currentTimeMillis() - this.yq > 1000 * this.yr) {
            this.yp = false;
        }
        if (this.yp) {
            i iVar2 = i.NONE;
            lVar = l.NONE;
            iVar = iVar2;
        } else {
            i iVar3 = this.ym;
            lVar = this.yn;
            iVar = iVar3;
        }
        int i6 = 0;
        int min = Math.min(list.size(), ee);
        afVar.e("_hr", list.size());
        long currentTimeMillis = System.currentTimeMillis();
        List arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        long j = 0;
        int i7 = 0;
        if (iVar != i.NONE) {
            Iterator it = list.iterator();
            while (true) {
                i2 = i7;
                if (!it.hasNext()) {
                    i = 1;
                    break;
                }
                ab abVar = (ab) it.next();
                String a = TextUtils.isEmpty(abVar.fa()) ? "" : ac.a(abVar, currentTimeMillis);
                if (a.getBytes().length > this.yi) {
                    a = "";
                }
                arrayList.add(a);
                if (!TextUtils.isEmpty(a)) {
                    j += (long) ((i2 == 0 ? 0 : 1) + a.getBytes().length);
                }
                arrayList2.add(Long.valueOf(j));
                i7 = j <= ((long) this.yj) ? i2 + 1 : i2;
                if (i7 == min) {
                    i2 = i7;
                    i = 1;
                    break;
                }
            }
        } else {
            i2 = 0;
            i = min;
        }
        while (i2 > 1 && ((Long) arrayList2.get(i2 - 1)).longValue() > ((long) this.yj)) {
            i2--;
        }
        if (j > ((long) this.yj)) {
            switch (iVar) {
                case BATCH_BY_COUNT:
                    i5 = arrayList.size() / 2;
                    if (i2 <= i5) {
                        i5 = i2;
                        break;
                    }
                    break;
                case BATCH_BY_TIME:
                    i5 = a(list, i2);
                    break;
                case BATCH_BY_SESSION:
                    i5 = b(list, i2);
                    break;
                case BATCH_BY_SIZE:
                    if (j >= ((long) (this.yj * 2))) {
                        i5 = i2;
                        break;
                    } else {
                        int size = arrayList2.size() - 1;
                        while (size > 0 && ((Long) arrayList2.get(size)).longValue() > j / 2) {
                            size--;
                        }
                        i5 = size;
                        break;
                    }
                case BATCH_BY_BRUTE_FORCE:
                    i5 = i2;
                    break;
                default:
                    ae.W("Unexpected batching strategy encountered; sending a single hit.");
                    String str = (String) arrayList.get(0);
                    arrayList.clear();
                    arrayList.add(str);
                    i5 = 1;
                    break;
            }
            if (i5 < arrayList.size()) {
                list2 = arrayList.subList(0, i5);
                int i8 = 0;
                HttpHost httpHost = null;
                i3 = 0;
                while (i3 < i) {
                    ab abVar2 = (ab) list.get(i3);
                    URL a2 = a(abVar2);
                    int max = Math.max(1, list2.size());
                    if (a2 == null) {
                        ae.W("No destination: discarding hit.");
                        i4 = i8 + max;
                    } else {
                        httpHost = new HttpHost(a2.getHost(), a2.getPort(), a2.getProtocol());
                        if (!a(a(abVar2, list2, iVar), httpHost, max, afVar, lVar)) {
                            afVar.e("_de", 1);
                            afVar.e("_hd", i8);
                            afVar.e("_hs", i6);
                            a(afVar, httpHost, iVar, lVar);
                            return i6;
                        }
                        i4 = i8;
                        for (String isEmpty : list2) {
                            i4 = TextUtils.isEmpty(isEmpty) ? i4 + 1 : i4;
                        }
                        afVar.e("_rs", 1);
                    }
                    i3++;
                    i8 = i4;
                    i6 += max;
                }
                afVar.e("_hd", i8);
                afVar.e("_hs", i6);
                if (z) {
                    a(afVar, httpHost, iVar, lVar);
                }
                return i6;
            }
        }
        list2 = arrayList;
        int i82 = 0;
        HttpHost httpHost2 = null;
        i3 = 0;
        while (i3 < i) {
        }
        afVar.e("_hd", i82);
        afVar.e("_hs", i6);
        if (z) {
        }
        return i6;
    }

    /* access modifiers changed from: 0000 */
    public String a(String str, String str2, String str3, String str4, String str5, String str6) {
        return String.format("%s/%s (Linux; U; Android %s; %s; %s Build/%s)", new Object[]{str, str2, str3, str4, str5, str6});
    }

    public void ad(String str) {
        try {
            this.yg = new URL(str);
        } catch (MalformedURLException e) {
            this.yg = null;
        }
    }

    /* access modifiers changed from: 0000 */
    public int b(List<ab> list, int i) {
        if (list.isEmpty()) {
            return 0;
        }
        for (int i2 = i - 1; i2 > 0; i2--) {
            String fa = ((ab) list.get(i2)).fa();
            if (!TextUtils.isEmpty(fa)) {
                if (fa.contains("sc=start")) {
                    return i2;
                }
                if (fa.contains("sc=end")) {
                    return i2 + 1;
                }
            }
        }
        return i;
    }

    public boolean ea() {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) this.mContext.getSystemService("connectivity")).getActiveNetworkInfo();
        if (activeNetworkInfo != null && activeNetworkInfo.isConnected()) {
            return true;
        }
        ae.V("...no network connectivity");
        return false;
    }

    public void setDryRun(boolean dryRun) {
        this.yt = dryRun;
    }
}
