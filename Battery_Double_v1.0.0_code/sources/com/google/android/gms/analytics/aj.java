package com.google.android.gms.analytics;

abstract class aj {
    aj() {
    }

    /* access modifiers changed from: 0000 */
    public abstract void C(boolean z);

    /* access modifiers changed from: 0000 */
    public abstract void dispatchLocalHits();

    /* access modifiers changed from: 0000 */
    public abstract void ey();

    /* access modifiers changed from: 0000 */
    public abstract void setLocalDispatchPeriod(int i);
}
