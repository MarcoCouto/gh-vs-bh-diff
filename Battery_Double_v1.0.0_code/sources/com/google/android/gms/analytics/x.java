package com.google.android.gms.analytics;

import android.content.Context;
import android.os.Process;
import android.text.TextUtils;
import com.google.android.gms.fitness.FitnessActivities;
import com.google.android.gms.internal.ha;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

class x extends Thread implements f {
    private static x zM;
    private volatile boolean mClosed = false;
    /* access modifiers changed from: private */
    public final Context mContext;
    private final LinkedBlockingQueue<Runnable> zJ = new LinkedBlockingQueue<>();
    private volatile boolean zK = false;
    /* access modifiers changed from: private */
    public volatile String zL;
    /* access modifiers changed from: private */
    public volatile ak zN;
    private final Lock zO;
    /* access modifiers changed from: private */
    public final List<ha> zP = new ArrayList();

    private class a implements Runnable {
        private a() {
        }

        public void run() {
            x.this.zN.dQ();
        }
    }

    private class b implements Runnable {
        private b() {
        }

        public void run() {
            x.this.zN.dispatch();
        }
    }

    private class c implements Runnable {
        private c() {
        }

        public void run() {
            x.this.zN.dW();
        }
    }

    private class d implements Runnable {
        private final Map<String, String> zR;

        d(Map<String, String> map) {
            this.zR = new HashMap(map);
            String str = (String) map.get("&ht");
            if (str != null) {
                try {
                    Long.valueOf(str);
                } catch (NumberFormatException e) {
                    str = null;
                }
            }
            if (str == null) {
                this.zR.put("&ht", Long.toString(System.currentTimeMillis()));
            }
        }

        private String v(Map<String, String> map) {
            return map.containsKey("useSecure") ? an.f((String) map.get("useSecure"), true) ? "https:" : "http:" : "https:";
        }

        private void w(Map<String, String> map) {
            q w = a.w(x.this.mContext);
            an.a(map, "&adid", w);
            an.a(map, "&ate", w);
        }

        private void x(Map<String, String> map) {
            g dZ = g.dZ();
            an.a(map, "&an", (q) dZ);
            an.a(map, "&av", (q) dZ);
            an.a(map, "&aid", (q) dZ);
            an.a(map, "&aiid", (q) dZ);
            map.put("&v", "1");
        }

        private boolean y(Map<String, String> map) {
            if (map.get("&sf") == null) {
                return false;
            }
            double a = an.a((String) map.get("&sf"), 100.0d);
            if (a >= 100.0d) {
                return false;
            }
            if (((double) (x.ah((String) map.get("&cid")) % 10000)) < a * 100.0d) {
                return false;
            }
            ae.V(String.format("%s hit sampled out", new Object[]{map.get("&t") == null ? FitnessActivities.UNKNOWN : (String) map.get("&t")}));
            return true;
        }

        public void run() {
            w(this.zR);
            if (TextUtils.isEmpty((CharSequence) this.zR.get("&cid"))) {
                this.zR.put("&cid", k.el().getValue("&cid"));
            }
            if (!GoogleAnalytics.getInstance(x.this.mContext).getAppOptOut() && !y(this.zR)) {
                if (!TextUtils.isEmpty(x.this.zL)) {
                    y.eK().D(true);
                    this.zR.putAll(new HitBuilder().setCampaignParamsFromUrl(x.this.zL).build());
                    y.eK().D(false);
                    x.this.zL = null;
                }
                x(this.zR);
                x.this.zN.b(ac.z(this.zR), Long.valueOf((String) this.zR.get("&ht")).longValue(), v(this.zR), x.this.zP);
            }
        }
    }

    private x(Context context) {
        super("GAThread");
        if (context != null) {
            this.mContext = context.getApplicationContext();
        } else {
            this.mContext = context;
        }
        this.zP.add(new ha("appendVersion", "&_v".substring(1), "ma4.0.4"));
        this.zO = new ReentrantLock();
        start();
    }

    static x A(Context context) {
        if (zM == null) {
            zM = new x(context);
        }
        return zM;
    }

    static String B(Context context) {
        try {
            FileInputStream openFileInput = context.openFileInput("gaInstallData");
            byte[] bArr = new byte[8192];
            int read = openFileInput.read(bArr, 0, 8192);
            if (openFileInput.available() > 0) {
                ae.T("Too much campaign data, ignoring it.");
                openFileInput.close();
                context.deleteFile("gaInstallData");
                return null;
            }
            openFileInput.close();
            context.deleteFile("gaInstallData");
            if (read <= 0) {
                ae.W("Campaign file is empty.");
                return null;
            }
            String str = new String(bArr, 0, read);
            ae.U("Campaign found: " + str);
            return str;
        } catch (FileNotFoundException e) {
            ae.U("No campaign data found.");
            return null;
        } catch (IOException e2) {
            ae.T("Error reading campaign data.");
            context.deleteFile("gaInstallData");
            return null;
        }
    }

    static int ah(String str) {
        int i = 1;
        if (!TextUtils.isEmpty(str)) {
            i = 0;
            for (int length = str.length() - 1; length >= 0; length--) {
                char charAt = str.charAt(length);
                i = ((i << 6) & 65535) + charAt + (charAt << 14);
                int i2 = 266338304 & i;
                if (i2 != 0) {
                    i ^= i2 >> 21;
                }
            }
        }
        return i;
    }

    private void b(Runnable runnable) {
        this.zJ.add(runnable);
    }

    private String g(Throwable th) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        PrintStream printStream = new PrintStream(byteArrayOutputStream);
        th.printStackTrace(printStream);
        printStream.flush();
        return new String(byteArrayOutputStream.toByteArray());
    }

    public void dQ() {
        b((Runnable) new a());
    }

    public void dW() {
        b((Runnable) new c());
    }

    public LinkedBlockingQueue<Runnable> dX() {
        return this.zJ;
    }

    public void dY() {
        init();
        ArrayList<Runnable> arrayList = new ArrayList<>();
        this.zJ.drainTo(arrayList);
        this.zO.lock();
        try {
            this.zK = true;
            for (Runnable run : arrayList) {
                run.run();
            }
        } catch (Throwable th) {
            this.zO.unlock();
            throw th;
        }
        this.zO.unlock();
    }

    public void dispatch() {
        b((Runnable) new b());
    }

    public Thread getThread() {
        return this;
    }

    /* access modifiers changed from: protected */
    public synchronized void init() {
        if (this.zN == null) {
            this.zN = new w(this.mContext, this);
            this.zN.eB();
        }
    }

    public void run() {
        Process.setThreadPriority(10);
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            ae.W("sleep interrupted in GAThread initialize");
        }
        try {
            init();
            this.zL = B(this.mContext);
            ae.V("Initialized GA Thread");
        } catch (Throwable th) {
            ae.T("Error initializing the GAThread: " + g(th));
            ae.T("Google Analytics will not start up.");
            this.zK = true;
        }
        while (!this.mClosed) {
            try {
                Runnable runnable = (Runnable) this.zJ.take();
                this.zO.lock();
                if (!this.zK) {
                    runnable.run();
                }
                this.zO.unlock();
            } catch (InterruptedException e2) {
                ae.U(e2.toString());
            } catch (Throwable th2) {
                ae.T("Error on GAThread: " + g(th2));
                ae.T("Google Analytics is shutting down.");
                this.zK = true;
            }
        }
    }

    public void u(Map<String, String> map) {
        b((Runnable) new d(map));
    }
}
