package com.google.android.gms.analytics;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.RemoteException;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.internal.ha;
import com.google.android.gms.internal.hb;
import java.util.List;
import java.util.Map;

class c implements b {
    /* access modifiers changed from: private */
    public Context mContext;
    /* access modifiers changed from: private */
    public ServiceConnection xV;
    /* access modifiers changed from: private */
    public b xW;
    /* access modifiers changed from: private */
    public C0000c xX;
    /* access modifiers changed from: private */
    public hb xY;

    final class a implements ServiceConnection {
        a() {
        }

        public void onServiceConnected(ComponentName component, IBinder binder) {
            ae.V("service connected, binder: " + binder);
            try {
                if ("com.google.android.gms.analytics.internal.IAnalyticsService".equals(binder.getInterfaceDescriptor())) {
                    ae.V("bound to service");
                    c.this.xY = com.google.android.gms.internal.hb.a.D(binder);
                    c.this.dT();
                    return;
                }
            } catch (RemoteException e) {
            }
            try {
                c.this.mContext.unbindService(this);
            } catch (IllegalArgumentException e2) {
            }
            c.this.xV = null;
            c.this.xX.a(2, null);
        }

        public void onServiceDisconnected(ComponentName component) {
            ae.V("service disconnected: " + component);
            c.this.xV = null;
            c.this.xW.onDisconnected();
        }
    }

    public interface b {
        void onConnected();

        void onDisconnected();
    }

    /* renamed from: com.google.android.gms.analytics.c$c reason: collision with other inner class name */
    public interface C0000c {
        void a(int i, Intent intent);
    }

    public c(Context context, b bVar, C0000c cVar) {
        this.mContext = context;
        if (bVar == null) {
            throw new IllegalArgumentException("onConnectedListener cannot be null");
        }
        this.xW = bVar;
        if (cVar == null) {
            throw new IllegalArgumentException("onConnectionFailedListener cannot be null");
        }
        this.xX = cVar;
    }

    private hb dR() {
        dS();
        return this.xY;
    }

    /* access modifiers changed from: private */
    public void dT() {
        dU();
    }

    private void dU() {
        this.xW.onConnected();
    }

    public void a(Map<String, String> map, long j, String str, List<ha> list) {
        try {
            dR().a(map, j, str, list);
        } catch (RemoteException e) {
            ae.T("sendHit failed: " + e);
        }
    }

    public void connect() {
        Intent intent = new Intent("com.google.android.gms.analytics.service.START");
        intent.setComponent(new ComponentName(GooglePlayServicesUtil.GOOGLE_PLAY_SERVICES_PACKAGE, "com.google.android.gms.analytics.service.AnalyticsService"));
        intent.putExtra("app_package_name", this.mContext.getPackageName());
        if (this.xV != null) {
            ae.T("Calling connect() while still connected, missing disconnect().");
            return;
        }
        this.xV = new a();
        boolean bindService = this.mContext.bindService(intent, this.xV, 129);
        ae.V("connect: bindService returned " + bindService + " for " + intent);
        if (!bindService) {
            this.xV = null;
            this.xX.a(1, null);
        }
    }

    public void dQ() {
        try {
            dR().dQ();
        } catch (RemoteException e) {
            ae.T("clear hits failed: " + e);
        }
    }

    /* access modifiers changed from: protected */
    public void dS() {
        if (!isConnected()) {
            throw new IllegalStateException("Not connected. Call connect() and wait for onConnected() to be called.");
        }
    }

    public void disconnect() {
        this.xY = null;
        if (this.xV != null) {
            try {
                this.mContext.unbindService(this.xV);
            } catch (IllegalArgumentException | IllegalStateException e) {
            }
            this.xV = null;
            this.xW.onDisconnected();
        }
    }

    public boolean isConnected() {
        return this.xY != null;
    }
}
