package com.google.android.gms.dynamic;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.dynamic.LifecycleDelegate;
import java.util.Iterator;
import java.util.LinkedList;

public abstract class a<T extends LifecycleDelegate> {
    /* access modifiers changed from: private */
    public T Tu;
    /* access modifiers changed from: private */
    public Bundle Tv;
    /* access modifiers changed from: private */
    public LinkedList<C0022a> Tw;
    private final f<T> Tx = new f<T>() {
        public void a(T t) {
            a.this.Tu = t;
            Iterator it = a.this.Tw.iterator();
            while (it.hasNext()) {
                ((C0022a) it.next()).b(a.this.Tu);
            }
            a.this.Tw.clear();
            a.this.Tv = null;
        }
    };

    /* renamed from: com.google.android.gms.dynamic.a$a reason: collision with other inner class name */
    private interface C0022a {
        void b(LifecycleDelegate lifecycleDelegate);

        int getState();
    }

    private void a(Bundle bundle, C0022a aVar) {
        if (this.Tu != null) {
            aVar.b(this.Tu);
            return;
        }
        if (this.Tw == null) {
            this.Tw = new LinkedList<>();
        }
        this.Tw.add(aVar);
        if (bundle != null) {
            if (this.Tv == null) {
                this.Tv = (Bundle) bundle.clone();
            } else {
                this.Tv.putAll(bundle);
            }
        }
        a(this.Tx);
    }

    public static void b(FrameLayout frameLayout) {
        final Context context = frameLayout.getContext();
        final int isGooglePlayServicesAvailable = GooglePlayServicesUtil.isGooglePlayServicesAvailable(context);
        String b = GooglePlayServicesUtil.b(context, isGooglePlayServicesAvailable);
        String c = GooglePlayServicesUtil.c(context, isGooglePlayServicesAvailable);
        LinearLayout linearLayout = new LinearLayout(frameLayout.getContext());
        linearLayout.setOrientation(1);
        linearLayout.setLayoutParams(new LayoutParams(-2, -2));
        frameLayout.addView(linearLayout);
        TextView textView = new TextView(frameLayout.getContext());
        textView.setLayoutParams(new LayoutParams(-2, -2));
        textView.setText(b);
        linearLayout.addView(textView);
        if (c != null) {
            Button button = new Button(context);
            button.setLayoutParams(new LayoutParams(-2, -2));
            button.setText(c);
            linearLayout.addView(button);
            button.setOnClickListener(new OnClickListener() {
                public void onClick(View v) {
                    context.startActivity(GooglePlayServicesUtil.aj(isGooglePlayServicesAvailable));
                }
            });
        }
    }

    private void cG(int i) {
        while (!this.Tw.isEmpty() && ((C0022a) this.Tw.getLast()).getState() >= i) {
            this.Tw.removeLast();
        }
    }

    /* access modifiers changed from: protected */
    public void a(FrameLayout frameLayout) {
        b(frameLayout);
    }

    /* access modifiers changed from: protected */
    public abstract void a(f<T> fVar);

    public T je() {
        return this.Tu;
    }

    public void onCreate(final Bundle savedInstanceState) {
        a(savedInstanceState, (C0022a) new C0022a() {
            public void b(LifecycleDelegate lifecycleDelegate) {
                a.this.Tu.onCreate(savedInstanceState);
            }

            public int getState() {
                return 1;
            }
        });
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final FrameLayout frameLayout = new FrameLayout(inflater.getContext());
        final LayoutInflater layoutInflater = inflater;
        final ViewGroup viewGroup = container;
        final Bundle bundle = savedInstanceState;
        a(savedInstanceState, (C0022a) new C0022a() {
            public void b(LifecycleDelegate lifecycleDelegate) {
                frameLayout.removeAllViews();
                frameLayout.addView(a.this.Tu.onCreateView(layoutInflater, viewGroup, bundle));
            }

            public int getState() {
                return 2;
            }
        });
        if (this.Tu == null) {
            a(frameLayout);
        }
        return frameLayout;
    }

    public void onDestroy() {
        if (this.Tu != null) {
            this.Tu.onDestroy();
        } else {
            cG(1);
        }
    }

    public void onDestroyView() {
        if (this.Tu != null) {
            this.Tu.onDestroyView();
        } else {
            cG(2);
        }
    }

    public void onInflate(final Activity activity, final Bundle attrs, final Bundle savedInstanceState) {
        a(savedInstanceState, (C0022a) new C0022a() {
            public void b(LifecycleDelegate lifecycleDelegate) {
                a.this.Tu.onInflate(activity, attrs, savedInstanceState);
            }

            public int getState() {
                return 0;
            }
        });
    }

    public void onLowMemory() {
        if (this.Tu != null) {
            this.Tu.onLowMemory();
        }
    }

    public void onPause() {
        if (this.Tu != null) {
            this.Tu.onPause();
        } else {
            cG(5);
        }
    }

    public void onResume() {
        a((Bundle) null, (C0022a) new C0022a() {
            public void b(LifecycleDelegate lifecycleDelegate) {
                a.this.Tu.onResume();
            }

            public int getState() {
                return 5;
            }
        });
    }

    public void onSaveInstanceState(Bundle outState) {
        if (this.Tu != null) {
            this.Tu.onSaveInstanceState(outState);
        } else if (this.Tv != null) {
            outState.putAll(this.Tv);
        }
    }

    public void onStart() {
        a((Bundle) null, (C0022a) new C0022a() {
            public void b(LifecycleDelegate lifecycleDelegate) {
                a.this.Tu.onStart();
            }

            public int getState() {
                return 4;
            }
        });
    }

    public void onStop() {
        if (this.Tu != null) {
            this.Tu.onStop();
        } else {
            cG(4);
        }
    }
}
