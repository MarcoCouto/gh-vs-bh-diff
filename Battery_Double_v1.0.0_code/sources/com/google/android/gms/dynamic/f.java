package com.google.android.gms.dynamic;

import com.google.android.gms.dynamic.LifecycleDelegate;

public interface f<T extends LifecycleDelegate> {
    void a(T t);
}
