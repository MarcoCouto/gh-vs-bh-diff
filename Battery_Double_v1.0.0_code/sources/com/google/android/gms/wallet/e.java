package com.google.android.gms.wallet;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.a.C0004a;
import com.google.android.gms.common.internal.safeparcel.b;

public class e implements Creator<d> {
    static void a(d dVar, Parcel parcel, int i) {
        int H = b.H(parcel);
        b.c(parcel, 1, dVar.getVersionCode());
        b.a(parcel, 2, (Parcelable) dVar.auJ, i, false);
        b.a(parcel, 3, (Parcelable) dVar.auK, i, false);
        b.H(parcel, H);
    }

    /* renamed from: dM */
    public d createFromParcel(Parcel parcel) {
        OfferWalletObject offerWalletObject;
        LoyaltyWalletObject loyaltyWalletObject;
        int i;
        OfferWalletObject offerWalletObject2 = null;
        int G = a.G(parcel);
        int i2 = 0;
        LoyaltyWalletObject loyaltyWalletObject2 = null;
        while (parcel.dataPosition() < G) {
            int F = a.F(parcel);
            switch (a.aH(F)) {
                case 1:
                    OfferWalletObject offerWalletObject3 = offerWalletObject2;
                    loyaltyWalletObject = loyaltyWalletObject2;
                    i = a.g(parcel, F);
                    offerWalletObject = offerWalletObject3;
                    break;
                case 2:
                    i = i2;
                    LoyaltyWalletObject loyaltyWalletObject3 = (LoyaltyWalletObject) a.a(parcel, F, LoyaltyWalletObject.CREATOR);
                    offerWalletObject = offerWalletObject2;
                    loyaltyWalletObject = loyaltyWalletObject3;
                    break;
                case 3:
                    offerWalletObject = (OfferWalletObject) a.a(parcel, F, OfferWalletObject.CREATOR);
                    loyaltyWalletObject = loyaltyWalletObject2;
                    i = i2;
                    break;
                default:
                    a.b(parcel, F);
                    offerWalletObject = offerWalletObject2;
                    loyaltyWalletObject = loyaltyWalletObject2;
                    i = i2;
                    break;
            }
            i2 = i;
            loyaltyWalletObject2 = loyaltyWalletObject;
            offerWalletObject2 = offerWalletObject;
        }
        if (parcel.dataPosition() == G) {
            return new d(i2, loyaltyWalletObject2, offerWalletObject2);
        }
        throw new C0004a("Overread allowed size end=" + G, parcel);
    }

    /* renamed from: fT */
    public d[] newArray(int i) {
        return new d[i];
    }
}
