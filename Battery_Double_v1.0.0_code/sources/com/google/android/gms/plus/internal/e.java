package com.google.android.gms.plus.internal;

import android.app.PendingIntent;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Looper;
import android.os.RemoteException;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.internal.jd;
import com.google.android.gms.internal.jj;
import com.google.android.gms.internal.jl;
import com.google.android.gms.internal.jq;
import com.google.android.gms.internal.jr;
import com.google.android.gms.internal.js;
import com.google.android.gms.internal.jt;
import com.google.android.gms.internal.ky;
import com.google.android.gms.internal.lm;
import com.google.android.gms.internal.pf;
import com.google.android.gms.internal.pi;
import com.google.android.gms.plus.Moments.LoadMomentsResult;
import com.google.android.gms.plus.People.LoadPeopleResult;
import com.google.android.gms.plus.model.moments.Moment;
import com.google.android.gms.plus.model.moments.MomentBuffer;
import com.google.android.gms.plus.model.people.Person;
import com.google.android.gms.plus.model.people.PersonBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class e extends jl<d> {
    private Person anG;
    private final h anH;

    final class a extends a {
        private final com.google.android.gms.common.api.BaseImplementation.b<Status> anI;

        public a(com.google.android.gms.common.api.BaseImplementation.b<Status> bVar) {
            this.anI = bVar;
        }

        public void aA(Status status) {
            e.this.a((b<?>) new d<Object>(this.anI, status));
        }
    }

    final class b extends a {
        private final com.google.android.gms.common.api.BaseImplementation.b<LoadMomentsResult> anI;

        public b(com.google.android.gms.common.api.BaseImplementation.b<LoadMomentsResult> bVar) {
            this.anI = bVar;
        }

        public void a(DataHolder dataHolder, String str, String str2) {
            DataHolder dataHolder2;
            Status status = new Status(dataHolder.getStatusCode(), null, dataHolder.gV() != null ? (PendingIntent) dataHolder.gV().getParcelable("pendingIntent") : null);
            if (status.isSuccess() || dataHolder == null) {
                dataHolder2 = dataHolder;
            } else {
                if (!dataHolder.isClosed()) {
                    dataHolder.close();
                }
                dataHolder2 = null;
            }
            e.this.a((b<?>) new c<Object>(this.anI, status, dataHolder2, str, str2));
        }
    }

    final class c extends com.google.android.gms.internal.jl.d<com.google.android.gms.common.api.BaseImplementation.b<LoadMomentsResult>> implements LoadMomentsResult {
        private final Status Eb;
        private final String OB;
        private final String anK;
        private MomentBuffer anL;

        public c(com.google.android.gms.common.api.BaseImplementation.b<LoadMomentsResult> bVar, Status status, DataHolder dataHolder, String str, String str2) {
            super(bVar, dataHolder);
            this.Eb = status;
            this.OB = str;
            this.anK = str2;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void b(com.google.android.gms.common.api.BaseImplementation.b<LoadMomentsResult> bVar, DataHolder dataHolder) {
            this.anL = dataHolder != null ? new MomentBuffer(dataHolder) : null;
            bVar.b(this);
        }

        public MomentBuffer getMomentBuffer() {
            return this.anL;
        }

        public String getNextPageToken() {
            return this.OB;
        }

        public Status getStatus() {
            return this.Eb;
        }

        public String getUpdated() {
            return this.anK;
        }

        public void release() {
            if (this.anL != null) {
                this.anL.close();
            }
        }
    }

    final class d extends b<com.google.android.gms.common.api.BaseImplementation.b<Status>> {
        private final Status Eb;

        public d(com.google.android.gms.common.api.BaseImplementation.b<Status> bVar, Status status) {
            super(bVar);
            this.Eb = status;
        }

        /* access modifiers changed from: protected */
        public void hx() {
        }

        /* access modifiers changed from: protected */
        /* renamed from: m */
        public void g(com.google.android.gms.common.api.BaseImplementation.b<Status> bVar) {
            if (bVar != null) {
                bVar.b(this.Eb);
            }
        }
    }

    /* renamed from: com.google.android.gms.plus.internal.e$e reason: collision with other inner class name */
    final class C0138e extends a {
        private final com.google.android.gms.common.api.BaseImplementation.b<LoadPeopleResult> anI;

        public C0138e(com.google.android.gms.common.api.BaseImplementation.b<LoadPeopleResult> bVar) {
            this.anI = bVar;
        }

        public void a(DataHolder dataHolder, String str) {
            DataHolder dataHolder2;
            Status status = new Status(dataHolder.getStatusCode(), null, dataHolder.gV() != null ? (PendingIntent) dataHolder.gV().getParcelable("pendingIntent") : null);
            if (status.isSuccess() || dataHolder == null) {
                dataHolder2 = dataHolder;
            } else {
                if (!dataHolder.isClosed()) {
                    dataHolder.close();
                }
                dataHolder2 = null;
            }
            e.this.a((b<?>) new f<Object>(this.anI, status, dataHolder2, str));
        }
    }

    final class f extends com.google.android.gms.internal.jl.d<com.google.android.gms.common.api.BaseImplementation.b<LoadPeopleResult>> implements LoadPeopleResult {
        private final Status Eb;
        private final String OB;
        private PersonBuffer anM;

        public f(com.google.android.gms.common.api.BaseImplementation.b<LoadPeopleResult> bVar, Status status, DataHolder dataHolder, String str) {
            super(bVar, dataHolder);
            this.Eb = status;
            this.OB = str;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void b(com.google.android.gms.common.api.BaseImplementation.b<LoadPeopleResult> bVar, DataHolder dataHolder) {
            this.anM = dataHolder != null ? new PersonBuffer(dataHolder) : null;
            bVar.b(this);
        }

        public String getNextPageToken() {
            return this.OB;
        }

        public PersonBuffer getPersonBuffer() {
            return this.anM;
        }

        public Status getStatus() {
            return this.Eb;
        }

        public void release() {
            if (this.anM != null) {
                this.anM.close();
            }
        }
    }

    final class g extends a {
        private final com.google.android.gms.common.api.BaseImplementation.b<Status> anI;

        public g(com.google.android.gms.common.api.BaseImplementation.b<Status> bVar) {
            this.anI = bVar;
        }

        public void h(int i, Bundle bundle) {
            e.this.a((b<?>) new h<Object>(this.anI, new Status(i, null, bundle != null ? (PendingIntent) bundle.getParcelable("pendingIntent") : null)));
        }
    }

    final class h extends b<com.google.android.gms.common.api.BaseImplementation.b<Status>> {
        private final Status Eb;

        public h(com.google.android.gms.common.api.BaseImplementation.b<Status> bVar, Status status) {
            super(bVar);
            this.Eb = status;
        }

        /* access modifiers changed from: protected */
        public void hx() {
        }

        /* access modifiers changed from: protected */
        /* renamed from: m */
        public void g(com.google.android.gms.common.api.BaseImplementation.b<Status> bVar) {
            e.this.disconnect();
            if (bVar != null) {
                bVar.b(this.Eb);
            }
        }
    }

    public e(Context context, Looper looper, ConnectionCallbacks connectionCallbacks, OnConnectionFailedListener onConnectionFailedListener, h hVar) {
        super(context, looper, connectionCallbacks, onConnectionFailedListener, hVar.oz());
        this.anH = hVar;
    }

    public jr a(com.google.android.gms.common.api.BaseImplementation.b<LoadPeopleResult> bVar, int i, String str) {
        dS();
        C0138e eVar = new C0138e(bVar);
        try {
            return ((d) hw()).a(eVar, 1, i, -1, str);
        } catch (RemoteException e) {
            eVar.a(DataHolder.av(8), null);
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public void a(int i, IBinder iBinder, Bundle bundle) {
        if (i == 0 && bundle != null && bundle.containsKey("loaded_person")) {
            this.anG = pi.i(bundle.getByteArray("loaded_person"));
        }
        super.a(i, iBinder, bundle);
    }

    public void a(com.google.android.gms.common.api.BaseImplementation.b<LoadMomentsResult> bVar, int i, String str, Uri uri, String str2, String str3) {
        dS();
        b bVar2 = bVar != null ? new b(bVar) : null;
        try {
            ((d) hw()).a(bVar2, i, str, uri, str2, str3);
        } catch (RemoteException e) {
            bVar2.a(DataHolder.av(8), null, null);
        }
    }

    public void a(com.google.android.gms.common.api.BaseImplementation.b<Status> bVar, Moment moment) {
        dS();
        a aVar = bVar != null ? new a(bVar) : null;
        try {
            ((d) hw()).a((b) aVar, ky.a((pf) moment));
        } catch (RemoteException e) {
            if (aVar == null) {
                throw new IllegalStateException(e);
            }
            aVar.aA(new Status(8, null, null));
        }
    }

    public void a(com.google.android.gms.common.api.BaseImplementation.b<LoadPeopleResult> bVar, Collection<String> collection) {
        dS();
        C0138e eVar = new C0138e(bVar);
        try {
            ((d) hw()).a((b) eVar, (List<String>) new ArrayList<String>(collection));
        } catch (RemoteException e) {
            eVar.a(DataHolder.av(8), null);
        }
    }

    /* access modifiers changed from: protected */
    public void a(jt jtVar, com.google.android.gms.internal.jl.e eVar) throws RemoteException {
        Bundle oH = this.anH.oH();
        oH.putStringArray("request_visible_actions", this.anH.oA());
        oH.putString("auth_package", this.anH.oC());
        jtVar.a((js) eVar, new jj(2).aX(this.anH.oD()).a((jq) jd.aT(this.anH.getAccountName())).a(lm.d(hv())).e(oH));
    }

    /* access modifiers changed from: protected */
    public String bK() {
        return "com.google.android.gms.plus.service.START";
    }

    /* access modifiers changed from: protected */
    public String bL() {
        return "com.google.android.gms.plus.internal.IPlusService";
    }

    /* access modifiers changed from: protected */
    /* renamed from: bN */
    public d l(IBinder iBinder) {
        return com.google.android.gms.plus.internal.d.a.bM(iBinder);
    }

    public boolean cj(String str) {
        return Arrays.asList(hv()).contains(str);
    }

    public void clearDefaultAccount() {
        dS();
        try {
            this.anG = null;
            ((d) hw()).clearDefaultAccount();
        } catch (RemoteException e) {
            throw new IllegalStateException(e);
        }
    }

    public void d(com.google.android.gms.common.api.BaseImplementation.b<LoadPeopleResult> bVar, String[] strArr) {
        a(bVar, (Collection<String>) Arrays.asList(strArr));
    }

    public String getAccountName() {
        dS();
        try {
            return ((d) hw()).getAccountName();
        } catch (RemoteException e) {
            throw new IllegalStateException(e);
        }
    }

    public Person getCurrentPerson() {
        dS();
        return this.anG;
    }

    public void j(com.google.android.gms.common.api.BaseImplementation.b<LoadMomentsResult> bVar) {
        a(bVar, 20, null, null, null, "me");
    }

    public void k(com.google.android.gms.common.api.BaseImplementation.b<LoadPeopleResult> bVar) {
        dS();
        C0138e eVar = new C0138e(bVar);
        try {
            ((d) hw()).a(eVar, 2, 1, -1, null);
        } catch (RemoteException e) {
            eVar.a(DataHolder.av(8), null);
        }
    }

    public void l(com.google.android.gms.common.api.BaseImplementation.b<Status> bVar) {
        dS();
        clearDefaultAccount();
        g gVar = new g(bVar);
        try {
            ((d) hw()).b(gVar);
        } catch (RemoteException e) {
            gVar.h(8, null);
        }
    }

    public jr q(com.google.android.gms.common.api.BaseImplementation.b<LoadPeopleResult> bVar, String str) {
        return a(bVar, 0, str);
    }

    public void removeMoment(String momentId) {
        dS();
        try {
            ((d) hw()).removeMoment(momentId);
        } catch (RemoteException e) {
            throw new IllegalStateException(e);
        }
    }
}
