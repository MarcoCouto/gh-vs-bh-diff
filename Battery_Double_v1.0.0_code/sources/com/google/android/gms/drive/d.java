package com.google.android.gms.drive;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.a.C0004a;
import com.google.android.gms.common.internal.safeparcel.b;

public class d implements Creator<DrivePreferences> {
    static void a(DrivePreferences drivePreferences, Parcel parcel, int i) {
        int H = b.H(parcel);
        b.c(parcel, 1, drivePreferences.CK);
        b.a(parcel, 2, drivePreferences.Ox);
        b.H(parcel, H);
    }

    /* renamed from: T */
    public DrivePreferences createFromParcel(Parcel parcel) {
        boolean z = false;
        int G = a.G(parcel);
        int i = 0;
        while (parcel.dataPosition() < G) {
            int F = a.F(parcel);
            switch (a.aH(F)) {
                case 1:
                    i = a.g(parcel, F);
                    break;
                case 2:
                    z = a.c(parcel, F);
                    break;
                default:
                    a.b(parcel, F);
                    break;
            }
        }
        if (parcel.dataPosition() == G) {
            return new DrivePreferences(i, z);
        }
        throw new C0004a("Overread allowed size end=" + G, parcel);
    }

    /* renamed from: aZ */
    public DrivePreferences[] newArray(int i) {
        return new DrivePreferences[i];
    }
}
