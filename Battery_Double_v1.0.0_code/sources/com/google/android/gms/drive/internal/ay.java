package com.google.android.gms.drive.internal;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.a.C0004a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.drive.DriveId;

public class ay implements Creator<OpenContentsRequest> {
    static void a(OpenContentsRequest openContentsRequest, Parcel parcel, int i) {
        int H = b.H(parcel);
        b.c(parcel, 1, openContentsRequest.CK);
        b.a(parcel, 2, (Parcelable) openContentsRequest.Pp, i, false);
        b.c(parcel, 3, openContentsRequest.Oi);
        b.c(parcel, 4, openContentsRequest.QT);
        b.H(parcel, H);
    }

    /* renamed from: aD */
    public OpenContentsRequest createFromParcel(Parcel parcel) {
        int g;
        int i;
        DriveId driveId;
        int i2;
        int i3 = 0;
        int G = a.G(parcel);
        DriveId driveId2 = null;
        int i4 = 0;
        int i5 = 0;
        while (parcel.dataPosition() < G) {
            int F = a.F(parcel);
            switch (a.aH(F)) {
                case 1:
                    int i6 = i3;
                    i = i4;
                    driveId = driveId2;
                    i2 = a.g(parcel, F);
                    g = i6;
                    break;
                case 2:
                    i2 = i5;
                    int i7 = i4;
                    driveId = (DriveId) a.a(parcel, F, DriveId.CREATOR);
                    g = i3;
                    i = i7;
                    break;
                case 3:
                    driveId = driveId2;
                    i2 = i5;
                    int i8 = i3;
                    i = a.g(parcel, F);
                    g = i8;
                    break;
                case 4:
                    g = a.g(parcel, F);
                    i = i4;
                    driveId = driveId2;
                    i2 = i5;
                    break;
                default:
                    a.b(parcel, F);
                    g = i3;
                    i = i4;
                    driveId = driveId2;
                    i2 = i5;
                    break;
            }
            i5 = i2;
            driveId2 = driveId;
            i4 = i;
            i3 = g;
        }
        if (parcel.dataPosition() == G) {
            return new OpenContentsRequest(i5, driveId2, i4, i3);
        }
        throw new C0004a("Overread allowed size end=" + G, parcel);
    }

    /* renamed from: bS */
    public OpenContentsRequest[] newArray(int i) {
        return new OpenContentsRequest[i];
    }
}
