package com.google.android.gms.drive.internal;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.a.C0004a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.drive.events.ChangeEvent;
import com.google.android.gms.drive.events.CompletionEvent;

public class aq implements Creator<OnEventResponse> {
    static void a(OnEventResponse onEventResponse, Parcel parcel, int i) {
        int H = b.H(parcel);
        b.c(parcel, 1, onEventResponse.CK);
        b.c(parcel, 2, onEventResponse.Pm);
        b.a(parcel, 3, (Parcelable) onEventResponse.QO, i, false);
        b.a(parcel, 5, (Parcelable) onEventResponse.QP, i, false);
        b.H(parcel, H);
    }

    /* renamed from: av */
    public OnEventResponse createFromParcel(Parcel parcel) {
        CompletionEvent completionEvent;
        ChangeEvent changeEvent;
        int i;
        int i2;
        CompletionEvent completionEvent2 = null;
        int i3 = 0;
        int G = a.G(parcel);
        ChangeEvent changeEvent2 = null;
        int i4 = 0;
        while (parcel.dataPosition() < G) {
            int F = a.F(parcel);
            switch (a.aH(F)) {
                case 1:
                    CompletionEvent completionEvent3 = completionEvent2;
                    changeEvent = changeEvent2;
                    i = i3;
                    i2 = a.g(parcel, F);
                    completionEvent = completionEvent3;
                    break;
                case 2:
                    i2 = i4;
                    ChangeEvent changeEvent3 = changeEvent2;
                    i = a.g(parcel, F);
                    completionEvent = completionEvent2;
                    changeEvent = changeEvent3;
                    break;
                case 3:
                    i = i3;
                    i2 = i4;
                    CompletionEvent completionEvent4 = completionEvent2;
                    changeEvent = (ChangeEvent) a.a(parcel, F, ChangeEvent.CREATOR);
                    completionEvent = completionEvent4;
                    break;
                case 5:
                    completionEvent = (CompletionEvent) a.a(parcel, F, CompletionEvent.CREATOR);
                    changeEvent = changeEvent2;
                    i = i3;
                    i2 = i4;
                    break;
                default:
                    a.b(parcel, F);
                    completionEvent = completionEvent2;
                    changeEvent = changeEvent2;
                    i = i3;
                    i2 = i4;
                    break;
            }
            i4 = i2;
            i3 = i;
            changeEvent2 = changeEvent;
            completionEvent2 = completionEvent;
        }
        if (parcel.dataPosition() == G) {
            return new OnEventResponse(i4, i3, changeEvent2, completionEvent2);
        }
        throw new C0004a("Overread allowed size end=" + G, parcel);
    }

    /* renamed from: bK */
    public OnEventResponse[] newArray(int i) {
        return new OnEventResponse[i];
    }
}
