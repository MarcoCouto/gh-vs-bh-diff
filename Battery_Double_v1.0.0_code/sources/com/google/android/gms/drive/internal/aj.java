package com.google.android.gms.drive.internal;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.a.C0004a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.drive.DriveId;

public class aj implements Creator<LoadRealtimeRequest> {
    static void a(LoadRealtimeRequest loadRealtimeRequest, Parcel parcel, int i) {
        int H = b.H(parcel);
        b.c(parcel, 1, loadRealtimeRequest.CK);
        b.a(parcel, 2, (Parcelable) loadRealtimeRequest.Oj, i, false);
        b.a(parcel, 3, loadRealtimeRequest.QF);
        b.H(parcel, H);
    }

    /* renamed from: ap */
    public LoadRealtimeRequest createFromParcel(Parcel parcel) {
        boolean c;
        DriveId driveId;
        int i;
        boolean z = false;
        int G = a.G(parcel);
        DriveId driveId2 = null;
        int i2 = 0;
        while (parcel.dataPosition() < G) {
            int F = a.F(parcel);
            switch (a.aH(F)) {
                case 1:
                    boolean z2 = z;
                    driveId = driveId2;
                    i = a.g(parcel, F);
                    c = z2;
                    break;
                case 2:
                    i = i2;
                    DriveId driveId3 = (DriveId) a.a(parcel, F, DriveId.CREATOR);
                    c = z;
                    driveId = driveId3;
                    break;
                case 3:
                    c = a.c(parcel, F);
                    driveId = driveId2;
                    i = i2;
                    break;
                default:
                    a.b(parcel, F);
                    c = z;
                    driveId = driveId2;
                    i = i2;
                    break;
            }
            i2 = i;
            driveId2 = driveId;
            z = c;
        }
        if (parcel.dataPosition() == G) {
            return new LoadRealtimeRequest(i2, driveId2, z);
        }
        throw new C0004a("Overread allowed size end=" + G, parcel);
    }

    /* renamed from: bE */
    public LoadRealtimeRequest[] newArray(int i) {
        return new LoadRealtimeRequest[i];
    }
}
