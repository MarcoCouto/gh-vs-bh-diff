package com.google.android.gms.drive.internal;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.a.C0004a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.drive.DrivePreferences;

public class ap implements Creator<OnDrivePreferencesResponse> {
    static void a(OnDrivePreferencesResponse onDrivePreferencesResponse, Parcel parcel, int i) {
        int H = b.H(parcel);
        b.c(parcel, 1, onDrivePreferencesResponse.CK);
        b.a(parcel, 2, (Parcelable) onDrivePreferencesResponse.QN, i, false);
        b.H(parcel, H);
    }

    /* renamed from: au */
    public OnDrivePreferencesResponse createFromParcel(Parcel parcel) {
        int G = a.G(parcel);
        int i = 0;
        DrivePreferences drivePreferences = null;
        while (parcel.dataPosition() < G) {
            int F = a.F(parcel);
            switch (a.aH(F)) {
                case 1:
                    i = a.g(parcel, F);
                    break;
                case 2:
                    drivePreferences = (DrivePreferences) a.a(parcel, F, DrivePreferences.CREATOR);
                    break;
                default:
                    a.b(parcel, F);
                    break;
            }
        }
        if (parcel.dataPosition() == G) {
            return new OnDrivePreferencesResponse(i, drivePreferences);
        }
        throw new C0004a("Overread allowed size end=" + G, parcel);
    }

    /* renamed from: bJ */
    public OnDrivePreferencesResponse[] newArray(int i) {
        return new OnDrivePreferencesResponse[i];
    }
}
