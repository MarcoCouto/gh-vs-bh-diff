package com.google.android.gms.drive.internal;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.a.C0004a;
import com.google.android.gms.common.internal.safeparcel.b;

public class ar implements Creator<OnListEntriesResponse> {
    static void a(OnListEntriesResponse onListEntriesResponse, Parcel parcel, int i) {
        int H = b.H(parcel);
        b.c(parcel, 1, onListEntriesResponse.CK);
        b.a(parcel, 2, (Parcelable) onListEntriesResponse.QQ, i, false);
        b.a(parcel, 3, onListEntriesResponse.PJ);
        b.H(parcel, H);
    }

    /* renamed from: aw */
    public OnListEntriesResponse createFromParcel(Parcel parcel) {
        boolean c;
        DataHolder dataHolder;
        int i;
        boolean z = false;
        int G = a.G(parcel);
        DataHolder dataHolder2 = null;
        int i2 = 0;
        while (parcel.dataPosition() < G) {
            int F = a.F(parcel);
            switch (a.aH(F)) {
                case 1:
                    boolean z2 = z;
                    dataHolder = dataHolder2;
                    i = a.g(parcel, F);
                    c = z2;
                    break;
                case 2:
                    i = i2;
                    DataHolder dataHolder3 = (DataHolder) a.a(parcel, F, (Creator<T>) DataHolder.CREATOR);
                    c = z;
                    dataHolder = dataHolder3;
                    break;
                case 3:
                    c = a.c(parcel, F);
                    dataHolder = dataHolder2;
                    i = i2;
                    break;
                default:
                    a.b(parcel, F);
                    c = z;
                    dataHolder = dataHolder2;
                    i = i2;
                    break;
            }
            i2 = i;
            dataHolder2 = dataHolder;
            z = c;
        }
        if (parcel.dataPosition() == G) {
            return new OnListEntriesResponse(i2, dataHolder2, z);
        }
        throw new C0004a("Overread allowed size end=" + G, parcel);
    }

    /* renamed from: bL */
    public OnListEntriesResponse[] newArray(int i) {
        return new OnListEntriesResponse[i];
    }
}
