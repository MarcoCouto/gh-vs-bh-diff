package com.google.android.gms.drive.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.drive.DrivePreferencesApi;
import com.google.android.gms.drive.DrivePreferencesApi.FileUploadPreferencesResult;
import com.google.android.gms.drive.FileUploadPreferences;

public class x implements DrivePreferencesApi {

    private class a extends c {
        private final com.google.android.gms.common.api.BaseImplementation.b<FileUploadPreferencesResult> Ea;

        private a(com.google.android.gms.common.api.BaseImplementation.b<FileUploadPreferencesResult> bVar) {
            this.Ea = bVar;
        }

        public void a(OnDeviceUsagePreferenceResponse onDeviceUsagePreferenceResponse) throws RemoteException {
            this.Ea.b(new b(Status.Kw, onDeviceUsagePreferenceResponse.iN()));
        }

        public void n(Status status) throws RemoteException {
            this.Ea.b(new b(status, null));
        }
    }

    private class b implements FileUploadPreferencesResult {
        private final Status Eb;
        private final FileUploadPreferences Qs;

        private b(Status status, FileUploadPreferences fileUploadPreferences) {
            this.Eb = status;
            this.Qs = fileUploadPreferences;
        }

        public FileUploadPreferences getFileUploadPreferences() {
            return this.Qs;
        }

        public Status getStatus() {
            return this.Eb;
        }
    }

    private abstract class c extends q<FileUploadPreferencesResult> {
        public c(GoogleApiClient googleApiClient) {
            super(googleApiClient);
        }

        /* access modifiers changed from: protected */
        /* renamed from: t */
        public FileUploadPreferencesResult c(Status status) {
            return new b(status, null);
        }
    }

    public PendingResult<FileUploadPreferencesResult> getFileUploadPreferences(GoogleApiClient apiClient) {
        return apiClient.a(new c(apiClient) {
            /* access modifiers changed from: protected */
            public void a(r rVar) throws RemoteException {
                rVar.iG().h(new a(this));
            }
        });
    }

    public PendingResult<Status> setFileUploadPreferences(GoogleApiClient apiClient, FileUploadPreferences fileUploadPreferences) {
        if (!(fileUploadPreferences instanceof FileUploadPreferencesImpl)) {
            throw new IllegalArgumentException("Invalid preference value");
        }
        final FileUploadPreferencesImpl fileUploadPreferencesImpl = (FileUploadPreferencesImpl) fileUploadPreferences;
        return apiClient.b(new a(apiClient) {
            /* access modifiers changed from: protected */
            public void a(r rVar) throws RemoteException {
                rVar.iG().a(new SetFileUploadPreferencesRequest(fileUploadPreferencesImpl), (af) new bg(this));
            }
        });
    }
}
