package com.google.android.gms.drive.internal;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.a.C0004a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.drive.Contents;

public class g implements Creator<CloseContentsRequest> {
    static void a(CloseContentsRequest closeContentsRequest, Parcel parcel, int i) {
        int H = b.H(parcel);
        b.c(parcel, 1, closeContentsRequest.CK);
        b.a(parcel, 2, (Parcelable) closeContentsRequest.Pr, i, false);
        b.a(parcel, 3, closeContentsRequest.Pt, false);
        b.H(parcel, H);
    }

    /* renamed from: ae */
    public CloseContentsRequest createFromParcel(Parcel parcel) {
        Boolean d;
        Contents contents;
        int i;
        Boolean bool = null;
        int G = a.G(parcel);
        int i2 = 0;
        Contents contents2 = null;
        while (parcel.dataPosition() < G) {
            int F = a.F(parcel);
            switch (a.aH(F)) {
                case 1:
                    Boolean bool2 = bool;
                    contents = contents2;
                    i = a.g(parcel, F);
                    d = bool2;
                    break;
                case 2:
                    i = i2;
                    Contents contents3 = (Contents) a.a(parcel, F, Contents.CREATOR);
                    d = bool;
                    contents = contents3;
                    break;
                case 3:
                    d = a.d(parcel, F);
                    contents = contents2;
                    i = i2;
                    break;
                default:
                    a.b(parcel, F);
                    d = bool;
                    contents = contents2;
                    i = i2;
                    break;
            }
            i2 = i;
            contents2 = contents;
            bool = d;
        }
        if (parcel.dataPosition() == G) {
            return new CloseContentsRequest(i2, contents2, bool);
        }
        throw new C0004a("Overread allowed size end=" + G, parcel);
    }

    /* renamed from: bo */
    public CloseContentsRequest[] newArray(int i) {
        return new CloseContentsRequest[i];
    }
}
