package com.google.android.gms.drive.internal;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.a.C0004a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.drive.DriveId;
import java.util.List;

public class bf implements Creator<SetResourceParentsRequest> {
    static void a(SetResourceParentsRequest setResourceParentsRequest, Parcel parcel, int i) {
        int H = b.H(parcel);
        b.c(parcel, 1, setResourceParentsRequest.CK);
        b.a(parcel, 2, (Parcelable) setResourceParentsRequest.QW, i, false);
        b.c(parcel, 3, setResourceParentsRequest.QX, false);
        b.H(parcel, H);
    }

    /* renamed from: aJ */
    public SetResourceParentsRequest createFromParcel(Parcel parcel) {
        List c;
        DriveId driveId;
        int i;
        List list = null;
        int G = a.G(parcel);
        int i2 = 0;
        DriveId driveId2 = null;
        while (parcel.dataPosition() < G) {
            int F = a.F(parcel);
            switch (a.aH(F)) {
                case 1:
                    List list2 = list;
                    driveId = driveId2;
                    i = a.g(parcel, F);
                    c = list2;
                    break;
                case 2:
                    i = i2;
                    DriveId driveId3 = (DriveId) a.a(parcel, F, DriveId.CREATOR);
                    c = list;
                    driveId = driveId3;
                    break;
                case 3:
                    c = a.c(parcel, F, DriveId.CREATOR);
                    driveId = driveId2;
                    i = i2;
                    break;
                default:
                    a.b(parcel, F);
                    c = list;
                    driveId = driveId2;
                    i = i2;
                    break;
            }
            i2 = i;
            driveId2 = driveId;
            list = c;
        }
        if (parcel.dataPosition() == G) {
            return new SetResourceParentsRequest(i2, driveId2, list);
        }
        throw new C0004a("Overread allowed size end=" + G, parcel);
    }

    /* renamed from: bY */
    public SetResourceParentsRequest[] newArray(int i) {
        return new SetResourceParentsRequest[i];
    }
}
