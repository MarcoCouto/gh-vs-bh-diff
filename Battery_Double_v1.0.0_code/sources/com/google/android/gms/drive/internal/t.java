package com.google.android.gms.drive.internal;

import android.os.RemoteException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.d;
import com.google.android.gms.common.api.d.b;
import com.google.android.gms.drive.DriveApi.DriveContentsResult;
import com.google.android.gms.drive.DriveFile;
import com.google.android.gms.drive.DriveFile.DownloadProgressListener;
import com.google.android.gms.drive.DriveId;

public class t extends y implements DriveFile {

    private static class a implements DownloadProgressListener {
        private final d<DownloadProgressListener> Qe;

        public a(d<DownloadProgressListener> dVar) {
            this.Qe = dVar;
        }

        public void onProgress(long bytesDownloaded, long bytesExpected) {
            final long j = bytesDownloaded;
            final long j2 = bytesExpected;
            this.Qe.a(new b<DownloadProgressListener>() {
                /* renamed from: a */
                public void c(DownloadProgressListener downloadProgressListener) {
                    downloadProgressListener.onProgress(j, j2);
                }

                public void gG() {
                }
            });
        }
    }

    public t(DriveId driveId) {
        super(driveId);
    }

    private static DownloadProgressListener a(GoogleApiClient googleApiClient, DownloadProgressListener downloadProgressListener) {
        if (downloadProgressListener == null) {
            return null;
        }
        return new a(googleApiClient.d(downloadProgressListener));
    }

    public PendingResult<DriveContentsResult> open(GoogleApiClient apiClient, final int mode, DownloadProgressListener listener) {
        if (mode == 268435456 || mode == 536870912 || mode == 805306368) {
            final DownloadProgressListener a2 = a(apiClient, listener);
            return apiClient.a(new b(apiClient) {
                /* access modifiers changed from: protected */
                public void a(r rVar) throws RemoteException {
                    rVar.iG().a(new OpenContentsRequest(t.this.getDriveId(), mode, 0), (af) new az(this, a2));
                }
            });
        }
        throw new IllegalArgumentException("Invalid mode provided.");
    }
}
