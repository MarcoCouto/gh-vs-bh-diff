package com.google.android.gms.drive.internal;

import android.content.IntentSender;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.dlten.lib.frmWork.CEventWnd;
import com.google.android.gms.drive.RealtimeDocumentSyncRequest;
import com.google.android.gms.games.Notifications;

public interface ae extends IInterface {

    public static abstract class a extends Binder implements ae {

        /* renamed from: com.google.android.gms.drive.internal.ae$a$a reason: collision with other inner class name */
        private static class C0005a implements ae {
            private IBinder le;

            C0005a(IBinder iBinder) {
                this.le = iBinder;
            }

            public IntentSender a(CreateFileIntentSenderRequest createFileIntentSenderRequest) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.drive.internal.IDriveService");
                    if (createFileIntentSenderRequest != null) {
                        obtain.writeInt(1);
                        createFileIntentSenderRequest.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.le.transact(11, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt() != 0 ? (IntentSender) IntentSender.CREATOR.createFromParcel(obtain2) : null;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public IntentSender a(OpenFileIntentSenderRequest openFileIntentSenderRequest) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.drive.internal.IDriveService");
                    if (openFileIntentSenderRequest != null) {
                        obtain.writeInt(1);
                        openFileIntentSenderRequest.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.le.transact(10, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt() != 0 ? (IntentSender) IntentSender.CREATOR.createFromParcel(obtain2) : null;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(RealtimeDocumentSyncRequest realtimeDocumentSyncRequest, af afVar) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.drive.internal.IDriveService");
                    if (realtimeDocumentSyncRequest != null) {
                        obtain.writeInt(1);
                        realtimeDocumentSyncRequest.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeStrongBinder(afVar != null ? afVar.asBinder() : null);
                    this.le.transact(34, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(AddEventListenerRequest addEventListenerRequest, ag agVar, String str, af afVar) throws RemoteException {
                IBinder iBinder = null;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.drive.internal.IDriveService");
                    if (addEventListenerRequest != null) {
                        obtain.writeInt(1);
                        addEventListenerRequest.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeStrongBinder(agVar != null ? agVar.asBinder() : null);
                    obtain.writeString(str);
                    if (afVar != null) {
                        iBinder = afVar.asBinder();
                    }
                    obtain.writeStrongBinder(iBinder);
                    this.le.transact(14, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(AuthorizeAccessRequest authorizeAccessRequest, af afVar) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.drive.internal.IDriveService");
                    if (authorizeAccessRequest != null) {
                        obtain.writeInt(1);
                        authorizeAccessRequest.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeStrongBinder(afVar != null ? afVar.asBinder() : null);
                    this.le.transact(12, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(CancelPendingActionsRequest cancelPendingActionsRequest, af afVar) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.drive.internal.IDriveService");
                    if (cancelPendingActionsRequest != null) {
                        obtain.writeInt(1);
                        cancelPendingActionsRequest.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeStrongBinder(afVar != null ? afVar.asBinder() : null);
                    this.le.transact(37, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(CheckResourceIdsExistRequest checkResourceIdsExistRequest, af afVar) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.drive.internal.IDriveService");
                    if (checkResourceIdsExistRequest != null) {
                        obtain.writeInt(1);
                        checkResourceIdsExistRequest.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeStrongBinder(afVar != null ? afVar.asBinder() : null);
                    this.le.transact(30, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(CloseContentsAndUpdateMetadataRequest closeContentsAndUpdateMetadataRequest, af afVar) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.drive.internal.IDriveService");
                    if (closeContentsAndUpdateMetadataRequest != null) {
                        obtain.writeInt(1);
                        closeContentsAndUpdateMetadataRequest.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeStrongBinder(afVar != null ? afVar.asBinder() : null);
                    this.le.transact(18, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(CloseContentsRequest closeContentsRequest, af afVar) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.drive.internal.IDriveService");
                    if (closeContentsRequest != null) {
                        obtain.writeInt(1);
                        closeContentsRequest.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeStrongBinder(afVar != null ? afVar.asBinder() : null);
                    this.le.transact(8, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(CreateContentsRequest createContentsRequest, af afVar) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.drive.internal.IDriveService");
                    if (createContentsRequest != null) {
                        obtain.writeInt(1);
                        createContentsRequest.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeStrongBinder(afVar != null ? afVar.asBinder() : null);
                    this.le.transact(4, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(CreateFileRequest createFileRequest, af afVar) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.drive.internal.IDriveService");
                    if (createFileRequest != null) {
                        obtain.writeInt(1);
                        createFileRequest.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeStrongBinder(afVar != null ? afVar.asBinder() : null);
                    this.le.transact(5, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(CreateFolderRequest createFolderRequest, af afVar) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.drive.internal.IDriveService");
                    if (createFolderRequest != null) {
                        obtain.writeInt(1);
                        createFolderRequest.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeStrongBinder(afVar != null ? afVar.asBinder() : null);
                    this.le.transact(6, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(DeleteResourceRequest deleteResourceRequest, af afVar) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.drive.internal.IDriveService");
                    if (deleteResourceRequest != null) {
                        obtain.writeInt(1);
                        deleteResourceRequest.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeStrongBinder(afVar != null ? afVar.asBinder() : null);
                    this.le.transact(24, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(DisconnectRequest disconnectRequest) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.drive.internal.IDriveService");
                    if (disconnectRequest != null) {
                        obtain.writeInt(1);
                        disconnectRequest.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.le.transact(16, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(GetDriveIdFromUniqueIdentifierRequest getDriveIdFromUniqueIdentifierRequest, af afVar) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.drive.internal.IDriveService");
                    if (getDriveIdFromUniqueIdentifierRequest != null) {
                        obtain.writeInt(1);
                        getDriveIdFromUniqueIdentifierRequest.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeStrongBinder(afVar != null ? afVar.asBinder() : null);
                    this.le.transact(29, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(GetMetadataRequest getMetadataRequest, af afVar) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.drive.internal.IDriveService");
                    if (getMetadataRequest != null) {
                        obtain.writeInt(1);
                        getMetadataRequest.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeStrongBinder(afVar != null ? afVar.asBinder() : null);
                    this.le.transact(1, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(ListParentsRequest listParentsRequest, af afVar) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.drive.internal.IDriveService");
                    if (listParentsRequest != null) {
                        obtain.writeInt(1);
                        listParentsRequest.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeStrongBinder(afVar != null ? afVar.asBinder() : null);
                    this.le.transact(13, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(LoadRealtimeRequest loadRealtimeRequest, af afVar) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.drive.internal.IDriveService");
                    if (loadRealtimeRequest != null) {
                        obtain.writeInt(1);
                        loadRealtimeRequest.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeStrongBinder(afVar != null ? afVar.asBinder() : null);
                    this.le.transact(27, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(OpenContentsRequest openContentsRequest, af afVar) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.drive.internal.IDriveService");
                    if (openContentsRequest != null) {
                        obtain.writeInt(1);
                        openContentsRequest.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeStrongBinder(afVar != null ? afVar.asBinder() : null);
                    this.le.transact(7, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(QueryRequest queryRequest, af afVar) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.drive.internal.IDriveService");
                    if (queryRequest != null) {
                        obtain.writeInt(1);
                        queryRequest.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeStrongBinder(afVar != null ? afVar.asBinder() : null);
                    this.le.transact(2, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(RemoveEventListenerRequest removeEventListenerRequest, ag agVar, String str, af afVar) throws RemoteException {
                IBinder iBinder = null;
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.drive.internal.IDriveService");
                    if (removeEventListenerRequest != null) {
                        obtain.writeInt(1);
                        removeEventListenerRequest.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeStrongBinder(agVar != null ? agVar.asBinder() : null);
                    obtain.writeString(str);
                    if (afVar != null) {
                        iBinder = afVar.asBinder();
                    }
                    obtain.writeStrongBinder(iBinder);
                    this.le.transact(15, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(SetDrivePreferencesRequest setDrivePreferencesRequest, af afVar) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.drive.internal.IDriveService");
                    if (setDrivePreferencesRequest != null) {
                        obtain.writeInt(1);
                        setDrivePreferencesRequest.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeStrongBinder(afVar != null ? afVar.asBinder() : null);
                    this.le.transact(33, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(SetFileUploadPreferencesRequest setFileUploadPreferencesRequest, af afVar) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.drive.internal.IDriveService");
                    if (setFileUploadPreferencesRequest != null) {
                        obtain.writeInt(1);
                        setFileUploadPreferencesRequest.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeStrongBinder(afVar != null ? afVar.asBinder() : null);
                    this.le.transact(36, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(SetResourceParentsRequest setResourceParentsRequest, af afVar) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.drive.internal.IDriveService");
                    if (setResourceParentsRequest != null) {
                        obtain.writeInt(1);
                        setResourceParentsRequest.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeStrongBinder(afVar != null ? afVar.asBinder() : null);
                    this.le.transact(28, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(TrashResourceRequest trashResourceRequest, af afVar) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.drive.internal.IDriveService");
                    if (trashResourceRequest != null) {
                        obtain.writeInt(1);
                        trashResourceRequest.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeStrongBinder(afVar != null ? afVar.asBinder() : null);
                    this.le.transact(17, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(UpdateMetadataRequest updateMetadataRequest, af afVar) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.drive.internal.IDriveService");
                    if (updateMetadataRequest != null) {
                        obtain.writeInt(1);
                        updateMetadataRequest.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeStrongBinder(afVar != null ? afVar.asBinder() : null);
                    this.le.transact(3, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(af afVar) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.drive.internal.IDriveService");
                    obtain.writeStrongBinder(afVar != null ? afVar.asBinder() : null);
                    this.le.transact(9, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public IBinder asBinder() {
                return this.le;
            }

            public void b(QueryRequest queryRequest, af afVar) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.drive.internal.IDriveService");
                    if (queryRequest != null) {
                        obtain.writeInt(1);
                        queryRequest.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeStrongBinder(afVar != null ? afVar.asBinder() : null);
                    this.le.transact(19, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void b(af afVar) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.drive.internal.IDriveService");
                    obtain.writeStrongBinder(afVar != null ? afVar.asBinder() : null);
                    this.le.transact(20, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void c(af afVar) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.drive.internal.IDriveService");
                    obtain.writeStrongBinder(afVar != null ? afVar.asBinder() : null);
                    this.le.transact(21, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void d(af afVar) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.drive.internal.IDriveService");
                    obtain.writeStrongBinder(afVar != null ? afVar.asBinder() : null);
                    this.le.transact(22, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void e(af afVar) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.drive.internal.IDriveService");
                    obtain.writeStrongBinder(afVar != null ? afVar.asBinder() : null);
                    this.le.transact(23, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void f(af afVar) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.drive.internal.IDriveService");
                    obtain.writeStrongBinder(afVar != null ? afVar.asBinder() : null);
                    this.le.transact(31, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void g(af afVar) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.drive.internal.IDriveService");
                    obtain.writeStrongBinder(afVar != null ? afVar.asBinder() : null);
                    this.le.transact(32, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void h(af afVar) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.drive.internal.IDriveService");
                    obtain.writeStrongBinder(afVar != null ? afVar.asBinder() : null);
                    this.le.transact(35, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
        }

        public static ae X(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.drive.internal.IDriveService");
            return (queryLocalInterface == null || !(queryLocalInterface instanceof ae)) ? new C0005a(iBinder) : (ae) queryLocalInterface;
        }

        /* JADX WARNING: type inference failed for: r0v0 */
        /* JADX WARNING: type inference failed for: r0v1, types: [com.google.android.gms.drive.internal.CancelPendingActionsRequest] */
        /* JADX WARNING: type inference failed for: r0v5, types: [com.google.android.gms.drive.internal.CancelPendingActionsRequest] */
        /* JADX WARNING: type inference failed for: r0v6, types: [com.google.android.gms.drive.internal.SetFileUploadPreferencesRequest] */
        /* JADX WARNING: type inference failed for: r0v10, types: [com.google.android.gms.drive.internal.SetFileUploadPreferencesRequest] */
        /* JADX WARNING: type inference failed for: r0v15, types: [com.google.android.gms.drive.RealtimeDocumentSyncRequest] */
        /* JADX WARNING: type inference failed for: r0v19, types: [com.google.android.gms.drive.RealtimeDocumentSyncRequest] */
        /* JADX WARNING: type inference failed for: r0v20, types: [com.google.android.gms.drive.internal.SetDrivePreferencesRequest] */
        /* JADX WARNING: type inference failed for: r0v24, types: [com.google.android.gms.drive.internal.SetDrivePreferencesRequest] */
        /* JADX WARNING: type inference failed for: r0v33, types: [com.google.android.gms.drive.internal.CheckResourceIdsExistRequest] */
        /* JADX WARNING: type inference failed for: r0v37, types: [com.google.android.gms.drive.internal.CheckResourceIdsExistRequest] */
        /* JADX WARNING: type inference failed for: r0v38, types: [com.google.android.gms.drive.internal.GetDriveIdFromUniqueIdentifierRequest] */
        /* JADX WARNING: type inference failed for: r0v42, types: [com.google.android.gms.drive.internal.GetDriveIdFromUniqueIdentifierRequest] */
        /* JADX WARNING: type inference failed for: r0v43, types: [com.google.android.gms.drive.internal.SetResourceParentsRequest] */
        /* JADX WARNING: type inference failed for: r0v47, types: [com.google.android.gms.drive.internal.SetResourceParentsRequest] */
        /* JADX WARNING: type inference failed for: r0v48, types: [com.google.android.gms.drive.internal.LoadRealtimeRequest] */
        /* JADX WARNING: type inference failed for: r0v52, types: [com.google.android.gms.drive.internal.LoadRealtimeRequest] */
        /* JADX WARNING: type inference failed for: r0v53, types: [com.google.android.gms.drive.internal.DeleteResourceRequest] */
        /* JADX WARNING: type inference failed for: r0v57, types: [com.google.android.gms.drive.internal.DeleteResourceRequest] */
        /* JADX WARNING: type inference failed for: r0v74, types: [com.google.android.gms.drive.internal.QueryRequest] */
        /* JADX WARNING: type inference failed for: r0v78, types: [com.google.android.gms.drive.internal.QueryRequest] */
        /* JADX WARNING: type inference failed for: r0v79, types: [com.google.android.gms.drive.internal.CloseContentsAndUpdateMetadataRequest] */
        /* JADX WARNING: type inference failed for: r0v83, types: [com.google.android.gms.drive.internal.CloseContentsAndUpdateMetadataRequest] */
        /* JADX WARNING: type inference failed for: r0v84, types: [com.google.android.gms.drive.internal.TrashResourceRequest] */
        /* JADX WARNING: type inference failed for: r0v88, types: [com.google.android.gms.drive.internal.TrashResourceRequest] */
        /* JADX WARNING: type inference failed for: r0v89, types: [com.google.android.gms.drive.internal.DisconnectRequest] */
        /* JADX WARNING: type inference failed for: r0v93, types: [com.google.android.gms.drive.internal.DisconnectRequest] */
        /* JADX WARNING: type inference failed for: r0v94, types: [com.google.android.gms.drive.internal.RemoveEventListenerRequest] */
        /* JADX WARNING: type inference failed for: r0v98, types: [com.google.android.gms.drive.internal.RemoveEventListenerRequest] */
        /* JADX WARNING: type inference failed for: r0v99, types: [com.google.android.gms.drive.internal.AddEventListenerRequest] */
        /* JADX WARNING: type inference failed for: r0v103, types: [com.google.android.gms.drive.internal.AddEventListenerRequest] */
        /* JADX WARNING: type inference failed for: r0v104, types: [com.google.android.gms.drive.internal.ListParentsRequest] */
        /* JADX WARNING: type inference failed for: r0v108, types: [com.google.android.gms.drive.internal.ListParentsRequest] */
        /* JADX WARNING: type inference failed for: r0v109, types: [com.google.android.gms.drive.internal.AuthorizeAccessRequest] */
        /* JADX WARNING: type inference failed for: r0v113, types: [com.google.android.gms.drive.internal.AuthorizeAccessRequest] */
        /* JADX WARNING: type inference failed for: r0v114, types: [com.google.android.gms.drive.internal.CreateFileIntentSenderRequest] */
        /* JADX WARNING: type inference failed for: r0v119, types: [com.google.android.gms.drive.internal.CreateFileIntentSenderRequest] */
        /* JADX WARNING: type inference failed for: r0v120, types: [com.google.android.gms.drive.internal.OpenFileIntentSenderRequest] */
        /* JADX WARNING: type inference failed for: r0v125, types: [com.google.android.gms.drive.internal.OpenFileIntentSenderRequest] */
        /* JADX WARNING: type inference failed for: r0v130, types: [com.google.android.gms.drive.internal.CloseContentsRequest] */
        /* JADX WARNING: type inference failed for: r0v134, types: [com.google.android.gms.drive.internal.CloseContentsRequest] */
        /* JADX WARNING: type inference failed for: r0v135, types: [com.google.android.gms.drive.internal.OpenContentsRequest] */
        /* JADX WARNING: type inference failed for: r0v139, types: [com.google.android.gms.drive.internal.OpenContentsRequest] */
        /* JADX WARNING: type inference failed for: r0v140, types: [com.google.android.gms.drive.internal.CreateFolderRequest] */
        /* JADX WARNING: type inference failed for: r0v144, types: [com.google.android.gms.drive.internal.CreateFolderRequest] */
        /* JADX WARNING: type inference failed for: r0v145, types: [com.google.android.gms.drive.internal.CreateFileRequest] */
        /* JADX WARNING: type inference failed for: r0v149, types: [com.google.android.gms.drive.internal.CreateFileRequest] */
        /* JADX WARNING: type inference failed for: r0v150, types: [com.google.android.gms.drive.internal.CreateContentsRequest] */
        /* JADX WARNING: type inference failed for: r0v154, types: [com.google.android.gms.drive.internal.CreateContentsRequest] */
        /* JADX WARNING: type inference failed for: r0v155, types: [com.google.android.gms.drive.internal.UpdateMetadataRequest] */
        /* JADX WARNING: type inference failed for: r0v159, types: [com.google.android.gms.drive.internal.UpdateMetadataRequest] */
        /* JADX WARNING: type inference failed for: r0v160, types: [com.google.android.gms.drive.internal.QueryRequest] */
        /* JADX WARNING: type inference failed for: r0v164, types: [com.google.android.gms.drive.internal.QueryRequest] */
        /* JADX WARNING: type inference failed for: r0v165, types: [com.google.android.gms.drive.internal.GetMetadataRequest] */
        /* JADX WARNING: type inference failed for: r0v169, types: [com.google.android.gms.drive.internal.GetMetadataRequest] */
        /* JADX WARNING: type inference failed for: r0v173 */
        /* JADX WARNING: type inference failed for: r0v174 */
        /* JADX WARNING: type inference failed for: r0v175 */
        /* JADX WARNING: type inference failed for: r0v176 */
        /* JADX WARNING: type inference failed for: r0v177 */
        /* JADX WARNING: type inference failed for: r0v178 */
        /* JADX WARNING: type inference failed for: r0v179 */
        /* JADX WARNING: type inference failed for: r0v180 */
        /* JADX WARNING: type inference failed for: r0v181 */
        /* JADX WARNING: type inference failed for: r0v182 */
        /* JADX WARNING: type inference failed for: r0v183 */
        /* JADX WARNING: type inference failed for: r0v184 */
        /* JADX WARNING: type inference failed for: r0v185 */
        /* JADX WARNING: type inference failed for: r0v186 */
        /* JADX WARNING: type inference failed for: r0v187 */
        /* JADX WARNING: type inference failed for: r0v188 */
        /* JADX WARNING: type inference failed for: r0v189 */
        /* JADX WARNING: type inference failed for: r0v190 */
        /* JADX WARNING: type inference failed for: r0v191 */
        /* JADX WARNING: type inference failed for: r0v192 */
        /* JADX WARNING: type inference failed for: r0v193 */
        /* JADX WARNING: type inference failed for: r0v194 */
        /* JADX WARNING: type inference failed for: r0v195 */
        /* JADX WARNING: type inference failed for: r0v196 */
        /* JADX WARNING: type inference failed for: r0v197 */
        /* JADX WARNING: type inference failed for: r0v198 */
        /* JADX WARNING: type inference failed for: r0v199 */
        /* JADX WARNING: Multi-variable type inference failed. Error: jadx.core.utils.exceptions.JadxRuntimeException: No candidate types for var: r0v0
  assigns: [?[int, float, boolean, short, byte, char, OBJECT, ARRAY], com.google.android.gms.drive.internal.SetFileUploadPreferencesRequest, com.google.android.gms.drive.internal.CancelPendingActionsRequest, com.google.android.gms.drive.RealtimeDocumentSyncRequest, com.google.android.gms.drive.internal.SetDrivePreferencesRequest, com.google.android.gms.drive.internal.CheckResourceIdsExistRequest, com.google.android.gms.drive.internal.GetDriveIdFromUniqueIdentifierRequest, com.google.android.gms.drive.internal.SetResourceParentsRequest, com.google.android.gms.drive.internal.LoadRealtimeRequest, com.google.android.gms.drive.internal.DeleteResourceRequest, com.google.android.gms.drive.internal.QueryRequest, com.google.android.gms.drive.internal.CloseContentsAndUpdateMetadataRequest, com.google.android.gms.drive.internal.TrashResourceRequest, com.google.android.gms.drive.internal.DisconnectRequest, com.google.android.gms.drive.internal.RemoveEventListenerRequest, com.google.android.gms.drive.internal.AddEventListenerRequest, com.google.android.gms.drive.internal.ListParentsRequest, com.google.android.gms.drive.internal.AuthorizeAccessRequest, com.google.android.gms.drive.internal.CreateFileIntentSenderRequest, com.google.android.gms.drive.internal.OpenFileIntentSenderRequest, com.google.android.gms.drive.internal.CloseContentsRequest, com.google.android.gms.drive.internal.OpenContentsRequest, com.google.android.gms.drive.internal.CreateFolderRequest, com.google.android.gms.drive.internal.CreateFileRequest, com.google.android.gms.drive.internal.CreateContentsRequest, com.google.android.gms.drive.internal.UpdateMetadataRequest, com.google.android.gms.drive.internal.GetMetadataRequest]
  uses: [com.google.android.gms.drive.internal.CancelPendingActionsRequest, com.google.android.gms.drive.internal.SetFileUploadPreferencesRequest, com.google.android.gms.drive.RealtimeDocumentSyncRequest, com.google.android.gms.drive.internal.SetDrivePreferencesRequest, com.google.android.gms.drive.internal.CheckResourceIdsExistRequest, com.google.android.gms.drive.internal.GetDriveIdFromUniqueIdentifierRequest, com.google.android.gms.drive.internal.SetResourceParentsRequest, com.google.android.gms.drive.internal.LoadRealtimeRequest, com.google.android.gms.drive.internal.DeleteResourceRequest, com.google.android.gms.drive.internal.QueryRequest, com.google.android.gms.drive.internal.CloseContentsAndUpdateMetadataRequest, com.google.android.gms.drive.internal.TrashResourceRequest, com.google.android.gms.drive.internal.DisconnectRequest, com.google.android.gms.drive.internal.RemoveEventListenerRequest, com.google.android.gms.drive.internal.AddEventListenerRequest, com.google.android.gms.drive.internal.ListParentsRequest, com.google.android.gms.drive.internal.AuthorizeAccessRequest, com.google.android.gms.drive.internal.CreateFileIntentSenderRequest, com.google.android.gms.drive.internal.OpenFileIntentSenderRequest, com.google.android.gms.drive.internal.CloseContentsRequest, com.google.android.gms.drive.internal.OpenContentsRequest, com.google.android.gms.drive.internal.CreateFolderRequest, com.google.android.gms.drive.internal.CreateFileRequest, com.google.android.gms.drive.internal.CreateContentsRequest, com.google.android.gms.drive.internal.UpdateMetadataRequest, com.google.android.gms.drive.internal.GetMetadataRequest]
  mth insns count: 422
        	at jadx.core.dex.visitors.typeinference.TypeSearch.fillTypeCandidates(TypeSearch.java:237)
        	at java.util.ArrayList.forEach(Unknown Source)
        	at jadx.core.dex.visitors.typeinference.TypeSearch.run(TypeSearch.java:53)
        	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.runMultiVariableSearch(TypeInferenceVisitor.java:99)
        	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.visit(TypeInferenceVisitor.java:92)
        	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:27)
        	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$1(DepthTraversal.java:14)
        	at java.util.ArrayList.forEach(Unknown Source)
        	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
        	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$0(DepthTraversal.java:13)
        	at java.util.ArrayList.forEach(Unknown Source)
        	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:13)
        	at jadx.core.ProcessClass.process(ProcessClass.java:30)
        	at jadx.core.ProcessClass.lambda$processDependencies$0(ProcessClass.java:49)
        	at java.util.ArrayList.forEach(Unknown Source)
        	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:49)
        	at jadx.core.ProcessClass.process(ProcessClass.java:35)
        	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
        	at jadx.api.JavaClass.decompile(JavaClass.java:62)
        	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
         */
        /* JADX WARNING: Unknown variable types count: 28 */
        public boolean onTransact(int code, Parcel data, Parcel reply, int flags) throws RemoteException {
            ? r0 = 0;
            switch (code) {
                case 1:
                    data.enforceInterface("com.google.android.gms.drive.internal.IDriveService");
                    if (data.readInt() != 0) {
                        r0 = (GetMetadataRequest) GetMetadataRequest.CREATOR.createFromParcel(data);
                    }
                    a((GetMetadataRequest) r0, com.google.android.gms.drive.internal.af.a.Y(data.readStrongBinder()));
                    reply.writeNoException();
                    return true;
                case 2:
                    data.enforceInterface("com.google.android.gms.drive.internal.IDriveService");
                    if (data.readInt() != 0) {
                        r0 = (QueryRequest) QueryRequest.CREATOR.createFromParcel(data);
                    }
                    a((QueryRequest) r0, com.google.android.gms.drive.internal.af.a.Y(data.readStrongBinder()));
                    reply.writeNoException();
                    return true;
                case 3:
                    data.enforceInterface("com.google.android.gms.drive.internal.IDriveService");
                    if (data.readInt() != 0) {
                        r0 = (UpdateMetadataRequest) UpdateMetadataRequest.CREATOR.createFromParcel(data);
                    }
                    a((UpdateMetadataRequest) r0, com.google.android.gms.drive.internal.af.a.Y(data.readStrongBinder()));
                    reply.writeNoException();
                    return true;
                case 4:
                    data.enforceInterface("com.google.android.gms.drive.internal.IDriveService");
                    if (data.readInt() != 0) {
                        r0 = (CreateContentsRequest) CreateContentsRequest.CREATOR.createFromParcel(data);
                    }
                    a((CreateContentsRequest) r0, com.google.android.gms.drive.internal.af.a.Y(data.readStrongBinder()));
                    reply.writeNoException();
                    return true;
                case 5:
                    data.enforceInterface("com.google.android.gms.drive.internal.IDriveService");
                    if (data.readInt() != 0) {
                        r0 = (CreateFileRequest) CreateFileRequest.CREATOR.createFromParcel(data);
                    }
                    a((CreateFileRequest) r0, com.google.android.gms.drive.internal.af.a.Y(data.readStrongBinder()));
                    reply.writeNoException();
                    return true;
                case 6:
                    data.enforceInterface("com.google.android.gms.drive.internal.IDriveService");
                    if (data.readInt() != 0) {
                        r0 = (CreateFolderRequest) CreateFolderRequest.CREATOR.createFromParcel(data);
                    }
                    a((CreateFolderRequest) r0, com.google.android.gms.drive.internal.af.a.Y(data.readStrongBinder()));
                    reply.writeNoException();
                    return true;
                case 7:
                    data.enforceInterface("com.google.android.gms.drive.internal.IDriveService");
                    if (data.readInt() != 0) {
                        r0 = (OpenContentsRequest) OpenContentsRequest.CREATOR.createFromParcel(data);
                    }
                    a((OpenContentsRequest) r0, com.google.android.gms.drive.internal.af.a.Y(data.readStrongBinder()));
                    reply.writeNoException();
                    return true;
                case 8:
                    data.enforceInterface("com.google.android.gms.drive.internal.IDriveService");
                    if (data.readInt() != 0) {
                        r0 = (CloseContentsRequest) CloseContentsRequest.CREATOR.createFromParcel(data);
                    }
                    a((CloseContentsRequest) r0, com.google.android.gms.drive.internal.af.a.Y(data.readStrongBinder()));
                    reply.writeNoException();
                    return true;
                case 9:
                    data.enforceInterface("com.google.android.gms.drive.internal.IDriveService");
                    a(com.google.android.gms.drive.internal.af.a.Y(data.readStrongBinder()));
                    reply.writeNoException();
                    return true;
                case 10:
                    data.enforceInterface("com.google.android.gms.drive.internal.IDriveService");
                    if (data.readInt() != 0) {
                        r0 = (OpenFileIntentSenderRequest) OpenFileIntentSenderRequest.CREATOR.createFromParcel(data);
                    }
                    IntentSender a = a((OpenFileIntentSenderRequest) r0);
                    reply.writeNoException();
                    if (a != null) {
                        reply.writeInt(1);
                        a.writeToParcel(reply, 1);
                    } else {
                        reply.writeInt(0);
                    }
                    return true;
                case 11:
                    data.enforceInterface("com.google.android.gms.drive.internal.IDriveService");
                    if (data.readInt() != 0) {
                        r0 = (CreateFileIntentSenderRequest) CreateFileIntentSenderRequest.CREATOR.createFromParcel(data);
                    }
                    IntentSender a2 = a((CreateFileIntentSenderRequest) r0);
                    reply.writeNoException();
                    if (a2 != null) {
                        reply.writeInt(1);
                        a2.writeToParcel(reply, 1);
                    } else {
                        reply.writeInt(0);
                    }
                    return true;
                case 12:
                    data.enforceInterface("com.google.android.gms.drive.internal.IDriveService");
                    if (data.readInt() != 0) {
                        r0 = (AuthorizeAccessRequest) AuthorizeAccessRequest.CREATOR.createFromParcel(data);
                    }
                    a((AuthorizeAccessRequest) r0, com.google.android.gms.drive.internal.af.a.Y(data.readStrongBinder()));
                    reply.writeNoException();
                    return true;
                case 13:
                    data.enforceInterface("com.google.android.gms.drive.internal.IDriveService");
                    if (data.readInt() != 0) {
                        r0 = (ListParentsRequest) ListParentsRequest.CREATOR.createFromParcel(data);
                    }
                    a((ListParentsRequest) r0, com.google.android.gms.drive.internal.af.a.Y(data.readStrongBinder()));
                    reply.writeNoException();
                    return true;
                case 14:
                    data.enforceInterface("com.google.android.gms.drive.internal.IDriveService");
                    if (data.readInt() != 0) {
                        r0 = (AddEventListenerRequest) AddEventListenerRequest.CREATOR.createFromParcel(data);
                    }
                    a((AddEventListenerRequest) r0, com.google.android.gms.drive.internal.ag.a.Z(data.readStrongBinder()), data.readString(), com.google.android.gms.drive.internal.af.a.Y(data.readStrongBinder()));
                    reply.writeNoException();
                    return true;
                case 15:
                    data.enforceInterface("com.google.android.gms.drive.internal.IDriveService");
                    if (data.readInt() != 0) {
                        r0 = (RemoveEventListenerRequest) RemoveEventListenerRequest.CREATOR.createFromParcel(data);
                    }
                    a((RemoveEventListenerRequest) r0, com.google.android.gms.drive.internal.ag.a.Z(data.readStrongBinder()), data.readString(), com.google.android.gms.drive.internal.af.a.Y(data.readStrongBinder()));
                    reply.writeNoException();
                    return true;
                case 16:
                    data.enforceInterface("com.google.android.gms.drive.internal.IDriveService");
                    if (data.readInt() != 0) {
                        r0 = (DisconnectRequest) DisconnectRequest.CREATOR.createFromParcel(data);
                    }
                    a((DisconnectRequest) r0);
                    reply.writeNoException();
                    return true;
                case CEventWnd.WM_NET /*17*/:
                    data.enforceInterface("com.google.android.gms.drive.internal.IDriveService");
                    if (data.readInt() != 0) {
                        r0 = (TrashResourceRequest) TrashResourceRequest.CREATOR.createFromParcel(data);
                    }
                    a((TrashResourceRequest) r0, com.google.android.gms.drive.internal.af.a.Y(data.readStrongBinder()));
                    reply.writeNoException();
                    return true;
                case CEventWnd.WM_APP_EXIT /*18*/:
                    data.enforceInterface("com.google.android.gms.drive.internal.IDriveService");
                    if (data.readInt() != 0) {
                        r0 = (CloseContentsAndUpdateMetadataRequest) CloseContentsAndUpdateMetadataRequest.CREATOR.createFromParcel(data);
                    }
                    a((CloseContentsAndUpdateMetadataRequest) r0, com.google.android.gms.drive.internal.af.a.Y(data.readStrongBinder()));
                    reply.writeNoException();
                    return true;
                case 19:
                    data.enforceInterface("com.google.android.gms.drive.internal.IDriveService");
                    if (data.readInt() != 0) {
                        r0 = (QueryRequest) QueryRequest.CREATOR.createFromParcel(data);
                    }
                    b(r0, com.google.android.gms.drive.internal.af.a.Y(data.readStrongBinder()));
                    reply.writeNoException();
                    return true;
                case 20:
                    data.enforceInterface("com.google.android.gms.drive.internal.IDriveService");
                    b(com.google.android.gms.drive.internal.af.a.Y(data.readStrongBinder()));
                    reply.writeNoException();
                    return true;
                case 21:
                    data.enforceInterface("com.google.android.gms.drive.internal.IDriveService");
                    c(com.google.android.gms.drive.internal.af.a.Y(data.readStrongBinder()));
                    reply.writeNoException();
                    return true;
                case 22:
                    data.enforceInterface("com.google.android.gms.drive.internal.IDriveService");
                    d(com.google.android.gms.drive.internal.af.a.Y(data.readStrongBinder()));
                    reply.writeNoException();
                    return true;
                case 23:
                    data.enforceInterface("com.google.android.gms.drive.internal.IDriveService");
                    e(com.google.android.gms.drive.internal.af.a.Y(data.readStrongBinder()));
                    reply.writeNoException();
                    return true;
                case 24:
                    data.enforceInterface("com.google.android.gms.drive.internal.IDriveService");
                    if (data.readInt() != 0) {
                        r0 = (DeleteResourceRequest) DeleteResourceRequest.CREATOR.createFromParcel(data);
                    }
                    a((DeleteResourceRequest) r0, com.google.android.gms.drive.internal.af.a.Y(data.readStrongBinder()));
                    reply.writeNoException();
                    return true;
                case 27:
                    data.enforceInterface("com.google.android.gms.drive.internal.IDriveService");
                    if (data.readInt() != 0) {
                        r0 = (LoadRealtimeRequest) LoadRealtimeRequest.CREATOR.createFromParcel(data);
                    }
                    a((LoadRealtimeRequest) r0, com.google.android.gms.drive.internal.af.a.Y(data.readStrongBinder()));
                    reply.writeNoException();
                    return true;
                case 28:
                    data.enforceInterface("com.google.android.gms.drive.internal.IDriveService");
                    if (data.readInt() != 0) {
                        r0 = (SetResourceParentsRequest) SetResourceParentsRequest.CREATOR.createFromParcel(data);
                    }
                    a((SetResourceParentsRequest) r0, com.google.android.gms.drive.internal.af.a.Y(data.readStrongBinder()));
                    reply.writeNoException();
                    return true;
                case 29:
                    data.enforceInterface("com.google.android.gms.drive.internal.IDriveService");
                    if (data.readInt() != 0) {
                        r0 = (GetDriveIdFromUniqueIdentifierRequest) GetDriveIdFromUniqueIdentifierRequest.CREATOR.createFromParcel(data);
                    }
                    a((GetDriveIdFromUniqueIdentifierRequest) r0, com.google.android.gms.drive.internal.af.a.Y(data.readStrongBinder()));
                    reply.writeNoException();
                    return true;
                case 30:
                    data.enforceInterface("com.google.android.gms.drive.internal.IDriveService");
                    if (data.readInt() != 0) {
                        r0 = (CheckResourceIdsExistRequest) CheckResourceIdsExistRequest.CREATOR.createFromParcel(data);
                    }
                    a((CheckResourceIdsExistRequest) r0, com.google.android.gms.drive.internal.af.a.Y(data.readStrongBinder()));
                    reply.writeNoException();
                    return true;
                case Notifications.NOTIFICATION_TYPES_ALL /*31*/:
                    data.enforceInterface("com.google.android.gms.drive.internal.IDriveService");
                    f(com.google.android.gms.drive.internal.af.a.Y(data.readStrongBinder()));
                    reply.writeNoException();
                    return true;
                case 32:
                    data.enforceInterface("com.google.android.gms.drive.internal.IDriveService");
                    g(com.google.android.gms.drive.internal.af.a.Y(data.readStrongBinder()));
                    reply.writeNoException();
                    return true;
                case 33:
                    data.enforceInterface("com.google.android.gms.drive.internal.IDriveService");
                    if (data.readInt() != 0) {
                        r0 = (SetDrivePreferencesRequest) SetDrivePreferencesRequest.CREATOR.createFromParcel(data);
                    }
                    a((SetDrivePreferencesRequest) r0, com.google.android.gms.drive.internal.af.a.Y(data.readStrongBinder()));
                    reply.writeNoException();
                    return true;
                case 34:
                    data.enforceInterface("com.google.android.gms.drive.internal.IDriveService");
                    if (data.readInt() != 0) {
                        r0 = (RealtimeDocumentSyncRequest) RealtimeDocumentSyncRequest.CREATOR.createFromParcel(data);
                    }
                    a((RealtimeDocumentSyncRequest) r0, com.google.android.gms.drive.internal.af.a.Y(data.readStrongBinder()));
                    reply.writeNoException();
                    return true;
                case 35:
                    data.enforceInterface("com.google.android.gms.drive.internal.IDriveService");
                    h(com.google.android.gms.drive.internal.af.a.Y(data.readStrongBinder()));
                    reply.writeNoException();
                    return true;
                case 36:
                    data.enforceInterface("com.google.android.gms.drive.internal.IDriveService");
                    if (data.readInt() != 0) {
                        r0 = (SetFileUploadPreferencesRequest) SetFileUploadPreferencesRequest.CREATOR.createFromParcel(data);
                    }
                    a((SetFileUploadPreferencesRequest) r0, com.google.android.gms.drive.internal.af.a.Y(data.readStrongBinder()));
                    reply.writeNoException();
                    return true;
                case 37:
                    data.enforceInterface("com.google.android.gms.drive.internal.IDriveService");
                    if (data.readInt() != 0) {
                        r0 = (CancelPendingActionsRequest) CancelPendingActionsRequest.CREATOR.createFromParcel(data);
                    }
                    a((CancelPendingActionsRequest) r0, com.google.android.gms.drive.internal.af.a.Y(data.readStrongBinder()));
                    reply.writeNoException();
                    return true;
                case 1598968902:
                    reply.writeString("com.google.android.gms.drive.internal.IDriveService");
                    return true;
                default:
                    return super.onTransact(code, data, reply, flags);
            }
        }
    }

    IntentSender a(CreateFileIntentSenderRequest createFileIntentSenderRequest) throws RemoteException;

    IntentSender a(OpenFileIntentSenderRequest openFileIntentSenderRequest) throws RemoteException;

    void a(RealtimeDocumentSyncRequest realtimeDocumentSyncRequest, af afVar) throws RemoteException;

    void a(AddEventListenerRequest addEventListenerRequest, ag agVar, String str, af afVar) throws RemoteException;

    void a(AuthorizeAccessRequest authorizeAccessRequest, af afVar) throws RemoteException;

    void a(CancelPendingActionsRequest cancelPendingActionsRequest, af afVar) throws RemoteException;

    void a(CheckResourceIdsExistRequest checkResourceIdsExistRequest, af afVar) throws RemoteException;

    void a(CloseContentsAndUpdateMetadataRequest closeContentsAndUpdateMetadataRequest, af afVar) throws RemoteException;

    void a(CloseContentsRequest closeContentsRequest, af afVar) throws RemoteException;

    void a(CreateContentsRequest createContentsRequest, af afVar) throws RemoteException;

    void a(CreateFileRequest createFileRequest, af afVar) throws RemoteException;

    void a(CreateFolderRequest createFolderRequest, af afVar) throws RemoteException;

    void a(DeleteResourceRequest deleteResourceRequest, af afVar) throws RemoteException;

    void a(DisconnectRequest disconnectRequest) throws RemoteException;

    void a(GetDriveIdFromUniqueIdentifierRequest getDriveIdFromUniqueIdentifierRequest, af afVar) throws RemoteException;

    void a(GetMetadataRequest getMetadataRequest, af afVar) throws RemoteException;

    void a(ListParentsRequest listParentsRequest, af afVar) throws RemoteException;

    void a(LoadRealtimeRequest loadRealtimeRequest, af afVar) throws RemoteException;

    void a(OpenContentsRequest openContentsRequest, af afVar) throws RemoteException;

    void a(QueryRequest queryRequest, af afVar) throws RemoteException;

    void a(RemoveEventListenerRequest removeEventListenerRequest, ag agVar, String str, af afVar) throws RemoteException;

    void a(SetDrivePreferencesRequest setDrivePreferencesRequest, af afVar) throws RemoteException;

    void a(SetFileUploadPreferencesRequest setFileUploadPreferencesRequest, af afVar) throws RemoteException;

    void a(SetResourceParentsRequest setResourceParentsRequest, af afVar) throws RemoteException;

    void a(TrashResourceRequest trashResourceRequest, af afVar) throws RemoteException;

    void a(UpdateMetadataRequest updateMetadataRequest, af afVar) throws RemoteException;

    void a(af afVar) throws RemoteException;

    void b(QueryRequest queryRequest, af afVar) throws RemoteException;

    void b(af afVar) throws RemoteException;

    void c(af afVar) throws RemoteException;

    void d(af afVar) throws RemoteException;

    void e(af afVar) throws RemoteException;

    void f(af afVar) throws RemoteException;

    void g(af afVar) throws RemoteException;

    void h(af afVar) throws RemoteException;
}
