package com.google.android.gms.drive.internal;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.a.C0004a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.drive.Contents;

public class al implements Creator<OnContentsResponse> {
    static void a(OnContentsResponse onContentsResponse, Parcel parcel, int i) {
        int H = b.H(parcel);
        b.c(parcel, 1, onContentsResponse.CK);
        b.a(parcel, 2, (Parcelable) onContentsResponse.PW, i, false);
        b.a(parcel, 3, onContentsResponse.QJ);
        b.H(parcel, H);
    }

    /* renamed from: aq */
    public OnContentsResponse createFromParcel(Parcel parcel) {
        boolean c;
        Contents contents;
        int i;
        boolean z = false;
        int G = a.G(parcel);
        Contents contents2 = null;
        int i2 = 0;
        while (parcel.dataPosition() < G) {
            int F = a.F(parcel);
            switch (a.aH(F)) {
                case 1:
                    boolean z2 = z;
                    contents = contents2;
                    i = a.g(parcel, F);
                    c = z2;
                    break;
                case 2:
                    i = i2;
                    Contents contents3 = (Contents) a.a(parcel, F, Contents.CREATOR);
                    c = z;
                    contents = contents3;
                    break;
                case 3:
                    c = a.c(parcel, F);
                    contents = contents2;
                    i = i2;
                    break;
                default:
                    a.b(parcel, F);
                    c = z;
                    contents = contents2;
                    i = i2;
                    break;
            }
            i2 = i;
            contents2 = contents;
            z = c;
        }
        if (parcel.dataPosition() == G) {
            return new OnContentsResponse(i2, contents2, z);
        }
        throw new C0004a("Overread allowed size end=" + G, parcel);
    }

    /* renamed from: bF */
    public OnContentsResponse[] newArray(int i) {
        return new OnContentsResponse[i];
    }
}
