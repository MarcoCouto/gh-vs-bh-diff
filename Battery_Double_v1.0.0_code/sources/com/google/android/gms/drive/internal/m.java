package com.google.android.gms.drive.internal;

import com.google.android.gms.drive.Metadata;
import com.google.android.gms.drive.metadata.MetadataField;
import com.google.android.gms.drive.metadata.internal.MetadataBundle;

public final class m extends Metadata {
    private final MetadataBundle PD;

    public m(MetadataBundle metadataBundle) {
        this.PD = metadataBundle;
    }

    /* access modifiers changed from: protected */
    public <T> T a(MetadataField<T> metadataField) {
        return this.PD.a(metadataField);
    }

    public boolean isDataValid() {
        return this.PD != null;
    }

    /* renamed from: iy */
    public Metadata freeze() {
        return new m(MetadataBundle.a(this.PD));
    }

    public String toString() {
        return "Metadata [mImpl=" + this.PD + "]";
    }
}
