package com.google.android.gms.drive.query.internal;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.a.C0004a;
import com.google.android.gms.common.internal.safeparcel.b;

public class k implements Creator<NotFilter> {
    static void a(NotFilter notFilter, Parcel parcel, int i) {
        int H = b.H(parcel);
        b.c(parcel, 1000, notFilter.CK);
        b.a(parcel, 1, (Parcelable) notFilter.Sv, i, false);
        b.H(parcel, H);
    }

    /* renamed from: ba */
    public NotFilter createFromParcel(Parcel parcel) {
        int G = a.G(parcel);
        int i = 0;
        FilterHolder filterHolder = null;
        while (parcel.dataPosition() < G) {
            int F = a.F(parcel);
            switch (a.aH(F)) {
                case 1:
                    filterHolder = (FilterHolder) a.a(parcel, F, FilterHolder.CREATOR);
                    break;
                case 1000:
                    i = a.g(parcel, F);
                    break;
                default:
                    a.b(parcel, F);
                    break;
            }
        }
        if (parcel.dataPosition() == G) {
            return new NotFilter(i, filterHolder);
        }
        throw new C0004a("Overread allowed size end=" + G, parcel);
    }

    /* renamed from: cp */
    public NotFilter[] newArray(int i) {
        return new NotFilter[i];
    }
}
