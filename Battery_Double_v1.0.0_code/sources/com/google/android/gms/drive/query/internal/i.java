package com.google.android.gms.drive.query.internal;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.a.C0004a;
import com.google.android.gms.common.internal.safeparcel.b;
import java.util.List;

public class i implements Creator<LogicalFilter> {
    static void a(LogicalFilter logicalFilter, Parcel parcel, int i) {
        int H = b.H(parcel);
        b.c(parcel, 1000, logicalFilter.CK);
        b.a(parcel, 1, (Parcelable) logicalFilter.Sh, i, false);
        b.c(parcel, 2, logicalFilter.Su, false);
        b.H(parcel, H);
    }

    /* renamed from: aY */
    public LogicalFilter createFromParcel(Parcel parcel) {
        List c;
        Operator operator;
        int i;
        List list = null;
        int G = a.G(parcel);
        int i2 = 0;
        Operator operator2 = null;
        while (parcel.dataPosition() < G) {
            int F = a.F(parcel);
            switch (a.aH(F)) {
                case 1:
                    i = i2;
                    Operator operator3 = (Operator) a.a(parcel, F, Operator.CREATOR);
                    c = list;
                    operator = operator3;
                    break;
                case 2:
                    c = a.c(parcel, F, FilterHolder.CREATOR);
                    operator = operator2;
                    i = i2;
                    break;
                case 1000:
                    List list2 = list;
                    operator = operator2;
                    i = a.g(parcel, F);
                    c = list2;
                    break;
                default:
                    a.b(parcel, F);
                    c = list;
                    operator = operator2;
                    i = i2;
                    break;
            }
            i2 = i;
            operator2 = operator;
            list = c;
        }
        if (parcel.dataPosition() == G) {
            return new LogicalFilter(i2, operator2, list);
        }
        throw new C0004a("Overread allowed size end=" + G, parcel);
    }

    /* renamed from: cn */
    public LogicalFilter[] newArray(int i) {
        return new LogicalFilter[i];
    }
}
