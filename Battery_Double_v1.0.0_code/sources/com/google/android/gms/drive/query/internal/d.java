package com.google.android.gms.drive.query.internal;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.a.C0004a;
import com.google.android.gms.common.internal.safeparcel.b;

public class d implements Creator<FilterHolder> {
    static void a(FilterHolder filterHolder, Parcel parcel, int i) {
        int H = b.H(parcel);
        b.a(parcel, 1, (Parcelable) filterHolder.Sl, i, false);
        b.c(parcel, 1000, filterHolder.CK);
        b.a(parcel, 2, (Parcelable) filterHolder.Sm, i, false);
        b.a(parcel, 3, (Parcelable) filterHolder.Sn, i, false);
        b.a(parcel, 4, (Parcelable) filterHolder.So, i, false);
        b.a(parcel, 5, (Parcelable) filterHolder.Sp, i, false);
        b.a(parcel, 6, (Parcelable) filterHolder.Sq, i, false);
        b.a(parcel, 7, (Parcelable) filterHolder.Sr, i, false);
        b.H(parcel, H);
    }

    /* renamed from: aV */
    public FilterHolder createFromParcel(Parcel parcel) {
        HasFilter hasFilter = null;
        int G = a.G(parcel);
        int i = 0;
        MatchAllFilter matchAllFilter = null;
        InFilter inFilter = null;
        NotFilter notFilter = null;
        LogicalFilter logicalFilter = null;
        FieldOnlyFilter fieldOnlyFilter = null;
        ComparisonFilter comparisonFilter = null;
        while (parcel.dataPosition() < G) {
            int F = a.F(parcel);
            switch (a.aH(F)) {
                case 1:
                    comparisonFilter = (ComparisonFilter) a.a(parcel, F, (Creator<T>) ComparisonFilter.CREATOR);
                    break;
                case 2:
                    fieldOnlyFilter = (FieldOnlyFilter) a.a(parcel, F, FieldOnlyFilter.CREATOR);
                    break;
                case 3:
                    logicalFilter = (LogicalFilter) a.a(parcel, F, LogicalFilter.CREATOR);
                    break;
                case 4:
                    notFilter = (NotFilter) a.a(parcel, F, NotFilter.CREATOR);
                    break;
                case 5:
                    inFilter = (InFilter) a.a(parcel, F, (Creator<T>) InFilter.CREATOR);
                    break;
                case 6:
                    matchAllFilter = (MatchAllFilter) a.a(parcel, F, (Creator<T>) MatchAllFilter.CREATOR);
                    break;
                case 7:
                    hasFilter = (HasFilter) a.a(parcel, F, (Creator<T>) HasFilter.CREATOR);
                    break;
                case 1000:
                    i = a.g(parcel, F);
                    break;
                default:
                    a.b(parcel, F);
                    break;
            }
        }
        if (parcel.dataPosition() == G) {
            return new FilterHolder(i, comparisonFilter, fieldOnlyFilter, logicalFilter, notFilter, inFilter, matchAllFilter, hasFilter);
        }
        throw new C0004a("Overread allowed size end=" + G, parcel);
    }

    /* renamed from: ck */
    public FilterHolder[] newArray(int i) {
        return new FilterHolder[i];
    }
}
