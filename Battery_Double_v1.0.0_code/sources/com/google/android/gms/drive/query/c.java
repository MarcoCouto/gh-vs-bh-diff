package com.google.android.gms.drive.query;

import com.google.android.gms.drive.metadata.MetadataField;
import com.google.android.gms.drive.metadata.b;
import com.google.android.gms.drive.query.internal.Operator;
import com.google.android.gms.drive.query.internal.f;
import java.util.Iterator;
import java.util.List;

public class c implements f<String> {
    /* renamed from: a */
    public <T> String b(b<T> bVar, T t) {
        return String.format("contains(%s,%s)", new Object[]{bVar.getName(), t});
    }

    /* renamed from: a */
    public <T> String b(Operator operator, MetadataField<T> metadataField, T t) {
        return String.format("cmp(%s,%s,%s)", new Object[]{operator.getTag(), metadataField.getName(), t});
    }

    /* renamed from: a */
    public String b(Operator operator, List<String> list) {
        StringBuilder sb = new StringBuilder(operator.getTag() + "(");
        String str = "";
        Iterator it = list.iterator();
        while (true) {
            String str2 = str;
            if (!it.hasNext()) {
                return sb.append(")").toString();
            }
            String str3 = (String) it.next();
            sb.append(str2);
            sb.append(str3);
            str = ",";
        }
    }

    /* renamed from: bq */
    public String j(String str) {
        return String.format("not(%s)", new Object[]{str});
    }

    /* renamed from: c */
    public String d(MetadataField<?> metadataField) {
        return String.format("fieldOnly(%s)", new Object[]{metadataField.getName()});
    }

    /* renamed from: c */
    public <T> String d(MetadataField<T> metadataField, T t) {
        return String.format("has(%s,%s)", new Object[]{metadataField.getName(), t});
    }

    /* renamed from: jc */
    public String jd() {
        return "all()";
    }
}
