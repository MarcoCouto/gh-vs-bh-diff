package com.google.android.gms.drive.query.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.a.C0004a;
import com.google.android.gms.common.internal.safeparcel.b;

public class j implements Creator<MatchAllFilter> {
    static void a(MatchAllFilter matchAllFilter, Parcel parcel, int i) {
        int H = b.H(parcel);
        b.c(parcel, 1000, matchAllFilter.CK);
        b.H(parcel, H);
    }

    /* renamed from: aZ */
    public MatchAllFilter createFromParcel(Parcel parcel) {
        int G = a.G(parcel);
        int i = 0;
        while (parcel.dataPosition() < G) {
            int F = a.F(parcel);
            switch (a.aH(F)) {
                case 1000:
                    i = a.g(parcel, F);
                    break;
                default:
                    a.b(parcel, F);
                    break;
            }
        }
        if (parcel.dataPosition() == G) {
            return new MatchAllFilter(i);
        }
        throw new C0004a("Overread allowed size end=" + G, parcel);
    }

    /* renamed from: co */
    public MatchAllFilter[] newArray(int i) {
        return new MatchAllFilter[i];
    }
}
