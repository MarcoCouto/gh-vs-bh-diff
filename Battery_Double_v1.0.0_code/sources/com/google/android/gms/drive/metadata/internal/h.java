package com.google.android.gms.drive.metadata.internal;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.a.C0004a;
import com.google.android.gms.common.internal.safeparcel.b;

public class h implements Creator<MetadataBundle> {
    static void a(MetadataBundle metadataBundle, Parcel parcel, int i) {
        int H = b.H(parcel);
        b.c(parcel, 1, metadataBundle.CK);
        b.a(parcel, 2, metadataBundle.Ri, false);
        b.H(parcel, H);
    }

    /* renamed from: aP */
    public MetadataBundle createFromParcel(Parcel parcel) {
        int G = a.G(parcel);
        int i = 0;
        Bundle bundle = null;
        while (parcel.dataPosition() < G) {
            int F = a.F(parcel);
            switch (a.aH(F)) {
                case 1:
                    i = a.g(parcel, F);
                    break;
                case 2:
                    bundle = a.q(parcel, F);
                    break;
                default:
                    a.b(parcel, F);
                    break;
            }
        }
        if (parcel.dataPosition() == G) {
            return new MetadataBundle(i, bundle);
        }
        throw new C0004a("Overread allowed size end=" + G, parcel);
    }

    /* renamed from: ce */
    public MetadataBundle[] newArray(int i) {
        return new MetadataBundle[i];
    }
}
