package com.google.android.gms.drive.metadata.internal;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.a.C0004a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.drive.metadata.CustomPropertyKey;

public class c implements Creator<CustomProperty> {
    static void a(CustomProperty customProperty, Parcel parcel, int i) {
        int H = b.H(parcel);
        b.c(parcel, 1, customProperty.CK);
        b.a(parcel, 2, (Parcelable) customProperty.Rg, i, false);
        b.a(parcel, 3, customProperty.mValue, false);
        b.H(parcel, H);
    }

    /* renamed from: aO */
    public CustomProperty createFromParcel(Parcel parcel) {
        String o;
        CustomPropertyKey customPropertyKey;
        int i;
        String str = null;
        int G = a.G(parcel);
        int i2 = 0;
        CustomPropertyKey customPropertyKey2 = null;
        while (parcel.dataPosition() < G) {
            int F = a.F(parcel);
            switch (a.aH(F)) {
                case 1:
                    String str2 = str;
                    customPropertyKey = customPropertyKey2;
                    i = a.g(parcel, F);
                    o = str2;
                    break;
                case 2:
                    i = i2;
                    CustomPropertyKey customPropertyKey3 = (CustomPropertyKey) a.a(parcel, F, CustomPropertyKey.CREATOR);
                    o = str;
                    customPropertyKey = customPropertyKey3;
                    break;
                case 3:
                    o = a.o(parcel, F);
                    customPropertyKey = customPropertyKey2;
                    i = i2;
                    break;
                default:
                    a.b(parcel, F);
                    o = str;
                    customPropertyKey = customPropertyKey2;
                    i = i2;
                    break;
            }
            i2 = i;
            customPropertyKey2 = customPropertyKey;
            str = o;
        }
        if (parcel.dataPosition() == G) {
            return new CustomProperty(i2, customPropertyKey2, str);
        }
        throw new C0004a("Overread allowed size end=" + G, parcel);
    }

    /* renamed from: cd */
    public CustomProperty[] newArray(int i) {
        return new CustomProperty[i];
    }
}
