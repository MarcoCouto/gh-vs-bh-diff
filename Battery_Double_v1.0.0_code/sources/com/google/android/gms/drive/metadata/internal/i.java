package com.google.android.gms.drive.metadata.internal;

import android.os.Bundle;
import android.os.Parcelable;
import com.google.android.gms.drive.metadata.b;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public class i<T extends Parcelable> extends b<T> {
    public i(String str, int i) {
        super(str, Collections.emptySet(), Collections.singleton(str), i);
    }

    /* access modifiers changed from: protected */
    public void a(Bundle bundle, Collection<T> collection) {
        bundle.putParcelableArrayList(getName(), new ArrayList(collection));
    }

    /* access modifiers changed from: protected */
    /* renamed from: n */
    public Collection<T> i(Bundle bundle) {
        return bundle.getParcelableArrayList(getName());
    }
}
