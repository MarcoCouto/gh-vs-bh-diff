package com.google.android.gms.drive.realtime.internal.event;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.a.C0004a;
import com.google.android.gms.common.internal.safeparcel.b;
import java.util.ArrayList;
import java.util.List;

public class c implements Creator<ParcelableEventList> {
    static void a(ParcelableEventList parcelableEventList, Parcel parcel, int i) {
        int H = b.H(parcel);
        b.c(parcel, 1, parcelableEventList.CK);
        b.c(parcel, 2, parcelableEventList.mp, false);
        b.a(parcel, 3, (Parcelable) parcelableEventList.Tb, i, false);
        b.a(parcel, 4, parcelableEventList.Tc);
        b.b(parcel, 5, parcelableEventList.Td, false);
        b.H(parcel, H);
    }

    /* renamed from: bi */
    public ParcelableEventList createFromParcel(Parcel parcel) {
        boolean z = false;
        ArrayList arrayList = null;
        int G = a.G(parcel);
        DataHolder dataHolder = null;
        List list = null;
        int i = 0;
        while (parcel.dataPosition() < G) {
            int F = a.F(parcel);
            switch (a.aH(F)) {
                case 1:
                    i = a.g(parcel, F);
                    break;
                case 2:
                    list = a.c(parcel, F, ParcelableEvent.CREATOR);
                    break;
                case 3:
                    dataHolder = (DataHolder) a.a(parcel, F, (Creator<T>) DataHolder.CREATOR);
                    break;
                case 4:
                    z = a.c(parcel, F);
                    break;
                case 5:
                    arrayList = a.C(parcel, F);
                    break;
                default:
                    a.b(parcel, F);
                    break;
            }
        }
        if (parcel.dataPosition() == G) {
            return new ParcelableEventList(i, list, dataHolder, z, arrayList);
        }
        throw new C0004a("Overread allowed size end=" + G, parcel);
    }

    /* renamed from: cy */
    public ParcelableEventList[] newArray(int i) {
        return new ParcelableEventList[i];
    }
}
