package com.google.android.gms.internal;

import android.app.Activity;
import android.app.Application;
import android.app.Application.ActivityLifecycleCallbacks;
import android.content.Context;
import android.os.Bundle;

@ey
public class am implements ActivityLifecycleCallbacks {
    private Context mContext;
    private final Object mH = new Object();
    private Activity nB;

    public am(Application application, Activity activity) {
        application.registerActivityLifecycleCallbacks(this);
        setActivity(activity);
        this.mContext = application.getApplicationContext();
    }

    private void setActivity(Activity activity) {
        synchronized (this.mH) {
            if (!activity.getClass().getName().startsWith("com.google.android.gms.ads")) {
                this.nB = activity;
            }
        }
    }

    public Activity getActivity() {
        return this.nB;
    }

    public Context getContext() {
        return this.mContext;
    }

    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:?, code lost:
        return;
     */
    public void onActivityDestroyed(Activity activity) {
        synchronized (this.mH) {
            if (this.nB != null) {
                if (this.nB.equals(activity)) {
                    this.nB = null;
                }
            }
        }
    }

    public void onActivityPaused(Activity activity) {
        setActivity(activity);
    }

    public void onActivityResumed(Activity activity) {
        setActivity(activity);
    }

    public void onActivitySaveInstanceState(Activity activity, Bundle savedInstanceState) {
    }

    public void onActivityStarted(Activity activity) {
        setActivity(activity);
    }

    public void onActivityStopped(Activity activity) {
    }
}
