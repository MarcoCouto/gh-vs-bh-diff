package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.a.C0004a;
import com.google.android.gms.common.internal.safeparcel.b;

public class px implements Creator<pw> {
    static void a(pw pwVar, Parcel parcel, int i) {
        int H = b.H(parcel);
        b.c(parcel, 1, pwVar.getVersionCode());
        b.a(parcel, 2, pwVar.avX, false);
        b.H(parcel, H);
    }

    /* renamed from: dX */
    public pw createFromParcel(Parcel parcel) {
        int G = a.G(parcel);
        int i = 0;
        int[] iArr = null;
        while (parcel.dataPosition() < G) {
            int F = a.F(parcel);
            switch (a.aH(F)) {
                case 1:
                    i = a.g(parcel, F);
                    break;
                case 2:
                    iArr = a.u(parcel, F);
                    break;
                default:
                    a.b(parcel, F);
                    break;
            }
        }
        if (parcel.dataPosition() == G) {
            return new pw(i, iArr);
        }
        throw new C0004a("Overread allowed size end=" + G, parcel);
    }

    /* renamed from: ge */
    public pw[] newArray(int i) {
        return new pw[i];
    }
}
