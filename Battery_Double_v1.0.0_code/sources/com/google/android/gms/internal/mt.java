package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.a.C0004a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.fitness.data.DataSource;

public class mt implements Creator<ms> {
    static void a(ms msVar, Parcel parcel, int i) {
        int H = b.H(parcel);
        b.a(parcel, 1, (Parcelable) msVar.getDataSource(), i, false);
        b.c(parcel, 1000, msVar.getVersionCode());
        b.H(parcel, H);
    }

    /* renamed from: bH */
    public ms createFromParcel(Parcel parcel) {
        int G = a.G(parcel);
        int i = 0;
        DataSource dataSource = null;
        while (parcel.dataPosition() < G) {
            int F = a.F(parcel);
            switch (a.aH(F)) {
                case 1:
                    dataSource = (DataSource) a.a(parcel, F, DataSource.CREATOR);
                    break;
                case 1000:
                    i = a.g(parcel, F);
                    break;
                default:
                    a.b(parcel, F);
                    break;
            }
        }
        if (parcel.dataPosition() == G) {
            return new ms(i, dataSource);
        }
        throw new C0004a("Overread allowed size end=" + G, parcel);
    }

    /* renamed from: db */
    public ms[] newArray(int i) {
        return new ms[i];
    }
}
