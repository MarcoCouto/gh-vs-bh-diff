package com.google.android.gms.internal;

import android.content.Context;
import android.os.Bundle;
import android.os.DeadObjectException;
import android.os.IBinder;
import android.os.RemoteException;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.internal.cm.a;
import com.google.android.gms.internal.jl.e;

@ey
public class cl extends jl<cm> {
    final int qg;

    public cl(Context context, ConnectionCallbacks connectionCallbacks, OnConnectionFailedListener onConnectionFailedListener, int i) {
        super(context, context.getMainLooper(), connectionCallbacks, onConnectionFailedListener, new String[0]);
        this.qg = i;
    }

    /* access modifiers changed from: protected */
    public void a(jt jtVar, e eVar) throws RemoteException {
        jtVar.g(eVar, this.qg, getContext().getPackageName(), new Bundle());
    }

    /* access modifiers changed from: protected */
    public String bK() {
        return "com.google.android.gms.ads.gservice.START";
    }

    /* access modifiers changed from: protected */
    public String bL() {
        return "com.google.android.gms.ads.internal.gservice.IGservicesValueService";
    }

    public cm bM() throws DeadObjectException {
        return (cm) super.hw();
    }

    /* access modifiers changed from: protected */
    /* renamed from: k */
    public cm l(IBinder iBinder) {
        return a.m(iBinder);
    }
}
