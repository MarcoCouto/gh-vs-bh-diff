package com.google.android.gms.internal;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

@ey
public class al {
    private final Object mH = new Object();
    private List<ak> nA = new LinkedList();
    private int nz;

    public boolean a(ak akVar) {
        boolean z;
        synchronized (this.mH) {
            z = this.nA.contains(akVar);
        }
        return z;
    }

    public ak aZ() {
        int i;
        ak akVar;
        ak akVar2 = null;
        synchronized (this.mH) {
            if (this.nA.size() == 0) {
                gr.S("Queue empty");
                return null;
            } else if (this.nA.size() >= 2) {
                int i2 = Integer.MIN_VALUE;
                for (ak akVar3 : this.nA) {
                    int score = akVar3.getScore();
                    if (score > i2) {
                        int i3 = score;
                        akVar = akVar3;
                        i = i3;
                    } else {
                        i = i2;
                        akVar = akVar2;
                    }
                    i2 = i;
                    akVar2 = akVar;
                }
                this.nA.remove(akVar2);
                return akVar2;
            } else {
                ak akVar4 = (ak) this.nA.get(0);
                akVar4.aU();
                return akVar4;
            }
        }
    }

    public boolean b(ak akVar) {
        boolean z;
        synchronized (this.mH) {
            Iterator it = this.nA.iterator();
            while (true) {
                if (!it.hasNext()) {
                    z = false;
                    break;
                }
                ak akVar2 = (ak) it.next();
                if (akVar != akVar2 && akVar2.aT().equals(akVar.aT())) {
                    this.nA.remove(akVar);
                    z = true;
                    break;
                }
            }
        }
        return z;
    }

    public void c(ak akVar) {
        synchronized (this.mH) {
            if (this.nA.size() >= 10) {
                gr.S("Queue is full, current size = " + this.nA.size());
                this.nA.remove(0);
            }
            int i = this.nz;
            this.nz = i + 1;
            akVar.c(i);
            this.nA.add(akVar);
        }
    }
}
