package com.google.android.gms.internal;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.SystemClock;
import java.util.LinkedList;
import java.util.List;

@ey
public class eh {
    private static final Object mH = new Object();
    /* access modifiers changed from: private */
    public static final String sZ = String.format("CREATE TABLE IF NOT EXISTS %s ( %s INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, %s TEXT NOT NULL, %s TEXT NOT NULL, %s INTEGER)", new Object[]{"InAppPurchase", "purchase_id", "product_id", "developer_payload", "record_time"});
    private static eh tb;
    private final a ta;

    public class a extends SQLiteOpenHelper {
        public a(Context context, String str) {
            super(context, str, null, 4);
        }

        public void onCreate(SQLiteDatabase db) {
            db.execSQL(eh.sZ);
        }

        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            gr.U("Database updated from version " + oldVersion + " to version " + newVersion);
            db.execSQL("DROP TABLE IF EXISTS InAppPurchase");
            onCreate(db);
        }
    }

    private eh(Context context) {
        this.ta = new a(context, "google_inapp_purchase.db");
    }

    public static eh j(Context context) {
        eh ehVar;
        synchronized (mH) {
            if (tb == null) {
                tb = new eh(context);
            }
            ehVar = tb;
        }
        return ehVar;
    }

    public ef a(Cursor cursor) {
        if (cursor == null) {
            return null;
        }
        return new ef(cursor.getLong(0), cursor.getString(1), cursor.getString(2));
    }

    public void a(ef efVar) {
        if (efVar != null) {
            synchronized (mH) {
                SQLiteDatabase writableDatabase = getWritableDatabase();
                if (writableDatabase != null) {
                    writableDatabase.delete("InAppPurchase", String.format("%s = %d", new Object[]{"purchase_id", Long.valueOf(efVar.sT)}), null);
                }
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:?, code lost:
        return;
     */
    public void b(ef efVar) {
        if (efVar != null) {
            synchronized (mH) {
                SQLiteDatabase writableDatabase = getWritableDatabase();
                if (writableDatabase != null) {
                    ContentValues contentValues = new ContentValues();
                    contentValues.put("product_id", efVar.sV);
                    contentValues.put("developer_payload", efVar.sU);
                    contentValues.put("record_time", Long.valueOf(SystemClock.elapsedRealtime()));
                    efVar.sT = writableDatabase.insert("InAppPurchase", null, contentValues);
                    if (((long) getRecordCount()) > 20000) {
                        cA();
                    }
                }
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:32:0x005a A[Catch:{ SQLiteException -> 0x0034, all -> 0x0056 }] */
    public void cA() {
        Cursor cursor;
        synchronized (mH) {
            SQLiteDatabase writableDatabase = getWritableDatabase();
            if (writableDatabase != null) {
                try {
                    cursor = writableDatabase.query("InAppPurchase", null, null, null, null, null, "record_time ASC", "1");
                    if (cursor != null) {
                        try {
                            if (cursor.moveToFirst()) {
                                a(a(cursor));
                            }
                        } catch (SQLiteException e) {
                            e = e;
                            try {
                                gr.W("Error remove oldest record" + e.getMessage());
                                if (cursor != null) {
                                    cursor.close();
                                }
                                return;
                            } catch (Throwable th) {
                                th = th;
                                if (cursor != null) {
                                }
                                throw th;
                            }
                        }
                    }
                    if (cursor != null) {
                        cursor.close();
                    }
                } catch (SQLiteException e2) {
                    e = e2;
                    cursor = null;
                } catch (Throwable th2) {
                    th = th2;
                    cursor = null;
                    if (cursor != null) {
                        cursor.close();
                    }
                    throw th;
                }
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:37:0x0070 A[SYNTHETIC, Splitter:B:37:0x0070] */
    public List<ef> d(long j) {
        Cursor cursor;
        synchronized (mH) {
            LinkedList linkedList = new LinkedList();
            if (j <= 0) {
                return linkedList;
            }
            SQLiteDatabase writableDatabase = getWritableDatabase();
            if (writableDatabase == null) {
                return linkedList;
            }
            try {
                cursor = writableDatabase.query("InAppPurchase", null, null, null, null, null, "record_time ASC", String.valueOf(j));
                try {
                    if (cursor.moveToFirst()) {
                        do {
                            linkedList.add(a(cursor));
                        } while (cursor.moveToNext());
                    }
                    if (cursor != null) {
                        cursor.close();
                    }
                } catch (SQLiteException e) {
                    e = e;
                    try {
                        gr.W("Error extracing purchase info: " + e.getMessage());
                        if (cursor != null) {
                            cursor.close();
                        }
                        return linkedList;
                    } catch (Throwable th) {
                        th = th;
                        if (cursor != null) {
                            cursor.close();
                        }
                        throw th;
                    }
                }
            } catch (SQLiteException e2) {
                e = e2;
                cursor = null;
            } catch (Throwable th2) {
                th = th2;
                cursor = null;
                if (cursor != null) {
                }
                throw th;
            }
        }
    }

    public int getRecordCount() {
        Cursor cursor = null;
        int i = 0;
        synchronized (mH) {
            SQLiteDatabase writableDatabase = getWritableDatabase();
            if (writableDatabase != null) {
                try {
                    Cursor rawQuery = writableDatabase.rawQuery("select count(*) from InAppPurchase", null);
                    if (rawQuery.moveToFirst()) {
                        i = rawQuery.getInt(0);
                        if (rawQuery != null) {
                            rawQuery.close();
                        }
                    } else if (rawQuery != null) {
                        rawQuery.close();
                    }
                } catch (SQLiteException e) {
                    gr.W("Error getting record count" + e.getMessage());
                    if (cursor != null) {
                        cursor.close();
                    }
                } catch (Throwable th) {
                    if (cursor != null) {
                        cursor.close();
                    }
                    throw th;
                }
            }
        }
        return i;
    }

    public SQLiteDatabase getWritableDatabase() {
        try {
            return this.ta.getWritableDatabase();
        } catch (SQLiteException e) {
            gr.W("Error opening writable conversion tracking database");
            return null;
        }
    }
}
