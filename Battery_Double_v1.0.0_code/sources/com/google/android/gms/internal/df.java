package com.google.android.gms.internal;

import android.os.RemoteException;
import com.google.ads.AdRequest.ErrorCode;
import com.google.ads.mediation.MediationBannerAdapter;
import com.google.ads.mediation.MediationBannerListener;
import com.google.ads.mediation.MediationInterstitialAdapter;
import com.google.ads.mediation.MediationInterstitialListener;
import com.google.ads.mediation.MediationServerParameters;
import com.google.ads.mediation.NetworkExtras;

@ey
public final class df<NETWORK_EXTRAS extends NetworkExtras, SERVER_PARAMETERS extends MediationServerParameters> implements MediationBannerListener, MediationInterstitialListener {
    /* access modifiers changed from: private */
    public final da qW;

    public df(da daVar) {
        this.qW = daVar;
    }

    public void onClick(MediationBannerAdapter<?, ?> mediationBannerAdapter) {
        gr.S("Adapter called onClick.");
        if (!gq.dB()) {
            gr.W("onClick must be called on the main UI thread.");
            gq.wR.post(new Runnable() {
                public void run() {
                    try {
                        df.this.qW.onAdClicked();
                    } catch (RemoteException e) {
                        gr.d("Could not call onAdClicked.", e);
                    }
                }
            });
            return;
        }
        try {
            this.qW.onAdClicked();
        } catch (RemoteException e) {
            gr.d("Could not call onAdClicked.", e);
        }
    }

    public void onDismissScreen(MediationBannerAdapter<?, ?> mediationBannerAdapter) {
        gr.S("Adapter called onDismissScreen.");
        if (!gq.dB()) {
            gr.W("onDismissScreen must be called on the main UI thread.");
            gq.wR.post(new Runnable() {
                public void run() {
                    try {
                        df.this.qW.onAdClosed();
                    } catch (RemoteException e) {
                        gr.d("Could not call onAdClosed.", e);
                    }
                }
            });
            return;
        }
        try {
            this.qW.onAdClosed();
        } catch (RemoteException e) {
            gr.d("Could not call onAdClosed.", e);
        }
    }

    public void onDismissScreen(MediationInterstitialAdapter<?, ?> mediationInterstitialAdapter) {
        gr.S("Adapter called onDismissScreen.");
        if (!gq.dB()) {
            gr.W("onDismissScreen must be called on the main UI thread.");
            gq.wR.post(new Runnable() {
                public void run() {
                    try {
                        df.this.qW.onAdClosed();
                    } catch (RemoteException e) {
                        gr.d("Could not call onAdClosed.", e);
                    }
                }
            });
            return;
        }
        try {
            this.qW.onAdClosed();
        } catch (RemoteException e) {
            gr.d("Could not call onAdClosed.", e);
        }
    }

    public void onFailedToReceiveAd(MediationBannerAdapter<?, ?> mediationBannerAdapter, final ErrorCode errorCode) {
        gr.S("Adapter called onFailedToReceiveAd with error. " + errorCode);
        if (!gq.dB()) {
            gr.W("onFailedToReceiveAd must be called on the main UI thread.");
            gq.wR.post(new Runnable() {
                public void run() {
                    try {
                        df.this.qW.onAdFailedToLoad(dg.a(errorCode));
                    } catch (RemoteException e) {
                        gr.d("Could not call onAdFailedToLoad.", e);
                    }
                }
            });
            return;
        }
        try {
            this.qW.onAdFailedToLoad(dg.a(errorCode));
        } catch (RemoteException e) {
            gr.d("Could not call onAdFailedToLoad.", e);
        }
    }

    public void onFailedToReceiveAd(MediationInterstitialAdapter<?, ?> mediationInterstitialAdapter, final ErrorCode errorCode) {
        gr.S("Adapter called onFailedToReceiveAd with error " + errorCode + ".");
        if (!gq.dB()) {
            gr.W("onFailedToReceiveAd must be called on the main UI thread.");
            gq.wR.post(new Runnable() {
                public void run() {
                    try {
                        df.this.qW.onAdFailedToLoad(dg.a(errorCode));
                    } catch (RemoteException e) {
                        gr.d("Could not call onAdFailedToLoad.", e);
                    }
                }
            });
            return;
        }
        try {
            this.qW.onAdFailedToLoad(dg.a(errorCode));
        } catch (RemoteException e) {
            gr.d("Could not call onAdFailedToLoad.", e);
        }
    }

    public void onLeaveApplication(MediationBannerAdapter<?, ?> mediationBannerAdapter) {
        gr.S("Adapter called onLeaveApplication.");
        if (!gq.dB()) {
            gr.W("onLeaveApplication must be called on the main UI thread.");
            gq.wR.post(new Runnable() {
                public void run() {
                    try {
                        df.this.qW.onAdLeftApplication();
                    } catch (RemoteException e) {
                        gr.d("Could not call onAdLeftApplication.", e);
                    }
                }
            });
            return;
        }
        try {
            this.qW.onAdLeftApplication();
        } catch (RemoteException e) {
            gr.d("Could not call onAdLeftApplication.", e);
        }
    }

    public void onLeaveApplication(MediationInterstitialAdapter<?, ?> mediationInterstitialAdapter) {
        gr.S("Adapter called onLeaveApplication.");
        if (!gq.dB()) {
            gr.W("onLeaveApplication must be called on the main UI thread.");
            gq.wR.post(new Runnable() {
                public void run() {
                    try {
                        df.this.qW.onAdLeftApplication();
                    } catch (RemoteException e) {
                        gr.d("Could not call onAdLeftApplication.", e);
                    }
                }
            });
            return;
        }
        try {
            this.qW.onAdLeftApplication();
        } catch (RemoteException e) {
            gr.d("Could not call onAdLeftApplication.", e);
        }
    }

    public void onPresentScreen(MediationBannerAdapter<?, ?> mediationBannerAdapter) {
        gr.S("Adapter called onPresentScreen.");
        if (!gq.dB()) {
            gr.W("onPresentScreen must be called on the main UI thread.");
            gq.wR.post(new Runnable() {
                public void run() {
                    try {
                        df.this.qW.onAdOpened();
                    } catch (RemoteException e) {
                        gr.d("Could not call onAdOpened.", e);
                    }
                }
            });
            return;
        }
        try {
            this.qW.onAdOpened();
        } catch (RemoteException e) {
            gr.d("Could not call onAdOpened.", e);
        }
    }

    public void onPresentScreen(MediationInterstitialAdapter<?, ?> mediationInterstitialAdapter) {
        gr.S("Adapter called onPresentScreen.");
        if (!gq.dB()) {
            gr.W("onPresentScreen must be called on the main UI thread.");
            gq.wR.post(new Runnable() {
                public void run() {
                    try {
                        df.this.qW.onAdOpened();
                    } catch (RemoteException e) {
                        gr.d("Could not call onAdOpened.", e);
                    }
                }
            });
            return;
        }
        try {
            this.qW.onAdOpened();
        } catch (RemoteException e) {
            gr.d("Could not call onAdOpened.", e);
        }
    }

    public void onReceivedAd(MediationBannerAdapter<?, ?> mediationBannerAdapter) {
        gr.S("Adapter called onReceivedAd.");
        if (!gq.dB()) {
            gr.W("onReceivedAd must be called on the main UI thread.");
            gq.wR.post(new Runnable() {
                public void run() {
                    try {
                        df.this.qW.onAdLoaded();
                    } catch (RemoteException e) {
                        gr.d("Could not call onAdLoaded.", e);
                    }
                }
            });
            return;
        }
        try {
            this.qW.onAdLoaded();
        } catch (RemoteException e) {
            gr.d("Could not call onAdLoaded.", e);
        }
    }

    public void onReceivedAd(MediationInterstitialAdapter<?, ?> mediationInterstitialAdapter) {
        gr.S("Adapter called onReceivedAd.");
        if (!gq.dB()) {
            gr.W("onReceivedAd must be called on the main UI thread.");
            gq.wR.post(new Runnable() {
                public void run() {
                    try {
                        df.this.qW.onAdLoaded();
                    } catch (RemoteException e) {
                        gr.d("Could not call onAdLoaded.", e);
                    }
                }
            });
            return;
        }
        try {
            this.qW.onAdLoaded();
        } catch (RemoteException e) {
            gr.d("Could not call onAdLoaded.", e);
        }
    }
}
