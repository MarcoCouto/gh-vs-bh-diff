package com.google.android.gms.internal;

import android.support.v4.view.MotionEventCompat;
import java.security.MessageDigest;

public class ar extends ao {
    private MessageDigest nZ;

    /* access modifiers changed from: 0000 */
    public byte[] a(String[] strArr) {
        byte[] bArr = new byte[strArr.length];
        for (int i = 0; i < strArr.length; i++) {
            bArr[i] = e(aq.o(strArr[i]));
        }
        return bArr;
    }

    /* access modifiers changed from: 0000 */
    public byte e(int i) {
        return (byte) ((((i & MotionEventCompat.ACTION_MASK) ^ ((65280 & i) >> 8)) ^ ((16711680 & i) >> 16)) ^ ((-16777216 & i) >> 24));
    }

    public byte[] l(String str) {
        byte[] bArr;
        byte[] a = a(str.split(" "));
        this.nZ = bf();
        synchronized (this.mH) {
            if (this.nZ == null) {
                bArr = new byte[0];
            } else {
                this.nZ.reset();
                this.nZ.update(a);
                byte[] digest = this.nZ.digest();
                int i = 4;
                if (digest.length <= 4) {
                    i = digest.length;
                }
                bArr = new byte[i];
                System.arraycopy(digest, 0, bArr, 0, bArr.length);
            }
        }
        return bArr;
    }
}
