package com.google.android.gms.internal;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.List;

public interface oq extends IInterface {

    public static abstract class a extends Binder implements oq {

        /* renamed from: com.google.android.gms.internal.oq$a$a reason: collision with other inner class name */
        private static class C0081a implements oq {
            private IBinder le;

            C0081a(IBinder iBinder) {
                this.le = iBinder;
            }

            public void a(String str, ov ovVar, or orVar) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.playlog.internal.IPlayLogService");
                    obtain.writeString(str);
                    if (ovVar != null) {
                        obtain.writeInt(1);
                        ovVar.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    if (orVar != null) {
                        obtain.writeInt(1);
                        orVar.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.le.transact(2, obtain, null, 1);
                } finally {
                    obtain.recycle();
                }
            }

            public void a(String str, ov ovVar, List<or> list) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.playlog.internal.IPlayLogService");
                    obtain.writeString(str);
                    if (ovVar != null) {
                        obtain.writeInt(1);
                        ovVar.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeTypedList(list);
                    this.le.transact(3, obtain, null, 1);
                } finally {
                    obtain.recycle();
                }
            }

            public void a(String str, ov ovVar, byte[] bArr) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.playlog.internal.IPlayLogService");
                    obtain.writeString(str);
                    if (ovVar != null) {
                        obtain.writeInt(1);
                        ovVar.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeByteArray(bArr);
                    this.le.transact(4, obtain, null, 1);
                } finally {
                    obtain.recycle();
                }
            }

            public IBinder asBinder() {
                return this.le;
            }
        }

        public static oq bI(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.playlog.internal.IPlayLogService");
            return (queryLocalInterface == null || !(queryLocalInterface instanceof oq)) ? new C0081a(iBinder) : (oq) queryLocalInterface;
        }

        /* JADX WARNING: type inference failed for: r1v0 */
        /* JADX WARNING: type inference failed for: r1v1, types: [com.google.android.gms.internal.ov] */
        /* JADX WARNING: type inference failed for: r1v3, types: [com.google.android.gms.internal.ov] */
        /* JADX WARNING: type inference failed for: r1v4, types: [com.google.android.gms.internal.ov] */
        /* JADX WARNING: type inference failed for: r1v6, types: [com.google.android.gms.internal.ov] */
        /* JADX WARNING: type inference failed for: r1v7, types: [com.google.android.gms.internal.or] */
        /* JADX WARNING: type inference failed for: r1v9, types: [com.google.android.gms.internal.or] */
        /* JADX WARNING: type inference failed for: r1v10 */
        /* JADX WARNING: type inference failed for: r1v11 */
        /* JADX WARNING: type inference failed for: r1v12 */
        /* JADX WARNING: Multi-variable type inference failed. Error: jadx.core.utils.exceptions.JadxRuntimeException: No candidate types for var: r1v0
  assigns: [?[int, float, boolean, short, byte, char, OBJECT, ARRAY], com.google.android.gms.internal.ov, com.google.android.gms.internal.or]
  uses: [com.google.android.gms.internal.ov, com.google.android.gms.internal.or]
  mth insns count: 45
        	at jadx.core.dex.visitors.typeinference.TypeSearch.fillTypeCandidates(TypeSearch.java:237)
        	at java.util.ArrayList.forEach(Unknown Source)
        	at jadx.core.dex.visitors.typeinference.TypeSearch.run(TypeSearch.java:53)
        	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.runMultiVariableSearch(TypeInferenceVisitor.java:99)
        	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.visit(TypeInferenceVisitor.java:92)
        	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:27)
        	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$1(DepthTraversal.java:14)
        	at java.util.ArrayList.forEach(Unknown Source)
        	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
        	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$0(DepthTraversal.java:13)
        	at java.util.ArrayList.forEach(Unknown Source)
        	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:13)
        	at jadx.core.ProcessClass.process(ProcessClass.java:30)
        	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
        	at jadx.api.JavaClass.decompile(JavaClass.java:62)
        	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
         */
        /* JADX WARNING: Unknown variable types count: 4 */
        public boolean onTransact(int code, Parcel data, Parcel reply, int flags) throws RemoteException {
            ? r1 = 0;
            switch (code) {
                case 2:
                    data.enforceInterface("com.google.android.gms.playlog.internal.IPlayLogService");
                    String readString = data.readString();
                    ov ovVar = data.readInt() != 0 ? ov.CREATOR.createFromParcel(data) : null;
                    if (data.readInt() != 0) {
                        r1 = or.CREATOR.createFromParcel(data);
                    }
                    a(readString, ovVar, (or) r1);
                    return true;
                case 3:
                    data.enforceInterface("com.google.android.gms.playlog.internal.IPlayLogService");
                    String readString2 = data.readString();
                    if (data.readInt() != 0) {
                        r1 = ov.CREATOR.createFromParcel(data);
                    }
                    a(readString2, (ov) r1, (List<or>) data.createTypedArrayList(or.CREATOR));
                    return true;
                case 4:
                    data.enforceInterface("com.google.android.gms.playlog.internal.IPlayLogService");
                    String readString3 = data.readString();
                    if (data.readInt() != 0) {
                        r1 = ov.CREATOR.createFromParcel(data);
                    }
                    a(readString3, (ov) r1, data.createByteArray());
                    return true;
                case 1598968902:
                    reply.writeString("com.google.android.gms.playlog.internal.IPlayLogService");
                    return true;
                default:
                    return super.onTransact(code, data, reply, flags);
            }
        }
    }

    void a(String str, ov ovVar, or orVar) throws RemoteException;

    void a(String str, ov ovVar, List<or> list) throws RemoteException;

    void a(String str, ov ovVar, byte[] bArr) throws RemoteException;
}
