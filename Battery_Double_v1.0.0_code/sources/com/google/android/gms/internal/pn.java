package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.a.C0004a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.internal.pi.b.C0082b;
import java.util.HashSet;
import java.util.Set;

public class pn implements Creator<C0082b> {
    static void a(C0082b bVar, Parcel parcel, int i) {
        int H = b.H(parcel);
        Set<Integer> set = bVar.aon;
        if (set.contains(Integer.valueOf(1))) {
            b.c(parcel, 1, bVar.CK);
        }
        if (set.contains(Integer.valueOf(2))) {
            b.c(parcel, 2, bVar.lj);
        }
        if (set.contains(Integer.valueOf(3))) {
            b.a(parcel, 3, bVar.vf, true);
        }
        if (set.contains(Integer.valueOf(4))) {
            b.c(parcel, 4, bVar.li);
        }
        b.H(parcel, H);
    }

    /* renamed from: dD */
    public C0082b createFromParcel(Parcel parcel) {
        int i = 0;
        int G = a.G(parcel);
        HashSet hashSet = new HashSet();
        String str = null;
        int i2 = 0;
        int i3 = 0;
        while (parcel.dataPosition() < G) {
            int F = a.F(parcel);
            switch (a.aH(F)) {
                case 1:
                    i3 = a.g(parcel, F);
                    hashSet.add(Integer.valueOf(1));
                    break;
                case 2:
                    i2 = a.g(parcel, F);
                    hashSet.add(Integer.valueOf(2));
                    break;
                case 3:
                    str = a.o(parcel, F);
                    hashSet.add(Integer.valueOf(3));
                    break;
                case 4:
                    i = a.g(parcel, F);
                    hashSet.add(Integer.valueOf(4));
                    break;
                default:
                    a.b(parcel, F);
                    break;
            }
        }
        if (parcel.dataPosition() == G) {
            return new C0082b(hashSet, i3, i2, str, i);
        }
        throw new C0004a("Overread allowed size end=" + G, parcel);
    }

    /* renamed from: fB */
    public C0082b[] newArray(int i) {
        return new C0082b[i];
    }
}
