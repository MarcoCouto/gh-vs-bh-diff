package com.google.android.gms.internal;

import android.content.Context;
import android.os.AsyncTask;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.ads.identifier.AdvertisingIdClient.Info;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import java.io.IOException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public class j extends i {
    /* access modifiers changed from: private */
    public static AdvertisingIdClient kO;
    /* access modifiers changed from: private */
    public static CountDownLatch kP = new CountDownLatch(1);
    /* access modifiers changed from: private */
    public static boolean kQ;

    class a {
        private String kR;
        private boolean kS;

        public a(String str, boolean z) {
            this.kR = str;
            this.kS = z;
        }

        public String getId() {
            return this.kR;
        }

        public boolean isLimitAdTrackingEnabled() {
            return this.kS;
        }
    }

    protected j(Context context, m mVar, n nVar) {
        super(context, mVar, nVar);
    }

    public static j a(String str, Context context) {
        e eVar = new e();
        a(str, context, eVar);
        synchronized (j.class) {
            if (kO == null) {
                kO = new AdvertisingIdClient(context);
                new AsyncTask<Void, Void, Void>() {
                    /* access modifiers changed from: protected */
                    /* renamed from: a */
                    public Void doInBackground(Void... voidArr) {
                        try {
                            j.kO.start();
                        } catch (GooglePlayServicesNotAvailableException e) {
                            j.kQ = true;
                            j.kO = null;
                        } catch (IOException e2) {
                            j.kO = null;
                        } catch (GooglePlayServicesRepairableException e3) {
                            j.kO = null;
                        }
                        j.kP.countDown();
                        return null;
                    }
                }.execute(new Void[0]);
            }
        }
        return new j(context, eVar, new p(239));
    }

    /* access modifiers changed from: protected */
    public void b(Context context) {
        super.b(context);
        try {
            if (kQ) {
                a(24, d(context));
                return;
            }
            a z = z();
            a(28, z.isLimitAdTrackingEnabled() ? 1 : 0);
            String id = z.getId();
            if (id != null) {
                a(26, 5);
                a(24, id);
            }
        } catch (a | IOException e) {
        }
    }

    /* access modifiers changed from: 0000 */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x003c, code lost:
        r2 = r3.getId();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0040, code lost:
        if (r2 == null) goto L_0x0094;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0048, code lost:
        if (r2.matches("^[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}$") == false) goto L_0x0094;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x004a, code lost:
        r4 = new byte[16];
        r1 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0051, code lost:
        if (r0 >= r2.length()) goto L_0x0082;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0055, code lost:
        if (r0 == 8) goto L_0x0063;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0059, code lost:
        if (r0 == 13) goto L_0x0063;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x005d, code lost:
        if (r0 == 18) goto L_0x0063;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0061, code lost:
        if (r0 != 23) goto L_0x0065;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x0063, code lost:
        r0 = r0 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x0065, code lost:
        r4[r1] = (byte) ((java.lang.Character.digit(r2.charAt(r0), 16) << 4) + java.lang.Character.digit(r2.charAt(r0 + 1), 16));
        r1 = r1 + 1;
        r0 = r0 + 2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x0082, code lost:
        r0 = r8.ky.a(r4, true);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x0094, code lost:
        r0 = r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:?, code lost:
        return new com.google.android.gms.internal.j.a(r8, r0, r3.isLimitAdTrackingEnabled());
     */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    public a z() throws IOException {
        int i = 0;
        synchronized (j.class) {
            try {
                if (!kP.await(2, TimeUnit.SECONDS)) {
                    a aVar = new a(null, false);
                    return aVar;
                } else if (kO == null) {
                    a aVar2 = new a(null, false);
                    return aVar2;
                } else {
                    Info info = kO.getInfo();
                }
            } catch (InterruptedException e) {
                return new a(null, false);
            }
        }
    }
}
