package com.google.android.gms.internal;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.RemoteException;
import android.text.TextUtils;
import com.google.android.gms.cast.ApplicationMetadata;
import com.google.android.gms.cast.Cast;
import com.google.android.gms.cast.Cast.ApplicationConnectionResult;
import com.google.android.gms.cast.Cast.Listener;
import com.google.android.gms.cast.Cast.MessageReceivedCallback;
import com.google.android.gms.cast.CastDevice;
import com.google.android.gms.cast.LaunchOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.internal.jl.e;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

public final class im extends jl<iq> {
    /* access modifiers changed from: private */
    public static final Object HB = new Object();
    /* access modifiers changed from: private */
    public static final Object HC = new Object();
    /* access modifiers changed from: private */
    public static final is Hh = new is("CastClientImpl");
    /* access modifiers changed from: private */
    public final Listener FD;
    private double Gp;
    private boolean Gq;
    /* access modifiers changed from: private */
    public com.google.android.gms.common.api.BaseImplementation.b<Status> HA;
    /* access modifiers changed from: private */
    public ApplicationMetadata Hi;
    /* access modifiers changed from: private */
    public final CastDevice Hj;
    /* access modifiers changed from: private */
    public final Map<String, MessageReceivedCallback> Hk = new HashMap();
    private final long Hl;
    private c Hm;
    private String Hn;
    private boolean Ho;
    private boolean Hp;
    private boolean Hq;
    private int Hr;
    private int Hs;
    private final AtomicLong Ht = new AtomicLong(0);
    /* access modifiers changed from: private */
    public String Hu;
    /* access modifiers changed from: private */
    public String Hv;
    private Bundle Hw;
    /* access modifiers changed from: private */
    public final Map<Long, com.google.android.gms.common.api.BaseImplementation.b<Status>> Hx = new HashMap();
    private final b Hy;
    /* access modifiers changed from: private */
    public com.google.android.gms.common.api.BaseImplementation.b<ApplicationConnectionResult> Hz;
    /* access modifiers changed from: private */
    public final Handler mHandler;

    private static final class a implements ApplicationConnectionResult {
        private final Status Eb;
        private final ApplicationMetadata HD;
        private final String HE;
        private final boolean HF;
        private final String vZ;

        public a(Status status) {
            this(status, null, null, null, false);
        }

        public a(Status status, ApplicationMetadata applicationMetadata, String str, String str2, boolean z) {
            this.Eb = status;
            this.HD = applicationMetadata;
            this.HE = str;
            this.vZ = str2;
            this.HF = z;
        }

        public ApplicationMetadata getApplicationMetadata() {
            return this.HD;
        }

        public String getApplicationStatus() {
            return this.HE;
        }

        public String getSessionId() {
            return this.vZ;
        }

        public Status getStatus() {
            return this.Eb;
        }

        public boolean getWasLaunched() {
            return this.HF;
        }
    }

    private class b implements OnConnectionFailedListener {
        private b() {
        }

        public void onConnectionFailed(ConnectionResult result) {
            im.this.ga();
        }
    }

    private class c extends com.google.android.gms.internal.ir.a {
        private final AtomicBoolean HH;

        private c() {
            this.HH = new AtomicBoolean(false);
        }

        private boolean ah(int i) {
            synchronized (im.HC) {
                if (im.this.HA == null) {
                    return false;
                }
                im.this.HA.b(new Status(i));
                im.this.HA = null;
                return true;
            }
        }

        private void c(long j, int i) {
            com.google.android.gms.common.api.BaseImplementation.b bVar;
            synchronized (im.this.Hx) {
                bVar = (com.google.android.gms.common.api.BaseImplementation.b) im.this.Hx.remove(Long.valueOf(j));
            }
            if (bVar != null) {
                bVar.b(new Status(i));
            }
        }

        public void a(ApplicationMetadata applicationMetadata, String str, String str2, boolean z) {
            if (!this.HH.get()) {
                im.this.Hi = applicationMetadata;
                im.this.Hu = applicationMetadata.getApplicationId();
                im.this.Hv = str2;
                synchronized (im.HB) {
                    if (im.this.Hz != null) {
                        im.this.Hz.b(new a(new Status(0), applicationMetadata, str, str2, z));
                        im.this.Hz = null;
                    }
                }
            }
        }

        public void a(String str, double d, boolean z) {
            im.Hh.b("Deprecated callback: \"onStatusreceived\"", new Object[0]);
        }

        public void a(String str, long j) {
            if (!this.HH.get()) {
                c(j, 0);
            }
        }

        public void a(String str, long j, int i) {
            if (!this.HH.get()) {
                c(j, i);
            }
        }

        public void ad(int i) {
            if (gg()) {
                im.Hh.b("ICastDeviceControllerListener.onDisconnected: %d", Integer.valueOf(i));
                if (i != 0) {
                    im.this.aD(2);
                }
            }
        }

        public void ae(int i) {
            if (!this.HH.get()) {
                synchronized (im.HB) {
                    if (im.this.Hz != null) {
                        im.this.Hz.b(new a(new Status(i)));
                        im.this.Hz = null;
                    }
                }
            }
        }

        public void af(int i) {
            if (!this.HH.get()) {
                ah(i);
            }
        }

        public void ag(int i) {
            if (!this.HH.get()) {
                ah(i);
            }
        }

        public void b(final ij ijVar) {
            if (!this.HH.get()) {
                im.Hh.b("onApplicationStatusChanged", new Object[0]);
                im.this.mHandler.post(new Runnable() {
                    public void run() {
                        im.this.a(ijVar);
                    }
                });
            }
        }

        public void b(final io ioVar) {
            if (!this.HH.get()) {
                im.Hh.b("onDeviceStatusChanged", new Object[0]);
                im.this.mHandler.post(new Runnable() {
                    public void run() {
                        im.this.a(ioVar);
                    }
                });
            }
        }

        public void b(String str, byte[] bArr) {
            if (!this.HH.get()) {
                im.Hh.b("IGNORING: Receive (type=binary, ns=%s) <%d bytes>", str, Integer.valueOf(bArr.length));
            }
        }

        public boolean gg() {
            if (this.HH.getAndSet(true)) {
                return false;
            }
            im.this.fW();
            return true;
        }

        public boolean gh() {
            return this.HH.get();
        }

        public void j(final String str, final String str2) {
            if (!this.HH.get()) {
                im.Hh.b("Receive (type=text, ns=%s) %s", str, str2);
                im.this.mHandler.post(new Runnable() {
                    public void run() {
                        MessageReceivedCallback messageReceivedCallback;
                        synchronized (im.this.Hk) {
                            messageReceivedCallback = (MessageReceivedCallback) im.this.Hk.get(str);
                        }
                        if (messageReceivedCallback != null) {
                            messageReceivedCallback.onMessageReceived(im.this.Hj, str, str2);
                            return;
                        }
                        im.Hh.b("Discarded message for unknown namespace '%s'", str);
                    }
                });
            }
        }

        public void onApplicationDisconnected(final int statusCode) {
            if (!this.HH.get()) {
                im.this.Hu = null;
                im.this.Hv = null;
                ah(statusCode);
                if (im.this.FD != null) {
                    im.this.mHandler.post(new Runnable() {
                        public void run() {
                            if (im.this.FD != null) {
                                im.this.FD.onApplicationDisconnected(statusCode);
                            }
                        }
                    });
                }
            }
        }
    }

    public im(Context context, Looper looper, CastDevice castDevice, long j, Listener listener, ConnectionCallbacks connectionCallbacks, OnConnectionFailedListener onConnectionFailedListener) {
        super(context, looper, connectionCallbacks, onConnectionFailedListener, null);
        this.Hj = castDevice;
        this.FD = listener;
        this.Hl = j;
        this.mHandler = new Handler(looper);
        fW();
        this.Hy = new b();
        registerConnectionFailedListener((OnConnectionFailedListener) this.Hy);
    }

    /* access modifiers changed from: private */
    public void a(ij ijVar) {
        boolean z;
        String fT = ijVar.fT();
        if (!in.a(fT, this.Hn)) {
            this.Hn = fT;
            z = true;
        } else {
            z = false;
        }
        Hh.b("hasChanged=%b, mFirstApplicationStatusUpdate=%b", Boolean.valueOf(z), Boolean.valueOf(this.Ho));
        if (this.FD != null && (z || this.Ho)) {
            this.FD.onApplicationStatusChanged();
        }
        this.Ho = false;
    }

    /* access modifiers changed from: private */
    public void a(io ioVar) {
        boolean z;
        boolean z2;
        boolean z3;
        ApplicationMetadata applicationMetadata = ioVar.getApplicationMetadata();
        if (!in.a(applicationMetadata, this.Hi)) {
            this.Hi = applicationMetadata;
            this.FD.onApplicationMetadataChanged(this.Hi);
        }
        double fZ = ioVar.fZ();
        if (fZ == Double.NaN || fZ == this.Gp) {
            z = false;
        } else {
            this.Gp = fZ;
            z = true;
        }
        boolean gi = ioVar.gi();
        if (gi != this.Gq) {
            this.Gq = gi;
            z = true;
        }
        Hh.b("hasVolumeChanged=%b, mFirstDeviceStatusUpdate=%b", Boolean.valueOf(z), Boolean.valueOf(this.Hp));
        if (this.FD != null && (z || this.Hp)) {
            this.FD.onVolumeChanged();
        }
        int gj = ioVar.gj();
        if (gj != this.Hr) {
            this.Hr = gj;
            z2 = true;
        } else {
            z2 = false;
        }
        Hh.b("hasActiveInputChanged=%b, mFirstDeviceStatusUpdate=%b", Boolean.valueOf(z2), Boolean.valueOf(this.Hp));
        if (this.FD != null && (z2 || this.Hp)) {
            this.FD.X(this.Hr);
        }
        int gk = ioVar.gk();
        if (gk != this.Hs) {
            this.Hs = gk;
            z3 = true;
        } else {
            z3 = false;
        }
        Hh.b("hasStandbyStateChanged=%b, mFirstDeviceStatusUpdate=%b", Boolean.valueOf(z3), Boolean.valueOf(this.Hp));
        if (this.FD != null && (z3 || this.Hp)) {
            this.FD.Y(this.Hs);
        }
        this.Hp = false;
    }

    private void c(com.google.android.gms.common.api.BaseImplementation.b<ApplicationConnectionResult> bVar) {
        synchronized (HB) {
            if (this.Hz != null) {
                this.Hz.b(new a(new Status(2002)));
            }
            this.Hz = bVar;
        }
    }

    private void e(com.google.android.gms.common.api.BaseImplementation.b<Status> bVar) {
        synchronized (HC) {
            if (this.HA != null) {
                bVar.b(new Status(2001));
            } else {
                this.HA = bVar;
            }
        }
    }

    /* access modifiers changed from: private */
    public void fW() {
        this.Hq = false;
        this.Hr = -1;
        this.Hs = -1;
        this.Hi = null;
        this.Hn = null;
        this.Gp = 0.0d;
        this.Gq = false;
    }

    /* access modifiers changed from: private */
    public void ga() {
        Hh.b("removing all MessageReceivedCallbacks", new Object[0]);
        synchronized (this.Hk) {
            this.Hk.clear();
        }
    }

    private void gb() throws IllegalStateException {
        if (!this.Hq || this.Hm == null || this.Hm.gh()) {
            throw new IllegalStateException("Not connected to a device");
        }
    }

    public void I(boolean z) throws IllegalStateException, RemoteException {
        ((iq) hw()).a(z, this.Gp, this.Gq);
    }

    /* access modifiers changed from: protected */
    /* renamed from: L */
    public iq l(IBinder iBinder) {
        return com.google.android.gms.internal.iq.a.M(iBinder);
    }

    public void a(double d) throws IllegalArgumentException, IllegalStateException, RemoteException {
        if (Double.isInfinite(d) || Double.isNaN(d)) {
            throw new IllegalArgumentException("Volume cannot be " + d);
        }
        ((iq) hw()).a(d, this.Gp, this.Gq);
    }

    /* access modifiers changed from: protected */
    public void a(int i, IBinder iBinder, Bundle bundle) {
        Hh.b("in onPostInitHandler; statusCode=%d", Integer.valueOf(i));
        if (i == 0 || i == 1001) {
            this.Hq = true;
            this.Ho = true;
            this.Hp = true;
        } else {
            this.Hq = false;
        }
        if (i == 1001) {
            this.Hw = new Bundle();
            this.Hw.putBoolean(Cast.EXTRA_APP_NO_LONGER_RUNNING, true);
            i = 0;
        }
        super.a(i, iBinder, bundle);
    }

    /* access modifiers changed from: protected */
    public void a(jt jtVar, e eVar) throws RemoteException {
        Bundle bundle = new Bundle();
        Hh.b("getServiceFromBroker(): mLastApplicationId=%s, mLastSessionId=%s", this.Hu, this.Hv);
        this.Hj.putInBundle(bundle);
        bundle.putLong("com.google.android.gms.cast.EXTRA_CAST_FLAGS", this.Hl);
        if (this.Hu != null) {
            bundle.putString("last_application_id", this.Hu);
            if (this.Hv != null) {
                bundle.putString("last_session_id", this.Hv);
            }
        }
        this.Hm = new c();
        jtVar.a((js) eVar, (int) GooglePlayServicesUtil.GOOGLE_PLAY_SERVICES_VERSION_CODE, getContext().getPackageName(), this.Hm.asBinder(), bundle);
    }

    public void a(String str, MessageReceivedCallback messageReceivedCallback) throws IllegalArgumentException, IllegalStateException, RemoteException {
        in.aF(str);
        aE(str);
        if (messageReceivedCallback != null) {
            synchronized (this.Hk) {
                this.Hk.put(str, messageReceivedCallback);
            }
            ((iq) hw()).aI(str);
        }
    }

    public void a(String str, LaunchOptions launchOptions, com.google.android.gms.common.api.BaseImplementation.b<ApplicationConnectionResult> bVar) throws IllegalStateException, RemoteException {
        c(bVar);
        ((iq) hw()).a(str, launchOptions);
    }

    public void a(String str, com.google.android.gms.common.api.BaseImplementation.b<Status> bVar) throws IllegalStateException, RemoteException {
        e(bVar);
        ((iq) hw()).aH(str);
    }

    public void a(String str, String str2, com.google.android.gms.common.api.BaseImplementation.b<Status> bVar) throws IllegalArgumentException, IllegalStateException, RemoteException {
        if (TextUtils.isEmpty(str2)) {
            throw new IllegalArgumentException("The message payload cannot be null or empty");
        } else if (str2.length() > 65536) {
            throw new IllegalArgumentException("Message exceeds maximum size");
        } else {
            in.aF(str);
            gb();
            long incrementAndGet = this.Ht.incrementAndGet();
            try {
                this.Hx.put(Long.valueOf(incrementAndGet), bVar);
                ((iq) hw()).a(str, str2, incrementAndGet);
            } catch (Throwable th) {
                this.Hx.remove(Long.valueOf(incrementAndGet));
                throw th;
            }
        }
    }

    public void a(String str, boolean z, com.google.android.gms.common.api.BaseImplementation.b<ApplicationConnectionResult> bVar) throws IllegalStateException, RemoteException {
        c(bVar);
        ((iq) hw()).g(str, z);
    }

    public void aE(String str) throws IllegalArgumentException, RemoteException {
        MessageReceivedCallback messageReceivedCallback;
        if (TextUtils.isEmpty(str)) {
            throw new IllegalArgumentException("Channel namespace cannot be null or empty");
        }
        synchronized (this.Hk) {
            messageReceivedCallback = (MessageReceivedCallback) this.Hk.remove(str);
        }
        if (messageReceivedCallback != null) {
            try {
                ((iq) hw()).aJ(str);
            } catch (IllegalStateException e) {
                Hh.a(e, "Error unregistering namespace (%s): %s", str, e.getMessage());
            }
        }
    }

    public void b(String str, String str2, com.google.android.gms.common.api.BaseImplementation.b<ApplicationConnectionResult> bVar) throws IllegalStateException, RemoteException {
        c(bVar);
        ((iq) hw()).k(str, str2);
    }

    /* access modifiers changed from: protected */
    public String bK() {
        return "com.google.android.gms.cast.service.BIND_CAST_DEVICE_CONTROLLER_SERVICE";
    }

    /* access modifiers changed from: protected */
    public String bL() {
        return "com.google.android.gms.cast.internal.ICastDeviceController";
    }

    public void d(com.google.android.gms.common.api.BaseImplementation.b<Status> bVar) throws IllegalStateException, RemoteException {
        e(bVar);
        ((iq) hw()).gl();
    }

    public void disconnect() {
        Hh.b("disconnect(); ServiceListener=%s, isConnected=%b", this.Hm, Boolean.valueOf(isConnected()));
        c cVar = this.Hm;
        this.Hm = null;
        if (cVar == null || !cVar.gg()) {
            Hh.b("already disposed, so short-circuiting", new Object[0]);
            return;
        }
        ga();
        try {
            if (isConnected() || isConnecting()) {
                ((iq) hw()).disconnect();
            }
        } catch (RemoteException e) {
            Hh.a(e, "Error while disconnecting the controller interface: %s", e.getMessage());
        } finally {
            super.disconnect();
        }
    }

    public Bundle fX() {
        if (this.Hw == null) {
            return super.fX();
        }
        Bundle bundle = this.Hw;
        this.Hw = null;
        return bundle;
    }

    public void fY() throws IllegalStateException, RemoteException {
        ((iq) hw()).fY();
    }

    public double fZ() throws IllegalStateException {
        gb();
        return this.Gp;
    }

    public ApplicationMetadata getApplicationMetadata() throws IllegalStateException {
        gb();
        return this.Hi;
    }

    public String getApplicationStatus() throws IllegalStateException {
        gb();
        return this.Hn;
    }

    public boolean isMute() throws IllegalStateException {
        gb();
        return this.Gq;
    }
}
