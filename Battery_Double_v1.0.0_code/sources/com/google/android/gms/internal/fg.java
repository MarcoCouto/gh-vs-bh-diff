package com.google.android.gms.internal;

import android.content.Context;
import android.os.Bundle;
import android.os.DeadObjectException;
import android.os.IBinder;
import android.os.RemoteException;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.internal.fl.a;
import com.google.android.gms.internal.jl.e;

@ey
public class fg extends jl<fl> {
    final int qg;

    public fg(Context context, ConnectionCallbacks connectionCallbacks, OnConnectionFailedListener onConnectionFailedListener, int i) {
        super(context, context.getMainLooper(), connectionCallbacks, onConnectionFailedListener, new String[0]);
        this.qg = i;
    }

    /* access modifiers changed from: protected */
    /* renamed from: B */
    public fl l(IBinder iBinder) {
        return a.C(iBinder);
    }

    /* access modifiers changed from: protected */
    public void a(jt jtVar, e eVar) throws RemoteException {
        jtVar.g(eVar, this.qg, getContext().getPackageName(), new Bundle());
    }

    /* access modifiers changed from: protected */
    public String bK() {
        return "com.google.android.gms.ads.service.START";
    }

    /* access modifiers changed from: protected */
    public String bL() {
        return "com.google.android.gms.ads.internal.request.IAdRequestService";
    }

    public fl cL() throws DeadObjectException {
        return (fl) super.hw();
    }
}
