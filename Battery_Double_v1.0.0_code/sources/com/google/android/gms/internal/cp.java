package com.google.android.gms.internal;

import android.content.Context;
import java.util.Iterator;

@ey
public final class cp {
    private final cy lA;
    private final Context mContext;
    private final Object mH = new Object();
    private final fh qh;
    private final cr qi;
    private boolean qj = false;
    private cu qk;

    public cp(Context context, fh fhVar, cy cyVar, cr crVar) {
        this.mContext = context;
        this.qh = fhVar;
        this.lA = cyVar;
        this.qi = crVar;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0080, code lost:
        r4 = r17.qk.b(r18, r20);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x008e, code lost:
        if (r4.qO != 0) goto L_0x0099;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0090, code lost:
        com.google.android.gms.internal.gr.S("Adapter succeeded.");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x009b, code lost:
        if (r4.qQ == null) goto L_0x0039;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x009d, code lost:
        com.google.android.gms.internal.gq.wR.post(new com.google.android.gms.internal.cp.AnonymousClass1(r17));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:?, code lost:
        return r4;
     */
    public cv a(long j, long j2) {
        gr.S("Starting mediation.");
        for (cq cqVar : this.qi.qu) {
            gr.U("Trying mediation network: " + cqVar.qo);
            Iterator it = cqVar.qp.iterator();
            while (true) {
                if (it.hasNext()) {
                    String str = (String) it.next();
                    synchronized (this.mH) {
                        if (this.qj) {
                            cv cvVar = new cv(-1);
                            return cvVar;
                        }
                        this.qk = new cu(this.mContext, str, this.lA, this.qi, cqVar, this.qh.tL, this.qh.lS, this.qh.lO);
                    }
                }
            }
            while (true) {
            }
        }
        return new cv(1);
    }

    public void cancel() {
        synchronized (this.mH) {
            this.qj = true;
            if (this.qk != null) {
                this.qk.cancel();
            }
        }
    }
}
