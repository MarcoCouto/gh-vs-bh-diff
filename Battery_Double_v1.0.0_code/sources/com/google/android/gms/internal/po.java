package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.a.C0004a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.internal.pi.c;
import java.util.HashSet;
import java.util.Set;

public class po implements Creator<c> {
    static void a(c cVar, Parcel parcel, int i) {
        int H = b.H(parcel);
        Set<Integer> set = cVar.aon;
        if (set.contains(Integer.valueOf(1))) {
            b.c(parcel, 1, cVar.CK);
        }
        if (set.contains(Integer.valueOf(2))) {
            b.a(parcel, 2, cVar.vf, true);
        }
        b.H(parcel, H);
    }

    /* renamed from: dE */
    public c createFromParcel(Parcel parcel) {
        int G = a.G(parcel);
        HashSet hashSet = new HashSet();
        int i = 0;
        String str = null;
        while (parcel.dataPosition() < G) {
            int F = a.F(parcel);
            switch (a.aH(F)) {
                case 1:
                    i = a.g(parcel, F);
                    hashSet.add(Integer.valueOf(1));
                    break;
                case 2:
                    str = a.o(parcel, F);
                    hashSet.add(Integer.valueOf(2));
                    break;
                default:
                    a.b(parcel, F);
                    break;
            }
        }
        if (parcel.dataPosition() == G) {
            return new c(hashSet, i, str);
        }
        throw new C0004a("Overread allowed size end=" + G, parcel);
    }

    /* renamed from: fC */
    public c[] newArray(int i) {
        return new c[i];
    }
}
