package com.google.android.gms.internal;

import android.app.PendingIntent;
import android.os.RemoteException;
import android.util.Log;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingApi;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.GeofencingRequest.Builder;
import com.google.android.gms.location.LocationStatusCodes;
import com.google.android.gms.location.c.b;
import java.util.List;

public class ng implements GeofencingApi {

    private static abstract class a extends com.google.android.gms.location.LocationServices.a<Status> {
        public a(GoogleApiClient googleApiClient) {
            super(googleApiClient);
        }

        /* renamed from: b */
        public Status c(Status status) {
            return status;
        }
    }

    public PendingResult<Status> addGeofences(GoogleApiClient client, final GeofencingRequest geofencingRequest, final PendingIntent pendingIntent) {
        return client.b(new a(client) {
            /* access modifiers changed from: protected */
            public void a(nk nkVar) throws RemoteException {
                nkVar.a(geofencingRequest, pendingIntent, (com.google.android.gms.location.c.a) new com.google.android.gms.location.c.a() {
                    public void a(int i, String[] strArr) {
                        AnonymousClass1.this.b(LocationStatusCodes.eD(i));
                    }
                });
            }
        });
    }

    @Deprecated
    public PendingResult<Status> addGeofences(GoogleApiClient client, List<Geofence> geofences, PendingIntent pendingIntent) {
        Builder builder = new Builder();
        builder.addGeofences(geofences);
        builder.setInitialTrigger(5);
        return addGeofences(client, builder.build(), pendingIntent);
    }

    public PendingResult<Status> removeGeofences(GoogleApiClient client, final PendingIntent pendingIntent) {
        return client.b(new a(client) {
            /* access modifiers changed from: protected */
            public void a(nk nkVar) throws RemoteException {
                nkVar.a(pendingIntent, (b) new b() {
                    public void a(int i, PendingIntent pendingIntent) {
                        AnonymousClass2.this.b(LocationStatusCodes.eD(i));
                    }

                    public void b(int i, String[] strArr) {
                        Log.wtf("GeofencingImpl", "Request ID callback shouldn't have been called");
                    }
                });
            }
        });
    }

    public PendingResult<Status> removeGeofences(GoogleApiClient client, final List<String> geofenceRequestIds) {
        return client.b(new a(client) {
            /* access modifiers changed from: protected */
            public void a(nk nkVar) throws RemoteException {
                nkVar.a(geofenceRequestIds, (b) new b() {
                    public void a(int i, PendingIntent pendingIntent) {
                        Log.wtf("GeofencingImpl", "PendingIntent callback shouldn't have been called");
                    }

                    public void b(int i, String[] strArr) {
                        AnonymousClass3.this.b(LocationStatusCodes.eD(i));
                    }
                });
            }
        });
    }
}
