package com.google.android.gms.internal;

import android.content.Context;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import dalvik.system.DexClassLoader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.ByteBuffer;
import java.util.ArrayList;

public abstract class i extends h {
    private static Method kA;
    private static Method kB;
    private static Method kC;
    private static Method kD;
    private static Method kE;
    private static Method kF;
    private static Method kG;
    private static Method kH;
    private static Method kI;
    private static String kJ;
    private static String kK;
    private static String kL;
    private static o kM;
    static boolean kN = false;
    private static long startTime = 0;

    static class a extends Exception {
        public a() {
        }

        public a(Throwable th) {
            super(th);
        }
    }

    protected i(Context context, m mVar, n nVar) {
        super(context, mVar, nVar);
    }

    static String a(Context context, m mVar) throws a {
        if (kK != null) {
            return kK;
        }
        if (kD == null) {
            throw new a();
        }
        try {
            ByteBuffer byteBuffer = (ByteBuffer) kD.invoke(null, new Object[]{context});
            if (byteBuffer == null) {
                throw new a();
            }
            kK = mVar.a(byteBuffer.array(), true);
            return kK;
        } catch (IllegalAccessException e) {
            throw new a(e);
        } catch (InvocationTargetException e2) {
            throw new a(e2);
        }
    }

    static ArrayList<Long> a(MotionEvent motionEvent, DisplayMetrics displayMetrics) throws a {
        if (kE == null || motionEvent == null) {
            throw new a();
        }
        try {
            return (ArrayList) kE.invoke(null, new Object[]{motionEvent, displayMetrics});
        } catch (IllegalAccessException e) {
            throw new a(e);
        } catch (InvocationTargetException e2) {
            throw new a(e2);
        }
    }

    protected static synchronized void a(String str, Context context, m mVar) {
        synchronized (i.class) {
            if (!kN) {
                try {
                    kM = new o(mVar, null);
                    kJ = str;
                    g(context);
                    startTime = w().longValue();
                    kN = true;
                } catch (a | UnsupportedOperationException e) {
                }
            }
        }
    }

    static String b(Context context, m mVar) throws a {
        if (kL != null) {
            return kL;
        }
        if (kG == null) {
            throw new a();
        }
        try {
            ByteBuffer byteBuffer = (ByteBuffer) kG.invoke(null, new Object[]{context});
            if (byteBuffer == null) {
                throw new a();
            }
            kL = mVar.a(byteBuffer.array(), true);
            return kL;
        } catch (IllegalAccessException e) {
            throw new a(e);
        } catch (InvocationTargetException e2) {
            throw new a(e2);
        }
    }

    private static String b(byte[] bArr, String str) throws a {
        try {
            return new String(kM.c(bArr, str), "UTF-8");
        } catch (com.google.android.gms.internal.o.a e) {
            throw new a(e);
        } catch (UnsupportedEncodingException e2) {
            throw new a(e2);
        }
    }

    static String d(Context context) throws a {
        if (kF == null) {
            throw new a();
        }
        try {
            String str = (String) kF.invoke(null, new Object[]{context});
            if (str != null) {
                return str;
            }
            throw new a();
        } catch (IllegalAccessException e) {
            throw new a(e);
        } catch (InvocationTargetException e2) {
            throw new a(e2);
        }
    }

    static ArrayList<Long> e(Context context) throws a {
        if (kH == null) {
            throw new a();
        }
        try {
            ArrayList<Long> arrayList = (ArrayList) kH.invoke(null, new Object[]{context});
            if (arrayList != null && arrayList.size() == 2) {
                return arrayList;
            }
            throw new a();
        } catch (IllegalAccessException e) {
            throw new a(e);
        } catch (InvocationTargetException e2) {
            throw new a(e2);
        }
    }

    static int[] f(Context context) throws a {
        if (kI == null) {
            throw new a();
        }
        try {
            return (int[]) kI.invoke(null, new Object[]{context});
        } catch (IllegalAccessException e) {
            throw new a(e);
        } catch (InvocationTargetException e2) {
            throw new a(e2);
        }
    }

    private static void g(Context context) throws a {
        File file;
        File createTempFile;
        try {
            byte[] b = kM.b(q.getKey());
            byte[] c = kM.c(b, q.E());
            File cacheDir = context.getCacheDir();
            if (cacheDir == null) {
                cacheDir = context.getDir("dex", 0);
                if (cacheDir == null) {
                    throw new a();
                }
            }
            file = cacheDir;
            createTempFile = File.createTempFile("ads", ".jar", file);
            FileOutputStream fileOutputStream = new FileOutputStream(createTempFile);
            fileOutputStream.write(c, 0, c.length);
            fileOutputStream.close();
            DexClassLoader dexClassLoader = new DexClassLoader(createTempFile.getAbsolutePath(), file.getAbsolutePath(), null, context.getClassLoader());
            Class loadClass = dexClassLoader.loadClass(b(b, q.H()));
            Class loadClass2 = dexClassLoader.loadClass(b(b, q.T()));
            Class loadClass3 = dexClassLoader.loadClass(b(b, q.N()));
            Class loadClass4 = dexClassLoader.loadClass(b(b, q.L()));
            Class loadClass5 = dexClassLoader.loadClass(b(b, q.V()));
            Class loadClass6 = dexClassLoader.loadClass(b(b, q.J()));
            Class loadClass7 = dexClassLoader.loadClass(b(b, q.R()));
            Class loadClass8 = dexClassLoader.loadClass(b(b, q.P()));
            Class loadClass9 = dexClassLoader.loadClass(b(b, q.F()));
            kA = loadClass.getMethod(b(b, q.I()), new Class[0]);
            kB = loadClass2.getMethod(b(b, q.U()), new Class[0]);
            kC = loadClass3.getMethod(b(b, q.O()), new Class[0]);
            kD = loadClass4.getMethod(b(b, q.M()), new Class[]{Context.class});
            kE = loadClass5.getMethod(b(b, q.W()), new Class[]{MotionEvent.class, DisplayMetrics.class});
            kF = loadClass6.getMethod(b(b, q.K()), new Class[]{Context.class});
            kG = loadClass7.getMethod(b(b, q.S()), new Class[]{Context.class});
            kH = loadClass8.getMethod(b(b, q.Q()), new Class[]{Context.class});
            kI = loadClass9.getMethod(b(b, q.G()), new Class[]{Context.class});
            String name = createTempFile.getName();
            createTempFile.delete();
            new File(file, name.replace(".jar", ".dex")).delete();
        } catch (FileNotFoundException e) {
            throw new a(e);
        } catch (IOException e2) {
            throw new a(e2);
        } catch (ClassNotFoundException e3) {
            throw new a(e3);
        } catch (com.google.android.gms.internal.o.a e4) {
            throw new a(e4);
        } catch (NoSuchMethodException e5) {
            throw new a(e5);
        } catch (NullPointerException e6) {
            throw new a(e6);
        } catch (Throwable th) {
            String name2 = createTempFile.getName();
            createTempFile.delete();
            new File(file, name2.replace(".jar", ".dex")).delete();
            throw th;
        }
    }

    static String v() throws a {
        if (kJ != null) {
            return kJ;
        }
        throw new a();
    }

    static Long w() throws a {
        if (kA == null) {
            throw new a();
        }
        try {
            return (Long) kA.invoke(null, new Object[0]);
        } catch (IllegalAccessException e) {
            throw new a(e);
        } catch (InvocationTargetException e2) {
            throw new a(e2);
        }
    }

    static String x() throws a {
        if (kC == null) {
            throw new a();
        }
        try {
            return (String) kC.invoke(null, new Object[0]);
        } catch (IllegalAccessException e) {
            throw new a(e);
        } catch (InvocationTargetException e2) {
            throw new a(e2);
        }
    }

    static Long y() throws a {
        if (kB == null) {
            throw new a();
        }
        try {
            return (Long) kB.invoke(null, new Object[0]);
        } catch (IllegalAccessException e) {
            throw new a(e);
        } catch (InvocationTargetException e2) {
            throw new a(e2);
        }
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:33:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:?, code lost:
        return;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0090 A[ExcHandler: IOException (e java.io.IOException), Splitter:B:6:0x0010] */
    public void b(Context context) {
        try {
            a(1, x());
        } catch (a e) {
        } catch (IOException e2) {
        }
        try {
            a(2, v());
        } catch (a e3) {
        } catch (IOException e22) {
        }
        try {
            long longValue = w().longValue();
            a(25, longValue);
            if (startTime != 0) {
                a(17, longValue - startTime);
                a(23, startTime);
            }
            ArrayList e4 = e(context);
            a(31, ((Long) e4.get(0)).longValue());
            a(32, ((Long) e4.get(1)).longValue());
            a(33, y().longValue());
            a(27, a(context, this.ky));
            a(29, b(context, this.ky));
        } catch (a e5) {
        } catch (IOException e222) {
        }
        int[] f = f(context);
        a(5, (long) f[0]);
        a(6, (long) f[1]);
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
        return;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x005d A[ExcHandler: IOException (e java.io.IOException), Splitter:B:1:0x0001] */
    public void c(Context context) {
        try {
            a(2, v());
            a(1, x());
            a(25, w().longValue());
        } catch (a e) {
        } catch (IOException e2) {
        }
        ArrayList a2 = a(this.kw, this.kx);
        a(14, ((Long) a2.get(0)).longValue());
        a(15, ((Long) a2.get(1)).longValue());
        if (a2.size() >= 3) {
            a(16, ((Long) a2.get(2)).longValue());
        }
    }
}
