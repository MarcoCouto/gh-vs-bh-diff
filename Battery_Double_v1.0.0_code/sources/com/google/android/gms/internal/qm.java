package com.google.android.gms.internal;

import com.dlten.lib.frmWork.CEventWnd;
import com.google.ads.AdSize;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import java.io.IOException;
import java.util.Arrays;

public final class qm extends qq<qm> {
    public a[] ayq;

    public static final class a extends qq<a> {
        private static volatile a[] ayr;
        public C0089a ays;
        public String name;

        /* renamed from: com.google.android.gms.internal.qm$a$a reason: collision with other inner class name */
        public static final class C0089a extends qq<C0089a> {
            private static volatile C0089a[] ayt;
            public C0090a ayu;
            public int type;

            /* renamed from: com.google.android.gms.internal.qm$a$a$a reason: collision with other inner class name */
            public static final class C0090a extends qq<C0090a> {
                public int ayA;
                public int ayB;
                public boolean ayC;
                public a[] ayD;
                public C0089a[] ayE;
                public String[] ayF;
                public long[] ayG;
                public float[] ayH;
                public long ayI;
                public byte[] ayv;
                public String ayw;
                public double ayx;
                public float ayy;
                public long ayz;

                public C0090a() {
                    ry();
                }

                public void a(qp qpVar) throws IOException {
                    if (!Arrays.equals(this.ayv, qz.azq)) {
                        qpVar.a(1, this.ayv);
                    }
                    if (!this.ayw.equals("")) {
                        qpVar.b(2, this.ayw);
                    }
                    if (Double.doubleToLongBits(this.ayx) != Double.doubleToLongBits(0.0d)) {
                        qpVar.a(3, this.ayx);
                    }
                    if (Float.floatToIntBits(this.ayy) != Float.floatToIntBits(BitmapDescriptorFactory.HUE_RED)) {
                        qpVar.b(4, this.ayy);
                    }
                    if (this.ayz != 0) {
                        qpVar.b(5, this.ayz);
                    }
                    if (this.ayA != 0) {
                        qpVar.t(6, this.ayA);
                    }
                    if (this.ayB != 0) {
                        qpVar.u(7, this.ayB);
                    }
                    if (this.ayC) {
                        qpVar.b(8, this.ayC);
                    }
                    if (this.ayD != null && this.ayD.length > 0) {
                        for (a aVar : this.ayD) {
                            if (aVar != null) {
                                qpVar.a(9, (qw) aVar);
                            }
                        }
                    }
                    if (this.ayE != null && this.ayE.length > 0) {
                        for (C0089a aVar2 : this.ayE) {
                            if (aVar2 != null) {
                                qpVar.a(10, (qw) aVar2);
                            }
                        }
                    }
                    if (this.ayF != null && this.ayF.length > 0) {
                        for (String str : this.ayF) {
                            if (str != null) {
                                qpVar.b(11, str);
                            }
                        }
                    }
                    if (this.ayG != null && this.ayG.length > 0) {
                        for (long b : this.ayG) {
                            qpVar.b(12, b);
                        }
                    }
                    if (this.ayI != 0) {
                        qpVar.b(13, this.ayI);
                    }
                    if (this.ayH != null && this.ayH.length > 0) {
                        for (float b2 : this.ayH) {
                            qpVar.b(14, b2);
                        }
                    }
                    super.a(qpVar);
                }

                /* access modifiers changed from: protected */
                public int c() {
                    int c = super.c();
                    if (!Arrays.equals(this.ayv, qz.azq)) {
                        c += qp.b(1, this.ayv);
                    }
                    if (!this.ayw.equals("")) {
                        c += qp.j(2, this.ayw);
                    }
                    if (Double.doubleToLongBits(this.ayx) != Double.doubleToLongBits(0.0d)) {
                        c += qp.b(3, this.ayx);
                    }
                    if (Float.floatToIntBits(this.ayy) != Float.floatToIntBits(BitmapDescriptorFactory.HUE_RED)) {
                        c += qp.c(4, this.ayy);
                    }
                    if (this.ayz != 0) {
                        c += qp.d(5, this.ayz);
                    }
                    if (this.ayA != 0) {
                        c += qp.v(6, this.ayA);
                    }
                    if (this.ayB != 0) {
                        c += qp.w(7, this.ayB);
                    }
                    if (this.ayC) {
                        c += qp.c(8, this.ayC);
                    }
                    if (this.ayD != null && this.ayD.length > 0) {
                        int i = c;
                        for (a aVar : this.ayD) {
                            if (aVar != null) {
                                i += qp.c(9, (qw) aVar);
                            }
                        }
                        c = i;
                    }
                    if (this.ayE != null && this.ayE.length > 0) {
                        int i2 = c;
                        for (C0089a aVar2 : this.ayE) {
                            if (aVar2 != null) {
                                i2 += qp.c(10, (qw) aVar2);
                            }
                        }
                        c = i2;
                    }
                    if (this.ayF != null && this.ayF.length > 0) {
                        int i3 = 0;
                        int i4 = 0;
                        for (String str : this.ayF) {
                            if (str != null) {
                                i4++;
                                i3 += qp.dk(str);
                            }
                        }
                        c = c + i3 + (i4 * 1);
                    }
                    if (this.ayG != null && this.ayG.length > 0) {
                        int i5 = 0;
                        for (long D : this.ayG) {
                            i5 += qp.D(D);
                        }
                        c = c + i5 + (this.ayG.length * 1);
                    }
                    if (this.ayI != 0) {
                        c += qp.d(13, this.ayI);
                    }
                    return (this.ayH == null || this.ayH.length <= 0) ? c : c + (this.ayH.length * 4) + (this.ayH.length * 1);
                }

                public boolean equals(Object o) {
                    if (o == this) {
                        return true;
                    }
                    if (!(o instanceof C0090a)) {
                        return false;
                    }
                    C0090a aVar = (C0090a) o;
                    if (!Arrays.equals(this.ayv, aVar.ayv)) {
                        return false;
                    }
                    if (this.ayw == null) {
                        if (aVar.ayw != null) {
                            return false;
                        }
                    } else if (!this.ayw.equals(aVar.ayw)) {
                        return false;
                    }
                    if (Double.doubleToLongBits(this.ayx) == Double.doubleToLongBits(aVar.ayx) && Float.floatToIntBits(this.ayy) == Float.floatToIntBits(aVar.ayy) && this.ayz == aVar.ayz && this.ayA == aVar.ayA && this.ayB == aVar.ayB && this.ayC == aVar.ayC && qu.equals((Object[]) this.ayD, (Object[]) aVar.ayD) && qu.equals((Object[]) this.ayE, (Object[]) aVar.ayE) && qu.equals((Object[]) this.ayF, (Object[]) aVar.ayF) && qu.equals(this.ayG, aVar.ayG) && qu.equals(this.ayH, aVar.ayH) && this.ayI == aVar.ayI) {
                        return a(aVar);
                    }
                    return false;
                }

                public int hashCode() {
                    int hashCode = (this.ayw == null ? 0 : this.ayw.hashCode()) + ((Arrays.hashCode(this.ayv) + 527) * 31);
                    long doubleToLongBits = Double.doubleToLongBits(this.ayx);
                    return (((((((((((((((this.ayC ? 1231 : 1237) + (((((((((((hashCode * 31) + ((int) (doubleToLongBits ^ (doubleToLongBits >>> 32)))) * 31) + Float.floatToIntBits(this.ayy)) * 31) + ((int) (this.ayz ^ (this.ayz >>> 32)))) * 31) + this.ayA) * 31) + this.ayB) * 31)) * 31) + qu.hashCode((Object[]) this.ayD)) * 31) + qu.hashCode((Object[]) this.ayE)) * 31) + qu.hashCode((Object[]) this.ayF)) * 31) + qu.hashCode(this.ayG)) * 31) + qu.hashCode(this.ayH)) * 31) + ((int) (this.ayI ^ (this.ayI >>> 32)))) * 31) + rQ();
                }

                public C0090a ry() {
                    this.ayv = qz.azq;
                    this.ayw = "";
                    this.ayx = 0.0d;
                    this.ayy = BitmapDescriptorFactory.HUE_RED;
                    this.ayz = 0;
                    this.ayA = 0;
                    this.ayB = 0;
                    this.ayC = false;
                    this.ayD = a.ru();
                    this.ayE = C0089a.rw();
                    this.ayF = qz.azo;
                    this.ayG = qz.azk;
                    this.ayH = qz.azl;
                    this.ayI = 0;
                    this.ayW = null;
                    this.azh = -1;
                    return this;
                }

                /* renamed from: w */
                public C0090a b(qo qoVar) throws IOException {
                    while (true) {
                        int rz = qoVar.rz();
                        switch (rz) {
                            case 0:
                                break;
                            case 10:
                                this.ayv = qoVar.readBytes();
                                continue;
                            case CEventWnd.WM_APP_EXIT /*18*/:
                                this.ayw = qoVar.readString();
                                continue;
                            case 25:
                                this.ayx = qoVar.readDouble();
                                continue;
                            case 37:
                                this.ayy = qoVar.readFloat();
                                continue;
                            case 40:
                                this.ayz = qoVar.rB();
                                continue;
                            case 48:
                                this.ayA = qoVar.rC();
                                continue;
                            case 56:
                                this.ayB = qoVar.rE();
                                continue;
                            case 64:
                                this.ayC = qoVar.rD();
                                continue;
                            case 74:
                                int b = qz.b(qoVar, 74);
                                int length = this.ayD == null ? 0 : this.ayD.length;
                                a[] aVarArr = new a[(b + length)];
                                if (length != 0) {
                                    System.arraycopy(this.ayD, 0, aVarArr, 0, length);
                                }
                                while (length < aVarArr.length - 1) {
                                    aVarArr[length] = new a();
                                    qoVar.a(aVarArr[length]);
                                    qoVar.rz();
                                    length++;
                                }
                                aVarArr[length] = new a();
                                qoVar.a(aVarArr[length]);
                                this.ayD = aVarArr;
                                continue;
                            case 82:
                                int b2 = qz.b(qoVar, 82);
                                int length2 = this.ayE == null ? 0 : this.ayE.length;
                                C0089a[] aVarArr2 = new C0089a[(b2 + length2)];
                                if (length2 != 0) {
                                    System.arraycopy(this.ayE, 0, aVarArr2, 0, length2);
                                }
                                while (length2 < aVarArr2.length - 1) {
                                    aVarArr2[length2] = new C0089a();
                                    qoVar.a(aVarArr2[length2]);
                                    qoVar.rz();
                                    length2++;
                                }
                                aVarArr2[length2] = new C0089a();
                                qoVar.a(aVarArr2[length2]);
                                this.ayE = aVarArr2;
                                continue;
                            case AdSize.LARGE_AD_HEIGHT /*90*/:
                                int b3 = qz.b(qoVar, 90);
                                int length3 = this.ayF == null ? 0 : this.ayF.length;
                                String[] strArr = new String[(b3 + length3)];
                                if (length3 != 0) {
                                    System.arraycopy(this.ayF, 0, strArr, 0, length3);
                                }
                                while (length3 < strArr.length - 1) {
                                    strArr[length3] = qoVar.readString();
                                    qoVar.rz();
                                    length3++;
                                }
                                strArr[length3] = qoVar.readString();
                                this.ayF = strArr;
                                continue;
                            case 96:
                                int b4 = qz.b(qoVar, 96);
                                int length4 = this.ayG == null ? 0 : this.ayG.length;
                                long[] jArr = new long[(b4 + length4)];
                                if (length4 != 0) {
                                    System.arraycopy(this.ayG, 0, jArr, 0, length4);
                                }
                                while (length4 < jArr.length - 1) {
                                    jArr[length4] = qoVar.rB();
                                    qoVar.rz();
                                    length4++;
                                }
                                jArr[length4] = qoVar.rB();
                                this.ayG = jArr;
                                continue;
                            case 98:
                                int gS = qoVar.gS(qoVar.rG());
                                int position = qoVar.getPosition();
                                int i = 0;
                                while (qoVar.rL() > 0) {
                                    qoVar.rB();
                                    i++;
                                }
                                qoVar.gU(position);
                                int length5 = this.ayG == null ? 0 : this.ayG.length;
                                long[] jArr2 = new long[(i + length5)];
                                if (length5 != 0) {
                                    System.arraycopy(this.ayG, 0, jArr2, 0, length5);
                                }
                                while (length5 < jArr2.length) {
                                    jArr2[length5] = qoVar.rB();
                                    length5++;
                                }
                                this.ayG = jArr2;
                                qoVar.gT(gS);
                                continue;
                            case LocationRequest.PRIORITY_LOW_POWER /*104*/:
                                this.ayI = qoVar.rB();
                                continue;
                            case 114:
                                int rG = qoVar.rG();
                                int gS2 = qoVar.gS(rG);
                                int i2 = rG / 4;
                                int length6 = this.ayH == null ? 0 : this.ayH.length;
                                float[] fArr = new float[(i2 + length6)];
                                if (length6 != 0) {
                                    System.arraycopy(this.ayH, 0, fArr, 0, length6);
                                }
                                while (length6 < fArr.length) {
                                    fArr[length6] = qoVar.readFloat();
                                    length6++;
                                }
                                this.ayH = fArr;
                                qoVar.gT(gS2);
                                continue;
                            case 117:
                                int b5 = qz.b(qoVar, 117);
                                int length7 = this.ayH == null ? 0 : this.ayH.length;
                                float[] fArr2 = new float[(b5 + length7)];
                                if (length7 != 0) {
                                    System.arraycopy(this.ayH, 0, fArr2, 0, length7);
                                }
                                while (length7 < fArr2.length - 1) {
                                    fArr2[length7] = qoVar.readFloat();
                                    qoVar.rz();
                                    length7++;
                                }
                                fArr2[length7] = qoVar.readFloat();
                                this.ayH = fArr2;
                                continue;
                            default:
                                if (!a(qoVar, rz)) {
                                    break;
                                } else {
                                    continue;
                                }
                        }
                    }
                    return this;
                }
            }

            public C0089a() {
                rx();
            }

            public static C0089a[] rw() {
                if (ayt == null) {
                    synchronized (qu.azg) {
                        if (ayt == null) {
                            ayt = new C0089a[0];
                        }
                    }
                }
                return ayt;
            }

            public void a(qp qpVar) throws IOException {
                qpVar.t(1, this.type);
                if (this.ayu != null) {
                    qpVar.a(2, (qw) this.ayu);
                }
                super.a(qpVar);
            }

            /* access modifiers changed from: protected */
            public int c() {
                int c = super.c() + qp.v(1, this.type);
                return this.ayu != null ? c + qp.c(2, (qw) this.ayu) : c;
            }

            public boolean equals(Object o) {
                if (o == this) {
                    return true;
                }
                if (!(o instanceof C0089a)) {
                    return false;
                }
                C0089a aVar = (C0089a) o;
                if (this.type != aVar.type) {
                    return false;
                }
                if (this.ayu == null) {
                    if (aVar.ayu != null) {
                        return false;
                    }
                } else if (!this.ayu.equals(aVar.ayu)) {
                    return false;
                }
                return a(aVar);
            }

            public int hashCode() {
                return (((this.ayu == null ? 0 : this.ayu.hashCode()) + ((this.type + 527) * 31)) * 31) + rQ();
            }

            public C0089a rx() {
                this.type = 1;
                this.ayu = null;
                this.ayW = null;
                this.azh = -1;
                return this;
            }

            /* renamed from: v */
            public C0089a b(qo qoVar) throws IOException {
                while (true) {
                    int rz = qoVar.rz();
                    switch (rz) {
                        case 0:
                            break;
                        case 8:
                            int rC = qoVar.rC();
                            switch (rC) {
                                case 1:
                                case 2:
                                case 3:
                                case 4:
                                case 5:
                                case 6:
                                case 7:
                                case 8:
                                case 9:
                                case 10:
                                case 11:
                                case 12:
                                case 13:
                                case 14:
                                case 15:
                                    this.type = rC;
                                    break;
                                default:
                                    continue;
                            }
                        case CEventWnd.WM_APP_EXIT /*18*/:
                            if (this.ayu == null) {
                                this.ayu = new C0090a();
                            }
                            qoVar.a(this.ayu);
                            continue;
                        default:
                            if (!a(qoVar, rz)) {
                                break;
                            } else {
                                continue;
                            }
                    }
                }
                return this;
            }
        }

        public a() {
            rv();
        }

        public static a[] ru() {
            if (ayr == null) {
                synchronized (qu.azg) {
                    if (ayr == null) {
                        ayr = new a[0];
                    }
                }
            }
            return ayr;
        }

        public void a(qp qpVar) throws IOException {
            qpVar.b(1, this.name);
            if (this.ays != null) {
                qpVar.a(2, (qw) this.ays);
            }
            super.a(qpVar);
        }

        /* access modifiers changed from: protected */
        public int c() {
            int c = super.c() + qp.j(1, this.name);
            return this.ays != null ? c + qp.c(2, (qw) this.ays) : c;
        }

        public boolean equals(Object o) {
            if (o == this) {
                return true;
            }
            if (!(o instanceof a)) {
                return false;
            }
            a aVar = (a) o;
            if (this.name == null) {
                if (aVar.name != null) {
                    return false;
                }
            } else if (!this.name.equals(aVar.name)) {
                return false;
            }
            if (this.ays == null) {
                if (aVar.ays != null) {
                    return false;
                }
            } else if (!this.ays.equals(aVar.ays)) {
                return false;
            }
            return a(aVar);
        }

        public int hashCode() {
            int i = 0;
            int hashCode = ((this.name == null ? 0 : this.name.hashCode()) + 527) * 31;
            if (this.ays != null) {
                i = this.ays.hashCode();
            }
            return ((hashCode + i) * 31) + rQ();
        }

        public a rv() {
            this.name = "";
            this.ays = null;
            this.ayW = null;
            this.azh = -1;
            return this;
        }

        /* renamed from: u */
        public a b(qo qoVar) throws IOException {
            while (true) {
                int rz = qoVar.rz();
                switch (rz) {
                    case 0:
                        break;
                    case 10:
                        this.name = qoVar.readString();
                        continue;
                    case CEventWnd.WM_APP_EXIT /*18*/:
                        if (this.ays == null) {
                            this.ays = new C0089a();
                        }
                        qoVar.a(this.ays);
                        continue;
                    default:
                        if (!a(qoVar, rz)) {
                            break;
                        } else {
                            continue;
                        }
                }
            }
            return this;
        }
    }

    public qm() {
        rt();
    }

    public static qm n(byte[] bArr) throws qv {
        return (qm) qw.a(new qm(), bArr);
    }

    public void a(qp qpVar) throws IOException {
        if (this.ayq != null && this.ayq.length > 0) {
            for (a aVar : this.ayq) {
                if (aVar != null) {
                    qpVar.a(1, (qw) aVar);
                }
            }
        }
        super.a(qpVar);
    }

    /* access modifiers changed from: protected */
    public int c() {
        int c = super.c();
        if (this.ayq != null && this.ayq.length > 0) {
            for (a aVar : this.ayq) {
                if (aVar != null) {
                    c += qp.c(1, (qw) aVar);
                }
            }
        }
        return c;
    }

    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof qm)) {
            return false;
        }
        qm qmVar = (qm) o;
        if (qu.equals((Object[]) this.ayq, (Object[]) qmVar.ayq)) {
            return a(qmVar);
        }
        return false;
    }

    public int hashCode() {
        return ((qu.hashCode((Object[]) this.ayq) + 527) * 31) + rQ();
    }

    public qm rt() {
        this.ayq = a.ru();
        this.ayW = null;
        this.azh = -1;
        return this;
    }

    /* renamed from: t */
    public qm b(qo qoVar) throws IOException {
        while (true) {
            int rz = qoVar.rz();
            switch (rz) {
                case 0:
                    break;
                case 10:
                    int b = qz.b(qoVar, 10);
                    int length = this.ayq == null ? 0 : this.ayq.length;
                    a[] aVarArr = new a[(b + length)];
                    if (length != 0) {
                        System.arraycopy(this.ayq, 0, aVarArr, 0, length);
                    }
                    while (length < aVarArr.length - 1) {
                        aVarArr[length] = new a();
                        qoVar.a(aVarArr[length]);
                        qoVar.rz();
                        length++;
                    }
                    aVarArr[length] = new a();
                    qoVar.a(aVarArr[length]);
                    this.ayq = aVarArr;
                    continue;
                default:
                    if (!a(qoVar, rz)) {
                        break;
                    } else {
                        continue;
                    }
            }
        }
        return this;
    }
}
