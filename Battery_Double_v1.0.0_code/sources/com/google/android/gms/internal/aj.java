package com.google.android.gms.internal;

import android.content.Context;
import android.webkit.WebView;
import com.google.android.gms.internal.ah.a;
import org.json.JSONObject;

@ey
public class aj implements ah {
    /* access modifiers changed from: private */
    public final gu mo;

    public aj(Context context, gs gsVar) {
        this.mo = gu.a(context, new ay(), false, false, null, gsVar);
    }

    private void runOnUiThread(Runnable runnable) {
        if (gq.dB()) {
            runnable.run();
        } else {
            gq.wR.post(runnable);
        }
    }

    public void a(final a aVar) {
        this.mo.dD().a((gv.a) new gv.a() {
            public void a(gu guVar) {
                aVar.aR();
            }
        });
    }

    public void a(t tVar, ds dsVar, cb cbVar, dv dvVar, boolean z, ce ceVar) {
        this.mo.dD().a(tVar, dsVar, cbVar, dvVar, z, ceVar, new v(false));
    }

    public void a(String str, cd cdVar) {
        this.mo.dD().a(str, cdVar);
    }

    public void a(final String str, final JSONObject jSONObject) {
        runOnUiThread(new Runnable() {
            public void run() {
                aj.this.mo.a(str, jSONObject);
            }
        });
    }

    public void destroy() {
        this.mo.destroy();
    }

    public void f(final String str) {
        runOnUiThread(new Runnable() {
            public void run() {
                aj.this.mo.loadUrl(str);
            }
        });
    }

    public void g(String str) {
        this.mo.dD().a(str, (cd) null);
    }

    public void pause() {
        gi.a((WebView) this.mo);
    }

    public void resume() {
        gi.b(this.mo);
    }
}
