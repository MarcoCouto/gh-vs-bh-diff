package com.google.android.gms.internal;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.net.Uri;
import android.net.Uri.Builder;
import android.text.TextUtils;
import android.view.MotionEvent;
import java.util.Map;

@ey
public final class gl {
    /* access modifiers changed from: private */
    public final Context mContext;
    private int mState;
    private final float rz;
    private String wH;
    private float wI;
    private float wJ;
    private float wK;

    public gl(Context context) {
        this.mState = 0;
        this.mContext = context;
        this.rz = context.getResources().getDisplayMetrics().density;
    }

    public gl(Context context, String str) {
        this(context);
        this.wH = str;
    }

    private void a(int i, float f, float f2) {
        if (i == 0) {
            this.mState = 0;
            this.wI = f;
            this.wJ = f2;
            this.wK = f2;
        } else if (this.mState == -1) {
        } else {
            if (i == 2) {
                if (f2 > this.wJ) {
                    this.wJ = f2;
                } else if (f2 < this.wK) {
                    this.wK = f2;
                }
                if (this.wJ - this.wK > 30.0f * this.rz) {
                    this.mState = -1;
                    return;
                }
                if (this.mState == 0 || this.mState == 2) {
                    if (f - this.wI >= 50.0f * this.rz) {
                        this.wI = f;
                        this.mState++;
                    }
                } else if ((this.mState == 1 || this.mState == 3) && f - this.wI <= -50.0f * this.rz) {
                    this.wI = f;
                    this.mState++;
                }
                if (this.mState == 1 || this.mState == 3) {
                    if (f > this.wI) {
                        this.wI = f;
                    }
                } else if (this.mState == 2 && f < this.wI) {
                    this.wI = f;
                }
            } else if (i == 1 && this.mState == 4) {
                showDialog();
            }
        }
    }

    private void showDialog() {
        final String str;
        if (!TextUtils.isEmpty(this.wH)) {
            Uri build = new Builder().encodedQuery(this.wH).build();
            StringBuilder sb = new StringBuilder();
            Map c = gi.c(build);
            for (String str2 : c.keySet()) {
                sb.append(str2).append(" = ").append((String) c.get(str2)).append("\n\n");
            }
            str = sb.toString().trim();
            if (TextUtils.isEmpty(str)) {
                str = "No debug information";
            }
        } else {
            str = "No debug information";
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(this.mContext);
        builder.setMessage(str);
        builder.setTitle("Ad Information");
        builder.setPositiveButton("Share", new OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                gl.this.mContext.startActivity(Intent.createChooser(new Intent("android.intent.action.SEND").setType("text/plain").putExtra("android.intent.extra.TEXT", str), "Share via"));
            }
        });
        builder.setNegativeButton("Close", new OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        builder.create().show();
    }

    public void Q(String str) {
        this.wH = str;
    }

    public void c(MotionEvent motionEvent) {
        int historySize = motionEvent.getHistorySize();
        for (int i = 0; i < historySize; i++) {
            a(motionEvent.getActionMasked(), motionEvent.getHistoricalX(0, i), motionEvent.getHistoricalY(0, i));
        }
        a(motionEvent.getActionMasked(), motionEvent.getX(), motionEvent.getY());
    }
}
