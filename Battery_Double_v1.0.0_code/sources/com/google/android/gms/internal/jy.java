package com.google.android.gms.internal;

import android.content.Context;
import android.os.IBinder;
import android.view.View;
import com.google.android.gms.dynamic.e;
import com.google.android.gms.dynamic.g;
import com.google.android.gms.dynamic.g.a;

public final class jy extends g<ju> {
    private static final jy Nt = new jy();

    private jy() {
        super("com.google.android.gms.common.ui.SignInButtonCreatorImpl");
    }

    public static View b(Context context, int i, int i2) throws a {
        return Nt.c(context, i, i2);
    }

    private View c(Context context, int i, int i2) throws a {
        try {
            return (View) e.f(((ju) L(context)).a(e.k(context), i, i2));
        } catch (Exception e) {
            throw new a("Could not get button with size " + i + " and color " + i2, e);
        }
    }

    /* renamed from: S */
    public ju d(IBinder iBinder) {
        return ju.a.R(iBinder);
    }
}
