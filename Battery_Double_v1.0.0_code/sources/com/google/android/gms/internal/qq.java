package com.google.android.gms.internal;

import com.google.android.gms.internal.qq;
import java.io.IOException;

public abstract class qq<M extends qq<M>> extends qw {
    protected qs ayW;

    public final <T> T a(qr<M, T> qrVar) {
        if (this.ayW == null) {
            return null;
        }
        qt hh = this.ayW.hh(qz.hl(qrVar.tag));
        if (hh != null) {
            return hh.b(qrVar);
        }
        return null;
    }

    public void a(qp qpVar) throws IOException {
        if (this.ayW != null) {
            for (int i = 0; i < this.ayW.size(); i++) {
                this.ayW.hi(i).a(qpVar);
            }
        }
    }

    /* access modifiers changed from: protected */
    public final boolean a(qo qoVar, int i) throws IOException {
        int position = qoVar.getPosition();
        if (!qoVar.gQ(i)) {
            return false;
        }
        int hl = qz.hl(i);
        qy qyVar = new qy(i, qoVar.s(position, qoVar.getPosition() - position));
        qt qtVar = null;
        if (this.ayW == null) {
            this.ayW = new qs();
        } else {
            qtVar = this.ayW.hh(hl);
        }
        if (qtVar == null) {
            qtVar = new qt();
            this.ayW.a(hl, qtVar);
        }
        qtVar.a(qyVar);
        return true;
    }

    /* access modifiers changed from: protected */
    public final boolean a(M m) {
        return (this.ayW == null || this.ayW.isEmpty()) ? m.ayW == null || m.ayW.isEmpty() : this.ayW.equals(m.ayW);
    }

    /* access modifiers changed from: protected */
    public int c() {
        if (this.ayW == null) {
            return 0;
        }
        int i = 0;
        for (int i2 = 0; i2 < this.ayW.size(); i2++) {
            i += this.ayW.hi(i2).c();
        }
        return i;
    }

    /* access modifiers changed from: protected */
    public final int rQ() {
        if (this.ayW == null || this.ayW.isEmpty()) {
            return 0;
        }
        return this.ayW.hashCode();
    }
}
