package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.a.C0004a;
import com.google.android.gms.common.internal.safeparcel.b;

public class kn implements Creator<km> {
    static void a(km kmVar, Parcel parcel, int i) {
        int H = b.H(parcel);
        b.c(parcel, 1, kmVar.getVersionCode());
        b.a(parcel, 2, (Parcelable) kmVar.hF(), i, false);
        b.H(parcel, H);
    }

    /* renamed from: J */
    public km createFromParcel(Parcel parcel) {
        int G = a.G(parcel);
        int i = 0;
        ko koVar = null;
        while (parcel.dataPosition() < G) {
            int F = a.F(parcel);
            switch (a.aH(F)) {
                case 1:
                    i = a.g(parcel, F);
                    break;
                case 2:
                    koVar = (ko) a.a(parcel, F, (Creator<T>) ko.CREATOR);
                    break;
                default:
                    a.b(parcel, F);
                    break;
            }
        }
        if (parcel.dataPosition() == G) {
            return new km(i, koVar);
        }
        throw new C0004a("Overread allowed size end=" + G, parcel);
    }

    /* renamed from: aK */
    public km[] newArray(int i) {
        return new km[i];
    }
}
