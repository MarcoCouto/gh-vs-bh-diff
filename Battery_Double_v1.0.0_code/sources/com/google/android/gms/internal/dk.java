package com.google.android.gms.internal;

import org.json.JSONException;
import org.json.JSONObject;

@ey
public class dk {
    private final boolean rs;
    private final boolean rt;
    private final boolean ru;
    private final boolean rv;
    private final boolean rw;

    public static final class a {
        /* access modifiers changed from: private */
        public boolean rs;
        /* access modifiers changed from: private */
        public boolean rt;
        /* access modifiers changed from: private */
        public boolean ru;
        /* access modifiers changed from: private */
        public boolean rv;
        /* access modifiers changed from: private */
        public boolean rw;

        public dk bU() {
            return new dk(this);
        }

        public a k(boolean z) {
            this.rs = z;
            return this;
        }

        public a l(boolean z) {
            this.rt = z;
            return this;
        }

        public a m(boolean z) {
            this.ru = z;
            return this;
        }

        public a n(boolean z) {
            this.rv = z;
            return this;
        }

        public a o(boolean z) {
            this.rw = z;
            return this;
        }
    }

    private dk(a aVar) {
        this.rs = aVar.rs;
        this.rt = aVar.rt;
        this.ru = aVar.ru;
        this.rv = aVar.rv;
        this.rw = aVar.rw;
    }

    public JSONObject toJson() {
        try {
            return new JSONObject().put("sms", this.rs).put("tel", this.rt).put("calendar", this.ru).put("storePicture", this.rv).put("inlineVideo", this.rw);
        } catch (JSONException e) {
            gr.b("Error occured while obtaining the MRAID capabilities.", e);
            return null;
        }
    }
}
