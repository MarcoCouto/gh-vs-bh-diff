package com.google.android.gms.internal;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.IBinder;
import android.os.Message;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.internal.jl.f;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

public final class jn implements Callback {
    private static final Object Nd = new Object();
    private static jn Ne;
    /* access modifiers changed from: private */
    public final HashMap<String, a> Nf = new HashMap<>();
    private final Handler mHandler;
    /* access modifiers changed from: private */
    public final Context mO;

    final class a {
        private final String Ng;
        private final C0057a Nh = new C0057a();
        /* access modifiers changed from: private */
        public final HashSet<f> Ni = new HashSet<>();
        private boolean Nj;
        /* access modifiers changed from: private */
        public IBinder Nk;
        /* access modifiers changed from: private */
        public ComponentName Nl;
        /* access modifiers changed from: private */
        public int mState = 2;

        /* renamed from: com.google.android.gms.internal.jn$a$a reason: collision with other inner class name */
        public class C0057a implements ServiceConnection {
            public C0057a() {
            }

            public void onServiceConnected(ComponentName component, IBinder binder) {
                synchronized (jn.this.Nf) {
                    a.this.Nk = binder;
                    a.this.Nl = component;
                    Iterator it = a.this.Ni.iterator();
                    while (it.hasNext()) {
                        ((f) it.next()).onServiceConnected(component, binder);
                    }
                    a.this.mState = 1;
                }
            }

            public void onServiceDisconnected(ComponentName component) {
                synchronized (jn.this.Nf) {
                    a.this.Nk = null;
                    a.this.Nl = component;
                    Iterator it = a.this.Ni.iterator();
                    while (it.hasNext()) {
                        ((f) it.next()).onServiceDisconnected(component);
                    }
                    a.this.mState = 2;
                }
            }
        }

        public a(String str) {
            this.Ng = str;
        }

        public void a(f fVar) {
            this.Ni.add(fVar);
        }

        public void b(f fVar) {
            this.Ni.remove(fVar);
        }

        public boolean c(f fVar) {
            return this.Ni.contains(fVar);
        }

        public IBinder getBinder() {
            return this.Nk;
        }

        public ComponentName getComponentName() {
            return this.Nl;
        }

        public int getState() {
            return this.mState;
        }

        public void hA() {
            this.Nj = jn.this.mO.bindService(new Intent(this.Ng).setPackage(GooglePlayServicesUtil.GOOGLE_PLAY_SERVICES_PACKAGE), this.Nh, 129);
            if (this.Nj) {
                this.mState = 3;
            } else {
                jn.this.mO.unbindService(this.Nh);
            }
        }

        public void hB() {
            jn.this.mO.unbindService(this.Nh);
            this.Nj = false;
            this.mState = 2;
        }

        public String hC() {
            return this.Ng;
        }

        public boolean hD() {
            return this.Ni.isEmpty();
        }

        public boolean isBound() {
            return this.Nj;
        }
    }

    private jn(Context context) {
        this.mHandler = new Handler(context.getMainLooper(), this);
        this.mO = context.getApplicationContext();
    }

    public static jn J(Context context) {
        synchronized (Nd) {
            if (Ne == null) {
                Ne = new jn(context.getApplicationContext());
            }
        }
        return Ne;
    }

    public boolean a(String str, f fVar) {
        boolean isBound;
        synchronized (this.Nf) {
            a aVar = (a) this.Nf.get(str);
            if (aVar != null) {
                this.mHandler.removeMessages(0, aVar);
                if (!aVar.c(fVar)) {
                    aVar.a(fVar);
                    switch (aVar.getState()) {
                        case 1:
                            fVar.onServiceConnected(aVar.getComponentName(), aVar.getBinder());
                            break;
                        case 2:
                            aVar.hA();
                            break;
                    }
                } else {
                    throw new IllegalStateException("Trying to bind a GmsServiceConnection that was already connected before.  startServiceAction=" + str);
                }
            } else {
                aVar = new a(str);
                aVar.a(fVar);
                aVar.hA();
                this.Nf.put(str, aVar);
            }
            isBound = aVar.isBound();
        }
        return isBound;
    }

    public void b(String str, f fVar) {
        synchronized (this.Nf) {
            a aVar = (a) this.Nf.get(str);
            if (aVar == null) {
                throw new IllegalStateException("Nonexistent connection status for service action: " + str);
            } else if (!aVar.c(fVar)) {
                throw new IllegalStateException("Trying to unbind a GmsServiceConnection  that was not bound before.  startServiceAction=" + str);
            } else {
                aVar.b(fVar);
                if (aVar.hD()) {
                    this.mHandler.sendMessageDelayed(this.mHandler.obtainMessage(0, aVar), 5000);
                }
            }
        }
    }

    public boolean handleMessage(Message msg) {
        switch (msg.what) {
            case 0:
                a aVar = (a) msg.obj;
                synchronized (this.Nf) {
                    if (aVar.hD()) {
                        aVar.hB();
                        this.Nf.remove(aVar.hC());
                    }
                }
                return true;
            default:
                return false;
        }
    }
}
