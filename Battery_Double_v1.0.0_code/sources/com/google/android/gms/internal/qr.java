package com.google.android.gms.internal;

import com.google.android.gms.internal.qq;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class qr<M extends qq<M>, T> {
    protected final Class<T> ayX;
    protected final boolean ayY;
    public final int tag;
    protected final int type;

    private qr(int i, Class<T> cls, int i2, boolean z) {
        this.type = i;
        this.ayX = cls;
        this.tag = i2;
        this.ayY = z;
    }

    public static <M extends qq<M>, T extends qw> qr<M, T> a(int i, Class<T> cls, int i2) {
        return new qr<>(i, cls, i2, false);
    }

    private T n(List<qy> list) {
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < list.size(); i++) {
            qy qyVar = (qy) list.get(i);
            if (qyVar.azi.length != 0) {
                a(qyVar, (List<Object>) arrayList);
            }
        }
        int size = arrayList.size();
        if (size == 0) {
            return null;
        }
        T cast = this.ayX.cast(Array.newInstance(this.ayX.getComponentType(), size));
        for (int i2 = 0; i2 < size; i2++) {
            Array.set(cast, i2, arrayList.get(i2));
        }
        return cast;
    }

    private T o(List<qy> list) {
        if (list.isEmpty()) {
            return null;
        }
        return this.ayX.cast(x(qo.p(((qy) list.get(list.size() - 1)).azi)));
    }

    /* access modifiers changed from: 0000 */
    public int B(Object obj) {
        return this.ayY ? C(obj) : D(obj);
    }

    /* access modifiers changed from: protected */
    public int C(Object obj) {
        int i = 0;
        int length = Array.getLength(obj);
        for (int i2 = 0; i2 < length; i2++) {
            if (Array.get(obj, i2) != null) {
                i += D(Array.get(obj, i2));
            }
        }
        return i;
    }

    /* access modifiers changed from: protected */
    public int D(Object obj) {
        int hl = qz.hl(this.tag);
        switch (this.type) {
            case 10:
                return qp.b(hl, (qw) obj);
            case 11:
                return qp.c(hl, (qw) obj);
            default:
                throw new IllegalArgumentException("Unknown type " + this.type);
        }
    }

    /* access modifiers changed from: protected */
    public void a(qy qyVar, List<Object> list) {
        list.add(x(qo.p(qyVar.azi)));
    }

    /* access modifiers changed from: 0000 */
    public void a(Object obj, qp qpVar) throws IOException {
        if (this.ayY) {
            c(obj, qpVar);
        } else {
            b(obj, qpVar);
        }
    }

    /* access modifiers changed from: protected */
    public void b(Object obj, qp qpVar) {
        try {
            qpVar.hd(this.tag);
            switch (this.type) {
                case 10:
                    qw qwVar = (qw) obj;
                    int hl = qz.hl(this.tag);
                    qpVar.b(qwVar);
                    qpVar.x(hl, 4);
                    return;
                case 11:
                    qpVar.c((qw) obj);
                    return;
                default:
                    throw new IllegalArgumentException("Unknown type " + this.type);
            }
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
        throw new IllegalStateException(e);
    }

    /* access modifiers changed from: protected */
    public void c(Object obj, qp qpVar) {
        int length = Array.getLength(obj);
        for (int i = 0; i < length; i++) {
            Object obj2 = Array.get(obj, i);
            if (obj2 != null) {
                b(obj2, qpVar);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public final T m(List<qy> list) {
        if (list == null) {
            return null;
        }
        return this.ayY ? n(list) : o(list);
    }

    /* access modifiers changed from: protected */
    public Object x(qo qoVar) {
        Class<T> cls = this.ayY ? this.ayX.getComponentType() : this.ayX;
        try {
            switch (this.type) {
                case 10:
                    qw qwVar = (qw) cls.newInstance();
                    qoVar.a(qwVar, qz.hl(this.tag));
                    return qwVar;
                case 11:
                    qw qwVar2 = (qw) cls.newInstance();
                    qoVar.a(qwVar2);
                    return qwVar2;
                default:
                    throw new IllegalArgumentException("Unknown type " + this.type);
            }
        } catch (InstantiationException e) {
            throw new IllegalArgumentException("Error creating instance of class " + cls, e);
        } catch (IllegalAccessException e2) {
            throw new IllegalArgumentException("Error creating instance of class " + cls, e2);
        } catch (IOException e3) {
            throw new IllegalArgumentException("Error reading extension field", e3);
        }
    }
}
