package com.google.android.gms.internal;

import com.google.android.gms.common.internal.safeparcel.SafeParcelable;

public abstract class ji implements SafeParcelable {
    private static ClassLoader MA = null;
    private static Integer MB = null;
    private static final Object Mz = new Object();
    private boolean MC = false;

    private static boolean a(Class<?> cls) {
        boolean z = false;
        try {
            return SafeParcelable.NULL.equals(cls.getField("NULL").get(null));
        } catch (IllegalAccessException | NoSuchFieldException e) {
            return z;
        }
    }

    /* access modifiers changed from: protected */
    public static boolean aW(String str) {
        ClassLoader hs = hs();
        if (hs == null) {
            return true;
        }
        try {
            return a(hs.loadClass(str));
        } catch (Exception e) {
            return false;
        }
    }

    protected static ClassLoader hs() {
        ClassLoader classLoader;
        synchronized (Mz) {
            classLoader = MA;
        }
        return classLoader;
    }

    /* access modifiers changed from: protected */
    public static Integer ht() {
        Integer num;
        synchronized (Mz) {
            num = MB;
        }
        return num;
    }

    /* access modifiers changed from: protected */
    public boolean hu() {
        return this.MC;
    }
}
