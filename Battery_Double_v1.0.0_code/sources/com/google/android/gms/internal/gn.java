package com.google.android.gms.internal;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;

@ey
public class gn {
    public static final a<Void> wN = new a() {
        /* renamed from: c */
        public Void b(InputStream inputStream) {
            return null;
        }

        /* renamed from: dz */
        public Void cQ() {
            return null;
        }
    };

    public interface a<T> {
        T b(InputStream inputStream);

        T cQ();
    }

    public <T> Future<T> a(final String str, final a<T> aVar) {
        return gh.submit(new Callable<T>() {
            /* JADX WARNING: Removed duplicated region for block: B:20:0x0042  */
            /* JADX WARNING: Removed duplicated region for block: B:26:0x004e  */
            /* JADX WARNING: Removed duplicated region for block: B:29:0x0055  */
            public T call() {
                HttpURLConnection httpURLConnection = null;
                try {
                    HttpURLConnection httpURLConnection2 = (HttpURLConnection) new URL(str).openConnection();
                    try {
                        httpURLConnection2.connect();
                        int responseCode = httpURLConnection2.getResponseCode();
                        if (responseCode < 200 || responseCode > 299) {
                            if (httpURLConnection2 != null) {
                                httpURLConnection2.disconnect();
                            }
                            return aVar.cQ();
                        }
                        Object b = aVar.b(httpURLConnection2.getInputStream());
                        if (httpURLConnection2 != null) {
                            httpURLConnection2.disconnect();
                        }
                        return b;
                    } catch (MalformedURLException e) {
                        Throwable th = e;
                        httpURLConnection = httpURLConnection2;
                        e = th;
                        try {
                            gr.d("Error making HTTP request.", e);
                            if (httpURLConnection != null) {
                            }
                            return aVar.cQ();
                        } catch (Throwable th2) {
                            th = th2;
                            if (httpURLConnection != null) {
                                httpURLConnection.disconnect();
                            }
                            throw th;
                        }
                    } catch (IOException e2) {
                        Throwable th3 = e2;
                        httpURLConnection = httpURLConnection2;
                        e = th3;
                        gr.d("Error making HTTP request.", e);
                        if (httpURLConnection != null) {
                        }
                        return aVar.cQ();
                    } catch (Throwable th4) {
                        Throwable th5 = th4;
                        httpURLConnection = httpURLConnection2;
                        th = th5;
                        if (httpURLConnection != null) {
                        }
                        throw th;
                    }
                } catch (MalformedURLException e3) {
                    e = e3;
                    gr.d("Error making HTTP request.", e);
                    if (httpURLConnection != null) {
                        httpURLConnection.disconnect();
                    }
                    return aVar.cQ();
                } catch (IOException e4) {
                    e = e4;
                    gr.d("Error making HTTP request.", e);
                    if (httpURLConnection != null) {
                        httpURLConnection.disconnect();
                    }
                    return aVar.cQ();
                }
            }
        });
    }
}
