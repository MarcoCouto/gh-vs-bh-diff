package com.google.android.gms.internal;

import android.content.Context;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.internal.jl.e;
import com.google.android.gms.internal.os.a;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ox extends jl<oq> {
    private final String CS;
    private final ou anm;
    private final os ann = new os();
    private boolean ano = true;
    private final Object mH = new Object();

    public ox(Context context, ou ouVar) {
        super(context, ouVar, ouVar, new String[0]);
        this.CS = context.getPackageName();
        this.anm = (ou) jx.i(ouVar);
        this.anm.a(this);
    }

    private void c(ov ovVar, or orVar) {
        this.ann.a(ovVar, orVar);
    }

    private void d(ov ovVar, or orVar) {
        try {
            or();
            ((oq) hw()).a(this.CS, ovVar, orVar);
        } catch (RemoteException e) {
            Log.e("PlayLoggerImpl", "Couldn't send log event.  Will try caching.");
            c(ovVar, orVar);
        } catch (IllegalStateException e2) {
            Log.e("PlayLoggerImpl", "Service was disconnected.  Will try caching.");
            c(ovVar, orVar);
        }
    }

    private void or() {
        ov ovVar;
        je.K(!this.ano);
        if (!this.ann.isEmpty()) {
            ov ovVar2 = null;
            try {
                ArrayList arrayList = new ArrayList();
                Iterator it = this.ann.op().iterator();
                while (it.hasNext()) {
                    a aVar = (a) it.next();
                    if (aVar.and != null) {
                        ((oq) hw()).a(this.CS, aVar.anb, qw.f(aVar.and));
                    } else {
                        if (aVar.anb.equals(ovVar2)) {
                            arrayList.add(aVar.anc);
                            ovVar = ovVar2;
                        } else {
                            if (!arrayList.isEmpty()) {
                                ((oq) hw()).a(this.CS, ovVar2, (List<or>) arrayList);
                                arrayList.clear();
                            }
                            ov ovVar3 = aVar.anb;
                            arrayList.add(aVar.anc);
                            ovVar = ovVar3;
                        }
                        ovVar2 = ovVar;
                    }
                }
                if (!arrayList.isEmpty()) {
                    ((oq) hw()).a(this.CS, ovVar2, (List<or>) arrayList);
                }
                this.ann.clear();
            } catch (RemoteException e) {
                Log.e("PlayLoggerImpl", "Couldn't send cached log events to AndroidLog service.  Retaining in memory cache.");
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void U(boolean z) {
        synchronized (this.mH) {
            boolean z2 = this.ano;
            this.ano = z;
            if (z2 && !this.ano) {
                or();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void a(jt jtVar, e eVar) throws RemoteException {
        jtVar.f(eVar, GooglePlayServicesUtil.GOOGLE_PLAY_SERVICES_VERSION_CODE, getContext().getPackageName(), new Bundle());
    }

    public void b(ov ovVar, or orVar) {
        synchronized (this.mH) {
            if (this.ano) {
                c(ovVar, orVar);
            } else {
                d(ovVar, orVar);
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: bJ */
    public oq l(IBinder iBinder) {
        return oq.a.bI(iBinder);
    }

    /* access modifiers changed from: protected */
    public String bK() {
        return "com.google.android.gms.playlog.service.START";
    }

    /* access modifiers changed from: protected */
    public String bL() {
        return "com.google.android.gms.playlog.internal.IPlayLogService";
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:?, code lost:
        return;
     */
    public void start() {
        synchronized (this.mH) {
            if (!isConnecting() && !isConnected()) {
                this.anm.T(true);
                connect();
            }
        }
    }

    public void stop() {
        synchronized (this.mH) {
            this.anm.T(false);
            disconnect();
        }
    }
}
