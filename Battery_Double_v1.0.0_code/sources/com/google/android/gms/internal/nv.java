package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.a.C0004a;
import com.google.android.gms.common.internal.safeparcel.b;
import java.util.ArrayList;
import java.util.List;

public class nv implements Creator<nu> {
    static void a(nu nuVar, Parcel parcel, int i) {
        int H = b.H(parcel);
        b.c(parcel, 1, nuVar.ahn, false);
        b.c(parcel, 1000, nuVar.CK);
        b.a(parcel, 2, nuVar.nu(), false);
        b.a(parcel, 3, nuVar.nv());
        b.c(parcel, 4, nuVar.aht, false);
        b.b(parcel, 6, nuVar.ahu, false);
        b.H(parcel, H);
    }

    /* renamed from: cQ */
    public nu createFromParcel(Parcel parcel) {
        boolean z = false;
        ArrayList arrayList = null;
        int G = a.G(parcel);
        List list = null;
        String str = null;
        List list2 = null;
        int i = 0;
        while (parcel.dataPosition() < G) {
            int F = a.F(parcel);
            switch (a.aH(F)) {
                case 1:
                    list2 = a.c(parcel, F, oa.CREATOR);
                    break;
                case 2:
                    str = a.o(parcel, F);
                    break;
                case 3:
                    z = a.c(parcel, F);
                    break;
                case 4:
                    list = a.c(parcel, F, oe.CREATOR);
                    break;
                case 6:
                    arrayList = a.C(parcel, F);
                    break;
                case 1000:
                    i = a.g(parcel, F);
                    break;
                default:
                    a.b(parcel, F);
                    break;
            }
        }
        if (parcel.dataPosition() == G) {
            return new nu(i, list2, str, z, list, arrayList);
        }
        throw new C0004a("Overread allowed size end=" + G, parcel);
    }

    /* renamed from: eM */
    public nu[] newArray(int i) {
        return new nu[i];
    }
}
