package com.google.android.gms.internal;

import com.dlten.lib.frmWork.CEventWnd;
import com.google.android.gms.internal.c.f;
import com.google.android.gms.internal.c.j;
import java.io.IOException;

public interface pu {

    public static final class a extends qq<a> {
        public long auB;
        public j auC;
        public f gs;

        public a() {
            rc();
        }

        public static a l(byte[] bArr) throws qv {
            return (a) qw.a(new a(), bArr);
        }

        public void a(qp qpVar) throws IOException {
            qpVar.b(1, this.auB);
            if (this.gs != null) {
                qpVar.a(2, (qw) this.gs);
            }
            if (this.auC != null) {
                qpVar.a(3, (qw) this.auC);
            }
            super.a(qpVar);
        }

        /* access modifiers changed from: protected */
        public int c() {
            int c = super.c() + qp.d(1, this.auB);
            if (this.gs != null) {
                c += qp.c(2, (qw) this.gs);
            }
            return this.auC != null ? c + qp.c(3, (qw) this.auC) : c;
        }

        public boolean equals(Object o) {
            if (o == this) {
                return true;
            }
            if (!(o instanceof a)) {
                return false;
            }
            a aVar = (a) o;
            if (this.auB != aVar.auB) {
                return false;
            }
            if (this.gs == null) {
                if (aVar.gs != null) {
                    return false;
                }
            } else if (!this.gs.equals(aVar.gs)) {
                return false;
            }
            if (this.auC == null) {
                if (aVar.auC != null) {
                    return false;
                }
            } else if (!this.auC.equals(aVar.auC)) {
                return false;
            }
            return a(aVar);
        }

        public int hashCode() {
            int i = 0;
            int hashCode = ((this.gs == null ? 0 : this.gs.hashCode()) + ((((int) (this.auB ^ (this.auB >>> 32))) + 527) * 31)) * 31;
            if (this.auC != null) {
                i = this.auC.hashCode();
            }
            return ((hashCode + i) * 31) + rQ();
        }

        public a rc() {
            this.auB = 0;
            this.gs = null;
            this.auC = null;
            this.ayW = null;
            this.azh = -1;
            return this;
        }

        /* renamed from: s */
        public a b(qo qoVar) throws IOException {
            while (true) {
                int rz = qoVar.rz();
                switch (rz) {
                    case 0:
                        break;
                    case 8:
                        this.auB = qoVar.rB();
                        continue;
                    case CEventWnd.WM_APP_EXIT /*18*/:
                        if (this.gs == null) {
                            this.gs = new f();
                        }
                        qoVar.a(this.gs);
                        continue;
                    case 26:
                        if (this.auC == null) {
                            this.auC = new j();
                        }
                        qoVar.a(this.auC);
                        continue;
                    default:
                        if (!a(qoVar, rz)) {
                            break;
                        } else {
                            continue;
                        }
                }
            }
            return this;
        }
    }
}
