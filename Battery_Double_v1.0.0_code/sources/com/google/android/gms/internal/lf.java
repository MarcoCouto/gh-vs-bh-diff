package com.google.android.gms.internal;

import android.os.SystemClock;

public final class lf implements ld {
    private static lf Of;

    /* renamed from: if reason: not valid java name */
    public static synchronized ld m2if() {
        lf lfVar;
        synchronized (lf.class) {
            if (Of == null) {
                Of = new lf();
            }
            lfVar = Of;
        }
        return lfVar;
    }

    public long currentTimeMillis() {
        return System.currentTimeMillis();
    }

    public long elapsedRealtime() {
        return SystemClock.elapsedRealtime();
    }
}
