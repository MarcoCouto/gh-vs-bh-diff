package com.google.android.gms.internal;

import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.DeadObjectException;
import android.os.Handler;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.util.Log;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.data.DataHolder;
import java.util.ArrayList;

public abstract class jl<T extends IInterface> implements com.google.android.gms.common.api.Api.a, com.google.android.gms.internal.jm.b {
    public static final String[] MP = {"service_esmobile", "service_googleme"};
    private final Looper JF;
    /* access modifiers changed from: private */
    public final jm JS;
    private T MJ;
    /* access modifiers changed from: private */
    public final ArrayList<b<?>> MK;
    /* access modifiers changed from: private */
    public f ML;
    private int MM;
    private final String[] MN;
    boolean MO;
    /* access modifiers changed from: private */
    public final Context mContext;
    private final Object mH;
    final Handler mHandler;

    final class a extends Handler {
        public a(Looper looper) {
            super(looper);
        }

        public void handleMessage(Message msg) {
            if (msg.what == 1 && !jl.this.isConnecting()) {
                b bVar = (b) msg.obj;
                bVar.hx();
                bVar.unregister();
            } else if (msg.what == 3) {
                jl.this.JS.b(new ConnectionResult(((Integer) msg.obj).intValue(), null));
            } else if (msg.what == 4) {
                jl.this.a(4, null);
                jl.this.JS.aE(((Integer) msg.obj).intValue());
                jl.this.a(4, 1, null);
            } else if (msg.what == 2 && !jl.this.isConnected()) {
                b bVar2 = (b) msg.obj;
                bVar2.hx();
                bVar2.unregister();
            } else if (msg.what == 2 || msg.what == 1) {
                ((b) msg.obj).hy();
            } else {
                Log.wtf("GmsClient", "Don't know how to handle this message.");
            }
        }
    }

    protected abstract class b<TListener> {
        private boolean MR = false;
        private TListener mListener;

        public b(TListener tlistener) {
            this.mListener = tlistener;
        }

        /* access modifiers changed from: protected */
        public abstract void g(TListener tlistener);

        /* access modifiers changed from: protected */
        public abstract void hx();

        public void hy() {
            TListener tlistener;
            synchronized (this) {
                tlistener = this.mListener;
                if (this.MR) {
                    Log.w("GmsClient", "Callback proxy " + this + " being reused. This is not safe.");
                }
            }
            if (tlistener != null) {
                try {
                    g(tlistener);
                } catch (RuntimeException e) {
                    hx();
                    throw e;
                }
            } else {
                hx();
            }
            synchronized (this) {
                this.MR = true;
            }
            unregister();
        }

        public void hz() {
            synchronized (this) {
                this.mListener = null;
            }
        }

        public void unregister() {
            hz();
            synchronized (jl.this.MK) {
                jl.this.MK.remove(this);
            }
        }
    }

    public static final class c implements ConnectionCallbacks {
        private final GooglePlayServicesClient.ConnectionCallbacks MS;

        public c(GooglePlayServicesClient.ConnectionCallbacks connectionCallbacks) {
            this.MS = connectionCallbacks;
        }

        public boolean equals(Object other) {
            return other instanceof c ? this.MS.equals(((c) other).MS) : this.MS.equals(other);
        }

        public void onConnected(Bundle connectionHint) {
            this.MS.onConnected(connectionHint);
        }

        public void onConnectionSuspended(int cause) {
            this.MS.onDisconnected();
        }
    }

    public abstract class d<TListener> extends b<TListener> {
        private final DataHolder JG;

        public d(TListener tlistener, DataHolder dataHolder) {
            super(tlistener);
            this.JG = dataHolder;
        }

        /* access modifiers changed from: protected */
        public abstract void b(TListener tlistener, DataHolder dataHolder);

        /* access modifiers changed from: protected */
        public final void g(TListener tlistener) {
            b(tlistener, this.JG);
        }

        /* access modifiers changed from: protected */
        public void hx() {
            if (this.JG != null) {
                this.JG.close();
            }
        }

        public /* bridge */ /* synthetic */ void hy() {
            super.hy();
        }

        public /* bridge */ /* synthetic */ void hz() {
            super.hz();
        }

        public /* bridge */ /* synthetic */ void unregister() {
            super.unregister();
        }
    }

    public static final class e extends com.google.android.gms.internal.js.a {
        private jl MT;

        public e(jl jlVar) {
            this.MT = jlVar;
        }

        public void b(int i, IBinder iBinder, Bundle bundle) {
            jx.b("onPostInitComplete can be called only once per call to getServiceFromBroker", (Object) this.MT);
            this.MT.a(i, iBinder, bundle);
            this.MT = null;
        }
    }

    public final class f implements ServiceConnection {
        public f() {
        }

        public void onServiceConnected(ComponentName component, IBinder binder) {
            jl.this.N(binder);
        }

        public void onServiceDisconnected(ComponentName component) {
            jl.this.mHandler.sendMessage(jl.this.mHandler.obtainMessage(4, Integer.valueOf(1)));
        }
    }

    public static final class g implements OnConnectionFailedListener {
        private final GooglePlayServicesClient.OnConnectionFailedListener MU;

        public g(GooglePlayServicesClient.OnConnectionFailedListener onConnectionFailedListener) {
            this.MU = onConnectionFailedListener;
        }

        public boolean equals(Object other) {
            return other instanceof g ? this.MU.equals(((g) other).MU) : this.MU.equals(other);
        }

        public void onConnectionFailed(ConnectionResult result) {
            this.MU.onConnectionFailed(result);
        }
    }

    protected final class h extends b<Boolean> {
        public final Bundle MV;
        public final IBinder MW;
        public final int statusCode;

        public h(int i, IBinder iBinder, Bundle bundle) {
            super(Boolean.valueOf(true));
            this.statusCode = i;
            this.MW = iBinder;
            this.MV = bundle;
        }

        /* access modifiers changed from: protected */
        /* renamed from: b */
        public void g(Boolean bool) {
            if (bool == null) {
                jl.this.a(1, null);
                return;
            }
            switch (this.statusCode) {
                case 0:
                    try {
                        if (jl.this.bL().equals(this.MW.getInterfaceDescriptor())) {
                            IInterface l = jl.this.l(this.MW);
                            if (l != null) {
                                jl.this.a(3, l);
                                jl.this.JS.dU();
                                return;
                            }
                        }
                    } catch (RemoteException e) {
                    }
                    jn.J(jl.this.mContext).b(jl.this.bK(), jl.this.ML);
                    jl.this.ML = null;
                    jl.this.a(1, null);
                    jl.this.JS.b(new ConnectionResult(8, null));
                    return;
                case 10:
                    jl.this.a(1, null);
                    throw new IllegalStateException("A fatal developer error has occurred. Check the logs for further information.");
                default:
                    PendingIntent pendingIntent = this.MV != null ? (PendingIntent) this.MV.getParcelable("pendingIntent") : null;
                    if (jl.this.ML != null) {
                        jn.J(jl.this.mContext).b(jl.this.bK(), jl.this.ML);
                        jl.this.ML = null;
                    }
                    jl.this.a(1, null);
                    jl.this.JS.b(new ConnectionResult(this.statusCode, pendingIntent));
                    return;
            }
        }

        /* access modifiers changed from: protected */
        public void hx() {
        }
    }

    protected jl(Context context, Looper looper, ConnectionCallbacks connectionCallbacks, OnConnectionFailedListener onConnectionFailedListener, String... strArr) {
        this.mH = new Object();
        this.MK = new ArrayList<>();
        this.MM = 1;
        this.MO = false;
        this.mContext = (Context) jx.i(context);
        this.JF = (Looper) jx.b(looper, (Object) "Looper must not be null");
        this.JS = new jm(context, looper, this);
        this.mHandler = new a(looper);
        c(strArr);
        this.MN = strArr;
        registerConnectionCallbacks((ConnectionCallbacks) jx.i(connectionCallbacks));
        registerConnectionFailedListener((OnConnectionFailedListener) jx.i(onConnectionFailedListener));
    }

    @Deprecated
    protected jl(Context context, GooglePlayServicesClient.ConnectionCallbacks connectionCallbacks, GooglePlayServicesClient.OnConnectionFailedListener onConnectionFailedListener, String... strArr) {
        this(context, context.getMainLooper(), new c(connectionCallbacks), new g(onConnectionFailedListener), strArr);
    }

    /* access modifiers changed from: private */
    public void a(int i, T t) {
        boolean z = true;
        if ((i == 3) != (t != null)) {
            z = false;
        }
        jx.L(z);
        synchronized (this.mH) {
            this.MM = i;
            this.MJ = t;
        }
    }

    /* access modifiers changed from: private */
    public boolean a(int i, int i2, T t) {
        boolean z;
        synchronized (this.mH) {
            if (this.MM != i) {
                z = false;
            } else {
                a(i2, t);
                z = true;
            }
        }
        return z;
    }

    /* access modifiers changed from: protected */
    public final void N(IBinder iBinder) {
        try {
            a(com.google.android.gms.internal.jt.a.Q(iBinder), new e(this));
        } catch (DeadObjectException e2) {
            Log.w("GmsClient", "service died");
            aD(1);
        } catch (RemoteException e3) {
            Log.w("GmsClient", "Remote exception occurred", e3);
        }
    }

    /* access modifiers changed from: protected */
    public void a(int i, IBinder iBinder, Bundle bundle) {
        this.mHandler.sendMessage(this.mHandler.obtainMessage(1, new h(i, iBinder, bundle)));
    }

    @Deprecated
    public final void a(b<?> bVar) {
        synchronized (this.MK) {
            this.MK.add(bVar);
        }
        this.mHandler.sendMessage(this.mHandler.obtainMessage(2, bVar));
    }

    /* access modifiers changed from: protected */
    public abstract void a(jt jtVar, e eVar) throws RemoteException;

    public void aD(int i) {
        this.mHandler.sendMessage(this.mHandler.obtainMessage(4, Integer.valueOf(i)));
    }

    /* access modifiers changed from: protected */
    public abstract String bK();

    /* access modifiers changed from: protected */
    public abstract String bL();

    /* access modifiers changed from: protected */
    public void c(String... strArr) {
    }

    public void connect() {
        this.MO = true;
        a(2, (T) null);
        int isGooglePlayServicesAvailable = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this.mContext);
        if (isGooglePlayServicesAvailable != 0) {
            a(1, (T) null);
            this.mHandler.sendMessage(this.mHandler.obtainMessage(3, Integer.valueOf(isGooglePlayServicesAvailable)));
            return;
        }
        if (this.ML != null) {
            Log.e("GmsClient", "Calling connect() while still connected, missing disconnect() for " + bK());
            jn.J(this.mContext).b(bK(), this.ML);
        }
        this.ML = new f<>();
        if (!jn.J(this.mContext).a(bK(), this.ML)) {
            Log.e("GmsClient", "unable to connect to service: " + bK());
            this.mHandler.sendMessage(this.mHandler.obtainMessage(3, Integer.valueOf(9)));
        }
    }

    /* access modifiers changed from: protected */
    public final void dS() {
        if (!isConnected()) {
            throw new IllegalStateException("Not connected. Call connect() and wait for onConnected() to be called.");
        }
    }

    public void disconnect() {
        this.MO = false;
        synchronized (this.MK) {
            int size = this.MK.size();
            for (int i = 0; i < size; i++) {
                ((b) this.MK.get(i)).hz();
            }
            this.MK.clear();
        }
        a(1, (T) null);
        if (this.ML != null) {
            jn.J(this.mContext).b(bK(), this.ML);
            this.ML = null;
        }
    }

    public Bundle fX() {
        return null;
    }

    public boolean gN() {
        return this.MO;
    }

    public final Context getContext() {
        return this.mContext;
    }

    public final Looper getLooper() {
        return this.JF;
    }

    public final String[] hv() {
        return this.MN;
    }

    public final T hw() throws DeadObjectException {
        T t;
        synchronized (this.mH) {
            if (this.MM == 4) {
                throw new DeadObjectException();
            }
            dS();
            jx.a(this.MJ != null, "Client is connected but service is null");
            t = this.MJ;
        }
        return t;
    }

    public boolean isConnected() {
        boolean z;
        synchronized (this.mH) {
            z = this.MM == 3;
        }
        return z;
    }

    public boolean isConnecting() {
        boolean z;
        synchronized (this.mH) {
            z = this.MM == 2;
        }
        return z;
    }

    @Deprecated
    public boolean isConnectionCallbacksRegistered(GooglePlayServicesClient.ConnectionCallbacks listener) {
        return this.JS.isConnectionCallbacksRegistered(new c(listener));
    }

    @Deprecated
    public boolean isConnectionFailedListenerRegistered(GooglePlayServicesClient.OnConnectionFailedListener listener) {
        return this.JS.isConnectionFailedListenerRegistered(listener);
    }

    /* access modifiers changed from: protected */
    public abstract T l(IBinder iBinder);

    @Deprecated
    public void registerConnectionCallbacks(GooglePlayServicesClient.ConnectionCallbacks listener) {
        this.JS.registerConnectionCallbacks(new c(listener));
    }

    public void registerConnectionCallbacks(ConnectionCallbacks listener) {
        this.JS.registerConnectionCallbacks(listener);
    }

    @Deprecated
    public void registerConnectionFailedListener(GooglePlayServicesClient.OnConnectionFailedListener listener) {
        this.JS.registerConnectionFailedListener(listener);
    }

    public void registerConnectionFailedListener(OnConnectionFailedListener listener) {
        this.JS.registerConnectionFailedListener(listener);
    }

    @Deprecated
    public void unregisterConnectionCallbacks(GooglePlayServicesClient.ConnectionCallbacks listener) {
        this.JS.unregisterConnectionCallbacks(new c(listener));
    }

    @Deprecated
    public void unregisterConnectionFailedListener(GooglePlayServicesClient.OnConnectionFailedListener listener) {
        this.JS.unregisterConnectionFailedListener(listener);
    }
}
