package com.google.android.gms.internal;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.dlten.lib.frmWork.CEventWnd;
import com.google.android.gms.fitness.request.DataDeleteRequest;
import com.google.android.gms.fitness.request.DataReadRequest;
import com.google.android.gms.fitness.request.DataSourcesRequest;
import com.google.android.gms.fitness.request.DataTypeCreateRequest;
import com.google.android.gms.fitness.request.SessionInsertRequest;
import com.google.android.gms.fitness.request.SessionReadRequest;
import com.google.android.gms.fitness.request.StartBleScanRequest;
import com.google.android.gms.fitness.request.aa;
import com.google.android.gms.fitness.request.ad;
import com.google.android.gms.fitness.request.af;
import com.google.android.gms.fitness.request.ah;
import com.google.android.gms.fitness.request.aj;
import com.google.android.gms.fitness.request.b;
import com.google.android.gms.fitness.request.e;
import com.google.android.gms.fitness.request.j;
import com.google.android.gms.fitness.request.m;
import com.google.android.gms.fitness.request.o;
import com.google.android.gms.fitness.request.q;
import com.google.android.gms.fitness.request.u;
import com.google.android.gms.fitness.request.w;
import com.google.android.gms.fitness.request.y;

public interface lz extends IInterface {

    public static abstract class a extends Binder implements lz {

        /* renamed from: com.google.android.gms.internal.lz$a$a reason: collision with other inner class name */
        private static class C0067a implements lz {
            private IBinder le;

            C0067a(IBinder iBinder) {
                this.le = iBinder;
            }

            public void a(DataDeleteRequest dataDeleteRequest, md mdVar, String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.fitness.internal.IGoogleFitnessService");
                    if (dataDeleteRequest != null) {
                        obtain.writeInt(1);
                        dataDeleteRequest.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    obtain.writeStrongBinder(mdVar != null ? mdVar.asBinder() : null);
                    obtain.writeString(str);
                    this.le.transact(19, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(DataReadRequest dataReadRequest, lw lwVar, String str) throws RemoteException {
                IBinder iBinder = null;
                Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.fitness.internal.IGoogleFitnessService");
                    if (dataReadRequest != null) {
                        obtain.writeInt(1);
                        dataReadRequest.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    if (lwVar != null) {
                        iBinder = lwVar.asBinder();
                    }
                    obtain.writeStrongBinder(iBinder);
                    obtain.writeString(str);
                    this.le.transact(8, obtain, null, 1);
                } finally {
                    obtain.recycle();
                }
            }

            public void a(DataSourcesRequest dataSourcesRequest, lx lxVar, String str) throws RemoteException {
                IBinder iBinder = null;
                Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.fitness.internal.IGoogleFitnessService");
                    if (dataSourcesRequest != null) {
                        obtain.writeInt(1);
                        dataSourcesRequest.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    if (lxVar != null) {
                        iBinder = lxVar.asBinder();
                    }
                    obtain.writeStrongBinder(iBinder);
                    obtain.writeString(str);
                    this.le.transact(1, obtain, null, 1);
                } finally {
                    obtain.recycle();
                }
            }

            public void a(DataTypeCreateRequest dataTypeCreateRequest, ly lyVar, String str) throws RemoteException {
                IBinder iBinder = null;
                Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.fitness.internal.IGoogleFitnessService");
                    if (dataTypeCreateRequest != null) {
                        obtain.writeInt(1);
                        dataTypeCreateRequest.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    if (lyVar != null) {
                        iBinder = lyVar.asBinder();
                    }
                    obtain.writeStrongBinder(iBinder);
                    obtain.writeString(str);
                    this.le.transact(13, obtain, null, 1);
                } finally {
                    obtain.recycle();
                }
            }

            public void a(SessionInsertRequest sessionInsertRequest, md mdVar, String str) throws RemoteException {
                IBinder iBinder = null;
                Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.fitness.internal.IGoogleFitnessService");
                    if (sessionInsertRequest != null) {
                        obtain.writeInt(1);
                        sessionInsertRequest.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    if (mdVar != null) {
                        iBinder = mdVar.asBinder();
                    }
                    obtain.writeStrongBinder(iBinder);
                    obtain.writeString(str);
                    this.le.transact(9, obtain, null, 1);
                } finally {
                    obtain.recycle();
                }
            }

            public void a(SessionReadRequest sessionReadRequest, mb mbVar, String str) throws RemoteException {
                IBinder iBinder = null;
                Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.fitness.internal.IGoogleFitnessService");
                    if (sessionReadRequest != null) {
                        obtain.writeInt(1);
                        sessionReadRequest.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    if (mbVar != null) {
                        iBinder = mbVar.asBinder();
                    }
                    obtain.writeStrongBinder(iBinder);
                    obtain.writeString(str);
                    this.le.transact(10, obtain, null, 1);
                } finally {
                    obtain.recycle();
                }
            }

            public void a(StartBleScanRequest startBleScanRequest, md mdVar, String str) throws RemoteException {
                IBinder iBinder = null;
                Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.fitness.internal.IGoogleFitnessService");
                    if (startBleScanRequest != null) {
                        obtain.writeInt(1);
                        startBleScanRequest.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    if (mdVar != null) {
                        iBinder = mdVar.asBinder();
                    }
                    obtain.writeStrongBinder(iBinder);
                    obtain.writeString(str);
                    this.le.transact(15, obtain, null, 1);
                } finally {
                    obtain.recycle();
                }
            }

            public void a(aa aaVar, md mdVar, String str) throws RemoteException {
                IBinder iBinder = null;
                Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.fitness.internal.IGoogleFitnessService");
                    if (aaVar != null) {
                        obtain.writeInt(1);
                        aaVar.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    if (mdVar != null) {
                        iBinder = mdVar.asBinder();
                    }
                    obtain.writeStrongBinder(iBinder);
                    obtain.writeString(str);
                    this.le.transact(21, obtain, null, 1);
                } finally {
                    obtain.recycle();
                }
            }

            public void a(ad adVar, md mdVar, String str) throws RemoteException {
                IBinder iBinder = null;
                Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.fitness.internal.IGoogleFitnessService");
                    if (adVar != null) {
                        obtain.writeInt(1);
                        adVar.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    if (mdVar != null) {
                        iBinder = mdVar.asBinder();
                    }
                    obtain.writeStrongBinder(iBinder);
                    obtain.writeString(str);
                    this.le.transact(16, obtain, null, 1);
                } finally {
                    obtain.recycle();
                }
            }

            public void a(af afVar, md mdVar, String str) throws RemoteException {
                IBinder iBinder = null;
                Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.fitness.internal.IGoogleFitnessService");
                    if (afVar != null) {
                        obtain.writeInt(1);
                        afVar.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    if (mdVar != null) {
                        iBinder = mdVar.asBinder();
                    }
                    obtain.writeStrongBinder(iBinder);
                    obtain.writeString(str);
                    this.le.transact(4, obtain, null, 1);
                } finally {
                    obtain.recycle();
                }
            }

            public void a(ah ahVar, md mdVar, String str) throws RemoteException {
                IBinder iBinder = null;
                Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.fitness.internal.IGoogleFitnessService");
                    if (ahVar != null) {
                        obtain.writeInt(1);
                        ahVar.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    if (mdVar != null) {
                        iBinder = mdVar.asBinder();
                    }
                    obtain.writeStrongBinder(iBinder);
                    obtain.writeString(str);
                    this.le.transact(18, obtain, null, 1);
                } finally {
                    obtain.recycle();
                }
            }

            public void a(aj ajVar, md mdVar, String str) throws RemoteException {
                IBinder iBinder = null;
                Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.fitness.internal.IGoogleFitnessService");
                    if (ajVar != null) {
                        obtain.writeInt(1);
                        ajVar.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    if (mdVar != null) {
                        iBinder = mdVar.asBinder();
                    }
                    obtain.writeStrongBinder(iBinder);
                    obtain.writeString(str);
                    this.le.transact(5, obtain, null, 1);
                } finally {
                    obtain.recycle();
                }
            }

            public void a(b bVar, md mdVar, String str) throws RemoteException {
                IBinder iBinder = null;
                Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.fitness.internal.IGoogleFitnessService");
                    if (bVar != null) {
                        obtain.writeInt(1);
                        bVar.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    if (mdVar != null) {
                        iBinder = mdVar.asBinder();
                    }
                    obtain.writeStrongBinder(iBinder);
                    obtain.writeString(str);
                    this.le.transact(17, obtain, null, 1);
                } finally {
                    obtain.recycle();
                }
            }

            public void a(e eVar, md mdVar, String str) throws RemoteException {
                IBinder iBinder = null;
                Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.fitness.internal.IGoogleFitnessService");
                    if (eVar != null) {
                        obtain.writeInt(1);
                        eVar.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    if (mdVar != null) {
                        iBinder = mdVar.asBinder();
                    }
                    obtain.writeStrongBinder(iBinder);
                    obtain.writeString(str);
                    this.le.transact(7, obtain, null, 1);
                } finally {
                    obtain.recycle();
                }
            }

            public void a(j jVar, ly lyVar, String str) throws RemoteException {
                IBinder iBinder = null;
                Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.fitness.internal.IGoogleFitnessService");
                    if (jVar != null) {
                        obtain.writeInt(1);
                        jVar.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    if (lyVar != null) {
                        iBinder = lyVar.asBinder();
                    }
                    obtain.writeStrongBinder(iBinder);
                    obtain.writeString(str);
                    this.le.transact(14, obtain, null, 1);
                } finally {
                    obtain.recycle();
                }
            }

            public void a(m mVar, ma maVar, String str) throws RemoteException {
                IBinder iBinder = null;
                Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.fitness.internal.IGoogleFitnessService");
                    if (mVar != null) {
                        obtain.writeInt(1);
                        mVar.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    if (maVar != null) {
                        iBinder = maVar.asBinder();
                    }
                    obtain.writeStrongBinder(iBinder);
                    obtain.writeString(str);
                    this.le.transact(6, obtain, null, 1);
                } finally {
                    obtain.recycle();
                }
            }

            public void a(o oVar, md mdVar, String str) throws RemoteException {
                IBinder iBinder = null;
                Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.fitness.internal.IGoogleFitnessService");
                    if (oVar != null) {
                        obtain.writeInt(1);
                        oVar.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    if (mdVar != null) {
                        iBinder = mdVar.asBinder();
                    }
                    obtain.writeStrongBinder(iBinder);
                    obtain.writeString(str);
                    this.le.transact(2, obtain, null, 1);
                } finally {
                    obtain.recycle();
                }
            }

            public void a(q qVar, md mdVar, String str) throws RemoteException {
                IBinder iBinder = null;
                Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.fitness.internal.IGoogleFitnessService");
                    if (qVar != null) {
                        obtain.writeInt(1);
                        qVar.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    if (mdVar != null) {
                        iBinder = mdVar.asBinder();
                    }
                    obtain.writeStrongBinder(iBinder);
                    obtain.writeString(str);
                    this.le.transact(3, obtain, null, 1);
                } finally {
                    obtain.recycle();
                }
            }

            public void a(u uVar, md mdVar, String str) throws RemoteException {
                IBinder iBinder = null;
                Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.fitness.internal.IGoogleFitnessService");
                    if (uVar != null) {
                        obtain.writeInt(1);
                        uVar.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    if (mdVar != null) {
                        iBinder = mdVar.asBinder();
                    }
                    obtain.writeStrongBinder(iBinder);
                    obtain.writeString(str);
                    this.le.transact(20, obtain, null, 1);
                } finally {
                    obtain.recycle();
                }
            }

            public void a(w wVar, md mdVar, String str) throws RemoteException {
                IBinder iBinder = null;
                Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.fitness.internal.IGoogleFitnessService");
                    if (wVar != null) {
                        obtain.writeInt(1);
                        wVar.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    if (mdVar != null) {
                        iBinder = mdVar.asBinder();
                    }
                    obtain.writeStrongBinder(iBinder);
                    obtain.writeString(str);
                    this.le.transact(11, obtain, null, 1);
                } finally {
                    obtain.recycle();
                }
            }

            public void a(y yVar, mc mcVar, String str) throws RemoteException {
                IBinder iBinder = null;
                Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.fitness.internal.IGoogleFitnessService");
                    if (yVar != null) {
                        obtain.writeInt(1);
                        yVar.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    if (mcVar != null) {
                        iBinder = mcVar.asBinder();
                    }
                    obtain.writeStrongBinder(iBinder);
                    obtain.writeString(str);
                    this.le.transact(12, obtain, null, 1);
                } finally {
                    obtain.recycle();
                }
            }

            public void a(md mdVar, String str) throws RemoteException {
                IBinder iBinder = null;
                Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.fitness.internal.IGoogleFitnessService");
                    if (mdVar != null) {
                        iBinder = mdVar.asBinder();
                    }
                    obtain.writeStrongBinder(iBinder);
                    obtain.writeString(str);
                    this.le.transact(22, obtain, null, 1);
                } finally {
                    obtain.recycle();
                }
            }

            public void a(mp mpVar, String str) throws RemoteException {
                IBinder iBinder = null;
                Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.fitness.internal.IGoogleFitnessService");
                    if (mpVar != null) {
                        iBinder = mpVar.asBinder();
                    }
                    obtain.writeStrongBinder(iBinder);
                    obtain.writeString(str);
                    this.le.transact(24, obtain, null, 1);
                } finally {
                    obtain.recycle();
                }
            }

            public IBinder asBinder() {
                return this.le;
            }

            public void b(md mdVar, String str) throws RemoteException {
                IBinder iBinder = null;
                Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.fitness.internal.IGoogleFitnessService");
                    if (mdVar != null) {
                        iBinder = mdVar.asBinder();
                    }
                    obtain.writeStrongBinder(iBinder);
                    obtain.writeString(str);
                    this.le.transact(23, obtain, null, 1);
                } finally {
                    obtain.recycle();
                }
            }
        }

        public static lz av(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.fitness.internal.IGoogleFitnessService");
            return (queryLocalInterface == null || !(queryLocalInterface instanceof lz)) ? new C0067a(iBinder) : (lz) queryLocalInterface;
        }

        /* JADX WARNING: type inference failed for: r0v0 */
        /* JADX WARNING: type inference failed for: r0v13, types: [com.google.android.gms.fitness.request.aa] */
        /* JADX WARNING: type inference failed for: r0v17, types: [com.google.android.gms.fitness.request.aa] */
        /* JADX WARNING: type inference failed for: r0v18, types: [com.google.android.gms.fitness.request.u] */
        /* JADX WARNING: type inference failed for: r0v22, types: [com.google.android.gms.fitness.request.u] */
        /* JADX WARNING: type inference failed for: r0v23, types: [com.google.android.gms.fitness.request.ah] */
        /* JADX WARNING: type inference failed for: r0v27, types: [com.google.android.gms.fitness.request.ah] */
        /* JADX WARNING: type inference failed for: r0v28, types: [com.google.android.gms.fitness.request.b] */
        /* JADX WARNING: type inference failed for: r0v32, types: [com.google.android.gms.fitness.request.b] */
        /* JADX WARNING: type inference failed for: r0v33, types: [com.google.android.gms.fitness.request.ad] */
        /* JADX WARNING: type inference failed for: r0v37, types: [com.google.android.gms.fitness.request.ad] */
        /* JADX WARNING: type inference failed for: r0v38, types: [com.google.android.gms.fitness.request.StartBleScanRequest] */
        /* JADX WARNING: type inference failed for: r0v42, types: [com.google.android.gms.fitness.request.StartBleScanRequest] */
        /* JADX WARNING: type inference failed for: r0v43, types: [com.google.android.gms.fitness.request.y] */
        /* JADX WARNING: type inference failed for: r0v47, types: [com.google.android.gms.fitness.request.y] */
        /* JADX WARNING: type inference failed for: r0v48, types: [com.google.android.gms.fitness.request.w] */
        /* JADX WARNING: type inference failed for: r0v52, types: [com.google.android.gms.fitness.request.w] */
        /* JADX WARNING: type inference failed for: r0v53, types: [com.google.android.gms.fitness.request.SessionReadRequest] */
        /* JADX WARNING: type inference failed for: r0v57, types: [com.google.android.gms.fitness.request.SessionReadRequest] */
        /* JADX WARNING: type inference failed for: r0v58, types: [com.google.android.gms.fitness.request.SessionInsertRequest] */
        /* JADX WARNING: type inference failed for: r0v62, types: [com.google.android.gms.fitness.request.SessionInsertRequest] */
        /* JADX WARNING: type inference failed for: r0v63, types: [com.google.android.gms.fitness.request.DataReadRequest] */
        /* JADX WARNING: type inference failed for: r0v67, types: [com.google.android.gms.fitness.request.DataReadRequest] */
        /* JADX WARNING: type inference failed for: r0v68, types: [com.google.android.gms.fitness.request.j] */
        /* JADX WARNING: type inference failed for: r0v72, types: [com.google.android.gms.fitness.request.j] */
        /* JADX WARNING: type inference failed for: r0v73, types: [com.google.android.gms.fitness.request.DataTypeCreateRequest] */
        /* JADX WARNING: type inference failed for: r0v77, types: [com.google.android.gms.fitness.request.DataTypeCreateRequest] */
        /* JADX WARNING: type inference failed for: r0v78, types: [com.google.android.gms.fitness.request.DataDeleteRequest] */
        /* JADX WARNING: type inference failed for: r0v82, types: [com.google.android.gms.fitness.request.DataDeleteRequest] */
        /* JADX WARNING: type inference failed for: r0v83, types: [com.google.android.gms.fitness.request.e] */
        /* JADX WARNING: type inference failed for: r0v87, types: [com.google.android.gms.fitness.request.e] */
        /* JADX WARNING: type inference failed for: r0v88, types: [com.google.android.gms.fitness.request.m] */
        /* JADX WARNING: type inference failed for: r0v92, types: [com.google.android.gms.fitness.request.m] */
        /* JADX WARNING: type inference failed for: r0v93, types: [com.google.android.gms.fitness.request.aj] */
        /* JADX WARNING: type inference failed for: r0v97, types: [com.google.android.gms.fitness.request.aj] */
        /* JADX WARNING: type inference failed for: r0v98, types: [com.google.android.gms.fitness.request.af] */
        /* JADX WARNING: type inference failed for: r0v102, types: [com.google.android.gms.fitness.request.af] */
        /* JADX WARNING: type inference failed for: r0v103, types: [com.google.android.gms.fitness.request.q] */
        /* JADX WARNING: type inference failed for: r0v107, types: [com.google.android.gms.fitness.request.q] */
        /* JADX WARNING: type inference failed for: r0v108, types: [com.google.android.gms.fitness.request.o] */
        /* JADX WARNING: type inference failed for: r0v112, types: [com.google.android.gms.fitness.request.o] */
        /* JADX WARNING: type inference failed for: r0v113, types: [com.google.android.gms.fitness.request.DataSourcesRequest] */
        /* JADX WARNING: type inference failed for: r0v117, types: [com.google.android.gms.fitness.request.DataSourcesRequest] */
        /* JADX WARNING: type inference failed for: r0v121 */
        /* JADX WARNING: type inference failed for: r0v122 */
        /* JADX WARNING: type inference failed for: r0v123 */
        /* JADX WARNING: type inference failed for: r0v124 */
        /* JADX WARNING: type inference failed for: r0v125 */
        /* JADX WARNING: type inference failed for: r0v126 */
        /* JADX WARNING: type inference failed for: r0v127 */
        /* JADX WARNING: type inference failed for: r0v128 */
        /* JADX WARNING: type inference failed for: r0v129 */
        /* JADX WARNING: type inference failed for: r0v130 */
        /* JADX WARNING: type inference failed for: r0v131 */
        /* JADX WARNING: type inference failed for: r0v132 */
        /* JADX WARNING: type inference failed for: r0v133 */
        /* JADX WARNING: type inference failed for: r0v134 */
        /* JADX WARNING: type inference failed for: r0v135 */
        /* JADX WARNING: type inference failed for: r0v136 */
        /* JADX WARNING: type inference failed for: r0v137 */
        /* JADX WARNING: type inference failed for: r0v138 */
        /* JADX WARNING: type inference failed for: r0v139 */
        /* JADX WARNING: type inference failed for: r0v140 */
        /* JADX WARNING: type inference failed for: r0v141 */
        /* JADX WARNING: Multi-variable type inference failed. Error: jadx.core.utils.exceptions.JadxRuntimeException: No candidate types for var: r0v0
  assigns: [?[int, float, boolean, short, byte, char, OBJECT, ARRAY], com.google.android.gms.fitness.request.u, com.google.android.gms.fitness.request.aa, com.google.android.gms.fitness.request.ah, com.google.android.gms.fitness.request.b, com.google.android.gms.fitness.request.ad, com.google.android.gms.fitness.request.StartBleScanRequest, com.google.android.gms.fitness.request.y, com.google.android.gms.fitness.request.w, com.google.android.gms.fitness.request.SessionReadRequest, com.google.android.gms.fitness.request.SessionInsertRequest, com.google.android.gms.fitness.request.DataReadRequest, com.google.android.gms.fitness.request.j, com.google.android.gms.fitness.request.DataTypeCreateRequest, com.google.android.gms.fitness.request.DataDeleteRequest, com.google.android.gms.fitness.request.e, com.google.android.gms.fitness.request.m, com.google.android.gms.fitness.request.aj, com.google.android.gms.fitness.request.af, com.google.android.gms.fitness.request.q, com.google.android.gms.fitness.request.o, com.google.android.gms.fitness.request.DataSourcesRequest]
  uses: [com.google.android.gms.fitness.request.aa, com.google.android.gms.fitness.request.u, com.google.android.gms.fitness.request.ah, com.google.android.gms.fitness.request.b, com.google.android.gms.fitness.request.ad, com.google.android.gms.fitness.request.StartBleScanRequest, com.google.android.gms.fitness.request.y, com.google.android.gms.fitness.request.w, com.google.android.gms.fitness.request.SessionReadRequest, com.google.android.gms.fitness.request.SessionInsertRequest, com.google.android.gms.fitness.request.DataReadRequest, com.google.android.gms.fitness.request.j, com.google.android.gms.fitness.request.DataTypeCreateRequest, com.google.android.gms.fitness.request.DataDeleteRequest, com.google.android.gms.fitness.request.e, com.google.android.gms.fitness.request.m, com.google.android.gms.fitness.request.aj, com.google.android.gms.fitness.request.af, com.google.android.gms.fitness.request.q, com.google.android.gms.fitness.request.o, com.google.android.gms.fitness.request.DataSourcesRequest]
  mth insns count: 302
        	at jadx.core.dex.visitors.typeinference.TypeSearch.fillTypeCandidates(TypeSearch.java:237)
        	at java.util.ArrayList.forEach(Unknown Source)
        	at jadx.core.dex.visitors.typeinference.TypeSearch.run(TypeSearch.java:53)
        	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.runMultiVariableSearch(TypeInferenceVisitor.java:99)
        	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.visit(TypeInferenceVisitor.java:92)
        	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:27)
        	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$1(DepthTraversal.java:14)
        	at java.util.ArrayList.forEach(Unknown Source)
        	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
        	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$0(DepthTraversal.java:13)
        	at java.util.ArrayList.forEach(Unknown Source)
        	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:13)
        	at jadx.core.ProcessClass.process(ProcessClass.java:30)
        	at jadx.core.ProcessClass.lambda$processDependencies$0(ProcessClass.java:49)
        	at java.util.ArrayList.forEach(Unknown Source)
        	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:49)
        	at jadx.core.ProcessClass.process(ProcessClass.java:35)
        	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
        	at jadx.api.JavaClass.decompile(JavaClass.java:62)
        	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
         */
        /* JADX WARNING: Unknown variable types count: 22 */
        public boolean onTransact(int code, Parcel data, Parcel reply, int flags) throws RemoteException {
            ? r0 = 0;
            switch (code) {
                case 1:
                    data.enforceInterface("com.google.android.gms.fitness.internal.IGoogleFitnessService");
                    if (data.readInt() != 0) {
                        r0 = (DataSourcesRequest) DataSourcesRequest.CREATOR.createFromParcel(data);
                    }
                    a((DataSourcesRequest) r0, com.google.android.gms.internal.lx.a.at(data.readStrongBinder()), data.readString());
                    return true;
                case 2:
                    data.enforceInterface("com.google.android.gms.fitness.internal.IGoogleFitnessService");
                    if (data.readInt() != 0) {
                        r0 = (o) o.CREATOR.createFromParcel(data);
                    }
                    a((o) r0, com.google.android.gms.internal.md.a.az(data.readStrongBinder()), data.readString());
                    return true;
                case 3:
                    data.enforceInterface("com.google.android.gms.fitness.internal.IGoogleFitnessService");
                    if (data.readInt() != 0) {
                        r0 = (q) q.CREATOR.createFromParcel(data);
                    }
                    a((q) r0, com.google.android.gms.internal.md.a.az(data.readStrongBinder()), data.readString());
                    return true;
                case 4:
                    data.enforceInterface("com.google.android.gms.fitness.internal.IGoogleFitnessService");
                    if (data.readInt() != 0) {
                        r0 = (af) af.CREATOR.createFromParcel(data);
                    }
                    a((af) r0, com.google.android.gms.internal.md.a.az(data.readStrongBinder()), data.readString());
                    return true;
                case 5:
                    data.enforceInterface("com.google.android.gms.fitness.internal.IGoogleFitnessService");
                    if (data.readInt() != 0) {
                        r0 = (aj) aj.CREATOR.createFromParcel(data);
                    }
                    a((aj) r0, com.google.android.gms.internal.md.a.az(data.readStrongBinder()), data.readString());
                    return true;
                case 6:
                    data.enforceInterface("com.google.android.gms.fitness.internal.IGoogleFitnessService");
                    if (data.readInt() != 0) {
                        r0 = (m) m.CREATOR.createFromParcel(data);
                    }
                    a((m) r0, com.google.android.gms.internal.ma.a.aw(data.readStrongBinder()), data.readString());
                    return true;
                case 7:
                    data.enforceInterface("com.google.android.gms.fitness.internal.IGoogleFitnessService");
                    if (data.readInt() != 0) {
                        r0 = (e) e.CREATOR.createFromParcel(data);
                    }
                    a((e) r0, com.google.android.gms.internal.md.a.az(data.readStrongBinder()), data.readString());
                    return true;
                case 8:
                    data.enforceInterface("com.google.android.gms.fitness.internal.IGoogleFitnessService");
                    if (data.readInt() != 0) {
                        r0 = (DataReadRequest) DataReadRequest.CREATOR.createFromParcel(data);
                    }
                    a((DataReadRequest) r0, com.google.android.gms.internal.lw.a.as(data.readStrongBinder()), data.readString());
                    return true;
                case 9:
                    data.enforceInterface("com.google.android.gms.fitness.internal.IGoogleFitnessService");
                    if (data.readInt() != 0) {
                        r0 = (SessionInsertRequest) SessionInsertRequest.CREATOR.createFromParcel(data);
                    }
                    a((SessionInsertRequest) r0, com.google.android.gms.internal.md.a.az(data.readStrongBinder()), data.readString());
                    return true;
                case 10:
                    data.enforceInterface("com.google.android.gms.fitness.internal.IGoogleFitnessService");
                    if (data.readInt() != 0) {
                        r0 = (SessionReadRequest) SessionReadRequest.CREATOR.createFromParcel(data);
                    }
                    a((SessionReadRequest) r0, com.google.android.gms.internal.mb.a.ax(data.readStrongBinder()), data.readString());
                    return true;
                case 11:
                    data.enforceInterface("com.google.android.gms.fitness.internal.IGoogleFitnessService");
                    if (data.readInt() != 0) {
                        r0 = (w) w.CREATOR.createFromParcel(data);
                    }
                    a((w) r0, com.google.android.gms.internal.md.a.az(data.readStrongBinder()), data.readString());
                    return true;
                case 12:
                    data.enforceInterface("com.google.android.gms.fitness.internal.IGoogleFitnessService");
                    if (data.readInt() != 0) {
                        r0 = (y) y.CREATOR.createFromParcel(data);
                    }
                    a((y) r0, com.google.android.gms.internal.mc.a.ay(data.readStrongBinder()), data.readString());
                    return true;
                case 13:
                    data.enforceInterface("com.google.android.gms.fitness.internal.IGoogleFitnessService");
                    if (data.readInt() != 0) {
                        r0 = (DataTypeCreateRequest) DataTypeCreateRequest.CREATOR.createFromParcel(data);
                    }
                    a((DataTypeCreateRequest) r0, com.google.android.gms.internal.ly.a.au(data.readStrongBinder()), data.readString());
                    return true;
                case 14:
                    data.enforceInterface("com.google.android.gms.fitness.internal.IGoogleFitnessService");
                    if (data.readInt() != 0) {
                        r0 = (j) j.CREATOR.createFromParcel(data);
                    }
                    a((j) r0, com.google.android.gms.internal.ly.a.au(data.readStrongBinder()), data.readString());
                    return true;
                case 15:
                    data.enforceInterface("com.google.android.gms.fitness.internal.IGoogleFitnessService");
                    if (data.readInt() != 0) {
                        r0 = (StartBleScanRequest) StartBleScanRequest.CREATOR.createFromParcel(data);
                    }
                    a((StartBleScanRequest) r0, com.google.android.gms.internal.md.a.az(data.readStrongBinder()), data.readString());
                    return true;
                case 16:
                    data.enforceInterface("com.google.android.gms.fitness.internal.IGoogleFitnessService");
                    if (data.readInt() != 0) {
                        r0 = (ad) ad.CREATOR.createFromParcel(data);
                    }
                    a((ad) r0, com.google.android.gms.internal.md.a.az(data.readStrongBinder()), data.readString());
                    return true;
                case CEventWnd.WM_NET /*17*/:
                    data.enforceInterface("com.google.android.gms.fitness.internal.IGoogleFitnessService");
                    if (data.readInt() != 0) {
                        r0 = (b) b.CREATOR.createFromParcel(data);
                    }
                    a((b) r0, com.google.android.gms.internal.md.a.az(data.readStrongBinder()), data.readString());
                    return true;
                case CEventWnd.WM_APP_EXIT /*18*/:
                    data.enforceInterface("com.google.android.gms.fitness.internal.IGoogleFitnessService");
                    if (data.readInt() != 0) {
                        r0 = (ah) ah.CREATOR.createFromParcel(data);
                    }
                    a((ah) r0, com.google.android.gms.internal.md.a.az(data.readStrongBinder()), data.readString());
                    return true;
                case 19:
                    data.enforceInterface("com.google.android.gms.fitness.internal.IGoogleFitnessService");
                    if (data.readInt() != 0) {
                        r0 = (DataDeleteRequest) DataDeleteRequest.CREATOR.createFromParcel(data);
                    }
                    a((DataDeleteRequest) r0, com.google.android.gms.internal.md.a.az(data.readStrongBinder()), data.readString());
                    reply.writeNoException();
                    return true;
                case 20:
                    data.enforceInterface("com.google.android.gms.fitness.internal.IGoogleFitnessService");
                    if (data.readInt() != 0) {
                        r0 = (u) u.CREATOR.createFromParcel(data);
                    }
                    a((u) r0, com.google.android.gms.internal.md.a.az(data.readStrongBinder()), data.readString());
                    return true;
                case 21:
                    data.enforceInterface("com.google.android.gms.fitness.internal.IGoogleFitnessService");
                    if (data.readInt() != 0) {
                        r0 = (aa) aa.CREATOR.createFromParcel(data);
                    }
                    a((aa) r0, com.google.android.gms.internal.md.a.az(data.readStrongBinder()), data.readString());
                    return true;
                case 22:
                    data.enforceInterface("com.google.android.gms.fitness.internal.IGoogleFitnessService");
                    a(com.google.android.gms.internal.md.a.az(data.readStrongBinder()), data.readString());
                    return true;
                case 23:
                    data.enforceInterface("com.google.android.gms.fitness.internal.IGoogleFitnessService");
                    b(com.google.android.gms.internal.md.a.az(data.readStrongBinder()), data.readString());
                    return true;
                case 24:
                    data.enforceInterface("com.google.android.gms.fitness.internal.IGoogleFitnessService");
                    a(com.google.android.gms.internal.mp.a.aA(data.readStrongBinder()), data.readString());
                    return true;
                case 1598968902:
                    reply.writeString("com.google.android.gms.fitness.internal.IGoogleFitnessService");
                    return true;
                default:
                    return super.onTransact(code, data, reply, flags);
            }
        }
    }

    void a(DataDeleteRequest dataDeleteRequest, md mdVar, String str) throws RemoteException;

    void a(DataReadRequest dataReadRequest, lw lwVar, String str) throws RemoteException;

    void a(DataSourcesRequest dataSourcesRequest, lx lxVar, String str) throws RemoteException;

    void a(DataTypeCreateRequest dataTypeCreateRequest, ly lyVar, String str) throws RemoteException;

    void a(SessionInsertRequest sessionInsertRequest, md mdVar, String str) throws RemoteException;

    void a(SessionReadRequest sessionReadRequest, mb mbVar, String str) throws RemoteException;

    void a(StartBleScanRequest startBleScanRequest, md mdVar, String str) throws RemoteException;

    void a(aa aaVar, md mdVar, String str) throws RemoteException;

    void a(ad adVar, md mdVar, String str) throws RemoteException;

    void a(af afVar, md mdVar, String str) throws RemoteException;

    void a(ah ahVar, md mdVar, String str) throws RemoteException;

    void a(aj ajVar, md mdVar, String str) throws RemoteException;

    void a(b bVar, md mdVar, String str) throws RemoteException;

    void a(e eVar, md mdVar, String str) throws RemoteException;

    void a(j jVar, ly lyVar, String str) throws RemoteException;

    void a(m mVar, ma maVar, String str) throws RemoteException;

    void a(o oVar, md mdVar, String str) throws RemoteException;

    void a(q qVar, md mdVar, String str) throws RemoteException;

    void a(u uVar, md mdVar, String str) throws RemoteException;

    void a(w wVar, md mdVar, String str) throws RemoteException;

    void a(y yVar, mc mcVar, String str) throws RemoteException;

    void a(md mdVar, String str) throws RemoteException;

    void a(mp mpVar, String str) throws RemoteException;

    void b(md mdVar, String str) throws RemoteException;
}
