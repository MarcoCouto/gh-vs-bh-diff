package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.a.C0004a;
import com.google.android.gms.internal.hl.b;
import java.util.List;

public class hn implements Creator<b> {
    static void a(b bVar, Parcel parcel, int i) {
        int H = com.google.android.gms.common.internal.safeparcel.b.H(parcel);
        com.google.android.gms.common.internal.safeparcel.b.c(parcel, 1000, bVar.CK);
        com.google.android.gms.common.internal.safeparcel.b.a(parcel, 1, (Parcelable) bVar.Dd, i, false);
        com.google.android.gms.common.internal.safeparcel.b.c(parcel, 2, bVar.De, false);
        com.google.android.gms.common.internal.safeparcel.b.H(parcel, H);
    }

    /* renamed from: O */
    public b[] newArray(int i) {
        return new b[i];
    }

    /* renamed from: q */
    public b createFromParcel(Parcel parcel) {
        List c;
        Status status;
        int i;
        List list = null;
        int G = a.G(parcel);
        int i2 = 0;
        Status status2 = null;
        while (parcel.dataPosition() < G) {
            int F = a.F(parcel);
            switch (a.aH(F)) {
                case 1:
                    i = i2;
                    Status status3 = (Status) a.a(parcel, F, (Creator<T>) Status.CREATOR);
                    c = list;
                    status = status3;
                    break;
                case 2:
                    c = a.c(parcel, F, hr.CREATOR);
                    status = status2;
                    i = i2;
                    break;
                case 1000:
                    List list2 = list;
                    status = status2;
                    i = a.g(parcel, F);
                    c = list2;
                    break;
                default:
                    a.b(parcel, F);
                    c = list;
                    status = status2;
                    i = i2;
                    break;
            }
            i2 = i;
            status2 = status;
            list = c;
        }
        if (parcel.dataPosition() == G) {
            return new b(i2, status2, list);
        }
        throw new C0004a("Overread allowed size end=" + G, parcel);
    }
}
