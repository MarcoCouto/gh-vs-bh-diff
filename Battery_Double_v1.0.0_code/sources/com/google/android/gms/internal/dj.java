package com.google.android.gms.internal;

import android.app.AlertDialog.Builder;
import android.app.DownloadManager;
import android.app.DownloadManager.Request;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.net.Uri;
import android.os.Environment;
import android.text.TextUtils;
import android.webkit.URLUtil;
import com.google.android.gms.R;
import java.util.Map;
import org.json.JSONObject;

@ey
public class dj {
    /* access modifiers changed from: private */
    public final Context mContext;
    /* access modifiers changed from: private */
    public final gu mo;
    private final Map<String, String> rd;

    public dj(gu guVar, Map<String, String> map) {
        this.mo = guVar;
        this.rd = map;
        this.mContext = guVar.dI();
    }

    /* access modifiers changed from: 0000 */
    public String B(String str) {
        return Uri.parse(str).getLastPathSegment();
    }

    /* access modifiers changed from: 0000 */
    public Request b(String str, String str2) {
        Request request = new Request(Uri.parse(str));
        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_PICTURES, str2);
        if (ll.ig()) {
            request.allowScanningByMediaScanner();
            request.setNotificationVisibility(1);
        } else {
            request.setShowRunningNotification(true);
        }
        return request;
    }

    public void execute() {
        if (!new bl(this.mContext).bq()) {
            gr.W("Store picture feature is not supported on this device.");
        } else if (TextUtils.isEmpty((CharSequence) this.rd.get("iurl"))) {
            gr.W("Image url cannot be empty.");
        } else {
            final String str = (String) this.rd.get("iurl");
            if (!URLUtil.isValidUrl(str)) {
                gr.W("Invalid image url:" + str);
                return;
            }
            final String B = B(str);
            if (!gi.N(B)) {
                gr.W("Image type not recognized:");
                return;
            }
            Builder builder = new Builder(this.mContext);
            builder.setTitle(ga.c(R.string.store_picture_title, "Save image"));
            builder.setMessage(ga.c(R.string.store_picture_message, "Allow Ad to store image in Picture gallery?"));
            builder.setPositiveButton(ga.c(R.string.accept, "Accept"), new OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    try {
                        ((DownloadManager) dj.this.mContext.getSystemService("download")).enqueue(dj.this.b(str, B));
                    } catch (IllegalStateException e) {
                        gr.U("Could not store picture.");
                    }
                }
            });
            builder.setNegativeButton(ga.c(R.string.decline, "Decline"), new OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dj.this.mo.b("onStorePictureCanceled", new JSONObject());
                }
            });
            builder.create().show();
        }
    }
}
