package com.google.android.gms.internal;

import com.google.android.gms.internal.da.a;

@ey
public final class ct extends a {
    private final Object mH = new Object();
    private cv.a qD;
    private cs qE;

    public void a(cs csVar) {
        synchronized (this.mH) {
            this.qE = csVar;
        }
    }

    public void a(cv.a aVar) {
        synchronized (this.mH) {
            this.qD = aVar;
        }
    }

    public void onAdClicked() {
        synchronized (this.mH) {
            if (this.qE != null) {
                this.qE.ai();
            }
        }
    }

    public void onAdClosed() {
        synchronized (this.mH) {
            if (this.qE != null) {
                this.qE.aj();
            }
        }
    }

    public void onAdFailedToLoad(int error) {
        synchronized (this.mH) {
            if (this.qD != null) {
                this.qD.k(error == 3 ? 1 : 2);
                this.qD = null;
            }
        }
    }

    public void onAdLeftApplication() {
        synchronized (this.mH) {
            if (this.qE != null) {
                this.qE.ak();
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:?, code lost:
        return;
     */
    public void onAdLoaded() {
        synchronized (this.mH) {
            if (this.qD != null) {
                this.qD.k(0);
                this.qD = null;
            } else if (this.qE != null) {
                this.qE.am();
            }
        }
    }

    public void onAdOpened() {
        synchronized (this.mH) {
            if (this.qE != null) {
                this.qE.al();
            }
        }
    }
}
