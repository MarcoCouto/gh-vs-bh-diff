package com.google.android.gms.internal;

import android.content.Context;
import android.os.Bundle;
import android.os.DeadObjectException;
import android.os.IBinder;
import android.os.Looper;
import android.os.RemoteException;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.internal.jl.e;
import com.google.android.gms.internal.ni.a;

public class nc extends jl<ni> {
    protected final np<ni> Ee = new np<ni>() {
        public void dS() {
            nc.this.dS();
        }

        /* renamed from: nh */
        public ni hw() throws DeadObjectException {
            return (ni) nc.this.hw();
        }
    };
    private final String agD;

    public nc(Context context, Looper looper, ConnectionCallbacks connectionCallbacks, OnConnectionFailedListener onConnectionFailedListener, String str) {
        super(context, looper, connectionCallbacks, onConnectionFailedListener, new String[0]);
        this.agD = str;
    }

    /* access modifiers changed from: protected */
    public void a(jt jtVar, e eVar) throws RemoteException {
        Bundle bundle = new Bundle();
        bundle.putString("client_name", this.agD);
        jtVar.e(eVar, GooglePlayServicesUtil.GOOGLE_PLAY_SERVICES_VERSION_CODE, getContext().getPackageName(), bundle);
    }

    /* access modifiers changed from: protected */
    /* renamed from: aM */
    public ni l(IBinder iBinder) {
        return a.aO(iBinder);
    }

    /* access modifiers changed from: protected */
    public String bK() {
        return "com.google.android.location.internal.GoogleLocationManagerService.START";
    }

    /* access modifiers changed from: protected */
    public String bL() {
        return "com.google.android.gms.location.internal.IGoogleLocationManagerService";
    }
}
