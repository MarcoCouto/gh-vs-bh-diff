package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.a.C0004a;
import com.google.android.gms.common.internal.safeparcel.b;

public class ob implements Creator<oa> {
    static void a(oa oaVar, Parcel parcel, int i) {
        int H = b.H(parcel);
        b.a(parcel, 1, oaVar.vc, false);
        b.c(parcel, 1000, oaVar.CK);
        b.H(parcel, H);
    }

    /* renamed from: cT */
    public oa createFromParcel(Parcel parcel) {
        int G = a.G(parcel);
        int i = 0;
        String str = null;
        while (parcel.dataPosition() < G) {
            int F = a.F(parcel);
            switch (a.aH(F)) {
                case 1:
                    str = a.o(parcel, F);
                    break;
                case 1000:
                    i = a.g(parcel, F);
                    break;
                default:
                    a.b(parcel, F);
                    break;
            }
        }
        if (parcel.dataPosition() == G) {
            return new oa(i, str);
        }
        throw new C0004a("Overread allowed size end=" + G, parcel);
    }

    /* renamed from: eP */
    public oa[] newArray(int i) {
        return new oa[i];
    }
}
