package com.google.android.gms.internal;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.a.C0004a;
import com.google.android.gms.internal.pi.b;
import com.google.android.gms.internal.pi.b.C0082b;
import java.util.HashSet;
import java.util.Set;

public class pl implements Creator<b> {
    static void a(b bVar, Parcel parcel, int i) {
        int H = com.google.android.gms.common.internal.safeparcel.b.H(parcel);
        Set<Integer> set = bVar.aon;
        if (set.contains(Integer.valueOf(1))) {
            com.google.android.gms.common.internal.safeparcel.b.c(parcel, 1, bVar.CK);
        }
        if (set.contains(Integer.valueOf(2))) {
            com.google.android.gms.common.internal.safeparcel.b.a(parcel, 2, (Parcelable) bVar.apG, i, true);
        }
        if (set.contains(Integer.valueOf(3))) {
            com.google.android.gms.common.internal.safeparcel.b.a(parcel, 3, (Parcelable) bVar.apH, i, true);
        }
        if (set.contains(Integer.valueOf(4))) {
            com.google.android.gms.common.internal.safeparcel.b.c(parcel, 4, bVar.apI);
        }
        com.google.android.gms.common.internal.safeparcel.b.H(parcel, H);
    }

    /* renamed from: dB */
    public b createFromParcel(Parcel parcel) {
        C0082b bVar = null;
        int i = 0;
        int G = a.G(parcel);
        HashSet hashSet = new HashSet();
        b.a aVar = null;
        int i2 = 0;
        while (parcel.dataPosition() < G) {
            int F = a.F(parcel);
            switch (a.aH(F)) {
                case 1:
                    i2 = a.g(parcel, F);
                    hashSet.add(Integer.valueOf(1));
                    break;
                case 2:
                    b.a aVar2 = (b.a) a.a(parcel, F, (Creator<T>) b.a.CREATOR);
                    hashSet.add(Integer.valueOf(2));
                    aVar = aVar2;
                    break;
                case 3:
                    C0082b bVar2 = (C0082b) a.a(parcel, F, (Creator<T>) C0082b.CREATOR);
                    hashSet.add(Integer.valueOf(3));
                    bVar = bVar2;
                    break;
                case 4:
                    i = a.g(parcel, F);
                    hashSet.add(Integer.valueOf(4));
                    break;
                default:
                    a.b(parcel, F);
                    break;
            }
        }
        if (parcel.dataPosition() == G) {
            return new b(hashSet, i2, aVar, bVar, i);
        }
        throw new C0004a("Overread allowed size end=" + G, parcel);
    }

    /* renamed from: fz */
    public b[] newArray(int i) {
        return new b[i];
    }
}
