package com.google.android.gms.internal;

import com.dlten.lib.frmWork.CEventWnd;
import com.google.ads.AdSize;
import java.io.IOException;

public interface d {

    public static final class a extends qq<a> {
        private static volatile a[] gu;
        public String gA;
        public long gB;
        public boolean gC;
        public a[] gD;
        public int[] gE;
        public boolean gF;
        public String gv;
        public a[] gw;
        public a[] gx;
        public a[] gy;
        public String gz;
        public int type;

        public a() {
            s();
        }

        public static a[] r() {
            if (gu == null) {
                synchronized (qu.azg) {
                    if (gu == null) {
                        gu = new a[0];
                    }
                }
            }
            return gu;
        }

        public void a(qp qpVar) throws IOException {
            qpVar.t(1, this.type);
            if (!this.gv.equals("")) {
                qpVar.b(2, this.gv);
            }
            if (this.gw != null && this.gw.length > 0) {
                for (a aVar : this.gw) {
                    if (aVar != null) {
                        qpVar.a(3, (qw) aVar);
                    }
                }
            }
            if (this.gx != null && this.gx.length > 0) {
                for (a aVar2 : this.gx) {
                    if (aVar2 != null) {
                        qpVar.a(4, (qw) aVar2);
                    }
                }
            }
            if (this.gy != null && this.gy.length > 0) {
                for (a aVar3 : this.gy) {
                    if (aVar3 != null) {
                        qpVar.a(5, (qw) aVar3);
                    }
                }
            }
            if (!this.gz.equals("")) {
                qpVar.b(6, this.gz);
            }
            if (!this.gA.equals("")) {
                qpVar.b(7, this.gA);
            }
            if (this.gB != 0) {
                qpVar.b(8, this.gB);
            }
            if (this.gF) {
                qpVar.b(9, this.gF);
            }
            if (this.gE != null && this.gE.length > 0) {
                for (int t : this.gE) {
                    qpVar.t(10, t);
                }
            }
            if (this.gD != null && this.gD.length > 0) {
                for (a aVar4 : this.gD) {
                    if (aVar4 != null) {
                        qpVar.a(11, (qw) aVar4);
                    }
                }
            }
            if (this.gC) {
                qpVar.b(12, this.gC);
            }
            super.a(qpVar);
        }

        /* access modifiers changed from: protected */
        public int c() {
            int c = super.c() + qp.v(1, this.type);
            if (!this.gv.equals("")) {
                c += qp.j(2, this.gv);
            }
            if (this.gw != null && this.gw.length > 0) {
                int i = c;
                for (a aVar : this.gw) {
                    if (aVar != null) {
                        i += qp.c(3, (qw) aVar);
                    }
                }
                c = i;
            }
            if (this.gx != null && this.gx.length > 0) {
                int i2 = c;
                for (a aVar2 : this.gx) {
                    if (aVar2 != null) {
                        i2 += qp.c(4, (qw) aVar2);
                    }
                }
                c = i2;
            }
            if (this.gy != null && this.gy.length > 0) {
                int i3 = c;
                for (a aVar3 : this.gy) {
                    if (aVar3 != null) {
                        i3 += qp.c(5, (qw) aVar3);
                    }
                }
                c = i3;
            }
            if (!this.gz.equals("")) {
                c += qp.j(6, this.gz);
            }
            if (!this.gA.equals("")) {
                c += qp.j(7, this.gA);
            }
            if (this.gB != 0) {
                c += qp.d(8, this.gB);
            }
            if (this.gF) {
                c += qp.c(9, this.gF);
            }
            if (this.gE != null && this.gE.length > 0) {
                int i4 = 0;
                for (int gZ : this.gE) {
                    i4 += qp.gZ(gZ);
                }
                c = c + i4 + (this.gE.length * 1);
            }
            if (this.gD != null && this.gD.length > 0) {
                for (a aVar4 : this.gD) {
                    if (aVar4 != null) {
                        c += qp.c(11, (qw) aVar4);
                    }
                }
            }
            return this.gC ? c + qp.c(12, this.gC) : c;
        }

        public boolean equals(Object o) {
            if (o == this) {
                return true;
            }
            if (!(o instanceof a)) {
                return false;
            }
            a aVar = (a) o;
            if (this.type != aVar.type) {
                return false;
            }
            if (this.gv == null) {
                if (aVar.gv != null) {
                    return false;
                }
            } else if (!this.gv.equals(aVar.gv)) {
                return false;
            }
            if (!qu.equals((Object[]) this.gw, (Object[]) aVar.gw) || !qu.equals((Object[]) this.gx, (Object[]) aVar.gx) || !qu.equals((Object[]) this.gy, (Object[]) aVar.gy)) {
                return false;
            }
            if (this.gz == null) {
                if (aVar.gz != null) {
                    return false;
                }
            } else if (!this.gz.equals(aVar.gz)) {
                return false;
            }
            if (this.gA == null) {
                if (aVar.gA != null) {
                    return false;
                }
            } else if (!this.gA.equals(aVar.gA)) {
                return false;
            }
            if (this.gB == aVar.gB && this.gC == aVar.gC && qu.equals((Object[]) this.gD, (Object[]) aVar.gD) && qu.equals(this.gE, aVar.gE) && this.gF == aVar.gF) {
                return a(aVar);
            }
            return false;
        }

        public int hashCode() {
            int i = 1231;
            int i2 = 0;
            int hashCode = ((this.gz == null ? 0 : this.gz.hashCode()) + (((((((((this.gv == null ? 0 : this.gv.hashCode()) + ((this.type + 527) * 31)) * 31) + qu.hashCode((Object[]) this.gw)) * 31) + qu.hashCode((Object[]) this.gx)) * 31) + qu.hashCode((Object[]) this.gy)) * 31)) * 31;
            if (this.gA != null) {
                i2 = this.gA.hashCode();
            }
            int hashCode2 = ((((((this.gC ? 1231 : 1237) + ((((hashCode + i2) * 31) + ((int) (this.gB ^ (this.gB >>> 32)))) * 31)) * 31) + qu.hashCode((Object[]) this.gD)) * 31) + qu.hashCode(this.gE)) * 31;
            if (!this.gF) {
                i = 1237;
            }
            return ((hashCode2 + i) * 31) + rQ();
        }

        /* renamed from: l */
        public a b(qo qoVar) throws IOException {
            int i;
            while (true) {
                int rz = qoVar.rz();
                switch (rz) {
                    case 0:
                        break;
                    case 8:
                        int rC = qoVar.rC();
                        switch (rC) {
                            case 1:
                            case 2:
                            case 3:
                            case 4:
                            case 5:
                            case 6:
                            case 7:
                            case 8:
                                this.type = rC;
                                break;
                            default:
                                continue;
                        }
                    case CEventWnd.WM_APP_EXIT /*18*/:
                        this.gv = qoVar.readString();
                        continue;
                    case 26:
                        int b = qz.b(qoVar, 26);
                        int length = this.gw == null ? 0 : this.gw.length;
                        a[] aVarArr = new a[(b + length)];
                        if (length != 0) {
                            System.arraycopy(this.gw, 0, aVarArr, 0, length);
                        }
                        while (length < aVarArr.length - 1) {
                            aVarArr[length] = new a();
                            qoVar.a(aVarArr[length]);
                            qoVar.rz();
                            length++;
                        }
                        aVarArr[length] = new a();
                        qoVar.a(aVarArr[length]);
                        this.gw = aVarArr;
                        continue;
                    case 34:
                        int b2 = qz.b(qoVar, 34);
                        int length2 = this.gx == null ? 0 : this.gx.length;
                        a[] aVarArr2 = new a[(b2 + length2)];
                        if (length2 != 0) {
                            System.arraycopy(this.gx, 0, aVarArr2, 0, length2);
                        }
                        while (length2 < aVarArr2.length - 1) {
                            aVarArr2[length2] = new a();
                            qoVar.a(aVarArr2[length2]);
                            qoVar.rz();
                            length2++;
                        }
                        aVarArr2[length2] = new a();
                        qoVar.a(aVarArr2[length2]);
                        this.gx = aVarArr2;
                        continue;
                    case 42:
                        int b3 = qz.b(qoVar, 42);
                        int length3 = this.gy == null ? 0 : this.gy.length;
                        a[] aVarArr3 = new a[(b3 + length3)];
                        if (length3 != 0) {
                            System.arraycopy(this.gy, 0, aVarArr3, 0, length3);
                        }
                        while (length3 < aVarArr3.length - 1) {
                            aVarArr3[length3] = new a();
                            qoVar.a(aVarArr3[length3]);
                            qoVar.rz();
                            length3++;
                        }
                        aVarArr3[length3] = new a();
                        qoVar.a(aVarArr3[length3]);
                        this.gy = aVarArr3;
                        continue;
                    case AdSize.PORTRAIT_AD_HEIGHT /*50*/:
                        this.gz = qoVar.readString();
                        continue;
                    case 58:
                        this.gA = qoVar.readString();
                        continue;
                    case 64:
                        this.gB = qoVar.rB();
                        continue;
                    case 72:
                        this.gF = qoVar.rD();
                        continue;
                    case 80:
                        int b4 = qz.b(qoVar, 80);
                        int[] iArr = new int[b4];
                        int i2 = 0;
                        int i3 = 0;
                        while (i2 < b4) {
                            if (i2 != 0) {
                                qoVar.rz();
                            }
                            int rC2 = qoVar.rC();
                            switch (rC2) {
                                case 1:
                                case 2:
                                case 3:
                                case 4:
                                case 5:
                                case 6:
                                case 7:
                                case 8:
                                case 9:
                                case 10:
                                case 11:
                                case 12:
                                case 13:
                                case 14:
                                case 15:
                                case 16:
                                case CEventWnd.WM_NET /*17*/:
                                    i = i3 + 1;
                                    iArr[i3] = rC2;
                                    break;
                                default:
                                    i = i3;
                                    break;
                            }
                            i2++;
                            i3 = i;
                        }
                        if (i3 != 0) {
                            int length4 = this.gE == null ? 0 : this.gE.length;
                            if (length4 != 0 || i3 != iArr.length) {
                                int[] iArr2 = new int[(length4 + i3)];
                                if (length4 != 0) {
                                    System.arraycopy(this.gE, 0, iArr2, 0, length4);
                                }
                                System.arraycopy(iArr, 0, iArr2, length4, i3);
                                this.gE = iArr2;
                                break;
                            } else {
                                this.gE = iArr;
                                break;
                            }
                        } else {
                            continue;
                        }
                    case 82:
                        int gS = qoVar.gS(qoVar.rG());
                        int position = qoVar.getPosition();
                        int i4 = 0;
                        while (qoVar.rL() > 0) {
                            switch (qoVar.rC()) {
                                case 1:
                                case 2:
                                case 3:
                                case 4:
                                case 5:
                                case 6:
                                case 7:
                                case 8:
                                case 9:
                                case 10:
                                case 11:
                                case 12:
                                case 13:
                                case 14:
                                case 15:
                                case 16:
                                case CEventWnd.WM_NET /*17*/:
                                    i4++;
                                    break;
                            }
                        }
                        if (i4 != 0) {
                            qoVar.gU(position);
                            int length5 = this.gE == null ? 0 : this.gE.length;
                            int[] iArr3 = new int[(i4 + length5)];
                            if (length5 != 0) {
                                System.arraycopy(this.gE, 0, iArr3, 0, length5);
                            }
                            while (qoVar.rL() > 0) {
                                int rC3 = qoVar.rC();
                                switch (rC3) {
                                    case 1:
                                    case 2:
                                    case 3:
                                    case 4:
                                    case 5:
                                    case 6:
                                    case 7:
                                    case 8:
                                    case 9:
                                    case 10:
                                    case 11:
                                    case 12:
                                    case 13:
                                    case 14:
                                    case 15:
                                    case 16:
                                    case CEventWnd.WM_NET /*17*/:
                                        int i5 = length5 + 1;
                                        iArr3[length5] = rC3;
                                        length5 = i5;
                                        break;
                                }
                            }
                            this.gE = iArr3;
                        }
                        qoVar.gT(gS);
                        continue;
                    case AdSize.LARGE_AD_HEIGHT /*90*/:
                        int b5 = qz.b(qoVar, 90);
                        int length6 = this.gD == null ? 0 : this.gD.length;
                        a[] aVarArr4 = new a[(b5 + length6)];
                        if (length6 != 0) {
                            System.arraycopy(this.gD, 0, aVarArr4, 0, length6);
                        }
                        while (length6 < aVarArr4.length - 1) {
                            aVarArr4[length6] = new a();
                            qoVar.a(aVarArr4[length6]);
                            qoVar.rz();
                            length6++;
                        }
                        aVarArr4[length6] = new a();
                        qoVar.a(aVarArr4[length6]);
                        this.gD = aVarArr4;
                        continue;
                    case 96:
                        this.gC = qoVar.rD();
                        continue;
                    default:
                        if (!a(qoVar, rz)) {
                            break;
                        } else {
                            continue;
                        }
                }
            }
            return this;
        }

        public a s() {
            this.type = 1;
            this.gv = "";
            this.gw = r();
            this.gx = r();
            this.gy = r();
            this.gz = "";
            this.gA = "";
            this.gB = 0;
            this.gC = false;
            this.gD = r();
            this.gE = qz.azj;
            this.gF = false;
            this.ayW = null;
            this.azh = -1;
            return this;
        }
    }
}
