package com.google.android.gms.internal;

import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.dlten.lib.frmWork.CEventWnd;
import com.google.android.gms.games.Notifications;

public interface jt extends IInterface {

    public static abstract class a extends Binder implements jt {

        /* renamed from: com.google.android.gms.internal.jt$a$a reason: collision with other inner class name */
        private static class C0060a implements jt {
            private IBinder le;

            C0060a(IBinder iBinder) {
                this.le = iBinder;
            }

            public void a(js jsVar, int i) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.common.internal.IGmsServiceBroker");
                    obtain.writeStrongBinder(jsVar != null ? jsVar.asBinder() : null);
                    obtain.writeInt(i);
                    this.le.transact(4, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(js jsVar, int i, String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.common.internal.IGmsServiceBroker");
                    obtain.writeStrongBinder(jsVar != null ? jsVar.asBinder() : null);
                    obtain.writeInt(i);
                    obtain.writeString(str);
                    this.le.transact(3, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(js jsVar, int i, String str, Bundle bundle) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.common.internal.IGmsServiceBroker");
                    obtain.writeStrongBinder(jsVar != null ? jsVar.asBinder() : null);
                    obtain.writeInt(i);
                    obtain.writeString(str);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.le.transact(2, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(js jsVar, int i, String str, IBinder iBinder, Bundle bundle) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.common.internal.IGmsServiceBroker");
                    obtain.writeStrongBinder(jsVar != null ? jsVar.asBinder() : null);
                    obtain.writeInt(i);
                    obtain.writeString(str);
                    obtain.writeStrongBinder(iBinder);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.le.transact(19, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(js jsVar, int i, String str, String str2) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.common.internal.IGmsServiceBroker");
                    obtain.writeStrongBinder(jsVar != null ? jsVar.asBinder() : null);
                    obtain.writeInt(i);
                    obtain.writeString(str);
                    obtain.writeString(str2);
                    this.le.transact(34, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(js jsVar, int i, String str, String str2, String str3, String[] strArr) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.common.internal.IGmsServiceBroker");
                    obtain.writeStrongBinder(jsVar != null ? jsVar.asBinder() : null);
                    obtain.writeInt(i);
                    obtain.writeString(str);
                    obtain.writeString(str2);
                    obtain.writeString(str3);
                    obtain.writeStringArray(strArr);
                    this.le.transact(33, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(js jsVar, int i, String str, String str2, String[] strArr) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.common.internal.IGmsServiceBroker");
                    obtain.writeStrongBinder(jsVar != null ? jsVar.asBinder() : null);
                    obtain.writeInt(i);
                    obtain.writeString(str);
                    obtain.writeString(str2);
                    obtain.writeStringArray(strArr);
                    this.le.transact(10, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(js jsVar, int i, String str, String str2, String[] strArr, Bundle bundle) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.common.internal.IGmsServiceBroker");
                    obtain.writeStrongBinder(jsVar != null ? jsVar.asBinder() : null);
                    obtain.writeInt(i);
                    obtain.writeString(str);
                    obtain.writeString(str2);
                    obtain.writeStringArray(strArr);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.le.transact(30, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(js jsVar, int i, String str, String str2, String[] strArr, String str3, Bundle bundle) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.common.internal.IGmsServiceBroker");
                    obtain.writeStrongBinder(jsVar != null ? jsVar.asBinder() : null);
                    obtain.writeInt(i);
                    obtain.writeString(str);
                    obtain.writeString(str2);
                    obtain.writeStringArray(strArr);
                    obtain.writeString(str3);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.le.transact(1, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(js jsVar, int i, String str, String str2, String[] strArr, String str3, IBinder iBinder, String str4, Bundle bundle) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.common.internal.IGmsServiceBroker");
                    obtain.writeStrongBinder(jsVar != null ? jsVar.asBinder() : null);
                    obtain.writeInt(i);
                    obtain.writeString(str);
                    obtain.writeString(str2);
                    obtain.writeStringArray(strArr);
                    obtain.writeString(str3);
                    obtain.writeStrongBinder(iBinder);
                    obtain.writeString(str4);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.le.transact(9, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(js jsVar, int i, String str, String[] strArr, String str2, Bundle bundle) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.common.internal.IGmsServiceBroker");
                    obtain.writeStrongBinder(jsVar != null ? jsVar.asBinder() : null);
                    obtain.writeInt(i);
                    obtain.writeString(str);
                    obtain.writeStringArray(strArr);
                    obtain.writeString(str2);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.le.transact(20, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(js jsVar, jj jjVar) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.common.internal.IGmsServiceBroker");
                    obtain.writeStrongBinder(jsVar != null ? jsVar.asBinder() : null);
                    if (jjVar != null) {
                        obtain.writeInt(1);
                        jjVar.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.le.transact(46, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public IBinder asBinder() {
                return this.le;
            }

            public void b(js jsVar, int i, String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.common.internal.IGmsServiceBroker");
                    obtain.writeStrongBinder(jsVar != null ? jsVar.asBinder() : null);
                    obtain.writeInt(i);
                    obtain.writeString(str);
                    this.le.transact(21, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void b(js jsVar, int i, String str, Bundle bundle) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.common.internal.IGmsServiceBroker");
                    obtain.writeStrongBinder(jsVar != null ? jsVar.asBinder() : null);
                    obtain.writeInt(i);
                    obtain.writeString(str);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.le.transact(5, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void b(js jsVar, int i, String str, String str2, String[] strArr) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.common.internal.IGmsServiceBroker");
                    obtain.writeStrongBinder(jsVar != null ? jsVar.asBinder() : null);
                    obtain.writeInt(i);
                    obtain.writeString(str);
                    obtain.writeString(str2);
                    obtain.writeStringArray(strArr);
                    this.le.transact(28, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void c(js jsVar, int i, String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.common.internal.IGmsServiceBroker");
                    obtain.writeStrongBinder(jsVar != null ? jsVar.asBinder() : null);
                    obtain.writeInt(i);
                    obtain.writeString(str);
                    this.le.transact(22, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void c(js jsVar, int i, String str, Bundle bundle) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.common.internal.IGmsServiceBroker");
                    obtain.writeStrongBinder(jsVar != null ? jsVar.asBinder() : null);
                    obtain.writeInt(i);
                    obtain.writeString(str);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.le.transact(6, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void d(js jsVar, int i, String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.common.internal.IGmsServiceBroker");
                    obtain.writeStrongBinder(jsVar != null ? jsVar.asBinder() : null);
                    obtain.writeInt(i);
                    obtain.writeString(str);
                    this.le.transact(24, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void d(js jsVar, int i, String str, Bundle bundle) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.common.internal.IGmsServiceBroker");
                    obtain.writeStrongBinder(jsVar != null ? jsVar.asBinder() : null);
                    obtain.writeInt(i);
                    obtain.writeString(str);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.le.transact(7, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void e(js jsVar, int i, String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.common.internal.IGmsServiceBroker");
                    obtain.writeStrongBinder(jsVar != null ? jsVar.asBinder() : null);
                    obtain.writeInt(i);
                    obtain.writeString(str);
                    this.le.transact(26, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void e(js jsVar, int i, String str, Bundle bundle) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.common.internal.IGmsServiceBroker");
                    obtain.writeStrongBinder(jsVar != null ? jsVar.asBinder() : null);
                    obtain.writeInt(i);
                    obtain.writeString(str);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.le.transact(8, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void f(js jsVar, int i, String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.common.internal.IGmsServiceBroker");
                    obtain.writeStrongBinder(jsVar != null ? jsVar.asBinder() : null);
                    obtain.writeInt(i);
                    obtain.writeString(str);
                    this.le.transact(31, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void f(js jsVar, int i, String str, Bundle bundle) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.common.internal.IGmsServiceBroker");
                    obtain.writeStrongBinder(jsVar != null ? jsVar.asBinder() : null);
                    obtain.writeInt(i);
                    obtain.writeString(str);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.le.transact(11, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void g(js jsVar, int i, String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.common.internal.IGmsServiceBroker");
                    obtain.writeStrongBinder(jsVar != null ? jsVar.asBinder() : null);
                    obtain.writeInt(i);
                    obtain.writeString(str);
                    this.le.transact(32, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void g(js jsVar, int i, String str, Bundle bundle) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.common.internal.IGmsServiceBroker");
                    obtain.writeStrongBinder(jsVar != null ? jsVar.asBinder() : null);
                    obtain.writeInt(i);
                    obtain.writeString(str);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.le.transact(12, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void h(js jsVar, int i, String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.common.internal.IGmsServiceBroker");
                    obtain.writeStrongBinder(jsVar != null ? jsVar.asBinder() : null);
                    obtain.writeInt(i);
                    obtain.writeString(str);
                    this.le.transact(35, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void h(js jsVar, int i, String str, Bundle bundle) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.common.internal.IGmsServiceBroker");
                    obtain.writeStrongBinder(jsVar != null ? jsVar.asBinder() : null);
                    obtain.writeInt(i);
                    obtain.writeString(str);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.le.transact(13, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void i(js jsVar, int i, String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.common.internal.IGmsServiceBroker");
                    obtain.writeStrongBinder(jsVar != null ? jsVar.asBinder() : null);
                    obtain.writeInt(i);
                    obtain.writeString(str);
                    this.le.transact(36, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void i(js jsVar, int i, String str, Bundle bundle) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.common.internal.IGmsServiceBroker");
                    obtain.writeStrongBinder(jsVar != null ? jsVar.asBinder() : null);
                    obtain.writeInt(i);
                    obtain.writeString(str);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.le.transact(14, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void j(js jsVar, int i, String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.common.internal.IGmsServiceBroker");
                    obtain.writeStrongBinder(jsVar != null ? jsVar.asBinder() : null);
                    obtain.writeInt(i);
                    obtain.writeString(str);
                    this.le.transact(40, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void j(js jsVar, int i, String str, Bundle bundle) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.common.internal.IGmsServiceBroker");
                    obtain.writeStrongBinder(jsVar != null ? jsVar.asBinder() : null);
                    obtain.writeInt(i);
                    obtain.writeString(str);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.le.transact(15, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void k(js jsVar, int i, String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.common.internal.IGmsServiceBroker");
                    obtain.writeStrongBinder(jsVar != null ? jsVar.asBinder() : null);
                    obtain.writeInt(i);
                    obtain.writeString(str);
                    this.le.transact(42, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void k(js jsVar, int i, String str, Bundle bundle) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.common.internal.IGmsServiceBroker");
                    obtain.writeStrongBinder(jsVar != null ? jsVar.asBinder() : null);
                    obtain.writeInt(i);
                    obtain.writeString(str);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.le.transact(16, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void l(js jsVar, int i, String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.common.internal.IGmsServiceBroker");
                    obtain.writeStrongBinder(jsVar != null ? jsVar.asBinder() : null);
                    obtain.writeInt(i);
                    obtain.writeString(str);
                    this.le.transact(44, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void l(js jsVar, int i, String str, Bundle bundle) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.common.internal.IGmsServiceBroker");
                    obtain.writeStrongBinder(jsVar != null ? jsVar.asBinder() : null);
                    obtain.writeInt(i);
                    obtain.writeString(str);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.le.transact(17, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void m(js jsVar, int i, String str, Bundle bundle) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.common.internal.IGmsServiceBroker");
                    obtain.writeStrongBinder(jsVar != null ? jsVar.asBinder() : null);
                    obtain.writeInt(i);
                    obtain.writeString(str);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.le.transact(18, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void n(js jsVar, int i, String str, Bundle bundle) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.common.internal.IGmsServiceBroker");
                    obtain.writeStrongBinder(jsVar != null ? jsVar.asBinder() : null);
                    obtain.writeInt(i);
                    obtain.writeString(str);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.le.transact(23, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void o(js jsVar, int i, String str, Bundle bundle) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.common.internal.IGmsServiceBroker");
                    obtain.writeStrongBinder(jsVar != null ? jsVar.asBinder() : null);
                    obtain.writeInt(i);
                    obtain.writeString(str);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.le.transact(25, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void p(js jsVar, int i, String str, Bundle bundle) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.common.internal.IGmsServiceBroker");
                    obtain.writeStrongBinder(jsVar != null ? jsVar.asBinder() : null);
                    obtain.writeInt(i);
                    obtain.writeString(str);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.le.transact(27, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void q(js jsVar, int i, String str, Bundle bundle) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.common.internal.IGmsServiceBroker");
                    obtain.writeStrongBinder(jsVar != null ? jsVar.asBinder() : null);
                    obtain.writeInt(i);
                    obtain.writeString(str);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.le.transact(37, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void r(js jsVar, int i, String str, Bundle bundle) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.common.internal.IGmsServiceBroker");
                    obtain.writeStrongBinder(jsVar != null ? jsVar.asBinder() : null);
                    obtain.writeInt(i);
                    obtain.writeString(str);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.le.transact(38, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void s(js jsVar, int i, String str, Bundle bundle) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.common.internal.IGmsServiceBroker");
                    obtain.writeStrongBinder(jsVar != null ? jsVar.asBinder() : null);
                    obtain.writeInt(i);
                    obtain.writeString(str);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.le.transact(41, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void t(js jsVar, int i, String str, Bundle bundle) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.common.internal.IGmsServiceBroker");
                    obtain.writeStrongBinder(jsVar != null ? jsVar.asBinder() : null);
                    obtain.writeInt(i);
                    obtain.writeString(str);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.le.transact(43, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
        }

        public static jt Q(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
            return (queryLocalInterface == null || !(queryLocalInterface instanceof jt)) ? new C0060a(iBinder) : (jt) queryLocalInterface;
        }

        /* JADX WARNING: type inference failed for: r0v0 */
        /* JADX WARNING: type inference failed for: r0v1, types: [com.google.android.gms.internal.jj] */
        /* JADX WARNING: type inference failed for: r0v5, types: [com.google.android.gms.internal.jj] */
        /* JADX WARNING: type inference failed for: r0v10, types: [android.os.Bundle] */
        /* JADX WARNING: type inference failed for: r0v14, types: [android.os.Bundle] */
        /* JADX WARNING: type inference failed for: r0v19, types: [android.os.Bundle] */
        /* JADX WARNING: type inference failed for: r0v23, types: [android.os.Bundle] */
        /* JADX WARNING: type inference failed for: r0v28, types: [android.os.Bundle] */
        /* JADX WARNING: type inference failed for: r0v32, types: [android.os.Bundle] */
        /* JADX WARNING: type inference failed for: r0v33, types: [android.os.Bundle] */
        /* JADX WARNING: type inference failed for: r0v37, types: [android.os.Bundle] */
        /* JADX WARNING: type inference failed for: r0v71, types: [android.os.Bundle] */
        /* JADX WARNING: type inference failed for: r0v75, types: [android.os.Bundle] */
        /* JADX WARNING: type inference failed for: r0v80, types: [android.os.Bundle] */
        /* JADX WARNING: type inference failed for: r0v84, types: [android.os.Bundle] */
        /* JADX WARNING: type inference failed for: r0v89, types: [android.os.Bundle] */
        /* JADX WARNING: type inference failed for: r0v93, types: [android.os.Bundle] */
        /* JADX WARNING: type inference failed for: r0v112, types: [android.os.Bundle] */
        /* JADX WARNING: type inference failed for: r0v116, types: [android.os.Bundle] */
        /* JADX WARNING: type inference failed for: r0v117, types: [android.os.Bundle] */
        /* JADX WARNING: type inference failed for: r0v121, types: [android.os.Bundle] */
        /* JADX WARNING: type inference failed for: r0v122, types: [android.os.Bundle] */
        /* JADX WARNING: type inference failed for: r0v126, types: [android.os.Bundle] */
        /* JADX WARNING: type inference failed for: r0v127, types: [android.os.Bundle] */
        /* JADX WARNING: type inference failed for: r0v131, types: [android.os.Bundle] */
        /* JADX WARNING: type inference failed for: r0v132, types: [android.os.Bundle] */
        /* JADX WARNING: type inference failed for: r0v136, types: [android.os.Bundle] */
        /* JADX WARNING: type inference failed for: r0v137, types: [android.os.Bundle] */
        /* JADX WARNING: type inference failed for: r0v141, types: [android.os.Bundle] */
        /* JADX WARNING: type inference failed for: r0v142, types: [android.os.Bundle] */
        /* JADX WARNING: type inference failed for: r0v146, types: [android.os.Bundle] */
        /* JADX WARNING: type inference failed for: r0v147, types: [android.os.Bundle] */
        /* JADX WARNING: type inference failed for: r0v151, types: [android.os.Bundle] */
        /* JADX WARNING: type inference failed for: r0v161, types: [android.os.Bundle] */
        /* JADX WARNING: type inference failed for: r0v165, types: [android.os.Bundle] */
        /* JADX WARNING: type inference failed for: r0v166, types: [android.os.Bundle] */
        /* JADX WARNING: type inference failed for: r0v170, types: [android.os.Bundle] */
        /* JADX WARNING: type inference failed for: r0v171, types: [android.os.Bundle] */
        /* JADX WARNING: type inference failed for: r0v175, types: [android.os.Bundle] */
        /* JADX WARNING: type inference failed for: r0v176, types: [android.os.Bundle] */
        /* JADX WARNING: type inference failed for: r0v180, types: [android.os.Bundle] */
        /* JADX WARNING: type inference failed for: r0v189, types: [android.os.Bundle] */
        /* JADX WARNING: type inference failed for: r0v193, types: [android.os.Bundle] */
        /* JADX WARNING: type inference failed for: r0v202 */
        /* JADX WARNING: type inference failed for: r0v203 */
        /* JADX WARNING: type inference failed for: r0v204 */
        /* JADX WARNING: type inference failed for: r0v205 */
        /* JADX WARNING: type inference failed for: r0v206 */
        /* JADX WARNING: type inference failed for: r0v207 */
        /* JADX WARNING: type inference failed for: r0v208 */
        /* JADX WARNING: type inference failed for: r0v209 */
        /* JADX WARNING: type inference failed for: r0v210 */
        /* JADX WARNING: type inference failed for: r0v211 */
        /* JADX WARNING: type inference failed for: r0v212 */
        /* JADX WARNING: type inference failed for: r0v213 */
        /* JADX WARNING: type inference failed for: r0v214 */
        /* JADX WARNING: type inference failed for: r0v215 */
        /* JADX WARNING: type inference failed for: r0v216 */
        /* JADX WARNING: type inference failed for: r0v217 */
        /* JADX WARNING: type inference failed for: r0v218 */
        /* JADX WARNING: type inference failed for: r0v219 */
        /* JADX WARNING: type inference failed for: r0v220 */
        /* JADX WARNING: type inference failed for: r0v221 */
        /* JADX WARNING: type inference failed for: r0v222 */
        /* JADX WARNING: Multi-variable type inference failed. Error: jadx.core.utils.exceptions.JadxRuntimeException: No candidate types for var: r0v0
  assigns: [?[int, float, boolean, short, byte, char, OBJECT, ARRAY], android.os.Bundle, com.google.android.gms.internal.jj]
  uses: [com.google.android.gms.internal.jj, android.os.Bundle]
  mth insns count: 581
        	at jadx.core.dex.visitors.typeinference.TypeSearch.fillTypeCandidates(TypeSearch.java:237)
        	at java.util.ArrayList.forEach(Unknown Source)
        	at jadx.core.dex.visitors.typeinference.TypeSearch.run(TypeSearch.java:53)
        	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.runMultiVariableSearch(TypeInferenceVisitor.java:99)
        	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.visit(TypeInferenceVisitor.java:92)
        	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:27)
        	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$1(DepthTraversal.java:14)
        	at java.util.ArrayList.forEach(Unknown Source)
        	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
        	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$0(DepthTraversal.java:13)
        	at java.util.ArrayList.forEach(Unknown Source)
        	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:13)
        	at jadx.core.ProcessClass.process(ProcessClass.java:30)
        	at jadx.core.ProcessClass.lambda$processDependencies$0(ProcessClass.java:49)
        	at java.util.ArrayList.forEach(Unknown Source)
        	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:49)
        	at jadx.core.ProcessClass.process(ProcessClass.java:35)
        	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
        	at jadx.api.JavaClass.decompile(JavaClass.java:62)
        	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
         */
        /* JADX WARNING: Unknown variable types count: 22 */
        public boolean onTransact(int code, Parcel data, Parcel reply, int flags) throws RemoteException {
            ? r0 = 0;
            switch (code) {
                case 1:
                    data.enforceInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
                    a(com.google.android.gms.internal.js.a.P(data.readStrongBinder()), data.readInt(), data.readString(), data.readString(), data.createStringArray(), data.readString(), data.readInt() != 0 ? (Bundle) Bundle.CREATOR.createFromParcel(data) : null);
                    reply.writeNoException();
                    return true;
                case 2:
                    data.enforceInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
                    js P = com.google.android.gms.internal.js.a.P(data.readStrongBinder());
                    int readInt = data.readInt();
                    String readString = data.readString();
                    if (data.readInt() != 0) {
                        r0 = (Bundle) Bundle.CREATOR.createFromParcel(data);
                    }
                    a(P, readInt, readString, (Bundle) r0);
                    reply.writeNoException();
                    return true;
                case 3:
                    data.enforceInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
                    a(com.google.android.gms.internal.js.a.P(data.readStrongBinder()), data.readInt(), data.readString());
                    reply.writeNoException();
                    return true;
                case 4:
                    data.enforceInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
                    a(com.google.android.gms.internal.js.a.P(data.readStrongBinder()), data.readInt());
                    reply.writeNoException();
                    return true;
                case 5:
                    data.enforceInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
                    js P2 = com.google.android.gms.internal.js.a.P(data.readStrongBinder());
                    int readInt2 = data.readInt();
                    String readString2 = data.readString();
                    if (data.readInt() != 0) {
                        r0 = (Bundle) Bundle.CREATOR.createFromParcel(data);
                    }
                    b(P2, readInt2, readString2, r0);
                    reply.writeNoException();
                    return true;
                case 6:
                    data.enforceInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
                    js P3 = com.google.android.gms.internal.js.a.P(data.readStrongBinder());
                    int readInt3 = data.readInt();
                    String readString3 = data.readString();
                    if (data.readInt() != 0) {
                        r0 = (Bundle) Bundle.CREATOR.createFromParcel(data);
                    }
                    c(P3, readInt3, readString3, r0);
                    reply.writeNoException();
                    return true;
                case 7:
                    data.enforceInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
                    js P4 = com.google.android.gms.internal.js.a.P(data.readStrongBinder());
                    int readInt4 = data.readInt();
                    String readString4 = data.readString();
                    if (data.readInt() != 0) {
                        r0 = (Bundle) Bundle.CREATOR.createFromParcel(data);
                    }
                    d(P4, readInt4, readString4, r0);
                    reply.writeNoException();
                    return true;
                case 8:
                    data.enforceInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
                    js P5 = com.google.android.gms.internal.js.a.P(data.readStrongBinder());
                    int readInt5 = data.readInt();
                    String readString5 = data.readString();
                    if (data.readInt() != 0) {
                        r0 = (Bundle) Bundle.CREATOR.createFromParcel(data);
                    }
                    e(P5, readInt5, readString5, r0);
                    reply.writeNoException();
                    return true;
                case 9:
                    data.enforceInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
                    a(com.google.android.gms.internal.js.a.P(data.readStrongBinder()), data.readInt(), data.readString(), data.readString(), data.createStringArray(), data.readString(), data.readStrongBinder(), data.readString(), data.readInt() != 0 ? (Bundle) Bundle.CREATOR.createFromParcel(data) : null);
                    reply.writeNoException();
                    return true;
                case 10:
                    data.enforceInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
                    a(com.google.android.gms.internal.js.a.P(data.readStrongBinder()), data.readInt(), data.readString(), data.readString(), data.createStringArray());
                    reply.writeNoException();
                    return true;
                case 11:
                    data.enforceInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
                    js P6 = com.google.android.gms.internal.js.a.P(data.readStrongBinder());
                    int readInt6 = data.readInt();
                    String readString6 = data.readString();
                    if (data.readInt() != 0) {
                        r0 = (Bundle) Bundle.CREATOR.createFromParcel(data);
                    }
                    f(P6, readInt6, readString6, r0);
                    reply.writeNoException();
                    return true;
                case 12:
                    data.enforceInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
                    js P7 = com.google.android.gms.internal.js.a.P(data.readStrongBinder());
                    int readInt7 = data.readInt();
                    String readString7 = data.readString();
                    if (data.readInt() != 0) {
                        r0 = (Bundle) Bundle.CREATOR.createFromParcel(data);
                    }
                    g(P7, readInt7, readString7, r0);
                    reply.writeNoException();
                    return true;
                case 13:
                    data.enforceInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
                    js P8 = com.google.android.gms.internal.js.a.P(data.readStrongBinder());
                    int readInt8 = data.readInt();
                    String readString8 = data.readString();
                    if (data.readInt() != 0) {
                        r0 = (Bundle) Bundle.CREATOR.createFromParcel(data);
                    }
                    h(P8, readInt8, readString8, r0);
                    reply.writeNoException();
                    return true;
                case 14:
                    data.enforceInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
                    js P9 = com.google.android.gms.internal.js.a.P(data.readStrongBinder());
                    int readInt9 = data.readInt();
                    String readString9 = data.readString();
                    if (data.readInt() != 0) {
                        r0 = (Bundle) Bundle.CREATOR.createFromParcel(data);
                    }
                    i(P9, readInt9, readString9, r0);
                    reply.writeNoException();
                    return true;
                case 15:
                    data.enforceInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
                    js P10 = com.google.android.gms.internal.js.a.P(data.readStrongBinder());
                    int readInt10 = data.readInt();
                    String readString10 = data.readString();
                    if (data.readInt() != 0) {
                        r0 = (Bundle) Bundle.CREATOR.createFromParcel(data);
                    }
                    j(P10, readInt10, readString10, r0);
                    reply.writeNoException();
                    return true;
                case 16:
                    data.enforceInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
                    js P11 = com.google.android.gms.internal.js.a.P(data.readStrongBinder());
                    int readInt11 = data.readInt();
                    String readString11 = data.readString();
                    if (data.readInt() != 0) {
                        r0 = (Bundle) Bundle.CREATOR.createFromParcel(data);
                    }
                    k(P11, readInt11, readString11, r0);
                    reply.writeNoException();
                    return true;
                case CEventWnd.WM_NET /*17*/:
                    data.enforceInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
                    js P12 = com.google.android.gms.internal.js.a.P(data.readStrongBinder());
                    int readInt12 = data.readInt();
                    String readString12 = data.readString();
                    if (data.readInt() != 0) {
                        r0 = (Bundle) Bundle.CREATOR.createFromParcel(data);
                    }
                    l(P12, readInt12, readString12, r0);
                    reply.writeNoException();
                    return true;
                case CEventWnd.WM_APP_EXIT /*18*/:
                    data.enforceInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
                    js P13 = com.google.android.gms.internal.js.a.P(data.readStrongBinder());
                    int readInt13 = data.readInt();
                    String readString13 = data.readString();
                    if (data.readInt() != 0) {
                        r0 = (Bundle) Bundle.CREATOR.createFromParcel(data);
                    }
                    m(P13, readInt13, readString13, r0);
                    reply.writeNoException();
                    return true;
                case 19:
                    data.enforceInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
                    a(com.google.android.gms.internal.js.a.P(data.readStrongBinder()), data.readInt(), data.readString(), data.readStrongBinder(), data.readInt() != 0 ? (Bundle) Bundle.CREATOR.createFromParcel(data) : null);
                    reply.writeNoException();
                    return true;
                case 20:
                    data.enforceInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
                    a(com.google.android.gms.internal.js.a.P(data.readStrongBinder()), data.readInt(), data.readString(), data.createStringArray(), data.readString(), data.readInt() != 0 ? (Bundle) Bundle.CREATOR.createFromParcel(data) : null);
                    reply.writeNoException();
                    return true;
                case 21:
                    data.enforceInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
                    b(com.google.android.gms.internal.js.a.P(data.readStrongBinder()), data.readInt(), data.readString());
                    reply.writeNoException();
                    return true;
                case 22:
                    data.enforceInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
                    c(com.google.android.gms.internal.js.a.P(data.readStrongBinder()), data.readInt(), data.readString());
                    reply.writeNoException();
                    return true;
                case 23:
                    data.enforceInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
                    js P14 = com.google.android.gms.internal.js.a.P(data.readStrongBinder());
                    int readInt14 = data.readInt();
                    String readString14 = data.readString();
                    if (data.readInt() != 0) {
                        r0 = (Bundle) Bundle.CREATOR.createFromParcel(data);
                    }
                    n(P14, readInt14, readString14, r0);
                    reply.writeNoException();
                    return true;
                case 24:
                    data.enforceInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
                    d(com.google.android.gms.internal.js.a.P(data.readStrongBinder()), data.readInt(), data.readString());
                    reply.writeNoException();
                    return true;
                case 25:
                    data.enforceInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
                    js P15 = com.google.android.gms.internal.js.a.P(data.readStrongBinder());
                    int readInt15 = data.readInt();
                    String readString15 = data.readString();
                    if (data.readInt() != 0) {
                        r0 = (Bundle) Bundle.CREATOR.createFromParcel(data);
                    }
                    o(P15, readInt15, readString15, r0);
                    reply.writeNoException();
                    return true;
                case 26:
                    data.enforceInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
                    e(com.google.android.gms.internal.js.a.P(data.readStrongBinder()), data.readInt(), data.readString());
                    reply.writeNoException();
                    return true;
                case 27:
                    data.enforceInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
                    js P16 = com.google.android.gms.internal.js.a.P(data.readStrongBinder());
                    int readInt16 = data.readInt();
                    String readString16 = data.readString();
                    if (data.readInt() != 0) {
                        r0 = (Bundle) Bundle.CREATOR.createFromParcel(data);
                    }
                    p(P16, readInt16, readString16, r0);
                    reply.writeNoException();
                    return true;
                case 28:
                    data.enforceInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
                    b(com.google.android.gms.internal.js.a.P(data.readStrongBinder()), data.readInt(), data.readString(), data.readString(), data.createStringArray());
                    reply.writeNoException();
                    return true;
                case 30:
                    data.enforceInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
                    a(com.google.android.gms.internal.js.a.P(data.readStrongBinder()), data.readInt(), data.readString(), data.readString(), data.createStringArray(), data.readInt() != 0 ? (Bundle) Bundle.CREATOR.createFromParcel(data) : null);
                    reply.writeNoException();
                    return true;
                case Notifications.NOTIFICATION_TYPES_ALL /*31*/:
                    data.enforceInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
                    f(com.google.android.gms.internal.js.a.P(data.readStrongBinder()), data.readInt(), data.readString());
                    reply.writeNoException();
                    return true;
                case 32:
                    data.enforceInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
                    g(com.google.android.gms.internal.js.a.P(data.readStrongBinder()), data.readInt(), data.readString());
                    reply.writeNoException();
                    return true;
                case 33:
                    data.enforceInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
                    a(com.google.android.gms.internal.js.a.P(data.readStrongBinder()), data.readInt(), data.readString(), data.readString(), data.readString(), data.createStringArray());
                    reply.writeNoException();
                    return true;
                case 34:
                    data.enforceInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
                    a(com.google.android.gms.internal.js.a.P(data.readStrongBinder()), data.readInt(), data.readString(), data.readString());
                    reply.writeNoException();
                    return true;
                case 35:
                    data.enforceInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
                    h(com.google.android.gms.internal.js.a.P(data.readStrongBinder()), data.readInt(), data.readString());
                    reply.writeNoException();
                    return true;
                case 36:
                    data.enforceInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
                    i(com.google.android.gms.internal.js.a.P(data.readStrongBinder()), data.readInt(), data.readString());
                    reply.writeNoException();
                    return true;
                case 37:
                    data.enforceInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
                    js P17 = com.google.android.gms.internal.js.a.P(data.readStrongBinder());
                    int readInt17 = data.readInt();
                    String readString17 = data.readString();
                    if (data.readInt() != 0) {
                        r0 = (Bundle) Bundle.CREATOR.createFromParcel(data);
                    }
                    q(P17, readInt17, readString17, r0);
                    reply.writeNoException();
                    return true;
                case 38:
                    data.enforceInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
                    js P18 = com.google.android.gms.internal.js.a.P(data.readStrongBinder());
                    int readInt18 = data.readInt();
                    String readString18 = data.readString();
                    if (data.readInt() != 0) {
                        r0 = (Bundle) Bundle.CREATOR.createFromParcel(data);
                    }
                    r(P18, readInt18, readString18, r0);
                    reply.writeNoException();
                    return true;
                case 40:
                    data.enforceInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
                    j(com.google.android.gms.internal.js.a.P(data.readStrongBinder()), data.readInt(), data.readString());
                    reply.writeNoException();
                    return true;
                case 41:
                    data.enforceInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
                    js P19 = com.google.android.gms.internal.js.a.P(data.readStrongBinder());
                    int readInt19 = data.readInt();
                    String readString19 = data.readString();
                    if (data.readInt() != 0) {
                        r0 = (Bundle) Bundle.CREATOR.createFromParcel(data);
                    }
                    s(P19, readInt19, readString19, r0);
                    reply.writeNoException();
                    return true;
                case 42:
                    data.enforceInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
                    k(com.google.android.gms.internal.js.a.P(data.readStrongBinder()), data.readInt(), data.readString());
                    reply.writeNoException();
                    return true;
                case 43:
                    data.enforceInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
                    js P20 = com.google.android.gms.internal.js.a.P(data.readStrongBinder());
                    int readInt20 = data.readInt();
                    String readString20 = data.readString();
                    if (data.readInt() != 0) {
                        r0 = (Bundle) Bundle.CREATOR.createFromParcel(data);
                    }
                    t(P20, readInt20, readString20, r0);
                    reply.writeNoException();
                    return true;
                case 44:
                    data.enforceInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
                    l(com.google.android.gms.internal.js.a.P(data.readStrongBinder()), data.readInt(), data.readString());
                    reply.writeNoException();
                    return true;
                case 46:
                    data.enforceInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
                    js P21 = com.google.android.gms.internal.js.a.P(data.readStrongBinder());
                    if (data.readInt() != 0) {
                        r0 = (jj) jj.CREATOR.createFromParcel(data);
                    }
                    a(P21, (jj) r0);
                    reply.writeNoException();
                    return true;
                case 1598968902:
                    reply.writeString("com.google.android.gms.common.internal.IGmsServiceBroker");
                    return true;
                default:
                    return super.onTransact(code, data, reply, flags);
            }
        }
    }

    void a(js jsVar, int i) throws RemoteException;

    void a(js jsVar, int i, String str) throws RemoteException;

    void a(js jsVar, int i, String str, Bundle bundle) throws RemoteException;

    void a(js jsVar, int i, String str, IBinder iBinder, Bundle bundle) throws RemoteException;

    void a(js jsVar, int i, String str, String str2) throws RemoteException;

    void a(js jsVar, int i, String str, String str2, String str3, String[] strArr) throws RemoteException;

    void a(js jsVar, int i, String str, String str2, String[] strArr) throws RemoteException;

    void a(js jsVar, int i, String str, String str2, String[] strArr, Bundle bundle) throws RemoteException;

    void a(js jsVar, int i, String str, String str2, String[] strArr, String str3, Bundle bundle) throws RemoteException;

    void a(js jsVar, int i, String str, String str2, String[] strArr, String str3, IBinder iBinder, String str4, Bundle bundle) throws RemoteException;

    void a(js jsVar, int i, String str, String[] strArr, String str2, Bundle bundle) throws RemoteException;

    void a(js jsVar, jj jjVar) throws RemoteException;

    void b(js jsVar, int i, String str) throws RemoteException;

    void b(js jsVar, int i, String str, Bundle bundle) throws RemoteException;

    void b(js jsVar, int i, String str, String str2, String[] strArr) throws RemoteException;

    void c(js jsVar, int i, String str) throws RemoteException;

    void c(js jsVar, int i, String str, Bundle bundle) throws RemoteException;

    void d(js jsVar, int i, String str) throws RemoteException;

    void d(js jsVar, int i, String str, Bundle bundle) throws RemoteException;

    void e(js jsVar, int i, String str) throws RemoteException;

    void e(js jsVar, int i, String str, Bundle bundle) throws RemoteException;

    void f(js jsVar, int i, String str) throws RemoteException;

    void f(js jsVar, int i, String str, Bundle bundle) throws RemoteException;

    void g(js jsVar, int i, String str) throws RemoteException;

    void g(js jsVar, int i, String str, Bundle bundle) throws RemoteException;

    void h(js jsVar, int i, String str) throws RemoteException;

    void h(js jsVar, int i, String str, Bundle bundle) throws RemoteException;

    void i(js jsVar, int i, String str) throws RemoteException;

    void i(js jsVar, int i, String str, Bundle bundle) throws RemoteException;

    void j(js jsVar, int i, String str) throws RemoteException;

    void j(js jsVar, int i, String str, Bundle bundle) throws RemoteException;

    void k(js jsVar, int i, String str) throws RemoteException;

    void k(js jsVar, int i, String str, Bundle bundle) throws RemoteException;

    void l(js jsVar, int i, String str) throws RemoteException;

    void l(js jsVar, int i, String str, Bundle bundle) throws RemoteException;

    void m(js jsVar, int i, String str, Bundle bundle) throws RemoteException;

    void n(js jsVar, int i, String str, Bundle bundle) throws RemoteException;

    void o(js jsVar, int i, String str, Bundle bundle) throws RemoteException;

    void p(js jsVar, int i, String str, Bundle bundle) throws RemoteException;

    void q(js jsVar, int i, String str, Bundle bundle) throws RemoteException;

    void r(js jsVar, int i, String str, Bundle bundle) throws RemoteException;

    void s(js jsVar, int i, String str, Bundle bundle) throws RemoteException;

    void t(js jsVar, int i, String str, Bundle bundle) throws RemoteException;
}
