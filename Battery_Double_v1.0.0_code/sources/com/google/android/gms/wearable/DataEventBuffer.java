package com.google.android.gms.wearable;

import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.common.data.g;
import com.google.android.gms.wearable.internal.h;

public class DataEventBuffer extends g<DataEvent> implements Result {
    private final Status Eb;

    public DataEventBuffer(DataHolder dataHolder) {
        super(dataHolder);
        this.Eb = new Status(dataHolder.getStatusCode());
    }

    public Status getStatus() {
        return this.Eb;
    }

    /* access modifiers changed from: protected */
    public String ha() {
        return "path";
    }

    /* access modifiers changed from: protected */
    /* renamed from: q */
    public DataEvent f(int i, int i2) {
        return new h(this.JG, i, i2);
    }
}
