package com.google.android.gms.wearable.internal;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.a.C0004a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.wearable.c;

public class s implements Creator<r> {
    static void a(r rVar, Parcel parcel, int i) {
        int H = b.H(parcel);
        b.c(parcel, 1, rVar.versionCode);
        b.c(parcel, 2, rVar.statusCode);
        b.a(parcel, 3, (Parcelable) rVar.axJ, i, false);
        b.H(parcel, H);
    }

    /* renamed from: es */
    public r createFromParcel(Parcel parcel) {
        int i = 0;
        int G = a.G(parcel);
        c cVar = null;
        int i2 = 0;
        while (parcel.dataPosition() < G) {
            int F = a.F(parcel);
            switch (a.aH(F)) {
                case 1:
                    i2 = a.g(parcel, F);
                    break;
                case 2:
                    i = a.g(parcel, F);
                    break;
                case 3:
                    cVar = (c) a.a(parcel, F, c.CREATOR);
                    break;
                default:
                    a.b(parcel, F);
                    break;
            }
        }
        if (parcel.dataPosition() == G) {
            return new r(i2, i, cVar);
        }
        throw new C0004a("Overread allowed size end=" + G, parcel);
    }

    /* renamed from: gB */
    public r[] newArray(int i) {
        return new r[i];
    }
}
