package com.google.android.gms.wearable.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.a.C0004a;
import com.google.android.gms.common.internal.safeparcel.b;
import java.util.ArrayList;

public class w implements Creator<v> {
    static void a(v vVar, Parcel parcel, int i) {
        int H = b.H(parcel);
        b.c(parcel, 1, vVar.versionCode);
        b.c(parcel, 2, vVar.statusCode);
        b.c(parcel, 3, vVar.axL, false);
        b.H(parcel, H);
    }

    /* renamed from: eu */
    public v createFromParcel(Parcel parcel) {
        int i = 0;
        int G = a.G(parcel);
        ArrayList arrayList = null;
        int i2 = 0;
        while (parcel.dataPosition() < G) {
            int F = a.F(parcel);
            switch (a.aH(F)) {
                case 1:
                    i2 = a.g(parcel, F);
                    break;
                case 2:
                    i = a.g(parcel, F);
                    break;
                case 3:
                    arrayList = a.c(parcel, F, al.CREATOR);
                    break;
                default:
                    a.b(parcel, F);
                    break;
            }
        }
        if (parcel.dataPosition() == G) {
            return new v(i2, i, arrayList);
        }
        throw new C0004a("Overread allowed size end=" + G, parcel);
    }

    /* renamed from: gD */
    public v[] newArray(int i) {
        return new v[i];
    }
}
