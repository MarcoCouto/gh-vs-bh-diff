package com.google.android.gms.wearable.internal;

import com.google.android.gms.wearable.DataItemAsset;

public class i implements DataItemAsset {
    private final String CE;
    private final String KP;

    public i(DataItemAsset dataItemAsset) {
        this.CE = dataItemAsset.getId();
        this.KP = dataItemAsset.getDataItemKey();
    }

    public String getDataItemKey() {
        return this.KP;
    }

    public String getId() {
        return this.CE;
    }

    public boolean isDataValid() {
        return true;
    }

    /* renamed from: ro */
    public DataItemAsset freeze() {
        return this;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("DataItemAssetEntity[");
        sb.append("@");
        sb.append(Integer.toHexString(hashCode()));
        if (this.CE == null) {
            sb.append(",noid");
        } else {
            sb.append(",");
            sb.append(this.CE);
        }
        sb.append(", key=");
        sb.append(this.KP);
        sb.append("]");
        return sb.toString();
    }
}
