package com.google.android.gms.wearable.internal;

import android.net.Uri;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.a.C0004a;
import com.google.android.gms.common.internal.safeparcel.b;

public class n implements Creator<m> {
    static void a(m mVar, Parcel parcel, int i) {
        int H = b.H(parcel);
        b.c(parcel, 1, mVar.CK);
        b.a(parcel, 2, (Parcelable) mVar.getUri(), i, false);
        b.a(parcel, 4, mVar.rk(), false);
        b.a(parcel, 5, mVar.getData(), false);
        b.H(parcel, H);
    }

    /* renamed from: eq */
    public m createFromParcel(Parcel parcel) {
        byte[] r;
        Bundle bundle;
        Uri uri;
        int i;
        byte[] bArr = null;
        int G = a.G(parcel);
        int i2 = 0;
        Bundle bundle2 = null;
        Uri uri2 = null;
        while (parcel.dataPosition() < G) {
            int F = a.F(parcel);
            switch (a.aH(F)) {
                case 1:
                    byte[] bArr2 = bArr;
                    bundle = bundle2;
                    uri = uri2;
                    i = a.g(parcel, F);
                    r = bArr2;
                    break;
                case 2:
                    i = i2;
                    Bundle bundle3 = bundle2;
                    uri = (Uri) a.a(parcel, F, Uri.CREATOR);
                    r = bArr;
                    bundle = bundle3;
                    break;
                case 4:
                    uri = uri2;
                    i = i2;
                    byte[] bArr3 = bArr;
                    bundle = a.q(parcel, F);
                    r = bArr3;
                    break;
                case 5:
                    r = a.r(parcel, F);
                    bundle = bundle2;
                    uri = uri2;
                    i = i2;
                    break;
                default:
                    a.b(parcel, F);
                    r = bArr;
                    bundle = bundle2;
                    uri = uri2;
                    i = i2;
                    break;
            }
            i2 = i;
            uri2 = uri;
            bundle2 = bundle;
            bArr = r;
        }
        if (parcel.dataPosition() == G) {
            return new m(i2, uri2, bundle2, bArr);
        }
        throw new C0004a("Overread allowed size end=" + G, parcel);
    }

    /* renamed from: gz */
    public m[] newArray(int i) {
        return new m[i];
    }
}
