package com.google.android.gms.wearable.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.a.C0004a;
import com.google.android.gms.common.internal.safeparcel.b;

public class am implements Creator<al> {
    static void a(al alVar, Parcel parcel, int i) {
        int H = b.H(parcel);
        b.c(parcel, 1, alVar.CK);
        b.a(parcel, 2, alVar.getId(), false);
        b.a(parcel, 3, alVar.getDisplayName(), false);
        b.H(parcel, H);
    }

    /* renamed from: ez */
    public al createFromParcel(Parcel parcel) {
        String str = null;
        int G = a.G(parcel);
        int i = 0;
        String str2 = null;
        while (parcel.dataPosition() < G) {
            int F = a.F(parcel);
            switch (a.aH(F)) {
                case 1:
                    i = a.g(parcel, F);
                    break;
                case 2:
                    str2 = a.o(parcel, F);
                    break;
                case 3:
                    str = a.o(parcel, F);
                    break;
                default:
                    a.b(parcel, F);
                    break;
            }
        }
        if (parcel.dataPosition() == G) {
            return new al(i, str2, str);
        }
        throw new C0004a("Overread allowed size end=" + G, parcel);
    }

    /* renamed from: gI */
    public al[] newArray(int i) {
        return new al[i];
    }
}
