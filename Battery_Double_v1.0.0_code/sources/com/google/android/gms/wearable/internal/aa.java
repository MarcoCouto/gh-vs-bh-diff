package com.google.android.gms.wearable.internal;

import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.a.C0004a;
import com.google.android.gms.common.internal.safeparcel.b;

public class aa implements Creator<z> {
    static void a(z zVar, Parcel parcel, int i) {
        int H = b.H(parcel);
        b.c(parcel, 1, zVar.versionCode);
        b.c(parcel, 2, zVar.statusCode);
        b.a(parcel, 3, (Parcelable) zVar.axN, i, false);
        b.H(parcel, H);
    }

    /* renamed from: ew */
    public z createFromParcel(Parcel parcel) {
        int i = 0;
        int G = a.G(parcel);
        ParcelFileDescriptor parcelFileDescriptor = null;
        int i2 = 0;
        while (parcel.dataPosition() < G) {
            int F = a.F(parcel);
            switch (a.aH(F)) {
                case 1:
                    i2 = a.g(parcel, F);
                    break;
                case 2:
                    i = a.g(parcel, F);
                    break;
                case 3:
                    parcelFileDescriptor = (ParcelFileDescriptor) a.a(parcel, F, ParcelFileDescriptor.CREATOR);
                    break;
                default:
                    a.b(parcel, F);
                    break;
            }
        }
        if (parcel.dataPosition() == G) {
            return new z(i2, i, parcelFileDescriptor);
        }
        throw new C0004a("Overread allowed size end=" + G, parcel);
    }

    /* renamed from: gF */
    public z[] newArray(int i) {
        return new z[i];
    }
}
