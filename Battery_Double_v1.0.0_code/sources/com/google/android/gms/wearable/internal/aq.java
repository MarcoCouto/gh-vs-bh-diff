package com.google.android.gms.wearable.internal;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.a.C0004a;
import com.google.android.gms.common.internal.safeparcel.b;

public class aq implements Creator<ap> {
    static void a(ap apVar, Parcel parcel, int i) {
        int H = b.H(parcel);
        b.c(parcel, 1, apVar.versionCode);
        b.c(parcel, 2, apVar.statusCode);
        b.a(parcel, 3, (Parcelable) apVar.axM, i, false);
        b.H(parcel, H);
    }

    /* renamed from: eB */
    public ap createFromParcel(Parcel parcel) {
        int i = 0;
        int G = a.G(parcel);
        m mVar = null;
        int i2 = 0;
        while (parcel.dataPosition() < G) {
            int F = a.F(parcel);
            switch (a.aH(F)) {
                case 1:
                    i2 = a.g(parcel, F);
                    break;
                case 2:
                    i = a.g(parcel, F);
                    break;
                case 3:
                    mVar = (m) a.a(parcel, F, m.CREATOR);
                    break;
                default:
                    a.b(parcel, F);
                    break;
            }
        }
        if (parcel.dataPosition() == G) {
            return new ap(i2, i, mVar);
        }
        throw new C0004a("Overread allowed size end=" + G, parcel);
    }

    /* renamed from: gK */
    public ap[] newArray(int i) {
        return new ap[i];
    }
}
