package com.google.android.gms.wearable.internal;

import android.content.Context;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Looper;
import android.os.ParcelFileDescriptor;
import android.os.ParcelFileDescriptor.AutoCloseOutputStream;
import android.os.RemoteException;
import android.util.Log;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.BaseImplementation.b;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.internal.jl;
import com.google.android.gms.internal.jl.e;
import com.google.android.gms.internal.jt;
import com.google.android.gms.wearable.Asset;
import com.google.android.gms.wearable.DataApi.DataItemResult;
import com.google.android.gms.wearable.DataApi.DataListener;
import com.google.android.gms.wearable.DataApi.DeleteDataItemsResult;
import com.google.android.gms.wearable.DataApi.GetFdForAssetResult;
import com.google.android.gms.wearable.DataItemAsset;
import com.google.android.gms.wearable.DataItemBuffer;
import com.google.android.gms.wearable.MessageApi.MessageListener;
import com.google.android.gms.wearable.MessageApi.SendMessageResult;
import com.google.android.gms.wearable.NodeApi.GetConnectedNodesResult;
import com.google.android.gms.wearable.NodeApi.GetLocalNodeResult;
import com.google.android.gms.wearable.NodeApi.NodeListener;
import com.google.android.gms.wearable.PutDataRequest;
import com.google.android.gms.wearable.internal.af.a;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map.Entry;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;

public class ba extends jl<af> {
    private final ExecutorService asJ = Executors.newCachedThreadPool();
    private final ag<DataListener> ayg = new b();
    private final ag<MessageListener> ayh = new c();
    private final ag<NodeListener> ayi = new d();

    public ba(Context context, Looper looper, ConnectionCallbacks connectionCallbacks, OnConnectionFailedListener onConnectionFailedListener) {
        super(context, looper, connectionCallbacks, onConnectionFailedListener, new String[0]);
    }

    private FutureTask<Boolean> a(final ParcelFileDescriptor parcelFileDescriptor, final byte[] bArr) {
        return new FutureTask<>(new Callable<Boolean>() {
            /* renamed from: rr */
            public Boolean call() {
                if (Log.isLoggable("WearableClient", 3)) {
                    Log.d("WearableClient", "processAssets: writing data to FD : " + parcelFileDescriptor);
                }
                AutoCloseOutputStream autoCloseOutputStream = new AutoCloseOutputStream(parcelFileDescriptor);
                try {
                    autoCloseOutputStream.write(bArr);
                    autoCloseOutputStream.flush();
                    if (Log.isLoggable("WearableClient", 3)) {
                        Log.d("WearableClient", "processAssets: wrote data: " + parcelFileDescriptor);
                    }
                    Boolean valueOf = Boolean.valueOf(true);
                    try {
                        if (Log.isLoggable("WearableClient", 3)) {
                            Log.d("WearableClient", "processAssets: closing: " + parcelFileDescriptor);
                        }
                        autoCloseOutputStream.close();
                        return valueOf;
                    } catch (IOException e) {
                        return valueOf;
                    }
                } catch (IOException e2) {
                    Log.w("WearableClient", "processAssets: writing data failed: " + parcelFileDescriptor);
                    try {
                        if (Log.isLoggable("WearableClient", 3)) {
                            Log.d("WearableClient", "processAssets: closing: " + parcelFileDescriptor);
                        }
                        autoCloseOutputStream.close();
                    } catch (IOException e3) {
                    }
                    return Boolean.valueOf(false);
                } finally {
                    try {
                        if (Log.isLoggable("WearableClient", 3)) {
                            Log.d("WearableClient", "processAssets: closing: " + parcelFileDescriptor);
                        }
                        autoCloseOutputStream.close();
                    } catch (IOException e4) {
                    }
                }
            }
        });
    }

    /* access modifiers changed from: protected */
    public void a(int i, IBinder iBinder, Bundle bundle) {
        if (Log.isLoggable("WearableClient", 2)) {
            Log.d("WearableClient", "onPostInitHandler: statusCode " + i);
        }
        if (i == 0) {
            this.ayg.ca(iBinder);
            this.ayh.ca(iBinder);
            this.ayi.ca(iBinder);
        }
        super.a(i, iBinder, bundle);
    }

    public void a(b<DataItemResult> bVar, Uri uri) throws RemoteException {
        ((af) hw()).a((ad) new d(bVar), uri);
    }

    public void a(b<GetFdForAssetResult> bVar, Asset asset) throws RemoteException {
        ((af) hw()).a((ad) new f(bVar), asset);
    }

    public void a(b<Status> bVar, DataListener dataListener) throws RemoteException {
        this.ayg.a(this, bVar, dataListener);
    }

    public void a(b<Status> bVar, DataListener dataListener, IntentFilter[] intentFilterArr) throws RemoteException {
        this.ayg.a(this, bVar, dataListener, intentFilterArr);
    }

    public void a(b<GetFdForAssetResult> bVar, DataItemAsset dataItemAsset) throws RemoteException {
        a(bVar, Asset.createFromRef(dataItemAsset.getId()));
    }

    public void a(b<Status> bVar, MessageListener messageListener) throws RemoteException {
        this.ayh.a(this, bVar, messageListener);
    }

    public void a(b<Status> bVar, MessageListener messageListener, IntentFilter[] intentFilterArr) throws RemoteException {
        this.ayh.a(this, bVar, messageListener, intentFilterArr);
    }

    public void a(b<Status> bVar, NodeListener nodeListener) throws RemoteException, RemoteException {
        this.ayi.a(this, bVar, nodeListener, null);
    }

    public void a(b<DataItemResult> bVar, PutDataRequest putDataRequest) throws RemoteException {
        for (Entry value : putDataRequest.getAssets().entrySet()) {
            Asset asset = (Asset) value.getValue();
            if (asset.getData() == null && asset.getDigest() == null && asset.getFd() == null && asset.getUri() == null) {
                throw new IllegalArgumentException("Put for " + putDataRequest.getUri() + " contains invalid asset: " + asset);
            }
        }
        PutDataRequest k = PutDataRequest.k(putDataRequest.getUri());
        k.setData(putDataRequest.getData());
        ArrayList arrayList = new ArrayList();
        for (Entry entry : putDataRequest.getAssets().entrySet()) {
            Asset asset2 = (Asset) entry.getValue();
            if (asset2.getData() == null) {
                k.putAsset((String) entry.getKey(), (Asset) entry.getValue());
            } else {
                try {
                    ParcelFileDescriptor[] createPipe = ParcelFileDescriptor.createPipe();
                    if (Log.isLoggable("WearableClient", 3)) {
                        Log.d("WearableClient", "processAssets: replacing data with FD in asset: " + asset2 + " read:" + createPipe[0] + " write:" + createPipe[1]);
                    }
                    k.putAsset((String) entry.getKey(), Asset.createFromFd(createPipe[0]));
                    FutureTask a = a(createPipe[1], asset2.getData());
                    arrayList.add(a);
                    this.asJ.submit(a);
                } catch (IOException e) {
                    throw new IllegalStateException("Unable to create ParcelFileDescriptor for asset in request: " + putDataRequest, e);
                }
            }
        }
        try {
            ((af) hw()).a((ad) new i(bVar, arrayList), k);
        } catch (NullPointerException e2) {
            throw new IllegalStateException("Unable to putDataItem: " + putDataRequest, e2);
        }
    }

    public void a(b<SendMessageResult> bVar, String str, String str2, byte[] bArr) throws RemoteException {
        ((af) hw()).a(new j(bVar), str, str2, bArr);
    }

    /* access modifiers changed from: protected */
    public void a(jt jtVar, e eVar) throws RemoteException {
        jtVar.e(eVar, GooglePlayServicesUtil.GOOGLE_PLAY_SERVICES_VERSION_CODE, getContext().getPackageName());
    }

    public void b(b<DataItemBuffer> bVar, Uri uri) throws RemoteException {
        ((af) hw()).b((ad) new e(bVar), uri);
    }

    public void b(b<Status> bVar, NodeListener nodeListener) throws RemoteException {
        this.ayi.a(this, bVar, nodeListener);
    }

    /* access modifiers changed from: protected */
    public String bK() {
        return "com.google.android.gms.wearable.BIND";
    }

    /* access modifiers changed from: protected */
    public String bL() {
        return "com.google.android.gms.wearable.internal.IWearableService";
    }

    public void c(b<DeleteDataItemsResult> bVar, Uri uri) throws RemoteException {
        ((af) hw()).c((ad) new b(bVar), uri);
    }

    /* access modifiers changed from: protected */
    /* renamed from: cb */
    public af l(IBinder iBinder) {
        return a.bZ(iBinder);
    }

    public void disconnect() {
        this.ayg.b(this);
        this.ayh.b(this);
        this.ayi.b(this);
        super.disconnect();
    }

    public void n(b<DataItemBuffer> bVar) throws RemoteException {
        ((af) hw()).b(new e(bVar));
    }

    public void o(b<GetLocalNodeResult> bVar) throws RemoteException {
        ((af) hw()).c(new g(bVar));
    }

    public void p(b<GetConnectedNodesResult> bVar) throws RemoteException {
        ((af) hw()).d(new c(bVar));
    }
}
