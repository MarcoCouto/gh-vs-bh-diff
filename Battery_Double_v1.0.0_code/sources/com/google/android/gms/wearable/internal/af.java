package com.google.android.gms.wearable.internal;

import android.net.Uri;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.dlten.lib.frmWork.CEventWnd;
import com.google.android.gms.wearable.Asset;
import com.google.android.gms.wearable.PutDataRequest;
import com.google.android.gms.wearable.c;

public interface af extends IInterface {

    public static abstract class a extends Binder implements af {

        /* renamed from: com.google.android.gms.wearable.internal.af$a$a reason: collision with other inner class name */
        private static class C0142a implements af {
            private IBinder le;

            C0142a(IBinder iBinder) {
                this.le = iBinder;
            }

            public void a(ad adVar) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.wearable.internal.IWearableService");
                    obtain.writeStrongBinder(adVar != null ? adVar.asBinder() : null);
                    this.le.transact(22, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(ad adVar, Uri uri) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.wearable.internal.IWearableService");
                    obtain.writeStrongBinder(adVar != null ? adVar.asBinder() : null);
                    if (uri != null) {
                        obtain.writeInt(1);
                        uri.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.le.transact(7, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(ad adVar, Asset asset) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.wearable.internal.IWearableService");
                    obtain.writeStrongBinder(adVar != null ? adVar.asBinder() : null);
                    if (asset != null) {
                        obtain.writeInt(1);
                        asset.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.le.transact(13, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(ad adVar, PutDataRequest putDataRequest) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.wearable.internal.IWearableService");
                    obtain.writeStrongBinder(adVar != null ? adVar.asBinder() : null);
                    if (putDataRequest != null) {
                        obtain.writeInt(1);
                        putDataRequest.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.le.transact(6, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(ad adVar, c cVar) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.wearable.internal.IWearableService");
                    obtain.writeStrongBinder(adVar != null ? adVar.asBinder() : null);
                    if (cVar != null) {
                        obtain.writeInt(1);
                        cVar.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.le.transact(20, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(ad adVar, ar arVar) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.wearable.internal.IWearableService");
                    obtain.writeStrongBinder(adVar != null ? adVar.asBinder() : null);
                    if (arVar != null) {
                        obtain.writeInt(1);
                        arVar.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.le.transact(17, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(ad adVar, b bVar) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.wearable.internal.IWearableService");
                    obtain.writeStrongBinder(adVar != null ? adVar.asBinder() : null);
                    if (bVar != null) {
                        obtain.writeInt(1);
                        bVar.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.le.transact(16, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(ad adVar, String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.wearable.internal.IWearableService");
                    obtain.writeStrongBinder(adVar != null ? adVar.asBinder() : null);
                    obtain.writeString(str);
                    this.le.transact(21, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(ad adVar, String str, String str2, byte[] bArr) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.wearable.internal.IWearableService");
                    obtain.writeStrongBinder(adVar != null ? adVar.asBinder() : null);
                    obtain.writeString(str);
                    obtain.writeString(str2);
                    obtain.writeByteArray(bArr);
                    this.le.transact(12, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public IBinder asBinder() {
                return this.le;
            }

            public void b(ad adVar) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.wearable.internal.IWearableService");
                    obtain.writeStrongBinder(adVar != null ? adVar.asBinder() : null);
                    this.le.transact(8, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void b(ad adVar, Uri uri) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.wearable.internal.IWearableService");
                    obtain.writeStrongBinder(adVar != null ? adVar.asBinder() : null);
                    if (uri != null) {
                        obtain.writeInt(1);
                        uri.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.le.transact(9, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void b(ad adVar, c cVar) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.wearable.internal.IWearableService");
                    obtain.writeStrongBinder(adVar != null ? adVar.asBinder() : null);
                    if (cVar != null) {
                        obtain.writeInt(1);
                        cVar.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.le.transact(2, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void b(ad adVar, String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.wearable.internal.IWearableService");
                    obtain.writeStrongBinder(adVar != null ? adVar.asBinder() : null);
                    obtain.writeString(str);
                    this.le.transact(23, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void c(ad adVar) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.wearable.internal.IWearableService");
                    obtain.writeStrongBinder(adVar != null ? adVar.asBinder() : null);
                    this.le.transact(14, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void c(ad adVar, Uri uri) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.wearable.internal.IWearableService");
                    obtain.writeStrongBinder(adVar != null ? adVar.asBinder() : null);
                    if (uri != null) {
                        obtain.writeInt(1);
                        uri.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.le.transact(11, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void c(ad adVar, String str) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.wearable.internal.IWearableService");
                    obtain.writeStrongBinder(adVar != null ? adVar.asBinder() : null);
                    obtain.writeString(str);
                    this.le.transact(24, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void d(ad adVar) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.wearable.internal.IWearableService");
                    obtain.writeStrongBinder(adVar != null ? adVar.asBinder() : null);
                    this.le.transact(15, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void e(ad adVar) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.wearable.internal.IWearableService");
                    obtain.writeStrongBinder(adVar != null ? adVar.asBinder() : null);
                    this.le.transact(18, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void f(ad adVar) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.wearable.internal.IWearableService");
                    obtain.writeStrongBinder(adVar != null ? adVar.asBinder() : null);
                    this.le.transact(19, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void g(ad adVar) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.wearable.internal.IWearableService");
                    obtain.writeStrongBinder(adVar != null ? adVar.asBinder() : null);
                    this.le.transact(25, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void h(ad adVar) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.wearable.internal.IWearableService");
                    obtain.writeStrongBinder(adVar != null ? adVar.asBinder() : null);
                    this.le.transact(26, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void i(ad adVar) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.wearable.internal.IWearableService");
                    obtain.writeStrongBinder(adVar != null ? adVar.asBinder() : null);
                    this.le.transact(3, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void j(ad adVar) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.wearable.internal.IWearableService");
                    obtain.writeStrongBinder(adVar != null ? adVar.asBinder() : null);
                    this.le.transact(4, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void k(ad adVar) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.gms.wearable.internal.IWearableService");
                    obtain.writeStrongBinder(adVar != null ? adVar.asBinder() : null);
                    this.le.transact(5, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
        }

        public static af bZ(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.wearable.internal.IWearableService");
            return (queryLocalInterface == null || !(queryLocalInterface instanceof af)) ? new C0142a(iBinder) : (af) queryLocalInterface;
        }

        /* JADX WARNING: type inference failed for: r0v0 */
        /* JADX WARNING: type inference failed for: r0v13, types: [com.google.android.gms.wearable.c] */
        /* JADX WARNING: type inference failed for: r0v17, types: [com.google.android.gms.wearable.c] */
        /* JADX WARNING: type inference failed for: r0v34, types: [com.google.android.gms.wearable.internal.ar] */
        /* JADX WARNING: type inference failed for: r0v38, types: [com.google.android.gms.wearable.internal.ar] */
        /* JADX WARNING: type inference failed for: r0v39, types: [com.google.android.gms.wearable.internal.b] */
        /* JADX WARNING: type inference failed for: r0v43, types: [com.google.android.gms.wearable.internal.b] */
        /* JADX WARNING: type inference failed for: r0v52, types: [com.google.android.gms.wearable.Asset] */
        /* JADX WARNING: type inference failed for: r0v56, types: [com.google.android.gms.wearable.Asset] */
        /* JADX WARNING: type inference failed for: r0v61, types: [android.net.Uri] */
        /* JADX WARNING: type inference failed for: r0v65, types: [android.net.Uri] */
        /* JADX WARNING: type inference failed for: r0v66, types: [android.net.Uri] */
        /* JADX WARNING: type inference failed for: r0v70, types: [android.net.Uri] */
        /* JADX WARNING: type inference failed for: r0v75, types: [android.net.Uri] */
        /* JADX WARNING: type inference failed for: r0v79, types: [android.net.Uri] */
        /* JADX WARNING: type inference failed for: r0v80, types: [com.google.android.gms.wearable.PutDataRequest] */
        /* JADX WARNING: type inference failed for: r0v84, types: [com.google.android.gms.wearable.PutDataRequest] */
        /* JADX WARNING: type inference failed for: r0v101, types: [com.google.android.gms.wearable.c] */
        /* JADX WARNING: type inference failed for: r0v105, types: [com.google.android.gms.wearable.c] */
        /* JADX WARNING: type inference failed for: r0v109 */
        /* JADX WARNING: type inference failed for: r0v110 */
        /* JADX WARNING: type inference failed for: r0v111 */
        /* JADX WARNING: type inference failed for: r0v112 */
        /* JADX WARNING: type inference failed for: r0v113 */
        /* JADX WARNING: type inference failed for: r0v114 */
        /* JADX WARNING: type inference failed for: r0v115 */
        /* JADX WARNING: type inference failed for: r0v116 */
        /* JADX WARNING: type inference failed for: r0v117 */
        /* JADX WARNING: Multi-variable type inference failed. Error: jadx.core.utils.exceptions.JadxRuntimeException: No candidate types for var: r0v0
  assigns: [?[int, float, boolean, short, byte, char, OBJECT, ARRAY], com.google.android.gms.wearable.internal.ar, com.google.android.gms.wearable.c, com.google.android.gms.wearable.internal.b, com.google.android.gms.wearable.Asset, android.net.Uri, com.google.android.gms.wearable.PutDataRequest]
  uses: [com.google.android.gms.wearable.c, com.google.android.gms.wearable.internal.ar, com.google.android.gms.wearable.internal.b, com.google.android.gms.wearable.Asset, android.net.Uri, com.google.android.gms.wearable.PutDataRequest]
  mth insns count: 235
        	at jadx.core.dex.visitors.typeinference.TypeSearch.fillTypeCandidates(TypeSearch.java:237)
        	at java.util.ArrayList.forEach(Unknown Source)
        	at jadx.core.dex.visitors.typeinference.TypeSearch.run(TypeSearch.java:53)
        	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.runMultiVariableSearch(TypeInferenceVisitor.java:99)
        	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.visit(TypeInferenceVisitor.java:92)
        	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:27)
        	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$1(DepthTraversal.java:14)
        	at java.util.ArrayList.forEach(Unknown Source)
        	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
        	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$0(DepthTraversal.java:13)
        	at java.util.ArrayList.forEach(Unknown Source)
        	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:13)
        	at jadx.core.ProcessClass.process(ProcessClass.java:30)
        	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
        	at jadx.api.JavaClass.decompile(JavaClass.java:62)
        	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
         */
        /* JADX WARNING: Unknown variable types count: 10 */
        public boolean onTransact(int code, Parcel data, Parcel reply, int flags) throws RemoteException {
            ? r0 = 0;
            switch (code) {
                case 2:
                    data.enforceInterface("com.google.android.gms.wearable.internal.IWearableService");
                    ad bX = com.google.android.gms.wearable.internal.ad.a.bX(data.readStrongBinder());
                    if (data.readInt() != 0) {
                        r0 = (c) c.CREATOR.createFromParcel(data);
                    }
                    b(bX, (c) r0);
                    reply.writeNoException();
                    return true;
                case 3:
                    data.enforceInterface("com.google.android.gms.wearable.internal.IWearableService");
                    i(com.google.android.gms.wearable.internal.ad.a.bX(data.readStrongBinder()));
                    reply.writeNoException();
                    return true;
                case 4:
                    data.enforceInterface("com.google.android.gms.wearable.internal.IWearableService");
                    j(com.google.android.gms.wearable.internal.ad.a.bX(data.readStrongBinder()));
                    reply.writeNoException();
                    return true;
                case 5:
                    data.enforceInterface("com.google.android.gms.wearable.internal.IWearableService");
                    k(com.google.android.gms.wearable.internal.ad.a.bX(data.readStrongBinder()));
                    reply.writeNoException();
                    return true;
                case 6:
                    data.enforceInterface("com.google.android.gms.wearable.internal.IWearableService");
                    ad bX2 = com.google.android.gms.wearable.internal.ad.a.bX(data.readStrongBinder());
                    if (data.readInt() != 0) {
                        r0 = (PutDataRequest) PutDataRequest.CREATOR.createFromParcel(data);
                    }
                    a(bX2, (PutDataRequest) r0);
                    reply.writeNoException();
                    return true;
                case 7:
                    data.enforceInterface("com.google.android.gms.wearable.internal.IWearableService");
                    ad bX3 = com.google.android.gms.wearable.internal.ad.a.bX(data.readStrongBinder());
                    if (data.readInt() != 0) {
                        r0 = (Uri) Uri.CREATOR.createFromParcel(data);
                    }
                    a(bX3, (Uri) r0);
                    reply.writeNoException();
                    return true;
                case 8:
                    data.enforceInterface("com.google.android.gms.wearable.internal.IWearableService");
                    b(com.google.android.gms.wearable.internal.ad.a.bX(data.readStrongBinder()));
                    reply.writeNoException();
                    return true;
                case 9:
                    data.enforceInterface("com.google.android.gms.wearable.internal.IWearableService");
                    ad bX4 = com.google.android.gms.wearable.internal.ad.a.bX(data.readStrongBinder());
                    if (data.readInt() != 0) {
                        r0 = (Uri) Uri.CREATOR.createFromParcel(data);
                    }
                    b(bX4, (Uri) r0);
                    reply.writeNoException();
                    return true;
                case 11:
                    data.enforceInterface("com.google.android.gms.wearable.internal.IWearableService");
                    ad bX5 = com.google.android.gms.wearable.internal.ad.a.bX(data.readStrongBinder());
                    if (data.readInt() != 0) {
                        r0 = (Uri) Uri.CREATOR.createFromParcel(data);
                    }
                    c(bX5, (Uri) r0);
                    reply.writeNoException();
                    return true;
                case 12:
                    data.enforceInterface("com.google.android.gms.wearable.internal.IWearableService");
                    a(com.google.android.gms.wearable.internal.ad.a.bX(data.readStrongBinder()), data.readString(), data.readString(), data.createByteArray());
                    reply.writeNoException();
                    return true;
                case 13:
                    data.enforceInterface("com.google.android.gms.wearable.internal.IWearableService");
                    ad bX6 = com.google.android.gms.wearable.internal.ad.a.bX(data.readStrongBinder());
                    if (data.readInt() != 0) {
                        r0 = (Asset) Asset.CREATOR.createFromParcel(data);
                    }
                    a(bX6, (Asset) r0);
                    reply.writeNoException();
                    return true;
                case 14:
                    data.enforceInterface("com.google.android.gms.wearable.internal.IWearableService");
                    c(com.google.android.gms.wearable.internal.ad.a.bX(data.readStrongBinder()));
                    reply.writeNoException();
                    return true;
                case 15:
                    data.enforceInterface("com.google.android.gms.wearable.internal.IWearableService");
                    d(com.google.android.gms.wearable.internal.ad.a.bX(data.readStrongBinder()));
                    reply.writeNoException();
                    return true;
                case 16:
                    data.enforceInterface("com.google.android.gms.wearable.internal.IWearableService");
                    ad bX7 = com.google.android.gms.wearable.internal.ad.a.bX(data.readStrongBinder());
                    if (data.readInt() != 0) {
                        r0 = (b) b.CREATOR.createFromParcel(data);
                    }
                    a(bX7, (b) r0);
                    reply.writeNoException();
                    return true;
                case CEventWnd.WM_NET /*17*/:
                    data.enforceInterface("com.google.android.gms.wearable.internal.IWearableService");
                    ad bX8 = com.google.android.gms.wearable.internal.ad.a.bX(data.readStrongBinder());
                    if (data.readInt() != 0) {
                        r0 = (ar) ar.CREATOR.createFromParcel(data);
                    }
                    a(bX8, (ar) r0);
                    reply.writeNoException();
                    return true;
                case CEventWnd.WM_APP_EXIT /*18*/:
                    data.enforceInterface("com.google.android.gms.wearable.internal.IWearableService");
                    e(com.google.android.gms.wearable.internal.ad.a.bX(data.readStrongBinder()));
                    reply.writeNoException();
                    return true;
                case 19:
                    data.enforceInterface("com.google.android.gms.wearable.internal.IWearableService");
                    f(com.google.android.gms.wearable.internal.ad.a.bX(data.readStrongBinder()));
                    reply.writeNoException();
                    return true;
                case 20:
                    data.enforceInterface("com.google.android.gms.wearable.internal.IWearableService");
                    ad bX9 = com.google.android.gms.wearable.internal.ad.a.bX(data.readStrongBinder());
                    if (data.readInt() != 0) {
                        r0 = (c) c.CREATOR.createFromParcel(data);
                    }
                    a(bX9, (c) r0);
                    reply.writeNoException();
                    return true;
                case 21:
                    data.enforceInterface("com.google.android.gms.wearable.internal.IWearableService");
                    a(com.google.android.gms.wearable.internal.ad.a.bX(data.readStrongBinder()), data.readString());
                    reply.writeNoException();
                    return true;
                case 22:
                    data.enforceInterface("com.google.android.gms.wearable.internal.IWearableService");
                    a(com.google.android.gms.wearable.internal.ad.a.bX(data.readStrongBinder()));
                    reply.writeNoException();
                    return true;
                case 23:
                    data.enforceInterface("com.google.android.gms.wearable.internal.IWearableService");
                    b(com.google.android.gms.wearable.internal.ad.a.bX(data.readStrongBinder()), data.readString());
                    reply.writeNoException();
                    return true;
                case 24:
                    data.enforceInterface("com.google.android.gms.wearable.internal.IWearableService");
                    c(com.google.android.gms.wearable.internal.ad.a.bX(data.readStrongBinder()), data.readString());
                    reply.writeNoException();
                    return true;
                case 25:
                    data.enforceInterface("com.google.android.gms.wearable.internal.IWearableService");
                    g(com.google.android.gms.wearable.internal.ad.a.bX(data.readStrongBinder()));
                    reply.writeNoException();
                    return true;
                case 26:
                    data.enforceInterface("com.google.android.gms.wearable.internal.IWearableService");
                    h(com.google.android.gms.wearable.internal.ad.a.bX(data.readStrongBinder()));
                    reply.writeNoException();
                    return true;
                case 1598968902:
                    reply.writeString("com.google.android.gms.wearable.internal.IWearableService");
                    return true;
                default:
                    return super.onTransact(code, data, reply, flags);
            }
        }
    }

    void a(ad adVar) throws RemoteException;

    void a(ad adVar, Uri uri) throws RemoteException;

    void a(ad adVar, Asset asset) throws RemoteException;

    void a(ad adVar, PutDataRequest putDataRequest) throws RemoteException;

    void a(ad adVar, c cVar) throws RemoteException;

    void a(ad adVar, ar arVar) throws RemoteException;

    void a(ad adVar, b bVar) throws RemoteException;

    void a(ad adVar, String str) throws RemoteException;

    void a(ad adVar, String str, String str2, byte[] bArr) throws RemoteException;

    void b(ad adVar) throws RemoteException;

    void b(ad adVar, Uri uri) throws RemoteException;

    void b(ad adVar, c cVar) throws RemoteException;

    void b(ad adVar, String str) throws RemoteException;

    void c(ad adVar) throws RemoteException;

    void c(ad adVar, Uri uri) throws RemoteException;

    void c(ad adVar, String str) throws RemoteException;

    void d(ad adVar) throws RemoteException;

    void e(ad adVar) throws RemoteException;

    void f(ad adVar) throws RemoteException;

    void g(ad adVar) throws RemoteException;

    void h(ad adVar) throws RemoteException;

    void i(ad adVar) throws RemoteException;

    void j(ad adVar) throws RemoteException;

    void k(ad adVar) throws RemoteException;
}
