package com.google.android.gms.wearable;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.util.Log;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.data.DataHolder;
import com.google.android.gms.wearable.DataApi.DataListener;
import com.google.android.gms.wearable.MessageApi.MessageListener;
import com.google.android.gms.wearable.NodeApi.NodeListener;
import com.google.android.gms.wearable.internal.ai;
import com.google.android.gms.wearable.internal.al;

public abstract class WearableListenerService extends Service implements DataListener, MessageListener, NodeListener {
    public static final String BIND_LISTENER_INTENT_ACTION = "com.google.android.gms.wearable.BIND_LISTENER";
    /* access modifiers changed from: private */
    public String CS;
    private IBinder Nk;
    private volatile int Pj = -1;
    /* access modifiers changed from: private */
    public Handler axn;
    /* access modifiers changed from: private */
    public Object axo = new Object();
    /* access modifiers changed from: private */
    public boolean axp;

    private class a extends com.google.android.gms.wearable.internal.ae.a {
        private a() {
        }

        public void a(final ai aiVar) {
            if (Log.isLoggable("WearableLS", 3)) {
                Log.d("WearableLS", "onMessageReceived: " + aiVar);
            }
            WearableListenerService.this.rl();
            synchronized (WearableListenerService.this.axo) {
                if (!WearableListenerService.this.axp) {
                    WearableListenerService.this.axn.post(new Runnable() {
                        public void run() {
                            WearableListenerService.this.onMessageReceived(aiVar);
                        }
                    });
                }
            }
        }

        public void a(final al alVar) {
            if (Log.isLoggable("WearableLS", 3)) {
                Log.d("WearableLS", "onPeerConnected: " + WearableListenerService.this.CS + ": " + alVar);
            }
            WearableListenerService.this.rl();
            synchronized (WearableListenerService.this.axo) {
                if (!WearableListenerService.this.axp) {
                    WearableListenerService.this.axn.post(new Runnable() {
                        public void run() {
                            WearableListenerService.this.onPeerConnected(alVar);
                        }
                    });
                }
            }
        }

        public void aa(final DataHolder dataHolder) {
            if (Log.isLoggable("WearableLS", 3)) {
                Log.d("WearableLS", "onDataItemChanged: " + WearableListenerService.this.CS + ": " + dataHolder);
            }
            WearableListenerService.this.rl();
            synchronized (WearableListenerService.this.axo) {
                if (WearableListenerService.this.axp) {
                    dataHolder.close();
                } else {
                    WearableListenerService.this.axn.post(new Runnable() {
                        public void run() {
                            DataEventBuffer dataEventBuffer = new DataEventBuffer(dataHolder);
                            try {
                                WearableListenerService.this.onDataChanged(dataEventBuffer);
                            } finally {
                                dataEventBuffer.release();
                            }
                        }
                    });
                }
            }
        }

        public void b(final al alVar) {
            if (Log.isLoggable("WearableLS", 3)) {
                Log.d("WearableLS", "onPeerDisconnected: " + WearableListenerService.this.CS + ": " + alVar);
            }
            WearableListenerService.this.rl();
            synchronized (WearableListenerService.this.axo) {
                if (!WearableListenerService.this.axp) {
                    WearableListenerService.this.axn.post(new Runnable() {
                        public void run() {
                            WearableListenerService.this.onPeerDisconnected(alVar);
                        }
                    });
                }
            }
        }
    }

    private boolean bh(int i) {
        String str = GooglePlayServicesUtil.GOOGLE_PLAY_SERVICES_PACKAGE;
        String[] packagesForUid = getPackageManager().getPackagesForUid(i);
        if (packagesForUid == null) {
            return false;
        }
        for (String equals : packagesForUid) {
            if (str.equals(equals)) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: private */
    public void rl() throws SecurityException {
        int callingUid = Binder.getCallingUid();
        if (callingUid != this.Pj) {
            if (!GooglePlayServicesUtil.b(getPackageManager(), GooglePlayServicesUtil.GOOGLE_PLAY_SERVICES_PACKAGE) || !bh(callingUid)) {
                throw new SecurityException("Caller is not GooglePlayServices");
            }
            this.Pj = callingUid;
        }
    }

    public final IBinder onBind(Intent intent) {
        if (BIND_LISTENER_INTENT_ACTION.equals(intent.getAction())) {
            return this.Nk;
        }
        return null;
    }

    public void onCreate() {
        super.onCreate();
        if (Log.isLoggable("WearableLS", 3)) {
            Log.d("WearableLS", "onCreate: " + getPackageName());
        }
        this.CS = getPackageName();
        HandlerThread handlerThread = new HandlerThread("WearableListenerService");
        handlerThread.start();
        this.axn = new Handler(handlerThread.getLooper());
        this.Nk = new a();
    }

    public void onDataChanged(DataEventBuffer dataEvents) {
    }

    public void onDestroy() {
        synchronized (this.axo) {
            this.axp = true;
            this.axn.getLooper().quit();
        }
        super.onDestroy();
    }

    public void onMessageReceived(MessageEvent messageEvent) {
    }

    public void onPeerConnected(Node peer) {
    }

    public void onPeerDisconnected(Node peer) {
    }
}
