package com.google.android.gms.fitness.data;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.a.C0004a;
import com.google.android.gms.common.internal.safeparcel.b;

public class r implements Creator<q> {
    static void a(q qVar, Parcel parcel, int i) {
        int H = b.H(parcel);
        b.a(parcel, 1, (Parcelable) qVar.getSession(), i, false);
        b.c(parcel, 1000, qVar.CK);
        b.a(parcel, 2, (Parcelable) qVar.jH(), i, false);
        b.H(parcel, H);
    }

    /* renamed from: bD */
    public q createFromParcel(Parcel parcel) {
        DataSet dataSet;
        Session session;
        int i;
        DataSet dataSet2 = null;
        int G = a.G(parcel);
        int i2 = 0;
        Session session2 = null;
        while (parcel.dataPosition() < G) {
            int F = a.F(parcel);
            switch (a.aH(F)) {
                case 1:
                    i = i2;
                    Session session3 = (Session) a.a(parcel, F, Session.CREATOR);
                    dataSet = dataSet2;
                    session = session3;
                    break;
                case 2:
                    dataSet = (DataSet) a.a(parcel, F, DataSet.CREATOR);
                    session = session2;
                    i = i2;
                    break;
                case 1000:
                    DataSet dataSet3 = dataSet2;
                    session = session2;
                    i = a.g(parcel, F);
                    dataSet = dataSet3;
                    break;
                default:
                    a.b(parcel, F);
                    dataSet = dataSet2;
                    session = session2;
                    i = i2;
                    break;
            }
            i2 = i;
            session2 = session;
            dataSet2 = dataSet;
        }
        if (parcel.dataPosition() == G) {
            return new q(i2, session2, dataSet2);
        }
        throw new C0004a("Overread allowed size end=" + G, parcel);
    }

    /* renamed from: cX */
    public q[] newArray(int i) {
        return new q[i];
    }
}
