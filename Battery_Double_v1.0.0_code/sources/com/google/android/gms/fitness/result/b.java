package com.google.android.gms.fitness.result;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.a.C0004a;
import com.google.android.gms.fitness.data.DataSource;
import com.google.android.gms.fitness.data.DataType;
import java.util.ArrayList;
import java.util.List;

public class b implements Creator<DataReadResult> {
    static void a(DataReadResult dataReadResult, Parcel parcel, int i) {
        int H = com.google.android.gms.common.internal.safeparcel.b.H(parcel);
        com.google.android.gms.common.internal.safeparcel.b.d(parcel, 1, dataReadResult.kt(), false);
        com.google.android.gms.common.internal.safeparcel.b.c(parcel, 1000, dataReadResult.getVersionCode());
        com.google.android.gms.common.internal.safeparcel.b.a(parcel, 2, (Parcelable) dataReadResult.getStatus(), i, false);
        com.google.android.gms.common.internal.safeparcel.b.d(parcel, 3, dataReadResult.ks(), false);
        com.google.android.gms.common.internal.safeparcel.b.c(parcel, 5, dataReadResult.kr());
        com.google.android.gms.common.internal.safeparcel.b.c(parcel, 6, dataReadResult.jw(), false);
        com.google.android.gms.common.internal.safeparcel.b.c(parcel, 7, dataReadResult.ku(), false);
        com.google.android.gms.common.internal.safeparcel.b.H(parcel, H);
    }

    /* renamed from: ce */
    public DataReadResult createFromParcel(Parcel parcel) {
        int i = 0;
        ArrayList arrayList = null;
        int G = a.G(parcel);
        ArrayList arrayList2 = new ArrayList();
        ArrayList arrayList3 = new ArrayList();
        List list = null;
        Status status = null;
        int i2 = 0;
        while (parcel.dataPosition() < G) {
            int F = a.F(parcel);
            switch (a.aH(F)) {
                case 1:
                    a.a(parcel, F, (List) arrayList2, getClass().getClassLoader());
                    break;
                case 2:
                    status = (Status) a.a(parcel, F, (Creator<T>) Status.CREATOR);
                    break;
                case 3:
                    a.a(parcel, F, (List) arrayList3, getClass().getClassLoader());
                    break;
                case 5:
                    i = a.g(parcel, F);
                    break;
                case 6:
                    list = a.c(parcel, F, DataSource.CREATOR);
                    break;
                case 7:
                    arrayList = a.c(parcel, F, DataType.CREATOR);
                    break;
                case 1000:
                    i2 = a.g(parcel, F);
                    break;
                default:
                    a.b(parcel, F);
                    break;
            }
        }
        if (parcel.dataPosition() == G) {
            return new DataReadResult(i2, arrayList2, status, arrayList3, i, list, arrayList);
        }
        throw new C0004a("Overread allowed size end=" + G, parcel);
    }

    /* renamed from: dz */
    public DataReadResult[] newArray(int i) {
        return new DataReadResult[i];
    }
}
