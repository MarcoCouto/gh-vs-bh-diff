package com.google.android.gms.fitness.result;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.a.C0004a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.fitness.data.DataSource;
import java.util.List;

public class c implements Creator<DataSourcesResult> {
    static void a(DataSourcesResult dataSourcesResult, Parcel parcel, int i) {
        int H = b.H(parcel);
        b.c(parcel, 1, dataSourcesResult.getDataSources(), false);
        b.c(parcel, 1000, dataSourcesResult.getVersionCode());
        b.a(parcel, 2, (Parcelable) dataSourcesResult.getStatus(), i, false);
        b.H(parcel, H);
    }

    /* renamed from: cf */
    public DataSourcesResult createFromParcel(Parcel parcel) {
        Status status = null;
        int G = a.G(parcel);
        int i = 0;
        List list = null;
        while (parcel.dataPosition() < G) {
            int F = a.F(parcel);
            switch (a.aH(F)) {
                case 1:
                    list = a.c(parcel, F, DataSource.CREATOR);
                    break;
                case 2:
                    status = (Status) a.a(parcel, F, (Creator<T>) Status.CREATOR);
                    break;
                case 1000:
                    i = a.g(parcel, F);
                    break;
                default:
                    a.b(parcel, F);
                    break;
            }
        }
        if (parcel.dataPosition() == G) {
            return new DataSourcesResult(i, list, status);
        }
        throw new C0004a("Overread allowed size end=" + G, parcel);
    }

    /* renamed from: dA */
    public DataSourcesResult[] newArray(int i) {
        return new DataSourcesResult[i];
    }
}
