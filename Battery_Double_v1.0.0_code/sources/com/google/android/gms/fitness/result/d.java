package com.google.android.gms.fitness.result;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.a.C0004a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.fitness.data.DataType;

public class d implements Creator<DataTypeResult> {
    static void a(DataTypeResult dataTypeResult, Parcel parcel, int i) {
        int H = b.H(parcel);
        b.a(parcel, 1, (Parcelable) dataTypeResult.getStatus(), i, false);
        b.c(parcel, 1000, dataTypeResult.getVersionCode());
        b.a(parcel, 3, (Parcelable) dataTypeResult.getDataType(), i, false);
        b.H(parcel, H);
    }

    /* renamed from: cg */
    public DataTypeResult createFromParcel(Parcel parcel) {
        DataType dataType;
        Status status;
        int i;
        DataType dataType2 = null;
        int G = a.G(parcel);
        int i2 = 0;
        Status status2 = null;
        while (parcel.dataPosition() < G) {
            int F = a.F(parcel);
            switch (a.aH(F)) {
                case 1:
                    i = i2;
                    Status status3 = (Status) a.a(parcel, F, (Creator<T>) Status.CREATOR);
                    dataType = dataType2;
                    status = status3;
                    break;
                case 3:
                    dataType = (DataType) a.a(parcel, F, DataType.CREATOR);
                    status = status2;
                    i = i2;
                    break;
                case 1000:
                    DataType dataType3 = dataType2;
                    status = status2;
                    i = a.g(parcel, F);
                    dataType = dataType3;
                    break;
                default:
                    a.b(parcel, F);
                    dataType = dataType2;
                    status = status2;
                    i = i2;
                    break;
            }
            i2 = i;
            status2 = status;
            dataType2 = dataType;
        }
        if (parcel.dataPosition() == G) {
            return new DataTypeResult(i2, status2, dataType2);
        }
        throw new C0004a("Overread allowed size end=" + G, parcel);
    }

    /* renamed from: dB */
    public DataTypeResult[] newArray(int i) {
        return new DataTypeResult[i];
    }
}
