package com.google.android.gms.fitness.result;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.a.C0004a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.fitness.data.Session;
import java.util.List;

public class g implements Creator<SessionStopResult> {
    static void a(SessionStopResult sessionStopResult, Parcel parcel, int i) {
        int H = b.H(parcel);
        b.c(parcel, 1000, sessionStopResult.getVersionCode());
        b.a(parcel, 2, (Parcelable) sessionStopResult.getStatus(), i, false);
        b.c(parcel, 3, sessionStopResult.getSessions(), false);
        b.H(parcel, H);
    }

    /* renamed from: cj */
    public SessionStopResult createFromParcel(Parcel parcel) {
        List c;
        Status status;
        int i;
        List list = null;
        int G = a.G(parcel);
        int i2 = 0;
        Status status2 = null;
        while (parcel.dataPosition() < G) {
            int F = a.F(parcel);
            switch (a.aH(F)) {
                case 2:
                    i = i2;
                    Status status3 = (Status) a.a(parcel, F, (Creator<T>) Status.CREATOR);
                    c = list;
                    status = status3;
                    break;
                case 3:
                    c = a.c(parcel, F, Session.CREATOR);
                    status = status2;
                    i = i2;
                    break;
                case 1000:
                    List list2 = list;
                    status = status2;
                    i = a.g(parcel, F);
                    c = list2;
                    break;
                default:
                    a.b(parcel, F);
                    c = list;
                    status = status2;
                    i = i2;
                    break;
            }
            i2 = i;
            status2 = status;
            list = c;
        }
        if (parcel.dataPosition() == G) {
            return new SessionStopResult(i2, status2, list);
        }
        throw new C0004a("Overread allowed size end=" + G, parcel);
    }

    /* renamed from: dE */
    public SessionStopResult[] newArray(int i) {
        return new SessionStopResult[i];
    }
}
