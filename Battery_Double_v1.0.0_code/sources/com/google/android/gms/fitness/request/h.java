package com.google.android.gms.fitness.request;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.a.C0004a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.fitness.data.DataType;
import java.util.ArrayList;
import java.util.List;

public class h implements Creator<DataSourcesRequest> {
    static void a(DataSourcesRequest dataSourcesRequest, Parcel parcel, int i) {
        int H = b.H(parcel);
        b.c(parcel, 1, dataSourcesRequest.getDataTypes(), false);
        b.c(parcel, 1000, dataSourcesRequest.getVersionCode());
        b.a(parcel, 2, dataSourcesRequest.jZ(), false);
        b.a(parcel, 3, dataSourcesRequest.jY());
        b.H(parcel, H);
    }

    /* renamed from: bM */
    public DataSourcesRequest createFromParcel(Parcel parcel) {
        ArrayList arrayList = null;
        boolean z = false;
        int G = a.G(parcel);
        List list = null;
        int i = 0;
        while (parcel.dataPosition() < G) {
            int F = a.F(parcel);
            switch (a.aH(F)) {
                case 1:
                    list = a.c(parcel, F, DataType.CREATOR);
                    break;
                case 2:
                    arrayList = a.B(parcel, F);
                    break;
                case 3:
                    z = a.c(parcel, F);
                    break;
                case 1000:
                    i = a.g(parcel, F);
                    break;
                default:
                    a.b(parcel, F);
                    break;
            }
        }
        if (parcel.dataPosition() == G) {
            return new DataSourcesRequest(i, list, arrayList, z);
        }
        throw new C0004a("Overread allowed size end=" + G, parcel);
    }

    /* renamed from: dg */
    public DataSourcesRequest[] newArray(int i) {
        return new DataSourcesRequest[i];
    }
}
