package com.google.android.gms.fitness.request;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.a.C0004a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.fitness.data.DataSource;
import com.google.android.gms.fitness.data.DataType;

public class ak implements Creator<aj> {
    static void a(aj ajVar, Parcel parcel, int i) {
        int H = b.H(parcel);
        b.a(parcel, 1, (Parcelable) ajVar.getDataType(), i, false);
        b.c(parcel, 1000, ajVar.getVersionCode());
        b.a(parcel, 2, (Parcelable) ajVar.getDataSource(), i, false);
        b.H(parcel, H);
    }

    /* renamed from: cc */
    public aj createFromParcel(Parcel parcel) {
        DataSource dataSource;
        DataType dataType;
        int i;
        DataSource dataSource2 = null;
        int G = a.G(parcel);
        int i2 = 0;
        DataType dataType2 = null;
        while (parcel.dataPosition() < G) {
            int F = a.F(parcel);
            switch (a.aH(F)) {
                case 1:
                    i = i2;
                    DataType dataType3 = (DataType) a.a(parcel, F, DataType.CREATOR);
                    dataSource = dataSource2;
                    dataType = dataType3;
                    break;
                case 2:
                    dataSource = (DataSource) a.a(parcel, F, DataSource.CREATOR);
                    dataType = dataType2;
                    i = i2;
                    break;
                case 1000:
                    DataSource dataSource3 = dataSource2;
                    dataType = dataType2;
                    i = a.g(parcel, F);
                    dataSource = dataSource3;
                    break;
                default:
                    a.b(parcel, F);
                    dataSource = dataSource2;
                    dataType = dataType2;
                    i = i2;
                    break;
            }
            i2 = i;
            dataType2 = dataType;
            dataSource2 = dataSource;
        }
        if (parcel.dataPosition() == G) {
            return new aj(i2, dataType2, dataSource2);
        }
        throw new C0004a("Overread allowed size end=" + G, parcel);
    }

    /* renamed from: dx */
    public aj[] newArray(int i) {
        return new aj[i];
    }
}
