package com.google.android.gms.fitness.request;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.a.C0004a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.fitness.data.Field;
import java.util.ArrayList;

public class i implements Creator<DataTypeCreateRequest> {
    static void a(DataTypeCreateRequest dataTypeCreateRequest, Parcel parcel, int i) {
        int H = b.H(parcel);
        b.a(parcel, 1, dataTypeCreateRequest.getName(), false);
        b.c(parcel, 1000, dataTypeCreateRequest.getVersionCode());
        b.c(parcel, 2, dataTypeCreateRequest.getFields(), false);
        b.H(parcel, H);
    }

    /* renamed from: bN */
    public DataTypeCreateRequest createFromParcel(Parcel parcel) {
        ArrayList arrayList = null;
        int G = a.G(parcel);
        int i = 0;
        String str = null;
        while (parcel.dataPosition() < G) {
            int F = a.F(parcel);
            switch (a.aH(F)) {
                case 1:
                    str = a.o(parcel, F);
                    break;
                case 2:
                    arrayList = a.c(parcel, F, Field.CREATOR);
                    break;
                case 1000:
                    i = a.g(parcel, F);
                    break;
                default:
                    a.b(parcel, F);
                    break;
            }
        }
        if (parcel.dataPosition() == G) {
            return new DataTypeCreateRequest(i, str, arrayList);
        }
        throw new C0004a("Overread allowed size end=" + G, parcel);
    }

    /* renamed from: dh */
    public DataTypeCreateRequest[] newArray(int i) {
        return new DataTypeCreateRequest[i];
    }
}
