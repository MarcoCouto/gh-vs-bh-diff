package com.google.android.gms.fitness.request;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.a.C0004a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.fitness.data.DataPoint;
import com.google.android.gms.fitness.data.DataSet;
import com.google.android.gms.fitness.data.Session;
import java.util.List;

public class s implements Creator<SessionInsertRequest> {
    static void a(SessionInsertRequest sessionInsertRequest, Parcel parcel, int i) {
        int H = b.H(parcel);
        b.a(parcel, 1, (Parcelable) sessionInsertRequest.getSession(), i, false);
        b.c(parcel, 1000, sessionInsertRequest.getVersionCode());
        b.c(parcel, 2, sessionInsertRequest.getDataSets(), false);
        b.c(parcel, 3, sessionInsertRequest.getAggregateDataPoints(), false);
        b.H(parcel, H);
    }

    /* renamed from: bS */
    public SessionInsertRequest createFromParcel(Parcel parcel) {
        List c;
        List list;
        Session session;
        int i;
        List list2 = null;
        int G = a.G(parcel);
        int i2 = 0;
        List list3 = null;
        Session session2 = null;
        while (parcel.dataPosition() < G) {
            int F = a.F(parcel);
            switch (a.aH(F)) {
                case 1:
                    i = i2;
                    List list4 = list3;
                    session = (Session) a.a(parcel, F, Session.CREATOR);
                    c = list2;
                    list = list4;
                    break;
                case 2:
                    session = session2;
                    i = i2;
                    List list5 = list2;
                    list = a.c(parcel, F, DataSet.CREATOR);
                    c = list5;
                    break;
                case 3:
                    c = a.c(parcel, F, DataPoint.CREATOR);
                    list = list3;
                    session = session2;
                    i = i2;
                    break;
                case 1000:
                    List list6 = list2;
                    list = list3;
                    session = session2;
                    i = a.g(parcel, F);
                    c = list6;
                    break;
                default:
                    a.b(parcel, F);
                    c = list2;
                    list = list3;
                    session = session2;
                    i = i2;
                    break;
            }
            i2 = i;
            session2 = session;
            list3 = list;
            list2 = c;
        }
        if (parcel.dataPosition() == G) {
            return new SessionInsertRequest(i2, session2, list3, list2);
        }
        throw new C0004a("Overread allowed size end=" + G, parcel);
    }

    /* renamed from: dn */
    public SessionInsertRequest[] newArray(int i) {
        return new SessionInsertRequest[i];
    }
}
