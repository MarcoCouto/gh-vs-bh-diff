package com.google.android.gms.fitness.request;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.a.C0004a;
import com.google.android.gms.common.internal.safeparcel.b;

public class ai implements Creator<ah> {
    static void a(ah ahVar, Parcel parcel, int i) {
        int H = b.H(parcel);
        b.c(parcel, 1000, ahVar.getVersionCode());
        b.a(parcel, 2, ahVar.getDeviceAddress(), false);
        b.H(parcel, H);
    }

    /* renamed from: cb */
    public ah createFromParcel(Parcel parcel) {
        int G = a.G(parcel);
        int i = 0;
        String str = null;
        while (parcel.dataPosition() < G) {
            int F = a.F(parcel);
            switch (a.aH(F)) {
                case 2:
                    str = a.o(parcel, F);
                    break;
                case 1000:
                    i = a.g(parcel, F);
                    break;
                default:
                    a.b(parcel, F);
                    break;
            }
        }
        if (parcel.dataPosition() == G) {
            return new ah(i, str);
        }
        throw new C0004a("Overread allowed size end=" + G, parcel);
    }

    /* renamed from: dw */
    public ah[] newArray(int i) {
        return new ah[i];
    }
}
