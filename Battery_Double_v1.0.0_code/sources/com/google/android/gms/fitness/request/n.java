package com.google.android.gms.fitness.request;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.a.C0004a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.fitness.data.DataType;

public class n implements Creator<m> {
    static void a(m mVar, Parcel parcel, int i) {
        int H = b.H(parcel);
        b.a(parcel, 1, (Parcelable) mVar.getDataType(), i, false);
        b.c(parcel, 1000, mVar.getVersionCode());
        b.H(parcel, H);
    }

    /* renamed from: bP */
    public m createFromParcel(Parcel parcel) {
        int G = a.G(parcel);
        int i = 0;
        DataType dataType = null;
        while (parcel.dataPosition() < G) {
            int F = a.F(parcel);
            switch (a.aH(F)) {
                case 1:
                    dataType = (DataType) a.a(parcel, F, DataType.CREATOR);
                    break;
                case 1000:
                    i = a.g(parcel, F);
                    break;
                default:
                    a.b(parcel, F);
                    break;
            }
        }
        if (parcel.dataPosition() == G) {
            return new m(i, dataType);
        }
        throw new C0004a("Overread allowed size end=" + G, parcel);
    }

    /* renamed from: dj */
    public m[] newArray(int i) {
        return new m[i];
    }
}
