package com.google.android.gms.fitness.request;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.a.C0004a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.fitness.data.Session;

public class x implements Creator<w> {
    static void a(w wVar, Parcel parcel, int i) {
        int H = b.H(parcel);
        b.a(parcel, 1, (Parcelable) wVar.getSession(), i, false);
        b.c(parcel, 1000, wVar.getVersionCode());
        b.H(parcel, H);
    }

    /* renamed from: bV */
    public w createFromParcel(Parcel parcel) {
        int G = a.G(parcel);
        int i = 0;
        Session session = null;
        while (parcel.dataPosition() < G) {
            int F = a.F(parcel);
            switch (a.aH(F)) {
                case 1:
                    session = (Session) a.a(parcel, F, Session.CREATOR);
                    break;
                case 1000:
                    i = a.g(parcel, F);
                    break;
                default:
                    a.b(parcel, F);
                    break;
            }
        }
        if (parcel.dataPosition() == G) {
            return new w(i, session);
        }
        throw new C0004a("Overread allowed size end=" + G, parcel);
    }

    /* renamed from: dq */
    public w[] newArray(int i) {
        return new w[i];
    }
}
