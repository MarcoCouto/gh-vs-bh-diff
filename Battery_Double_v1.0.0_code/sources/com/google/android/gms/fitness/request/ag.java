package com.google.android.gms.fitness.request;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.a.C0004a;
import com.google.android.gms.common.internal.safeparcel.b;
import com.google.android.gms.fitness.data.Subscription;

public class ag implements Creator<af> {
    static void a(af afVar, Parcel parcel, int i) {
        int H = b.H(parcel);
        b.a(parcel, 1, (Parcelable) afVar.kn(), i, false);
        b.c(parcel, 1000, afVar.getVersionCode());
        b.a(parcel, 2, afVar.ko());
        b.H(parcel, H);
    }

    /* renamed from: ca */
    public af createFromParcel(Parcel parcel) {
        boolean c;
        Subscription subscription;
        int i;
        boolean z = false;
        int G = a.G(parcel);
        Subscription subscription2 = null;
        int i2 = 0;
        while (parcel.dataPosition() < G) {
            int F = a.F(parcel);
            switch (a.aH(F)) {
                case 1:
                    i = i2;
                    Subscription subscription3 = (Subscription) a.a(parcel, F, Subscription.CREATOR);
                    c = z;
                    subscription = subscription3;
                    break;
                case 2:
                    c = a.c(parcel, F);
                    subscription = subscription2;
                    i = i2;
                    break;
                case 1000:
                    boolean z2 = z;
                    subscription = subscription2;
                    i = a.g(parcel, F);
                    c = z2;
                    break;
                default:
                    a.b(parcel, F);
                    c = z;
                    subscription = subscription2;
                    i = i2;
                    break;
            }
            i2 = i;
            subscription2 = subscription;
            z = c;
        }
        if (parcel.dataPosition() == G) {
            return new af(i2, subscription2, z);
        }
        throw new C0004a("Overread allowed size end=" + G, parcel);
    }

    /* renamed from: dv */
    public af[] newArray(int i) {
        return new af[i];
    }
}
