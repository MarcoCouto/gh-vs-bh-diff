package com.google.android.gms.maps.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.a.C0004a;
import com.google.android.gms.common.internal.safeparcel.b;

public class y implements Creator<VisibleRegion> {
    static void a(VisibleRegion visibleRegion, Parcel parcel, int i) {
        int H = b.H(parcel);
        b.c(parcel, 1, visibleRegion.getVersionCode());
        b.a(parcel, 2, (Parcelable) visibleRegion.nearLeft, i, false);
        b.a(parcel, 3, (Parcelable) visibleRegion.nearRight, i, false);
        b.a(parcel, 4, (Parcelable) visibleRegion.farLeft, i, false);
        b.a(parcel, 5, (Parcelable) visibleRegion.farRight, i, false);
        b.a(parcel, 6, (Parcelable) visibleRegion.latLngBounds, i, false);
        b.H(parcel, H);
    }

    /* renamed from: do reason: not valid java name */
    public VisibleRegion createFromParcel(Parcel parcel) {
        LatLngBounds latLngBounds = null;
        int G = a.G(parcel);
        int i = 0;
        LatLng latLng = null;
        LatLng latLng2 = null;
        LatLng latLng3 = null;
        LatLng latLng4 = null;
        while (parcel.dataPosition() < G) {
            int F = a.F(parcel);
            switch (a.aH(F)) {
                case 1:
                    i = a.g(parcel, F);
                    break;
                case 2:
                    latLng4 = (LatLng) a.a(parcel, F, (Creator<T>) LatLng.CREATOR);
                    break;
                case 3:
                    latLng3 = (LatLng) a.a(parcel, F, (Creator<T>) LatLng.CREATOR);
                    break;
                case 4:
                    latLng2 = (LatLng) a.a(parcel, F, (Creator<T>) LatLng.CREATOR);
                    break;
                case 5:
                    latLng = (LatLng) a.a(parcel, F, (Creator<T>) LatLng.CREATOR);
                    break;
                case 6:
                    latLngBounds = (LatLngBounds) a.a(parcel, F, (Creator<T>) LatLngBounds.CREATOR);
                    break;
                default:
                    a.b(parcel, F);
                    break;
            }
        }
        if (parcel.dataPosition() == G) {
            return new VisibleRegion(i, latLng4, latLng3, latLng2, latLng, latLngBounds);
        }
        throw new C0004a("Overread allowed size end=" + G, parcel);
    }

    /* renamed from: fk */
    public VisibleRegion[] newArray(int i) {
        return new VisibleRegion[i];
    }
}
