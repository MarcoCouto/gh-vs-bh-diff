package com.google.android.gms.maps.model.internal;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.a.C0004a;
import com.google.android.gms.common.internal.safeparcel.b;

public class d implements Creator<c> {
    static void a(c cVar, Parcel parcel, int i) {
        int H = b.H(parcel);
        b.c(parcel, 1, cVar.getVersionCode());
        b.c(parcel, 2, cVar.getType());
        b.a(parcel, 3, cVar.oi(), false);
        b.H(parcel, H);
    }

    /* renamed from: dq */
    public c createFromParcel(Parcel parcel) {
        int i = 0;
        int G = a.G(parcel);
        Bundle bundle = null;
        int i2 = 0;
        while (parcel.dataPosition() < G) {
            int F = a.F(parcel);
            switch (a.aH(F)) {
                case 1:
                    i2 = a.g(parcel, F);
                    break;
                case 2:
                    i = a.g(parcel, F);
                    break;
                case 3:
                    bundle = a.q(parcel, F);
                    break;
                default:
                    a.b(parcel, F);
                    break;
            }
        }
        if (parcel.dataPosition() == G) {
            return new c(i2, i, bundle);
        }
        throw new C0004a("Overread allowed size end=" + G, parcel);
    }

    /* renamed from: fm */
    public c[] newArray(int i) {
        return new c[i];
    }
}
