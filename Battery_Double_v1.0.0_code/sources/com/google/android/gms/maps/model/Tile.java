package com.google.android.gms.maps.model;

import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.maps.internal.aa;

public final class Tile implements SafeParcelable {
    public static final u CREATOR = new u();
    private final int CK;
    public final byte[] data;
    public final int height;
    public final int width;

    Tile(int versionCode, int width2, int height2, byte[] data2) {
        this.CK = versionCode;
        this.width = width2;
        this.height = height2;
        this.data = data2;
    }

    public Tile(int width2, int height2, byte[] data2) {
        this(1, width2, height2, data2);
    }

    public int describeContents() {
        return 0;
    }

    /* access modifiers changed from: 0000 */
    public int getVersionCode() {
        return this.CK;
    }

    public void writeToParcel(Parcel out, int flags) {
        if (aa.ob()) {
            v.a(this, out, flags);
        } else {
            u.a(this, out, flags);
        }
    }
}
