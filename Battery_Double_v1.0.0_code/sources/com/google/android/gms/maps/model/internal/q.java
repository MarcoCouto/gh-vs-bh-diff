package com.google.android.gms.maps.model.internal;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.a.C0004a;
import com.google.android.gms.common.internal.safeparcel.b;

public class q implements Creator<p> {
    static void a(p pVar, Parcel parcel, int i) {
        int H = b.H(parcel);
        b.c(parcel, 1, pVar.getVersionCode());
        b.a(parcel, 2, (Parcelable) pVar.ol(), i, false);
        b.H(parcel, H);
    }

    /* renamed from: ds */
    public p createFromParcel(Parcel parcel) {
        int G = a.G(parcel);
        int i = 0;
        a aVar = null;
        while (parcel.dataPosition() < G) {
            int F = a.F(parcel);
            switch (a.aH(F)) {
                case 1:
                    i = a.g(parcel, F);
                    break;
                case 2:
                    aVar = (a) a.a(parcel, F, (Creator<T>) a.CREATOR);
                    break;
                default:
                    a.b(parcel, F);
                    break;
            }
        }
        if (parcel.dataPosition() == G) {
            return new p(i, aVar);
        }
        throw new C0004a("Overread allowed size end=" + G, parcel);
    }

    /* renamed from: fp */
    public p[] newArray(int i) {
        return new p[i];
    }
}
