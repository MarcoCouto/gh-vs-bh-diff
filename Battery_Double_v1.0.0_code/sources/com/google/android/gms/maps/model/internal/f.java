package com.google.android.gms.maps.model.internal;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.a.C0004a;
import com.google.android.gms.common.internal.safeparcel.b;

public class f implements Creator<e> {
    static void a(e eVar, Parcel parcel, int i) {
        int H = b.H(parcel);
        b.c(parcel, 1, eVar.getVersionCode());
        b.a(parcel, 2, (Parcelable) eVar.oj(), i, false);
        b.H(parcel, H);
    }

    /* renamed from: dr */
    public e createFromParcel(Parcel parcel) {
        int G = a.G(parcel);
        int i = 0;
        a aVar = null;
        while (parcel.dataPosition() < G) {
            int F = a.F(parcel);
            switch (a.aH(F)) {
                case 1:
                    i = a.g(parcel, F);
                    break;
                case 2:
                    aVar = (a) a.a(parcel, F, (Creator<T>) a.CREATOR);
                    break;
                default:
                    a.b(parcel, F);
                    break;
            }
        }
        if (parcel.dataPosition() == G) {
            return new e(i, aVar);
        }
        throw new C0004a("Overread allowed size end=" + G, parcel);
    }

    /* renamed from: fn */
    public e[] newArray(int i) {
        return new e[i];
    }
}
