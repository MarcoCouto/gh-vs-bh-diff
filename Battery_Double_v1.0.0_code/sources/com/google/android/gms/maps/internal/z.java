package com.google.android.gms.maps.internal;

import android.graphics.Point;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.a.C0004a;
import com.google.android.gms.common.internal.safeparcel.b;

public class z implements Creator<y> {
    static void a(y yVar, Parcel parcel, int i) {
        int H = b.H(parcel);
        b.c(parcel, 1, yVar.getVersionCode());
        b.a(parcel, 2, (Parcelable) yVar.oa(), i, false);
        b.H(parcel, H);
    }

    /* renamed from: cZ */
    public y createFromParcel(Parcel parcel) {
        int G = a.G(parcel);
        int i = 0;
        Point point = null;
        while (parcel.dataPosition() < G) {
            int F = a.F(parcel);
            switch (a.aH(F)) {
                case 1:
                    i = a.g(parcel, F);
                    break;
                case 2:
                    point = (Point) a.a(parcel, F, Point.CREATOR);
                    break;
                default:
                    a.b(parcel, F);
                    break;
            }
        }
        if (parcel.dataPosition() == G) {
            return new y(i, point);
        }
        throw new C0004a("Overread allowed size end=" + G, parcel);
    }

    /* renamed from: eV */
    public y[] newArray(int i) {
        return new y[i];
    }
}
