package com.google.android.gms.common.data;

import java.util.ArrayList;

public abstract class g<T> extends DataBuffer<T> {
    private boolean Lr = false;
    private ArrayList<Integer> Ls;

    protected g(DataHolder dataHolder) {
        super(dataHolder);
    }

    private void hb() {
        synchronized (this) {
            if (!this.Lr) {
                int count = this.JG.getCount();
                this.Ls = new ArrayList<>();
                if (count > 0) {
                    this.Ls.add(Integer.valueOf(0));
                    String ha = ha();
                    String c = this.JG.c(ha, 0, this.JG.au(0));
                    int i = 1;
                    while (i < count) {
                        int au = this.JG.au(i);
                        String c2 = this.JG.c(ha, i, au);
                        if (c2 == null) {
                            throw new NullPointerException("Missing value for markerColumn: " + ha + ", at row: " + i + ", for window: " + au);
                        }
                        if (!c2.equals(c)) {
                            this.Ls.add(Integer.valueOf(i));
                        } else {
                            c2 = c;
                        }
                        i++;
                        c = c2;
                    }
                }
                this.Lr = true;
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public int ax(int i) {
        if (i >= 0 && i < this.Ls.size()) {
            return ((Integer) this.Ls.get(i)).intValue();
        }
        throw new IllegalArgumentException("Position " + i + " is out of bounds for this buffer");
    }

    /* access modifiers changed from: protected */
    public int ay(int i) {
        if (i < 0 || i == this.Ls.size()) {
            return 0;
        }
        int intValue = i == this.Ls.size() + -1 ? this.JG.getCount() - ((Integer) this.Ls.get(i)).intValue() : ((Integer) this.Ls.get(i + 1)).intValue() - ((Integer) this.Ls.get(i)).intValue();
        if (intValue != 1) {
            return intValue;
        }
        int ax = ax(i);
        int au = this.JG.au(ax);
        String hc = hc();
        if (hc == null || this.JG.c(hc, ax, au) != null) {
            return intValue;
        }
        return 0;
    }

    /* access modifiers changed from: protected */
    public abstract T f(int i, int i2);

    public final T get(int position) {
        hb();
        return f(ax(position), ay(position));
    }

    public int getCount() {
        hb();
        return this.Ls.size();
    }

    /* access modifiers changed from: protected */
    public abstract String ha();

    /* access modifiers changed from: protected */
    public String hc() {
        return null;
    }
}
