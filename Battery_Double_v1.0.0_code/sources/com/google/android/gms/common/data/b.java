package com.google.android.gms.common.data;

import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.a.C0004a;

public class b implements Creator<a> {
    static void a(a aVar, Parcel parcel, int i) {
        int H = com.google.android.gms.common.internal.safeparcel.b.H(parcel);
        com.google.android.gms.common.internal.safeparcel.b.c(parcel, 1, aVar.CK);
        com.google.android.gms.common.internal.safeparcel.b.a(parcel, 2, (Parcelable) aVar.KS, i, false);
        com.google.android.gms.common.internal.safeparcel.b.c(parcel, 3, aVar.Gt);
        com.google.android.gms.common.internal.safeparcel.b.H(parcel, H);
    }

    /* renamed from: A */
    public a createFromParcel(Parcel parcel) {
        int g;
        ParcelFileDescriptor parcelFileDescriptor;
        int i;
        int i2 = 0;
        int G = a.G(parcel);
        ParcelFileDescriptor parcelFileDescriptor2 = null;
        int i3 = 0;
        while (parcel.dataPosition() < G) {
            int F = a.F(parcel);
            switch (a.aH(F)) {
                case 1:
                    int i4 = i2;
                    parcelFileDescriptor = parcelFileDescriptor2;
                    i = a.g(parcel, F);
                    g = i4;
                    break;
                case 2:
                    i = i3;
                    ParcelFileDescriptor parcelFileDescriptor3 = (ParcelFileDescriptor) a.a(parcel, F, ParcelFileDescriptor.CREATOR);
                    g = i2;
                    parcelFileDescriptor = parcelFileDescriptor3;
                    break;
                case 3:
                    g = a.g(parcel, F);
                    parcelFileDescriptor = parcelFileDescriptor2;
                    i = i3;
                    break;
                default:
                    a.b(parcel, F);
                    g = i2;
                    parcelFileDescriptor = parcelFileDescriptor2;
                    i = i3;
                    break;
            }
            i3 = i;
            parcelFileDescriptor2 = parcelFileDescriptor;
            i2 = g;
        }
        if (parcel.dataPosition() == G) {
            return new a(i3, parcelFileDescriptor2, i2);
        }
        throw new C0004a("Overread allowed size end=" + G, parcel);
    }

    /* renamed from: ar */
    public a[] newArray(int i) {
        return new a[i];
    }
}
