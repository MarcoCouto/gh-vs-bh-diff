package com.google.android.gms.common.data;

import android.database.CharArrayBuffer;
import android.net.Uri;
import com.google.android.gms.internal.jv;
import com.google.android.gms.internal.jx;

public abstract class d {
    protected final DataHolder JG;
    protected int KZ;
    private int La;

    public d(DataHolder dataHolder, int i) {
        this.JG = (DataHolder) jx.i(dataHolder);
        as(i);
    }

    /* access modifiers changed from: protected */
    public void a(String str, CharArrayBuffer charArrayBuffer) {
        this.JG.a(str, this.KZ, this.La, charArrayBuffer);
    }

    public boolean aQ(String str) {
        return this.JG.aQ(str);
    }

    /* access modifiers changed from: protected */
    public Uri aR(String str) {
        return this.JG.g(str, this.KZ, this.La);
    }

    /* access modifiers changed from: protected */
    public boolean aS(String str) {
        return this.JG.h(str, this.KZ, this.La);
    }

    /* access modifiers changed from: protected */
    public void as(int i) {
        jx.K(i >= 0 && i < this.JG.getCount());
        this.KZ = i;
        this.La = this.JG.au(this.KZ);
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof d)) {
            return false;
        }
        d dVar = (d) obj;
        return jv.equal(Integer.valueOf(dVar.KZ), Integer.valueOf(this.KZ)) && jv.equal(Integer.valueOf(dVar.La), Integer.valueOf(this.La)) && dVar.JG == this.JG;
    }

    /* access modifiers changed from: protected */
    public int gW() {
        return this.KZ;
    }

    /* access modifiers changed from: protected */
    public boolean getBoolean(String column) {
        return this.JG.d(column, this.KZ, this.La);
    }

    /* access modifiers changed from: protected */
    public byte[] getByteArray(String column) {
        return this.JG.f(column, this.KZ, this.La);
    }

    /* access modifiers changed from: protected */
    public float getFloat(String column) {
        return this.JG.e(column, this.KZ, this.La);
    }

    /* access modifiers changed from: protected */
    public int getInteger(String column) {
        return this.JG.b(column, this.KZ, this.La);
    }

    /* access modifiers changed from: protected */
    public long getLong(String column) {
        return this.JG.a(column, this.KZ, this.La);
    }

    /* access modifiers changed from: protected */
    public String getString(String column) {
        return this.JG.c(column, this.KZ, this.La);
    }

    public int hashCode() {
        return jv.hashCode(Integer.valueOf(this.KZ), Integer.valueOf(this.La), this.JG);
    }

    public boolean isDataValid() {
        return !this.JG.isClosed();
    }
}
