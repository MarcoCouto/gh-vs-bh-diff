package com.google.android.gms.common.images;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.widget.ImageView;
import com.google.android.gms.common.images.ImageManager.OnImageLoadedListener;
import com.google.android.gms.internal.iz;
import com.google.android.gms.internal.ja;
import com.google.android.gms.internal.jb;
import com.google.android.gms.internal.jc;
import com.google.android.gms.internal.je;
import com.google.android.gms.internal.jv;
import java.lang.ref.WeakReference;

public abstract class a {
    final C0003a LJ;
    protected int LK = 0;
    protected int LL = 0;
    protected boolean LM = false;
    protected OnImageLoadedListener LN;
    private boolean LO = true;
    private boolean LP = false;
    private boolean LQ = true;
    protected int LR;

    /* renamed from: com.google.android.gms.common.images.a$a reason: collision with other inner class name */
    static final class C0003a {
        public final Uri uri;

        public C0003a(Uri uri2) {
            this.uri = uri2;
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof C0003a)) {
                return false;
            }
            if (this == obj) {
                return true;
            }
            return jv.equal(((C0003a) obj).uri, this.uri);
        }

        public int hashCode() {
            return jv.hashCode(this.uri);
        }
    }

    public static final class b extends a {
        private WeakReference<ImageView> LS;

        public b(ImageView imageView, int i) {
            super(null, i);
            je.f(imageView);
            this.LS = new WeakReference<>(imageView);
        }

        public b(ImageView imageView, Uri uri) {
            super(uri, 0);
            je.f(imageView);
            this.LS = new WeakReference<>(imageView);
        }

        private void a(ImageView imageView, Drawable drawable, boolean z, boolean z2, boolean z3) {
            boolean z4 = !z2 && !z3;
            if (z4 && (imageView instanceof jb)) {
                int hj = ((jb) imageView).hj();
                if (this.LL != 0 && hj == this.LL) {
                    return;
                }
            }
            boolean b = b(z, z2);
            Drawable drawable2 = (!this.LM || drawable == null) ? drawable : drawable.getConstantState().newDrawable();
            if (b) {
                drawable2 = a(imageView.getDrawable(), drawable2);
            }
            imageView.setImageDrawable(drawable2);
            if (imageView instanceof jb) {
                jb jbVar = (jb) imageView;
                jbVar.g(z3 ? this.LJ.uri : null);
                jbVar.aB(z4 ? this.LL : 0);
            }
            if (b) {
                ((iz) drawable2).startTransition(250);
            }
        }

        /* access modifiers changed from: protected */
        public void a(Drawable drawable, boolean z, boolean z2, boolean z3) {
            ImageView imageView = (ImageView) this.LS.get();
            if (imageView != null) {
                a(imageView, drawable, z, z2, z3);
            }
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof b)) {
                return false;
            }
            if (this == obj) {
                return true;
            }
            ImageView imageView = (ImageView) this.LS.get();
            ImageView imageView2 = (ImageView) ((b) obj).LS.get();
            return (imageView2 == null || imageView == null || !jv.equal(imageView2, imageView)) ? false : true;
        }

        public int hashCode() {
            return 0;
        }
    }

    public static final class c extends a {
        private WeakReference<OnImageLoadedListener> LT;

        public c(OnImageLoadedListener onImageLoadedListener, Uri uri) {
            super(uri, 0);
            je.f(onImageLoadedListener);
            this.LT = new WeakReference<>(onImageLoadedListener);
        }

        /* access modifiers changed from: protected */
        public void a(Drawable drawable, boolean z, boolean z2, boolean z3) {
            if (!z2) {
                OnImageLoadedListener onImageLoadedListener = (OnImageLoadedListener) this.LT.get();
                if (onImageLoadedListener != null) {
                    onImageLoadedListener.onImageLoaded(this.LJ.uri, drawable, z3);
                }
            }
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof c)) {
                return false;
            }
            if (this == obj) {
                return true;
            }
            c cVar = (c) obj;
            OnImageLoadedListener onImageLoadedListener = (OnImageLoadedListener) this.LT.get();
            OnImageLoadedListener onImageLoadedListener2 = (OnImageLoadedListener) cVar.LT.get();
            return onImageLoadedListener2 != null && onImageLoadedListener != null && jv.equal(onImageLoadedListener2, onImageLoadedListener) && jv.equal(cVar.LJ, this.LJ);
        }

        public int hashCode() {
            return jv.hashCode(this.LJ);
        }
    }

    public a(Uri uri, int i) {
        this.LJ = new C0003a(uri);
        this.LL = i;
    }

    private Drawable a(Context context, jc jcVar, int i) {
        Resources resources = context.getResources();
        if (this.LR <= 0) {
            return resources.getDrawable(i);
        }
        com.google.android.gms.internal.jc.a aVar = new com.google.android.gms.internal.jc.a(i, this.LR);
        Drawable drawable = (Drawable) jcVar.get(aVar);
        if (drawable != null) {
            return drawable;
        }
        Drawable drawable2 = resources.getDrawable(i);
        if ((this.LR & 1) != 0) {
            drawable2 = a(resources, drawable2);
        }
        jcVar.put(aVar, drawable2);
        return drawable2;
    }

    /* access modifiers changed from: protected */
    public Drawable a(Resources resources, Drawable drawable) {
        return ja.a(resources, drawable);
    }

    /* access modifiers changed from: protected */
    public iz a(Drawable drawable, Drawable drawable2) {
        if (drawable == null) {
            drawable = null;
        } else if (drawable instanceof iz) {
            drawable = ((iz) drawable).hh();
        }
        return new iz(drawable, drawable2);
    }

    /* access modifiers changed from: 0000 */
    public void a(Context context, Bitmap bitmap, boolean z) {
        je.f(bitmap);
        if ((this.LR & 1) != 0) {
            bitmap = ja.a(bitmap);
        }
        BitmapDrawable bitmapDrawable = new BitmapDrawable(context.getResources(), bitmap);
        if (this.LN != null) {
            this.LN.onImageLoaded(this.LJ.uri, bitmapDrawable, true);
        }
        a(bitmapDrawable, z, false, true);
    }

    /* access modifiers changed from: 0000 */
    public void a(Context context, jc jcVar) {
        if (this.LQ) {
            Drawable drawable = null;
            if (this.LK != 0) {
                drawable = a(context, jcVar, this.LK);
            }
            a(drawable, false, true, false);
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(Context context, jc jcVar, boolean z) {
        Drawable drawable = null;
        if (this.LL != 0) {
            drawable = a(context, jcVar, this.LL);
        }
        if (this.LN != null) {
            this.LN.onImageLoaded(this.LJ.uri, drawable, false);
        }
        a(drawable, z, false, false);
    }

    /* access modifiers changed from: protected */
    public abstract void a(Drawable drawable, boolean z, boolean z2, boolean z3);

    public void az(int i) {
        this.LL = i;
    }

    /* access modifiers changed from: protected */
    public boolean b(boolean z, boolean z2) {
        return this.LO && !z2 && (!z || this.LP);
    }
}
