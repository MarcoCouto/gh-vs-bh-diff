package com.google.android.gms.common.api;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.os.DeadObjectException;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.Api.ApiOptions;
import com.google.android.gms.common.api.GoogleApiClient.Builder;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.internal.jg;
import com.google.android.gms.internal.jm;
import com.google.android.gms.internal.jx;
import com.google.android.gms.internal.kc;
import java.lang.ref.WeakReference;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

final class c implements GoogleApiClient {
    private final Looper JF;
    private final Condition JR = this.zO.newCondition();
    private final jm JS;
    private final int JT;
    final Queue<d<?>> JU = new LinkedList();
    /* access modifiers changed from: private */
    public ConnectionResult JV;
    /* access modifiers changed from: private */
    public int JW;
    /* access modifiers changed from: private */
    public volatile int JX = 4;
    /* access modifiers changed from: private */
    public volatile boolean JY;
    private boolean JZ = false;
    private final b Jy = new b() {
        public void b(d<?> dVar) {
            c.this.Kk.remove(dVar);
        }
    };
    private int Ka;
    /* access modifiers changed from: private */
    public long Kb = 120000;
    /* access modifiers changed from: private */
    public long Kc = 5000;
    final Handler Kd;
    BroadcastReceiver Ke;
    /* access modifiers changed from: private */
    public final Bundle Kf = new Bundle();
    private final Map<com.google.android.gms.common.api.Api.c<?>, com.google.android.gms.common.api.Api.a> Kg = new HashMap();
    private final List<String> Kh;
    /* access modifiers changed from: private */
    public boolean Ki;
    private final Set<d<?>> Kj = Collections.newSetFromMap(new WeakHashMap());
    final Set<d<?>> Kk = Collections.newSetFromMap(new ConcurrentHashMap());
    private final ConnectionCallbacks Kl = new ConnectionCallbacks() {
        public void onConnected(Bundle connectionHint) {
            c.this.zO.lock();
            try {
                if (c.this.JX == 1) {
                    if (connectionHint != null) {
                        c.this.Kf.putAll(connectionHint);
                    }
                    c.this.gJ();
                }
            } finally {
                c.this.zO.unlock();
            }
        }

        public void onConnectionSuspended(int cause) {
            c.this.zO.lock();
            switch (cause) {
                case 1:
                    if (!c.this.gL()) {
                        c.this.JY = true;
                        c.this.Ke = new a(c.this);
                        IntentFilter intentFilter = new IntentFilter("android.intent.action.PACKAGE_ADDED");
                        intentFilter.addDataScheme("package");
                        c.this.mContext.registerReceiver(c.this.Ke, intentFilter);
                        c.this.Kd.sendMessageDelayed(c.this.Kd.obtainMessage(1), c.this.Kb);
                        c.this.Kd.sendMessageDelayed(c.this.Kd.obtainMessage(2), c.this.Kc);
                        c.this.al(cause);
                        break;
                    } else {
                        c.this.zO.unlock();
                        return;
                    }
                case 2:
                    try {
                        c.this.al(cause);
                        c.this.connect();
                        break;
                    } catch (Throwable th) {
                        c.this.zO.unlock();
                        throw th;
                    }
            }
            c.this.zO.unlock();
        }
    };
    private final com.google.android.gms.internal.jm.b Km = new com.google.android.gms.internal.jm.b() {
        public Bundle fX() {
            return null;
        }

        public boolean gN() {
            return c.this.Ki;
        }

        public boolean isConnected() {
            return c.this.isConnected();
        }
    };
    /* access modifiers changed from: private */
    public final Context mContext;
    /* access modifiers changed from: private */
    public final Lock zO = new ReentrantLock();

    private static class a extends BroadcastReceiver {
        private WeakReference<c> Ks;

        a(c cVar) {
            this.Ks = new WeakReference<>(cVar);
        }

        public void onReceive(Context context, Intent intent) {
            Uri data = intent.getData();
            String str = null;
            if (data != null) {
                str = data.getSchemeSpecificPart();
            }
            if (str != null && str.equals(GooglePlayServicesUtil.GOOGLE_PLAY_SERVICES_PACKAGE)) {
                c cVar = (c) this.Ks.get();
                if (cVar != null && !cVar.isConnected() && !cVar.isConnecting() && cVar.gL()) {
                    cVar.connect();
                }
            }
        }
    }

    interface b {
        void b(d<?> dVar);
    }

    /* renamed from: com.google.android.gms.common.api.c$c reason: collision with other inner class name */
    private class C0002c extends Handler {
        C0002c(Looper looper) {
            super(looper);
        }

        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    c.this.gM();
                    return;
                case 2:
                    c.this.resume();
                    return;
                default:
                    Log.w("GoogleApiClientImpl", "Unknown message id: " + msg.what);
                    return;
            }
        }
    }

    interface d<A extends com.google.android.gms.common.api.Api.a> {
        void a(b bVar);

        void b(A a) throws DeadObjectException;

        void cancel();

        int gF();

        com.google.android.gms.common.api.Api.c<A> gz();

        void l(Status status);
    }

    public c(Context context, Looper looper, jg jgVar, Map<Api<?>, ApiOptions> map, Set<ConnectionCallbacks> set, Set<OnConnectionFailedListener> set2, int i) {
        this.mContext = context;
        this.JS = new jm(context, looper, this.Km);
        this.JF = looper;
        this.Kd = new C0002c(looper);
        this.JT = i;
        for (ConnectionCallbacks registerConnectionCallbacks : set) {
            this.JS.registerConnectionCallbacks(registerConnectionCallbacks);
        }
        for (OnConnectionFailedListener registerConnectionFailedListener : set2) {
            this.JS.registerConnectionFailedListener(registerConnectionFailedListener);
        }
        for (Api api : map.keySet()) {
            final com.google.android.gms.common.api.Api.b gx = api.gx();
            Object obj = map.get(api);
            this.Kg.put(api.gz(), a(gx, obj, context, looper, jgVar, this.Kl, new OnConnectionFailedListener() {
                public void onConnectionFailed(ConnectionResult result) {
                    c.this.zO.lock();
                    try {
                        if (c.this.JV == null || gx.getPriority() < c.this.JW) {
                            c.this.JV = result;
                            c.this.JW = gx.getPriority();
                        }
                        c.this.gJ();
                    } finally {
                        c.this.zO.unlock();
                    }
                }
            }));
        }
        this.Kh = Collections.unmodifiableList(jgVar.ho());
    }

    private static <C extends com.google.android.gms.common.api.Api.a, O> C a(com.google.android.gms.common.api.Api.b<C, O> bVar, Object obj, Context context, Looper looper, jg jgVar, ConnectionCallbacks connectionCallbacks, OnConnectionFailedListener onConnectionFailedListener) {
        return bVar.a(context, looper, jgVar, obj, connectionCallbacks, onConnectionFailedListener);
    }

    /* access modifiers changed from: private */
    public void a(final GoogleApiClient googleApiClient, final f fVar, final boolean z) {
        kc.Nu.c(googleApiClient).setResultCallback(new ResultCallback<Status>() {
            /* renamed from: j */
            public void onResult(Status status) {
                if (status.isSuccess() && c.this.isConnected()) {
                    c.this.reconnect();
                }
                fVar.b(status);
                if (z) {
                    googleApiClient.disconnect();
                }
            }
        });
    }

    private <A extends com.google.android.gms.common.api.Api.a> void a(d<A> dVar) throws DeadObjectException {
        this.zO.lock();
        try {
            jx.b(dVar.gz() != null, (Object) "This task can not be executed or enqueued (it's probably a Batch or malformed)");
            this.Kk.add(dVar);
            dVar.a(this.Jy);
            if (gL()) {
                dVar.l(new Status(8));
                return;
            }
            dVar.b(a(dVar.gz()));
            this.zO.unlock();
        } finally {
            this.zO.unlock();
        }
    }

    /* access modifiers changed from: private */
    public void al(int i) {
        this.zO.lock();
        try {
            if (this.JX != 3) {
                if (i == -1) {
                    if (isConnecting()) {
                        Iterator it = this.JU.iterator();
                        while (it.hasNext()) {
                            d dVar = (d) it.next();
                            if (dVar.gF() != 1) {
                                dVar.cancel();
                                it.remove();
                            }
                        }
                    } else {
                        this.JU.clear();
                    }
                    for (d cancel : this.Kk) {
                        cancel.cancel();
                    }
                    this.Kk.clear();
                    for (d clear : this.Kj) {
                        clear.clear();
                    }
                    this.Kj.clear();
                    if (this.JV == null && !this.JU.isEmpty()) {
                        this.JZ = true;
                        return;
                    }
                }
                boolean isConnecting = isConnecting();
                boolean isConnected = isConnected();
                this.JX = 3;
                if (isConnecting) {
                    if (i == -1) {
                        this.JV = null;
                    }
                    this.JR.signalAll();
                }
                this.Ki = false;
                for (com.google.android.gms.common.api.Api.a aVar : this.Kg.values()) {
                    if (aVar.isConnected()) {
                        aVar.disconnect();
                    }
                }
                this.Ki = true;
                this.JX = 4;
                if (isConnected) {
                    if (i != -1) {
                        this.JS.aE(i);
                    }
                    this.Ki = false;
                }
            }
            this.zO.unlock();
        } finally {
            this.zO.unlock();
        }
    }

    /* access modifiers changed from: private */
    public void gJ() {
        this.Ka--;
        if (this.Ka != 0) {
            return;
        }
        if (this.JV != null) {
            this.JZ = false;
            al(3);
            if (!gL() || !GooglePlayServicesUtil.e(this.mContext, this.JV.getErrorCode())) {
                gM();
                this.JS.b(this.JV);
            }
            this.Ki = false;
            return;
        }
        this.JX = 2;
        gM();
        this.JR.signalAll();
        gK();
        if (this.JZ) {
            this.JZ = false;
            al(-1);
            return;
        }
        this.JS.f(this.Kf.isEmpty() ? null : this.Kf);
    }

    private void gK() {
        this.zO.lock();
        try {
            jx.a(isConnected() || gL(), "GoogleApiClient is not connected yet.");
            while (!this.JU.isEmpty()) {
                a((d) this.JU.remove());
            }
            this.zO.unlock();
        } catch (DeadObjectException e) {
            Log.w("GoogleApiClientImpl", "Service died while flushing queue", e);
        } catch (Throwable th) {
            this.zO.unlock();
            throw th;
        }
    }

    /* access modifiers changed from: private */
    public void gM() {
        this.zO.lock();
        try {
            if (this.JY) {
                this.JY = false;
                this.Kd.removeMessages(2);
                this.Kd.removeMessages(1);
                this.mContext.unregisterReceiver(this.Ke);
                this.zO.unlock();
            }
        } finally {
            this.zO.unlock();
        }
    }

    /* access modifiers changed from: private */
    public void resume() {
        this.zO.lock();
        try {
            if (gL()) {
                connect();
            }
        } finally {
            this.zO.unlock();
        }
    }

    public <C extends com.google.android.gms.common.api.Api.a> C a(com.google.android.gms.common.api.Api.c<C> cVar) {
        C c = (com.google.android.gms.common.api.Api.a) this.Kg.get(cVar);
        jx.b(c, (Object) "Appropriate Api was not requested.");
        return c;
    }

    public <A extends com.google.android.gms.common.api.Api.a, R extends Result, T extends com.google.android.gms.common.api.BaseImplementation.a<R, A>> T a(T t) {
        this.zO.lock();
        try {
            if (isConnected()) {
                b(t);
            } else {
                this.JU.add(t);
            }
            return t;
        } finally {
            this.zO.unlock();
        }
    }

    public boolean a(Scope scope) {
        return this.Kh.contains(scope.gO());
    }

    public <A extends com.google.android.gms.common.api.Api.a, T extends com.google.android.gms.common.api.BaseImplementation.a<? extends Result, A>> T b(T t) {
        jx.a(isConnected() || gL(), "GoogleApiClient is not connected yet.");
        gK();
        try {
            a((d<A>) t);
        } catch (DeadObjectException e) {
            al(1);
        }
        return t;
    }

    public ConnectionResult blockingConnect() {
        ConnectionResult connectionResult;
        jx.a(Looper.myLooper() != Looper.getMainLooper(), "blockingConnect must not be called on the UI thread");
        this.zO.lock();
        try {
            connect();
            while (isConnecting()) {
                this.JR.await();
            }
            if (isConnected()) {
                connectionResult = ConnectionResult.Iu;
            } else if (this.JV != null) {
                connectionResult = this.JV;
                this.zO.unlock();
            } else {
                connectionResult = new ConnectionResult(13, null);
                this.zO.unlock();
            }
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            connectionResult = new ConnectionResult(15, null);
        } finally {
            this.zO.unlock();
        }
        return connectionResult;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0058, code lost:
        if (isConnected() == false) goto L_0x0062;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x005a, code lost:
        r0 = com.google.android.gms.common.ConnectionResult.Iu;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0064, code lost:
        if (r5.JV == null) goto L_0x006e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0066, code lost:
        r0 = r5.JV;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0068, code lost:
        r5.zO.unlock();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:?, code lost:
        r0 = new com.google.android.gms.common.ConnectionResult(13, null);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0076, code lost:
        r5.zO.unlock();
     */
    public ConnectionResult blockingConnect(long timeout, TimeUnit unit) {
        ConnectionResult connectionResult;
        jx.a(Looper.myLooper() != Looper.getMainLooper(), "blockingConnect must not be called on the UI thread");
        this.zO.lock();
        try {
            connect();
            long nanos = unit.toNanos(timeout);
            while (true) {
                if (!isConnecting()) {
                    break;
                }
                nanos = this.JR.awaitNanos(nanos);
                if (nanos <= 0) {
                    connectionResult = new ConnectionResult(14, null);
                    break;
                }
            }
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            connectionResult = new ConnectionResult(15, null);
        } finally {
            this.zO.unlock();
        }
        return connectionResult;
    }

    public PendingResult<Status> clearDefaultAccountAndReconnect() {
        jx.a(isConnected(), "GoogleApiClient is not connected yet.");
        final f fVar = new f(this.JF);
        if (this.Kg.containsKey(kc.DQ)) {
            a(this, fVar, false);
        } else {
            final AtomicReference atomicReference = new AtomicReference();
            AnonymousClass5 r2 = new ConnectionCallbacks() {
                public void onConnected(Bundle connectionHint) {
                    c.this.a((GoogleApiClient) atomicReference.get(), fVar, true);
                }

                public void onConnectionSuspended(int cause) {
                }
            };
            GoogleApiClient build = new Builder(this.mContext).addApi(kc.API).addConnectionCallbacks(r2).addOnConnectionFailedListener(new OnConnectionFailedListener() {
                public void onConnectionFailed(ConnectionResult result) {
                    fVar.b(new Status(8));
                }
            }).setHandler(this.Kd).build();
            atomicReference.set(build);
            build.connect();
        }
        return fVar;
    }

    public void connect() {
        this.zO.lock();
        try {
            this.JZ = false;
            if (!isConnected() && !isConnecting()) {
                this.Ki = true;
                this.JV = null;
                this.JX = 1;
                this.Kf.clear();
                this.Ka = this.Kg.size();
                for (com.google.android.gms.common.api.Api.a connect : this.Kg.values()) {
                    connect.connect();
                }
                this.zO.unlock();
            }
        } finally {
            this.zO.unlock();
        }
    }

    public <L> d<L> d(L l) {
        jx.b(l, (Object) "Listener must not be null");
        this.zO.lock();
        try {
            d<L> dVar = new d<>(this.JF, l);
            this.Kj.add(dVar);
            return dVar;
        } finally {
            this.zO.unlock();
        }
    }

    public void disconnect() {
        gM();
        al(-1);
    }

    /* access modifiers changed from: 0000 */
    public boolean gL() {
        return this.JY;
    }

    public Looper getLooper() {
        return this.JF;
    }

    public boolean isConnected() {
        return this.JX == 2;
    }

    public boolean isConnecting() {
        return this.JX == 1;
    }

    public boolean isConnectionCallbacksRegistered(ConnectionCallbacks listener) {
        return this.JS.isConnectionCallbacksRegistered(listener);
    }

    public boolean isConnectionFailedListenerRegistered(OnConnectionFailedListener listener) {
        return this.JS.isConnectionFailedListenerRegistered(listener);
    }

    public void reconnect() {
        disconnect();
        connect();
    }

    public void registerConnectionCallbacks(ConnectionCallbacks listener) {
        this.JS.registerConnectionCallbacks(listener);
    }

    public void registerConnectionFailedListener(OnConnectionFailedListener listener) {
        this.JS.registerConnectionFailedListener(listener);
    }

    public void stopAutoManage(FragmentActivity lifecycleActivity) {
        jx.a(this.JT >= 0, "Called stopAutoManage but automatic lifecycle management is not enabled.");
        g.a(lifecycleActivity).ao(this.JT);
    }

    public void unregisterConnectionCallbacks(ConnectionCallbacks listener) {
        this.JS.unregisterConnectionCallbacks(listener);
    }

    public void unregisterConnectionFailedListener(OnConnectionFailedListener listener) {
        this.JS.unregisterConnectionFailedListener(listener);
    }
}
