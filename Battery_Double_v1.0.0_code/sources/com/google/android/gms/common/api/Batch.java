package com.google.android.gms.common.api;

import android.os.Looper;
import com.google.android.gms.common.api.BaseImplementation.AbstractPendingResult;
import com.google.android.gms.common.api.BaseImplementation.CallbackHandler;
import com.google.android.gms.common.api.PendingResult.a;
import java.util.ArrayList;
import java.util.List;

public final class Batch extends AbstractPendingResult<BatchResult> {
    /* access modifiers changed from: private */
    public boolean JA;
    /* access modifiers changed from: private */
    public boolean JB;
    /* access modifiers changed from: private */
    public final PendingResult<?>[] JC;
    /* access modifiers changed from: private */
    public int Jz;
    /* access modifiers changed from: private */
    public final Object mH;

    public static final class Builder {
        private List<PendingResult<?>> JE = new ArrayList();
        private Looper JF;

        public Builder(GoogleApiClient googleApiClient) {
            this.JF = googleApiClient.getLooper();
        }

        public <R extends Result> BatchResultToken<R> add(PendingResult<R> pendingResult) {
            BatchResultToken<R> batchResultToken = new BatchResultToken<>(this.JE.size());
            this.JE.add(pendingResult);
            return batchResultToken;
        }

        public Batch build() {
            return new Batch(this.JE, this.JF);
        }
    }

    private Batch(List<PendingResult<?>> pendingResultList, Looper looper) {
        super(new CallbackHandler<>(looper));
        this.mH = new Object();
        this.Jz = pendingResultList.size();
        this.JC = new PendingResult[this.Jz];
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < pendingResultList.size()) {
                PendingResult<?> pendingResult = (PendingResult) pendingResultList.get(i2);
                this.JC[i2] = pendingResult;
                pendingResult.a(new a() {
                    /* JADX WARNING: Code restructure failed: missing block: B:29:?, code lost:
                        return;
                     */
                    public void m(Status status) {
                        synchronized (Batch.this.mH) {
                            if (!Batch.this.isCanceled()) {
                                if (status.isCanceled()) {
                                    Batch.this.JB = true;
                                } else if (!status.isSuccess()) {
                                    Batch.this.JA = true;
                                }
                                Batch.this.Jz = Batch.this.Jz - 1;
                                if (Batch.this.Jz == 0) {
                                    if (Batch.this.JB) {
                                        Batch.super.cancel();
                                    } else {
                                        Batch.this.b(new BatchResult(Batch.this.JA ? new Status(13) : Status.Kw, Batch.this.JC));
                                    }
                                }
                            }
                        }
                    }
                });
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    public void cancel() {
        super.cancel();
        for (PendingResult<?> cancel : this.JC) {
            cancel.cancel();
        }
    }

    /* renamed from: createFailedResult */
    public BatchResult c(Status status) {
        return new BatchResult(status, this.JC);
    }
}
