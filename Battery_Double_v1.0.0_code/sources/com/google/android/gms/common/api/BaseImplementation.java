package com.google.android.gms.common.api;

import android.os.DeadObjectException;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.util.Log;
import android.util.Pair;
import com.google.android.gms.common.api.Api.c;
import com.google.android.gms.internal.jr;
import com.google.android.gms.internal.jx;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public class BaseImplementation {

    public static abstract class AbstractPendingResult<R extends Result> implements b<R>, PendingResult<R> {
        private final Object Jp = new Object();
        private final ArrayList<com.google.android.gms.common.api.PendingResult.a> Jq = new ArrayList<>();
        private ResultCallback<R> Jr;
        private volatile R Js;
        private volatile boolean Jt;
        private boolean Ju;
        private boolean Jv;
        private jr Jw;
        protected final CallbackHandler<R> mHandler;
        private final CountDownLatch mr = new CountDownLatch(1);

        protected AbstractPendingResult(Looper looper) {
            this.mHandler = new CallbackHandler<>(looper);
        }

        protected AbstractPendingResult(CallbackHandler<R> callbackHandler) {
            this.mHandler = callbackHandler;
        }

        private void c(R r) {
            this.Js = r;
            this.Jw = null;
            this.mr.countDown();
            Status status = this.Js.getStatus();
            if (this.Jr != null) {
                this.mHandler.removeTimeoutMessages();
                if (!this.Ju) {
                    this.mHandler.sendResultCallback(this.Jr, gA());
                }
            }
            Iterator it = this.Jq.iterator();
            while (it.hasNext()) {
                ((com.google.android.gms.common.api.PendingResult.a) it.next()).m(status);
            }
            this.Jq.clear();
        }

        private R gA() {
            R r;
            synchronized (this.Jp) {
                jx.a(!this.Jt, "Result has already been consumed.");
                jx.a(isReady(), "Result is not ready.");
                r = this.Js;
                gB();
            }
            return r;
        }

        private void gC() {
            synchronized (this.Jp) {
                if (!isReady()) {
                    b((R) c(Status.Kx));
                    this.Jv = true;
                }
            }
        }

        /* access modifiers changed from: private */
        public void gD() {
            synchronized (this.Jp) {
                if (!isReady()) {
                    b((R) c(Status.Kz));
                    this.Jv = true;
                }
            }
        }

        public final void a(com.google.android.gms.common.api.PendingResult.a aVar) {
            jx.a(!this.Jt, "Result has already been consumed.");
            synchronized (this.Jp) {
                if (isReady()) {
                    aVar.m(this.Js.getStatus());
                } else {
                    this.Jq.add(aVar);
                }
            }
        }

        /* access modifiers changed from: protected */
        public final void a(jr jrVar) {
            synchronized (this.Jp) {
                this.Jw = jrVar;
            }
        }

        public final R await() {
            boolean z = true;
            jx.a(Looper.myLooper() != Looper.getMainLooper(), "await must not be called on the UI thread");
            if (this.Jt) {
                z = false;
            }
            jx.a(z, "Result has already been consumed");
            try {
                this.mr.await();
            } catch (InterruptedException e) {
                gC();
            }
            jx.a(isReady(), "Result is not ready.");
            return gA();
        }

        public final R await(long time, TimeUnit units) {
            boolean z = true;
            jx.a(time <= 0 || Looper.myLooper() != Looper.getMainLooper(), "await must not be called on the UI thread when time is greater than zero.");
            if (this.Jt) {
                z = false;
            }
            jx.a(z, "Result has already been consumed.");
            try {
                if (!this.mr.await(time, units)) {
                    gD();
                }
            } catch (InterruptedException e) {
                gC();
            }
            jx.a(isReady(), "Result is not ready.");
            return gA();
        }

        public final void b(R r) {
            boolean z = true;
            synchronized (this.Jp) {
                if (this.Jv || this.Ju) {
                    BaseImplementation.a(r);
                    return;
                }
                jx.a(!isReady(), "Results have already been set");
                if (this.Jt) {
                    z = false;
                }
                jx.a(z, "Result has already been consumed");
                c(r);
            }
        }

        /* access modifiers changed from: protected */
        public abstract R c(Status status);

        /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
            return;
         */
        public void cancel() {
            synchronized (this.Jp) {
                if (!this.Ju && !this.Jt) {
                    if (this.Jw != null) {
                        try {
                            this.Jw.cancel();
                        } catch (RemoteException e) {
                        }
                    }
                    BaseImplementation.a(this.Js);
                    this.Jr = null;
                    this.Ju = true;
                    c((R) c(Status.KA));
                }
            }
        }

        /* access modifiers changed from: protected */
        public void gB() {
            this.Jt = true;
            this.Js = null;
            this.Jr = null;
        }

        public boolean isCanceled() {
            boolean z;
            synchronized (this.Jp) {
                z = this.Ju;
            }
            return z;
        }

        public final boolean isReady() {
            return this.mr.getCount() == 0;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
            return;
         */
        public final void setResultCallback(ResultCallback<R> callback) {
            jx.a(!this.Jt, "Result has already been consumed.");
            synchronized (this.Jp) {
                if (!isCanceled()) {
                    if (isReady()) {
                        this.mHandler.sendResultCallback(callback, gA());
                    } else {
                        this.Jr = callback;
                    }
                }
            }
        }

        /* JADX WARNING: Code restructure failed: missing block: B:24:?, code lost:
            return;
         */
        public final void setResultCallback(ResultCallback<R> callback, long time, TimeUnit units) {
            boolean z = true;
            jx.a(!this.Jt, "Result has already been consumed.");
            if (this.mHandler == null) {
                z = false;
            }
            jx.a(z, "CallbackHandler has not been set before calling setResultCallback.");
            synchronized (this.Jp) {
                if (!isCanceled()) {
                    if (isReady()) {
                        this.mHandler.sendResultCallback(callback, gA());
                    } else {
                        this.Jr = callback;
                        this.mHandler.sendTimeoutResultCallback(this, units.toMillis(time));
                    }
                }
            }
        }
    }

    public static class CallbackHandler<R extends Result> extends Handler {
        public static final int CALLBACK_ON_COMPLETE = 1;
        public static final int CALLBACK_ON_TIMEOUT = 2;

        public CallbackHandler() {
            this(Looper.getMainLooper());
        }

        public CallbackHandler(Looper looper) {
            super(looper);
        }

        /* access modifiers changed from: protected */
        public void deliverResultCallback(ResultCallback<R> callback, R result) {
            try {
                callback.onResult(result);
            } catch (RuntimeException e) {
                BaseImplementation.a(result);
                throw e;
            }
        }

        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    Pair pair = (Pair) msg.obj;
                    deliverResultCallback((ResultCallback) pair.first, (Result) pair.second);
                    return;
                case 2:
                    ((AbstractPendingResult) msg.obj).gD();
                    return;
                default:
                    Log.wtf("GoogleApi", "Don't know how to handle this message.");
                    return;
            }
        }

        public void removeTimeoutMessages() {
            removeMessages(2);
        }

        public void sendResultCallback(ResultCallback<R> callback, R result) {
            sendMessage(obtainMessage(1, new Pair(callback, result)));
        }

        public void sendTimeoutResultCallback(AbstractPendingResult<R> pendingResult, long millis) {
            sendMessageDelayed(obtainMessage(2, pendingResult), millis);
        }
    }

    public static abstract class a<R extends Result, A extends com.google.android.gms.common.api.Api.a> extends AbstractPendingResult<R> implements d<A> {
        private final c<A> Jn;
        private final GoogleApiClient Jx;
        private b Jy;

        protected a(c<A> cVar, GoogleApiClient googleApiClient) {
            super(googleApiClient.getLooper());
            this.Jn = (c) jx.i(cVar);
            this.Jx = googleApiClient;
        }

        private void a(RemoteException remoteException) {
            l(new Status(8, remoteException.getLocalizedMessage(), null));
        }

        /* access modifiers changed from: protected */
        public abstract void a(A a) throws RemoteException;

        public void a(b bVar) {
            this.Jy = bVar;
        }

        public final void b(A a) throws DeadObjectException {
            try {
                a(a);
            } catch (DeadObjectException e) {
                a((RemoteException) e);
                throw e;
            } catch (RemoteException e2) {
                a(e2);
            }
        }

        /* access modifiers changed from: protected */
        public void gB() {
            super.gB();
            if (this.Jy != null) {
                this.Jy.b(this);
                this.Jy = null;
            }
        }

        public final a gE() {
            jx.b(this.Jx, (Object) "GoogleApiClient was not set.");
            this.Jx.b(this);
            return this;
        }

        public int gF() {
            return 0;
        }

        public final c<A> gz() {
            return this.Jn;
        }

        public final void l(Status status) {
            jx.b(!status.isSuccess(), (Object) "Failed result must not be success");
            b(c(status));
        }
    }

    public interface b<R> {
        void b(R r);
    }

    static void a(Result result) {
        if (result instanceof Releasable) {
            try {
                ((Releasable) result).release();
            } catch (RuntimeException e) {
                Log.w("GoogleApi", "Unable to release " + result, e);
            }
        }
    }
}
