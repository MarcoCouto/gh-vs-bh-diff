package com.google.android.gms.common.api;

import android.os.Looper;
import com.google.android.gms.common.api.BaseImplementation.AbstractPendingResult;

public class f extends AbstractPendingResult<Status> {
    public f(Looper looper) {
        super(looper);
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public Status c(Status status) {
        return status;
    }
}
