package io.realm;

public interface NetworkStatisticsRealmProxyInterface {
    double realmGet$mobileReceived();

    double realmGet$mobileSent();

    double realmGet$wifiReceived();

    double realmGet$wifiSent();

    void realmSet$mobileReceived(double d);

    void realmSet$mobileSent(double d);

    void realmSet$wifiReceived(double d);

    void realmSet$wifiSent(double d);
}
