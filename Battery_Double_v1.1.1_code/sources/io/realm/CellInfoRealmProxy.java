package io.realm;

import android.annotation.TargetApi;
import android.util.JsonReader;
import android.util.JsonToken;
import com.mansoon.BatteryDouble.models.data.CellInfo;
import io.realm.BaseRealm.RealmObjectContext;
import io.realm.internal.ColumnInfo;
import io.realm.internal.OsObject;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.OsObjectSchemaInfo.Builder;
import io.realm.internal.OsSchemaInfo;
import io.realm.internal.RealmObjectProxy;
import io.realm.internal.RealmObjectProxy.CacheData;
import io.realm.internal.Row;
import io.realm.internal.Table;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class CellInfoRealmProxy extends CellInfo implements RealmObjectProxy, CellInfoRealmProxyInterface {
    private static final List<String> FIELD_NAMES;
    private static final OsObjectSchemaInfo expectedObjectSchemaInfo = createExpectedObjectSchemaInfo();
    private CellInfoColumnInfo columnInfo;
    private ProxyState<CellInfo> proxyState;

    static final class CellInfoColumnInfo extends ColumnInfo {
        long cidIndex;
        long lacIndex;
        long mccIndex;
        long mncIndex;
        long radioTypeIndex;

        CellInfoColumnInfo(OsSchemaInfo osSchemaInfo) {
            super(5);
            OsObjectSchemaInfo objectSchemaInfo = osSchemaInfo.getObjectSchemaInfo("CellInfo");
            this.mccIndex = addColumnDetails("mcc", objectSchemaInfo);
            this.mncIndex = addColumnDetails("mnc", objectSchemaInfo);
            this.lacIndex = addColumnDetails("lac", objectSchemaInfo);
            this.cidIndex = addColumnDetails("cid", objectSchemaInfo);
            this.radioTypeIndex = addColumnDetails("radioType", objectSchemaInfo);
        }

        CellInfoColumnInfo(ColumnInfo columnInfo, boolean z) {
            super(columnInfo, z);
            copy(columnInfo, this);
        }

        /* access modifiers changed from: protected */
        public final ColumnInfo copy(boolean z) {
            return new CellInfoColumnInfo(this, z);
        }

        /* access modifiers changed from: protected */
        public final void copy(ColumnInfo columnInfo, ColumnInfo columnInfo2) {
            CellInfoColumnInfo cellInfoColumnInfo = (CellInfoColumnInfo) columnInfo;
            CellInfoColumnInfo cellInfoColumnInfo2 = (CellInfoColumnInfo) columnInfo2;
            cellInfoColumnInfo2.mccIndex = cellInfoColumnInfo.mccIndex;
            cellInfoColumnInfo2.mncIndex = cellInfoColumnInfo.mncIndex;
            cellInfoColumnInfo2.lacIndex = cellInfoColumnInfo.lacIndex;
            cellInfoColumnInfo2.cidIndex = cellInfoColumnInfo.cidIndex;
            cellInfoColumnInfo2.radioTypeIndex = cellInfoColumnInfo.radioTypeIndex;
        }
    }

    public static String getTableName() {
        return "class_CellInfo";
    }

    static {
        ArrayList arrayList = new ArrayList(5);
        arrayList.add("mcc");
        arrayList.add("mnc");
        arrayList.add("lac");
        arrayList.add("cid");
        arrayList.add("radioType");
        FIELD_NAMES = Collections.unmodifiableList(arrayList);
    }

    CellInfoRealmProxy() {
        this.proxyState.setConstructionFinished();
    }

    public void realm$injectObjectContext() {
        if (this.proxyState == null) {
            RealmObjectContext realmObjectContext = (RealmObjectContext) BaseRealm.objectContext.get();
            this.columnInfo = (CellInfoColumnInfo) realmObjectContext.getColumnInfo();
            this.proxyState = new ProxyState<>(this);
            this.proxyState.setRealm$realm(realmObjectContext.getRealm());
            this.proxyState.setRow$realm(realmObjectContext.getRow());
            this.proxyState.setAcceptDefaultValue$realm(realmObjectContext.getAcceptDefaultValue());
            this.proxyState.setExcludeFields$realm(realmObjectContext.getExcludeFields());
        }
    }

    public int realmGet$mcc() {
        this.proxyState.getRealm$realm().checkIfValid();
        return (int) this.proxyState.getRow$realm().getLong(this.columnInfo.mccIndex);
    }

    public void realmSet$mcc(int i) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            this.proxyState.getRow$realm().setLong(this.columnInfo.mccIndex, (long) i);
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            row$realm.getTable().setLong(this.columnInfo.mccIndex, row$realm.getIndex(), (long) i, true);
        }
    }

    public int realmGet$mnc() {
        this.proxyState.getRealm$realm().checkIfValid();
        return (int) this.proxyState.getRow$realm().getLong(this.columnInfo.mncIndex);
    }

    public void realmSet$mnc(int i) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            this.proxyState.getRow$realm().setLong(this.columnInfo.mncIndex, (long) i);
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            row$realm.getTable().setLong(this.columnInfo.mncIndex, row$realm.getIndex(), (long) i, true);
        }
    }

    public int realmGet$lac() {
        this.proxyState.getRealm$realm().checkIfValid();
        return (int) this.proxyState.getRow$realm().getLong(this.columnInfo.lacIndex);
    }

    public void realmSet$lac(int i) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            this.proxyState.getRow$realm().setLong(this.columnInfo.lacIndex, (long) i);
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            row$realm.getTable().setLong(this.columnInfo.lacIndex, row$realm.getIndex(), (long) i, true);
        }
    }

    public int realmGet$cid() {
        this.proxyState.getRealm$realm().checkIfValid();
        return (int) this.proxyState.getRow$realm().getLong(this.columnInfo.cidIndex);
    }

    public void realmSet$cid(int i) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            this.proxyState.getRow$realm().setLong(this.columnInfo.cidIndex, (long) i);
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            row$realm.getTable().setLong(this.columnInfo.cidIndex, row$realm.getIndex(), (long) i, true);
        }
    }

    public String realmGet$radioType() {
        this.proxyState.getRealm$realm().checkIfValid();
        return this.proxyState.getRow$realm().getString(this.columnInfo.radioTypeIndex);
    }

    public void realmSet$radioType(String str) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            if (str == null) {
                this.proxyState.getRow$realm().setNull(this.columnInfo.radioTypeIndex);
            } else {
                this.proxyState.getRow$realm().setString(this.columnInfo.radioTypeIndex, str);
            }
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            if (str == null) {
                row$realm.getTable().setNull(this.columnInfo.radioTypeIndex, row$realm.getIndex(), true);
            } else {
                row$realm.getTable().setString(this.columnInfo.radioTypeIndex, row$realm.getIndex(), str, true);
            }
        }
    }

    private static OsObjectSchemaInfo createExpectedObjectSchemaInfo() {
        Builder builder = new Builder("CellInfo", 5, 0);
        Builder builder2 = builder;
        builder2.addPersistedProperty("mcc", RealmFieldType.INTEGER, false, false, true);
        builder2.addPersistedProperty("mnc", RealmFieldType.INTEGER, false, false, true);
        builder2.addPersistedProperty("lac", RealmFieldType.INTEGER, false, false, true);
        builder2.addPersistedProperty("cid", RealmFieldType.INTEGER, false, false, true);
        builder2.addPersistedProperty("radioType", RealmFieldType.STRING, false, false, false);
        return builder.build();
    }

    public static OsObjectSchemaInfo getExpectedObjectSchemaInfo() {
        return expectedObjectSchemaInfo;
    }

    public static CellInfoColumnInfo createColumnInfo(OsSchemaInfo osSchemaInfo) {
        return new CellInfoColumnInfo(osSchemaInfo);
    }

    public static List<String> getFieldNames() {
        return FIELD_NAMES;
    }

    public static CellInfo createOrUpdateUsingJsonObject(Realm realm, JSONObject jSONObject, boolean z) throws JSONException {
        CellInfo cellInfo = (CellInfo) realm.createObjectInternal(CellInfo.class, true, Collections.emptyList());
        CellInfoRealmProxyInterface cellInfoRealmProxyInterface = cellInfo;
        if (jSONObject.has("mcc")) {
            if (jSONObject.isNull("mcc")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'mcc' to null.");
            }
            cellInfoRealmProxyInterface.realmSet$mcc(jSONObject.getInt("mcc"));
        }
        if (jSONObject.has("mnc")) {
            if (jSONObject.isNull("mnc")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'mnc' to null.");
            }
            cellInfoRealmProxyInterface.realmSet$mnc(jSONObject.getInt("mnc"));
        }
        if (jSONObject.has("lac")) {
            if (jSONObject.isNull("lac")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'lac' to null.");
            }
            cellInfoRealmProxyInterface.realmSet$lac(jSONObject.getInt("lac"));
        }
        if (jSONObject.has("cid")) {
            if (jSONObject.isNull("cid")) {
                throw new IllegalArgumentException("Trying to set non-nullable field 'cid' to null.");
            }
            cellInfoRealmProxyInterface.realmSet$cid(jSONObject.getInt("cid"));
        }
        if (jSONObject.has("radioType")) {
            if (jSONObject.isNull("radioType")) {
                cellInfoRealmProxyInterface.realmSet$radioType(null);
            } else {
                cellInfoRealmProxyInterface.realmSet$radioType(jSONObject.getString("radioType"));
            }
        }
        return cellInfo;
    }

    @TargetApi(11)
    public static CellInfo createUsingJsonStream(Realm realm, JsonReader jsonReader) throws IOException {
        CellInfo cellInfo = new CellInfo();
        CellInfoRealmProxyInterface cellInfoRealmProxyInterface = cellInfo;
        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            String nextName = jsonReader.nextName();
            if (nextName.equals("mcc")) {
                if (jsonReader.peek() != JsonToken.NULL) {
                    cellInfoRealmProxyInterface.realmSet$mcc(jsonReader.nextInt());
                } else {
                    jsonReader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'mcc' to null.");
                }
            } else if (nextName.equals("mnc")) {
                if (jsonReader.peek() != JsonToken.NULL) {
                    cellInfoRealmProxyInterface.realmSet$mnc(jsonReader.nextInt());
                } else {
                    jsonReader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'mnc' to null.");
                }
            } else if (nextName.equals("lac")) {
                if (jsonReader.peek() != JsonToken.NULL) {
                    cellInfoRealmProxyInterface.realmSet$lac(jsonReader.nextInt());
                } else {
                    jsonReader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'lac' to null.");
                }
            } else if (nextName.equals("cid")) {
                if (jsonReader.peek() != JsonToken.NULL) {
                    cellInfoRealmProxyInterface.realmSet$cid(jsonReader.nextInt());
                } else {
                    jsonReader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'cid' to null.");
                }
            } else if (!nextName.equals("radioType")) {
                jsonReader.skipValue();
            } else if (jsonReader.peek() != JsonToken.NULL) {
                cellInfoRealmProxyInterface.realmSet$radioType(jsonReader.nextString());
            } else {
                jsonReader.skipValue();
                cellInfoRealmProxyInterface.realmSet$radioType(null);
            }
        }
        jsonReader.endObject();
        return (CellInfo) realm.copyToRealm(cellInfo);
    }

    public static CellInfo copyOrUpdate(Realm realm, CellInfo cellInfo, boolean z, Map<RealmModel, RealmObjectProxy> map) {
        if (cellInfo instanceof RealmObjectProxy) {
            RealmObjectProxy realmObjectProxy = (RealmObjectProxy) cellInfo;
            if (realmObjectProxy.realmGet$proxyState().getRealm$realm() != null) {
                BaseRealm realm$realm = realmObjectProxy.realmGet$proxyState().getRealm$realm();
                if (realm$realm.threadId != realm.threadId) {
                    throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
                } else if (realm$realm.getPath().equals(realm.getPath())) {
                    return cellInfo;
                }
            }
        }
        RealmObjectContext realmObjectContext = (RealmObjectContext) BaseRealm.objectContext.get();
        RealmObjectProxy realmObjectProxy2 = (RealmObjectProxy) map.get(cellInfo);
        if (realmObjectProxy2 != null) {
            return (CellInfo) realmObjectProxy2;
        }
        return copy(realm, cellInfo, z, map);
    }

    public static CellInfo copy(Realm realm, CellInfo cellInfo, boolean z, Map<RealmModel, RealmObjectProxy> map) {
        RealmObjectProxy realmObjectProxy = (RealmObjectProxy) map.get(cellInfo);
        if (realmObjectProxy != null) {
            return (CellInfo) realmObjectProxy;
        }
        CellInfo cellInfo2 = (CellInfo) realm.createObjectInternal(CellInfo.class, false, Collections.emptyList());
        map.put(cellInfo, (RealmObjectProxy) cellInfo2);
        CellInfoRealmProxyInterface cellInfoRealmProxyInterface = cellInfo;
        CellInfoRealmProxyInterface cellInfoRealmProxyInterface2 = cellInfo2;
        cellInfoRealmProxyInterface2.realmSet$mcc(cellInfoRealmProxyInterface.realmGet$mcc());
        cellInfoRealmProxyInterface2.realmSet$mnc(cellInfoRealmProxyInterface.realmGet$mnc());
        cellInfoRealmProxyInterface2.realmSet$lac(cellInfoRealmProxyInterface.realmGet$lac());
        cellInfoRealmProxyInterface2.realmSet$cid(cellInfoRealmProxyInterface.realmGet$cid());
        cellInfoRealmProxyInterface2.realmSet$radioType(cellInfoRealmProxyInterface.realmGet$radioType());
        return cellInfo2;
    }

    public static long insert(Realm realm, CellInfo cellInfo, Map<RealmModel, Long> map) {
        CellInfo cellInfo2 = cellInfo;
        if (cellInfo2 instanceof RealmObjectProxy) {
            RealmObjectProxy realmObjectProxy = (RealmObjectProxy) cellInfo2;
            if (realmObjectProxy.realmGet$proxyState().getRealm$realm() != null && realmObjectProxy.realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                return realmObjectProxy.realmGet$proxyState().getRow$realm().getIndex();
            }
        }
        Table table = realm.getTable(CellInfo.class);
        long nativePtr = table.getNativePtr();
        CellInfoColumnInfo cellInfoColumnInfo = (CellInfoColumnInfo) realm.getSchema().getColumnInfo(CellInfo.class);
        long createRow = OsObject.createRow(table);
        map.put(cellInfo2, Long.valueOf(createRow));
        CellInfoRealmProxyInterface cellInfoRealmProxyInterface = cellInfo2;
        long j = nativePtr;
        long j2 = createRow;
        Table.nativeSetLong(j, cellInfoColumnInfo.mccIndex, j2, (long) cellInfoRealmProxyInterface.realmGet$mcc(), false);
        Table.nativeSetLong(j, cellInfoColumnInfo.mncIndex, j2, (long) cellInfoRealmProxyInterface.realmGet$mnc(), false);
        Table.nativeSetLong(j, cellInfoColumnInfo.lacIndex, j2, (long) cellInfoRealmProxyInterface.realmGet$lac(), false);
        Table.nativeSetLong(j, cellInfoColumnInfo.cidIndex, j2, (long) cellInfoRealmProxyInterface.realmGet$cid(), false);
        String realmGet$radioType = cellInfoRealmProxyInterface.realmGet$radioType();
        if (realmGet$radioType != null) {
            Table.nativeSetString(nativePtr, cellInfoColumnInfo.radioTypeIndex, createRow, realmGet$radioType, false);
        }
        return createRow;
    }

    public static void insert(Realm realm, Iterator<? extends RealmModel> it, Map<RealmModel, Long> map) {
        Map<RealmModel, Long> map2 = map;
        Table table = realm.getTable(CellInfo.class);
        long nativePtr = table.getNativePtr();
        CellInfoColumnInfo cellInfoColumnInfo = (CellInfoColumnInfo) realm.getSchema().getColumnInfo(CellInfo.class);
        while (it.hasNext()) {
            CellInfo cellInfo = (CellInfo) it.next();
            if (!map2.containsKey(cellInfo)) {
                if (cellInfo instanceof RealmObjectProxy) {
                    RealmObjectProxy realmObjectProxy = (RealmObjectProxy) cellInfo;
                    if (realmObjectProxy.realmGet$proxyState().getRealm$realm() != null && realmObjectProxy.realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                        map2.put(cellInfo, Long.valueOf(realmObjectProxy.realmGet$proxyState().getRow$realm().getIndex()));
                    }
                }
                long createRow = OsObject.createRow(table);
                map2.put(cellInfo, Long.valueOf(createRow));
                CellInfoRealmProxyInterface cellInfoRealmProxyInterface = cellInfo;
                long j = createRow;
                Table.nativeSetLong(nativePtr, cellInfoColumnInfo.mccIndex, createRow, (long) cellInfoRealmProxyInterface.realmGet$mcc(), false);
                long j2 = j;
                Table.nativeSetLong(nativePtr, cellInfoColumnInfo.mncIndex, j2, (long) cellInfoRealmProxyInterface.realmGet$mnc(), false);
                Table.nativeSetLong(nativePtr, cellInfoColumnInfo.lacIndex, j2, (long) cellInfoRealmProxyInterface.realmGet$lac(), false);
                Table.nativeSetLong(nativePtr, cellInfoColumnInfo.cidIndex, j2, (long) cellInfoRealmProxyInterface.realmGet$cid(), false);
                String realmGet$radioType = cellInfoRealmProxyInterface.realmGet$radioType();
                if (realmGet$radioType != null) {
                    Table.nativeSetString(nativePtr, cellInfoColumnInfo.radioTypeIndex, j, realmGet$radioType, false);
                }
            }
        }
    }

    public static long insertOrUpdate(Realm realm, CellInfo cellInfo, Map<RealmModel, Long> map) {
        CellInfo cellInfo2 = cellInfo;
        if (cellInfo2 instanceof RealmObjectProxy) {
            RealmObjectProxy realmObjectProxy = (RealmObjectProxy) cellInfo2;
            if (realmObjectProxy.realmGet$proxyState().getRealm$realm() != null && realmObjectProxy.realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                return realmObjectProxy.realmGet$proxyState().getRow$realm().getIndex();
            }
        }
        Table table = realm.getTable(CellInfo.class);
        long nativePtr = table.getNativePtr();
        CellInfoColumnInfo cellInfoColumnInfo = (CellInfoColumnInfo) realm.getSchema().getColumnInfo(CellInfo.class);
        long createRow = OsObject.createRow(table);
        map.put(cellInfo2, Long.valueOf(createRow));
        CellInfoRealmProxyInterface cellInfoRealmProxyInterface = cellInfo2;
        long j = nativePtr;
        long j2 = createRow;
        Table.nativeSetLong(j, cellInfoColumnInfo.mccIndex, j2, (long) cellInfoRealmProxyInterface.realmGet$mcc(), false);
        Table.nativeSetLong(j, cellInfoColumnInfo.mncIndex, j2, (long) cellInfoRealmProxyInterface.realmGet$mnc(), false);
        Table.nativeSetLong(j, cellInfoColumnInfo.lacIndex, j2, (long) cellInfoRealmProxyInterface.realmGet$lac(), false);
        Table.nativeSetLong(j, cellInfoColumnInfo.cidIndex, j2, (long) cellInfoRealmProxyInterface.realmGet$cid(), false);
        String realmGet$radioType = cellInfoRealmProxyInterface.realmGet$radioType();
        if (realmGet$radioType != null) {
            Table.nativeSetString(nativePtr, cellInfoColumnInfo.radioTypeIndex, createRow, realmGet$radioType, false);
        } else {
            Table.nativeSetNull(nativePtr, cellInfoColumnInfo.radioTypeIndex, createRow, false);
        }
        return createRow;
    }

    public static void insertOrUpdate(Realm realm, Iterator<? extends RealmModel> it, Map<RealmModel, Long> map) {
        Map<RealmModel, Long> map2 = map;
        Table table = realm.getTable(CellInfo.class);
        long nativePtr = table.getNativePtr();
        CellInfoColumnInfo cellInfoColumnInfo = (CellInfoColumnInfo) realm.getSchema().getColumnInfo(CellInfo.class);
        while (it.hasNext()) {
            CellInfo cellInfo = (CellInfo) it.next();
            if (!map2.containsKey(cellInfo)) {
                if (cellInfo instanceof RealmObjectProxy) {
                    RealmObjectProxy realmObjectProxy = (RealmObjectProxy) cellInfo;
                    if (realmObjectProxy.realmGet$proxyState().getRealm$realm() != null && realmObjectProxy.realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                        map2.put(cellInfo, Long.valueOf(realmObjectProxy.realmGet$proxyState().getRow$realm().getIndex()));
                    }
                }
                long createRow = OsObject.createRow(table);
                map2.put(cellInfo, Long.valueOf(createRow));
                CellInfoRealmProxyInterface cellInfoRealmProxyInterface = cellInfo;
                long j = createRow;
                Table.nativeSetLong(nativePtr, cellInfoColumnInfo.mccIndex, createRow, (long) cellInfoRealmProxyInterface.realmGet$mcc(), false);
                long j2 = j;
                Table.nativeSetLong(nativePtr, cellInfoColumnInfo.mncIndex, j2, (long) cellInfoRealmProxyInterface.realmGet$mnc(), false);
                Table.nativeSetLong(nativePtr, cellInfoColumnInfo.lacIndex, j2, (long) cellInfoRealmProxyInterface.realmGet$lac(), false);
                Table.nativeSetLong(nativePtr, cellInfoColumnInfo.cidIndex, j2, (long) cellInfoRealmProxyInterface.realmGet$cid(), false);
                String realmGet$radioType = cellInfoRealmProxyInterface.realmGet$radioType();
                if (realmGet$radioType != null) {
                    Table.nativeSetString(nativePtr, cellInfoColumnInfo.radioTypeIndex, j, realmGet$radioType, false);
                } else {
                    Table.nativeSetNull(nativePtr, cellInfoColumnInfo.radioTypeIndex, j, false);
                }
            }
        }
    }

    public static CellInfo createDetachedCopy(CellInfo cellInfo, int i, int i2, Map<RealmModel, CacheData<RealmModel>> map) {
        CellInfo cellInfo2;
        if (i > i2 || cellInfo == null) {
            return null;
        }
        CacheData cacheData = (CacheData) map.get(cellInfo);
        if (cacheData == null) {
            cellInfo2 = new CellInfo();
            map.put(cellInfo, new CacheData(i, cellInfo2));
        } else if (i >= cacheData.minDepth) {
            return (CellInfo) cacheData.object;
        } else {
            CellInfo cellInfo3 = (CellInfo) cacheData.object;
            cacheData.minDepth = i;
            cellInfo2 = cellInfo3;
        }
        CellInfoRealmProxyInterface cellInfoRealmProxyInterface = cellInfo2;
        CellInfoRealmProxyInterface cellInfoRealmProxyInterface2 = cellInfo;
        cellInfoRealmProxyInterface.realmSet$mcc(cellInfoRealmProxyInterface2.realmGet$mcc());
        cellInfoRealmProxyInterface.realmSet$mnc(cellInfoRealmProxyInterface2.realmGet$mnc());
        cellInfoRealmProxyInterface.realmSet$lac(cellInfoRealmProxyInterface2.realmGet$lac());
        cellInfoRealmProxyInterface.realmSet$cid(cellInfoRealmProxyInterface2.realmGet$cid());
        cellInfoRealmProxyInterface.realmSet$radioType(cellInfoRealmProxyInterface2.realmGet$radioType());
        return cellInfo2;
    }

    public String toString() {
        if (!RealmObject.isValid(this)) {
            return "Invalid object";
        }
        StringBuilder sb = new StringBuilder("CellInfo = proxy[");
        sb.append("{mcc:");
        sb.append(realmGet$mcc());
        sb.append("}");
        sb.append(",");
        sb.append("{mnc:");
        sb.append(realmGet$mnc());
        sb.append("}");
        sb.append(",");
        sb.append("{lac:");
        sb.append(realmGet$lac());
        sb.append("}");
        sb.append(",");
        sb.append("{cid:");
        sb.append(realmGet$cid());
        sb.append("}");
        sb.append(",");
        sb.append("{radioType:");
        sb.append(realmGet$radioType() != null ? realmGet$radioType() : "null");
        sb.append("}");
        sb.append("]");
        return sb.toString();
    }

    public ProxyState<?> realmGet$proxyState() {
        return this.proxyState;
    }

    public int hashCode() {
        String path = this.proxyState.getRealm$realm().getPath();
        String name = this.proxyState.getRow$realm().getTable().getName();
        long index = this.proxyState.getRow$realm().getIndex();
        int i = 0;
        int hashCode = (527 + (path != null ? path.hashCode() : 0)) * 31;
        if (name != null) {
            i = name.hashCode();
        }
        return (31 * (hashCode + i)) + ((int) (index ^ (index >>> 32)));
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        CellInfoRealmProxy cellInfoRealmProxy = (CellInfoRealmProxy) obj;
        String path = this.proxyState.getRealm$realm().getPath();
        String path2 = cellInfoRealmProxy.proxyState.getRealm$realm().getPath();
        if (path == null ? path2 != null : !path.equals(path2)) {
            return false;
        }
        String name = this.proxyState.getRow$realm().getTable().getName();
        String name2 = cellInfoRealmProxy.proxyState.getRow$realm().getTable().getName();
        if (name == null ? name2 == null : name.equals(name2)) {
            return this.proxyState.getRow$realm().getIndex() == cellInfoRealmProxy.proxyState.getRow$realm().getIndex();
        }
        return false;
    }
}
