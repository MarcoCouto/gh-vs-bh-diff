package io.realm.internal.android;

import java.text.ParseException;
import java.text.ParsePosition;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

public class ISO8601Utils {
    private static final TimeZone TIMEZONE_UTC = TimeZone.getTimeZone(UTC_ID);
    private static final TimeZone TIMEZONE_Z = TIMEZONE_UTC;
    private static final String UTC_ID = "UTC";

    /* JADX WARNING: Removed duplicated region for block: B:47:0x00c7 A[Catch:{ IndexOutOfBoundsException -> 0x01b9, NumberFormatException -> 0x01b6, IllegalArgumentException -> 0x01b3 }] */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x00cf A[Catch:{ IndexOutOfBoundsException -> 0x01b9, NumberFormatException -> 0x01b6, IllegalArgumentException -> 0x01b3 }] */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x01bd  */
    /* JADX WARNING: Removed duplicated region for block: B:83:0x01bf  */
    public static Date parse(String str, ParsePosition parsePosition) throws ParseException {
        Exception exc;
        String str2;
        String message;
        int i;
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        TimeZone timeZone;
        String str3 = str;
        ParsePosition parsePosition2 = parsePosition;
        try {
            int index = parsePosition.getIndex();
            int i7 = index + 4;
            int parseInt = parseInt(str3, index, i7);
            if (checkOffset(str3, i7, '-')) {
                i7++;
            }
            int i8 = i7 + 2;
            int parseInt2 = parseInt(str3, i7, i8);
            if (checkOffset(str3, i8, '-')) {
                i8++;
            }
            int i9 = i8 + 2;
            int parseInt3 = parseInt(str3, i8, i9);
            boolean checkOffset = checkOffset(str3, i9, 'T');
            if (checkOffset || str.length() > i9) {
                if (checkOffset) {
                    int i10 = i9 + 1;
                    int i11 = i10 + 2;
                    i5 = parseInt(str3, i10, i11);
                    if (checkOffset(str3, i11, ':')) {
                        i11++;
                    }
                    int i12 = i11 + 2;
                    i4 = parseInt(str3, i11, i12);
                    if (checkOffset(str3, i12, ':')) {
                        i12++;
                    }
                    if (str.length() > i12) {
                        char charAt = str3.charAt(i12);
                        if (!(charAt == 'Z' || charAt == '+' || charAt == '-')) {
                            i2 = i12 + 2;
                            i = parseInt(str3, i12, i2);
                            if (i > 59 && i < 63) {
                                i = 59;
                            }
                            if (checkOffset(str3, i2, '.')) {
                                int i13 = i2 + 1;
                                int indexOfNonDigit = indexOfNonDigit(str3, i13 + 1);
                                int min = Math.min(indexOfNonDigit, i13 + 3);
                                int parseInt4 = parseInt(str3, i13, min);
                                switch (min - i13) {
                                    case 1:
                                        parseInt4 *= 100;
                                        break;
                                    case 2:
                                        parseInt4 *= 10;
                                        break;
                                }
                                i3 = parseInt4;
                                i2 = indexOfNonDigit;
                            } else {
                                i3 = 0;
                            }
                            if (str.length() > i2) {
                                throw new IllegalArgumentException("No time zone indicator");
                            }
                            char charAt2 = str3.charAt(i2);
                            if (charAt2 == 'Z') {
                                timeZone = TIMEZONE_Z;
                                i6 = i2 + 1;
                            } else {
                                if (charAt2 != '+') {
                                    if (charAt2 != '-') {
                                        StringBuilder sb = new StringBuilder();
                                        sb.append("Invalid time zone indicator '");
                                        sb.append(charAt2);
                                        sb.append("'");
                                        throw new IndexOutOfBoundsException(sb.toString());
                                    }
                                }
                                String substring = str3.substring(i2);
                                i6 = i2 + substring.length();
                                if (substring.length() == 3) {
                                    StringBuilder sb2 = new StringBuilder();
                                    sb2.append(substring);
                                    sb2.append("00");
                                    substring = sb2.toString();
                                }
                                if (!"+0000".equals(substring)) {
                                    if (!"+00:00".equals(substring)) {
                                        StringBuilder sb3 = new StringBuilder();
                                        sb3.append("GMT");
                                        sb3.append(substring);
                                        String sb4 = sb3.toString();
                                        TimeZone timeZone2 = TimeZone.getTimeZone(sb4);
                                        String id = timeZone2.getID();
                                        if (id.equals(sb4) || id.replace(":", "").equals(sb4)) {
                                            timeZone = timeZone2;
                                        } else {
                                            StringBuilder sb5 = new StringBuilder();
                                            sb5.append("Mismatching time zone indicator: ");
                                            sb5.append(sb4);
                                            sb5.append(" given, resolves to ");
                                            sb5.append(timeZone2.getID());
                                            throw new IndexOutOfBoundsException(sb5.toString());
                                        }
                                    }
                                }
                                timeZone = TIMEZONE_Z;
                            }
                            GregorianCalendar gregorianCalendar = new GregorianCalendar(timeZone);
                            gregorianCalendar.setLenient(false);
                            gregorianCalendar.set(1, parseInt);
                            gregorianCalendar.set(2, parseInt2 - 1);
                            gregorianCalendar.set(5, parseInt3);
                            gregorianCalendar.set(11, i5);
                            gregorianCalendar.set(12, i4);
                            gregorianCalendar.set(13, i);
                            gregorianCalendar.set(14, i3);
                            parsePosition2.setIndex(i6);
                            return gregorianCalendar.getTime();
                        }
                    }
                    i2 = i12;
                } else {
                    i2 = i9;
                    i5 = 0;
                    i4 = 0;
                }
                i3 = 0;
                i = 0;
                if (str.length() > i2) {
                }
            } else {
                GregorianCalendar gregorianCalendar2 = new GregorianCalendar(parseInt, parseInt2 - 1, parseInt3);
                parsePosition2.setIndex(i9);
                return gregorianCalendar2.getTime();
            }
        } catch (IndexOutOfBoundsException e) {
            exc = e;
            if (str3 == null) {
                str2 = null;
            } else {
                StringBuilder sb6 = new StringBuilder();
                sb6.append('\"');
                sb6.append(str3);
                sb6.append("'");
                str2 = sb6.toString();
            }
            message = exc.getMessage();
            if (message == null || message.isEmpty()) {
                StringBuilder sb7 = new StringBuilder();
                sb7.append("(");
                sb7.append(exc.getClass().getName());
                sb7.append(")");
                message = sb7.toString();
            }
            StringBuilder sb8 = new StringBuilder();
            sb8.append("Failed to parse date [");
            sb8.append(str2);
            sb8.append("]: ");
            sb8.append(message);
            ParseException parseException = new ParseException(sb8.toString(), parsePosition.getIndex());
            parseException.initCause(exc);
            throw parseException;
        } catch (NumberFormatException e2) {
            exc = e2;
            if (str3 == null) {
            }
            message = exc.getMessage();
            StringBuilder sb72 = new StringBuilder();
            sb72.append("(");
            sb72.append(exc.getClass().getName());
            sb72.append(")");
            message = sb72.toString();
            StringBuilder sb82 = new StringBuilder();
            sb82.append("Failed to parse date [");
            sb82.append(str2);
            sb82.append("]: ");
            sb82.append(message);
            ParseException parseException2 = new ParseException(sb82.toString(), parsePosition.getIndex());
            parseException2.initCause(exc);
            throw parseException2;
        } catch (IllegalArgumentException e3) {
            exc = e3;
            if (str3 == null) {
            }
            message = exc.getMessage();
            StringBuilder sb722 = new StringBuilder();
            sb722.append("(");
            sb722.append(exc.getClass().getName());
            sb722.append(")");
            message = sb722.toString();
            StringBuilder sb822 = new StringBuilder();
            sb822.append("Failed to parse date [");
            sb822.append(str2);
            sb822.append("]: ");
            sb822.append(message);
            ParseException parseException22 = new ParseException(sb822.toString(), parsePosition.getIndex());
            parseException22.initCause(exc);
            throw parseException22;
        }
    }

    private static boolean checkOffset(String str, int i, char c) {
        return i < str.length() && str.charAt(i) == c;
    }

    private static int parseInt(String str, int i, int i2) throws NumberFormatException {
        int i3;
        int i4;
        if (i < 0 || i2 > str.length() || i > i2) {
            throw new NumberFormatException(str);
        }
        if (i < i2) {
            i4 = i + 1;
            int digit = Character.digit(str.charAt(i), 10);
            if (digit < 0) {
                StringBuilder sb = new StringBuilder();
                sb.append("Invalid number: ");
                sb.append(str.substring(i, i2));
                throw new NumberFormatException(sb.toString());
            }
            i3 = -digit;
        } else {
            i3 = 0;
            i4 = i;
        }
        while (i4 < i2) {
            int i5 = i4 + 1;
            int digit2 = Character.digit(str.charAt(i4), 10);
            if (digit2 < 0) {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("Invalid number: ");
                sb2.append(str.substring(i, i2));
                throw new NumberFormatException(sb2.toString());
            }
            i3 = (i3 * 10) - digit2;
            i4 = i5;
        }
        return -i3;
    }

    private static int indexOfNonDigit(String str, int i) {
        while (i < str.length()) {
            char charAt = str.charAt(i);
            if (charAt < '0' || charAt > '9') {
                return i;
            }
            i++;
        }
        return str.length();
    }
}
