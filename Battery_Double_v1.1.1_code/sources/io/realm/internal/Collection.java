package io.realm.internal;

import io.realm.OrderedRealmCollectionChangeListener;
import io.realm.RealmChangeListener;
import io.realm.internal.ObservableCollection.Callback;
import io.realm.internal.ObservableCollection.CollectionObserverPair;
import io.realm.internal.ObservableCollection.RealmChangeListenerWrapper;
import java.util.ConcurrentModificationException;
import java.util.Date;
import java.util.NoSuchElementException;
import javax.annotation.Nullable;

public class Collection implements NativeObject, ObservableCollection {
    public static final byte AGGREGATE_FUNCTION_AVERAGE = 3;
    public static final byte AGGREGATE_FUNCTION_MAXIMUM = 2;
    public static final byte AGGREGATE_FUNCTION_MINIMUM = 1;
    public static final byte AGGREGATE_FUNCTION_SUM = 4;
    private static final String CLOSED_REALM_MESSAGE = "This Realm instance has already been closed, making it unusable.";
    public static final byte MODE_EMPTY = 0;
    public static final byte MODE_LINKVIEW = 3;
    public static final byte MODE_QUERY = 2;
    public static final byte MODE_TABLE = 1;
    public static final byte MODE_TABLEVIEW = 4;
    private static final long nativeFinalizerPtr = nativeGetFinalizerPtr();
    private final NativeContext context;
    /* access modifiers changed from: private */
    public boolean isSnapshot;
    private boolean loaded;
    private final long nativePtr;
    private final ObserverPairList<CollectionObserverPair> observerPairs;
    /* access modifiers changed from: private */
    public final SharedRealm sharedRealm;
    private final Table table;

    public enum Aggregate {
        MINIMUM(1),
        MAXIMUM(2),
        AVERAGE(3),
        SUM(4);
        
        private final byte value;

        private Aggregate(byte b) {
            this.value = b;
        }

        public byte getValue() {
            return this.value;
        }
    }

    public static abstract class Iterator<T> implements java.util.Iterator<T> {
        Collection iteratorCollection;
        protected int pos = -1;

        /* access modifiers changed from: protected */
        public abstract T convertRowToObject(UncheckedRow uncheckedRow);

        public Iterator(Collection collection) {
            if (collection.sharedRealm.isClosed()) {
                throw new IllegalStateException(Collection.CLOSED_REALM_MESSAGE);
            }
            this.iteratorCollection = collection;
            if (!collection.isSnapshot) {
                if (collection.sharedRealm.isInTransaction()) {
                    detach();
                } else {
                    this.iteratorCollection.sharedRealm.addIterator(this);
                }
            }
        }

        public boolean hasNext() {
            checkValid();
            return ((long) (this.pos + 1)) < this.iteratorCollection.size();
        }

        @Nullable
        public T next() {
            checkValid();
            this.pos++;
            if (((long) this.pos) < this.iteratorCollection.size()) {
                return get(this.pos);
            }
            StringBuilder sb = new StringBuilder();
            sb.append("Cannot access index ");
            sb.append(this.pos);
            sb.append(" when size is ");
            sb.append(this.iteratorCollection.size());
            sb.append(". Remember to check hasNext() before using next().");
            throw new NoSuchElementException(sb.toString());
        }

        @Deprecated
        public void remove() {
            throw new UnsupportedOperationException("remove() is not supported by RealmResults iterators.");
        }

        /* access modifiers changed from: 0000 */
        public void detach() {
            this.iteratorCollection = this.iteratorCollection.createSnapshot();
        }

        /* access modifiers changed from: 0000 */
        public void invalidate() {
            this.iteratorCollection = null;
        }

        /* access modifiers changed from: 0000 */
        public void checkValid() {
            if (this.iteratorCollection == null) {
                throw new ConcurrentModificationException("No outside changes to a Realm is allowed while iterating a living Realm collection.");
            }
        }

        /* access modifiers changed from: 0000 */
        @Nullable
        public T get(int i) {
            return convertRowToObject(this.iteratorCollection.getUncheckedRow(i));
        }
    }

    public static abstract class ListIterator<T> extends Iterator<T> implements java.util.ListIterator<T> {
        public ListIterator(Collection collection, int i) {
            super(collection);
            if (i < 0 || ((long) i) > this.iteratorCollection.size()) {
                StringBuilder sb = new StringBuilder();
                sb.append("Starting location must be a valid index: [0, ");
                sb.append(this.iteratorCollection.size() - 1);
                sb.append("]. Yours was ");
                sb.append(i);
                throw new IndexOutOfBoundsException(sb.toString());
            }
            this.pos = i - 1;
        }

        @Deprecated
        public void add(@Nullable T t) {
            throw new UnsupportedOperationException("Adding an element is not supported. Use Realm.createObject() instead.");
        }

        public boolean hasPrevious() {
            checkValid();
            return this.pos >= 0;
        }

        public int nextIndex() {
            checkValid();
            return this.pos + 1;
        }

        @Nullable
        public T previous() {
            checkValid();
            try {
                T t = get(this.pos);
                this.pos--;
                return t;
            } catch (IndexOutOfBoundsException unused) {
                StringBuilder sb = new StringBuilder();
                sb.append("Cannot access index less than zero. This was ");
                sb.append(this.pos);
                sb.append(". Remember to check hasPrevious() before using previous().");
                throw new NoSuchElementException(sb.toString());
            }
        }

        public int previousIndex() {
            checkValid();
            return this.pos;
        }

        @Deprecated
        public void set(@Nullable T t) {
            throw new UnsupportedOperationException("Replacing and element is not supported.");
        }
    }

    public enum Mode {
        EMPTY,
        TABLE,
        QUERY,
        LINKVIEW,
        TABLEVIEW;

        static Mode getByValue(byte b) {
            switch (b) {
                case 0:
                    return EMPTY;
                case 1:
                    return TABLE;
                case 2:
                    return QUERY;
                case 3:
                    return LINKVIEW;
                case 4:
                    return TABLEVIEW;
                default:
                    StringBuilder sb = new StringBuilder();
                    sb.append("Invalid value: ");
                    sb.append(b);
                    throw new IllegalArgumentException(sb.toString());
            }
        }
    }

    private static native Object nativeAggregate(long j, long j2, byte b);

    private static native void nativeClear(long j);

    private static native boolean nativeContains(long j, long j2);

    private static native long nativeCreateResults(long j, long j2, @Nullable SortDescriptor sortDescriptor, @Nullable SortDescriptor sortDescriptor2);

    private static native long nativeCreateResultsFromBacklinks(long j, long j2, long j3, long j4);

    private static native long nativeCreateResultsFromList(long j, long j2, @Nullable SortDescriptor sortDescriptor);

    private static native long nativeCreateSnapshot(long j);

    private static native void nativeDelete(long j, long j2);

    private static native boolean nativeDeleteFirst(long j);

    private static native boolean nativeDeleteLast(long j);

    private static native long nativeDistinct(long j, SortDescriptor sortDescriptor);

    private static native long nativeFirstRow(long j);

    private static native long nativeGetFinalizerPtr();

    private static native byte nativeGetMode(long j);

    private static native long nativeGetRow(long j, int i);

    private static native long nativeIndexOf(long j, long j2);

    private static native boolean nativeIsValid(long j);

    private static native long nativeLastRow(long j);

    private static native long nativeSize(long j);

    private static native long nativeSort(long j, SortDescriptor sortDescriptor);

    private native void nativeStartListening(long j);

    private native void nativeStopListening(long j);

    private static native long nativeWhere(long j);

    public static Collection createBacklinksCollection(SharedRealm sharedRealm2, UncheckedRow uncheckedRow, Table table2, String str) {
        Collection collection = new Collection(sharedRealm2, table2, nativeCreateResultsFromBacklinks(sharedRealm2.getNativePtr(), uncheckedRow.getNativePtr(), table2.getNativePtr(), table2.getColumnIndex(str)), true);
        return collection;
    }

    public Collection(SharedRealm sharedRealm2, TableQuery tableQuery, @Nullable SortDescriptor sortDescriptor, @Nullable SortDescriptor sortDescriptor2) {
        this.isSnapshot = false;
        this.observerPairs = new ObserverPairList<>();
        tableQuery.validateQuery();
        this.nativePtr = nativeCreateResults(sharedRealm2.getNativePtr(), tableQuery.getNativePtr(), sortDescriptor, sortDescriptor2);
        this.sharedRealm = sharedRealm2;
        this.context = sharedRealm2.context;
        this.table = tableQuery.getTable();
        this.context.addReference(this);
        this.loaded = false;
    }

    public Collection(SharedRealm sharedRealm2, TableQuery tableQuery, @Nullable SortDescriptor sortDescriptor) {
        this(sharedRealm2, tableQuery, sortDescriptor, (SortDescriptor) null);
    }

    public Collection(SharedRealm sharedRealm2, TableQuery tableQuery) {
        this(sharedRealm2, tableQuery, (SortDescriptor) null, (SortDescriptor) null);
    }

    public Collection(SharedRealm sharedRealm2, OsList osList, @Nullable SortDescriptor sortDescriptor) {
        this.isSnapshot = false;
        this.observerPairs = new ObserverPairList<>();
        this.nativePtr = nativeCreateResultsFromList(sharedRealm2.getNativePtr(), osList.getNativePtr(), sortDescriptor);
        this.sharedRealm = sharedRealm2;
        this.context = sharedRealm2.context;
        this.table = osList.getTargetTable();
        this.context.addReference(this);
        this.loaded = true;
    }

    private Collection(SharedRealm sharedRealm2, Table table2, long j) {
        this(sharedRealm2, table2, j, false);
    }

    Collection(SharedRealm sharedRealm2, Table table2, long j, boolean z) {
        this.isSnapshot = false;
        this.observerPairs = new ObserverPairList<>();
        this.sharedRealm = sharedRealm2;
        this.context = sharedRealm2.context;
        this.table = table2;
        this.nativePtr = j;
        this.context.addReference(this);
        this.loaded = z;
    }

    public Collection createSnapshot() {
        if (this.isSnapshot) {
            return this;
        }
        Collection collection = new Collection(this.sharedRealm, this.table, nativeCreateSnapshot(this.nativePtr));
        collection.isSnapshot = true;
        return collection;
    }

    public long getNativePtr() {
        return this.nativePtr;
    }

    public long getNativeFinalizerPtr() {
        return nativeFinalizerPtr;
    }

    public UncheckedRow getUncheckedRow(int i) {
        return this.table.getUncheckedRowByPointer(nativeGetRow(this.nativePtr, i));
    }

    public UncheckedRow firstUncheckedRow() {
        long nativeFirstRow = nativeFirstRow(this.nativePtr);
        if (nativeFirstRow != 0) {
            return this.table.getUncheckedRowByPointer(nativeFirstRow);
        }
        return null;
    }

    public UncheckedRow lastUncheckedRow() {
        long nativeLastRow = nativeLastRow(this.nativePtr);
        if (nativeLastRow != 0) {
            return this.table.getUncheckedRowByPointer(nativeLastRow);
        }
        return null;
    }

    public Table getTable() {
        return this.table;
    }

    public TableQuery where() {
        return new TableQuery(this.context, this.table, nativeWhere(this.nativePtr));
    }

    public Number aggregateNumber(Aggregate aggregate, long j) {
        return (Number) nativeAggregate(this.nativePtr, j, aggregate.getValue());
    }

    public Date aggregateDate(Aggregate aggregate, long j) {
        return (Date) nativeAggregate(this.nativePtr, j, aggregate.getValue());
    }

    public long size() {
        return nativeSize(this.nativePtr);
    }

    public void clear() {
        nativeClear(this.nativePtr);
    }

    public Collection sort(SortDescriptor sortDescriptor) {
        return new Collection(this.sharedRealm, this.table, nativeSort(this.nativePtr, sortDescriptor));
    }

    public Collection distinct(SortDescriptor sortDescriptor) {
        return new Collection(this.sharedRealm, this.table, nativeDistinct(this.nativePtr, sortDescriptor));
    }

    public boolean contains(UncheckedRow uncheckedRow) {
        return nativeContains(this.nativePtr, uncheckedRow.getNativePtr());
    }

    public int indexOf(UncheckedRow uncheckedRow) {
        long nativeIndexOf = nativeIndexOf(this.nativePtr, uncheckedRow.getNativePtr());
        if (nativeIndexOf > 2147483647L) {
            return Integer.MAX_VALUE;
        }
        return (int) nativeIndexOf;
    }

    public void delete(long j) {
        nativeDelete(this.nativePtr, j);
    }

    public boolean deleteFirst() {
        return nativeDeleteFirst(this.nativePtr);
    }

    public boolean deleteLast() {
        return nativeDeleteLast(this.nativePtr);
    }

    public <T> void addListener(T t, OrderedRealmCollectionChangeListener<T> orderedRealmCollectionChangeListener) {
        if (this.observerPairs.isEmpty()) {
            nativeStartListening(this.nativePtr);
        }
        this.observerPairs.add(new CollectionObserverPair(t, orderedRealmCollectionChangeListener));
    }

    public <T> void addListener(T t, RealmChangeListener<T> realmChangeListener) {
        addListener(t, (OrderedRealmCollectionChangeListener<T>) new RealmChangeListenerWrapper<T>(realmChangeListener));
    }

    public <T> void removeListener(T t, OrderedRealmCollectionChangeListener<T> orderedRealmCollectionChangeListener) {
        this.observerPairs.remove(t, orderedRealmCollectionChangeListener);
        if (this.observerPairs.isEmpty()) {
            nativeStopListening(this.nativePtr);
        }
    }

    public <T> void removeListener(T t, RealmChangeListener<T> realmChangeListener) {
        removeListener(t, (OrderedRealmCollectionChangeListener<T>) new RealmChangeListenerWrapper<T>(realmChangeListener));
    }

    public void removeAllListeners() {
        this.observerPairs.clear();
        nativeStopListening(this.nativePtr);
    }

    public boolean isValid() {
        return nativeIsValid(this.nativePtr);
    }

    public void notifyChangeListeners(long j) {
        if (j != 0 || !isLoaded()) {
            boolean z = this.loaded;
            this.loaded = true;
            this.observerPairs.foreach(new Callback((j == 0 || !z) ? null : new OsCollectionChangeSet(j)));
        }
    }

    public Mode getMode() {
        return Mode.getByValue(nativeGetMode(this.nativePtr));
    }

    public boolean isLoaded() {
        return this.loaded;
    }

    public void load() {
        if (!this.loaded) {
            notifyChangeListeners(0);
        }
    }
}
