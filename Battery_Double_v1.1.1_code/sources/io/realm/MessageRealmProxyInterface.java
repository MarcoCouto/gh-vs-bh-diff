package io.realm;

public interface MessageRealmProxyInterface {
    String realmGet$body();

    String realmGet$date();

    int realmGet$id();

    boolean realmGet$read();

    String realmGet$title();

    String realmGet$type();

    void realmSet$body(String str);

    void realmSet$date(String str);

    void realmSet$id(int i);

    void realmSet$read(boolean z);

    void realmSet$title(String str);

    void realmSet$type(String str);
}
