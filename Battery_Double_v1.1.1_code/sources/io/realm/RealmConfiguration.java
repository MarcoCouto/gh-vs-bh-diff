package io.realm;

import android.content.Context;
import io.realm.Realm.Transaction;
import io.realm.annotations.RealmModule;
import io.realm.exceptions.RealmException;
import io.realm.exceptions.RealmFileException;
import io.realm.exceptions.RealmFileException.Kind;
import io.realm.internal.OsRealmConfig.Durability;
import io.realm.internal.RealmCore;
import io.realm.internal.RealmProxyMediator;
import io.realm.internal.Util;
import io.realm.internal.modules.CompositeMediator;
import io.realm.internal.modules.FilterableMediator;
import io.realm.rx.RealmObservableFactory;
import io.realm.rx.RxObservableFactory;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;
import javax.annotation.Nullable;

public class RealmConfiguration {
    /* access modifiers changed from: private */
    public static final Object DEFAULT_MODULE = Realm.getDefaultModule();
    protected static final RealmProxyMediator DEFAULT_MODULE_MEDIATOR;
    public static final String DEFAULT_REALM_NAME = "default.realm";
    public static final int KEY_LENGTH = 64;
    private static Boolean rxJavaAvailable;
    private final String assetFilePath;
    private final String canonicalPath;
    private final CompactOnLaunchCallback compactOnLaunch;
    private final boolean deleteRealmIfMigrationNeeded;
    private final Durability durability;
    private final Transaction initialDataTransaction;
    private final boolean isRecoveryConfiguration;
    private final byte[] key;
    private final RealmMigration migration;
    private final boolean readOnly;
    private final File realmDirectory;
    private final String realmFileName;
    private final RxObservableFactory rxObservableFactory;
    private final RealmProxyMediator schemaMediator;
    private final long schemaVersion;

    public static class Builder {
        private String assetFilePath;
        private CompactOnLaunchCallback compactOnLaunch;
        private HashSet<Class<? extends RealmModel>> debugSchema;
        private boolean deleteRealmIfMigrationNeeded;
        private File directory;
        private Durability durability;
        private String fileName;
        private Transaction initialDataTransaction;
        private byte[] key;
        private RealmMigration migration;
        private HashSet<Object> modules;
        private boolean readOnly;
        private RxObservableFactory rxFactory;
        private long schemaVersion;

        public Builder() {
            this(BaseRealm.applicationContext);
        }

        Builder(Context context) {
            this.modules = new HashSet<>();
            this.debugSchema = new HashSet<>();
            if (context == null) {
                throw new IllegalStateException("Call `Realm.init(Context)` before creating a RealmConfiguration");
            }
            RealmCore.loadLibrary(context);
            initializeBuilder(context);
        }

        private void initializeBuilder(Context context) {
            this.directory = context.getFilesDir();
            this.fileName = "default.realm";
            this.key = null;
            this.schemaVersion = 0;
            this.migration = null;
            this.deleteRealmIfMigrationNeeded = false;
            this.durability = Durability.FULL;
            this.readOnly = false;
            this.compactOnLaunch = null;
            if (RealmConfiguration.DEFAULT_MODULE != null) {
                this.modules.add(RealmConfiguration.DEFAULT_MODULE);
            }
        }

        public Builder name(String str) {
            if (str == null || str.isEmpty()) {
                throw new IllegalArgumentException("A non-empty filename must be provided");
            }
            this.fileName = str;
            return this;
        }

        public Builder directory(File file) {
            if (file == null) {
                throw new IllegalArgumentException("Non-null 'dir' required.");
            } else if (file.isFile()) {
                StringBuilder sb = new StringBuilder();
                sb.append("'dir' is a file, not a directory: ");
                sb.append(file.getAbsolutePath());
                sb.append(".");
                throw new IllegalArgumentException(sb.toString());
            } else if (!file.exists() && !file.mkdirs()) {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("Could not create the specified directory: ");
                sb2.append(file.getAbsolutePath());
                sb2.append(".");
                throw new IllegalArgumentException(sb2.toString());
            } else if (!file.canWrite()) {
                StringBuilder sb3 = new StringBuilder();
                sb3.append("Realm directory is not writable: ");
                sb3.append(file.getAbsolutePath());
                sb3.append(".");
                throw new IllegalArgumentException(sb3.toString());
            } else {
                this.directory = file;
                return this;
            }
        }

        public Builder encryptionKey(byte[] bArr) {
            if (bArr == null) {
                throw new IllegalArgumentException("A non-null key must be provided");
            } else if (bArr.length != 64) {
                throw new IllegalArgumentException(String.format(Locale.US, "The provided key must be %s bytes. Yours was: %s", new Object[]{Integer.valueOf(64), Integer.valueOf(bArr.length)}));
            } else {
                this.key = Arrays.copyOf(bArr, bArr.length);
                return this;
            }
        }

        public Builder schemaVersion(long j) {
            if (j < 0) {
                StringBuilder sb = new StringBuilder();
                sb.append("Realm schema version numbers must be 0 (zero) or higher. Yours was: ");
                sb.append(j);
                throw new IllegalArgumentException(sb.toString());
            }
            this.schemaVersion = j;
            return this;
        }

        public Builder migration(RealmMigration realmMigration) {
            if (realmMigration == null) {
                throw new IllegalArgumentException("A non-null migration must be provided");
            }
            this.migration = realmMigration;
            return this;
        }

        public Builder deleteRealmIfMigrationNeeded() {
            if (this.assetFilePath == null || this.assetFilePath.length() == 0) {
                this.deleteRealmIfMigrationNeeded = true;
                return this;
            }
            throw new IllegalStateException("Realm cannot clear its schema when previously configured to use an asset file by calling assetFile().");
        }

        public Builder inMemory() {
            if (!Util.isEmptyString(this.assetFilePath)) {
                throw new RealmException("Realm can not use in-memory configuration if asset file is present.");
            }
            this.durability = Durability.MEM_ONLY;
            return this;
        }

        public Builder modules(Object obj, Object... objArr) {
            this.modules.clear();
            addModule(obj);
            if (objArr != null) {
                for (Object addModule : objArr) {
                    addModule(addModule);
                }
            }
            return this;
        }

        public Builder rxFactory(RxObservableFactory rxObservableFactory) {
            this.rxFactory = rxObservableFactory;
            return this;
        }

        public Builder initialData(Transaction transaction) {
            this.initialDataTransaction = transaction;
            return this;
        }

        public Builder assetFile(String str) {
            if (Util.isEmptyString(str)) {
                throw new IllegalArgumentException("A non-empty asset file path must be provided");
            } else if (this.durability == Durability.MEM_ONLY) {
                throw new RealmException("Realm can not use in-memory configuration if asset file is present.");
            } else if (this.deleteRealmIfMigrationNeeded) {
                throw new IllegalStateException("Realm cannot use an asset file when previously configured to clear its schema in migration by calling deleteRealmIfMigrationNeeded().");
            } else {
                this.assetFilePath = str;
                return this;
            }
        }

        public Builder readOnly() {
            this.readOnly = true;
            return this;
        }

        public Builder compactOnLaunch() {
            return compactOnLaunch(new DefaultCompactOnLaunchCallback());
        }

        public Builder compactOnLaunch(CompactOnLaunchCallback compactOnLaunchCallback) {
            if (compactOnLaunchCallback == null) {
                throw new IllegalArgumentException("A non-null compactOnLaunch must be provided");
            }
            this.compactOnLaunch = compactOnLaunchCallback;
            return this;
        }

        private void addModule(Object obj) {
            if (obj != null) {
                checkModule(obj);
                this.modules.add(obj);
            }
        }

        /* access modifiers changed from: 0000 */
        @SafeVarargs
        public final Builder schema(Class<? extends RealmModel> cls, Class<? extends RealmModel>... clsArr) {
            if (cls == null) {
                throw new IllegalArgumentException("A non-null class must be provided");
            }
            this.modules.clear();
            this.modules.add(RealmConfiguration.DEFAULT_MODULE_MEDIATOR);
            this.debugSchema.add(cls);
            if (clsArr != null) {
                Collections.addAll(this.debugSchema, clsArr);
            }
            return this;
        }

        public RealmConfiguration build() {
            if (this.readOnly) {
                if (this.initialDataTransaction != null) {
                    throw new IllegalStateException("This Realm is marked as read-only. Read-only Realms cannot use initialData(Realm.Transaction).");
                } else if (this.assetFilePath == null) {
                    throw new IllegalStateException("Only Realms provided using 'assetFile(path)' can be marked read-only. No such Realm was provided.");
                } else if (this.deleteRealmIfMigrationNeeded) {
                    throw new IllegalStateException("'deleteRealmIfMigrationNeeded()' and read-only Realms cannot be combined");
                } else if (this.compactOnLaunch != null) {
                    throw new IllegalStateException("'compactOnLaunch()' and read-only Realms cannot be combined");
                }
            }
            if (this.rxFactory == null && RealmConfiguration.isRxJavaAvailable()) {
                this.rxFactory = new RealmObservableFactory();
            }
            File file = this.directory;
            String str = this.fileName;
            String canonicalPath = RealmConfiguration.getCanonicalPath(new File(this.directory, this.fileName));
            String str2 = this.assetFilePath;
            byte[] bArr = this.key;
            long j = this.schemaVersion;
            RealmMigration realmMigration = this.migration;
            boolean z = this.deleteRealmIfMigrationNeeded;
            Durability durability2 = this.durability;
            RealmProxyMediator createSchemaMediator = RealmConfiguration.createSchemaMediator(this.modules, this.debugSchema);
            RxObservableFactory rxObservableFactory = this.rxFactory;
            Transaction transaction = this.initialDataTransaction;
            Transaction transaction2 = transaction;
            Transaction transaction3 = transaction2;
            RealmConfiguration realmConfiguration = new RealmConfiguration(file, str, canonicalPath, str2, bArr, j, realmMigration, z, durability2, createSchemaMediator, rxObservableFactory, transaction3, this.readOnly, this.compactOnLaunch, false);
            return realmConfiguration;
        }

        private void checkModule(Object obj) {
            if (!obj.getClass().isAnnotationPresent(RealmModule.class)) {
                StringBuilder sb = new StringBuilder();
                sb.append(obj.getClass().getCanonicalName());
                sb.append(" is not a RealmModule. Add @RealmModule to the class definition.");
                throw new IllegalArgumentException(sb.toString());
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public boolean isSyncConfiguration() {
        return false;
    }

    static {
        if (DEFAULT_MODULE != null) {
            RealmProxyMediator moduleMediator = getModuleMediator(DEFAULT_MODULE.getClass().getCanonicalName());
            if (!moduleMediator.transformerApplied()) {
                throw new ExceptionInInitializerError("RealmTransformer doesn't seem to be applied. Please update the project configuration to use the Realm Gradle plugin. See https://realm.io/news/android-installation-change/");
            }
            DEFAULT_MODULE_MEDIATOR = moduleMediator;
            return;
        }
        DEFAULT_MODULE_MEDIATOR = null;
    }

    protected RealmConfiguration(@Nullable File file, @Nullable String str, String str2, @Nullable String str3, @Nullable byte[] bArr, long j, @Nullable RealmMigration realmMigration, boolean z, Durability durability2, RealmProxyMediator realmProxyMediator, @Nullable RxObservableFactory rxObservableFactory2, @Nullable Transaction transaction, boolean z2, @Nullable CompactOnLaunchCallback compactOnLaunchCallback, boolean z3) {
        this.realmDirectory = file;
        this.realmFileName = str;
        this.canonicalPath = str2;
        this.assetFilePath = str3;
        this.key = bArr;
        this.schemaVersion = j;
        this.migration = realmMigration;
        this.deleteRealmIfMigrationNeeded = z;
        this.durability = durability2;
        this.schemaMediator = realmProxyMediator;
        this.rxObservableFactory = rxObservableFactory2;
        this.initialDataTransaction = transaction;
        this.readOnly = z2;
        this.compactOnLaunch = compactOnLaunchCallback;
        this.isRecoveryConfiguration = z3;
    }

    public File getRealmDirectory() {
        return this.realmDirectory;
    }

    public String getRealmFileName() {
        return this.realmFileName;
    }

    public byte[] getEncryptionKey() {
        if (this.key == null) {
            return null;
        }
        return Arrays.copyOf(this.key, this.key.length);
    }

    public long getSchemaVersion() {
        return this.schemaVersion;
    }

    public RealmMigration getMigration() {
        return this.migration;
    }

    public boolean shouldDeleteRealmIfMigrationNeeded() {
        return this.deleteRealmIfMigrationNeeded;
    }

    public Durability getDurability() {
        return this.durability;
    }

    /* access modifiers changed from: protected */
    public RealmProxyMediator getSchemaMediator() {
        return this.schemaMediator;
    }

    /* access modifiers changed from: 0000 */
    public Transaction getInitialDataTransaction() {
        return this.initialDataTransaction;
    }

    /* access modifiers changed from: 0000 */
    public boolean hasAssetFile() {
        return !Util.isEmptyString(this.assetFilePath);
    }

    /* access modifiers changed from: 0000 */
    public String getAssetFilePath() {
        return this.assetFilePath;
    }

    public CompactOnLaunchCallback getCompactOnLaunchCallback() {
        return this.compactOnLaunch;
    }

    public Set<Class<? extends RealmModel>> getRealmObjectClasses() {
        return this.schemaMediator.getModelClasses();
    }

    public String getPath() {
        return this.canonicalPath;
    }

    /* access modifiers changed from: 0000 */
    public boolean realmExists() {
        return new File(this.canonicalPath).exists();
    }

    public RxObservableFactory getRxFactory() {
        if (this.rxObservableFactory != null) {
            return this.rxObservableFactory;
        }
        throw new UnsupportedOperationException("RxJava seems to be missing from the classpath. Remember to add it as a compile dependency. See https://realm.io/docs/java/latest/#rxjava for more details.");
    }

    public boolean isReadOnly() {
        return this.readOnly;
    }

    public boolean isRecoveryConfiguration() {
        return this.isRecoveryConfiguration;
    }

    public boolean equals(Object obj) {
        boolean z = true;
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        RealmConfiguration realmConfiguration = (RealmConfiguration) obj;
        if (this.schemaVersion != realmConfiguration.schemaVersion || this.deleteRealmIfMigrationNeeded != realmConfiguration.deleteRealmIfMigrationNeeded || this.readOnly != realmConfiguration.readOnly || this.isRecoveryConfiguration != realmConfiguration.isRecoveryConfiguration) {
            return false;
        }
        if (this.realmDirectory == null ? realmConfiguration.realmDirectory != null : !this.realmDirectory.equals(realmConfiguration.realmDirectory)) {
            return false;
        }
        if (this.realmFileName == null ? realmConfiguration.realmFileName != null : !this.realmFileName.equals(realmConfiguration.realmFileName)) {
            return false;
        }
        if (!this.canonicalPath.equals(realmConfiguration.canonicalPath)) {
            return false;
        }
        if (this.assetFilePath == null ? realmConfiguration.assetFilePath != null : !this.assetFilePath.equals(realmConfiguration.assetFilePath)) {
            return false;
        }
        if (!Arrays.equals(this.key, realmConfiguration.key)) {
            return false;
        }
        if (this.migration == null ? realmConfiguration.migration != null : !this.migration.equals(realmConfiguration.migration)) {
            return false;
        }
        if (this.durability != realmConfiguration.durability || !this.schemaMediator.equals(realmConfiguration.schemaMediator)) {
            return false;
        }
        if (this.rxObservableFactory == null ? realmConfiguration.rxObservableFactory != null : !this.rxObservableFactory.equals(realmConfiguration.rxObservableFactory)) {
            return false;
        }
        if (this.initialDataTransaction == null ? realmConfiguration.initialDataTransaction != null : !this.initialDataTransaction.equals(realmConfiguration.initialDataTransaction)) {
            return false;
        }
        if (this.compactOnLaunch != null) {
            z = this.compactOnLaunch.equals(realmConfiguration.compactOnLaunch);
        } else if (realmConfiguration.compactOnLaunch != null) {
            z = false;
        }
        return z;
    }

    public int hashCode() {
        int i = 0;
        int hashCode = (((((((((((((((((((((((((this.realmDirectory != null ? this.realmDirectory.hashCode() : 0) * 31) + (this.realmFileName != null ? this.realmFileName.hashCode() : 0)) * 31) + this.canonicalPath.hashCode()) * 31) + (this.assetFilePath != null ? this.assetFilePath.hashCode() : 0)) * 31) + Arrays.hashCode(this.key)) * 31) + ((int) (this.schemaVersion ^ (this.schemaVersion >>> 32)))) * 31) + (this.migration != null ? this.migration.hashCode() : 0)) * 31) + (this.deleteRealmIfMigrationNeeded ? 1 : 0)) * 31) + this.durability.hashCode()) * 31) + this.schemaMediator.hashCode()) * 31) + (this.rxObservableFactory != null ? this.rxObservableFactory.hashCode() : 0)) * 31) + (this.initialDataTransaction != null ? this.initialDataTransaction.hashCode() : 0)) * 31) + (this.readOnly ? 1 : 0)) * 31;
        if (this.compactOnLaunch != null) {
            i = this.compactOnLaunch.hashCode();
        }
        return (31 * (hashCode + i)) + (this.isRecoveryConfiguration ? 1 : 0);
    }

    protected static RealmProxyMediator createSchemaMediator(Set<Object> set, Set<Class<? extends RealmModel>> set2) {
        if (set2.size() > 0) {
            return new FilterableMediator(DEFAULT_MODULE_MEDIATOR, set2);
        }
        if (set.size() == 1) {
            return getModuleMediator(set.iterator().next().getClass().getCanonicalName());
        }
        RealmProxyMediator[] realmProxyMediatorArr = new RealmProxyMediator[set.size()];
        int i = 0;
        for (Object obj : set) {
            realmProxyMediatorArr[i] = getModuleMediator(obj.getClass().getCanonicalName());
            i++;
        }
        return new CompositeMediator(realmProxyMediatorArr);
    }

    private static RealmProxyMediator getModuleMediator(String str) {
        String[] split = str.split("\\.");
        String format = String.format(Locale.US, "io.realm.%s%s", new Object[]{split[split.length - 1], "Mediator"});
        try {
            Constructor constructor = Class.forName(format).getDeclaredConstructors()[0];
            constructor.setAccessible(true);
            return (RealmProxyMediator) constructor.newInstance(new Object[0]);
        } catch (ClassNotFoundException e) {
            StringBuilder sb = new StringBuilder();
            sb.append("Could not find ");
            sb.append(format);
            throw new RealmException(sb.toString(), e);
        } catch (InvocationTargetException e2) {
            StringBuilder sb2 = new StringBuilder();
            sb2.append("Could not create an instance of ");
            sb2.append(format);
            throw new RealmException(sb2.toString(), e2);
        } catch (InstantiationException e3) {
            StringBuilder sb3 = new StringBuilder();
            sb3.append("Could not create an instance of ");
            sb3.append(format);
            throw new RealmException(sb3.toString(), e3);
        } catch (IllegalAccessException e4) {
            StringBuilder sb4 = new StringBuilder();
            sb4.append("Could not create an instance of ");
            sb4.append(format);
            throw new RealmException(sb4.toString(), e4);
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("realmDirectory: ");
        sb.append(this.realmDirectory != null ? this.realmDirectory.toString() : "");
        sb.append("\n");
        sb.append("realmFileName : ");
        sb.append(this.realmFileName);
        sb.append("\n");
        sb.append("canonicalPath: ");
        sb.append(this.canonicalPath);
        sb.append("\n");
        sb.append("key: ");
        sb.append("[length: ");
        sb.append(this.key == null ? 0 : 64);
        sb.append("]");
        sb.append("\n");
        sb.append("schemaVersion: ");
        sb.append(Long.toString(this.schemaVersion));
        sb.append("\n");
        sb.append("migration: ");
        sb.append(this.migration);
        sb.append("\n");
        sb.append("deleteRealmIfMigrationNeeded: ");
        sb.append(this.deleteRealmIfMigrationNeeded);
        sb.append("\n");
        sb.append("durability: ");
        sb.append(this.durability);
        sb.append("\n");
        sb.append("schemaMediator: ");
        sb.append(this.schemaMediator);
        sb.append("\n");
        sb.append("readOnly: ");
        sb.append(this.readOnly);
        sb.append("\n");
        sb.append("compactOnLaunch: ");
        sb.append(this.compactOnLaunch);
        return sb.toString();
    }

    static synchronized boolean isRxJavaAvailable() {
        boolean booleanValue;
        synchronized (RealmConfiguration.class) {
            if (rxJavaAvailable == null) {
                try {
                    Class.forName("io.reactivex.Flowable");
                    rxJavaAvailable = Boolean.valueOf(true);
                } catch (ClassNotFoundException unused) {
                    rxJavaAvailable = Boolean.valueOf(false);
                }
            }
            booleanValue = rxJavaAvailable.booleanValue();
        }
        return booleanValue;
    }

    protected static String getCanonicalPath(File file) {
        try {
            return file.getCanonicalPath();
        } catch (IOException e) {
            Kind kind = Kind.ACCESS_ERROR;
            StringBuilder sb = new StringBuilder();
            sb.append("Could not resolve the canonical path to the Realm file: ");
            sb.append(file.getAbsolutePath());
            throw new RealmFileException(kind, sb.toString(), e);
        }
    }
}
