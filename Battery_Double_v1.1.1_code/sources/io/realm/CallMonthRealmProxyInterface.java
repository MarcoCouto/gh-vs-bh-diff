package io.realm;

public interface CallMonthRealmProxyInterface {
    long realmGet$totalCallInDur();

    int realmGet$totalCallInNum();

    long realmGet$totalCallOutDur();

    int realmGet$totalCallOutNum();

    int realmGet$totalMissedCallNum();

    void realmSet$totalCallInDur(long j);

    void realmSet$totalCallInNum(int i);

    void realmSet$totalCallOutDur(long j);

    void realmSet$totalCallOutNum(int i);

    void realmSet$totalMissedCallNum(int i);
}
