package com.mansoon.BatteryDouble.ui;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import com.mansoon.BatteryDouble.R;
import com.mansoon.BatteryDouble.fragments.TosFragment;
import com.mansoon.BatteryDouble.fragments.WelcomeFragment.WelcomeFragmentContainer;
import com.mansoon.BatteryDouble.util.LogUtils;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class WelcomeActivity extends AppCompatActivity implements WelcomeFragmentContainer {
    private static final String TAG = LogUtils.makeLogTag(WelcomeActivity.class);
    WelcomeActivityContent mContentFragment;

    public interface WelcomeActivityContent {
        boolean shouldDisplay(Context context);
    }

    /* access modifiers changed from: protected */
    public void onCreate(@Nullable Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_welcome);
        this.mContentFragment = getCurrentFragment(this);
        if (this.mContentFragment == null) {
            finish();
        }
        FragmentTransaction beginTransaction = getFragmentManager().beginTransaction();
        beginTransaction.add(R.id.welcome_content, (Fragment) this.mContentFragment);
        beginTransaction.commit();
        LogUtils.LOGD(TAG, "Inside Create View.");
    }

    private static WelcomeActivityContent getCurrentFragment(Context context) {
        for (WelcomeActivityContent welcomeActivityContent : getWelcomeFragments()) {
            if (welcomeActivityContent.shouldDisplay(context)) {
                return welcomeActivityContent;
            }
        }
        return null;
    }

    public static boolean shouldDisplay(Context context) {
        return getCurrentFragment(context) != null;
    }

    private static List<WelcomeActivityContent> getWelcomeFragments() {
        return new ArrayList(Collections.singletonList(new TosFragment()));
    }

    public Button getPositiveButton() {
        return (Button) findViewById(R.id.button_accept);
    }

    public void setPositiveButtonEnabled(Boolean bool) {
        try {
            getPositiveButton().setEnabled(bool.booleanValue());
        } catch (NullPointerException unused) {
            LogUtils.LOGD(TAG, "Positive welcome button doesn't exist to set enabled.");
        }
    }

    public Button getNegativeButton() {
        return (Button) findViewById(R.id.button_decline);
    }

    public void setNegativeButtonEnabled(Boolean bool) {
        try {
            getNegativeButton().setEnabled(bool.booleanValue());
        } catch (NullPointerException unused) {
            LogUtils.LOGD(TAG, "Negative welcome button doesn't exist to set enabled.");
        }
    }
}
