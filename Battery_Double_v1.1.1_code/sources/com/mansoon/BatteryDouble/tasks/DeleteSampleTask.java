package com.mansoon.BatteryDouble.tasks;

import android.os.AsyncTask;
import com.mansoon.BatteryDouble.models.data.Sample;
import io.realm.Realm;
import io.realm.Realm.Transaction;

public class DeleteSampleTask extends AsyncTask<Integer, Void, Void> {
    private static final String TAG = "DeleteSampleTask";

    /* JADX INFO: finally extract failed */
    /* access modifiers changed from: protected */
    public Void doInBackground(Integer... numArr) {
        Realm defaultInstance = Realm.getDefaultInstance();
        try {
            final int intValue = numArr[0].intValue();
            defaultInstance.executeTransaction(new Transaction() {
                public void execute(Realm realm) {
                    Sample sample = (Sample) realm.where(Sample.class).equalTo("id", Integer.valueOf(intValue)).findFirst();
                    if (sample != null) {
                        sample.deleteFromRealm();
                    }
                }
            });
            defaultInstance.close();
            return null;
        } catch (Throwable th) {
            defaultInstance.close();
            throw th;
        }
    }
}
