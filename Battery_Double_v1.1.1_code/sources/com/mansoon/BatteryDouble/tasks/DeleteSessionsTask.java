package com.mansoon.BatteryDouble.tasks;

import android.os.AsyncTask;
import com.google.android.gms.measurement.AppMeasurement.Param;
import com.mansoon.BatteryDouble.models.data.BatterySession;
import com.mansoon.BatteryDouble.util.DateUtils;
import io.realm.Realm;
import io.realm.Realm.Transaction;
import io.realm.RealmResults;

public class DeleteSessionsTask extends AsyncTask<Integer, Void, Boolean> {
    private static final String TAG = "DeleteSessionsTask";
    /* access modifiers changed from: private */
    public boolean mResponse;

    /* JADX INFO: finally extract failed */
    /* access modifiers changed from: protected */
    public Boolean doInBackground(Integer... numArr) {
        this.mResponse = false;
        Realm defaultInstance = Realm.getDefaultInstance();
        try {
            final int intValue = numArr[0].intValue();
            defaultInstance.executeTransaction(new Transaction() {
                public void execute(Realm realm) {
                    RealmResults findAll = realm.where(BatterySession.class).lessThan(Param.TIMESTAMP, DateUtils.getMilliSecondsInterval(intValue)).findAll();
                    if (findAll != null && !findAll.isEmpty()) {
                        DeleteSessionsTask.this.mResponse = findAll.deleteAllFromRealm();
                    }
                }
            });
            defaultInstance.close();
            return Boolean.valueOf(this.mResponse);
        } catch (Throwable th) {
            defaultInstance.close();
            throw th;
        }
    }
}
