package com.mansoon.BatteryDouble.managers;

import android.annotation.TargetApi;
import android.app.ActivityManager;
import android.app.AppOpsManager;
import android.app.usage.UsageStats;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ServiceInfo;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.os.Process;
import android.support.annotation.NonNull;
import com.github.mikephil.charting.utils.Utils;
import com.jaredrummler.android.processes.AndroidProcesses;
import com.jaredrummler.android.processes.models.AndroidAppProcess;
import com.jaredrummler.android.processes.models.Statm;
import com.mansoon.BatteryDouble.managers.sampling.UStats;
import com.mansoon.BatteryDouble.models.ui.Task;
import com.mansoon.BatteryDouble.util.SettingsUtils;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

public class TaskController {
    private Context mContext;
    private PackageManager mPackageManager;

    public TaskController(@NonNull Context context) {
        this.mPackageManager = context.getPackageManager();
        this.mContext = context;
    }

    public List<Task> getRunningTasks() {
        if (VERSION.SDK_INT < 24) {
            return getRunningTasksStandard();
        }
        return getRunningTasksNougat();
    }

    public void killApp(Task task) {
        ((ActivityManager) this.mContext.getSystemService("activity")).killBackgroundProcesses(task.getPackageInfo().packageName);
        Iterator it = task.getProcesses().iterator();
        while (it.hasNext()) {
            Process.killProcess(((Integer) it.next()).intValue());
        }
    }

    public Drawable iconForApp(PackageInfo packageInfo) {
        try {
            return this.mPackageManager.getApplicationIcon(packageInfo.packageName);
        } catch (NameNotFoundException unused) {
            return packageInfo.applicationInfo.loadIcon(this.mPackageManager);
        }
    }

    private List<Task> getRunningTasksStandard() {
        ArrayList arrayList = new ArrayList();
        List<AndroidAppProcess> runningAppProcesses = AndroidProcesses.getRunningAppProcesses();
        if (runningAppProcesses == null) {
            return arrayList;
        }
        for (AndroidAppProcess androidAppProcess : runningAppProcesses) {
            if (!androidAppProcess.name.equals("com.mansoon.BatteryDouble")) {
                PackageInfo packageInfo = getPackageInfo(androidAppProcess, 0);
                if (packageInfo != null && ((!isSystemApp(packageInfo) || !SettingsUtils.isSystemAppsHidden(this.mContext)) && packageInfo.applicationInfo != null)) {
                    String charSequence = packageInfo.applicationInfo.loadLabel(this.mPackageManager).toString();
                    if (!charSequence.isEmpty()) {
                        Task taskByUid = getTaskByUid(arrayList, androidAppProcess.uid);
                        if (taskByUid == null) {
                            Task task = new Task(androidAppProcess.uid, androidAppProcess.name);
                            task.setPackageInfo(packageInfo);
                            task.setLabel(charSequence);
                            task.setMemory(getMemoryFromProcess(androidAppProcess));
                            task.setIsAutoStart(isAutoStartApp(androidAppProcess.getPackageName()));
                            task.setHasBackgroundService(hasBackgroundServices(androidAppProcess.getPackageName()));
                            task.getProcesses().add(Integer.valueOf(androidAppProcess.pid));
                            arrayList.add(task);
                        } else {
                            taskByUid.getProcesses().add(Integer.valueOf(androidAppProcess.pid));
                            taskByUid.setMemory(taskByUid.getMemory() + getMemoryFromProcess(androidAppProcess));
                        }
                    }
                }
            }
        }
        if (!arrayList.isEmpty()) {
            Collections.sort(arrayList, new Comparator<Task>() {
                public int compare(Task task, Task task2) {
                    return task.getLabel().compareTo(task2.getLabel());
                }
            });
        }
        return arrayList;
    }

    @TargetApi(21)
    private List<Task> getRunningTasksNougat() {
        ArrayList arrayList = new ArrayList();
        if (!(((AppOpsManager) this.mContext.getSystemService("appops")).checkOpNoThrow("android:get_usage_stats", Process.myUid(), this.mContext.getPackageName()) == 0)) {
            return arrayList;
        }
        for (UsageStats packageName : UStats.getUsageStatsList(this.mContext)) {
            String packageName2 = packageName.getPackageName();
            if (!packageName2.equals("com.mansoon.BatteryDouble")) {
                PackageInfo packageInfo = getPackageInfo(packageName2, 0);
                if (packageInfo != null && (!isSystemApp(packageInfo) || !SettingsUtils.isSystemAppsHidden(this.mContext))) {
                    String charSequence = packageInfo.applicationInfo.loadLabel(this.mPackageManager).toString();
                    if (!charSequence.isEmpty()) {
                        int i = packageInfo.applicationInfo.uid;
                        if (getTaskByUid(arrayList, i) == null) {
                            Task task = new Task(i, packageInfo.applicationInfo.processName);
                            task.setPackageInfo(packageInfo);
                            task.setLabel(charSequence);
                            task.setIsAutoStart(isAutoStartApp(packageName2));
                            task.setHasBackgroundService(hasBackgroundServices(packageName2));
                            arrayList.add(task);
                        }
                    }
                }
            }
        }
        Collections.sort(arrayList, new Comparator<Task>() {
            public int compare(Task task, Task task2) {
                return task.getLabel().compareTo(task2.getLabel());
            }
        });
        return arrayList;
    }

    private double getMemoryFromProcess(AndroidAppProcess androidAppProcess) {
        double d = Utils.DOUBLE_EPSILON;
        try {
            Statm statm = androidAppProcess.statm();
            if (statm != null) {
                d = (((double) statm.getResidentSetSize()) / 1024.0d) / 1024.0d;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return ((double) Math.round(d * 100.0d)) / 100.0d;
    }

    private boolean isSystemApp(PackageInfo packageInfo) {
        return (packageInfo == null || (packageInfo.applicationInfo.flags & 1) == 0) ? false : true;
    }

    private boolean isAutoStartApp(String str) {
        try {
            PackageInfo packageInfo = this.mPackageManager.getPackageInfo(str, 4096);
            if (packageInfo.requestedPermissions != null) {
                for (String equals : packageInfo.requestedPermissions) {
                    if (equals.equals("android.permission.RECEIVE_BOOT_COMPLETED")) {
                        return true;
                    }
                }
            }
        } catch (NameNotFoundException e) {
            e.printStackTrace();
        }
        return false;
    }

    private boolean hasBackgroundServices(String str) {
        try {
            PackageInfo packageInfo = this.mPackageManager.getPackageInfo(str, 4);
            if (packageInfo.services != null) {
                for (ServiceInfo serviceInfo : packageInfo.services) {
                    if (serviceInfo.exported) {
                        return true;
                    }
                }
            }
        } catch (NameNotFoundException e) {
            e.printStackTrace();
        }
        return false;
    }

    private PackageInfo getPackageInfo(AndroidAppProcess androidAppProcess, int i) {
        try {
            return androidAppProcess.getPackageInfo(this.mContext, i);
        } catch (NameNotFoundException unused) {
            return null;
        }
    }

    @TargetApi(21)
    private PackageInfo getPackageInfo(String str, int i) {
        try {
            return this.mPackageManager.getPackageInfo(str, i);
        } catch (NameNotFoundException unused) {
            return null;
        }
    }

    private Task getTaskByUid(List<Task> list, int i) {
        for (Task task : list) {
            if (task.getUid() == i) {
                return task;
            }
        }
        return null;
    }
}
