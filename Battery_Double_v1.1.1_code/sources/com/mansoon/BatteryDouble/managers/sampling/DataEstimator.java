package com.mansoon.BatteryDouble.managers.sampling;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ReceiverCallNotAllowedException;
import android.support.v4.content.WakefulBroadcastReceiver;
import com.facebook.internal.AnalyticsEvents;
import com.google.firebase.analytics.FirebaseAnalytics.Param;
import com.mansoon.BatteryDouble.R;
import com.mansoon.BatteryDouble.events.BatteryLevelEvent;
import com.mansoon.BatteryDouble.util.LogUtils;
import com.mansoon.BatteryDouble.util.Notifier;
import com.mansoon.BatteryDouble.util.SettingsUtils;
import java.util.Calendar;
import org.greenrobot.eventbus.EventBus;

public class DataEstimator extends WakefulBroadcastReceiver {
    private static final String TAG = LogUtils.makeLogTag(DataEstimator.class);
    private long lastNotify;
    private int level;
    private int mHealth;
    private int plugged;
    private boolean present;
    private int scale;
    private int status;
    private String technology;
    private float temperature;
    private float voltage;

    public void onReceive(Context context, Intent intent) {
        if (context == null) {
            LogUtils.LOGE(TAG, "Error, context is null");
        } else if (intent == null) {
            LogUtils.LOGE(TAG, "Data Estimator error, received intent is null");
        } else {
            String str = TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("onReceive action => ");
            sb.append(intent.getAction());
            LogUtils.LOGI(str, sb.toString());
            if (intent.getAction().equals("android.intent.action.BATTERY_CHANGED")) {
                try {
                    this.level = intent.getIntExtra(Param.LEVEL, -1);
                    this.scale = intent.getIntExtra("scale", -1);
                    this.mHealth = intent.getIntExtra("health", 0);
                    this.plugged = intent.getIntExtra("plugged", 0);
                    this.present = intent.getExtras().getBoolean("present");
                    this.status = intent.getIntExtra("status", 0);
                    this.technology = intent.getExtras().getString("technology");
                    this.temperature = ((float) intent.getIntExtra("temperature", 0)) / 10.0f;
                    this.voltage = ((float) intent.getIntExtra("voltage", 0)) / 1000.0f;
                } catch (RuntimeException e) {
                    e.printStackTrace();
                }
                if (this.temperature > ((float) SettingsUtils.fetchTemperatureWarning(context)) && SettingsUtils.isBatteryAlertsOn(context) && SettingsUtils.isTemperatureAlertsOn(context)) {
                    Calendar instance = Calendar.getInstance();
                    long fetchLastTemperatureAlertDate = SettingsUtils.fetchLastTemperatureAlertDate(context);
                    if (fetchLastTemperatureAlertDate != 0) {
                        instance.setTimeInMillis(fetchLastTemperatureAlertDate);
                    }
                    instance.add(12, SettingsUtils.fetchTemperatureAlertsRate(context));
                    if (fetchLastTemperatureAlertDate == 0 || Calendar.getInstance().after(instance)) {
                        if (this.temperature > ((float) SettingsUtils.fetchTemperatureHigh(context))) {
                            Notifier.batteryHighTemperature(context);
                            SettingsUtils.saveLastTemperatureAlertDate(context, System.currentTimeMillis());
                        } else if (this.temperature <= ((float) SettingsUtils.fetchTemperatureHigh(context)) && this.temperature > ((float) SettingsUtils.fetchTemperatureWarning(context))) {
                            Notifier.batteryWarningTemperature(context);
                            SettingsUtils.saveLastTemperatureAlertDate(context, System.currentTimeMillis());
                        }
                    }
                }
            }
            if (this.scale == 0) {
                this.scale = 100;
            }
            if (this.level > 0) {
                Inspector.setCurrentBatteryLevel((double) this.level, (double) this.scale);
                Intent intent2 = new Intent(context, DataEstimatorService.class);
                intent2.putExtra("OriginalAction", intent.getAction());
                intent2.fillIn(intent, 0);
                if (SettingsUtils.isPowerIndicatorShown(context)) {
                    LogUtils.LOGI(TAG, "Updating notification status bar");
                    Notifier.updateStatusBar(context);
                }
                EventBus.getDefault().post(new BatteryLevelEvent(this.level));
                startWakefulService(context, intent2);
            }
        }
    }

    public static Intent getBatteryChangedIntent(Context context) {
        return context.registerReceiver(null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
    }

    public void getCurrentStatus(Context context) {
        try {
            Intent registerReceiver = context.registerReceiver(null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
            if (registerReceiver != null) {
                this.level = registerReceiver.getIntExtra(Param.LEVEL, -1);
                this.scale = registerReceiver.getIntExtra("scale", -1);
                this.mHealth = registerReceiver.getIntExtra("health", 0);
                this.plugged = registerReceiver.getIntExtra("plugged", 0);
                this.present = registerReceiver.getExtras().getBoolean("present");
                this.status = registerReceiver.getIntExtra("status", 0);
                this.technology = registerReceiver.getExtras().getString("technology");
                this.temperature = (float) (registerReceiver.getIntExtra("temperature", 0) / 10);
                this.voltage = (float) (registerReceiver.getIntExtra("voltage", 0) / 1000);
            }
        } catch (ReceiverCallNotAllowedException e) {
            LogUtils.LOGE(TAG, "ReceiverCallNotAllowedException from Notification Receiver?");
            e.printStackTrace();
        }
    }

    public String getHealthStatus() {
        String str = "";
        switch (this.mHealth) {
            case 1:
                return AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN;
            case 2:
                return "Good";
            case 3:
                return "Overheat";
            case 4:
                return "Dead";
            case 5:
                return "Over Voltage";
            case 6:
                return "Unspecified Failure";
            default:
                return str;
        }
    }

    public String getHealthStatus(Context context) {
        String str = "";
        switch (this.mHealth) {
            case 1:
                return context.getString(R.string.battery_health_unknown);
            case 2:
                return context.getString(R.string.battery_health_good);
            case 3:
                return context.getString(R.string.battery_health_overheat);
            case 4:
                return context.getString(R.string.battery_health_dead);
            case 5:
                return context.getString(R.string.battery_health_over_voltage);
            case 6:
                return context.getString(R.string.battery_health_failure);
            default:
                return str;
        }
    }

    public long getLastNotify() {
        return this.lastNotify;
    }

    public void setLastNotify(long j) {
        this.lastNotify = j;
    }

    public int getHealth() {
        return this.mHealth;
    }

    public int getLevel() {
        return this.level;
    }

    public int getPlugged() {
        return this.plugged;
    }

    public boolean isPresent() {
        return this.present;
    }

    public int getScale() {
        return this.scale;
    }

    public int getStatus() {
        return this.status;
    }

    public String getTechnology() {
        return this.technology;
    }

    public float getTemperature() {
        return this.temperature;
    }

    public float getVoltage() {
        return this.voltage;
    }
}
