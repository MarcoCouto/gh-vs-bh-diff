package com.mansoon.BatteryDouble.util;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import com.facebook.internal.AnalyticsEvents;

public class GreenHubHelper {
    private static final String TAG = LogUtils.makeLogTag(GreenHubHelper.class);

    public static Drawable iconForApp(Context context, String str) {
        try {
            return context.getPackageManager().getApplicationIcon(str);
        } catch (NameNotFoundException e) {
            e.printStackTrace();
            return ContextCompat.getDrawable(context, 17301651);
        }
    }

    public static String labelForApp(Context context, String str) {
        if (str == null) {
            return AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN;
        }
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(str, 0);
            return applicationInfo != null ? context.getPackageManager().getApplicationLabel(applicationInfo).toString() : str;
        } catch (NameNotFoundException unused) {
            return str;
        }
    }
}
