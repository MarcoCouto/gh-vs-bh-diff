package com.mansoon.BatteryDouble.events;

import com.mansoon.BatteryDouble.models.ui.Task;

public class TaskRemovedEvent {
    public final int position;
    public final Task task;

    public TaskRemovedEvent(int i, Task task2) {
        this.position = i;
        this.task = task2;
    }
}
