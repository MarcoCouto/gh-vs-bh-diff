package com.mansoon.BatteryDouble.models;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.content.Context;
import android.content.SharedPreferences.Editor;
import com.mansoon.BatteryDouble.Config;
import com.mansoon.BatteryDouble.models.data.AppSignature;
import com.mansoon.BatteryDouble.models.data.ProcessInfo;
import com.mansoon.BatteryDouble.util.LogUtils;
import com.mansoon.BatteryDouble.util.StringHelper;
import java.lang.ref.WeakReference;
import java.util.List;

public class Process {
    private static final String TAG = LogUtils.makeLogTag(Process.class);
    private static WeakReference<List<RunningAppProcessInfo>> runningAppInfo;

    public static List<RunningAppProcessInfo> getRunningProcessInfo(Context context) {
        if (runningAppInfo != null && runningAppInfo.get() != null) {
            return (List) runningAppInfo.get();
        }
        List<RunningAppProcessInfo> runningAppProcesses = ((ActivityManager) context.getSystemService("activity")).getRunningAppProcesses();
        for (RunningAppProcessInfo runningAppProcessInfo : runningAppProcesses) {
            if (!(runningAppProcessInfo == null || runningAppProcessInfo.processName == null)) {
                runningAppProcessInfo.processName = StringHelper.formatProcessName(runningAppProcessInfo.processName);
            }
        }
        runningAppInfo = new WeakReference<>(runningAppProcesses);
        return runningAppProcesses;
    }

    public static ProcessInfo uninstalledItem(String str, String str2, Editor editor) {
        ProcessInfo processInfo = new ProcessInfo();
        processInfo.realmSet$name(str);
        processInfo.realmGet$appSignatures().add(new AppSignature(Config.IMPORTANCE_UNINSTALLED));
        processInfo.realmSet$processId(-1);
        processInfo.realmSet$importance(Config.IMPORTANCE_UNINSTALLED);
        editor.remove(str2);
        return processInfo;
    }

    public static ProcessInfo disabledItem(String str, String str2, Editor editor) {
        ProcessInfo processInfo = new ProcessInfo();
        processInfo.realmSet$name(str);
        processInfo.realmSet$processId(-1);
        processInfo.realmSet$importance(Config.IMPORTANCE_DISABLED);
        editor.remove(str2);
        return processInfo;
    }

    public static void clear() {
        runningAppInfo = null;
    }
}
