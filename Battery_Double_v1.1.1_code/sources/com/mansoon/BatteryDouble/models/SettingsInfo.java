package com.mansoon.BatteryDouble.models;

import android.content.ContentResolver;
import android.content.Context;
import android.nfc.NfcAdapter;
import android.os.Build.VERSION;
import android.os.PowerManager;
import android.provider.Settings.Secure;
import android.provider.Settings.SettingNotFoundException;
import java.util.Calendar;

public class SettingsInfo {
    private static final String TAG = "SettingsInfo";

    public static String getTimeZone() {
        return Calendar.getInstance().getTimeZone().getID();
    }

    public static int allowUnknownSources(Context context) {
        return Secure.getInt(context.getContentResolver(), "install_non_market_apps", 0);
    }

    public static int isDeveloperModeOn(Context context) {
        ContentResolver contentResolver = context.getContentResolver();
        if (VERSION.SDK_INT >= 17) {
            try {
                return Secure.getInt(contentResolver, "adb_enabled");
            } catch (SettingNotFoundException e) {
                e.printStackTrace();
            }
        }
        return Secure.getInt(contentResolver, "adb_enabled", 0);
    }

    public static boolean isNfcEnabled(Context context) {
        NfcAdapter defaultAdapter = NfcAdapter.getDefaultAdapter(context);
        return defaultAdapter != null && defaultAdapter.isEnabled();
    }

    public static boolean isPowerSaveEnabled(Context context) {
        return VERSION.SDK_INT >= 21 && ((PowerManager) context.getSystemService("power")).isPowerSaveMode();
    }
}
