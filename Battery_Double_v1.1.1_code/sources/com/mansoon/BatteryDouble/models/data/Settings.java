package com.mansoon.BatteryDouble.models.data;

import io.realm.RealmObject;
import io.realm.SettingsRealmProxyInterface;
import io.realm.internal.RealmObjectProxy;

public class Settings extends RealmObject implements SettingsRealmProxyInterface {
    public boolean bluetoothEnabled;
    public int developerMode;
    public boolean flashlightEnabled;
    public boolean locationEnabled;
    public boolean nfcEnabled;
    public boolean powersaverEnabled;
    public int unknownSources;

    public boolean realmGet$bluetoothEnabled() {
        return this.bluetoothEnabled;
    }

    public int realmGet$developerMode() {
        return this.developerMode;
    }

    public boolean realmGet$flashlightEnabled() {
        return this.flashlightEnabled;
    }

    public boolean realmGet$locationEnabled() {
        return this.locationEnabled;
    }

    public boolean realmGet$nfcEnabled() {
        return this.nfcEnabled;
    }

    public boolean realmGet$powersaverEnabled() {
        return this.powersaverEnabled;
    }

    public int realmGet$unknownSources() {
        return this.unknownSources;
    }

    public void realmSet$bluetoothEnabled(boolean z) {
        this.bluetoothEnabled = z;
    }

    public void realmSet$developerMode(int i) {
        this.developerMode = i;
    }

    public void realmSet$flashlightEnabled(boolean z) {
        this.flashlightEnabled = z;
    }

    public void realmSet$locationEnabled(boolean z) {
        this.locationEnabled = z;
    }

    public void realmSet$nfcEnabled(boolean z) {
        this.nfcEnabled = z;
    }

    public void realmSet$powersaverEnabled(boolean z) {
        this.powersaverEnabled = z;
    }

    public void realmSet$unknownSources(int i) {
        this.unknownSources = i;
    }

    public Settings() {
        if (this instanceof RealmObjectProxy) {
            ((RealmObjectProxy) this).realm$injectObjectContext();
        }
    }
}
