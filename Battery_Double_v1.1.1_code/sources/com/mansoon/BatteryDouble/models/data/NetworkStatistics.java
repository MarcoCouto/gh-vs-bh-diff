package com.mansoon.BatteryDouble.models.data;

import io.realm.NetworkStatisticsRealmProxyInterface;
import io.realm.RealmObject;
import io.realm.internal.RealmObjectProxy;

public class NetworkStatistics extends RealmObject implements NetworkStatisticsRealmProxyInterface {
    public double mobileReceived;
    public double mobileSent;
    public double wifiReceived;
    public double wifiSent;

    public double realmGet$mobileReceived() {
        return this.mobileReceived;
    }

    public double realmGet$mobileSent() {
        return this.mobileSent;
    }

    public double realmGet$wifiReceived() {
        return this.wifiReceived;
    }

    public double realmGet$wifiSent() {
        return this.wifiSent;
    }

    public void realmSet$mobileReceived(double d) {
        this.mobileReceived = d;
    }

    public void realmSet$mobileSent(double d) {
        this.mobileSent = d;
    }

    public void realmSet$wifiReceived(double d) {
        this.wifiReceived = d;
    }

    public void realmSet$wifiSent(double d) {
        this.wifiSent = d;
    }

    public NetworkStatistics() {
        if (this instanceof RealmObjectProxy) {
            ((RealmObjectProxy) this).realm$injectObjectContext();
        }
    }
}
