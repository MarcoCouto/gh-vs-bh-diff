package com.mansoon.BatteryDouble.models;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.os.Build.VERSION;
import com.mansoon.BatteryDouble.Config;
import com.mansoon.BatteryDouble.util.LogUtils;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.lang.reflect.Method;

public class Battery {
    private static final String TAG = LogUtils.makeLogTag(Battery.class);

    public static double getBatteryVoltage(Context context) {
        Intent registerReceiver = context.registerReceiver(null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
        if (registerReceiver == null) {
            return -1.0d;
        }
        double intExtra = (double) registerReceiver.getIntExtra("voltage", 0);
        if (intExtra != -1.0d) {
            intExtra /= 1000.0d;
        }
        return intExtra;
    }

    public static int getBatteryCapacity(Context context) {
        if (VERSION.SDK_INT >= 21) {
            int intProperty = ((BatteryManager) context.getSystemService("batterymanager")).getIntProperty(4);
            if (intProperty == Integer.MIN_VALUE) {
                intProperty = -1;
            }
            return intProperty;
        }
        try {
            Class cls = Class.forName("com.android.internal.os.PowerProfile");
            Object newInstance = cls.getConstructor(new Class[]{Context.class}).newInstance(new Object[]{context});
            Method method = cls.getMethod("getAveragePower", new Class[]{String.class});
            method.setAccessible(true);
            return (int) Double.parseDouble(method.invoke(newInstance, new Object[]{"battery.capacity"}).toString());
        } catch (Throwable th) {
            th.printStackTrace();
            return -1;
        }
    }

    public static int getActualBatteryCapacity(Context context) {
        Object obj;
        int i;
        try {
            obj = Class.forName("com.android.internal.os.PowerProfile").getConstructor(new Class[]{Context.class}).newInstance(new Object[]{context});
        } catch (Exception e) {
            e.printStackTrace();
            obj = null;
        }
        try {
            i = ((Integer) Class.forName("com.android.internal.os.PowerProfile").getMethod("getAveragePower", new Class[]{String.class}).invoke(obj, new Object[]{"battery.capacity"})).intValue();
            try {
                String str = TAG;
                StringBuilder sb = new StringBuilder();
                sb.append(i);
                sb.append(" mah");
                LogUtils.LOGI(str, sb.toString());
            } catch (Exception e2) {
                e = e2;
            }
        } catch (Exception e3) {
            e = e3;
            i = 0;
            e.printStackTrace();
            return i;
        }
        return i;
    }

    public static int getBatteryChargeCounter(Context context) {
        if (VERSION.SDK_INT >= 21) {
            return ((BatteryManager) context.getSystemService("batterymanager")).getIntProperty(1);
        }
        return -1;
    }

    public static int getBatteryCurrentAverage(Context context) {
        int intProperty = VERSION.SDK_INT >= 21 ? ((BatteryManager) context.getSystemService("batterymanager")).getIntProperty(3) : -1;
        if (intProperty != Integer.MIN_VALUE) {
            return intProperty;
        }
        return -1;
    }

    public static int getBatteryCurrentNow(Context context) {
        int i;
        if (VERSION.SDK_INT >= 21) {
            i = ((BatteryManager) context.getSystemService("batterymanager")).getIntProperty(2);
        } else {
            i = getBatteryCurrentNowLegacy();
        }
        if (i != Integer.MIN_VALUE) {
            return (-i) / 1000;
        }
        return -1;
    }

    public static long getBatteryEnergyCounter(Context context) {
        long longProperty = VERSION.SDK_INT >= 21 ? ((BatteryManager) context.getSystemService("batterymanager")).getLongProperty(5) : -1;
        if (longProperty != Long.MIN_VALUE) {
            return longProperty;
        }
        return -1;
    }

    public static int getBatteryAveragePower(Context context) {
        Intent registerReceiver = context.registerReceiver(null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
        if (registerReceiver == null) {
            return -1;
        }
        int i = 0;
        int intExtra = registerReceiver.getIntExtra("voltage", 0);
        if (VERSION.SDK_INT >= 21) {
            i = ((BatteryManager) context.getSystemService("batterymanager")).getIntProperty(3);
        }
        return (intExtra * i) / 1000000000;
    }

    public static double getBatteryCapacityConsumed(double d, Context context) {
        return (((double) (VERSION.SDK_INT >= 21 ? ((BatteryManager) context.getSystemService("batterymanager")).getIntProperty(3) : 0)) * d) / 1000.0d;
    }

    private static int getBatteryCurrentNowLegacy() {
        int i;
        try {
            RandomAccessFile randomAccessFile = new RandomAccessFile(Config.BATTERY_SOURCE_DEFAULT, "r");
            i = Integer.parseInt(randomAccessFile.readLine());
            try {
                randomAccessFile.close();
            } catch (IOException unused) {
            }
        } catch (IOException unused2) {
            i = -1;
            LogUtils.LOGI(TAG, "Device has no current_avg file available");
            return -i;
        }
        return -i;
    }
}
