package com.github.mikephil.charting.renderer;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.Path.Direction;
import android.graphics.RectF;
import android.os.Build.VERSION;
import android.support.v4.view.ViewCompat;
import android.text.Layout.Alignment;
import android.text.StaticLayout;
import android.text.TextPaint;
import com.github.mikephil.charting.animation.ChartAnimator;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet.ValuePosition;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.IValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.IPieDataSet;
import com.github.mikephil.charting.utils.MPPointF;
import com.github.mikephil.charting.utils.Utils;
import com.github.mikephil.charting.utils.ViewPortHandler;
import java.lang.ref.WeakReference;
import java.util.List;

public class PieChartRenderer extends DataRenderer {
    protected Canvas mBitmapCanvas;
    private RectF mCenterTextLastBounds = new RectF();
    private CharSequence mCenterTextLastValue;
    private StaticLayout mCenterTextLayout;
    private TextPaint mCenterTextPaint;
    protected PieChart mChart;
    protected WeakReference<Bitmap> mDrawBitmap;
    protected Path mDrawCenterTextPathBuffer = new Path();
    protected RectF mDrawHighlightedRectF = new RectF();
    private Paint mEntryLabelsPaint;
    private Path mHoleCirclePath = new Path();
    protected Paint mHolePaint;
    private RectF mInnerRectBuffer = new RectF();
    private Path mPathBuffer = new Path();
    private RectF[] mRectBuffer = {new RectF(), new RectF(), new RectF()};
    protected Paint mTransparentCirclePaint;
    protected Paint mValueLinePaint;

    public void initBuffers() {
    }

    public PieChartRenderer(PieChart pieChart, ChartAnimator chartAnimator, ViewPortHandler viewPortHandler) {
        super(chartAnimator, viewPortHandler);
        this.mChart = pieChart;
        this.mHolePaint = new Paint(1);
        this.mHolePaint.setColor(-1);
        this.mHolePaint.setStyle(Style.FILL);
        this.mTransparentCirclePaint = new Paint(1);
        this.mTransparentCirclePaint.setColor(-1);
        this.mTransparentCirclePaint.setStyle(Style.FILL);
        this.mTransparentCirclePaint.setAlpha(105);
        this.mCenterTextPaint = new TextPaint(1);
        this.mCenterTextPaint.setColor(ViewCompat.MEASURED_STATE_MASK);
        this.mCenterTextPaint.setTextSize(Utils.convertDpToPixel(12.0f));
        this.mValuePaint.setTextSize(Utils.convertDpToPixel(13.0f));
        this.mValuePaint.setColor(-1);
        this.mValuePaint.setTextAlign(Align.CENTER);
        this.mEntryLabelsPaint = new Paint(1);
        this.mEntryLabelsPaint.setColor(-1);
        this.mEntryLabelsPaint.setTextAlign(Align.CENTER);
        this.mEntryLabelsPaint.setTextSize(Utils.convertDpToPixel(13.0f));
        this.mValueLinePaint = new Paint(1);
        this.mValueLinePaint.setStyle(Style.STROKE);
    }

    public Paint getPaintHole() {
        return this.mHolePaint;
    }

    public Paint getPaintTransparentCircle() {
        return this.mTransparentCirclePaint;
    }

    public TextPaint getPaintCenterText() {
        return this.mCenterTextPaint;
    }

    public Paint getPaintEntryLabels() {
        return this.mEntryLabelsPaint;
    }

    public void drawData(Canvas canvas) {
        int chartWidth = (int) this.mViewPortHandler.getChartWidth();
        int chartHeight = (int) this.mViewPortHandler.getChartHeight();
        if (!(this.mDrawBitmap != null && ((Bitmap) this.mDrawBitmap.get()).getWidth() == chartWidth && ((Bitmap) this.mDrawBitmap.get()).getHeight() == chartHeight)) {
            if (chartWidth > 0 && chartHeight > 0) {
                this.mDrawBitmap = new WeakReference<>(Bitmap.createBitmap(chartWidth, chartHeight, Config.ARGB_4444));
                this.mBitmapCanvas = new Canvas((Bitmap) this.mDrawBitmap.get());
            } else {
                return;
            }
        }
        ((Bitmap) this.mDrawBitmap.get()).eraseColor(0);
        for (IPieDataSet iPieDataSet : ((PieData) this.mChart.getData()).getDataSets()) {
            if (iPieDataSet.isVisible() && iPieDataSet.getEntryCount() > 0) {
                drawDataSet(canvas, iPieDataSet);
            }
        }
    }

    /* access modifiers changed from: protected */
    public float calculateMinimumRadiusForSpacedSlice(MPPointF mPPointF, float f, float f2, float f3, float f4, float f5, float f6) {
        MPPointF mPPointF2 = mPPointF;
        double d = (double) ((f5 + f6) * 0.017453292f);
        float cos = mPPointF2.x + (((float) Math.cos(d)) * f);
        float sin = mPPointF2.y + (((float) Math.sin(d)) * f);
        double d2 = (double) ((f5 + (f6 / 2.0f)) * 0.017453292f);
        return (float) (((double) (f - ((float) ((Math.sqrt(Math.pow((double) (cos - f3), 2.0d) + Math.pow((double) (sin - f4), 2.0d)) / 2.0d) * Math.tan(0.017453292519943295d * ((180.0d - ((double) f2)) / 2.0d)))))) - Math.sqrt(Math.pow((double) ((mPPointF2.x + (((float) Math.cos(d2)) * f)) - ((cos + f3) / 2.0f)), 2.0d) + Math.pow((double) ((mPPointF2.y + (((float) Math.sin(d2)) * f)) - ((sin + f4) / 2.0f)), 2.0d)));
    }

    /* access modifiers changed from: protected */
    public float getSliceSpace(IPieDataSet iPieDataSet) {
        float f;
        if (!iPieDataSet.isAutomaticallyDisableSliceSpacingEnabled()) {
            return iPieDataSet.getSliceSpace();
        }
        if (iPieDataSet.getSliceSpace() / this.mViewPortHandler.getSmallestContentExtension() > (iPieDataSet.getYMin() / ((PieData) this.mChart.getData()).getYValueSum()) * 2.0f) {
            f = 0.0f;
        } else {
            f = iPieDataSet.getSliceSpace();
        }
        return f;
    }

    /* access modifiers changed from: protected */
    public void drawDataSet(Canvas canvas, IPieDataSet iPieDataSet) {
        float f;
        float f2;
        int i;
        RectF rectF;
        float f3;
        float[] fArr;
        int i2;
        int i3;
        float f4;
        float f5;
        int i4;
        MPPointF mPPointF;
        float f6;
        float f7;
        float f8;
        float f9;
        float f10;
        RectF rectF2;
        int i5;
        int i6;
        int i7;
        float f11;
        MPPointF mPPointF2;
        int i8;
        PieChartRenderer pieChartRenderer = this;
        IPieDataSet iPieDataSet2 = iPieDataSet;
        float rotationAngle = pieChartRenderer.mChart.getRotationAngle();
        float phaseX = pieChartRenderer.mAnimator.getPhaseX();
        float phaseY = pieChartRenderer.mAnimator.getPhaseY();
        RectF circleBox = pieChartRenderer.mChart.getCircleBox();
        int entryCount = iPieDataSet.getEntryCount();
        float[] drawAngles = pieChartRenderer.mChart.getDrawAngles();
        MPPointF centerCircleBox = pieChartRenderer.mChart.getCenterCircleBox();
        float radius = pieChartRenderer.mChart.getRadius();
        int i9 = 1;
        boolean z = pieChartRenderer.mChart.isDrawHoleEnabled() && !pieChartRenderer.mChart.isDrawSlicesUnderHoleEnabled();
        float holeRadius = z ? (pieChartRenderer.mChart.getHoleRadius() / 100.0f) * radius : 0.0f;
        int i10 = 0;
        for (int i11 = 0; i11 < entryCount; i11++) {
            if (Math.abs(((PieEntry) iPieDataSet2.getEntryForIndex(i11)).getY()) > Utils.FLOAT_EPSILON) {
                i10++;
            }
        }
        if (i10 <= 1) {
            f = 0.0f;
        } else {
            f = pieChartRenderer.getSliceSpace(iPieDataSet2);
        }
        int i12 = 0;
        float f12 = 0.0f;
        while (i12 < entryCount) {
            float f13 = drawAngles[i12];
            if (Math.abs(iPieDataSet2.getEntryForIndex(i12).getY()) <= Utils.FLOAT_EPSILON || pieChartRenderer.mChart.needsHighlight(i12)) {
                i = i12;
                i3 = i9;
                f4 = radius;
                f2 = rotationAngle;
                f3 = phaseX;
                rectF = circleBox;
                i2 = entryCount;
                fArr = drawAngles;
                i4 = i10;
                f5 = holeRadius;
                mPPointF = centerCircleBox;
            } else {
                int i13 = (f <= 0.0f || f13 > 180.0f) ? 0 : i9;
                pieChartRenderer.mRenderPaint.setColor(iPieDataSet2.getColor(i12));
                float f14 = i10 == 1 ? 0.0f : f / (0.017453292f * radius);
                float f15 = rotationAngle + ((f12 + (f14 / 2.0f)) * phaseY);
                float f16 = (f13 - f14) * phaseY;
                if (f16 < 0.0f) {
                    f16 = 0.0f;
                }
                pieChartRenderer.mPathBuffer.reset();
                int i14 = i12;
                int i15 = i10;
                double d = (double) (f15 * 0.017453292f);
                i2 = entryCount;
                fArr = drawAngles;
                float cos = centerCircleBox.x + (((float) Math.cos(d)) * radius);
                float sin = centerCircleBox.y + (((float) Math.sin(d)) * radius);
                if (f16 < 360.0f || f16 % 360.0f > Utils.FLOAT_EPSILON) {
                    f3 = phaseX;
                    pieChartRenderer.mPathBuffer.moveTo(cos, sin);
                    pieChartRenderer.mPathBuffer.arcTo(circleBox, f15, f16);
                } else {
                    f3 = phaseX;
                    pieChartRenderer.mPathBuffer.addCircle(centerCircleBox.x, centerCircleBox.y, radius, Direction.CW);
                }
                float f17 = f16;
                pieChartRenderer.mInnerRectBuffer.set(centerCircleBox.x - holeRadius, centerCircleBox.y - holeRadius, centerCircleBox.x + holeRadius, centerCircleBox.y + holeRadius);
                if (!z) {
                    f8 = holeRadius;
                    f9 = radius;
                    f10 = rotationAngle;
                    rectF2 = circleBox;
                    i5 = i14;
                    i6 = i15;
                    f6 = f17;
                    i7 = 1;
                    mPPointF = centerCircleBox;
                    f7 = 360.0f;
                } else if (holeRadius > 0.0f || i13 != 0) {
                    if (i13 != 0) {
                        f11 = f17;
                        float f18 = radius;
                        rectF = circleBox;
                        i4 = i15;
                        i = i14;
                        f5 = holeRadius;
                        i8 = 1;
                        f4 = radius;
                        float f19 = f15;
                        mPPointF2 = centerCircleBox;
                        float calculateMinimumRadiusForSpacedSlice = pieChartRenderer.calculateMinimumRadiusForSpacedSlice(centerCircleBox, f18, f13 * phaseY, cos, sin, f19, f11);
                        if (calculateMinimumRadiusForSpacedSlice < 0.0f) {
                            calculateMinimumRadiusForSpacedSlice = -calculateMinimumRadiusForSpacedSlice;
                        }
                        holeRadius = Math.max(f5, calculateMinimumRadiusForSpacedSlice);
                    } else {
                        f5 = holeRadius;
                        f4 = radius;
                        mPPointF2 = centerCircleBox;
                        rectF = circleBox;
                        i = i14;
                        i4 = i15;
                        f11 = f17;
                        i8 = 1;
                    }
                    float f20 = (i4 == i8 || holeRadius == 0.0f) ? 0.0f : f / (0.017453292f * holeRadius);
                    float f21 = ((f12 + (f20 / 2.0f)) * phaseY) + rotationAngle;
                    float f22 = (f13 - f20) * phaseY;
                    if (f22 < 0.0f) {
                        f22 = 0.0f;
                    }
                    float f23 = f21 + f22;
                    if (f11 < 360.0f || f11 % 360.0f > Utils.FLOAT_EPSILON) {
                        i3 = i8;
                        pieChartRenderer = this;
                        double d2 = (double) (f23 * 0.017453292f);
                        f2 = rotationAngle;
                        pieChartRenderer.mPathBuffer.lineTo(mPPointF2.x + (((float) Math.cos(d2)) * holeRadius), mPPointF2.y + (holeRadius * ((float) Math.sin(d2))));
                        pieChartRenderer.mPathBuffer.arcTo(pieChartRenderer.mInnerRectBuffer, f23, -f22);
                    } else {
                        i3 = i8;
                        pieChartRenderer = this;
                        pieChartRenderer.mPathBuffer.addCircle(mPPointF2.x, mPPointF2.y, holeRadius, Direction.CCW);
                        f2 = rotationAngle;
                    }
                    mPPointF = mPPointF2;
                    pieChartRenderer.mPathBuffer.close();
                    pieChartRenderer.mBitmapCanvas.drawPath(pieChartRenderer.mPathBuffer, pieChartRenderer.mRenderPaint);
                } else {
                    f8 = holeRadius;
                    f9 = radius;
                    f10 = rotationAngle;
                    rectF2 = circleBox;
                    i5 = i14;
                    i6 = i15;
                    f6 = f17;
                    f7 = 360.0f;
                    i7 = 1;
                    mPPointF = centerCircleBox;
                }
                if (f6 % f7 > Utils.FLOAT_EPSILON) {
                    if (i13 != 0) {
                        float f24 = f15 + (f6 / 2.0f);
                        float calculateMinimumRadiusForSpacedSlice2 = pieChartRenderer.calculateMinimumRadiusForSpacedSlice(mPPointF, f4, f13 * phaseY, cos, sin, f15, f6);
                        double d3 = (double) (f24 * 0.017453292f);
                        pieChartRenderer.mPathBuffer.lineTo(mPPointF.x + (((float) Math.cos(d3)) * calculateMinimumRadiusForSpacedSlice2), mPPointF.y + (calculateMinimumRadiusForSpacedSlice2 * ((float) Math.sin(d3))));
                    } else {
                        pieChartRenderer.mPathBuffer.lineTo(mPPointF.x, mPPointF.y);
                    }
                }
                pieChartRenderer.mPathBuffer.close();
                pieChartRenderer.mBitmapCanvas.drawPath(pieChartRenderer.mPathBuffer, pieChartRenderer.mRenderPaint);
            }
            f12 += f13 * f3;
            i12 = i + 1;
            centerCircleBox = mPPointF;
            i10 = i4;
            holeRadius = f5;
            radius = f4;
            i9 = i3;
            entryCount = i2;
            drawAngles = fArr;
            phaseX = f3;
            circleBox = rectF;
            rotationAngle = f2;
            iPieDataSet2 = iPieDataSet;
        }
        MPPointF.recycleInstance(centerCircleBox);
    }

    public void drawValues(Canvas canvas) {
        float f;
        float[] fArr;
        float[] fArr2;
        float f2;
        List list;
        int i;
        MPPointF mPPointF;
        PieData pieData;
        float f3;
        float y;
        PieEntry pieEntry;
        MPPointF mPPointF2;
        float f4;
        float f5;
        ValuePosition valuePosition;
        ValuePosition valuePosition2;
        int i2;
        int i3;
        IPieDataSet iPieDataSet;
        int i4;
        PieData pieData2;
        MPPointF mPPointF3;
        float f6;
        float f7;
        float f8;
        PieEntry pieEntry2;
        float f9;
        PieEntry pieEntry3;
        Canvas canvas2 = canvas;
        MPPointF centerCircleBox = this.mChart.getCenterCircleBox();
        float radius = this.mChart.getRadius();
        float rotationAngle = this.mChart.getRotationAngle();
        float[] drawAngles = this.mChart.getDrawAngles();
        float[] absoluteAngles = this.mChart.getAbsoluteAngles();
        float phaseX = this.mAnimator.getPhaseX();
        float phaseY = this.mAnimator.getPhaseY();
        float holeRadius = this.mChart.getHoleRadius() / 100.0f;
        float f10 = (radius / 10.0f) * 3.6f;
        if (this.mChart.isDrawHoleEnabled()) {
            f10 = (radius - (radius * holeRadius)) / 2.0f;
        }
        float f11 = radius - f10;
        PieData pieData3 = (PieData) this.mChart.getData();
        List dataSets = pieData3.getDataSets();
        float yValueSum = pieData3.getYValueSum();
        boolean isDrawEntryLabelsEnabled = this.mChart.isDrawEntryLabelsEnabled();
        canvas.save();
        float convertDpToPixel = Utils.convertDpToPixel(5.0f);
        int i5 = 0;
        int i6 = 0;
        while (i6 < dataSets.size()) {
            IPieDataSet iPieDataSet2 = (IPieDataSet) dataSets.get(i6);
            boolean isDrawValuesEnabled = iPieDataSet2.isDrawValuesEnabled();
            if (isDrawValuesEnabled || isDrawEntryLabelsEnabled) {
                ValuePosition xValuePosition = iPieDataSet2.getXValuePosition();
                ValuePosition yValuePosition = iPieDataSet2.getYValuePosition();
                applyValueTextStyle(iPieDataSet2);
                float calcTextHeight = ((float) Utils.calcTextHeight(this.mValuePaint, "Q")) + Utils.convertDpToPixel(4.0f);
                IValueFormatter valueFormatter = iPieDataSet2.getValueFormatter();
                int entryCount = iPieDataSet2.getEntryCount();
                int i7 = i5;
                this.mValueLinePaint.setColor(iPieDataSet2.getValueLineColor());
                this.mValueLinePaint.setStrokeWidth(Utils.convertDpToPixel(iPieDataSet2.getValueLineWidth()));
                float sliceSpace = getSliceSpace(iPieDataSet2);
                int i8 = 0;
                while (i8 < entryCount) {
                    PieEntry pieEntry4 = (PieEntry) iPieDataSet2.getEntryForIndex(i8);
                    if (i7 == 0) {
                        f3 = 0.0f;
                    } else {
                        f3 = absoluteAngles[i7 - 1] * phaseX;
                    }
                    int i9 = i8;
                    float f12 = rotationAngle + ((f3 + ((drawAngles[i7] - ((sliceSpace / (0.017453292f * f11)) / 2.0f)) / 2.0f)) * phaseY);
                    int i10 = entryCount;
                    if (this.mChart.isUsePercentValuesEnabled()) {
                        y = (pieEntry4.getY() / yValueSum) * 100.0f;
                    } else {
                        y = pieEntry4.getY();
                    }
                    float f13 = y;
                    int i11 = i6;
                    List list2 = dataSets;
                    double d = (double) (f12 * 0.017453292f);
                    float f14 = rotationAngle;
                    float[] fArr3 = drawAngles;
                    float cos = (float) Math.cos(d);
                    float[] fArr4 = absoluteAngles;
                    float sin = (float) Math.sin(d);
                    boolean z = isDrawEntryLabelsEnabled && xValuePosition == ValuePosition.OUTSIDE_SLICE;
                    boolean z2 = isDrawValuesEnabled && yValuePosition == ValuePosition.OUTSIDE_SLICE;
                    boolean z3 = isDrawEntryLabelsEnabled && xValuePosition == ValuePosition.INSIDE_SLICE;
                    boolean z4 = isDrawValuesEnabled && yValuePosition == ValuePosition.INSIDE_SLICE;
                    if (z || z2) {
                        float valueLinePart1Length = iPieDataSet2.getValueLinePart1Length();
                        float valueLinePart2Length = iPieDataSet2.getValueLinePart2Length();
                        float valueLinePart1OffsetPercentage = iPieDataSet2.getValueLinePart1OffsetPercentage() / 100.0f;
                        PieEntry pieEntry5 = pieEntry4;
                        if (this.mChart.isDrawHoleEnabled()) {
                            float f15 = radius * holeRadius;
                            f6 = ((radius - f15) * valueLinePart1OffsetPercentage) + f15;
                        } else {
                            f6 = radius * valueLinePart1OffsetPercentage;
                        }
                        float abs = iPieDataSet2.isValueLineVariableLength() ? valueLinePart2Length * f11 * ((float) Math.abs(Math.sin(d))) : valueLinePart2Length * f11;
                        float f16 = centerCircleBox.x + (f6 * cos);
                        float f17 = (f6 * sin) + centerCircleBox.y;
                        float f18 = (1.0f + valueLinePart1Length) * f11;
                        ValuePosition valuePosition3 = yValuePosition;
                        float f19 = (f18 * cos) + centerCircleBox.x;
                        float f20 = (f18 * sin) + centerCircleBox.y;
                        double d2 = ((double) f12) % 360.0d;
                        if (d2 < 90.0d || d2 > 270.0d) {
                            f7 = f19 + abs;
                            this.mValuePaint.setTextAlign(Align.LEFT);
                            if (z) {
                                this.mEntryLabelsPaint.setTextAlign(Align.LEFT);
                            }
                            f8 = f7 + convertDpToPixel;
                        } else {
                            float f21 = f19 - abs;
                            this.mValuePaint.setTextAlign(Align.RIGHT);
                            if (z) {
                                this.mEntryLabelsPaint.setTextAlign(Align.RIGHT);
                            }
                            f7 = f21;
                            f8 = f21 - convertDpToPixel;
                        }
                        if (iPieDataSet2.getValueLineColor() != 1122867) {
                            Canvas canvas3 = canvas2;
                            pieEntry2 = pieEntry5;
                            f5 = radius;
                            i4 = i9;
                            i2 = i10;
                            float f22 = f17;
                            f9 = f8;
                            valuePosition2 = valuePosition3;
                            valuePosition = xValuePosition;
                            float f23 = f20;
                            f4 = sin;
                            iPieDataSet = iPieDataSet2;
                            canvas3.drawLine(f16, f22, f19, f23, this.mValueLinePaint);
                            canvas3.drawLine(f19, f20, f7, f23, this.mValueLinePaint);
                        } else {
                            f9 = f8;
                            f5 = radius;
                            f4 = sin;
                            i4 = i9;
                            i2 = i10;
                            pieEntry2 = pieEntry5;
                            valuePosition2 = valuePosition3;
                            valuePosition = xValuePosition;
                            iPieDataSet = iPieDataSet2;
                        }
                        if (!z || !z2) {
                            float f24 = f9;
                            mPPointF2 = centerCircleBox;
                            i3 = i11;
                            pieData2 = pieData3;
                            pieEntry3 = pieEntry2;
                            if (z) {
                                if (i4 < pieData2.getEntryCount() && pieEntry3.getLabel() != null) {
                                    drawEntryLabel(canvas2, pieEntry3.getLabel(), f24, f20 + (calcTextHeight / 2.0f));
                                }
                            } else if (z2) {
                                pieEntry = pieEntry3;
                                drawValue(canvas2, valueFormatter, f13, pieEntry3, 0, f24, f20 + (calcTextHeight / 2.0f), iPieDataSet.getValueTextColor(i4));
                            }
                        } else {
                            i3 = i11;
                            float f25 = f9;
                            mPPointF2 = centerCircleBox;
                            pieData2 = pieData3;
                            drawValue(canvas2, valueFormatter, f13, pieEntry2, 0, f9, f20, iPieDataSet.getValueTextColor(i4));
                            if (i4 < pieData2.getEntryCount()) {
                                pieEntry3 = pieEntry2;
                                if (pieEntry3.getLabel() != null) {
                                    drawEntryLabel(canvas2, pieEntry3.getLabel(), f25, f20 + calcTextHeight);
                                }
                            } else {
                                pieEntry = pieEntry2;
                            }
                        }
                        pieEntry = pieEntry3;
                    } else {
                        pieEntry = pieEntry4;
                        valuePosition = xValuePosition;
                        mPPointF2 = centerCircleBox;
                        f5 = radius;
                        f4 = sin;
                        i4 = i9;
                        i2 = i10;
                        i3 = i11;
                        valuePosition2 = yValuePosition;
                        iPieDataSet = iPieDataSet2;
                        pieData2 = pieData3;
                    }
                    if (z3 || z4) {
                        mPPointF3 = mPPointF2;
                        float f26 = (cos * f11) + mPPointF3.x;
                        float f27 = (f11 * f4) + mPPointF3.y;
                        this.mValuePaint.setTextAlign(Align.CENTER);
                        if (!z3 || !z4) {
                            PieEntry pieEntry6 = pieEntry;
                            if (z3) {
                                if (i4 < pieData2.getEntryCount() && pieEntry6.getLabel() != null) {
                                    drawEntryLabel(canvas2, pieEntry6.getLabel(), f26, f27 + (calcTextHeight / 2.0f));
                                }
                            } else if (z4) {
                                drawValue(canvas2, valueFormatter, f13, pieEntry6, 0, f26, f27 + (calcTextHeight / 2.0f), iPieDataSet.getValueTextColor(i4));
                            }
                        } else {
                            drawValue(canvas2, valueFormatter, f13, pieEntry, 0, f26, f27, iPieDataSet.getValueTextColor(i4));
                            if (i4 < pieData2.getEntryCount()) {
                                PieEntry pieEntry7 = pieEntry;
                                if (pieEntry7.getLabel() != null) {
                                    drawEntryLabel(canvas2, pieEntry7.getLabel(), f26, f27 + calcTextHeight);
                                }
                            }
                        }
                    } else {
                        mPPointF3 = mPPointF2;
                    }
                    i7++;
                    i8 = i4 + 1;
                    pieData3 = pieData2;
                    iPieDataSet2 = iPieDataSet;
                    centerCircleBox = mPPointF3;
                    i6 = i3;
                    entryCount = i2;
                    yValuePosition = valuePosition2;
                    dataSets = list2;
                    rotationAngle = f14;
                    drawAngles = fArr3;
                    absoluteAngles = fArr4;
                    xValuePosition = valuePosition;
                    radius = f5;
                }
                i = i6;
                list = dataSets;
                f = radius;
                f2 = rotationAngle;
                fArr2 = drawAngles;
                fArr = absoluteAngles;
                mPPointF = centerCircleBox;
                pieData = pieData3;
                i5 = i7;
            } else {
                i = i6;
                list = dataSets;
                f = radius;
                f2 = rotationAngle;
                fArr2 = drawAngles;
                fArr = absoluteAngles;
                mPPointF = centerCircleBox;
                pieData = pieData3;
            }
            i6 = i + 1;
            pieData3 = pieData;
            centerCircleBox = mPPointF;
            dataSets = list;
            rotationAngle = f2;
            drawAngles = fArr2;
            absoluteAngles = fArr;
            radius = f;
        }
        MPPointF.recycleInstance(centerCircleBox);
        canvas.restore();
    }

    /* access modifiers changed from: protected */
    public void drawEntryLabel(Canvas canvas, String str, float f, float f2) {
        canvas.drawText(str, f, f2, this.mEntryLabelsPaint);
    }

    public void drawExtras(Canvas canvas) {
        drawHole(canvas);
        canvas.drawBitmap((Bitmap) this.mDrawBitmap.get(), 0.0f, 0.0f, null);
        drawCenterText(canvas);
    }

    /* access modifiers changed from: protected */
    public void drawHole(Canvas canvas) {
        if (this.mChart.isDrawHoleEnabled() && this.mBitmapCanvas != null) {
            float radius = this.mChart.getRadius();
            float holeRadius = (this.mChart.getHoleRadius() / 100.0f) * radius;
            MPPointF centerCircleBox = this.mChart.getCenterCircleBox();
            if (Color.alpha(this.mHolePaint.getColor()) > 0) {
                this.mBitmapCanvas.drawCircle(centerCircleBox.x, centerCircleBox.y, holeRadius, this.mHolePaint);
            }
            if (Color.alpha(this.mTransparentCirclePaint.getColor()) > 0 && this.mChart.getTransparentCircleRadius() > this.mChart.getHoleRadius()) {
                int alpha = this.mTransparentCirclePaint.getAlpha();
                float transparentCircleRadius = radius * (this.mChart.getTransparentCircleRadius() / 100.0f);
                this.mTransparentCirclePaint.setAlpha((int) (((float) alpha) * this.mAnimator.getPhaseX() * this.mAnimator.getPhaseY()));
                this.mHoleCirclePath.reset();
                this.mHoleCirclePath.addCircle(centerCircleBox.x, centerCircleBox.y, transparentCircleRadius, Direction.CW);
                this.mHoleCirclePath.addCircle(centerCircleBox.x, centerCircleBox.y, holeRadius, Direction.CCW);
                this.mBitmapCanvas.drawPath(this.mHoleCirclePath, this.mTransparentCirclePaint);
                this.mTransparentCirclePaint.setAlpha(alpha);
            }
            MPPointF.recycleInstance(centerCircleBox);
        }
    }

    /* access modifiers changed from: protected */
    public void drawCenterText(Canvas canvas) {
        float f;
        MPPointF mPPointF;
        Canvas canvas2 = canvas;
        CharSequence centerText = this.mChart.getCenterText();
        if (this.mChart.isDrawCenterTextEnabled() && centerText != null) {
            MPPointF centerCircleBox = this.mChart.getCenterCircleBox();
            MPPointF centerTextOffset = this.mChart.getCenterTextOffset();
            float f2 = centerCircleBox.x + centerTextOffset.x;
            float f3 = centerCircleBox.y + centerTextOffset.y;
            if (!this.mChart.isDrawHoleEnabled() || this.mChart.isDrawSlicesUnderHoleEnabled()) {
                f = this.mChart.getRadius();
            } else {
                f = this.mChart.getRadius() * (this.mChart.getHoleRadius() / 100.0f);
            }
            RectF rectF = this.mRectBuffer[0];
            rectF.left = f2 - f;
            rectF.top = f3 - f;
            rectF.right = f2 + f;
            rectF.bottom = f3 + f;
            RectF rectF2 = this.mRectBuffer[1];
            rectF2.set(rectF);
            float centerTextRadiusPercent = this.mChart.getCenterTextRadiusPercent() / 100.0f;
            if (((double) centerTextRadiusPercent) > Utils.DOUBLE_EPSILON) {
                rectF2.inset((rectF2.width() - (rectF2.width() * centerTextRadiusPercent)) / 2.0f, (rectF2.height() - (rectF2.height() * centerTextRadiusPercent)) / 2.0f);
            }
            if (!centerText.equals(this.mCenterTextLastValue) || !rectF2.equals(this.mCenterTextLastBounds)) {
                this.mCenterTextLastBounds.set(rectF2);
                this.mCenterTextLastValue = centerText;
                mPPointF = centerTextOffset;
                StaticLayout staticLayout = r3;
                StaticLayout staticLayout2 = new StaticLayout(centerText, 0, centerText.length(), this.mCenterTextPaint, (int) Math.max(Math.ceil((double) this.mCenterTextLastBounds.width()), 1.0d), Alignment.ALIGN_CENTER, 1.0f, 0.0f, false);
                this.mCenterTextLayout = staticLayout;
            } else {
                mPPointF = centerTextOffset;
            }
            float height = (float) this.mCenterTextLayout.getHeight();
            canvas.save();
            if (VERSION.SDK_INT >= 18) {
                Path path = this.mDrawCenterTextPathBuffer;
                path.reset();
                path.addOval(rectF, Direction.CW);
                canvas2.clipPath(path);
            }
            canvas2.translate(rectF2.left, rectF2.top + ((rectF2.height() - height) / 2.0f));
            this.mCenterTextLayout.draw(canvas2);
            canvas.restore();
            MPPointF.recycleInstance(centerCircleBox);
            MPPointF.recycleInstance(mPPointF);
        }
    }

    public void drawHighlighted(Canvas canvas, Highlight[] highlightArr) {
        float f;
        RectF rectF;
        float[] fArr;
        float[] fArr2;
        float f2;
        boolean z;
        int i;
        float f3;
        int i2;
        float f4;
        float f5;
        float f6;
        int i3;
        int i4;
        float f7;
        float f8;
        Highlight[] highlightArr2 = highlightArr;
        float phaseX = this.mAnimator.getPhaseX();
        float phaseY = this.mAnimator.getPhaseY();
        float rotationAngle = this.mChart.getRotationAngle();
        float[] drawAngles = this.mChart.getDrawAngles();
        float[] absoluteAngles = this.mChart.getAbsoluteAngles();
        MPPointF centerCircleBox = this.mChart.getCenterCircleBox();
        float radius = this.mChart.getRadius();
        boolean z2 = this.mChart.isDrawHoleEnabled() && !this.mChart.isDrawSlicesUnderHoleEnabled();
        boolean z3 = false;
        float holeRadius = z2 ? (this.mChart.getHoleRadius() / 100.0f) * radius : 0.0f;
        RectF rectF2 = this.mDrawHighlightedRectF;
        rectF2.set(0.0f, 0.0f, 0.0f, 0.0f);
        int i5 = 0;
        while (i5 < highlightArr2.length) {
            int x = (int) highlightArr2[i5].getX();
            if (x < drawAngles.length) {
                IPieDataSet dataSetByIndex = ((PieData) this.mChart.getData()).getDataSetByIndex(highlightArr2[i5].getDataSetIndex());
                if (dataSetByIndex != null && dataSetByIndex.isHighlightEnabled()) {
                    int entryCount = dataSetByIndex.getEntryCount();
                    int i6 = 0;
                    int i7 = 0;
                    while (i6 < entryCount) {
                        int i8 = entryCount;
                        if (Math.abs(((PieEntry) dataSetByIndex.getEntryForIndex(i6)).getY()) > Utils.FLOAT_EPSILON) {
                            i7++;
                        }
                        i6++;
                        entryCount = i8;
                    }
                    if (x == 0) {
                        i2 = 1;
                        f3 = 0.0f;
                    } else {
                        f3 = absoluteAngles[x - 1] * phaseX;
                        i2 = 1;
                    }
                    if (i7 <= i2) {
                        f4 = 0.0f;
                    } else {
                        f4 = dataSetByIndex.getSliceSpace();
                    }
                    float f9 = drawAngles[x];
                    float selectionShift = dataSetByIndex.getSelectionShift();
                    float f10 = radius + selectionShift;
                    int i9 = i5;
                    rectF2.set(this.mChart.getCircleBox());
                    float f11 = -selectionShift;
                    rectF2.inset(f11, f11);
                    boolean z4 = f4 > 0.0f && f9 <= 180.0f;
                    this.mRenderPaint.setColor(dataSetByIndex.getColor(x));
                    float f12 = i7 == 1 ? 0.0f : f4 / (0.017453292f * radius);
                    float f13 = i7 == 1 ? 0.0f : f4 / (0.017453292f * f10);
                    float f14 = rotationAngle + ((f3 + (f12 / 2.0f)) * phaseY);
                    float f15 = (f9 - f12) * phaseY;
                    z = false;
                    float f16 = f15 < 0.0f ? 0.0f : f15;
                    float f17 = ((f3 + (f13 / 2.0f)) * phaseY) + rotationAngle;
                    float f18 = (f9 - f13) * phaseY;
                    if (f18 < 0.0f) {
                        f18 = 0.0f;
                    }
                    this.mPathBuffer.reset();
                    if (f16 < 360.0f || f16 % 360.0f > Utils.FLOAT_EPSILON) {
                        f5 = holeRadius;
                        f2 = phaseX;
                        double d = (double) (f17 * 0.017453292f);
                        fArr2 = drawAngles;
                        fArr = absoluteAngles;
                        this.mPathBuffer.moveTo(centerCircleBox.x + (((float) Math.cos(d)) * f10), centerCircleBox.y + (f10 * ((float) Math.sin(d))));
                        this.mPathBuffer.arcTo(rectF2, f17, f18);
                    } else {
                        this.mPathBuffer.addCircle(centerCircleBox.x, centerCircleBox.y, f10, Direction.CW);
                        f5 = holeRadius;
                        f2 = phaseX;
                        fArr2 = drawAngles;
                        fArr = absoluteAngles;
                    }
                    if (z4) {
                        double d2 = (double) (f14 * 0.017453292f);
                        i = i9;
                        f6 = f5;
                        rectF = rectF2;
                        i3 = 1;
                        i4 = i7;
                        f7 = calculateMinimumRadiusForSpacedSlice(centerCircleBox, radius, f9 * phaseY, (((float) Math.cos(d2)) * radius) + centerCircleBox.x, centerCircleBox.y + (((float) Math.sin(d2)) * radius), f14, f16);
                    } else {
                        rectF = rectF2;
                        i4 = i7;
                        i = i9;
                        f6 = f5;
                        i3 = 1;
                        f7 = 0.0f;
                    }
                    this.mInnerRectBuffer.set(centerCircleBox.x - f6, centerCircleBox.y - f6, centerCircleBox.x + f6, centerCircleBox.y + f6);
                    if (!z2 || (f6 <= 0.0f && !z4)) {
                        f = f6;
                        if (f16 % 360.0f > Utils.FLOAT_EPSILON) {
                            if (z4) {
                                double d3 = (double) ((f14 + (f16 / 2.0f)) * 0.017453292f);
                                this.mPathBuffer.lineTo(centerCircleBox.x + (((float) Math.cos(d3)) * f7), centerCircleBox.y + (f7 * ((float) Math.sin(d3))));
                            } else {
                                this.mPathBuffer.lineTo(centerCircleBox.x, centerCircleBox.y);
                            }
                        }
                    } else {
                        if (z4) {
                            if (f7 < 0.0f) {
                                f7 = -f7;
                            }
                            f8 = Math.max(f6, f7);
                        } else {
                            f8 = f6;
                        }
                        float f19 = (i4 == i3 || f8 == 0.0f) ? 0.0f : f4 / (0.017453292f * f8);
                        float f20 = rotationAngle + ((f3 + (f19 / 2.0f)) * phaseY);
                        float f21 = (f9 - f19) * phaseY;
                        if (f21 < 0.0f) {
                            f21 = 0.0f;
                        }
                        float f22 = f20 + f21;
                        if (f16 < 360.0f || f16 % 360.0f > Utils.FLOAT_EPSILON) {
                            double d4 = (double) (f22 * 0.017453292f);
                            f = f6;
                            this.mPathBuffer.lineTo(centerCircleBox.x + (((float) Math.cos(d4)) * f8), centerCircleBox.y + (f8 * ((float) Math.sin(d4))));
                            this.mPathBuffer.arcTo(this.mInnerRectBuffer, f22, -f21);
                        } else {
                            this.mPathBuffer.addCircle(centerCircleBox.x, centerCircleBox.y, f8, Direction.CCW);
                            f = f6;
                        }
                    }
                    this.mPathBuffer.close();
                    this.mBitmapCanvas.drawPath(this.mPathBuffer, this.mRenderPaint);
                    i5 = i + 1;
                    z3 = z;
                    phaseX = f2;
                    drawAngles = fArr2;
                    absoluteAngles = fArr;
                    rectF2 = rectF;
                    holeRadius = f;
                    highlightArr2 = highlightArr;
                }
            }
            i = i5;
            rectF = rectF2;
            f = holeRadius;
            z = z3;
            f2 = phaseX;
            fArr2 = drawAngles;
            fArr = absoluteAngles;
            i5 = i + 1;
            z3 = z;
            phaseX = f2;
            drawAngles = fArr2;
            absoluteAngles = fArr;
            rectF2 = rectF;
            holeRadius = f;
            highlightArr2 = highlightArr;
        }
        MPPointF.recycleInstance(centerCircleBox);
    }

    /* access modifiers changed from: protected */
    public void drawRoundedSlices(Canvas canvas) {
        float f;
        float f2;
        float f3;
        float[] fArr;
        if (this.mChart.isDrawRoundedSlicesEnabled()) {
            IPieDataSet dataSet = ((PieData) this.mChart.getData()).getDataSet();
            if (dataSet.isVisible()) {
                float phaseX = this.mAnimator.getPhaseX();
                float phaseY = this.mAnimator.getPhaseY();
                MPPointF centerCircleBox = this.mChart.getCenterCircleBox();
                float radius = this.mChart.getRadius();
                float holeRadius = (radius - ((this.mChart.getHoleRadius() * radius) / 100.0f)) / 2.0f;
                float[] drawAngles = this.mChart.getDrawAngles();
                float rotationAngle = this.mChart.getRotationAngle();
                int i = 0;
                while (i < dataSet.getEntryCount()) {
                    float f4 = drawAngles[i];
                    if (Math.abs(dataSet.getEntryForIndex(i).getY()) > Utils.FLOAT_EPSILON) {
                        double d = (double) (radius - holeRadius);
                        double d2 = (double) ((rotationAngle + f4) * phaseY);
                        fArr = drawAngles;
                        f3 = rotationAngle;
                        f2 = phaseY;
                        f = phaseX;
                        float cos = (float) ((Math.cos(Math.toRadians(d2)) * d) + ((double) centerCircleBox.x));
                        float sin = (float) ((d * Math.sin(Math.toRadians(d2))) + ((double) centerCircleBox.y));
                        this.mRenderPaint.setColor(dataSet.getColor(i));
                        this.mBitmapCanvas.drawCircle(cos, sin, holeRadius, this.mRenderPaint);
                    } else {
                        f = phaseX;
                        f2 = phaseY;
                        fArr = drawAngles;
                        f3 = rotationAngle;
                    }
                    rotationAngle = f3 + (f4 * f);
                    i++;
                    drawAngles = fArr;
                    phaseY = f2;
                    phaseX = f;
                }
                MPPointF.recycleInstance(centerCircleBox);
            }
        }
    }

    public void releaseBitmap() {
        if (this.mBitmapCanvas != null) {
            this.mBitmapCanvas.setBitmap(null);
            this.mBitmapCanvas = null;
        }
        if (this.mDrawBitmap != null) {
            ((Bitmap) this.mDrawBitmap.get()).recycle();
            this.mDrawBitmap.clear();
            this.mDrawBitmap = null;
        }
    }
}
