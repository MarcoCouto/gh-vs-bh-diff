package com.github.mikephil.charting.buffer;

import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;

public class BarBuffer extends AbstractBuffer<IBarDataSet> {
    protected float mBarWidth = 1.0f;
    protected boolean mContainsStacks = false;
    protected int mDataSetCount = 1;
    protected int mDataSetIndex = 0;
    protected boolean mInverted = false;

    public BarBuffer(int i, int i2, boolean z) {
        super(i);
        this.mDataSetCount = i2;
        this.mContainsStacks = z;
    }

    public void setBarWidth(float f) {
        this.mBarWidth = f;
    }

    public void setDataSet(int i) {
        this.mDataSetIndex = i;
    }

    public void setInverted(boolean z) {
        this.mInverted = z;
    }

    /* access modifiers changed from: protected */
    public void addBar(float f, float f2, float f3, float f4) {
        float[] fArr = this.buffer;
        int i = this.index;
        this.index = i + 1;
        fArr[i] = f;
        float[] fArr2 = this.buffer;
        int i2 = this.index;
        this.index = i2 + 1;
        fArr2[i2] = f2;
        float[] fArr3 = this.buffer;
        int i3 = this.index;
        this.index = i3 + 1;
        fArr3[i3] = f3;
        float[] fArr4 = this.buffer;
        int i4 = this.index;
        this.index = i4 + 1;
        fArr4[i4] = f4;
    }

    public void feed(IBarDataSet iBarDataSet) {
        float f;
        float f2;
        float f3;
        float f4;
        float f5;
        float entryCount = ((float) iBarDataSet.getEntryCount()) * this.phaseX;
        float f6 = this.mBarWidth / 2.0f;
        for (int i = 0; ((float) i) < entryCount; i++) {
            BarEntry barEntry = (BarEntry) iBarDataSet.getEntryForIndex(i);
            if (barEntry != null) {
                float x = barEntry.getX();
                float y = barEntry.getY();
                float[] yVals = barEntry.getYVals();
                if (!this.mContainsStacks || yVals == null) {
                    float f7 = x - f6;
                    float f8 = x + f6;
                    if (this.mInverted) {
                        float f9 = y >= 0.0f ? y : 0.0f;
                        if (y > 0.0f) {
                            y = 0.0f;
                        }
                        float f10 = y;
                        y = f9;
                        f = f10;
                    } else {
                        f = y >= 0.0f ? y : 0.0f;
                        if (y > 0.0f) {
                            y = 0.0f;
                        }
                    }
                    if (f > 0.0f) {
                        f *= this.phaseY;
                    } else {
                        y *= this.phaseY;
                    }
                    addBar(f7, f, f8, y);
                } else {
                    float f11 = -barEntry.getNegativeSum();
                    float f12 = 0.0f;
                    int i2 = 0;
                    while (i2 < yVals.length) {
                        float f13 = yVals[i2];
                        if (f13 >= 0.0f) {
                            f3 = f13 + f12;
                            f2 = f11;
                            f4 = f3;
                        } else {
                            float abs = Math.abs(f13) + f11;
                            float abs2 = Math.abs(f13) + f11;
                            float f14 = f11;
                            f4 = f12;
                            f12 = f14;
                            float f15 = abs;
                            f2 = abs2;
                            f3 = f15;
                        }
                        float f16 = x - f6;
                        float f17 = x + f6;
                        if (this.mInverted) {
                            f5 = f12 >= f3 ? f12 : f3;
                            if (f12 <= f3) {
                                f3 = f12;
                            }
                        } else {
                            float f18 = f12 >= f3 ? f12 : f3;
                            if (f12 <= f3) {
                                f3 = f12;
                            }
                            float f19 = f3;
                            f3 = f18;
                            f5 = f19;
                        }
                        addBar(f16, f3 * this.phaseY, f17, f5 * this.phaseY);
                        i2++;
                        f12 = f4;
                        f11 = f2;
                    }
                }
            }
        }
        reset();
    }
}
