package com.revmob.client;

public abstract class AdData {
    protected String appOrSite;
    protected String campaignId;
    protected String clickUrl;
    protected boolean followRedirect;
    protected String impressionUrl;
    protected boolean openInside;
    protected int parallaxDelta;

    public String getDspUrl() {
        return null;
    }

    protected AdData(String str, String str2, boolean z, String str3, boolean z2) {
        this.impressionUrl = str;
        this.clickUrl = str2;
        this.followRedirect = z;
        this.appOrSite = str3;
        this.openInside = z2;
    }

    public String getImpressionUrl() {
        return this.impressionUrl;
    }

    public String getClickUrl() {
        return this.clickUrl;
    }

    public String getClickUrl(String str) {
        if (str != null) {
            this.clickUrl = this.clickUrl.concat("&videoPosition=");
            this.clickUrl = this.clickUrl.concat(str);
        }
        return this.clickUrl;
    }

    public boolean shouldFollowRedirect() {
        return this.followRedirect;
    }

    public String getAppOrSite() {
        return this.appOrSite;
    }

    public boolean isOpenInside() {
        return this.openInside;
    }
}
