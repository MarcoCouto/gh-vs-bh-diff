package com.revmob;

public enum RevMobUserGender {
    MALE("male"),
    FEMALE("female"),
    UNDEFINED(null);
    
    private String value;

    private RevMobUserGender(String str) {
        this.value = str;
    }

    public String getValue() {
        return this.value;
    }
}
