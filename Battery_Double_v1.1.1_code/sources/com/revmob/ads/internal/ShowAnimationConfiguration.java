package com.revmob.ads.internal;

import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;

public class ShowAnimationConfiguration extends AnimationConfiguration {
    public Animation createFadeIn() {
        return new AlphaAnimation(0.0f, 1.0f);
    }

    public Animation createFadeOut() {
        return new AlphaAnimation(2.0f, 1.0f);
    }

    public Animation createSlideUp() {
        TranslateAnimation translateAnimation = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, 1.0f, 1, 0.0f);
        return translateAnimation;
    }

    public Animation createSlideDown() {
        TranslateAnimation translateAnimation = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, -2.0f, 1, 0.0f);
        return translateAnimation;
    }

    public Animation createSlideRight() {
        TranslateAnimation translateAnimation = new TranslateAnimation(1, -2.0f, 1, 0.0f, 1, 0.0f, 1, 0.0f);
        return translateAnimation;
    }

    public Animation createSlideLeft() {
        TranslateAnimation translateAnimation = new TranslateAnimation(1, 2.0f, 1, 0.0f, 1, 0.0f, 1, 0.0f);
        return translateAnimation;
    }

    public Animation createZoomIn() {
        ScaleAnimation scaleAnimation = new ScaleAnimation(0.5f, 1.0f, 0.5f, 1.0f, 1, 0.5f, 1, 0.5f);
        return scaleAnimation;
    }

    public Animation createZoomOut() {
        ScaleAnimation scaleAnimation = new ScaleAnimation(1.5f, 1.0f, 1.5f, 1.0f, 1, 0.5f, 1, 0.5f);
        return scaleAnimation;
    }

    public Animation createClockwise() {
        RotateAnimation rotateAnimation = new RotateAnimation(-90.0f, 0.0f, 1, 1.0f, 1, 0.0f);
        return rotateAnimation;
    }

    public Animation createCounterClockwise() {
        return new RotateAnimation(90.0f, 0.0f);
    }
}
