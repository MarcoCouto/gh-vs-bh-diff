package com.revmob.ads.interstitial.internal;

public interface FullscreenView {
    void update();
}
