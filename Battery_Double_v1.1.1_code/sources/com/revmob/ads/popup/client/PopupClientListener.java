package com.revmob.ads.popup.client;

import com.revmob.RevMobAdsListener;
import com.revmob.ads.internal.Ad;
import com.revmob.ads.internal.AdRevMobClientListener;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class PopupClientListener extends AdRevMobClientListener {
    public PopupClientListener(Ad ad, RevMobAdsListener revMobAdsListener) {
        super(ad, revMobAdsListener);
    }

    public void handleResponse(String str) throws JSONException {
        this.ad.updateWithData(createData(str));
    }

    public static PopupData createData(String str) throws JSONException {
        String str2;
        String str3;
        String str4;
        JSONObject jSONObject = new JSONObject(str).getJSONObject("pop_up");
        String appOrSite = getAppOrSite(jSONObject);
        boolean openInside = getOpenInside(jSONObject);
        JSONArray jSONArray = jSONObject.getJSONArray("links");
        String clickUrl = getClickUrl(jSONArray);
        boolean followRedirect = getFollowRedirect(jSONObject);
        String impressionUrl = getImpressionUrl(jSONArray);
        try {
            str2 = jSONObject.getString("message");
        } catch (JSONException unused) {
            str2 = new String();
        }
        String str5 = new String();
        String str6 = new String();
        try {
            JSONObject jSONObject2 = jSONObject.getJSONObject("sound");
            String string = jSONObject2.getString("on_show");
            try {
                str4 = string;
                str3 = jSONObject2.getString("on_click");
            } catch (JSONException unused2) {
                str5 = string;
                str3 = str6;
                str4 = str5;
                PopupData popupData = new PopupData(impressionUrl, clickUrl, followRedirect, str2, appOrSite, openInside, str4, str3);
                return popupData;
            }
        } catch (JSONException unused3) {
            str3 = str6;
            str4 = str5;
            PopupData popupData2 = new PopupData(impressionUrl, clickUrl, followRedirect, str2, appOrSite, openInside, str4, str3);
            return popupData2;
        }
        PopupData popupData22 = new PopupData(impressionUrl, clickUrl, followRedirect, str2, appOrSite, openInside, str4, str3);
        return popupData22;
    }
}
