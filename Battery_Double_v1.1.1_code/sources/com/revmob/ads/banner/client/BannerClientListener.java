package com.revmob.ads.banner.client;

import android.graphics.Bitmap;
import com.facebook.share.internal.MessengerShareContentUtility;
import com.revmob.RevMobAdsListener;
import com.revmob.ads.internal.Ad;
import com.revmob.ads.internal.AdRevMobClientListener;
import com.revmob.ads.internal.CloseAnimationConfiguration;
import com.revmob.ads.internal.ShowAnimationConfiguration;
import com.revmob.client.RevMobClient;
import com.revmob.internal.HTTPHelper;
import com.revmob.internal.RMLog;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class BannerClientListener extends AdRevMobClientListener {
    public BannerClientListener(Ad ad, RevMobAdsListener revMobAdsListener) {
        super(ad, revMobAdsListener);
    }

    /* JADX WARNING: Removed duplicated region for block: B:39:0x00f3 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x0110 A[ADDED_TO_REGION] */
    public void handleResponse(String str) throws JSONException {
        boolean z;
        String str2;
        String str3;
        String str4;
        RevMobClient.sett2(System.currentTimeMillis());
        JSONObject jSONObject = new JSONObject(str).getJSONArray("banners").getJSONObject(0);
        String appOrSite = getAppOrSite(jSONObject);
        boolean openInside = getOpenInside(jSONObject);
        JSONArray jSONArray = jSONObject.getJSONArray("links");
        String clickUrl = getClickUrl(jSONArray);
        boolean followRedirect = getFollowRedirect(jSONObject);
        String impressionUrl = getImpressionUrl(jSONArray);
        String linkByRel = getLinkByRel(jSONArray, "html");
        String linkByRel2 = getLinkByRel(jSONArray, "dsp_url");
        String linkByRel3 = getLinkByRel(jSONArray, "dsp_html");
        String linkByRel4 = getLinkByRel(jSONArray, "imageSize");
        int i = jSONObject.has("refreshTime") ? jSONObject.getInt("refreshTime") : 0;
        int[] iArr = new int[2];
        try {
            String[] split = linkByRel4.split(",");
            iArr[0] = Integer.parseInt(split[0]);
            iArr[1] = Integer.parseInt(split[1]);
        } catch (Exception unused) {
            iArr[0] = 0;
            iArr[1] = 0;
        }
        ShowAnimationConfiguration showAnimationConfiguration = new ShowAnimationConfiguration();
        CloseAnimationConfiguration closeAnimationConfiguration = new CloseAnimationConfiguration();
        try {
            JSONObject jSONObject2 = jSONObject.getJSONObject("animation");
            str2 = appOrSite;
            z = openInside;
            try {
                long j = jSONObject2.getLong("duration");
                showAnimationConfiguration.setTime(j);
                closeAnimationConfiguration.setTime(j);
                JSONArray jSONArray2 = jSONObject2.getJSONArray("show");
                for (int i2 = 0; i2 < jSONArray2.length(); i2++) {
                    showAnimationConfiguration.addAnimation(jSONArray2.getString(i2));
                }
                JSONArray jSONArray3 = jSONObject2.getJSONArray("close");
                for (int i3 = 0; i3 < jSONArray3.length(); i3++) {
                    closeAnimationConfiguration.addAnimation(jSONArray3.getString(i3));
                }
            } catch (JSONException unused2) {
            }
        } catch (JSONException unused3) {
            str2 = appOrSite;
            z = openInside;
        }
        String str5 = new String();
        String str6 = new String();
        try {
            JSONObject jSONObject3 = jSONObject.getJSONObject("sound");
            String string = jSONObject3.getString("on_show");
            try {
                str4 = jSONObject3.getString("on_click");
                str3 = string;
            } catch (JSONException unused4) {
                str5 = string;
                str3 = str5;
                str4 = str6;
                Bitmap bitmap = null;
                if (linkByRel2 == null) {
                }
                RMLog.d("Banner DSP");
                Bitmap bitmap2 = bitmap;
                if (clickUrl == null) {
                }
            }
        } catch (JSONException unused5) {
            str3 = str5;
            str4 = str6;
            Bitmap bitmap3 = null;
            if (linkByRel2 == null) {
            }
            RMLog.d("Banner DSP");
            Bitmap bitmap22 = bitmap3;
            if (clickUrl == null) {
            }
        }
        Bitmap bitmap32 = null;
        if (linkByRel2 == null || linkByRel3 != null) {
            RMLog.d("Banner DSP");
        } else if (linkByRel == null) {
            bitmap32 = new HTTPHelper().downloadBitmap(getLinkByRel(jSONArray, MessengerShareContentUtility.MEDIA_IMAGE), linkByRel4);
        }
        Bitmap bitmap222 = bitmap32;
        if ((clickUrl == null && (linkByRel != null || bitmap222 != null)) || linkByRel2 != null || linkByRel3 != null) {
            RevMobClient.sett3(System.currentTimeMillis());
            BannerData bannerData = r2;
            Ad ad = this.ad;
            BannerData bannerData2 = new BannerData(impressionUrl, clickUrl, followRedirect, bitmap222, linkByRel, linkByRel2, linkByRel3, showAnimationConfiguration, closeAnimationConfiguration, str2, z, str3, str4, iArr, i);
            ad.updateWithData(bannerData);
        }
    }
}
