package com.revmob.internal;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.AsyncTask;
import android.os.Build.VERSION;
import android.util.Log;
import com.revmob.ads.interstitial.client.AeSimpleSHA1;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.NoSuchAlgorithmException;
import org.apache.http.util.ByteArrayBuffer;

@SuppressLint({"NewApi"})
public class DownloadManager extends AsyncTask<String, Void, String> {
    private Activity activity;
    private boolean callResponse;
    private AsyncTaskCompleteListener callback;
    private File file;
    private String filePath;
    private String filename;
    private ConnectionHandler handler;
    private boolean isSuccessful;
    private URL url;
    private boolean willCache;

    public DownloadManager(Activity activity2, String str, String str2, AsyncTaskCompleteListener asyncTaskCompleteListener) {
        this.activity = activity2;
        this.callback = asyncTaskCompleteListener;
        this.willCache = false;
        this.callResponse = false;
        try {
            this.filename = new AeSimpleSHA1().SHA1(str);
        } catch (NoSuchAlgorithmException e) {
            this.filename = str2;
            e.printStackTrace();
        } catch (UnsupportedEncodingException e2) {
            this.filename = str2;
            e2.printStackTrace();
        }
        try {
            this.url = new URL(str);
        } catch (MalformedURLException e3) {
            Log.e("download manager malformed url error, download url", str);
            e3.printStackTrace();
        }
    }

    public DownloadManager(Activity activity2, String str, String str2, ConnectionHandler connectionHandler, boolean z, boolean z2) {
        this.activity = activity2;
        this.handler = connectionHandler;
        this.willCache = z;
        this.callResponse = z2;
        try {
            this.filename = new AeSimpleSHA1().SHA1(str);
        } catch (NoSuchAlgorithmException e) {
            this.filename = str2;
            e.printStackTrace();
        } catch (UnsupportedEncodingException e2) {
            this.filename = str2;
            e2.printStackTrace();
        }
        try {
            this.url = new URL(str);
        } catch (MalformedURLException e3) {
            e3.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(String str) {
        super.onPostExecute(str);
        Log.e("onPostExecute", "download manager");
        if (this.callback != null) {
            this.callback.onTaskComplete(str);
        } else if (this.callResponse) {
            this.handler.onResponse(str, this.isSuccessful);
        }
    }

    /* access modifiers changed from: protected */
    public String doInBackground(String... strArr) {
        try {
            if (!this.willCache) {
                RMLog.d("Download Manager : wont cache file");
            } else {
                RMLog.d("Download Manager : will cache file");
            }
            createFilePath(this.activity);
            this.file = new File(this.filePath, this.filename);
            this.filePath = this.file.getAbsolutePath();
            if (!this.file.exists()) {
                long currentTimeMillis = System.currentTimeMillis();
                RMLog.d("Download begining");
                StringBuilder sb = new StringBuilder();
                sb.append("Download url:");
                sb.append(this.url);
                RMLog.d(sb.toString());
                StringBuilder sb2 = new StringBuilder();
                sb2.append("Downloaded file name:");
                sb2.append(this.filename);
                RMLog.d(sb2.toString());
                StringBuilder sb3 = new StringBuilder();
                sb3.append("filepath ");
                sb3.append(this.filePath);
                RMLog.d(sb3.toString());
                BufferedInputStream bufferedInputStream = new BufferedInputStream(this.url.openConnection().getInputStream());
                ByteArrayBuffer byteArrayBuffer = new ByteArrayBuffer(50);
                while (true) {
                    int read = bufferedInputStream.read();
                    if (read == -1) {
                        break;
                    }
                    byteArrayBuffer.append((byte) read);
                }
                FileOutputStream fileOutputStream = new FileOutputStream(this.file);
                fileOutputStream.write(byteArrayBuffer.toByteArray());
                fileOutputStream.close();
                if (VERSION.SDK_INT >= 9 && VERSION.SDK_INT < 15) {
                    this.file.setReadable(true, false);
                }
                StringBuilder sb4 = new StringBuilder();
                sb4.append("Download ready in ");
                sb4.append((System.currentTimeMillis() - currentTimeMillis) / 1000);
                sb4.append(" sec");
                RMLog.d(sb4.toString());
            } else {
                Log.e("download Managaer", "file alreadyExists");
                StringBuilder sb5 = new StringBuilder();
                sb5.append("");
                sb5.append(this.filePath);
                Log.e("file path", sb5.toString());
                StringBuilder sb6 = new StringBuilder();
                sb6.append("");
                sb6.append(this.filename);
                Log.e("file name", sb6.toString());
            }
            this.isSuccessful = true;
        } catch (IOException e) {
            this.isSuccessful = false;
            StringBuilder sb7 = new StringBuilder();
            sb7.append("Download Error: ");
            sb7.append(e);
            RMLog.i(sb7.toString());
            e.printStackTrace();
        }
        return null;
    }

    private void createFilePath(Activity activity2) {
        if (!this.willCache) {
            this.filePath = activity2.getApplicationContext().getFilesDir().getPath();
        } else {
            this.filePath = activity2.getApplicationContext().getCacheDir().getPath();
        }
    }

    public String getFilePath() {
        return this.filePath;
    }

    public File getFile() {
        return this.file;
    }

    public boolean getIsSuccessful() {
        return this.isSuccessful;
    }
}
