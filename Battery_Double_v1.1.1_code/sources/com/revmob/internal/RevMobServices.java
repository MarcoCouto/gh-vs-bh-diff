package com.revmob.internal;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Handler;
import android.os.IBinder;
import android.preference.PreferenceManager;
import com.google.android.gms.measurement.AppMeasurement.Param;
import com.revmob.client.RevMobClient;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class RevMobServices extends Service {
    private int NOTIFY_FREQUENCY;
    private String NOTIFY_URL;
    private int SCAN_FREQUENCY;
    Handler handler = new Handler();
    private JSONObject mRevmobJSON;
    /* access modifiers changed from: private */
    public Runnable notifyService;
    /* access modifiers changed from: private */
    public Runnable runningAppsService;

    public IBinder onBind(Intent intent) {
        return null;
    }

    public int onStartCommand(Intent intent, int i, int i2) {
        RMLog.i("onStartCommand beacon");
        SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        try {
            this.mRevmobJSON = new JSONObject(defaultSharedPreferences.getString("deviceIdentifierJSON", ""));
            parseParameters(new JSONObject(defaultSharedPreferences.getString("runningAppsConfiguration", "")));
            startRunningAppsService(this.SCAN_FREQUENCY, this.NOTIFY_FREQUENCY);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return 2;
    }

    private void startRunningAppsService(int i, int i2) {
        if (i > 0) {
            startScanRunningApps(i);
        }
        if (i2 > 0) {
            startNotifyRunningApps(i2);
        }
    }

    private void startScanRunningApps(final int i) {
        this.runningAppsService = new Runnable() {
            public void run() {
                RevMobServices.this.detectRunningApps();
                RevMobServices.this.handler.postDelayed(RevMobServices.this.runningAppsService, (long) (i * 1000));
            }
        };
        this.runningAppsService.run();
    }

    private void startNotifyRunningApps(final int i) {
        this.notifyService = new Runnable() {
            public void run() {
                RevMobServices.this.notifyRunningApps();
                RevMobServices.this.handler.postDelayed(RevMobServices.this.notifyService, (long) (i * 1000));
            }
        };
        this.notifyService.run();
    }

    /* access modifiers changed from: private */
    public void detectRunningApps() {
        List runningAppProcesses = ((ActivityManager) getSystemService("activity")).getRunningAppProcesses();
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < runningAppProcesses.size(); i++) {
            arrayList.add(((RunningAppProcessInfo) runningAppProcesses.get(i)).processName);
        }
        saveRunningApps(arrayList);
        RMLog.i("fetchRunningApps");
    }

    private void saveRunningApps(ArrayList<String> arrayList) {
        JSONArray jSONArray;
        SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        Editor edit = defaultSharedPreferences.edit();
        JSONArray jSONArray2 = new JSONArray();
        try {
            jSONArray = new JSONArray(defaultSharedPreferences.getString("runningApps", new JSONArray().toString()));
            try {
                JSONObject jSONObject = new JSONObject();
                jSONObject.put("apps", new JSONArray(arrayList));
                jSONObject.put(Param.TIMESTAMP, getCurrentTimestamp());
                jSONArray.put(jSONObject);
            } catch (JSONException e) {
                e = e;
            }
        } catch (JSONException e2) {
            e = e2;
            jSONArray = jSONArray2;
            e.printStackTrace();
            edit.putString("runningApps", jSONArray.toString());
            edit.commit();
        }
        edit.putString("runningApps", jSONArray.toString());
        edit.commit();
    }

    /* access modifiers changed from: private */
    public void notifyRunningApps() {
        SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        Editor edit = defaultSharedPreferences.edit();
        try {
            try {
                this.mRevmobJSON.put("runningApps", new JSONArray(defaultSharedPreferences.getString("runningApps", new JSONArray().toString())));
                if (this.mRevmobJSON.optJSONObject("identifiers").length() > 0) {
                    RevMobClient.getInstance().serverRequest(this.NOTIFY_URL, this.mRevmobJSON.toString(), null);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
        edit.remove("runningApps");
        edit.commit();
    }

    private void parseParameters(JSONObject jSONObject) {
        try {
            this.SCAN_FREQUENCY = jSONObject.getInt("scanFrequency");
            this.NOTIFY_FREQUENCY = jSONObject.getInt("notifyFrequency");
            this.NOTIFY_URL = jSONObject.getString("url");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private int getCurrentTimestamp() {
        return ((int) System.currentTimeMillis()) / 1000;
    }
}
