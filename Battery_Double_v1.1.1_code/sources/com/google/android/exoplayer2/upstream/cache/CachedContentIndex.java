package com.google.android.exoplayer2.upstream.cache;

import android.util.Log;
import android.util.SparseArray;
import com.google.android.exoplayer2.upstream.cache.Cache.CacheException;
import com.google.android.exoplayer2.util.Assertions;
import com.google.android.exoplayer2.util.AtomicFile;
import com.google.android.exoplayer2.util.ReusableBufferedOutputStream;
import com.google.android.exoplayer2.util.Util;
import java.io.BufferedInputStream;
import java.io.Closeable;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Random;
import java.util.Set;
import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

final class CachedContentIndex {
    public static final String FILE_NAME = "cached_content_index.exi";
    private static final int FLAG_ENCRYPTED_INDEX = 1;
    private static final String TAG = "CachedContentIndex";
    private static final int VERSION = 1;
    private final AtomicFile atomicFile;
    private ReusableBufferedOutputStream bufferedOutputStream;
    private boolean changed;
    private final Cipher cipher;
    private final SparseArray<String> idToKey;
    private final HashMap<String, CachedContent> keyToContent;
    private final SecretKeySpec secretKeySpec;

    public CachedContentIndex(File file) {
        this(file, null);
    }

    public CachedContentIndex(File file, byte[] bArr) {
        if (bArr != null) {
            Assertions.checkArgument(bArr.length == 16);
            try {
                this.cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
                this.secretKeySpec = new SecretKeySpec(bArr, "AES");
            } catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
                throw new IllegalStateException(e);
            }
        } else {
            this.cipher = null;
            this.secretKeySpec = null;
        }
        this.keyToContent = new HashMap<>();
        this.idToKey = new SparseArray<>();
        this.atomicFile = new AtomicFile(new File(file, FILE_NAME));
    }

    public void load() {
        Assertions.checkState(!this.changed);
        if (!readFile()) {
            this.atomicFile.delete();
            this.keyToContent.clear();
            this.idToKey.clear();
        }
    }

    public void store() throws CacheException {
        if (this.changed) {
            writeFile();
            this.changed = false;
        }
    }

    public CachedContent add(String str) {
        CachedContent cachedContent = (CachedContent) this.keyToContent.get(str);
        return cachedContent == null ? addNew(str, -1) : cachedContent;
    }

    public CachedContent get(String str) {
        return (CachedContent) this.keyToContent.get(str);
    }

    public Collection<CachedContent> getAll() {
        return this.keyToContent.values();
    }

    public int assignIdForKey(String str) {
        return add(str).id;
    }

    public String getKeyForId(int i) {
        return (String) this.idToKey.get(i);
    }

    public void removeEmpty(String str) {
        CachedContent cachedContent = (CachedContent) this.keyToContent.remove(str);
        if (cachedContent != null) {
            Assertions.checkState(cachedContent.isEmpty());
            this.idToKey.remove(cachedContent.id);
            this.changed = true;
        }
    }

    public void removeEmpty() {
        LinkedList linkedList = new LinkedList();
        for (CachedContent cachedContent : this.keyToContent.values()) {
            if (cachedContent.isEmpty()) {
                linkedList.add(cachedContent.key);
            }
        }
        Iterator it = linkedList.iterator();
        while (it.hasNext()) {
            removeEmpty((String) it.next());
        }
    }

    public Set<String> getKeys() {
        return this.keyToContent.keySet();
    }

    public void setContentLength(String str, long j) {
        CachedContent cachedContent = get(str);
        if (cachedContent == null) {
            addNew(str, j);
        } else if (cachedContent.getLength() != j) {
            cachedContent.setLength(j);
            this.changed = true;
        }
    }

    public long getContentLength(String str) {
        CachedContent cachedContent = get(str);
        if (cachedContent == null) {
            return -1;
        }
        return cachedContent.getLength();
    }

    /* JADX WARNING: Removed duplicated region for block: B:53:0x009a  */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x00a1  */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x00a8  */
    private boolean readFile() {
        DataInputStream dataInputStream;
        IOException e;
        DataInputStream dataInputStream2 = null;
        try {
            BufferedInputStream bufferedInputStream = new BufferedInputStream(this.atomicFile.openRead());
            dataInputStream = new DataInputStream(bufferedInputStream);
            try {
                if (dataInputStream.readInt() != 1) {
                    if (dataInputStream != null) {
                        Util.closeQuietly((Closeable) dataInputStream);
                    }
                    return false;
                }
                if ((dataInputStream.readInt() & 1) == 0) {
                    if (this.cipher != null) {
                        this.changed = true;
                    }
                    dataInputStream2 = dataInputStream;
                } else if (this.cipher == null) {
                    if (dataInputStream != null) {
                        Util.closeQuietly((Closeable) dataInputStream);
                    }
                    return false;
                } else {
                    byte[] bArr = new byte[16];
                    dataInputStream.readFully(bArr);
                    this.cipher.init(2, this.secretKeySpec, new IvParameterSpec(bArr));
                    dataInputStream2 = new DataInputStream(new CipherInputStream(bufferedInputStream, this.cipher));
                }
                int readInt = dataInputStream2.readInt();
                int i = 0;
                for (int i2 = 0; i2 < readInt; i2++) {
                    CachedContent cachedContent = new CachedContent(dataInputStream2);
                    add(cachedContent);
                    i += cachedContent.headerHashCode();
                }
                if (dataInputStream2.readInt() != i) {
                    if (dataInputStream2 != null) {
                        Util.closeQuietly((Closeable) dataInputStream2);
                    }
                    return false;
                }
                if (dataInputStream2 != null) {
                    Util.closeQuietly((Closeable) dataInputStream2);
                }
                return true;
            } catch (InvalidAlgorithmParameterException | InvalidKeyException e2) {
                throw new IllegalStateException(e2);
            } catch (FileNotFoundException unused) {
                if (dataInputStream != null) {
                }
                return false;
            } catch (IOException e3) {
                e = e3;
                try {
                    Log.e(TAG, "Error reading cache content index file.", e);
                    if (dataInputStream != null) {
                    }
                    return false;
                } catch (Throwable th) {
                    th = th;
                    if (dataInputStream != null) {
                    }
                    throw th;
                }
            }
        } catch (FileNotFoundException unused2) {
            dataInputStream = dataInputStream2;
            if (dataInputStream != null) {
                Util.closeQuietly((Closeable) dataInputStream);
            }
            return false;
        } catch (IOException e4) {
            dataInputStream = dataInputStream2;
            e = e4;
            Log.e(TAG, "Error reading cache content index file.", e);
            if (dataInputStream != null) {
                Util.closeQuietly((Closeable) dataInputStream);
            }
            return false;
        } catch (Throwable th2) {
            th = th2;
            dataInputStream = dataInputStream2;
            if (dataInputStream != null) {
                Util.closeQuietly((Closeable) dataInputStream);
            }
            throw th;
        }
    }

    private void writeFile() throws CacheException {
        DataOutputStream dataOutputStream;
        IOException e;
        try {
            OutputStream startWrite = this.atomicFile.startWrite();
            if (this.bufferedOutputStream == null) {
                this.bufferedOutputStream = new ReusableBufferedOutputStream(startWrite);
            } else {
                this.bufferedOutputStream.reset(startWrite);
            }
            dataOutputStream = new DataOutputStream(this.bufferedOutputStream);
            try {
                dataOutputStream.writeInt(1);
                int i = 0;
                dataOutputStream.writeInt(this.cipher != null ? 1 : 0);
                if (this.cipher != null) {
                    byte[] bArr = new byte[16];
                    new Random().nextBytes(bArr);
                    dataOutputStream.write(bArr);
                    this.cipher.init(1, this.secretKeySpec, new IvParameterSpec(bArr));
                    dataOutputStream.flush();
                    dataOutputStream = new DataOutputStream(new CipherOutputStream(this.bufferedOutputStream, this.cipher));
                }
                dataOutputStream.writeInt(this.keyToContent.size());
                for (CachedContent cachedContent : this.keyToContent.values()) {
                    cachedContent.writeToStream(dataOutputStream);
                    i += cachedContent.headerHashCode();
                }
                dataOutputStream.writeInt(i);
                this.atomicFile.endWrite(dataOutputStream);
                Util.closeQuietly((Closeable) null);
            } catch (InvalidAlgorithmParameterException | InvalidKeyException e2) {
                throw new IllegalStateException(e2);
            } catch (IOException e3) {
                e = e3;
                try {
                    throw new CacheException(e);
                } catch (Throwable th) {
                    th = th;
                    Util.closeQuietly((Closeable) dataOutputStream);
                    throw th;
                }
            }
        } catch (IOException e4) {
            IOException iOException = e4;
            dataOutputStream = null;
            e = iOException;
            throw new CacheException(e);
        } catch (Throwable th2) {
            Throwable th3 = th2;
            dataOutputStream = null;
            th = th3;
            Util.closeQuietly((Closeable) dataOutputStream);
            throw th;
        }
    }

    private void add(CachedContent cachedContent) {
        this.keyToContent.put(cachedContent.key, cachedContent);
        this.idToKey.put(cachedContent.id, cachedContent.key);
    }

    /* access modifiers changed from: 0000 */
    public void addNew(CachedContent cachedContent) {
        add(cachedContent);
        this.changed = true;
    }

    private CachedContent addNew(String str, long j) {
        CachedContent cachedContent = new CachedContent(getNewId(this.idToKey), str, j);
        addNew(cachedContent);
        return cachedContent;
    }

    public static int getNewId(SparseArray<String> sparseArray) {
        int i;
        int size = sparseArray.size();
        if (size == 0) {
            i = 0;
        } else {
            i = sparseArray.keyAt(size - 1) + 1;
        }
        if (i < 0) {
            int i2 = 0;
            while (i < size && i == sparseArray.keyAt(i)) {
                i2 = i + 1;
            }
        }
        return i;
    }
}
