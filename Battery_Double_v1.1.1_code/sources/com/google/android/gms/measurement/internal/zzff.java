package com.google.android.gms.measurement.internal;

import android.support.annotation.WorkerThread;

final class zzff extends zzy {
    private final /* synthetic */ zzfd zzatd;

    zzff(zzfd zzfd, zzct zzct) {
        this.zzatd = zzfd;
        super(zzct);
    }

    @WorkerThread
    public final void run() {
        this.zzatd.zzlq();
    }
}
