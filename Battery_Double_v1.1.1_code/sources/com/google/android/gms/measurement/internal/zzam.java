package com.google.android.gms.measurement.internal;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Build.VERSION;
import android.support.annotation.WorkerThread;
import android.text.TextUtils;
import com.facebook.internal.AnalyticsEvents;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.GoogleServices;
import com.google.android.gms.common.internal.StringResourceValueReader;
import com.google.android.gms.common.util.Clock;
import com.google.android.gms.common.util.VisibleForTesting;
import com.google.android.gms.common.wrappers.InstantApps;

public final class zzam extends zzf {
    private String zzafi;
    private String zzafp;
    private long zzafs;
    private String zzafv;
    private int zzagp;
    private int zzalk;
    private long zzall;
    private String zztr;
    private String zzts;
    private String zztt;

    zzam(zzbw zzbw) {
        super(zzbw);
    }

    /* access modifiers changed from: protected */
    public final boolean zzgy() {
        return true;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x00a7  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00bc  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x00d0  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x00fb  */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x019f A[Catch:{ IllegalStateException -> 0x01d0 }] */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x01a2 A[Catch:{ IllegalStateException -> 0x01d0 }] */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x01ab A[Catch:{ IllegalStateException -> 0x01d0 }] */
    /* JADX WARNING: Removed duplicated region for block: B:76:0x01be A[Catch:{ IllegalStateException -> 0x01d0 }] */
    /* JADX WARNING: Removed duplicated region for block: B:81:0x01e8  */
    /* JADX WARNING: Removed duplicated region for block: B:83:0x01f3  */
    public final void zzgz() {
        boolean z;
        boolean z2;
        String googleAppId;
        String str = "unknown";
        String str2 = AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN;
        String str3 = AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN;
        String packageName = getContext().getPackageName();
        PackageManager packageManager = getContext().getPackageManager();
        int i = Integer.MIN_VALUE;
        if (packageManager == null) {
            zzgt().zzjg().zzg("PackageManager is null, app identity information might be inaccurate. appId", zzas.zzbw(packageName));
        } else {
            try {
                str = packageManager.getInstallerPackageName(packageName);
            } catch (IllegalArgumentException unused) {
                zzgt().zzjg().zzg("Error retrieving app installer package name. appId", zzas.zzbw(packageName));
            }
            if (str == null) {
                str = "manual_install";
            } else if ("com.android.vending".equals(str)) {
                str = "";
            }
            try {
                PackageInfo packageInfo = packageManager.getPackageInfo(getContext().getPackageName(), 0);
                if (packageInfo != null) {
                    CharSequence applicationLabel = packageManager.getApplicationLabel(packageInfo.applicationInfo);
                    if (!TextUtils.isEmpty(applicationLabel)) {
                        str3 = applicationLabel.toString();
                    }
                    String str4 = packageInfo.versionName;
                    try {
                        i = packageInfo.versionCode;
                        str2 = str4;
                    } catch (NameNotFoundException unused2) {
                        str2 = str4;
                        zzgt().zzjg().zze("Error retrieving package info. appId, appName", zzas.zzbw(packageName), str3);
                        this.zztt = packageName;
                        this.zzafp = str;
                        this.zzts = str2;
                        this.zzalk = i;
                        this.zztr = str3;
                        this.zzall = 0;
                        zzgw();
                        Status initialize = GoogleServices.initialize(getContext());
                        z = true;
                        z2 = (initialize == null && initialize.isSuccess()) | (TextUtils.isEmpty(this.zzada.zzko()) && "am".equals(this.zzada.zzkp()));
                        if (!z2) {
                        }
                        if (z2) {
                        }
                        z = false;
                        this.zzafi = "";
                        this.zzafv = "";
                        this.zzafs = 0;
                        zzgw();
                        this.zzafv = this.zzada.zzko();
                        googleAppId = GoogleServices.getGoogleAppId();
                        this.zzafi = !TextUtils.isEmpty(googleAppId) ? "" : googleAppId;
                        if (!TextUtils.isEmpty(googleAppId)) {
                        }
                        if (z) {
                        }
                        if (VERSION.SDK_INT < 16) {
                        }
                    }
                }
            } catch (NameNotFoundException unused3) {
                zzgt().zzjg().zze("Error retrieving package info. appId, appName", zzas.zzbw(packageName), str3);
                this.zztt = packageName;
                this.zzafp = str;
                this.zzts = str2;
                this.zzalk = i;
                this.zztr = str3;
                this.zzall = 0;
                zzgw();
                Status initialize2 = GoogleServices.initialize(getContext());
                z = true;
                z2 = (initialize2 == null && initialize2.isSuccess()) | (TextUtils.isEmpty(this.zzada.zzko()) && "am".equals(this.zzada.zzkp()));
                if (!z2) {
                }
                if (z2) {
                }
                z = false;
                this.zzafi = "";
                this.zzafv = "";
                this.zzafs = 0;
                zzgw();
                this.zzafv = this.zzada.zzko();
                googleAppId = GoogleServices.getGoogleAppId();
                this.zzafi = !TextUtils.isEmpty(googleAppId) ? "" : googleAppId;
                if (!TextUtils.isEmpty(googleAppId)) {
                }
                if (z) {
                }
                if (VERSION.SDK_INT < 16) {
                }
            }
        }
        this.zztt = packageName;
        this.zzafp = str;
        this.zzts = str2;
        this.zzalk = i;
        this.zztr = str3;
        this.zzall = 0;
        zzgw();
        Status initialize22 = GoogleServices.initialize(getContext());
        z = true;
        z2 = (initialize22 == null && initialize22.isSuccess()) | (TextUtils.isEmpty(this.zzada.zzko()) && "am".equals(this.zzada.zzkp()));
        if (!z2) {
            if (initialize22 == null) {
                zzgt().zzjg().zzby("GoogleService failed to initialize (no status)");
            } else {
                zzgt().zzjg().zze("GoogleService failed to initialize, status", Integer.valueOf(initialize22.getStatusCode()), initialize22.getStatusMessage());
            }
        }
        if (z2) {
            Boolean zzia = zzgv().zzia();
            if (zzgv().zzhz()) {
                if (this.zzada.zzkn()) {
                    zzgt().zzjm().zzby("Collection disabled with firebase_analytics_collection_deactivated=1");
                }
            } else if (zzia == null || zzia.booleanValue()) {
                if (zzia != null || !GoogleServices.isMeasurementExplicitlyDisabled()) {
                    zzgt().zzjo().zzby("Collection enabled");
                    this.zzafi = "";
                    this.zzafv = "";
                    this.zzafs = 0;
                    zzgw();
                    if (!TextUtils.isEmpty(this.zzada.zzko()) && "am".equals(this.zzada.zzkp())) {
                        this.zzafv = this.zzada.zzko();
                    }
                    googleAppId = GoogleServices.getGoogleAppId();
                    this.zzafi = !TextUtils.isEmpty(googleAppId) ? "" : googleAppId;
                    if (!TextUtils.isEmpty(googleAppId)) {
                        this.zzafv = new StringResourceValueReader(getContext()).getString("admob_app_id");
                    }
                    if (z) {
                        zzgt().zzjo().zze("App package, google app id", this.zztt, this.zzafi);
                    }
                    if (VERSION.SDK_INT < 16) {
                        this.zzagp = InstantApps.isInstantApp(getContext()) ? 1 : 0;
                        return;
                    } else {
                        this.zzagp = 0;
                        return;
                    }
                } else {
                    zzgt().zzjm().zzby("Collection disabled with google_app_measurement_enable=0");
                }
            } else if (this.zzada.zzkn()) {
                zzgt().zzjm().zzby("Collection disabled with firebase_analytics_collection_enabled=0");
            }
        }
        z = false;
        this.zzafi = "";
        this.zzafv = "";
        this.zzafs = 0;
        zzgw();
        this.zzafv = this.zzada.zzko();
        try {
            googleAppId = GoogleServices.getGoogleAppId();
            this.zzafi = !TextUtils.isEmpty(googleAppId) ? "" : googleAppId;
            if (!TextUtils.isEmpty(googleAppId)) {
            }
            if (z) {
            }
        } catch (IllegalStateException e) {
            zzgt().zzjg().zze("getGoogleAppId or isMeasurementEnabled failed with exception. appId", zzas.zzbw(packageName), e);
        }
        if (VERSION.SDK_INT < 16) {
        }
    }

    /* access modifiers changed from: 0000 */
    @WorkerThread
    public final zzk zzbs(String str) {
        String str2;
        zzaf();
        zzgg();
        String zzal = zzal();
        String gmpAppId = getGmpAppId();
        zzcl();
        String str3 = this.zzts;
        long zzjd = (long) zzjd();
        zzcl();
        String str4 = this.zzafp;
        long zzhh = zzgv().zzhh();
        zzcl();
        zzaf();
        if (this.zzall == 0) {
            this.zzall = this.zzada.zzgr().zzd(getContext(), getContext().getPackageName());
        }
        long j = this.zzall;
        boolean isEnabled = this.zzada.isEnabled();
        boolean z = !zzgu().zzanq;
        zzaf();
        zzgg();
        if (!zzgv().zzaz(this.zztt) || this.zzada.isEnabled()) {
            str2 = zzjc();
        } else {
            str2 = null;
        }
        String str5 = str2;
        zzcl();
        boolean z2 = z;
        String str6 = str5;
        long j2 = this.zzafs;
        long zzkt = this.zzada.zzkt();
        int zzje = zzje();
        zzq zzgv = zzgv();
        zzgv.zzgg();
        Boolean zzar = zzgv.zzar("google_analytics_adid_collection_enabled");
        boolean booleanValue = Boolean.valueOf(zzar == null || zzar.booleanValue()).booleanValue();
        zzq zzgv2 = zzgv();
        zzgv2.zzgg();
        Boolean zzar2 = zzgv2.zzar("google_analytics_ssaid_collection_enabled");
        zzk zzk = new zzk(zzal, gmpAppId, str3, zzjd, str4, zzhh, j, str, isEnabled, z2, str6, j2, zzkt, zzje, booleanValue, Boolean.valueOf(zzar2 == null || zzar2.booleanValue()).booleanValue(), zzgu().zzkb(), zzhb());
        return zzk;
    }

    @WorkerThread
    @VisibleForTesting
    private final String zzjc() {
        try {
            Class loadClass = getContext().getClassLoader().loadClass("com.google.firebase.analytics.FirebaseAnalytics");
            if (loadClass == null) {
                return null;
            }
            try {
                Object invoke = loadClass.getDeclaredMethod("getInstance", new Class[]{Context.class}).invoke(null, new Object[]{getContext()});
                if (invoke == null) {
                    return null;
                }
                try {
                    return (String) loadClass.getDeclaredMethod("getFirebaseInstanceId", new Class[0]).invoke(invoke, new Object[0]);
                } catch (Exception unused) {
                    zzgt().zzjl().zzby("Failed to retrieve Firebase Instance Id");
                    return null;
                }
            } catch (Exception unused2) {
                zzgt().zzjk().zzby("Failed to obtain Firebase Analytics instance");
                return null;
            }
        } catch (ClassNotFoundException unused3) {
            return null;
        }
    }

    /* access modifiers changed from: 0000 */
    public final String zzal() {
        zzcl();
        return this.zztt;
    }

    /* access modifiers changed from: 0000 */
    public final String getGmpAppId() {
        zzcl();
        return this.zzafi;
    }

    /* access modifiers changed from: 0000 */
    public final String zzhb() {
        zzcl();
        return this.zzafv;
    }

    /* access modifiers changed from: 0000 */
    public final int zzjd() {
        zzcl();
        return this.zzalk;
    }

    /* access modifiers changed from: 0000 */
    public final int zzje() {
        zzcl();
        return this.zzagp;
    }

    public final /* bridge */ /* synthetic */ void zzgf() {
        super.zzgf();
    }

    public final /* bridge */ /* synthetic */ void zzgg() {
        super.zzgg();
    }

    public final /* bridge */ /* synthetic */ void zzgh() {
        super.zzgh();
    }

    public final /* bridge */ /* synthetic */ void zzaf() {
        super.zzaf();
    }

    public final /* bridge */ /* synthetic */ zza zzgi() {
        return super.zzgi();
    }

    public final /* bridge */ /* synthetic */ zzda zzgj() {
        return super.zzgj();
    }

    public final /* bridge */ /* synthetic */ zzam zzgk() {
        return super.zzgk();
    }

    public final /* bridge */ /* synthetic */ zzeb zzgl() {
        return super.zzgl();
    }

    public final /* bridge */ /* synthetic */ zzdy zzgm() {
        return super.zzgm();
    }

    public final /* bridge */ /* synthetic */ zzao zzgn() {
        return super.zzgn();
    }

    public final /* bridge */ /* synthetic */ zzfd zzgo() {
        return super.zzgo();
    }

    public final /* bridge */ /* synthetic */ zzaa zzgp() {
        return super.zzgp();
    }

    public final /* bridge */ /* synthetic */ Clock zzbx() {
        return super.zzbx();
    }

    public final /* bridge */ /* synthetic */ Context getContext() {
        return super.getContext();
    }

    public final /* bridge */ /* synthetic */ zzaq zzgq() {
        return super.zzgq();
    }

    public final /* bridge */ /* synthetic */ zzfy zzgr() {
        return super.zzgr();
    }

    public final /* bridge */ /* synthetic */ zzbr zzgs() {
        return super.zzgs();
    }

    public final /* bridge */ /* synthetic */ zzas zzgt() {
        return super.zzgt();
    }

    public final /* bridge */ /* synthetic */ zzbd zzgu() {
        return super.zzgu();
    }

    public final /* bridge */ /* synthetic */ zzq zzgv() {
        return super.zzgv();
    }

    public final /* bridge */ /* synthetic */ zzn zzgw() {
        return super.zzgw();
    }
}
