package com.google.android.gms.measurement.internal;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.net.Uri.Builder;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.provider.Settings.Secure;
import android.support.annotation.NonNull;
import android.support.annotation.WorkerThread;
import android.support.v4.util.ArrayMap;
import android.text.TextUtils;
import android.util.Pair;
import com.applovin.sdk.AppLovinErrorCodes;
import com.facebook.internal.AnalyticsEvents;
import com.facebook.share.internal.ShareConstants;
import com.github.mikephil.charting.utils.Utils;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.source.dash.DashMediaSource;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.util.ArrayUtils;
import com.google.android.gms.common.util.Clock;
import com.google.android.gms.common.util.VisibleForTesting;
import com.google.android.gms.common.wrappers.Wrappers;
import com.google.android.gms.internal.measurement.zzfp;
import com.google.android.gms.internal.measurement.zzft;
import com.google.android.gms.internal.measurement.zzfu;
import com.google.android.gms.internal.measurement.zzfv;
import com.google.android.gms.internal.measurement.zzfw;
import com.google.android.gms.internal.measurement.zzfz;
import com.google.android.gms.internal.measurement.zzxz;
import com.google.android.gms.measurement.AppMeasurement;
import com.google.firebase.analytics.FirebaseAnalytics.Event;
import com.google.firebase.analytics.FirebaseAnalytics.Param;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class zzfo implements zzct {
    private static volatile zzfo zzatg;
    private final zzbw zzada;
    private zzbq zzath;
    private zzaw zzati;
    private zzt zzatj;
    private zzbb zzatk;
    private zzfk zzatl;
    private zzm zzatm;
    private final zzfu zzatn;
    private zzdv zzato;
    private boolean zzatp;
    private boolean zzatq;
    @VisibleForTesting
    private long zzatr;
    private List<Runnable> zzats;
    private int zzatt;
    private int zzatu;
    private boolean zzatv;
    private boolean zzatw;
    private boolean zzatx;
    private FileLock zzaty;
    private FileChannel zzatz;
    private List<Long> zzaua;
    private List<Long> zzaub;
    private long zzauc;
    private boolean zzvz;

    class zza implements zzv {
        zzfw zzaug;
        List<Long> zzauh;
        List<zzft> zzaui;
        private long zzauj;

        private zza() {
        }

        public final void zzb(zzfw zzfw) {
            Preconditions.checkNotNull(zzfw);
            this.zzaug = zzfw;
        }

        public final boolean zza(long j, zzft zzft) {
            Preconditions.checkNotNull(zzft);
            if (this.zzaui == null) {
                this.zzaui = new ArrayList();
            }
            if (this.zzauh == null) {
                this.zzauh = new ArrayList();
            }
            if (this.zzaui.size() > 0 && zza((zzft) this.zzaui.get(0)) != zza(zzft)) {
                return false;
            }
            long zzvx = this.zzauj + ((long) zzft.zzvx());
            if (zzvx >= ((long) Math.max(0, ((Integer) zzai.zzajc.get()).intValue()))) {
                return false;
            }
            this.zzauj = zzvx;
            this.zzaui.add(zzft);
            this.zzauh.add(Long.valueOf(j));
            if (this.zzaui.size() >= Math.max(1, ((Integer) zzai.zzajd.get()).intValue())) {
                return false;
            }
            return true;
        }

        private static long zza(zzft zzft) {
            return ((zzft.zzaxb.longValue() / 1000) / 60) / 60;
        }

        /* synthetic */ zza(zzfo zzfo, zzfp zzfp) {
            this();
        }
    }

    public static zzfo zzn(Context context) {
        Preconditions.checkNotNull(context);
        Preconditions.checkNotNull(context.getApplicationContext());
        if (zzatg == null) {
            synchronized (zzfo.class) {
                if (zzatg == null) {
                    zzatg = new zzfo(new zzft(context));
                }
            }
        }
        return zzatg;
    }

    private zzfo(zzft zzft) {
        this(zzft, null);
    }

    private zzfo(zzft zzft, zzbw zzbw) {
        this.zzvz = false;
        Preconditions.checkNotNull(zzft);
        this.zzada = zzbw.zza(zzft.zzri, (zzan) null);
        this.zzauc = -1;
        zzfu zzfu = new zzfu(this);
        zzfu.zzq();
        this.zzatn = zzfu;
        zzaw zzaw = new zzaw(this);
        zzaw.zzq();
        this.zzati = zzaw;
        zzbq zzbq = new zzbq(this);
        zzbq.zzq();
        this.zzath = zzbq;
        this.zzada.zzgs().zzc((Runnable) new zzfp(this, zzft));
    }

    /* access modifiers changed from: private */
    @WorkerThread
    public final void zza(zzft zzft) {
        this.zzada.zzgs().zzaf();
        zzt zzt = new zzt(this);
        zzt.zzq();
        this.zzatj = zzt;
        this.zzada.zzgv().zza((zzs) this.zzath);
        zzm zzm = new zzm(this);
        zzm.zzq();
        this.zzatm = zzm;
        zzdv zzdv = new zzdv(this);
        zzdv.zzq();
        this.zzato = zzdv;
        zzfk zzfk = new zzfk(this);
        zzfk.zzq();
        this.zzatl = zzfk;
        this.zzatk = new zzbb(this);
        if (this.zzatt != this.zzatu) {
            this.zzada.zzgt().zzjg().zze("Not all upload components initialized", Integer.valueOf(this.zzatt), Integer.valueOf(this.zzatu));
        }
        this.zzvz = true;
    }

    /* access modifiers changed from: protected */
    @WorkerThread
    public final void start() {
        this.zzada.zzgs().zzaf();
        zzjt().zzij();
        if (this.zzada.zzgu().zzana.get() == 0) {
            this.zzada.zzgu().zzana.set(this.zzada.zzbx().currentTimeMillis());
        }
        zzmb();
    }

    public final zzn zzgw() {
        return this.zzada.zzgw();
    }

    public final zzq zzgv() {
        return this.zzada.zzgv();
    }

    public final zzas zzgt() {
        return this.zzada.zzgt();
    }

    public final zzbr zzgs() {
        return this.zzada.zzgs();
    }

    private final zzbq zzls() {
        zza((zzfn) this.zzath);
        return this.zzath;
    }

    public final zzaw zzlt() {
        zza((zzfn) this.zzati);
        return this.zzati;
    }

    public final zzt zzjt() {
        zza((zzfn) this.zzatj);
        return this.zzatj;
    }

    private final zzbb zzlu() {
        if (this.zzatk != null) {
            return this.zzatk;
        }
        throw new IllegalStateException("Network broadcast receiver not created");
    }

    private final zzfk zzlv() {
        zza((zzfn) this.zzatl);
        return this.zzatl;
    }

    public final zzm zzjs() {
        zza((zzfn) this.zzatm);
        return this.zzatm;
    }

    public final zzdv zzlw() {
        zza((zzfn) this.zzato);
        return this.zzato;
    }

    public final zzfu zzjr() {
        zza((zzfn) this.zzatn);
        return this.zzatn;
    }

    public final zzaq zzgq() {
        return this.zzada.zzgq();
    }

    public final Context getContext() {
        return this.zzada.getContext();
    }

    public final Clock zzbx() {
        return this.zzada.zzbx();
    }

    public final zzfy zzgr() {
        return this.zzada.zzgr();
    }

    @WorkerThread
    private final void zzaf() {
        this.zzada.zzgs().zzaf();
    }

    /* access modifiers changed from: 0000 */
    public final void zzlx() {
        if (!this.zzvz) {
            throw new IllegalStateException("UploadController is not initialized");
        }
    }

    private static void zza(zzfn zzfn) {
        if (zzfn == null) {
            throw new IllegalStateException("Upload Component not created");
        } else if (!zzfn.isInitialized()) {
            String valueOf = String.valueOf(zzfn.getClass());
            StringBuilder sb = new StringBuilder(27 + String.valueOf(valueOf).length());
            sb.append("Component not initialized: ");
            sb.append(valueOf);
            throw new IllegalStateException(sb.toString());
        }
    }

    /* access modifiers changed from: 0000 */
    public final void zze(zzk zzk) {
        zzaf();
        zzlx();
        Preconditions.checkNotEmpty(zzk.packageName);
        zzg(zzk);
    }

    private final long zzly() {
        long currentTimeMillis = this.zzada.zzbx().currentTimeMillis();
        zzbd zzgu = this.zzada.zzgu();
        zzgu.zzcl();
        zzgu.zzaf();
        long j = zzgu.zzane.get();
        if (j == 0) {
            long nextInt = 1 + ((long) zzgu.zzgr().zzmk().nextInt(86400000));
            zzgu.zzane.set(nextInt);
            j = nextInt;
        }
        return ((((currentTimeMillis + j) / 1000) / 60) / 60) / 24;
    }

    /* access modifiers changed from: 0000 */
    @WorkerThread
    public final void zzd(zzag zzag, String str) {
        zzag zzag2 = zzag;
        String str2 = str;
        zzg zzbm = zzjt().zzbm(str2);
        if (zzbm == null || TextUtils.isEmpty(zzbm.zzak())) {
            this.zzada.zzgt().zzjn().zzg("No app data available; dropping event", str2);
            return;
        }
        Boolean zzc = zzc(zzbm);
        if (zzc == null) {
            if (!"_ui".equals(zzag2.name)) {
                this.zzada.zzgt().zzjj().zzg("Could not find package. appId", zzas.zzbw(str));
            }
        } else if (!zzc.booleanValue()) {
            this.zzada.zzgt().zzjg().zzg("App version does not match; dropping event. appId", zzas.zzbw(str));
            return;
        }
        zzk zzk = r2;
        zzk zzk2 = new zzk(str2, zzbm.getGmpAppId(), zzbm.zzak(), zzbm.zzhf(), zzbm.zzhg(), zzbm.zzhh(), zzbm.zzhi(), (String) null, zzbm.isMeasurementEnabled(), false, zzbm.getFirebaseInstanceId(), zzbm.zzhv(), 0, 0, zzbm.zzhw(), zzbm.zzhx(), false, zzbm.zzhb());
        zzc(zzag2, zzk);
    }

    /* access modifiers changed from: 0000 */
    @WorkerThread
    public final void zzc(zzag zzag, zzk zzk) {
        List<zzo> list;
        List<zzo> list2;
        List list3;
        zzag zzag2 = zzag;
        zzk zzk2 = zzk;
        Preconditions.checkNotNull(zzk);
        Preconditions.checkNotEmpty(zzk2.packageName);
        zzaf();
        zzlx();
        String str = zzk2.packageName;
        long j = zzag2.zzaig;
        if (zzjr().zze(zzag2, zzk2)) {
            if (!zzk2.zzafr) {
                zzg(zzk2);
                return;
            }
            zzjt().beginTransaction();
            try {
                zzt zzjt = zzjt();
                Preconditions.checkNotEmpty(str);
                zzjt.zzaf();
                zzjt.zzcl();
                if (j < 0) {
                    zzjt.zzgt().zzjj().zze("Invalid time querying timed out conditional properties", zzas.zzbw(str), Long.valueOf(j));
                    list = Collections.emptyList();
                } else {
                    list = zzjt.zzb("active=0 and app_id=? and abs(? - creation_timestamp) > trigger_timeout", new String[]{str, String.valueOf(j)});
                }
                for (zzo zzo : list) {
                    if (zzo != null) {
                        this.zzada.zzgt().zzjn().zzd("User property timed out", zzo.packageName, this.zzada.zzgq().zzbv(zzo.zzags.name), zzo.zzags.getValue());
                        if (zzo.zzagt != null) {
                            zzd(new zzag(zzo.zzagt, j), zzk2);
                        }
                        zzjt().zzk(str, zzo.zzags.name);
                    }
                }
                zzt zzjt2 = zzjt();
                Preconditions.checkNotEmpty(str);
                zzjt2.zzaf();
                zzjt2.zzcl();
                if (j < 0) {
                    zzjt2.zzgt().zzjj().zze("Invalid time querying expired conditional properties", zzas.zzbw(str), Long.valueOf(j));
                    list2 = Collections.emptyList();
                } else {
                    list2 = zzjt2.zzb("active<>0 and app_id=? and abs(? - triggered_timestamp) > time_to_live", new String[]{str, String.valueOf(j)});
                }
                ArrayList arrayList = new ArrayList(list2.size());
                for (zzo zzo2 : list2) {
                    if (zzo2 != null) {
                        this.zzada.zzgt().zzjn().zzd("User property expired", zzo2.packageName, this.zzada.zzgq().zzbv(zzo2.zzags.name), zzo2.zzags.getValue());
                        zzjt().zzh(str, zzo2.zzags.name);
                        if (zzo2.zzagv != null) {
                            arrayList.add(zzo2.zzagv);
                        }
                        zzjt().zzk(str, zzo2.zzags.name);
                    }
                }
                ArrayList arrayList2 = arrayList;
                int size = arrayList2.size();
                int i = 0;
                while (i < size) {
                    Object obj = arrayList2.get(i);
                    i++;
                    zzd(new zzag((zzag) obj, j), zzk2);
                }
                zzt zzjt3 = zzjt();
                String str2 = zzag2.name;
                Preconditions.checkNotEmpty(str);
                Preconditions.checkNotEmpty(str2);
                zzjt3.zzaf();
                zzjt3.zzcl();
                if (j < 0) {
                    zzjt3.zzgt().zzjj().zzd("Invalid time querying triggered conditional properties", zzas.zzbw(str), zzjt3.zzgq().zzbt(str2), Long.valueOf(j));
                    list3 = Collections.emptyList();
                } else {
                    list3 = zzjt3.zzb("active=0 and app_id=? and trigger_event_name=? and abs(? - creation_timestamp) <= trigger_timeout", new String[]{str, str2, String.valueOf(j)});
                }
                ArrayList arrayList3 = new ArrayList(list3.size());
                Iterator it = list3.iterator();
                while (it.hasNext()) {
                    zzo zzo3 = (zzo) it.next();
                    if (zzo3 != null) {
                        zzfv zzfv = zzo3.zzags;
                        zzfx zzfx = r5;
                        Iterator it2 = it;
                        zzo zzo4 = zzo3;
                        zzfx zzfx2 = new zzfx(zzo3.packageName, zzo3.origin, zzfv.name, j, zzfv.getValue());
                        if (zzjt().zza(zzfx)) {
                            this.zzada.zzgt().zzjn().zzd("User property triggered", zzo4.packageName, this.zzada.zzgq().zzbv(zzfx.name), zzfx.value);
                        } else {
                            this.zzada.zzgt().zzjg().zzd("Too many active user properties, ignoring", zzas.zzbw(zzo4.packageName), this.zzada.zzgq().zzbv(zzfx.name), zzfx.value);
                        }
                        if (zzo4.zzagu != null) {
                            arrayList3.add(zzo4.zzagu);
                        }
                        zzo4.zzags = new zzfv(zzfx);
                        zzo4.active = true;
                        zzjt().zza(zzo4);
                        it = it2;
                    }
                }
                zzd(zzag, zzk);
                ArrayList arrayList4 = arrayList3;
                int size2 = arrayList4.size();
                int i2 = 0;
                while (i2 < size2) {
                    Object obj2 = arrayList4.get(i2);
                    i2++;
                    zzd(new zzag((zzag) obj2, j), zzk2);
                }
                zzjt().setTransactionSuccessful();
                zzjt().endTransaction();
            } catch (Throwable th) {
                Throwable th2 = th;
                zzjt().endTransaction();
                throw th2;
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:100:0x0339  */
    /* JADX WARNING: Removed duplicated region for block: B:209:0x07b4 A[Catch:{ IOException -> 0x07b7, all -> 0x0828 }] */
    /* JADX WARNING: Removed duplicated region for block: B:214:0x07e2 A[Catch:{ IOException -> 0x07b7, all -> 0x0828 }] */
    /* JADX WARNING: Removed duplicated region for block: B:84:0x027e A[Catch:{ IOException -> 0x07b7, all -> 0x0828 }] */
    /* JADX WARNING: Removed duplicated region for block: B:88:0x02b8 A[Catch:{ IOException -> 0x07b7, all -> 0x0828 }] */
    /* JADX WARNING: Removed duplicated region for block: B:94:0x030c A[Catch:{ IOException -> 0x07b7, all -> 0x0828 }] */
    @WorkerThread
    private final void zzd(zzag zzag, zzk zzk) {
        boolean z;
        char c;
        long intValue;
        zzac zzac;
        zzfw zzfw;
        boolean z2;
        long j;
        boolean z3;
        zzfx zzfx;
        zzag zzag2 = zzag;
        zzk zzk2 = zzk;
        Preconditions.checkNotNull(zzk);
        Preconditions.checkNotEmpty(zzk2.packageName);
        long nanoTime = System.nanoTime();
        zzaf();
        zzlx();
        String str = zzk2.packageName;
        if (zzjr().zze(zzag2, zzk2)) {
            if (!zzk2.zzafr) {
                zzg(zzk2);
            } else if (zzls().zzo(str, zzag2.name)) {
                this.zzada.zzgt().zzjj().zze("Dropping blacklisted event. appId", zzas.zzbw(str), this.zzada.zzgq().zzbt(zzag2.name));
                boolean z4 = zzls().zzcl(str) || zzls().zzcm(str);
                if (!z4 && !"_err".equals(zzag2.name)) {
                    this.zzada.zzgr().zza(str, 11, "_ev", zzag2.name, 0);
                }
                if (z4) {
                    zzg zzbm = zzjt().zzbm(str);
                    if (zzbm != null) {
                        if (Math.abs(this.zzada.zzbx().currentTimeMillis() - Math.max(zzbm.zzhl(), zzbm.zzhk())) > ((Long) zzai.zzajt.get()).longValue()) {
                            this.zzada.zzgt().zzjn().zzby("Fetching config for blacklisted app");
                            zzb(zzbm);
                        }
                    }
                }
            } else {
                if (this.zzada.zzgt().isLoggable(2)) {
                    this.zzada.zzgt().zzjo().zzg("Logging event", this.zzada.zzgq().zzb(zzag2));
                }
                zzjt().beginTransaction();
                zzg(zzk2);
                if (!"_iap".equals(zzag2.name)) {
                    if (!Event.ECOMMERCE_PURCHASE.equals(zzag2.name)) {
                        c = 2;
                        z = false;
                        boolean zzct = zzfy.zzct(zzag2.name);
                        boolean equals = "_err".equals(zzag2.name);
                        long j2 = nanoTime;
                        char c2 = c;
                        boolean z5 = z;
                        zzu zza2 = zzjt().zza(zzly(), str, true, zzct, false, equals, false);
                        intValue = zza2.zzahi - ((long) ((Integer) zzai.zzaje.get()).intValue());
                        if (intValue > 0) {
                            if (intValue % 1000 == 1) {
                                this.zzada.zzgt().zzjg().zze("Data loss. Too many events logged. appId, count", zzas.zzbw(str), Long.valueOf(zza2.zzahi));
                            }
                            zzjt().setTransactionSuccessful();
                            zzjt().endTransaction();
                            return;
                        }
                        if (zzct) {
                            long intValue2 = zza2.zzahh - ((long) ((Integer) zzai.zzajg.get()).intValue());
                            if (intValue2 > 0) {
                                if (intValue2 % 1000 == 1) {
                                    this.zzada.zzgt().zzjg().zze("Data loss. Too many public events logged. appId, count", zzas.zzbw(str), Long.valueOf(zza2.zzahh));
                                }
                                this.zzada.zzgr().zza(str, 16, "_ev", zzag2.name, 0);
                                zzjt().setTransactionSuccessful();
                                zzjt().endTransaction();
                                return;
                            }
                        }
                        if (equals) {
                            long max = zza2.zzahk - ((long) Math.max(0, Math.min(1000000, this.zzada.zzgv().zzb(zzk2.packageName, zzai.zzajf))));
                            if (max > 0) {
                                if (max == 1) {
                                    this.zzada.zzgt().zzjg().zze("Too many error events logged. appId, count", zzas.zzbw(str), Long.valueOf(zza2.zzahk));
                                }
                                zzjt().setTransactionSuccessful();
                                zzjt().endTransaction();
                                return;
                            }
                        }
                        Bundle zziy = zzag2.zzahu.zziy();
                        this.zzada.zzgr().zza(zziy, "_o", (Object) zzag2.origin);
                        if (this.zzada.zzgr().zzcz(str)) {
                            this.zzada.zzgr().zza(zziy, "_dbg", (Object) Long.valueOf(1));
                            this.zzada.zzgr().zza(zziy, "_r", (Object) Long.valueOf(1));
                        }
                        if (this.zzada.zzgv().zzbh(zzk2.packageName) && "_s".equals(zzag2.name)) {
                            zzfx zzi = zzjt().zzi(zzk2.packageName, "_sno");
                            if (zzi != null && (zzi.value instanceof Long)) {
                                this.zzada.zzgr().zza(zziy, "_sno", zzi.value);
                            }
                        }
                        long zzbn = zzjt().zzbn(str);
                        if (zzbn > 0) {
                            this.zzada.zzgt().zzjj().zze("Data lost. Too many events stored on disk, deleted. appId", zzas.zzbw(str), Long.valueOf(zzbn));
                        }
                        zzbw zzbw = this.zzada;
                        String str2 = zzag2.origin;
                        String str3 = zzag2.name;
                        long j3 = zzag2.zzaig;
                        String str4 = str;
                        zzab zzab = new zzab(zzbw, str2, str, str3, j3, 0, zziy);
                        zzac zzg = zzjt().zzg(str4, zzab.name);
                        if (zzg != null) {
                            zzab = zzab.zza(this.zzada, zzg.zzahx);
                            zzac = zzg.zzae(zzab.timestamp);
                        } else if (zzjt().zzbq(str4) < 500 || !zzct) {
                            zzac = new zzac(str4, zzab.name, 0, 0, zzab.timestamp, 0, null, null, null, null);
                        } else {
                            this.zzada.zzgt().zzjg().zzd("Too many event names used, ignoring event. appId, name, supported count", zzas.zzbw(str4), this.zzada.zzgq().zzbt(zzab.name), Integer.valueOf(500));
                            this.zzada.zzgr().zza(str4, 8, (String) null, (String) null, 0);
                            zzjt().endTransaction();
                            return;
                        }
                        zzjt().zza(zzac);
                        zzaf();
                        zzlx();
                        Preconditions.checkNotNull(zzab);
                        Preconditions.checkNotNull(zzk);
                        Preconditions.checkNotEmpty(zzab.zztt);
                        Preconditions.checkArgument(zzab.zztt.equals(zzk2.packageName));
                        zzfw = new zzfw();
                        boolean z6 = true;
                        zzfw.zzaxh = Integer.valueOf(1);
                        zzfw.zzaxp = "android";
                        zzfw.zztt = zzk2.packageName;
                        zzfw.zzafp = zzk2.zzafp;
                        zzfw.zzts = zzk2.zzts;
                        zzfw.zzayb = zzk2.zzafo == -2147483648L ? null : Integer.valueOf((int) zzk2.zzafo);
                        zzfw.zzaxt = Long.valueOf(zzk2.zzade);
                        zzfw.zzafi = zzk2.zzafi;
                        zzfw.zzawp = zzk2.zzafv;
                        zzfw.zzaxx = zzk2.zzafq == 0 ? null : Long.valueOf(zzk2.zzafq);
                        if (this.zzada.zzgv().zze(zzk2.packageName, zzai.zzale)) {
                            zzfw.zzayl = zzjr().zzmi();
                        }
                        Pair zzbz = this.zzada.zzgu().zzbz(zzk2.packageName);
                        if (zzbz == null || TextUtils.isEmpty((CharSequence) zzbz.first)) {
                            if (!this.zzada.zzgp().zzl(this.zzada.getContext()) && zzk2.zzafu) {
                                String string = Secure.getString(this.zzada.getContext().getContentResolver(), "android_id");
                                if (string == null) {
                                    this.zzada.zzgt().zzjj().zzg("null secure ID. appId", zzas.zzbw(zzfw.zztt));
                                    string = "null";
                                } else if (string.isEmpty()) {
                                    this.zzada.zzgt().zzjj().zzg("empty secure ID. appId", zzas.zzbw(zzfw.zztt));
                                }
                                zzfw.zzaye = string;
                            }
                        } else if (zzk2.zzaft) {
                            zzfw.zzaxv = (String) zzbz.first;
                            zzfw.zzaxw = (Boolean) zzbz.second;
                        }
                        this.zzada.zzgp().zzcl();
                        zzfw.zzaxr = Build.MODEL;
                        this.zzada.zzgp().zzcl();
                        zzfw.zzaxq = VERSION.RELEASE;
                        zzfw.zzaxs = Integer.valueOf((int) this.zzada.zzgp().zziw());
                        zzfw.zzahr = this.zzada.zzgp().zzix();
                        zzfw.zzaxu = null;
                        zzfw.zzaxk = null;
                        zzfw.zzaxl = null;
                        zzfw.zzaxm = null;
                        zzfw.zzayg = Long.valueOf(zzk2.zzafs);
                        if (this.zzada.isEnabled() && zzq.zzie()) {
                            zzfw.zzayh = null;
                        }
                        zzg zzbm2 = zzjt().zzbm(zzk2.packageName);
                        if (zzbm2 == null) {
                            zzbm2 = new zzg(this.zzada, zzk2.packageName);
                            zzbm2.zzaj(this.zzada.zzgr().zzmm());
                            zzbm2.zzan(zzk2.zzafk);
                            zzbm2.zzak(zzk2.zzafi);
                            zzbm2.zzam(this.zzada.zzgu().zzca(zzk2.packageName));
                            zzbm2.zzt(0);
                            zzbm2.zzo(0);
                            zzbm2.zzp(0);
                            zzbm2.setAppVersion(zzk2.zzts);
                            zzbm2.zzq(zzk2.zzafo);
                            zzbm2.zzao(zzk2.zzafp);
                            zzbm2.zzr(zzk2.zzade);
                            zzbm2.zzs(zzk2.zzafq);
                            zzbm2.setMeasurementEnabled(zzk2.zzafr);
                            zzbm2.zzac(zzk2.zzafs);
                            zzjt().zza(zzbm2);
                        }
                        zzfw.zzafh = zzbm2.getAppInstanceId();
                        zzfw.zzafk = zzbm2.getFirebaseInstanceId();
                        List zzbl = zzjt().zzbl(zzk2.packageName);
                        zzfw.zzaxj = new zzfz[zzbl.size()];
                        for (int i = 0; i < zzbl.size(); i++) {
                            zzfz zzfz = new zzfz();
                            zzfw.zzaxj[i] = zzfz;
                            zzfz.name = ((zzfx) zzbl.get(i)).name;
                            zzfz.zzayu = Long.valueOf(((zzfx) zzbl.get(i)).zzauk);
                            zzjr().zza(zzfz, ((zzfx) zzbl.get(i)).value);
                        }
                        long zza3 = zzjt().zza(zzfw);
                        zzt zzjt = zzjt();
                        if (zzab.zzahu != null) {
                            Iterator it = zzab.zzahu.iterator();
                            while (true) {
                                if (it.hasNext()) {
                                    if ("_r".equals((String) it.next())) {
                                        break;
                                    }
                                } else {
                                    boolean zzp = zzls().zzp(zzab.zztt, zzab.name);
                                    zzu zza4 = zzjt().zza(zzly(), zzab.zztt, false, false, false, false, false);
                                    if (zzp && zza4.zzahl < ((long) this.zzada.zzgv().zzaq(zzab.zztt))) {
                                    }
                                }
                            }
                            if (zzjt.zza(zzab, zza3, z6)) {
                                this.zzatr = 0;
                            }
                            zzjt().setTransactionSuccessful();
                            if (this.zzada.zzgt().isLoggable(2)) {
                                this.zzada.zzgt().zzjo().zzg("Event recorded", this.zzada.zzgq().zza(zzab));
                            }
                            zzjt().endTransaction();
                            zzmb();
                            this.zzada.zzgt().zzjo().zzg("Background event processing time, ms", Long.valueOf(((System.nanoTime() - j2) + 500000) / C.MICROS_PER_SECOND));
                            return;
                        }
                        z6 = false;
                        if (zzjt.zza(zzab, zza3, z6)) {
                        }
                        zzjt().setTransactionSuccessful();
                        if (this.zzada.zzgt().isLoggable(2)) {
                        }
                        zzjt().endTransaction();
                        zzmb();
                        this.zzada.zzgt().zzjo().zzg("Background event processing time, ms", Long.valueOf(((System.nanoTime() - j2) + 500000) / C.MICROS_PER_SECOND));
                        return;
                    }
                }
                String string2 = zzag2.zzahu.getString(Param.CURRENCY);
                if (Event.ECOMMERCE_PURCHASE.equals(zzag2.name)) {
                    double doubleValue = zzag2.zzahu.zzbr("value").doubleValue() * 1000000.0d;
                    if (doubleValue == Utils.DOUBLE_EPSILON) {
                        doubleValue = ((double) zzag2.zzahu.getLong("value").longValue()) * 1000000.0d;
                    }
                    if (doubleValue > 9.223372036854776E18d || doubleValue < -9.223372036854776E18d) {
                        this.zzada.zzgt().zzjj().zze("Data lost. Currency value is too big. appId", zzas.zzbw(str), Double.valueOf(doubleValue));
                        c = 2;
                        z2 = false;
                        z = false;
                        if (!z2) {
                            zzjt().setTransactionSuccessful();
                            zzjt().endTransaction();
                            return;
                        }
                        boolean zzct2 = zzfy.zzct(zzag2.name);
                        boolean equals2 = "_err".equals(zzag2.name);
                        long j22 = nanoTime;
                        char c22 = c;
                        boolean z52 = z;
                        zzu zza22 = zzjt().zza(zzly(), str, true, zzct2, false, equals2, false);
                        intValue = zza22.zzahi - ((long) ((Integer) zzai.zzaje.get()).intValue());
                        if (intValue > 0) {
                        }
                    } else {
                        j = Math.round(doubleValue);
                    }
                } else {
                    j = zzag2.zzahu.getLong("value").longValue();
                }
                if (!TextUtils.isEmpty(string2)) {
                    String upperCase = string2.toUpperCase(Locale.US);
                    if (upperCase.matches("[A-Z]{3}")) {
                        String valueOf = String.valueOf("_ltv_");
                        String valueOf2 = String.valueOf(upperCase);
                        String concat = valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf);
                        zzfx zzi2 = zzjt().zzi(str, concat);
                        if (zzi2 != null) {
                            if (zzi2.value instanceof Long) {
                                c = 2;
                                zzfx zzfx2 = new zzfx(str, zzag2.origin, concat, this.zzada.zzbx().currentTimeMillis(), Long.valueOf(((Long) zzi2.value).longValue() + j));
                                zzfx = zzfx2;
                                z = false;
                                z3 = true;
                                if (!zzjt().zza(zzfx)) {
                                    this.zzada.zzgt().zzjg().zzd("Too many unique user properties are set. Ignoring user property. appId", zzas.zzbw(str), this.zzada.zzgq().zzbv(zzfx.name), zzfx.value);
                                    this.zzada.zzgr().zza(str, 9, (String) null, (String) null, 0);
                                }
                                z2 = z3;
                                if (!z2) {
                                }
                                boolean zzct22 = zzfy.zzct(zzag2.name);
                                boolean equals22 = "_err".equals(zzag2.name);
                                long j222 = nanoTime;
                                char c222 = c;
                                boolean z522 = z;
                                zzu zza222 = zzjt().zza(zzly(), str, true, zzct22, false, equals22, false);
                                intValue = zza222.zzahi - ((long) ((Integer) zzai.zzaje.get()).intValue());
                                if (intValue > 0) {
                                }
                            }
                        }
                        c = 2;
                        zzt zzjt2 = zzjt();
                        int zzb = this.zzada.zzgv().zzb(str, zzai.zzajy) - 1;
                        Preconditions.checkNotEmpty(str);
                        zzjt2.zzaf();
                        zzjt2.zzcl();
                        try {
                            SQLiteDatabase writableDatabase = zzjt2.getWritableDatabase();
                            String str5 = "delete from user_attributes where app_id=? and name in (select name from user_attributes where app_id=? and name like '_ltv_%' order by set_timestamp desc limit ?,10);";
                            String[] strArr = new String[3];
                            z = false;
                            try {
                                strArr[0] = str;
                                z3 = true;
                                try {
                                    strArr[1] = str;
                                    strArr[2] = String.valueOf(zzb);
                                    writableDatabase.execSQL(str5, strArr);
                                } catch (SQLiteException e) {
                                    e = e;
                                }
                            } catch (SQLiteException e2) {
                                e = e2;
                                z3 = true;
                                zzjt2.zzgt().zzjg().zze("Error pruning currencies. appId", zzas.zzbw(str), e);
                                zzfx = new zzfx(str, zzag2.origin, concat, this.zzada.zzbx().currentTimeMillis(), Long.valueOf(j));
                                if (!zzjt().zza(zzfx)) {
                                }
                                z2 = z3;
                                if (!z2) {
                                }
                                boolean zzct222 = zzfy.zzct(zzag2.name);
                                boolean equals222 = "_err".equals(zzag2.name);
                                long j2222 = nanoTime;
                                char c2222 = c;
                                boolean z5222 = z;
                                zzu zza2222 = zzjt().zza(zzly(), str, true, zzct222, false, equals222, false);
                                intValue = zza2222.zzahi - ((long) ((Integer) zzai.zzaje.get()).intValue());
                                if (intValue > 0) {
                                }
                            }
                        } catch (SQLiteException e3) {
                            e = e3;
                            z = false;
                            z3 = true;
                            zzjt2.zzgt().zzjg().zze("Error pruning currencies. appId", zzas.zzbw(str), e);
                            zzfx = new zzfx(str, zzag2.origin, concat, this.zzada.zzbx().currentTimeMillis(), Long.valueOf(j));
                            if (!zzjt().zza(zzfx)) {
                            }
                            z2 = z3;
                            if (!z2) {
                            }
                            boolean zzct2222 = zzfy.zzct(zzag2.name);
                            boolean equals2222 = "_err".equals(zzag2.name);
                            long j22222 = nanoTime;
                            char c22222 = c;
                            boolean z52222 = z;
                            zzu zza22222 = zzjt().zza(zzly(), str, true, zzct2222, false, equals2222, false);
                            intValue = zza22222.zzahi - ((long) ((Integer) zzai.zzaje.get()).intValue());
                            if (intValue > 0) {
                            }
                        }
                        zzfx = new zzfx(str, zzag2.origin, concat, this.zzada.zzbx().currentTimeMillis(), Long.valueOf(j));
                        if (!zzjt().zza(zzfx)) {
                        }
                        z2 = z3;
                        if (!z2) {
                        }
                        boolean zzct22222 = zzfy.zzct(zzag2.name);
                        boolean equals22222 = "_err".equals(zzag2.name);
                        long j222222 = nanoTime;
                        char c222222 = c;
                        boolean z522222 = z;
                        zzu zza222222 = zzjt().zza(zzly(), str, true, zzct22222, false, equals22222, false);
                        intValue = zza222222.zzahi - ((long) ((Integer) zzai.zzaje.get()).intValue());
                        if (intValue > 0) {
                        }
                    }
                }
                c = 2;
                z = false;
                z3 = true;
                z2 = z3;
                if (!z2) {
                }
                try {
                    boolean zzct222222 = zzfy.zzct(zzag2.name);
                    boolean equals222222 = "_err".equals(zzag2.name);
                    long j2222222 = nanoTime;
                    char c2222222 = c;
                    boolean z5222222 = z;
                    zzu zza2222222 = zzjt().zza(zzly(), str, true, zzct222222, false, equals222222, false);
                    intValue = zza2222222.zzahi - ((long) ((Integer) zzai.zzaje.get()).intValue());
                    if (intValue > 0) {
                    }
                } catch (IOException e4) {
                    this.zzada.zzgt().zzjg().zze("Data loss. Failed to insert raw event metadata. appId", zzas.zzbw(zzfw.zztt), e4);
                } catch (Throwable th) {
                    Throwable th2 = th;
                    zzjt().endTransaction();
                    throw th2;
                }
            }
        }
    }

    /* access modifiers changed from: 0000 */
    /* JADX WARNING: Can't wrap try/catch for region: R(2:88|89) */
    /* JADX WARNING: Code restructure failed: missing block: B:89:?, code lost:
        r14.zzada.zzgt().zzjg().zze("Failed to parse upload URL. Not uploading. appId", com.google.android.gms.measurement.internal.zzas.zzbw(r4), r5);
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:88:0x0298 */
    @WorkerThread
    public final void zzlz() {
        String str;
        zzaf();
        zzlx();
        this.zzatx = true;
        try {
            this.zzada.zzgw();
            Boolean zzli = this.zzada.zzgl().zzli();
            if (zzli == null) {
                this.zzada.zzgt().zzjj().zzby("Upload data called on the client side before use of service was decided");
            } else if (zzli.booleanValue()) {
                this.zzada.zzgt().zzjg().zzby("Upload called in the client side when service should be used");
                this.zzatx = false;
                zzmc();
            } else if (this.zzatr > 0) {
                zzmb();
                this.zzatx = false;
                zzmc();
            } else {
                zzaf();
                if (this.zzaua != null) {
                    this.zzada.zzgt().zzjo().zzby("Uploading requested multiple times");
                    this.zzatx = false;
                    zzmc();
                } else if (!zzlt().zzfb()) {
                    this.zzada.zzgt().zzjo().zzby("Network not connected, ignoring upload request");
                    zzmb();
                    this.zzatx = false;
                    zzmc();
                } else {
                    long currentTimeMillis = this.zzada.zzbx().currentTimeMillis();
                    String str2 = null;
                    zzd((String) null, currentTimeMillis - zzq.zzic());
                    long j = this.zzada.zzgu().zzana.get();
                    if (j != 0) {
                        this.zzada.zzgt().zzjn().zzg("Uploading events. Elapsed time since last upload attempt (ms)", Long.valueOf(Math.abs(currentTimeMillis - j)));
                    }
                    String zzih = zzjt().zzih();
                    if (!TextUtils.isEmpty(zzih)) {
                        if (this.zzauc == -1) {
                            this.zzauc = zzjt().zzio();
                        }
                        List zzb = zzjt().zzb(zzih, this.zzada.zzgv().zzb(zzih, zzai.zzaja), Math.max(0, this.zzada.zzgv().zzb(zzih, zzai.zzajb)));
                        if (!zzb.isEmpty()) {
                            Iterator it = zzb.iterator();
                            while (true) {
                                if (!it.hasNext()) {
                                    str = null;
                                    break;
                                }
                                zzfw zzfw = (zzfw) ((Pair) it.next()).first;
                                if (!TextUtils.isEmpty(zzfw.zzaxv)) {
                                    str = zzfw.zzaxv;
                                    break;
                                }
                            }
                            if (str != null) {
                                int i = 0;
                                while (true) {
                                    if (i >= zzb.size()) {
                                        break;
                                    }
                                    zzfw zzfw2 = (zzfw) ((Pair) zzb.get(i)).first;
                                    if (!TextUtils.isEmpty(zzfw2.zzaxv) && !zzfw2.zzaxv.equals(str)) {
                                        zzb = zzb.subList(0, i);
                                        break;
                                    }
                                    i++;
                                }
                            }
                            zzfv zzfv = new zzfv();
                            zzfv.zzaxf = new zzfw[zzb.size()];
                            ArrayList arrayList = new ArrayList(zzb.size());
                            boolean z = zzq.zzie() && this.zzada.zzgv().zzas(zzih);
                            for (int i2 = 0; i2 < zzfv.zzaxf.length; i2++) {
                                zzfv.zzaxf[i2] = (zzfw) ((Pair) zzb.get(i2)).first;
                                arrayList.add((Long) ((Pair) zzb.get(i2)).second);
                                zzfv.zzaxf[i2].zzaxu = Long.valueOf(this.zzada.zzgv().zzhh());
                                zzfv.zzaxf[i2].zzaxk = Long.valueOf(currentTimeMillis);
                                zzfw zzfw3 = zzfv.zzaxf[i2];
                                this.zzada.zzgw();
                                zzfw3.zzaxz = Boolean.valueOf(false);
                                if (!z) {
                                    zzfv.zzaxf[i2].zzayh = null;
                                }
                            }
                            if (this.zzada.zzgt().isLoggable(2)) {
                                str2 = zzjr().zzb(zzfv);
                            }
                            byte[] zza2 = zzjr().zza(zzfv);
                            String str3 = (String) zzai.zzajk.get();
                            URL url = new URL(str3);
                            Preconditions.checkArgument(!arrayList.isEmpty());
                            if (this.zzaua != null) {
                                this.zzada.zzgt().zzjg().zzby("Set uploading progress before finishing the previous upload");
                            } else {
                                this.zzaua = new ArrayList(arrayList);
                            }
                            this.zzada.zzgu().zzanb.set(currentTimeMillis);
                            String str4 = "?";
                            if (zzfv.zzaxf.length > 0) {
                                str4 = zzfv.zzaxf[0].zztt;
                            }
                            this.zzada.zzgt().zzjo().zzd("Uploading data. app, uncompressed size, data", str4, Integer.valueOf(zza2.length), str2);
                            this.zzatw = true;
                            zzaw zzlt = zzlt();
                            zzfq zzfq = new zzfq(this, zzih);
                            zzlt.zzaf();
                            zzlt.zzcl();
                            Preconditions.checkNotNull(url);
                            Preconditions.checkNotNull(zza2);
                            Preconditions.checkNotNull(zzfq);
                            zzbr zzgs = zzlt.zzgs();
                            zzba zzba = new zzba(zzlt, zzih, url, zza2, null, zzfq);
                            zzgs.zzd((Runnable) zzba);
                        }
                    } else {
                        this.zzauc = -1;
                        String zzad = zzjt().zzad(currentTimeMillis - zzq.zzic());
                        if (!TextUtils.isEmpty(zzad)) {
                            zzg zzbm = zzjt().zzbm(zzad);
                            if (zzbm != null) {
                                zzb(zzbm);
                            }
                        }
                    }
                    this.zzatx = false;
                    zzmc();
                }
            }
        } finally {
            this.zzatx = false;
            zzmc();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0040, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0041, code lost:
        r2 = r0;
        r8 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0045, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0046, code lost:
        r7 = null;
        r8 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x009f, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x00a0, code lost:
        r8 = r3;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0040 A[ExcHandler: all (r0v14 'th' java.lang.Throwable A[CUSTOM_DECLARE]), PHI: r3 
  PHI: (r3v149 android.database.Cursor) = (r3v144 android.database.Cursor), (r3v144 android.database.Cursor), (r3v144 android.database.Cursor), (r3v152 android.database.Cursor), (r3v152 android.database.Cursor), (r3v152 android.database.Cursor), (r3v152 android.database.Cursor), (r3v1 android.database.Cursor), (r3v1 android.database.Cursor) binds: [B:48:0x00e5, B:54:0x00f2, B:55:?, B:25:0x0082, B:31:0x008f, B:33:0x0093, B:34:?, B:9:0x0031, B:10:?] A[DONT_GENERATE, DONT_INLINE], Splitter:B:9:0x0031] */
    /* JADX WARNING: Removed duplicated region for block: B:130:0x0278 A[SYNTHETIC, Splitter:B:130:0x0278] */
    /* JADX WARNING: Removed duplicated region for block: B:134:0x027f A[Catch:{ IOException -> 0x022f, all -> 0x0df9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:140:0x028d A[Catch:{ IOException -> 0x022f, all -> 0x0df9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:177:0x03a5 A[Catch:{ IOException -> 0x022f, all -> 0x0df9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:178:0x03a7 A[Catch:{ IOException -> 0x022f, all -> 0x0df9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:180:0x03aa A[Catch:{ IOException -> 0x022f, all -> 0x0df9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:181:0x03ab A[Catch:{ IOException -> 0x022f, all -> 0x0df9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:285:0x0686 A[Catch:{ IOException -> 0x022f, all -> 0x0df9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:311:0x0704 A[ADDED_TO_REGION, Catch:{ IOException -> 0x022f, all -> 0x0df9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:361:0x0875 A[Catch:{ IOException -> 0x022f, all -> 0x0df9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:367:0x088f A[Catch:{ IOException -> 0x022f, all -> 0x0df9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:370:0x08af A[Catch:{ IOException -> 0x022f, all -> 0x0df9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:421:0x0a0a A[Catch:{ IOException -> 0x022f, all -> 0x0df9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:422:0x0a19 A[Catch:{ IOException -> 0x022f, all -> 0x0df9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:424:0x0a1c A[Catch:{ IOException -> 0x022f, all -> 0x0df9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:425:0x0a3a A[Catch:{ IOException -> 0x022f, all -> 0x0df9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:545:0x0de1 A[SYNTHETIC, Splitter:B:545:0x0de1] */
    /* JADX WARNING: Removed duplicated region for block: B:552:0x0df5 A[SYNTHETIC, Splitter:B:552:0x0df5] */
    /* JADX WARNING: Removed duplicated region for block: B:587:0x088c A[SYNTHETIC] */
    @WorkerThread
    private final boolean zzd(String str, long j) {
        zzt zzjt;
        Cursor cursor;
        Throwable th;
        boolean z;
        long j2;
        int i;
        zza zza2;
        SecureRandom secureRandom;
        zza zza3;
        int i2;
        zzfw zzfw;
        int i3;
        zzft[] zzftArr;
        boolean z2;
        int zzq;
        long j3;
        Long l;
        boolean z3;
        SecureRandom secureRandom2;
        zza zza4;
        long j4;
        zzfw zzfw2;
        zzfx zzfx;
        int i4;
        boolean z4;
        int i5;
        zza zza5;
        zzfw zzfw3;
        long j5;
        zzfw zzfw4;
        zza zza6;
        boolean z5;
        boolean z6;
        boolean z7;
        zzfw zzfw5;
        char c;
        boolean z8;
        boolean z9;
        String str2;
        Object obj;
        Cursor cursor2;
        String str3;
        String str4;
        SQLiteDatabase sQLiteDatabase;
        String[] strArr;
        String str5;
        String[] strArr2;
        zzjt().beginTransaction();
        try {
            Cursor cursor3 = null;
            zza zza7 = new zza(this, null);
            zzjt = zzjt();
            long j6 = this.zzauc;
            Preconditions.checkNotNull(zza7);
            zzjt.zzaf();
            zzjt.zzcl();
            try {
                SQLiteDatabase writableDatabase = zzjt.getWritableDatabase();
                if (TextUtils.isEmpty(null)) {
                    if (j6 != -1) {
                        try {
                            strArr2 = new String[]{String.valueOf(j6), String.valueOf(j)};
                        } catch (SQLiteException e) {
                            e = e;
                            cursor = cursor3;
                            str2 = null;
                        } catch (Throwable th2) {
                        }
                    } else {
                        strArr2 = new String[]{String.valueOf(j)};
                    }
                    String str6 = j6 != -1 ? "rowid <= ? and " : "";
                    StringBuilder sb = new StringBuilder(148 + String.valueOf(str6).length());
                    sb.append("select app_id, metadata_fingerprint from raw_events where ");
                    sb.append(str6);
                    sb.append("app_id in (select app_id from apps where config_fetched_time >= ?) order by rowid limit 1;");
                    cursor3 = writableDatabase.rawQuery(sb.toString(), strArr2);
                    if (!cursor3.moveToFirst()) {
                        if (cursor3 != null) {
                            cursor3.close();
                        }
                        if (zza7.zzaui != null) {
                            if (!zza7.zzaui.isEmpty()) {
                                z = false;
                                if (!z) {
                                    zzfw zzfw6 = zza7.zzaug;
                                    zzfw6.zzaxi = new zzft[zza7.zzaui.size()];
                                    boolean zzau = this.zzada.zzgv().zzau(zzfw6.zztt);
                                    boolean zze = this.zzada.zzgv().zze(zza7.zzaug.zztt, zzai.zzala);
                                    int i6 = 0;
                                    boolean z10 = false;
                                    int i7 = 0;
                                    zzft zzft = null;
                                    zzft zzft2 = null;
                                    long j7 = 0;
                                    while (true) {
                                        int i8 = i7;
                                        if (i6 < zza7.zzaui.size()) {
                                            zzft zzft3 = (zzft) zza7.zzaui.get(i6);
                                            if (zzls().zzo(zza7.zzaug.zztt, zzft3.name)) {
                                                boolean z11 = z10;
                                                i5 = i6;
                                                this.zzada.zzgt().zzjj().zze("Dropping blacklisted raw event. appId", zzas.zzbw(zza7.zzaug.zztt), this.zzada.zzgq().zzbt(zzft3.name));
                                                if (!zzls().zzcl(zza7.zzaug.zztt)) {
                                                    if (!zzls().zzcm(zza7.zzaug.zztt)) {
                                                        z9 = false;
                                                        if (!z9 && !"_err".equals(zzft3.name)) {
                                                            this.zzada.zzgr().zza(zza7.zzaug.zztt, 11, "_ev", zzft3.name, 0);
                                                        }
                                                        zza5 = zza7;
                                                        zzfw3 = zzfw6;
                                                        i7 = i8;
                                                        z10 = z11;
                                                    }
                                                }
                                                z9 = true;
                                                this.zzada.zzgr().zza(zza7.zzaug.zztt, 11, "_ev", zzft3.name, 0);
                                                zza5 = zza7;
                                                zzfw3 = zzfw6;
                                                i7 = i8;
                                                z10 = z11;
                                            } else {
                                                i5 = i6;
                                                boolean z12 = z10;
                                                boolean zzp = zzls().zzp(zza7.zzaug.zztt, zzft3.name);
                                                if (!zzp) {
                                                    zzjr();
                                                    String str7 = zzft3.name;
                                                    Preconditions.checkNotEmpty(str7);
                                                    int hashCode = str7.hashCode();
                                                    if (hashCode != 94660) {
                                                        if (hashCode != 95025) {
                                                            if (hashCode == 95027) {
                                                                if (str7.equals("_ui")) {
                                                                    c = 1;
                                                                    switch (c) {
                                                                        case 0:
                                                                        case 1:
                                                                        case 2:
                                                                            z8 = true;
                                                                            break;
                                                                        default:
                                                                            z8 = false;
                                                                            break;
                                                                    }
                                                                    if (z8) {
                                                                        zzfw4 = zzfw6;
                                                                        j5 = j7;
                                                                        if (!this.zzada.zzgv().zzbd(zza7.zzaug.zztt) && zzp) {
                                                                            zzfu[] zzfuArr = zzft3.zzaxa;
                                                                            int i9 = -1;
                                                                            int i10 = -1;
                                                                            for (int i11 = 0; i11 < zzfuArr.length; i11++) {
                                                                                if ("value".equals(zzfuArr[i11].name)) {
                                                                                    i9 = i11;
                                                                                } else if (Param.CURRENCY.equals(zzfuArr[i11].name)) {
                                                                                    i10 = i11;
                                                                                }
                                                                            }
                                                                            if (i9 != -1) {
                                                                                if (zzfuArr[i9].zzaxe == null && zzfuArr[i9].zzaun == null) {
                                                                                    this.zzada.zzgt().zzjl().zzby("Value must be specified with a numeric type.");
                                                                                    zzfuArr = zza(zza(zza(zzfuArr, i9), "_c"), 18, "value");
                                                                                } else {
                                                                                    if (i10 == -1) {
                                                                                        z5 = true;
                                                                                    } else {
                                                                                        String str8 = zzfuArr[i10].zzaml;
                                                                                        if (str8 != null) {
                                                                                            if (str8.length() == 3) {
                                                                                                int i12 = 0;
                                                                                                while (i12 < str8.length()) {
                                                                                                    int codePointAt = str8.codePointAt(i12);
                                                                                                    if (Character.isLetter(codePointAt)) {
                                                                                                        i12 += Character.charCount(codePointAt);
                                                                                                    }
                                                                                                }
                                                                                                z5 = false;
                                                                                            }
                                                                                        }
                                                                                        z5 = true;
                                                                                    }
                                                                                    if (z5) {
                                                                                        this.zzada.zzgt().zzjl().zzby("Value parameter discarded. You must also supply a 3-letter ISO_4217 currency code in the currency parameter.");
                                                                                        zzfuArr = zza(zza(zza(zzfuArr, i9), "_c"), 19, Param.CURRENCY);
                                                                                    }
                                                                                    zzft3.zzaxa = zzfuArr;
                                                                                }
                                                                            }
                                                                            zzft3.zzaxa = zzfuArr;
                                                                        }
                                                                        if (this.zzada.zzgv().zze(zza7.zzaug.zztt, zzai.zzakz)) {
                                                                            if ("_e".equals(zzft3.name)) {
                                                                                zzjr();
                                                                                if (zzfu.zza(zzft3, "_fr") == null) {
                                                                                    if (zzft == null || Math.abs(zzft.zzaxb.longValue() - zzft3.zzaxb.longValue()) > 1000 || !zza(zzft3, zzft)) {
                                                                                        zza6 = zza7;
                                                                                        zzft2 = zzft3;
                                                                                        if (zzau || zze || !"_e".equals(zzft3.name)) {
                                                                                            zza5 = zza6;
                                                                                        } else {
                                                                                            if (zzft3.zzaxa != null) {
                                                                                                if (zzft3.zzaxa.length != 0) {
                                                                                                    zzjr();
                                                                                                    Long l2 = (Long) zzfu.zzb(zzft3, "_et");
                                                                                                    if (l2 == null) {
                                                                                                        zza5 = zza6;
                                                                                                        this.zzada.zzgt().zzjj().zzg("Engagement event does not include duration. appId", zzas.zzbw(zza5.zzaug.zztt));
                                                                                                    } else {
                                                                                                        zza5 = zza6;
                                                                                                        j7 = j5 + l2.longValue();
                                                                                                        zzfw3 = zzfw4;
                                                                                                        i7 = i8 + 1;
                                                                                                        zzfw3.zzaxi[i8] = zzft3;
                                                                                                        z10 = z12;
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                            zza5 = zza6;
                                                                                            this.zzada.zzgt().zzjj().zzg("Engagement event does not contain any parameters. appId", zzas.zzbw(zza5.zzaug.zztt));
                                                                                        }
                                                                                        zzfw3 = zzfw4;
                                                                                        j7 = j5;
                                                                                        i7 = i8 + 1;
                                                                                        zzfw3.zzaxi[i8] = zzft3;
                                                                                        z10 = z12;
                                                                                    } else {
                                                                                        zza6 = zza7;
                                                                                    }
                                                                                }
                                                                            } else if ("_vs".equals(zzft3.name)) {
                                                                                zzjr();
                                                                                if (zzfu.zza(zzft3, "_et") == null) {
                                                                                    if (zzft2 != null) {
                                                                                        zza6 = zza7;
                                                                                        if (Math.abs(zzft2.zzaxb.longValue() - zzft3.zzaxb.longValue()) <= 1000 && zza(zzft2, zzft3)) {
                                                                                        }
                                                                                    } else {
                                                                                        zza6 = zza7;
                                                                                    }
                                                                                    zzft = zzft3;
                                                                                    if (zzau) {
                                                                                    }
                                                                                    zza5 = zza6;
                                                                                    zzfw3 = zzfw4;
                                                                                    j7 = j5;
                                                                                    i7 = i8 + 1;
                                                                                    zzfw3.zzaxi[i8] = zzft3;
                                                                                    z10 = z12;
                                                                                }
                                                                            }
                                                                            zzft = null;
                                                                            zzft2 = null;
                                                                            if (zzau) {
                                                                            }
                                                                            zza5 = zza6;
                                                                            zzfw3 = zzfw4;
                                                                            j7 = j5;
                                                                            i7 = i8 + 1;
                                                                            zzfw3.zzaxi[i8] = zzft3;
                                                                            z10 = z12;
                                                                        }
                                                                        zza6 = zza7;
                                                                        if (zzau) {
                                                                        }
                                                                        zza5 = zza6;
                                                                        zzfw3 = zzfw4;
                                                                        j7 = j5;
                                                                        i7 = i8 + 1;
                                                                        zzfw3.zzaxi[i8] = zzft3;
                                                                        z10 = z12;
                                                                    }
                                                                }
                                                            }
                                                        } else if (str7.equals("_ug")) {
                                                            c = 2;
                                                            switch (c) {
                                                                case 0:
                                                                case 1:
                                                                case 2:
                                                                    break;
                                                            }
                                                            if (z8) {
                                                            }
                                                        }
                                                    } else if (str7.equals("_in")) {
                                                        c = 0;
                                                        switch (c) {
                                                            case 0:
                                                            case 1:
                                                            case 2:
                                                                break;
                                                        }
                                                        if (z8) {
                                                        }
                                                    }
                                                    c = 65535;
                                                    switch (c) {
                                                        case 0:
                                                        case 1:
                                                        case 2:
                                                            break;
                                                    }
                                                    if (z8) {
                                                    }
                                                }
                                                if (zzft3.zzaxa == null) {
                                                    zzft3.zzaxa = new zzfu[0];
                                                }
                                                zzfu[] zzfuArr2 = zzft3.zzaxa;
                                                int length = zzfuArr2.length;
                                                int i13 = 0;
                                                boolean z13 = false;
                                                boolean z14 = false;
                                                while (i13 < length) {
                                                    zzfu zzfu = zzfuArr2[i13];
                                                    zzfu[] zzfuArr3 = zzfuArr2;
                                                    int i14 = length;
                                                    if ("_c".equals(zzfu.name)) {
                                                        zzfw5 = zzfw6;
                                                        zzfu.zzaxe = Long.valueOf(1);
                                                        z13 = true;
                                                    } else {
                                                        zzfw5 = zzfw6;
                                                        if ("_r".equals(zzfu.name)) {
                                                            zzfu.zzaxe = Long.valueOf(1);
                                                            z14 = true;
                                                        }
                                                    }
                                                    i13++;
                                                    zzfuArr2 = zzfuArr3;
                                                    length = i14;
                                                    zzfw6 = zzfw5;
                                                }
                                                zzfw4 = zzfw6;
                                                if (!z13 && zzp) {
                                                    this.zzada.zzgt().zzjo().zzg("Marking event as conversion", this.zzada.zzgq().zzbt(zzft3.name));
                                                    zzfu[] zzfuArr4 = (zzfu[]) Arrays.copyOf(zzft3.zzaxa, zzft3.zzaxa.length + 1);
                                                    zzfu zzfu2 = new zzfu();
                                                    zzfu2.name = "_c";
                                                    zzfu2.zzaxe = Long.valueOf(1);
                                                    zzfuArr4[zzfuArr4.length - 1] = zzfu2;
                                                    zzft3.zzaxa = zzfuArr4;
                                                }
                                                if (!z14) {
                                                    this.zzada.zzgt().zzjo().zzg("Marking event as real-time", this.zzada.zzgq().zzbt(zzft3.name));
                                                    zzfu[] zzfuArr5 = (zzfu[]) Arrays.copyOf(zzft3.zzaxa, zzft3.zzaxa.length + 1);
                                                    zzfu zzfu3 = new zzfu();
                                                    zzfu3.name = "_r";
                                                    zzfu3.zzaxe = Long.valueOf(1);
                                                    zzfuArr5[zzfuArr5.length - 1] = zzfu3;
                                                    zzft3.zzaxa = zzfuArr5;
                                                }
                                                j5 = j7;
                                                if (zzjt().zza(zzly(), zza7.zzaug.zztt, false, false, false, false, true).zzahl > ((long) this.zzada.zzgv().zzaq(zza7.zzaug.zztt))) {
                                                    int i15 = 0;
                                                    while (true) {
                                                        if (i15 < zzft3.zzaxa.length) {
                                                            if ("_r".equals(zzft3.zzaxa[i15].name)) {
                                                                zzfu[] zzfuArr6 = new zzfu[(zzft3.zzaxa.length - 1)];
                                                                if (i15 > 0) {
                                                                    System.arraycopy(zzft3.zzaxa, 0, zzfuArr6, 0, i15);
                                                                }
                                                                if (i15 < zzfuArr6.length) {
                                                                    System.arraycopy(zzft3.zzaxa, i15 + 1, zzfuArr6, i15, zzfuArr6.length - i15);
                                                                }
                                                                zzft3.zzaxa = zzfuArr6;
                                                            } else {
                                                                i15++;
                                                            }
                                                        }
                                                    }
                                                    z6 = z12;
                                                } else {
                                                    z6 = true;
                                                }
                                                if (!zzfy.zzct(zzft3.name) || !zzp || zzjt().zza(zzly(), zza7.zzaug.zztt, false, false, true, false, false).zzahj <= ((long) this.zzada.zzgv().zzb(zza7.zzaug.zztt, zzai.zzajh))) {
                                                    z7 = z6;
                                                } else {
                                                    this.zzada.zzgt().zzjj().zzg("Too many conversions. Not logging as conversion. appId", zzas.zzbw(zza7.zzaug.zztt));
                                                    zzfu[] zzfuArr7 = zzft3.zzaxa;
                                                    int length2 = zzfuArr7.length;
                                                    int i16 = 0;
                                                    boolean z15 = false;
                                                    zzfu zzfu4 = null;
                                                    while (i16 < length2) {
                                                        zzfu zzfu5 = zzfuArr7[i16];
                                                        zzfu[] zzfuArr8 = zzfuArr7;
                                                        boolean z16 = z6;
                                                        if ("_c".equals(zzfu5.name)) {
                                                            zzfu4 = zzfu5;
                                                        } else if ("_err".equals(zzfu5.name)) {
                                                            z15 = true;
                                                        }
                                                        i16++;
                                                        zzfuArr7 = zzfuArr8;
                                                        z6 = z16;
                                                    }
                                                    z7 = z6;
                                                    if (z15 && zzfu4 != null) {
                                                        zzft3.zzaxa = (zzfu[]) ArrayUtils.removeAll(zzft3.zzaxa, zzfu4);
                                                    } else if (zzfu4 != null) {
                                                        zzfu4.name = "_err";
                                                        zzfu4.zzaxe = Long.valueOf(10);
                                                    } else {
                                                        this.zzada.zzgt().zzjg().zzg("Did not find conversion parameter. appId", zzas.zzbw(zza7.zzaug.zztt));
                                                    }
                                                }
                                                z12 = z7;
                                                if (!this.zzada.zzgv().zzbd(zza7.zzaug.zztt)) {
                                                }
                                                if (this.zzada.zzgv().zze(zza7.zzaug.zztt, zzai.zzakz)) {
                                                }
                                                zza6 = zza7;
                                                if (zzau) {
                                                }
                                                zza5 = zza6;
                                                zzfw3 = zzfw4;
                                                j7 = j5;
                                                i7 = i8 + 1;
                                                zzfw3.zzaxi[i8] = zzft3;
                                                z10 = z12;
                                            }
                                            zzfw6 = zzfw3;
                                            zza7 = zza5;
                                            i6 = i5 + 1;
                                        } else {
                                            zza zza8 = zza7;
                                            zzfw zzfw7 = zzfw6;
                                            boolean z17 = z10;
                                            long j8 = j7;
                                            if (zze) {
                                                i = i8;
                                                j2 = j8;
                                                int i17 = 0;
                                                while (i17 < i) {
                                                    zzft zzft4 = zzfw7.zzaxi[i17];
                                                    if ("_e".equals(zzft4.name)) {
                                                        zzjr();
                                                        if (zzfu.zza(zzft4, "_fr") != null) {
                                                            System.arraycopy(zzfw7.zzaxi, i17 + 1, zzfw7.zzaxi, i17, (i - i17) - 1);
                                                            i--;
                                                            i17--;
                                                            i17++;
                                                        }
                                                    }
                                                    if (zzau) {
                                                        zzjr();
                                                        zzfu zza9 = zzfu.zza(zzft4, "_et");
                                                        if (zza9 != null) {
                                                            Long l3 = zza9.zzaxe;
                                                            if (l3 != null && l3.longValue() > 0) {
                                                                j2 += l3.longValue();
                                                            }
                                                        }
                                                    }
                                                    i17++;
                                                }
                                            } else {
                                                i = i8;
                                                j2 = j8;
                                            }
                                            if (i < zza8.zzaui.size()) {
                                                zzfw7.zzaxi = (zzft[]) Arrays.copyOf(zzfw7.zzaxi, i);
                                            }
                                            if (zzau) {
                                                zzfx zzi = zzjt().zzi(zzfw7.zztt, "_lte");
                                                if (zzi != null) {
                                                    if (zzi.value != null) {
                                                        zzfx zzfx2 = new zzfx(zzfw7.zztt, "auto", "_lte", this.zzada.zzbx().currentTimeMillis(), Long.valueOf(((Long) zzi.value).longValue() + j2));
                                                        zzfx = zzfx2;
                                                        zzfz zzfz = new zzfz();
                                                        zzfz.name = "_lte";
                                                        zzfz.zzayu = Long.valueOf(this.zzada.zzbx().currentTimeMillis());
                                                        zzfz.zzaxe = (Long) zzfx.value;
                                                        i4 = 0;
                                                        while (true) {
                                                            if (i4 < zzfw7.zzaxj.length) {
                                                                z4 = false;
                                                            } else if ("_lte".equals(zzfw7.zzaxj[i4].name)) {
                                                                zzfw7.zzaxj[i4] = zzfz;
                                                                z4 = true;
                                                            } else {
                                                                i4++;
                                                            }
                                                        }
                                                        if (!z4) {
                                                            zzfw7.zzaxj = (zzfz[]) Arrays.copyOf(zzfw7.zzaxj, zzfw7.zzaxj.length + 1);
                                                            zzfw7.zzaxj[zza8.zzaug.zzaxj.length - 1] = zzfz;
                                                        }
                                                        if (j2 > 0) {
                                                            zzjt().zza(zzfx);
                                                            this.zzada.zzgt().zzjn().zzg("Updated lifetime engagement user property with value. Value", zzfx.value);
                                                        }
                                                    }
                                                }
                                                zzfx = new zzfx(zzfw7.zztt, "auto", "_lte", this.zzada.zzbx().currentTimeMillis(), Long.valueOf(j2));
                                                zzfz zzfz2 = new zzfz();
                                                zzfz2.name = "_lte";
                                                zzfz2.zzayu = Long.valueOf(this.zzada.zzbx().currentTimeMillis());
                                                zzfz2.zzaxe = (Long) zzfx.value;
                                                i4 = 0;
                                                while (true) {
                                                    if (i4 < zzfw7.zzaxj.length) {
                                                    }
                                                    i4++;
                                                }
                                                if (!z4) {
                                                }
                                                if (j2 > 0) {
                                                }
                                            }
                                            String str9 = zzfw7.zztt;
                                            zzfz[] zzfzArr = zzfw7.zzaxj;
                                            zzft[] zzftArr2 = zzfw7.zzaxi;
                                            Preconditions.checkNotEmpty(str9);
                                            zzfw7.zzaya = zzjs().zza(str9, zzftArr2, zzfzArr);
                                            if (this.zzada.zzgv().zzat(zza8.zzaug.zztt)) {
                                                HashMap hashMap = new HashMap();
                                                zzft[] zzftArr3 = new zzft[zzfw7.zzaxi.length];
                                                SecureRandom zzmk = this.zzada.zzgr().zzmk();
                                                zzft[] zzftArr4 = zzfw7.zzaxi;
                                                int length3 = zzftArr4.length;
                                                int i18 = 0;
                                                int i19 = 0;
                                                while (i18 < length3) {
                                                    zzft zzft5 = zzftArr4[i18];
                                                    if (zzft5.name.equals("_ep")) {
                                                        zzjr();
                                                        String str10 = (String) zzfu.zzb(zzft5, "_en");
                                                        zzac zzac = (zzac) hashMap.get(str10);
                                                        if (zzac == null) {
                                                            zzac = zzjt().zzg(zza8.zzaug.zztt, str10);
                                                            hashMap.put(str10, zzac);
                                                        }
                                                        if (zzac.zzaia == null) {
                                                            if (zzac.zzaib.longValue() > 1) {
                                                                zzjr();
                                                                zzft5.zzaxa = zzfu.zza(zzft5.zzaxa, "_sr", (Object) zzac.zzaib);
                                                            }
                                                            if (zzac.zzaic == null || !zzac.zzaic.booleanValue()) {
                                                                zzftArr = zzftArr4;
                                                            } else {
                                                                zzjr();
                                                                zzftArr = zzftArr4;
                                                                zzft5.zzaxa = zzfu.zza(zzft5.zzaxa, "_efs", (Object) Long.valueOf(1));
                                                            }
                                                            int i20 = i19 + 1;
                                                            zzftArr3[i19] = zzft5;
                                                            zzfw = zzfw7;
                                                            secureRandom = zzmk;
                                                            i19 = i20;
                                                        } else {
                                                            zzftArr = zzftArr4;
                                                            zzfw = zzfw7;
                                                            secureRandom = zzmk;
                                                        }
                                                        i3 = length3;
                                                    } else {
                                                        zzftArr = zzftArr4;
                                                        long zzck = zzls().zzck(zza8.zzaug.zztt);
                                                        this.zzada.zzgr();
                                                        long zzc = zzfy.zzc(zzft5.zzaxb.longValue(), zzck);
                                                        String str11 = "_dbg";
                                                        i3 = length3;
                                                        long j9 = zzck;
                                                        Long valueOf = Long.valueOf(1);
                                                        if (!TextUtils.isEmpty(str11)) {
                                                            if (valueOf != null) {
                                                                zzfu[] zzfuArr9 = zzft5.zzaxa;
                                                                int length4 = zzfuArr9.length;
                                                                zzfw2 = zzfw7;
                                                                int i21 = 0;
                                                                while (true) {
                                                                    if (i21 < length4) {
                                                                        int i22 = length4;
                                                                        zzfu zzfu6 = zzfuArr9[i21];
                                                                        zzfu[] zzfuArr10 = zzfuArr9;
                                                                        if (!str11.equals(zzfu6.name)) {
                                                                            i21++;
                                                                            length4 = i22;
                                                                            zzfuArr9 = zzfuArr10;
                                                                        } else if (((valueOf instanceof Long) && valueOf.equals(zzfu6.zzaxe)) || (((valueOf instanceof String) && valueOf.equals(zzfu6.zzaml)) || ((valueOf instanceof Double) && valueOf.equals(zzfu6.zzaun)))) {
                                                                            z2 = true;
                                                                        }
                                                                    }
                                                                }
                                                                z2 = false;
                                                                zzq = z2 ? zzls().zzq(zza8.zzaug.zztt, zzft5.name) : 1;
                                                                if (zzq > 0) {
                                                                    this.zzada.zzgt().zzjj().zze("Sample rate must be positive. event, rate", zzft5.name, Integer.valueOf(zzq));
                                                                    int i23 = i19 + 1;
                                                                    zzftArr3[i19] = zzft5;
                                                                    i19 = i23;
                                                                    secureRandom = zzmk;
                                                                } else {
                                                                    zzac zzac2 = (zzac) hashMap.get(zzft5.name);
                                                                    if (zzac2 == null) {
                                                                        zzac2 = zzjt().zzg(zza8.zzaug.zztt, zzft5.name);
                                                                        if (zzac2 == null) {
                                                                            this.zzada.zzgt().zzjj().zze("Event being bundled has no eventAggregate. appId, eventName", zza8.zzaug.zztt, zzft5.name);
                                                                            zzac2 = new zzac(zza8.zzaug.zztt, zzft5.name, 1, 1, zzft5.zzaxb.longValue(), 0, null, null, null, null);
                                                                        }
                                                                    }
                                                                    zzjr();
                                                                    Long l4 = (Long) zzfu.zzb(zzft5, "_eid");
                                                                    Boolean valueOf2 = Boolean.valueOf(l4 != null);
                                                                    if (zzq == 1) {
                                                                        int i24 = i19 + 1;
                                                                        zzftArr3[i19] = zzft5;
                                                                        if (valueOf2.booleanValue() && !(zzac2.zzaia == null && zzac2.zzaib == null && zzac2.zzaic == null)) {
                                                                            hashMap.put(zzft5.name, zzac2.zza(null, null, null));
                                                                        }
                                                                        secureRandom = zzmk;
                                                                        i19 = i24;
                                                                    } else if (zzmk.nextInt(zzq) == 0) {
                                                                        zzjr();
                                                                        zza zza10 = zza8;
                                                                        i2 = i18;
                                                                        long j10 = (long) zzq;
                                                                        zzft5.zzaxa = zzfu.zza(zzft5.zzaxa, "_sr", (Object) Long.valueOf(j10));
                                                                        int i25 = i19 + 1;
                                                                        zzftArr3[i19] = zzft5;
                                                                        if (valueOf2.booleanValue()) {
                                                                            zzac2 = zzac2.zza(null, Long.valueOf(j10), null);
                                                                        }
                                                                        hashMap.put(zzft5.name, zzac2.zza(zzft5.zzaxb.longValue(), zzc));
                                                                        secureRandom = zzmk;
                                                                        i19 = i25;
                                                                        zza3 = zza10;
                                                                        i18 = i2 + 1;
                                                                        zzftArr4 = zzftArr;
                                                                        length3 = i3;
                                                                        zzfw7 = zzfw;
                                                                        zza8 = zza3;
                                                                        zzmk = secureRandom;
                                                                    } else {
                                                                        i2 = i18;
                                                                        zza zza11 = zza8;
                                                                        if (this.zzada.zzgv().zzbf(zza11.zzaug.zztt)) {
                                                                            if (zzac2.zzahz != null) {
                                                                                j4 = zzac2.zzahz.longValue();
                                                                                secureRandom2 = zzmk;
                                                                                l = l4;
                                                                                zza4 = zza11;
                                                                            } else {
                                                                                this.zzada.zzgr();
                                                                                zza4 = zza11;
                                                                                secureRandom2 = zzmk;
                                                                                l = l4;
                                                                                j4 = zzfy.zzc(zzft5.zzaxc.longValue(), j9);
                                                                            }
                                                                            z3 = j4 != zzc;
                                                                            j3 = zzc;
                                                                        } else {
                                                                            secureRandom2 = zzmk;
                                                                            l = l4;
                                                                            zza4 = zza11;
                                                                            j3 = zzc;
                                                                            z3 = Math.abs(zzft5.zzaxb.longValue() - zzac2.zzahy) >= 86400000;
                                                                        }
                                                                        if (z3) {
                                                                            zzjr();
                                                                            zzft5.zzaxa = zzfu.zza(zzft5.zzaxa, "_efs", (Object) Long.valueOf(1));
                                                                            zzjr();
                                                                            long j11 = (long) zzq;
                                                                            zzft5.zzaxa = zzfu.zza(zzft5.zzaxa, "_sr", (Object) Long.valueOf(j11));
                                                                            int i26 = i19 + 1;
                                                                            zzftArr3[i19] = zzft5;
                                                                            if (valueOf2.booleanValue()) {
                                                                                zzac2 = zzac2.zza(null, Long.valueOf(j11), Boolean.valueOf(true));
                                                                            }
                                                                            hashMap.put(zzft5.name, zzac2.zza(zzft5.zzaxb.longValue(), j3));
                                                                            i19 = i26;
                                                                        } else if (valueOf2.booleanValue()) {
                                                                            hashMap.put(zzft5.name, zzac2.zza(l, null, null));
                                                                        }
                                                                        i18 = i2 + 1;
                                                                        zzftArr4 = zzftArr;
                                                                        length3 = i3;
                                                                        zzfw7 = zzfw;
                                                                        zza8 = zza3;
                                                                        zzmk = secureRandom;
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        zzfw2 = zzfw7;
                                                        z2 = false;
                                                        if (z2) {
                                                        }
                                                        if (zzq > 0) {
                                                        }
                                                    }
                                                    zza3 = zza8;
                                                    i2 = i18;
                                                    i18 = i2 + 1;
                                                    zzftArr4 = zzftArr;
                                                    length3 = i3;
                                                    zzfw7 = zzfw;
                                                    zza8 = zza3;
                                                    zzmk = secureRandom;
                                                }
                                                zza2 = zza8;
                                                if (i19 < zzfw7.zzaxi.length) {
                                                    zzfw7.zzaxi = (zzft[]) Arrays.copyOf(zzftArr3, i19);
                                                }
                                                for (Entry value : hashMap.entrySet()) {
                                                    zzjt().zza((zzac) value.getValue());
                                                }
                                            } else {
                                                zza2 = zza8;
                                            }
                                            zzfw7.zzaxl = Long.valueOf(Long.MAX_VALUE);
                                            zzfw7.zzaxm = Long.valueOf(Long.MIN_VALUE);
                                            for (zzft zzft6 : zzfw7.zzaxi) {
                                                if (zzft6.zzaxb.longValue() < zzfw7.zzaxl.longValue()) {
                                                    zzfw7.zzaxl = zzft6.zzaxb;
                                                }
                                                if (zzft6.zzaxb.longValue() > zzfw7.zzaxm.longValue()) {
                                                    zzfw7.zzaxm = zzft6.zzaxb;
                                                }
                                            }
                                            zza zza12 = zza2;
                                            String str12 = zza12.zzaug.zztt;
                                            zzg zzbm = zzjt().zzbm(str12);
                                            if (zzbm == null) {
                                                this.zzada.zzgt().zzjg().zzg("Bundling raw events w/o app info. appId", zzas.zzbw(zza12.zzaug.zztt));
                                            } else if (zzfw7.zzaxi.length > 0) {
                                                long zzhe = zzbm.zzhe();
                                                zzfw7.zzaxo = zzhe != 0 ? Long.valueOf(zzhe) : null;
                                                long zzhd = zzbm.zzhd();
                                                if (zzhd != 0) {
                                                    zzhe = zzhd;
                                                }
                                                zzfw7.zzaxn = zzhe != 0 ? Long.valueOf(zzhe) : null;
                                                zzbm.zzhm();
                                                zzfw7.zzaxy = Integer.valueOf((int) zzbm.zzhj());
                                                zzbm.zzo(zzfw7.zzaxl.longValue());
                                                zzbm.zzp(zzfw7.zzaxm.longValue());
                                                zzfw7.zzagm = zzbm.zzhu();
                                                zzjt().zza(zzbm);
                                            }
                                            if (zzfw7.zzaxi.length > 0) {
                                                this.zzada.zzgw();
                                                zzfp zzcg = zzls().zzcg(zza12.zzaug.zztt);
                                                if (zzcg != null) {
                                                    if (zzcg.zzawk != null) {
                                                        zzfw7.zzayf = zzcg.zzawk;
                                                        zzjt().zza(zzfw7, z17);
                                                    }
                                                }
                                                if (TextUtils.isEmpty(zza12.zzaug.zzafi)) {
                                                    zzfw7.zzayf = Long.valueOf(-1);
                                                } else {
                                                    this.zzada.zzgt().zzjj().zzg("Did not find measurement config or missing version info. appId", zzas.zzbw(zza12.zzaug.zztt));
                                                }
                                                zzjt().zza(zzfw7, z17);
                                            }
                                            zzt zzjt2 = zzjt();
                                            List<Long> list = zza12.zzauh;
                                            Preconditions.checkNotNull(list);
                                            zzjt2.zzaf();
                                            zzjt2.zzcl();
                                            StringBuilder sb2 = new StringBuilder("rowid in (");
                                            for (int i27 = 0; i27 < list.size(); i27++) {
                                                if (i27 != 0) {
                                                    sb2.append(",");
                                                }
                                                sb2.append(((Long) list.get(i27)).longValue());
                                            }
                                            sb2.append(")");
                                            int delete = zzjt2.getWritableDatabase().delete("raw_events", sb2.toString(), null);
                                            if (delete != list.size()) {
                                                zzjt2.zzgt().zzjg().zze("Deleted fewer rows from raw events table than expected", Integer.valueOf(delete), Integer.valueOf(list.size()));
                                            }
                                            zzt zzjt3 = zzjt();
                                            try {
                                                zzjt3.getWritableDatabase().execSQL("delete from raw_events_metadata where app_id=? and metadata_fingerprint not in (select distinct metadata_fingerprint from raw_events where app_id=?)", new String[]{str12, str12});
                                            } catch (SQLiteException e2) {
                                                zzjt3.zzgt().zzjg().zze("Failed to remove unused event metadata. appId", zzas.zzbw(str12), e2);
                                            }
                                            zzjt().setTransactionSuccessful();
                                            zzjt().endTransaction();
                                            return true;
                                        }
                                    }
                                } else {
                                    zzjt().setTransactionSuccessful();
                                    zzjt().endTransaction();
                                    return false;
                                }
                            }
                        }
                        z = true;
                        if (!z) {
                        }
                    } else {
                        str2 = cursor3.getString(0);
                        String string = cursor3.getString(1);
                        cursor3.close();
                        cursor2 = cursor3;
                        str4 = str2;
                        str3 = string;
                    }
                } else {
                    String[] strArr3 = j6 != -1 ? new String[]{null, String.valueOf(j6)} : new String[]{null};
                    String str13 = j6 != -1 ? " and rowid <= ?" : "";
                    StringBuilder sb3 = new StringBuilder(84 + String.valueOf(str13).length());
                    sb3.append("select metadata_fingerprint from raw_events where app_id = ?");
                    sb3.append(str13);
                    sb3.append(" order by rowid limit 1;");
                    cursor3 = writableDatabase.rawQuery(sb3.toString(), strArr3);
                    if (!cursor3.moveToFirst()) {
                        if (cursor3 != null) {
                            cursor3.close();
                        }
                        if (zza7.zzaui != null) {
                        }
                        z = true;
                        if (!z) {
                        }
                    } else {
                        String string2 = cursor3.getString(0);
                        cursor3.close();
                        cursor2 = cursor3;
                        str3 = string2;
                        str4 = null;
                    }
                }
                try {
                    sQLiteDatabase = writableDatabase;
                    cursor = writableDatabase.query("raw_events_metadata", new String[]{TtmlNode.TAG_METADATA}, "app_id = ? and metadata_fingerprint = ?", new String[]{str4, str3}, null, null, "rowid", "2");
                } catch (SQLiteException e3) {
                    e = e3;
                    str2 = str4;
                    cursor = cursor2;
                    obj = e;
                    try {
                        zzjt.zzgt().zzjg().zze("Data loss. Error selecting raw event. appId", zzas.zzbw(str2), obj);
                        if (cursor != null) {
                        }
                        if (zza7.zzaui != null) {
                        }
                        z = true;
                        if (!z) {
                        }
                    } catch (Throwable th3) {
                        th = th3;
                        if (cursor != null) {
                            cursor.close();
                        }
                        throw th;
                    }
                } catch (Throwable th4) {
                    th = th4;
                    cursor = cursor2;
                    if (cursor != null) {
                    }
                    throw th;
                }
                try {
                    if (!cursor.moveToFirst()) {
                        zzjt.zzgt().zzjg().zzg("Raw event metadata record is missing. appId", zzas.zzbw(str4));
                        if (cursor != null) {
                            cursor.close();
                        }
                        if (zza7.zzaui != null) {
                        }
                        z = true;
                        if (!z) {
                        }
                    } else {
                        byte[] blob = cursor.getBlob(0);
                        zzxz zzj = zzxz.zzj(blob, 0, blob.length);
                        zzfw zzfw8 = new zzfw();
                        zzfw8.zza(zzj);
                        if (cursor.moveToNext()) {
                            zzjt.zzgt().zzjj().zzg("Get multiple raw event metadata records, expected one. appId", zzas.zzbw(str4));
                        }
                        cursor.close();
                        zza7.zzb(zzfw8);
                        if (j6 != -1) {
                            str5 = "app_id = ? and metadata_fingerprint = ? and rowid <= ?";
                            strArr = new String[]{str4, str3, String.valueOf(j6)};
                        } else {
                            str5 = "app_id = ? and metadata_fingerprint = ?";
                            strArr = new String[]{str4, str3};
                        }
                        Cursor query = sQLiteDatabase.query("raw_events", new String[]{"rowid", "name", AppMeasurement.Param.TIMESTAMP, ShareConstants.WEB_DIALOG_PARAM_DATA}, str5, strArr, null, null, "rowid", null);
                        try {
                            if (!query.moveToFirst()) {
                                zzjt.zzgt().zzjj().zzg("Raw event data disappeared while in transaction. appId", zzas.zzbw(str4));
                                if (query != null) {
                                    query.close();
                                }
                                if (zza7.zzaui != null) {
                                }
                                z = true;
                                if (!z) {
                                }
                            } else {
                                do {
                                    long j12 = query.getLong(0);
                                    byte[] blob2 = query.getBlob(3);
                                    zzxz zzj2 = zzxz.zzj(blob2, 0, blob2.length);
                                    zzft zzft7 = new zzft();
                                    try {
                                        zzft7.zza(zzj2);
                                        zzft7.name = query.getString(1);
                                        zzft7.zzaxb = Long.valueOf(query.getLong(2));
                                        if (!zza7.zza(j12, zzft7)) {
                                            if (query != null) {
                                                query.close();
                                            }
                                            if (zza7.zzaui != null) {
                                            }
                                            z = true;
                                            if (!z) {
                                            }
                                        }
                                    } catch (IOException e4) {
                                        zzjt.zzgt().zzjg().zze("Data loss. Failed to merge raw event. appId", zzas.zzbw(str4), e4);
                                    }
                                } while (query.moveToNext());
                                if (query != null) {
                                    query.close();
                                }
                                if (zza7.zzaui != null) {
                                }
                                z = true;
                                if (!z) {
                                }
                            }
                        } catch (SQLiteException e5) {
                            e = e5;
                            str2 = str4;
                            cursor = query;
                            obj = e;
                            zzjt.zzgt().zzjg().zze("Data loss. Error selecting raw event. appId", zzas.zzbw(str2), obj);
                            if (cursor != null) {
                            }
                            if (zza7.zzaui != null) {
                            }
                            z = true;
                            if (!z) {
                            }
                        } catch (Throwable th5) {
                            th = th5;
                            cursor = query;
                            if (cursor != null) {
                            }
                            throw th;
                        }
                    }
                } catch (SQLiteException e6) {
                    e = e6;
                    str2 = str4;
                    obj = e;
                    zzjt.zzgt().zzjg().zze("Data loss. Error selecting raw event. appId", zzas.zzbw(str2), obj);
                    if (cursor != null) {
                    }
                    if (zza7.zzaui != null) {
                    }
                    z = true;
                    if (!z) {
                    }
                }
            } catch (SQLiteException e7) {
                obj = e7;
                str2 = null;
                cursor = null;
                zzjt.zzgt().zzjg().zze("Data loss. Error selecting raw event. appId", zzas.zzbw(str2), obj);
                if (cursor != null) {
                    cursor.close();
                }
                if (zza7.zzaui != null) {
                }
                z = true;
                if (!z) {
                }
            } catch (Throwable th6) {
                th = th6;
                cursor = null;
                if (cursor != null) {
                }
                throw th;
            }
        } catch (IOException e8) {
            zzjt.zzgt().zzjg().zze("Data loss. Failed to merge raw event metadata. appId", zzas.zzbw(str4), e8);
            if (cursor != null) {
                cursor.close();
            }
        } catch (Throwable th7) {
            Throwable th8 = th7;
            zzjt().endTransaction();
            throw th8;
        }
    }

    private final boolean zza(zzft zzft, zzft zzft2) {
        Object obj;
        Preconditions.checkArgument("_e".equals(zzft.name));
        zzjr();
        zzfu zza2 = zzfu.zza(zzft, "_sc");
        String str = null;
        if (zza2 == null) {
            obj = null;
        } else {
            obj = zza2.zzaml;
        }
        zzjr();
        zzfu zza3 = zzfu.zza(zzft2, "_pc");
        if (zza3 != null) {
            str = zza3.zzaml;
        }
        if (str == null || !str.equals(obj)) {
            return false;
        }
        zzjr();
        zzfu zza4 = zzfu.zza(zzft, "_et");
        if (zza4.zzaxe == null || zza4.zzaxe.longValue() <= 0) {
            return true;
        }
        long longValue = zza4.zzaxe.longValue();
        zzjr();
        zzfu zza5 = zzfu.zza(zzft2, "_et");
        if (!(zza5 == null || zza5.zzaxe == null || zza5.zzaxe.longValue() <= 0)) {
            longValue += zza5.zzaxe.longValue();
        }
        zzjr();
        zzft2.zzaxa = zzfu.zza(zzft2.zzaxa, "_et", (Object) Long.valueOf(longValue));
        zzjr();
        zzft.zzaxa = zzfu.zza(zzft.zzaxa, "_fr", (Object) Long.valueOf(1));
        return true;
    }

    @VisibleForTesting
    private static zzfu[] zza(zzfu[] zzfuArr, @NonNull String str) {
        int i = 0;
        while (true) {
            if (i >= zzfuArr.length) {
                i = -1;
                break;
            } else if (str.equals(zzfuArr[i].name)) {
                break;
            } else {
                i++;
            }
        }
        if (i < 0) {
            return zzfuArr;
        }
        return zza(zzfuArr, i);
    }

    @VisibleForTesting
    private static zzfu[] zza(zzfu[] zzfuArr, int i) {
        zzfu[] zzfuArr2 = new zzfu[(zzfuArr.length - 1)];
        if (i > 0) {
            System.arraycopy(zzfuArr, 0, zzfuArr2, 0, i);
        }
        if (i < zzfuArr2.length) {
            System.arraycopy(zzfuArr, i + 1, zzfuArr2, i, zzfuArr2.length - i);
        }
        return zzfuArr2;
    }

    @VisibleForTesting
    private static zzfu[] zza(zzfu[] zzfuArr, int i, String str) {
        for (zzfu zzfu : zzfuArr) {
            if ("_err".equals(zzfu.name)) {
                return zzfuArr;
            }
        }
        zzfu[] zzfuArr2 = new zzfu[(zzfuArr.length + 2)];
        System.arraycopy(zzfuArr, 0, zzfuArr2, 0, zzfuArr.length);
        zzfu zzfu2 = new zzfu();
        zzfu2.name = "_err";
        zzfu2.zzaxe = Long.valueOf((long) i);
        zzfu zzfu3 = new zzfu();
        zzfu3.name = "_ev";
        zzfu3.zzaml = str;
        zzfuArr2[zzfuArr2.length - 2] = zzfu2;
        zzfuArr2[zzfuArr2.length - 1] = zzfu3;
        return zzfuArr2;
    }

    /* JADX INFO: finally extract failed */
    /* access modifiers changed from: 0000 */
    @WorkerThread
    @VisibleForTesting
    public final void zza(int i, Throwable th, byte[] bArr, String str) {
        zzt zzjt;
        zzaf();
        zzlx();
        if (bArr == null) {
            try {
                bArr = new byte[0];
            } catch (Throwable th2) {
                this.zzatw = false;
                zzmc();
                throw th2;
            }
        }
        List<Long> list = this.zzaua;
        this.zzaua = null;
        boolean z = true;
        if ((i == 200 || i == 204) && th == null) {
            try {
                this.zzada.zzgu().zzana.set(this.zzada.zzbx().currentTimeMillis());
                this.zzada.zzgu().zzanb.set(0);
                zzmb();
                this.zzada.zzgt().zzjo().zze("Successful upload. Got network response. code, size", Integer.valueOf(i), Integer.valueOf(bArr.length));
                zzjt().beginTransaction();
                try {
                    for (Long l : list) {
                        try {
                            zzjt = zzjt();
                            long longValue = l.longValue();
                            zzjt.zzaf();
                            zzjt.zzcl();
                            if (zzjt.getWritableDatabase().delete("queue", "rowid=?", new String[]{String.valueOf(longValue)}) != 1) {
                                throw new SQLiteException("Deleted fewer rows from queue than expected");
                            }
                        } catch (SQLiteException e) {
                            zzjt.zzgt().zzjg().zzg("Failed to delete a bundle in a queue table", e);
                            throw e;
                        } catch (SQLiteException e2) {
                            if (this.zzaub == null || !this.zzaub.contains(l)) {
                                throw e2;
                            }
                        }
                    }
                    zzjt().setTransactionSuccessful();
                    zzjt().endTransaction();
                    this.zzaub = null;
                    if (!zzlt().zzfb() || !zzma()) {
                        this.zzauc = -1;
                        zzmb();
                    } else {
                        zzlz();
                    }
                    this.zzatr = 0;
                } catch (Throwable th3) {
                    zzjt().endTransaction();
                    throw th3;
                }
            } catch (SQLiteException e3) {
                this.zzada.zzgt().zzjg().zzg("Database error while trying to delete uploaded bundles", e3);
                this.zzatr = this.zzada.zzbx().elapsedRealtime();
                this.zzada.zzgt().zzjo().zzg("Disable upload, time", Long.valueOf(this.zzatr));
            }
        } else {
            this.zzada.zzgt().zzjo().zze("Network upload failed. Will retry later. code, error", Integer.valueOf(i), th);
            this.zzada.zzgu().zzanb.set(this.zzada.zzbx().currentTimeMillis());
            if (i != 503) {
                if (i != 429) {
                    z = false;
                }
            }
            if (z) {
                this.zzada.zzgu().zzanc.set(this.zzada.zzbx().currentTimeMillis());
            }
            if (this.zzada.zzgv().zzaw(str)) {
                zzjt().zzc(list);
            }
            zzmb();
        }
        this.zzatw = false;
        zzmc();
    }

    private final boolean zzma() {
        zzaf();
        zzlx();
        return zzjt().zzim() || !TextUtils.isEmpty(zzjt().zzih());
    }

    @WorkerThread
    private final void zzb(zzg zzg) {
        Map map;
        zzaf();
        if (!TextUtils.isEmpty(zzg.getGmpAppId()) || (zzq.zzig() && !TextUtils.isEmpty(zzg.zzhb()))) {
            zzq zzgv = this.zzada.zzgv();
            Builder builder = new Builder();
            String gmpAppId = zzg.getGmpAppId();
            if (TextUtils.isEmpty(gmpAppId) && zzq.zzig()) {
                gmpAppId = zzg.zzhb();
            }
            Builder encodedAuthority = builder.scheme((String) zzai.zzaiy.get()).encodedAuthority((String) zzai.zzaiz.get());
            String str = "config/app/";
            String valueOf = String.valueOf(gmpAppId);
            encodedAuthority.path(valueOf.length() != 0 ? str.concat(valueOf) : new String(str)).appendQueryParameter("app_instance_id", zzg.getAppInstanceId()).appendQueryParameter("platform", "android").appendQueryParameter("gmp_version", String.valueOf(zzgv.zzhh()));
            String uri = builder.build().toString();
            try {
                URL url = new URL(uri);
                this.zzada.zzgt().zzjo().zzg("Fetching remote configuration", zzg.zzal());
                zzfp zzcg = zzls().zzcg(zzg.zzal());
                String zzch = zzls().zzch(zzg.zzal());
                if (zzcg == null || TextUtils.isEmpty(zzch)) {
                    map = null;
                } else {
                    ArrayMap arrayMap = new ArrayMap();
                    arrayMap.put("If-Modified-Since", zzch);
                    map = arrayMap;
                }
                this.zzatv = true;
                zzaw zzlt = zzlt();
                String zzal = zzg.zzal();
                zzfr zzfr = new zzfr(this);
                zzlt.zzaf();
                zzlt.zzcl();
                Preconditions.checkNotNull(url);
                Preconditions.checkNotNull(zzfr);
                zzbr zzgs = zzlt.zzgs();
                zzba zzba = new zzba(zzlt, zzal, url, null, map, zzfr);
                zzgs.zzd((Runnable) zzba);
            } catch (MalformedURLException unused) {
                this.zzada.zzgt().zzjg().zze("Failed to parse config URL. Not fetching. appId", zzas.zzbw(zzg.zzal()), uri);
            }
        } else {
            zzb(zzg.zzal(), AppLovinErrorCodes.NO_FILL, null, null, null);
        }
    }

    /* access modifiers changed from: 0000 */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x013e A[Catch:{ all -> 0x0191, all -> 0x000f }] */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x014e A[Catch:{ all -> 0x0191, all -> 0x000f }] */
    @WorkerThread
    @VisibleForTesting
    public final void zzb(String str, int i, Throwable th, byte[] bArr, Map<String, List<String>> map) {
        zzaf();
        zzlx();
        Preconditions.checkNotEmpty(str);
        if (bArr == null) {
            try {
                bArr = new byte[0];
            } catch (Throwable th2) {
                this.zzatv = false;
                zzmc();
                throw th2;
            }
        }
        this.zzada.zzgt().zzjo().zzg("onConfigFetched. Response size", Integer.valueOf(bArr.length));
        zzjt().beginTransaction();
        zzg zzbm = zzjt().zzbm(str);
        boolean z = true;
        boolean z2 = (i == 200 || i == 204 || i == 304) && th == null;
        if (zzbm == null) {
            this.zzada.zzgt().zzjj().zzg("App does not exist in onConfigFetched. appId", zzas.zzbw(str));
        } else {
            if (!z2) {
                if (i != 404) {
                    zzbm.zzv(this.zzada.zzbx().currentTimeMillis());
                    zzjt().zza(zzbm);
                    this.zzada.zzgt().zzjo().zze("Fetching config failed. code, error", Integer.valueOf(i), th);
                    zzls().zzci(str);
                    this.zzada.zzgu().zzanb.set(this.zzada.zzbx().currentTimeMillis());
                    if (i != 503) {
                        if (i != 429) {
                            z = false;
                        }
                    }
                    if (z) {
                        this.zzada.zzgu().zzanc.set(this.zzada.zzbx().currentTimeMillis());
                    }
                    zzmb();
                }
            }
            List list = map != null ? (List) map.get(HttpRequest.HEADER_LAST_MODIFIED) : null;
            String str2 = (list == null || list.size() <= 0) ? null : (String) list.get(0);
            if (i != 404) {
                if (i != 304) {
                    if (!zzls().zza(str, bArr, str2)) {
                        zzjt().endTransaction();
                        this.zzatv = false;
                        zzmc();
                        return;
                    }
                    zzbm.zzu(this.zzada.zzbx().currentTimeMillis());
                    zzjt().zza(zzbm);
                    if (i != 404) {
                        this.zzada.zzgt().zzjl().zzg("Config not found. Using empty config. appId", str);
                    } else {
                        this.zzada.zzgt().zzjo().zze("Successfully fetched config. Got network response. code, size", Integer.valueOf(i), Integer.valueOf(bArr.length));
                    }
                    if (zzlt().zzfb() || !zzma()) {
                        zzmb();
                    } else {
                        zzlz();
                    }
                }
            }
            if (zzls().zzcg(str) == null && !zzls().zza(str, null, null)) {
                zzjt().endTransaction();
                this.zzatv = false;
                zzmc();
                return;
            }
            zzbm.zzu(this.zzada.zzbx().currentTimeMillis());
            zzjt().zza(zzbm);
            if (i != 404) {
            }
            if (zzlt().zzfb()) {
            }
            zzmb();
        }
        zzjt().setTransactionSuccessful();
        zzjt().endTransaction();
        this.zzatv = false;
        zzmc();
    }

    /* JADX WARNING: Removed duplicated region for block: B:54:0x01a8  */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x01c6  */
    @WorkerThread
    private final void zzmb() {
        long j;
        long j2;
        zzaf();
        zzlx();
        if (zzmf() || this.zzada.zzgv().zza(zzai.zzald)) {
            if (this.zzatr > 0) {
                long abs = 3600000 - Math.abs(this.zzada.zzbx().elapsedRealtime() - this.zzatr);
                if (abs > 0) {
                    this.zzada.zzgt().zzjo().zzg("Upload has been suspended. Will update scheduling later in approximately ms", Long.valueOf(abs));
                    zzlu().unregister();
                    zzlv().cancel();
                    return;
                }
                this.zzatr = 0;
            }
            if (!this.zzada.zzkv() || !zzma()) {
                this.zzada.zzgt().zzjo().zzby("Nothing to upload or uploading impossible");
                zzlu().unregister();
                zzlv().cancel();
                return;
            }
            long currentTimeMillis = this.zzada.zzbx().currentTimeMillis();
            long max = Math.max(0, ((Long) zzai.zzaju.get()).longValue());
            boolean z = zzjt().zzin() || zzjt().zzii();
            if (z) {
                String zzid = this.zzada.zzgv().zzid();
                if (TextUtils.isEmpty(zzid) || ".none.".equals(zzid)) {
                    j = Math.max(0, ((Long) zzai.zzajo.get()).longValue());
                } else {
                    j = Math.max(0, ((Long) zzai.zzajp.get()).longValue());
                }
            } else {
                j = Math.max(0, ((Long) zzai.zzajn.get()).longValue());
            }
            long j3 = this.zzada.zzgu().zzana.get();
            long j4 = this.zzada.zzgu().zzanb.get();
            long j5 = j;
            long j6 = max;
            long max2 = Math.max(zzjt().zzik(), zzjt().zzil());
            if (max2 != 0) {
                long abs2 = currentTimeMillis - Math.abs(max2 - currentTimeMillis);
                long abs3 = currentTimeMillis - Math.abs(j4 - currentTimeMillis);
                long max3 = Math.max(currentTimeMillis - Math.abs(j3 - currentTimeMillis), abs3);
                j2 = abs2 + j6;
                if (z && max3 > 0) {
                    j2 = Math.min(abs2, max3) + j5;
                }
                long j7 = j5;
                if (!zzjr().zzb(max3, j7)) {
                    j2 = max3 + j7;
                }
                if (abs3 != 0 && abs3 >= abs2) {
                    int i = 0;
                    while (true) {
                        if (i >= Math.min(20, Math.max(0, ((Integer) zzai.zzajw.get()).intValue()))) {
                            break;
                        }
                        long max4 = j2 + (Math.max(0, ((Long) zzai.zzajv.get()).longValue()) * (1 << i));
                        if (max4 > abs3) {
                            j2 = max4;
                            break;
                        } else {
                            i++;
                            j2 = max4;
                        }
                    }
                }
                if (j2 != 0) {
                    this.zzada.zzgt().zzjo().zzby("Next upload time is 0");
                    zzlu().unregister();
                    zzlv().cancel();
                    return;
                } else if (!zzlt().zzfb()) {
                    this.zzada.zzgt().zzjo().zzby("No network");
                    zzlu().zzey();
                    zzlv().cancel();
                    return;
                } else {
                    long j8 = this.zzada.zzgu().zzanc.get();
                    long max5 = Math.max(0, ((Long) zzai.zzajl.get()).longValue());
                    if (!zzjr().zzb(j8, max5)) {
                        j2 = Math.max(j2, j8 + max5);
                    }
                    zzlu().unregister();
                    long currentTimeMillis2 = j2 - this.zzada.zzbx().currentTimeMillis();
                    if (currentTimeMillis2 <= 0) {
                        currentTimeMillis2 = Math.max(0, ((Long) zzai.zzajq.get()).longValue());
                        this.zzada.zzgu().zzana.set(this.zzada.zzbx().currentTimeMillis());
                    }
                    this.zzada.zzgt().zzjo().zzg("Upload scheduled in approximately ms", Long.valueOf(currentTimeMillis2));
                    zzlv().zzh(currentTimeMillis2);
                    return;
                }
            }
            j2 = 0;
            if (j2 != 0) {
            }
        }
    }

    /* access modifiers changed from: 0000 */
    @WorkerThread
    public final void zzg(Runnable runnable) {
        zzaf();
        if (this.zzats == null) {
            this.zzats = new ArrayList();
        }
        this.zzats.add(runnable);
    }

    @WorkerThread
    private final void zzmc() {
        zzaf();
        if (this.zzatv || this.zzatw || this.zzatx) {
            this.zzada.zzgt().zzjo().zzd("Not stopping services. fetch, network, upload", Boolean.valueOf(this.zzatv), Boolean.valueOf(this.zzatw), Boolean.valueOf(this.zzatx));
            return;
        }
        this.zzada.zzgt().zzjo().zzby("Stopping uploading service(s)");
        if (this.zzats != null) {
            for (Runnable run : this.zzats) {
                run.run();
            }
            this.zzats.clear();
        }
    }

    @WorkerThread
    private final Boolean zzc(zzg zzg) {
        try {
            if (zzg.zzhf() != -2147483648L) {
                if (zzg.zzhf() == ((long) Wrappers.packageManager(this.zzada.getContext()).getPackageInfo(zzg.zzal(), 0).versionCode)) {
                    return Boolean.valueOf(true);
                }
            } else {
                String str = Wrappers.packageManager(this.zzada.getContext()).getPackageInfo(zzg.zzal(), 0).versionName;
                if (zzg.zzak() != null && zzg.zzak().equals(str)) {
                    return Boolean.valueOf(true);
                }
            }
            return Boolean.valueOf(false);
        } catch (NameNotFoundException unused) {
            return null;
        }
    }

    @WorkerThread
    @VisibleForTesting
    private final boolean zzmd() {
        zzaf();
        try {
            this.zzatz = new RandomAccessFile(new File(this.zzada.getContext().getFilesDir(), "google_app_measurement.db"), "rw").getChannel();
            this.zzaty = this.zzatz.tryLock();
            if (this.zzaty != null) {
                this.zzada.zzgt().zzjo().zzby("Storage concurrent access okay");
                return true;
            }
            this.zzada.zzgt().zzjg().zzby("Storage concurrent data access panic");
            return false;
        } catch (FileNotFoundException e) {
            this.zzada.zzgt().zzjg().zzg("Failed to acquire storage lock", e);
        } catch (IOException e2) {
            this.zzada.zzgt().zzjg().zzg("Failed to access storage lock file", e2);
        }
    }

    @WorkerThread
    @VisibleForTesting
    private final int zza(FileChannel fileChannel) {
        int i;
        zzaf();
        if (fileChannel == null || !fileChannel.isOpen()) {
            this.zzada.zzgt().zzjg().zzby("Bad channel to read from");
            return 0;
        }
        ByteBuffer allocate = ByteBuffer.allocate(4);
        try {
            fileChannel.position(0);
            int read = fileChannel.read(allocate);
            if (read != 4) {
                if (read != -1) {
                    this.zzada.zzgt().zzjj().zzg("Unexpected data length. Bytes read", Integer.valueOf(read));
                }
                return 0;
            }
            allocate.flip();
            i = allocate.getInt();
            return i;
        } catch (IOException e) {
            this.zzada.zzgt().zzjg().zzg("Failed to read from channel", e);
            i = 0;
        }
    }

    @WorkerThread
    @VisibleForTesting
    private final boolean zza(int i, FileChannel fileChannel) {
        zzaf();
        if (fileChannel == null || !fileChannel.isOpen()) {
            this.zzada.zzgt().zzjg().zzby("Bad channel to read from");
            return false;
        }
        ByteBuffer allocate = ByteBuffer.allocate(4);
        allocate.putInt(i);
        allocate.flip();
        try {
            fileChannel.truncate(0);
            fileChannel.write(allocate);
            fileChannel.force(true);
            if (fileChannel.size() != 4) {
                this.zzada.zzgt().zzjg().zzg("Error writing to channel. Bytes written", Long.valueOf(fileChannel.size()));
            }
            return true;
        } catch (IOException e) {
            this.zzada.zzgt().zzjg().zzg("Failed to write to channel", e);
            return false;
        }
    }

    /* access modifiers changed from: 0000 */
    @WorkerThread
    public final void zzme() {
        zzaf();
        zzlx();
        if (!this.zzatq) {
            this.zzatq = true;
            zzaf();
            zzlx();
            if ((this.zzada.zzgv().zza(zzai.zzald) || zzmf()) && zzmd()) {
                int zza2 = zza(this.zzatz);
                int zzjd = this.zzada.zzgk().zzjd();
                zzaf();
                if (zza2 > zzjd) {
                    this.zzada.zzgt().zzjg().zze("Panic: can't downgrade version. Previous, current version", Integer.valueOf(zza2), Integer.valueOf(zzjd));
                } else if (zza2 < zzjd) {
                    if (zza(zzjd, this.zzatz)) {
                        this.zzada.zzgt().zzjo().zze("Storage version upgraded. Previous, current version", Integer.valueOf(zza2), Integer.valueOf(zzjd));
                    } else {
                        this.zzada.zzgt().zzjg().zze("Storage version upgrade failed. Previous, current version", Integer.valueOf(zza2), Integer.valueOf(zzjd));
                    }
                }
            }
        }
        if (!this.zzatp && !this.zzada.zzgv().zza(zzai.zzald)) {
            this.zzada.zzgt().zzjm().zzby("This instance being marked as an uploader");
            this.zzatp = true;
            zzmb();
        }
    }

    @WorkerThread
    private final boolean zzmf() {
        zzaf();
        zzlx();
        return this.zzatp;
    }

    /* access modifiers changed from: 0000 */
    @WorkerThread
    @VisibleForTesting
    public final void zzd(zzk zzk) {
        if (this.zzaua != null) {
            this.zzaub = new ArrayList();
            this.zzaub.addAll(this.zzaua);
        }
        zzt zzjt = zzjt();
        String str = zzk.packageName;
        Preconditions.checkNotEmpty(str);
        zzjt.zzaf();
        zzjt.zzcl();
        try {
            SQLiteDatabase writableDatabase = zzjt.getWritableDatabase();
            String[] strArr = {str};
            int delete = 0 + writableDatabase.delete("apps", "app_id=?", strArr) + writableDatabase.delete("events", "app_id=?", strArr) + writableDatabase.delete("user_attributes", "app_id=?", strArr) + writableDatabase.delete("conditional_properties", "app_id=?", strArr) + writableDatabase.delete("raw_events", "app_id=?", strArr) + writableDatabase.delete("raw_events_metadata", "app_id=?", strArr) + writableDatabase.delete("queue", "app_id=?", strArr) + writableDatabase.delete("audience_filter_values", "app_id=?", strArr) + writableDatabase.delete("main_event_params", "app_id=?", strArr);
            if (delete > 0) {
                zzjt.zzgt().zzjo().zze("Reset analytics data. app, records", str, Integer.valueOf(delete));
            }
        } catch (SQLiteException e) {
            zzjt.zzgt().zzjg().zze("Error resetting analytics data. appId, error", zzas.zzbw(str), e);
        }
        zzk zza2 = zza(this.zzada.getContext(), zzk.packageName, zzk.zzafi, zzk.zzafr, zzk.zzaft, zzk.zzafu, zzk.zzago, zzk.zzafv);
        if (!this.zzada.zzgv().zzba(zzk.packageName) || zzk.zzafr) {
            zzf(zza2);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x0057 A[Catch:{ NameNotFoundException -> 0x00bd }] */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x006f  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0087  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x008a  */
    private final zzk zza(Context context, String str, String str2, boolean z, boolean z2, boolean z3, long j, String str3) {
        String str4;
        String str5;
        PackageInfo packageInfo;
        int i;
        String str6;
        String str7 = str;
        String str8 = AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN;
        String str9 = AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN;
        String str10 = AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN;
        PackageManager packageManager = context.getPackageManager();
        if (packageManager == null) {
            this.zzada.zzgt().zzjg().zzby("PackageManager is null, can not log app install information");
            return null;
        }
        try {
            str4 = packageManager.getInstallerPackageName(str7);
        } catch (IllegalArgumentException unused) {
            this.zzada.zzgt().zzjg().zzg("Error retrieving installer package name. appId", zzas.zzbw(str));
            str4 = str8;
        }
        if (str4 == null) {
            str6 = "manual_install";
        } else if ("com.android.vending".equals(str4)) {
            str6 = "";
        } else {
            str5 = str4;
            packageInfo = Wrappers.packageManager(context).getPackageInfo(str7, 0);
            if (packageInfo == null) {
                CharSequence applicationLabel = Wrappers.packageManager(context).getApplicationLabel(str7);
                if (!TextUtils.isEmpty(applicationLabel)) {
                    String charSequence = applicationLabel.toString();
                }
                str9 = packageInfo.versionName;
                i = packageInfo.versionCode;
            } else {
                i = Integer.MIN_VALUE;
            }
            String str11 = str9;
            this.zzada.zzgw();
            zzk zzk = new zzk(str7, str2, str11, (long) i, str5, this.zzada.zzgv().zzhh(), this.zzada.zzgr().zzd(context, str7), (String) null, z, false, "", 0, !this.zzada.zzgv().zzbc(str7) ? j : 0, 0, z2, z3, false, str3);
            return zzk;
        }
        str5 = str6;
        try {
            packageInfo = Wrappers.packageManager(context).getPackageInfo(str7, 0);
            if (packageInfo == null) {
            }
            String str112 = str9;
            this.zzada.zzgw();
            zzk zzk2 = new zzk(str7, str2, str112, (long) i, str5, this.zzada.zzgv().zzhh(), this.zzada.zzgr().zzd(context, str7), (String) null, z, false, "", 0, !this.zzada.zzgv().zzbc(str7) ? j : 0, 0, z2, z3, false, str3);
            return zzk2;
        } catch (NameNotFoundException unused2) {
            this.zzada.zzgt().zzjg().zze("Error retrieving newly installed package info. appId, appName", zzas.zzbw(str), str10);
            return null;
        }
    }

    /* access modifiers changed from: 0000 */
    @WorkerThread
    public final void zzb(zzfv zzfv, zzk zzk) {
        zzaf();
        zzlx();
        if (TextUtils.isEmpty(zzk.zzafi) && TextUtils.isEmpty(zzk.zzafv)) {
            return;
        }
        if (!zzk.zzafr) {
            zzg(zzk);
            return;
        }
        int zzcv = this.zzada.zzgr().zzcv(zzfv.name);
        int i = 0;
        if (zzcv != 0) {
            this.zzada.zzgr();
            this.zzada.zzgr().zza(zzk.packageName, zzcv, "_ev", zzfy.zza(zzfv.name, 24, true), zzfv.name != null ? zzfv.name.length() : 0);
            return;
        }
        int zzi = this.zzada.zzgr().zzi(zzfv.name, zzfv.getValue());
        if (zzi != 0) {
            this.zzada.zzgr();
            String zza2 = zzfy.zza(zzfv.name, 24, true);
            Object value = zzfv.getValue();
            if (value != null && ((value instanceof String) || (value instanceof CharSequence))) {
                i = String.valueOf(value).length();
            }
            this.zzada.zzgr().zza(zzk.packageName, zzi, "_ev", zza2, i);
            return;
        }
        Object zzj = this.zzada.zzgr().zzj(zzfv.name, zzfv.getValue());
        if (zzj != null) {
            if (this.zzada.zzgv().zzbh(zzk.packageName) && "_sno".equals(zzfv.name)) {
                long j = 0;
                zzfx zzi2 = zzjt().zzi(zzk.packageName, "_sno");
                if (zzi2 == null || !(zzi2.value instanceof Long)) {
                    zzac zzg = zzjt().zzg(zzk.packageName, "_s");
                    if (zzg != null) {
                        j = zzg.zzahv;
                        this.zzada.zzgt().zzjo().zzg("Backfill the session number. Last used session number", Long.valueOf(j));
                    }
                } else {
                    j = ((Long) zzi2.value).longValue();
                }
                zzj = Long.valueOf(j + 1);
            }
            zzfx zzfx = new zzfx(zzk.packageName, zzfv.origin, zzfv.name, zzfv.zzauk, zzj);
            this.zzada.zzgt().zzjn().zze("Setting user property", this.zzada.zzgq().zzbv(zzfx.name), zzj);
            zzjt().beginTransaction();
            try {
                zzg(zzk);
                boolean zza3 = zzjt().zza(zzfx);
                zzjt().setTransactionSuccessful();
                if (zza3) {
                    this.zzada.zzgt().zzjn().zze("User property set", this.zzada.zzgq().zzbv(zzfx.name), zzfx.value);
                } else {
                    this.zzada.zzgt().zzjg().zze("Too many unique user properties are set. Ignoring user property", this.zzada.zzgq().zzbv(zzfx.name), zzfx.value);
                    this.zzada.zzgr().zza(zzk.packageName, 9, (String) null, (String) null, 0);
                }
            } finally {
                zzjt().endTransaction();
            }
        }
    }

    /* access modifiers changed from: 0000 */
    @WorkerThread
    public final void zzc(zzfv zzfv, zzk zzk) {
        zzaf();
        zzlx();
        if (TextUtils.isEmpty(zzk.zzafi) && TextUtils.isEmpty(zzk.zzafv)) {
            return;
        }
        if (!zzk.zzafr) {
            zzg(zzk);
            return;
        }
        this.zzada.zzgt().zzjn().zzg("Removing user property", this.zzada.zzgq().zzbv(zzfv.name));
        zzjt().beginTransaction();
        try {
            zzg(zzk);
            zzjt().zzh(zzk.packageName, zzfv.name);
            zzjt().setTransactionSuccessful();
            this.zzada.zzgt().zzjn().zzg("User property removed", this.zzada.zzgq().zzbv(zzfv.name));
        } finally {
            zzjt().endTransaction();
        }
    }

    /* access modifiers changed from: 0000 */
    public final void zzb(zzfn zzfn) {
        this.zzatt++;
    }

    /* access modifiers changed from: 0000 */
    public final void zzmg() {
        this.zzatu++;
    }

    /* access modifiers changed from: 0000 */
    public final zzbw zzmh() {
        return this.zzada;
    }

    /* access modifiers changed from: 0000 */
    @WorkerThread
    public final void zzf(zzk zzk) {
        zzg zzbm;
        long j;
        PackageInfo packageInfo;
        ApplicationInfo applicationInfo;
        boolean z;
        zzt zzjt;
        String zzal;
        zzk zzk2 = zzk;
        zzaf();
        zzlx();
        Preconditions.checkNotNull(zzk);
        Preconditions.checkNotEmpty(zzk2.packageName);
        if (!TextUtils.isEmpty(zzk2.zzafi) || !TextUtils.isEmpty(zzk2.zzafv)) {
            zzg zzbm2 = zzjt().zzbm(zzk2.packageName);
            if (zzbm2 != null && TextUtils.isEmpty(zzbm2.getGmpAppId()) && !TextUtils.isEmpty(zzk2.zzafi)) {
                zzbm2.zzu(0);
                zzjt().zza(zzbm2);
                zzls().zzcj(zzk2.packageName);
            }
            if (!zzk2.zzafr) {
                zzg(zzk);
                return;
            }
            long j2 = zzk2.zzago;
            if (j2 == 0) {
                j2 = this.zzada.zzbx().currentTimeMillis();
            }
            int i = zzk2.zzagp;
            if (!(i == 0 || i == 1)) {
                this.zzada.zzgt().zzjj().zze("Incorrect app type, assuming installed app. appId, appType", zzas.zzbw(zzk2.packageName), Integer.valueOf(i));
                i = 0;
            }
            zzjt().beginTransaction();
            try {
                zzbm = zzjt().zzbm(zzk2.packageName);
                if (zzbm != null) {
                    this.zzada.zzgr();
                    if (zzfy.zza(zzk2.zzafi, zzbm.getGmpAppId(), zzk2.zzafv, zzbm.zzhb())) {
                        this.zzada.zzgt().zzjj().zzg("New GMP App Id passed in. Removing cached database data. appId", zzas.zzbw(zzbm.zzal()));
                        zzjt = zzjt();
                        zzal = zzbm.zzal();
                        zzjt.zzcl();
                        zzjt.zzaf();
                        Preconditions.checkNotEmpty(zzal);
                        SQLiteDatabase writableDatabase = zzjt.getWritableDatabase();
                        String[] strArr = {zzal};
                        int delete = writableDatabase.delete("events", "app_id=?", strArr) + 0 + writableDatabase.delete("user_attributes", "app_id=?", strArr) + writableDatabase.delete("conditional_properties", "app_id=?", strArr) + writableDatabase.delete("apps", "app_id=?", strArr) + writableDatabase.delete("raw_events", "app_id=?", strArr) + writableDatabase.delete("raw_events_metadata", "app_id=?", strArr) + writableDatabase.delete("event_filters", "app_id=?", strArr) + writableDatabase.delete("property_filters", "app_id=?", strArr) + writableDatabase.delete("audience_filter_values", "app_id=?", strArr);
                        if (delete > 0) {
                            zzjt.zzgt().zzjo().zze("Deleted application data. app, records", zzal, Integer.valueOf(delete));
                        }
                        zzbm = null;
                    }
                }
            } catch (SQLiteException e) {
                zzjt.zzgt().zzjg().zze("Error deleting application data. appId, error", zzas.zzbw(zzal), e);
            } catch (Throwable th) {
                Throwable th2 = th;
                zzjt().endTransaction();
                throw th2;
            }
            if (zzbm != null) {
                if (zzbm.zzhf() != -2147483648L) {
                    if (zzbm.zzhf() != zzk2.zzafo) {
                        Bundle bundle = new Bundle();
                        bundle.putString("_pv", zzbm.zzak());
                        zzag zzag = new zzag("_au", new zzad(bundle), "auto", j2);
                        zzc(zzag, zzk2);
                    }
                } else if (zzbm.zzak() != null && !zzbm.zzak().equals(zzk2.zzts)) {
                    Bundle bundle2 = new Bundle();
                    bundle2.putString("_pv", zzbm.zzak());
                    zzag zzag2 = new zzag("_au", new zzad(bundle2), "auto", j2);
                    zzc(zzag2, zzk2);
                }
            }
            zzg(zzk);
            zzac zzac = i == 0 ? zzjt().zzg(zzk2.packageName, "_f") : i == 1 ? zzjt().zzg(zzk2.packageName, "_v") : null;
            if (zzac == null) {
                long j3 = 3600000 * (1 + (j2 / 3600000));
                if (i == 0) {
                    j = 1;
                    zzfv zzfv = new zzfv("_fot", j2, Long.valueOf(j3), "auto");
                    zzb(zzfv, zzk2);
                    if (this.zzada.zzgv().zzbe(zzk2.zzafi)) {
                        zzaf();
                        this.zzada.zzkk().zzce(zzk2.packageName);
                    }
                    zzaf();
                    zzlx();
                    Bundle bundle3 = new Bundle();
                    bundle3.putLong("_c", 1);
                    bundle3.putLong("_r", 1);
                    bundle3.putLong("_uwa", 0);
                    bundle3.putLong("_pfo", 0);
                    bundle3.putLong("_sys", 0);
                    bundle3.putLong("_sysu", 0);
                    if (this.zzada.zzgv().zzbk(zzk2.packageName)) {
                        bundle3.putLong("_et", 1);
                    }
                    if (this.zzada.zzgv().zzba(zzk2.packageName) && zzk2.zzagq) {
                        bundle3.putLong("_dac", 1);
                    }
                    if (this.zzada.getContext().getPackageManager() == null) {
                        this.zzada.zzgt().zzjg().zzg("PackageManager is null, first open report might be inaccurate. appId", zzas.zzbw(zzk2.packageName));
                    } else {
                        try {
                            packageInfo = Wrappers.packageManager(this.zzada.getContext()).getPackageInfo(zzk2.packageName, 0);
                        } catch (NameNotFoundException e2) {
                            this.zzada.zzgt().zzjg().zze("Package info is null, first open report might be inaccurate. appId", zzas.zzbw(zzk2.packageName), e2);
                            packageInfo = null;
                        }
                        if (!(packageInfo == null || packageInfo.firstInstallTime == 0)) {
                            if (packageInfo.firstInstallTime != packageInfo.lastUpdateTime) {
                                bundle3.putLong("_uwa", 1);
                                z = false;
                            } else {
                                z = true;
                            }
                            zzfv zzfv2 = new zzfv("_fi", j2, Long.valueOf(z ? 1 : 0), "auto");
                            zzb(zzfv2, zzk2);
                        }
                        try {
                            applicationInfo = Wrappers.packageManager(this.zzada.getContext()).getApplicationInfo(zzk2.packageName, 0);
                        } catch (NameNotFoundException e3) {
                            this.zzada.zzgt().zzjg().zze("Application info is null, first open report might be inaccurate. appId", zzas.zzbw(zzk2.packageName), e3);
                            applicationInfo = null;
                        }
                        if (applicationInfo != null) {
                            if ((applicationInfo.flags & 1) != 0) {
                                bundle3.putLong("_sys", 1);
                            }
                            if ((applicationInfo.flags & 128) != 0) {
                                bundle3.putLong("_sysu", 1);
                            }
                        }
                    }
                    zzt zzjt2 = zzjt();
                    String str = zzk2.packageName;
                    Preconditions.checkNotEmpty(str);
                    zzjt2.zzaf();
                    zzjt2.zzcl();
                    long zzn = zzjt2.zzn(str, "first_open_count");
                    if (zzn >= 0) {
                        bundle3.putLong("_pfo", zzn);
                    }
                    zzag zzag3 = new zzag("_f", new zzad(bundle3), "auto", j2);
                    zzc(zzag3, zzk2);
                } else {
                    j = 1;
                    if (i == 1) {
                        zzfv zzfv3 = new zzfv("_fvt", j2, Long.valueOf(j3), "auto");
                        zzb(zzfv3, zzk2);
                        zzaf();
                        zzlx();
                        Bundle bundle4 = new Bundle();
                        bundle4.putLong("_c", 1);
                        bundle4.putLong("_r", 1);
                        if (this.zzada.zzgv().zzbk(zzk2.packageName)) {
                            bundle4.putLong("_et", 1);
                        }
                        if (this.zzada.zzgv().zzba(zzk2.packageName) && zzk2.zzagq) {
                            bundle4.putLong("_dac", 1);
                        }
                        zzag zzag4 = new zzag("_v", new zzad(bundle4), "auto", j2);
                        zzc(zzag4, zzk2);
                    }
                }
                if (!this.zzada.zzgv().zze(zzk2.packageName, zzai.zzala)) {
                    Bundle bundle5 = new Bundle();
                    bundle5.putLong("_et", j);
                    if (this.zzada.zzgv().zzbk(zzk2.packageName)) {
                        bundle5.putLong("_fr", j);
                    }
                    zzag zzag5 = new zzag("_e", new zzad(bundle5), "auto", j2);
                    zzc(zzag5, zzk2);
                }
            } else if (zzk2.zzagn) {
                zzag zzag6 = new zzag("_cd", new zzad(new Bundle()), "auto", j2);
                zzc(zzag6, zzk2);
            }
            zzjt().setTransactionSuccessful();
            zzjt().endTransaction();
        }
    }

    @WorkerThread
    private final zzk zzcr(String str) {
        String str2 = str;
        zzg zzbm = zzjt().zzbm(str2);
        if (zzbm == null || TextUtils.isEmpty(zzbm.zzak())) {
            this.zzada.zzgt().zzjn().zzg("No app data available; dropping", str2);
            return null;
        }
        Boolean zzc = zzc(zzbm);
        if (zzc == null || zzc.booleanValue()) {
            zzk zzk = new zzk(str2, zzbm.getGmpAppId(), zzbm.zzak(), zzbm.zzhf(), zzbm.zzhg(), zzbm.zzhh(), zzbm.zzhi(), (String) null, zzbm.isMeasurementEnabled(), false, zzbm.getFirebaseInstanceId(), zzbm.zzhv(), 0, 0, zzbm.zzhw(), zzbm.zzhx(), false, zzbm.zzhb());
            return zzk;
        }
        this.zzada.zzgt().zzjg().zzg("App version does not match; dropping. appId", zzas.zzbw(str));
        return null;
    }

    /* access modifiers changed from: 0000 */
    @WorkerThread
    public final void zze(zzo zzo) {
        zzk zzcr = zzcr(zzo.packageName);
        if (zzcr != null) {
            zzb(zzo, zzcr);
        }
    }

    /* access modifiers changed from: 0000 */
    @WorkerThread
    public final void zzb(zzo zzo, zzk zzk) {
        Preconditions.checkNotNull(zzo);
        Preconditions.checkNotEmpty(zzo.packageName);
        Preconditions.checkNotNull(zzo.origin);
        Preconditions.checkNotNull(zzo.zzags);
        Preconditions.checkNotEmpty(zzo.zzags.name);
        zzaf();
        zzlx();
        if (TextUtils.isEmpty(zzk.zzafi) && TextUtils.isEmpty(zzk.zzafv)) {
            return;
        }
        if (!zzk.zzafr) {
            zzg(zzk);
            return;
        }
        zzo zzo2 = new zzo(zzo);
        boolean z = false;
        zzo2.active = false;
        zzjt().beginTransaction();
        try {
            zzo zzj = zzjt().zzj(zzo2.packageName, zzo2.zzags.name);
            if (zzj != null && !zzj.origin.equals(zzo2.origin)) {
                this.zzada.zzgt().zzjj().zzd("Updating a conditional user property with different origin. name, origin, origin (from DB)", this.zzada.zzgq().zzbv(zzo2.zzags.name), zzo2.origin, zzj.origin);
            }
            if (zzj != null && zzj.active) {
                zzo2.origin = zzj.origin;
                zzo2.creationTimestamp = zzj.creationTimestamp;
                zzo2.triggerTimeout = zzj.triggerTimeout;
                zzo2.triggerEventName = zzj.triggerEventName;
                zzo2.zzagu = zzj.zzagu;
                zzo2.active = zzj.active;
                zzfv zzfv = new zzfv(zzo2.zzags.name, zzj.zzags.zzauk, zzo2.zzags.getValue(), zzj.zzags.origin);
                zzo2.zzags = zzfv;
            } else if (TextUtils.isEmpty(zzo2.triggerEventName)) {
                zzfv zzfv2 = new zzfv(zzo2.zzags.name, zzo2.creationTimestamp, zzo2.zzags.getValue(), zzo2.zzags.origin);
                zzo2.zzags = zzfv2;
                zzo2.active = true;
                z = true;
            }
            if (zzo2.active) {
                zzfv zzfv3 = zzo2.zzags;
                zzfx zzfx = new zzfx(zzo2.packageName, zzo2.origin, zzfv3.name, zzfv3.zzauk, zzfv3.getValue());
                if (zzjt().zza(zzfx)) {
                    this.zzada.zzgt().zzjn().zzd("User property updated immediately", zzo2.packageName, this.zzada.zzgq().zzbv(zzfx.name), zzfx.value);
                } else {
                    this.zzada.zzgt().zzjg().zzd("(2)Too many active user properties, ignoring", zzas.zzbw(zzo2.packageName), this.zzada.zzgq().zzbv(zzfx.name), zzfx.value);
                }
                if (z && zzo2.zzagu != null) {
                    zzd(new zzag(zzo2.zzagu, zzo2.creationTimestamp), zzk);
                }
            }
            if (zzjt().zza(zzo2)) {
                this.zzada.zzgt().zzjn().zzd("Conditional property added", zzo2.packageName, this.zzada.zzgq().zzbv(zzo2.zzags.name), zzo2.zzags.getValue());
            } else {
                this.zzada.zzgt().zzjg().zzd("Too many conditional properties, ignoring", zzas.zzbw(zzo2.packageName), this.zzada.zzgq().zzbv(zzo2.zzags.name), zzo2.zzags.getValue());
            }
            zzjt().setTransactionSuccessful();
        } finally {
            zzjt().endTransaction();
        }
    }

    /* access modifiers changed from: 0000 */
    @WorkerThread
    public final void zzf(zzo zzo) {
        zzk zzcr = zzcr(zzo.packageName);
        if (zzcr != null) {
            zzc(zzo, zzcr);
        }
    }

    /* access modifiers changed from: 0000 */
    @WorkerThread
    public final void zzc(zzo zzo, zzk zzk) {
        Preconditions.checkNotNull(zzo);
        Preconditions.checkNotEmpty(zzo.packageName);
        Preconditions.checkNotNull(zzo.zzags);
        Preconditions.checkNotEmpty(zzo.zzags.name);
        zzaf();
        zzlx();
        if (TextUtils.isEmpty(zzk.zzafi) && TextUtils.isEmpty(zzk.zzafv)) {
            return;
        }
        if (!zzk.zzafr) {
            zzg(zzk);
            return;
        }
        zzjt().beginTransaction();
        try {
            zzg(zzk);
            zzo zzj = zzjt().zzj(zzo.packageName, zzo.zzags.name);
            if (zzj != null) {
                this.zzada.zzgt().zzjn().zze("Removing conditional user property", zzo.packageName, this.zzada.zzgq().zzbv(zzo.zzags.name));
                zzjt().zzk(zzo.packageName, zzo.zzags.name);
                if (zzj.active) {
                    zzjt().zzh(zzo.packageName, zzo.zzags.name);
                }
                if (zzo.zzagv != null) {
                    Bundle bundle = null;
                    if (zzo.zzagv.zzahu != null) {
                        bundle = zzo.zzagv.zzahu.zziy();
                    }
                    Bundle bundle2 = bundle;
                    zzd(this.zzada.zzgr().zza(zzo.packageName, zzo.zzagv.name, bundle2, zzj.origin, zzo.zzagv.zzaig, true, false), zzk);
                }
            } else {
                this.zzada.zzgt().zzjj().zze("Conditional user property doesn't exist", zzas.zzbw(zzo.packageName), this.zzada.zzgq().zzbv(zzo.zzags.name));
            }
            zzjt().setTransactionSuccessful();
        } finally {
            zzjt().endTransaction();
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:10:0x006a  */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x007c  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00d8  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00fe  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x010c  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x0136  */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x0144  */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x0152  */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x015a  */
    @WorkerThread
    public final zzg zzg(zzk zzk) {
        boolean z;
        zzaf();
        zzlx();
        Preconditions.checkNotNull(zzk);
        Preconditions.checkNotEmpty(zzk.packageName);
        zzg zzbm = zzjt().zzbm(zzk.packageName);
        String zzca = this.zzada.zzgu().zzca(zzk.packageName);
        if (zzbm == null) {
            zzbm = new zzg(this.zzada, zzk.packageName);
            zzbm.zzaj(this.zzada.zzgr().zzmm());
            zzbm.zzam(zzca);
        } else if (!zzca.equals(zzbm.zzhc())) {
            zzbm.zzam(zzca);
            zzbm.zzaj(this.zzada.zzgr().zzmm());
        } else {
            z = false;
            if (!TextUtils.equals(zzk.zzafi, zzbm.getGmpAppId())) {
                zzbm.zzak(zzk.zzafi);
                z = true;
            }
            if (!TextUtils.equals(zzk.zzafv, zzbm.zzhb())) {
                zzbm.zzal(zzk.zzafv);
                z = true;
            }
            if (!TextUtils.isEmpty(zzk.zzafk) && !zzk.zzafk.equals(zzbm.getFirebaseInstanceId())) {
                zzbm.zzan(zzk.zzafk);
                z = true;
            }
            if (!(zzk.zzade == 0 || zzk.zzade == zzbm.zzhh())) {
                zzbm.zzr(zzk.zzade);
                z = true;
            }
            if (!TextUtils.isEmpty(zzk.zzts) && !zzk.zzts.equals(zzbm.zzak())) {
                zzbm.setAppVersion(zzk.zzts);
                z = true;
            }
            if (zzk.zzafo != zzbm.zzhf()) {
                zzbm.zzq(zzk.zzafo);
                z = true;
            }
            if (zzk.zzafp != null && !zzk.zzafp.equals(zzbm.zzhg())) {
                zzbm.zzao(zzk.zzafp);
                z = true;
            }
            if (zzk.zzafq != zzbm.zzhi()) {
                zzbm.zzs(zzk.zzafq);
                z = true;
            }
            if (zzk.zzafr != zzbm.isMeasurementEnabled()) {
                zzbm.setMeasurementEnabled(zzk.zzafr);
                z = true;
            }
            if (!TextUtils.isEmpty(zzk.zzagm) && !zzk.zzagm.equals(zzbm.zzht())) {
                zzbm.zzap(zzk.zzagm);
                z = true;
            }
            if (zzk.zzafs != zzbm.zzhv()) {
                zzbm.zzac(zzk.zzafs);
                z = true;
            }
            if (zzk.zzaft != zzbm.zzhw()) {
                zzbm.zze(zzk.zzaft);
                z = true;
            }
            if (zzk.zzafu != zzbm.zzhx()) {
                zzbm.zzf(zzk.zzafu);
                z = true;
            }
            if (z) {
                zzjt().zza(zzbm);
            }
            return zzbm;
        }
        z = true;
        if (!TextUtils.equals(zzk.zzafi, zzbm.getGmpAppId())) {
        }
        if (!TextUtils.equals(zzk.zzafv, zzbm.zzhb())) {
        }
        zzbm.zzan(zzk.zzafk);
        z = true;
        zzbm.zzr(zzk.zzade);
        z = true;
        zzbm.setAppVersion(zzk.zzts);
        z = true;
        if (zzk.zzafo != zzbm.zzhf()) {
        }
        zzbm.zzao(zzk.zzafp);
        z = true;
        if (zzk.zzafq != zzbm.zzhi()) {
        }
        if (zzk.zzafr != zzbm.isMeasurementEnabled()) {
        }
        zzbm.zzap(zzk.zzagm);
        z = true;
        if (zzk.zzafs != zzbm.zzhv()) {
        }
        if (zzk.zzaft != zzbm.zzhw()) {
        }
        if (zzk.zzafu != zzbm.zzhx()) {
        }
        if (z) {
        }
        return zzbm;
    }

    /* access modifiers changed from: 0000 */
    public final String zzh(zzk zzk) {
        try {
            return (String) this.zzada.zzgs().zzb((Callable<V>) new zzfs<V>(this, zzk)).get(DashMediaSource.DEFAULT_LIVE_PRESENTATION_DELAY_FIXED_MS, TimeUnit.MILLISECONDS);
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            this.zzada.zzgt().zzjg().zze("Failed to get app instance id. appId", zzas.zzbw(zzk.packageName), e);
            return null;
        }
    }

    /* access modifiers changed from: 0000 */
    public final void zzm(boolean z) {
        zzmb();
    }
}
