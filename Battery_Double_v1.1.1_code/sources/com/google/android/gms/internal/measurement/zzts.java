package com.google.android.gms.internal.measurement;

import java.io.IOException;
import java.util.Arrays;

final class zzts extends zztq {
    private final byte[] buffer;
    private int limit;
    private int pos;
    private final boolean zzbud;
    private int zzbue;
    private int zzbuf;
    private int zzbug;
    private int zzbuh;

    private zzts(byte[] bArr, int i, int i2, boolean z) {
        super();
        this.zzbuh = Integer.MAX_VALUE;
        this.buffer = bArr;
        this.limit = i2 + i;
        this.pos = i;
        this.zzbuf = this.pos;
        this.zzbud = z;
    }

    public final int zzuj() throws IOException {
        if (zzuz()) {
            this.zzbug = 0;
            return 0;
        }
        this.zzbug = zzvb();
        if ((this.zzbug >>> 3) != 0) {
            return this.zzbug;
        }
        throw new zzuv("Protocol message contained an invalid tag (zero).");
    }

    public final void zzap(int i) throws zzuv {
        if (this.zzbug != i) {
            throw zzuv.zzwt();
        }
    }

    public final boolean zzaq(int i) throws IOException {
        int zzuj;
        int i2 = 0;
        switch (i & 7) {
            case 0:
                if (this.limit - this.pos >= 10) {
                    while (i2 < 10) {
                        byte[] bArr = this.buffer;
                        int i3 = this.pos;
                        this.pos = i3 + 1;
                        if (bArr[i3] < 0) {
                            i2++;
                        }
                    }
                    throw zzuv.zzws();
                }
                while (i2 < 10) {
                    if (zzvg() < 0) {
                        i2++;
                    }
                }
                throw zzuv.zzws();
                return true;
            case 1:
                zzau(8);
                return true;
            case 2:
                zzau(zzvb());
                return true;
            case 3:
                break;
            case 4:
                return false;
            case 5:
                zzau(4);
                return true;
            default:
                throw zzuv.zzwu();
        }
        do {
            zzuj = zzuj();
            if (zzuj != 0) {
            }
            zzap(((i >>> 3) << 3) | 4);
            return true;
        } while (zzaq(zzuj));
        zzap(((i >>> 3) << 3) | 4);
        return true;
    }

    public final double readDouble() throws IOException {
        return Double.longBitsToDouble(zzve());
    }

    public final float readFloat() throws IOException {
        return Float.intBitsToFloat(zzvd());
    }

    public final long zzuk() throws IOException {
        return zzvc();
    }

    public final long zzul() throws IOException {
        return zzvc();
    }

    public final int zzum() throws IOException {
        return zzvb();
    }

    public final long zzun() throws IOException {
        return zzve();
    }

    public final int zzuo() throws IOException {
        return zzvd();
    }

    public final boolean zzup() throws IOException {
        return zzvc() != 0;
    }

    public final String readString() throws IOException {
        int zzvb = zzvb();
        if (zzvb > 0 && zzvb <= this.limit - this.pos) {
            String str = new String(this.buffer, this.pos, zzvb, zzuq.UTF_8);
            this.pos += zzvb;
            return str;
        } else if (zzvb == 0) {
            return "";
        } else {
            if (zzvb < 0) {
                throw zzuv.zzwr();
            }
            throw zzuv.zzwq();
        }
    }

    public final String zzuq() throws IOException {
        int zzvb = zzvb();
        if (zzvb > 0 && zzvb <= this.limit - this.pos) {
            String zzh = zzxl.zzh(this.buffer, this.pos, zzvb);
            this.pos += zzvb;
            return zzh;
        } else if (zzvb == 0) {
            return "";
        } else {
            if (zzvb <= 0) {
                throw zzuv.zzwr();
            }
            throw zzuv.zzwq();
        }
    }

    public final <T extends zzvv> T zza(zzwf<T> zzwf, zzub zzub) throws IOException {
        int zzvb = zzvb();
        if (this.zzbty >= this.zzbtz) {
            throw zzuv.zzwv();
        }
        int zzas = zzas(zzvb);
        this.zzbty++;
        T t = (zzvv) zzwf.zza(this, zzub);
        zzap(0);
        this.zzbty--;
        zzat(zzas);
        return t;
    }

    public final zzte zzur() throws IOException {
        byte[] bArr;
        int zzvb = zzvb();
        if (zzvb > 0 && zzvb <= this.limit - this.pos) {
            zzte zzb = zzte.zzb(this.buffer, this.pos, zzvb);
            this.pos += zzvb;
            return zzb;
        } else if (zzvb == 0) {
            return zzte.zzbtq;
        } else {
            if (zzvb > 0 && zzvb <= this.limit - this.pos) {
                int i = this.pos;
                this.pos += zzvb;
                bArr = Arrays.copyOfRange(this.buffer, i, this.pos);
            } else if (zzvb > 0) {
                throw zzuv.zzwq();
            } else if (zzvb == 0) {
                bArr = zzuq.zzbza;
            } else {
                throw zzuv.zzwr();
            }
            return zzte.zzi(bArr);
        }
    }

    public final int zzus() throws IOException {
        return zzvb();
    }

    public final int zzut() throws IOException {
        return zzvb();
    }

    public final int zzuu() throws IOException {
        return zzvd();
    }

    public final long zzuv() throws IOException {
        return zzve();
    }

    public final int zzuw() throws IOException {
        int zzvb = zzvb();
        return (-(zzvb & 1)) ^ (zzvb >>> 1);
    }

    public final long zzux() throws IOException {
        long zzvc = zzvc();
        return (zzvc >>> 1) ^ (-(zzvc & 1));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0068, code lost:
        if (r1[r2] >= 0) goto L_0x006a;
     */
    private final int zzvb() throws IOException {
        byte b;
        int i = this.pos;
        if (this.limit != i) {
            byte[] bArr = this.buffer;
            int i2 = i + 1;
            byte b2 = bArr[i];
            if (b2 >= 0) {
                this.pos = i2;
                return b2;
            } else if (this.limit - i2 >= 9) {
                int i3 = i2 + 1;
                byte b3 = b2 ^ (bArr[i2] << 7);
                if (b3 < 0) {
                    b = b3 ^ Byte.MIN_VALUE;
                } else {
                    int i4 = i3 + 1;
                    byte b4 = b3 ^ (bArr[i3] << 14);
                    if (b4 >= 0) {
                        b = b4 ^ 16256;
                    } else {
                        i3 = i4 + 1;
                        byte b5 = b4 ^ (bArr[i4] << 21);
                        if (b5 < 0) {
                            b = b5 ^ -2080896;
                        } else {
                            i4 = i3 + 1;
                            byte b6 = bArr[i3];
                            b = (b5 ^ (b6 << 28)) ^ 266354560;
                            if (b6 < 0) {
                                i3 = i4 + 1;
                                if (bArr[i4] < 0) {
                                    i4 = i3 + 1;
                                    if (bArr[i3] < 0) {
                                        i3 = i4 + 1;
                                        if (bArr[i4] < 0) {
                                            i4 = i3 + 1;
                                            if (bArr[i3] < 0) {
                                                i3 = i4 + 1;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    i3 = i4;
                }
                this.pos = i3;
                return b;
            }
        }
        return (int) zzuy();
    }

    private final long zzvc() throws IOException {
        long j;
        int i;
        long j2;
        long j3;
        long j4;
        int i2 = this.pos;
        if (this.limit != i2) {
            byte[] bArr = this.buffer;
            int i3 = i2 + 1;
            byte b = bArr[i2];
            if (b >= 0) {
                this.pos = i3;
                return (long) b;
            } else if (this.limit - i3 >= 9) {
                int i4 = i3 + 1;
                byte b2 = b ^ (bArr[i3] << 7);
                if (b2 < 0) {
                    j3 = (long) (b2 ^ Byte.MIN_VALUE);
                } else {
                    int i5 = i4 + 1;
                    byte b3 = b2 ^ (bArr[i4] << 14);
                    if (b3 >= 0) {
                        j4 = (long) (b3 ^ 16256);
                        i = i5;
                        j = j4;
                        this.pos = i;
                        return j;
                    }
                    i4 = i5 + 1;
                    byte b4 = b3 ^ (bArr[i5] << 21);
                    if (b4 < 0) {
                        j3 = (long) (b4 ^ -2080896);
                    } else {
                        long j5 = (long) b4;
                        int i6 = i4 + 1;
                        long j6 = j5 ^ (((long) bArr[i4]) << 28);
                        if (j6 >= 0) {
                            j2 = j6 ^ 266354560;
                        } else {
                            int i7 = i6 + 1;
                            long j7 = j6 ^ (((long) bArr[i6]) << 35);
                            if (j7 < 0) {
                                j = j7 ^ -34093383808L;
                            } else {
                                i6 = i7 + 1;
                                long j8 = j7 ^ (((long) bArr[i7]) << 42);
                                if (j8 >= 0) {
                                    j2 = j8 ^ 4363953127296L;
                                } else {
                                    i7 = i6 + 1;
                                    long j9 = j8 ^ (((long) bArr[i6]) << 49);
                                    if (j9 < 0) {
                                        j = j9 ^ -558586000294016L;
                                    } else {
                                        int i8 = i7 + 1;
                                        long j10 = (j9 ^ (((long) bArr[i7]) << 56)) ^ 71499008037633920L;
                                        if (j10 < 0) {
                                            int i9 = i8 + 1;
                                            if (((long) bArr[i8]) >= 0) {
                                                i8 = i9;
                                            }
                                        }
                                        j = j10;
                                        this.pos = i;
                                        return j;
                                    }
                                }
                            }
                            i = i7;
                            this.pos = i;
                            return j;
                        }
                        j = j2;
                        this.pos = i;
                        return j;
                    }
                }
                j4 = j3;
                i = i4;
                j = j4;
                this.pos = i;
                return j;
            }
        }
        return zzuy();
    }

    /* access modifiers changed from: 0000 */
    public final long zzuy() throws IOException {
        long j = 0;
        int i = 0;
        while (i < 64) {
            byte zzvg = zzvg();
            long j2 = j | (((long) (zzvg & Byte.MAX_VALUE)) << i);
            if ((zzvg & 128) == 0) {
                return j2;
            }
            i += 7;
            j = j2;
        }
        throw zzuv.zzws();
    }

    private final int zzvd() throws IOException {
        int i = this.pos;
        if (this.limit - i < 4) {
            throw zzuv.zzwq();
        }
        byte[] bArr = this.buffer;
        this.pos = i + 4;
        return ((bArr[i + 3] & 255) << 24) | (bArr[i] & 255) | ((bArr[i + 1] & 255) << 8) | ((bArr[i + 2] & 255) << 16);
    }

    private final long zzve() throws IOException {
        int i = this.pos;
        if (this.limit - i < 8) {
            throw zzuv.zzwq();
        }
        byte[] bArr = this.buffer;
        this.pos = i + 8;
        return (((long) bArr[i]) & 255) | ((((long) bArr[i + 1]) & 255) << 8) | ((((long) bArr[i + 2]) & 255) << 16) | ((((long) bArr[i + 3]) & 255) << 24) | ((((long) bArr[i + 4]) & 255) << 32) | ((((long) bArr[i + 5]) & 255) << 40) | ((((long) bArr[i + 6]) & 255) << 48) | ((((long) bArr[i + 7]) & 255) << 56);
    }

    public final int zzas(int i) throws zzuv {
        if (i < 0) {
            throw zzuv.zzwr();
        }
        int zzva = i + zzva();
        int i2 = this.zzbuh;
        if (zzva > i2) {
            throw zzuv.zzwq();
        }
        this.zzbuh = zzva;
        zzvf();
        return i2;
    }

    private final void zzvf() {
        this.limit += this.zzbue;
        int i = this.limit - this.zzbuf;
        if (i > this.zzbuh) {
            this.zzbue = i - this.zzbuh;
            this.limit -= this.zzbue;
            return;
        }
        this.zzbue = 0;
    }

    public final void zzat(int i) {
        this.zzbuh = i;
        zzvf();
    }

    public final boolean zzuz() throws IOException {
        return this.pos == this.limit;
    }

    public final int zzva() {
        return this.pos - this.zzbuf;
    }

    private final byte zzvg() throws IOException {
        if (this.pos == this.limit) {
            throw zzuv.zzwq();
        }
        byte[] bArr = this.buffer;
        int i = this.pos;
        this.pos = i + 1;
        return bArr[i];
    }

    public final void zzau(int i) throws IOException {
        if (i >= 0 && i <= this.limit - this.pos) {
            this.pos += i;
        } else if (i < 0) {
            throw zzuv.zzwr();
        } else {
            throw zzuv.zzwq();
        }
    }
}
