package com.google.android.gms.internal.measurement;

import com.google.android.gms.internal.measurement.zzfe.zzb.C0035zzb;

final class zzfh implements zzut {
    static final zzut zzoc = new zzfh();

    private zzfh() {
    }

    public final boolean zzb(int i) {
        return C0035zzb.zzt(i) != null;
    }
}
