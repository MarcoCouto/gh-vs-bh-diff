package com.google.android.gms.internal.ads;

import com.google.android.gms.ads.internal.zzbv;
import com.google.android.gms.common.util.IOUtils;
import java.io.Closeable;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;

final class zzasz implements Runnable {
    private final /* synthetic */ OutputStream zzdzl;
    private final /* synthetic */ byte[] zzdzm;

    zzasz(zzasy zzasy, OutputStream outputStream, byte[] bArr) {
        this.zzdzl = outputStream;
        this.zzdzm = bArr;
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x0032  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0038  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x003f  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0045  */
    public final void run() {
        DataOutputStream dataOutputStream;
        Throwable e;
        try {
            dataOutputStream = new DataOutputStream(this.zzdzl);
            try {
                dataOutputStream.writeInt(this.zzdzm.length);
                dataOutputStream.write(this.zzdzm);
                IOUtils.closeQuietly((Closeable) dataOutputStream);
            } catch (IOException e2) {
                e = e2;
                try {
                    zzaxz.zzb("Error transporting the ad response", e);
                    zzbv.zzlj().zza(e, "LargeParcelTeleporter.pipeData.1");
                    if (dataOutputStream != null) {
                        IOUtils.closeQuietly((Closeable) this.zzdzl);
                    } else {
                        IOUtils.closeQuietly((Closeable) dataOutputStream);
                    }
                } catch (Throwable th) {
                    th = th;
                    if (dataOutputStream != null) {
                        IOUtils.closeQuietly((Closeable) this.zzdzl);
                    } else {
                        IOUtils.closeQuietly((Closeable) dataOutputStream);
                    }
                    throw th;
                }
            }
        } catch (IOException e3) {
            Throwable th2 = e3;
            dataOutputStream = null;
            e = th2;
            zzaxz.zzb("Error transporting the ad response", e);
            zzbv.zzlj().zza(e, "LargeParcelTeleporter.pipeData.1");
            if (dataOutputStream != null) {
            }
        } catch (Throwable th3) {
            Throwable th4 = th3;
            dataOutputStream = null;
            th = th4;
            if (dataOutputStream != null) {
            }
            throw th;
        }
    }
}
