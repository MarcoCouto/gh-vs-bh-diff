package com.google.android.gms.internal.ads;

import android.annotation.TargetApi;
import android.content.Context;
import android.view.WindowManager;

@TargetApi(16)
public final class zzqs {
    private final zzqt zzbjy;
    private final boolean zzbjz;
    private final long zzbka;
    private final long zzbkb;
    private long zzbkc;
    private long zzbkd;
    private long zzbke;
    private boolean zzbkf;
    private long zzbkg;
    private long zzbkh;
    private long zzbki;

    public zzqs() {
        this(-1.0d);
    }

    public zzqs(Context context) {
        WindowManager windowManager = (WindowManager) context.getSystemService("window");
        this(windowManager.getDefaultDisplay() != null ? (double) windowManager.getDefaultDisplay().getRefreshRate() : -1.0d);
    }

    private zzqs(double d) {
        this.zzbjz = d != -1.0d;
        if (this.zzbjz) {
            this.zzbjy = zzqt.zzhv();
            this.zzbka = (long) (1.0E9d / d);
            this.zzbkb = (this.zzbka * 80) / 100;
            return;
        }
        this.zzbjy = null;
        this.zzbka = -1;
        this.zzbkb = -1;
    }

    public final void enable() {
        this.zzbkf = false;
        if (this.zzbjz) {
            this.zzbjy.zzhw();
        }
    }

    public final void disable() {
        if (this.zzbjz) {
            this.zzbjy.zzhx();
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x0055  */
    public final long zzh(long j, long j2) {
        long j3;
        long j4;
        long j5;
        long j6 = j;
        long j7 = j2;
        long j8 = 1000 * j6;
        if (this.zzbkf) {
            if (j6 != this.zzbkc) {
                this.zzbki++;
                this.zzbkd = this.zzbke;
            }
            if (this.zzbki >= 6) {
                j4 = this.zzbkd + ((j8 - this.zzbkh) / this.zzbki);
                if (zzi(j4, j7)) {
                    this.zzbkf = false;
                } else {
                    j3 = (this.zzbkg + j4) - this.zzbkh;
                    if (!this.zzbkf) {
                        this.zzbkh = j8;
                        this.zzbkg = j7;
                        this.zzbki = 0;
                        this.zzbkf = true;
                    }
                    this.zzbkc = j6;
                    this.zzbke = j4;
                    if (this.zzbjy != null || this.zzbjy.zzbkj == 0) {
                        return j3;
                    }
                    long j9 = this.zzbjy.zzbkj;
                    long j10 = this.zzbka;
                    long j11 = j9 + (((j3 - j9) / j10) * j10);
                    if (j3 <= j11) {
                        j5 = j11;
                        j11 -= j10;
                    } else {
                        j5 = j11 + j10;
                    }
                    if (j5 - j3 >= j3 - j11) {
                        j5 = j11;
                    }
                    return j5 - this.zzbkb;
                }
            } else if (zzi(j8, j7)) {
                this.zzbkf = false;
            }
        }
        j3 = j7;
        j4 = j8;
        if (!this.zzbkf) {
        }
        this.zzbkc = j6;
        this.zzbke = j4;
        if (this.zzbjy != null) {
        }
        return j3;
    }

    private final boolean zzi(long j, long j2) {
        return Math.abs((j2 - this.zzbkg) - (j - this.zzbkh)) > 20000000;
    }
}
