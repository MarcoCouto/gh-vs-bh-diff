package com.google.android.gms.internal.ads;

import android.annotation.TargetApi;
import android.graphics.Point;
import android.media.MediaCodecInfo.AudioCapabilities;
import android.media.MediaCodecInfo.CodecCapabilities;
import android.media.MediaCodecInfo.CodecProfileLevel;
import android.media.MediaCodecInfo.VideoCapabilities;
import android.util.Log;
import android.util.Pair;
import com.github.mikephil.charting.utils.Utils;
import com.google.android.exoplayer2.mediacodec.MediaCodecInfo;

@TargetApi(16)
public final class zzjx {
    private final String mimeType;
    public final String name;
    public final boolean zzadt;
    public final boolean zzatq;
    public final boolean zzatr;
    private final CodecCapabilities zzats;

    public static zzjx zzt(String str) {
        zzjx zzjx = new zzjx(str, null, null, false, false);
        return zzjx;
    }

    public static zzjx zza(String str, String str2, CodecCapabilities codecCapabilities, boolean z, boolean z2) {
        zzjx zzjx = new zzjx(str, str2, codecCapabilities, z, z2);
        return zzjx;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:32:0x005a, code lost:
        if ((com.google.android.gms.internal.ads.zzqe.SDK_INT >= 21 && r4.isFeatureSupported("secure-playback")) != false) goto L_0x005c;
     */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0031  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0049  */
    private zzjx(String str, String str2, CodecCapabilities codecCapabilities, boolean z, boolean z2) {
        boolean z3;
        boolean z4;
        this.name = (String) zzpo.checkNotNull(str);
        this.mimeType = str2;
        this.zzats = codecCapabilities;
        boolean z5 = false;
        if (!z && codecCapabilities != null) {
            if (zzqe.SDK_INT >= 19 && codecCapabilities.isFeatureSupported("adaptive-playback")) {
                z3 = true;
                this.zzatq = z3;
                if (codecCapabilities != null) {
                    if (zzqe.SDK_INT >= 21 && codecCapabilities.isFeatureSupported("tunneled-playback")) {
                        z4 = true;
                        this.zzadt = z4;
                        if (!z2) {
                            if (codecCapabilities != null) {
                            }
                            this.zzatr = z5;
                        }
                        z5 = true;
                        this.zzatr = z5;
                    }
                }
                z4 = false;
                this.zzadt = z4;
                if (!z2) {
                }
                z5 = true;
                this.zzatr = z5;
            }
        }
        z3 = false;
        this.zzatq = z3;
        if (codecCapabilities != null) {
        }
        z4 = false;
        this.zzadt = z4;
        if (!z2) {
        }
        z5 = true;
        this.zzatr = z5;
    }

    public final CodecProfileLevel[] zzej() {
        if (this.zzats == null || this.zzats.profileLevels == null) {
            return new CodecProfileLevel[0];
        }
        return this.zzats.profileLevels;
    }

    public final boolean zzu(String str) {
        CodecProfileLevel[] zzej;
        if (str == null || this.mimeType == null) {
            return true;
        }
        String zzae = zzpt.zzae(str);
        if (zzae == null) {
            return true;
        }
        if (!this.mimeType.equals(zzae)) {
            StringBuilder sb = new StringBuilder(13 + String.valueOf(str).length() + String.valueOf(zzae).length());
            sb.append("codec.mime ");
            sb.append(str);
            sb.append(", ");
            sb.append(zzae);
            zzv(sb.toString());
            return false;
        }
        Pair zzw = zzkc.zzw(str);
        if (zzw == null) {
            return true;
        }
        for (CodecProfileLevel codecProfileLevel : zzej()) {
            if (codecProfileLevel.profile == ((Integer) zzw.first).intValue() && codecProfileLevel.level >= ((Integer) zzw.second).intValue()) {
                return true;
            }
        }
        StringBuilder sb2 = new StringBuilder(22 + String.valueOf(str).length() + String.valueOf(zzae).length());
        sb2.append("codec.profileLevel, ");
        sb2.append(str);
        sb2.append(", ");
        sb2.append(zzae);
        zzv(sb2.toString());
        return false;
    }

    @TargetApi(21)
    public final boolean zza(int i, int i2, double d) {
        if (this.zzats == null) {
            zzv("sizeAndRate.caps");
            return false;
        }
        VideoCapabilities videoCapabilities = this.zzats.getVideoCapabilities();
        if (videoCapabilities == null) {
            zzv("sizeAndRate.vCaps");
            return false;
        }
        if (!zza(videoCapabilities, i, i2, d)) {
            if (i >= i2 || !zza(videoCapabilities, i2, i, d)) {
                StringBuilder sb = new StringBuilder(69);
                sb.append("sizeAndRate.support, ");
                sb.append(i);
                sb.append("x");
                sb.append(i2);
                sb.append("x");
                sb.append(d);
                zzv(sb.toString());
                return false;
            }
            StringBuilder sb2 = new StringBuilder(69);
            sb2.append("sizeAndRate.rotated, ");
            sb2.append(i);
            sb2.append("x");
            sb2.append(i2);
            sb2.append("x");
            sb2.append(d);
            String sb3 = sb2.toString();
            String str = MediaCodecInfo.TAG;
            String str2 = this.name;
            String str3 = this.mimeType;
            String str4 = zzqe.zzbic;
            StringBuilder sb4 = new StringBuilder(25 + String.valueOf(sb3).length() + String.valueOf(str2).length() + String.valueOf(str3).length() + String.valueOf(str4).length());
            sb4.append("AssumedSupport [");
            sb4.append(sb3);
            sb4.append("] [");
            sb4.append(str2);
            sb4.append(", ");
            sb4.append(str3);
            sb4.append("] [");
            sb4.append(str4);
            sb4.append("]");
            Log.d(str, sb4.toString());
        }
        return true;
    }

    @TargetApi(21)
    public final Point zzc(int i, int i2) {
        if (this.zzats == null) {
            zzv("align.caps");
            return null;
        }
        VideoCapabilities videoCapabilities = this.zzats.getVideoCapabilities();
        if (videoCapabilities == null) {
            zzv("align.vCaps");
            return null;
        }
        int widthAlignment = videoCapabilities.getWidthAlignment();
        int heightAlignment = videoCapabilities.getHeightAlignment();
        return new Point(zzqe.zzf(i, widthAlignment) * widthAlignment, zzqe.zzf(i2, heightAlignment) * heightAlignment);
    }

    @TargetApi(21)
    public final boolean zzam(int i) {
        if (this.zzats == null) {
            zzv("sampleRate.caps");
            return false;
        }
        AudioCapabilities audioCapabilities = this.zzats.getAudioCapabilities();
        if (audioCapabilities == null) {
            zzv("sampleRate.aCaps");
            return false;
        } else if (audioCapabilities.isSampleRateSupported(i)) {
            return true;
        } else {
            StringBuilder sb = new StringBuilder(31);
            sb.append("sampleRate.support, ");
            sb.append(i);
            zzv(sb.toString());
            return false;
        }
    }

    @TargetApi(21)
    public final boolean zzan(int i) {
        if (this.zzats == null) {
            zzv("channelCount.caps");
            return false;
        }
        AudioCapabilities audioCapabilities = this.zzats.getAudioCapabilities();
        if (audioCapabilities == null) {
            zzv("channelCount.aCaps");
            return false;
        } else if (audioCapabilities.getMaxInputChannelCount() >= i) {
            return true;
        } else {
            StringBuilder sb = new StringBuilder(33);
            sb.append("channelCount.support, ");
            sb.append(i);
            zzv(sb.toString());
            return false;
        }
    }

    private final void zzv(String str) {
        String str2 = MediaCodecInfo.TAG;
        String str3 = this.name;
        String str4 = this.mimeType;
        String str5 = zzqe.zzbic;
        StringBuilder sb = new StringBuilder(20 + String.valueOf(str).length() + String.valueOf(str3).length() + String.valueOf(str4).length() + String.valueOf(str5).length());
        sb.append("NoSupport [");
        sb.append(str);
        sb.append("] [");
        sb.append(str3);
        sb.append(", ");
        sb.append(str4);
        sb.append("] [");
        sb.append(str5);
        sb.append("]");
        Log.d(str2, sb.toString());
    }

    @TargetApi(21)
    private static boolean zza(VideoCapabilities videoCapabilities, int i, int i2, double d) {
        if (d == -1.0d || d <= Utils.DOUBLE_EPSILON) {
            return videoCapabilities.isSizeSupported(i, i2);
        }
        return videoCapabilities.areSizeAndRateSupported(i, i2, d);
    }
}
