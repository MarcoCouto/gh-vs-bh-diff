package com.google.android.gms.internal.ads;

final /* synthetic */ class zzbkv {
    static final /* synthetic */ int[] zzfds = new int[zzbmn.values().length];
    static final /* synthetic */ int[] zzfdt = new int[zzbml.values().length];
    static final /* synthetic */ int[] zzfdu = new int[zzblx.values().length];

    /* JADX WARNING: Can't wrap try/catch for region: R(21:0|(2:1|2)|3|(2:5|6)|7|(2:9|10)|11|13|14|15|16|17|18|19|21|22|23|24|25|26|28) */
    /* JADX WARNING: Can't wrap try/catch for region: R(22:0|1|2|3|(2:5|6)|7|(2:9|10)|11|13|14|15|16|17|18|19|21|22|23|24|25|26|28) */
    /* JADX WARNING: Can't wrap try/catch for region: R(23:0|1|2|3|5|6|7|(2:9|10)|11|13|14|15|16|17|18|19|21|22|23|24|25|26|28) */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:15:0x003d */
    /* JADX WARNING: Missing exception handler attribute for start block: B:17:0x0047 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:23:0x0064 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:25:0x006e */
    static {
        try {
            zzfdu[zzblx.UNCOMPRESSED.ordinal()] = 1;
        } catch (NoSuchFieldError unused) {
        }
        try {
            zzfdu[zzblx.DO_NOT_USE_CRUNCHY_UNCOMPRESSED.ordinal()] = 2;
        } catch (NoSuchFieldError unused2) {
        }
        try {
            zzfdu[zzblx.COMPRESSED.ordinal()] = 3;
        } catch (NoSuchFieldError unused3) {
        }
        zzfdt[zzbml.NIST_P256.ordinal()] = 1;
        zzfdt[zzbml.NIST_P384.ordinal()] = 2;
        zzfdt[zzbml.NIST_P521.ordinal()] = 3;
        zzfds[zzbmn.SHA1.ordinal()] = 1;
        zzfds[zzbmn.SHA256.ordinal()] = 2;
        try {
            zzfds[zzbmn.SHA512.ordinal()] = 3;
        } catch (NoSuchFieldError unused4) {
        }
    }
}
