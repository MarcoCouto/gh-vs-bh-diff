package com.google.android.gms.internal.ads;

import android.media.AudioAttributes.Builder;
import android.media.AudioFormat;
import android.media.AudioTrack;
import android.os.ConditionVariable;
import android.os.SystemClock;
import android.support.v4.media.session.PlaybackStateCompat;
import android.util.Log;
import com.applovin.sdk.AppLovinErrorCodes;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.extractor.ts.TsExtractor;
import com.google.android.exoplayer2.source.dash.DashMediaSource;
import com.google.android.exoplayer2.upstream.cache.CacheDataSink;
import com.google.android.exoplayer2.util.MimeTypes;
import java.lang.reflect.Method;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.LinkedList;

public final class zzgs {
    private static boolean zzabu = false;
    private static boolean zzabv = false;
    private int streamType;
    private final zzgh zzabw = null;
    private final zzhb zzabx;
    private final zzhi zzaby;
    private final zzgi[] zzabz;
    private final zzgy zzaca;
    /* access modifiers changed from: private */
    public final ConditionVariable zzacb;
    private final long[] zzacc;
    private final zzgu zzacd;
    private final LinkedList<zzgz> zzace;
    private AudioTrack zzacf;
    private int zzacg;
    private int zzach;
    private int zzaci;
    private boolean zzacj;
    private int zzack;
    private long zzacl;
    private zzfy zzacm;
    private long zzacn;
    private long zzaco;
    private ByteBuffer zzacp;
    private int zzacq;
    private int zzacr;
    private int zzacs;
    private long zzact;
    private long zzacu;
    private boolean zzacv;
    private long zzacw;
    private Method zzacx;
    private int zzacy;
    private long zzacz;
    private long zzada;
    private int zzadb;
    private long zzadc;
    private long zzadd;
    private int zzade;
    private int zzadf;
    private long zzadg;
    private long zzadh;
    private long zzadi;
    private zzgi[] zzadj;
    private ByteBuffer[] zzadk;
    private ByteBuffer zzadl;
    private ByteBuffer zzadm;
    private byte[] zzadn;
    private int zzado;
    private int zzadp;
    private boolean zzadq;
    private boolean zzadr;
    private int zzads;
    private boolean zzadt;
    private boolean zzadu;
    private long zzadv;
    private float zzcu;
    private zzfy zzxm;
    private int zzzu;

    public zzgs(zzgh zzgh, zzgi[] zzgiArr, zzgy zzgy) {
        this.zzaca = zzgy;
        this.zzacb = new ConditionVariable(true);
        if (zzqe.SDK_INT >= 18) {
            try {
                this.zzacx = AudioTrack.class.getMethod("getLatency", null);
            } catch (NoSuchMethodException unused) {
            }
        }
        if (zzqe.SDK_INT >= 19) {
            this.zzacd = new zzgv();
        } else {
            this.zzacd = new zzgu(null);
        }
        this.zzabx = new zzhb();
        this.zzaby = new zzhi();
        this.zzabz = new zzgi[(zzgiArr.length + 3)];
        this.zzabz[0] = new zzhg();
        this.zzabz[1] = this.zzabx;
        System.arraycopy(zzgiArr, 0, this.zzabz, 2, zzgiArr.length);
        this.zzabz[2 + zzgiArr.length] = this.zzaby;
        this.zzacc = new long[10];
        this.zzcu = 1.0f;
        this.zzadf = 0;
        this.streamType = 3;
        this.zzads = 0;
        this.zzxm = zzfy.zzaaf;
        this.zzadp = -1;
        this.zzadj = new zzgi[0];
        this.zzadk = new ByteBuffer[0];
        this.zzace = new LinkedList<>();
    }

    public final boolean zzq(String str) {
        return this.zzabw != null && this.zzabw.zzk(zzr(str));
    }

    public final long zzg(boolean z) {
        long j;
        long j2;
        long j3;
        if (!(isInitialized() && this.zzadf != 0)) {
            return Long.MIN_VALUE;
        }
        if (this.zzacf.getPlayState() == 3) {
            long zzde = this.zzacd.zzde();
            if (zzde != 0) {
                long nanoTime = System.nanoTime() / 1000;
                if (nanoTime - this.zzacu >= DashMediaSource.DEFAULT_LIVE_PRESENTATION_DELAY_FIXED_MS) {
                    this.zzacc[this.zzacr] = zzde - nanoTime;
                    this.zzacr = (this.zzacr + 1) % 10;
                    if (this.zzacs < 10) {
                        this.zzacs++;
                    }
                    this.zzacu = nanoTime;
                    this.zzact = 0;
                    for (int i = 0; i < this.zzacs; i++) {
                        this.zzact += this.zzacc[i] / ((long) this.zzacs);
                    }
                }
                if (!zzdc() && nanoTime - this.zzacw >= 500000) {
                    this.zzacv = this.zzacd.zzdf();
                    if (this.zzacv) {
                        long zzdg = this.zzacd.zzdg() / 1000;
                        long zzdh = this.zzacd.zzdh();
                        if (zzdg < this.zzadh) {
                            this.zzacv = false;
                        } else if (Math.abs(zzdg - nanoTime) > 5000000) {
                            StringBuilder sb = new StringBuilder(136);
                            sb.append("Spurious audio timestamp (system clock mismatch): ");
                            sb.append(zzdh);
                            sb.append(", ");
                            sb.append(zzdg);
                            sb.append(", ");
                            sb.append(nanoTime);
                            sb.append(", ");
                            sb.append(zzde);
                            Log.w("AudioTrack", sb.toString());
                            this.zzacv = false;
                        } else {
                            long j4 = nanoTime;
                            if (Math.abs(zzn(zzdh) - zzde) > 5000000) {
                                StringBuilder sb2 = new StringBuilder(TsExtractor.TS_STREAM_TYPE_DTS);
                                sb2.append("Spurious audio timestamp (frame position mismatch): ");
                                sb2.append(zzdh);
                                sb2.append(", ");
                                sb2.append(zzdg);
                                sb2.append(", ");
                                nanoTime = j4;
                                sb2.append(nanoTime);
                                sb2.append(", ");
                                sb2.append(zzde);
                                Log.w("AudioTrack", sb2.toString());
                                this.zzacv = false;
                            } else {
                                nanoTime = j4;
                            }
                        }
                    }
                    if (this.zzacx != null && !this.zzacj) {
                        try {
                            this.zzadi = (((long) ((Integer) this.zzacx.invoke(this.zzacf, null)).intValue()) * 1000) - this.zzacl;
                            this.zzadi = Math.max(this.zzadi, 0);
                            if (this.zzadi > 5000000) {
                                long j5 = this.zzadi;
                                StringBuilder sb3 = new StringBuilder(61);
                                sb3.append("Ignoring impossibly large audio latency: ");
                                sb3.append(j5);
                                Log.w("AudioTrack", sb3.toString());
                                this.zzadi = 0;
                            }
                        } catch (Exception unused) {
                            this.zzacx = null;
                        }
                    }
                    this.zzacw = nanoTime;
                }
            }
        }
        long nanoTime2 = System.nanoTime() / 1000;
        if (this.zzacv) {
            j = zzn(this.zzacd.zzdh() + zzo(nanoTime2 - (this.zzacd.zzdg() / 1000)));
        } else {
            if (this.zzacs == 0) {
                j3 = this.zzacd.zzde();
            } else {
                j3 = nanoTime2 + this.zzact;
            }
            j = !z ? j3 - this.zzadi : j3;
        }
        long j6 = this.zzadg;
        while (!this.zzace.isEmpty() && j >= ((zzgz) this.zzace.getFirst()).zzyz) {
            zzgz zzgz = (zzgz) this.zzace.remove();
            this.zzxm = zzgz.zzxm;
            this.zzaco = zzgz.zzyz;
            this.zzacn = zzgz.zzaek - this.zzadg;
        }
        if (this.zzxm.zzaag == 1.0f) {
            j2 = (j + this.zzacn) - this.zzaco;
        } else if (!this.zzace.isEmpty() || this.zzaby.zzdn() < PlaybackStateCompat.ACTION_PLAY_FROM_MEDIA_ID) {
            j2 = this.zzacn + ((long) (((double) this.zzxm.zzaag) * ((double) (j - this.zzaco))));
        } else {
            j2 = this.zzacn + zzqe.zza(j - this.zzaco, this.zzaby.zzdm(), this.zzaby.zzdn());
        }
        return j6 + j2;
    }

    /* JADX WARNING: Removed duplicated region for block: B:66:0x00e6  */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x00f1  */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x0102  */
    /* JADX WARNING: Removed duplicated region for block: B:85:0x013a  */
    /* JADX WARNING: Removed duplicated region for block: B:86:0x0140  */
    public final void zza(String str, int i, int i2, int i3, int i4, int[] iArr) throws zzgw {
        boolean z;
        int i5;
        int i6;
        long j;
        boolean z2 = true;
        boolean z3 = !MimeTypes.AUDIO_RAW.equals(str);
        int zzr = z3 ? zzr(str) : i3;
        if (!z3) {
            this.zzacy = zzqe.zzg(i3, i);
            this.zzabx.zzb(iArr);
            zzgi[] zzgiArr = this.zzabz;
            int length = zzgiArr.length;
            int i7 = zzr;
            int i8 = i;
            int i9 = 0;
            boolean z4 = false;
            while (i9 < length) {
                zzgi zzgi = zzgiArr[i9];
                try {
                    z4 |= zzgi.zzb(i2, i8, i7);
                    if (zzgi.isActive()) {
                        i8 = zzgi.zzco();
                        i7 = zzgi.zzcp();
                    }
                    i9++;
                } catch (zzgj e) {
                    throw new zzgw((Throwable) e);
                }
            }
            if (z4) {
                zzcs();
            }
            z = z4;
            i = i8;
            zzr = i7;
        } else {
            z = false;
        }
        int i10 = 12;
        int i11 = 252;
        switch (i) {
            case 1:
                i5 = 4;
                break;
            case 2:
                i5 = 12;
                break;
            case 3:
                i5 = 28;
                break;
            case 4:
                i5 = AppLovinErrorCodes.NO_FILL;
                break;
            case 5:
                i5 = 220;
                break;
            case 6:
                i5 = 252;
                break;
            case 7:
                i5 = 1276;
                break;
            case 8:
                i5 = zzfe.CHANNEL_OUT_7POINT1_SURROUND;
                break;
            default:
                StringBuilder sb = new StringBuilder(38);
                sb.append("Unsupported channel count: ");
                sb.append(i);
                throw new zzgw(sb.toString());
        }
        if (zzqe.SDK_INT <= 23 && "foster".equals(zzqe.DEVICE) && "NVIDIA".equals(zzqe.MANUFACTURER)) {
            if (!(i == 3 || i == 5)) {
                if (i == 7) {
                    i11 = zzfe.CHANNEL_OUT_7POINT1_SURROUND;
                }
            }
            if (zzqe.SDK_INT > 25 || !"fugu".equals(zzqe.DEVICE) || !z3 || i != 1) {
                i10 = i11;
            }
            if (!z || !isInitialized() || this.zzach != zzr || this.zzzu != i2 || this.zzacg != i10) {
                reset();
                this.zzach = zzr;
                this.zzacj = z3;
                this.zzzu = i2;
                this.zzacg = i10;
                if (!z3) {
                    zzr = 2;
                }
                this.zzaci = zzr;
                this.zzadb = zzqe.zzg(2, i);
                if (!z3) {
                    i6 = (this.zzaci == 5 || this.zzaci == 6) ? CacheDataSink.DEFAULT_BUFFER_SIZE : 49152;
                } else {
                    int minBufferSize = AudioTrack.getMinBufferSize(i2, i10, this.zzaci);
                    if (minBufferSize == -2) {
                        z2 = false;
                    }
                    zzpo.checkState(z2);
                    int i12 = minBufferSize << 2;
                    int zzo = ((int) zzo(250000)) * this.zzadb;
                    i6 = (int) Math.max((long) minBufferSize, zzo(750000) * ((long) this.zzadb));
                    if (i12 < zzo) {
                        i6 = zzo;
                    } else if (i12 <= i6) {
                        i6 = i12;
                    }
                }
                this.zzack = i6;
                if (!z3) {
                    j = C.TIME_UNSET;
                } else {
                    j = zzn((long) (this.zzack / this.zzadb));
                }
                this.zzacl = j;
                zzb(this.zzxm);
            }
            return;
        }
        i11 = i5;
        i10 = i11;
        if (!z) {
        }
        reset();
        this.zzach = zzr;
        this.zzacj = z3;
        this.zzzu = i2;
        this.zzacg = i10;
        if (!z3) {
        }
        this.zzaci = zzr;
        this.zzadb = zzqe.zzg(2, i);
        if (!z3) {
        }
        this.zzack = i6;
        if (!z3) {
        }
        this.zzacl = j;
        zzb(this.zzxm);
    }

    private final void zzcs() {
        zzgi[] zzgiArr;
        ArrayList arrayList = new ArrayList();
        for (zzgi zzgi : this.zzabz) {
            if (zzgi.isActive()) {
                arrayList.add(zzgi);
            } else {
                zzgi.flush();
            }
        }
        int size = arrayList.size();
        this.zzadj = (zzgi[]) arrayList.toArray(new zzgi[size]);
        this.zzadk = new ByteBuffer[size];
        for (int i = 0; i < size; i++) {
            zzgi zzgi2 = this.zzadj[i];
            zzgi2.flush();
            this.zzadk[i] = zzgi2.zzcr();
        }
    }

    public final void play() {
        this.zzadr = true;
        if (isInitialized()) {
            this.zzadh = System.nanoTime() / 1000;
            this.zzacf.play();
        }
    }

    public final void zzct() {
        if (this.zzadf == 1) {
            this.zzadf = 2;
        }
    }

    public final boolean zza(ByteBuffer byteBuffer, long j) throws zzgx, zzha {
        int i;
        int i2;
        ByteBuffer byteBuffer2 = byteBuffer;
        long j2 = j;
        zzpo.checkArgument(this.zzadl == null || byteBuffer2 == this.zzadl);
        if (!isInitialized()) {
            this.zzacb.block();
            if (this.zzadt) {
                AudioTrack audioTrack = new AudioTrack(new Builder().setUsage(1).setContentType(3).setFlags(16).build(), new AudioFormat.Builder().setChannelMask(this.zzacg).setEncoding(this.zzaci).setSampleRate(this.zzzu).build(), this.zzack, 1, this.zzads);
                this.zzacf = audioTrack;
            } else if (this.zzads == 0) {
                AudioTrack audioTrack2 = new AudioTrack(this.streamType, this.zzzu, this.zzacg, this.zzaci, this.zzack, 1);
                this.zzacf = audioTrack2;
            } else {
                AudioTrack audioTrack3 = new AudioTrack(this.streamType, this.zzzu, this.zzacg, this.zzaci, this.zzack, 1, this.zzads);
                this.zzacf = audioTrack3;
            }
            int state = this.zzacf.getState();
            if (state != 1) {
                try {
                    this.zzacf.release();
                    this.zzacf = null;
                } catch (Exception unused) {
                    this.zzacf = null;
                } catch (Throwable th) {
                    Throwable th2 = th;
                    this.zzacf = null;
                    throw th2;
                }
                throw new zzgx(state, this.zzzu, this.zzacg, this.zzack);
            }
            int audioSessionId = this.zzacf.getAudioSessionId();
            if (this.zzads != audioSessionId) {
                this.zzads = audioSessionId;
                this.zzaca.zzl(audioSessionId);
            }
            this.zzacd.zza(this.zzacf, zzdc());
            zzcz();
            this.zzadu = false;
            if (this.zzadr) {
                play();
            }
        }
        if (zzdc()) {
            if (this.zzacf.getPlayState() == 2) {
                this.zzadu = false;
                return false;
            } else if (this.zzacf.getPlayState() == 1 && this.zzacd.zzdd() != 0) {
                return false;
            }
        }
        boolean z = this.zzadu;
        this.zzadu = zzcw();
        if (z && !this.zzadu && this.zzacf.getPlayState() != 1) {
            this.zzaca.zzc(this.zzack, zzfe.zzf(this.zzacl), SystemClock.elapsedRealtime() - this.zzadv);
        }
        if (this.zzadl == null) {
            if (!byteBuffer.hasRemaining()) {
                return true;
            }
            if (this.zzacj && this.zzade == 0) {
                int i3 = this.zzaci;
                if (i3 == 7 || i3 == 8) {
                    i2 = zzhc.zzj(byteBuffer);
                } else if (i3 == 5) {
                    i2 = zzgg.zzcn();
                } else if (i3 == 6) {
                    i2 = zzgg.zzh(byteBuffer);
                } else {
                    StringBuilder sb = new StringBuilder(38);
                    sb.append("Unexpected audio encoding: ");
                    sb.append(i3);
                    throw new IllegalStateException(sb.toString());
                }
                this.zzade = i2;
            }
            if (this.zzacm != null) {
                if (!zzcv()) {
                    return false;
                }
                LinkedList<zzgz> linkedList = this.zzace;
                zzgz zzgz = r12;
                zzgz zzgz2 = new zzgz(this.zzacm, Math.max(0, j2), zzn(zzda()), null);
                linkedList.add(zzgz);
                this.zzacm = null;
                zzcs();
            }
            if (this.zzadf == 0) {
                this.zzadg = Math.max(0, j2);
                this.zzadf = 1;
            } else {
                long zzn = this.zzadg + zzn(this.zzacj ? this.zzada : this.zzacz / ((long) this.zzacy));
                if (this.zzadf != 1 || Math.abs(zzn - j2) <= 200000) {
                    i = 2;
                } else {
                    StringBuilder sb2 = new StringBuilder(80);
                    sb2.append("Discontinuity detected [expected ");
                    sb2.append(zzn);
                    sb2.append(", got ");
                    sb2.append(j2);
                    sb2.append("]");
                    Log.e("AudioTrack", sb2.toString());
                    i = 2;
                    this.zzadf = 2;
                }
                if (this.zzadf == i) {
                    this.zzadg += j2 - zzn;
                    this.zzadf = 1;
                    this.zzaca.zzbs();
                }
            }
            if (this.zzacj) {
                this.zzada += (long) this.zzade;
            } else {
                this.zzacz += (long) byteBuffer.remaining();
            }
            this.zzadl = byteBuffer2;
        }
        if (this.zzacj) {
            zzb(this.zzadl, j2);
        } else {
            zzm(j2);
        }
        if (this.zzadl.hasRemaining()) {
            return false;
        }
        this.zzadl = null;
        return true;
    }

    private final void zzm(long j) throws zzha {
        int length = this.zzadj.length;
        int i = length;
        while (i >= 0) {
            ByteBuffer byteBuffer = i > 0 ? this.zzadk[i - 1] : this.zzadl != null ? this.zzadl : zzgi.zzabh;
            if (i == length) {
                zzb(byteBuffer, j);
            } else {
                zzgi zzgi = this.zzadj[i];
                zzgi.zzi(byteBuffer);
                ByteBuffer zzcr = zzgi.zzcr();
                this.zzadk[i] = zzcr;
                if (zzcr.hasRemaining()) {
                    i++;
                }
            }
            if (!byteBuffer.hasRemaining()) {
                i--;
            } else {
                return;
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00dc, code lost:
        if (r13 < r12) goto L_0x007a;
     */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x00fd  */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x0103  */
    private final boolean zzb(ByteBuffer byteBuffer, long j) throws zzha {
        int i;
        if (!byteBuffer.hasRemaining()) {
            return true;
        }
        if (this.zzadm != null) {
            zzpo.checkArgument(this.zzadm == byteBuffer);
        } else {
            this.zzadm = byteBuffer;
            if (zzqe.SDK_INT < 21) {
                int remaining = byteBuffer.remaining();
                if (this.zzadn == null || this.zzadn.length < remaining) {
                    this.zzadn = new byte[remaining];
                }
                int position = byteBuffer.position();
                byteBuffer.get(this.zzadn, 0, remaining);
                byteBuffer.position(position);
                this.zzado = 0;
            }
        }
        int remaining2 = byteBuffer.remaining();
        if (zzqe.SDK_INT < 21) {
            int zzdd = this.zzack - ((int) (this.zzadc - (this.zzacd.zzdd() * ((long) this.zzadb))));
            if (zzdd > 0) {
                i = this.zzacf.write(this.zzadn, this.zzado, Math.min(remaining2, zzdd));
                if (i > 0) {
                    this.zzado += i;
                    byteBuffer.position(byteBuffer.position() + i);
                }
                this.zzadv = SystemClock.elapsedRealtime();
                if (i < 0) {
                    throw new zzha(i);
                }
                if (!this.zzacj) {
                    this.zzadc += (long) i;
                }
                if (i != remaining2) {
                    return false;
                }
                if (this.zzacj) {
                    this.zzadd += (long) this.zzade;
                }
                this.zzadm = null;
                return true;
            }
        } else {
            if (this.zzadt) {
                zzpo.checkState(j != C.TIME_UNSET);
                AudioTrack audioTrack = this.zzacf;
                if (this.zzacp == null) {
                    this.zzacp = ByteBuffer.allocate(16);
                    this.zzacp.order(ByteOrder.BIG_ENDIAN);
                    this.zzacp.putInt(1431633921);
                }
                if (this.zzacq == 0) {
                    this.zzacp.putInt(4, remaining2);
                    this.zzacp.putLong(8, j * 1000);
                    this.zzacp.position(0);
                    this.zzacq = remaining2;
                }
                int remaining3 = this.zzacp.remaining();
                if (remaining3 > 0) {
                    int write = audioTrack.write(this.zzacp, remaining3, 1);
                    if (write < 0) {
                        this.zzacq = 0;
                        i = write;
                    }
                }
                int write2 = audioTrack.write(byteBuffer, remaining2, 1);
                if (write2 < 0) {
                    this.zzacq = 0;
                } else {
                    this.zzacq -= write2;
                }
                i = write2;
            } else {
                i = this.zzacf.write(byteBuffer, remaining2, 1);
            }
            this.zzadv = SystemClock.elapsedRealtime();
            if (i < 0) {
            }
        }
        i = 0;
        this.zzadv = SystemClock.elapsedRealtime();
        if (i < 0) {
        }
    }

    public final void zzcu() throws zzha {
        if (!this.zzadq && isInitialized() && zzcv()) {
            this.zzacd.zzp(zzda());
            this.zzacq = 0;
            this.zzadq = true;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x0021  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0040  */
    private final boolean zzcv() throws zzha {
        boolean z;
        if (this.zzadp == -1) {
            this.zzadp = this.zzacj ? this.zzadj.length : 0;
        } else {
            z = false;
            if (this.zzadp >= this.zzadj.length) {
                zzgi zzgi = this.zzadj[this.zzadp];
                if (z) {
                    zzgi.zzcq();
                }
                zzm(C.TIME_UNSET);
                if (!zzgi.zzcj()) {
                    return false;
                }
                this.zzadp++;
                return false;
            }
            if (this.zzadm != null) {
                zzb(this.zzadm, C.TIME_UNSET);
                if (this.zzadm != null) {
                    return false;
                }
            }
            this.zzadp = -1;
            return true;
        }
        z = true;
        if (this.zzadp >= this.zzadj.length) {
        }
        if (this.zzadm != null) {
        }
        this.zzadp = -1;
        return true;
    }

    public final boolean zzcj() {
        return !isInitialized() || (this.zzadq && !zzcw());
    }

    public final boolean zzcw() {
        if (isInitialized()) {
            if (zzda() <= this.zzacd.zzdd()) {
                if (zzdc() && this.zzacf.getPlayState() == 2 && this.zzacf.getPlaybackHeadPosition() == 0) {
                    return true;
                }
            }
            return true;
        }
        return false;
    }

    public final zzfy zzb(zzfy zzfy) {
        zzfy zzfy2;
        if (this.zzacj) {
            this.zzxm = zzfy.zzaaf;
            return this.zzxm;
        }
        zzfy zzfy3 = new zzfy(this.zzaby.zzb(zzfy.zzaag), this.zzaby.zzc(zzfy.zzaah));
        if (this.zzacm != null) {
            zzfy2 = this.zzacm;
        } else if (!this.zzace.isEmpty()) {
            zzfy2 = ((zzgz) this.zzace.getLast()).zzxm;
        } else {
            zzfy2 = this.zzxm;
        }
        if (!zzfy3.equals(zzfy2)) {
            if (isInitialized()) {
                this.zzacm = zzfy3;
            } else {
                this.zzxm = zzfy3;
            }
        }
        return this.zzxm;
    }

    public final zzfy zzcx() {
        return this.zzxm;
    }

    public final void setStreamType(int i) {
        if (this.streamType != i) {
            this.streamType = i;
            if (!this.zzadt) {
                reset();
                this.zzads = 0;
            }
        }
    }

    public final void zzn(int i) {
        zzpo.checkState(zzqe.SDK_INT >= 21);
        if (!this.zzadt || this.zzads != i) {
            this.zzadt = true;
            this.zzads = i;
            reset();
        }
    }

    public final void zzcy() {
        if (this.zzadt) {
            this.zzadt = false;
            this.zzads = 0;
            reset();
        }
    }

    public final void setVolume(float f) {
        if (this.zzcu != f) {
            this.zzcu = f;
            zzcz();
        }
    }

    private final void zzcz() {
        if (isInitialized()) {
            if (zzqe.SDK_INT >= 21) {
                this.zzacf.setVolume(this.zzcu);
                return;
            }
            AudioTrack audioTrack = this.zzacf;
            float f = this.zzcu;
            audioTrack.setStereoVolume(f, f);
        }
    }

    public final void pause() {
        this.zzadr = false;
        if (isInitialized()) {
            zzdb();
            this.zzacd.pause();
        }
    }

    public final void reset() {
        if (isInitialized()) {
            this.zzacz = 0;
            this.zzada = 0;
            this.zzadc = 0;
            this.zzadd = 0;
            this.zzade = 0;
            if (this.zzacm != null) {
                this.zzxm = this.zzacm;
                this.zzacm = null;
            } else if (!this.zzace.isEmpty()) {
                this.zzxm = ((zzgz) this.zzace.getLast()).zzxm;
            }
            this.zzace.clear();
            this.zzacn = 0;
            this.zzaco = 0;
            this.zzadl = null;
            this.zzadm = null;
            for (int i = 0; i < this.zzadj.length; i++) {
                zzgi zzgi = this.zzadj[i];
                zzgi.flush();
                this.zzadk[i] = zzgi.zzcr();
            }
            this.zzadq = false;
            this.zzadp = -1;
            this.zzacp = null;
            this.zzacq = 0;
            this.zzadf = 0;
            this.zzadi = 0;
            zzdb();
            if (this.zzacf.getPlayState() == 3) {
                this.zzacf.pause();
            }
            AudioTrack audioTrack = this.zzacf;
            this.zzacf = null;
            this.zzacd.zza(null, false);
            this.zzacb.close();
            new zzgt(this, audioTrack).start();
        }
    }

    public final void release() {
        reset();
        for (zzgi reset : this.zzabz) {
            reset.reset();
        }
        this.zzads = 0;
        this.zzadr = false;
    }

    private final boolean isInitialized() {
        return this.zzacf != null;
    }

    private final long zzn(long j) {
        return (j * C.MICROS_PER_SECOND) / ((long) this.zzzu);
    }

    private final long zzo(long j) {
        return (j * ((long) this.zzzu)) / C.MICROS_PER_SECOND;
    }

    private final long zzda() {
        return this.zzacj ? this.zzadd : this.zzadc / ((long) this.zzadb);
    }

    private final void zzdb() {
        this.zzact = 0;
        this.zzacs = 0;
        this.zzacr = 0;
        this.zzacu = 0;
        this.zzacv = false;
        this.zzacw = 0;
    }

    private final boolean zzdc() {
        return zzqe.SDK_INT < 23 && (this.zzaci == 5 || this.zzaci == 6);
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x0046 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0047  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x004a  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x004c  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x004e  */
    private static int zzr(String str) {
        char c;
        int hashCode = str.hashCode();
        if (hashCode == -1095064472) {
            if (str.equals(MimeTypes.AUDIO_DTS)) {
                c = 2;
                switch (c) {
                    case 0:
                        break;
                    case 1:
                        break;
                    case 2:
                        break;
                    case 3:
                        break;
                }
            }
        } else if (hashCode == 187078296) {
            if (str.equals(MimeTypes.AUDIO_AC3)) {
                c = 0;
                switch (c) {
                    case 0:
                        break;
                    case 1:
                        break;
                    case 2:
                        break;
                    case 3:
                        break;
                }
            }
        } else if (hashCode == 1504578661) {
            if (str.equals(MimeTypes.AUDIO_E_AC3)) {
                c = 1;
                switch (c) {
                    case 0:
                        break;
                    case 1:
                        break;
                    case 2:
                        break;
                    case 3:
                        break;
                }
            }
        } else if (hashCode == 1505942594 && str.equals(MimeTypes.AUDIO_DTS_HD)) {
            c = 3;
            switch (c) {
                case 0:
                    return 5;
                case 1:
                    return 6;
                case 2:
                    return 7;
                case 3:
                    return 8;
                default:
                    return 0;
            }
        }
        c = 65535;
        switch (c) {
            case 0:
                break;
            case 1:
                break;
            case 2:
                break;
            case 3:
                break;
        }
    }
}
