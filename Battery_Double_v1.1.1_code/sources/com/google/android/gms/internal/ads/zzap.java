package com.google.android.gms.internal.ads;

import io.fabric.sdk.android.services.network.HttpRequest;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

public final class zzap {
    public static zzc zzb(zzp zzp) {
        boolean z;
        long j;
        boolean z2;
        long j2;
        long j3;
        long j4;
        zzp zzp2 = zzp;
        long currentTimeMillis = System.currentTimeMillis();
        Map<String, String> map = zzp2.zzab;
        String str = (String) map.get(HttpRequest.HEADER_DATE);
        long zzf = str != null ? zzf(str) : 0;
        String str2 = (String) map.get(HttpRequest.HEADER_CACHE_CONTROL);
        if (str2 != null) {
            String[] split = str2.split(",", 0);
            z2 = false;
            j2 = 0;
            j = 0;
            for (String trim : split) {
                String trim2 = trim.trim();
                if (trim2.equals("no-cache") || trim2.equals("no-store")) {
                    return null;
                }
                if (trim2.startsWith("max-age=")) {
                    try {
                        j2 = Long.parseLong(trim2.substring(8));
                    } catch (Exception unused) {
                    }
                } else if (trim2.startsWith("stale-while-revalidate=")) {
                    j = Long.parseLong(trim2.substring(23));
                } else if (trim2.equals("must-revalidate") || trim2.equals("proxy-revalidate")) {
                    z2 = true;
                }
            }
            z = true;
        } else {
            z2 = false;
            z = false;
            j2 = 0;
            j = 0;
        }
        String str3 = (String) map.get(HttpRequest.HEADER_EXPIRES);
        long zzf2 = str3 != null ? zzf(str3) : 0;
        String str4 = (String) map.get(HttpRequest.HEADER_LAST_MODIFIED);
        long zzf3 = str4 != null ? zzf(str4) : 0;
        String str5 = (String) map.get(HttpRequest.HEADER_ETAG);
        if (z) {
            long j5 = currentTimeMillis + (j2 * 1000);
            j4 = z2 ? j5 : j5 + (j * 1000);
            j3 = j5;
        } else if (zzf <= 0 || zzf2 < zzf) {
            j4 = 0;
            j3 = 0;
        } else {
            j3 = currentTimeMillis + (zzf2 - zzf);
            j4 = j3;
        }
        zzc zzc = new zzc();
        zzc.data = zzp2.data;
        zzc.zza = str5;
        zzc.zze = j3;
        zzc.zzd = j4;
        zzc.zzb = zzf;
        zzc.zzc = zzf3;
        zzc.zzf = map;
        zzc.zzg = zzp2.allHeaders;
        return zzc;
    }

    private static long zzf(String str) {
        try {
            return zzq().parse(str).getTime();
        } catch (ParseException e) {
            zzaf.zza(e, "Unable to parse dateStr: %s, falling back to 0", str);
            return 0;
        }
    }

    static String zzb(long j) {
        return zzq().format(new Date(j));
    }

    private static SimpleDateFormat zzq() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz", Locale.US);
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        return simpleDateFormat;
    }
}
