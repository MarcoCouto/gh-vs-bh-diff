package com.google.android.gms.internal.ads;

import com.github.mikephil.charting.utils.Utils;
import com.google.android.gms.internal.ads.zzbrd.zze;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import sun.misc.Unsafe;

final class zzbsp<T> implements zzbtc<T> {
    private static final int[] zzfsg = new int[0];
    private static final Unsafe zzfsh = zzbua.zzape();
    private final int[] zzfsi;
    private final Object[] zzfsj;
    private final int zzfsk;
    private final int zzfsl;
    private final zzbsl zzfsm;
    private final boolean zzfsn;
    private final boolean zzfso;
    private final boolean zzfsp;
    private final boolean zzfsq;
    private final int[] zzfsr;
    private final int zzfss;
    private final int zzfst;
    private final zzbst zzfsu;
    private final zzbrv zzfsv;
    private final zzbtu<?, ?> zzfsw;
    private final zzbqr<?> zzfsx;
    private final zzbsg zzfsy;

    private zzbsp(int[] iArr, Object[] objArr, int i, int i2, zzbsl zzbsl, boolean z, boolean z2, int[] iArr2, int i3, int i4, zzbst zzbst, zzbrv zzbrv, zzbtu<?, ?> zzbtu, zzbqr<?> zzbqr, zzbsg zzbsg) {
        this.zzfsi = iArr;
        this.zzfsj = objArr;
        this.zzfsk = i;
        this.zzfsl = i2;
        this.zzfso = zzbsl instanceof zzbrd;
        this.zzfsp = z;
        this.zzfsn = zzbqr != null && zzbqr.zzh(zzbsl);
        this.zzfsq = false;
        this.zzfsr = iArr2;
        this.zzfss = i3;
        this.zzfst = i4;
        this.zzfsu = zzbst;
        this.zzfsv = zzbrv;
        this.zzfsw = zzbtu;
        this.zzfsx = zzbqr;
        this.zzfsm = zzbsl;
        this.zzfsy = zzbsg;
    }

    private static boolean zzfv(int i) {
        return (i & 536870912) != 0;
    }

    /* JADX WARNING: Removed duplicated region for block: B:167:0x03ba  */
    static <T> zzbsp<T> zza(Class<T> cls, zzbsj zzbsj, zzbst zzbst, zzbrv zzbrv, zzbtu<?, ?> zzbtu, zzbqr<?> zzbqr, zzbsg zzbsg) {
        int i;
        int[] iArr;
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        int i7;
        int i8;
        int i9;
        int i10;
        int i11;
        int i12;
        int i13;
        boolean z;
        int i14;
        int i15;
        int i16;
        int i17;
        Field zza;
        int i18;
        char charAt;
        int i19;
        int i20;
        int i21;
        int i22;
        char c;
        Field zza2;
        Field zza3;
        char charAt2;
        int i23;
        char charAt3;
        int i24;
        char charAt4;
        int i25;
        int i26;
        char charAt5;
        int i27;
        char charAt6;
        int i28;
        char charAt7;
        int i29;
        char charAt8;
        int i30;
        char charAt9;
        int i31;
        char charAt10;
        int i32;
        char charAt11;
        int i33;
        char charAt12;
        int i34;
        char charAt13;
        char charAt14;
        zzbsj zzbsj2 = zzbsj;
        if (zzbsj2 instanceof zzbta) {
            zzbta zzbta = (zzbta) zzbsj2;
            int i35 = 0;
            boolean z2 = zzbta.zzanz() == zze.zzfqk;
            String zzaoi = zzbta.zzaoi();
            int length = zzaoi.length();
            char charAt15 = zzaoi.charAt(0);
            if (charAt15 >= 55296) {
                char c2 = charAt15 & 8191;
                int i36 = 13;
                int i37 = 1;
                while (true) {
                    i = i37 + 1;
                    charAt14 = zzaoi.charAt(i37);
                    if (charAt14 < 55296) {
                        break;
                    }
                    c2 |= (charAt14 & 8191) << i36;
                    i36 += 13;
                    i37 = i;
                }
                charAt15 = (charAt14 << i36) | c2;
            } else {
                i = 1;
            }
            int i38 = i + 1;
            char charAt16 = zzaoi.charAt(i);
            if (charAt16 >= 55296) {
                char c3 = charAt16 & 8191;
                int i39 = 13;
                while (true) {
                    i34 = i38 + 1;
                    charAt13 = zzaoi.charAt(i38);
                    if (charAt13 < 55296) {
                        break;
                    }
                    c3 |= (charAt13 & 8191) << i39;
                    i39 += 13;
                    i38 = i34;
                }
                charAt16 = c3 | (charAt13 << i39);
                i38 = i34;
            }
            if (charAt16 == 0) {
                i6 = 0;
                i5 = 0;
                i4 = 0;
                i3 = 0;
                i2 = 0;
                iArr = zzfsg;
                i7 = 0;
            } else {
                int i40 = i38 + 1;
                char charAt17 = zzaoi.charAt(i38);
                if (charAt17 >= 55296) {
                    char c4 = charAt17 & 8191;
                    int i41 = 13;
                    while (true) {
                        i33 = i40 + 1;
                        charAt12 = zzaoi.charAt(i40);
                        if (charAt12 < 55296) {
                            break;
                        }
                        c4 |= (charAt12 & 8191) << i41;
                        i41 += 13;
                        i40 = i33;
                    }
                    charAt17 = c4 | (charAt12 << i41);
                    i40 = i33;
                }
                int i42 = i40 + 1;
                char charAt18 = zzaoi.charAt(i40);
                if (charAt18 >= 55296) {
                    char c5 = charAt18 & 8191;
                    int i43 = 13;
                    while (true) {
                        i32 = i42 + 1;
                        charAt11 = zzaoi.charAt(i42);
                        if (charAt11 < 55296) {
                            break;
                        }
                        c5 |= (charAt11 & 8191) << i43;
                        i43 += 13;
                        i42 = i32;
                    }
                    charAt18 = c5 | (charAt11 << i43);
                    i42 = i32;
                }
                int i44 = i42 + 1;
                i6 = zzaoi.charAt(i42);
                if (i6 >= 55296) {
                    int i45 = i6 & 8191;
                    int i46 = 13;
                    while (true) {
                        i31 = i44 + 1;
                        charAt10 = zzaoi.charAt(i44);
                        if (charAt10 < 55296) {
                            break;
                        }
                        i45 |= (charAt10 & 8191) << i46;
                        i46 += 13;
                        i44 = i31;
                    }
                    i6 = i45 | (charAt10 << i46);
                    i44 = i31;
                }
                int i47 = i44 + 1;
                i5 = zzaoi.charAt(i44);
                if (i5 >= 55296) {
                    int i48 = i5 & 8191;
                    int i49 = 13;
                    while (true) {
                        i30 = i47 + 1;
                        charAt9 = zzaoi.charAt(i47);
                        if (charAt9 < 55296) {
                            break;
                        }
                        i48 |= (charAt9 & 8191) << i49;
                        i49 += 13;
                        i47 = i30;
                    }
                    i5 = i48 | (charAt9 << i49);
                    i47 = i30;
                }
                int i50 = i47 + 1;
                i4 = zzaoi.charAt(i47);
                if (i4 >= 55296) {
                    int i51 = i4 & 8191;
                    int i52 = 13;
                    while (true) {
                        i29 = i50 + 1;
                        charAt8 = zzaoi.charAt(i50);
                        if (charAt8 < 55296) {
                            break;
                        }
                        i51 |= (charAt8 & 8191) << i52;
                        i52 += 13;
                        i50 = i29;
                    }
                    i4 = i51 | (charAt8 << i52);
                    i50 = i29;
                }
                int i53 = i50 + 1;
                char charAt19 = zzaoi.charAt(i50);
                if (charAt19 >= 55296) {
                    char c6 = charAt19 & 8191;
                    int i54 = 13;
                    while (true) {
                        i28 = i53 + 1;
                        charAt7 = zzaoi.charAt(i53);
                        if (charAt7 < 55296) {
                            break;
                        }
                        c6 |= (charAt7 & 8191) << i54;
                        i54 += 13;
                        i53 = i28;
                    }
                    charAt19 = c6 | (charAt7 << i54);
                    i53 = i28;
                }
                int i55 = i53 + 1;
                char charAt20 = zzaoi.charAt(i53);
                if (charAt20 >= 55296) {
                    int i56 = 13;
                    int i57 = i55;
                    int i58 = charAt20 & 8191;
                    int i59 = i57;
                    while (true) {
                        i27 = i59 + 1;
                        charAt6 = zzaoi.charAt(i59);
                        if (charAt6 < 55296) {
                            break;
                        }
                        i58 |= (charAt6 & 8191) << i56;
                        i56 += 13;
                        i59 = i27;
                    }
                    charAt20 = i58 | (charAt6 << i56);
                    i25 = i27;
                } else {
                    i25 = i55;
                }
                int i60 = i25 + 1;
                char charAt21 = zzaoi.charAt(i25);
                if (charAt21 >= 55296) {
                    int i61 = 13;
                    int i62 = i60;
                    int i63 = charAt21 & 8191;
                    int i64 = i62;
                    while (true) {
                        i26 = i64 + 1;
                        charAt5 = zzaoi.charAt(i64);
                        if (charAt5 < 55296) {
                            break;
                        }
                        i63 |= (charAt5 & 8191) << i61;
                        i61 += 13;
                        i64 = i26;
                    }
                    charAt21 = i63 | (charAt5 << i61);
                    i60 = i26;
                }
                int i65 = charAt19;
                i3 = charAt21;
                i35 = i65;
                int i66 = (charAt17 << 1) + charAt18;
                i7 = charAt17;
                i38 = i60;
                iArr = new int[(charAt21 + charAt19 + charAt20)];
                i2 = i66;
            }
            Unsafe unsafe = zzfsh;
            Object[] zzaoj = zzbta.zzaoj();
            Class cls2 = zzbta.zzaob().getClass();
            int i67 = i38;
            int[] iArr2 = new int[(i4 * 3)];
            Object[] objArr = new Object[(i4 << 1)];
            int i68 = i35 + i3;
            int i69 = i3;
            int i70 = i2;
            int i71 = 0;
            int i72 = 0;
            int i73 = i68;
            for (int i74 = i67; i74 < length; i74 = i10) {
                int i75 = i74 + 1;
                int charAt22 = zzaoi.charAt(i74);
                char c7 = 55296;
                if (charAt22 >= 55296) {
                    int i76 = 13;
                    int i77 = i75;
                    int i78 = charAt22 & 8191;
                    int i79 = i77;
                    while (true) {
                        i24 = i79 + 1;
                        charAt4 = zzaoi.charAt(i79);
                        if (charAt4 < c7) {
                            break;
                        }
                        i78 |= (charAt4 & 8191) << i76;
                        i76 += 13;
                        i79 = i24;
                        c7 = 55296;
                    }
                    charAt22 = i78 | (charAt4 << i76);
                    i8 = i24;
                } else {
                    i8 = i75;
                }
                int i80 = i8 + 1;
                char charAt23 = zzaoi.charAt(i8);
                int i81 = length;
                char c8 = 55296;
                if (charAt23 >= 55296) {
                    int i82 = 13;
                    int i83 = i80;
                    int i84 = charAt23 & 8191;
                    int i85 = i83;
                    while (true) {
                        i23 = i85 + 1;
                        charAt3 = zzaoi.charAt(i85);
                        if (charAt3 < c8) {
                            break;
                        }
                        i84 |= (charAt3 & 8191) << i82;
                        i82 += 13;
                        i85 = i23;
                        c8 = 55296;
                    }
                    charAt23 = i84 | (charAt3 << i82);
                    i9 = i23;
                } else {
                    i9 = i80;
                }
                int i86 = i68;
                char c9 = charAt23 & 255;
                int i87 = i3;
                if ((charAt23 & 1024) != 0) {
                    int i88 = i71 + 1;
                    iArr[i71] = i72;
                    i71 = i88;
                }
                if (c9 > zzbqx.MAP.id()) {
                    int i89 = i9 + 1;
                    char charAt24 = zzaoi.charAt(i9);
                    int i90 = i89;
                    char c10 = 55296;
                    if (charAt24 >= 55296) {
                        char c11 = charAt24 & 8191;
                        int i91 = i90;
                        int i92 = 13;
                        while (true) {
                            i21 = i91 + 1;
                            charAt2 = zzaoi.charAt(i91);
                            if (charAt2 < c10) {
                                break;
                            }
                            c11 |= (charAt2 & 8191) << i92;
                            i92 += 13;
                            i91 = i21;
                            c10 = 55296;
                        }
                        charAt24 = c11 | (charAt2 << i92);
                    } else {
                        i21 = i90;
                    }
                    if (c9 == zzbqx.MESSAGE.id() + 51 || c9 == zzbqx.GROUP.id() + 51) {
                        z = z2;
                        c = 1;
                        i22 = i70 + 1;
                        objArr[((i72 / 3) << 1) + 1] = zzaoj[i70];
                    } else {
                        if (c9 == zzbqx.ENUM.id() + 51) {
                            z = z2;
                            if ((charAt15 & 1) == 1) {
                                int i93 = i70 + 1;
                                objArr[((i72 / 3) << 1) + 1] = zzaoj[i70];
                                i22 = i93;
                                c = 1;
                            }
                        } else {
                            z = z2;
                        }
                        i22 = i70;
                        c = 1;
                    }
                    int i94 = charAt24 << c;
                    Object obj = zzaoj[i94];
                    if (obj instanceof Field) {
                        zza2 = (Field) obj;
                    } else {
                        zza2 = zza(cls2, (String) obj);
                        zzaoj[i94] = zza2;
                    }
                    i13 = i5;
                    int objectFieldOffset = (int) unsafe.objectFieldOffset(zza2);
                    int i95 = i94 + 1;
                    Object obj2 = zzaoj[i95];
                    if (obj2 instanceof Field) {
                        zza3 = (Field) obj2;
                    } else {
                        zza3 = zza(cls2, (String) obj2);
                        zzaoj[i95] = zza3;
                    }
                    i11 = i7;
                    i12 = i6;
                    i70 = i22;
                    i10 = i21;
                    i14 = objectFieldOffset;
                    i15 = (int) unsafe.objectFieldOffset(zza3);
                    i16 = 0;
                } else {
                    z = z2;
                    i13 = i5;
                    int i96 = i70 + 1;
                    Field zza4 = zza(cls2, (String) zzaoj[i70]);
                    if (c9 == zzbqx.MESSAGE.id() || c9 == zzbqx.GROUP.id()) {
                        i19 = i6;
                        objArr[((i72 / 3) << 1) + 1] = zza4.getType();
                    } else {
                        if (c9 == zzbqx.MESSAGE_LIST.id() || c9 == zzbqx.GROUP_LIST.id()) {
                            i19 = i6;
                            i20 = i96 + 1;
                            objArr[((i72 / 3) << 1) + 1] = zzaoj[i96];
                        } else if (c9 == zzbqx.ENUM.id() || c9 == zzbqx.ENUM_LIST.id() || c9 == zzbqx.ENUM_LIST_PACKED.id()) {
                            i19 = i6;
                            if ((charAt15 & 1) == 1) {
                                i20 = i96 + 1;
                                objArr[((i72 / 3) << 1) + 1] = zzaoj[i96];
                            }
                        } else if (c9 == zzbqx.MAP.id()) {
                            int i97 = i69 + 1;
                            iArr[i69] = i72;
                            int i98 = (i72 / 3) << 1;
                            int i99 = i96 + 1;
                            objArr[i98] = zzaoj[i96];
                            if ((charAt23 & 2048) != 0) {
                                int i100 = i99 + 1;
                                objArr[i98 + 1] = zzaoj[i99];
                                i12 = i6;
                                i17 = i100;
                            } else {
                                i12 = i6;
                                i17 = i99;
                            }
                            i69 = i97;
                            i14 = (int) unsafe.objectFieldOffset(zza4);
                            if ((charAt15 & 1) == 1 || c9 > zzbqx.GROUP.id()) {
                                i11 = i7;
                                i10 = i9;
                                i70 = i17;
                                i16 = 0;
                                i15 = 0;
                            } else {
                                int i101 = i9 + 1;
                                char charAt25 = zzaoi.charAt(i9);
                                if (charAt25 >= 55296) {
                                    char c12 = charAt25 & 8191;
                                    int i102 = 13;
                                    while (true) {
                                        i18 = i101 + 1;
                                        charAt = zzaoi.charAt(i101);
                                        if (charAt < 55296) {
                                            break;
                                        }
                                        c12 |= (charAt & 8191) << i102;
                                        i102 += 13;
                                        i101 = i18;
                                    }
                                    charAt25 = c12 | (charAt << i102);
                                    i101 = i18;
                                }
                                int i103 = (i7 << 1) + (charAt25 / ' ');
                                Object obj3 = zzaoj[i103];
                                if (obj3 instanceof Field) {
                                    zza = (Field) obj3;
                                } else {
                                    zza = zza(cls2, (String) obj3);
                                    zzaoj[i103] = zza;
                                }
                                i11 = i7;
                                i10 = i101;
                                i15 = (int) unsafe.objectFieldOffset(zza);
                                i16 = charAt25 % ' ';
                                i70 = i17;
                            }
                        } else {
                            i19 = i6;
                        }
                        i17 = i20;
                        i14 = (int) unsafe.objectFieldOffset(zza4);
                        if ((charAt15 & 1) == 1) {
                        }
                        i11 = i7;
                        i10 = i9;
                        i70 = i17;
                        i16 = 0;
                        i15 = 0;
                    }
                    i17 = i96;
                    i14 = (int) unsafe.objectFieldOffset(zza4);
                    if ((charAt15 & 1) == 1) {
                    }
                    i11 = i7;
                    i10 = i9;
                    i70 = i17;
                    i16 = 0;
                    i15 = 0;
                }
                if (c9 >= 18 && c9 <= '1') {
                    int i104 = i73 + 1;
                    iArr[i73] = i14;
                    i73 = i104;
                }
                int i105 = i72 + 1;
                iArr2[i72] = charAt22;
                int i106 = i105 + 1;
                iArr2[i105] = (c9 << 20) | ((charAt23 & 256) != 0 ? 268435456 : 0) | ((charAt23 & 512) != 0 ? 536870912 : 0) | i14;
                i72 = i106 + 1;
                iArr2[i106] = (i16 << 20) | i15;
                length = i81;
                i68 = i86;
                i3 = i87;
                z2 = z;
                i5 = i13;
                i6 = i12;
                i7 = i11;
            }
            boolean z3 = z2;
            int i107 = i3;
            zzbsp zzbsp = new zzbsp(iArr2, objArr, i6, i5, zzbta.zzaob(), z2, false, iArr, i3, i68, zzbst, zzbrv, zzbtu, zzbqr, zzbsg);
            return zzbsp;
        }
        ((zzbtp) zzbsj2).zzanz();
        throw new NoSuchMethodError();
    }

    private static Field zza(Class<?> cls, String str) {
        try {
            return cls.getDeclaredField(str);
        } catch (NoSuchFieldException unused) {
            Field[] declaredFields = cls.getDeclaredFields();
            for (Field field : declaredFields) {
                if (str.equals(field.getName())) {
                    return field;
                }
            }
            String name = cls.getName();
            String arrays = Arrays.toString(declaredFields);
            StringBuilder sb = new StringBuilder(40 + String.valueOf(str).length() + String.valueOf(name).length() + String.valueOf(arrays).length());
            sb.append("Field ");
            sb.append(str);
            sb.append(" for ");
            sb.append(name);
            sb.append(" not found. Known fields are ");
            sb.append(arrays);
            throw new RuntimeException(sb.toString());
        }
    }

    public final T newInstance() {
        return this.zzfsu.newInstance(this.zzfsm);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x006a, code lost:
        if (com.google.android.gms.internal.ads.zzbte.zze(com.google.android.gms.internal.ads.zzbua.zzp(r10, r6), com.google.android.gms.internal.ads.zzbua.zzp(r11, r6)) != false) goto L_0x01b2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x007e, code lost:
        if (com.google.android.gms.internal.ads.zzbua.zzl(r10, r6) == com.google.android.gms.internal.ads.zzbua.zzl(r11, r6)) goto L_0x01b2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0090, code lost:
        if (com.google.android.gms.internal.ads.zzbua.zzk(r10, r6) == com.google.android.gms.internal.ads.zzbua.zzk(r11, r6)) goto L_0x01b2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x00a4, code lost:
        if (com.google.android.gms.internal.ads.zzbua.zzl(r10, r6) == com.google.android.gms.internal.ads.zzbua.zzl(r11, r6)) goto L_0x01b2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x00b6, code lost:
        if (com.google.android.gms.internal.ads.zzbua.zzk(r10, r6) == com.google.android.gms.internal.ads.zzbua.zzk(r11, r6)) goto L_0x01b2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x00c8, code lost:
        if (com.google.android.gms.internal.ads.zzbua.zzk(r10, r6) == com.google.android.gms.internal.ads.zzbua.zzk(r11, r6)) goto L_0x01b2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00da, code lost:
        if (com.google.android.gms.internal.ads.zzbua.zzk(r10, r6) == com.google.android.gms.internal.ads.zzbua.zzk(r11, r6)) goto L_0x01b2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00f0, code lost:
        if (com.google.android.gms.internal.ads.zzbte.zze(com.google.android.gms.internal.ads.zzbua.zzp(r10, r6), com.google.android.gms.internal.ads.zzbua.zzp(r11, r6)) != false) goto L_0x01b2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x0106, code lost:
        if (com.google.android.gms.internal.ads.zzbte.zze(com.google.android.gms.internal.ads.zzbua.zzp(r10, r6), com.google.android.gms.internal.ads.zzbua.zzp(r11, r6)) != false) goto L_0x01b2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x011c, code lost:
        if (com.google.android.gms.internal.ads.zzbte.zze(com.google.android.gms.internal.ads.zzbua.zzp(r10, r6), com.google.android.gms.internal.ads.zzbua.zzp(r11, r6)) != false) goto L_0x01b2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x012e, code lost:
        if (com.google.android.gms.internal.ads.zzbua.zzm(r10, r6) == com.google.android.gms.internal.ads.zzbua.zzm(r11, r6)) goto L_0x01b2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x0140, code lost:
        if (com.google.android.gms.internal.ads.zzbua.zzk(r10, r6) == com.google.android.gms.internal.ads.zzbua.zzk(r11, r6)) goto L_0x01b2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x0154, code lost:
        if (com.google.android.gms.internal.ads.zzbua.zzl(r10, r6) == com.google.android.gms.internal.ads.zzbua.zzl(r11, r6)) goto L_0x01b2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x0165, code lost:
        if (com.google.android.gms.internal.ads.zzbua.zzk(r10, r6) == com.google.android.gms.internal.ads.zzbua.zzk(r11, r6)) goto L_0x01b2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x0178, code lost:
        if (com.google.android.gms.internal.ads.zzbua.zzl(r10, r6) == com.google.android.gms.internal.ads.zzbua.zzl(r11, r6)) goto L_0x01b2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x018b, code lost:
        if (com.google.android.gms.internal.ads.zzbua.zzl(r10, r6) == com.google.android.gms.internal.ads.zzbua.zzl(r11, r6)) goto L_0x01b2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:78:0x019c, code lost:
        if (com.google.android.gms.internal.ads.zzbua.zzk(r10, r6) == com.google.android.gms.internal.ads.zzbua.zzk(r11, r6)) goto L_0x01b2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:0x01af, code lost:
        if (com.google.android.gms.internal.ads.zzbua.zzl(r10, r6) == com.google.android.gms.internal.ads.zzbua.zzl(r11, r6)) goto L_0x01b2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:0x01b1, code lost:
        r3 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0038, code lost:
        if (com.google.android.gms.internal.ads.zzbte.zze(com.google.android.gms.internal.ads.zzbua.zzp(r10, r6), com.google.android.gms.internal.ads.zzbua.zzp(r11, r6)) != false) goto L_0x01b2;
     */
    public final boolean equals(T t, T t2) {
        int length = this.zzfsi.length;
        int i = 0;
        while (true) {
            boolean z = true;
            if (i < length) {
                int zzft = zzft(i);
                long j = (long) (zzft & 1048575);
                switch ((zzft & 267386880) >>> 20) {
                    case 0:
                        if (zzc(t, t2, i)) {
                            break;
                        }
                    case 1:
                        if (zzc(t, t2, i)) {
                            break;
                        }
                    case 2:
                        if (zzc(t, t2, i)) {
                            break;
                        }
                    case 3:
                        if (zzc(t, t2, i)) {
                            break;
                        }
                    case 4:
                        if (zzc(t, t2, i)) {
                            break;
                        }
                    case 5:
                        if (zzc(t, t2, i)) {
                            break;
                        }
                    case 6:
                        if (zzc(t, t2, i)) {
                            break;
                        }
                    case 7:
                        if (zzc(t, t2, i)) {
                            break;
                        }
                    case 8:
                        if (zzc(t, t2, i)) {
                            break;
                        }
                    case 9:
                        if (zzc(t, t2, i)) {
                            break;
                        }
                    case 10:
                        if (zzc(t, t2, i)) {
                            break;
                        }
                    case 11:
                        if (zzc(t, t2, i)) {
                            break;
                        }
                    case 12:
                        if (zzc(t, t2, i)) {
                            break;
                        }
                    case 13:
                        if (zzc(t, t2, i)) {
                            break;
                        }
                    case 14:
                        if (zzc(t, t2, i)) {
                            break;
                        }
                    case 15:
                        if (zzc(t, t2, i)) {
                            break;
                        }
                    case 16:
                        if (zzc(t, t2, i)) {
                            break;
                        }
                    case 17:
                        if (zzc(t, t2, i)) {
                            break;
                        }
                    case 18:
                    case 19:
                    case 20:
                    case 21:
                    case 22:
                    case 23:
                    case 24:
                    case 25:
                    case 26:
                    case 27:
                    case 28:
                    case 29:
                    case 30:
                    case 31:
                    case 32:
                    case 33:
                    case 34:
                    case 35:
                    case 36:
                    case 37:
                    case 38:
                    case 39:
                    case 40:
                    case 41:
                    case 42:
                    case 43:
                    case 44:
                    case 45:
                    case 46:
                    case 47:
                    case 48:
                    case 49:
                        z = zzbte.zze(zzbua.zzp(t, j), zzbua.zzp(t2, j));
                        break;
                    case 50:
                        z = zzbte.zze(zzbua.zzp(t, j), zzbua.zzp(t2, j));
                        break;
                    case 51:
                    case 52:
                    case 53:
                    case 54:
                    case 55:
                    case 56:
                    case 57:
                    case 58:
                    case 59:
                    case 60:
                    case 61:
                    case 62:
                    case 63:
                    case 64:
                    case 65:
                    case 66:
                    case 67:
                    case 68:
                        long zzfu = (long) (zzfu(i) & 1048575);
                        if (zzbua.zzk(t, zzfu) == zzbua.zzk(t2, zzfu)) {
                            break;
                        }
                }
                if (!z) {
                    return false;
                }
                i += 3;
            } else if (!this.zzfsw.zzag(t).equals(this.zzfsw.zzag(t2))) {
                return false;
            } else {
                if (this.zzfsn) {
                    return this.zzfsx.zzq(t).equals(this.zzfsx.zzq(t2));
                }
                return true;
            }
        }
    }

    public final int hashCode(T t) {
        int length = this.zzfsi.length;
        int i = 0;
        for (int i2 = 0; i2 < length; i2 += 3) {
            int zzft = zzft(i2);
            int i3 = this.zzfsi[i2];
            long j = (long) (1048575 & zzft);
            int i4 = 37;
            switch ((zzft & 267386880) >>> 20) {
                case 0:
                    i = (i * 53) + zzbrf.zzbi(Double.doubleToLongBits(zzbua.zzo(t, j)));
                    break;
                case 1:
                    i = (i * 53) + Float.floatToIntBits(zzbua.zzn(t, j));
                    break;
                case 2:
                    i = (i * 53) + zzbrf.zzbi(zzbua.zzl(t, j));
                    break;
                case 3:
                    i = (i * 53) + zzbrf.zzbi(zzbua.zzl(t, j));
                    break;
                case 4:
                    i = (i * 53) + zzbua.zzk(t, j);
                    break;
                case 5:
                    i = (i * 53) + zzbrf.zzbi(zzbua.zzl(t, j));
                    break;
                case 6:
                    i = (i * 53) + zzbua.zzk(t, j);
                    break;
                case 7:
                    i = (i * 53) + zzbrf.zzbf(zzbua.zzm(t, j));
                    break;
                case 8:
                    i = (i * 53) + ((String) zzbua.zzp(t, j)).hashCode();
                    break;
                case 9:
                    Object zzp = zzbua.zzp(t, j);
                    if (zzp != null) {
                        i4 = zzp.hashCode();
                    }
                    i = (i * 53) + i4;
                    break;
                case 10:
                    i = (i * 53) + zzbua.zzp(t, j).hashCode();
                    break;
                case 11:
                    i = (i * 53) + zzbua.zzk(t, j);
                    break;
                case 12:
                    i = (i * 53) + zzbua.zzk(t, j);
                    break;
                case 13:
                    i = (i * 53) + zzbua.zzk(t, j);
                    break;
                case 14:
                    i = (i * 53) + zzbrf.zzbi(zzbua.zzl(t, j));
                    break;
                case 15:
                    i = (i * 53) + zzbua.zzk(t, j);
                    break;
                case 16:
                    i = (i * 53) + zzbrf.zzbi(zzbua.zzl(t, j));
                    break;
                case 17:
                    Object zzp2 = zzbua.zzp(t, j);
                    if (zzp2 != null) {
                        i4 = zzp2.hashCode();
                    }
                    i = (i * 53) + i4;
                    break;
                case 18:
                case 19:
                case 20:
                case 21:
                case 22:
                case 23:
                case 24:
                case 25:
                case 26:
                case 27:
                case 28:
                case 29:
                case 30:
                case 31:
                case 32:
                case 33:
                case 34:
                case 35:
                case 36:
                case 37:
                case 38:
                case 39:
                case 40:
                case 41:
                case 42:
                case 43:
                case 44:
                case 45:
                case 46:
                case 47:
                case 48:
                case 49:
                    i = (i * 53) + zzbua.zzp(t, j).hashCode();
                    break;
                case 50:
                    i = (i * 53) + zzbua.zzp(t, j).hashCode();
                    break;
                case 51:
                    if (!zza(t, i3, i2)) {
                        break;
                    } else {
                        i = (i * 53) + zzbrf.zzbi(Double.doubleToLongBits(zzf(t, j)));
                        break;
                    }
                case 52:
                    if (!zza(t, i3, i2)) {
                        break;
                    } else {
                        i = (i * 53) + Float.floatToIntBits(zzg(t, j));
                        break;
                    }
                case 53:
                    if (!zza(t, i3, i2)) {
                        break;
                    } else {
                        i = (i * 53) + zzbrf.zzbi(zzi(t, j));
                        break;
                    }
                case 54:
                    if (!zza(t, i3, i2)) {
                        break;
                    } else {
                        i = (i * 53) + zzbrf.zzbi(zzi(t, j));
                        break;
                    }
                case 55:
                    if (!zza(t, i3, i2)) {
                        break;
                    } else {
                        i = (i * 53) + zzh(t, j);
                        break;
                    }
                case 56:
                    if (!zza(t, i3, i2)) {
                        break;
                    } else {
                        i = (i * 53) + zzbrf.zzbi(zzi(t, j));
                        break;
                    }
                case 57:
                    if (!zza(t, i3, i2)) {
                        break;
                    } else {
                        i = (i * 53) + zzh(t, j);
                        break;
                    }
                case 58:
                    if (!zza(t, i3, i2)) {
                        break;
                    } else {
                        i = (i * 53) + zzbrf.zzbf(zzj(t, j));
                        break;
                    }
                case 59:
                    if (!zza(t, i3, i2)) {
                        break;
                    } else {
                        i = (i * 53) + ((String) zzbua.zzp(t, j)).hashCode();
                        break;
                    }
                case 60:
                    if (!zza(t, i3, i2)) {
                        break;
                    } else {
                        i = (i * 53) + zzbua.zzp(t, j).hashCode();
                        break;
                    }
                case 61:
                    if (!zza(t, i3, i2)) {
                        break;
                    } else {
                        i = (i * 53) + zzbua.zzp(t, j).hashCode();
                        break;
                    }
                case 62:
                    if (!zza(t, i3, i2)) {
                        break;
                    } else {
                        i = (i * 53) + zzh(t, j);
                        break;
                    }
                case 63:
                    if (!zza(t, i3, i2)) {
                        break;
                    } else {
                        i = (i * 53) + zzh(t, j);
                        break;
                    }
                case 64:
                    if (!zza(t, i3, i2)) {
                        break;
                    } else {
                        i = (i * 53) + zzh(t, j);
                        break;
                    }
                case 65:
                    if (!zza(t, i3, i2)) {
                        break;
                    } else {
                        i = (i * 53) + zzbrf.zzbi(zzi(t, j));
                        break;
                    }
                case 66:
                    if (!zza(t, i3, i2)) {
                        break;
                    } else {
                        i = (i * 53) + zzh(t, j);
                        break;
                    }
                case 67:
                    if (!zza(t, i3, i2)) {
                        break;
                    } else {
                        i = (i * 53) + zzbrf.zzbi(zzi(t, j));
                        break;
                    }
                case 68:
                    if (!zza(t, i3, i2)) {
                        break;
                    } else {
                        i = (i * 53) + zzbua.zzp(t, j).hashCode();
                        break;
                    }
            }
        }
        int hashCode = (i * 53) + this.zzfsw.zzag(t).hashCode();
        return this.zzfsn ? (hashCode * 53) + this.zzfsx.zzq(t).hashCode() : hashCode;
    }

    public final void zzd(T t, T t2) {
        if (t2 == null) {
            throw new NullPointerException();
        }
        for (int i = 0; i < this.zzfsi.length; i += 3) {
            int zzft = zzft(i);
            long j = (long) (1048575 & zzft);
            int i2 = this.zzfsi[i];
            switch ((zzft & 267386880) >>> 20) {
                case 0:
                    if (!zzd(t2, i)) {
                        break;
                    } else {
                        zzbua.zza((Object) t, j, zzbua.zzo(t2, j));
                        zze(t, i);
                        break;
                    }
                case 1:
                    if (!zzd(t2, i)) {
                        break;
                    } else {
                        zzbua.zza((Object) t, j, zzbua.zzn(t2, j));
                        zze(t, i);
                        break;
                    }
                case 2:
                    if (!zzd(t2, i)) {
                        break;
                    } else {
                        zzbua.zza((Object) t, j, zzbua.zzl(t2, j));
                        zze(t, i);
                        break;
                    }
                case 3:
                    if (!zzd(t2, i)) {
                        break;
                    } else {
                        zzbua.zza((Object) t, j, zzbua.zzl(t2, j));
                        zze(t, i);
                        break;
                    }
                case 4:
                    if (!zzd(t2, i)) {
                        break;
                    } else {
                        zzbua.zzb((Object) t, j, zzbua.zzk(t2, j));
                        zze(t, i);
                        break;
                    }
                case 5:
                    if (!zzd(t2, i)) {
                        break;
                    } else {
                        zzbua.zza((Object) t, j, zzbua.zzl(t2, j));
                        zze(t, i);
                        break;
                    }
                case 6:
                    if (!zzd(t2, i)) {
                        break;
                    } else {
                        zzbua.zzb((Object) t, j, zzbua.zzk(t2, j));
                        zze(t, i);
                        break;
                    }
                case 7:
                    if (!zzd(t2, i)) {
                        break;
                    } else {
                        zzbua.zza((Object) t, j, zzbua.zzm(t2, j));
                        zze(t, i);
                        break;
                    }
                case 8:
                    if (!zzd(t2, i)) {
                        break;
                    } else {
                        zzbua.zza((Object) t, j, zzbua.zzp(t2, j));
                        zze(t, i);
                        break;
                    }
                case 9:
                    zza(t, t2, i);
                    break;
                case 10:
                    if (!zzd(t2, i)) {
                        break;
                    } else {
                        zzbua.zza((Object) t, j, zzbua.zzp(t2, j));
                        zze(t, i);
                        break;
                    }
                case 11:
                    if (!zzd(t2, i)) {
                        break;
                    } else {
                        zzbua.zzb((Object) t, j, zzbua.zzk(t2, j));
                        zze(t, i);
                        break;
                    }
                case 12:
                    if (!zzd(t2, i)) {
                        break;
                    } else {
                        zzbua.zzb((Object) t, j, zzbua.zzk(t2, j));
                        zze(t, i);
                        break;
                    }
                case 13:
                    if (!zzd(t2, i)) {
                        break;
                    } else {
                        zzbua.zzb((Object) t, j, zzbua.zzk(t2, j));
                        zze(t, i);
                        break;
                    }
                case 14:
                    if (!zzd(t2, i)) {
                        break;
                    } else {
                        zzbua.zza((Object) t, j, zzbua.zzl(t2, j));
                        zze(t, i);
                        break;
                    }
                case 15:
                    if (!zzd(t2, i)) {
                        break;
                    } else {
                        zzbua.zzb((Object) t, j, zzbua.zzk(t2, j));
                        zze(t, i);
                        break;
                    }
                case 16:
                    if (!zzd(t2, i)) {
                        break;
                    } else {
                        zzbua.zza((Object) t, j, zzbua.zzl(t2, j));
                        zze(t, i);
                        break;
                    }
                case 17:
                    zza(t, t2, i);
                    break;
                case 18:
                case 19:
                case 20:
                case 21:
                case 22:
                case 23:
                case 24:
                case 25:
                case 26:
                case 27:
                case 28:
                case 29:
                case 30:
                case 31:
                case 32:
                case 33:
                case 34:
                case 35:
                case 36:
                case 37:
                case 38:
                case 39:
                case 40:
                case 41:
                case 42:
                case 43:
                case 44:
                case 45:
                case 46:
                case 47:
                case 48:
                case 49:
                    this.zzfsv.zza(t, t2, j);
                    break;
                case 50:
                    zzbte.zza(this.zzfsy, t, t2, j);
                    break;
                case 51:
                case 52:
                case 53:
                case 54:
                case 55:
                case 56:
                case 57:
                case 58:
                case 59:
                    if (!zza(t2, i2, i)) {
                        break;
                    } else {
                        zzbua.zza((Object) t, j, zzbua.zzp(t2, j));
                        zzb(t, i2, i);
                        break;
                    }
                case 60:
                    zzb(t, t2, i);
                    break;
                case 61:
                case 62:
                case 63:
                case 64:
                case 65:
                case 66:
                case 67:
                    if (!zza(t2, i2, i)) {
                        break;
                    } else {
                        zzbua.zza((Object) t, j, zzbua.zzp(t2, j));
                        zzb(t, i2, i);
                        break;
                    }
                case 68:
                    zzb(t, t2, i);
                    break;
            }
        }
        if (!this.zzfsp) {
            zzbte.zza(this.zzfsw, t, t2);
            if (this.zzfsn) {
                zzbte.zza(this.zzfsx, t, t2);
            }
        }
    }

    private final void zza(T t, T t2, int i) {
        long zzft = (long) (zzft(i) & 1048575);
        if (zzd(t2, i)) {
            Object zzp = zzbua.zzp(t, zzft);
            Object zzp2 = zzbua.zzp(t2, zzft);
            if (zzp == null || zzp2 == null) {
                if (zzp2 != null) {
                    zzbua.zza((Object) t, zzft, zzp2);
                    zze(t, i);
                }
                return;
            }
            zzbua.zza((Object) t, zzft, zzbrf.zzb(zzp, zzp2));
            zze(t, i);
        }
    }

    private final void zzb(T t, T t2, int i) {
        int zzft = zzft(i);
        int i2 = this.zzfsi[i];
        long j = (long) (zzft & 1048575);
        if (zza(t2, i2, i)) {
            Object zzp = zzbua.zzp(t, j);
            Object zzp2 = zzbua.zzp(t2, j);
            if (zzp == null || zzp2 == null) {
                if (zzp2 != null) {
                    zzbua.zza((Object) t, j, zzp2);
                    zzb(t, i2, i);
                }
                return;
            }
            zzbua.zza((Object) t, j, zzbrf.zzb(zzp, zzp2));
            zzb(t, i2, i);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:410:0x09bd, code lost:
        r6 = r11;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:413:0x09cc, code lost:
        r9 = false;
        r18 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:473:0x0af0, code lost:
        r3 = r3 + 3;
        r11 = r6;
        r6 = r9;
        r9 = r18;
     */
    public final int zzac(T t) {
        int i;
        int i2;
        long j;
        boolean z;
        boolean z2;
        boolean z3;
        T t2 = t;
        int i3 = 267386880;
        if (this.zzfsp) {
            Unsafe unsafe = zzfsh;
            int i4 = 0;
            int i5 = 0;
            while (i4 < this.zzfsi.length) {
                int zzft = zzft(i4);
                int i6 = (zzft & i3) >>> 20;
                int i7 = this.zzfsi[i4];
                long j2 = (long) (zzft & 1048575);
                int i8 = (i6 < zzbqx.DOUBLE_LIST_PACKED.id() || i6 > zzbqx.SINT64_LIST_PACKED.id()) ? 0 : this.zzfsi[i4 + 2] & 1048575;
                switch (i6) {
                    case 0:
                        if (!zzd(t2, i4)) {
                            break;
                        } else {
                            i5 += zzbqk.zzc(i7, (double) Utils.DOUBLE_EPSILON);
                            break;
                        }
                    case 1:
                        if (!zzd(t2, i4)) {
                            break;
                        } else {
                            i5 += zzbqk.zzb(i7, 0.0f);
                            break;
                        }
                    case 2:
                        if (!zzd(t2, i4)) {
                            break;
                        } else {
                            i5 += zzbqk.zzm(i7, zzbua.zzl(t2, j2));
                            break;
                        }
                    case 3:
                        if (!zzd(t2, i4)) {
                            break;
                        } else {
                            i5 += zzbqk.zzn(i7, zzbua.zzl(t2, j2));
                            break;
                        }
                    case 4:
                        if (!zzd(t2, i4)) {
                            break;
                        } else {
                            i5 += zzbqk.zzz(i7, zzbua.zzk(t2, j2));
                            break;
                        }
                    case 5:
                        if (!zzd(t2, i4)) {
                            break;
                        } else {
                            i5 += zzbqk.zzp(i7, 0);
                            break;
                        }
                    case 6:
                        if (!zzd(t2, i4)) {
                            break;
                        } else {
                            i5 += zzbqk.zzac(i7, 0);
                            break;
                        }
                    case 7:
                        if (!zzd(t2, i4)) {
                            break;
                        } else {
                            i5 += zzbqk.zzk(i7, true);
                            break;
                        }
                    case 8:
                        if (!zzd(t2, i4)) {
                            break;
                        } else {
                            Object zzp = zzbua.zzp(t2, j2);
                            if (!(zzp instanceof zzbpu)) {
                                i5 += zzbqk.zzg(i7, (String) zzp);
                                break;
                            } else {
                                i5 += zzbqk.zzc(i7, (zzbpu) zzp);
                                break;
                            }
                        }
                    case 9:
                        if (!zzd(t2, i4)) {
                            break;
                        } else {
                            i5 += zzbte.zzc(i7, zzbua.zzp(t2, j2), zzfq(i4));
                            break;
                        }
                    case 10:
                        if (!zzd(t2, i4)) {
                            break;
                        } else {
                            i5 += zzbqk.zzc(i7, (zzbpu) zzbua.zzp(t2, j2));
                            break;
                        }
                    case 11:
                        if (!zzd(t2, i4)) {
                            break;
                        } else {
                            i5 += zzbqk.zzaa(i7, zzbua.zzk(t2, j2));
                            break;
                        }
                    case 12:
                        if (!zzd(t2, i4)) {
                            break;
                        } else {
                            i5 += zzbqk.zzae(i7, zzbua.zzk(t2, j2));
                            break;
                        }
                    case 13:
                        if (!zzd(t2, i4)) {
                            break;
                        } else {
                            i5 += zzbqk.zzad(i7, 0);
                            break;
                        }
                    case 14:
                        if (!zzd(t2, i4)) {
                            break;
                        } else {
                            i5 += zzbqk.zzq(i7, 0);
                            break;
                        }
                    case 15:
                        if (!zzd(t2, i4)) {
                            break;
                        } else {
                            i5 += zzbqk.zzab(i7, zzbua.zzk(t2, j2));
                            break;
                        }
                    case 16:
                        if (!zzd(t2, i4)) {
                            break;
                        } else {
                            i5 += zzbqk.zzo(i7, zzbua.zzl(t2, j2));
                            break;
                        }
                    case 17:
                        if (!zzd(t2, i4)) {
                            break;
                        } else {
                            i5 += zzbqk.zzc(i7, (zzbsl) zzbua.zzp(t2, j2), zzfq(i4));
                            break;
                        }
                    case 18:
                        i5 += zzbte.zzw(i7, zze((Object) t2, j2), false);
                        break;
                    case 19:
                        i5 += zzbte.zzv(i7, zze((Object) t2, j2), false);
                        break;
                    case 20:
                        i5 += zzbte.zzo(i7, zze((Object) t2, j2), false);
                        break;
                    case 21:
                        i5 += zzbte.zzp(i7, zze((Object) t2, j2), false);
                        break;
                    case 22:
                        i5 += zzbte.zzs(i7, zze((Object) t2, j2), false);
                        break;
                    case 23:
                        i5 += zzbte.zzw(i7, zze((Object) t2, j2), false);
                        break;
                    case 24:
                        i5 += zzbte.zzv(i7, zze((Object) t2, j2), false);
                        break;
                    case 25:
                        i5 += zzbte.zzx(i7, zze((Object) t2, j2), false);
                        break;
                    case 26:
                        i5 += zzbte.zzc(i7, zze((Object) t2, j2));
                        break;
                    case 27:
                        i5 += zzbte.zzc(i7, zze((Object) t2, j2), zzfq(i4));
                        break;
                    case 28:
                        i5 += zzbte.zzd(i7, zze((Object) t2, j2));
                        break;
                    case 29:
                        i5 += zzbte.zzt(i7, zze((Object) t2, j2), false);
                        break;
                    case 30:
                        i5 += zzbte.zzr(i7, zze((Object) t2, j2), false);
                        break;
                    case 31:
                        i5 += zzbte.zzv(i7, zze((Object) t2, j2), false);
                        break;
                    case 32:
                        i5 += zzbte.zzw(i7, zze((Object) t2, j2), false);
                        break;
                    case 33:
                        i5 += zzbte.zzu(i7, zze((Object) t2, j2), false);
                        break;
                    case 34:
                        i5 += zzbte.zzq(i7, zze((Object) t2, j2), false);
                        break;
                    case 35:
                        int zzan = zzbte.zzan((List) unsafe.getObject(t2, j2));
                        if (zzan > 0) {
                            if (this.zzfsq) {
                                unsafe.putInt(t2, (long) i8, zzan);
                            }
                            i5 += zzbqk.zzfd(i7) + zzbqk.zzff(zzan) + zzan;
                            break;
                        } else {
                            break;
                        }
                    case 36:
                        int zzam = zzbte.zzam((List) unsafe.getObject(t2, j2));
                        if (zzam > 0) {
                            if (this.zzfsq) {
                                unsafe.putInt(t2, (long) i8, zzam);
                            }
                            i5 += zzbqk.zzfd(i7) + zzbqk.zzff(zzam) + zzam;
                            break;
                        } else {
                            break;
                        }
                    case 37:
                        int zzaf = zzbte.zzaf((List) unsafe.getObject(t2, j2));
                        if (zzaf > 0) {
                            if (this.zzfsq) {
                                unsafe.putInt(t2, (long) i8, zzaf);
                            }
                            i5 += zzbqk.zzfd(i7) + zzbqk.zzff(zzaf) + zzaf;
                            break;
                        } else {
                            break;
                        }
                    case 38:
                        int zzag = zzbte.zzag((List) unsafe.getObject(t2, j2));
                        if (zzag > 0) {
                            if (this.zzfsq) {
                                unsafe.putInt(t2, (long) i8, zzag);
                            }
                            i5 += zzbqk.zzfd(i7) + zzbqk.zzff(zzag) + zzag;
                            break;
                        } else {
                            break;
                        }
                    case 39:
                        int zzaj = zzbte.zzaj((List) unsafe.getObject(t2, j2));
                        if (zzaj > 0) {
                            if (this.zzfsq) {
                                unsafe.putInt(t2, (long) i8, zzaj);
                            }
                            i5 += zzbqk.zzfd(i7) + zzbqk.zzff(zzaj) + zzaj;
                            break;
                        } else {
                            break;
                        }
                    case 40:
                        int zzan2 = zzbte.zzan((List) unsafe.getObject(t2, j2));
                        if (zzan2 > 0) {
                            if (this.zzfsq) {
                                unsafe.putInt(t2, (long) i8, zzan2);
                            }
                            i5 += zzbqk.zzfd(i7) + zzbqk.zzff(zzan2) + zzan2;
                            break;
                        } else {
                            break;
                        }
                    case 41:
                        int zzam2 = zzbte.zzam((List) unsafe.getObject(t2, j2));
                        if (zzam2 > 0) {
                            if (this.zzfsq) {
                                unsafe.putInt(t2, (long) i8, zzam2);
                            }
                            i5 += zzbqk.zzfd(i7) + zzbqk.zzff(zzam2) + zzam2;
                            break;
                        } else {
                            break;
                        }
                    case 42:
                        int zzao = zzbte.zzao((List) unsafe.getObject(t2, j2));
                        if (zzao > 0) {
                            if (this.zzfsq) {
                                unsafe.putInt(t2, (long) i8, zzao);
                            }
                            i5 += zzbqk.zzfd(i7) + zzbqk.zzff(zzao) + zzao;
                            break;
                        } else {
                            break;
                        }
                    case 43:
                        int zzak = zzbte.zzak((List) unsafe.getObject(t2, j2));
                        if (zzak > 0) {
                            if (this.zzfsq) {
                                unsafe.putInt(t2, (long) i8, zzak);
                            }
                            i5 += zzbqk.zzfd(i7) + zzbqk.zzff(zzak) + zzak;
                            break;
                        } else {
                            break;
                        }
                    case 44:
                        int zzai = zzbte.zzai((List) unsafe.getObject(t2, j2));
                        if (zzai > 0) {
                            if (this.zzfsq) {
                                unsafe.putInt(t2, (long) i8, zzai);
                            }
                            i5 += zzbqk.zzfd(i7) + zzbqk.zzff(zzai) + zzai;
                            break;
                        } else {
                            break;
                        }
                    case 45:
                        int zzam3 = zzbte.zzam((List) unsafe.getObject(t2, j2));
                        if (zzam3 > 0) {
                            if (this.zzfsq) {
                                unsafe.putInt(t2, (long) i8, zzam3);
                            }
                            i5 += zzbqk.zzfd(i7) + zzbqk.zzff(zzam3) + zzam3;
                            break;
                        } else {
                            break;
                        }
                    case 46:
                        int zzan3 = zzbte.zzan((List) unsafe.getObject(t2, j2));
                        if (zzan3 > 0) {
                            if (this.zzfsq) {
                                unsafe.putInt(t2, (long) i8, zzan3);
                            }
                            i5 += zzbqk.zzfd(i7) + zzbqk.zzff(zzan3) + zzan3;
                            break;
                        } else {
                            break;
                        }
                    case 47:
                        int zzal = zzbte.zzal((List) unsafe.getObject(t2, j2));
                        if (zzal > 0) {
                            if (this.zzfsq) {
                                unsafe.putInt(t2, (long) i8, zzal);
                            }
                            i5 += zzbqk.zzfd(i7) + zzbqk.zzff(zzal) + zzal;
                            break;
                        } else {
                            break;
                        }
                    case 48:
                        int zzah = zzbte.zzah((List) unsafe.getObject(t2, j2));
                        if (zzah > 0) {
                            if (this.zzfsq) {
                                unsafe.putInt(t2, (long) i8, zzah);
                            }
                            i5 += zzbqk.zzfd(i7) + zzbqk.zzff(zzah) + zzah;
                            break;
                        } else {
                            break;
                        }
                    case 49:
                        i5 += zzbte.zzd(i7, zze((Object) t2, j2), zzfq(i4));
                        break;
                    case 50:
                        i5 += this.zzfsy.zzb(i7, zzbua.zzp(t2, j2), zzfr(i4));
                        break;
                    case 51:
                        if (!zza(t2, i7, i4)) {
                            break;
                        } else {
                            i5 += zzbqk.zzc(i7, (double) Utils.DOUBLE_EPSILON);
                            break;
                        }
                    case 52:
                        if (!zza(t2, i7, i4)) {
                            break;
                        } else {
                            i5 += zzbqk.zzb(i7, 0.0f);
                            break;
                        }
                    case 53:
                        if (!zza(t2, i7, i4)) {
                            break;
                        } else {
                            i5 += zzbqk.zzm(i7, zzi(t2, j2));
                            break;
                        }
                    case 54:
                        if (!zza(t2, i7, i4)) {
                            break;
                        } else {
                            i5 += zzbqk.zzn(i7, zzi(t2, j2));
                            break;
                        }
                    case 55:
                        if (!zza(t2, i7, i4)) {
                            break;
                        } else {
                            i5 += zzbqk.zzz(i7, zzh(t2, j2));
                            break;
                        }
                    case 56:
                        if (!zza(t2, i7, i4)) {
                            break;
                        } else {
                            i5 += zzbqk.zzp(i7, 0);
                            break;
                        }
                    case 57:
                        if (!zza(t2, i7, i4)) {
                            break;
                        } else {
                            i5 += zzbqk.zzac(i7, 0);
                            break;
                        }
                    case 58:
                        if (!zza(t2, i7, i4)) {
                            break;
                        } else {
                            i5 += zzbqk.zzk(i7, true);
                            break;
                        }
                    case 59:
                        if (!zza(t2, i7, i4)) {
                            break;
                        } else {
                            Object zzp2 = zzbua.zzp(t2, j2);
                            if (!(zzp2 instanceof zzbpu)) {
                                i5 += zzbqk.zzg(i7, (String) zzp2);
                                break;
                            } else {
                                i5 += zzbqk.zzc(i7, (zzbpu) zzp2);
                                break;
                            }
                        }
                    case 60:
                        if (!zza(t2, i7, i4)) {
                            break;
                        } else {
                            i5 += zzbte.zzc(i7, zzbua.zzp(t2, j2), zzfq(i4));
                            break;
                        }
                    case 61:
                        if (!zza(t2, i7, i4)) {
                            break;
                        } else {
                            i5 += zzbqk.zzc(i7, (zzbpu) zzbua.zzp(t2, j2));
                            break;
                        }
                    case 62:
                        if (!zza(t2, i7, i4)) {
                            break;
                        } else {
                            i5 += zzbqk.zzaa(i7, zzh(t2, j2));
                            break;
                        }
                    case 63:
                        if (!zza(t2, i7, i4)) {
                            break;
                        } else {
                            i5 += zzbqk.zzae(i7, zzh(t2, j2));
                            break;
                        }
                    case 64:
                        if (!zza(t2, i7, i4)) {
                            break;
                        } else {
                            i5 += zzbqk.zzad(i7, 0);
                            break;
                        }
                    case 65:
                        if (!zza(t2, i7, i4)) {
                            break;
                        } else {
                            i5 += zzbqk.zzq(i7, 0);
                            break;
                        }
                    case 66:
                        if (!zza(t2, i7, i4)) {
                            break;
                        } else {
                            i5 += zzbqk.zzab(i7, zzh(t2, j2));
                            break;
                        }
                    case 67:
                        if (!zza(t2, i7, i4)) {
                            break;
                        } else {
                            i5 += zzbqk.zzo(i7, zzi(t2, j2));
                            break;
                        }
                    case 68:
                        if (!zza(t2, i7, i4)) {
                            break;
                        } else {
                            i5 += zzbqk.zzc(i7, (zzbsl) zzbua.zzp(t2, j2), zzfq(i4));
                            break;
                        }
                }
                i4 += 3;
                i3 = 267386880;
            }
            return i5 + zza(this.zzfsw, t2);
        }
        Unsafe unsafe2 = zzfsh;
        int i9 = -1;
        int i10 = 0;
        int i11 = 0;
        int i12 = 0;
        while (i10 < this.zzfsi.length) {
            int zzft2 = zzft(i10);
            int i13 = this.zzfsi[i10];
            int i14 = (zzft2 & 267386880) >>> 20;
            if (i14 <= 17) {
                i2 = this.zzfsi[i10 + 2];
                int i15 = i2 & 1048575;
                i = 1 << (i2 >>> 20);
                if (i15 != i9) {
                    i12 = unsafe2.getInt(t2, (long) i15);
                    i9 = i15;
                }
            } else {
                i2 = (!this.zzfsq || i14 < zzbqx.DOUBLE_LIST_PACKED.id() || i14 > zzbqx.SINT64_LIST_PACKED.id()) ? 0 : this.zzfsi[i10 + 2] & 1048575;
                i = 0;
            }
            long j3 = (long) (zzft2 & 1048575);
            switch (i14) {
                case 0:
                    z2 = false;
                    z = false;
                    j = 0;
                    if ((i12 & i) != 0) {
                        i11 += zzbqk.zzc(i13, (double) Utils.DOUBLE_EPSILON);
                        break;
                    }
                case 1:
                    z2 = false;
                    j = 0;
                    if ((i12 & i) != 0) {
                        z = false;
                        i11 += zzbqk.zzb(i13, 0.0f);
                        break;
                    }
                case 2:
                    z2 = false;
                    j = 0;
                    if ((i12 & i) != 0) {
                        i11 += zzbqk.zzm(i13, unsafe2.getLong(t2, j3));
                    }
                    z = false;
                    break;
                case 3:
                    z2 = false;
                    j = 0;
                    if ((i12 & i) != 0) {
                        i11 += zzbqk.zzn(i13, unsafe2.getLong(t2, j3));
                    }
                    z = false;
                    break;
                case 4:
                    z2 = false;
                    j = 0;
                    if ((i12 & i) != 0) {
                        i11 += zzbqk.zzz(i13, unsafe2.getInt(t2, j3));
                    }
                    z = false;
                    break;
                case 5:
                    z2 = false;
                    if ((i12 & i) != 0) {
                        i11 += zzbqk.zzp(i13, 0);
                        j = 0;
                        z = false;
                        break;
                    }
                    break;
                case 6:
                    if ((i12 & i) != 0) {
                        z2 = false;
                        i11 += zzbqk.zzac(i13, 0);
                        break;
                    }
                case 7:
                    if ((i12 & i) != 0) {
                        i11 += zzbqk.zzk(i13, true);
                    }
                    z2 = false;
                    break;
                case 8:
                    if ((i12 & i) != 0) {
                        Object object = unsafe2.getObject(t2, j3);
                        i11 = object instanceof zzbpu ? i11 + zzbqk.zzc(i13, (zzbpu) object) : i11 + zzbqk.zzg(i13, (String) object);
                    }
                    z2 = false;
                    break;
                case 9:
                    if ((i12 & i) != 0) {
                        i11 += zzbte.zzc(i13, unsafe2.getObject(t2, j3), zzfq(i10));
                    }
                    z2 = false;
                    break;
                case 10:
                    if ((i12 & i) != 0) {
                        i11 += zzbqk.zzc(i13, (zzbpu) unsafe2.getObject(t2, j3));
                    }
                    z2 = false;
                    break;
                case 11:
                    if ((i12 & i) != 0) {
                        i11 += zzbqk.zzaa(i13, unsafe2.getInt(t2, j3));
                    }
                    z2 = false;
                    break;
                case 12:
                    if ((i12 & i) != 0) {
                        i11 += zzbqk.zzae(i13, unsafe2.getInt(t2, j3));
                    }
                    z2 = false;
                    break;
                case 13:
                    if ((i12 & i) != 0) {
                        i11 += zzbqk.zzad(i13, 0);
                    }
                    z2 = false;
                    break;
                case 14:
                    if ((i12 & i) != 0) {
                        i11 += zzbqk.zzq(i13, 0);
                    }
                    z2 = false;
                    break;
                case 15:
                    if ((i12 & i) != 0) {
                        i11 += zzbqk.zzab(i13, unsafe2.getInt(t2, j3));
                    }
                    z2 = false;
                    break;
                case 16:
                    if ((i12 & i) != 0) {
                        i11 += zzbqk.zzo(i13, unsafe2.getLong(t2, j3));
                    }
                    z2 = false;
                    break;
                case 17:
                    if ((i12 & i) != 0) {
                        i11 += zzbqk.zzc(i13, (zzbsl) unsafe2.getObject(t2, j3), zzfq(i10));
                    }
                    z2 = false;
                    break;
                case 18:
                    i11 += zzbte.zzw(i13, (List) unsafe2.getObject(t2, j3), false);
                    z2 = false;
                    break;
                case 19:
                    z3 = false;
                    i11 += zzbte.zzv(i13, (List) unsafe2.getObject(t2, j3), false);
                    break;
                case 20:
                    z3 = false;
                    i11 += zzbte.zzo(i13, (List) unsafe2.getObject(t2, j3), false);
                    break;
                case 21:
                    z3 = false;
                    i11 += zzbte.zzp(i13, (List) unsafe2.getObject(t2, j3), false);
                    break;
                case 22:
                    z3 = false;
                    i11 += zzbte.zzs(i13, (List) unsafe2.getObject(t2, j3), false);
                    break;
                case 23:
                    z3 = false;
                    i11 += zzbte.zzw(i13, (List) unsafe2.getObject(t2, j3), false);
                    break;
                case 24:
                    z3 = false;
                    i11 += zzbte.zzv(i13, (List) unsafe2.getObject(t2, j3), false);
                    break;
                case 25:
                    z3 = false;
                    i11 += zzbte.zzx(i13, (List) unsafe2.getObject(t2, j3), false);
                    break;
                case 26:
                    i11 += zzbte.zzc(i13, (List) unsafe2.getObject(t2, j3));
                    z2 = false;
                    break;
                case 27:
                    i11 += zzbte.zzc(i13, (List) unsafe2.getObject(t2, j3), zzfq(i10));
                    z2 = false;
                    break;
                case 28:
                    i11 += zzbte.zzd(i13, (List) unsafe2.getObject(t2, j3));
                    z2 = false;
                    break;
                case 29:
                    i11 += zzbte.zzt(i13, (List) unsafe2.getObject(t2, j3), false);
                    z2 = false;
                    break;
                case 30:
                    z3 = false;
                    i11 += zzbte.zzr(i13, (List) unsafe2.getObject(t2, j3), false);
                    break;
                case 31:
                    z3 = false;
                    i11 += zzbte.zzv(i13, (List) unsafe2.getObject(t2, j3), false);
                    break;
                case 32:
                    z3 = false;
                    i11 += zzbte.zzw(i13, (List) unsafe2.getObject(t2, j3), false);
                    break;
                case 33:
                    z3 = false;
                    i11 += zzbte.zzu(i13, (List) unsafe2.getObject(t2, j3), false);
                    break;
                case 34:
                    z3 = false;
                    i11 += zzbte.zzq(i13, (List) unsafe2.getObject(t2, j3), false);
                    break;
                case 35:
                    int zzan4 = zzbte.zzan((List) unsafe2.getObject(t2, j3));
                    if (zzan4 > 0) {
                        if (this.zzfsq) {
                            unsafe2.putInt(t2, (long) i2, zzan4);
                        }
                        i11 += zzbqk.zzfd(i13) + zzbqk.zzff(zzan4) + zzan4;
                    }
                    z2 = false;
                    break;
                case 36:
                    int zzam4 = zzbte.zzam((List) unsafe2.getObject(t2, j3));
                    if (zzam4 > 0) {
                        if (this.zzfsq) {
                            unsafe2.putInt(t2, (long) i2, zzam4);
                        }
                        i11 += zzbqk.zzfd(i13) + zzbqk.zzff(zzam4) + zzam4;
                    }
                    z2 = false;
                    break;
                case 37:
                    int zzaf2 = zzbte.zzaf((List) unsafe2.getObject(t2, j3));
                    if (zzaf2 > 0) {
                        if (this.zzfsq) {
                            unsafe2.putInt(t2, (long) i2, zzaf2);
                        }
                        i11 += zzbqk.zzfd(i13) + zzbqk.zzff(zzaf2) + zzaf2;
                    }
                    z2 = false;
                    break;
                case 38:
                    int zzag2 = zzbte.zzag((List) unsafe2.getObject(t2, j3));
                    if (zzag2 > 0) {
                        if (this.zzfsq) {
                            unsafe2.putInt(t2, (long) i2, zzag2);
                        }
                        i11 += zzbqk.zzfd(i13) + zzbqk.zzff(zzag2) + zzag2;
                    }
                    z2 = false;
                    break;
                case 39:
                    int zzaj2 = zzbte.zzaj((List) unsafe2.getObject(t2, j3));
                    if (zzaj2 > 0) {
                        if (this.zzfsq) {
                            unsafe2.putInt(t2, (long) i2, zzaj2);
                        }
                        i11 += zzbqk.zzfd(i13) + zzbqk.zzff(zzaj2) + zzaj2;
                    }
                    z2 = false;
                    break;
                case 40:
                    int zzan5 = zzbte.zzan((List) unsafe2.getObject(t2, j3));
                    if (zzan5 > 0) {
                        if (this.zzfsq) {
                            unsafe2.putInt(t2, (long) i2, zzan5);
                        }
                        i11 += zzbqk.zzfd(i13) + zzbqk.zzff(zzan5) + zzan5;
                    }
                    z2 = false;
                    break;
                case 41:
                    int zzam5 = zzbte.zzam((List) unsafe2.getObject(t2, j3));
                    if (zzam5 > 0) {
                        if (this.zzfsq) {
                            unsafe2.putInt(t2, (long) i2, zzam5);
                        }
                        i11 += zzbqk.zzfd(i13) + zzbqk.zzff(zzam5) + zzam5;
                    }
                    z2 = false;
                    break;
                case 42:
                    int zzao2 = zzbte.zzao((List) unsafe2.getObject(t2, j3));
                    if (zzao2 > 0) {
                        if (this.zzfsq) {
                            unsafe2.putInt(t2, (long) i2, zzao2);
                        }
                        i11 += zzbqk.zzfd(i13) + zzbqk.zzff(zzao2) + zzao2;
                    }
                    z2 = false;
                    break;
                case 43:
                    int zzak2 = zzbte.zzak((List) unsafe2.getObject(t2, j3));
                    if (zzak2 > 0) {
                        if (this.zzfsq) {
                            unsafe2.putInt(t2, (long) i2, zzak2);
                        }
                        i11 += zzbqk.zzfd(i13) + zzbqk.zzff(zzak2) + zzak2;
                    }
                    z2 = false;
                    break;
                case 44:
                    int zzai2 = zzbte.zzai((List) unsafe2.getObject(t2, j3));
                    if (zzai2 > 0) {
                        if (this.zzfsq) {
                            unsafe2.putInt(t2, (long) i2, zzai2);
                        }
                        i11 += zzbqk.zzfd(i13) + zzbqk.zzff(zzai2) + zzai2;
                    }
                    z2 = false;
                    break;
                case 45:
                    int zzam6 = zzbte.zzam((List) unsafe2.getObject(t2, j3));
                    if (zzam6 > 0) {
                        if (this.zzfsq) {
                            unsafe2.putInt(t2, (long) i2, zzam6);
                        }
                        i11 += zzbqk.zzfd(i13) + zzbqk.zzff(zzam6) + zzam6;
                    }
                    z2 = false;
                    break;
                case 46:
                    int zzan6 = zzbte.zzan((List) unsafe2.getObject(t2, j3));
                    if (zzan6 > 0) {
                        if (this.zzfsq) {
                            unsafe2.putInt(t2, (long) i2, zzan6);
                        }
                        i11 += zzbqk.zzfd(i13) + zzbqk.zzff(zzan6) + zzan6;
                    }
                    z2 = false;
                    break;
                case 47:
                    int zzal2 = zzbte.zzal((List) unsafe2.getObject(t2, j3));
                    if (zzal2 > 0) {
                        if (this.zzfsq) {
                            unsafe2.putInt(t2, (long) i2, zzal2);
                        }
                        i11 += zzbqk.zzfd(i13) + zzbqk.zzff(zzal2) + zzal2;
                    }
                    z2 = false;
                    break;
                case 48:
                    int zzah2 = zzbte.zzah((List) unsafe2.getObject(t2, j3));
                    if (zzah2 > 0) {
                        if (this.zzfsq) {
                            unsafe2.putInt(t2, (long) i2, zzah2);
                        }
                        i11 += zzbqk.zzfd(i13) + zzbqk.zzff(zzah2) + zzah2;
                    }
                    z2 = false;
                    break;
                case 49:
                    i11 += zzbte.zzd(i13, (List) unsafe2.getObject(t2, j3), zzfq(i10));
                    z2 = false;
                    break;
                case 50:
                    i11 += this.zzfsy.zzb(i13, unsafe2.getObject(t2, j3), zzfr(i10));
                    z2 = false;
                    break;
                case 51:
                    if (zza(t2, i13, i10)) {
                        i11 += zzbqk.zzc(i13, (double) Utils.DOUBLE_EPSILON);
                    }
                    z2 = false;
                    break;
                case 52:
                    if (zza(t2, i13, i10)) {
                        i11 += zzbqk.zzb(i13, 0.0f);
                    }
                    z2 = false;
                    break;
                case 53:
                    if (zza(t2, i13, i10)) {
                        i11 += zzbqk.zzm(i13, zzi(t2, j3));
                    }
                    z2 = false;
                    break;
                case 54:
                    if (zza(t2, i13, i10)) {
                        i11 += zzbqk.zzn(i13, zzi(t2, j3));
                    }
                    z2 = false;
                    break;
                case 55:
                    if (zza(t2, i13, i10)) {
                        i11 += zzbqk.zzz(i13, zzh(t2, j3));
                    }
                    z2 = false;
                    break;
                case 56:
                    if (zza(t2, i13, i10)) {
                        i11 += zzbqk.zzp(i13, 0);
                    }
                    z2 = false;
                    break;
                case 57:
                    if (zza(t2, i13, i10)) {
                        i11 += zzbqk.zzac(i13, 0);
                    }
                    z2 = false;
                    break;
                case 58:
                    if (zza(t2, i13, i10)) {
                        i11 += zzbqk.zzk(i13, true);
                    }
                    z2 = false;
                    break;
                case 59:
                    if (zza(t2, i13, i10)) {
                        Object object2 = unsafe2.getObject(t2, j3);
                        i11 = object2 instanceof zzbpu ? i11 + zzbqk.zzc(i13, (zzbpu) object2) : i11 + zzbqk.zzg(i13, (String) object2);
                    }
                    z2 = false;
                    break;
                case 60:
                    if (zza(t2, i13, i10)) {
                        i11 += zzbte.zzc(i13, unsafe2.getObject(t2, j3), zzfq(i10));
                    }
                    z2 = false;
                    break;
                case 61:
                    if (zza(t2, i13, i10)) {
                        i11 += zzbqk.zzc(i13, (zzbpu) unsafe2.getObject(t2, j3));
                    }
                    z2 = false;
                    break;
                case 62:
                    if (zza(t2, i13, i10)) {
                        i11 += zzbqk.zzaa(i13, zzh(t2, j3));
                    }
                    z2 = false;
                    break;
                case 63:
                    if (zza(t2, i13, i10)) {
                        i11 += zzbqk.zzae(i13, zzh(t2, j3));
                    }
                    z2 = false;
                    break;
                case 64:
                    if (zza(t2, i13, i10)) {
                        i11 += zzbqk.zzad(i13, 0);
                    }
                    z2 = false;
                    break;
                case 65:
                    if (zza(t2, i13, i10)) {
                        i11 += zzbqk.zzq(i13, 0);
                    }
                    z2 = false;
                    break;
                case 66:
                    if (zza(t2, i13, i10)) {
                        i11 += zzbqk.zzab(i13, zzh(t2, j3));
                    }
                    z2 = false;
                    break;
                case 67:
                    if (zza(t2, i13, i10)) {
                        i11 += zzbqk.zzo(i13, zzi(t2, j3));
                    }
                    z2 = false;
                    break;
                case 68:
                    if (zza(t2, i13, i10)) {
                        i11 += zzbqk.zzc(i13, (zzbsl) unsafe2.getObject(t2, j3), zzfq(i10));
                    }
                    z2 = false;
                    break;
            }
        }
        int zza = i11 + zza(this.zzfsw, t2);
        if (this.zzfsn) {
            zza += this.zzfsx.zzq(t2).zzamj();
        }
        return zza;
    }

    private static <UT, UB> int zza(zzbtu<UT, UB> zzbtu, T t) {
        return zzbtu.zzac(zzbtu.zzag(t));
    }

    private static <E> List<E> zze(Object obj, long j) {
        return (List) zzbua.zzp(obj, j);
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:163:0x0511  */
    /* JADX WARNING: Removed duplicated region for block: B:178:0x054f  */
    /* JADX WARNING: Removed duplicated region for block: B:331:0x0a27  */
    public final void zza(T t, zzbup zzbup) throws IOException {
        Entry entry;
        Iterator it;
        int length;
        Entry entry2;
        int i;
        Entry entry3;
        Iterator it2;
        int length2;
        if (zzbup.zzaly() == zze.zzfqn) {
            zza(this.zzfsw, t, zzbup);
            if (this.zzfsn) {
                zzbqu zzq = this.zzfsx.zzq(t);
                if (!zzq.isEmpty()) {
                    it2 = zzq.descendingIterator();
                    entry3 = (Entry) it2.next();
                    for (length2 = this.zzfsi.length - 3; length2 >= 0; length2 -= 3) {
                        int zzft = zzft(length2);
                        int i2 = this.zzfsi[length2];
                        while (entry3 != null && this.zzfsx.zza(entry3) > i2) {
                            this.zzfsx.zza(zzbup, entry3);
                            entry3 = it2.hasNext() ? (Entry) it2.next() : null;
                        }
                        switch ((zzft & 267386880) >>> 20) {
                            case 0:
                                if (!zzd(t, length2)) {
                                    break;
                                } else {
                                    zzbup.zzb(i2, zzbua.zzo(t, (long) (zzft & 1048575)));
                                    break;
                                }
                            case 1:
                                if (!zzd(t, length2)) {
                                    break;
                                } else {
                                    zzbup.zza(i2, zzbua.zzn(t, (long) (zzft & 1048575)));
                                    break;
                                }
                            case 2:
                                if (!zzd(t, length2)) {
                                    break;
                                } else {
                                    zzbup.zzr(i2, zzbua.zzl(t, (long) (zzft & 1048575)));
                                    break;
                                }
                            case 3:
                                if (!zzd(t, length2)) {
                                    break;
                                } else {
                                    zzbup.zzj(i2, zzbua.zzl(t, (long) (zzft & 1048575)));
                                    break;
                                }
                            case 4:
                                if (!zzd(t, length2)) {
                                    break;
                                } else {
                                    zzbup.zzv(i2, zzbua.zzk(t, (long) (zzft & 1048575)));
                                    break;
                                }
                            case 5:
                                if (!zzd(t, length2)) {
                                    break;
                                } else {
                                    zzbup.zzl(i2, zzbua.zzl(t, (long) (zzft & 1048575)));
                                    break;
                                }
                            case 6:
                                if (!zzd(t, length2)) {
                                    break;
                                } else {
                                    zzbup.zzy(i2, zzbua.zzk(t, (long) (zzft & 1048575)));
                                    break;
                                }
                            case 7:
                                if (!zzd(t, length2)) {
                                    break;
                                } else {
                                    zzbup.zzj(i2, zzbua.zzm(t, (long) (zzft & 1048575)));
                                    break;
                                }
                            case 8:
                                if (!zzd(t, length2)) {
                                    break;
                                } else {
                                    zza(i2, zzbua.zzp(t, (long) (zzft & 1048575)), zzbup);
                                    break;
                                }
                            case 9:
                                if (!zzd(t, length2)) {
                                    break;
                                } else {
                                    zzbup.zza(i2, zzbua.zzp(t, (long) (zzft & 1048575)), zzfq(length2));
                                    break;
                                }
                            case 10:
                                if (!zzd(t, length2)) {
                                    break;
                                } else {
                                    zzbup.zza(i2, (zzbpu) zzbua.zzp(t, (long) (zzft & 1048575)));
                                    break;
                                }
                            case 11:
                                if (!zzd(t, length2)) {
                                    break;
                                } else {
                                    zzbup.zzw(i2, zzbua.zzk(t, (long) (zzft & 1048575)));
                                    break;
                                }
                            case 12:
                                if (!zzd(t, length2)) {
                                    break;
                                } else {
                                    zzbup.zzag(i2, zzbua.zzk(t, (long) (zzft & 1048575)));
                                    break;
                                }
                            case 13:
                                if (!zzd(t, length2)) {
                                    break;
                                } else {
                                    zzbup.zzaf(i2, zzbua.zzk(t, (long) (zzft & 1048575)));
                                    break;
                                }
                            case 14:
                                if (!zzd(t, length2)) {
                                    break;
                                } else {
                                    zzbup.zzs(i2, zzbua.zzl(t, (long) (zzft & 1048575)));
                                    break;
                                }
                            case 15:
                                if (!zzd(t, length2)) {
                                    break;
                                } else {
                                    zzbup.zzx(i2, zzbua.zzk(t, (long) (zzft & 1048575)));
                                    break;
                                }
                            case 16:
                                if (!zzd(t, length2)) {
                                    break;
                                } else {
                                    zzbup.zzk(i2, zzbua.zzl(t, (long) (zzft & 1048575)));
                                    break;
                                }
                            case 17:
                                if (!zzd(t, length2)) {
                                    break;
                                } else {
                                    zzbup.zzb(i2, zzbua.zzp(t, (long) (zzft & 1048575)), zzfq(length2));
                                    break;
                                }
                            case 18:
                                zzbte.zza(this.zzfsi[length2], (List) zzbua.zzp(t, (long) (zzft & 1048575)), zzbup, false);
                                break;
                            case 19:
                                zzbte.zzb(this.zzfsi[length2], (List) zzbua.zzp(t, (long) (zzft & 1048575)), zzbup, false);
                                break;
                            case 20:
                                zzbte.zzc(this.zzfsi[length2], (List) zzbua.zzp(t, (long) (zzft & 1048575)), zzbup, false);
                                break;
                            case 21:
                                zzbte.zzd(this.zzfsi[length2], (List) zzbua.zzp(t, (long) (zzft & 1048575)), zzbup, false);
                                break;
                            case 22:
                                zzbte.zzh(this.zzfsi[length2], (List) zzbua.zzp(t, (long) (zzft & 1048575)), zzbup, false);
                                break;
                            case 23:
                                zzbte.zzf(this.zzfsi[length2], (List) zzbua.zzp(t, (long) (zzft & 1048575)), zzbup, false);
                                break;
                            case 24:
                                zzbte.zzk(this.zzfsi[length2], (List) zzbua.zzp(t, (long) (zzft & 1048575)), zzbup, false);
                                break;
                            case 25:
                                zzbte.zzn(this.zzfsi[length2], (List) zzbua.zzp(t, (long) (zzft & 1048575)), zzbup, false);
                                break;
                            case 26:
                                zzbte.zza(this.zzfsi[length2], (List) zzbua.zzp(t, (long) (zzft & 1048575)), zzbup);
                                break;
                            case 27:
                                zzbte.zza(this.zzfsi[length2], (List) zzbua.zzp(t, (long) (zzft & 1048575)), zzbup, zzfq(length2));
                                break;
                            case 28:
                                zzbte.zzb(this.zzfsi[length2], (List) zzbua.zzp(t, (long) (zzft & 1048575)), zzbup);
                                break;
                            case 29:
                                zzbte.zzi(this.zzfsi[length2], (List) zzbua.zzp(t, (long) (zzft & 1048575)), zzbup, false);
                                break;
                            case 30:
                                zzbte.zzm(this.zzfsi[length2], (List) zzbua.zzp(t, (long) (zzft & 1048575)), zzbup, false);
                                break;
                            case 31:
                                zzbte.zzl(this.zzfsi[length2], (List) zzbua.zzp(t, (long) (zzft & 1048575)), zzbup, false);
                                break;
                            case 32:
                                zzbte.zzg(this.zzfsi[length2], (List) zzbua.zzp(t, (long) (zzft & 1048575)), zzbup, false);
                                break;
                            case 33:
                                zzbte.zzj(this.zzfsi[length2], (List) zzbua.zzp(t, (long) (zzft & 1048575)), zzbup, false);
                                break;
                            case 34:
                                zzbte.zze(this.zzfsi[length2], (List) zzbua.zzp(t, (long) (zzft & 1048575)), zzbup, false);
                                break;
                            case 35:
                                zzbte.zza(this.zzfsi[length2], (List) zzbua.zzp(t, (long) (zzft & 1048575)), zzbup, true);
                                break;
                            case 36:
                                zzbte.zzb(this.zzfsi[length2], (List) zzbua.zzp(t, (long) (zzft & 1048575)), zzbup, true);
                                break;
                            case 37:
                                zzbte.zzc(this.zzfsi[length2], (List) zzbua.zzp(t, (long) (zzft & 1048575)), zzbup, true);
                                break;
                            case 38:
                                zzbte.zzd(this.zzfsi[length2], (List) zzbua.zzp(t, (long) (zzft & 1048575)), zzbup, true);
                                break;
                            case 39:
                                zzbte.zzh(this.zzfsi[length2], (List) zzbua.zzp(t, (long) (zzft & 1048575)), zzbup, true);
                                break;
                            case 40:
                                zzbte.zzf(this.zzfsi[length2], (List) zzbua.zzp(t, (long) (zzft & 1048575)), zzbup, true);
                                break;
                            case 41:
                                zzbte.zzk(this.zzfsi[length2], (List) zzbua.zzp(t, (long) (zzft & 1048575)), zzbup, true);
                                break;
                            case 42:
                                zzbte.zzn(this.zzfsi[length2], (List) zzbua.zzp(t, (long) (zzft & 1048575)), zzbup, true);
                                break;
                            case 43:
                                zzbte.zzi(this.zzfsi[length2], (List) zzbua.zzp(t, (long) (zzft & 1048575)), zzbup, true);
                                break;
                            case 44:
                                zzbte.zzm(this.zzfsi[length2], (List) zzbua.zzp(t, (long) (zzft & 1048575)), zzbup, true);
                                break;
                            case 45:
                                zzbte.zzl(this.zzfsi[length2], (List) zzbua.zzp(t, (long) (zzft & 1048575)), zzbup, true);
                                break;
                            case 46:
                                zzbte.zzg(this.zzfsi[length2], (List) zzbua.zzp(t, (long) (zzft & 1048575)), zzbup, true);
                                break;
                            case 47:
                                zzbte.zzj(this.zzfsi[length2], (List) zzbua.zzp(t, (long) (zzft & 1048575)), zzbup, true);
                                break;
                            case 48:
                                zzbte.zze(this.zzfsi[length2], (List) zzbua.zzp(t, (long) (zzft & 1048575)), zzbup, true);
                                break;
                            case 49:
                                zzbte.zzb(this.zzfsi[length2], (List) zzbua.zzp(t, (long) (zzft & 1048575)), zzbup, zzfq(length2));
                                break;
                            case 50:
                                zza(zzbup, i2, zzbua.zzp(t, (long) (zzft & 1048575)), length2);
                                break;
                            case 51:
                                if (!zza(t, i2, length2)) {
                                    break;
                                } else {
                                    zzbup.zzb(i2, zzf(t, (long) (zzft & 1048575)));
                                    break;
                                }
                            case 52:
                                if (!zza(t, i2, length2)) {
                                    break;
                                } else {
                                    zzbup.zza(i2, zzg(t, (long) (zzft & 1048575)));
                                    break;
                                }
                            case 53:
                                if (!zza(t, i2, length2)) {
                                    break;
                                } else {
                                    zzbup.zzr(i2, zzi(t, (long) (zzft & 1048575)));
                                    break;
                                }
                            case 54:
                                if (!zza(t, i2, length2)) {
                                    break;
                                } else {
                                    zzbup.zzj(i2, zzi(t, (long) (zzft & 1048575)));
                                    break;
                                }
                            case 55:
                                if (!zza(t, i2, length2)) {
                                    break;
                                } else {
                                    zzbup.zzv(i2, zzh(t, (long) (zzft & 1048575)));
                                    break;
                                }
                            case 56:
                                if (!zza(t, i2, length2)) {
                                    break;
                                } else {
                                    zzbup.zzl(i2, zzi(t, (long) (zzft & 1048575)));
                                    break;
                                }
                            case 57:
                                if (!zza(t, i2, length2)) {
                                    break;
                                } else {
                                    zzbup.zzy(i2, zzh(t, (long) (zzft & 1048575)));
                                    break;
                                }
                            case 58:
                                if (!zza(t, i2, length2)) {
                                    break;
                                } else {
                                    zzbup.zzj(i2, zzj(t, (long) (zzft & 1048575)));
                                    break;
                                }
                            case 59:
                                if (!zza(t, i2, length2)) {
                                    break;
                                } else {
                                    zza(i2, zzbua.zzp(t, (long) (zzft & 1048575)), zzbup);
                                    break;
                                }
                            case 60:
                                if (!zza(t, i2, length2)) {
                                    break;
                                } else {
                                    zzbup.zza(i2, zzbua.zzp(t, (long) (zzft & 1048575)), zzfq(length2));
                                    break;
                                }
                            case 61:
                                if (!zza(t, i2, length2)) {
                                    break;
                                } else {
                                    zzbup.zza(i2, (zzbpu) zzbua.zzp(t, (long) (zzft & 1048575)));
                                    break;
                                }
                            case 62:
                                if (!zza(t, i2, length2)) {
                                    break;
                                } else {
                                    zzbup.zzw(i2, zzh(t, (long) (zzft & 1048575)));
                                    break;
                                }
                            case 63:
                                if (!zza(t, i2, length2)) {
                                    break;
                                } else {
                                    zzbup.zzag(i2, zzh(t, (long) (zzft & 1048575)));
                                    break;
                                }
                            case 64:
                                if (!zza(t, i2, length2)) {
                                    break;
                                } else {
                                    zzbup.zzaf(i2, zzh(t, (long) (zzft & 1048575)));
                                    break;
                                }
                            case 65:
                                if (!zza(t, i2, length2)) {
                                    break;
                                } else {
                                    zzbup.zzs(i2, zzi(t, (long) (zzft & 1048575)));
                                    break;
                                }
                            case 66:
                                if (!zza(t, i2, length2)) {
                                    break;
                                } else {
                                    zzbup.zzx(i2, zzh(t, (long) (zzft & 1048575)));
                                    break;
                                }
                            case 67:
                                if (!zza(t, i2, length2)) {
                                    break;
                                } else {
                                    zzbup.zzk(i2, zzi(t, (long) (zzft & 1048575)));
                                    break;
                                }
                            case 68:
                                if (!zza(t, i2, length2)) {
                                    break;
                                } else {
                                    zzbup.zzb(i2, zzbua.zzp(t, (long) (zzft & 1048575)), zzfq(length2));
                                    break;
                                }
                        }
                    }
                    while (entry3 != null) {
                        this.zzfsx.zza(zzbup, entry3);
                        entry3 = it2.hasNext() ? (Entry) it2.next() : null;
                    }
                }
            }
            it2 = null;
            entry3 = null;
            while (length2 >= 0) {
            }
            while (entry3 != null) {
            }
        } else if (this.zzfsp) {
            if (this.zzfsn) {
                zzbqu zzq2 = this.zzfsx.zzq(t);
                if (!zzq2.isEmpty()) {
                    it = zzq2.iterator();
                    entry = (Entry) it.next();
                    length = this.zzfsi.length;
                    entry2 = entry;
                    for (i = 0; i < length; i += 3) {
                        int zzft2 = zzft(i);
                        int i3 = this.zzfsi[i];
                        while (entry2 != null && this.zzfsx.zza(entry2) <= i3) {
                            this.zzfsx.zza(zzbup, entry2);
                            entry2 = it.hasNext() ? (Entry) it.next() : null;
                        }
                        switch ((zzft2 & 267386880) >>> 20) {
                            case 0:
                                if (!zzd(t, i)) {
                                    break;
                                } else {
                                    zzbup.zzb(i3, zzbua.zzo(t, (long) (zzft2 & 1048575)));
                                    break;
                                }
                            case 1:
                                if (!zzd(t, i)) {
                                    break;
                                } else {
                                    zzbup.zza(i3, zzbua.zzn(t, (long) (zzft2 & 1048575)));
                                    break;
                                }
                            case 2:
                                if (!zzd(t, i)) {
                                    break;
                                } else {
                                    zzbup.zzr(i3, zzbua.zzl(t, (long) (zzft2 & 1048575)));
                                    break;
                                }
                            case 3:
                                if (!zzd(t, i)) {
                                    break;
                                } else {
                                    zzbup.zzj(i3, zzbua.zzl(t, (long) (zzft2 & 1048575)));
                                    break;
                                }
                            case 4:
                                if (!zzd(t, i)) {
                                    break;
                                } else {
                                    zzbup.zzv(i3, zzbua.zzk(t, (long) (zzft2 & 1048575)));
                                    break;
                                }
                            case 5:
                                if (!zzd(t, i)) {
                                    break;
                                } else {
                                    zzbup.zzl(i3, zzbua.zzl(t, (long) (zzft2 & 1048575)));
                                    break;
                                }
                            case 6:
                                if (!zzd(t, i)) {
                                    break;
                                } else {
                                    zzbup.zzy(i3, zzbua.zzk(t, (long) (zzft2 & 1048575)));
                                    break;
                                }
                            case 7:
                                if (!zzd(t, i)) {
                                    break;
                                } else {
                                    zzbup.zzj(i3, zzbua.zzm(t, (long) (zzft2 & 1048575)));
                                    break;
                                }
                            case 8:
                                if (!zzd(t, i)) {
                                    break;
                                } else {
                                    zza(i3, zzbua.zzp(t, (long) (zzft2 & 1048575)), zzbup);
                                    break;
                                }
                            case 9:
                                if (!zzd(t, i)) {
                                    break;
                                } else {
                                    zzbup.zza(i3, zzbua.zzp(t, (long) (zzft2 & 1048575)), zzfq(i));
                                    break;
                                }
                            case 10:
                                if (!zzd(t, i)) {
                                    break;
                                } else {
                                    zzbup.zza(i3, (zzbpu) zzbua.zzp(t, (long) (zzft2 & 1048575)));
                                    break;
                                }
                            case 11:
                                if (!zzd(t, i)) {
                                    break;
                                } else {
                                    zzbup.zzw(i3, zzbua.zzk(t, (long) (zzft2 & 1048575)));
                                    break;
                                }
                            case 12:
                                if (!zzd(t, i)) {
                                    break;
                                } else {
                                    zzbup.zzag(i3, zzbua.zzk(t, (long) (zzft2 & 1048575)));
                                    break;
                                }
                            case 13:
                                if (!zzd(t, i)) {
                                    break;
                                } else {
                                    zzbup.zzaf(i3, zzbua.zzk(t, (long) (zzft2 & 1048575)));
                                    break;
                                }
                            case 14:
                                if (!zzd(t, i)) {
                                    break;
                                } else {
                                    zzbup.zzs(i3, zzbua.zzl(t, (long) (zzft2 & 1048575)));
                                    break;
                                }
                            case 15:
                                if (!zzd(t, i)) {
                                    break;
                                } else {
                                    zzbup.zzx(i3, zzbua.zzk(t, (long) (zzft2 & 1048575)));
                                    break;
                                }
                            case 16:
                                if (!zzd(t, i)) {
                                    break;
                                } else {
                                    zzbup.zzk(i3, zzbua.zzl(t, (long) (zzft2 & 1048575)));
                                    break;
                                }
                            case 17:
                                if (!zzd(t, i)) {
                                    break;
                                } else {
                                    zzbup.zzb(i3, zzbua.zzp(t, (long) (zzft2 & 1048575)), zzfq(i));
                                    break;
                                }
                            case 18:
                                zzbte.zza(this.zzfsi[i], (List) zzbua.zzp(t, (long) (zzft2 & 1048575)), zzbup, false);
                                break;
                            case 19:
                                zzbte.zzb(this.zzfsi[i], (List) zzbua.zzp(t, (long) (zzft2 & 1048575)), zzbup, false);
                                break;
                            case 20:
                                zzbte.zzc(this.zzfsi[i], (List) zzbua.zzp(t, (long) (zzft2 & 1048575)), zzbup, false);
                                break;
                            case 21:
                                zzbte.zzd(this.zzfsi[i], (List) zzbua.zzp(t, (long) (zzft2 & 1048575)), zzbup, false);
                                break;
                            case 22:
                                zzbte.zzh(this.zzfsi[i], (List) zzbua.zzp(t, (long) (zzft2 & 1048575)), zzbup, false);
                                break;
                            case 23:
                                zzbte.zzf(this.zzfsi[i], (List) zzbua.zzp(t, (long) (zzft2 & 1048575)), zzbup, false);
                                break;
                            case 24:
                                zzbte.zzk(this.zzfsi[i], (List) zzbua.zzp(t, (long) (zzft2 & 1048575)), zzbup, false);
                                break;
                            case 25:
                                zzbte.zzn(this.zzfsi[i], (List) zzbua.zzp(t, (long) (zzft2 & 1048575)), zzbup, false);
                                break;
                            case 26:
                                zzbte.zza(this.zzfsi[i], (List) zzbua.zzp(t, (long) (zzft2 & 1048575)), zzbup);
                                break;
                            case 27:
                                zzbte.zza(this.zzfsi[i], (List) zzbua.zzp(t, (long) (zzft2 & 1048575)), zzbup, zzfq(i));
                                break;
                            case 28:
                                zzbte.zzb(this.zzfsi[i], (List) zzbua.zzp(t, (long) (zzft2 & 1048575)), zzbup);
                                break;
                            case 29:
                                zzbte.zzi(this.zzfsi[i], (List) zzbua.zzp(t, (long) (zzft2 & 1048575)), zzbup, false);
                                break;
                            case 30:
                                zzbte.zzm(this.zzfsi[i], (List) zzbua.zzp(t, (long) (zzft2 & 1048575)), zzbup, false);
                                break;
                            case 31:
                                zzbte.zzl(this.zzfsi[i], (List) zzbua.zzp(t, (long) (zzft2 & 1048575)), zzbup, false);
                                break;
                            case 32:
                                zzbte.zzg(this.zzfsi[i], (List) zzbua.zzp(t, (long) (zzft2 & 1048575)), zzbup, false);
                                break;
                            case 33:
                                zzbte.zzj(this.zzfsi[i], (List) zzbua.zzp(t, (long) (zzft2 & 1048575)), zzbup, false);
                                break;
                            case 34:
                                zzbte.zze(this.zzfsi[i], (List) zzbua.zzp(t, (long) (zzft2 & 1048575)), zzbup, false);
                                break;
                            case 35:
                                zzbte.zza(this.zzfsi[i], (List) zzbua.zzp(t, (long) (zzft2 & 1048575)), zzbup, true);
                                break;
                            case 36:
                                zzbte.zzb(this.zzfsi[i], (List) zzbua.zzp(t, (long) (zzft2 & 1048575)), zzbup, true);
                                break;
                            case 37:
                                zzbte.zzc(this.zzfsi[i], (List) zzbua.zzp(t, (long) (zzft2 & 1048575)), zzbup, true);
                                break;
                            case 38:
                                zzbte.zzd(this.zzfsi[i], (List) zzbua.zzp(t, (long) (zzft2 & 1048575)), zzbup, true);
                                break;
                            case 39:
                                zzbte.zzh(this.zzfsi[i], (List) zzbua.zzp(t, (long) (zzft2 & 1048575)), zzbup, true);
                                break;
                            case 40:
                                zzbte.zzf(this.zzfsi[i], (List) zzbua.zzp(t, (long) (zzft2 & 1048575)), zzbup, true);
                                break;
                            case 41:
                                zzbte.zzk(this.zzfsi[i], (List) zzbua.zzp(t, (long) (zzft2 & 1048575)), zzbup, true);
                                break;
                            case 42:
                                zzbte.zzn(this.zzfsi[i], (List) zzbua.zzp(t, (long) (zzft2 & 1048575)), zzbup, true);
                                break;
                            case 43:
                                zzbte.zzi(this.zzfsi[i], (List) zzbua.zzp(t, (long) (zzft2 & 1048575)), zzbup, true);
                                break;
                            case 44:
                                zzbte.zzm(this.zzfsi[i], (List) zzbua.zzp(t, (long) (zzft2 & 1048575)), zzbup, true);
                                break;
                            case 45:
                                zzbte.zzl(this.zzfsi[i], (List) zzbua.zzp(t, (long) (zzft2 & 1048575)), zzbup, true);
                                break;
                            case 46:
                                zzbte.zzg(this.zzfsi[i], (List) zzbua.zzp(t, (long) (zzft2 & 1048575)), zzbup, true);
                                break;
                            case 47:
                                zzbte.zzj(this.zzfsi[i], (List) zzbua.zzp(t, (long) (zzft2 & 1048575)), zzbup, true);
                                break;
                            case 48:
                                zzbte.zze(this.zzfsi[i], (List) zzbua.zzp(t, (long) (zzft2 & 1048575)), zzbup, true);
                                break;
                            case 49:
                                zzbte.zzb(this.zzfsi[i], (List) zzbua.zzp(t, (long) (zzft2 & 1048575)), zzbup, zzfq(i));
                                break;
                            case 50:
                                zza(zzbup, i3, zzbua.zzp(t, (long) (zzft2 & 1048575)), i);
                                break;
                            case 51:
                                if (!zza(t, i3, i)) {
                                    break;
                                } else {
                                    zzbup.zzb(i3, zzf(t, (long) (zzft2 & 1048575)));
                                    break;
                                }
                            case 52:
                                if (!zza(t, i3, i)) {
                                    break;
                                } else {
                                    zzbup.zza(i3, zzg(t, (long) (zzft2 & 1048575)));
                                    break;
                                }
                            case 53:
                                if (!zza(t, i3, i)) {
                                    break;
                                } else {
                                    zzbup.zzr(i3, zzi(t, (long) (zzft2 & 1048575)));
                                    break;
                                }
                            case 54:
                                if (!zza(t, i3, i)) {
                                    break;
                                } else {
                                    zzbup.zzj(i3, zzi(t, (long) (zzft2 & 1048575)));
                                    break;
                                }
                            case 55:
                                if (!zza(t, i3, i)) {
                                    break;
                                } else {
                                    zzbup.zzv(i3, zzh(t, (long) (zzft2 & 1048575)));
                                    break;
                                }
                            case 56:
                                if (!zza(t, i3, i)) {
                                    break;
                                } else {
                                    zzbup.zzl(i3, zzi(t, (long) (zzft2 & 1048575)));
                                    break;
                                }
                            case 57:
                                if (!zza(t, i3, i)) {
                                    break;
                                } else {
                                    zzbup.zzy(i3, zzh(t, (long) (zzft2 & 1048575)));
                                    break;
                                }
                            case 58:
                                if (!zza(t, i3, i)) {
                                    break;
                                } else {
                                    zzbup.zzj(i3, zzj(t, (long) (zzft2 & 1048575)));
                                    break;
                                }
                            case 59:
                                if (!zza(t, i3, i)) {
                                    break;
                                } else {
                                    zza(i3, zzbua.zzp(t, (long) (zzft2 & 1048575)), zzbup);
                                    break;
                                }
                            case 60:
                                if (!zza(t, i3, i)) {
                                    break;
                                } else {
                                    zzbup.zza(i3, zzbua.zzp(t, (long) (zzft2 & 1048575)), zzfq(i));
                                    break;
                                }
                            case 61:
                                if (!zza(t, i3, i)) {
                                    break;
                                } else {
                                    zzbup.zza(i3, (zzbpu) zzbua.zzp(t, (long) (zzft2 & 1048575)));
                                    break;
                                }
                            case 62:
                                if (!zza(t, i3, i)) {
                                    break;
                                } else {
                                    zzbup.zzw(i3, zzh(t, (long) (zzft2 & 1048575)));
                                    break;
                                }
                            case 63:
                                if (!zza(t, i3, i)) {
                                    break;
                                } else {
                                    zzbup.zzag(i3, zzh(t, (long) (zzft2 & 1048575)));
                                    break;
                                }
                            case 64:
                                if (!zza(t, i3, i)) {
                                    break;
                                } else {
                                    zzbup.zzaf(i3, zzh(t, (long) (zzft2 & 1048575)));
                                    break;
                                }
                            case 65:
                                if (!zza(t, i3, i)) {
                                    break;
                                } else {
                                    zzbup.zzs(i3, zzi(t, (long) (zzft2 & 1048575)));
                                    break;
                                }
                            case 66:
                                if (!zza(t, i3, i)) {
                                    break;
                                } else {
                                    zzbup.zzx(i3, zzh(t, (long) (zzft2 & 1048575)));
                                    break;
                                }
                            case 67:
                                if (!zza(t, i3, i)) {
                                    break;
                                } else {
                                    zzbup.zzk(i3, zzi(t, (long) (zzft2 & 1048575)));
                                    break;
                                }
                            case 68:
                                if (!zza(t, i3, i)) {
                                    break;
                                } else {
                                    zzbup.zzb(i3, zzbua.zzp(t, (long) (zzft2 & 1048575)), zzfq(i));
                                    break;
                                }
                        }
                    }
                    while (entry2 != null) {
                        this.zzfsx.zza(zzbup, entry2);
                        entry2 = it.hasNext() ? (Entry) it.next() : null;
                    }
                    zza(this.zzfsw, t, zzbup);
                }
            }
            it = null;
            entry = null;
            length = this.zzfsi.length;
            entry2 = entry;
            while (i < length) {
            }
            while (entry2 != null) {
            }
            zza(this.zzfsw, t, zzbup);
        } else {
            zzb(t, zzbup);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:105:0x0344, code lost:
        r14 = r13;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:171:0x051d, code lost:
        r5 = r12 + 3;
     */
    /* JADX WARNING: Removed duplicated region for block: B:173:0x0523  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002e  */
    private final void zzb(T t, zzbup zzbup) throws IOException {
        Entry entry;
        Iterator it;
        int length;
        Entry entry2;
        int i;
        int i2;
        int i3;
        int i4;
        int i5;
        boolean z;
        T t2 = t;
        zzbup zzbup2 = zzbup;
        if (this.zzfsn) {
            zzbqu zzq = this.zzfsx.zzq(t2);
            if (!zzq.isEmpty()) {
                it = zzq.iterator();
                entry = (Entry) it.next();
                int i6 = -1;
                length = this.zzfsi.length;
                Unsafe unsafe = zzfsh;
                entry2 = entry;
                i = 0;
                int i7 = 0;
                while (i < length) {
                    int zzft = zzft(i);
                    int i8 = this.zzfsi[i];
                    int i9 = (267386880 & zzft) >>> 20;
                    if (this.zzfsp || i9 > 17) {
                        i2 = i;
                        i3 = 0;
                    } else {
                        int i10 = this.zzfsi[i + 2];
                        int i11 = i10 & 1048575;
                        if (i11 != i6) {
                            i2 = i;
                            i7 = unsafe.getInt(t2, (long) i11);
                            i6 = i11;
                        } else {
                            i2 = i;
                        }
                        i3 = 1 << (i10 >>> 20);
                    }
                    while (entry2 != null && this.zzfsx.zza(entry2) <= i8) {
                        this.zzfsx.zza(zzbup2, entry2);
                        entry2 = it.hasNext() ? (Entry) it.next() : null;
                    }
                    long j = (long) (zzft & 1048575);
                    switch (i9) {
                        case 0:
                            i4 = i2;
                            if ((i3 & i7) == 0) {
                                break;
                            } else {
                                zzbup2.zzb(i8, zzbua.zzo(t2, j));
                                break;
                            }
                        case 1:
                            i4 = i2;
                            if ((i3 & i7) == 0) {
                                break;
                            } else {
                                zzbup2.zza(i8, zzbua.zzn(t2, j));
                                break;
                            }
                        case 2:
                            i4 = i2;
                            if ((i3 & i7) == 0) {
                                break;
                            } else {
                                zzbup2.zzr(i8, unsafe.getLong(t2, j));
                                break;
                            }
                        case 3:
                            i4 = i2;
                            if ((i3 & i7) == 0) {
                                break;
                            } else {
                                zzbup2.zzj(i8, unsafe.getLong(t2, j));
                                break;
                            }
                        case 4:
                            i4 = i2;
                            if ((i3 & i7) == 0) {
                                break;
                            } else {
                                zzbup2.zzv(i8, unsafe.getInt(t2, j));
                                break;
                            }
                        case 5:
                            i4 = i2;
                            if ((i3 & i7) == 0) {
                                break;
                            } else {
                                zzbup2.zzl(i8, unsafe.getLong(t2, j));
                                break;
                            }
                        case 6:
                            i4 = i2;
                            if ((i3 & i7) == 0) {
                                break;
                            } else {
                                zzbup2.zzy(i8, unsafe.getInt(t2, j));
                                break;
                            }
                        case 7:
                            i4 = i2;
                            if ((i3 & i7) == 0) {
                                break;
                            } else {
                                zzbup2.zzj(i8, zzbua.zzm(t2, j));
                                break;
                            }
                        case 8:
                            i4 = i2;
                            if ((i3 & i7) == 0) {
                                break;
                            } else {
                                zza(i8, unsafe.getObject(t2, j), zzbup2);
                                break;
                            }
                        case 9:
                            i4 = i2;
                            if ((i3 & i7) == 0) {
                                break;
                            } else {
                                zzbup2.zza(i8, unsafe.getObject(t2, j), zzfq(i4));
                                break;
                            }
                        case 10:
                            i4 = i2;
                            if ((i3 & i7) == 0) {
                                break;
                            } else {
                                zzbup2.zza(i8, (zzbpu) unsafe.getObject(t2, j));
                                break;
                            }
                        case 11:
                            i4 = i2;
                            if ((i3 & i7) == 0) {
                                break;
                            } else {
                                zzbup2.zzw(i8, unsafe.getInt(t2, j));
                                break;
                            }
                        case 12:
                            i4 = i2;
                            if ((i3 & i7) == 0) {
                                break;
                            } else {
                                zzbup2.zzag(i8, unsafe.getInt(t2, j));
                                break;
                            }
                        case 13:
                            i4 = i2;
                            if ((i3 & i7) == 0) {
                                break;
                            } else {
                                zzbup2.zzaf(i8, unsafe.getInt(t2, j));
                                break;
                            }
                        case 14:
                            i4 = i2;
                            if ((i3 & i7) == 0) {
                                break;
                            } else {
                                zzbup2.zzs(i8, unsafe.getLong(t2, j));
                                break;
                            }
                        case 15:
                            i4 = i2;
                            if ((i3 & i7) == 0) {
                                break;
                            } else {
                                zzbup2.zzx(i8, unsafe.getInt(t2, j));
                                break;
                            }
                        case 16:
                            i4 = i2;
                            if ((i3 & i7) == 0) {
                                break;
                            } else {
                                zzbup2.zzk(i8, unsafe.getLong(t2, j));
                                break;
                            }
                        case 17:
                            i4 = i2;
                            if ((i3 & i7) == 0) {
                                break;
                            } else {
                                zzbup2.zzb(i8, unsafe.getObject(t2, j), zzfq(i4));
                                break;
                            }
                        case 18:
                            i4 = i2;
                            zzbte.zza(this.zzfsi[i4], (List) unsafe.getObject(t2, j), zzbup2, false);
                            break;
                        case 19:
                            i4 = i2;
                            zzbte.zzb(this.zzfsi[i4], (List) unsafe.getObject(t2, j), zzbup2, false);
                            break;
                        case 20:
                            i4 = i2;
                            zzbte.zzc(this.zzfsi[i4], (List) unsafe.getObject(t2, j), zzbup2, false);
                            break;
                        case 21:
                            i4 = i2;
                            zzbte.zzd(this.zzfsi[i4], (List) unsafe.getObject(t2, j), zzbup2, false);
                            break;
                        case 22:
                            i4 = i2;
                            zzbte.zzh(this.zzfsi[i4], (List) unsafe.getObject(t2, j), zzbup2, false);
                            break;
                        case 23:
                            i4 = i2;
                            zzbte.zzf(this.zzfsi[i4], (List) unsafe.getObject(t2, j), zzbup2, false);
                            break;
                        case 24:
                            i4 = i2;
                            zzbte.zzk(this.zzfsi[i4], (List) unsafe.getObject(t2, j), zzbup2, false);
                            break;
                        case 25:
                            i4 = i2;
                            zzbte.zzn(this.zzfsi[i4], (List) unsafe.getObject(t2, j), zzbup2, false);
                            break;
                        case 26:
                            i5 = i2;
                            zzbte.zza(this.zzfsi[i5], (List) unsafe.getObject(t2, j), zzbup2);
                            break;
                        case 27:
                            i5 = i2;
                            zzbte.zza(this.zzfsi[i5], (List) unsafe.getObject(t2, j), zzbup2, zzfq(i5));
                            break;
                        case 28:
                            i5 = i2;
                            zzbte.zzb(this.zzfsi[i5], (List) unsafe.getObject(t2, j), zzbup2);
                            break;
                        case 29:
                            i4 = i2;
                            z = false;
                            zzbte.zzi(this.zzfsi[i4], (List) unsafe.getObject(t2, j), zzbup2, false);
                            break;
                        case 30:
                            i4 = i2;
                            z = false;
                            zzbte.zzm(this.zzfsi[i4], (List) unsafe.getObject(t2, j), zzbup2, false);
                            break;
                        case 31:
                            i4 = i2;
                            z = false;
                            zzbte.zzl(this.zzfsi[i4], (List) unsafe.getObject(t2, j), zzbup2, false);
                            break;
                        case 32:
                            i4 = i2;
                            z = false;
                            zzbte.zzg(this.zzfsi[i4], (List) unsafe.getObject(t2, j), zzbup2, false);
                            break;
                        case 33:
                            i4 = i2;
                            z = false;
                            zzbte.zzj(this.zzfsi[i4], (List) unsafe.getObject(t2, j), zzbup2, false);
                            break;
                        case 34:
                            i4 = i2;
                            z = false;
                            zzbte.zze(this.zzfsi[i4], (List) unsafe.getObject(t2, j), zzbup2, false);
                            break;
                        case 35:
                            i5 = i2;
                            zzbte.zza(this.zzfsi[i5], (List) unsafe.getObject(t2, j), zzbup2, true);
                            break;
                        case 36:
                            i5 = i2;
                            zzbte.zzb(this.zzfsi[i5], (List) unsafe.getObject(t2, j), zzbup2, true);
                            break;
                        case 37:
                            i5 = i2;
                            zzbte.zzc(this.zzfsi[i5], (List) unsafe.getObject(t2, j), zzbup2, true);
                            break;
                        case 38:
                            i5 = i2;
                            zzbte.zzd(this.zzfsi[i5], (List) unsafe.getObject(t2, j), zzbup2, true);
                            break;
                        case 39:
                            i5 = i2;
                            zzbte.zzh(this.zzfsi[i5], (List) unsafe.getObject(t2, j), zzbup2, true);
                            break;
                        case 40:
                            i5 = i2;
                            zzbte.zzf(this.zzfsi[i5], (List) unsafe.getObject(t2, j), zzbup2, true);
                            break;
                        case 41:
                            i5 = i2;
                            zzbte.zzk(this.zzfsi[i5], (List) unsafe.getObject(t2, j), zzbup2, true);
                            break;
                        case 42:
                            i5 = i2;
                            zzbte.zzn(this.zzfsi[i5], (List) unsafe.getObject(t2, j), zzbup2, true);
                            break;
                        case 43:
                            i5 = i2;
                            zzbte.zzi(this.zzfsi[i5], (List) unsafe.getObject(t2, j), zzbup2, true);
                            break;
                        case 44:
                            i5 = i2;
                            zzbte.zzm(this.zzfsi[i5], (List) unsafe.getObject(t2, j), zzbup2, true);
                            break;
                        case 45:
                            i5 = i2;
                            zzbte.zzl(this.zzfsi[i5], (List) unsafe.getObject(t2, j), zzbup2, true);
                            break;
                        case 46:
                            i5 = i2;
                            zzbte.zzg(this.zzfsi[i5], (List) unsafe.getObject(t2, j), zzbup2, true);
                            break;
                        case 47:
                            i5 = i2;
                            zzbte.zzj(this.zzfsi[i5], (List) unsafe.getObject(t2, j), zzbup2, true);
                            break;
                        case 48:
                            i5 = i2;
                            zzbte.zze(this.zzfsi[i5], (List) unsafe.getObject(t2, j), zzbup2, true);
                            break;
                        case 49:
                            i5 = i2;
                            zzbte.zzb(this.zzfsi[i5], (List) unsafe.getObject(t2, j), zzbup2, zzfq(i5));
                            break;
                        case 50:
                            i5 = i2;
                            zza(zzbup2, i8, unsafe.getObject(t2, j), i5);
                            break;
                        case 51:
                            i5 = i2;
                            if (zza(t2, i8, i5)) {
                                zzbup2.zzb(i8, zzf(t2, j));
                                break;
                            }
                            break;
                        case 52:
                            i5 = i2;
                            if (zza(t2, i8, i5)) {
                                zzbup2.zza(i8, zzg(t2, j));
                                break;
                            }
                            break;
                        case 53:
                            i5 = i2;
                            if (zza(t2, i8, i5)) {
                                zzbup2.zzr(i8, zzi(t2, j));
                                break;
                            }
                            break;
                        case 54:
                            i5 = i2;
                            if (zza(t2, i8, i5)) {
                                zzbup2.zzj(i8, zzi(t2, j));
                                break;
                            }
                            break;
                        case 55:
                            i5 = i2;
                            if (zza(t2, i8, i5)) {
                                zzbup2.zzv(i8, zzh(t2, j));
                                break;
                            }
                            break;
                        case 56:
                            i5 = i2;
                            if (zza(t2, i8, i5)) {
                                zzbup2.zzl(i8, zzi(t2, j));
                                break;
                            }
                            break;
                        case 57:
                            i5 = i2;
                            if (zza(t2, i8, i5)) {
                                zzbup2.zzy(i8, zzh(t2, j));
                                break;
                            }
                            break;
                        case 58:
                            i5 = i2;
                            if (zza(t2, i8, i5)) {
                                zzbup2.zzj(i8, zzj(t2, j));
                                break;
                            }
                            break;
                        case 59:
                            i5 = i2;
                            if (zza(t2, i8, i5)) {
                                zza(i8, unsafe.getObject(t2, j), zzbup2);
                                break;
                            }
                            break;
                        case 60:
                            i5 = i2;
                            if (zza(t2, i8, i5)) {
                                zzbup2.zza(i8, unsafe.getObject(t2, j), zzfq(i5));
                                break;
                            }
                            break;
                        case 61:
                            i5 = i2;
                            if (zza(t2, i8, i5)) {
                                zzbup2.zza(i8, (zzbpu) unsafe.getObject(t2, j));
                                break;
                            }
                            break;
                        case 62:
                            i5 = i2;
                            if (zza(t2, i8, i5)) {
                                zzbup2.zzw(i8, zzh(t2, j));
                                break;
                            }
                            break;
                        case 63:
                            i5 = i2;
                            if (zza(t2, i8, i5)) {
                                zzbup2.zzag(i8, zzh(t2, j));
                                break;
                            }
                            break;
                        case 64:
                            i5 = i2;
                            if (zza(t2, i8, i5)) {
                                zzbup2.zzaf(i8, zzh(t2, j));
                                break;
                            }
                            break;
                        case 65:
                            i5 = i2;
                            if (zza(t2, i8, i5)) {
                                zzbup2.zzs(i8, zzi(t2, j));
                                break;
                            }
                            break;
                        case 66:
                            i5 = i2;
                            if (zza(t2, i8, i5)) {
                                zzbup2.zzx(i8, zzh(t2, j));
                                break;
                            }
                            break;
                        case 67:
                            i5 = i2;
                            if (zza(t2, i8, i5)) {
                                zzbup2.zzk(i8, zzi(t2, j));
                                break;
                            }
                            break;
                        case 68:
                            i5 = i2;
                            if (zza(t2, i8, i5)) {
                                zzbup2.zzb(i8, unsafe.getObject(t2, j), zzfq(i5));
                                break;
                            }
                            break;
                        default:
                            i5 = i2;
                            break;
                    }
                }
                while (entry2 != null) {
                    this.zzfsx.zza(zzbup2, entry2);
                    entry2 = it.hasNext() ? (Entry) it.next() : null;
                }
                zza(this.zzfsw, t2, zzbup2);
            }
        }
        it = null;
        entry = null;
        int i62 = -1;
        length = this.zzfsi.length;
        Unsafe unsafe2 = zzfsh;
        entry2 = entry;
        i = 0;
        int i72 = 0;
        while (i < length) {
        }
        while (entry2 != null) {
        }
        zza(this.zzfsw, t2, zzbup2);
    }

    private final <K, V> void zza(zzbup zzbup, int i, Object obj, int i2) throws IOException {
        if (obj != null) {
            zzbup.zza(i, this.zzfsy.zzab(zzfr(i2)), this.zzfsy.zzx(obj));
        }
    }

    private static <UT, UB> void zza(zzbtu<UT, UB> zzbtu, T t, zzbup zzbup) throws IOException {
        zzbtu.zza(zzbtu.zzag(t), zzbup);
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(5:155|156|(1:158)|159|(6:180|161|(2:164|162)|259|(1:166)|167)(1:255)) */
    /* JADX WARNING: Code restructure failed: missing block: B:156:?, code lost:
        r7.zza(r14);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:157:0x05aa, code lost:
        if (r10 == null) goto L_0x05ac;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:158:0x05ac, code lost:
        r10 = r7.zzah(r13);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:160:0x05b5, code lost:
        if (r7.zza(r10, r14) == false) goto L_0x05b7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:161:0x05b7, code lost:
        r14 = r12.zzfss;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:163:0x05bb, code lost:
        if (r14 < r12.zzfst) goto L_0x05bd;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:164:0x05bd, code lost:
        r10 = zza((java.lang.Object) r13, r12.zzfsr[r14], (UB) r10, r7);
        r14 = r14 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:165:0x05c8, code lost:
        if (r10 != null) goto L_0x05ca;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:166:0x05ca, code lost:
        r7.zzg(r13, r10);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:167:0x05cd, code lost:
        return;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:155:0x05a7 */
    public final void zza(T t, zzbtb zzbtb, zzbqq zzbqq) throws IOException {
        Object zza;
        Object obj;
        if (zzbqq == null) {
            throw new NullPointerException();
        }
        zzbtu<?, ?> zzbtu = this.zzfsw;
        zzbqr<?> zzbqr = this.zzfsx;
        zzbqu zzbqu = null;
        Object obj2 = null;
        while (true) {
            int zzals = zzbtb.zzals();
            int zzfw = zzfw(zzals);
            if (zzfw >= 0) {
                int zzft = zzft(zzfw);
                switch ((267386880 & zzft) >>> 20) {
                    case 0:
                        zzbua.zza((Object) t, (long) (zzft & 1048575), zzbtb.readDouble());
                        zze(t, zzfw);
                        continue;
                    case 1:
                        zzbua.zza((Object) t, (long) (zzft & 1048575), zzbtb.readFloat());
                        zze(t, zzfw);
                        continue;
                    case 2:
                        zzbua.zza((Object) t, (long) (zzft & 1048575), zzbtb.zzakw());
                        zze(t, zzfw);
                        continue;
                    case 3:
                        zzbua.zza((Object) t, (long) (zzft & 1048575), zzbtb.zzakv());
                        zze(t, zzfw);
                        continue;
                    case 4:
                        zzbua.zzb((Object) t, (long) (zzft & 1048575), zzbtb.zzakx());
                        zze(t, zzfw);
                        continue;
                    case 5:
                        zzbua.zza((Object) t, (long) (zzft & 1048575), zzbtb.zzaky());
                        zze(t, zzfw);
                        continue;
                    case 6:
                        zzbua.zzb((Object) t, (long) (zzft & 1048575), zzbtb.zzakz());
                        zze(t, zzfw);
                        continue;
                    case 7:
                        zzbua.zza((Object) t, (long) (zzft & 1048575), zzbtb.zzala());
                        zze(t, zzfw);
                        continue;
                    case 8:
                        zza((Object) t, zzft, zzbtb);
                        zze(t, zzfw);
                        continue;
                    case 9:
                        if (!zzd(t, zzfw)) {
                            zzbua.zza((Object) t, (long) (zzft & 1048575), zzbtb.zza(zzfq(zzfw), zzbqq));
                            zze(t, zzfw);
                            break;
                        } else {
                            long j = (long) (zzft & 1048575);
                            zzbua.zza((Object) t, j, zzbrf.zzb(zzbua.zzp(t, j), zzbtb.zza(zzfq(zzfw), zzbqq)));
                            continue;
                        }
                    case 10:
                        zzbua.zza((Object) t, (long) (zzft & 1048575), (Object) zzbtb.zzalc());
                        zze(t, zzfw);
                        continue;
                    case 11:
                        zzbua.zzb((Object) t, (long) (zzft & 1048575), zzbtb.zzald());
                        zze(t, zzfw);
                        continue;
                    case 12:
                        int zzale = zzbtb.zzale();
                        zzbri zzfs = zzfs(zzfw);
                        if (zzfs != null) {
                            if (!zzfs.zzcb(zzale)) {
                                zza = zzbte.zza(zzals, zzale, obj2, zzbtu);
                            }
                        }
                        zzbua.zzb((Object) t, (long) (zzft & 1048575), zzale);
                        zze(t, zzfw);
                        continue;
                    case 13:
                        zzbua.zzb((Object) t, (long) (zzft & 1048575), zzbtb.zzalf());
                        zze(t, zzfw);
                        continue;
                    case 14:
                        zzbua.zza((Object) t, (long) (zzft & 1048575), zzbtb.zzalg());
                        zze(t, zzfw);
                        continue;
                    case 15:
                        zzbua.zzb((Object) t, (long) (zzft & 1048575), zzbtb.zzalh());
                        zze(t, zzfw);
                        continue;
                    case 16:
                        zzbua.zza((Object) t, (long) (zzft & 1048575), zzbtb.zzali());
                        zze(t, zzfw);
                        continue;
                    case 17:
                        if (!zzd(t, zzfw)) {
                            zzbua.zza((Object) t, (long) (zzft & 1048575), zzbtb.zzb(zzfq(zzfw), zzbqq));
                            zze(t, zzfw);
                            break;
                        } else {
                            long j2 = (long) (zzft & 1048575);
                            zzbua.zza((Object) t, j2, zzbrf.zzb(zzbua.zzp(t, j2), zzbtb.zzb(zzfq(zzfw), zzbqq)));
                            continue;
                        }
                    case 18:
                        zzbtb.zzp(this.zzfsv.zza(t, (long) (zzft & 1048575)));
                        continue;
                    case 19:
                        zzbtb.zzq(this.zzfsv.zza(t, (long) (zzft & 1048575)));
                        continue;
                    case 20:
                        zzbtb.zzs(this.zzfsv.zza(t, (long) (zzft & 1048575)));
                        continue;
                    case 21:
                        zzbtb.zzr(this.zzfsv.zza(t, (long) (zzft & 1048575)));
                        continue;
                    case 22:
                        zzbtb.zzt(this.zzfsv.zza(t, (long) (zzft & 1048575)));
                        continue;
                    case 23:
                        zzbtb.zzu(this.zzfsv.zza(t, (long) (zzft & 1048575)));
                        continue;
                    case 24:
                        zzbtb.zzv(this.zzfsv.zza(t, (long) (zzft & 1048575)));
                        continue;
                    case 25:
                        zzbtb.zzw(this.zzfsv.zza(t, (long) (zzft & 1048575)));
                        continue;
                    case 26:
                        if (!zzfv(zzft)) {
                            zzbtb.readStringList(this.zzfsv.zza(t, (long) (zzft & 1048575)));
                            break;
                        } else {
                            zzbtb.zzx(this.zzfsv.zza(t, (long) (zzft & 1048575)));
                            continue;
                        }
                    case 27:
                        zzbtb.zza(this.zzfsv.zza(t, (long) (zzft & 1048575)), zzfq(zzfw), zzbqq);
                        continue;
                    case 28:
                        zzbtb.zzy(this.zzfsv.zza(t, (long) (zzft & 1048575)));
                        continue;
                    case 29:
                        zzbtb.zzz(this.zzfsv.zza(t, (long) (zzft & 1048575)));
                        continue;
                    case 30:
                        List zza2 = this.zzfsv.zza(t, (long) (zzft & 1048575));
                        zzbtb.zzaa(zza2);
                        zza = zzbte.zza(zzals, zza2, zzfs(zzfw), obj2, zzbtu);
                        obj2 = zza;
                        break;
                    case 31:
                        zzbtb.zzab(this.zzfsv.zza(t, (long) (zzft & 1048575)));
                        continue;
                    case 32:
                        zzbtb.zzac(this.zzfsv.zza(t, (long) (zzft & 1048575)));
                        continue;
                    case 33:
                        zzbtb.zzad(this.zzfsv.zza(t, (long) (zzft & 1048575)));
                        continue;
                    case 34:
                        zzbtb.zzae(this.zzfsv.zza(t, (long) (zzft & 1048575)));
                        continue;
                    case 35:
                        zzbtb.zzp(this.zzfsv.zza(t, (long) (zzft & 1048575)));
                        continue;
                    case 36:
                        zzbtb.zzq(this.zzfsv.zza(t, (long) (zzft & 1048575)));
                        continue;
                    case 37:
                        zzbtb.zzs(this.zzfsv.zza(t, (long) (zzft & 1048575)));
                        continue;
                    case 38:
                        zzbtb.zzr(this.zzfsv.zza(t, (long) (zzft & 1048575)));
                        continue;
                    case 39:
                        zzbtb.zzt(this.zzfsv.zza(t, (long) (zzft & 1048575)));
                        continue;
                    case 40:
                        zzbtb.zzu(this.zzfsv.zza(t, (long) (zzft & 1048575)));
                        continue;
                    case 41:
                        zzbtb.zzv(this.zzfsv.zza(t, (long) (zzft & 1048575)));
                        continue;
                    case 42:
                        zzbtb.zzw(this.zzfsv.zza(t, (long) (zzft & 1048575)));
                        continue;
                    case 43:
                        zzbtb.zzz(this.zzfsv.zza(t, (long) (zzft & 1048575)));
                        continue;
                    case 44:
                        List zza3 = this.zzfsv.zza(t, (long) (zzft & 1048575));
                        zzbtb.zzaa(zza3);
                        zza = zzbte.zza(zzals, zza3, zzfs(zzfw), obj2, zzbtu);
                        obj2 = zza;
                        break;
                    case 45:
                        zzbtb.zzab(this.zzfsv.zza(t, (long) (zzft & 1048575)));
                        continue;
                    case 46:
                        zzbtb.zzac(this.zzfsv.zza(t, (long) (zzft & 1048575)));
                        continue;
                    case 47:
                        zzbtb.zzad(this.zzfsv.zza(t, (long) (zzft & 1048575)));
                        continue;
                    case 48:
                        zzbtb.zzae(this.zzfsv.zza(t, (long) (zzft & 1048575)));
                        continue;
                    case 49:
                        long j3 = (long) (zzft & 1048575);
                        zzbtb.zzb(this.zzfsv.zza(t, j3), zzfq(zzfw), zzbqq);
                        continue;
                    case 50:
                        Object zzfr = zzfr(zzfw);
                        long zzft2 = (long) (zzft(zzfw) & 1048575);
                        Object zzp = zzbua.zzp(t, zzft2);
                        if (zzp == null) {
                            zzp = this.zzfsy.zzaa(zzfr);
                            zzbua.zza((Object) t, zzft2, zzp);
                        } else if (this.zzfsy.zzy(zzp)) {
                            Object zzaa = this.zzfsy.zzaa(zzfr);
                            this.zzfsy.zzc(zzaa, zzp);
                            zzbua.zza((Object) t, zzft2, zzaa);
                            zzp = zzaa;
                        }
                        zzbtb.zza(this.zzfsy.zzw(zzp), this.zzfsy.zzab(zzfr), zzbqq);
                        continue;
                    case 51:
                        zzbua.zza((Object) t, (long) (zzft & 1048575), (Object) Double.valueOf(zzbtb.readDouble()));
                        zzb(t, zzals, zzfw);
                        continue;
                    case 52:
                        zzbua.zza((Object) t, (long) (zzft & 1048575), (Object) Float.valueOf(zzbtb.readFloat()));
                        zzb(t, zzals, zzfw);
                        continue;
                    case 53:
                        zzbua.zza((Object) t, (long) (zzft & 1048575), (Object) Long.valueOf(zzbtb.zzakw()));
                        zzb(t, zzals, zzfw);
                        continue;
                    case 54:
                        zzbua.zza((Object) t, (long) (zzft & 1048575), (Object) Long.valueOf(zzbtb.zzakv()));
                        zzb(t, zzals, zzfw);
                        continue;
                    case 55:
                        zzbua.zza((Object) t, (long) (zzft & 1048575), (Object) Integer.valueOf(zzbtb.zzakx()));
                        zzb(t, zzals, zzfw);
                        continue;
                    case 56:
                        zzbua.zza((Object) t, (long) (zzft & 1048575), (Object) Long.valueOf(zzbtb.zzaky()));
                        zzb(t, zzals, zzfw);
                        continue;
                    case 57:
                        zzbua.zza((Object) t, (long) (zzft & 1048575), (Object) Integer.valueOf(zzbtb.zzakz()));
                        zzb(t, zzals, zzfw);
                        continue;
                    case 58:
                        zzbua.zza((Object) t, (long) (zzft & 1048575), (Object) Boolean.valueOf(zzbtb.zzala()));
                        zzb(t, zzals, zzfw);
                        continue;
                    case 59:
                        zza((Object) t, zzft, zzbtb);
                        zzb(t, zzals, zzfw);
                        continue;
                    case 60:
                        if (zza(t, zzals, zzfw)) {
                            long j4 = (long) (zzft & 1048575);
                            zzbua.zza((Object) t, j4, zzbrf.zzb(zzbua.zzp(t, j4), zzbtb.zza(zzfq(zzfw), zzbqq)));
                        } else {
                            zzbua.zza((Object) t, (long) (zzft & 1048575), zzbtb.zza(zzfq(zzfw), zzbqq));
                            zze(t, zzfw);
                        }
                        zzb(t, zzals, zzfw);
                        continue;
                    case 61:
                        zzbua.zza((Object) t, (long) (zzft & 1048575), (Object) zzbtb.zzalc());
                        zzb(t, zzals, zzfw);
                        continue;
                    case 62:
                        zzbua.zza((Object) t, (long) (zzft & 1048575), (Object) Integer.valueOf(zzbtb.zzald()));
                        zzb(t, zzals, zzfw);
                        continue;
                    case 63:
                        int zzale2 = zzbtb.zzale();
                        zzbri zzfs2 = zzfs(zzfw);
                        if (zzfs2 != null) {
                            if (!zzfs2.zzcb(zzale2)) {
                                zza = zzbte.zza(zzals, zzale2, obj2, zzbtu);
                            }
                        }
                        zzbua.zza((Object) t, (long) (zzft & 1048575), (Object) Integer.valueOf(zzale2));
                        zzb(t, zzals, zzfw);
                        continue;
                    case 64:
                        zzbua.zza((Object) t, (long) (zzft & 1048575), (Object) Integer.valueOf(zzbtb.zzalf()));
                        zzb(t, zzals, zzfw);
                        continue;
                    case 65:
                        zzbua.zza((Object) t, (long) (zzft & 1048575), (Object) Long.valueOf(zzbtb.zzalg()));
                        zzb(t, zzals, zzfw);
                        continue;
                    case 66:
                        zzbua.zza((Object) t, (long) (zzft & 1048575), (Object) Integer.valueOf(zzbtb.zzalh()));
                        zzb(t, zzals, zzfw);
                        continue;
                    case 67:
                        zzbua.zza((Object) t, (long) (zzft & 1048575), (Object) Long.valueOf(zzbtb.zzali()));
                        zzb(t, zzals, zzfw);
                        continue;
                    case 68:
                        zzbua.zza((Object) t, (long) (zzft & 1048575), zzbtb.zzb(zzfq(zzfw), zzbqq));
                        zzb(t, zzals, zzfw);
                        continue;
                    default:
                        if (obj2 == null) {
                            try {
                                obj2 = zzbtu.zzaoy();
                                break;
                            } catch (Throwable th) {
                                for (int i = this.zzfss; i < this.zzfst; i++) {
                                    obj2 = zza((Object) t, this.zzfsr[i], (UB) obj2, zzbtu);
                                }
                                if (obj2 != null) {
                                    zzbtu.zzg(t, obj2);
                                }
                                throw th;
                            }
                        }
                        if (!zzbtu.zza(obj2, zzbtb)) {
                            for (int i2 = this.zzfss; i2 < this.zzfst; i2++) {
                                obj2 = zza((Object) t, this.zzfsr[i2], (UB) obj2, zzbtu);
                            }
                            if (obj2 != null) {
                                zzbtu.zzg(t, obj2);
                            }
                            return;
                        }
                        continue;
                }
                obj2 = zza;
            } else if (zzals == Integer.MAX_VALUE) {
                for (int i3 = this.zzfss; i3 < this.zzfst; i3++) {
                    obj2 = zza((Object) t, this.zzfsr[i3], (UB) obj2, zzbtu);
                }
                if (obj2 != null) {
                    zzbtu.zzg(t, obj2);
                }
                return;
            } else {
                if (!this.zzfsn) {
                    obj = null;
                } else {
                    obj = zzbqr.zza(zzbqq, this.zzfsm, zzals);
                }
                if (obj != null) {
                    if (zzbqu == null) {
                        zzbqu = zzbqr.zzr(t);
                    }
                    zzbqu zzbqu2 = zzbqu;
                    obj2 = zzbqr.zza(zzbtb, obj, zzbqq, zzbqu2, obj2, zzbtu);
                    zzbqu = zzbqu2;
                } else {
                    zzbtu.zza(zzbtb);
                    if (obj2 == null) {
                        obj2 = zzbtu.zzah(t);
                    }
                    if (!zzbtu.zza(obj2, zzbtb)) {
                        for (int i4 = this.zzfss; i4 < this.zzfst; i4++) {
                            obj2 = zza((Object) t, this.zzfsr[i4], (UB) obj2, zzbtu);
                        }
                        if (obj2 != null) {
                            zzbtu.zzg(t, obj2);
                        }
                        return;
                    }
                }
            }
        }
    }

    private static zzbtv zzad(Object obj) {
        zzbrd zzbrd = (zzbrd) obj;
        zzbtv zzbtv = zzbrd.zzfpu;
        if (zzbtv != zzbtv.zzaoz()) {
            return zzbtv;
        }
        zzbtv zzapa = zzbtv.zzapa();
        zzbrd.zzfpu = zzapa;
        return zzapa;
    }

    /* JADX WARNING: type inference failed for: r8v2, types: [int] */
    /* JADX WARNING: type inference failed for: r8v5 */
    /* JADX WARNING: Multi-variable type inference failed */
    private static int zza(zzbtc zzbtc, byte[] bArr, int i, int i2, zzbpr zzbpr) throws IOException {
        int i3 = i + 1;
        byte b = bArr[i];
        if (b < 0) {
            i3 = zzbpq.zza((int) b, bArr, i3, zzbpr);
            b = zzbpr.zzfld;
        }
        int i4 = i3;
        if (b < 0 || b > i2 - i4) {
            throw zzbrl.zzanc();
        }
        Object newInstance = zzbtc.newInstance();
        int i5 = b + i4;
        zzbtc.zza(newInstance, bArr, i4, i5, zzbpr);
        zzbtc.zzs(newInstance);
        zzbpr.zzflf = newInstance;
        return i5;
    }

    private static int zza(zzbtc zzbtc, byte[] bArr, int i, int i2, int i3, zzbpr zzbpr) throws IOException {
        zzbsp zzbsp = (zzbsp) zzbtc;
        Object newInstance = zzbsp.newInstance();
        int zza = zzbsp.zza((T) newInstance, bArr, i, i2, i3, zzbpr);
        zzbsp.zzs(newInstance);
        zzbpr.zzflf = newInstance;
        return zza;
    }

    private static int zza(zzbtc<?> zzbtc, int i, byte[] bArr, int i2, int i3, zzbrk<?> zzbrk, zzbpr zzbpr) throws IOException {
        int zza = zza((zzbtc) zzbtc, bArr, i2, i3, zzbpr);
        zzbrk.add(zzbpr.zzflf);
        while (zza < i3) {
            int zza2 = zzbpq.zza(bArr, zza, zzbpr);
            if (i != zzbpr.zzfld) {
                break;
            }
            zza = zza((zzbtc) zzbtc, bArr, zza2, i3, zzbpr);
            zzbrk.add(zzbpr.zzflf);
        }
        return zza;
    }

    private static int zza(byte[] bArr, int i, int i2, zzbuj zzbuj, Class<?> cls, zzbpr zzbpr) throws IOException {
        switch (zzbuj) {
            case BOOL:
                int zzb = zzbpq.zzb(bArr, i, zzbpr);
                zzbpr.zzflf = Boolean.valueOf(zzbpr.zzfle != 0);
                return zzb;
            case BYTES:
                return zzbpq.zze(bArr, i, zzbpr);
            case DOUBLE:
                zzbpr.zzflf = Double.valueOf(zzbpq.zzi(bArr, i));
                return i + 8;
            case FIXED32:
            case SFIXED32:
                zzbpr.zzflf = Integer.valueOf(zzbpq.zzg(bArr, i));
                return i + 4;
            case FIXED64:
            case SFIXED64:
                zzbpr.zzflf = Long.valueOf(zzbpq.zzh(bArr, i));
                return i + 8;
            case FLOAT:
                zzbpr.zzflf = Float.valueOf(zzbpq.zzj(bArr, i));
                return i + 4;
            case ENUM:
            case INT32:
            case UINT32:
                int zza = zzbpq.zza(bArr, i, zzbpr);
                zzbpr.zzflf = Integer.valueOf(zzbpr.zzfld);
                return zza;
            case INT64:
            case UINT64:
                int zzb2 = zzbpq.zzb(bArr, i, zzbpr);
                zzbpr.zzflf = Long.valueOf(zzbpr.zzfle);
                return zzb2;
            case MESSAGE:
                return zza(zzbsy.zzaog().zzf(cls), bArr, i, i2, zzbpr);
            case SINT32:
                int zza2 = zzbpq.zza(bArr, i, zzbpr);
                zzbpr.zzflf = Integer.valueOf(zzbqf.zzeu(zzbpr.zzfld));
                return zza2;
            case SINT64:
                int zzb3 = zzbpq.zzb(bArr, i, zzbpr);
                zzbpr.zzflf = Long.valueOf(zzbqf.zzax(zzbpr.zzfle));
                return zzb3;
            case STRING:
                return zzbpq.zzd(bArr, i, zzbpr);
            default:
                throw new RuntimeException("unsupported field type.");
        }
    }

    private static int zza(int i, byte[] bArr, int i2, int i3, Object obj, zzbpr zzbpr) throws IOException {
        return zzbpq.zza(i, bArr, i2, i3, zzad(obj), zzbpr);
    }

    /*  JADX ERROR: JadxRuntimeException in pass: RegionMakerVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Regions count limit reached
        	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:89)
        	at jadx.core.dex.visitors.regions.RegionMaker.makeEndlessLoop(RegionMaker.java:368)
        	at jadx.core.dex.visitors.regions.RegionMaker.processLoop(RegionMaker.java:172)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:106)
        	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:86)
        	at jadx.core.dex.visitors.regions.RegionMaker.processIf(RegionMaker.java:695)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:123)
        	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:86)
        	at jadx.core.dex.visitors.regions.RegionMaker.processIf(RegionMaker.java:695)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:123)
        	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:86)
        	at jadx.core.dex.visitors.regions.RegionMaker.processIf(RegionMaker.java:690)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:123)
        	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:86)
        	at jadx.core.dex.visitors.regions.RegionMaker.processSwitch(RegionMaker.java:869)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:128)
        	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:86)
        	at jadx.core.dex.visitors.regions.RegionMakerVisitor.visit(RegionMakerVisitor.java:49)
        	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:27)
        	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$1(DepthTraversal.java:14)
        	at java.util.ArrayList.forEach(Unknown Source)
        	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
        	at jadx.core.ProcessClass.process(ProcessClass.java:30)
        	at jadx.core.ProcessClass.lambda$processDependencies$0(ProcessClass.java:49)
        	at java.util.ArrayList.forEach(Unknown Source)
        	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:49)
        	at jadx.core.ProcessClass.process(ProcessClass.java:35)
        	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
        	at jadx.api.JavaClass.decompile(JavaClass.java:62)
        	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
        */
    /* JADX WARNING: Removed duplicated region for block: B:114:0x022a  */
    /* JADX WARNING: Removed duplicated region for block: B:247:0x0406 A[SYNTHETIC] */
    private final int zza(T r18, byte[] r19, int r20, int r21, int r22, int r23, int r24, int r25, long r26, int r28, long r29, com.google.android.gms.internal.ads.zzbpr r31) throws java.io.IOException {
        /*
            r17 = this;
            r0 = r17
            r1 = r18
            r7 = r19
            r4 = r20
            r8 = r21
            r9 = r22
            r2 = r24
            r10 = r25
            r5 = r29
            r11 = r31
            sun.misc.Unsafe r3 = zzfsh
            java.lang.Object r3 = r3.getObject(r1, r5)
            com.google.android.gms.internal.ads.zzbrk r3 = (com.google.android.gms.internal.ads.zzbrk) r3
            boolean r12 = r3.zzaki()
            r13 = 1
            if (r12 != 0) goto L_0x0036
            int r12 = r3.size()
            if (r12 != 0) goto L_0x002c
            r12 = 10
            goto L_0x002d
        L_0x002c:
            int r12 = r12 << r13
        L_0x002d:
            com.google.android.gms.internal.ads.zzbrk r3 = r3.zzel(r12)
            sun.misc.Unsafe r12 = zzfsh
            r12.putObject(r1, r5, r3)
        L_0x0036:
            r12 = r3
            r3 = 5
            r5 = 0
            r14 = 2
            switch(r28) {
                case 18: goto L_0x03c6;
                case 19: goto L_0x0387;
                case 20: goto L_0x0348;
                case 21: goto L_0x0348;
                case 22: goto L_0x0332;
                case 23: goto L_0x02f3;
                case 24: goto L_0x02b4;
                case 25: goto L_0x025f;
                case 26: goto L_0x01a5;
                case 27: goto L_0x0194;
                case 28: goto L_0x013b;
                case 29: goto L_0x0332;
                case 30: goto L_0x0109;
                case 31: goto L_0x02b4;
                case 32: goto L_0x02f3;
                case 33: goto L_0x00be;
                case 34: goto L_0x0073;
                case 35: goto L_0x03c6;
                case 36: goto L_0x0387;
                case 37: goto L_0x0348;
                case 38: goto L_0x0348;
                case 39: goto L_0x0332;
                case 40: goto L_0x02f3;
                case 41: goto L_0x02b4;
                case 42: goto L_0x025f;
                case 43: goto L_0x0332;
                case 44: goto L_0x0109;
                case 45: goto L_0x02b4;
                case 46: goto L_0x02f3;
                case 47: goto L_0x00be;
                case 48: goto L_0x0073;
                case 49: goto L_0x0040;
                default: goto L_0x003e;
            }
        L_0x003e:
            goto L_0x0405
        L_0x0040:
            r1 = 3
            if (r2 != r1) goto L_0x0405
            com.google.android.gms.internal.ads.zzbtc r10 = r0.zzfq(r10)
            r1 = r9 & -8
            r13 = r1 | 4
            r1 = r10
            r2 = r7
            r3 = r4
            r4 = r8
            r5 = r13
            r6 = r11
            int r1 = zza(r1, r2, r3, r4, r5, r6)
            java.lang.Object r2 = r11.zzflf
            r12.add(r2)
        L_0x005a:
            if (r1 >= r8) goto L_0x0406
            int r3 = com.google.android.gms.internal.ads.zzbpq.zza(r7, r1, r11)
            int r2 = r11.zzfld
            if (r9 != r2) goto L_0x0406
            r1 = r10
            r2 = r7
            r4 = r8
            r5 = r13
            r6 = r11
            int r1 = zza(r1, r2, r3, r4, r5, r6)
            java.lang.Object r2 = r11.zzflf
            r12.add(r2)
            goto L_0x005a
        L_0x0073:
            if (r2 != r14) goto L_0x0095
            com.google.android.gms.internal.ads.zzbrz r12 = (com.google.android.gms.internal.ads.zzbrz) r12
            int r1 = com.google.android.gms.internal.ads.zzbpq.zza(r7, r4, r11)
            int r2 = r11.zzfld
            int r2 = r2 + r1
        L_0x007e:
            if (r1 >= r2) goto L_0x008e
            int r1 = com.google.android.gms.internal.ads.zzbpq.zzb(r7, r1, r11)
            long r3 = r11.zzfle
            long r3 = com.google.android.gms.internal.ads.zzbqf.zzax(r3)
            r12.zzbj(r3)
            goto L_0x007e
        L_0x008e:
            if (r1 == r2) goto L_0x0406
            com.google.android.gms.internal.ads.zzbrl r1 = com.google.android.gms.internal.ads.zzbrl.zzanc()
            throw r1
        L_0x0095:
            if (r2 != 0) goto L_0x0405
            com.google.android.gms.internal.ads.zzbrz r12 = (com.google.android.gms.internal.ads.zzbrz) r12
            int r1 = com.google.android.gms.internal.ads.zzbpq.zzb(r7, r4, r11)
            long r2 = r11.zzfle
            long r2 = com.google.android.gms.internal.ads.zzbqf.zzax(r2)
            r12.zzbj(r2)
        L_0x00a6:
            if (r1 >= r8) goto L_0x0406
            int r2 = com.google.android.gms.internal.ads.zzbpq.zza(r7, r1, r11)
            int r3 = r11.zzfld
            if (r9 != r3) goto L_0x0406
            int r1 = com.google.android.gms.internal.ads.zzbpq.zzb(r7, r2, r11)
            long r2 = r11.zzfle
            long r2 = com.google.android.gms.internal.ads.zzbqf.zzax(r2)
            r12.zzbj(r2)
            goto L_0x00a6
        L_0x00be:
            if (r2 != r14) goto L_0x00e0
            com.google.android.gms.internal.ads.zzbre r12 = (com.google.android.gms.internal.ads.zzbre) r12
            int r1 = com.google.android.gms.internal.ads.zzbpq.zza(r7, r4, r11)
            int r2 = r11.zzfld
            int r2 = r2 + r1
        L_0x00c9:
            if (r1 >= r2) goto L_0x00d9
            int r1 = com.google.android.gms.internal.ads.zzbpq.zza(r7, r1, r11)
            int r3 = r11.zzfld
            int r3 = com.google.android.gms.internal.ads.zzbqf.zzeu(r3)
            r12.zzfo(r3)
            goto L_0x00c9
        L_0x00d9:
            if (r1 == r2) goto L_0x0406
            com.google.android.gms.internal.ads.zzbrl r1 = com.google.android.gms.internal.ads.zzbrl.zzanc()
            throw r1
        L_0x00e0:
            if (r2 != 0) goto L_0x0405
            com.google.android.gms.internal.ads.zzbre r12 = (com.google.android.gms.internal.ads.zzbre) r12
            int r1 = com.google.android.gms.internal.ads.zzbpq.zza(r7, r4, r11)
            int r2 = r11.zzfld
            int r2 = com.google.android.gms.internal.ads.zzbqf.zzeu(r2)
            r12.zzfo(r2)
        L_0x00f1:
            if (r1 >= r8) goto L_0x0406
            int r2 = com.google.android.gms.internal.ads.zzbpq.zza(r7, r1, r11)
            int r3 = r11.zzfld
            if (r9 != r3) goto L_0x0406
            int r1 = com.google.android.gms.internal.ads.zzbpq.zza(r7, r2, r11)
            int r2 = r11.zzfld
            int r2 = com.google.android.gms.internal.ads.zzbqf.zzeu(r2)
            r12.zzfo(r2)
            goto L_0x00f1
        L_0x0109:
            if (r2 != r14) goto L_0x0110
            int r2 = com.google.android.gms.internal.ads.zzbpq.zza(r7, r4, r12, r11)
            goto L_0x011b
        L_0x0110:
            if (r2 != 0) goto L_0x0405
            r2 = r9
            r3 = r7
            r5 = r8
            r6 = r12
            r7 = r11
            int r2 = com.google.android.gms.internal.ads.zzbpq.zza(r2, r3, r4, r5, r6, r7)
        L_0x011b:
            com.google.android.gms.internal.ads.zzbrd r1 = (com.google.android.gms.internal.ads.zzbrd) r1
            com.google.android.gms.internal.ads.zzbtv r3 = r1.zzfpu
            com.google.android.gms.internal.ads.zzbtv r4 = com.google.android.gms.internal.ads.zzbtv.zzaoz()
            if (r3 != r4) goto L_0x0126
            r3 = 0
        L_0x0126:
            com.google.android.gms.internal.ads.zzbri r4 = r0.zzfs(r10)
            com.google.android.gms.internal.ads.zzbtu<?, ?> r5 = r0.zzfsw
            r6 = r23
            java.lang.Object r3 = com.google.android.gms.internal.ads.zzbte.zza(r6, r12, r4, r3, r5)
            com.google.android.gms.internal.ads.zzbtv r3 = (com.google.android.gms.internal.ads.zzbtv) r3
            if (r3 == 0) goto L_0x0138
            r1.zzfpu = r3
        L_0x0138:
            r1 = r2
            goto L_0x0406
        L_0x013b:
            if (r2 != r14) goto L_0x0405
            int r1 = com.google.android.gms.internal.ads.zzbpq.zza(r7, r4, r11)
            int r2 = r11.zzfld
            if (r2 >= 0) goto L_0x014a
            com.google.android.gms.internal.ads.zzbrl r1 = com.google.android.gms.internal.ads.zzbrl.zzand()
            throw r1
        L_0x014a:
            int r3 = r7.length
            int r3 = r3 - r1
            if (r2 <= r3) goto L_0x0153
            com.google.android.gms.internal.ads.zzbrl r1 = com.google.android.gms.internal.ads.zzbrl.zzanc()
            throw r1
        L_0x0153:
            if (r2 != 0) goto L_0x015b
            com.google.android.gms.internal.ads.zzbpu r2 = com.google.android.gms.internal.ads.zzbpu.zzfli
            r12.add(r2)
            goto L_0x0163
        L_0x015b:
            com.google.android.gms.internal.ads.zzbpu r3 = com.google.android.gms.internal.ads.zzbpu.zzi(r7, r1, r2)
            r12.add(r3)
            int r1 = r1 + r2
        L_0x0163:
            if (r1 >= r8) goto L_0x0406
            int r2 = com.google.android.gms.internal.ads.zzbpq.zza(r7, r1, r11)
            int r3 = r11.zzfld
            if (r9 != r3) goto L_0x0406
            int r1 = com.google.android.gms.internal.ads.zzbpq.zza(r7, r2, r11)
            int r2 = r11.zzfld
            if (r2 >= 0) goto L_0x017a
            com.google.android.gms.internal.ads.zzbrl r1 = com.google.android.gms.internal.ads.zzbrl.zzand()
            throw r1
        L_0x017a:
            int r3 = r7.length
            int r3 = r3 - r1
            if (r2 <= r3) goto L_0x0183
            com.google.android.gms.internal.ads.zzbrl r1 = com.google.android.gms.internal.ads.zzbrl.zzanc()
            throw r1
        L_0x0183:
            if (r2 != 0) goto L_0x018b
            com.google.android.gms.internal.ads.zzbpu r2 = com.google.android.gms.internal.ads.zzbpu.zzfli
            r12.add(r2)
            goto L_0x0163
        L_0x018b:
            com.google.android.gms.internal.ads.zzbpu r3 = com.google.android.gms.internal.ads.zzbpu.zzi(r7, r1, r2)
            r12.add(r3)
            int r1 = r1 + r2
            goto L_0x0163
        L_0x0194:
            if (r2 != r14) goto L_0x0405
            com.google.android.gms.internal.ads.zzbtc r1 = r0.zzfq(r10)
            r2 = r9
            r3 = r7
            r5 = r8
            r6 = r12
            r7 = r11
            int r1 = zza(r1, r2, r3, r4, r5, r6, r7)
            goto L_0x0406
        L_0x01a5:
            if (r2 != r14) goto L_0x0405
            r1 = 536870912(0x20000000, double:2.652494739E-315)
            long r15 = r26 & r1
            int r1 = (r15 > r5 ? 1 : (r15 == r5 ? 0 : -1))
            if (r1 != 0) goto L_0x01fb
            int r1 = com.google.android.gms.internal.ads.zzbpq.zza(r7, r4, r11)
            int r2 = r11.zzfld
            if (r2 >= 0) goto L_0x01bd
            com.google.android.gms.internal.ads.zzbrl r1 = com.google.android.gms.internal.ads.zzbrl.zzand()
            throw r1
        L_0x01bd:
            if (r2 != 0) goto L_0x01c5
            java.lang.String r2 = ""
            r12.add(r2)
            goto L_0x01d0
        L_0x01c5:
            java.lang.String r3 = new java.lang.String
            java.nio.charset.Charset r4 = com.google.android.gms.internal.ads.zzbrf.UTF_8
            r3.<init>(r7, r1, r2, r4)
            r12.add(r3)
            int r1 = r1 + r2
        L_0x01d0:
            if (r1 >= r8) goto L_0x0406
            int r2 = com.google.android.gms.internal.ads.zzbpq.zza(r7, r1, r11)
            int r3 = r11.zzfld
            if (r9 != r3) goto L_0x0406
            int r1 = com.google.android.gms.internal.ads.zzbpq.zza(r7, r2, r11)
            int r2 = r11.zzfld
            if (r2 >= 0) goto L_0x01e7
            com.google.android.gms.internal.ads.zzbrl r1 = com.google.android.gms.internal.ads.zzbrl.zzand()
            throw r1
        L_0x01e7:
            if (r2 != 0) goto L_0x01ef
            java.lang.String r2 = ""
            r12.add(r2)
            goto L_0x01d0
        L_0x01ef:
            java.lang.String r3 = new java.lang.String
            java.nio.charset.Charset r4 = com.google.android.gms.internal.ads.zzbrf.UTF_8
            r3.<init>(r7, r1, r2, r4)
            r12.add(r3)
            int r1 = r1 + r2
            goto L_0x01d0
        L_0x01fb:
            int r1 = com.google.android.gms.internal.ads.zzbpq.zza(r7, r4, r11)
            int r2 = r11.zzfld
            if (r2 >= 0) goto L_0x0208
            com.google.android.gms.internal.ads.zzbrl r1 = com.google.android.gms.internal.ads.zzbrl.zzand()
            throw r1
        L_0x0208:
            if (r2 != 0) goto L_0x0210
            java.lang.String r2 = ""
            r12.add(r2)
            goto L_0x0228
        L_0x0210:
            int r3 = r1 + r2
            boolean r4 = com.google.android.gms.internal.ads.zzbuc.zzm(r7, r1, r3)
            if (r4 != 0) goto L_0x021d
            com.google.android.gms.internal.ads.zzbrl r1 = com.google.android.gms.internal.ads.zzbrl.zzank()
            throw r1
        L_0x021d:
            java.lang.String r4 = new java.lang.String
            java.nio.charset.Charset r5 = com.google.android.gms.internal.ads.zzbrf.UTF_8
            r4.<init>(r7, r1, r2, r5)
            r12.add(r4)
        L_0x0227:
            r1 = r3
        L_0x0228:
            if (r1 >= r8) goto L_0x0406
            int r2 = com.google.android.gms.internal.ads.zzbpq.zza(r7, r1, r11)
            int r3 = r11.zzfld
            if (r9 != r3) goto L_0x0406
            int r1 = com.google.android.gms.internal.ads.zzbpq.zza(r7, r2, r11)
            int r2 = r11.zzfld
            if (r2 >= 0) goto L_0x023f
            com.google.android.gms.internal.ads.zzbrl r1 = com.google.android.gms.internal.ads.zzbrl.zzand()
            throw r1
        L_0x023f:
            if (r2 != 0) goto L_0x0247
            java.lang.String r2 = ""
            r12.add(r2)
            goto L_0x0228
        L_0x0247:
            int r3 = r1 + r2
            boolean r4 = com.google.android.gms.internal.ads.zzbuc.zzm(r7, r1, r3)
            if (r4 != 0) goto L_0x0254
            com.google.android.gms.internal.ads.zzbrl r1 = com.google.android.gms.internal.ads.zzbrl.zzank()
            throw r1
        L_0x0254:
            java.lang.String r4 = new java.lang.String
            java.nio.charset.Charset r5 = com.google.android.gms.internal.ads.zzbrf.UTF_8
            r4.<init>(r7, r1, r2, r5)
            r12.add(r4)
            goto L_0x0227
        L_0x025f:
            r1 = 0
            if (r2 != r14) goto L_0x0285
            com.google.android.gms.internal.ads.zzbps r12 = (com.google.android.gms.internal.ads.zzbps) r12
            int r2 = com.google.android.gms.internal.ads.zzbpq.zza(r7, r4, r11)
            int r3 = r11.zzfld
            int r3 = r3 + r2
        L_0x026b:
            if (r2 >= r3) goto L_0x027e
            int r2 = com.google.android.gms.internal.ads.zzbpq.zzb(r7, r2, r11)
            long r8 = r11.zzfle
            int r4 = (r8 > r5 ? 1 : (r8 == r5 ? 0 : -1))
            if (r4 == 0) goto L_0x0279
            r4 = r13
            goto L_0x027a
        L_0x0279:
            r4 = r1
        L_0x027a:
            r12.addBoolean(r4)
            goto L_0x026b
        L_0x027e:
            if (r2 == r3) goto L_0x0138
            com.google.android.gms.internal.ads.zzbrl r1 = com.google.android.gms.internal.ads.zzbrl.zzanc()
            throw r1
        L_0x0285:
            if (r2 != 0) goto L_0x0405
            com.google.android.gms.internal.ads.zzbps r12 = (com.google.android.gms.internal.ads.zzbps) r12
            int r2 = com.google.android.gms.internal.ads.zzbpq.zzb(r7, r4, r11)
            long r3 = r11.zzfle
            int r10 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r10 == 0) goto L_0x0295
            r3 = r13
            goto L_0x0296
        L_0x0295:
            r3 = r1
        L_0x0296:
            r12.addBoolean(r3)
        L_0x0299:
            if (r2 >= r8) goto L_0x0138
            int r3 = com.google.android.gms.internal.ads.zzbpq.zza(r7, r2, r11)
            int r4 = r11.zzfld
            if (r9 != r4) goto L_0x0138
            int r2 = com.google.android.gms.internal.ads.zzbpq.zzb(r7, r3, r11)
            long r3 = r11.zzfle
            int r10 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r10 == 0) goto L_0x02af
            r3 = r13
            goto L_0x02b0
        L_0x02af:
            r3 = r1
        L_0x02b0:
            r12.addBoolean(r3)
            goto L_0x0299
        L_0x02b4:
            if (r2 != r14) goto L_0x02d2
            com.google.android.gms.internal.ads.zzbre r12 = (com.google.android.gms.internal.ads.zzbre) r12
            int r1 = com.google.android.gms.internal.ads.zzbpq.zza(r7, r4, r11)
            int r2 = r11.zzfld
            int r2 = r2 + r1
        L_0x02bf:
            if (r1 >= r2) goto L_0x02cb
            int r3 = com.google.android.gms.internal.ads.zzbpq.zzg(r7, r1)
            r12.zzfo(r3)
            int r1 = r1 + 4
            goto L_0x02bf
        L_0x02cb:
            if (r1 == r2) goto L_0x0406
            com.google.android.gms.internal.ads.zzbrl r1 = com.google.android.gms.internal.ads.zzbrl.zzanc()
            throw r1
        L_0x02d2:
            if (r2 != r3) goto L_0x0405
            com.google.android.gms.internal.ads.zzbre r12 = (com.google.android.gms.internal.ads.zzbre) r12
            int r1 = com.google.android.gms.internal.ads.zzbpq.zzg(r19, r20)
            r12.zzfo(r1)
            int r1 = r4 + 4
        L_0x02df:
            if (r1 >= r8) goto L_0x0406
            int r2 = com.google.android.gms.internal.ads.zzbpq.zza(r7, r1, r11)
            int r3 = r11.zzfld
            if (r9 != r3) goto L_0x0406
            int r1 = com.google.android.gms.internal.ads.zzbpq.zzg(r7, r2)
            r12.zzfo(r1)
            int r1 = r2 + 4
            goto L_0x02df
        L_0x02f3:
            if (r2 != r14) goto L_0x0311
            com.google.android.gms.internal.ads.zzbrz r12 = (com.google.android.gms.internal.ads.zzbrz) r12
            int r1 = com.google.android.gms.internal.ads.zzbpq.zza(r7, r4, r11)
            int r2 = r11.zzfld
            int r2 = r2 + r1
        L_0x02fe:
            if (r1 >= r2) goto L_0x030a
            long r3 = com.google.android.gms.internal.ads.zzbpq.zzh(r7, r1)
            r12.zzbj(r3)
            int r1 = r1 + 8
            goto L_0x02fe
        L_0x030a:
            if (r1 == r2) goto L_0x0406
            com.google.android.gms.internal.ads.zzbrl r1 = com.google.android.gms.internal.ads.zzbrl.zzanc()
            throw r1
        L_0x0311:
            if (r2 != r13) goto L_0x0405
            com.google.android.gms.internal.ads.zzbrz r12 = (com.google.android.gms.internal.ads.zzbrz) r12
            long r1 = com.google.android.gms.internal.ads.zzbpq.zzh(r19, r20)
            r12.zzbj(r1)
            int r1 = r4 + 8
        L_0x031e:
            if (r1 >= r8) goto L_0x0406
            int r2 = com.google.android.gms.internal.ads.zzbpq.zza(r7, r1, r11)
            int r3 = r11.zzfld
            if (r9 != r3) goto L_0x0406
            long r3 = com.google.android.gms.internal.ads.zzbpq.zzh(r7, r2)
            r12.zzbj(r3)
            int r1 = r2 + 8
            goto L_0x031e
        L_0x0332:
            if (r2 != r14) goto L_0x033a
            int r1 = com.google.android.gms.internal.ads.zzbpq.zza(r7, r4, r12, r11)
            goto L_0x0406
        L_0x033a:
            if (r2 != 0) goto L_0x0405
            r1 = r9
            r2 = r7
            r3 = r4
            r4 = r8
            r5 = r12
            r6 = r11
            int r1 = com.google.android.gms.internal.ads.zzbpq.zza(r1, r2, r3, r4, r5, r6)
            goto L_0x0406
        L_0x0348:
            if (r2 != r14) goto L_0x0366
            com.google.android.gms.internal.ads.zzbrz r12 = (com.google.android.gms.internal.ads.zzbrz) r12
            int r1 = com.google.android.gms.internal.ads.zzbpq.zza(r7, r4, r11)
            int r2 = r11.zzfld
            int r2 = r2 + r1
        L_0x0353:
            if (r1 >= r2) goto L_0x035f
            int r1 = com.google.android.gms.internal.ads.zzbpq.zzb(r7, r1, r11)
            long r3 = r11.zzfle
            r12.zzbj(r3)
            goto L_0x0353
        L_0x035f:
            if (r1 == r2) goto L_0x0406
            com.google.android.gms.internal.ads.zzbrl r1 = com.google.android.gms.internal.ads.zzbrl.zzanc()
            throw r1
        L_0x0366:
            if (r2 != 0) goto L_0x0405
            com.google.android.gms.internal.ads.zzbrz r12 = (com.google.android.gms.internal.ads.zzbrz) r12
            int r1 = com.google.android.gms.internal.ads.zzbpq.zzb(r7, r4, r11)
            long r2 = r11.zzfle
            r12.zzbj(r2)
        L_0x0373:
            if (r1 >= r8) goto L_0x0406
            int r2 = com.google.android.gms.internal.ads.zzbpq.zza(r7, r1, r11)
            int r3 = r11.zzfld
            if (r9 != r3) goto L_0x0406
            int r1 = com.google.android.gms.internal.ads.zzbpq.zzb(r7, r2, r11)
            long r2 = r11.zzfle
            r12.zzbj(r2)
            goto L_0x0373
        L_0x0387:
            if (r2 != r14) goto L_0x03a5
            com.google.android.gms.internal.ads.zzbra r12 = (com.google.android.gms.internal.ads.zzbra) r12
            int r1 = com.google.android.gms.internal.ads.zzbpq.zza(r7, r4, r11)
            int r2 = r11.zzfld
            int r2 = r2 + r1
        L_0x0392:
            if (r1 >= r2) goto L_0x039e
            float r3 = com.google.android.gms.internal.ads.zzbpq.zzj(r7, r1)
            r12.zzh(r3)
            int r1 = r1 + 4
            goto L_0x0392
        L_0x039e:
            if (r1 == r2) goto L_0x0406
            com.google.android.gms.internal.ads.zzbrl r1 = com.google.android.gms.internal.ads.zzbrl.zzanc()
            throw r1
        L_0x03a5:
            if (r2 != r3) goto L_0x0405
            com.google.android.gms.internal.ads.zzbra r12 = (com.google.android.gms.internal.ads.zzbra) r12
            float r1 = com.google.android.gms.internal.ads.zzbpq.zzj(r19, r20)
            r12.zzh(r1)
            int r1 = r4 + 4
        L_0x03b2:
            if (r1 >= r8) goto L_0x0406
            int r2 = com.google.android.gms.internal.ads.zzbpq.zza(r7, r1, r11)
            int r3 = r11.zzfld
            if (r9 != r3) goto L_0x0406
            float r1 = com.google.android.gms.internal.ads.zzbpq.zzj(r7, r2)
            r12.zzh(r1)
            int r1 = r2 + 4
            goto L_0x03b2
        L_0x03c6:
            if (r2 != r14) goto L_0x03e4
            com.google.android.gms.internal.ads.zzbqn r12 = (com.google.android.gms.internal.ads.zzbqn) r12
            int r1 = com.google.android.gms.internal.ads.zzbpq.zza(r7, r4, r11)
            int r2 = r11.zzfld
            int r2 = r2 + r1
        L_0x03d1:
            if (r1 >= r2) goto L_0x03dd
            double r3 = com.google.android.gms.internal.ads.zzbpq.zzi(r7, r1)
            r12.zzd(r3)
            int r1 = r1 + 8
            goto L_0x03d1
        L_0x03dd:
            if (r1 == r2) goto L_0x0406
            com.google.android.gms.internal.ads.zzbrl r1 = com.google.android.gms.internal.ads.zzbrl.zzanc()
            throw r1
        L_0x03e4:
            if (r2 != r13) goto L_0x0405
            com.google.android.gms.internal.ads.zzbqn r12 = (com.google.android.gms.internal.ads.zzbqn) r12
            double r1 = com.google.android.gms.internal.ads.zzbpq.zzi(r19, r20)
            r12.zzd(r1)
            int r1 = r4 + 8
        L_0x03f1:
            if (r1 >= r8) goto L_0x0406
            int r2 = com.google.android.gms.internal.ads.zzbpq.zza(r7, r1, r11)
            int r3 = r11.zzfld
            if (r9 != r3) goto L_0x0406
            double r3 = com.google.android.gms.internal.ads.zzbpq.zzi(r7, r2)
            r12.zzd(r3)
            int r1 = r2 + 8
            goto L_0x03f1
        L_0x0405:
            r1 = r4
        L_0x0406:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.ads.zzbsp.zza(java.lang.Object, byte[], int, int, int, int, int, int, long, int, long, com.google.android.gms.internal.ads.zzbpr):int");
    }

    /* JADX WARNING: type inference failed for: r10v4, types: [int] */
    /* JADX WARNING: type inference failed for: r10v11 */
    /* JADX WARNING: Multi-variable type inference failed */
    private final <K, V> int zza(T t, byte[] bArr, int i, int i2, int i3, long j, zzbpr zzbpr) throws IOException {
        Unsafe unsafe = zzfsh;
        Object zzfr = zzfr(i3);
        Object object = unsafe.getObject(t, j);
        if (this.zzfsy.zzy(object)) {
            Object zzaa = this.zzfsy.zzaa(zzfr);
            this.zzfsy.zzc(zzaa, object);
            unsafe.putObject(t, j, zzaa);
            object = zzaa;
        }
        zzbse zzab = this.zzfsy.zzab(zzfr);
        Map zzw = this.zzfsy.zzw(object);
        int zza = zzbpq.zza(bArr, i, zzbpr);
        int i4 = zzbpr.zzfld;
        if (i4 < 0 || i4 > i2 - zza) {
            throw zzbrl.zzanc();
        }
        int i5 = i4 + zza;
        K k = zzab.zzfsa;
        V v = zzab.zzfsc;
        while (zza < i5) {
            int i6 = zza + 1;
            byte b = bArr[zza];
            if (b < 0) {
                i6 = zzbpq.zza((int) b, bArr, i6, zzbpr);
                b = zzbpr.zzfld;
            }
            int i7 = i6;
            int i8 = b & 7;
            switch (b >>> 3) {
                case 1:
                    if (i8 == zzab.zzfrz.zzapk()) {
                        zza = zza(bArr, i7, i2, zzab.zzfrz, null, zzbpr);
                        k = zzbpr.zzflf;
                        break;
                    }
                case 2:
                    if (i8 == zzab.zzfsb.zzapk()) {
                        zza = zza(bArr, i7, i2, zzab.zzfsb, zzab.zzfsc.getClass(), zzbpr);
                        v = zzbpr.zzflf;
                        break;
                    }
                default:
                    zza = zzbpq.zza(b, bArr, i7, i2, zzbpr);
                    break;
            }
        }
        if (zza != i5) {
            throw zzbrl.zzanj();
        }
        zzw.put(k, v);
        return i5;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:65:0x019b, code lost:
        r12.putInt(r1, r13, r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:?, code lost:
        return r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:?, code lost:
        return r2;
     */
    private final int zza(T t, byte[] bArr, int i, int i2, int i3, int i4, int i5, int i6, int i7, long j, int i8, zzbpr zzbpr) throws IOException {
        int i9;
        T t2 = t;
        byte[] bArr2 = bArr;
        int i10 = i;
        int i11 = i3;
        int i12 = i4;
        int i13 = i5;
        long j2 = j;
        int i14 = i8;
        zzbpr zzbpr2 = zzbpr;
        Unsafe unsafe = zzfsh;
        long j3 = (long) (this.zzfsi[i14 + 2] & 1048575);
        switch (i7) {
            case 51:
                if (i13 == 1) {
                    unsafe.putObject(t2, j2, Double.valueOf(zzbpq.zzi(bArr, i)));
                    i9 = i10 + 8;
                    break;
                }
                break;
            case 52:
                if (i13 == 5) {
                    unsafe.putObject(t2, j2, Float.valueOf(zzbpq.zzj(bArr, i)));
                    i9 = i10 + 4;
                    break;
                }
                break;
            case 53:
            case 54:
                if (i13 == 0) {
                    i9 = zzbpq.zzb(bArr2, i10, zzbpr2);
                    unsafe.putObject(t2, j2, Long.valueOf(zzbpr2.zzfle));
                    break;
                }
                break;
            case 55:
            case 62:
                if (i13 == 0) {
                    i9 = zzbpq.zza(bArr2, i10, zzbpr2);
                    unsafe.putObject(t2, j2, Integer.valueOf(zzbpr2.zzfld));
                    break;
                }
                break;
            case 56:
            case 65:
                if (i13 == 1) {
                    unsafe.putObject(t2, j2, Long.valueOf(zzbpq.zzh(bArr, i)));
                    i9 = i10 + 8;
                    break;
                }
                break;
            case 57:
            case 64:
                if (i13 == 5) {
                    unsafe.putObject(t2, j2, Integer.valueOf(zzbpq.zzg(bArr, i)));
                    i9 = i10 + 4;
                    break;
                }
                break;
            case 58:
                if (i13 == 0) {
                    i9 = zzbpq.zzb(bArr2, i10, zzbpr2);
                    unsafe.putObject(t2, j2, Boolean.valueOf(zzbpr2.zzfle != 0));
                    break;
                }
                break;
            case 59:
                if (i13 == 2) {
                    int zza = zzbpq.zza(bArr2, i10, zzbpr2);
                    int i15 = zzbpr2.zzfld;
                    if (i15 == 0) {
                        unsafe.putObject(t2, j2, "");
                    } else if ((i6 & 536870912) == 0 || zzbuc.zzm(bArr2, zza, zza + i15)) {
                        unsafe.putObject(t2, j2, new String(bArr2, zza, i15, zzbrf.UTF_8));
                        zza += i15;
                    } else {
                        throw zzbrl.zzank();
                    }
                    unsafe.putInt(t2, j3, i12);
                    return zza;
                }
                break;
            case 60:
                if (i13 == 2) {
                    int zza2 = zza(zzfq(i14), bArr2, i10, i2, zzbpr2);
                    Object object = unsafe.getInt(t2, j3) == i12 ? unsafe.getObject(t2, j2) : null;
                    if (object == null) {
                        unsafe.putObject(t2, j2, zzbpr2.zzflf);
                    } else {
                        unsafe.putObject(t2, j2, zzbrf.zzb(object, zzbpr2.zzflf));
                    }
                    unsafe.putInt(t2, j3, i12);
                    return zza2;
                }
                break;
            case 61:
                if (i13 == 2) {
                    i9 = zzbpq.zze(bArr2, i10, zzbpr2);
                    unsafe.putObject(t2, j2, zzbpr2.zzflf);
                    break;
                }
                break;
            case 63:
                if (i13 == 0) {
                    int zza3 = zzbpq.zza(bArr2, i10, zzbpr2);
                    int i16 = zzbpr2.zzfld;
                    zzbri zzfs = zzfs(i14);
                    if (zzfs == null || zzfs.zzcb(i16)) {
                        unsafe.putObject(t2, j2, Integer.valueOf(i16));
                        i9 = zza3;
                        break;
                    } else {
                        zzad(t).zzc(i11, Long.valueOf((long) i16));
                        return zza3;
                    }
                }
                break;
            case 66:
                if (i13 == 0) {
                    i9 = zzbpq.zza(bArr2, i10, zzbpr2);
                    unsafe.putObject(t2, j2, Integer.valueOf(zzbqf.zzeu(zzbpr2.zzfld)));
                    break;
                }
                break;
            case 67:
                if (i13 == 0) {
                    i9 = zzbpq.zzb(bArr2, i10, zzbpr2);
                    unsafe.putObject(t2, j2, Long.valueOf(zzbqf.zzax(zzbpr2.zzfle)));
                    break;
                }
                break;
            case 68:
                if (i13 == 3) {
                    i9 = zza(zzfq(i14), bArr2, i10, i2, (i11 & -8) | 4, zzbpr2);
                    Object object2 = unsafe.getInt(t2, j3) == i12 ? unsafe.getObject(t2, j2) : null;
                    if (object2 != null) {
                        unsafe.putObject(t2, j2, zzbrf.zzb(object2, zzbpr2.zzflf));
                        break;
                    } else {
                        unsafe.putObject(t2, j2, zzbpr2.zzflf);
                        break;
                    }
                }
                break;
        }
    }

    private final zzbtc zzfq(int i) {
        int i2 = (i / 3) << 1;
        zzbtc zzbtc = (zzbtc) this.zzfsj[i2];
        if (zzbtc != null) {
            return zzbtc;
        }
        zzbtc zzf = zzbsy.zzaog().zzf((Class) this.zzfsj[i2 + 1]);
        this.zzfsj[i2] = zzf;
        return zzf;
    }

    private final Object zzfr(int i) {
        return this.zzfsj[(i / 3) << 1];
    }

    private final zzbri zzfs(int i) {
        return (zzbri) this.zzfsj[((i / 3) << 1) + 1];
    }

    /* JADX WARNING: type inference failed for: r33v0, types: [byte[]] */
    /* JADX WARNING: type inference failed for: r12v0 */
    /* JADX WARNING: type inference failed for: r12v1, types: [byte[]] */
    /* JADX WARNING: type inference failed for: r0v10, types: [byte, int] */
    /* JADX WARNING: type inference failed for: r5v2, types: [int] */
    /* JADX WARNING: type inference failed for: r12v2 */
    /* JADX WARNING: type inference failed for: r3v10 */
    /* JADX WARNING: type inference failed for: r12v3 */
    /* JADX WARNING: type inference failed for: r3v11 */
    /* JADX WARNING: type inference failed for: r7v6 */
    /* JADX WARNING: type inference failed for: r0v14, types: [int] */
    /* JADX WARNING: type inference failed for: r1v12, types: [byte[]] */
    /* JADX WARNING: type inference failed for: r12v4 */
    /* JADX WARNING: type inference failed for: r3v13 */
    /* JADX WARNING: type inference failed for: r20v0 */
    /* JADX WARNING: type inference failed for: r26v0 */
    /* JADX WARNING: type inference failed for: r7v8 */
    /* JADX WARNING: type inference failed for: r26v1 */
    /* JADX WARNING: type inference failed for: r12v5 */
    /* JADX WARNING: type inference failed for: r3v15 */
    /* JADX WARNING: type inference failed for: r2v15, types: [byte[]] */
    /* JADX WARNING: type inference failed for: r5v6, types: [int] */
    /* JADX WARNING: type inference failed for: r7v12 */
    /* JADX WARNING: type inference failed for: r12v7 */
    /* JADX WARNING: type inference failed for: r3v17 */
    /* JADX WARNING: type inference failed for: r2v17, types: [byte[]] */
    /* JADX WARNING: type inference failed for: r12v8 */
    /* JADX WARNING: type inference failed for: r3v19 */
    /* JADX WARNING: type inference failed for: r2v19, types: [byte[]] */
    /* JADX WARNING: type inference failed for: r5v8, types: [int] */
    /* JADX WARNING: type inference failed for: r12v10 */
    /* JADX WARNING: type inference failed for: r3v20 */
    /* JADX WARNING: type inference failed for: r7v18 */
    /* JADX WARNING: type inference failed for: r1v25, types: [int] */
    /* JADX WARNING: type inference failed for: r2v23, types: [byte[]] */
    /* JADX WARNING: type inference failed for: r12v11 */
    /* JADX WARNING: type inference failed for: r3v21 */
    /* JADX WARNING: type inference failed for: r26v2 */
    /* JADX WARNING: type inference failed for: r12v12 */
    /* JADX WARNING: type inference failed for: r3v22 */
    /* JADX WARNING: type inference failed for: r26v3 */
    /* JADX WARNING: type inference failed for: r26v4 */
    /* JADX WARNING: type inference failed for: r12v13 */
    /* JADX WARNING: type inference failed for: r26v5 */
    /* JADX WARNING: type inference failed for: r12v14, types: [byte[]] */
    /* JADX WARNING: type inference failed for: r26v6 */
    /* JADX WARNING: type inference failed for: r12v15, types: [byte[]] */
    /* JADX WARNING: type inference failed for: r26v7 */
    /* JADX WARNING: type inference failed for: r12v16, types: [byte[]] */
    /* JADX WARNING: type inference failed for: r26v8 */
    /* JADX WARNING: type inference failed for: r12v17, types: [byte[]] */
    /* JADX WARNING: type inference failed for: r3v28 */
    /* JADX WARNING: type inference failed for: r26v9 */
    /* JADX WARNING: type inference failed for: r3v30 */
    /* JADX WARNING: type inference failed for: r12v18, types: [byte[]] */
    /* JADX WARNING: type inference failed for: r26v10 */
    /* JADX WARNING: type inference failed for: r12v19 */
    /* JADX WARNING: type inference failed for: r3v31 */
    /* JADX WARNING: type inference failed for: r3v32 */
    /* JADX WARNING: type inference failed for: r12v20, types: [byte[]] */
    /* JADX WARNING: type inference failed for: r3v33 */
    /* JADX WARNING: type inference failed for: r12v21, types: [byte[]] */
    /* JADX WARNING: type inference failed for: r3v34 */
    /* JADX WARNING: type inference failed for: r12v22, types: [byte[]] */
    /* JADX WARNING: type inference failed for: r13v12 */
    /* JADX WARNING: type inference failed for: r12v23, types: [byte[]] */
    /* JADX WARNING: type inference failed for: r3v35 */
    /* JADX WARNING: type inference failed for: r3v36 */
    /* JADX WARNING: type inference failed for: r26v11 */
    /* JADX WARNING: type inference failed for: r13v16 */
    /* JADX WARNING: type inference failed for: r26v12 */
    /* JADX WARNING: type inference failed for: r12v24 */
    /* JADX WARNING: type inference failed for: r3v39 */
    /* JADX WARNING: type inference failed for: r13v18 */
    /* JADX WARNING: type inference failed for: r12v25 */
    /* JADX WARNING: type inference failed for: r3v40 */
    /* JADX WARNING: type inference failed for: r13v19 */
    /* JADX WARNING: type inference failed for: r12v26, types: [byte[]] */
    /* JADX WARNING: type inference failed for: r13v20, types: [int] */
    /* JADX WARNING: type inference failed for: r12v27, types: [byte[]] */
    /* JADX WARNING: type inference failed for: r13v21 */
    /* JADX WARNING: type inference failed for: r12v28, types: [byte[]] */
    /* JADX WARNING: type inference failed for: r26v13 */
    /* JADX WARNING: type inference failed for: r13v22 */
    /* JADX WARNING: type inference failed for: r12v29, types: [byte[]] */
    /* JADX WARNING: type inference failed for: r26v14 */
    /* JADX WARNING: type inference failed for: r3v43 */
    /* JADX WARNING: type inference failed for: r12v30 */
    /* JADX WARNING: type inference failed for: r26v15 */
    /* JADX WARNING: type inference failed for: r12v31 */
    /* JADX WARNING: type inference failed for: r1v56, types: [byte[]] */
    /* JADX WARNING: type inference failed for: r13v23 */
    /* JADX WARNING: type inference failed for: r26v16 */
    /* JADX WARNING: type inference failed for: r12v32 */
    /* JADX WARNING: type inference failed for: r7v40 */
    /* JADX WARNING: type inference failed for: r5v14 */
    /* JADX WARNING: type inference failed for: r3v46, types: [int] */
    /* JADX WARNING: type inference failed for: r5v15 */
    /* JADX WARNING: type inference failed for: r12v33 */
    /* JADX WARNING: type inference failed for: r12v34 */
    /* JADX WARNING: type inference failed for: r3v47 */
    /* JADX WARNING: type inference failed for: r12v35 */
    /* JADX WARNING: type inference failed for: r3v48 */
    /* JADX WARNING: type inference failed for: r7v41 */
    /* JADX WARNING: type inference failed for: r26v17 */
    /* JADX WARNING: type inference failed for: r26v18 */
    /* JADX WARNING: type inference failed for: r26v19 */
    /* JADX WARNING: type inference failed for: r12v36 */
    /* JADX WARNING: type inference failed for: r3v49 */
    /* JADX WARNING: type inference failed for: r12v37 */
    /* JADX WARNING: type inference failed for: r3v50 */
    /* JADX WARNING: type inference failed for: r12v38 */
    /* JADX WARNING: type inference failed for: r26v20 */
    /* JADX WARNING: type inference failed for: r26v21 */
    /* JADX WARNING: type inference failed for: r12v39 */
    /* JADX WARNING: type inference failed for: r12v40 */
    /* JADX WARNING: type inference failed for: r12v41 */
    /* JADX WARNING: type inference failed for: r12v42 */
    /* JADX WARNING: type inference failed for: r12v43 */
    /* JADX WARNING: type inference failed for: r12v44 */
    /* JADX WARNING: type inference failed for: r12v45 */
    /* JADX WARNING: type inference failed for: r3v51 */
    /* JADX WARNING: type inference failed for: r12v46 */
    /* JADX WARNING: type inference failed for: r12v47 */
    /* JADX WARNING: type inference failed for: r12v48 */
    /* JADX WARNING: type inference failed for: r12v49 */
    /* JADX WARNING: type inference failed for: r26v22 */
    /* JADX WARNING: type inference failed for: r12v50 */
    /* JADX WARNING: type inference failed for: r3v52 */
    /* JADX WARNING: type inference failed for: r12v51 */
    /* JADX WARNING: type inference failed for: r12v52 */
    /* JADX WARNING: type inference failed for: r12v53 */
    /* JADX WARNING: type inference failed for: r12v54 */
    /* JADX WARNING: type inference failed for: r12v55 */
    /* JADX WARNING: type inference failed for: r26v23 */
    /* JADX WARNING: type inference failed for: r12v56 */
    /* JADX WARNING: type inference failed for: r12v57 */
    /* JADX WARNING: Code restructure failed: missing block: B:163:0x0018, code lost:
        r12 = r12;
        r3 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0109, code lost:
        r3 = r21;
        r26 = r26;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x017c, code lost:
        r2 = r11;
        r3 = r13;
        r1 = r17;
        r12 = r12;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x0180, code lost:
        r7 = r25;
        r11 = r36;
        r13 = r35;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x0188, code lost:
        r3 = r2;
        r26 = r13;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x018b, code lost:
        r13 = r35;
        r26 = r26;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:79:0x022e, code lost:
        r2 = r11;
        r1 = r17;
        r7 = r25;
        r12 = r12;
        r3 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:0x025a, code lost:
        r26 = r3;
        r3 = r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:96:0x02d3, code lost:
        r2 = r11;
        r1 = r17;
        r26 = r26;
        r12 = r12;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:99:0x02de, code lost:
        r2 = r3;
        r18 = r6;
        r29 = r10;
        r19 = r11;
        r14 = r15;
        r26 = r26;
     */
    /* JADX WARNING: Incorrect type for immutable var: ssa=byte, code=null, for r0v10, types: [byte, int] */
    /* JADX WARNING: Incorrect type for immutable var: ssa=byte[], code=null, for r33v0, types: [byte[]] */
    /* JADX WARNING: Multi-variable type inference failed. Error: jadx.core.utils.exceptions.JadxRuntimeException: No candidate types for var: r12v2
  assigns: []
  uses: []
  mth insns count: 567
    	at jadx.core.dex.visitors.typeinference.TypeSearch.fillTypeCandidates(TypeSearch.java:237)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.typeinference.TypeSearch.run(TypeSearch.java:53)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.runMultiVariableSearch(TypeInferenceVisitor.java:99)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.visit(TypeInferenceVisitor.java:92)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:27)
    	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$1(DepthTraversal.java:14)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
    	at jadx.core.ProcessClass.process(ProcessClass.java:30)
    	at jadx.core.ProcessClass.lambda$processDependencies$0(ProcessClass.java:49)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:49)
    	at jadx.core.ProcessClass.process(ProcessClass.java:35)
    	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
    	at jadx.api.JavaClass.decompile(JavaClass.java:62)
    	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
     */
    /* JADX WARNING: Unknown variable types count: 58 */
    private final int zza(T t, byte[] r33, int i, int i2, int i3, zzbpr zzbpr) throws IOException {
        int i4;
        int i5;
        Unsafe unsafe;
        int i6;
        zzbsp zzbsp;
        int i7;
        ? r7;
        T t2;
        ? r5;
        int i8;
        int zzfw;
        ? r12;
        ? r3;
        ? r122;
        ? r32;
        int i9;
        int i10;
        int i11;
        int zza;
        int i12;
        int i13;
        int i14;
        int i15;
        ? r26;
        int i16;
        ? r123;
        ? r34;
        zzbsp zzbsp2;
        int zza2;
        int i17;
        int i18;
        int i19;
        ? r262;
        ? r124;
        int i20;
        int i21;
        int i22;
        ? r125;
        ? r35;
        int i23;
        ? r126;
        ? r13;
        int i24;
        int i25;
        ? r127;
        int i26;
        ? r263;
        zzbsp zzbsp3 = this;
        T t3 = t;
        ? r128 = r33;
        int i27 = i2;
        int i28 = i3;
        zzbpr zzbpr2 = zzbpr;
        Unsafe unsafe2 = zzfsh;
        int i29 = i;
        int i30 = 0;
        ? r36 = 0;
        int i31 = 0;
        int i32 = -1;
        int i33 = -1;
        while (true) {
            if (i29 < i27) {
                int i34 = i29 + 1;
                ? r0 = r128[i29];
                if (r0 < 0) {
                    i8 = zzbpq.zza((int) r0, (byte[]) r128, i34, zzbpr2);
                    r5 = zzbpr2.zzfld;
                } else {
                    r5 = r0;
                    i8 = i34;
                }
                int i35 = r5 >>> 3;
                int i36 = r5 & 7;
                if (i35 > i32) {
                    zzfw = zzbsp3.zzai(i35, i30 / 3);
                } else {
                    zzfw = zzbsp3.zzfw(i35);
                }
                int i37 = zzfw;
                if (i37 == -1) {
                    i10 = i35;
                    i7 = i8;
                    i4 = i31;
                    i5 = i33;
                    unsafe = unsafe2;
                    i6 = i28;
                    zzbsp = zzbsp3;
                    i9 = 0;
                    i11 = r5;
                } else {
                    int i38 = zzbsp3.zzfsi[i37 + 1];
                    int i39 = (i38 & 267386880) >>> 20;
                    ? r20 = r5;
                    int i40 = i8;
                    long j = (long) (i38 & 1048575);
                    if (i39 <= 17) {
                        int i41 = zzbsp3.zzfsi[i37 + 2];
                        boolean z = true;
                        int i42 = 1 << (i41 >>> 20);
                        int i43 = i41 & 1048575;
                        if (i43 != i33) {
                            if (i33 != -1) {
                                unsafe2.putInt(t3, (long) i33, i31);
                            }
                            i31 = unsafe2.getInt(t3, (long) i43);
                            i33 = i43;
                        }
                        switch (i39) {
                            case 0:
                                i21 = i37;
                                i10 = i35;
                                i5 = i33;
                                r262 = r20;
                                i22 = i40;
                                long j2 = j;
                                ? r129 = r33;
                                if (i36 == 1) {
                                    zzbua.zza((Object) t3, j2, zzbpq.zzi(r129, i22));
                                    zza2 = i22 + 8;
                                    i19 = i31 | i42;
                                    r125 = r129;
                                    break;
                                }
                                break;
                            case 1:
                                i21 = i37;
                                i10 = i35;
                                i5 = i33;
                                r262 = r20;
                                i22 = i40;
                                long j3 = j;
                                ? r1210 = r33;
                                if (i36 == 5) {
                                    zzbua.zza((Object) t3, j3, zzbpq.zzj(r1210, i22));
                                    zza2 = i22 + 4;
                                    i19 = i31 | i42;
                                    r125 = r1210;
                                    break;
                                }
                                break;
                            case 2:
                            case 3:
                                i21 = i37;
                                i10 = i35;
                                i5 = i33;
                                r262 = r20;
                                i22 = i40;
                                long j4 = j;
                                ? r1211 = r33;
                                if (i36 == 0) {
                                    int zzb = zzbpq.zzb(r1211, i22, zzbpr2);
                                    unsafe2.putLong(t3, j4, zzbpr2.zzfle);
                                    i19 = i31 | i42;
                                    i17 = i21;
                                    i18 = i10;
                                    zza2 = zzb;
                                    r124 = r1211;
                                    break;
                                }
                                break;
                            case 4:
                            case 11:
                                i21 = i37;
                                i10 = i35;
                                i5 = i33;
                                r262 = r20;
                                i22 = i40;
                                long j5 = j;
                                ? r1212 = r33;
                                if (i36 == 0) {
                                    zza2 = zzbpq.zza(r1212, i22, zzbpr2);
                                    unsafe2.putInt(t3, j5, zzbpr2.zzfld);
                                    i19 = i31 | i42;
                                    r125 = r1212;
                                    break;
                                }
                                break;
                            case 5:
                            case 14:
                                i21 = i37;
                                i10 = i35;
                                i5 = i33;
                                r35 = r20;
                                i23 = i40;
                                long j6 = j;
                                ? r1213 = r33;
                                if (i36 == 1) {
                                    long zzh = zzbpq.zzh(r1213, i23);
                                    int i44 = i23;
                                    r262 = r35;
                                    long j7 = j6;
                                    int i45 = i44;
                                    unsafe2.putLong(t3, j7, zzh);
                                    zza2 = i45 + 8;
                                    i19 = i31 | i42;
                                    r125 = r1213;
                                    break;
                                }
                                break;
                            case 6:
                            case 13:
                                i21 = i37;
                                i10 = i35;
                                i5 = i33;
                                r35 = r20;
                                i23 = i40;
                                long j8 = j;
                                ? r1214 = r33;
                                if (i36 == 5) {
                                    unsafe2.putInt(t3, j8, zzbpq.zzg(r1214, i23));
                                    i20 = i23 + 4;
                                    i31 |= i42;
                                    r126 = r1214;
                                    break;
                                }
                                break;
                            case 7:
                                i21 = i37;
                                i10 = i35;
                                i5 = i33;
                                r35 = r20;
                                i23 = i40;
                                long j9 = j;
                                ? r1215 = r33;
                                if (i36 == 0) {
                                    i20 = zzbpq.zzb(r1215, i23, zzbpr2);
                                    if (zzbpr2.zzfle == 0) {
                                        z = false;
                                    }
                                    zzbua.zza((Object) t3, j9, z);
                                    i31 |= i42;
                                    r126 = r1215;
                                    break;
                                }
                                break;
                            case 8:
                                i21 = i37;
                                i10 = i35;
                                i5 = i33;
                                r35 = r20;
                                i23 = i40;
                                long j10 = j;
                                ? r1216 = r33;
                                if (i36 == 2) {
                                    if ((536870912 & i38) == 0) {
                                        i20 = zzbpq.zzc(r1216, i23, zzbpr2);
                                    } else {
                                        i20 = zzbpq.zzd(r1216, i23, zzbpr2);
                                    }
                                    unsafe2.putObject(t3, j10, zzbpr2.zzflf);
                                    i31 |= i42;
                                    r126 = r1216;
                                    break;
                                }
                                break;
                            case 9:
                                i21 = i37;
                                i10 = i35;
                                i5 = i33;
                                ? r132 = r20;
                                i23 = i40;
                                long j11 = j;
                                ? r1217 = r33;
                                if (i36 != 2) {
                                    r35 = r132;
                                    int i46 = i2;
                                    break;
                                } else {
                                    r35 = r132;
                                    i27 = i2;
                                    i20 = zza(zzbsp3.zzfq(i21), (byte[]) r1217, i23, i27, zzbpr2);
                                    if ((i31 & i42) == 0) {
                                        unsafe2.putObject(t3, j11, zzbpr2.zzflf);
                                    } else {
                                        unsafe2.putObject(t3, j11, zzbrf.zzb(unsafe2.getObject(t3, j11), zzbpr2.zzflf));
                                    }
                                    i31 |= i42;
                                    r126 = r1217;
                                    break;
                                }
                            case 10:
                                i26 = i37;
                                i25 = i35;
                                i5 = i33;
                                r13 = r20;
                                i24 = i40;
                                long j12 = j;
                                ? r1218 = r33;
                                if (i36 == 2) {
                                    i29 = zzbpq.zze(r1218, i24, zzbpr2);
                                    unsafe2.putObject(t3, j12, zzbpr2.zzflf);
                                    i31 |= i42;
                                    r127 = r1218;
                                    break;
                                }
                                break;
                            case 12:
                                i26 = i37;
                                i25 = i35;
                                i5 = i33;
                                r13 = r20;
                                i24 = i40;
                                long j13 = j;
                                ? r1219 = r33;
                                if (i36 == 0) {
                                    i29 = zzbpq.zza(r1219, i24, zzbpr2);
                                    int i47 = zzbpr2.zzfld;
                                    zzbri zzfs = zzbsp3.zzfs(i26);
                                    if (zzfs != null && !zzfs.zzcb(i47)) {
                                        zzad(t).zzc(r13, Long.valueOf((long) i47));
                                        r127 = r1219;
                                        break;
                                    } else {
                                        unsafe2.putInt(t3, j13, i47);
                                        i31 |= i42;
                                        r127 = r1219;
                                        break;
                                    }
                                }
                                break;
                            case 15:
                                i26 = i37;
                                i25 = i35;
                                i5 = i33;
                                r13 = r20;
                                i24 = i40;
                                long j14 = j;
                                ? r1220 = r33;
                                if (i36 == 0) {
                                    i29 = zzbpq.zza(r1220, i24, zzbpr2);
                                    unsafe2.putInt(t3, j14, zzbqf.zzeu(zzbpr2.zzfld));
                                    i31 |= i42;
                                    r127 = r1220;
                                    break;
                                }
                                break;
                            case 16:
                                i21 = i37;
                                i10 = i35;
                                i5 = i33;
                                ? r133 = r20;
                                long j15 = j;
                                ? r1221 = r33;
                                if (i36 != 0) {
                                    r263 = r133;
                                    break;
                                } else {
                                    int zzb2 = zzbpq.zzb(r1221, i40, zzbpr2);
                                    unsafe2.putLong(t3, j15, zzbqf.zzax(zzbpr2.zzfle));
                                    i31 |= i42;
                                    i30 = i21;
                                    int i48 = r133;
                                    i32 = i10;
                                    i29 = zzb2;
                                    ? r1222 = r1221;
                                    break;
                                }
                            case 17:
                                if (i36 != 3) {
                                    i21 = i37;
                                    i10 = i35;
                                    i5 = i33;
                                    ? r1223 = r33;
                                    r263 = r20;
                                    break;
                                } else {
                                    int i49 = (i35 << 3) | 4;
                                    ? r1224 = r33;
                                    i26 = i37;
                                    i25 = i35;
                                    int i50 = i27;
                                    i5 = i33;
                                    long j16 = j;
                                    r13 = r20;
                                    i29 = zza(zzbsp3.zzfq(i37), (byte[]) r1224, i40, i50, i49, zzbpr2);
                                    if ((i31 & i42) == 0) {
                                        unsafe2.putObject(t3, j16, zzbpr2.zzflf);
                                    } else {
                                        unsafe2.putObject(t3, j16, zzbrf.zzb(unsafe2.getObject(t3, j16), zzbpr2.zzflf));
                                    }
                                    i31 |= i42;
                                    r127 = r1224;
                                    break;
                                }
                            default:
                                i21 = i37;
                                i10 = i35;
                                i5 = i33;
                                r262 = r20;
                                i22 = i40;
                                ? r1225 = r33;
                                break;
                        }
                    } else {
                        int i51 = i37;
                        i10 = i35;
                        i5 = i33;
                        int i52 = i39;
                        ? r264 = r20;
                        int i53 = i40;
                        long j17 = j;
                        if (i52 != 27) {
                            i4 = i31;
                            if (i52 <= 49) {
                                int i54 = i53;
                                unsafe = unsafe2;
                                i9 = i51;
                                i29 = zzbsp3.zza(t3, (byte[]) r128, i53, i27, (int) r264, i10, i36, i51, (long) i38, i52, j17, zzbpr);
                                if (i29 == i54) {
                                    i7 = i29;
                                    i11 = r264;
                                    i6 = i3;
                                    zzbsp = this;
                                } else {
                                    t3 = t;
                                    r12 = r33;
                                    i27 = i2;
                                    zzbpr2 = zzbpr;
                                    i32 = i10;
                                    i31 = i4;
                                    i30 = i9;
                                    i33 = i5;
                                    r3 = r264;
                                    unsafe2 = unsafe;
                                    i28 = i3;
                                    zzbsp3 = this;
                                    r128 = r12;
                                    r36 = r3;
                                }
                            } else {
                                int i55 = i36;
                                i16 = i53;
                                long j18 = j17;
                                unsafe = unsafe2;
                                i9 = i51;
                                int i56 = i52;
                                if (i56 != 50) {
                                    zzbsp2 = this;
                                    zza = zzbsp2.zza(t, (byte[]) r33, i16, i2, (int) r264, i10, i55, i38, i56, j18, i9, zzbpr);
                                    if (zza != i16) {
                                        r123 = r33;
                                        i27 = i2;
                                        zzbpr2 = zzbpr;
                                        r34 = r264;
                                        zzbsp3 = zzbsp2;
                                        i12 = i10;
                                        i13 = i4;
                                        i14 = i9;
                                        i15 = i5;
                                        unsafe2 = unsafe;
                                        i28 = i3;
                                        r122 = r123;
                                        r32 = r34;
                                        t3 = t;
                                        r12 = r122;
                                        r3 = r32;
                                        r128 = r12;
                                        r36 = r3;
                                    }
                                } else if (i55 == 2) {
                                    zzbsp2 = this;
                                    zza = zzbsp2.zza(t, (byte[]) r33, i16, i2, i9, j18, zzbpr);
                                    if (zza != i16) {
                                        r123 = r33;
                                        i27 = i2;
                                        zzbpr2 = zzbpr;
                                        zzbsp3 = zzbsp2;
                                        i12 = i10;
                                        i13 = i4;
                                        i14 = i9;
                                        i15 = i5;
                                        r34 = r264;
                                        unsafe2 = unsafe;
                                        i28 = i3;
                                        r122 = r123;
                                        r32 = r34;
                                        t3 = t;
                                        r12 = r122;
                                        r3 = r32;
                                        r128 = r12;
                                        r36 = r3;
                                    }
                                } else {
                                    zzbsp = this;
                                }
                                i7 = zza;
                                r26 = r264;
                                i11 = r26;
                                i6 = i3;
                            }
                        } else if (i36 == 2) {
                            zzbrk zzbrk = (zzbrk) unsafe2.getObject(t3, j17);
                            if (!zzbrk.zzaki()) {
                                int size = zzbrk.size();
                                zzbrk = zzbrk.zzel(size == 0 ? 10 : size << 1);
                                unsafe2.putObject(t3, j17, zzbrk);
                            }
                            int i57 = i31;
                            zza2 = zza(zzbsp3.zzfq(i51), (int) r264, (byte[]) r128, i53, i27, zzbrk, zzbpr2);
                            i17 = i51;
                            i18 = i10;
                            i19 = i57;
                            r124 = r128;
                            r262 = r264;
                        } else {
                            i4 = i31;
                            unsafe = unsafe2;
                            i9 = i51;
                            zzbsp = zzbsp3;
                            i16 = i53;
                        }
                        i7 = i16;
                        r26 = r264;
                        i11 = r26;
                        i6 = i3;
                    }
                    i33 = i5;
                    ? r37 = r262;
                    ? r1226 = r124;
                    i28 = i3;
                    r12 = r1226;
                    r3 = r37;
                    r128 = r12;
                    r36 = r3;
                }
                if (i11 != i6 || i6 == 0) {
                    zza = zza((int) i11, (byte[]) r33, i7, i2, (Object) t, zzbpr);
                    r122 = r33;
                    i27 = i2;
                    zzbpr2 = zzbpr;
                    i28 = i6;
                    r32 = i11;
                    zzbsp3 = zzbsp;
                    i12 = i10;
                    i13 = i4;
                    i14 = i9;
                    i15 = i5;
                    unsafe2 = unsafe;
                    t3 = t;
                    r12 = r122;
                    r3 = r32;
                    r128 = r12;
                    r36 = r3;
                } else {
                    r7 = i11;
                }
            } else {
                i4 = i31;
                i5 = i33;
                unsafe = unsafe2;
                i6 = i28;
                zzbsp = zzbsp3;
                i7 = i29;
                r7 = r36;
            }
        }
        int i58 = i4;
        int i59 = i5;
        if (i59 != -1) {
            long j19 = (long) i59;
            t2 = t;
            unsafe.putInt(t2, j19, i58);
        } else {
            t2 = t;
        }
        zzbtv zzbtv = null;
        for (int i60 = zzbsp.zzfss; i60 < zzbsp.zzfst; i60++) {
            zzbtv = (zzbtv) zzbsp.zza((Object) t2, zzbsp.zzfsr[i60], (UB) zzbtv, zzbsp.zzfsw);
        }
        if (zzbtv != null) {
            zzbsp.zzfsw.zzg(t2, zzbtv);
        }
        if (i6 == 0) {
            if (i7 != i2) {
                throw zzbrl.zzanj();
            }
        } else if (i7 > i2 || r7 != i6) {
            throw zzbrl.zzanj();
        }
        return i7;
    }

    /* JADX WARNING: type inference failed for: r30v0, types: [byte[]] */
    /* JADX WARNING: type inference failed for: r12v0 */
    /* JADX WARNING: type inference failed for: r2v0, types: [byte[]] */
    /* JADX WARNING: type inference failed for: r12v1, types: [byte[]] */
    /* JADX WARNING: type inference failed for: r0v5, types: [byte, int] */
    /* JADX WARNING: type inference failed for: r17v0, types: [int] */
    /* JADX WARNING: type inference failed for: r12v2 */
    /* JADX WARNING: type inference failed for: r12v3 */
    /* JADX WARNING: type inference failed for: r0v10, types: [int] */
    /* JADX WARNING: type inference failed for: r1v5, types: [byte[]] */
    /* JADX WARNING: type inference failed for: r2v7, types: [byte[]] */
    /* JADX WARNING: type inference failed for: r5v3, types: [int] */
    /* JADX WARNING: type inference failed for: r2v8, types: [byte[]] */
    /* JADX WARNING: type inference failed for: r2v10, types: [byte[]] */
    /* JADX WARNING: type inference failed for: r5v5, types: [int] */
    /* JADX WARNING: type inference failed for: r12v6 */
    /* JADX WARNING: type inference failed for: r1v12, types: [int] */
    /* JADX WARNING: type inference failed for: r2v12, types: [byte[]] */
    /* JADX WARNING: type inference failed for: r17v1 */
    /* JADX WARNING: type inference failed for: r3v13, types: [int] */
    /* JADX WARNING: type inference failed for: r17v2 */
    /* JADX WARNING: type inference failed for: r12v7 */
    /* JADX WARNING: type inference failed for: r12v8 */
    /* JADX WARNING: Code restructure failed: missing block: B:100:0x0241, code lost:
        if (r0 == r14) goto L_0x01de;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x0116, code lost:
        r2 = r4;
        r1 = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x0148, code lost:
        r0 = r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x0166, code lost:
        r1 = r7;
        r2 = r10;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x016b, code lost:
        r25 = r7;
        r14 = r8;
        r18 = r9;
        r19 = r10;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:96:0x0220, code lost:
        if (r0 == r14) goto L_0x01de;
     */
    /* JADX WARNING: Incorrect type for immutable var: ssa=byte, code=null, for r0v5, types: [byte, int] */
    /* JADX WARNING: Incorrect type for immutable var: ssa=byte[], code=null, for r30v0, types: [byte[]] */
    /* JADX WARNING: Multi-variable type inference failed. Error: jadx.core.utils.exceptions.JadxRuntimeException: No candidate types for var: r12v2
  assigns: []
  uses: []
  mth insns count: 286
    	at jadx.core.dex.visitors.typeinference.TypeSearch.fillTypeCandidates(TypeSearch.java:237)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.typeinference.TypeSearch.run(TypeSearch.java:53)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.runMultiVariableSearch(TypeInferenceVisitor.java:99)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.visit(TypeInferenceVisitor.java:92)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:27)
    	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$1(DepthTraversal.java:14)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
    	at jadx.core.ProcessClass.process(ProcessClass.java:30)
    	at jadx.core.ProcessClass.lambda$processDependencies$0(ProcessClass.java:49)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:49)
    	at jadx.core.ProcessClass.process(ProcessClass.java:35)
    	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
    	at jadx.api.JavaClass.decompile(JavaClass.java:62)
    	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
     */
    /* JADX WARNING: Unknown variable types count: 16 */
    public final void zza(T t, byte[] r30, int i, int i2, zzbpr zzbpr) throws IOException {
        ? r17;
        int i3;
        int zzfw;
        ? r12;
        int i4;
        int i5;
        int i6;
        Unsafe unsafe;
        int i7;
        int i8;
        int i9;
        int i10;
        int i11;
        zzbsp zzbsp = this;
        T t2 = t;
        ? r122 = r30;
        int i12 = i2;
        zzbpr zzbpr2 = zzbpr;
        if (zzbsp.zzfsp) {
            Unsafe unsafe2 = zzfsh;
            int i13 = -1;
            int i14 = i;
            int i15 = -1;
            int i16 = 0;
            while (i14 < i12) {
                int i17 = i14 + 1;
                ? r0 = r122[i14];
                if (r0 < 0) {
                    i3 = zzbpq.zza((int) r0, (byte[]) r122, i17, zzbpr2);
                    r17 = zzbpr2.zzfld;
                } else {
                    r17 = r0;
                    i3 = i17;
                }
                int i18 = r17 >>> 3;
                int i19 = r17 & 7;
                if (i18 > i15) {
                    zzfw = zzbsp.zzai(i18, i16 / 3);
                } else {
                    zzfw = zzbsp.zzfw(i18);
                }
                int i20 = zzfw;
                if (i20 == i13) {
                    i5 = i18;
                    i7 = i3;
                    unsafe = unsafe2;
                    i4 = i13;
                    i6 = 0;
                } else {
                    int i21 = zzbsp.zzfsi[i20 + 1];
                    int i22 = (267386880 & i21) >>> 20;
                    long j = (long) (1048575 & i21);
                    if (i22 <= 17) {
                        boolean z = true;
                        switch (i22) {
                            case 0:
                                long j2 = j;
                                i9 = i20;
                                if (i19 == 1) {
                                    zzbua.zza((Object) t2, j2, zzbpq.zzi(r122, i3));
                                    i10 = i3 + 8;
                                    break;
                                }
                                break;
                            case 1:
                                long j3 = j;
                                i9 = i20;
                                if (i19 == 5) {
                                    zzbua.zza((Object) t2, j3, zzbpq.zzj(r122, i3));
                                    i10 = i3 + 4;
                                    break;
                                }
                                break;
                            case 2:
                            case 3:
                                long j4 = j;
                                i9 = i20;
                                if (i19 == 0) {
                                    i11 = zzbpq.zzb(r122, i3, zzbpr2);
                                    unsafe2.putLong(t2, j4, zzbpr2.zzfle);
                                    break;
                                }
                                break;
                            case 4:
                            case 11:
                                long j5 = j;
                                i9 = i20;
                                if (i19 == 0) {
                                    i10 = zzbpq.zza(r122, i3, zzbpr2);
                                    unsafe2.putInt(t2, j5, zzbpr2.zzfld);
                                    break;
                                }
                                break;
                            case 5:
                            case 14:
                                long j6 = j;
                                if (i19 == 1) {
                                    i9 = i20;
                                    unsafe2.putLong(t2, j6, zzbpq.zzh(r122, i3));
                                    i10 = i3 + 8;
                                    break;
                                }
                                break;
                            case 6:
                            case 13:
                                long j7 = j;
                                if (i19 == 5) {
                                    unsafe2.putInt(t2, j7, zzbpq.zzg(r122, i3));
                                    i14 = i3 + 4;
                                    break;
                                }
                                break;
                            case 7:
                                long j8 = j;
                                if (i19 == 0) {
                                    int zzb = zzbpq.zzb(r122, i3, zzbpr2);
                                    if (zzbpr2.zzfle == 0) {
                                        z = false;
                                    }
                                    zzbua.zza((Object) t2, j8, z);
                                    i14 = zzb;
                                    break;
                                }
                                break;
                            case 8:
                                long j9 = j;
                                if (i19 == 2) {
                                    if ((536870912 & i21) == 0) {
                                        i14 = zzbpq.zzc(r122, i3, zzbpr2);
                                    } else {
                                        i14 = zzbpq.zzd(r122, i3, zzbpr2);
                                    }
                                    unsafe2.putObject(t2, j9, zzbpr2.zzflf);
                                    break;
                                }
                                break;
                            case 9:
                                long j10 = j;
                                if (i19 == 2) {
                                    i14 = zza(zzbsp.zzfq(i20), (byte[]) r122, i3, i12, zzbpr2);
                                    Object object = unsafe2.getObject(t2, j10);
                                    if (object != null) {
                                        unsafe2.putObject(t2, j10, zzbrf.zzb(object, zzbpr2.zzflf));
                                        break;
                                    } else {
                                        unsafe2.putObject(t2, j10, zzbpr2.zzflf);
                                        break;
                                    }
                                }
                                break;
                            case 10:
                                long j11 = j;
                                if (i19 == 2) {
                                    i14 = zzbpq.zze(r122, i3, zzbpr2);
                                    unsafe2.putObject(t2, j11, zzbpr2.zzflf);
                                    break;
                                }
                                break;
                            case 12:
                                long j12 = j;
                                i9 = i20;
                                if (i19 == 0) {
                                    i10 = zzbpq.zza(r122, i3, zzbpr2);
                                    unsafe2.putInt(t2, j12, zzbpr2.zzfld);
                                    break;
                                }
                                break;
                            case 15:
                                long j13 = j;
                                i9 = i20;
                                if (i19 == 0) {
                                    i10 = zzbpq.zza(r122, i3, zzbpr2);
                                    unsafe2.putInt(t2, j13, zzbqf.zzeu(zzbpr2.zzfld));
                                    break;
                                }
                                break;
                            case 16:
                                if (i19 == 0) {
                                    i11 = zzbpq.zzb(r122, i3, zzbpr2);
                                    i9 = i20;
                                    unsafe2.putLong(t2, j, zzbqf.zzax(zzbpr2.zzfle));
                                    break;
                                }
                                break;
                        }
                    } else if (i22 != 27) {
                        i6 = i20;
                        if (i22 <= 49) {
                            long j14 = (long) i21;
                            int i23 = i19;
                            i5 = i18;
                            int i24 = i3;
                            unsafe = unsafe2;
                            i4 = -1;
                            i14 = zzbsp.zza(t2, (byte[]) r122, i3, i12, (int) r17, i18, i23, i6, j14, i22, j, zzbpr);
                            if (i14 != i24) {
                                t2 = t;
                                r12 = r30;
                                i12 = i2;
                                zzbpr2 = zzbpr;
                                unsafe2 = unsafe;
                                i16 = i6;
                                i15 = i5;
                                i13 = -1;
                                zzbsp = this;
                                r122 = r12;
                            }
                        } else {
                            long j15 = j;
                            int i25 = i19;
                            i5 = i18;
                            int i26 = i3;
                            unsafe = unsafe2;
                            i4 = -1;
                            int i27 = i22;
                            if (i27 != 50) {
                                int i28 = i26;
                                i14 = zza(t, (byte[]) r30, i28, i2, (int) r17, i5, i25, i21, i27, j15, i6, zzbpr);
                            } else if (i25 == 2) {
                                int i29 = i26;
                                i14 = zza(t, (byte[]) r30, i29, i2, i6, j15, zzbpr);
                            } else {
                                i8 = i26;
                                i7 = i8;
                            }
                        }
                        i7 = i14;
                    } else if (i19 == 2) {
                        zzbrk zzbrk = (zzbrk) unsafe2.getObject(t2, j);
                        if (!zzbrk.zzaki()) {
                            int size = zzbrk.size();
                            zzbrk = zzbrk.zzel(size == 0 ? 10 : size << 1);
                            unsafe2.putObject(t2, j, zzbrk);
                        }
                        int i30 = i20;
                        i14 = zza(zzbsp.zzfq(i20), (int) r17, (byte[]) r122, i3, i12, zzbrk, zzbpr2);
                        i15 = i18;
                        i16 = i30;
                        i13 = -1;
                        r12 = r122;
                        r122 = r12;
                    }
                    i6 = i20;
                    i5 = i18;
                    i8 = i3;
                    unsafe = unsafe2;
                    i4 = -1;
                    i7 = i8;
                }
                i14 = zza((int) r17, (byte[]) r30, i7, i2, (Object) t, zzbpr);
                zzbsp = this;
                t2 = t;
                r12 = r30;
                i12 = i2;
                zzbpr2 = zzbpr;
                unsafe2 = unsafe;
                i16 = i6;
                i15 = i5;
                i13 = i4;
                r122 = r12;
            }
            if (i14 != i12) {
                throw zzbrl.zzanj();
            }
            return;
        }
        zza(t, (byte[]) r30, i, i12, 0, zzbpr);
    }

    public final void zzs(T t) {
        for (int i = this.zzfss; i < this.zzfst; i++) {
            long zzft = (long) (zzft(this.zzfsr[i]) & 1048575);
            Object zzp = zzbua.zzp(t, zzft);
            if (zzp != null) {
                zzbua.zza((Object) t, zzft, this.zzfsy.zzz(zzp));
            }
        }
        int length = this.zzfsr.length;
        for (int i2 = this.zzfst; i2 < length; i2++) {
            this.zzfsv.zzb(t, (long) this.zzfsr[i2]);
        }
        this.zzfsw.zzs(t);
        if (this.zzfsn) {
            this.zzfsx.zzs(t);
        }
    }

    private final <UT, UB> UB zza(Object obj, int i, UB ub, zzbtu<UT, UB> zzbtu) {
        int i2 = this.zzfsi[i];
        Object zzp = zzbua.zzp(obj, (long) (zzft(i) & 1048575));
        if (zzp == null) {
            return ub;
        }
        zzbri zzfs = zzfs(i);
        if (zzfs == null) {
            return ub;
        }
        return zza(i, i2, this.zzfsy.zzw(zzp), zzfs, ub, zzbtu);
    }

    private final <K, V, UT, UB> UB zza(int i, int i2, Map<K, V> map, zzbri zzbri, UB ub, zzbtu<UT, UB> zzbtu) {
        zzbse zzab = this.zzfsy.zzab(zzfr(i));
        Iterator it = map.entrySet().iterator();
        while (it.hasNext()) {
            Entry entry = (Entry) it.next();
            if (!zzbri.zzcb(((Integer) entry.getValue()).intValue())) {
                if (ub == null) {
                    ub = zzbtu.zzaoy();
                }
                zzbqb zzen = zzbpu.zzen(zzbsd.zza(zzab, entry.getKey(), entry.getValue()));
                try {
                    zzbsd.zza(zzen.zzakt(), zzab, entry.getKey(), entry.getValue());
                    zzbtu.zza(ub, i2, zzen.zzaks());
                    it.remove();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        }
        return ub;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:83:0x0104, code lost:
        continue;
     */
    public final boolean zzae(T t) {
        int i;
        int i2 = 0;
        int i3 = -1;
        int i4 = 0;
        while (true) {
            boolean z = true;
            if (i4 >= this.zzfss) {
                return !this.zzfsn || this.zzfsx.zzq(t).isInitialized();
            }
            int i5 = this.zzfsr[i4];
            int i6 = this.zzfsi[i5];
            int zzft = zzft(i5);
            if (!this.zzfsp) {
                int i7 = this.zzfsi[i5 + 2];
                int i8 = i7 & 1048575;
                i = 1 << (i7 >>> 20);
                if (i8 != i3) {
                    i2 = zzfsh.getInt(t, (long) i8);
                    i3 = i8;
                }
            } else {
                i = 0;
            }
            if (((268435456 & zzft) != 0) && !zza(t, i5, i2, i)) {
                return false;
            }
            int i9 = (267386880 & zzft) >>> 20;
            if (i9 != 9 && i9 != 17) {
                if (i9 != 27) {
                    if (i9 != 60 && i9 != 68) {
                        switch (i9) {
                            case 49:
                                break;
                            case 50:
                                Map zzx = this.zzfsy.zzx(zzbua.zzp(t, (long) (zzft & 1048575)));
                                if (!zzx.isEmpty()) {
                                    if (this.zzfsy.zzab(zzfr(i5)).zzfsb.zzapj() == zzbuo.MESSAGE) {
                                        zzbtc zzbtc = null;
                                        Iterator it = zzx.values().iterator();
                                        while (true) {
                                            if (it.hasNext()) {
                                                Object next = it.next();
                                                if (zzbtc == null) {
                                                    zzbtc = zzbsy.zzaog().zzf(next.getClass());
                                                }
                                                if (!zzbtc.zzae(next)) {
                                                    z = false;
                                                }
                                            }
                                        }
                                    }
                                }
                                if (!z) {
                                    return false;
                                }
                                continue;
                        }
                    } else if (zza(t, i6, i5) && !zza((Object) t, zzft, zzfq(i5))) {
                        return false;
                    }
                }
                List list = (List) zzbua.zzp(t, (long) (zzft & 1048575));
                if (!list.isEmpty()) {
                    zzbtc zzfq = zzfq(i5);
                    int i10 = 0;
                    while (true) {
                        if (i10 < list.size()) {
                            if (!zzfq.zzae(list.get(i10))) {
                                z = false;
                            } else {
                                i10++;
                            }
                        }
                    }
                }
                if (!z) {
                    return false;
                }
            } else if (zza(t, i5, i2, i) && !zza((Object) t, zzft, zzfq(i5))) {
                return false;
            }
            i4++;
        }
    }

    private static boolean zza(Object obj, int i, zzbtc zzbtc) {
        return zzbtc.zzae(zzbua.zzp(obj, (long) (i & 1048575)));
    }

    private static void zza(int i, Object obj, zzbup zzbup) throws IOException {
        if (obj instanceof String) {
            zzbup.zzf(i, (String) obj);
        } else {
            zzbup.zza(i, (zzbpu) obj);
        }
    }

    private final void zza(Object obj, int i, zzbtb zzbtb) throws IOException {
        if (zzfv(i)) {
            zzbua.zza(obj, (long) (i & 1048575), (Object) zzbtb.zzalb());
        } else if (this.zzfso) {
            zzbua.zza(obj, (long) (i & 1048575), (Object) zzbtb.readString());
        } else {
            zzbua.zza(obj, (long) (i & 1048575), (Object) zzbtb.zzalc());
        }
    }

    private final int zzft(int i) {
        return this.zzfsi[i + 1];
    }

    private final int zzfu(int i) {
        return this.zzfsi[i + 2];
    }

    private static <T> double zzf(T t, long j) {
        return ((Double) zzbua.zzp(t, j)).doubleValue();
    }

    private static <T> float zzg(T t, long j) {
        return ((Float) zzbua.zzp(t, j)).floatValue();
    }

    private static <T> int zzh(T t, long j) {
        return ((Integer) zzbua.zzp(t, j)).intValue();
    }

    private static <T> long zzi(T t, long j) {
        return ((Long) zzbua.zzp(t, j)).longValue();
    }

    private static <T> boolean zzj(T t, long j) {
        return ((Boolean) zzbua.zzp(t, j)).booleanValue();
    }

    private final boolean zzc(T t, T t2, int i) {
        return zzd(t, i) == zzd(t2, i);
    }

    private final boolean zza(T t, int i, int i2, int i3) {
        if (this.zzfsp) {
            return zzd(t, i);
        }
        return (i2 & i3) != 0;
    }

    private final boolean zzd(T t, int i) {
        if (this.zzfsp) {
            int zzft = zzft(i);
            long j = (long) (zzft & 1048575);
            switch ((zzft & 267386880) >>> 20) {
                case 0:
                    return zzbua.zzo(t, j) != Utils.DOUBLE_EPSILON;
                case 1:
                    return zzbua.zzn(t, j) != 0.0f;
                case 2:
                    return zzbua.zzl(t, j) != 0;
                case 3:
                    return zzbua.zzl(t, j) != 0;
                case 4:
                    return zzbua.zzk(t, j) != 0;
                case 5:
                    return zzbua.zzl(t, j) != 0;
                case 6:
                    return zzbua.zzk(t, j) != 0;
                case 7:
                    return zzbua.zzm(t, j);
                case 8:
                    Object zzp = zzbua.zzp(t, j);
                    if (zzp instanceof String) {
                        return !((String) zzp).isEmpty();
                    }
                    if (zzp instanceof zzbpu) {
                        return !zzbpu.zzfli.equals(zzp);
                    }
                    throw new IllegalArgumentException();
                case 9:
                    return zzbua.zzp(t, j) != null;
                case 10:
                    return !zzbpu.zzfli.equals(zzbua.zzp(t, j));
                case 11:
                    return zzbua.zzk(t, j) != 0;
                case 12:
                    return zzbua.zzk(t, j) != 0;
                case 13:
                    return zzbua.zzk(t, j) != 0;
                case 14:
                    return zzbua.zzl(t, j) != 0;
                case 15:
                    return zzbua.zzk(t, j) != 0;
                case 16:
                    return zzbua.zzl(t, j) != 0;
                case 17:
                    return zzbua.zzp(t, j) != null;
                default:
                    throw new IllegalArgumentException();
            }
        } else {
            int zzfu = zzfu(i);
            return (zzbua.zzk(t, (long) (zzfu & 1048575)) & (1 << (zzfu >>> 20))) != 0;
        }
    }

    private final void zze(T t, int i) {
        if (!this.zzfsp) {
            int zzfu = zzfu(i);
            long j = (long) (zzfu & 1048575);
            zzbua.zzb((Object) t, j, zzbua.zzk(t, j) | (1 << (zzfu >>> 20)));
        }
    }

    private final boolean zza(T t, int i, int i2) {
        return zzbua.zzk(t, (long) (zzfu(i2) & 1048575)) == i;
    }

    private final void zzb(T t, int i, int i2) {
        zzbua.zzb((Object) t, (long) (zzfu(i2) & 1048575), i);
    }

    private final int zzfw(int i) {
        if (i < this.zzfsk || i > this.zzfsl) {
            return -1;
        }
        return zzaj(i, 0);
    }

    private final int zzai(int i, int i2) {
        if (i < this.zzfsk || i > this.zzfsl) {
            return -1;
        }
        return zzaj(i, i2);
    }

    private final int zzaj(int i, int i2) {
        int length = (this.zzfsi.length / 3) - 1;
        while (i2 <= length) {
            int i3 = (length + i2) >>> 1;
            int i4 = i3 * 3;
            int i5 = this.zzfsi[i4];
            if (i == i5) {
                return i4;
            }
            if (i < i5) {
                length = i3 - 1;
            } else {
                i2 = i3 + 1;
            }
        }
        return -1;
    }
}
