package com.google.android.gms.internal.ads;

import android.os.SystemClock;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeSet;

public class zzaj implements zzm {
    private static final boolean DEBUG = zzaf.DEBUG;
    @Deprecated
    private final zzar zzbo;
    private final zzai zzbp;
    private final zzak zzbq;

    @Deprecated
    public zzaj(zzar zzar) {
        this(zzar, new zzak(4096));
    }

    @Deprecated
    private zzaj(zzar zzar, zzak zzak) {
        this.zzbo = zzar;
        this.zzbp = new zzah(zzar);
        this.zzbq = zzak;
    }

    public zzaj(zzai zzai) {
        this(zzai, new zzak(4096));
    }

    private zzaj(zzai zzai, zzak zzak) {
        this.zzbp = zzai;
        this.zzbo = zzai;
        this.zzbq = zzak;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:100:0x01cb, code lost:
        if (r13 != null) goto L_0x01cd;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:101:0x01cd, code lost:
        r11 = new com.google.android.gms.internal.ads.zzp(r5, r13, false, android.os.SystemClock.elapsedRealtime() - r3, r17);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:102:0x01dd, code lost:
        if (r5 == 401) goto L_0x0206;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:106:0x01e6, code lost:
        if (r5 < 400) goto L_0x01f2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:110:0x01f1, code lost:
        throw new com.google.android.gms.internal.ads.zzg(r11);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:112:0x01f4, code lost:
        if (r5 < 500) goto L_0x0200;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:116:0x01ff, code lost:
        throw new com.google.android.gms.internal.ads.zzac(r11);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:118:0x0205, code lost:
        throw new com.google.android.gms.internal.ads.zzac(r11);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:119:0x0206, code lost:
        zza("auth", r2, new com.google.android.gms.internal.ads.zza(r11));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:120:0x0212, code lost:
        zza("network", r2, new com.google.android.gms.internal.ads.zzo());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:122:0x0223, code lost:
        throw new com.google.android.gms.internal.ads.zzq(r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:123:0x0224, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:124:0x0225, code lost:
        r3 = r0;
        r5 = "Bad URL ";
        r2 = java.lang.String.valueOf(r24.getUrl());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:125:0x0236, code lost:
        if (r2.length() != 0) goto L_0x0238;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:126:0x0238, code lost:
        r2 = r5.concat(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:127:0x023d, code lost:
        r2 = new java.lang.String(r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:129:0x0245, code lost:
        throw new java.lang.RuntimeException(r2, r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:130:0x0246, code lost:
        zza("socket", r2, new com.google.android.gms.internal.ads.zzad());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x0114, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x0115, code lost:
        r5 = r0;
        r17 = r11;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x0118, code lost:
        r13 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:86:0x0197, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:87:0x0199, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:88:0x019a, code lost:
        r7 = r11;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:89:0x019b, code lost:
        r13 = r5;
        r17 = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:90:0x019f, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:91:0x01a0, code lost:
        r5 = r0;
        r17 = r11;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:92:0x01a6, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:93:0x01a7, code lost:
        r17 = r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:94:0x01aa, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:95:0x01ab, code lost:
        r17 = r5;
        r10 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:96:0x01ae, code lost:
        r13 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:99:0x01b2, code lost:
        r5 = r10.getStatusCode();
        com.google.android.gms.internal.ads.zzaf.e("Unexpected response code %d for %s", java.lang.Integer.valueOf(r5), r24.getUrl());
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:123:0x0224 A[ExcHandler: MalformedURLException (r0v0 'e' java.net.MalformedURLException A[CUSTOM_DECLARE]), Splitter:B:2:0x000d] */
    /* JADX WARNING: Removed duplicated region for block: B:131:? A[ExcHandler: SocketTimeoutException (unused java.net.SocketTimeoutException), SYNTHETIC, Splitter:B:2:0x000d] */
    /* JADX WARNING: Removed duplicated region for block: B:133:0x021e A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:99:0x01b2  */
    public zzp zzc(zzr<?> zzr) throws zzae {
        zzaq zzaq;
        Map map;
        List<zzl> zzr2;
        byte[] bArr;
        zzr<?> zzr3 = zzr;
        long elapsedRealtime = SystemClock.elapsedRealtime();
        while (true) {
            List emptyList = Collections.emptyList();
            try {
                zzc zzg = zzr.zzg();
                if (zzg == null) {
                    map = Collections.emptyMap();
                } else {
                    Map hashMap = new HashMap();
                    if (zzg.zza != null) {
                        hashMap.put(HttpRequest.HEADER_IF_NONE_MATCH, zzg.zza);
                    }
                    if (zzg.zzc > 0) {
                        hashMap.put("If-Modified-Since", zzap.zzb(zzg.zzc));
                    }
                    map = hashMap;
                }
                zzaq = this.zzbp.zza(zzr3, map);
                int statusCode = zzaq.getStatusCode();
                zzr2 = zzaq.zzr();
                if (statusCode == 304) {
                    zzc zzg2 = zzr.zzg();
                    if (zzg2 == null) {
                        zzp zzp = new zzp(304, (byte[]) null, true, SystemClock.elapsedRealtime() - elapsedRealtime, zzr2);
                        return zzp;
                    }
                    TreeSet treeSet = new TreeSet(String.CASE_INSENSITIVE_ORDER);
                    if (!zzr2.isEmpty()) {
                        for (zzl name : zzr2) {
                            treeSet.add(name.getName());
                        }
                    }
                    ArrayList arrayList = new ArrayList(zzr2);
                    if (zzg2.zzg != null) {
                        if (!zzg2.zzg.isEmpty()) {
                            for (zzl zzl : zzg2.zzg) {
                                if (!treeSet.contains(zzl.getName())) {
                                    arrayList.add(zzl);
                                }
                            }
                        }
                    } else if (!zzg2.zzf.isEmpty()) {
                        for (Entry entry : zzg2.zzf.entrySet()) {
                            if (!treeSet.contains(entry.getKey())) {
                                arrayList.add(new zzl((String) entry.getKey(), (String) entry.getValue()));
                            }
                        }
                    }
                    zzp zzp2 = new zzp(304, zzg2.data, true, SystemClock.elapsedRealtime() - elapsedRealtime, (List<zzl>) arrayList);
                    return zzp2;
                }
                InputStream content = zzaq.getContent();
                if (content != null) {
                    bArr = zza(content, zzaq.getContentLength());
                } else {
                    bArr = new byte[0];
                }
                long elapsedRealtime2 = SystemClock.elapsedRealtime() - elapsedRealtime;
                if (DEBUG || elapsedRealtime2 > 3000) {
                    String str = "HTTP response for request=<%s> [lifetime=%d], [size=%s], [rc=%d], [retryCount=%s]";
                    Object[] objArr = new Object[5];
                    objArr[0] = zzr3;
                    objArr[1] = Long.valueOf(elapsedRealtime2);
                    objArr[2] = bArr != null ? Integer.valueOf(bArr.length) : "null";
                    objArr[3] = Integer.valueOf(statusCode);
                    objArr[4] = Integer.valueOf(zzr.zzk().zzd());
                    zzaf.d(str, objArr);
                }
                if (statusCode < 200) {
                    break;
                } else if (statusCode > 299) {
                    break;
                } else {
                    List list = zzr2;
                    r11 = r11;
                    zzp zzp3 = new zzp(statusCode, bArr, false, SystemClock.elapsedRealtime() - elapsedRealtime, list);
                    return zzp3;
                }
            } catch (SocketTimeoutException unused) {
            } catch (MalformedURLException e) {
            } catch (IOException e2) {
                e = e2;
                byte[] bArr2 = bArr;
                List list2 = zzr2;
                Throwable th = e;
                if (zzaq != null) {
                }
            }
        }
        List list3 = zzr2;
        throw new IOException();
    }

    private static void zza(String str, zzr<?> zzr, zzae zzae) throws zzae {
        zzab zzk = zzr.zzk();
        int zzj = zzr.zzj();
        try {
            zzk.zza(zzae);
            zzr.zzb(String.format("%s-retry [timeout=%s]", new Object[]{str, Integer.valueOf(zzj)}));
        } catch (zzae e) {
            zzr.zzb(String.format("%s-timeout-giveup [timeout=%s]", new Object[]{str, Integer.valueOf(zzj)}));
            throw e;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:23:0x0046 A[SYNTHETIC, Splitter:B:23:0x0046] */
    private final byte[] zza(InputStream inputStream, int i) throws IOException, zzac {
        Throwable th;
        zzav zzav = new zzav(this.zzbq, i);
        byte[] bArr = null;
        if (inputStream == null) {
            try {
                throw new zzac();
            } catch (Throwable th2) {
                th = th2;
                if (inputStream != null) {
                }
                this.zzbq.zza(bArr);
                zzav.close();
                throw th;
            }
        } else {
            byte[] zzb = this.zzbq.zzb(1024);
            while (true) {
                try {
                    int read = inputStream.read(zzb);
                    if (read == -1) {
                        break;
                    }
                    zzav.write(zzb, 0, read);
                } catch (Throwable th3) {
                    byte[] bArr2 = zzb;
                    th = th3;
                    bArr = bArr2;
                    if (inputStream != null) {
                        try {
                            inputStream.close();
                        } catch (IOException unused) {
                            zzaf.v("Error occurred when closing InputStream", new Object[0]);
                        }
                    }
                    this.zzbq.zza(bArr);
                    zzav.close();
                    throw th;
                }
            }
            byte[] byteArray = zzav.toByteArray();
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException unused2) {
                    zzaf.v("Error occurred when closing InputStream", new Object[0]);
                }
            }
            this.zzbq.zza(zzb);
            zzav.close();
            return byteArray;
        }
    }
}
