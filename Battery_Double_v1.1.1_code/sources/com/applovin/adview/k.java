package com.applovin.adview;

class k implements Runnable {
    final /* synthetic */ int a;
    final /* synthetic */ int b;
    final /* synthetic */ j c;

    k(j jVar, int i, int i2) {
        this.c = jVar;
        this.a = i;
        this.b = i2;
    }

    public void run() {
        StringBuilder sb = new StringBuilder();
        sb.append("Video view error (");
        sb.append(this.a);
        sb.append(",");
        sb.append(this.b);
        sb.append(") - showing close button.");
        this.c.a.d.e("AppLovinInterstitialActivity", sb.toString());
        this.c.a.e();
    }
}
