package com.applovin.impl.sdk;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import com.applovin.sdk.AppLovinAdType;
import com.applovin.sdk.AppLovinLogger;
import com.applovin.sdk.AppLovinSdkSettings;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Map.Entry;
import org.json.JSONException;
import org.json.JSONObject;

class be {
    private final AppLovinSdkImpl a;
    private final AppLovinLogger b;
    private final Context c;
    private final Object[] d = new Object[bb.b()];

    be(AppLovinSdkImpl appLovinSdkImpl) {
        this.a = appLovinSdkImpl;
        this.b = appLovinSdkImpl.getLogger();
        this.c = appLovinSdkImpl.getApplicationContext();
    }

    private static bd a(String str) {
        for (bd bdVar : bb.a()) {
            if (bdVar.b().equals(str)) {
                return bdVar;
            }
        }
        return null;
    }

    private static Object a(String str, JSONObject jSONObject, Object obj) {
        if (obj instanceof Boolean) {
            return Boolean.valueOf(jSONObject.getBoolean(str));
        }
        if (obj instanceof Float) {
            return Float.valueOf((float) jSONObject.getDouble(str));
        }
        if (obj instanceof Integer) {
            return Integer.valueOf(jSONObject.getInt(str));
        }
        if (obj instanceof Long) {
            return Long.valueOf(jSONObject.getLong(str));
        }
        if (obj instanceof String) {
            return jSONObject.getString(str);
        }
        StringBuilder sb = new StringBuilder();
        sb.append("SDK Error: unknown value type: ");
        sb.append(obj.getClass());
        throw new RuntimeException(sb.toString());
    }

    private String e() {
        StringBuilder sb = new StringBuilder();
        sb.append("com.applovin.sdk.");
        sb.append(cd.a(this.a.getSdkKey()));
        sb.append(".");
        return sb.toString();
    }

    public SharedPreferences a() {
        if (this.c != null) {
            return this.c.getSharedPreferences("com.applovin.sdk.1", 0);
        }
        throw new IllegalArgumentException("No context specified");
    }

    public Object a(bd bdVar) {
        Object a2;
        if (bdVar == null) {
            throw new IllegalArgumentException("No setting type specified");
        }
        synchronized (this.d) {
            Object obj = this.d[bdVar.a()];
            a2 = obj != null ? bdVar.a(obj) : bdVar.c();
        }
        return a2;
    }

    public void a(bd bdVar, Object obj) {
        if (bdVar == null) {
            throw new IllegalArgumentException("No setting type specified");
        } else if (obj == null) {
            throw new IllegalArgumentException("No new value specified");
        } else {
            synchronized (this.d) {
                this.d[bdVar.a()] = obj;
            }
            StringBuilder sb = new StringBuilder();
            sb.append("Setting update: ");
            sb.append(bdVar.b());
            sb.append(" set to \"");
            sb.append(obj);
            sb.append("\"");
            this.b.d("SettingsManager", sb.toString());
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(AppLovinSdkSettings appLovinSdkSettings) {
        boolean z;
        boolean z2;
        String[] split;
        this.b.i("SettingsManager", "Loading user-defined settings...");
        if (appLovinSdkSettings != null) {
            synchronized (this.d) {
                this.d[bb.j.a()] = Boolean.valueOf(appLovinSdkSettings.isVerboseLoggingEnabled());
                long bannerAdRefreshSeconds = appLovinSdkSettings.getBannerAdRefreshSeconds();
                long j = 0;
                if (bannerAdRefreshSeconds >= 0) {
                    if (bannerAdRefreshSeconds > 0) {
                        j = Math.max(30, bannerAdRefreshSeconds);
                    }
                    this.d[bb.A.a()] = Long.valueOf(j);
                    this.d[bb.z.a()] = Boolean.valueOf(true);
                } else if (bannerAdRefreshSeconds == -1) {
                    this.d[bb.z.a()] = Boolean.valueOf(false);
                }
                String autoPreloadSizes = appLovinSdkSettings.getAutoPreloadSizes();
                if (autoPreloadSizes == null) {
                    autoPreloadSizes = "NONE";
                }
                Object[] objArr = this.d;
                int a2 = bb.J.a();
                if (autoPreloadSizes.equals("NONE")) {
                    autoPreloadSizes = "";
                }
                objArr[a2] = autoPreloadSizes;
                String autoPreloadTypes = appLovinSdkSettings.getAutoPreloadTypes();
                if (autoPreloadTypes == null) {
                    autoPreloadTypes = "NONE";
                }
                if (!autoPreloadTypes.equals("NONE")) {
                    z2 = false;
                    z = false;
                    for (String str : autoPreloadTypes.split(",")) {
                        if (str.equals(AppLovinAdType.REGULAR.getLabel())) {
                            z2 = true;
                        } else if (str.equals(AppLovinAdType.INCENTIVIZED.getLabel()) || str.contains("INCENT") || str.contains("REWARD")) {
                            z = true;
                        }
                    }
                } else {
                    z2 = false;
                    z = false;
                }
                if (!z2) {
                    this.d[bb.J.a()] = "";
                }
                this.d[bb.K.a()] = Boolean.valueOf(z);
                if (appLovinSdkSettings instanceof at) {
                    for (Entry entry : ((at) appLovinSdkSettings).b().entrySet()) {
                        this.d[((bd) entry.getKey()).a()] = entry.getValue();
                    }
                }
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(JSONObject jSONObject) {
        AppLovinLogger appLovinLogger;
        String str;
        String str2;
        this.b.d("SettingsManager", "Loading settings from JSON array...");
        synchronized (this.d) {
            Iterator keys = jSONObject.keys();
            while (keys.hasNext()) {
                String str3 = (String) keys.next();
                if (str3 != null && str3.length() > 0) {
                    try {
                        bd a2 = a(str3);
                        if (a2 != null) {
                            Object a3 = a(str3, jSONObject, a2.c());
                            this.d[a2.a()] = a3;
                            StringBuilder sb = new StringBuilder();
                            sb.append("Setting update: ");
                            sb.append(a2.b());
                            sb.append(" set to \"");
                            sb.append(a3);
                            sb.append("\"");
                            this.b.d("SettingsManager", sb.toString());
                        } else {
                            StringBuilder sb2 = new StringBuilder();
                            sb2.append("Unknown setting recieved: ");
                            sb2.append(str3);
                            this.b.w("SettingsManager", sb2.toString());
                        }
                    } catch (JSONException e) {
                        th = e;
                        appLovinLogger = this.b;
                        str = "SettingsManager";
                        str2 = "Unable to parse JSON settings array";
                        appLovinLogger.e(str, str2, th);
                    } catch (Throwable th) {
                        th = th;
                        appLovinLogger = this.b;
                        str = "SettingsManager";
                        str2 = "Unable to convert setting object ";
                        appLovinLogger.e(str, str2, th);
                    }
                }
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void b() {
        if (this.c == null) {
            throw new IllegalArgumentException("No context specified");
        }
        this.b.i("SettingsManager", "Saving settings with the application...");
        String e = e();
        Editor edit = a().edit();
        synchronized (this.d) {
            for (bd bdVar : bb.a()) {
                Object obj = this.d[bdVar.a()];
                if (obj != null) {
                    StringBuilder sb = new StringBuilder();
                    sb.append(e);
                    sb.append(bdVar.b());
                    String sb2 = sb.toString();
                    if (obj instanceof Boolean) {
                        edit.putBoolean(sb2, ((Boolean) obj).booleanValue());
                    } else if (obj instanceof Float) {
                        edit.putFloat(sb2, ((Float) obj).floatValue());
                    } else if (obj instanceof Integer) {
                        edit.putInt(sb2, ((Integer) obj).intValue());
                    } else if (obj instanceof Long) {
                        edit.putLong(sb2, ((Long) obj).longValue());
                    } else if (obj instanceof String) {
                        edit.putString(sb2, (String) obj);
                    } else {
                        StringBuilder sb3 = new StringBuilder();
                        sb3.append("SDK Error: unknown value: ");
                        sb3.append(obj.getClass());
                        throw new RuntimeException(sb3.toString());
                    }
                }
            }
        }
        edit.commit();
        this.b.d("SettingsManager", "Settings saved with the application.");
    }

    /* access modifiers changed from: 0000 */
    public void c() {
        Object obj;
        if (this.c == null) {
            throw new IllegalArgumentException("No context specified");
        }
        this.b.i("SettingsManager", "Loading settings saved with the application...");
        String e = e();
        SharedPreferences a2 = a();
        synchronized (this.d) {
            for (bd bdVar : bb.a()) {
                try {
                    StringBuilder sb = new StringBuilder();
                    sb.append(e);
                    sb.append(bdVar.b());
                    String sb2 = sb.toString();
                    Object c2 = bdVar.c();
                    if (c2 instanceof Boolean) {
                        obj = Boolean.valueOf(a2.getBoolean(sb2, ((Boolean) c2).booleanValue()));
                    } else if (c2 instanceof Float) {
                        obj = Float.valueOf(a2.getFloat(sb2, ((Float) c2).floatValue()));
                    } else if (c2 instanceof Integer) {
                        obj = Integer.valueOf(a2.getInt(sb2, ((Integer) c2).intValue()));
                    } else if (c2 instanceof Long) {
                        obj = Long.valueOf(a2.getLong(sb2, ((Long) c2).longValue()));
                    } else if (c2 instanceof String) {
                        obj = a2.getString(sb2, (String) c2);
                    } else {
                        StringBuilder sb3 = new StringBuilder();
                        sb3.append("SDK Error: unknown value: ");
                        sb3.append(c2.getClass());
                        throw new RuntimeException(sb3.toString());
                    }
                    this.d[bdVar.a()] = obj;
                } catch (Exception e2) {
                    StringBuilder sb4 = new StringBuilder();
                    sb4.append("Unable to load \"");
                    sb4.append(bdVar.b());
                    sb4.append("\"");
                    this.b.e("SettingsManager", sb4.toString(), e2);
                }
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void d() {
        synchronized (this.d) {
            Arrays.fill(this.d, null);
        }
        Editor edit = a().edit();
        edit.clear();
        edit.commit();
    }
}
