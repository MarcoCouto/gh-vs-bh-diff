package com.applovin.impl.sdk;

import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdLoadListener;
import com.applovin.sdk.AppLovinAdUpdateListener;
import java.util.HashSet;

class g implements AppLovinAdLoadListener {
    final /* synthetic */ e a;
    private final h b;

    private g(e eVar, h hVar) {
        this.a = eVar;
        this.b = hVar;
    }

    public void adReceived(AppLovinAd appLovinAd) {
        HashSet<AppLovinAdLoadListener> hashSet;
        HashSet<AppLovinAdUpdateListener> hashSet2;
        synchronized (this.b.b) {
            if (this.a.a(this.b.a)) {
                long b2 = this.a.b(this.b.a);
                if (b2 > 0) {
                    this.b.d = System.currentTimeMillis() + (1000 * b2);
                } else if (b2 == 0) {
                    this.b.d = Long.MAX_VALUE;
                }
                this.b.c = appLovinAd;
            } else {
                this.b.c = null;
                this.b.d = 0;
            }
            hashSet = new HashSet<>(this.b.g);
            this.b.g.clear();
            hashSet2 = new HashSet<>(this.b.f);
            this.b.e = false;
        }
        this.a.c(this.b.a);
        for (AppLovinAdLoadListener adReceived : hashSet) {
            try {
                adReceived.adReceived(appLovinAd);
            } catch (Throwable th) {
                this.a.b.e("AppLovinAdService", "Unable to notify listener about a newly loaded ad", th);
            }
        }
        for (AppLovinAdUpdateListener adUpdated : hashSet2) {
            try {
                adUpdated.adUpdated(appLovinAd);
            } catch (Throwable th2) {
                this.a.b.e("AppLovinAdService", "Unable to notify listener about an updated loaded ad", th2);
            }
        }
    }

    public void failedToReceiveAd(int i) {
        HashSet<AppLovinAdLoadListener> hashSet;
        synchronized (this.b.b) {
            hashSet = new HashSet<>(this.b.g);
            this.b.g.clear();
            this.b.e = false;
        }
        for (AppLovinAdLoadListener failedToReceiveAd : hashSet) {
            try {
                failedToReceiveAd.failedToReceiveAd(i);
            } catch (Throwable th) {
                this.a.b.e("AppLovinAdService", "Unable to notify listener about ad load failure", th);
            }
        }
    }
}
