package com.applovin.impl.sdk;

import com.applovin.sdk.AppLovinLogger;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

class bn {
    /* access modifiers changed from: private */
    public final AppLovinSdkImpl a;
    /* access modifiers changed from: private */
    public final AppLovinLogger b;
    private final ScheduledExecutorService c = a("main");
    private final ScheduledExecutorService d = a("back");

    bn(AppLovinSdkImpl appLovinSdkImpl) {
        this.a = appLovinSdkImpl;
        this.b = appLovinSdkImpl.getLogger();
    }

    private static void a(Runnable runnable, long j, ScheduledExecutorService scheduledExecutorService) {
        if (j > 0) {
            scheduledExecutorService.schedule(runnable, j, TimeUnit.MILLISECONDS);
        } else {
            scheduledExecutorService.submit(runnable);
        }
    }

    /* access modifiers changed from: 0000 */
    public ScheduledExecutorService a(String str) {
        return Executors.newScheduledThreadPool(1, new bp(this, str));
    }

    /* access modifiers changed from: 0000 */
    public void a(ba baVar, bo boVar) {
        a(baVar, boVar, 0);
    }

    /* access modifiers changed from: 0000 */
    public void a(ba baVar, bo boVar, long j) {
        if (baVar == null) {
            throw new IllegalArgumentException("No task specified");
        } else if (j < 0) {
            StringBuilder sb = new StringBuilder();
            sb.append("Invalid delay specified: ");
            sb.append(j);
            throw new IllegalArgumentException(sb.toString());
        } else {
            AppLovinLogger appLovinLogger = this.b;
            String str = baVar.e;
            StringBuilder sb2 = new StringBuilder();
            sb2.append("Scheduling ");
            sb2.append(baVar.e);
            sb2.append(" on ");
            sb2.append(boVar);
            sb2.append(" queue in ");
            sb2.append(j);
            sb2.append("ms.");
            appLovinLogger.d(str, sb2.toString());
            a((Runnable) new br(this, baVar, boVar), j, boVar == bo.MAIN ? this.c : this.d);
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(bm bmVar, long j) {
        if (bmVar == null) {
            throw new IllegalArgumentException("No task specified");
        }
        a((Runnable) bmVar, j, this.c);
    }
}
