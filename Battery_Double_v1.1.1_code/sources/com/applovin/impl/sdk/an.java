package com.applovin.impl.sdk;

import android.app.Activity;
import com.applovin.sdk.AppLovinAdClickListener;
import com.applovin.sdk.AppLovinAdDisplayListener;
import com.applovin.sdk.AppLovinAdRewardListener;
import com.applovin.sdk.AppLovinAdVideoPlaybackListener;
import java.util.Timer;

class an {
    /* access modifiers changed from: private */
    public final AppLovinSdkImpl a;
    /* access modifiers changed from: private */
    public final w b;
    /* access modifiers changed from: private */
    public Activity c;
    private AppLovinAdDisplayListener d;
    private AppLovinAdVideoPlaybackListener e;
    private AppLovinAdClickListener f;
    /* access modifiers changed from: private */
    public AppLovinAdRewardListener g;
    /* access modifiers changed from: private */
    public final Timer h = new Timer("IncentivizedAdLauncher");

    an(AppLovinSdkImpl appLovinSdkImpl, w wVar) {
        this.a = appLovinSdkImpl;
        this.b = wVar;
    }

    /* access modifiers changed from: private */
    public void b() {
        this.b.b(this.c, this.g, this.e, this.d, this.f);
    }

    /* access modifiers changed from: 0000 */
    public void a() {
        this.c.runOnUiThread(new ao(this));
    }

    public void a(Activity activity) {
        this.c = activity;
    }

    public void a(AppLovinAdClickListener appLovinAdClickListener) {
        this.f = appLovinAdClickListener;
    }

    public void a(AppLovinAdDisplayListener appLovinAdDisplayListener) {
        this.d = appLovinAdDisplayListener;
    }

    public void a(AppLovinAdRewardListener appLovinAdRewardListener) {
        this.g = appLovinAdRewardListener;
    }

    public void a(AppLovinAdVideoPlaybackListener appLovinAdVideoPlaybackListener) {
        this.e = appLovinAdVideoPlaybackListener;
    }
}
