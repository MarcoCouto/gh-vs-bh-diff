package com.applovin.impl.sdk;

import android.graphics.Point;
import com.applovin.adview.AppLovinInterstitialActivity;
import com.applovin.sdk.AppLovinAdLoadListener;
import com.applovin.sdk.AppLovinAdSize;
import com.applovin.sdk.AppLovinAdType;
import com.applovin.sdk.AppLovinLogger;
import com.google.android.exoplayer2.source.chunk.ChunkedTrackBlacklistUtil;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONObject;

class bk extends ba implements cc {
    private final AppLovinAdSize a;
    private final AppLovinAdType b;
    private final AppLovinAdLoadListener c;
    private boolean d = false;

    bk(AppLovinAdSize appLovinAdSize, AppLovinAdType appLovinAdType, AppLovinAdLoadListener appLovinAdLoadListener, AppLovinSdkImpl appLovinSdkImpl) {
        super("FetchNextAd", appLovinSdkImpl);
        this.a = appLovinAdSize;
        this.b = appLovinAdType;
        this.c = appLovinAdLoadListener;
    }

    private void a(bg bgVar) {
        if (System.currentTimeMillis() - bgVar.b("ad_session_start") > ChunkedTrackBlacklistUtil.DEFAULT_TRACK_BLACKLIST_MS * ((long) ((Integer) this.f.a(bb.x)).intValue())) {
            bgVar.b("ad_session_start", System.currentTimeMillis());
            bgVar.c("ad_imp_session");
        }
    }

    private void b(Map map) {
        bz a2 = bx.a().a("tFNA");
        if (a2 != null) {
            map.put("etf", Long.toString(a2.b()));
            map.put("ntf", a2.a());
        }
        bz a3 = bx.a().a("tRA");
        if (a3 != null) {
            map.put("etr", Long.toString(a3.b()));
            map.put("ntr", a3.a());
        }
    }

    private void c(Map map) {
        map.put("api_did", this.f.a(bb.c));
        map.put("sdk_key", this.f.getSdkKey());
        map.put("sdk_version", "5.4.3");
        String str = (String) this.f.a(bb.F);
        if (str != null && str.length() > 0) {
            map.put("plugin_version", str);
        }
        map.put("accept", g());
        map.put("preloading", String.valueOf(this.d));
        map.put("size", this.a.getLabel());
        map.put("format", "json");
    }

    private void d(Map map) {
        if (((Boolean) this.f.a(bb.N)).booleanValue()) {
            bg b2 = this.f.b();
            map.put("li", String.valueOf(b2.b("ad_imp")));
            map.put("si", String.valueOf(b2.b("ad_imp_session")));
        }
    }

    private void e(Map map) {
        if (((Boolean) this.f.a(bb.N)).booleanValue()) {
            Map a2 = ((l) this.f.getTargetingData()).a();
            if (a2 != null && !a2.isEmpty()) {
                map.putAll(a2);
            }
        }
    }

    private void f(Map map) {
        Map a2 = a.a(this.f);
        if (a2.isEmpty()) {
            try {
                g(a2);
                a.a(a2, this.f);
            } catch (Exception e) {
                this.g.e(this.e, "Unable to populate device information", e);
            }
        }
        map.putAll(a2);
        map.put("network", p.a(this.f));
        i(map);
        map.put("vz", cd.a(this.f.getApplicationContext().getPackageName(), this.f));
    }

    private String g() {
        String str = "custom_size,launch_app";
        if (!m.b() || !m.a(AppLovinInterstitialActivity.class, this.h) || !m.a("android.permission.WRITE_EXTERNAL_STORAGE", this.h)) {
            return str;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(",video");
        return sb.toString();
    }

    private void g(Map map) {
        t a2 = c().a();
        map.put("brand", cd.b(a2.c));
        map.put("carrier", cd.b(a2.g));
        map.put("locale", a2.h.toString());
        map.put("model", cd.b(a2.a));
        map.put("os", cd.b(a2.b));
        map.put("platform", "android");
        map.put("revision", cd.b(a2.d));
        h(map);
    }

    private void h(Map map) {
        Point a2 = m.a(this.f.getApplicationContext());
        map.put("dx", Integer.toString(a2.x));
        map.put("dy", Integer.toString(a2.y));
    }

    private void i(Map map) {
        r c2 = c().c();
        String str = c2.b;
        if (!c2.a && cd.c(str)) {
            map.put("idfa", str);
        }
    }

    private void j(Map map) {
        if (this.b != null) {
            map.put("require", this.b.getLabel());
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(int i) {
        AppLovinLogger appLovinLogger = this.g;
        String str = this.e;
        StringBuilder sb = new StringBuilder();
        sb.append("Unable to fetch ");
        sb.append(this.a);
        sb.append(" ad: server returned ");
        sb.append(i);
        appLovinLogger.e(str, sb.toString());
        try {
            if (this.c != null) {
                if (this.c instanceof u) {
                    ((u) this.c).a(new c(this.a, this.b), i);
                } else {
                    this.c.failedToReceiveAd(i);
                }
            }
        } catch (Throwable th) {
            this.g.e(this.e, "Unable process a failure to recieve an ad", th);
        }
        p.b(i, this.f);
    }

    /* access modifiers changed from: protected */
    public void a(Map map) {
        e(map);
        f(map);
        d(map);
        c(map);
        j(map);
        b(map);
    }

    /* access modifiers changed from: 0000 */
    public void a(JSONObject jSONObject) {
        this.f.a().a((ba) new bs(jSONObject, this.c, this.f), bo.MAIN);
        p.a(jSONObject, this.f);
    }

    public void a(boolean z) {
        this.d = z;
    }

    /* access modifiers changed from: 0000 */
    public void b() {
        super.b();
        a(-410);
    }

    /* access modifiers changed from: 0000 */
    public String d() {
        HashMap hashMap = new HashMap();
        a((Map) hashMap);
        StringBuffer stringBuffer = new StringBuffer(e());
        stringBuffer.append("?");
        stringBuffer.append(cd.a((Map) hashMap));
        return stringBuffer.toString();
    }

    /* access modifiers changed from: 0000 */
    public String e() {
        return p.b("ad", this.f);
    }

    public String f() {
        return "tFNA";
    }

    public void run() {
        AppLovinLogger appLovinLogger;
        String str;
        String str2;
        if (this.d) {
            appLovinLogger = this.g;
            str = this.e;
            str2 = "Preloading next ad...";
        } else {
            appLovinLogger = this.g;
            str = this.e;
            str2 = "Fetching next ad...";
        }
        appLovinLogger.d(str, str2);
        bg b2 = this.f.b();
        b2.a("ad_req");
        a(b2);
        try {
            bl blVar = new bl(this, "RepeatFetchNextAd", bb.i, this.f);
            blVar.a(bb.l);
            blVar.run();
        } catch (Throwable th) {
            AppLovinLogger appLovinLogger2 = this.g;
            String str3 = this.e;
            StringBuilder sb = new StringBuilder();
            sb.append("Unable to fetch ");
            sb.append(this.a);
            sb.append(" ad");
            appLovinLogger2.e(str3, sb.toString(), th);
            a(0);
        }
    }
}
