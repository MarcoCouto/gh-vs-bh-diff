package com.facebook.ads.internal.c;

import android.os.Bundle;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.annotation.UiThread;
import android.support.v4.view.InputDeviceCompat;
import android.support.v4.view.PointerIconCompat;
import android.util.Log;
import com.facebook.ads.AdError;
import com.facebook.ads.AudienceNetworkAds;
import com.facebook.ads.CacheFlag;
import com.facebook.ads.InterstitialAd;
import com.facebook.ads.InterstitialAdExtendedListener;
import com.facebook.ads.internal.b.e;
import com.facebook.ads.internal.c.a.C0003a;
import com.facebook.ads.internal.protocol.AdErrorType;
import com.facebook.ads.internal.settings.AdInternalSettings;
import com.facebook.ads.internal.w.b.v;
import com.facebook.ads.internal.w.h.a;
import com.facebook.ads.internal.w.h.b;
import java.util.EnumSet;

@UiThread
public class f extends b {
    private final g e;
    @Nullable
    private d f;

    public f(g gVar) {
        super(gVar.a.getApplicationContext());
        this.e = gVar;
    }

    private void h() {
        a(PointerIconCompat.TYPE_NO_DROP, null);
        this.b.b();
        this.e.a(null);
    }

    /* access modifiers changed from: 0000 */
    public Message a() {
        Message obtain = Message.obtain(null, PointerIconCompat.TYPE_ALIAS);
        obtain.getData().putString("STR_PLACEMENT_KEY", this.e.b);
        obtain.getData().putString("STR_AD_ID_KEY", this.c);
        obtain.getData().putString("STR_BID_PAYLOAD_KEY", this.e.f);
        obtain.getData().putString("STR_EXTRA_HINTS_KEY", this.e.d);
        obtain.getData().putSerializable("SRL_INT_CACHE_FLAGS_KEY", this.e.e);
        obtain.getData().putBundle("BUNDLE_SETTINGS_KEY", AdInternalSettings.a);
        return obtain;
    }

    /* JADX INFO: used method not loaded: com.facebook.ads.internal.c.h.a(java.lang.String):null, types can be incorrect */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x004a, code lost:
        r1.a(r2);
     */
    public void a(Message message) {
        String str;
        h hVar;
        InterstitialAd a = this.e.a();
        if (a == null) {
            a.b(this.a, "api", b.n, new Exception("Ad object is null"));
            return;
        }
        int i = message.what;
        if (i != 10) {
            if (i != 1020) {
                switch (i) {
                    case PointerIconCompat.TYPE_VERTICAL_DOUBLE_ARROW /*1015*/:
                        hVar = this.b;
                        str = "Received load confirmation.";
                        break;
                    case PointerIconCompat.TYPE_TOP_RIGHT_DIAGONAL_DOUBLE_ARROW /*1016*/:
                        hVar = this.b;
                        str = "Received show confirmation.";
                        break;
                    case PointerIconCompat.TYPE_TOP_LEFT_DIAGONAL_DOUBLE_ARROW /*1017*/:
                        hVar = this.b;
                        str = "Received destroy confirmation.";
                        break;
                    default:
                        switch (i) {
                            case 1022:
                                this.d.a(C0003a.SHOWN);
                                if (this.b.b) {
                                    h();
                                    break;
                                }
                                break;
                            case 1023:
                                break;
                        }
                }
            } else {
                this.d.a(C0003a.LOADED);
                Bundle bundle = message.getData().getBundle("BUNDLE_EXTRAS_KEY");
                if (bundle != null) {
                    this.e.g = bundle.getLong("LONG_INVALIDATION_TIME_KEY");
                } else {
                    int i2 = b.m;
                    StringBuilder sb = new StringBuilder();
                    sb.append("Missing bundle for message: ");
                    sb.append(message);
                    a.b(this.a, "api", i2, new Exception(sb.toString()));
                }
                this.e.a(null);
            }
            if (this.e.c != null) {
                switch (message.what) {
                    case PointerIconCompat.TYPE_GRAB /*1020*/:
                        this.e.c.onAdLoaded(a);
                        break;
                    case PointerIconCompat.TYPE_GRABBING /*1021*/:
                        this.e.c.onInterstitialDisplayed(a);
                        return;
                    case 1022:
                        this.e.c.onInterstitialDismissed(a);
                        return;
                    case 1024:
                        this.e.c.onAdClicked(a);
                        return;
                    case InputDeviceCompat.SOURCE_GAMEPAD /*1025*/:
                        this.e.c.onLoggingImpression(a);
                        return;
                    case 1026:
                        if (this.e.c instanceof InterstitialAdExtendedListener) {
                            ((InterstitialAdExtendedListener) this.e.c).onInterstitialActivityDestroyed();
                            return;
                        }
                        break;
                    default:
                        return;
                }
                return;
            }
            return;
        }
        this.d.a(C0003a.ERROR);
        if (this.b.b) {
            h();
        }
        Bundle bundle2 = message.getData().getBundle("BUNDLE_EXTRAS_KEY");
        if (bundle2 != null) {
            int i3 = bundle2.getInt("INT_ERROR_CODE_KEY");
            String string = bundle2.getString("STR_ERROR_MESSAGE_KEY");
            if (this.e.c != null) {
                this.e.c.onError(a, new AdError(i3, string));
            } else {
                Log.e(AudienceNetworkAds.TAG, string);
            }
        } else {
            int i4 = b.m;
            StringBuilder sb2 = new StringBuilder();
            sb2.append("Missing bundle for message: ");
            sb2.append(message);
            a.b(this.a, "api", i4, new Exception(sb2.toString()));
        }
        this.e.a(null);
    }

    public void a(InterstitialAd interstitialAd, EnumSet<CacheFlag> enumSet, String str) {
        com.facebook.ads.internal.protocol.a a = e.a(this.a, Integer.valueOf(0), Integer.valueOf(1));
        if (a != null) {
            a(10, AdErrorType.MISSING_DEPENDENCIES_ERROR, a.b());
        } else if (!this.d.a(C0003a.LOADING, "load()")) {
            this.e.a(interstitialAd);
            if (this.f != null) {
                this.f.a(enumSet, str);
                return;
            }
            this.e.e = enumSet;
            this.e.f = str;
            if (!a(this.e.a)) {
                c();
            } else if (this.b.b) {
                b();
            } else {
                this.b.c = true;
                this.b.a();
            }
        }
    }

    public boolean a(InterstitialAd interstitialAd) {
        if (this.d.a(C0003a.SHOWING, "show()")) {
            return false;
        }
        this.e.a(interstitialAd);
        if (this.b.b) {
            a(PointerIconCompat.TYPE_COPY, null);
            return true;
        } else if (this.f != null) {
            return this.f.e();
        } else {
            this.f = new d(this.e, this, this.c);
            this.f.e();
            return false;
        }
    }

    public void c() {
        this.f = new d(this.e, this, this.c);
        this.f.a(this.e.e, this.e.f);
    }

    public void d() {
        if (this.b.b) {
            h();
        }
        if (this.f != null) {
            this.f.a();
        }
        this.d.a(C0003a.DESTROYED);
    }

    public boolean f() {
        return this.f != null ? this.f.d() : this.d.a == C0003a.LOADED;
    }

    public boolean g() {
        return this.f != null ? this.f.c() : this.e.g > 0 && v.a() > this.e.g;
    }
}
