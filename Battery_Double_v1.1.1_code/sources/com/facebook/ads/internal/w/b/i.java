package com.facebook.ads.internal.w.b;

import android.support.annotation.Nullable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class i {
    /* JADX WARNING: Missing exception handler attribute for start block: B:16:0x002a */
    @Nullable
    public static String a(File file) {
        int read;
        try {
            FileInputStream fileInputStream = new FileInputStream(file);
            try {
                MessageDigest instance = MessageDigest.getInstance("MD5");
                byte[] bArr = new byte[1024];
                do {
                    read = fileInputStream.read(bArr);
                    if (read > 0) {
                        instance.update(bArr, 0, read);
                    }
                } while (read != -1);
                String b = b(instance.digest());
                try {
                    fileInputStream.close();
                } catch (IOException unused) {
                }
                return b;
            } catch (NoSuchAlgorithmException unused2) {
                throw new Exception("No such algorithm.");
            } catch (IOException ) {
                throw new Exception("IO exception.");
            } catch (Throwable th) {
                try {
                    fileInputStream.close();
                } catch (IOException unused3) {
                }
                throw th;
            }
        } catch (FileNotFoundException unused4) {
            throw new Exception("File not found or not accessible.");
        }
    }

    public static String a(byte[] bArr) {
        int i;
        StringBuilder sb = new StringBuilder();
        for (byte b : bArr) {
            int i2 = (b >>> 4) & 15;
            int i3 = 0;
            while (true) {
                if (i2 < 0 || i2 > 9) {
                    i = 97;
                    i2 -= 10;
                } else {
                    i = 48;
                }
                sb.append((char) (i + i2));
                i2 = b & 15;
                int i4 = i3 + 1;
                if (i3 >= 1) {
                    break;
                }
                i3 = i4;
            }
        }
        return sb.toString();
    }

    private static String b(byte[] bArr) {
        StringBuilder sb = new StringBuilder();
        for (byte b : bArr) {
            sb.append(Integer.toString((b & 255) + 256, 16).substring(1));
        }
        return sb.toString();
    }
}
