package com.facebook.ads.internal.view.component;

import android.content.Context;
import android.content.res.Resources;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.text.TextUtils.TruncateAt;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import com.facebook.ads.internal.adapters.b.h;
import com.facebook.ads.internal.w.b.x;

public class j extends LinearLayout {
    private static final float a = Resources.getSystem().getDisplayMetrics().density;
    private static final int b = ((int) (6.0f * a));
    private static final int c = ((int) (8.0f * a));
    private final TextView d;
    private final TextView e;
    private final TextView f;

    public j(Context context, h hVar, boolean z, int i, int i2, int i3) {
        super(context);
        setOrientation(1);
        this.d = new TextView(context);
        x.a(this.d, true, i);
        this.d.setTextColor(hVar.c(z));
        this.d.setEllipsize(TruncateAt.END);
        this.d.setLineSpacing((float) b, 1.0f);
        this.f = new TextView(context);
        this.f.setTextColor(hVar.a(z));
        this.e = new TextView(context);
        x.a(this.e, false, i2);
        this.e.setTextColor(hVar.b(z));
        this.e.setEllipsize(TruncateAt.END);
        this.e.setLineSpacing((float) b, 1.0f);
        addView(this.d, new LayoutParams(-1, -2));
        addView(this.f, new LayoutParams(-1, -2));
        this.f.setVisibility(8);
        LayoutParams layoutParams = new LayoutParams(-1, -2);
        layoutParams.setMargins(0, i3, 0, 0);
        addView(this.e, layoutParams);
    }

    public j(Context context, h hVar, boolean z, boolean z2, boolean z3) {
        this(context, hVar, z, z2 ? 18 : 22, z2 ? 14 : 16, z3 ? c / 2 : c);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x003c, code lost:
        if (r9 != false) goto L_0x0048;
     */
    public void a(String str, String str2, @Nullable String str3, boolean z, boolean z2) {
        TextView textView;
        boolean z3 = !TextUtils.isEmpty(str);
        boolean z4 = !TextUtils.isEmpty(str2);
        TextView textView2 = this.d;
        if (!z3) {
            str = str2;
        }
        textView2.setText(str);
        if (str3 != null) {
            this.f.setText(str3);
        }
        TextView textView3 = this.e;
        if (!z3) {
            str2 = "";
        }
        textView3.setText(str2);
        int i = 3;
        if (!z3 || !z4) {
            textView = this.d;
            if (!z) {
                if (z2) {
                    i = 4;
                }
                textView.setMaxLines(i);
            }
        } else {
            this.d.setMaxLines(z ? 1 : 2);
            textView = this.e;
            if (z) {
                i = 1;
            }
            textView.setMaxLines(i);
        }
        i = 2;
        textView.setMaxLines(i);
    }

    public void a(boolean z, int i) {
        TextView textView;
        int i2;
        if (z) {
            this.f.setGravity(i);
            textView = this.f;
            i2 = 0;
        } else {
            textView = this.f;
            i2 = 8;
        }
        textView.setVisibility(i2);
    }

    public TextView getDescriptionTextView() {
        return this.e;
    }

    public TextView getTitleTextView() {
        return this.d;
    }

    public void setAlignment(int i) {
        this.d.setGravity(i);
        this.e.setGravity(i);
    }

    public void setDescriptionGravity(int i) {
        this.e.setGravity(i);
    }

    public void setTitleGravity(int i) {
        this.d.setGravity(i);
    }
}
