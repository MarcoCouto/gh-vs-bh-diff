package com.facebook.ads.internal.adapters;

import android.content.Context;
import android.net.Uri;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.widget.helper.ItemTouchHelper.Callback;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import com.facebook.ads.internal.a.b;
import com.facebook.ads.internal.a.d;
import com.facebook.ads.internal.a.e.a;
import com.facebook.ads.internal.protocol.AdErrorType;
import com.facebook.ads.internal.protocol.AdPlacementType;
import com.facebook.ads.internal.s.c;
import com.facebook.ads.internal.t.e;
import com.facebook.ads.internal.t.g;
import com.facebook.ads.internal.t.j;
import com.facebook.ads.internal.t.l;
import com.facebook.ads.internal.w.b.k;
import com.facebook.ads.internal.w.b.z;
import com.facebook.appevents.UserDataStore;
import com.facebook.share.internal.MessengerShareContentUtility;
import com.facebook.share.internal.ShareConstants;
import com.google.ads.mediation.facebook.FacebookAdapter;
import io.fabric.sdk.android.services.settings.SettingsJsonConstants;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class i implements a, AdAdapter {
    private static final String a = "i";
    private int A;
    /* access modifiers changed from: private */
    public String B;
    private boolean C;
    private boolean D;
    private boolean E;
    private boolean F;
    private boolean G;
    /* access modifiers changed from: private */
    @Nullable
    public c H;
    private e.c I;
    private Context b;
    private q c;
    private Uri d;
    private HashMap<String, String> e = new HashMap<>();
    private g f;
    private g g;
    private com.facebook.ads.internal.t.i h;
    private String i;
    private d j;
    private Collection<String> k;
    private boolean l;
    private boolean m;
    private int n;
    private int o;
    private int p;
    private int q;
    private String r;
    private String s;
    private l t;
    private int u = Callback.DEFAULT_DRAG_ANIMATION_DURATION;
    private g v;
    private String w;
    private j x;
    private List<e> y;
    private int z = -1;

    private void C() {
        if (!this.G) {
            if (this.H != null) {
                this.H.a(this.i);
            }
            this.G = true;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:28:0x010b  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x0154  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x0159  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x0181 A[Catch:{ JSONException -> 0x01ad }, LOOP:1: B:49:0x017f->B:50:0x0181, LOOP_END] */
    private void a(JSONObject jSONObject, String str) {
        String[] strArr;
        JSONArray jSONArray;
        JSONArray optJSONArray;
        int length;
        int i2;
        j jVar;
        if (this.D) {
            throw new IllegalStateException("Adapter already loaded data");
        } else if (jSONObject != null) {
            com.facebook.ads.internal.w.b.c.a(this.b, "Audience Network Loaded");
            this.B = str;
            String a2 = k.a(jSONObject, "fbad_command");
            this.d = TextUtils.isEmpty(a2) ? null : Uri.parse(a2);
            boolean z2 = false;
            for (String str2 : new String[]{"advertiser_name", "title", "subtitle", "headline", TtmlNode.TAG_BODY, FacebookAdapter.KEY_SOCIAL_CONTEXT_ASSET, "link_description", "sponsored_translation", "ad_translation", "promoted_translation"}) {
                this.e.put(str2, k.a(jSONObject, str2));
            }
            String a3 = k.a(jSONObject, "call_to_action");
            if (!TextUtils.isEmpty(a3)) {
                this.e.put("call_to_action", a3);
            }
            this.f = g.a(jSONObject.optJSONObject(SettingsJsonConstants.APP_ICON_KEY));
            this.g = g.a(jSONObject.optJSONObject(MessengerShareContentUtility.MEDIA_IMAGE));
            this.h = com.facebook.ads.internal.t.i.a(jSONObject.optJSONObject("star_rating"));
            this.i = k.a(jSONObject, "used_report_url");
            this.l = jSONObject.optBoolean("enable_view_log");
            this.m = jSONObject.optBoolean("enable_snapshot_log");
            this.n = jSONObject.optInt("snapshot_log_delay_second", 4);
            this.o = jSONObject.optInt("snapshot_compress_quality", 0);
            this.p = jSONObject.optInt("viewability_check_initial_delay", 0);
            this.q = jSONObject.optInt("viewability_check_interval", 1000);
            JSONObject optJSONObject = jSONObject.optJSONObject("ad_choices_icon");
            JSONObject optJSONObject2 = jSONObject.optJSONObject("native_ui_config");
            if (optJSONObject2 != null) {
                try {
                    if (optJSONObject2.length() != 0) {
                        jVar = new j(optJSONObject2);
                        this.x = jVar;
                        if (optJSONObject != null) {
                            this.v = g.a(optJSONObject);
                        }
                        this.w = k.a(jSONObject, "ad_choices_link_url");
                        this.j = d.a(jSONObject.optString("invalidation_behavior"));
                        jSONArray = new JSONArray(jSONObject.optString("detection_strings"));
                        this.k = com.facebook.ads.internal.a.e.a(jSONArray);
                        this.r = k.a(jSONObject, "video_url");
                        this.s = k.a(jSONObject, "video_mpd");
                        l lVar = jSONObject.has("video_autoplay_enabled") ? l.DEFAULT : jSONObject.optBoolean("video_autoplay_enabled") ? l.ON : l.OFF;
                        this.t = lVar;
                        optJSONArray = jSONObject.optJSONArray("carousel");
                        if (optJSONArray != null && optJSONArray.length() > 0) {
                            length = optJSONArray.length();
                            ArrayList arrayList = new ArrayList(length);
                            for (i2 = 0; i2 < length; i2++) {
                                i iVar = new i();
                                Context context = this.b;
                                JSONObject jSONObject2 = optJSONArray.getJSONObject(i2);
                                c cVar = this.H;
                                iVar.C = true;
                                iVar.b = context;
                                iVar.H = cVar;
                                iVar.z = i2;
                                iVar.A = length;
                                iVar.a(jSONObject2, str);
                                arrayList.add(new e(this.b, iVar, null, this.I));
                            }
                            this.y = arrayList;
                        }
                        this.D = true;
                        if (((!this.C && !TextUtils.isEmpty((CharSequence) this.e.get("advertiser_name"))) || (!TextUtils.isEmpty((CharSequence) this.e.get("title")) && this.C)) && ((this.f != null || this.C) && (this.g != null || getPlacementType() == AdPlacementType.NATIVE_BANNER))) {
                            z2 = true;
                        }
                        this.E = z2;
                    }
                } catch (JSONException unused) {
                    this.x = null;
                }
            }
            jVar = null;
            this.x = jVar;
            if (optJSONObject != null) {
            }
            this.w = k.a(jSONObject, "ad_choices_link_url");
            this.j = d.a(jSONObject.optString("invalidation_behavior"));
            try {
                jSONArray = new JSONArray(jSONObject.optString("detection_strings"));
            } catch (JSONException e2) {
                e2.printStackTrace();
                jSONArray = null;
            }
            this.k = com.facebook.ads.internal.a.e.a(jSONArray);
            this.r = k.a(jSONObject, "video_url");
            this.s = k.a(jSONObject, "video_mpd");
            if (jSONObject.has("video_autoplay_enabled")) {
            }
            this.t = lVar;
            try {
                optJSONArray = jSONObject.optJSONArray("carousel");
                length = optJSONArray.length();
                ArrayList arrayList2 = new ArrayList(length);
                while (i2 < length) {
                }
                this.y = arrayList2;
            } catch (JSONException e3) {
                Log.e(a, "Unable to parse carousel data.", e3);
            }
            this.D = true;
            z2 = true;
            this.E = z2;
        }
    }

    public boolean A() {
        return this.D && this.E;
    }

    public boolean B() {
        return this.C;
    }

    public d a() {
        return this.j;
    }

    @Nullable
    public String a(String str) {
        if (!A()) {
            return null;
        }
        C();
        return (String) this.e.get(str);
    }

    public void a(int i2) {
    }

    public void a(Context context, q qVar, c cVar, Map<String, Object> map, e.c cVar2) {
        this.b = context;
        this.c = qVar;
        this.H = cVar;
        this.I = cVar2;
        JSONObject jSONObject = (JSONObject) map.get(ShareConstants.WEB_DIALOG_PARAM_DATA);
        com.facebook.ads.internal.m.d dVar = (com.facebook.ads.internal.m.d) map.get("definition");
        this.u = dVar != null ? dVar.k() : Callback.DEFAULT_DRAG_ANIMATION_DURATION;
        a(jSONObject, k.a(jSONObject, UserDataStore.CITY));
        if (com.facebook.ads.internal.a.e.a(context, this, cVar)) {
            qVar.a(this, new com.facebook.ads.internal.protocol.a(AdErrorType.NO_FILL, "No Fill"));
            return;
        }
        if (qVar != null) {
            qVar.a(this);
        }
    }

    public void a(View view, List<View> list) {
    }

    public void a(q qVar) {
        this.c = qVar;
    }

    public void a(Map<String, String> map) {
        if (A() && !this.F) {
            if (this.c != null) {
                this.c.b(this);
            }
            final HashMap hashMap = new HashMap();
            if (map != null) {
                hashMap.putAll(map);
            }
            if (this.C) {
                hashMap.put("cardind", String.valueOf(this.z));
                hashMap.put("cardcnt", String.valueOf(this.A));
            }
            if (!TextUtils.isEmpty(getClientToken()) && this.H != null) {
                this.H.a(getClientToken(), hashMap);
            }
            if (d() || h()) {
                try {
                    final HashMap hashMap2 = new HashMap();
                    if (map.containsKey("view")) {
                        hashMap2.put("view", map.get("view"));
                    }
                    if (map.containsKey("snapshot")) {
                        hashMap2.put("snapshot", map.get("snapshot"));
                    }
                    new Handler().postDelayed(new Runnable() {
                        public void run() {
                            if (!TextUtils.isEmpty(i.this.B)) {
                                HashMap hashMap = new HashMap();
                                hashMap.putAll(hashMap);
                                hashMap.putAll(hashMap2);
                                if (i.this.H != null) {
                                    i.this.H.f(i.this.B, hashMap);
                                }
                            }
                        }
                    }, (long) (this.n * 1000));
                } catch (Exception unused) {
                }
            }
            this.F = true;
        }
    }

    public Collection<String> b() {
        return this.k;
    }

    public void b(Map<String, String> map) {
        if (this.H != null) {
            this.H.k(this.B, map);
        }
    }

    public void c() {
        if (this.y != null && !this.y.isEmpty()) {
            for (e z2 : this.y) {
                z2.z();
            }
        }
    }

    public void c(Map<String, String> map) {
        if (this.H != null) {
            this.H.j(this.B, map);
        }
    }

    public void d(Map<String, String> map) {
        if (this.H != null) {
            this.H.i(this.B, map);
        }
    }

    public boolean d() {
        return A() && this.l;
    }

    public void e(Map<String, String> map) {
        if (A()) {
            if (!com.facebook.ads.internal.r.a.j(this.b) || !z.a(map)) {
                HashMap hashMap = new HashMap();
                if (map != null) {
                    hashMap.putAll(map);
                }
                com.facebook.ads.internal.w.b.c.a(this.b, "Click logged");
                if (this.c != null) {
                    this.c.c(this);
                }
                if (this.C) {
                    hashMap.put("cardind", String.valueOf(this.z));
                    hashMap.put("cardcnt", String.valueOf(this.A));
                }
                b a2 = com.facebook.ads.internal.a.c.a(this.b, this.H, this.B, this.d, hashMap);
                if (a2 != null) {
                    try {
                        a2.a();
                        return;
                    } catch (Exception e2) {
                        Log.e(a, "Error executing action", e2);
                    }
                }
                return;
            }
            Log.e(a, "Click happened on lockscreen ad");
        }
    }

    public boolean e() {
        return A() && this.x != null;
    }

    public boolean f() {
        return A() && this.d != null;
    }

    public boolean g() {
        return true;
    }

    public String getClientToken() {
        return this.B;
    }

    public AdPlacementType getPlacementType() {
        return AdPlacementType.NATIVE;
    }

    public boolean h() {
        return A() && this.m;
    }

    public int i() {
        if (this.o < 0 || this.o > 100) {
            return 0;
        }
        return this.o;
    }

    public int j() {
        return this.p;
    }

    public int k() {
        return this.q;
    }

    public g l() {
        if (!A()) {
            return null;
        }
        return this.f;
    }

    public g m() {
        if (!A()) {
            return null;
        }
        return this.g;
    }

    public j n() {
        if (!A()) {
            return null;
        }
        return this.x;
    }

    public String o() {
        StringBuilder sb;
        String substring;
        if (!A()) {
            return null;
        }
        C();
        String str = (String) this.e.get(TtmlNode.TAG_BODY);
        if (str != null) {
            StringTokenizer stringTokenizer = new StringTokenizer(str, " ", true);
            if (str.length() > 90 && (str.length() > 93 || !str.endsWith("..."))) {
                int i2 = 0;
                while (stringTokenizer.hasMoreTokens()) {
                    int length = stringTokenizer.nextToken().length() + i2;
                    if (length < 90) {
                        i2 = length;
                    }
                }
                if (i2 == 0) {
                    sb = new StringBuilder();
                    substring = str.substring(0, 90);
                } else {
                    sb = new StringBuilder();
                    substring = str.substring(0, i2);
                }
                sb.append(substring);
                sb.append("...");
                return sb.toString();
            }
        }
        return str;
    }

    public void onDestroy() {
    }

    public com.facebook.ads.internal.t.i p() {
        if (!A()) {
            return null;
        }
        C();
        return this.h;
    }

    public g q() {
        if (!A()) {
            return null;
        }
        return this.v;
    }

    public String r() {
        if (!A()) {
            return null;
        }
        return this.w;
    }

    public String s() {
        if (!A()) {
            return null;
        }
        return "AdChoices";
    }

    public String t() {
        if (!A()) {
            return null;
        }
        return this.r;
    }

    public String u() {
        if (!A()) {
            return null;
        }
        return this.s;
    }

    public l v() {
        return !A() ? l.DEFAULT : this.t;
    }

    public int w() {
        return this.u;
    }

    public List<e> x() {
        if (!A()) {
            return null;
        }
        return this.y;
    }

    public int y() {
        return this.z;
    }

    public int z() {
        return this.A;
    }
}
