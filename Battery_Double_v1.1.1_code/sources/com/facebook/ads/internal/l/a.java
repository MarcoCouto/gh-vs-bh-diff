package com.facebook.ads.internal.l;

import android.app.ActivityManager;
import android.app.ActivityManager.MemoryInfo;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Build.VERSION;
import android.os.Environment;
import android.os.StatFs;
import com.facebook.appevents.AppEventsConstants;
import com.google.firebase.analytics.FirebaseAnalytics.Param;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class a {
    private static SensorManager a;
    private static Sensor b;
    private static Sensor c;
    /* access modifiers changed from: private */
    public static volatile float[] d;
    /* access modifiers changed from: private */
    public static volatile float[] e;
    private static Map<String, String> f = new ConcurrentHashMap();
    private static String[] g = {"x", "y", "z"};
    private static SensorEventListener h;
    private static SensorEventListener i;

    /* renamed from: com.facebook.ads.internal.l.a$a reason: collision with other inner class name */
    private static class C0007a implements SensorEventListener {
        private C0007a() {
        }

        public void onAccuracyChanged(Sensor sensor, int i) {
        }

        public void onSensorChanged(SensorEvent sensorEvent) {
            a.d = sensorEvent.values;
            a.d();
        }
    }

    private static class b implements SensorEventListener {
        private b() {
        }

        public void onAccuracyChanged(Sensor sensor, int i) {
        }

        public void onSensorChanged(SensorEvent sensorEvent) {
            a.e = sensorEvent.values;
            a.e();
        }
    }

    public static Map<String, String> a() {
        HashMap hashMap = new HashMap();
        hashMap.putAll(f);
        a((Map<String, String>) hashMap);
        return hashMap;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:48:0x0104, code lost:
        return;
     */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0086  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x009d  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x00a0  */
    public static synchronized void a(Context context) {
        boolean z;
        synchronized (a.class) {
            MemoryInfo memoryInfo = new MemoryInfo();
            ((ActivityManager) context.getSystemService("activity")).getMemoryInfo(memoryInfo);
            f.put("available_memory", String.valueOf(memoryInfo.availMem));
            if (VERSION.SDK_INT >= 16) {
                f.put("total_memory", String.valueOf(memoryInfo.totalMem));
            }
            StatFs statFs = new StatFs(Environment.getDataDirectory().getPath());
            f.put("free_space", String.valueOf(((long) statFs.getAvailableBlocks()) * ((long) statFs.getBlockSize())));
            Intent registerReceiver = context.registerReceiver(null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
            if (registerReceiver != null) {
                int intExtra = registerReceiver.getIntExtra(Param.LEVEL, -1);
                int intExtra2 = registerReceiver.getIntExtra("scale", -1);
                int intExtra3 = registerReceiver.getIntExtra("status", -1);
                if (intExtra3 != 2) {
                    if (intExtra3 != 5) {
                        z = false;
                        float f2 = 0.0f;
                        if (intExtra2 > 0) {
                            f2 = 100.0f * (((float) intExtra) / ((float) intExtra2));
                        }
                        f.put("battery", String.valueOf(f2));
                        f.put("charging", !z ? AppEventsConstants.EVENT_PARAM_VALUE_YES : "0");
                    }
                }
                z = true;
                float f22 = 0.0f;
                if (intExtra2 > 0) {
                }
                f.put("battery", String.valueOf(f22));
                f.put("charging", !z ? AppEventsConstants.EVENT_PARAM_VALUE_YES : "0");
            }
            if (a == null) {
                a = (SensorManager) context.getSystemService("sensor");
                if (a == null) {
                    return;
                }
            }
            if (b == null) {
                b = a.getDefaultSensor(1);
            }
            if (c == null) {
                c = a.getDefaultSensor(4);
            }
            if (h == null) {
                h = new C0007a();
                if (b != null) {
                    a.registerListener(h, b, 3);
                }
            }
            if (i == null) {
                i = new b();
                if (c != null) {
                    a.registerListener(i, c, 3);
                }
            }
        }
    }

    private static void a(Map<String, String> map) {
        float[] fArr = d;
        float[] fArr2 = e;
        if (fArr != null) {
            int min = Math.min(g.length, fArr.length);
            for (int i2 = 0; i2 < min; i2++) {
                StringBuilder sb = new StringBuilder();
                sb.append("accelerometer_");
                sb.append(g[i2]);
                map.put(sb.toString(), String.valueOf(fArr[i2]));
            }
        }
        if (fArr2 != null) {
            int min2 = Math.min(g.length, fArr2.length);
            for (int i3 = 0; i3 < min2; i3++) {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("rotation_");
                sb2.append(g[i3]);
                map.put(sb2.toString(), String.valueOf(fArr2[i3]));
            }
        }
    }

    /* access modifiers changed from: private */
    public static synchronized void d() {
        synchronized (a.class) {
            if (a != null) {
                a.unregisterListener(h);
            }
            h = null;
        }
    }

    /* access modifiers changed from: private */
    public static synchronized void e() {
        synchronized (a.class) {
            if (a != null) {
                a.unregisterListener(i);
            }
            i = null;
        }
    }
}
