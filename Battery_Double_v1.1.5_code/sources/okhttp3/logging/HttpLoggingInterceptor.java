package okhttp3.logging;

import io.fabric.sdk.android.services.network.HttpRequest;
import java.io.EOFException;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Collections;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.TimeUnit;
import okhttp3.Connection;
import okhttp3.Headers;
import okhttp3.Interceptor;
import okhttp3.Interceptor.Chain;
import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okhttp3.internal.http.HttpHeaders;
import okhttp3.internal.platform.Platform;
import okio.Buffer;
import okio.BufferedSource;
import okio.GzipSource;

public final class HttpLoggingInterceptor implements Interceptor {
    private static final Charset UTF8 = Charset.forName("UTF-8");
    private volatile Set<String> headersToRedact;
    private volatile Level level;
    private final Logger logger;

    public enum Level {
        NONE,
        BASIC,
        HEADERS,
        BODY
    }

    public interface Logger {
        public static final Logger DEFAULT = new Logger() {
            public void log(String str) {
                Platform.get().log(4, str, null);
            }
        };

        void log(String str);
    }

    public HttpLoggingInterceptor() {
        this(Logger.DEFAULT);
    }

    public HttpLoggingInterceptor(Logger logger2) {
        this.headersToRedact = Collections.emptySet();
        this.level = Level.NONE;
        this.logger = logger2;
    }

    public void redactHeader(String str) {
        TreeSet treeSet = new TreeSet(String.CASE_INSENSITIVE_ORDER);
        treeSet.addAll(this.headersToRedact);
        treeSet.add(str);
        this.headersToRedact = treeSet;
    }

    public HttpLoggingInterceptor setLevel(Level level2) {
        if (level2 != null) {
            this.level = level2;
            return this;
        }
        throw new NullPointerException("level == null. Use Level.NONE instead.");
    }

    public Level getLevel() {
        return this.level;
    }

    /* JADX WARNING: Removed duplicated region for block: B:92:0x02ed  */
    public Response intercept(Chain chain) throws IOException {
        String str;
        String str2;
        String str3;
        String str4;
        Object obj;
        Level level2 = this.level;
        Request request = chain.request();
        if (level2 == Level.NONE) {
            return chain.proceed(request);
        }
        boolean z = true;
        boolean z2 = level2 == Level.BODY;
        boolean z3 = z2 || level2 == Level.HEADERS;
        RequestBody body = request.body();
        if (body == null) {
            z = false;
        }
        Connection connection = chain.connection();
        StringBuilder sb = new StringBuilder();
        sb.append("--> ");
        sb.append(request.method());
        sb.append(' ');
        sb.append(request.url());
        if (connection != null) {
            StringBuilder sb2 = new StringBuilder();
            sb2.append(" ");
            sb2.append(connection.protocol());
            str = sb2.toString();
        } else {
            str = "";
        }
        sb.append(str);
        String sb3 = sb.toString();
        if (!z3 && z) {
            StringBuilder sb4 = new StringBuilder();
            sb4.append(sb3);
            sb4.append(" (");
            sb4.append(body.contentLength());
            sb4.append("-byte body)");
            sb3 = sb4.toString();
        }
        this.logger.log(sb3);
        if (z3) {
            if (z) {
                if (body.contentType() != null) {
                    Logger logger2 = this.logger;
                    StringBuilder sb5 = new StringBuilder();
                    sb5.append("Content-Type: ");
                    sb5.append(body.contentType());
                    logger2.log(sb5.toString());
                }
                if (body.contentLength() != -1) {
                    Logger logger3 = this.logger;
                    StringBuilder sb6 = new StringBuilder();
                    sb6.append("Content-Length: ");
                    sb6.append(body.contentLength());
                    logger3.log(sb6.toString());
                }
            }
            Headers headers = request.headers();
            int size = headers.size();
            for (int i = 0; i < size; i++) {
                String name = headers.name(i);
                if (!"Content-Type".equalsIgnoreCase(name) && !HttpRequest.HEADER_CONTENT_LENGTH.equalsIgnoreCase(name)) {
                    logHeader(headers, i);
                }
            }
            if (!z2 || !z) {
                Logger logger4 = this.logger;
                StringBuilder sb7 = new StringBuilder();
                sb7.append("--> END ");
                sb7.append(request.method());
                logger4.log(sb7.toString());
            } else if (bodyHasUnknownEncoding(request.headers())) {
                Logger logger5 = this.logger;
                StringBuilder sb8 = new StringBuilder();
                sb8.append("--> END ");
                sb8.append(request.method());
                sb8.append(" (encoded body omitted)");
                logger5.log(sb8.toString());
            } else {
                Buffer buffer = new Buffer();
                body.writeTo(buffer);
                Charset charset = UTF8;
                MediaType contentType = body.contentType();
                if (contentType != null) {
                    charset = contentType.charset(UTF8);
                }
                this.logger.log("");
                if (isPlaintext(buffer)) {
                    this.logger.log(buffer.readString(charset));
                    Logger logger6 = this.logger;
                    StringBuilder sb9 = new StringBuilder();
                    sb9.append("--> END ");
                    sb9.append(request.method());
                    sb9.append(" (");
                    sb9.append(body.contentLength());
                    sb9.append("-byte body)");
                    logger6.log(sb9.toString());
                } else {
                    Logger logger7 = this.logger;
                    StringBuilder sb10 = new StringBuilder();
                    sb10.append("--> END ");
                    sb10.append(request.method());
                    sb10.append(" (binary ");
                    sb10.append(body.contentLength());
                    sb10.append("-byte body omitted)");
                    logger7.log(sb10.toString());
                }
            }
        }
        long nanoTime = System.nanoTime();
        try {
            Response proceed = chain.proceed(request);
            long millis = TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - nanoTime);
            ResponseBody body2 = proceed.body();
            long contentLength = body2.contentLength();
            if (contentLength != -1) {
                StringBuilder sb11 = new StringBuilder();
                sb11.append(contentLength);
                sb11.append("-byte");
                str2 = sb11.toString();
            } else {
                str2 = "unknown-length";
            }
            Logger logger8 = this.logger;
            StringBuilder sb12 = new StringBuilder();
            sb12.append("<-- ");
            sb12.append(proceed.code());
            if (proceed.message().isEmpty()) {
                str3 = "";
            } else {
                StringBuilder sb13 = new StringBuilder();
                sb13.append(' ');
                sb13.append(proceed.message());
                str3 = sb13.toString();
            }
            sb12.append(str3);
            sb12.append(' ');
            sb12.append(proceed.request().url());
            sb12.append(" (");
            sb12.append(millis);
            sb12.append("ms");
            if (!z3) {
                StringBuilder sb14 = new StringBuilder();
                sb14.append(", ");
                sb14.append(str2);
                sb14.append(" body");
                str4 = sb14.toString();
            } else {
                str4 = "";
            }
            sb12.append(str4);
            sb12.append(')');
            logger8.log(sb12.toString());
            if (z3) {
                Headers headers2 = proceed.headers();
                int size2 = headers2.size();
                for (int i2 = 0; i2 < size2; i2++) {
                    logHeader(headers2, i2);
                }
                if (!z2 || !HttpHeaders.hasBody(proceed)) {
                    this.logger.log("<-- END HTTP");
                } else if (bodyHasUnknownEncoding(proceed.headers())) {
                    this.logger.log("<-- END HTTP (encoded body omitted)");
                } else {
                    BufferedSource source = body2.source();
                    source.request(Long.MAX_VALUE);
                    Buffer buffer2 = source.buffer();
                    GzipSource gzipSource = null;
                    if (HttpRequest.ENCODING_GZIP.equalsIgnoreCase(headers2.get(HttpRequest.HEADER_CONTENT_ENCODING))) {
                        obj = Long.valueOf(buffer2.size());
                        try {
                            GzipSource gzipSource2 = new GzipSource(buffer2.clone());
                            try {
                                buffer2 = new Buffer();
                                buffer2.writeAll(gzipSource2);
                                gzipSource2.close();
                            } catch (Throwable th) {
                                th = th;
                                gzipSource = gzipSource2;
                                if (gzipSource != null) {
                                    gzipSource.close();
                                }
                                throw th;
                            }
                        } catch (Throwable th2) {
                            th = th2;
                            if (gzipSource != null) {
                            }
                            throw th;
                        }
                    } else {
                        obj = null;
                    }
                    Charset charset2 = UTF8;
                    MediaType contentType2 = body2.contentType();
                    if (contentType2 != null) {
                        charset2 = contentType2.charset(UTF8);
                    }
                    if (!isPlaintext(buffer2)) {
                        this.logger.log("");
                        Logger logger9 = this.logger;
                        StringBuilder sb15 = new StringBuilder();
                        sb15.append("<-- END HTTP (binary ");
                        sb15.append(buffer2.size());
                        sb15.append("-byte body omitted)");
                        logger9.log(sb15.toString());
                        return proceed;
                    }
                    if (contentLength != 0) {
                        this.logger.log("");
                        this.logger.log(buffer2.clone().readString(charset2));
                    }
                    if (obj != null) {
                        Logger logger10 = this.logger;
                        StringBuilder sb16 = new StringBuilder();
                        sb16.append("<-- END HTTP (");
                        sb16.append(buffer2.size());
                        sb16.append("-byte, ");
                        sb16.append(obj);
                        sb16.append("-gzipped-byte body)");
                        logger10.log(sb16.toString());
                    } else {
                        Logger logger11 = this.logger;
                        StringBuilder sb17 = new StringBuilder();
                        sb17.append("<-- END HTTP (");
                        sb17.append(buffer2.size());
                        sb17.append("-byte body)");
                        logger11.log(sb17.toString());
                    }
                }
            }
            return proceed;
        } catch (Exception e) {
            Logger logger12 = this.logger;
            StringBuilder sb18 = new StringBuilder();
            sb18.append("<-- HTTP FAILED: ");
            sb18.append(e);
            logger12.log(sb18.toString());
            throw e;
        }
    }

    private void logHeader(Headers headers, int i) {
        String value = this.headersToRedact.contains(headers.name(i)) ? "██" : headers.value(i);
        Logger logger2 = this.logger;
        StringBuilder sb = new StringBuilder();
        sb.append(headers.name(i));
        sb.append(": ");
        sb.append(value);
        logger2.log(sb.toString());
    }

    static boolean isPlaintext(Buffer buffer) {
        try {
            Buffer buffer2 = new Buffer();
            buffer.copyTo(buffer2, 0, buffer.size() < 64 ? buffer.size() : 64);
            int i = 0;
            while (true) {
                if (i >= 16) {
                    break;
                } else if (buffer2.exhausted()) {
                    break;
                } else {
                    int readUtf8CodePoint = buffer2.readUtf8CodePoint();
                    if (Character.isISOControl(readUtf8CodePoint) && !Character.isWhitespace(readUtf8CodePoint)) {
                        return false;
                    }
                    i++;
                }
            }
            return true;
        } catch (EOFException unused) {
            return false;
        }
    }

    private static boolean bodyHasUnknownEncoding(Headers headers) {
        String str = headers.get(HttpRequest.HEADER_CONTENT_ENCODING);
        return str != null && !str.equalsIgnoreCase("identity") && !str.equalsIgnoreCase(HttpRequest.ENCODING_GZIP);
    }
}
