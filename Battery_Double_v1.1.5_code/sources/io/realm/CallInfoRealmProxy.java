package io.realm;

import android.annotation.TargetApi;
import android.util.JsonReader;
import android.util.JsonToken;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.mansoon.BatteryDouble.models.data.CallInfo;
import io.realm.BaseRealm.RealmObjectContext;
import io.realm.internal.ColumnInfo;
import io.realm.internal.OsObject;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.OsObjectSchemaInfo.Builder;
import io.realm.internal.OsSchemaInfo;
import io.realm.internal.RealmObjectProxy;
import io.realm.internal.RealmObjectProxy.CacheData;
import io.realm.internal.Row;
import io.realm.internal.Table;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class CallInfoRealmProxy extends CallInfo implements RealmObjectProxy, CallInfoRealmProxyInterface {
    private static final List<String> FIELD_NAMES;
    private static final OsObjectSchemaInfo expectedObjectSchemaInfo = createExpectedObjectSchemaInfo();
    private CallInfoColumnInfo columnInfo;
    private ProxyState<CallInfo> proxyState;

    static final class CallInfoColumnInfo extends ColumnInfo {
        long callStatusIndex;
        long incomingCallTimeIndex;
        long nonCallTimeIndex;
        long outgoingCallTimeIndex;

        CallInfoColumnInfo(OsSchemaInfo osSchemaInfo) {
            super(4);
            OsObjectSchemaInfo objectSchemaInfo = osSchemaInfo.getObjectSchemaInfo("CallInfo");
            this.incomingCallTimeIndex = addColumnDetails("incomingCallTime", objectSchemaInfo);
            this.outgoingCallTimeIndex = addColumnDetails("outgoingCallTime", objectSchemaInfo);
            this.nonCallTimeIndex = addColumnDetails("nonCallTime", objectSchemaInfo);
            this.callStatusIndex = addColumnDetails("callStatus", objectSchemaInfo);
        }

        CallInfoColumnInfo(ColumnInfo columnInfo, boolean z) {
            super(columnInfo, z);
            copy(columnInfo, this);
        }

        /* access modifiers changed from: protected */
        public final ColumnInfo copy(boolean z) {
            return new CallInfoColumnInfo(this, z);
        }

        /* access modifiers changed from: protected */
        public final void copy(ColumnInfo columnInfo, ColumnInfo columnInfo2) {
            CallInfoColumnInfo callInfoColumnInfo = (CallInfoColumnInfo) columnInfo;
            CallInfoColumnInfo callInfoColumnInfo2 = (CallInfoColumnInfo) columnInfo2;
            callInfoColumnInfo2.incomingCallTimeIndex = callInfoColumnInfo.incomingCallTimeIndex;
            callInfoColumnInfo2.outgoingCallTimeIndex = callInfoColumnInfo.outgoingCallTimeIndex;
            callInfoColumnInfo2.nonCallTimeIndex = callInfoColumnInfo.nonCallTimeIndex;
            callInfoColumnInfo2.callStatusIndex = callInfoColumnInfo.callStatusIndex;
        }
    }

    public static String getTableName() {
        return "class_CallInfo";
    }

    static {
        ArrayList arrayList = new ArrayList(4);
        arrayList.add("incomingCallTime");
        arrayList.add("outgoingCallTime");
        arrayList.add("nonCallTime");
        arrayList.add("callStatus");
        FIELD_NAMES = Collections.unmodifiableList(arrayList);
    }

    CallInfoRealmProxy() {
        this.proxyState.setConstructionFinished();
    }

    public void realm$injectObjectContext() {
        if (this.proxyState == null) {
            RealmObjectContext realmObjectContext = (RealmObjectContext) BaseRealm.objectContext.get();
            this.columnInfo = (CallInfoColumnInfo) realmObjectContext.getColumnInfo();
            this.proxyState = new ProxyState<>(this);
            this.proxyState.setRealm$realm(realmObjectContext.getRealm());
            this.proxyState.setRow$realm(realmObjectContext.getRow());
            this.proxyState.setAcceptDefaultValue$realm(realmObjectContext.getAcceptDefaultValue());
            this.proxyState.setExcludeFields$realm(realmObjectContext.getExcludeFields());
        }
    }

    public double realmGet$incomingCallTime() {
        this.proxyState.getRealm$realm().checkIfValid();
        return this.proxyState.getRow$realm().getDouble(this.columnInfo.incomingCallTimeIndex);
    }

    public void realmSet$incomingCallTime(double d) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            this.proxyState.getRow$realm().setDouble(this.columnInfo.incomingCallTimeIndex, d);
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            row$realm.getTable().setDouble(this.columnInfo.incomingCallTimeIndex, row$realm.getIndex(), d, true);
        }
    }

    public double realmGet$outgoingCallTime() {
        this.proxyState.getRealm$realm().checkIfValid();
        return this.proxyState.getRow$realm().getDouble(this.columnInfo.outgoingCallTimeIndex);
    }

    public void realmSet$outgoingCallTime(double d) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            this.proxyState.getRow$realm().setDouble(this.columnInfo.outgoingCallTimeIndex, d);
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            row$realm.getTable().setDouble(this.columnInfo.outgoingCallTimeIndex, row$realm.getIndex(), d, true);
        }
    }

    public double realmGet$nonCallTime() {
        this.proxyState.getRealm$realm().checkIfValid();
        return this.proxyState.getRow$realm().getDouble(this.columnInfo.nonCallTimeIndex);
    }

    public void realmSet$nonCallTime(double d) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            this.proxyState.getRow$realm().setDouble(this.columnInfo.nonCallTimeIndex, d);
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            row$realm.getTable().setDouble(this.columnInfo.nonCallTimeIndex, row$realm.getIndex(), d, true);
        }
    }

    public String realmGet$callStatus() {
        this.proxyState.getRealm$realm().checkIfValid();
        return this.proxyState.getRow$realm().getString(this.columnInfo.callStatusIndex);
    }

    public void realmSet$callStatus(String str) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            if (str == null) {
                this.proxyState.getRow$realm().setNull(this.columnInfo.callStatusIndex);
            } else {
                this.proxyState.getRow$realm().setString(this.columnInfo.callStatusIndex, str);
            }
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            if (str == null) {
                row$realm.getTable().setNull(this.columnInfo.callStatusIndex, row$realm.getIndex(), true);
            } else {
                row$realm.getTable().setString(this.columnInfo.callStatusIndex, row$realm.getIndex(), str, true);
            }
        }
    }

    private static OsObjectSchemaInfo createExpectedObjectSchemaInfo() {
        Builder builder = new Builder("CallInfo", 4, 0);
        Builder builder2 = builder;
        builder2.addPersistedProperty("incomingCallTime", RealmFieldType.DOUBLE, false, false, true);
        builder2.addPersistedProperty("outgoingCallTime", RealmFieldType.DOUBLE, false, false, true);
        builder2.addPersistedProperty("nonCallTime", RealmFieldType.DOUBLE, false, false, true);
        builder2.addPersistedProperty("callStatus", RealmFieldType.STRING, false, false, false);
        return builder.build();
    }

    public static OsObjectSchemaInfo getExpectedObjectSchemaInfo() {
        return expectedObjectSchemaInfo;
    }

    public static CallInfoColumnInfo createColumnInfo(OsSchemaInfo osSchemaInfo) {
        return new CallInfoColumnInfo(osSchemaInfo);
    }

    public static List<String> getFieldNames() {
        return FIELD_NAMES;
    }

    public static CallInfo createOrUpdateUsingJsonObject(Realm realm, JSONObject jSONObject, boolean z) throws JSONException {
        CallInfo callInfo = (CallInfo) realm.createObjectInternal(CallInfo.class, true, Collections.emptyList());
        CallInfoRealmProxyInterface callInfoRealmProxyInterface = callInfo;
        if (jSONObject.has("incomingCallTime")) {
            if (!jSONObject.isNull("incomingCallTime")) {
                callInfoRealmProxyInterface.realmSet$incomingCallTime(jSONObject.getDouble("incomingCallTime"));
            } else {
                throw new IllegalArgumentException("Trying to set non-nullable field 'incomingCallTime' to null.");
            }
        }
        if (jSONObject.has("outgoingCallTime")) {
            if (!jSONObject.isNull("outgoingCallTime")) {
                callInfoRealmProxyInterface.realmSet$outgoingCallTime(jSONObject.getDouble("outgoingCallTime"));
            } else {
                throw new IllegalArgumentException("Trying to set non-nullable field 'outgoingCallTime' to null.");
            }
        }
        if (jSONObject.has("nonCallTime")) {
            if (!jSONObject.isNull("nonCallTime")) {
                callInfoRealmProxyInterface.realmSet$nonCallTime(jSONObject.getDouble("nonCallTime"));
            } else {
                throw new IllegalArgumentException("Trying to set non-nullable field 'nonCallTime' to null.");
            }
        }
        if (jSONObject.has("callStatus")) {
            if (jSONObject.isNull("callStatus")) {
                callInfoRealmProxyInterface.realmSet$callStatus(null);
            } else {
                callInfoRealmProxyInterface.realmSet$callStatus(jSONObject.getString("callStatus"));
            }
        }
        return callInfo;
    }

    @TargetApi(11)
    public static CallInfo createUsingJsonStream(Realm realm, JsonReader jsonReader) throws IOException {
        CallInfo callInfo = new CallInfo();
        CallInfoRealmProxyInterface callInfoRealmProxyInterface = callInfo;
        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            String nextName = jsonReader.nextName();
            if (nextName.equals("incomingCallTime")) {
                if (jsonReader.peek() != JsonToken.NULL) {
                    callInfoRealmProxyInterface.realmSet$incomingCallTime(jsonReader.nextDouble());
                } else {
                    jsonReader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'incomingCallTime' to null.");
                }
            } else if (nextName.equals("outgoingCallTime")) {
                if (jsonReader.peek() != JsonToken.NULL) {
                    callInfoRealmProxyInterface.realmSet$outgoingCallTime(jsonReader.nextDouble());
                } else {
                    jsonReader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'outgoingCallTime' to null.");
                }
            } else if (nextName.equals("nonCallTime")) {
                if (jsonReader.peek() != JsonToken.NULL) {
                    callInfoRealmProxyInterface.realmSet$nonCallTime(jsonReader.nextDouble());
                } else {
                    jsonReader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'nonCallTime' to null.");
                }
            } else if (!nextName.equals("callStatus")) {
                jsonReader.skipValue();
            } else if (jsonReader.peek() != JsonToken.NULL) {
                callInfoRealmProxyInterface.realmSet$callStatus(jsonReader.nextString());
            } else {
                jsonReader.skipValue();
                callInfoRealmProxyInterface.realmSet$callStatus(null);
            }
        }
        jsonReader.endObject();
        return (CallInfo) realm.copyToRealm(callInfo);
    }

    public static CallInfo copyOrUpdate(Realm realm, CallInfo callInfo, boolean z, Map<RealmModel, RealmObjectProxy> map) {
        if (callInfo instanceof RealmObjectProxy) {
            RealmObjectProxy realmObjectProxy = (RealmObjectProxy) callInfo;
            if (realmObjectProxy.realmGet$proxyState().getRealm$realm() != null) {
                BaseRealm realm$realm = realmObjectProxy.realmGet$proxyState().getRealm$realm();
                if (realm$realm.threadId != realm.threadId) {
                    throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
                } else if (realm$realm.getPath().equals(realm.getPath())) {
                    return callInfo;
                }
            }
        }
        RealmObjectContext realmObjectContext = (RealmObjectContext) BaseRealm.objectContext.get();
        RealmObjectProxy realmObjectProxy2 = (RealmObjectProxy) map.get(callInfo);
        if (realmObjectProxy2 != null) {
            return (CallInfo) realmObjectProxy2;
        }
        return copy(realm, callInfo, z, map);
    }

    public static CallInfo copy(Realm realm, CallInfo callInfo, boolean z, Map<RealmModel, RealmObjectProxy> map) {
        RealmObjectProxy realmObjectProxy = (RealmObjectProxy) map.get(callInfo);
        if (realmObjectProxy != null) {
            return (CallInfo) realmObjectProxy;
        }
        CallInfo callInfo2 = (CallInfo) realm.createObjectInternal(CallInfo.class, false, Collections.emptyList());
        map.put(callInfo, (RealmObjectProxy) callInfo2);
        CallInfoRealmProxyInterface callInfoRealmProxyInterface = callInfo;
        CallInfoRealmProxyInterface callInfoRealmProxyInterface2 = callInfo2;
        callInfoRealmProxyInterface2.realmSet$incomingCallTime(callInfoRealmProxyInterface.realmGet$incomingCallTime());
        callInfoRealmProxyInterface2.realmSet$outgoingCallTime(callInfoRealmProxyInterface.realmGet$outgoingCallTime());
        callInfoRealmProxyInterface2.realmSet$nonCallTime(callInfoRealmProxyInterface.realmGet$nonCallTime());
        callInfoRealmProxyInterface2.realmSet$callStatus(callInfoRealmProxyInterface.realmGet$callStatus());
        return callInfo2;
    }

    public static long insert(Realm realm, CallInfo callInfo, Map<RealmModel, Long> map) {
        CallInfo callInfo2 = callInfo;
        if (callInfo2 instanceof RealmObjectProxy) {
            RealmObjectProxy realmObjectProxy = (RealmObjectProxy) callInfo2;
            if (realmObjectProxy.realmGet$proxyState().getRealm$realm() != null && realmObjectProxy.realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                return realmObjectProxy.realmGet$proxyState().getRow$realm().getIndex();
            }
        }
        Table table = realm.getTable(CallInfo.class);
        long nativePtr = table.getNativePtr();
        CallInfoColumnInfo callInfoColumnInfo = (CallInfoColumnInfo) realm.getSchema().getColumnInfo(CallInfo.class);
        long createRow = OsObject.createRow(table);
        map.put(callInfo2, Long.valueOf(createRow));
        CallInfoRealmProxyInterface callInfoRealmProxyInterface = callInfo2;
        long j = nativePtr;
        long j2 = createRow;
        Table.nativeSetDouble(j, callInfoColumnInfo.incomingCallTimeIndex, j2, callInfoRealmProxyInterface.realmGet$incomingCallTime(), false);
        Table.nativeSetDouble(j, callInfoColumnInfo.outgoingCallTimeIndex, j2, callInfoRealmProxyInterface.realmGet$outgoingCallTime(), false);
        Table.nativeSetDouble(j, callInfoColumnInfo.nonCallTimeIndex, j2, callInfoRealmProxyInterface.realmGet$nonCallTime(), false);
        String realmGet$callStatus = callInfoRealmProxyInterface.realmGet$callStatus();
        if (realmGet$callStatus != null) {
            Table.nativeSetString(nativePtr, callInfoColumnInfo.callStatusIndex, createRow, realmGet$callStatus, false);
        }
        return createRow;
    }

    public static void insert(Realm realm, Iterator<? extends RealmModel> it, Map<RealmModel, Long> map) {
        Map<RealmModel, Long> map2 = map;
        Table table = realm.getTable(CallInfo.class);
        long nativePtr = table.getNativePtr();
        CallInfoColumnInfo callInfoColumnInfo = (CallInfoColumnInfo) realm.getSchema().getColumnInfo(CallInfo.class);
        while (it.hasNext()) {
            CallInfo callInfo = (CallInfo) it.next();
            if (!map2.containsKey(callInfo)) {
                if (callInfo instanceof RealmObjectProxy) {
                    RealmObjectProxy realmObjectProxy = (RealmObjectProxy) callInfo;
                    if (realmObjectProxy.realmGet$proxyState().getRealm$realm() != null && realmObjectProxy.realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                        map2.put(callInfo, Long.valueOf(realmObjectProxy.realmGet$proxyState().getRow$realm().getIndex()));
                    }
                }
                long createRow = OsObject.createRow(table);
                map2.put(callInfo, Long.valueOf(createRow));
                CallInfoRealmProxyInterface callInfoRealmProxyInterface = callInfo;
                long j = nativePtr;
                long j2 = createRow;
                Table.nativeSetDouble(j, callInfoColumnInfo.incomingCallTimeIndex, j2, callInfoRealmProxyInterface.realmGet$incomingCallTime(), false);
                Table.nativeSetDouble(j, callInfoColumnInfo.outgoingCallTimeIndex, j2, callInfoRealmProxyInterface.realmGet$outgoingCallTime(), false);
                Table.nativeSetDouble(j, callInfoColumnInfo.nonCallTimeIndex, j2, callInfoRealmProxyInterface.realmGet$nonCallTime(), false);
                String realmGet$callStatus = callInfoRealmProxyInterface.realmGet$callStatus();
                if (realmGet$callStatus != null) {
                    Table.nativeSetString(nativePtr, callInfoColumnInfo.callStatusIndex, createRow, realmGet$callStatus, false);
                }
            }
        }
    }

    public static long insertOrUpdate(Realm realm, CallInfo callInfo, Map<RealmModel, Long> map) {
        CallInfo callInfo2 = callInfo;
        if (callInfo2 instanceof RealmObjectProxy) {
            RealmObjectProxy realmObjectProxy = (RealmObjectProxy) callInfo2;
            if (realmObjectProxy.realmGet$proxyState().getRealm$realm() != null && realmObjectProxy.realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                return realmObjectProxy.realmGet$proxyState().getRow$realm().getIndex();
            }
        }
        Table table = realm.getTable(CallInfo.class);
        long nativePtr = table.getNativePtr();
        CallInfoColumnInfo callInfoColumnInfo = (CallInfoColumnInfo) realm.getSchema().getColumnInfo(CallInfo.class);
        long createRow = OsObject.createRow(table);
        map.put(callInfo2, Long.valueOf(createRow));
        CallInfoRealmProxyInterface callInfoRealmProxyInterface = callInfo2;
        long j = nativePtr;
        long j2 = createRow;
        Table.nativeSetDouble(j, callInfoColumnInfo.incomingCallTimeIndex, j2, callInfoRealmProxyInterface.realmGet$incomingCallTime(), false);
        Table.nativeSetDouble(j, callInfoColumnInfo.outgoingCallTimeIndex, j2, callInfoRealmProxyInterface.realmGet$outgoingCallTime(), false);
        Table.nativeSetDouble(j, callInfoColumnInfo.nonCallTimeIndex, j2, callInfoRealmProxyInterface.realmGet$nonCallTime(), false);
        String realmGet$callStatus = callInfoRealmProxyInterface.realmGet$callStatus();
        if (realmGet$callStatus != null) {
            Table.nativeSetString(nativePtr, callInfoColumnInfo.callStatusIndex, createRow, realmGet$callStatus, false);
        } else {
            Table.nativeSetNull(nativePtr, callInfoColumnInfo.callStatusIndex, createRow, false);
        }
        return createRow;
    }

    public static void insertOrUpdate(Realm realm, Iterator<? extends RealmModel> it, Map<RealmModel, Long> map) {
        Map<RealmModel, Long> map2 = map;
        Table table = realm.getTable(CallInfo.class);
        long nativePtr = table.getNativePtr();
        CallInfoColumnInfo callInfoColumnInfo = (CallInfoColumnInfo) realm.getSchema().getColumnInfo(CallInfo.class);
        while (it.hasNext()) {
            CallInfo callInfo = (CallInfo) it.next();
            if (!map2.containsKey(callInfo)) {
                if (callInfo instanceof RealmObjectProxy) {
                    RealmObjectProxy realmObjectProxy = (RealmObjectProxy) callInfo;
                    if (realmObjectProxy.realmGet$proxyState().getRealm$realm() != null && realmObjectProxy.realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                        map2.put(callInfo, Long.valueOf(realmObjectProxy.realmGet$proxyState().getRow$realm().getIndex()));
                    }
                }
                long createRow = OsObject.createRow(table);
                map2.put(callInfo, Long.valueOf(createRow));
                CallInfoRealmProxyInterface callInfoRealmProxyInterface = callInfo;
                long j = nativePtr;
                long j2 = createRow;
                Table.nativeSetDouble(j, callInfoColumnInfo.incomingCallTimeIndex, j2, callInfoRealmProxyInterface.realmGet$incomingCallTime(), false);
                Table.nativeSetDouble(j, callInfoColumnInfo.outgoingCallTimeIndex, j2, callInfoRealmProxyInterface.realmGet$outgoingCallTime(), false);
                Table.nativeSetDouble(j, callInfoColumnInfo.nonCallTimeIndex, j2, callInfoRealmProxyInterface.realmGet$nonCallTime(), false);
                String realmGet$callStatus = callInfoRealmProxyInterface.realmGet$callStatus();
                if (realmGet$callStatus != null) {
                    Table.nativeSetString(nativePtr, callInfoColumnInfo.callStatusIndex, createRow, realmGet$callStatus, false);
                } else {
                    Table.nativeSetNull(nativePtr, callInfoColumnInfo.callStatusIndex, createRow, false);
                }
            }
        }
    }

    public static CallInfo createDetachedCopy(CallInfo callInfo, int i, int i2, Map<RealmModel, CacheData<RealmModel>> map) {
        CallInfo callInfo2;
        if (i > i2 || callInfo == null) {
            return null;
        }
        CacheData cacheData = (CacheData) map.get(callInfo);
        if (cacheData == null) {
            callInfo2 = new CallInfo();
            map.put(callInfo, new CacheData(i, callInfo2));
        } else if (i >= cacheData.minDepth) {
            return (CallInfo) cacheData.object;
        } else {
            CallInfo callInfo3 = (CallInfo) cacheData.object;
            cacheData.minDepth = i;
            callInfo2 = callInfo3;
        }
        CallInfoRealmProxyInterface callInfoRealmProxyInterface = callInfo2;
        CallInfoRealmProxyInterface callInfoRealmProxyInterface2 = callInfo;
        callInfoRealmProxyInterface.realmSet$incomingCallTime(callInfoRealmProxyInterface2.realmGet$incomingCallTime());
        callInfoRealmProxyInterface.realmSet$outgoingCallTime(callInfoRealmProxyInterface2.realmGet$outgoingCallTime());
        callInfoRealmProxyInterface.realmSet$nonCallTime(callInfoRealmProxyInterface2.realmGet$nonCallTime());
        callInfoRealmProxyInterface.realmSet$callStatus(callInfoRealmProxyInterface2.realmGet$callStatus());
        return callInfo2;
    }

    public String toString() {
        if (!RealmObject.isValid(this)) {
            return "Invalid object";
        }
        StringBuilder sb = new StringBuilder("CallInfo = proxy[");
        sb.append("{incomingCallTime:");
        sb.append(realmGet$incomingCallTime());
        sb.append("}");
        sb.append(",");
        sb.append("{outgoingCallTime:");
        sb.append(realmGet$outgoingCallTime());
        sb.append("}");
        sb.append(",");
        sb.append("{nonCallTime:");
        sb.append(realmGet$nonCallTime());
        sb.append("}");
        sb.append(",");
        sb.append("{callStatus:");
        sb.append(realmGet$callStatus() != null ? realmGet$callStatus() : "null");
        sb.append("}");
        sb.append(RequestParameters.RIGHT_BRACKETS);
        return sb.toString();
    }

    public ProxyState<?> realmGet$proxyState() {
        return this.proxyState;
    }

    public int hashCode() {
        String path = this.proxyState.getRealm$realm().getPath();
        String name = this.proxyState.getRow$realm().getTable().getName();
        long index = this.proxyState.getRow$realm().getIndex();
        int i = 0;
        int hashCode = (IronSourceError.ERROR_NON_EXISTENT_INSTANCE + (path != null ? path.hashCode() : 0)) * 31;
        if (name != null) {
            i = name.hashCode();
        }
        return ((hashCode + i) * 31) + ((int) ((index >>> 32) ^ index));
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        CallInfoRealmProxy callInfoRealmProxy = (CallInfoRealmProxy) obj;
        String path = this.proxyState.getRealm$realm().getPath();
        String path2 = callInfoRealmProxy.proxyState.getRealm$realm().getPath();
        if (path == null ? path2 != null : !path.equals(path2)) {
            return false;
        }
        String name = this.proxyState.getRow$realm().getTable().getName();
        String name2 = callInfoRealmProxy.proxyState.getRow$realm().getTable().getName();
        if (name == null ? name2 == null : name.equals(name2)) {
            return this.proxyState.getRow$realm().getIndex() == callInfoRealmProxy.proxyState.getRow$realm().getIndex();
        }
        return false;
    }
}
