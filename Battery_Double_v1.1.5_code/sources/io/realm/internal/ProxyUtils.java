package io.realm.internal;

import io.realm.RealmFieldType;
import io.realm.exceptions.RealmMigrationNeededException;
import java.util.Map;

public class ProxyUtils {
    public static void verifyField(SharedRealm sharedRealm, Map<String, RealmFieldType> map, String str, RealmFieldType realmFieldType, String str2) {
        if (!map.containsKey(str)) {
            throw new RealmMigrationNeededException(sharedRealm.getPath(), String.format("Missing field '%s' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn().", new Object[]{str}));
        } else if (map.get(str) != realmFieldType) {
            throw new RealmMigrationNeededException(sharedRealm.getPath(), String.format("Invalid type '%s' for field '%s' in existing Realm file.", new Object[]{str2, str}));
        }
    }
}
