package io.realm;

import android.annotation.TargetApi;
import android.util.JsonReader;
import android.util.JsonToken;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.mansoon.BatteryDouble.models.data.AppSignature;
import com.unity3d.ads.metadata.InAppPurchaseMetaData;
import io.realm.BaseRealm.RealmObjectContext;
import io.realm.internal.ColumnInfo;
import io.realm.internal.OsObject;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.OsObjectSchemaInfo.Builder;
import io.realm.internal.OsSchemaInfo;
import io.realm.internal.RealmObjectProxy;
import io.realm.internal.RealmObjectProxy.CacheData;
import io.realm.internal.Row;
import io.realm.internal.Table;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class AppSignatureRealmProxy extends AppSignature implements RealmObjectProxy, AppSignatureRealmProxyInterface {
    private static final List<String> FIELD_NAMES;
    private static final OsObjectSchemaInfo expectedObjectSchemaInfo = createExpectedObjectSchemaInfo();
    private AppSignatureColumnInfo columnInfo;
    private ProxyState<AppSignature> proxyState;

    static final class AppSignatureColumnInfo extends ColumnInfo {
        long signatureIndex;

        AppSignatureColumnInfo(OsSchemaInfo osSchemaInfo) {
            super(1);
            this.signatureIndex = addColumnDetails(InAppPurchaseMetaData.KEY_SIGNATURE, osSchemaInfo.getObjectSchemaInfo("AppSignature"));
        }

        AppSignatureColumnInfo(ColumnInfo columnInfo, boolean z) {
            super(columnInfo, z);
            copy(columnInfo, this);
        }

        /* access modifiers changed from: protected */
        public final ColumnInfo copy(boolean z) {
            return new AppSignatureColumnInfo(this, z);
        }

        /* access modifiers changed from: protected */
        public final void copy(ColumnInfo columnInfo, ColumnInfo columnInfo2) {
            ((AppSignatureColumnInfo) columnInfo2).signatureIndex = ((AppSignatureColumnInfo) columnInfo).signatureIndex;
        }
    }

    public static String getTableName() {
        return "class_AppSignature";
    }

    static {
        ArrayList arrayList = new ArrayList(1);
        arrayList.add(InAppPurchaseMetaData.KEY_SIGNATURE);
        FIELD_NAMES = Collections.unmodifiableList(arrayList);
    }

    AppSignatureRealmProxy() {
        this.proxyState.setConstructionFinished();
    }

    public void realm$injectObjectContext() {
        if (this.proxyState == null) {
            RealmObjectContext realmObjectContext = (RealmObjectContext) BaseRealm.objectContext.get();
            this.columnInfo = (AppSignatureColumnInfo) realmObjectContext.getColumnInfo();
            this.proxyState = new ProxyState<>(this);
            this.proxyState.setRealm$realm(realmObjectContext.getRealm());
            this.proxyState.setRow$realm(realmObjectContext.getRow());
            this.proxyState.setAcceptDefaultValue$realm(realmObjectContext.getAcceptDefaultValue());
            this.proxyState.setExcludeFields$realm(realmObjectContext.getExcludeFields());
        }
    }

    public String realmGet$signature() {
        this.proxyState.getRealm$realm().checkIfValid();
        return this.proxyState.getRow$realm().getString(this.columnInfo.signatureIndex);
    }

    public void realmSet$signature(String str) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            if (str == null) {
                this.proxyState.getRow$realm().setNull(this.columnInfo.signatureIndex);
            } else {
                this.proxyState.getRow$realm().setString(this.columnInfo.signatureIndex, str);
            }
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            if (str == null) {
                row$realm.getTable().setNull(this.columnInfo.signatureIndex, row$realm.getIndex(), true);
            } else {
                row$realm.getTable().setString(this.columnInfo.signatureIndex, row$realm.getIndex(), str, true);
            }
        }
    }

    private static OsObjectSchemaInfo createExpectedObjectSchemaInfo() {
        Builder builder = new Builder("AppSignature", 1, 0);
        builder.addPersistedProperty(InAppPurchaseMetaData.KEY_SIGNATURE, RealmFieldType.STRING, false, false, false);
        return builder.build();
    }

    public static OsObjectSchemaInfo getExpectedObjectSchemaInfo() {
        return expectedObjectSchemaInfo;
    }

    public static AppSignatureColumnInfo createColumnInfo(OsSchemaInfo osSchemaInfo) {
        return new AppSignatureColumnInfo(osSchemaInfo);
    }

    public static List<String> getFieldNames() {
        return FIELD_NAMES;
    }

    public static AppSignature createOrUpdateUsingJsonObject(Realm realm, JSONObject jSONObject, boolean z) throws JSONException {
        AppSignature appSignature = (AppSignature) realm.createObjectInternal(AppSignature.class, true, Collections.emptyList());
        AppSignatureRealmProxyInterface appSignatureRealmProxyInterface = appSignature;
        if (jSONObject.has(InAppPurchaseMetaData.KEY_SIGNATURE)) {
            if (jSONObject.isNull(InAppPurchaseMetaData.KEY_SIGNATURE)) {
                appSignatureRealmProxyInterface.realmSet$signature(null);
            } else {
                appSignatureRealmProxyInterface.realmSet$signature(jSONObject.getString(InAppPurchaseMetaData.KEY_SIGNATURE));
            }
        }
        return appSignature;
    }

    @TargetApi(11)
    public static AppSignature createUsingJsonStream(Realm realm, JsonReader jsonReader) throws IOException {
        AppSignature appSignature = new AppSignature();
        AppSignatureRealmProxyInterface appSignatureRealmProxyInterface = appSignature;
        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            if (!jsonReader.nextName().equals(InAppPurchaseMetaData.KEY_SIGNATURE)) {
                jsonReader.skipValue();
            } else if (jsonReader.peek() != JsonToken.NULL) {
                appSignatureRealmProxyInterface.realmSet$signature(jsonReader.nextString());
            } else {
                jsonReader.skipValue();
                appSignatureRealmProxyInterface.realmSet$signature(null);
            }
        }
        jsonReader.endObject();
        return (AppSignature) realm.copyToRealm(appSignature);
    }

    public static AppSignature copyOrUpdate(Realm realm, AppSignature appSignature, boolean z, Map<RealmModel, RealmObjectProxy> map) {
        if (appSignature instanceof RealmObjectProxy) {
            RealmObjectProxy realmObjectProxy = (RealmObjectProxy) appSignature;
            if (realmObjectProxy.realmGet$proxyState().getRealm$realm() != null) {
                BaseRealm realm$realm = realmObjectProxy.realmGet$proxyState().getRealm$realm();
                if (realm$realm.threadId != realm.threadId) {
                    throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
                } else if (realm$realm.getPath().equals(realm.getPath())) {
                    return appSignature;
                }
            }
        }
        RealmObjectContext realmObjectContext = (RealmObjectContext) BaseRealm.objectContext.get();
        RealmObjectProxy realmObjectProxy2 = (RealmObjectProxy) map.get(appSignature);
        if (realmObjectProxy2 != null) {
            return (AppSignature) realmObjectProxy2;
        }
        return copy(realm, appSignature, z, map);
    }

    public static AppSignature copy(Realm realm, AppSignature appSignature, boolean z, Map<RealmModel, RealmObjectProxy> map) {
        RealmObjectProxy realmObjectProxy = (RealmObjectProxy) map.get(appSignature);
        if (realmObjectProxy != null) {
            return (AppSignature) realmObjectProxy;
        }
        AppSignature appSignature2 = (AppSignature) realm.createObjectInternal(AppSignature.class, false, Collections.emptyList());
        map.put(appSignature, (RealmObjectProxy) appSignature2);
        appSignature2.realmSet$signature(appSignature.realmGet$signature());
        return appSignature2;
    }

    public static long insert(Realm realm, AppSignature appSignature, Map<RealmModel, Long> map) {
        if (appSignature instanceof RealmObjectProxy) {
            RealmObjectProxy realmObjectProxy = (RealmObjectProxy) appSignature;
            if (realmObjectProxy.realmGet$proxyState().getRealm$realm() != null && realmObjectProxy.realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                return realmObjectProxy.realmGet$proxyState().getRow$realm().getIndex();
            }
        }
        Table table = realm.getTable(AppSignature.class);
        long nativePtr = table.getNativePtr();
        AppSignatureColumnInfo appSignatureColumnInfo = (AppSignatureColumnInfo) realm.getSchema().getColumnInfo(AppSignature.class);
        long createRow = OsObject.createRow(table);
        map.put(appSignature, Long.valueOf(createRow));
        String realmGet$signature = appSignature.realmGet$signature();
        if (realmGet$signature != null) {
            Table.nativeSetString(nativePtr, appSignatureColumnInfo.signatureIndex, createRow, realmGet$signature, false);
        }
        return createRow;
    }

    public static void insert(Realm realm, Iterator<? extends RealmModel> it, Map<RealmModel, Long> map) {
        Table table = realm.getTable(AppSignature.class);
        long nativePtr = table.getNativePtr();
        AppSignatureColumnInfo appSignatureColumnInfo = (AppSignatureColumnInfo) realm.getSchema().getColumnInfo(AppSignature.class);
        while (it.hasNext()) {
            AppSignature appSignature = (AppSignature) it.next();
            if (!map.containsKey(appSignature)) {
                if (appSignature instanceof RealmObjectProxy) {
                    RealmObjectProxy realmObjectProxy = (RealmObjectProxy) appSignature;
                    if (realmObjectProxy.realmGet$proxyState().getRealm$realm() != null && realmObjectProxy.realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                        map.put(appSignature, Long.valueOf(realmObjectProxy.realmGet$proxyState().getRow$realm().getIndex()));
                    }
                }
                long createRow = OsObject.createRow(table);
                map.put(appSignature, Long.valueOf(createRow));
                String realmGet$signature = appSignature.realmGet$signature();
                if (realmGet$signature != null) {
                    Table.nativeSetString(nativePtr, appSignatureColumnInfo.signatureIndex, createRow, realmGet$signature, false);
                }
            }
        }
    }

    public static long insertOrUpdate(Realm realm, AppSignature appSignature, Map<RealmModel, Long> map) {
        if (appSignature instanceof RealmObjectProxy) {
            RealmObjectProxy realmObjectProxy = (RealmObjectProxy) appSignature;
            if (realmObjectProxy.realmGet$proxyState().getRealm$realm() != null && realmObjectProxy.realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                return realmObjectProxy.realmGet$proxyState().getRow$realm().getIndex();
            }
        }
        Table table = realm.getTable(AppSignature.class);
        long nativePtr = table.getNativePtr();
        AppSignatureColumnInfo appSignatureColumnInfo = (AppSignatureColumnInfo) realm.getSchema().getColumnInfo(AppSignature.class);
        long createRow = OsObject.createRow(table);
        map.put(appSignature, Long.valueOf(createRow));
        String realmGet$signature = appSignature.realmGet$signature();
        if (realmGet$signature != null) {
            Table.nativeSetString(nativePtr, appSignatureColumnInfo.signatureIndex, createRow, realmGet$signature, false);
        } else {
            Table.nativeSetNull(nativePtr, appSignatureColumnInfo.signatureIndex, createRow, false);
        }
        return createRow;
    }

    public static void insertOrUpdate(Realm realm, Iterator<? extends RealmModel> it, Map<RealmModel, Long> map) {
        Table table = realm.getTable(AppSignature.class);
        long nativePtr = table.getNativePtr();
        AppSignatureColumnInfo appSignatureColumnInfo = (AppSignatureColumnInfo) realm.getSchema().getColumnInfo(AppSignature.class);
        while (it.hasNext()) {
            AppSignature appSignature = (AppSignature) it.next();
            if (!map.containsKey(appSignature)) {
                if (appSignature instanceof RealmObjectProxy) {
                    RealmObjectProxy realmObjectProxy = (RealmObjectProxy) appSignature;
                    if (realmObjectProxy.realmGet$proxyState().getRealm$realm() != null && realmObjectProxy.realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                        map.put(appSignature, Long.valueOf(realmObjectProxy.realmGet$proxyState().getRow$realm().getIndex()));
                    }
                }
                long createRow = OsObject.createRow(table);
                map.put(appSignature, Long.valueOf(createRow));
                String realmGet$signature = appSignature.realmGet$signature();
                if (realmGet$signature != null) {
                    Table.nativeSetString(nativePtr, appSignatureColumnInfo.signatureIndex, createRow, realmGet$signature, false);
                } else {
                    Table.nativeSetNull(nativePtr, appSignatureColumnInfo.signatureIndex, createRow, false);
                }
            }
        }
    }

    public static AppSignature createDetachedCopy(AppSignature appSignature, int i, int i2, Map<RealmModel, CacheData<RealmModel>> map) {
        AppSignature appSignature2;
        if (i > i2 || appSignature == null) {
            return null;
        }
        CacheData cacheData = (CacheData) map.get(appSignature);
        if (cacheData == null) {
            appSignature2 = new AppSignature();
            map.put(appSignature, new CacheData(i, appSignature2));
        } else if (i >= cacheData.minDepth) {
            return (AppSignature) cacheData.object;
        } else {
            AppSignature appSignature3 = (AppSignature) cacheData.object;
            cacheData.minDepth = i;
            appSignature2 = appSignature3;
        }
        appSignature2.realmSet$signature(appSignature.realmGet$signature());
        return appSignature2;
    }

    public String toString() {
        if (!RealmObject.isValid(this)) {
            return "Invalid object";
        }
        StringBuilder sb = new StringBuilder("AppSignature = proxy[");
        sb.append("{signature:");
        sb.append(realmGet$signature() != null ? realmGet$signature() : "null");
        sb.append("}");
        sb.append(RequestParameters.RIGHT_BRACKETS);
        return sb.toString();
    }

    public ProxyState<?> realmGet$proxyState() {
        return this.proxyState;
    }

    public int hashCode() {
        String path = this.proxyState.getRealm$realm().getPath();
        String name = this.proxyState.getRow$realm().getTable().getName();
        long index = this.proxyState.getRow$realm().getIndex();
        int i = 0;
        int hashCode = (IronSourceError.ERROR_NON_EXISTENT_INSTANCE + (path != null ? path.hashCode() : 0)) * 31;
        if (name != null) {
            i = name.hashCode();
        }
        return ((hashCode + i) * 31) + ((int) ((index >>> 32) ^ index));
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        AppSignatureRealmProxy appSignatureRealmProxy = (AppSignatureRealmProxy) obj;
        String path = this.proxyState.getRealm$realm().getPath();
        String path2 = appSignatureRealmProxy.proxyState.getRealm$realm().getPath();
        if (path == null ? path2 != null : !path.equals(path2)) {
            return false;
        }
        String name = this.proxyState.getRow$realm().getTable().getName();
        String name2 = appSignatureRealmProxy.proxyState.getRow$realm().getTable().getName();
        if (name == null ? name2 == null : name.equals(name2)) {
            return this.proxyState.getRow$realm().getIndex() == appSignatureRealmProxy.proxyState.getRow$realm().getIndex();
        }
        return false;
    }
}
