package io.realm;

import android.annotation.TargetApi;
import android.util.JsonReader;
import android.util.JsonToken;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.mansoon.BatteryDouble.models.data.BatterySession;
import io.realm.BaseRealm.RealmObjectContext;
import io.realm.exceptions.RealmException;
import io.realm.internal.ColumnInfo;
import io.realm.internal.OsObject;
import io.realm.internal.OsObjectSchemaInfo;
import io.realm.internal.OsObjectSchemaInfo.Builder;
import io.realm.internal.OsSchemaInfo;
import io.realm.internal.RealmObjectProxy;
import io.realm.internal.RealmObjectProxy.CacheData;
import io.realm.internal.Row;
import io.realm.internal.Table;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class BatterySessionRealmProxy extends BatterySession implements RealmObjectProxy, BatterySessionRealmProxyInterface {
    private static final List<String> FIELD_NAMES;
    private static final OsObjectSchemaInfo expectedObjectSchemaInfo = createExpectedObjectSchemaInfo();
    private BatterySessionColumnInfo columnInfo;
    private ProxyState<BatterySession> proxyState;

    static final class BatterySessionColumnInfo extends ColumnInfo {
        long idIndex;
        long levelIndex;
        long screenOnIndex;
        long timestampIndex;
        long triggeredByIndex;

        BatterySessionColumnInfo(OsSchemaInfo osSchemaInfo) {
            super(5);
            OsObjectSchemaInfo objectSchemaInfo = osSchemaInfo.getObjectSchemaInfo("BatterySession");
            this.idIndex = addColumnDetails("id", objectSchemaInfo);
            this.timestampIndex = addColumnDetails("timestamp", objectSchemaInfo);
            this.levelIndex = addColumnDetails("level", objectSchemaInfo);
            this.screenOnIndex = addColumnDetails("screenOn", objectSchemaInfo);
            this.triggeredByIndex = addColumnDetails("triggeredBy", objectSchemaInfo);
        }

        BatterySessionColumnInfo(ColumnInfo columnInfo, boolean z) {
            super(columnInfo, z);
            copy(columnInfo, this);
        }

        /* access modifiers changed from: protected */
        public final ColumnInfo copy(boolean z) {
            return new BatterySessionColumnInfo(this, z);
        }

        /* access modifiers changed from: protected */
        public final void copy(ColumnInfo columnInfo, ColumnInfo columnInfo2) {
            BatterySessionColumnInfo batterySessionColumnInfo = (BatterySessionColumnInfo) columnInfo;
            BatterySessionColumnInfo batterySessionColumnInfo2 = (BatterySessionColumnInfo) columnInfo2;
            batterySessionColumnInfo2.idIndex = batterySessionColumnInfo.idIndex;
            batterySessionColumnInfo2.timestampIndex = batterySessionColumnInfo.timestampIndex;
            batterySessionColumnInfo2.levelIndex = batterySessionColumnInfo.levelIndex;
            batterySessionColumnInfo2.screenOnIndex = batterySessionColumnInfo.screenOnIndex;
            batterySessionColumnInfo2.triggeredByIndex = batterySessionColumnInfo.triggeredByIndex;
        }
    }

    public static String getTableName() {
        return "class_BatterySession";
    }

    static {
        ArrayList arrayList = new ArrayList(5);
        arrayList.add("id");
        arrayList.add("timestamp");
        arrayList.add("level");
        arrayList.add("screenOn");
        arrayList.add("triggeredBy");
        FIELD_NAMES = Collections.unmodifiableList(arrayList);
    }

    BatterySessionRealmProxy() {
        this.proxyState.setConstructionFinished();
    }

    public void realm$injectObjectContext() {
        if (this.proxyState == null) {
            RealmObjectContext realmObjectContext = (RealmObjectContext) BaseRealm.objectContext.get();
            this.columnInfo = (BatterySessionColumnInfo) realmObjectContext.getColumnInfo();
            this.proxyState = new ProxyState<>(this);
            this.proxyState.setRealm$realm(realmObjectContext.getRealm());
            this.proxyState.setRow$realm(realmObjectContext.getRow());
            this.proxyState.setAcceptDefaultValue$realm(realmObjectContext.getAcceptDefaultValue());
            this.proxyState.setExcludeFields$realm(realmObjectContext.getExcludeFields());
        }
    }

    public int realmGet$id() {
        this.proxyState.getRealm$realm().checkIfValid();
        return (int) this.proxyState.getRow$realm().getLong(this.columnInfo.idIndex);
    }

    public void realmSet$id(int i) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            throw new RealmException("Primary key field 'id' cannot be changed after object was created.");
        }
    }

    public long realmGet$timestamp() {
        this.proxyState.getRealm$realm().checkIfValid();
        return this.proxyState.getRow$realm().getLong(this.columnInfo.timestampIndex);
    }

    public void realmSet$timestamp(long j) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            this.proxyState.getRow$realm().setLong(this.columnInfo.timestampIndex, j);
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            row$realm.getTable().setLong(this.columnInfo.timestampIndex, row$realm.getIndex(), j, true);
        }
    }

    public float realmGet$level() {
        this.proxyState.getRealm$realm().checkIfValid();
        return this.proxyState.getRow$realm().getFloat(this.columnInfo.levelIndex);
    }

    public void realmSet$level(float f) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            this.proxyState.getRow$realm().setFloat(this.columnInfo.levelIndex, f);
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            row$realm.getTable().setFloat(this.columnInfo.levelIndex, row$realm.getIndex(), f, true);
        }
    }

    public int realmGet$screenOn() {
        this.proxyState.getRealm$realm().checkIfValid();
        return (int) this.proxyState.getRow$realm().getLong(this.columnInfo.screenOnIndex);
    }

    public void realmSet$screenOn(int i) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            this.proxyState.getRow$realm().setLong(this.columnInfo.screenOnIndex, (long) i);
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            row$realm.getTable().setLong(this.columnInfo.screenOnIndex, row$realm.getIndex(), (long) i, true);
        }
    }

    public String realmGet$triggeredBy() {
        this.proxyState.getRealm$realm().checkIfValid();
        return this.proxyState.getRow$realm().getString(this.columnInfo.triggeredByIndex);
    }

    public void realmSet$triggeredBy(String str) {
        if (!this.proxyState.isUnderConstruction()) {
            this.proxyState.getRealm$realm().checkIfValid();
            if (str == null) {
                this.proxyState.getRow$realm().setNull(this.columnInfo.triggeredByIndex);
            } else {
                this.proxyState.getRow$realm().setString(this.columnInfo.triggeredByIndex, str);
            }
        } else if (this.proxyState.getAcceptDefaultValue$realm()) {
            Row row$realm = this.proxyState.getRow$realm();
            if (str == null) {
                row$realm.getTable().setNull(this.columnInfo.triggeredByIndex, row$realm.getIndex(), true);
            } else {
                row$realm.getTable().setString(this.columnInfo.triggeredByIndex, row$realm.getIndex(), str, true);
            }
        }
    }

    private static OsObjectSchemaInfo createExpectedObjectSchemaInfo() {
        Builder builder = new Builder("BatterySession", 5, 0);
        Builder builder2 = builder;
        builder2.addPersistedProperty("id", RealmFieldType.INTEGER, true, true, true);
        builder2.addPersistedProperty("timestamp", RealmFieldType.INTEGER, false, true, true);
        builder2.addPersistedProperty("level", RealmFieldType.FLOAT, false, false, true);
        builder2.addPersistedProperty("screenOn", RealmFieldType.INTEGER, false, false, true);
        builder2.addPersistedProperty("triggeredBy", RealmFieldType.STRING, false, false, false);
        return builder.build();
    }

    public static OsObjectSchemaInfo getExpectedObjectSchemaInfo() {
        return expectedObjectSchemaInfo;
    }

    public static BatterySessionColumnInfo createColumnInfo(OsSchemaInfo osSchemaInfo) {
        return new BatterySessionColumnInfo(osSchemaInfo);
    }

    public static List<String> getFieldNames() {
        return FIELD_NAMES;
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x0066  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00a8  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00ca  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x00ed  */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x010f  */
    public static BatterySession createOrUpdateUsingJsonObject(Realm realm, JSONObject jSONObject, boolean z) throws JSONException {
        BatterySessionRealmProxy batterySessionRealmProxy;
        List emptyList = Collections.emptyList();
        if (z) {
            Table table = realm.getTable(BatterySession.class);
            long findFirstLong = !jSONObject.isNull("id") ? table.findFirstLong(((BatterySessionColumnInfo) realm.getSchema().getColumnInfo(BatterySession.class)).idIndex, jSONObject.getLong("id")) : -1;
            if (findFirstLong != -1) {
                RealmObjectContext realmObjectContext = (RealmObjectContext) BaseRealm.objectContext.get();
                try {
                    realmObjectContext.set(realm, table.getUncheckedRow(findFirstLong), realm.getSchema().getColumnInfo(BatterySession.class), false, Collections.emptyList());
                    batterySessionRealmProxy = new BatterySessionRealmProxy();
                    if (batterySessionRealmProxy == null) {
                        if (!jSONObject.has("id")) {
                            throw new IllegalArgumentException("JSON object doesn't have the primary key field 'id'.");
                        } else if (jSONObject.isNull("id")) {
                            batterySessionRealmProxy = (BatterySessionRealmProxy) realm.createObjectInternal(BatterySession.class, null, true, emptyList);
                        } else {
                            batterySessionRealmProxy = (BatterySessionRealmProxy) realm.createObjectInternal(BatterySession.class, Integer.valueOf(jSONObject.getInt("id")), true, emptyList);
                        }
                    }
                    BatterySessionRealmProxyInterface batterySessionRealmProxyInterface = batterySessionRealmProxy;
                    if (jSONObject.has("timestamp")) {
                        if (!jSONObject.isNull("timestamp")) {
                            batterySessionRealmProxyInterface.realmSet$timestamp(jSONObject.getLong("timestamp"));
                        } else {
                            throw new IllegalArgumentException("Trying to set non-nullable field 'timestamp' to null.");
                        }
                    }
                    if (jSONObject.has("level")) {
                        if (!jSONObject.isNull("level")) {
                            batterySessionRealmProxyInterface.realmSet$level((float) jSONObject.getDouble("level"));
                        } else {
                            throw new IllegalArgumentException("Trying to set non-nullable field 'level' to null.");
                        }
                    }
                    if (jSONObject.has("screenOn")) {
                        if (!jSONObject.isNull("screenOn")) {
                            batterySessionRealmProxyInterface.realmSet$screenOn(jSONObject.getInt("screenOn"));
                        } else {
                            throw new IllegalArgumentException("Trying to set non-nullable field 'screenOn' to null.");
                        }
                    }
                    if (jSONObject.has("triggeredBy")) {
                        if (jSONObject.isNull("triggeredBy")) {
                            batterySessionRealmProxyInterface.realmSet$triggeredBy(null);
                        } else {
                            batterySessionRealmProxyInterface.realmSet$triggeredBy(jSONObject.getString("triggeredBy"));
                        }
                    }
                    return batterySessionRealmProxy;
                } finally {
                    realmObjectContext.clear();
                }
            }
        }
        batterySessionRealmProxy = null;
        if (batterySessionRealmProxy == null) {
        }
        BatterySessionRealmProxyInterface batterySessionRealmProxyInterface2 = batterySessionRealmProxy;
        if (jSONObject.has("timestamp")) {
        }
        if (jSONObject.has("level")) {
        }
        if (jSONObject.has("screenOn")) {
        }
        if (jSONObject.has("triggeredBy")) {
        }
        return batterySessionRealmProxy;
    }

    @TargetApi(11)
    public static BatterySession createUsingJsonStream(Realm realm, JsonReader jsonReader) throws IOException {
        BatterySession batterySession = new BatterySession();
        BatterySessionRealmProxyInterface batterySessionRealmProxyInterface = batterySession;
        jsonReader.beginObject();
        boolean z = false;
        while (jsonReader.hasNext()) {
            String nextName = jsonReader.nextName();
            if (nextName.equals("id")) {
                if (jsonReader.peek() != JsonToken.NULL) {
                    batterySessionRealmProxyInterface.realmSet$id(jsonReader.nextInt());
                    z = true;
                } else {
                    jsonReader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'id' to null.");
                }
            } else if (nextName.equals("timestamp")) {
                if (jsonReader.peek() != JsonToken.NULL) {
                    batterySessionRealmProxyInterface.realmSet$timestamp(jsonReader.nextLong());
                } else {
                    jsonReader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'timestamp' to null.");
                }
            } else if (nextName.equals("level")) {
                if (jsonReader.peek() != JsonToken.NULL) {
                    batterySessionRealmProxyInterface.realmSet$level((float) jsonReader.nextDouble());
                } else {
                    jsonReader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'level' to null.");
                }
            } else if (nextName.equals("screenOn")) {
                if (jsonReader.peek() != JsonToken.NULL) {
                    batterySessionRealmProxyInterface.realmSet$screenOn(jsonReader.nextInt());
                } else {
                    jsonReader.skipValue();
                    throw new IllegalArgumentException("Trying to set non-nullable field 'screenOn' to null.");
                }
            } else if (!nextName.equals("triggeredBy")) {
                jsonReader.skipValue();
            } else if (jsonReader.peek() != JsonToken.NULL) {
                batterySessionRealmProxyInterface.realmSet$triggeredBy(jsonReader.nextString());
            } else {
                jsonReader.skipValue();
                batterySessionRealmProxyInterface.realmSet$triggeredBy(null);
            }
        }
        jsonReader.endObject();
        if (z) {
            return (BatterySession) realm.copyToRealm(batterySession);
        }
        throw new IllegalArgumentException("JSON object doesn't have the primary key field 'id'.");
    }

    /* JADX WARNING: Removed duplicated region for block: B:28:0x00a5  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x00aa  */
    public static BatterySession copyOrUpdate(Realm realm, BatterySession batterySession, boolean z, Map<RealmModel, RealmObjectProxy> map) {
        boolean z2;
        if (batterySession instanceof RealmObjectProxy) {
            RealmObjectProxy realmObjectProxy = (RealmObjectProxy) batterySession;
            if (realmObjectProxy.realmGet$proxyState().getRealm$realm() != null) {
                BaseRealm realm$realm = realmObjectProxy.realmGet$proxyState().getRealm$realm();
                if (realm$realm.threadId != realm.threadId) {
                    throw new IllegalArgumentException("Objects which belong to Realm instances in other threads cannot be copied into this Realm instance.");
                } else if (realm$realm.getPath().equals(realm.getPath())) {
                    return batterySession;
                }
            }
        }
        RealmObjectContext realmObjectContext = (RealmObjectContext) BaseRealm.objectContext.get();
        RealmObjectProxy realmObjectProxy2 = (RealmObjectProxy) map.get(batterySession);
        if (realmObjectProxy2 != null) {
            return (BatterySession) realmObjectProxy2;
        }
        BatterySessionRealmProxy batterySessionRealmProxy = null;
        if (z) {
            Table table = realm.getTable(BatterySession.class);
            long findFirstLong = table.findFirstLong(((BatterySessionColumnInfo) realm.getSchema().getColumnInfo(BatterySession.class)).idIndex, (long) batterySession.realmGet$id());
            if (findFirstLong == -1) {
                z2 = false;
                return !z2 ? update(realm, batterySessionRealmProxy, batterySession, map) : copy(realm, batterySession, z, map);
            }
            try {
                realmObjectContext.set(realm, table.getUncheckedRow(findFirstLong), realm.getSchema().getColumnInfo(BatterySession.class), false, Collections.emptyList());
                batterySessionRealmProxy = new BatterySessionRealmProxy();
                map.put(batterySession, batterySessionRealmProxy);
            } finally {
                realmObjectContext.clear();
            }
        }
        z2 = z;
        return !z2 ? update(realm, batterySessionRealmProxy, batterySession, map) : copy(realm, batterySession, z, map);
    }

    public static BatterySession copy(Realm realm, BatterySession batterySession, boolean z, Map<RealmModel, RealmObjectProxy> map) {
        RealmObjectProxy realmObjectProxy = (RealmObjectProxy) map.get(batterySession);
        if (realmObjectProxy != null) {
            return (BatterySession) realmObjectProxy;
        }
        BatterySessionRealmProxyInterface batterySessionRealmProxyInterface = batterySession;
        BatterySession batterySession2 = (BatterySession) realm.createObjectInternal(BatterySession.class, Integer.valueOf(batterySessionRealmProxyInterface.realmGet$id()), false, Collections.emptyList());
        map.put(batterySession, (RealmObjectProxy) batterySession2);
        BatterySessionRealmProxyInterface batterySessionRealmProxyInterface2 = batterySession2;
        batterySessionRealmProxyInterface2.realmSet$timestamp(batterySessionRealmProxyInterface.realmGet$timestamp());
        batterySessionRealmProxyInterface2.realmSet$level(batterySessionRealmProxyInterface.realmGet$level());
        batterySessionRealmProxyInterface2.realmSet$screenOn(batterySessionRealmProxyInterface.realmGet$screenOn());
        batterySessionRealmProxyInterface2.realmSet$triggeredBy(batterySessionRealmProxyInterface.realmGet$triggeredBy());
        return batterySession2;
    }

    public static long insert(Realm realm, BatterySession batterySession, Map<RealmModel, Long> map) {
        long j;
        long j2;
        BatterySession batterySession2 = batterySession;
        if (batterySession2 instanceof RealmObjectProxy) {
            RealmObjectProxy realmObjectProxy = (RealmObjectProxy) batterySession2;
            if (realmObjectProxy.realmGet$proxyState().getRealm$realm() != null && realmObjectProxy.realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                return realmObjectProxy.realmGet$proxyState().getRow$realm().getIndex();
            }
        }
        Table table = realm.getTable(BatterySession.class);
        long nativePtr = table.getNativePtr();
        BatterySessionColumnInfo batterySessionColumnInfo = (BatterySessionColumnInfo) realm.getSchema().getColumnInfo(BatterySession.class);
        long j3 = batterySessionColumnInfo.idIndex;
        BatterySessionRealmProxyInterface batterySessionRealmProxyInterface = batterySession2;
        Integer valueOf = Integer.valueOf(batterySessionRealmProxyInterface.realmGet$id());
        if (valueOf != null) {
            j = Table.nativeFindFirstInt(nativePtr, j3, (long) batterySessionRealmProxyInterface.realmGet$id());
        } else {
            j = -1;
        }
        if (j == -1) {
            j2 = OsObject.createRowWithPrimaryKey(table, j3, Integer.valueOf(batterySessionRealmProxyInterface.realmGet$id()));
        } else {
            Table.throwDuplicatePrimaryKeyException(valueOf);
            j2 = j;
        }
        map.put(batterySession2, Long.valueOf(j2));
        long j4 = nativePtr;
        long j5 = j2;
        Table.nativeSetLong(j4, batterySessionColumnInfo.timestampIndex, j5, batterySessionRealmProxyInterface.realmGet$timestamp(), false);
        Table.nativeSetFloat(j4, batterySessionColumnInfo.levelIndex, j5, batterySessionRealmProxyInterface.realmGet$level(), false);
        Table.nativeSetLong(j4, batterySessionColumnInfo.screenOnIndex, j5, (long) batterySessionRealmProxyInterface.realmGet$screenOn(), false);
        String realmGet$triggeredBy = batterySessionRealmProxyInterface.realmGet$triggeredBy();
        if (realmGet$triggeredBy != null) {
            Table.nativeSetString(nativePtr, batterySessionColumnInfo.triggeredByIndex, j2, realmGet$triggeredBy, false);
        }
        return j2;
    }

    public static void insert(Realm realm, Iterator<? extends RealmModel> it, Map<RealmModel, Long> map) {
        long j;
        Map<RealmModel, Long> map2 = map;
        Table table = realm.getTable(BatterySession.class);
        long nativePtr = table.getNativePtr();
        BatterySessionColumnInfo batterySessionColumnInfo = (BatterySessionColumnInfo) realm.getSchema().getColumnInfo(BatterySession.class);
        long j2 = batterySessionColumnInfo.idIndex;
        while (it.hasNext()) {
            BatterySession batterySession = (BatterySession) it.next();
            if (!map2.containsKey(batterySession)) {
                if (batterySession instanceof RealmObjectProxy) {
                    RealmObjectProxy realmObjectProxy = (RealmObjectProxy) batterySession;
                    if (realmObjectProxy.realmGet$proxyState().getRealm$realm() != null && realmObjectProxy.realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                        map2.put(batterySession, Long.valueOf(realmObjectProxy.realmGet$proxyState().getRow$realm().getIndex()));
                    }
                }
                BatterySessionRealmProxyInterface batterySessionRealmProxyInterface = batterySession;
                Integer valueOf = Integer.valueOf(batterySessionRealmProxyInterface.realmGet$id());
                if (valueOf != null) {
                    j = Table.nativeFindFirstInt(nativePtr, j2, (long) batterySessionRealmProxyInterface.realmGet$id());
                } else {
                    j = -1;
                }
                if (j == -1) {
                    j = OsObject.createRowWithPrimaryKey(table, j2, Integer.valueOf(batterySessionRealmProxyInterface.realmGet$id()));
                } else {
                    Table.throwDuplicatePrimaryKeyException(valueOf);
                }
                long j3 = j;
                map2.put(batterySession, Long.valueOf(j3));
                long j4 = nativePtr;
                long j5 = j3;
                long j6 = j2;
                Table.nativeSetLong(j4, batterySessionColumnInfo.timestampIndex, j5, batterySessionRealmProxyInterface.realmGet$timestamp(), false);
                Table.nativeSetFloat(j4, batterySessionColumnInfo.levelIndex, j5, batterySessionRealmProxyInterface.realmGet$level(), false);
                Table.nativeSetLong(nativePtr, batterySessionColumnInfo.screenOnIndex, j5, (long) batterySessionRealmProxyInterface.realmGet$screenOn(), false);
                String realmGet$triggeredBy = batterySessionRealmProxyInterface.realmGet$triggeredBy();
                if (realmGet$triggeredBy != null) {
                    Table.nativeSetString(nativePtr, batterySessionColumnInfo.triggeredByIndex, j3, realmGet$triggeredBy, false);
                }
                j2 = j6;
            }
        }
    }

    public static long insertOrUpdate(Realm realm, BatterySession batterySession, Map<RealmModel, Long> map) {
        BatterySession batterySession2 = batterySession;
        if (batterySession2 instanceof RealmObjectProxy) {
            RealmObjectProxy realmObjectProxy = (RealmObjectProxy) batterySession2;
            if (realmObjectProxy.realmGet$proxyState().getRealm$realm() != null && realmObjectProxy.realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                return realmObjectProxy.realmGet$proxyState().getRow$realm().getIndex();
            }
        }
        Table table = realm.getTable(BatterySession.class);
        long nativePtr = table.getNativePtr();
        BatterySessionColumnInfo batterySessionColumnInfo = (BatterySessionColumnInfo) realm.getSchema().getColumnInfo(BatterySession.class);
        long j = batterySessionColumnInfo.idIndex;
        BatterySessionRealmProxyInterface batterySessionRealmProxyInterface = batterySession2;
        long nativeFindFirstInt = Integer.valueOf(batterySessionRealmProxyInterface.realmGet$id()) != null ? Table.nativeFindFirstInt(nativePtr, j, (long) batterySessionRealmProxyInterface.realmGet$id()) : -1;
        long createRowWithPrimaryKey = nativeFindFirstInt == -1 ? OsObject.createRowWithPrimaryKey(table, j, Integer.valueOf(batterySessionRealmProxyInterface.realmGet$id())) : nativeFindFirstInt;
        map.put(batterySession2, Long.valueOf(createRowWithPrimaryKey));
        long j2 = nativePtr;
        long j3 = createRowWithPrimaryKey;
        Table.nativeSetLong(j2, batterySessionColumnInfo.timestampIndex, j3, batterySessionRealmProxyInterface.realmGet$timestamp(), false);
        Table.nativeSetFloat(j2, batterySessionColumnInfo.levelIndex, j3, batterySessionRealmProxyInterface.realmGet$level(), false);
        Table.nativeSetLong(j2, batterySessionColumnInfo.screenOnIndex, j3, (long) batterySessionRealmProxyInterface.realmGet$screenOn(), false);
        String realmGet$triggeredBy = batterySessionRealmProxyInterface.realmGet$triggeredBy();
        if (realmGet$triggeredBy != null) {
            Table.nativeSetString(nativePtr, batterySessionColumnInfo.triggeredByIndex, createRowWithPrimaryKey, realmGet$triggeredBy, false);
        } else {
            Table.nativeSetNull(nativePtr, batterySessionColumnInfo.triggeredByIndex, createRowWithPrimaryKey, false);
        }
        return createRowWithPrimaryKey;
    }

    public static void insertOrUpdate(Realm realm, Iterator<? extends RealmModel> it, Map<RealmModel, Long> map) {
        long j;
        Map<RealmModel, Long> map2 = map;
        Table table = realm.getTable(BatterySession.class);
        long nativePtr = table.getNativePtr();
        BatterySessionColumnInfo batterySessionColumnInfo = (BatterySessionColumnInfo) realm.getSchema().getColumnInfo(BatterySession.class);
        long j2 = batterySessionColumnInfo.idIndex;
        while (it.hasNext()) {
            BatterySession batterySession = (BatterySession) it.next();
            if (!map2.containsKey(batterySession)) {
                if (batterySession instanceof RealmObjectProxy) {
                    RealmObjectProxy realmObjectProxy = (RealmObjectProxy) batterySession;
                    if (realmObjectProxy.realmGet$proxyState().getRealm$realm() != null && realmObjectProxy.realmGet$proxyState().getRealm$realm().getPath().equals(realm.getPath())) {
                        map2.put(batterySession, Long.valueOf(realmObjectProxy.realmGet$proxyState().getRow$realm().getIndex()));
                    }
                }
                BatterySessionRealmProxyInterface batterySessionRealmProxyInterface = batterySession;
                if (Integer.valueOf(batterySessionRealmProxyInterface.realmGet$id()) != null) {
                    j = Table.nativeFindFirstInt(nativePtr, j2, (long) batterySessionRealmProxyInterface.realmGet$id());
                } else {
                    j = -1;
                }
                if (j == -1) {
                    j = OsObject.createRowWithPrimaryKey(table, j2, Integer.valueOf(batterySessionRealmProxyInterface.realmGet$id()));
                }
                long j3 = j;
                map2.put(batterySession, Long.valueOf(j3));
                long j4 = nativePtr;
                long j5 = j3;
                long j6 = j2;
                Table.nativeSetLong(j4, batterySessionColumnInfo.timestampIndex, j5, batterySessionRealmProxyInterface.realmGet$timestamp(), false);
                Table.nativeSetFloat(j4, batterySessionColumnInfo.levelIndex, j5, batterySessionRealmProxyInterface.realmGet$level(), false);
                Table.nativeSetLong(nativePtr, batterySessionColumnInfo.screenOnIndex, j5, (long) batterySessionRealmProxyInterface.realmGet$screenOn(), false);
                String realmGet$triggeredBy = batterySessionRealmProxyInterface.realmGet$triggeredBy();
                if (realmGet$triggeredBy != null) {
                    Table.nativeSetString(nativePtr, batterySessionColumnInfo.triggeredByIndex, j3, realmGet$triggeredBy, false);
                } else {
                    Table.nativeSetNull(nativePtr, batterySessionColumnInfo.triggeredByIndex, j3, false);
                }
                j2 = j6;
            }
        }
    }

    public static BatterySession createDetachedCopy(BatterySession batterySession, int i, int i2, Map<RealmModel, CacheData<RealmModel>> map) {
        BatterySession batterySession2;
        if (i > i2 || batterySession == null) {
            return null;
        }
        CacheData cacheData = (CacheData) map.get(batterySession);
        if (cacheData == null) {
            batterySession2 = new BatterySession();
            map.put(batterySession, new CacheData(i, batterySession2));
        } else if (i >= cacheData.minDepth) {
            return (BatterySession) cacheData.object;
        } else {
            BatterySession batterySession3 = (BatterySession) cacheData.object;
            cacheData.minDepth = i;
            batterySession2 = batterySession3;
        }
        BatterySessionRealmProxyInterface batterySessionRealmProxyInterface = batterySession2;
        BatterySessionRealmProxyInterface batterySessionRealmProxyInterface2 = batterySession;
        batterySessionRealmProxyInterface.realmSet$id(batterySessionRealmProxyInterface2.realmGet$id());
        batterySessionRealmProxyInterface.realmSet$timestamp(batterySessionRealmProxyInterface2.realmGet$timestamp());
        batterySessionRealmProxyInterface.realmSet$level(batterySessionRealmProxyInterface2.realmGet$level());
        batterySessionRealmProxyInterface.realmSet$screenOn(batterySessionRealmProxyInterface2.realmGet$screenOn());
        batterySessionRealmProxyInterface.realmSet$triggeredBy(batterySessionRealmProxyInterface2.realmGet$triggeredBy());
        return batterySession2;
    }

    static BatterySession update(Realm realm, BatterySession batterySession, BatterySession batterySession2, Map<RealmModel, RealmObjectProxy> map) {
        BatterySessionRealmProxyInterface batterySessionRealmProxyInterface = batterySession;
        BatterySessionRealmProxyInterface batterySessionRealmProxyInterface2 = batterySession2;
        batterySessionRealmProxyInterface.realmSet$timestamp(batterySessionRealmProxyInterface2.realmGet$timestamp());
        batterySessionRealmProxyInterface.realmSet$level(batterySessionRealmProxyInterface2.realmGet$level());
        batterySessionRealmProxyInterface.realmSet$screenOn(batterySessionRealmProxyInterface2.realmGet$screenOn());
        batterySessionRealmProxyInterface.realmSet$triggeredBy(batterySessionRealmProxyInterface2.realmGet$triggeredBy());
        return batterySession;
    }

    public String toString() {
        if (!RealmObject.isValid(this)) {
            return "Invalid object";
        }
        StringBuilder sb = new StringBuilder("BatterySession = proxy[");
        sb.append("{id:");
        sb.append(realmGet$id());
        sb.append("}");
        sb.append(",");
        sb.append("{timestamp:");
        sb.append(realmGet$timestamp());
        sb.append("}");
        sb.append(",");
        sb.append("{level:");
        sb.append(realmGet$level());
        sb.append("}");
        sb.append(",");
        sb.append("{screenOn:");
        sb.append(realmGet$screenOn());
        sb.append("}");
        sb.append(",");
        sb.append("{triggeredBy:");
        sb.append(realmGet$triggeredBy() != null ? realmGet$triggeredBy() : "null");
        sb.append("}");
        sb.append(RequestParameters.RIGHT_BRACKETS);
        return sb.toString();
    }

    public ProxyState<?> realmGet$proxyState() {
        return this.proxyState;
    }

    public int hashCode() {
        String path = this.proxyState.getRealm$realm().getPath();
        String name = this.proxyState.getRow$realm().getTable().getName();
        long index = this.proxyState.getRow$realm().getIndex();
        int i = 0;
        int hashCode = (IronSourceError.ERROR_NON_EXISTENT_INSTANCE + (path != null ? path.hashCode() : 0)) * 31;
        if (name != null) {
            i = name.hashCode();
        }
        return ((hashCode + i) * 31) + ((int) ((index >>> 32) ^ index));
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        BatterySessionRealmProxy batterySessionRealmProxy = (BatterySessionRealmProxy) obj;
        String path = this.proxyState.getRealm$realm().getPath();
        String path2 = batterySessionRealmProxy.proxyState.getRealm$realm().getPath();
        if (path == null ? path2 != null : !path.equals(path2)) {
            return false;
        }
        String name = this.proxyState.getRow$realm().getTable().getName();
        String name2 = batterySessionRealmProxy.proxyState.getRow$realm().getTable().getName();
        if (name == null ? name2 == null : name.equals(name2)) {
            return this.proxyState.getRow$realm().getIndex() == batterySessionRealmProxy.proxyState.getRow$realm().getIndex();
        }
        return false;
    }
}
