package io.presage;

import android.content.Context;

public final class t {

    /* renamed from: a reason: collision with root package name */
    public static final CamembertauCalvados f6183a = new CamembertauCalvados(0);
    private final u b;
    private final u c;
    private final u d;
    private final u e;

    public static final class CamembertauCalvados {
        private CamembertauCalvados() {
        }

        public /* synthetic */ CamembertauCalvados(byte b) {
            this();
        }

        public static t a(Context context) {
            Cheddar cheddar = new Cheddar(context);
            w wVar = new w();
            u vVar = new v(cheddar);
            t tVar = new t(wVar, new s(cheddar, vVar), new C0138r(), vVar, 0);
            return tVar;
        }
    }

    private t(u uVar, u uVar2, u uVar3, u uVar4) {
        this.b = uVar;
        this.c = uVar2;
        this.d = uVar3;
        this.e = uVar4;
    }

    public /* synthetic */ t(u uVar, u uVar2, u uVar3, u uVar4, byte b2) {
        this(uVar, uVar2, uVar3, uVar4);
    }

    public final void a(x xVar) {
        this.b.a(new RacletteSuisse(xVar));
    }

    public final void b(x xVar) {
        this.c.a(new RacletteSuisse(xVar));
    }

    public final void c(x xVar) {
        this.d.a(new RacletteSuisse(xVar));
    }

    public final void d(x xVar) {
        this.e.a(new RacletteSuisse(xVar));
    }
}
