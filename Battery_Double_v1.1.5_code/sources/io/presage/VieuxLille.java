package io.presage;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.ShortcutManager;

public final class VieuxLille {
    @SuppressLint({"NewApi"})
    public static ShortcutManager a(Context context) {
        Object systemService = context.getSystemService(ShortcutManager.class);
        df.a(systemService, "context.getSystemService…rtcutManager::class.java)");
        return (ShortcutManager) systemService;
    }
}
