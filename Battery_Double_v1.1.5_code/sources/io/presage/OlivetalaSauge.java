package io.presage;

import android.annotation.TargetApi;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public abstract class OlivetalaSauge extends WebViewClient {
    public WebResourceResponse a(WebView webView, String str) {
        return null;
    }

    public boolean b(WebView webView, String str) {
        return true;
    }

    public WebResourceResponse shouldInterceptRequest(WebView webView, String str) {
        return a(webView, str);
    }

    @TargetApi(21)
    public WebResourceResponse shouldInterceptRequest(WebView webView, WebResourceRequest webResourceRequest) {
        String uri = webResourceRequest.getUrl().toString();
        df.a((Object) uri, "request.url.toString()");
        return a(webView, uri);
    }

    @TargetApi(21)
    public boolean shouldOverrideUrlLoading(WebView webView, WebResourceRequest webResourceRequest) {
        String uri = webResourceRequest.getUrl().toString();
        df.a((Object) uri, "request.url.toString()");
        return b(webView, uri);
    }

    public boolean shouldOverrideUrlLoading(WebView webView, String str) {
        return b(webView, str);
    }

    public void onReceivedError(WebView webView, int i, String str, String str2) {
        a(i, str, str2);
    }

    @TargetApi(23)
    public void onReceivedError(WebView webView, WebResourceRequest webResourceRequest, WebResourceError webResourceError) {
        a(webResourceError.getErrorCode(), webResourceError.getDescription().toString(), webResourceRequest.getUrl().toString());
    }

    public void a(int i, String str, String str2) {
        StringBuilder sb = new StringBuilder("onReceivedError: ");
        sb.append(i);
        sb.append(" description: ");
        sb.append(str);
        sb.append(" failingUrl: ");
        sb.append(str2);
    }
}
