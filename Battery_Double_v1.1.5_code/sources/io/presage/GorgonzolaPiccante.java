package io.presage;

import android.annotation.TargetApi;
import android.app.job.JobInfo.Builder;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import io.presage.common.profig.schedule.ProfigJobService;

@TargetApi(21)
public final class GorgonzolaPiccante implements Goudaauxepices {

    /* renamed from: a reason: collision with root package name */
    public static final CamembertauCalvados f6062a = new CamembertauCalvados(0);
    private JobScheduler b;
    private final Context c;

    public static final class CamembertauCalvados {
        private CamembertauCalvados() {
        }

        public /* synthetic */ CamembertauCalvados(byte b) {
            this();
        }
    }

    public GorgonzolaPiccante(Context context) {
        this.c = context;
        Object systemService = this.c.getSystemService("jobscheduler");
        if (systemService != null) {
            this.b = (JobScheduler) systemService;
            return;
        }
        throw new be("null cannot be cast to non-null type android.app.job.JobScheduler");
    }

    public final void a(long j) {
        Builder builder = new Builder(475439765, new ComponentName(this.c, ProfigJobService.class));
        builder.setMinimumLatency(j);
        double d = (double) j;
        Double.isNaN(d);
        builder.setOverrideDeadline((long) (d * 1.2d));
        builder.setRequiredNetworkType(1);
        builder.setRequiresCharging(false);
        builder.setRequiresDeviceIdle(false);
        this.b.schedule(builder.build());
    }

    public final void a() {
        this.b.cancel(475439765);
    }
}
