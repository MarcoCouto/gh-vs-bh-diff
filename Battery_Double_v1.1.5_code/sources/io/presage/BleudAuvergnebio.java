package io.presage;

import android.content.Context;
import com.appodeal.ads.at;
import org.json.JSONArray;
import org.json.JSONObject;

public final class BleudAuvergnebio {

    /* renamed from: a reason: collision with root package name */
    private final Cheddar f6013a;
    private final CoeurdArras b;

    private BleudAuvergnebio(Cheddar cheddar, CoeurdArras coeurdArras) {
        this.f6013a = cheddar;
        this.b = coeurdArras;
    }

    public /* synthetic */ BleudAuvergnebio(Context context) {
        this(new Cheddar(context), new CoeurdArras(context));
    }

    public final JSONObject a() {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("connectivity", this.f6013a.i());
        jSONObject.put(at.f1547a, this.f6013a.f());
        jSONObject.put("build", 30051);
        jSONObject.put("version", "3.2.2-moat");
        JSONArray jSONArray = new JSONArray();
        jSONArray.put(this.b.a());
        jSONObject.put("apps_publishers", jSONArray);
        return jSONObject;
    }
}
