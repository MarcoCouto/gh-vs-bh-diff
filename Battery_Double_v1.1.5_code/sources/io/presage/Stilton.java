package io.presage;

import com.tapjoy.TJAdUnitConstants.String;
import org.json.JSONObject;

public final class Stilton implements SaintNectaire {

    /* renamed from: a reason: collision with root package name */
    public static final CamembertauCalvados f6099a = new CamembertauCalvados(0);
    private final String[] b = {"ogyCreateWebView", "ogyUpdateWebView", "ogyCloseWebView", "ogyNavigateBack", "ogyNavigateForward"};
    private final StRomans c;
    private final StVincentauChablis d;

    public static final class CamembertauCalvados {
        private CamembertauCalvados() {
        }

        public /* synthetic */ CamembertauCalvados(byte b) {
            this();
        }
    }

    public Stilton(StRomans stRomans, StVincentauChablis stVincentauChablis) {
        this.c = stRomans;
        this.d = stVincentauChablis;
    }

    public final boolean a(String str, x xVar, CarreMirabelle carreMirabelle) {
        String lowerCase = str.toLowerCase();
        df.a((Object) lowerCase, "(this as java.lang.String).toLowerCase()");
        if (!en.b(lowerCase, "http://ogymraid")) {
            return false;
        }
        String substring = str.substring(19);
        df.a((Object) substring, "(this as java.lang.String).substring(startIndex)");
        JSONObject jSONObject = new JSONObject(MunsterauCumin.a(substring));
        String optString = jSONObject.optString("method", "");
        String optString2 = jSONObject.optString(String.CALLBACK_ID, "");
        JSONObject optJSONObject = jSONObject.optJSONObject("args");
        if (optJSONObject == null) {
            optJSONObject = new JSONObject();
        }
        df.a((Object) optString, "command");
        df.a((Object) optString2, String.CALLBACK_ID);
        a(optString, optJSONObject, optString2, xVar);
        return bi.a(this.b, optString);
    }

    private final void a(String str, JSONObject jSONObject, String str2, x xVar) {
        switch (str.hashCode()) {
            case -1797727422:
                if (str.equals("ogyCloseWebView")) {
                    c(jSONObject, str2, xVar);
                    return;
                }
                break;
            case -1244773540:
                if (str.equals("ogyCreateWebView")) {
                    a(jSONObject, str2, xVar);
                    return;
                }
                break;
            case -692274449:
                if (str.equals("ogyUpdateWebView")) {
                    b(jSONObject, str2, xVar);
                    return;
                }
                break;
            case 960350259:
                if (str.equals("ogyNavigateForward")) {
                    b(jSONObject);
                    break;
                }
                break;
            case 1635219001:
                if (str.equals("ogyNavigateBack")) {
                    a(jSONObject);
                    return;
                }
                break;
        }
    }

    private final void a(JSONObject jSONObject, String str, x xVar) {
        TommedeSavoie tommedeSavoie = TommedeSavoie.f6105a;
        TommedAuvergne a2 = TommedeSavoie.a(jSONObject);
        this.c.a(a2);
        StVincentauChablis.a(xVar, str, a2.c());
    }

    private final void b(JSONObject jSONObject, String str, x xVar) {
        TommedeSavoie tommedeSavoie = TommedeSavoie.f6105a;
        TommedAuvergne a2 = TommedeSavoie.a(jSONObject);
        this.c.b(a2);
        StVincentauChablis.a(xVar, str, a2.c());
    }

    private final void c(JSONObject jSONObject, String str, x xVar) {
        String optString = jSONObject.optString("webViewId", "");
        StRomans stRomans = this.c;
        df.a((Object) optString, "webViewId");
        stRomans.a(optString);
        StVincentauChablis.a(xVar, str, optString);
    }

    private final void a(JSONObject jSONObject) {
        String optString = jSONObject.optString("webViewId", "");
        StRomans stRomans = this.c;
        df.a((Object) optString, "webViewId");
        stRomans.b(optString);
    }

    private final void b(JSONObject jSONObject) {
        String optString = jSONObject.optString("webViewId", "");
        StRomans stRomans = this.c;
        df.a((Object) optString, "webViewId");
        stRomans.c(optString);
    }
}
