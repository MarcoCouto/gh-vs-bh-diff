package io.presage;

import java.io.Serializable;

public final class BriedeMelun implements Serializable {

    /* renamed from: a reason: collision with root package name */
    private String f6026a = "";
    private String b = "";
    private String c = "";
    private String d = "";
    private String e = "";
    private String f = "";
    private String g = "";
    private String h = "";
    private String i = "";
    private String j = "";
    private String k = "";
    private String l = "";
    private CarreMirabelle m = new CarreMirabelle();
    private boolean n;
    private boolean o;
    private boolean p;
    private boolean q;
    private String r = "";
    private String s = "";
    private boolean t;
    private String u = "";

    public final String a() {
        return this.f6026a;
    }

    public final void a(String str) {
        this.f6026a = str;
    }

    public final String b() {
        return this.b;
    }

    public final void b(String str) {
        this.b = str;
    }

    public final String c() {
        return this.c;
    }

    public final void c(String str) {
        this.c = str;
    }

    public final String d() {
        return this.e;
    }

    public final void d(String str) {
        this.e = str;
    }

    public final String e() {
        return this.f;
    }

    public final void e(String str) {
        this.f = str;
    }

    public final String f() {
        return this.g;
    }

    public final void f(String str) {
        this.g = str;
    }

    public final String g() {
        return this.h;
    }

    public final void g(String str) {
        this.h = str;
    }

    public final String h() {
        return this.i;
    }

    public final void h(String str) {
        this.i = str;
    }

    public final String i() {
        return this.j;
    }

    public final void i(String str) {
        this.j = str;
    }

    public final String j() {
        return this.k;
    }

    public final void j(String str) {
        this.k = str;
    }

    public final String k() {
        return this.l;
    }

    public final void k(String str) {
        this.l = str;
    }

    public final void a(CarreMirabelle carreMirabelle) {
        this.m = carreMirabelle;
    }

    public final CarreMirabelle l() {
        return this.m;
    }

    public final void a(boolean z) {
        this.n = z;
    }

    public final boolean m() {
        return this.n;
    }

    public final void b(boolean z) {
        this.o = z;
    }

    public final boolean n() {
        return this.o;
    }

    public final void c(boolean z) {
        this.p = z;
    }

    public final boolean o() {
        return this.p;
    }

    public final void d(boolean z) {
        this.q = z;
    }

    public final boolean p() {
        return this.q;
    }

    public final void l(String str) {
        this.r = str;
    }

    public final String q() {
        return this.r;
    }

    public final void m(String str) {
        this.s = str;
    }

    public final String r() {
        return this.s;
    }

    public final void e(boolean z) {
        this.t = z;
    }

    public final boolean s() {
        return this.t;
    }

    public final void n(String str) {
        this.u = str;
    }

    public final String t() {
        return this.u;
    }
}
