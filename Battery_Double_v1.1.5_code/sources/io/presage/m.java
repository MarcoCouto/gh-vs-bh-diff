package io.presage;

import com.tapjoy.TJAdUnitConstants.String;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import org.json.JSONObject;

public abstract class m implements SaintNectaire {

    /* renamed from: a reason: collision with root package name */
    public static final CamembertauCalvados f6173a = new CamembertauCalvados(0);
    private final RacletteSuisse b;

    public static final class CamembertauCalvados {
        private CamembertauCalvados() {
        }

        public /* synthetic */ CamembertauCalvados(byte b) {
            this();
        }
    }

    public void a() {
    }

    public void a(CarreMirabelle carreMirabelle) {
    }

    public void a(String str) {
    }

    public void a(String str, String str2) {
    }

    public void a(Map<String, String> map, String str) {
    }

    public void a(boolean z) {
    }

    public void b(String str) {
    }

    public void b(String str, String str2) {
    }

    public m(RacletteSuisse racletteSuisse) {
        this.b = racletteSuisse;
    }

    public final boolean a(String str, x xVar, CarreMirabelle carreMirabelle) {
        if (!Salers.a(str)) {
            return false;
        }
        MunsterauCumin.a(str);
        a(str, carreMirabelle);
        return true;
    }

    private final void a(String str, CarreMirabelle carreMirabelle) {
        String str2;
        Map linkedHashMap = new LinkedHashMap();
        String str3 = "";
        try {
            JSONObject c = c(str);
            str2 = c.optString("method", "");
            df.a((Object) str2, "json.optString(\"method\", \"\")");
            String str4 = String.CALLBACK_ID;
            try {
                String optString = c.optString(String.CALLBACK_ID);
                df.a((Object) optString, "json.optString(\"callbackId\")");
                linkedHashMap.put(str4, optString);
                Object opt = c.opt("args");
                a(opt, linkedHashMap);
                a(str2, linkedHashMap, opt.toString(), carreMirabelle);
            } catch (Exception e) {
                e = e;
                BurratadesPouilles burratadesPouilles = BurratadesPouilles.f6029a;
                BurratadesPouilles.a(e);
                this.b.a(str2, "");
            }
        } catch (Exception e2) {
            e = e2;
            str2 = str3;
            BurratadesPouilles burratadesPouilles2 = BurratadesPouilles.f6029a;
            BurratadesPouilles.a(e);
            this.b.a(str2, "");
        }
    }

    private static void a(Object obj, Map<String, String> map) {
        if (obj != null && (obj instanceof JSONObject)) {
            JSONObject jSONObject = (JSONObject) obj;
            Iterator keys = jSONObject.keys();
            df.a((Object) keys, "keys");
            while (keys.hasNext()) {
                String str = (String) keys.next();
                df.a((Object) str, "key");
                String optString = jSONObject.optString(str, "");
                df.a((Object) optString, "args.optString(key, \"\")");
                map.put(str, optString);
            }
        }
    }

    private static JSONObject c(String str) {
        int b2 = en.a((CharSequence) str, "/?q=") + 4;
        if (str != null) {
            String substring = str.substring(b2);
            df.a((Object) substring, "(this as java.lang.String).substring(startIndex)");
            return new JSONObject(MunsterauCumin.a(substring));
        }
        throw new be("null cannot be cast to non-null type java.lang.String");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0053, code lost:
        if (r2.equals("close") != false) goto L_0x0072;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0070, code lost:
        if (r2.equals("unload") != false) goto L_0x0072;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0072, code lost:
        a(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0075, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0096, code lost:
        return;
     */
    private final void a(String str, Map<String, String> map, String str2, CarreMirabelle carreMirabelle) {
        switch (str.hashCode()) {
            case -984419449:
                if (str.equals("ogyResolveIntent")) {
                    String str3 = (String) map.get("intentUri");
                    String str4 = (String) map.get(String.CALLBACK_ID);
                    if (str4 == null) {
                        str4 = "";
                    }
                    a(str3, str4);
                    return;
                }
                break;
            case -840442113:
                break;
            case 3417674:
                if (str.equals("open")) {
                    a((String) map.get("url"));
                    return;
                }
                break;
            case 94756344:
                break;
            case 451326307:
                if (str.equals("ogyCreateShortcut")) {
                    a(map, str2);
                    return;
                }
                break;
            case 901631159:
                if (str.equals("ogyOnAdEvent")) {
                    a(carreMirabelle);
                    break;
                }
                break;
            case 1614272768:
                if (str.equals("useCustomClose")) {
                    b(map);
                    return;
                }
                break;
            case 1805873469:
                if (str.equals("ogyStartIntent")) {
                    String str5 = (String) map.get("intentUri");
                    String str6 = (String) map.get(String.CALLBACK_ID);
                    if (str6 == null) {
                        str6 = "";
                    }
                    b(str5, str6);
                    return;
                }
                break;
        }
    }

    private final void a(Map<String, String> map) {
        String str = (String) map.get("showNextAd");
        if (str == null) {
            str = "true";
        }
        String str2 = (String) map.get("nextAdId");
        if (str2 == null) {
            str2 = "";
        }
        if (df.a((Object) str, (Object) "true")) {
            b(str2);
        }
        a();
    }

    private final void b(Map<String, String> map) {
        String str = (String) map.get("useCustomClose");
        a(str != null ? str.equals("false") : false);
    }
}
