package io.presage;

public final class RegaldeBourgogne {

    /* renamed from: a reason: collision with root package name */
    public static final CamembertauCalvados f6087a = new CamembertauCalvados(0);

    public static final class CamembertauCalvados {
        private CamembertauCalvados() {
        }

        public /* synthetic */ CamembertauCalvados(byte b) {
            this();
        }
    }

    private static String a() {
        return "window.MRAID_ENV =  { version: '3.0', sdk: 'Presage', sdkVersion: '3.2.2-moat'};";
    }

    private static String b(CarreMirabelle carreMirabelle) {
        StringBuilder sb = new StringBuilder("window.MRAID_ENV =  { version: '3.0', sdk: 'Presage', sdkVersion: '3.2.2-moat', adUnit: { type: '");
        sb.append(carreMirabelle.c());
        sb.append("', reward : { name: '");
        sb.append(carreMirabelle.e().getName());
        sb.append("', value: '");
        sb.append(carreMirabelle.e().getValue());
        sb.append("', launch: '");
        sb.append(carreMirabelle.d());
        sb.append("'}}};");
        return sb.toString();
    }

    public static String a(CarreMirabelle carreMirabelle) {
        if (carreMirabelle == null || !df.a((Object) carreMirabelle.c(), (Object) "optin_video")) {
            return a();
        }
        return b(carreMirabelle);
    }
}
