package io.presage;

import android.content.res.Resources;

public final class Montbriac {
    private static float c(int i) {
        float f = (float) i;
        Resources system = Resources.getSystem();
        df.a((Object) system, "Resources.getSystem()");
        return f / system.getDisplayMetrics().density;
    }

    public static final int a(int i) {
        return ds.a(c(i));
    }

    public static final int b(int i) {
        float f = (float) i;
        Resources system = Resources.getSystem();
        df.a((Object) system, "Resources.getSystem()");
        return ds.a(f * system.getDisplayMetrics().density);
    }
}
