package io.presage;

import android.widget.FrameLayout;

public final class PavedAremberg {

    /* renamed from: a reason: collision with root package name */
    public static final PavedAremberg f6079a = new PavedAremberg();

    private PavedAremberg() {
    }

    public static PasdelEscalette a(PersilledumontBlanc persilledumontBlanc, FrameLayout frameLayout, Bofavre bofavre, String str) {
        return new PasdelEscalette(persilledumontBlanc, frameLayout, bofavre, str);
    }
}
