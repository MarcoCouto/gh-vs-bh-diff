package io.presage;

import org.json.JSONObject;

public final class CureNantais {

    /* renamed from: a reason: collision with root package name */
    public static final CamembertauCalvados f6044a = new CamembertauCalvados(0);
    private JSONObject b;
    private String c;
    private final String d = this.g.d();
    private final FourmedeHauteLoire e;
    private final EpoissesdeBourgogne f;
    private final FourmedAmbert g;
    private final boolean h;

    public static final class CamembertauCalvados {
        private CamembertauCalvados() {
        }

        public /* synthetic */ CamembertauCalvados(byte b) {
            this();
        }
    }

    public CureNantais(Coulommiers coulommiers, EpoissesdeBourgogne epoissesdeBourgogne, FourmedAmbert fourmedAmbert, boolean z) {
        this.f = epoissesdeBourgogne;
        this.g = fourmedAmbert;
        this.h = z;
        this.b = coulommiers.a(this.f);
        CoeurdeNeufchatel coeurdeNeufchatel = CoeurdeNeufchatel.f6040a;
        String jSONObject = this.b.toString();
        df.a((Object) jSONObject, "generatedProfig.toString()");
        this.c = CoeurdeNeufchatel.a(jSONObject);
        FourmedeRochefort fourmedeRochefort = FourmedeRochefort.f6061a;
        this.e = FourmedeRochefort.a(this.d);
    }

    public final FourmedeHauteLoire a() {
        return this.e;
    }

    private final boolean c() {
        return this.e != null ? this.g.a() >= this.e.f() : this.g.a() >= 10;
    }

    private final boolean d() {
        FourmedeHauteLoire fourmedeHauteLoire = this.e;
        if (fourmedeHauteLoire != null) {
            return fourmedeHauteLoire.a();
        }
        return true;
    }

    private final boolean e() {
        return df.a((Object) this.g.g(), (Object) Murol.a());
    }

    private final boolean f() {
        FourmedeHauteLoire fourmedeHauteLoire = this.e;
        return this.g.h() + (fourmedeHauteLoire != null ? fourmedeHauteLoire.g() : 0) > System.currentTimeMillis();
    }

    public final CremeuxduJura b() {
        FourmedeHauteLoire fourmedeHauteLoire = this.e;
        long g2 = fourmedeHauteLoire != null ? fourmedeHauteLoire.g() : 43200000;
        boolean d2 = d();
        boolean z = !d2;
        boolean c2 = c();
        boolean z2 = true;
        boolean z3 = !f();
        boolean z4 = !e();
        boolean z5 = z && z4;
        if (!d2 || c2 || !z3 || (!this.h && !g() && !z4 && !z3)) {
            z2 = false;
        }
        if (j()) {
            CremeuxduJura cremeuxduJura = new CremeuxduJura(true, g2, this.b, d2, this.c);
            return cremeuxduJura;
        } else if (!z2 && !z5 && !h()) {
            return new CremeuxduJura(g2, new JSONObject(), d2);
        } else {
            String str = null;
            if (g()) {
                str = this.c;
            }
            CremeuxduJura cremeuxduJura2 = new CremeuxduJura(true, g2, i(), d2, str);
            return cremeuxduJura2;
        }
    }

    private final boolean g() {
        return !df.a((Object) this.g.b(), (Object) this.c);
    }

    private final boolean h() {
        return !df.a((Object) this.g.c(), (Object) this.f.a());
    }

    private final JSONObject i() {
        if (this.h || g()) {
            return this.b;
        }
        return new JSONObject();
    }

    private final boolean j() {
        return (this.d.length() == 0) || df.a((Object) this.d, (Object) "{}");
    }
}
