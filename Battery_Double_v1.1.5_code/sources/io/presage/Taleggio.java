package io.presage;

import android.widget.FrameLayout;
import io.presage.StRomans.CamembertauCalvados;
import io.presage.mraid.browser.ShortcutActivity;

public final class Taleggio {

    /* renamed from: a reason: collision with root package name */
    private StRomans f6100a;
    private final TommeCrayeuse b;
    private final ShortcutActivity c;
    private final TommedeSavoie d;
    private final CamembertauCalvados e;

    private Taleggio(TommeCrayeuse tommeCrayeuse, ShortcutActivity shortcutActivity, TommedeSavoie tommedeSavoie, CamembertauCalvados camembertauCalvados) {
        this.b = tommeCrayeuse;
        this.c = shortcutActivity;
        this.d = tommedeSavoie;
        this.e = camembertauCalvados;
    }

    public /* synthetic */ Taleggio(TommeCrayeuse tommeCrayeuse, ShortcutActivity shortcutActivity) {
        this(tommeCrayeuse, shortcutActivity, TommedeSavoie.f6105a, StRomans.f6096a);
    }

    public final boolean a(String str, String str2, FrameLayout frameLayout) {
        String b2 = this.b.b(str2);
        if (b2.length() > 0) {
            str = b2;
        }
        if (str.length() == 0) {
            return false;
        }
        TommedAuvergne a2 = TommedeSavoie.a(str);
        if (a2 == null) {
            return false;
        }
        if (!this.b.a(a2.c()) && !this.b.c(a2.c())) {
            return false;
        }
        a(frameLayout, a2);
        return true;
    }

    private final void a(FrameLayout frameLayout, TommedAuvergne tommedAuvergne) {
        BriedeMelun briedeMelun = new BriedeMelun();
        briedeMelun.h("http://ogury.io");
        this.f6100a = CamembertauCalvados.a(this.c, briedeMelun, frameLayout);
        StRomans stRomans = this.f6100a;
        if (stRomans != null) {
            stRomans.a(tommedAuvergne);
        }
    }

    public final void a() {
        StRomans stRomans = this.f6100a;
        if (stRomans != null) {
            stRomans.d();
        }
    }
}
