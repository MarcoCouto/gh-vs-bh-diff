package io.presage;

import android.content.Context;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import com.facebook.share.internal.ShareConstants;
import java.io.ByteArrayInputStream;

public abstract class SaintPaulin extends OlivetalaSauge {

    /* renamed from: a reason: collision with root package name */
    public static final CamembertauCalvados f6092a = new CamembertauCalvados(0);
    private static final WebResourceResponse e;
    private final Handler b;
    private SableduBoulonnais c;
    private CarreMirabelle d;

    public static final class CamembertauCalvados {
        private CamembertauCalvados() {
        }

        public /* synthetic */ CamembertauCalvados(byte b) {
            this();
        }
    }

    static final class CamembertdeNormandie implements Runnable {

        /* renamed from: a reason: collision with root package name */
        final /* synthetic */ SaintPaulin f6093a;

        CamembertdeNormandie(SaintPaulin saintPaulin) {
            this.f6093a = saintPaulin;
        }

        public final void run() {
            this.f6093a.a();
        }
    }

    static final class EcirdelAubrac implements Runnable {

        /* renamed from: a reason: collision with root package name */
        final /* synthetic */ SaintPaulin f6094a;
        final /* synthetic */ String b;

        EcirdelAubrac(SaintPaulin saintPaulin, String str) {
            this.f6094a = saintPaulin;
            this.b = str;
        }

        public final void run() {
            MunsterauCumin.a(this.b);
            this.f6094a.a(this.b);
        }
    }

    public abstract void a();

    public abstract void a(String str);

    public boolean b(WebView webView, String str) {
        return true;
    }

    private SaintPaulin() {
        this.d = null;
        this.b = new Handler(Looper.getMainLooper());
        this.c = SableduBoulonnais.f6090a;
    }

    public /* synthetic */ SaintPaulin(byte b2) {
        this();
    }

    public final void a(CarreMirabelle carreMirabelle) {
        this.d = carreMirabelle;
    }

    static {
        byte[] bytes = "".getBytes(el.f6159a);
        df.a((Object) bytes, "(this as java.lang.String).getBytes(charset)");
        e = new WebResourceResponse("text/image", "UTF-8", new ByteArrayInputStream(bytes));
    }

    public WebResourceResponse a(WebView webView, String str) {
        if (Salers.a(str)) {
            this.b.post(new EcirdelAubrac(this, str));
            return e;
        } else if (b(str)) {
            return a(webView);
        } else {
            return null;
        }
    }

    private final WebResourceResponse a(WebView webView) {
        Context context = webView.getContext();
        df.a((Object) context, "view.context");
        WebResourceResponse a2 = SableduBoulonnais.a(context, this.d);
        if (a2 != null) {
            return a2;
        }
        this.b.post(new CamembertdeNormandie(this));
        return e;
    }

    private static boolean b(String str) {
        Uri parse = Uri.parse(str);
        df.a((Object) parse, ShareConstants.MEDIA_URI);
        return df.a((Object) "mraid.js", (Object) parse.getLastPathSegment());
    }
}
