package io.presage;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import java.io.ByteArrayInputStream;

@SuppressLint({"ViewConstructor"})
public final class x extends WebView {

    /* renamed from: a reason: collision with root package name */
    private boolean f6186a = true;
    private y b;
    private final t c;
    private SaintNectaire d;
    private final SaintPaulin e;
    private final em f;
    private BriedeMelun g;

    public static final class CamembertauCalvados extends SaintPaulin {
        final /* synthetic */ x b;

        CamembertauCalvados(x xVar) {
            this.b = xVar;
            super(0);
        }

        public final void onPageStarted(WebView webView, String str, Bitmap bitmap) {
            y clientAdapter = this.b.getClientAdapter();
            if (clientAdapter != null) {
                clientAdapter.b(webView, str);
            }
        }

        public final void onPageFinished(WebView webView, String str) {
            y clientAdapter = this.b.getClientAdapter();
            if (clientAdapter != null) {
                clientAdapter.a(webView, str);
            }
        }

        public final void a(int i, String str, String str2) {
            StringBuilder sb = new StringBuilder("onReceivedError ");
            sb.append(i);
            sb.append(" description ");
            sb.append(str);
            sb.append(" ulr ");
            sb.append(str2);
            y clientAdapter = this.b.getClientAdapter();
            if (clientAdapter != null) {
                clientAdapter.c();
            }
        }

        public final boolean b(WebView webView, String str) {
            y clientAdapter = this.b.getClientAdapter();
            if (clientAdapter != null) {
                return clientAdapter.a();
            }
            return super.b(webView, str);
        }

        public final void a(String str) {
            this.b.a(str);
        }

        public final WebResourceResponse a(WebView webView, String str) {
            y clientAdapter = this.b.getClientAdapter();
            if (clientAdapter != null) {
                clientAdapter.c(webView, str);
            }
            y clientAdapter2 = this.b.getClientAdapter();
            if (clientAdapter2 == null || !clientAdapter2.a(str)) {
                return super.a(webView, str);
            }
            return x.d();
        }

        public final void a() {
            y clientAdapter = this.b.getClientAdapter();
            if (clientAdapter != null) {
                clientAdapter.c();
            }
        }
    }

    public x(Context context, BriedeMelun briedeMelun) {
        super(context);
        this.g = briedeMelun;
        io.presage.t.CamembertauCalvados camembertauCalvados = t.f6183a;
        this.c = io.presage.t.CamembertauCalvados.a(context);
        this.d = new l(new RacletteSuisse(this), this.c);
        this.f = new em("bunaZiua");
        this.e = c();
        setAdUnit(this.g.l());
        setWebViewClient(this.e);
    }

    public final boolean getShowSdkCloseButton() {
        return this.f6186a;
    }

    public final void setShowSdkCloseButton(boolean z) {
        this.f6186a = z;
    }

    public final y getClientAdapter() {
        return this.b;
    }

    public final void setClientAdapter(y yVar) {
        this.b = yVar;
    }

    public final SaintNectaire getMraidUrlHandler() {
        return this.d;
    }

    public final void setMraidUrlHandler(SaintNectaire saintNectaire) {
        this.d = saintNectaire;
    }

    private final void setAdUnit(CarreMirabelle carreMirabelle) {
        this.e.a(carreMirabelle);
    }

    private final SaintPaulin c() {
        return new CamembertauCalvados(this);
    }

    /* access modifiers changed from: private */
    public final void a(String str) {
        if (this.f.a(str)) {
            this.c.b(this);
            y yVar = this.b;
            if (yVar != null) {
                yVar.a((WebView) this);
            }
        }
        this.d.a(str, this, this.g.l());
    }

    /* access modifiers changed from: private */
    public static WebResourceResponse d() {
        byte[] bytes = "".getBytes(el.f6159a);
        df.a((Object) bytes, "(this as java.lang.String).getBytes(charset)");
        return new WebResourceResponse("text/image", "UTF-8", new ByteArrayInputStream(bytes));
    }

    public final void setWebViewClient(WebViewClient webViewClient) {
        if (!df.a((Object) this.e, (Object) webViewClient)) {
            new IllegalAccessError("Cannot change the webview client for MraidWebView");
        }
        super.setWebViewClient(webViewClient);
    }

    public final void a() {
        b bVar = b.f6126a;
        b.a(this.g.a());
        y yVar = this.b;
        if (yVar != null) {
            yVar.b();
        }
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder("MraidWebView>> ");
        sb.append(Integer.toHexString(System.identityHashCode(this)));
        return sb.toString();
    }

    public final SaintPaulin getMraidWebViewClient() {
        return this.e;
    }
}
