package io.presage;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class em implements Serializable {

    /* renamed from: a reason: collision with root package name */
    public static final CamembertauCalvados f6160a = new CamembertauCalvados(0);
    private final Pattern b;

    public static final class CamembertauCalvados {
        private CamembertauCalvados() {
        }

        public /* synthetic */ CamembertauCalvados(byte b) {
            this();
        }
    }

    static final class CamembertdeNormandie implements Serializable {

        /* renamed from: a reason: collision with root package name */
        public static final CamembertauCalvados f6161a = new CamembertauCalvados(0);
        private static final long serialVersionUID = 0;
        private final String b;
        private final int c;

        public static final class CamembertauCalvados {
            private CamembertauCalvados() {
            }

            public /* synthetic */ CamembertauCalvados(byte b) {
                this();
            }
        }

        public CamembertdeNormandie(String str, int i) {
            this.b = str;
            this.c = i;
        }

        private final Object readResolve() {
            Pattern compile = Pattern.compile(this.b, this.c);
            df.a((Object) compile, "Pattern.compile(pattern, flags)");
            return new em(compile);
        }
    }

    public em(Pattern pattern) {
        this.b = pattern;
    }

    public em(String str) {
        Pattern compile = Pattern.compile(str);
        df.a((Object) compile, "Pattern.compile(pattern)");
        this(compile);
    }

    public final boolean a(CharSequence charSequence) {
        return this.b.matcher(charSequence).find();
    }

    public final List<String> b(CharSequence charSequence) {
        Matcher matcher = this.b.matcher(charSequence);
        if (!matcher.find()) {
            return bn.a(charSequence.toString());
        }
        ArrayList arrayList = new ArrayList(10);
        int i = 0;
        do {
            arrayList.add(charSequence.subSequence(i, matcher.start()).toString());
            i = matcher.end();
        } while (matcher.find());
        arrayList.add(charSequence.subSequence(i, charSequence.length()).toString());
        return arrayList;
    }

    public final String toString() {
        String pattern = this.b.toString();
        df.a((Object) pattern, "nativePattern.toString()");
        return pattern;
    }

    private final Object writeReplace() {
        String pattern = this.b.pattern();
        df.a((Object) pattern, "nativePattern.pattern()");
        return new CamembertdeNormandie(pattern, this.b.flags());
    }
}
