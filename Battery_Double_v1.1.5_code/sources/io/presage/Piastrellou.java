package io.presage;

import android.app.Application;
import android.webkit.WebView;
import com.moat.analytics.mobile.ogury.MoatAnalytics;
import com.moat.analytics.mobile.ogury.WebAdTracker;

public final class Piastrellou {

    /* renamed from: a reason: collision with root package name */
    private WebAdTracker f6082a;
    private final PlaisirauChablis b;

    private Piastrellou(PlaisirauChablis plaisirauChablis) {
        this.b = plaisirauChablis;
    }

    public Piastrellou() {
        MoatAnalytics instance = MoatAnalytics.getInstance();
        df.a((Object) instance, "MoatAnalytics.getInstance()");
        this(new PlaisirauChablis(instance));
    }

    public final void a(Application application) {
        this.b.a(application);
    }

    public final void a(WebView webView) {
        this.f6082a = PlaisirauChablis.a().createWebAdTracker(webView);
    }

    public final void a() {
        WebAdTracker webAdTracker = this.f6082a;
        if (webAdTracker != null) {
            webAdTracker.startTracking();
        }
    }

    public final void b() {
        WebAdTracker webAdTracker = this.f6082a;
        if (webAdTracker != null) {
            webAdTracker.stopTracking();
        }
    }
}
