package io.presage;

import java.io.Closeable;

public final class cn {
    public static final void a(Closeable closeable, Throwable th) {
        if (th == null) {
            closeable.close();
            return;
        }
        try {
            closeable.close();
        } catch (Throwable th2) {
            au.a(th, th2);
        }
    }
}
