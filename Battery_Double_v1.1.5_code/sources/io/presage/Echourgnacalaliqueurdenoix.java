package io.presage;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import java.util.concurrent.LinkedBlockingQueue;

public final class Echourgnacalaliqueurdenoix implements ServiceConnection {

    /* renamed from: a reason: collision with root package name */
    private final LinkedBlockingQueue<IBinder> f6048a = new LinkedBlockingQueue<>(1);
    private boolean b;

    public final void onServiceDisconnected(ComponentName componentName) {
    }

    public final IBinder a() throws InterruptedException {
        if (!this.b) {
            this.b = true;
            Object take = this.f6048a.take();
            if (take != null) {
                return (IBinder) take;
            }
            throw new be("null cannot be cast to non-null type android.os.IBinder");
        }
        throw new IllegalStateException();
    }

    public final void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        try {
            this.f6048a.put(iBinder);
        } catch (InterruptedException unused) {
        }
    }
}
