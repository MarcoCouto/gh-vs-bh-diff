package io.presage;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.content.res.Resources;
import io.presage.mraid.browser.listeners.OrientationListener$1;

public final class TrouduCru {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public int f6108a;
    private final BroadcastReceiver b = new OrientationListener$1(this);
    private final Context c;
    private final StVincentauChablis d;

    public TrouduCru(Context context, StVincentauChablis stVincentauChablis) {
        this.c = context;
        this.d = stVincentauChablis;
        Resources resources = this.c.getResources();
        df.a((Object) resources, "context.resources");
        this.f6108a = resources.getConfiguration().orientation;
        b();
    }

    private final void b() {
        this.c.registerReceiver(this.b, new IntentFilter("android.intent.action.CONFIGURATION_CHANGED"));
    }

    /* access modifiers changed from: private */
    public final void c() {
        this.d.c();
    }

    public final void a() {
        this.c.unregisterReceiver(this.b);
    }
}
