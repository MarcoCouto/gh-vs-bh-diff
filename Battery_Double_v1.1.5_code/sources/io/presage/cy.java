package io.presage;

import java.io.ObjectStreamException;
import java.io.Serializable;

public abstract class cy implements ec, Serializable {
    public static final Object b = CamembertauCalvados.f6151a;

    /* renamed from: a reason: collision with root package name */
    protected final Object f6150a;
    private transient ec c;

    static class CamembertauCalvados implements Serializable {
        /* access modifiers changed from: private */

        /* renamed from: a reason: collision with root package name */
        public static final CamembertauCalvados f6151a = new CamembertauCalvados();

        private CamembertauCalvados() {
        }

        private Object readResolve() throws ObjectStreamException {
            return f6151a;
        }
    }

    /* access modifiers changed from: protected */
    public abstract ec d();

    public cy() {
        this(b);
    }

    protected cy(Object obj) {
        this.f6150a = obj;
    }

    public final Object e() {
        return this.f6150a;
    }

    public final ec f() {
        ec ecVar = this.c;
        if (ecVar != null) {
            return ecVar;
        }
        ec d = d();
        this.c = d;
        return d;
    }

    /* access modifiers changed from: protected */
    public ec g() {
        ec f = f();
        if (f != this) {
            return f;
        }
        throw new cv();
    }

    public ee a() {
        throw new AbstractMethodError();
    }

    public String b() {
        throw new AbstractMethodError();
    }

    public String c() {
        throw new AbstractMethodError();
    }

    public final Object a(Object... objArr) {
        return g().a(objArr);
    }
}
