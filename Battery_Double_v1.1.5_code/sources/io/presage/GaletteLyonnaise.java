package io.presage;

import java.util.concurrent.TimeUnit;

public final class GaletteLyonnaise {
    public static final long a(long j) {
        return TimeUnit.SECONDS.toMillis(j);
    }

    public static final int a(int i) {
        return (int) TimeUnit.SECONDS.toMillis((long) i);
    }
}
