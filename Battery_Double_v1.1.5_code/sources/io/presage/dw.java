package io.presage;

import java.util.NoSuchElementException;

public final class dw extends cc {

    /* renamed from: a reason: collision with root package name */
    private final int f6158a;
    private boolean b;
    private int c;
    private final int d;

    public dw(int i, int i2, int i3) {
        this.d = i3;
        this.f6158a = i2;
        boolean z = false;
        if (this.d <= 0 ? i >= i2 : i <= i2) {
            z = true;
        }
        this.b = z;
        if (!this.b) {
            i = this.f6158a;
        }
        this.c = i;
    }

    public final boolean hasNext() {
        return this.b;
    }

    public final int a() {
        int i = this.c;
        if (i != this.f6158a) {
            this.c += this.d;
        } else if (this.b) {
            this.b = false;
        } else {
            throw new NoSuchElementException();
        }
        return i;
    }
}
