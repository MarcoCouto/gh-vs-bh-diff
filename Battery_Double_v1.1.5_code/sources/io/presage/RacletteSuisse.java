package io.presage;

public final class RacletteSuisse {

    /* renamed from: a reason: collision with root package name */
    private final x f6084a;

    public RacletteSuisse(x xVar) {
        this.f6084a = xVar;
    }

    public final x b() {
        return this.f6084a;
    }

    public final void a(String str, String str2) {
        x xVar = this.f6084a;
        Reblochon reblochon = Reblochon.f6086a;
        z.a(xVar, Reblochon.a(str, str2));
    }

    public final void a(int i) {
        x xVar = this.f6084a;
        Reblochon reblochon = Reblochon.f6086a;
        z.a(xVar, Reblochon.a(i));
    }

    public final void a() {
        x xVar = this.f6084a;
        Reblochon reblochon = Reblochon.f6086a;
        z.a(xVar, Reblochon.a());
    }

    public final void a(String str, boolean z) {
        x xVar = this.f6084a;
        Reblochon reblochon = Reblochon.f6086a;
        z.a(xVar, Reblochon.a(str, z));
    }

    public final void a(boolean z, String str) {
        x xVar = this.f6084a;
        Reblochon reblochon = Reblochon.f6086a;
        z.a(xVar, Reblochon.a(z, str));
    }

    public final void a(int i, int i2) {
        x xVar = this.f6084a;
        Reblochon reblochon = Reblochon.f6086a;
        z.a(xVar, Reblochon.a(i, i2));
    }

    public final void a(String str) {
        x xVar = this.f6084a;
        Reblochon reblochon = Reblochon.f6086a;
        z.a(xVar, Reblochon.a(str));
    }

    public final void a(boolean z) {
        x xVar = this.f6084a;
        Reblochon reblochon = Reblochon.f6086a;
        z.a(xVar, Reblochon.a(z));
    }

    public final void b(int i, int i2) {
        x xVar = this.f6084a;
        Reblochon reblochon = Reblochon.f6086a;
        z.a(xVar, Reblochon.b(i, i2));
    }

    public final void c(int i, int i2) {
        x xVar = this.f6084a;
        Reblochon reblochon = Reblochon.f6086a;
        z.a(xVar, Reblochon.c(i, i2));
    }

    public final void a(int i, int i2, int i3, int i4) {
        x xVar = this.f6084a;
        Reblochon reblochon = Reblochon.f6086a;
        z.a(xVar, Reblochon.a(i, i2, i3, i4));
    }

    public final void b(int i, int i2, int i3, int i4) {
        x xVar = this.f6084a;
        Reblochon reblochon = Reblochon.f6086a;
        z.a(xVar, Reblochon.b(i, i2, i3, i4));
    }

    public final void d(int i, int i2) {
        x xVar = this.f6084a;
        Reblochon reblochon = Reblochon.f6086a;
        z.a(xVar, Reblochon.d(i, i2));
    }

    public final void b(String str) {
        x xVar = this.f6084a;
        Reblochon reblochon = Reblochon.f6086a;
        z.a(xVar, Reblochon.b(str));
    }

    public final void b(String str, String str2) {
        x xVar = this.f6084a;
        Reblochon reblochon = Reblochon.f6086a;
        z.a(xVar, Reblochon.b(str, str2));
    }

    public final void e(int i, int i2) {
        x xVar = this.f6084a;
        Reblochon reblochon = Reblochon.f6086a;
        z.a(xVar, Reblochon.e(i, i2));
    }
}
