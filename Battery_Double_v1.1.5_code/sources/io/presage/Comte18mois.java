package io.presage;

import android.annotation.SuppressLint;
import android.content.Context;
import java.util.concurrent.TimeUnit;
import org.json.JSONObject;

public final class Comte18mois {

    /* renamed from: a reason: collision with root package name */
    public static final CamembertauCalvados f6041a = new CamembertauCalvados(0);
    /* access modifiers changed from: private */
    @SuppressLint({"StaticFieldLeak"})
    public static Comte18mois l;
    private boolean b;
    private long c;
    private EpoissesdeBourgogne d;
    private final Context e;
    private final FourmedAmbert f;
    private final Coulommiers g;
    private final Dauphin h;
    private final Bofavre i;
    private final Goudaaucumin j;
    private final Machecoulais k;

    public static final class CamembertauCalvados {
        private CamembertauCalvados() {
        }

        public /* synthetic */ CamembertauCalvados(byte b) {
            this();
        }

        public final Comte18mois a(Context context) {
            Comte18mois a2;
            Munster.a("Profig.getInstance");
            synchronized (this) {
                if (Comte18mois.l == null) {
                    CamembertauCalvados camembertauCalvados = Comte18mois.f6041a;
                    Context applicationContext = context.getApplicationContext();
                    df.a((Object) applicationContext, "context.applicationContext");
                    Comte18mois.l = b(applicationContext);
                }
                a2 = Comte18mois.l;
                if (a2 == null) {
                    df.a();
                }
            }
            return a2;
        }

        private static Comte18mois b(Context context) {
            io.presage.FourmedAmbert.CamembertauCalvados camembertauCalvados = FourmedAmbert.f6057a;
            FourmedAmbert a2 = io.presage.FourmedAmbert.CamembertauCalvados.a(context);
            Coulommiers coulommiers = new Coulommiers(new Cheddar(context), new CoeurdArras(context));
            BoulettedAvesnes boulettedAvesnes = BoulettedAvesnes.f6017a;
            Context context2 = context;
            Comte18mois comte18mois = new Comte18mois(context2, a2, coulommiers, Dauphin.f6045a, BoulettedAvesnes.a(context), GrandMunster.f6064a, new Maroilles(), 0);
            return comte18mois;
        }
    }

    private Comte18mois(Context context, FourmedAmbert fourmedAmbert, Coulommiers coulommiers, Dauphin dauphin, Bofavre bofavre, Goudaaucumin goudaaucumin, Machecoulais machecoulais) {
        this.e = context;
        this.f = fourmedAmbert;
        this.g = coulommiers;
        this.h = dauphin;
        this.i = bofavre;
        this.j = goudaaucumin;
        this.k = machecoulais;
        this.c = TimeUnit.SECONDS.toMillis(7200);
        this.d = Dauphin.a(this.e);
    }

    public /* synthetic */ Comte18mois(Context context, FourmedAmbert fourmedAmbert, Coulommiers coulommiers, Dauphin dauphin, Bofavre bofavre, Goudaaucumin goudaaucumin, Machecoulais machecoulais, byte b2) {
        this(context, fourmedAmbert, coulommiers, dauphin, bofavre, goudaaucumin, machecoulais);
    }

    private final void b() {
        if (c()) {
            this.f.a(0);
            this.f.e();
        }
    }

    private final boolean c() {
        return this.f.f() != System.currentTimeMillis() / TimeUnit.DAYS.toMillis(1);
    }

    private final void d() {
        this.f.a(this.f.a() + 1);
    }

    private final CureNantais a(boolean z) {
        this.d = Dauphin.a(this.e);
        return new CureNantais(this.g, this.d, this.f, z);
    }

    /* access modifiers changed from: private */
    public void b(boolean z) {
        if (!this.b || z) {
            this.b = true;
            b();
            CureNantais a2 = a(z);
            CremeuxduJura b2 = a2.b();
            StringBuilder sb = new StringBuilder("Profig - profigRequest ");
            sb.append(z);
            sb.append(' ');
            sb.append(b2);
            FourmedeHauteLoire a3 = a2.a();
            if (a3 != null) {
                this.c = a3.e();
            }
            if (!this.k.a(this.e)) {
                this.j.a(this.e, this.c);
                this.b = false;
                return;
            }
            if (b2.d()) {
                this.j.a(this.e, b2.b());
            } else {
                this.j.a(this.e);
            }
            if (b2.a()) {
                a(b2);
            }
            this.b = false;
        }
    }

    private final void a(CremeuxduJura cremeuxduJura) {
        new StringBuilder("making profig api call ").append(cremeuxduJura);
        try {
            am a2 = this.i.a(cremeuxduJura.c());
            if (a2 instanceof an) {
                a(cremeuxduJura, new JSONObject(((an) a2).a()));
                return;
            }
            if (a2 instanceof ad) {
                a(((ad) a2).a());
            }
        } catch (Exception e2) {
            a((Throwable) e2);
        }
    }

    private final void a(Throwable th) {
        if (Munster.a(th)) {
            d();
            e();
        }
    }

    private final void a(CremeuxduJura cremeuxduJura, JSONObject jSONObject) {
        d();
        e();
        a(cremeuxduJura.e());
        this.f.b(this.d.a());
        this.f.a(System.currentTimeMillis());
        FourmedeRochefort fourmedeRochefort = FourmedeRochefort.f6061a;
        a(cremeuxduJura, FourmedeRochefort.a(jSONObject), jSONObject);
    }

    private final void a(String str) {
        if (str != null) {
            this.f.a(str);
        }
    }

    private final void e() {
        this.f.d(Murol.a());
    }

    private final void a(CremeuxduJura cremeuxduJura, Gaperon gaperon, JSONObject jSONObject) {
        if (gaperon instanceof FourmedeHauteLoire) {
            FourmedeMontbrison fourmedeMontbrison = FourmedeMontbrison.f6060a;
            FourmedeHauteLoire fourmedeHauteLoire = (FourmedeHauteLoire) gaperon;
            FourmedeMontbrison.a(fourmedeHauteLoire);
            FourmedAmbert fourmedAmbert = this.f;
            String jSONObject2 = jSONObject.toString();
            df.a((Object) jSONObject2, "profigJsonResponse.toString()");
            fourmedAmbert.c(jSONObject2);
            a(fourmedeHauteLoire.a(), fourmedeHauteLoire.g());
        } else if (gaperon instanceof EtivazGruyereSuisse) {
            b(true);
        } else {
            if (gaperon instanceof FourmedAmbertBio) {
                a(cremeuxduJura.d(), cremeuxduJura.b());
            }
        }
    }

    private final void a(boolean z, long j2) {
        if (z) {
            this.j.a(this.e, j2);
        } else {
            this.j.a(this.e);
        }
    }
}
