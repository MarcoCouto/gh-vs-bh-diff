package io.presage;

import android.content.Context;

public final class BleudeGex implements ac {

    /* renamed from: a reason: collision with root package name */
    public static final CamembertauCalvados f6014a = new CamembertauCalvados(0);
    private final FourmedeMontbrison b;
    private final Context c;
    private final int d;

    public static final class CamembertauCalvados {
        private CamembertauCalvados() {
        }

        public /* synthetic */ CamembertauCalvados(byte b) {
            this();
        }
    }

    public BleudeGex(FourmedeMontbrison fourmedeMontbrison, Context context, int i) {
        this.b = fourmedeMontbrison;
        this.c = context;
        this.d = i;
    }

    public final int a() {
        FourmedeHauteLoire a2 = FourmedeMontbrison.a(this.c);
        return a2 != null ? a2.n() : this.d;
    }

    public final int b() {
        return a() * 5;
    }
}
