package io.presage;

class ev extends eu {
    public static final String a(String str, String str2, String str3) {
        CharSequence charSequence = str;
        int b = en.b(charSequence, str2);
        if (b < 0) {
            return str;
        }
        return en.a(charSequence, b, str2.length() + b, (CharSequence) str3).toString();
    }

    public static final boolean a(String str, String str2) {
        return str.startsWith(str2);
    }

    public static final boolean a(String str, String str2, int i, int i2, boolean z) {
        if (!z) {
            return str.regionMatches(0, str2, i, i2);
        }
        return str.regionMatches(z, 0, str2, i, i2);
    }
}
