package io.presage;

import io.fabric.sdk.android.services.network.HttpRequest;
import io.presage.common.AdConfig;
import org.json.JSONObject;

public final class Bouyssou implements Bofavre {

    /* renamed from: a reason: collision with root package name */
    private final BleudeSassenage f6018a;
    private final CoeurdArras b;
    private final Cheddar c;
    /* access modifiers changed from: private */
    public final aj d;
    private final BleudesCausses e;

    static final class AbbayedeTamie extends dg implements cw<bh> {

        /* renamed from: a reason: collision with root package name */
        final /* synthetic */ Bouyssou f6019a;
        final /* synthetic */ al b;

        AbbayedeTamie(Bouyssou bouyssou, al alVar) {
            this.f6019a = bouyssou;
            this.b = alVar;
            super(0);
        }

        public final /* synthetic */ Object a_() {
            b();
            return bh.f6130a;
        }

        private void b() {
            am a2 = this.f6019a.d.a(this.b).a();
            if (a2 instanceof ad) {
                throw ((ad) a2).a();
            }
        }
    }

    static final class AbbayedeTimadeuc extends dg implements cw<bh> {

        /* renamed from: a reason: collision with root package name */
        final /* synthetic */ Bouyssou f6020a;
        final /* synthetic */ al b;

        AbbayedeTimadeuc(Bouyssou bouyssou, al alVar) {
            this.f6020a = bouyssou;
            this.b = alVar;
            super(0);
        }

        public final /* synthetic */ Object a_() {
            b();
            return bh.f6130a;
        }

        private void b() {
            am a2 = this.f6020a.d.a(this.b).a();
            if (a2 instanceof ad) {
                throw ((ad) a2).a();
            }
        }
    }

    static final class AbbayeduMontdesCats extends dg implements cw<bh> {

        /* renamed from: a reason: collision with root package name */
        final /* synthetic */ Bouyssou f6021a;
        final /* synthetic */ al b;

        AbbayeduMontdesCats(Bouyssou bouyssou, al alVar) {
            this.f6021a = bouyssou;
            this.b = alVar;
            super(0);
        }

        public final /* synthetic */ Object a_() {
            b();
            return bh.f6130a;
        }

        private void b() {
            am a2 = this.f6021a.d.a(this.b).a();
            if (a2 instanceof ad) {
                throw ((ad) a2).a();
            }
        }
    }

    static final /* synthetic */ class CamembertauCalvados extends de implements cx<Throwable, bh> {
        CamembertauCalvados(Mimolette24mois mimolette24mois) {
            super(1, mimolette24mois);
        }

        public final ee a() {
            return dk.a(Mimolette24mois.class);
        }

        public final String b() {
            return "e";
        }

        public final String c() {
            return "e(Ljava/lang/Throwable;)V";
        }

        public final /* bridge */ /* synthetic */ Object a(Object obj) {
            return bh.f6130a;
        }
    }

    static final class CamembertdeNormandie extends dg implements cw<bh> {

        /* renamed from: a reason: collision with root package name */
        public static final CamembertdeNormandie f6022a = new CamembertdeNormandie();

        CamembertdeNormandie() {
            super(0);
        }

        public final /* bridge */ /* synthetic */ Object a_() {
            return bh.f6130a;
        }
    }

    static final class EcirdelAubrac extends dg implements cw<bh> {

        /* renamed from: a reason: collision with root package name */
        final /* synthetic */ Bouyssou f6023a;
        final /* synthetic */ al b;

        EcirdelAubrac(Bouyssou bouyssou, al alVar) {
            this.f6023a = bouyssou;
            this.b = alVar;
            super(0);
        }

        public final /* synthetic */ Object a_() {
            b();
            return bh.f6130a;
        }

        private void b() {
            am a2 = this.f6023a.d.a(this.b).a();
            if (a2 instanceof ad) {
                throw ((ad) a2).a();
            }
        }
    }

    private Bouyssou(BleudeSassenage bleudeSassenage, CoeurdArras coeurdArras, Cheddar cheddar, aj ajVar, BleudesCausses bleudesCausses) {
        this.f6018a = bleudeSassenage;
        this.b = coeurdArras;
        this.c = cheddar;
        this.d = ajVar;
        this.e = bleudesCausses;
    }

    public /* synthetic */ Bouyssou(BleudeSassenage bleudeSassenage, CoeurdArras coeurdArras, Cheddar cheddar, aj ajVar) {
        this(bleudeSassenage, coeurdArras, cheddar, ajVar, new BleudesCausses());
    }

    public final am a(String str, AdConfig adConfig, String str2) {
        BriedeMeaux briedeMeaux = BriedeMeaux.f6025a;
        String a2 = BriedeMeaux.a();
        BrillatSavarin brillatSavarin = new BrillatSavarin(str, adConfig != null ? adConfig.getAdUnitId() : null);
        CoeurdArras coeurdArras = this.b;
        Cheddar cheddar = this.c;
        FourmedeMontbrison fourmedeMontbrison = FourmedeMontbrison.f6060a;
        CantalEntreDeux cantalEntreDeux = new CantalEntreDeux(coeurdArras, cheddar, CarreNormand.f6035a);
        Camembertbio camembertbio = Camembertbio.f6030a;
        return this.d.a(new al(a2, HttpRequest.METHOD_POST, Camembertbio.a(cantalEntreDeux, brillatSavarin, str2), this.f6018a.a())).a();
    }

    public final am c(String str) {
        return this.d.a(new al(str, HttpRequest.METHOD_GET, "", this.f6018a.b())).a();
    }

    public final am a(JSONObject jSONObject) {
        boolean a2 = BleudesCausses.a(jSONObject);
        BriedeMeaux briedeMeaux = BriedeMeaux.f6025a;
        String a3 = BriedeMeaux.a(a2);
        String str = HttpRequest.METHOD_POST;
        String jSONObject2 = jSONObject.toString();
        df.a((Object) jSONObject2, "requestBody.toString()");
        return this.d.a(new al(a3, str, jSONObject2, this.f6018a.b())).a();
    }

    public final EcirdelAubrac a(String str) {
        BriedeMeaux briedeMeaux = BriedeMeaux.f6025a;
        al alVar = new al(BriedeMeaux.b(), HttpRequest.METHOD_POST, str, this.f6018a.b());
        io.presage.EcirdelAubrac.CamembertauCalvados camembertauCalvados = EcirdelAubrac.f6049a;
        return io.presage.EcirdelAubrac.CamembertauCalvados.a(new AbbayedeTamie(this, alVar));
    }

    public final EcirdelAubrac b(JSONObject jSONObject) {
        BriedeMeaux briedeMeaux = BriedeMeaux.f6025a;
        String c2 = BriedeMeaux.c();
        String str = HttpRequest.METHOD_POST;
        String jSONObject2 = jSONObject.toString();
        df.a((Object) jSONObject2, "requestBody.toString()");
        al alVar = new al(c2, str, jSONObject2, this.f6018a.a());
        io.presage.EcirdelAubrac.CamembertauCalvados camembertauCalvados = EcirdelAubrac.f6049a;
        return io.presage.EcirdelAubrac.CamembertauCalvados.a(new AbbayeduMontdesCats(this, alVar));
    }

    public final void b(String str) {
        al alVar = new al(str, HttpRequest.METHOD_GET, "", this.f6018a.a());
        io.presage.EcirdelAubrac.CamembertauCalvados camembertauCalvados = EcirdelAubrac.f6049a;
        io.presage.EcirdelAubrac.CamembertauCalvados.a(new EcirdelAubrac(this, alVar)).a((cx<? super Throwable, bh>) new CamembertauCalvados<Object,bh>(Mimolette24mois.f6066a)).a((cw<bh>) CamembertdeNormandie.f6022a);
    }

    public final EcirdelAubrac c(JSONObject jSONObject) {
        BriedeMeaux briedeMeaux = BriedeMeaux.f6025a;
        String d2 = BriedeMeaux.d();
        String str = HttpRequest.METHOD_POST;
        String jSONObject2 = jSONObject.toString();
        df.a((Object) jSONObject2, "requestBody.toString()");
        al alVar = new al(d2, str, jSONObject2, this.f6018a.a());
        io.presage.EcirdelAubrac.CamembertauCalvados camembertauCalvados = EcirdelAubrac.f6049a;
        return io.presage.EcirdelAubrac.CamembertauCalvados.a(new AbbayedeTimadeuc(this, alVar));
    }
}
