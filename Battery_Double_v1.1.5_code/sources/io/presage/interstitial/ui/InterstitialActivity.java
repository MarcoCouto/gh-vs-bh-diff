package io.presage.interstitial.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build.VERSION;
import android.os.Bundle;
import com.mintegral.msdk.base.entity.CampaignUnit;
import io.presage.BriedeMelun;
import io.presage.BurratadesPouilles;
import io.presage.PersilledumontBlanc;
import io.presage.PontlEveque;
import io.presage.RaclettedeSavoiefumee;
import io.presage.be;
import io.presage.bn;
import io.presage.df;
import io.presage.dn;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class InterstitialActivity extends Activity implements RaclettedeSavoiefumee {

    /* renamed from: a reason: collision with root package name */
    public static final CamembertauCalvados f6169a = new CamembertauCalvados(0);
    private PersilledumontBlanc b = new PersilledumontBlanc(this);

    public static final class CamembertauCalvados {
        private CamembertauCalvados() {
        }

        public /* synthetic */ CamembertauCalvados(byte b) {
            this();
        }

        public static void a(Context context, List<BriedeMelun> list) {
            if (!list.isEmpty()) {
                Intent intent = new Intent(context, a((BriedeMelun) bn.b(list)));
                intent.putExtra(CampaignUnit.JSON_KEY_ADS, new ArrayList(list));
                intent.addFlags(268435456);
                context.startActivity(intent);
            }
        }

        private static Class<?> a(BriedeMelun briedeMelun) {
            if (a()) {
                return InterstitialActivity.class;
            }
            if (briedeMelun.m()) {
                return InterstitialAndroid8TransparentActivity.class;
            }
            return InterstitialAndroid8RotableActivity.class;
        }

        private static boolean a() {
            return VERSION.SDK_INT != 26;
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        d();
        PontlEveque pontlEveque = new PontlEveque(this);
        try {
            this.b.a(pontlEveque, c());
            setContentView(pontlEveque);
        } catch (Exception e) {
            BurratadesPouilles burratadesPouilles = BurratadesPouilles.f6029a;
            BurratadesPouilles.a(e);
            finish();
        }
    }

    private final List<BriedeMelun> c() {
        Serializable serializableExtra = getIntent().getSerializableExtra(CampaignUnit.JSON_KEY_ADS);
        if (serializableExtra != null) {
            return dn.a((Object) serializableExtra);
        }
        throw new be("null cannot be cast to non-null type kotlin.collections.MutableList<io.presage.common.network.models.Ad>");
    }

    public final void a() {
        this.b.a();
    }

    public final void b() {
        this.b.b();
    }

    public final void a(String str) {
        this.b.a(str);
    }

    private final void d() {
        getWindow().setFlags(16777216, 16777216);
    }

    public void a(BriedeMelun briedeMelun) {
        String str = null;
        if (df.a(briedeMelun != null ? briedeMelun.d() : null, (Object) "landscape")) {
            setRequestedOrientation(0);
            return;
        }
        if (briedeMelun != null) {
            str = briedeMelun.d();
        }
        if (df.a((Object) str, (Object) "portrait")) {
            setRequestedOrientation(1);
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        overridePendingTransition(0, 0);
        this.b.c();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.b.d();
        super.onDestroy();
    }

    public void onBackPressed() {
        if (this.b.e()) {
            super.onBackPressed();
        }
    }
}
