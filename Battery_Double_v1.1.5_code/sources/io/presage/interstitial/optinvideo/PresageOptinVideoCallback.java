package io.presage.interstitial.optinvideo;

import io.presage.common.network.models.RewardItem;
import io.presage.interstitial.PresageInterstitialCallback;

public interface PresageOptinVideoCallback extends PresageInterstitialCallback {
    void onAdRewarded(RewardItem rewardItem);
}
