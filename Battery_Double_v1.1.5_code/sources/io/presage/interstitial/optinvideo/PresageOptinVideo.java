package io.presage.interstitial.optinvideo;

import android.content.Context;
import io.presage.ParmigianoReggiano;
import io.presage.common.AdConfig;
import io.presage.dc;

public final class PresageOptinVideo {

    /* renamed from: a reason: collision with root package name */
    public static final CamembertauCalvados f6168a = new CamembertauCalvados(0);
    private final ParmigianoReggiano b;

    public static final class CamembertauCalvados {
        private CamembertauCalvados() {
        }

        public /* synthetic */ CamembertauCalvados(byte b) {
            this();
        }
    }

    private PresageOptinVideo(ParmigianoReggiano parmigianoReggiano) {
        this.b = parmigianoReggiano;
    }

    public /* synthetic */ PresageOptinVideo(ParmigianoReggiano parmigianoReggiano, dc dcVar) {
        this(parmigianoReggiano);
    }

    public PresageOptinVideo(Context context, AdConfig adConfig) {
        this(new ParmigianoReggiano(context, adConfig));
    }

    public final boolean isLoaded() {
        return this.b.a();
    }

    public final void setOptinVideoCallback(PresageOptinVideoCallback presageOptinVideoCallback) {
        this.b.a(presageOptinVideoCallback);
    }

    public final void load() {
        this.b.d();
    }

    public final void show() {
        this.b.e();
    }

    public final void setUserId(String str) {
        this.b.b(str);
    }
}
