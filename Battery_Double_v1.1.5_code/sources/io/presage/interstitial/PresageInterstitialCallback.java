package io.presage.interstitial;

public interface PresageInterstitialCallback {
    void onAdAvailable();

    void onAdClosed();

    void onAdDisplayed();

    void onAdError(int i);

    void onAdLoaded();

    void onAdNotAvailable();

    void onAdNotLoaded();
}
