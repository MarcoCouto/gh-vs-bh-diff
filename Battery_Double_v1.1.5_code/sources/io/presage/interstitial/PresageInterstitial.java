package io.presage.interstitial;

import android.content.Context;
import io.presage.OlivetCendre;
import io.presage.common.AdConfig;
import io.presage.dc;

public final class PresageInterstitial {

    /* renamed from: a reason: collision with root package name */
    public static final CamembertauCalvados f6167a = new CamembertauCalvados(0);
    private final OlivetCendre b;

    public static final class CamembertauCalvados {
        private CamembertauCalvados() {
        }

        public /* synthetic */ CamembertauCalvados(byte b) {
            this();
        }
    }

    private PresageInterstitial(OlivetCendre olivetCendre) {
        this.b = olivetCendre;
    }

    public /* synthetic */ PresageInterstitial(OlivetCendre olivetCendre, dc dcVar) {
        this(olivetCendre);
    }

    public PresageInterstitial(Context context) {
        this(context, (AdConfig) null);
    }

    public PresageInterstitial(Context context, AdConfig adConfig) {
        this(new OlivetCendre(context, adConfig));
    }

    public final boolean isLoaded() {
        return this.b.a();
    }

    public final void setInterstitialCallback(PresageInterstitialCallback presageInterstitialCallback) {
        this.b.a(presageInterstitialCallback);
    }

    public final void load() {
        this.b.d();
    }

    public final void show() {
        this.b.e();
    }
}
