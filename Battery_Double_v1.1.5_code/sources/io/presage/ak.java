package io.presage;

public final class ak extends Exception {

    /* renamed from: a reason: collision with root package name */
    private final int f6117a;

    public ak(int i) {
        StringBuilder sb = new StringBuilder("Received ");
        sb.append(i);
        sb.append(" from the server");
        super(sb.toString());
        this.f6117a = i;
    }
}
