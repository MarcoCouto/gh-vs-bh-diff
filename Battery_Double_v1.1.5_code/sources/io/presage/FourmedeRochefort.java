package io.presage;

import com.applovin.sdk.AppLovinMediationProvider;
import com.ironsource.sdk.constants.Constants.ParametersKeys;
import com.mintegral.msdk.base.entity.CampaignUnit;
import com.tapjoy.TJAdUnitConstants.String;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class FourmedeRochefort {

    /* renamed from: a reason: collision with root package name */
    public static final FourmedeRochefort f6061a = new FourmedeRochefort();

    private FourmedeRochefort() {
    }

    public static Gaperon a(JSONObject jSONObject) {
        Gaperon gaperon;
        if (jSONObject.optBoolean("force")) {
            return EtivazGruyereSuisse.f6056a;
        }
        if (Mascare.a(jSONObject)) {
            return FourmedAmbertBio.f6058a;
        }
        try {
            gaperon = b(jSONObject);
        } catch (JSONException e) {
            BurratadesPouilles burratadesPouilles = BurratadesPouilles.f6029a;
            BurratadesPouilles.a(e);
            gaperon = FourmedAmbertBio.f6058a;
        }
        return gaperon;
    }

    private static FourmedeHauteLoire b(JSONObject jSONObject) {
        FourmedeHauteLoire fourmedeHauteLoire = new FourmedeHauteLoire();
        JSONObject optJSONObject = jSONObject.optJSONObject("profig");
        if (optJSONObject == null) {
            optJSONObject = new JSONObject();
        }
        a(optJSONObject, fourmedeHauteLoire);
        fourmedeHauteLoire.a(Mascare.a(optJSONObject.optJSONObject("max_per_day"), "profig", 10));
        fourmedeHauteLoire.c(GaletteLyonnaise.a(Mascare.a(optJSONObject.optJSONObject("timeout"), CampaignUnit.JSON_KEY_ADS, 3)));
        fourmedeHauteLoire.a(Mascare.a(optJSONObject.optJSONObject("logs"), "crash_report", ""));
        b(optJSONObject, fourmedeHauteLoire);
        c(optJSONObject, fourmedeHauteLoire);
        JSONObject optJSONObject2 = optJSONObject.optJSONObject("cache");
        fourmedeHauteLoire.b(Mascare.a(optJSONObject2 != null ? optJSONObject2.optJSONObject("ads_to_precache") : null, AppLovinMediationProvider.MAX, -1));
        fourmedeHauteLoire.d(GaletteLyonnaise.a(Mascare.a(optJSONObject2, "ad_expiration", 14400)));
        return fourmedeHauteLoire;
    }

    private static void a(JSONObject jSONObject, FourmedeHauteLoire fourmedeHauteLoire) {
        JSONObject optJSONObject = jSONObject.optJSONObject("timing_finder");
        fourmedeHauteLoire.b(GaletteLyonnaise.a(Mascare.a(optJSONObject, "profig", 43200)));
        fourmedeHauteLoire.a(GaletteLyonnaise.a(Mascare.a(optJSONObject, "no_internet_retry", 7200)));
        fourmedeHauteLoire.e(GaletteLyonnaise.a(Mascare.a(optJSONObject, "show_close_button", 2)));
    }

    private static void b(JSONObject jSONObject, FourmedeHauteLoire fourmedeHauteLoire) {
        JSONObject optJSONObject = jSONObject.optJSONObject(ParametersKeys.WEB_VIEW);
        fourmedeHauteLoire.f(Mascare.a(optJSONObject, "back_button_enabled", false));
        fourmedeHauteLoire.g(Mascare.a(optJSONObject, "close_ad_when_leaving_app", true));
        fourmedeHauteLoire.c(GaletteLyonnaise.a(Mascare.a(optJSONObject, "webview_load_timeout", 80)));
    }

    private static void c(JSONObject jSONObject, FourmedeHauteLoire fourmedeHauteLoire) {
        JSONArray optJSONArray = jSONObject.optJSONArray(String.ENABLED);
        if (optJSONArray == null) {
            optJSONArray = new JSONArray();
        }
        String jSONArray = optJSONArray.toString();
        df.a((Object) jSONArray, "enabledArray.toString()");
        CharSequence charSequence = jSONArray;
        fourmedeHauteLoire.a(en.b(charSequence, (CharSequence) "profig"));
        fourmedeHauteLoire.b(en.b(charSequence, (CharSequence) CampaignUnit.JSON_KEY_ADS));
        fourmedeHauteLoire.c(en.b(charSequence, (CharSequence) "launch"));
        fourmedeHauteLoire.d(en.b(charSequence, (CharSequence) "moat"));
        fourmedeHauteLoire.e(en.b(charSequence, (CharSequence) "omid"));
    }

    public static FourmedeHauteLoire a(String str) {
        try {
            if ((str.length() > 0) && (!df.a((Object) str, (Object) "{}"))) {
                return b(new JSONObject(str));
            }
        } catch (Exception unused) {
        }
        return null;
    }
}
