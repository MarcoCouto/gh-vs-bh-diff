package io.presage;

import java.lang.ref.WeakReference;

public final class a {

    /* renamed from: a reason: collision with root package name */
    private final WeakReference<e> f6111a;
    private final x b;
    private final BriedeMelun c;
    private final long d;

    private a(WeakReference<e> weakReference, x xVar, BriedeMelun briedeMelun, long j) {
        this.f6111a = weakReference;
        this.b = xVar;
        this.c = briedeMelun;
        this.d = j;
    }

    public final WeakReference<e> a() {
        return this.f6111a;
    }

    public final x b() {
        return this.b;
    }

    public final BriedeMelun c() {
        return this.c;
    }

    public /* synthetic */ a(WeakReference weakReference, x xVar, BriedeMelun briedeMelun) {
        this(weakReference, xVar, briedeMelun, System.currentTimeMillis());
    }

    public final long d() {
        return this.d;
    }
}
