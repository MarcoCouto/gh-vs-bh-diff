package io.presage;

import java.util.Iterator;

public abstract class cc implements Cdo, Iterator<Integer> {
    public abstract int a();

    public void remove() {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public Integer next() {
        return Integer.valueOf(a());
    }
}
