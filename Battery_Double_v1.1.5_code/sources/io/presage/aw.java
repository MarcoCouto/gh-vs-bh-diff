package io.presage;

import java.io.Serializable;

public final class aw<T> implements ay<T>, Serializable {

    /* renamed from: a reason: collision with root package name */
    private final T f6125a;

    public aw(T t) {
        this.f6125a = t;
    }

    public final T a() {
        return this.f6125a;
    }

    public final String toString() {
        return String.valueOf(a());
    }
}
