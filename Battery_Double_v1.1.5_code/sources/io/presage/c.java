package io.presage;

import android.content.Context;
import java.util.List;

public final class c {

    /* renamed from: a reason: collision with root package name */
    public static final CamembertauCalvados f6133a = new CamembertauCalvados(0);
    private final Context b;
    private long c = 80000;
    private k d = new k();
    private final b e;

    public static final class CamembertauCalvados {
        private CamembertauCalvados() {
        }

        public /* synthetic */ CamembertauCalvados(byte b) {
            this();
        }

        public static c a(Context context) {
            return new c(context, b.f6126a);
        }
    }

    public c(Context context, b bVar) {
        this.e = bVar;
        this.b = context.getApplicationContext();
    }

    public final void a(e eVar, List<BriedeMelun> list, d dVar) {
        this.d.a(dVar);
        this.d.a();
        a(eVar);
        FourmedeMontbrison fourmedeMontbrison = FourmedeMontbrison.f6060a;
        Context context = this.b;
        df.a((Object) context, "context");
        FourmedeHauteLoire a2 = FourmedeMontbrison.a(context);
        if (a2 != null) {
            this.c = a2.k();
        }
        for (BriedeMelun briedeMelun : list) {
            Context context2 = this.b;
            df.a((Object) context2, "context");
            x a3 = z.a(context2, briedeMelun);
            if (a3 != null) {
                OlivetauPoivre.a(a3);
                this.d.a((j) new g(a3, briedeMelun, this.e, eVar));
                if (briedeMelun.r().length() > 0) {
                    a(briedeMelun);
                }
            } else {
                dVar.b();
                return;
            }
        }
        this.d.a(this.c, list.size());
    }

    private final void a(BriedeMelun briedeMelun) {
        x b2 = b(briedeMelun);
        if (b2 != null) {
            StMarcellin.a(b2);
            this.d.a((j) new i(b2, briedeMelun));
        }
    }

    private final x b(BriedeMelun briedeMelun) {
        Context context = this.b;
        df.a((Object) context, "context");
        x a2 = z.a(context, briedeMelun);
        if (a2 != null) {
            OlivetauPoivre.a(a2);
        }
        return a2;
    }

    private static void a(e eVar) {
        b.a(eVar);
    }
}
