package io.presage;

public class de extends cy implements dd, ef {
    private final int c;

    public de(int i, Object obj) {
        super(obj);
        this.c = i;
    }

    /* access modifiers changed from: private */
    /* renamed from: h */
    public ef g() {
        return (ef) super.g();
    }

    /* access modifiers changed from: protected */
    public final ec d() {
        return dk.a(this);
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof de) {
            de deVar = (de) obj;
            if (a() != null ? a().equals(deVar.a()) : deVar.a() == null) {
                return b().equals(deVar.b()) && c().equals(deVar.c()) && df.a(e(), deVar.e());
            }
        } else if (obj instanceof ef) {
            return obj.equals(f());
        } else {
            return false;
        }
    }

    public int hashCode() {
        return (((a() == null ? 0 : a().hashCode() * 31) + b().hashCode()) * 31) + c().hashCode();
    }

    public String toString() {
        ec f = f();
        if (f != this) {
            return f.toString();
        }
        if ("<init>".equals(b())) {
            return "constructor (Kotlin reflection is not available)";
        }
        StringBuilder sb = new StringBuilder("function ");
        sb.append(b());
        sb.append(" (Kotlin reflection is not available)");
        return sb.toString();
    }
}
