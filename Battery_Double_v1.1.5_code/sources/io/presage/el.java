package io.presage;

import com.google.android.exoplayer2.C;
import java.nio.charset.Charset;

public final class el {

    /* renamed from: a reason: collision with root package name */
    public static final Charset f6159a;
    public static final Charset b;
    public static final Charset c;
    public static final Charset d;
    public static final Charset e;
    public static final Charset f;
    public static final el g = new el();

    static {
        Charset forName = Charset.forName("UTF-8");
        df.a((Object) forName, "Charset.forName(\"UTF-8\")");
        f6159a = forName;
        Charset forName2 = Charset.forName("UTF-16");
        df.a((Object) forName2, "Charset.forName(\"UTF-16\")");
        b = forName2;
        Charset forName3 = Charset.forName("UTF-16BE");
        df.a((Object) forName3, "Charset.forName(\"UTF-16BE\")");
        c = forName3;
        Charset forName4 = Charset.forName("UTF-16LE");
        df.a((Object) forName4, "Charset.forName(\"UTF-16LE\")");
        d = forName4;
        Charset forName5 = Charset.forName(C.ASCII_NAME);
        df.a((Object) forName5, "Charset.forName(\"US-ASCII\")");
        e = forName5;
        Charset forName6 = Charset.forName("ISO-8859-1");
        df.a((Object) forName6, "Charset.forName(\"ISO-8859-1\")");
        f = forName6;
    }

    private el() {
    }
}
