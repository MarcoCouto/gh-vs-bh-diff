package io.presage;

public final class VacherinduhautDoubsMontdOr extends VacherinSuisse {

    /* renamed from: a reason: collision with root package name */
    private final BriedeMelun f6110a;
    private final String b;
    private final String c;
    private final String d;
    private final String e;

    public VacherinduhautDoubsMontdOr(BriedeMelun briedeMelun, String str, String str2, String str3, String str4) {
        super("ad_history", 0);
        this.f6110a = briedeMelun;
        this.b = str;
        this.c = str2;
        this.d = str3;
        this.e = str4;
    }

    public final BriedeMelun a() {
        return this.f6110a;
    }

    public final String b() {
        return this.b;
    }

    public final String c() {
        return this.c;
    }

    public final String d() {
        return this.d;
    }

    public final String e() {
        return this.e;
    }
}
