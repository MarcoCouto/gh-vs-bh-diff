package io.presage;

public final class da implements cz, ed<Object> {

    /* renamed from: a reason: collision with root package name */
    private final Class<?> f6152a;

    public da(Class<?> cls) {
        this.f6152a = cls;
    }

    public final Class<?> a() {
        return this.f6152a;
    }

    public final boolean equals(Object obj) {
        return (obj instanceof da) && df.a((Object) cu.a(this), (Object) cu.a((ed) obj));
    }

    public final int hashCode() {
        return cu.a(this).hashCode();
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(a().toString());
        sb.append(" (Kotlin reflection is not available)");
        return sb.toString();
    }
}
