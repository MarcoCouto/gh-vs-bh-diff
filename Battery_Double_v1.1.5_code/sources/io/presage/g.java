package io.presage;

import android.webkit.WebView;
import com.ironsource.sdk.constants.Constants.ParametersKeys;
import java.lang.ref.WeakReference;

public final class g implements j {

    /* renamed from: a reason: collision with root package name */
    private h f6163a;
    /* access modifiers changed from: private */
    public boolean b;
    /* access modifiers changed from: private */
    public final x c;
    /* access modifiers changed from: private */
    public final BriedeMelun d;
    private final b e;
    private final e f;

    public static final class CamembertauCalvados extends y {

        /* renamed from: a reason: collision with root package name */
        final /* synthetic */ g f6164a;

        CamembertauCalvados(g gVar) {
            this.f6164a = gVar;
        }

        public final void b() {
            g.b(this.f6164a.c, this.f6164a.d);
            this.f6164a.d();
        }

        public final void a(WebView webView, String str) {
            this.f6164a.b = true;
            this.f6164a.g();
        }

        public final void c() {
            this.f6164a.e();
        }
    }

    public g(x xVar, BriedeMelun briedeMelun, b bVar, e eVar) {
        this.c = xVar;
        this.d = briedeMelun;
        this.e = bVar;
        this.f = eVar;
        c();
    }

    private final void c() {
        this.c.setClientAdapter(new CamembertauCalvados(this));
    }

    /* access modifiers changed from: private */
    public final void d() {
        h hVar = this.f6163a;
        if (hVar != null) {
            hVar.c();
        }
        f();
    }

    /* access modifiers changed from: private */
    public final void e() {
        AbbayedeCiteauxentiere abbayedeCiteauxentiere = AbbayedeCiteauxentiere.f5998a;
        AbbayedeCiteauxentiere.a((VacherinSuisse) new Bethmale("loaded_error", this.d));
        h hVar = this.f6163a;
        if (hVar != null) {
            hVar.b();
        }
        f();
    }

    private final void f() {
        this.c.setClientAdapter(null);
    }

    /* access modifiers changed from: private */
    public final void g() {
        AbbayedeCiteauxentiere abbayedeCiteauxentiere = AbbayedeCiteauxentiere.f5998a;
        AbbayedeCiteauxentiere.a((VacherinSuisse) new Bethmale(ParametersKeys.LOADED, this.d));
        b.a(new a(new WeakReference(this.f), this.c, this.d));
        h hVar = this.f6163a;
        if (hVar != null) {
            hVar.a();
        }
    }

    /* access modifiers changed from: private */
    public static void b(WebView webView, BriedeMelun briedeMelun) {
        OlivetauPoivre.d(webView);
        AbbayedeCiteauxentiere abbayedeCiteauxentiere = AbbayedeCiteauxentiere.f5998a;
        AbbayedeCiteauxentiere.a((VacherinSuisse) new Bethmale("loaded_error", briedeMelun));
    }

    public final void a(h hVar) {
        this.f6163a = hVar;
        z.a(this.c, this.d);
    }

    public final boolean a() {
        return this.b;
    }

    public final void b() {
        this.f6163a = null;
        f();
        OlivetauPoivre.d(this.c);
    }
}
