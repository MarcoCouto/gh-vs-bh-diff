package io.presage;

import android.os.Handler;
import android.os.Looper;
import java.util.LinkedList;
import java.util.List;

public final class k {

    /* renamed from: a reason: collision with root package name */
    private final List<j> f6170a = new LinkedList();
    private final h b = b();
    /* access modifiers changed from: private */
    public int c;
    private int d;
    /* access modifiers changed from: private */
    public boolean e;
    private Handler f = new Handler(Looper.getMainLooper());
    private d g;

    public static final class CamembertauCalvados implements h {

        /* renamed from: a reason: collision with root package name */
        final /* synthetic */ k f6171a;

        CamembertauCalvados(k kVar) {
            this.f6171a = kVar;
        }

        public final void c() {
            k kVar = this.f6171a;
            kVar.c = kVar.c + 1;
            this.f6171a.c();
        }

        public final void a() {
            k kVar = this.f6171a;
            kVar.c = kVar.c + 1;
            if (this.f6171a.d() && !this.f6171a.e) {
                this.f6171a.f();
            }
        }

        public final void b() {
            this.f6171a.g();
        }
    }

    static final class CamembertdeNormandie implements Runnable {

        /* renamed from: a reason: collision with root package name */
        final /* synthetic */ k f6172a;

        CamembertdeNormandie(k kVar) {
            this.f6172a = kVar;
        }

        public final void run() {
            this.f6172a.i();
        }
    }

    public final void a(d dVar) {
        this.g = dVar;
    }

    private final h b() {
        return new CamembertauCalvados(this);
    }

    /* access modifiers changed from: private */
    public final void c() {
        if (e()) {
            g();
            return;
        }
        if (d()) {
            f();
        }
    }

    /* access modifiers changed from: private */
    public final boolean d() {
        return this.c == this.f6170a.size();
    }

    private final boolean e() {
        return this.d == 1;
    }

    /* access modifiers changed from: private */
    public final void f() {
        h();
        d dVar = this.g;
        if (dVar != null) {
            dVar.a();
        }
    }

    /* access modifiers changed from: private */
    public final void g() {
        h();
        this.e = true;
        d dVar = this.g;
        if (dVar != null) {
            dVar.b();
        }
    }

    public final void a(j jVar) {
        this.f6170a.add(jVar);
    }

    public final void a(long j, int i) {
        this.d = i;
        for (j a2 : this.f6170a) {
            a2.a(this.b);
        }
        a(j);
    }

    private final void h() {
        this.f.removeCallbacksAndMessages(null);
    }

    private final void a(long j) {
        this.f.postDelayed(new CamembertdeNormandie(this), j);
    }

    /* access modifiers changed from: private */
    public final void i() {
        if (k()) {
            f();
            j();
            return;
        }
        l();
        d dVar = this.g;
        if (dVar != null) {
            dVar.b();
        }
    }

    private final void j() {
        for (j jVar : this.f6170a) {
            if (jVar instanceof i) {
                jVar.b();
            }
        }
    }

    private final boolean k() {
        for (j jVar : this.f6170a) {
            if (!jVar.a() && !(jVar instanceof i)) {
                return false;
            }
        }
        return true;
    }

    private final void l() {
        for (j b2 : this.f6170a) {
            b2.b();
        }
    }

    public final void a() {
        h();
        l();
        this.f6170a.clear();
        this.c = 0;
        this.e = false;
    }
}
