package io.presage;

import java.net.URLDecoder;

public final class MunsterauCumin {
    public static final String a(String str) {
        try {
            String decode = URLDecoder.decode(str, "UTF-8");
            df.a((Object) decode, "URLDecoder.decode(this, \"UTF-8\")");
            return decode;
        } catch (Exception unused) {
            return "";
        }
    }
}
