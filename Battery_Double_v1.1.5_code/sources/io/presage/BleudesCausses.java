package io.presage;

import android.os.Build.VERSION;
import android.security.NetworkSecurityPolicy;
import org.json.JSONObject;

public final class BleudesCausses {
    public static boolean a(JSONObject jSONObject) {
        return a() && Mascare.a(jSONObject);
    }

    private static boolean a() {
        if (VERSION.SDK_INT < 23) {
            return true;
        }
        NetworkSecurityPolicy instance = NetworkSecurityPolicy.getInstance();
        df.a((Object) instance, "NetworkSecurityPolicy.getInstance()");
        return instance.isCleartextTrafficPermitted();
    }
}
