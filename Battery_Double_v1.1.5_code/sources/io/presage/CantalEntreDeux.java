package io.presage;

public final class CantalEntreDeux implements CancoillotteNature {

    /* renamed from: a reason: collision with root package name */
    private final FourmedeHauteLoire f6032a;
    private final CoeurdArras b;
    private final Cheddar c;
    private final CarreNormand d;

    public final String i() {
        return "3.2.2-moat";
    }

    public CantalEntreDeux(CoeurdArras coeurdArras, Cheddar cheddar, CarreNormand carreNormand) {
        this.b = coeurdArras;
        this.c = cheddar;
        this.d = carreNormand;
        FourmedeHauteLoire a2 = FourmedeMontbrison.a(this.b.e());
        if (a2 != null) {
            this.f6032a = a2;
            return;
        }
        throw new IllegalStateException("Profig must not be null");
    }

    public final int g() {
        return this.c.j();
    }

    public final int h() {
        return this.c.k();
    }

    public final String a() {
        return this.c.i();
    }

    public final String b() {
        return this.c.f();
    }

    public final String c() {
        return this.b.c();
    }

    public final String d() {
        return this.b.a();
    }

    public final boolean e() {
        return this.f6032a.c() && CarreNormand.a();
    }

    public final boolean f() {
        return this.f6032a.d() && CarreNormand.b();
    }
}
