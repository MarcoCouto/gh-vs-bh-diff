package io.presage;

import android.os.Handler;
import android.os.Looper;

public final class AbbayedeTimadeuc implements AbbayeduMontdesCats {

    /* renamed from: a reason: collision with root package name */
    private final Handler f5999a = new Handler(Looper.getMainLooper());

    public final void a(Runnable runnable) {
        this.f5999a.post(runnable);
    }
}
