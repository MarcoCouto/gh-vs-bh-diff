package io.presage;

public final class AffideliceauChablis implements Appenzeller {

    /* renamed from: a reason: collision with root package name */
    static final /* synthetic */ eg[] f6000a = {dk.a((di) new dj(dk.a(AffideliceauChablis.class), "backgroundScheduler", "getBackgroundScheduler()Lio/presage/async/Scheduler;")), dk.a((di) new dj(dk.a(AffideliceauChablis.class), "mainThreadScheduler", "getMainThreadScheduler()Lio/presage/async/Scheduler;"))};
    public static final AffideliceauChablis b;
    private static Appenzeller c;
    private static final ay d = az.a(CamembertauCalvados.f6001a);
    private static final ay e = az.a(CamembertdeNormandie.f6002a);

    static final class CamembertauCalvados extends dg implements cw<AbbayeduMontdesCats> {

        /* renamed from: a reason: collision with root package name */
        public static final CamembertauCalvados f6001a = new CamembertauCalvados();

        CamembertauCalvados() {
            super(0);
        }

        public final /* synthetic */ Object a_() {
            return b();
        }

        private static AbbayeduMontdesCats b() {
            AffideliceauChablis affideliceauChablis = AffideliceauChablis.b;
            return AffideliceauChablis.a().e();
        }
    }

    static final class CamembertdeNormandie extends dg implements cw<AbbayeduMontdesCats> {

        /* renamed from: a reason: collision with root package name */
        public static final CamembertdeNormandie f6002a = new CamembertdeNormandie();

        CamembertdeNormandie() {
            super(0);
        }

        public final /* synthetic */ Object a_() {
            return b();
        }

        private static AbbayeduMontdesCats b() {
            AffideliceauChablis affideliceauChablis = AffideliceauChablis.b;
            return AffideliceauChablis.a().d();
        }
    }

    private static AbbayeduMontdesCats f() {
        return (AbbayeduMontdesCats) d.a();
    }

    private static AbbayeduMontdesCats g() {
        return (AbbayeduMontdesCats) e.a();
    }

    static {
        AffideliceauChablis affideliceauChablis = new AffideliceauChablis();
        b = affideliceauChablis;
        c = affideliceauChablis;
    }

    private AffideliceauChablis() {
    }

    public static Appenzeller a() {
        return c;
    }

    public static AbbayeduMontdesCats b() {
        return f();
    }

    public static AbbayeduMontdesCats c() {
        return g();
    }

    public final AbbayeduMontdesCats d() {
        return new AbbayedeTimadeuc();
    }

    public final AbbayeduMontdesCats e() {
        return new CamembertdeNormandie();
    }
}
