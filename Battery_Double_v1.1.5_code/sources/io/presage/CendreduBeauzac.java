package io.presage;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import com.mintegral.msdk.base.entity.CampaignEx;
import java.io.File;

public final class CendreduBeauzac {

    /* renamed from: a reason: collision with root package name */
    public static final CamembertauCalvados f6036a = new CamembertauCalvados(0);
    /* access modifiers changed from: private */
    @SuppressLint({"StaticFieldLeak"})
    public static CendreduBeauzac d;
    private final SharedPreferences b;
    private final Context c;

    public static final class CamembertauCalvados {
        private CamembertauCalvados() {
        }

        public /* synthetic */ CamembertauCalvados(byte b) {
            this();
        }

        public static CendreduBeauzac a(Context context) {
            if (CendreduBeauzac.d == null) {
                Context applicationContext = context.getApplicationContext();
                df.a((Object) applicationContext, "context.applicationContext");
                CendreduBeauzac.d = new CendreduBeauzac(applicationContext, 0);
            }
            CendreduBeauzac c = CendreduBeauzac.d;
            if (c == null) {
                df.a();
            }
            return c;
        }
    }

    private CendreduBeauzac(Context context) {
        this.c = context;
        this.b = this.c.getSharedPreferences(CampaignEx.JSON_KEY_MRAID, 0);
    }

    public /* synthetic */ CendreduBeauzac(Context context, byte b2) {
        this(context);
    }

    public final void a(String str) {
        StringBuilder sb = new StringBuilder();
        File filesDir = this.c.getFilesDir();
        df.a((Object) filesDir, "context.filesDir");
        sb.append(filesDir.getPath().toString());
        sb.append("/mraidJs.txt");
        co.a(new File(sb.toString()), str);
    }

    public final void b(String str) {
        this.b.edit().putString("mraid_download_url", str).apply();
    }

    public final String a() {
        String string = this.b.getString("mraid_download_url", "");
        df.a((Object) string, "sharedPref.getString(MRAID_DOWNLOAD_URL, \"\")");
        return string;
    }

    public final String b() {
        StringBuilder sb = new StringBuilder();
        File filesDir = this.c.getFilesDir();
        df.a((Object) filesDir, "context.filesDir");
        sb.append(filesDir.getPath().toString());
        sb.append("/mraidJs.txt");
        File file = new File(sb.toString());
        return file.exists() ? co.b(file) : "";
    }
}
