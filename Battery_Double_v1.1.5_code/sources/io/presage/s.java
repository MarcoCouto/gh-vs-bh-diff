package io.presage;

import com.smaato.sdk.core.api.VideoType;

public final class s implements u {

    /* renamed from: a reason: collision with root package name */
    private final Cheddar f6182a;
    private final u b;

    public s(Cheddar cheddar, u uVar) {
        this.f6182a = cheddar;
        this.b = uVar;
    }

    public final void a(RacletteSuisse racletteSuisse) {
        x b2 = racletteSuisse.b();
        int a2 = Montbriac.a(b2.getWidth());
        int a3 = Montbriac.a(b2.getHeight());
        int x = (int) b2.getX();
        int y = (int) b2.getY();
        this.b.a(racletteSuisse);
        racletteSuisse.a(VideoType.INTERSTITIAL);
        racletteSuisse.a(false);
        racletteSuisse.a(this.f6182a.l());
        racletteSuisse.a();
        racletteSuisse.c(a2, a3);
        racletteSuisse.a(a2, a3, x, y);
        racletteSuisse.b(a2, a3, x, y);
        racletteSuisse.d(a2, a3);
        racletteSuisse.b("default");
    }
}
