package io.presage;

import java.util.ListIterator;
import java.util.NoSuchElementException;

public final class by implements Cdo, ListIterator {

    /* renamed from: a reason: collision with root package name */
    public static final by f6131a = new by();

    public final /* synthetic */ void add(Object obj) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public final boolean hasNext() {
        return false;
    }

    public final boolean hasPrevious() {
        return false;
    }

    public final int nextIndex() {
        return 0;
    }

    public final int previousIndex() {
        return -1;
    }

    public final void remove() {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public final /* synthetic */ void set(Object obj) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    private by() {
    }

    public final /* synthetic */ Object next() {
        return a();
    }

    public final /* synthetic */ Object previous() {
        return b();
    }

    private static Void a() {
        throw new NoSuchElementException();
    }

    private static Void b() {
        throw new NoSuchElementException();
    }
}
