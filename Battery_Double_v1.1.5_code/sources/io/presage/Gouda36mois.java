package io.presage;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build.VERSION;
import android.support.v4.app.NotificationCompat;
import io.presage.common.profig.schedule.ProfigAlarmReceiver;
import java.util.GregorianCalendar;

public final class Gouda36mois implements Goudaauxepices {

    /* renamed from: a reason: collision with root package name */
    public static final CamembertauCalvados f6063a = new CamembertauCalvados(0);
    private AlarmManager b;
    private final Context c;

    public static final class CamembertauCalvados {
        private CamembertauCalvados() {
        }

        public /* synthetic */ CamembertauCalvados(byte b) {
            this();
        }
    }

    public Gouda36mois(Context context) {
        this.c = context;
        Object systemService = this.c.getSystemService(NotificationCompat.CATEGORY_ALARM);
        if (systemService != null) {
            this.b = (AlarmManager) systemService;
            return;
        }
        throw new be("null cannot be cast to non-null type android.app.AlarmManager");
    }

    public final void a(long j) {
        PendingIntent a2 = a(this.c);
        this.b.cancel(a2);
        long timeInMillis = new GregorianCalendar().getTimeInMillis() + j;
        if (VERSION.SDK_INT >= 19) {
            this.b.setExact(0, timeInMillis, a2);
        } else {
            this.b.set(0, timeInMillis, a2);
        }
    }

    public final void a() {
        PendingIntent a2 = a(this.c);
        Object systemService = this.c.getSystemService(NotificationCompat.CATEGORY_ALARM);
        if (systemService != null) {
            ((AlarmManager) systemService).cancel(a2);
            a2.cancel();
            return;
        }
        throw new be("null cannot be cast to non-null type android.app.AlarmManager");
    }

    private static PendingIntent a(Context context) {
        PendingIntent broadcast = PendingIntent.getBroadcast(context, 1001, new Intent(context, ProfigAlarmReceiver.class), 268435456);
        df.a((Object) broadcast, "PendingIntent.getBroadca…tent.FLAG_CANCEL_CURRENT)");
        return broadcast;
    }
}
