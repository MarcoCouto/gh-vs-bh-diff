package io.presage;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.FrameLayout;
import com.ironsource.sdk.constants.Constants.ParametersKeys;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.regex.Pattern;

public final class StRomans {

    /* renamed from: a reason: collision with root package name */
    public static final CamembertauCalvados f6096a = new CamembertauCalvados(0);
    /* access modifiers changed from: private */
    public Stilton b;
    private final Pattern c;
    private final BriedeMelun d;
    /* access modifiers changed from: private */
    public final t e;
    private final Map<String, x> f;
    /* access modifiers changed from: private */
    public final Map<String, TommedeYenne> g;
    private final Soumaintrain h;
    private final StVincentauChablis i;
    private final TrouduCru j;
    private final AbbayedeCiteauxentiere k;
    private final o l;
    private final TommeduJura m;

    public static final class CamembertauCalvados {
        private CamembertauCalvados() {
        }

        public /* synthetic */ CamembertauCalvados(byte b) {
            this();
        }

        public static StRomans a(Activity activity, BriedeMelun briedeMelun, FrameLayout frameLayout) {
            io.presage.t.CamembertauCalvados camembertauCalvados = t.f6183a;
            Context context = activity;
            t a2 = io.presage.t.CamembertauCalvados.a(context);
            Map synchronizedMap = Collections.synchronizedMap(new LinkedHashMap());
            df.a((Object) synchronizedMap, "Collections.synchronizedMap(mutableMapOf())");
            Map synchronizedMap2 = Collections.synchronizedMap(new LinkedHashMap());
            df.a((Object) synchronizedMap2, "Collections.synchronizedMap(mutableMapOf())");
            Soumaintrain soumaintrain = new Soumaintrain(context, frameLayout, briedeMelun);
            StVincentauChablis stVincentauChablis = new StVincentauChablis(synchronizedMap, synchronizedMap2, a2);
            Context applicationContext = activity.getApplicationContext();
            df.a((Object) applicationContext, "activity.applicationContext");
            BriedeMelun briedeMelun2 = briedeMelun;
            StVincentauChablis stVincentauChablis2 = stVincentauChablis;
            StRomans stRomans = new StRomans(briedeMelun2, a2, synchronizedMap, synchronizedMap2, soumaintrain, stVincentauChablis2, new TrouduCru(applicationContext, stVincentauChablis), AbbayedeCiteauxentiere.f5998a, new o(activity), new TommeduJura(context, stVincentauChablis), 0);
            stRomans.b = new Stilton(stRomans, stVincentauChablis);
            return stRomans;
        }
    }

    public static final class CamembertdeNormandie extends y {

        /* renamed from: a reason: collision with root package name */
        final /* synthetic */ StRomans f6097a;
        final /* synthetic */ x b;
        private String c = "";

        public final boolean a() {
            return false;
        }

        CamembertdeNormandie(StRomans stRomans, x xVar) {
            this.f6097a = stRomans;
            this.b = xVar;
        }

        public final void b(WebView webView, String str) {
            this.c = str;
            this.f6097a.a(webView, str);
        }

        public final void a(WebView webView, String str) {
            this.f6097a.b(webView, str);
        }

        public final void c(WebView webView, String str) {
            this.f6097a.a(webView, this.c, str);
        }

        public final void a(WebView webView) {
            TommedeYenne tommedeYenne = (TommedeYenne) this.f6097a.g.get(StMarcellin.b(webView));
            if (tommedeYenne != null) {
                tommedeYenne.e();
            }
            this.f6097a.e.a(this.b);
        }
    }

    private StRomans(BriedeMelun briedeMelun, t tVar, Map<String, x> map, Map<String, TommedeYenne> map2, Soumaintrain soumaintrain, StVincentauChablis stVincentauChablis, TrouduCru trouduCru, AbbayedeCiteauxentiere abbayedeCiteauxentiere, o oVar, TommeduJura tommeduJura) {
        this.d = briedeMelun;
        this.e = tVar;
        this.f = map;
        this.g = map2;
        this.h = soumaintrain;
        this.i = stVincentauChablis;
        this.j = trouduCru;
        this.k = abbayedeCiteauxentiere;
        this.l = oVar;
        this.m = tommeduJura;
        this.c = Pattern.compile(this.d.i());
    }

    public /* synthetic */ StRomans(BriedeMelun briedeMelun, t tVar, Map map, Map map2, Soumaintrain soumaintrain, StVincentauChablis stVincentauChablis, TrouduCru trouduCru, AbbayedeCiteauxentiere abbayedeCiteauxentiere, o oVar, TommeduJura tommeduJura, byte b2) {
        this(briedeMelun, tVar, map, map2, soumaintrain, stVincentauChablis, trouduCru, abbayedeCiteauxentiere, oVar, tommeduJura);
    }

    public final SaintNectaire a() {
        Stilton stilton = this.b;
        if (stilton == null) {
            df.a("multiWebViewUrlHandler");
        }
        return stilton;
    }

    public final boolean b() {
        boolean z;
        Iterable values = this.g.values();
        if (!(values instanceof Collection) || !((Collection) values).isEmpty()) {
            Iterator it = values.iterator();
            while (true) {
                if (it.hasNext()) {
                    if (((TommedeYenne) it.next()).b()) {
                        z = true;
                        break;
                    }
                } else {
                    break;
                }
            }
        }
        z = false;
        return !z;
    }

    @SuppressLint({"ClickableViewAccessibility"})
    public final void a(TommedAuvergne tommedAuvergne) {
        x a2 = this.h.a(tommedAuvergne);
        if (a2 != null) {
            this.f.put(tommedAuvergne.c(), a2);
            TommedeYenne tommedeYenne = new TommedeYenne(tommedAuvergne.h(), tommedAuvergne.i(), tommedAuvergne.a(), false, 56);
            this.g.put(tommedAuvergne.c(), tommedeYenne);
            a(a2);
            if (tommedAuvergne.j()) {
                OlivetauPoivre.a(a2);
                WebSettings settings = a2.getSettings();
                df.a((Object) settings, "webView.settings");
                settings.setCacheMode(1);
            }
            a(tommedAuvergne, (WebView) a2);
        }
    }

    private final void a(TommedAuvergne tommedAuvergne, WebView webView) {
        if (tommedAuvergne.a().length() > 0) {
            webView.loadUrl(tommedAuvergne.a());
        } else {
            webView.loadDataWithBaseURL(this.d.h(), tommedAuvergne.b(), WebRequest.CONTENT_TYPE_HTML, "UTF-8", null);
        }
    }

    public final void a(String str, x xVar) {
        xVar.setTag(str);
        this.f.put(str, xVar);
        Map<String, TommedeYenne> map = this.g;
        TommedeYenne tommedeYenne = new TommedeYenne(false, false, "", true, 48);
        map.put(str, tommedeYenne);
    }

    public final void b(TommedAuvergne tommedAuvergne) {
        x xVar = (x) this.f.get(tommedAuvergne.c());
        if (xVar != null) {
            WebView webView = xVar;
            Soumaintrain.a(webView, tommedAuvergne);
            boolean z = false;
            if (!(tommedAuvergne.a().length() > 0)) {
                if (tommedAuvergne.b().length() > 0) {
                    z = true;
                }
                if (!z) {
                    return;
                }
            }
            a(tommedAuvergne, webView);
        }
    }

    public final void a(String str) {
        x xVar = (x) this.f.get(str);
        if (xVar != null) {
            this.h.a((WebView) xVar);
        }
        this.f.remove(str);
        this.g.remove(str);
    }

    private final void a(x xVar) {
        SaintNectaire[] saintNectaireArr = new SaintNectaire[2];
        Stilton stilton = this.b;
        if (stilton == null) {
            df.a("multiWebViewUrlHandler");
        }
        saintNectaireArr[0] = stilton;
        saintNectaireArr[1] = this.l.a(xVar);
        xVar.setMraidUrlHandler(new n(saintNectaireArr));
        xVar.setClientAdapter(new CamembertdeNormandie(this, xVar));
    }

    /* access modifiers changed from: private */
    public final void a(WebView webView, String str) {
        this.i.a(ParametersKeys.VIDEO_STATUS_STARTED, f(), e(), StMarcellin.b(webView), str);
    }

    /* access modifiers changed from: private */
    public final void b(WebView webView, String str) {
        this.i.a("finished", f(), e(), StMarcellin.b(webView), str);
        TommedeYenne tommedeYenne = (TommedeYenne) this.g.get(StMarcellin.b(webView));
        if (tommedeYenne != null) {
            boolean z = false;
            boolean z2 = !tommedeYenne.f() || (df.a((Object) tommedeYenne.c(), (Object) str) ^ true);
            if (tommedeYenne.a() && z2) {
                if (this.d.i().length() == 0) {
                    z = true;
                }
                if (z) {
                    VacherinduhautDoubsMontdOr vacherinduhautDoubsMontdOr = new VacherinduhautDoubsMontdOr(this.d, str, "format", null, null);
                    AbbayedeCiteauxentiere.a((VacherinSuisse) vacherinduhautDoubsMontdOr);
                }
            }
            tommedeYenne.g();
        }
    }

    /* access modifiers changed from: private */
    public final void a(WebView webView, String str, String str2) {
        TommedeYenne tommedeYenne = (TommedeYenne) this.g.get(StMarcellin.b(webView));
        if (tommedeYenne != null && !tommedeYenne.h()) {
            if ((this.d.i().length() > 0) && this.c.matcher(str2).matches()) {
                VacherinduhautDoubsMontdOr vacherinduhautDoubsMontdOr = new VacherinduhautDoubsMontdOr(this.d, str, "format", this.d.i(), str2);
                AbbayedeCiteauxentiere.a((VacherinSuisse) vacherinduhautDoubsMontdOr);
                tommedeYenne.i();
            }
        }
    }

    public final void c() {
        for (x xVar : this.f.values()) {
            if (xVar.canGoBack()) {
                xVar.goBack();
            }
        }
    }

    public final void d() {
        this.i.a();
        this.j.a();
        this.m.a();
    }

    private final boolean e() {
        for (x canGoBack : this.f.values()) {
            if (canGoBack.canGoBack()) {
                return true;
            }
        }
        return false;
    }

    private final boolean f() {
        for (x canGoForward : this.f.values()) {
            if (canGoForward.canGoForward()) {
                return true;
            }
        }
        return false;
    }

    public final void b(String str) {
        x xVar = (x) this.f.get(str);
        if (xVar != null && xVar.canGoBack()) {
            xVar.goBack();
        }
    }

    public final void c(String str) {
        x xVar = (x) this.f.get(str);
        if (xVar != null && xVar.canGoForward()) {
            xVar.goForward();
        }
    }
}
