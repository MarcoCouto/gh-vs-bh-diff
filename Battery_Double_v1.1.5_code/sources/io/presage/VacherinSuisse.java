package io.presage;

public abstract class VacherinSuisse {

    /* renamed from: a reason: collision with root package name */
    private final String f6109a;

    private VacherinSuisse(String str) {
        this.f6109a = str;
    }

    public /* synthetic */ VacherinSuisse(String str, byte b) {
        this(str);
    }

    public final String f() {
        return this.f6109a;
    }
}
