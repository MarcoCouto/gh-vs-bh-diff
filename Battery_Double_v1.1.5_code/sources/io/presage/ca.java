package io.presage;

import java.io.Serializable;
import java.util.Collection;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

final class ca implements Cdo, Serializable, Map {

    /* renamed from: a reason: collision with root package name */
    public static final ca f6134a = new ca();
    private static final long serialVersionUID = 8246714829545688274L;

    public final void clear() {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public final boolean containsKey(Object obj) {
        return false;
    }

    public final /* bridge */ Object get(Object obj) {
        return null;
    }

    public final int hashCode() {
        return 0;
    }

    public final boolean isEmpty() {
        return true;
    }

    public final /* synthetic */ Object put(Object obj, Object obj2) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public final void putAll(Map map) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public final Object remove(Object obj) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public final /* bridge */ int size() {
        return 0;
    }

    public final String toString() {
        return "{}";
    }

    private ca() {
    }

    public final /* bridge */ boolean containsValue(Object obj) {
        if (!(obj instanceof Void)) {
        }
        return false;
    }

    public final Set<Entry> entrySet() {
        return a();
    }

    public final Set<Object> keySet() {
        return b();
    }

    public final Collection values() {
        return c();
    }

    public final boolean equals(Object obj) {
        return (obj instanceof Map) && ((Map) obj).isEmpty();
    }

    private static Set<Entry> a() {
        return cb.f6135a;
    }

    private static Set<Object> b() {
        return cb.f6135a;
    }

    private static Collection c() {
        return bz.f6132a;
    }

    private final Object readResolve() {
        return f6134a;
    }
}
