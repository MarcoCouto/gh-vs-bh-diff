package io.presage;

import android.content.Context;

public final class BleudeSassenage {

    /* renamed from: a reason: collision with root package name */
    public static final CamembertauCalvados f6016a = new CamembertauCalvados(0);
    private final BleudeLaqueuille b;
    private final BrieauPoivre c;

    public static final class CamembertauCalvados {
        private CamembertauCalvados() {
        }

        public /* synthetic */ CamembertauCalvados(byte b) {
            this();
        }

        public static BleudeSassenage a(Context context) {
            Context applicationContext = context.getApplicationContext();
            df.a((Object) applicationContext, "context.applicationContext");
            return new BleudeSassenage(applicationContext);
        }
    }

    public BleudeSassenage(Context context) {
        this.b = new BleudeLaqueuille(context);
        this.c = new BrieauPoivre(context);
    }

    public final af a() {
        return this.b;
    }

    public final af b() {
        return this.c;
    }
}
