package io.presage;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.NoSuchElementException;

public class bx extends bw {
    public static final <T> T b(List<? extends T> list) {
        if (!list.isEmpty()) {
            return list.get(0);
        }
        throw new NoSuchElementException("List is empty.");
    }

    public static final <T> T c(List<? extends T> list) {
        if (list.isEmpty()) {
            return null;
        }
        return list.get(0);
    }

    public static final <T> T d(List<? extends T> list) {
        if (!list.isEmpty()) {
            return list.get(bn.a(list));
        }
        throw new NoSuchElementException("List is empty.");
    }

    public static final <T> List<T> a(Collection<? extends T> collection) {
        return new ArrayList<>(collection);
    }
}
