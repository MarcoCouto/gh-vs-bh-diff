package io.presage;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public final class Livarot {
    @SuppressLint({"MissingPermission"})
    public static final NetworkInfo a(Context context) {
        if (!MoelleuxduRevard.a(context, "android.permission.ACCESS_NETWORK_STATE")) {
            return null;
        }
        Object systemService = context.getSystemService("connectivity");
        if (systemService != null) {
            return ((ConnectivityManager) systemService).getActiveNetworkInfo();
        }
        throw new be("null cannot be cast to non-null type android.net.ConnectivityManager");
    }

    public static final boolean a(NetworkInfo networkInfo) {
        return networkInfo.isConnected() && networkInfo.getType() == 1;
    }

    public static final boolean b(Context context) {
        NetworkInfo a2 = a(context);
        return a2 != null && a2.isConnected();
    }
}
