package io.presage;

public abstract class dh extends cy implements eg {
    /* access modifiers changed from: protected */
    /* renamed from: h */
    public final eg g() {
        return (eg) super.g();
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof dh) {
            dh dhVar = (dh) obj;
            return a().equals(dhVar.a()) && b().equals(dhVar.b()) && c().equals(dhVar.c()) && df.a(e(), dhVar.e());
        } else if (obj instanceof eg) {
            return obj.equals(f());
        } else {
            return false;
        }
    }

    public int hashCode() {
        return (((a().hashCode() * 31) + b().hashCode()) * 31) + c().hashCode();
    }

    public String toString() {
        ec f = f();
        if (f != this) {
            return f.toString();
        }
        StringBuilder sb = new StringBuilder("property ");
        sb.append(b());
        sb.append(" (Kotlin reflection is not available)");
        return sb.toString();
    }
}
