package io.presage;

import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Looper;
import java.io.IOException;

public final class EmmentalGrandCru {

    /* renamed from: a reason: collision with root package name */
    public static final EmmentalGrandCru f6053a = new EmmentalGrandCru();

    private EmmentalGrandCru() {
    }

    public static DelicedesFiouves a(Context context) throws Exception {
        a();
        b(context);
        Echourgnacalaliqueurdenoix echourgnacalaliqueurdenoix = new Echourgnacalaliqueurdenoix();
        Intent intent = new Intent(AdvertisingInfoServiceStrategy.GOOGLE_PLAY_SERVICES_INTENT);
        intent.setPackage("com.google.android.gms");
        ServiceConnection serviceConnection = echourgnacalaliqueurdenoix;
        if (context.bindService(intent, serviceConnection, 1)) {
            try {
                Entrammes entrammes = new Entrammes(echourgnacalaliqueurdenoix.a());
                return new DelicedesFiouves(entrammes.a(), entrammes.b());
            } finally {
                context.unbindService(serviceConnection);
            }
        } else {
            throw new IOException("Google Play connection failed");
        }
    }

    private static void a() {
        if (df.a((Object) Looper.myLooper(), (Object) Looper.getMainLooper())) {
            throw new IllegalStateException("Cannot be called from the main thread");
        }
    }

    private static void b(Context context) {
        if (c(context)) {
            throw new IllegalStateException("Google play is not installed");
        }
    }

    private static boolean c(Context context) {
        try {
            context.getPackageManager().getPackageInfo("com.android.vending", 0);
            return false;
        } catch (Exception unused) {
            return true;
        }
    }
}
