package io.presage;

import android.app.Application;
import com.moat.analytics.mobile.ogury.MoatAnalytics;
import com.moat.analytics.mobile.ogury.MoatFactory;

public final class PlaisirauChablis {

    /* renamed from: a reason: collision with root package name */
    private boolean f6083a;
    private final MoatAnalytics b;

    public PlaisirauChablis(MoatAnalytics moatAnalytics) {
        this.b = moatAnalytics;
    }

    public final void a(Application application) {
        this.f6083a = true;
        this.b.start(application);
    }

    public static MoatFactory a() {
        MoatFactory create = MoatFactory.create();
        df.a((Object) create, "MoatFactory.create()");
        return create;
    }
}
