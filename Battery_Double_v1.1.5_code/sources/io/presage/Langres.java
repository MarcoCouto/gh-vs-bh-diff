package io.presage;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

public final class Langres {

    /* renamed from: a reason: collision with root package name */
    public static final CamembertauCalvados f6065a = new CamembertauCalvados(0);

    public static final class CamembertauCalvados {
        private CamembertauCalvados() {
        }

        public /* synthetic */ CamembertauCalvados(byte b) {
            this();
        }
    }

    public static Bitmap a(String str) {
        HttpURLConnection httpURLConnection;
        InputStream inputStream;
        InputStream inputStream2;
        HttpURLConnection httpURLConnection2;
        InputStream inputStream3;
        HttpURLConnection httpURLConnection3 = null;
        try {
            httpURLConnection2 = b(str);
            try {
                inputStream2 = httpURLConnection2.getInputStream();
            } catch (Throwable th) {
                th = th;
                inputStream2 = null;
                inputStream = inputStream2;
                a(httpURLConnection2, inputStream2);
                a(httpURLConnection3, inputStream);
                throw th;
            }
            try {
                Options options = new Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeStream(inputStream2, null, options);
                options.inSampleSize = a(options, Montbriac.b(48), Montbriac.b(48));
                options.inJustDecodeBounds = false;
                httpURLConnection = b(str);
                try {
                    inputStream = httpURLConnection.getInputStream();
                    try {
                        Bitmap decodeStream = BitmapFactory.decodeStream(inputStream, null, options);
                        a(httpURLConnection2, inputStream2);
                        a(httpURLConnection, inputStream);
                        return decodeStream;
                    } catch (Throwable th2) {
                        Throwable th3 = th2;
                        httpURLConnection3 = httpURLConnection;
                        th = th3;
                        a(httpURLConnection2, inputStream2);
                        a(httpURLConnection3, inputStream);
                        throw th;
                    }
                } catch (Throwable th4) {
                    inputStream = null;
                    httpURLConnection3 = httpURLConnection;
                    th = th4;
                    a(httpURLConnection2, inputStream2);
                    a(httpURLConnection3, inputStream);
                    throw th;
                }
            } catch (Throwable th5) {
                th = th5;
                inputStream = null;
                a(httpURLConnection2, inputStream2);
                a(httpURLConnection3, inputStream);
                throw th;
            }
        } catch (Throwable th6) {
            th = th6;
            httpURLConnection2 = null;
            inputStream2 = null;
            inputStream = inputStream2;
            a(httpURLConnection2, inputStream2);
            a(httpURLConnection3, inputStream);
            throw th;
        }
    }

    private static HttpURLConnection b(String str) {
        URLConnection openConnection = new URL(str).openConnection();
        if (openConnection != null) {
            HttpURLConnection httpURLConnection = (HttpURLConnection) openConnection;
            httpURLConnection.setDoInput(true);
            httpURLConnection.setRequestProperty("Connection", "close");
            httpURLConnection.connect();
            return httpURLConnection;
        }
        throw new be("null cannot be cast to non-null type java.net.HttpURLConnection");
    }

    private static int a(Options options, int i, int i2) {
        int i3 = options.outHeight;
        int i4 = options.outWidth;
        int i5 = 1;
        if (i3 > i2 || i4 > i) {
            int i6 = i3 / 2;
            int i7 = i4 / 2;
            while (i6 / i5 >= i2 && i7 / i5 >= i) {
                i5 *= 2;
            }
        }
        return i5;
    }

    private static void a(HttpURLConnection httpURLConnection, InputStream inputStream) {
        if (httpURLConnection != null) {
            try {
                httpURLConnection.disconnect();
            } catch (Throwable unused) {
                return;
            }
        }
        if (inputStream != null) {
            ab.a(inputStream);
        }
    }
}
