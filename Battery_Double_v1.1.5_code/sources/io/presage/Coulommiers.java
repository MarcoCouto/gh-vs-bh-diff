package io.presage;

import android.content.res.Resources;
import android.util.DisplayMetrics;
import com.facebook.internal.NativeProtocol;
import com.tapjoy.TapjoyConstants;
import org.json.JSONObject;

public final class Coulommiers {

    /* renamed from: a reason: collision with root package name */
    private final Cheddar f6042a;
    private final CoeurdArras b;

    public Coulommiers(Cheddar cheddar, CoeurdArras coeurdArras) {
        this.f6042a = cheddar;
        this.b = coeurdArras;
    }

    public final JSONObject a(EpoissesdeBourgogne epoissesdeBourgogne) {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put(TapjoyConstants.TJC_DEVICE_TIMEZONE, Cheddar.e());
        jSONObject.put("aaid", epoissesdeBourgogne.a());
        jSONObject.put(TapjoyConstants.TJC_DEVICE_LANGUAGE, this.b.b());
        jSONObject.put(TapjoyConstants.TJC_DEVICE_COUNTRY_CODE, this.b.c());
        jSONObject.put("install_unknown_sources", this.f6042a.g());
        jSONObject.put("aaid_optin", epoissesdeBourgogne.b());
        jSONObject.put("fake_aaid", epoissesdeBourgogne.c());
        jSONObject.put("device", a());
        jSONObject.put(NativeProtocol.RESULT_ARGS_PERMISSIONS, c());
        return jSONObject;
    }

    private final JSONObject a() {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("name", this.f6042a.h());
        jSONObject.put("screen", b());
        jSONObject.put(TapjoyConstants.TJC_DEVICE_OS_VERSION_NAME, Cheddar.a());
        jSONObject.put("vm_name", Cheddar.c());
        jSONObject.put("phone_arch", Cheddar.d());
        jSONObject.put("vm_version", Cheddar.b());
        return jSONObject;
    }

    private static JSONObject b() {
        Resources system = Resources.getSystem();
        df.a((Object) system, "Resources.getSystem()");
        DisplayMetrics displayMetrics = system.getDisplayMetrics();
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("density", Float.valueOf(displayMetrics.density));
        jSONObject.put("height", displayMetrics.heightPixels);
        jSONObject.put("width", displayMetrics.widthPixels);
        return jSONObject;
    }

    private final JSONObject c() {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("ACCESS_NETWORK_STATE", this.b.a("android.permission.ACCESS_NETWORK_STATE"));
        jSONObject.put("RECEIVE_BOOT_COMPLETED", this.b.a("android.permission.RECEIVE_BOOT_COMPLETED"));
        jSONObject.put("SYSTEM_ALERT_WINDOW", this.b.a("android.permission.SYSTEM_ALERT_WINDOW"));
        jSONObject.put("GET_ACCOUNTS", this.b.a("android.permission.GET_ACCOUNTS"));
        return jSONObject;
    }
}
