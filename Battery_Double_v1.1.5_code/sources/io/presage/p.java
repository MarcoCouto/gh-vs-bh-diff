package io.presage;

import android.app.Activity;
import android.content.Context;
import io.fabric.sdk.android.services.settings.SettingsJsonConstants;
import io.presage.mraid.browser.ShortcutActivity;
import io.presage.mraid.browser.ShortcutActivity.CamembertauCalvados;
import java.util.Map;

public final class p extends m {
    private final Activity b;
    private final RacletteSuisse c;
    private final RaclettedeSavoie d;
    private final RouedeBrie e;

    public /* synthetic */ p(Activity activity, RacletteSuisse racletteSuisse) {
        this(activity, racletteSuisse, RaclettedeSavoie.f6085a, RouedeBrie.f6089a);
    }

    private p(Activity activity, RacletteSuisse racletteSuisse, RaclettedeSavoie raclettedeSavoie, RouedeBrie rouedeBrie) {
        super(racletteSuisse);
        this.b = activity;
        this.c = racletteSuisse;
        this.d = raclettedeSavoie;
        this.e = rouedeBrie;
    }

    public final void a(boolean z) {
        Activity activity = this.b;
        if (!(activity instanceof RaclettedeSavoiefumee)) {
            activity = null;
        }
        RaclettedeSavoiefumee raclettedeSavoiefumee = (RaclettedeSavoiefumee) activity;
        if (raclettedeSavoiefumee != null) {
            if (z) {
                raclettedeSavoiefumee.a();
            } else {
                raclettedeSavoiefumee.b();
            }
        }
    }

    public final void a() {
        this.b.finish();
    }

    public final void b(String str) {
        if (this.b instanceof RaclettedeSavoiefumee) {
            ((RaclettedeSavoiefumee) this.b).a(str);
        }
    }

    public final void a(String str) {
        RaclettedeSavoie.a((Context) this.b, str);
    }

    public final void a(String str, String str2) {
        if (RaclettedeSavoie.c(this.b, str)) {
            this.c.b(str2, "{isResolved: true}");
        } else {
            this.c.b(str2, "{isResolved: false}");
        }
    }

    public final void b(String str, String str2) {
        RaclettedeSavoie.b((Context) this.b, str);
        this.c.b(str2, "{isStarted: true}");
        if (this.b instanceof ShortcutActivity) {
            this.b.finish();
        }
    }

    public final void a(Map<String, String> map, String str) {
        boolean z = false;
        if (str.length() > 0) {
            if (String.valueOf(map.get("name")).length() > 0) {
                if (String.valueOf(map.get(SettingsJsonConstants.APP_ICON_KEY)).length() > 0) {
                    z = true;
                }
                if (z) {
                    q qVar = new q(String.valueOf(map.get("id")), String.valueOf(map.get("name")), String.valueOf(map.get(SettingsJsonConstants.APP_ICON_KEY)), str);
                    CamembertauCalvados camembertauCalvados = ShortcutActivity.f6174a;
                    CamembertauCalvados.a(this.b, qVar);
                }
            }
        }
    }

    public final void a(CarreMirabelle carreMirabelle) {
        RouedeBrie.a((RomansPartDieu) new SaintFelicien(carreMirabelle.b(), carreMirabelle.e()));
    }
}
