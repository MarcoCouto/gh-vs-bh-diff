package io.presage;

import io.fabric.sdk.android.services.network.HttpRequest;
import java.util.Map;

public final class ag {

    /* renamed from: a reason: collision with root package name */
    private static final af f6114a = new CamembertauCalvados();

    public static final class CamembertauCalvados implements af {
        CamembertauCalvados() {
        }

        public final Map<String, String> a() {
            return cd.a();
        }
    }

    public static final boolean a(af afVar) {
        return df.a((Object) (String) afVar.a().get(HttpRequest.HEADER_CONTENT_ENCODING), (Object) HttpRequest.ENCODING_GZIP);
    }
}
