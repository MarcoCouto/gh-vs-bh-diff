package io.presage;

import android.webkit.WebView;

public final class PetitMorin {

    /* renamed from: a reason: collision with root package name */
    private as f6081a;

    public final void a(BriedeMelun briedeMelun, WebView webView) {
        ar arVar = ar.f6123a;
        if (ar.a() && briedeMelun.o()) {
            boolean p = briedeMelun.p();
            this.f6081a = new as();
            as asVar = this.f6081a;
            if (asVar != null) {
                asVar.a(webView, p);
            }
        }
    }

    public final void a() {
        ar arVar = ar.f6123a;
        if (ar.a()) {
            as asVar = this.f6081a;
            if (asVar != null) {
                asVar.a();
            }
        }
    }
}
