package io.presage;

import android.content.Context;
import io.presage.common.AdConfig;
import io.presage.interstitial.PresageInterstitialCallback;
import io.presage.interstitial.optinvideo.PresageOptinVideoCallback;

public final class ParmigianoReggiano extends OlivetCendre implements e {
    public static final CamembertauCalvados b = new CamembertauCalvados(0);
    private PresageOptinVideoCallback c;

    public static final class CamembertauCalvados {
        private CamembertauCalvados() {
        }

        public /* synthetic */ CamembertauCalvados(byte b) {
            this();
        }
    }

    public ParmigianoReggiano(Context context, AdConfig adConfig) {
        super(context, adConfig, "optin_video");
    }

    public final void a(PresageOptinVideoCallback presageOptinVideoCallback) {
        a((PresageInterstitialCallback) presageOptinVideoCallback);
        this.c = presageOptinVideoCallback;
    }

    public final void b(String str) {
        a(str);
    }

    public final void a(RomansPartDieu romansPartDieu) {
        super.a(romansPartDieu);
        if (romansPartDieu instanceof SaintFelicien) {
            PresageOptinVideoCallback presageOptinVideoCallback = this.c;
            if (presageOptinVideoCallback != null) {
                presageOptinVideoCallback.onAdRewarded(((SaintFelicien) romansPartDieu).c());
            }
        }
    }
}
