package io.presage;

import android.app.DownloadManager;
import android.app.DownloadManager.Request;
import android.content.Context;
import android.net.Uri;
import android.os.Environment;
import android.webkit.DownloadListener;
import android.widget.Toast;
import com.facebook.share.internal.ShareConstants;
import com.vungle.warren.ui.JavascriptBridge.MraidHandler;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public final class TommeMarcdeRaisin implements DownloadListener {

    /* renamed from: a reason: collision with root package name */
    private final Context f6103a;

    public TommeMarcdeRaisin(Context context) {
        this.f6103a = context;
    }

    public final void onDownloadStart(String str, String str2, String str3, String str4, long j) {
        String str5;
        if (MoelleuxduRevard.a(this.f6103a, "android.permission.WRITE_EXTERNAL_STORAGE")) {
            Uri parse = Uri.parse(str);
            df.a((Object) parse, ShareConstants.MEDIA_URI);
            String path = parse.getPath();
            df.a((Object) path, "uri.path");
            List b = new em("/").b(path);
            if (!b.isEmpty()) {
                str5 = (String) bn.d(b);
            } else {
                str5 = UUID.randomUUID().toString();
                df.a((Object) str5, "UUID.randomUUID().toString()");
            }
            Request request = new Request(parse);
            request.setTitle(str5);
            request.allowScanningByMediaScanner();
            request.setNotificationVisibility(1);
            request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, MraidHandler.DOWNLOAD_ACTION);
            Object systemService = this.f6103a.getSystemService(MraidHandler.DOWNLOAD_ACTION);
            if (systemService != null) {
                ((DownloadManager) systemService).enqueue(request);
                Context context = this.f6103a;
                dm dmVar = dm.f6156a;
                String format = String.format("Start downloading %s", Arrays.copyOf(new Object[]{str5}, 1));
                df.a((Object) format, "java.lang.String.format(format, *args)");
                Toast.makeText(context, format, 0).show();
                return;
            }
            throw new be("null cannot be cast to non-null type android.app.DownloadManager");
        }
    }
}
