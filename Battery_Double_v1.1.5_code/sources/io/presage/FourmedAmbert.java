package io.presage;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.tapjoy.TapjoyConstants;
import java.util.concurrent.TimeUnit;

public final class FourmedAmbert {

    /* renamed from: a reason: collision with root package name */
    public static final CamembertauCalvados f6057a = new CamembertauCalvados(0);
    /* access modifiers changed from: private */
    @SuppressLint({"StaticFieldLeak"})
    public static FourmedAmbert c;
    private final SharedPreferences b;

    public static final class CamembertauCalvados {
        private CamembertauCalvados() {
        }

        public /* synthetic */ CamembertauCalvados(byte b) {
            this();
        }

        public static FourmedAmbert a(Context context) {
            if (FourmedAmbert.c == null) {
                Context applicationContext = context.getApplicationContext();
                df.a((Object) applicationContext, "context.applicationContext");
                FourmedAmbert.c = new FourmedAmbert(applicationContext, 0);
            }
            FourmedAmbert j = FourmedAmbert.c;
            if (j == null) {
                df.a();
            }
            return j;
        }
    }

    private FourmedAmbert(Context context) {
        this.b = context.getSharedPreferences("profig", 0);
    }

    public /* synthetic */ FourmedAmbert(Context context, byte b2) {
        this(context);
    }

    public final void a(int i) {
        this.b.edit().putInt("numberOfProfigApiCalls", i).apply();
    }

    public final int a() {
        return this.b.getInt("numberOfProfigApiCalls", 0);
    }

    public final String b() {
        String string = this.b.getString("md5Profig", "");
        df.a((Object) string, "sharedPref.getString(MD5_PROFIG, \"\")");
        return string;
    }

    public final void a(String str) {
        this.b.edit().putString("md5Profig", str).apply();
    }

    public final void b(String str) {
        this.b.edit().putString("aaid", str).apply();
    }

    public final String c() {
        String string = this.b.getString("aaid", "");
        df.a((Object) string, "sharedPref.getString(AAID, \"\")");
        return string;
    }

    public final void c(String str) {
        this.b.edit().putString("fullProfigResponseJson", str).apply();
    }

    public final String d() {
        String string = this.b.getString("fullProfigResponseJson", "");
        df.a((Object) string, "sharedPref.getString(FUL…PROFIG_RESPONSE_JSON, \"\")");
        return string;
    }

    public final void e() {
        this.b.edit().putLong("numberOfDays", System.currentTimeMillis() / TimeUnit.DAYS.toMillis(1)).apply();
    }

    public final long f() {
        return this.b.getLong("numberOfDays", 0);
    }

    public final void d(String str) {
        this.b.edit().putString(RequestParameters.APPLICATION_VERSION_NAME, str).apply();
    }

    public final String g() {
        String string = this.b.getString(RequestParameters.APPLICATION_VERSION_NAME, Murol.a());
        df.a((Object) string, "sharedPref.getString(APP…ERSION, fullSdkVersion())");
        return string;
    }

    public final void a(long j) {
        this.b.edit().putLong("last_profig_sync", j).apply();
    }

    public final long h() {
        return this.b.getLong("last_profig_sync", 0);
    }

    public final void e(String str) {
        this.b.edit().putString(TapjoyConstants.TJC_API_KEY, str).apply();
    }

    public final String i() {
        String string = this.b.getString(TapjoyConstants.TJC_API_KEY, "");
        df.a((Object) string, "sharedPref.getString(API_KEY, \"\")");
        return string;
    }
}
