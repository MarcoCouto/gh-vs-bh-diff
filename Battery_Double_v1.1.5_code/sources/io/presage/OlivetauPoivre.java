package io.presage;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build.VERSION;
import android.webkit.WebSettings;
import android.webkit.WebView;
import com.ironsource.sdk.precache.DownloadManager;
import java.io.File;

public final class OlivetauPoivre {

    static final class CamembertauCalvados implements Runnable {

        /* renamed from: a reason: collision with root package name */
        final /* synthetic */ cw f6073a;

        CamembertauCalvados(cw cwVar) {
            this.f6073a = cwVar;
        }

        public final void run() {
            this.f6073a.a_();
        }
    }

    @SuppressLint({"SetJavaScriptEnabled"})
    public static final void a(WebView webView) {
        webView.getSettings().setAppCacheEnabled(true);
        Context context = webView.getContext();
        df.a((Object) context, "context");
        File cacheDir = context.getCacheDir();
        String absolutePath = cacheDir != null ? cacheDir.getAbsolutePath() : null;
        if (absolutePath != null) {
            webView.getSettings().setAppCachePath(absolutePath);
        }
        WebSettings settings = webView.getSettings();
        df.a((Object) settings, DownloadManager.SETTINGS);
        settings.setJavaScriptEnabled(true);
        WebSettings settings2 = webView.getSettings();
        df.a((Object) settings2, DownloadManager.SETTINGS);
        settings2.setDomStorageEnabled(true);
    }

    public static final void b(WebView webView) {
        webView.setHorizontalScrollBarEnabled(false);
        webView.setVerticalScrollBarEnabled(false);
        webView.setScrollContainer(false);
    }

    public static final void c(WebView webView) {
        if (VERSION.SDK_INT >= 17) {
            WebSettings settings = webView.getSettings();
            df.a((Object) settings, DownloadManager.SETTINGS);
            settings.setMediaPlaybackRequiresUserGesture(false);
        }
    }

    public static final void a(WebView webView, cw<bh> cwVar) {
        if (webView.getHeight() > 0 || webView.getWidth() > 0) {
            cwVar.a_();
        } else {
            webView.post(new CamembertauCalvados(cwVar));
        }
    }

    private static boolean e(WebView webView) {
        if (VERSION.SDK_INT >= 19) {
            return webView.isAttachedToWindow();
        }
        return webView.getParent() != null;
    }

    public static final void d(WebView webView) {
        if (!e(webView)) {
            webView.destroy();
        }
    }
}
