package io.presage;

public final class q {

    /* renamed from: a reason: collision with root package name */
    private final String f6181a;
    private final String b;
    private final String c;
    private final String d;

    public q(String str, String str2, String str3, String str4) {
        this.f6181a = str;
        this.b = str2;
        this.c = str3;
        this.d = str4;
    }

    public final String b() {
        return this.f6181a;
    }

    public final String c() {
        return this.b;
    }

    public final String d() {
        return this.c;
    }

    public final String e() {
        return this.d;
    }

    public final boolean a() {
        if (!(this.d.length() == 0)) {
            if (!(this.f6181a.length() == 0)) {
                return this.c.length() == 0;
            }
        }
    }
}
