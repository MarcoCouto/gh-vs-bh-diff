package io.presage;

import android.content.Context;
import com.smaato.sdk.core.api.VideoType;
import io.presage.common.AdConfig;
import io.presage.common.PresageSdk;
import io.presage.common.PresageSdkInitCallback;
import io.presage.interstitial.PresageInterstitialCallback;
import io.presage.interstitial.ui.InterstitialActivity;
import java.util.ArrayList;
import java.util.List;

public class OlivetCendre implements e {

    /* renamed from: a reason: collision with root package name */
    public static final CamembertauCalvados f6067a = new CamembertauCalvados(0);
    /* access modifiers changed from: private */
    public boolean b;
    private String c;
    private List<BriedeMelun> d;
    private PresageInterstitialCallback e;
    private PresageSdk f;
    private final Context g;
    private final AdConfig h;
    private final b i;
    private final FourmedeMontbrison j;
    private final AbbayedeCiteauxentiere k;
    private final RouedeBrie l;
    private final c m;
    private final BleudAuvergne n;
    private final Machecoulais o;
    private final Laguiole p;
    private final String q;

    static final class AbbayedeTamie extends dg implements cx<FourmedeHauteLoire, bh> {

        /* renamed from: a reason: collision with root package name */
        final /* synthetic */ OlivetCendre f6068a;

        AbbayedeTamie(OlivetCendre olivetCendre) {
            this.f6068a = olivetCendre;
            super(1);
        }

        public final /* bridge */ /* synthetic */ Object a(Object obj) {
            a((FourmedeHauteLoire) obj);
            return bh.f6130a;
        }

        private void a(FourmedeHauteLoire fourmedeHauteLoire) {
            this.f6068a.a(fourmedeHauteLoire);
        }
    }

    static final class AbbayedeTimadeuc extends dg implements cx<Throwable, bh> {

        /* renamed from: a reason: collision with root package name */
        final /* synthetic */ OlivetCendre f6069a;

        AbbayedeTimadeuc(OlivetCendre olivetCendre) {
            this.f6069a = olivetCendre;
            super(1);
        }

        public final /* bridge */ /* synthetic */ Object a(Object obj) {
            a();
            return bh.f6130a;
        }

        private void a() {
            PresageInterstitialCallback b = this.f6069a.b();
            if (b != null) {
                b.onAdError(0);
            }
        }
    }

    static final class AbbayeduMontdesCats extends dg implements cx<CapGrisNez, bh> {

        /* renamed from: a reason: collision with root package name */
        final /* synthetic */ OlivetCendre f6070a;

        AbbayeduMontdesCats(OlivetCendre olivetCendre) {
            this.f6070a = olivetCendre;
            super(1);
        }

        public final /* bridge */ /* synthetic */ Object a(Object obj) {
            a((CapGrisNez) obj);
            return bh.f6130a;
        }

        private void a(CapGrisNez capGrisNez) {
            if (!capGrisNez.a().isEmpty()) {
                PresageInterstitialCallback b = this.f6070a.b();
                if (b != null) {
                    b.onAdAvailable();
                }
                this.f6070a.a(bn.a(capGrisNez.a()));
                return;
            }
            PresageInterstitialCallback b2 = this.f6070a.b();
            if (b2 != null) {
                b2.onAdNotAvailable();
            }
        }
    }

    public static final class AffideliceauChablis implements d {

        /* renamed from: a reason: collision with root package name */
        final /* synthetic */ OlivetCendre f6071a;

        AffideliceauChablis(OlivetCendre olivetCendre) {
            this.f6071a = olivetCendre;
        }

        public final void a() {
            this.f6071a.b = true;
            PresageInterstitialCallback b = this.f6071a.b();
            if (b != null) {
                b.onAdLoaded();
            }
        }

        public final void b() {
            PresageInterstitialCallback b = this.f6071a.b();
            if (b != null) {
                b.onAdError(0);
            }
        }
    }

    static final /* synthetic */ class Appenzeller extends de implements cx<RomansPartDieu, bh> {
        Appenzeller(OlivetCendre olivetCendre) {
            super(1, olivetCendre);
        }

        public final ee a() {
            return dk.a(OlivetCendre.class);
        }

        public final String b() {
            return "sendShowEvent";
        }

        public final String c() {
            return "sendShowEvent$presage_interstitial_release(Lio/presage/mraid/MraidEvent;)V";
        }

        public final /* bridge */ /* synthetic */ Object a(Object obj) {
            a((RomansPartDieu) obj);
            return bh.f6130a;
        }

        private void a(RomansPartDieu romansPartDieu) {
            ((OlivetCendre) this.f6150a).a(romansPartDieu);
        }
    }

    public static final class CamembertauCalvados {
        private CamembertauCalvados() {
        }

        public /* synthetic */ CamembertauCalvados(byte b) {
            this();
        }
    }

    public static final class CamembertdeNormandie implements PresageSdkInitCallback {

        /* renamed from: a reason: collision with root package name */
        final /* synthetic */ OlivetCendre f6072a;

        CamembertdeNormandie(OlivetCendre olivetCendre) {
            this.f6072a = olivetCendre;
        }

        public final void onSdkNotInitialized() {
            PresageInterstitialCallback b = this.f6072a.b();
            if (b != null) {
                b.onAdError(5);
            }
        }

        public final void onSdkInitialized() {
            this.f6072a.h();
        }

        public final void onSdkInitFailed() {
            PresageInterstitialCallback b = this.f6072a.b();
            if (b != null) {
                b.onAdError(6);
            }
        }
    }

    static final /* synthetic */ class EcirdelAubrac extends de implements cw<FourmedeHauteLoire> {
        EcirdelAubrac(OlivetCendre olivetCendre) {
            super(0, olivetCendre);
        }

        public final ee a() {
            return dk.a(OlivetCendre.class);
        }

        public final String b() {
            return "getProfigAndSyncIfNeeded";
        }

        public final String c() {
            return "getProfigAndSyncIfNeeded()Lio/presage/common/profig/data/ProfigFullResponse;";
        }

        /* access modifiers changed from: private */
        /* renamed from: h */
        public FourmedeHauteLoire a_() {
            return ((OlivetCendre) this.f6150a).i();
        }
    }

    private OlivetCendre(Context context, AdConfig adConfig, b bVar, FourmedeMontbrison fourmedeMontbrison, AbbayedeCiteauxentiere abbayedeCiteauxentiere, RouedeBrie rouedeBrie, c cVar, BleudAuvergne bleudAuvergne, Machecoulais machecoulais, Laguiole laguiole, String str) {
        this.g = context;
        this.h = adConfig;
        this.i = bVar;
        this.j = fourmedeMontbrison;
        this.k = abbayedeCiteauxentiere;
        this.l = rouedeBrie;
        this.m = cVar;
        this.n = bleudAuvergne;
        this.o = machecoulais;
        this.p = laguiole;
        this.q = str;
        this.c = "";
        this.d = new ArrayList();
        this.f = PresageSdk.f6137a;
    }

    public final boolean a() {
        return this.b;
    }

    public final void a(String str) {
        this.c = str;
    }

    public final void a(PresageInterstitialCallback presageInterstitialCallback) {
        this.e = presageInterstitialCallback;
    }

    public final PresageInterstitialCallback b() {
        return this.e;
    }

    public /* synthetic */ OlivetCendre(Context context, AdConfig adConfig) {
        this(context, adConfig, VideoType.INTERSTITIAL);
    }

    public OlivetCendre(Context context, AdConfig adConfig, String str) {
        Context applicationContext = context.getApplicationContext();
        df.a((Object) applicationContext, "context.applicationContext");
        b bVar = b.f6126a;
        FourmedeMontbrison fourmedeMontbrison = FourmedeMontbrison.f6060a;
        AbbayedeCiteauxentiere abbayedeCiteauxentiere = AbbayedeCiteauxentiere.f5998a;
        RouedeBrie rouedeBrie = RouedeBrie.f6089a;
        io.presage.c.CamembertauCalvados camembertauCalvados = c.f6133a;
        this(applicationContext, adConfig, bVar, fourmedeMontbrison, abbayedeCiteauxentiere, rouedeBrie, io.presage.c.CamembertauCalvados.a(context), new BleudAuvergne(context), new Maroilles(), new Laguiole(), str);
    }

    public final void c() {
        PresageInterstitialCallback presageInterstitialCallback = this.e;
        if (presageInterstitialCallback != null) {
            presageInterstitialCallback.onAdError(4);
        }
    }

    private static boolean f() {
        return !PresageSdk.b();
    }

    public final void d() {
        if (!k()) {
            if (PresageSdk.a()) {
                g();
            } else if (f()) {
                PresageInterstitialCallback presageInterstitialCallback = this.e;
                if (presageInterstitialCallback != null) {
                    presageInterstitialCallback.onAdError(5);
                }
            } else {
                h();
            }
        }
    }

    private final void g() {
        this.f.addSdkInitCallback(new CamembertdeNormandie(this));
    }

    /* access modifiers changed from: private */
    public final void h() {
        b.a();
        io.presage.Aveyronnais.CamembertauCalvados camembertauCalvados = Aveyronnais.f6003a;
        io.presage.Aveyronnais.CamembertauCalvados.a(new EcirdelAubrac(this)).b((cx<? super T, bh>) new AbbayedeTamie<Object,bh>(this));
    }

    /* access modifiers changed from: private */
    public final FourmedeHauteLoire i() {
        FourmedeHauteLoire a2 = FourmedeMontbrison.a(this.g);
        if (a2 != null) {
            return a2;
        }
        FourmedeMontbrison.b(this.g);
        return FourmedeMontbrison.a(this.g);
    }

    /* access modifiers changed from: private */
    public final void a(FourmedeHauteLoire fourmedeHauteLoire) {
        if (fourmedeHauteLoire == null) {
            PresageInterstitialCallback presageInterstitialCallback = this.e;
            if (presageInterstitialCallback != null) {
                presageInterstitialCallback.onAdError(3);
            }
        } else if (!fourmedeHauteLoire.b()) {
            PresageInterstitialCallback presageInterstitialCallback2 = this.e;
            if (presageInterstitialCallback2 != null) {
                presageInterstitialCallback2.onAdError(2);
            }
        } else {
            AbbayedeCiteauxentiere.a((VacherinSuisse) new Abondance("LOAD"));
            this.n.a(this.q, this.h, this.c).a((cx<? super Throwable, bh>) new AbbayedeTimadeuc<Object,bh>(this)).b((cx<? super T, bh>) new AbbayeduMontdesCats<Object,bh>(this));
        }
    }

    /* access modifiers changed from: private */
    public final void a(List<BriedeMelun> list) {
        boolean z;
        for (BriedeMelun c2 : list) {
            if (c2.c().length() == 0) {
                z = true;
                continue;
            } else {
                z = false;
                continue;
            }
            if (z) {
                PresageInterstitialCallback presageInterstitialCallback = this.e;
                if (presageInterstitialCallback != null) {
                    presageInterstitialCallback.onAdNotLoaded();
                    return;
                }
                return;
            }
        }
        this.d = bn.a(list);
        this.m.a(this, list, new AffideliceauChablis(this));
    }

    public final void a(BriedeMelun briedeMelun) {
        this.b = false;
        this.d.remove(briedeMelun);
    }

    public final void e() {
        b.a();
        if (j() && !k()) {
            AbbayedeCiteauxentiere.a((VacherinSuisse) new Abondance("SHOW"));
            this.b = false;
            List<BriedeMelun> list = this.d;
            for (BriedeMelun b2 : list) {
                RouedeBrie.a(b2.b(), new Appenzeller(this));
            }
            io.presage.interstitial.ui.InterstitialActivity.CamembertauCalvados camembertauCalvados = InterstitialActivity.f6169a;
            io.presage.interstitial.ui.InterstitialActivity.CamembertauCalvados.a(this.g, list);
        }
    }

    private final boolean j() {
        if (f()) {
            PresageInterstitialCallback presageInterstitialCallback = this.e;
            if (presageInterstitialCallback != null) {
                presageInterstitialCallback.onAdError(5);
            }
            return false;
        }
        FourmedeHauteLoire a2 = FourmedeMontbrison.a(this.g);
        if (!this.b || this.d.isEmpty()) {
            PresageInterstitialCallback presageInterstitialCallback2 = this.e;
            if (presageInterstitialCallback2 != null) {
                presageInterstitialCallback2.onAdNotLoaded();
            }
            return false;
        } else if (a2 == null) {
            PresageInterstitialCallback presageInterstitialCallback3 = this.e;
            if (presageInterstitialCallback3 != null) {
                presageInterstitialCallback3.onAdError(3);
            }
            return false;
        } else if (!a2.b()) {
            PresageInterstitialCallback presageInterstitialCallback4 = this.e;
            if (presageInterstitialCallback4 != null) {
                presageInterstitialCallback4.onAdError(2);
            }
            return false;
        } else if (!Laguiole.a(this.g)) {
            return true;
        } else {
            PresageInterstitialCallback presageInterstitialCallback5 = this.e;
            if (presageInterstitialCallback5 != null) {
                presageInterstitialCallback5.onAdError(7);
            }
            return false;
        }
    }

    private final boolean k() {
        if (this.o.a(this.g)) {
            return false;
        }
        PresageInterstitialCallback presageInterstitialCallback = this.e;
        if (presageInterstitialCallback != null) {
            presageInterstitialCallback.onAdError(1);
        }
        return true;
    }

    public void a(RomansPartDieu romansPartDieu) {
        if (df.a((Object) romansPartDieu.b(), (Object) "impression")) {
            PresageInterstitialCallback presageInterstitialCallback = this.e;
            if (presageInterstitialCallback != null) {
                presageInterstitialCallback.onAdDisplayed();
            }
            return;
        }
        if (df.a((Object) romansPartDieu.b(), (Object) "adClosed")) {
            PresageInterstitialCallback presageInterstitialCallback2 = this.e;
            if (presageInterstitialCallback2 != null) {
                presageInterstitialCallback2.onAdClosed();
            }
        }
    }
}
