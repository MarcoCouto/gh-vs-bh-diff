package io.presage;

import android.content.Context;
import android.webkit.WebResourceResponse;
import io.presage.CendreduBeauzac.CamembertauCalvados;
import java.io.ByteArrayInputStream;
import java.nio.charset.Charset;

public final class SableduBoulonnais {

    /* renamed from: a reason: collision with root package name */
    public static final SableduBoulonnais f6090a = new SableduBoulonnais();

    private SableduBoulonnais() {
    }

    private static WebResourceResponse a(String str, String str2) {
        StringBuilder sb = new StringBuilder("javascript:");
        sb.append(str2);
        sb.append(str);
        String sb2 = sb.toString();
        String str3 = "text/javascript";
        String str4 = "UTF-8";
        Charset charset = el.f6159a;
        if (sb2 != null) {
            byte[] bytes = sb2.getBytes(charset);
            df.a((Object) bytes, "(this as java.lang.String).getBytes(charset)");
            return new WebResourceResponse(str3, str4, new ByteArrayInputStream(bytes));
        }
        throw new be("null cannot be cast to non-null type java.lang.String");
    }

    public static WebResourceResponse a(Context context, CarreMirabelle carreMirabelle) {
        new RegaldeBourgogne();
        String a2 = RegaldeBourgogne.a(carreMirabelle);
        CamembertauCalvados camembertauCalvados = CendreduBeauzac.f6036a;
        String b = CamembertauCalvados.a(context).b();
        if (b.length() > 0) {
            return a(b, a2);
        }
        return null;
    }
}
