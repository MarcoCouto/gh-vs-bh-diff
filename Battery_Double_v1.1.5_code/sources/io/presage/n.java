package io.presage;

public final class n implements SaintNectaire {

    /* renamed from: a reason: collision with root package name */
    private final SaintNectaire[] f6179a;

    public n(SaintNectaire[] saintNectaireArr) {
        this.f6179a = saintNectaireArr;
    }

    public final boolean a(String str, x xVar, CarreMirabelle carreMirabelle) {
        for (SaintNectaire a2 : this.f6179a) {
            if (a2.a(str, xVar, carreMirabelle)) {
                return true;
            }
        }
        return false;
    }
}
