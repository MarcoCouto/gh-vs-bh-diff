package io.presage;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

public final class RouedeBrie {

    /* renamed from: a reason: collision with root package name */
    public static final RouedeBrie f6089a = new RouedeBrie();
    private static final Map<String, cx<RomansPartDieu, bh>> b = Collections.synchronizedMap(new LinkedHashMap());

    private RouedeBrie() {
    }

    public static void a(String str, cx<? super RomansPartDieu, bh> cxVar) {
        Map<String, cx<RomansPartDieu, bh>> map = b;
        df.a((Object) map, "listeners");
        map.put(str, cxVar);
    }

    public static void a(RomansPartDieu romansPartDieu) {
        cx cxVar = (cx) b.get(romansPartDieu.a());
        if (cxVar != null) {
            cxVar.a(romansPartDieu);
        }
    }

    public static void a(String str) {
        b.remove(str);
    }
}
