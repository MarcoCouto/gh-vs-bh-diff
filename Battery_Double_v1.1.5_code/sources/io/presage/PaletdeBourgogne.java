package io.presage;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.webkit.WebSettings;
import android.webkit.WebView;

public final class PaletdeBourgogne {

    /* renamed from: a reason: collision with root package name */
    public static final CamembertauCalvados f6074a = new CamembertauCalvados(0);
    private final Context b;
    private x c;
    private Bofavre d;
    /* access modifiers changed from: private */
    public boolean e;
    private boolean f;
    private AbbayedeCiteauxentiere g;
    private RouedeBrie h;
    private final b i;
    private final SaintNectaire j;
    private final o k;
    private final t l;

    public static final class CamembertauCalvados {
        private CamembertauCalvados() {
        }

        public /* synthetic */ CamembertauCalvados(byte b) {
            this();
        }

        public static PaletdeBourgogne a(Activity activity, StRomans stRomans) {
            Context context = activity;
            b bVar = b.f6126a;
            SaintNectaire a2 = stRomans.a();
            o oVar = new o(activity);
            io.presage.t.CamembertauCalvados camembertauCalvados = t.f6183a;
            PaletdeBourgogne paletdeBourgogne = new PaletdeBourgogne(context, bVar, a2, oVar, io.presage.t.CamembertauCalvados.a(context), 0);
            return paletdeBourgogne;
        }
    }

    public static final class CamembertdeNormandie extends y {

        /* renamed from: a reason: collision with root package name */
        final /* synthetic */ PaletdeBourgogne f6075a;
        final /* synthetic */ BriedeMelun b;

        CamembertdeNormandie(PaletdeBourgogne paletdeBourgogne, BriedeMelun briedeMelun) {
            this.f6075a = paletdeBourgogne;
            this.b = briedeMelun;
        }

        public final void a(WebView webView, String str) {
            if (!this.f6075a.e) {
                this.f6075a.e = true;
                this.f6075a.c(this.b);
            }
        }

        public final void a(WebView webView) {
            this.f6075a.b();
        }
    }

    private PaletdeBourgogne(Context context, b bVar, SaintNectaire saintNectaire, o oVar, t tVar) {
        this.i = bVar;
        this.j = saintNectaire;
        this.k = oVar;
        this.l = tVar;
        this.b = context.getApplicationContext();
        BoulettedAvesnes boulettedAvesnes = BoulettedAvesnes.f6017a;
        this.d = BoulettedAvesnes.a(context);
        this.g = AbbayedeCiteauxentiere.f5998a;
        this.h = RouedeBrie.f6089a;
    }

    public /* synthetic */ PaletdeBourgogne(Context context, b bVar, SaintNectaire saintNectaire, o oVar, t tVar, byte b2) {
        this(context, bVar, saintNectaire, oVar, tVar);
    }

    public final x a(BriedeMelun briedeMelun) {
        b(briedeMelun);
        a();
        a(this.c, briedeMelun);
        if (!this.f) {
            x xVar = this.c;
            if (xVar != null) {
                z.a(xVar, briedeMelun);
            }
        } else {
            this.e = true;
            b();
            c(briedeMelun);
        }
        return this.c;
    }

    private final void b(BriedeMelun briedeMelun) {
        x b2 = b.b(briedeMelun.a());
        this.f = b2 != null;
        if (b2 == null) {
            Context context = this.b;
            df.a((Object) context, "context");
            b2 = z.a(context, briedeMelun);
        }
        if (b2 != null) {
            this.c = b2;
        }
    }

    @SuppressLint({"SetJavaScriptEnabled"})
    private final void a() {
        x xVar = this.c;
        if (xVar != null) {
            xVar.setMraidUrlHandler(new n(new SaintNectaire[]{this.j, this.k.a(xVar)}));
            WebSettings settings = xVar.getSettings();
            if (settings != null) {
                settings.setJavaScriptEnabled(true);
            }
            WebView webView = xVar;
            OlivetauPoivre.c(webView);
            OlivetauPoivre.b(webView);
        }
    }

    private final void a(x xVar, BriedeMelun briedeMelun) {
        if (xVar != null) {
            xVar.setClientAdapter(new CamembertdeNormandie(this, briedeMelun));
        }
    }

    /* access modifiers changed from: private */
    public final void b() {
        if (this.c != null) {
            t tVar = this.l;
            x xVar = this.c;
            if (xVar == null) {
                df.a();
            }
            tVar.a(xVar);
        }
    }

    /* access modifiers changed from: private */
    public final void c(BriedeMelun briedeMelun) {
        if (briedeMelun.e().length() > 0) {
            d(briedeMelun);
        } else {
            AbbayedeCiteauxentiere.a((VacherinSuisse) new Bethmale("shown", briedeMelun));
        }
        RouedeBrie.a(new RomansPartDieu(briedeMelun.b(), "impression"));
    }

    private final void d(BriedeMelun briedeMelun) {
        this.d.b(briedeMelun.e());
    }
}
