package io.presage;

import io.fabric.sdk.android.services.network.HttpRequest;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.util.Map.Entry;

public final class ah implements aa {

    /* renamed from: a reason: collision with root package name */
    private final al f6115a;
    private final ac b;

    public ah(al alVar, ac acVar) {
        this.f6115a = alVar;
        this.b = acVar;
    }

    public final am a() {
        try {
            HttpURLConnection a2 = a(new URL(this.f6115a.a()));
            b(a2);
            c(a2);
            int responseCode = a2.getResponseCode();
            if (ai.a(responseCode)) {
                return a(a2);
            }
            return new ad(new ak(responseCode));
        } catch (Exception e) {
            return new ad(e);
        }
    }

    private static an a(HttpURLConnection httpURLConnection) {
        if (httpURLConnection.getContentLength() == 0) {
            return new an("");
        }
        InputStream inputStream = httpURLConnection.getInputStream();
        df.a((Object) inputStream, "inputStream");
        byte[] a2 = cm.a(inputStream);
        inputStream.close();
        String headerField = httpURLConnection.getHeaderField("content-encoding");
        if (headerField != null) {
            String lowerCase = headerField.toLowerCase();
            df.a((Object) lowerCase, "(this as java.lang.String).toLowerCase()");
            if (df.a((Object) lowerCase, (Object) HttpRequest.ENCODING_GZIP)) {
                ae aeVar = ae.f6113a;
                return new an(ae.a(a2));
            }
        }
        return new an(new String(a2, el.f6159a));
    }

    private final void b(HttpURLConnection httpURLConnection) {
        for (Entry entry : this.f6115a.d().a().entrySet()) {
            httpURLConnection.setRequestProperty((String) entry.getKey(), (String) entry.getValue());
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:27:0x0060  */
    private final void c(HttpURLConnection httpURLConnection) {
        OutputStream outputStream;
        Throwable th;
        byte[] bArr;
        if (this.f6115a.c().length() > 0) {
            try {
                outputStream = httpURLConnection.getOutputStream();
                try {
                    if (ag.a(this.f6115a.d())) {
                        ae aeVar = ae.f6113a;
                        bArr = ae.a(this.f6115a.c());
                    } else {
                        String c = this.f6115a.c();
                        Charset charset = el.f6159a;
                        if (c != null) {
                            bArr = c.getBytes(charset);
                            df.a((Object) bArr, "(this as java.lang.String).getBytes(charset)");
                        } else {
                            throw new be("null cannot be cast to non-null type java.lang.String");
                        }
                    }
                    outputStream.write(bArr);
                    if (outputStream != null) {
                        ab.a(outputStream);
                    }
                } catch (Throwable th2) {
                    th = th2;
                    if (outputStream != null) {
                        ab.a(outputStream);
                    }
                    throw th;
                }
            } catch (Throwable th3) {
                th = th3;
                outputStream = null;
                if (outputStream != null) {
                }
                throw th;
            }
        }
    }

    private final HttpURLConnection a(URL url) throws IOException {
        URLConnection openConnection = url.openConnection();
        if (openConnection != null) {
            HttpURLConnection httpURLConnection = (HttpURLConnection) openConnection;
            httpURLConnection.setRequestProperty("Connection", "close");
            httpURLConnection.setReadTimeout(this.b.a());
            httpURLConnection.setConnectTimeout(this.b.b());
            httpURLConnection.setRequestMethod(this.f6115a.b());
            httpURLConnection.setDoOutput(this.f6115a.c().length() > 0);
            return httpURLConnection;
        }
        throw new be("null cannot be cast to non-null type java.net.HttpURLConnection");
    }
}
