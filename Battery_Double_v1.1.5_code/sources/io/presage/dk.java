package io.presage;

public final class dk {

    /* renamed from: a reason: collision with root package name */
    private static final dl f6155a;
    private static final ed[] b = new ed[0];

    static {
        dl dlVar = null;
        try {
            dlVar = (dl) Class.forName("kotlin.reflect.jvm.internal.ReflectionFactoryImpl").newInstance();
        } catch (ClassCastException | ClassNotFoundException | IllegalAccessException | InstantiationException unused) {
        }
        if (dlVar == null) {
            dlVar = new dl();
        }
        f6155a = dlVar;
    }

    public static ed a(Class cls) {
        return dl.a(cls);
    }

    public static String a(dg dgVar) {
        return dl.a(dgVar);
    }

    public static ef a(de deVar) {
        return dl.a(deVar);
    }

    public static eh a(di diVar) {
        return dl.a(diVar);
    }
}
