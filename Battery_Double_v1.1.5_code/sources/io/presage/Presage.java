package io.presage;

import android.content.Context;
import io.presage.ads.Ads;
import io.presage.core.Core;

public class Presage {
    private static Presage IIIIIIII;

    private Presage() {
    }

    public static Presage getInstance() {
        if (IIIIIIII == null) {
            IIIIIIII = new Presage();
        }
        return IIIIIIII;
    }

    public void start(String str, Context context) {
        Ads.initialize(context, str);
        Core.initialize(context, str);
    }
}
