package io.presage;

import android.util.Log;

public final class aq {

    /* renamed from: a reason: collision with root package name */
    public static final aq f6122a = new aq();
    private static boolean b;

    private aq() {
    }

    public static void a(Throwable th) {
        if (b) {
            Log.e("omid", "caught_error", th);
        }
    }
}
