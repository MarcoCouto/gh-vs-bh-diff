package io.presage;

import android.content.Context;
import io.presage.BleudeSassenage.CamembertauCalvados;

public final class BoulettedAvesnes {

    /* renamed from: a reason: collision with root package name */
    public static final BoulettedAvesnes f6017a = new BoulettedAvesnes();
    private static Bofavre b;

    private BoulettedAvesnes() {
    }

    public static Bofavre a(Context context) {
        if (b == null) {
            Context applicationContext = context.getApplicationContext();
            CamembertauCalvados camembertauCalvados = BleudeSassenage.f6016a;
            df.a((Object) applicationContext, "appContext");
            b = new Bouyssou(CamembertauCalvados.a(applicationContext), new CoeurdArras(applicationContext), new Cheddar(applicationContext), new aj(new BleudeGex(FourmedeMontbrison.f6060a, applicationContext, GaletteLyonnaise.a(3))));
        }
        Bofavre bofavre = b;
        if (bofavre == null) {
            df.a();
        }
        return bofavre;
    }
}
