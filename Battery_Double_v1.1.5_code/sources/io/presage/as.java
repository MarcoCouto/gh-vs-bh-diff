package io.presage;

import android.webkit.WebView;
import com.iab.omid.library.oguryco.adsession.AdSession;

public final class as {

    /* renamed from: a reason: collision with root package name */
    private AdSession f6124a;
    private at b = new at();

    public final void a(WebView webView, boolean z) {
        this.f6124a = at.a(webView, z);
        AdSession adSession = this.f6124a;
        if (adSession != null) {
            adSession.start();
        }
    }

    public final void a() {
        AdSession adSession = this.f6124a;
        if (adSession != null) {
            adSession.finish();
        }
    }
}
