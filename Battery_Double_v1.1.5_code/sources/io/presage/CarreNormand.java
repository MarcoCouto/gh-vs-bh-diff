package io.presage;

public final class CarreNormand {

    /* renamed from: a reason: collision with root package name */
    public static final CarreNormand f6035a = new CarreNormand();

    private CarreNormand() {
    }

    public static boolean a() {
        try {
            Class.forName("com.moat.analytics.mobile.ogury.MoatAnalytics");
            return true;
        } catch (Exception unused) {
            return false;
        }
    }

    public static boolean b() {
        try {
            Class.forName("com.iab.omid.library.oguryco.Omid");
            return true;
        } catch (Exception unused) {
            return false;
        }
    }
}
