package io.presage;

public final class dl {
    public static ef a(de deVar) {
        return deVar;
    }

    public static eh a(di diVar) {
        return diVar;
    }

    public static ed a(Class cls) {
        return new da(cls);
    }

    public static String a(dg dgVar) {
        return a((dd) dgVar);
    }

    private static String a(dd ddVar) {
        String obj = ddVar.getClass().getGenericInterfaces()[0].toString();
        return obj.startsWith("kotlin.jvm.functions.") ? obj.substring(21) : obj;
    }
}
