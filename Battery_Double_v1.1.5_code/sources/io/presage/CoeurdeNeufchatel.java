package io.presage;

import com.google.android.exoplayer2.C;
import com.integralads.avid.library.inmobi.utils.AvidJSONUtil;
import com.mintegral.msdk.base.utils.CommonMD5;
import java.math.BigInteger;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

public final class CoeurdeNeufchatel {

    /* renamed from: a reason: collision with root package name */
    public static final CoeurdeNeufchatel f6040a = new CoeurdeNeufchatel();

    private CoeurdeNeufchatel() {
    }

    public static String a(String str) {
        try {
            MessageDigest instance = MessageDigest.getInstance(CommonMD5.TAG);
            df.a((Object) instance, "MessageDigest.getInstance(\"MD5\")");
            Charset forName = Charset.forName(C.ASCII_NAME);
            df.a((Object) forName, "Charset.forName(\"US-ASCII\")");
            byte[] bytes = str.getBytes(forName);
            df.a((Object) bytes, "(this as java.lang.String).getBytes(charset)");
            instance.update(bytes, 0, str.length());
            byte[] digest = instance.digest();
            BigInteger bigInteger = new BigInteger(1, digest);
            dm dmVar = dm.f6156a;
            StringBuilder sb = new StringBuilder("%0");
            sb.append(digest.length << 1);
            sb.append(AvidJSONUtil.KEY_X);
            String format = String.format(sb.toString(), Arrays.copyOf(new Object[]{bigInteger}, 1));
            df.a((Object) format, "java.lang.String.format(format, *args)");
            return format;
        } catch (NoSuchAlgorithmException e) {
            BurratadesPouilles burratadesPouilles = BurratadesPouilles.f6029a;
            BurratadesPouilles.a(e);
            return "";
        }
    }
}
