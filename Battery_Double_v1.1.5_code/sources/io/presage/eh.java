package io.presage;

public interface eh<T, R> extends cx<T, R>, eg<R> {

    public interface CamembertauCalvados<T, R> extends cx<T, R>, io.presage.eg.CamembertauCalvados<R> {
    }

    R b(T t);

    CamembertauCalvados<T, R> i();
}
