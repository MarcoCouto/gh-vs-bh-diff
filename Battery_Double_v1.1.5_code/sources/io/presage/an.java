package io.presage;

public final class an extends am {

    /* renamed from: a reason: collision with root package name */
    private final String f6119a;

    /* JADX INFO: used method not loaded: io.presage.df.a(java.lang.Object, java.lang.Object):null, types can be incorrect */
    /* JADX WARNING: Code restructure failed: missing block: B:4:0x0010, code lost:
        if (io.presage.df.a((java.lang.Object) r1.f6119a, (java.lang.Object) ((io.presage.an) r2).f6119a) != false) goto L_0x0015;
     */
    public final boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof an) {
            }
            return false;
        }
        return true;
    }

    public final int hashCode() {
        String str = this.f6119a;
        if (str != null) {
            return str.hashCode();
        }
        return 0;
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder("SuccessResponse(responseBody=");
        sb.append(this.f6119a);
        sb.append(")");
        return sb.toString();
    }

    public an(String str) {
        super(0);
        this.f6119a = str;
    }

    public final String a() {
        return this.f6119a;
    }
}
