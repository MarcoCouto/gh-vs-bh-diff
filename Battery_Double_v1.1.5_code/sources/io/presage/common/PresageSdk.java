package io.presage.common;

import android.content.Context;
import android.util.Log;
import com.ogury.crashreport.CrashConfig;
import com.ogury.crashreport.CrashReport;
import com.ogury.crashreport.SdkInfo;
import io.presage.AbbayedeCiteauxentiere;
import io.presage.Aveyronnais;
import io.presage.Comte18mois;
import io.presage.FourmedAmbert;
import io.presage.FourmedeHauteLoire;
import io.presage.FourmedeMontbrison;
import io.presage.Morbier;
import io.presage.Munster;
import io.presage.ar;
import io.presage.bh;
import io.presage.cw;
import io.presage.cx;
import io.presage.df;
import io.presage.dg;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public final class PresageSdk {

    /* renamed from: a reason: collision with root package name */
    public static final PresageSdk f6137a = new PresageSdk();
    /* access modifiers changed from: private */
    public static int b;
    private static List<PresageSdkInitCallback> c;

    static final class AbbayedeTamie extends dg implements cw<FourmedeHauteLoire> {

        /* renamed from: a reason: collision with root package name */
        final /* synthetic */ Context f6138a;

        AbbayedeTamie(Context context) {
            this.f6138a = context;
            super(0);
        }

        /* access modifiers changed from: private */
        /* renamed from: b */
        public FourmedeHauteLoire a_() {
            FourmedeMontbrison fourmedeMontbrison = FourmedeMontbrison.f6060a;
            return FourmedeMontbrison.a(this.f6138a);
        }
    }

    static final class AbbayedeTimadeuc extends dg implements cx<FourmedeHauteLoire, bh> {

        /* renamed from: a reason: collision with root package name */
        final /* synthetic */ Context f6139a;

        AbbayedeTimadeuc(Context context) {
            this.f6139a = context;
            super(1);
        }

        public final /* bridge */ /* synthetic */ Object a(Object obj) {
            a((FourmedeHauteLoire) obj);
            return bh.f6130a;
        }

        private void a(FourmedeHauteLoire fourmedeHauteLoire) {
            PresageSdk presageSdk = PresageSdk.f6137a;
            PresageSdk.b(fourmedeHauteLoire, this.f6139a);
        }
    }

    static final class CamembertauCalvados extends dg implements cw<bh> {

        /* renamed from: a reason: collision with root package name */
        final /* synthetic */ Context f6140a;
        final /* synthetic */ String b;

        CamembertauCalvados(Context context, String str) {
            this.f6140a = context;
            this.b = str;
            super(0);
        }

        public final /* synthetic */ Object a_() {
            b();
            return bh.f6130a;
        }

        private void b() {
            io.presage.FourmedAmbert.CamembertauCalvados camembertauCalvados = FourmedAmbert.f6057a;
            io.presage.FourmedAmbert.CamembertauCalvados.a(this.f6140a).e(this.b);
            PresageSdk presageSdk = PresageSdk.f6137a;
            PresageSdk.init(this.f6140a);
        }
    }

    static final class CamembertdeNormandie extends dg implements cx<Throwable, bh> {

        /* renamed from: a reason: collision with root package name */
        public static final CamembertdeNormandie f6141a = new CamembertdeNormandie();

        CamembertdeNormandie() {
            super(1);
        }

        public final /* bridge */ /* synthetic */ Object a(Object obj) {
            a();
            return bh.f6130a;
        }

        private static void a() {
            PresageSdk presageSdk = PresageSdk.f6137a;
            PresageSdk.b = 0;
            PresageSdk presageSdk2 = PresageSdk.f6137a;
            PresageSdk.g();
        }
    }

    static final class EcirdelAubrac extends dg implements cw<bh> {

        /* renamed from: a reason: collision with root package name */
        final /* synthetic */ Context f6142a;

        EcirdelAubrac(Context context) {
            this.f6142a = context;
            super(0);
        }

        public final /* synthetic */ Object a_() {
            b();
            return bh.f6130a;
        }

        private void b() {
            PresageSdk presageSdk = PresageSdk.f6137a;
            PresageSdk.b = 1;
            PresageSdk presageSdk2 = PresageSdk.f6137a;
            PresageSdk.f();
            PresageSdk presageSdk3 = PresageSdk.f6137a;
            PresageSdk.c(this.f6142a);
        }
    }

    public static final String getAdsSdkVersion() {
        return "3.2.2-moat";
    }

    static {
        List<PresageSdkInitCallback> synchronizedList = Collections.synchronizedList(new LinkedList());
        df.a((Object) synchronizedList, "Collections.synchronizedList(LinkedList())");
        c = synchronizedList;
    }

    private PresageSdk() {
    }

    public static final void init(Context context, String str) {
        if (b == 0) {
            b = 2;
            CharSequence charSequence = str;
            if (!(charSequence == null || charSequence.length() == 0)) {
                io.presage.EcirdelAubrac.CamembertauCalvados camembertauCalvados = io.presage.EcirdelAubrac.f6049a;
                io.presage.EcirdelAubrac.CamembertauCalvados.a(new CamembertauCalvados(context, str)).a((cx<? super Throwable, bh>) CamembertdeNormandie.f6141a).a((cw<bh>) new EcirdelAubrac<bh>(context));
                return;
            }
            Log.e("Presage", "PresageSdk.init() error", new IllegalArgumentException("The api key is null empty. Please provide a valid api key"));
            b = 0;
        }
    }

    public static boolean a() {
        return b == 2;
    }

    public static boolean b() {
        return b == 1;
    }

    private static boolean e() {
        return b == 0;
    }

    public final void addSdkInitCallback(PresageSdkInitCallback presageSdkInitCallback) {
        if (b()) {
            presageSdkInitCallback.onSdkInitialized();
        } else if (a()) {
            c.add(presageSdkInitCallback);
        } else {
            if (e()) {
                presageSdkInitCallback.onSdkNotInitialized();
            }
        }
    }

    /* access modifiers changed from: private */
    public static void init(Context context) {
        Munster.a("PresageSdk.getInstance");
        if (!d(context)) {
            Context applicationContext = context.getApplicationContext();
            io.presage.FourmedAmbert.CamembertauCalvados camembertauCalvados = FourmedAmbert.f6057a;
            df.a((Object) applicationContext, "appContext");
            FourmedAmbert a2 = io.presage.FourmedAmbert.CamembertauCalvados.a(applicationContext);
            String i = a2.i();
            if (!df.a((Object) i, (Object) "")) {
                Comte18mois.f6041a.a(applicationContext).b(false);
                AbbayedeCiteauxentiere abbayedeCiteauxentiere = AbbayedeCiteauxentiere.f5998a;
                AbbayedeCiteauxentiere.a(context);
                FourmedeMontbrison fourmedeMontbrison = FourmedeMontbrison.f6060a;
                FourmedeHauteLoire a3 = FourmedeMontbrison.a(context);
                if (a3 != null) {
                    try {
                        CrashReport.register(applicationContext, new SdkInfo(getAdsSdkVersion(), i, a2.c()), new CrashConfig(a3.o(), applicationContext.getPackageName()));
                        return;
                    } catch (Throwable unused) {
                    }
                }
                return;
            }
            Throwable illegalStateException = new IllegalStateException("There is no api key. Please call PresageSdk.init(context, apiKey) before trying to load or display an ad");
            Log.e("Presage", "Init Error", illegalStateException);
            throw illegalStateException;
        }
        throw new IllegalStateException("The app is not in main application process");
    }

    /* access modifiers changed from: private */
    public static void c(Context context) {
        io.presage.Aveyronnais.CamembertauCalvados camembertauCalvados = Aveyronnais.f6003a;
        io.presage.Aveyronnais.CamembertauCalvados.a(new AbbayedeTamie(context)).b((cx<? super T, bh>) new AbbayedeTimadeuc<Object,bh>(context));
    }

    /* access modifiers changed from: private */
    public static void b(FourmedeHauteLoire fourmedeHauteLoire, Context context) {
        if (fourmedeHauteLoire != null && fourmedeHauteLoire.d()) {
            ar arVar = ar.f6123a;
            ar.a(context);
        }
    }

    private static boolean d(Context context) {
        return !b() && !Morbier.a(context);
    }

    /* access modifiers changed from: private */
    public static void f() {
        for (PresageSdkInitCallback onSdkInitialized : c) {
            onSdkInitialized.onSdkInitialized();
        }
        c.clear();
    }

    /* access modifiers changed from: private */
    public static void g() {
        for (PresageSdkInitCallback onSdkInitFailed : c) {
            onSdkInitFailed.onSdkInitFailed();
        }
        c.clear();
    }
}
