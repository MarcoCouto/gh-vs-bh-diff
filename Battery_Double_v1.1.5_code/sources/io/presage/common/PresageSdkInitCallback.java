package io.presage.common;

public interface PresageSdkInitCallback {
    void onSdkInitFailed();

    void onSdkInitialized();

    void onSdkNotInitialized();
}
