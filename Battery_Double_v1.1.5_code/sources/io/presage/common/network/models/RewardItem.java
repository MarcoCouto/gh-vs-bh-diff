package io.presage.common.network.models;

import io.presage.df;
import java.io.Serializable;

public final class RewardItem implements Serializable {

    /* renamed from: a reason: collision with root package name */
    private String f6144a;
    private String b;

    public static /* synthetic */ RewardItem copy$default(RewardItem rewardItem, String str, String str2, int i, Object obj) {
        if ((i & 1) != 0) {
            str = rewardItem.f6144a;
        }
        if ((i & 2) != 0) {
            str2 = rewardItem.b;
        }
        return rewardItem.copy(str, str2);
    }

    public final String component1() {
        return this.f6144a;
    }

    public final String component2() {
        return this.b;
    }

    public final RewardItem copy(String str, String str2) {
        return new RewardItem(str, str2);
    }

    /* JADX INFO: used method not loaded: io.presage.df.a(java.lang.Object, java.lang.Object):null, types can be incorrect */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x001a, code lost:
        if (io.presage.df.a((java.lang.Object) r2.b, (java.lang.Object) r3.b) != false) goto L_0x001f;
     */
    public final boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof RewardItem) {
                RewardItem rewardItem = (RewardItem) obj;
                if (df.a((Object) this.f6144a, (Object) rewardItem.f6144a)) {
                }
            }
            return false;
        }
        return true;
    }

    public final int hashCode() {
        String str = this.f6144a;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.b;
        if (str2 != null) {
            i = str2.hashCode();
        }
        return hashCode + i;
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder("RewardItem(name=");
        sb.append(this.f6144a);
        sb.append(", value=");
        sb.append(this.b);
        sb.append(")");
        return sb.toString();
    }

    public RewardItem(String str, String str2) {
        this.f6144a = str;
        this.b = str2;
    }

    public final String getName() {
        return this.f6144a;
    }

    public final String getValue() {
        return this.b;
    }

    public final void setName(String str) {
        this.f6144a = str;
    }

    public final void setValue(String str) {
        this.b = str;
    }
}
