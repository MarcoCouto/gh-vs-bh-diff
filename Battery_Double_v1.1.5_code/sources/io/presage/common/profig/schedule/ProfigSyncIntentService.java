package io.presage.common.profig.schedule;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import io.presage.Comte18mois;
import io.presage.df;

public final class ProfigSyncIntentService extends IntentService {

    /* renamed from: a reason: collision with root package name */
    public static final CamembertauCalvados f6149a = new CamembertauCalvados(0);

    public static final class CamembertauCalvados {
        private CamembertauCalvados() {
        }

        public /* synthetic */ CamembertauCalvados(byte b) {
            this();
        }

        public static void a(Context context) {
            context.startService(new Intent(context, ProfigSyncIntentService.class));
        }

        public static void b(Context context) {
            try {
                io.presage.Comte18mois.CamembertauCalvados camembertauCalvados = Comte18mois.f6041a;
                Context applicationContext = context.getApplicationContext();
                df.a((Object) applicationContext, "context.applicationContext");
                camembertauCalvados.a(applicationContext).b(false);
            } catch (Exception unused) {
            }
        }
    }

    public ProfigSyncIntentService() {
        super("ProfigService");
    }

    /* access modifiers changed from: protected */
    public final void onHandleIntent(Intent intent) {
        Context applicationContext = getApplicationContext();
        df.a((Object) applicationContext, "applicationContext");
        CamembertauCalvados.b(applicationContext);
    }
}
