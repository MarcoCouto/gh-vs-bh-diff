package io.presage.common.profig.schedule;

import android.annotation.TargetApi;
import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.Context;
import io.presage.bh;
import io.presage.cw;
import io.presage.cx;
import io.presage.df;
import io.presage.dg;

@TargetApi(21)
public final class ProfigJobService extends JobService {

    /* renamed from: a reason: collision with root package name */
    private JobParameters f6145a;

    static final class CamembertauCalvados extends dg implements cw<bh> {

        /* renamed from: a reason: collision with root package name */
        final /* synthetic */ ProfigJobService f6146a;

        CamembertauCalvados(ProfigJobService profigJobService) {
            this.f6146a = profigJobService;
            super(0);
        }

        public final /* synthetic */ Object a_() {
            b();
            return bh.f6130a;
        }

        private void b() {
            io.presage.common.profig.schedule.ProfigSyncIntentService.CamembertauCalvados camembertauCalvados = ProfigSyncIntentService.f6149a;
            Context applicationContext = this.f6146a.getApplicationContext();
            df.a((Object) applicationContext, "applicationContext");
            io.presage.common.profig.schedule.ProfigSyncIntentService.CamembertauCalvados.b(applicationContext);
        }
    }

    static final class CamembertdeNormandie extends dg implements cx<Throwable, bh> {

        /* renamed from: a reason: collision with root package name */
        final /* synthetic */ ProfigJobService f6147a;

        CamembertdeNormandie(ProfigJobService profigJobService) {
            this.f6147a = profigJobService;
            super(1);
        }

        public final /* bridge */ /* synthetic */ Object a(Object obj) {
            a();
            return bh.f6130a;
        }

        private void a() {
            this.f6147a.a();
        }
    }

    static final class EcirdelAubrac extends dg implements cw<bh> {

        /* renamed from: a reason: collision with root package name */
        final /* synthetic */ ProfigJobService f6148a;

        EcirdelAubrac(ProfigJobService profigJobService) {
            this.f6148a = profigJobService;
            super(0);
        }

        public final /* synthetic */ Object a_() {
            b();
            return bh.f6130a;
        }

        private void b() {
            this.f6148a.a();
        }
    }

    public final boolean onStopJob(JobParameters jobParameters) {
        return false;
    }

    public final boolean onStartJob(JobParameters jobParameters) {
        this.f6145a = jobParameters;
        io.presage.EcirdelAubrac.CamembertauCalvados camembertauCalvados = io.presage.EcirdelAubrac.f6049a;
        io.presage.EcirdelAubrac.CamembertauCalvados.a(new CamembertauCalvados(this)).a((cx<? super Throwable, bh>) new CamembertdeNormandie<Object,bh>(this)).a((cw<bh>) new EcirdelAubrac<bh>(this));
        return true;
    }

    /* access modifiers changed from: private */
    public final void a() {
        StringBuilder sb = new StringBuilder("marking job as finished ");
        JobParameters jobParameters = this.f6145a;
        sb.append(jobParameters != null ? jobParameters.toString() : null);
        jobFinished(this.f6145a, false);
    }
}
