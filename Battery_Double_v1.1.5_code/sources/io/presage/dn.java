package io.presage;

import java.util.List;

public class dn {
    private static <T extends Throwable> T a(T t) {
        return df.a(t, dn.class.getName());
    }

    private static void a(Object obj, String str) {
        String name = obj == null ? "null" : obj.getClass().getName();
        StringBuilder sb = new StringBuilder();
        sb.append(name);
        sb.append(" cannot be cast to ");
        sb.append(str);
        a(sb.toString());
    }

    private static void a(String str) {
        throw a(new ClassCastException(str));
    }

    private static ClassCastException a(ClassCastException classCastException) {
        throw ((ClassCastException) a((T) classCastException));
    }

    public static List a(Object obj) {
        if ((obj instanceof Cdo) && !(obj instanceof dr)) {
            a(obj, "kotlin.collections.MutableList");
        }
        return b(obj);
    }

    private static List b(Object obj) {
        try {
            return (List) obj;
        } catch (ClassCastException e) {
            throw a(e);
        }
    }
}
