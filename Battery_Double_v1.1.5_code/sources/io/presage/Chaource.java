package io.presage;

import java.util.Iterator;
import java.util.List;

public final class Chaource {

    /* renamed from: a reason: collision with root package name */
    public static final Chaource f6037a = new Chaource();

    private Chaource() {
    }

    public final void a(List<BriedeMelun> list, CendreduBeauzac cendreduBeauzac, Bofavre bofavre) throws Chambertin {
        BriedeMelun a2 = a(list);
        if (a2 != null && (!df.a((Object) a2.k(), (Object) cendreduBeauzac.a()))) {
            try {
                a(a2.k(), true, cendreduBeauzac, bofavre);
            } catch (Chambertin e) {
                AbbayedeCiteauxentiere abbayedeCiteauxentiere = AbbayedeCiteauxentiere.f5998a;
                AbbayedeCiteauxentiere.a((VacherinSuisse) new Bethmale("loaded_error", a2));
                throw e;
            }
        }
    }

    private static BriedeMelun a(List<BriedeMelun> list) {
        Object obj;
        boolean z;
        Iterator it = list.iterator();
        while (true) {
            if (!it.hasNext()) {
                obj = null;
                break;
            }
            obj = it.next();
            if (((BriedeMelun) obj).k().length() > 0) {
                z = true;
                continue;
            } else {
                z = false;
                continue;
            }
            if (z) {
                break;
            }
        }
        return (BriedeMelun) obj;
    }

    private final void a(String str, boolean z, CendreduBeauzac cendreduBeauzac, Bofavre bofavre) throws Chambertin {
        am c = bofavre.c(str);
        if (c instanceof an) {
            cendreduBeauzac.a(((an) c).a());
            cendreduBeauzac.b(str);
        } else if (z) {
            Thread.sleep(400);
            a(str, false, cendreduBeauzac, bofavre);
        } else {
            throw new Chambertin();
        }
    }
}
