package io.presage;

import android.content.Context;
import android.os.Build.VERSION;

public final class z {
    public static final void a(x xVar, String str) {
        try {
            xVar.loadUrl("javascript:".concat(String.valueOf(str)));
        } catch (Throwable th) {
            BurratadesPouilles burratadesPouilles = BurratadesPouilles.f6029a;
            BurratadesPouilles.a(th);
        }
    }

    public static final void a(x xVar, BriedeMelun briedeMelun) {
        boolean z = false;
        String h = briedeMelun.h().length() > 0 ? briedeMelun.h() : "http://ads-test.st.ogury.com/";
        if (briedeMelun.c().length() > 0) {
            z = true;
        }
        try {
            xVar.loadDataWithBaseURL(h, z ? briedeMelun.c() : "The ad contains no ad_content", WebRequest.CONTENT_TYPE_HTML, "UTF-8", null);
        } catch (Throwable th) {
            BurratadesPouilles burratadesPouilles = BurratadesPouilles.f6029a;
            BurratadesPouilles.a(th);
        }
    }

    public static final x a(Context context, BriedeMelun briedeMelun) {
        try {
            x xVar = new x(context, briedeMelun);
            xVar.setBackgroundColor(0);
            if (VERSION.SDK_INT >= 19) {
                xVar.setLayerType(2, null);
            }
            return xVar;
        } catch (Throwable th) {
            BurratadesPouilles burratadesPouilles = BurratadesPouilles.f6029a;
            BurratadesPouilles.a(th);
            return null;
        }
    }
}
