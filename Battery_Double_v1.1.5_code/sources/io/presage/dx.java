package io.presage;

public final class dx extends dv {
    public static final CamembertauCalvados b = new CamembertauCalvados(0);
    private static final dx c = new dx(1, 0);

    public static final class CamembertauCalvados {
        private CamembertauCalvados() {
        }

        public /* synthetic */ CamembertauCalvados(byte b) {
            this();
        }
    }

    public dx(int i, int i2) {
        super(i, i2);
    }

    public final boolean d() {
        return a() > b();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0027, code lost:
        if (b() == r3.b()) goto L_0x0029;
     */
    public final boolean equals(Object obj) {
        if (obj instanceof dx) {
            if (!d() || !((dx) obj).d()) {
                dx dxVar = (dx) obj;
                if (a() == dxVar.a()) {
                }
            }
            return true;
        }
        return false;
    }

    public final int hashCode() {
        if (d()) {
            return -1;
        }
        return (a() * 31) + b();
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(a());
        sb.append("..");
        sb.append(b());
        return sb.toString();
    }
}
