package io.presage;

public final class TommedeYenne {

    /* renamed from: a reason: collision with root package name */
    private final boolean f6106a;
    private boolean b;
    private final String c;
    private boolean d;
    private boolean e;
    private boolean f;

    private TommedeYenne(boolean z, boolean z2, String str, boolean z3) {
        this.f6106a = z;
        this.b = z2;
        this.c = str;
        this.d = z3;
        this.e = false;
        this.f = false;
    }

    public final boolean a() {
        return this.f6106a;
    }

    public final boolean b() {
        return this.b;
    }

    public final String c() {
        return this.c;
    }

    public final boolean d() {
        return this.d;
    }

    public final void e() {
        this.d = true;
    }

    public final boolean f() {
        return this.e;
    }

    public final void g() {
        this.e = true;
    }

    public /* synthetic */ TommedeYenne(boolean z, boolean z2, String str, boolean z3, int i) {
        if ((i & 8) != 0) {
            z3 = false;
        }
        this(z, z2, str, z3);
    }

    public final boolean h() {
        return this.f;
    }

    public final void i() {
        this.f = true;
    }
}
