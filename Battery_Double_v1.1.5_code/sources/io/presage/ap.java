package io.presage;

import com.iab.omid.library.oguryco.adsession.AdSessionConfiguration;
import com.iab.omid.library.oguryco.adsession.AdSessionContext;

public final class ap {

    /* renamed from: a reason: collision with root package name */
    private AdSessionContext f6121a;
    private AdSessionConfiguration b;

    public final AdSessionContext a() {
        return this.f6121a;
    }

    public final void a(AdSessionContext adSessionContext) {
        this.f6121a = adSessionContext;
    }

    public final void a(AdSessionConfiguration adSessionConfiguration) {
        this.b = adSessionConfiguration;
    }

    public final AdSessionConfiguration b() {
        return this.b;
    }
}
