package io.presage;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build.VERSION;
import android.webkit.CookieManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import com.ironsource.sdk.precache.DownloadManager;
import java.io.File;

public final class StMarcellin {
    @SuppressLint({"SetJavaScriptEnabled"})
    public static final void a(WebView webView) {
        WebSettings settings = webView.getSettings();
        df.a((Object) settings, DownloadManager.SETTINGS);
        settings.setJavaScriptEnabled(true);
        WebSettings settings2 = webView.getSettings();
        df.a((Object) settings2, DownloadManager.SETTINGS);
        settings2.setBuiltInZoomControls(true);
        WebSettings settings3 = webView.getSettings();
        df.a((Object) settings3, DownloadManager.SETTINGS);
        settings3.setDisplayZoomControls(false);
        WebSettings settings4 = webView.getSettings();
        df.a((Object) settings4, DownloadManager.SETTINGS);
        settings4.setUseWideViewPort(true);
        WebSettings settings5 = webView.getSettings();
        df.a((Object) settings5, DownloadManager.SETTINGS);
        settings5.setLoadWithOverviewMode(true);
        WebSettings settings6 = webView.getSettings();
        df.a((Object) settings6, DownloadManager.SETTINGS);
        settings6.setDomStorageEnabled(true);
        WebSettings settings7 = webView.getSettings();
        df.a((Object) settings7, DownloadManager.SETTINGS);
        settings7.setDatabaseEnabled(true);
        webView.getSettings().setSupportMultipleWindows(true);
        WebSettings settings8 = webView.getSettings();
        df.a((Object) settings8, DownloadManager.SETTINGS);
        settings8.setJavaScriptCanOpenWindowsAutomatically(true);
        if (VERSION.SDK_INT >= 21) {
            CookieManager.getInstance().setAcceptThirdPartyCookies(webView, true);
            WebSettings settings9 = webView.getSettings();
            df.a((Object) settings9, DownloadManager.SETTINGS);
            settings9.setMixedContentMode(2);
        }
        if (VERSION.SDK_INT <= 19) {
            WebSettings settings10 = webView.getSettings();
            df.a((Object) settings10, DownloadManager.SETTINGS);
            File dir = webView.getContext().getDir("webviewdatabase", 0);
            df.a((Object) dir, "context.getDir(WEB_DB_PATH, Context.MODE_PRIVATE)");
            settings10.setDatabasePath(dir.getPath());
        }
        if (VERSION.SDK_INT <= 18) {
            WebSettings settings11 = webView.getSettings();
            df.a((Object) settings11, DownloadManager.SETTINGS);
            settings11.setSavePassword(true);
        }
        WebSettings settings12 = webView.getSettings();
        df.a((Object) settings12, DownloadManager.SETTINGS);
        settings12.setLoadsImagesAutomatically(true);
        if (VERSION.SDK_INT >= 17) {
            WebSettings settings13 = webView.getSettings();
            df.a((Object) settings13, DownloadManager.SETTINGS);
            settings13.setMediaPlaybackRequiresUserGesture(false);
        }
        WebSettings settings14 = webView.getSettings();
        df.a((Object) settings14, DownloadManager.SETTINGS);
        settings14.setSaveFormData(true);
        webView.getSettings().setSupportZoom(true);
        Context context = webView.getContext();
        df.a((Object) context, "this.context");
        webView.setDownloadListener(new TommeMarcdeRaisin(context));
    }

    public static final String b(WebView webView) {
        Object tag = webView.getTag();
        if (!(tag instanceof String)) {
            tag = null;
        }
        String str = (String) tag;
        return str == null ? "" : str;
    }
}
