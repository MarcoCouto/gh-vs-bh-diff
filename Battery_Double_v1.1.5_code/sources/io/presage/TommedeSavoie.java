package io.presage;

import com.integralads.avid.library.inmobi.utils.AvidJSONUtil;
import com.ironsource.sdk.constants.Constants.ParametersKeys;
import org.json.JSONObject;

public final class TommedeSavoie {

    /* renamed from: a reason: collision with root package name */
    public static final TommedeSavoie f6105a = new TommedeSavoie();

    private TommedeSavoie() {
    }

    public static TommedAuvergne a(String str) {
        try {
            return a(new JSONObject(str));
        } catch (Throwable unused) {
            return null;
        }
    }

    public static TommedAuvergne a(JSONObject jSONObject) {
        TommedAuvergne tommedAuvergne = new TommedAuvergne();
        String optString = jSONObject.optString("url", "");
        df.a((Object) optString, "zoneJson.optString(\"url\", \"\")");
        tommedAuvergne.a(optString);
        String optString2 = jSONObject.optString("content", "");
        df.a((Object) optString2, "zoneJson.optString(\"content\", \"\")");
        tommedAuvergne.b(optString2);
        String optString3 = jSONObject.optString("webViewId", jSONObject.optString("id", ""));
        df.a((Object) optString3, "zoneJson.optString(\"webViewId\", id)");
        tommedAuvergne.c(optString3);
        JSONObject optJSONObject = jSONObject.optJSONObject("size");
        int i = -1;
        tommedAuvergne.b(optJSONObject != null ? optJSONObject.optInt("width", -1) : -1);
        JSONObject optJSONObject2 = jSONObject.optJSONObject("size");
        tommedAuvergne.a(optJSONObject2 != null ? optJSONObject2.optInt("height", -1) : -1);
        JSONObject optJSONObject3 = jSONObject.optJSONObject(ParametersKeys.POSITION);
        tommedAuvergne.d(optJSONObject3 != null ? optJSONObject3.optInt(AvidJSONUtil.KEY_X, -1) : -1);
        JSONObject optJSONObject4 = jSONObject.optJSONObject(ParametersKeys.POSITION);
        if (optJSONObject4 != null) {
            i = optJSONObject4.optInt(AvidJSONUtil.KEY_Y, -1);
        }
        tommedAuvergne.c(i);
        tommedAuvergne.a(jSONObject.optBoolean("enableTracking", false));
        tommedAuvergne.b(jSONObject.optBoolean("keepAlive", false));
        tommedAuvergne.c(jSONObject.optBoolean("isLandingPage", false));
        return tommedAuvergne;
    }
}
