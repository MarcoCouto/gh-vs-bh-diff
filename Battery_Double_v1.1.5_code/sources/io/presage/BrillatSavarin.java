package io.presage;

public final class BrillatSavarin {

    /* renamed from: a reason: collision with root package name */
    private final String f6027a;
    private final String b;
    private final String c;

    public BrillatSavarin(String str, String str2) {
        this.f6027a = str;
        this.b = str2;
        this.c = null;
    }

    public final String a() {
        return this.f6027a;
    }

    public final String b() {
        return this.b;
    }

    private /* synthetic */ BrillatSavarin() {
        this("", null);
    }
}
