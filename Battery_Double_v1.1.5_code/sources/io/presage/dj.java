package io.presage;

public final class dj extends di {
    private final ee c;
    private final String d;
    private final String e;

    public dj(ee eeVar, String str, String str2) {
        this.c = eeVar;
        this.d = str;
        this.e = str2;
    }

    public final ee a() {
        return this.c;
    }

    public final String b() {
        return this.d;
    }

    public final String c() {
        return this.e;
    }

    public final Object b(Object obj) {
        return i().a(obj);
    }
}
