package io.presage;

public final class v implements u {

    /* renamed from: a reason: collision with root package name */
    private final Cheddar f6184a;

    public v(Cheddar cheddar) {
        this.f6184a = cheddar;
    }

    public final void a(RacletteSuisse racletteSuisse) {
        int a2 = Montbriac.a(this.f6184a.j());
        int a3 = Montbriac.a(this.f6184a.k());
        racletteSuisse.a(a2, a3);
        racletteSuisse.b(a2, a3);
        b(racletteSuisse);
    }

    private final void b(RacletteSuisse racletteSuisse) {
        String m = this.f6184a.m();
        boolean n = this.f6184a.n();
        racletteSuisse.a(m, n);
        if (!n) {
            m = "none";
        }
        racletteSuisse.a(!n, m);
    }
}
