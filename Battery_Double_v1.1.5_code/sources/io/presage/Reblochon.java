package io.presage;

public final class Reblochon {

    /* renamed from: a reason: collision with root package name */
    public static final Reblochon f6086a = new Reblochon();

    public static String a() {
        return "ogySdkMraidGateway.updateSupportFlags({sms: false, tel: false, calendar: false, storePicture: false, inlineVideo: false, vpaid: false, location: false})";
    }

    public static String b() {
        return "ogySdkMraidGateway.callEventListeners(\"ogyOnCloseSystem\", {})";
    }

    private Reblochon() {
    }

    public static String a(String str, String str2) {
        StringBuilder sb = new StringBuilder("ogySdkMraidGateway.callErrorListeners(\"");
        sb.append(str2);
        sb.append("\", \"");
        sb.append(str);
        sb.append("\")");
        return sb.toString();
    }

    public static String a(int i) {
        StringBuilder sb = new StringBuilder("ogySdkMraidGateway.updateAudioVolume(");
        sb.append(i);
        sb.append(')');
        return sb.toString();
    }

    public static String a(String str, boolean z) {
        StringBuilder sb = new StringBuilder("ogySdkMraidGateway.updateCurrentAppOrientation({orientation: \"");
        sb.append(str);
        sb.append("\", locked: ");
        sb.append(z);
        sb.append("})");
        return sb.toString();
    }

    public static String a(boolean z, String str) {
        StringBuilder sb = new StringBuilder("ogySdkMraidGateway.updateOrientationProperties({allowOrientationChange: ");
        sb.append(z);
        sb.append(", forceOrientation: \"");
        sb.append(str);
        sb.append("\"})");
        return sb.toString();
    }

    public static String a(int i, int i2) {
        StringBuilder sb = new StringBuilder("ogySdkMraidGateway.updateScreenSize({width: ");
        sb.append(i);
        sb.append(", height: ");
        sb.append(i2);
        sb.append("})");
        return sb.toString();
    }

    public static String a(String str) {
        StringBuilder sb = new StringBuilder("ogySdkMraidGateway.updatePlacementType(\"");
        sb.append(str);
        sb.append("\")");
        return sb.toString();
    }

    public static String a(boolean z) {
        StringBuilder sb = new StringBuilder("ogySdkMraidGateway.updateViewability(");
        sb.append(z);
        sb.append(')');
        return sb.toString();
    }

    public static String b(int i, int i2) {
        StringBuilder sb = new StringBuilder("ogySdkMraidGateway.updateMaxSize({width: ");
        sb.append(i);
        sb.append(", height: ");
        sb.append(i2);
        sb.append("})");
        return sb.toString();
    }

    public static String c(int i, int i2) {
        StringBuilder sb = new StringBuilder("ogySdkMraidGateway.updateDefaultPosition({x: 0, y: 0, width: ");
        sb.append(i);
        sb.append(", height: ");
        sb.append(i2);
        sb.append("})");
        return sb.toString();
    }

    public static String a(int i, int i2, int i3, int i4) {
        StringBuilder sb = new StringBuilder("ogySdkMraidGateway.updateCurrentPosition({x: ");
        sb.append(i3);
        sb.append(", y: ");
        sb.append(i4);
        sb.append(", width: ");
        sb.append(i);
        sb.append(", height: ");
        sb.append(i2);
        sb.append("})");
        return sb.toString();
    }

    public static String b(int i, int i2, int i3, int i4) {
        StringBuilder sb = new StringBuilder("ogySdkMraidGateway.updateResizeProperties({width: ");
        sb.append(i);
        sb.append(", height: ");
        sb.append(i2);
        sb.append(", offsetX: ");
        sb.append(i3);
        sb.append(", offsetY: ");
        sb.append(i4);
        sb.append(", customClosePosition: \"right\", allowOffscreen: false})");
        return sb.toString();
    }

    public static String d(int i, int i2) {
        StringBuilder sb = new StringBuilder("ogySdkMraidGateway.updateExpandProperties({width: ");
        sb.append(i);
        sb.append(", height: ");
        sb.append(i2);
        sb.append(", useCustomClose: false, isModal: true})");
        return sb.toString();
    }

    public static String b(String str) {
        StringBuilder sb = new StringBuilder("ogySdkMraidGateway.updateState(\"");
        sb.append(str);
        sb.append("\")");
        return sb.toString();
    }

    public static String b(String str, String str2) {
        StringBuilder sb = new StringBuilder("ogySdkMraidGateway.callPendingMethodCallback(\"");
        sb.append(str);
        sb.append("\", null, {webviewId:\"");
        sb.append(str2);
        sb.append("\"})");
        return sb.toString();
    }

    public static String e(int i, int i2) {
        StringBuilder sb = new StringBuilder("ogySdkMraidGateway.updateExposure({exposedPercentage: 100.0, visibleRectangle: {x: 0, y: 0, width: ");
        sb.append(i);
        sb.append(", height: ");
        sb.append(i2);
        sb.append("}})");
        return sb.toString();
    }

    public static String a(String str, boolean z, boolean z2, String str2, String str3) {
        StringBuilder sb = new StringBuilder("ogySdkMraidGateway.callEventListeners(\"ogyOnNavigation\", {event: \"");
        sb.append(str);
        sb.append("\", canGoBack: ");
        sb.append(z2);
        sb.append(", canGoForward: ");
        sb.append(z);
        sb.append(", webviewId: \"");
        sb.append(str2);
        sb.append("\", url: \"");
        sb.append(str3);
        sb.append("\"})");
        return sb.toString();
    }
}
