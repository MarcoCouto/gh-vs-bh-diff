package io.presage;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import io.presage.mraid.browser.listeners.CloseSystemDialogsListener$1;

public final class TommeduJura {

    /* renamed from: a reason: collision with root package name */
    private final BroadcastReceiver f6107a = new CloseSystemDialogsListener$1(this);
    private final Context b;
    private final StVincentauChablis c;

    public TommeduJura(Context context, StVincentauChablis stVincentauChablis) {
        this.b = context;
        this.c = stVincentauChablis;
        b();
    }

    private final void b() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.CLOSE_SYSTEM_DIALOGS");
        intentFilter.addAction("android.intent.action.SCREEN_OFF");
        this.b.registerReceiver(this.f6107a, intentFilter);
    }

    /* access modifiers changed from: private */
    public final void c() {
        this.c.b();
    }

    public final void a() {
        this.b.unregisterReceiver(this.f6107a);
    }
}
