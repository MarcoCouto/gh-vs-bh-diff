package io.presage;

import android.content.Context;
import com.iab.omid.library.oguryco.Omid;

public final class ar {

    /* renamed from: a reason: collision with root package name */
    public static final ar f6123a = new ar();

    private ar() {
    }

    public static void a(Context context) {
        try {
            Omid.activateWithOmidApiVersion(Omid.getVersion(), context);
        } catch (IllegalArgumentException e) {
            aq aqVar = aq.f6122a;
            aq.a(e);
        }
    }

    public static boolean a() {
        return Omid.isActive();
    }
}
