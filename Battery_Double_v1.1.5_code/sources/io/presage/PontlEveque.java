package io.presage;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.webkit.WebView;
import android.widget.FrameLayout;

public final class PontlEveque extends FrameLayout {
    public PontlEveque(Context context) {
        super(context);
    }

    public final void addView(View view, LayoutParams layoutParams) {
        if (!(view instanceof WebView) || getChildCount() <= 0) {
            super.addView(view, layoutParams);
        } else {
            super.addView(view, getChildCount() - 1, layoutParams);
        }
    }
}
