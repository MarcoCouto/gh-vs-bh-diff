package io.presage;

public final class Bethmale extends VacherinSuisse {

    /* renamed from: a reason: collision with root package name */
    private final BriedeMelun f6010a;

    public Bethmale(String str, BriedeMelun briedeMelun) {
        super(str, 0);
        this.f6010a = briedeMelun;
    }

    public final BriedeMelun a() {
        return this.f6010a;
    }
}
