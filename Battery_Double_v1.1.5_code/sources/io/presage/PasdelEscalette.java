package io.presage;

import android.annotation.SuppressLint;
import android.os.Build.VERSION;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageButton;

public final class PasdelEscalette {

    /* renamed from: a reason: collision with root package name */
    private ImageButton f6076a = new ImageButton(this.d.getContext());
    private final Handler b = new Handler();
    /* access modifiers changed from: private */
    public final PersilledumontBlanc c;
    private final ViewGroup d;
    private final Bofavre e;
    private final String f;

    static final class CamembertauCalvados implements OnClickListener {

        /* renamed from: a reason: collision with root package name */
        final /* synthetic */ PasdelEscalette f6077a;

        CamembertauCalvados(PasdelEscalette pasdelEscalette) {
            this.f6077a = pasdelEscalette;
        }

        public final void onClick(View view) {
            this.f6077a.c.f();
            this.f6077a.e();
        }
    }

    static final class CamembertdeNormandie implements Runnable {

        /* renamed from: a reason: collision with root package name */
        final /* synthetic */ PasdelEscalette f6078a;

        CamembertdeNormandie(PasdelEscalette pasdelEscalette) {
            this.f6078a = pasdelEscalette;
        }

        public final void run() {
            this.f6078a.a();
        }
    }

    public PasdelEscalette(PersilledumontBlanc persilledumontBlanc, ViewGroup viewGroup, Bofavre bofavre, String str) {
        this.c = persilledumontBlanc;
        this.d = viewGroup;
        this.e = bofavre;
        this.f = str;
        d();
    }

    @SuppressLint({"RtlHardcoded"})
    private final void d() {
        f();
        LayoutParams layoutParams = new LayoutParams(-2, -2);
        layoutParams.gravity = 5;
        ViewGroup.LayoutParams layoutParams2 = layoutParams;
        this.f6076a.setLayoutParams(layoutParams2);
        this.f6076a.setOnClickListener(new CamembertauCalvados(this));
        this.f6076a.setVisibility(8);
        this.d.addView(this.f6076a, layoutParams2);
    }

    /* access modifiers changed from: private */
    public final void e() {
        if (this.f.length() > 0) {
            this.e.b(this.f);
        }
    }

    private final void f() {
        if (VERSION.SDK_INT >= 16) {
            this.f6076a.setBackground(null);
        } else {
            this.f6076a.setBackgroundResource(0);
        }
        this.f6076a.setImageResource(R.drawable.btn_presage_mraid_close);
    }

    public final void a(long j) {
        this.b.postDelayed(new CamembertdeNormandie(this), j);
    }

    public final void a() {
        this.f6076a.setVisibility(0);
    }

    public final void b() {
        this.b.removeCallbacksAndMessages(null);
        this.f6076a.setVisibility(8);
    }

    public final void c() {
        this.b.removeCallbacksAndMessages(null);
    }
}
