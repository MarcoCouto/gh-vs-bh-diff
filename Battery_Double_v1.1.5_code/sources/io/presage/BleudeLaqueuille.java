package io.presage;

import android.content.Context;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.util.LinkedHashMap;
import java.util.Map;

public class BleudeLaqueuille implements af {

    /* renamed from: a reason: collision with root package name */
    private final Context f6015a;
    private final CoeurdArras b;
    private final Dauphin c;

    public BleudeLaqueuille(Context context, CoeurdArras coeurdArras, Dauphin dauphin) {
        this.f6015a = context;
        this.b = coeurdArras;
        this.c = dauphin;
    }

    public /* synthetic */ BleudeLaqueuille(Context context) {
        this(context, new CoeurdArras(context), Dauphin.f6045a);
    }

    public Map<String, String> a() {
        EpoissesdeBourgogne a2 = Dauphin.a(this.f6015a);
        Map<String, String> linkedHashMap = new LinkedHashMap<>();
        linkedHashMap.put("Content-Type", "application/json");
        linkedHashMap.put(HttpRequest.HEADER_ACCEPT_ENCODING, HttpRequest.ENCODING_GZIP);
        linkedHashMap.put(HttpRequest.HEADER_CONTENT_ENCODING, HttpRequest.ENCODING_GZIP);
        linkedHashMap.put("Device-OS", "android");
        linkedHashMap.put("User", a2.a());
        linkedHashMap.put("User-Agent", this.b.d());
        return linkedHashMap;
    }
}
