package io.presage;

import android.content.Context;
import io.presage.Beaufort.CamembertauCalvados;

public final class AbbayedeCiteauxentiere {

    /* renamed from: a reason: collision with root package name */
    public static final AbbayedeCiteauxentiere f5998a = new AbbayedeCiteauxentiere();
    private static Beaufort b;

    private AbbayedeCiteauxentiere() {
    }

    public static void a(Context context) {
        if (b == null) {
            CamembertauCalvados camembertauCalvados = Beaufort.f6006a;
            b = CamembertauCalvados.a(context);
        }
    }

    public static void a(VacherinSuisse vacherinSuisse) {
        Beaufort beaufort = b;
        if (beaufort != null) {
            beaufort.a(vacherinSuisse);
        }
    }
}
