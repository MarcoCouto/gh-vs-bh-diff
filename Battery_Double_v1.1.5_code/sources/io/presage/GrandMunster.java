package io.presage;

import android.content.Context;
import android.os.Build.VERSION;

public final class GrandMunster implements Goudaaucumin {

    /* renamed from: a reason: collision with root package name */
    public static final GrandMunster f6064a = new GrandMunster();
    private static Goudaauxepices b;

    private GrandMunster() {
    }

    private static Goudaauxepices b(Context context) {
        if (b == null) {
            Context applicationContext = context.getApplicationContext();
            df.a((Object) applicationContext, "context.applicationContext");
            b = c(applicationContext);
        }
        Goudaauxepices goudaauxepices = b;
        if (goudaauxepices == null) {
            df.a();
        }
        return goudaauxepices;
    }

    private static Goudaauxepices c(Context context) {
        if (VERSION.SDK_INT >= 21) {
            return new GorgonzolaPiccante(context);
        }
        return new Gouda36mois(context);
    }

    public final void a(Context context) {
        b(context).a();
    }

    public final void a(Context context, long j) {
        Goudaauxepices b2 = b(context);
        b2.a();
        b2.a(j);
    }
}
