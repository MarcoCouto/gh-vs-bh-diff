package io.presage;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public final class CamembertdeNormandie implements AbbayeduMontdesCats {

    /* renamed from: a reason: collision with root package name */
    public static final CamembertauCalvados f6031a = new CamembertauCalvados(0);
    private final ExecutorService b;

    public static final class CamembertauCalvados {
        private CamembertauCalvados() {
        }

        public /* synthetic */ CamembertauCalvados(byte b) {
            this();
        }
    }

    public CamembertdeNormandie() {
        ExecutorService newFixedThreadPool = Executors.newFixedThreadPool(Math.max(4, Runtime.getRuntime().availableProcessors()));
        df.a((Object) newFixedThreadPool, "Executors.newFixedThreadPool(nrOfCachedThreads)");
        this.b = newFixedThreadPool;
    }

    public final void a(Runnable runnable) {
        this.b.execute(runnable);
    }
}
