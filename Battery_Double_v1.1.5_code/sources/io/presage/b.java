package io.presage;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

@SuppressLint({"StaticFieldLeak"})
public final class b {

    /* renamed from: a reason: collision with root package name */
    public static final b f6126a = new b();
    private static final Map<String, a> b;
    private static Handler c = new Handler(Looper.getMainLooper());
    private static final Runnable d = CamembertauCalvados.f6127a;
    private static Integer e;
    private static FourmedeMontbrison f = FourmedeMontbrison.f6060a;

    static final class CamembertauCalvados implements Runnable {

        /* renamed from: a reason: collision with root package name */
        public static final CamembertauCalvados f6127a = new CamembertauCalvados();

        CamembertauCalvados() {
        }

        public final void run() {
            b bVar = b.f6126a;
            b.a(false);
        }
    }

    static {
        Map<String, a> synchronizedMap = Collections.synchronizedMap(new LinkedHashMap());
        df.a((Object) synchronizedMap, "Collections.synchronized…tring, MraidCacheItem>())");
        b = synchronizedMap;
    }

    private b() {
    }

    public static void a(e eVar) {
        Iterator it = b.entrySet().iterator();
        while (it.hasNext()) {
            if (df.a((Object) (e) ((a) ((Entry) it.next()).getValue()).a().get(), (Object) eVar)) {
                it.remove();
            }
        }
    }

    public static void a(String str) {
        Iterator it = b.entrySet().iterator();
        while (it.hasNext()) {
            Entry entry = (Entry) it.next();
            if (df.a((Object) (String) entry.getKey(), (Object) str)) {
                it.remove();
                e eVar = (e) ((a) entry.getValue()).a().get();
                if (eVar != null) {
                    eVar.a(((a) entry.getValue()).c());
                }
            }
        }
    }

    public static void a(a aVar) {
        b(aVar);
        a(true);
        b.put(aVar.c().a(), aVar);
        new StringBuilder("final cache size ").append(b.size());
    }

    private static void b(a aVar) {
        if (e == null) {
            Context context = aVar.b().getContext();
            df.a((Object) context, "mraidCacheItem.webView.context");
            FourmedeHauteLoire a2 = FourmedeMontbrison.a(context);
            if (a2 != null) {
                e = a2.h() > 0 ? Integer.valueOf(a2.h()) : null;
            }
        }
    }

    public static void a() {
        Iterator it = b.entrySet().iterator();
        while (it.hasNext()) {
            Entry entry = (Entry) it.next();
            if (c((a) entry.getValue())) {
                it.remove();
                AbbayedeCiteauxentiere abbayedeCiteauxentiere = AbbayedeCiteauxentiere.f5998a;
                AbbayedeCiteauxentiere.a((VacherinSuisse) new Bethmale("expired", ((a) entry.getValue()).c()));
                e eVar = (e) ((a) entry.getValue()).a().get();
                if (eVar != null) {
                    eVar.c();
                }
            }
        }
    }

    private static boolean c(a aVar) {
        Context context = aVar.b().getContext();
        df.a((Object) context, "mraidCacheItem.webView.context");
        FourmedeHauteLoire a2 = FourmedeMontbrison.a(context);
        return a2 != null && System.currentTimeMillis() - aVar.d() > a2.l();
    }

    public static x b(String str) {
        x xVar = null;
        if (!b.containsKey(str)) {
            return null;
        }
        a aVar = (a) b.get(str);
        if (aVar != null) {
            xVar = aVar.b();
        }
        b.remove(str);
        return xVar;
    }

    /* access modifiers changed from: private */
    public static void a(boolean z) {
        new StringBuilder("clean cache ").append(b.size());
        Iterator it = b.entrySet().iterator();
        while (it.hasNext()) {
            if (((a) ((Entry) it.next()).getValue()).a().get() == null) {
                it.remove();
            }
        }
        if (z || (!b.isEmpty())) {
            c.removeCallbacksAndMessages(null);
            c.postDelayed(d, 1200000);
        }
        new StringBuilder("after cache ").append(b.size());
    }
}
