package io.presage;

class ew extends ev {
    public static final int a(CharSequence charSequence) {
        return charSequence.length() - 1;
    }

    public static final CharSequence a(CharSequence charSequence, int i, int i2, CharSequence charSequence2) {
        if (i2 >= i) {
            StringBuilder sb = new StringBuilder();
            sb.append(charSequence, 0, i);
            sb.append(charSequence2);
            sb.append(charSequence, i2, charSequence.length());
            return sb;
        }
        StringBuilder sb2 = new StringBuilder("End index (");
        sb2.append(i2);
        sb2.append(") is less than start index (");
        sb2.append(i);
        sb2.append(").");
        throw new IndexOutOfBoundsException(sb2.toString());
    }

    public static final boolean a(CharSequence charSequence, CharSequence charSequence2, int i, int i2, boolean z) {
        if (i < 0 || charSequence.length() - i2 < 0 || i > charSequence2.length() - i2) {
            return false;
        }
        for (int i3 = 0; i3 < i2; i3++) {
            if (!ei.a(charSequence.charAt(i3 + 0), charSequence2.charAt(i + i3), z)) {
                return false;
            }
        }
        return true;
    }

    public static final int a(CharSequence charSequence, char[] cArr, int i) {
        if (charSequence instanceof String) {
            return ((String) charSequence).indexOf(bi.a(cArr), i);
        }
        int a2 = dy.a(i);
        int a3 = en.a(charSequence);
        if (a2 <= a3) {
            while (true) {
                char charAt = charSequence.charAt(a2);
                boolean z = false;
                int i2 = 0;
                while (true) {
                    if (i2 > 0) {
                        break;
                    } else if (ei.a(cArr[i2], charAt, false)) {
                        z = true;
                        break;
                    } else {
                        i2++;
                    }
                }
                if (!z) {
                    if (a2 == a3) {
                        break;
                    }
                    a2++;
                } else {
                    return a2;
                }
            }
        }
        return -1;
    }

    /* access modifiers changed from: private */
    public static final int a(CharSequence charSequence, CharSequence charSequence2, int i, boolean z) {
        dv dxVar = new dx(dy.a(0), dy.a(i, charSequence.length()));
        if (!(charSequence instanceof String) || !(charSequence2 instanceof String)) {
            int a2 = dxVar.a();
            int b = dxVar.b();
            int c = dxVar.c();
            if (c < 0 ? a2 >= b : a2 <= b) {
                while (!en.a(charSequence2, charSequence, a2, charSequence2.length(), z)) {
                    if (a2 != b) {
                        a2 += c;
                    }
                }
                return a2;
            }
        } else {
            int a3 = dxVar.a();
            int b2 = dxVar.b();
            int c2 = dxVar.c();
            if (c2 < 0 ? a3 >= b2 : a3 <= b2) {
                while (!en.a((String) charSequence2, (String) charSequence, a3, charSequence2.length(), z)) {
                    if (a3 != b2) {
                        a3 += c2;
                    }
                }
                return a3;
            }
        }
        return -1;
    }

    public static /* synthetic */ int a(CharSequence charSequence, int i, int i2) {
        if ((i2 & 2) != 0) {
            i = 0;
        }
        return en.a(charSequence, i);
    }

    public static final int a(CharSequence charSequence, int i) {
        if (charSequence instanceof String) {
            return ((String) charSequence).indexOf(46, i);
        }
        return en.a(charSequence, new char[]{'.'}, i);
    }

    public static final int a(CharSequence charSequence, String str) {
        if (!(charSequence instanceof String)) {
            return a(charSequence, (CharSequence) str, charSequence.length(), false);
        }
        return ((String) charSequence).indexOf(str, 0);
    }

    public static final boolean a(CharSequence charSequence, CharSequence charSequence2) {
        return charSequence2 instanceof String ? en.a(charSequence, (String) charSequence2) >= 0 : a(charSequence, charSequence2, charSequence.length(), false) >= 0;
    }
}
