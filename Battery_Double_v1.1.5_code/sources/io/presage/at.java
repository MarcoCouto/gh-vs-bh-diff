package io.presage;

import android.webkit.WebView;
import com.iab.omid.library.oguryco.adsession.AdSession;

public final class at {
    public static AdSession a(WebView webView, boolean z) {
        try {
            ao aoVar = ao.f6120a;
            ap a2 = ao.a(webView, z);
            AdSession createAdSession = AdSession.createAdSession(a2 != null ? a2.b() : null, a2 != null ? a2.a() : null);
            createAdSession.registerAdView(webView);
            return createAdSession;
        } catch (Exception e) {
            aq aqVar = aq.f6122a;
            aq.a(e);
            return null;
        }
    }
}
