package io.presage;

public final class Aveyronnais<T> implements AbbayedeTamie {

    /* renamed from: a reason: collision with root package name */
    public static final CamembertauCalvados f6003a = new CamembertauCalvados(0);
    /* access modifiers changed from: private */
    public boolean b;
    /* access modifiers changed from: private */
    public cx<? super Throwable, bh> c;
    /* access modifiers changed from: private */
    public final cw<T> d;

    public static final class CamembertauCalvados {
        private CamembertauCalvados() {
        }

        public /* synthetic */ CamembertauCalvados(byte b) {
            this();
        }

        public static <T> Aveyronnais<T> a(cw<? extends T> cwVar) {
            return new Aveyronnais<>(cwVar, 0);
        }
    }

    static final class CamembertdeNormandie implements Runnable {

        /* renamed from: a reason: collision with root package name */
        final /* synthetic */ Aveyronnais f6004a;
        final /* synthetic */ cx b;
        final /* synthetic */ Object c;

        CamembertdeNormandie(Aveyronnais aveyronnais, cx cxVar, Object obj) {
            this.f6004a = aveyronnais;
            this.b = cxVar;
            this.c = obj;
        }

        public final void run() {
            if (!this.f6004a.b) {
                this.b.a(this.c);
            }
        }
    }

    static final class EcirdelAubrac implements Runnable {

        /* renamed from: a reason: collision with root package name */
        final /* synthetic */ Aveyronnais f6005a;
        final /* synthetic */ cx b;

        EcirdelAubrac(Aveyronnais aveyronnais, cx cxVar) {
            this.f6005a = aveyronnais;
            this.b = cxVar;
        }

        public final void run() {
            try {
                this.f6005a.a(this.b, this.f6005a.d.a_());
            } catch (Throwable th) {
                cx b2 = this.f6005a.c;
                if (b2 != null) {
                    this.f6005a.a(b2, th);
                }
            }
        }
    }

    private Aveyronnais(cw<? extends T> cwVar) {
        this.d = cwVar;
    }

    public /* synthetic */ Aveyronnais(cw cwVar, byte b2) {
        this(cwVar);
    }

    public final Aveyronnais<T> a(cx<? super Throwable, bh> cxVar) {
        this.c = cxVar;
        return this;
    }

    public final AbbayedeTamie b(cx<? super T, bh> cxVar) {
        AffideliceauChablis affideliceauChablis = AffideliceauChablis.b;
        AffideliceauChablis.b().a(new EcirdelAubrac(this, cxVar));
        return this;
    }

    /* access modifiers changed from: private */
    public final <R> void a(cx<? super R, bh> cxVar, R r) {
        AffideliceauChablis affideliceauChablis = AffideliceauChablis.b;
        AffideliceauChablis.c().a(new CamembertdeNormandie(this, cxVar, r));
    }
}
