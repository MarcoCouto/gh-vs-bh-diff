package io.presage;

public final class EpoissesdeBourgogne {

    /* renamed from: a reason: collision with root package name */
    private final String f6055a;
    private final boolean b;
    private final boolean c;

    public EpoissesdeBourgogne(String str, boolean z, boolean z2) {
        this.f6055a = str;
        this.b = z;
        this.c = z2;
    }

    public final String a() {
        return this.f6055a;
    }

    public final boolean b() {
        return this.b;
    }

    public final boolean c() {
        return this.c;
    }
}
