package io.presage;

import java.util.List;

public final class CapGrisNez {

    /* renamed from: a reason: collision with root package name */
    private final List<BriedeMelun> f6033a;

    public CapGrisNez(List<BriedeMelun> list) {
        this.f6033a = list;
    }

    public final List<BriedeMelun> a() {
        return this.f6033a;
    }
}
