package io.presage;

public final class al {

    /* renamed from: a reason: collision with root package name */
    private final String f6118a;
    private final String b;
    private final String c;
    private final af d;

    public al(String str, String str2, String str3, af afVar) {
        this.f6118a = str;
        this.b = str2;
        this.c = str3;
        this.d = afVar;
    }

    public final String a() {
        return this.f6118a;
    }

    public final String b() {
        return this.b;
    }

    public final String c() {
        return this.c;
    }

    public final af d() {
        return this.d;
    }
}
