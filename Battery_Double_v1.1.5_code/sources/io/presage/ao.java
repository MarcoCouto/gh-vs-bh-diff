package io.presage;

import android.webkit.WebView;
import com.iab.omid.library.oguryco.adsession.AdSessionConfiguration;
import com.iab.omid.library.oguryco.adsession.AdSessionContext;
import com.iab.omid.library.oguryco.adsession.Owner;
import com.iab.omid.library.oguryco.adsession.Partner;

public final class ao {

    /* renamed from: a reason: collision with root package name */
    public static final ao f6120a = new ao();

    private ao() {
    }

    public static ap a(WebView webView, boolean z) {
        ap apVar = new ap();
        Partner a2 = a();
        if (a2 == null) {
            return null;
        }
        apVar.a(a(a2, webView));
        apVar.a(a(z));
        return apVar;
    }

    private static Partner a() {
        try {
            return Partner.createPartner("Ogury", "3.2.2-moat");
        } catch (IllegalArgumentException e) {
            aq aqVar = aq.f6122a;
            aq.a(e);
            return null;
        }
    }

    private static AdSessionContext a(Partner partner, WebView webView) {
        try {
            return AdSessionContext.createHtmlAdSessionContext(partner, webView, "");
        } catch (IllegalArgumentException e) {
            aq aqVar = aq.f6122a;
            aq.a(e);
            return null;
        }
    }

    private static AdSessionConfiguration a(boolean z) {
        Owner owner = Owner.JAVASCRIPT;
        Owner owner2 = Owner.NONE;
        if (z) {
            owner2 = Owner.JAVASCRIPT;
        }
        try {
            return AdSessionConfiguration.createAdSessionConfiguration(owner, owner2, false);
        } catch (IllegalArgumentException e) {
            aq aqVar = aq.f6122a;
            aq.a(e);
            return null;
        }
    }
}
