package io.presage;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.webkit.WebView;
import android.widget.FrameLayout.LayoutParams;
import io.presage.interstitial.ui.InterstitialActivity;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public final class PersilledumontBlanc {

    /* renamed from: a reason: collision with root package name */
    public static final CamembertauCalvados f6080a = new CamembertauCalvados(0);
    private x b;
    private PaletdeBourgogne c;
    private boolean d;
    private boolean e;
    private StRomans f;
    private BriedeMelun g;
    private List<BriedeMelun> h;
    private PasdelEscalette i;
    private final InterstitialActivity j;
    private final io.presage.StRomans.CamembertauCalvados k;
    private final io.presage.PaletdeBourgogne.CamembertauCalvados l;
    private final FourmedeMontbrison m;
    private final Piastrellou n;
    private final BoulettedAvesnes o;
    private final PetitMorin p;
    private final PavedAremberg q;
    private final io.presage.interstitial.ui.InterstitialActivity.CamembertauCalvados r;
    private final RouedeBrie s;

    public static final class CamembertauCalvados {
        private CamembertauCalvados() {
        }

        public /* synthetic */ CamembertauCalvados(byte b) {
            this();
        }
    }

    private PersilledumontBlanc(InterstitialActivity interstitialActivity, io.presage.StRomans.CamembertauCalvados camembertauCalvados, io.presage.PaletdeBourgogne.CamembertauCalvados camembertauCalvados2, FourmedeMontbrison fourmedeMontbrison, Piastrellou piastrellou, BoulettedAvesnes boulettedAvesnes, PetitMorin petitMorin, PavedAremberg pavedAremberg, io.presage.interstitial.ui.InterstitialActivity.CamembertauCalvados camembertauCalvados3, RouedeBrie rouedeBrie) {
        this.j = interstitialActivity;
        this.k = camembertauCalvados;
        this.l = camembertauCalvados2;
        this.m = fourmedeMontbrison;
        this.n = piastrellou;
        this.o = boulettedAvesnes;
        this.p = petitMorin;
        this.q = pavedAremberg;
        this.r = camembertauCalvados3;
        this.s = rouedeBrie;
        this.e = true;
        this.h = new ArrayList();
    }

    public PersilledumontBlanc(InterstitialActivity interstitialActivity) {
        this(interstitialActivity, StRomans.f6096a, PaletdeBourgogne.f6074a, FourmedeMontbrison.f6060a, new Piastrellou(), BoulettedAvesnes.f6017a, new PetitMorin(), PavedAremberg.f6079a, InterstitialActivity.f6169a, RouedeBrie.f6089a);
    }

    public final void a(PontlEveque pontlEveque, List<BriedeMelun> list) {
        this.h = list;
        this.g = g();
        BriedeMelun briedeMelun = this.g;
        if (briedeMelun != null) {
            this.j.a(briedeMelun);
            a(pontlEveque);
            StRomans a2 = io.presage.StRomans.CamembertauCalvados.a(this.j, briedeMelun, pontlEveque);
            this.f = a2;
            this.c = io.presage.PaletdeBourgogne.CamembertauCalvados.a(this.j, a2);
            PaletdeBourgogne paletdeBourgogne = this.c;
            if (paletdeBourgogne == null) {
                df.a("webViewGateway");
            }
            x a3 = paletdeBourgogne.a(briedeMelun);
            if (a3 != null) {
                this.b = a3;
                a2.a(briedeMelun.j().length() > 0 ? briedeMelun.j() : "controller", a3);
                FourmedeHauteLoire a4 = FourmedeMontbrison.a((Context) this.j);
                if (a4 != null) {
                    a(a4, briedeMelun);
                    a(a3);
                    pontlEveque.addView(a3, new LayoutParams(-1, -1));
                    this.n.a();
                    return;
                }
                throw new IllegalStateException("Profig must not be null");
            }
            throw new IllegalStateException("WebView must not be null");
        }
        throw new IllegalStateException("Ad must not be null");
    }

    private final BriedeMelun g() {
        return (BriedeMelun) bn.c(this.h);
    }

    private final void a(FourmedeHauteLoire fourmedeHauteLoire, BriedeMelun briedeMelun) {
        this.d = fourmedeHauteLoire.i();
        this.e = fourmedeHauteLoire.j();
        b(fourmedeHauteLoire, briedeMelun);
        PetitMorin petitMorin = this.p;
        x xVar = this.b;
        if (xVar == null) {
            df.a("webView");
        }
        petitMorin.a(briedeMelun, xVar);
        PasdelEscalette pasdelEscalette = this.i;
        if (pasdelEscalette != null) {
            pasdelEscalette.a(fourmedeHauteLoire.m());
        }
    }

    private final void b(FourmedeHauteLoire fourmedeHauteLoire, BriedeMelun briedeMelun) {
        if (fourmedeHauteLoire.c() && briedeMelun.n()) {
            Piastrellou piastrellou = this.n;
            Application application = this.j.getApplication();
            df.a((Object) application, "activity.application");
            piastrellou.a(application);
            Piastrellou piastrellou2 = this.n;
            x xVar = this.b;
            if (xVar == null) {
                df.a("webView");
            }
            piastrellou2.a((WebView) xVar);
        }
    }

    private final void a(x xVar) {
        if (!xVar.getShowSdkCloseButton()) {
            b();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0008, code lost:
        if (r0 == null) goto L_0x000a;
     */
    @SuppressLint({"RtlHardcoded"})
    private final void a(PontlEveque pontlEveque) {
        String str;
        BriedeMelun briedeMelun = this.g;
        if (briedeMelun != null) {
            str = briedeMelun.q();
        }
        str = "";
        this.i = PavedAremberg.a(this, pontlEveque, BoulettedAvesnes.a(this.j), str);
    }

    public final void a() {
        PasdelEscalette pasdelEscalette = this.i;
        if (pasdelEscalette != null) {
            pasdelEscalette.a();
        }
    }

    public final void b() {
        PasdelEscalette pasdelEscalette = this.i;
        if (pasdelEscalette != null) {
            pasdelEscalette.b();
        }
    }

    public final void a(String str) {
        boolean z = true;
        if (this.h.size() > 1) {
            this.h.remove(0);
            if (str.length() != 0) {
                z = false;
            }
            if (z || b(str)) {
                io.presage.interstitial.ui.InterstitialActivity.CamembertauCalvados.a(this.j, this.h);
            }
        }
    }

    private final boolean b(String str) {
        Iterator it = this.h.iterator();
        int i2 = 0;
        while (true) {
            if (!it.hasNext()) {
                i2 = -1;
                break;
            } else if (df.a((Object) ((BriedeMelun) it.next()).b(), (Object) str)) {
                break;
            } else {
                i2++;
            }
        }
        if (i2 < 0) {
            return false;
        }
        Collections.swap(this.h, i2, 0);
        return true;
    }

    public final void c() {
        if (h() && this.e) {
            this.j.finish();
        }
    }

    private final boolean h() {
        StRomans stRomans = this.f;
        if (stRomans != null) {
            return stRomans.b();
        }
        return true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0016, code lost:
        if (r0 == null) goto L_0x0018;
     */
    public final void d() {
        String str;
        StRomans stRomans = this.f;
        if (stRomans != null) {
            stRomans.d();
        }
        PasdelEscalette pasdelEscalette = this.i;
        if (pasdelEscalette != null) {
            pasdelEscalette.c();
        }
        BriedeMelun briedeMelun = this.g;
        if (briedeMelun != null) {
            str = briedeMelun.b();
        }
        str = "";
        RouedeBrie.a(new RomansPartDieu(str, "adClosed"));
        RouedeBrie.a(str);
        this.n.b();
        this.p.a();
    }

    public final boolean e() {
        StRomans stRomans = this.f;
        if (stRomans != null) {
            stRomans.c();
        }
        return this.d;
    }

    public final void f() {
        this.j.finish();
    }
}
