package io.presage;

import java.util.regex.Pattern;

public class f extends y {

    /* renamed from: a reason: collision with root package name */
    private final String f6162a = this.b.pattern();
    private final Pattern b;

    public f(Pattern pattern) {
        this.b = pattern;
    }

    public final boolean a(String str) {
        String str2 = this.f6162a;
        df.a((Object) str2, "stringPattern");
        return (str2.length() > 0) && !this.b.matcher(str).find();
    }
}
