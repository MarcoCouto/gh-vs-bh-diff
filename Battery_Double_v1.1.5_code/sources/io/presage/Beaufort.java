package io.presage;

import android.content.Context;
import org.json.JSONObject;

public final class Beaufort {

    /* renamed from: a reason: collision with root package name */
    public static final CamembertauCalvados f6006a = new CamembertauCalvados(0);
    private final Bofavre b;
    private final BleudAuvergnebio c;

    static final /* synthetic */ class AbbayedeTamie extends de implements cx<Throwable, bh> {
        AbbayedeTamie(Mimolette24mois mimolette24mois) {
            super(1, mimolette24mois);
        }

        public final ee a() {
            return dk.a(Mimolette24mois.class);
        }

        public final String b() {
            return "e";
        }

        public final String c() {
            return "e(Ljava/lang/Throwable;)V";
        }

        public final /* bridge */ /* synthetic */ Object a(Object obj) {
            return bh.f6130a;
        }
    }

    static final class AbbayedeTimadeuc extends dg implements cw<bh> {

        /* renamed from: a reason: collision with root package name */
        public static final AbbayedeTimadeuc f6007a = new AbbayedeTimadeuc();

        AbbayedeTimadeuc() {
            super(0);
        }

        public final /* bridge */ /* synthetic */ Object a_() {
            return bh.f6130a;
        }
    }

    static final /* synthetic */ class AbbayeduMontdesCats extends de implements cx<Throwable, bh> {
        AbbayeduMontdesCats(Mimolette24mois mimolette24mois) {
            super(1, mimolette24mois);
        }

        public final ee a() {
            return dk.a(Mimolette24mois.class);
        }

        public final String b() {
            return "e";
        }

        public final String c() {
            return "e(Ljava/lang/Throwable;)V";
        }

        public final /* bridge */ /* synthetic */ Object a(Object obj) {
            return bh.f6130a;
        }
    }

    static final class AffideliceauChablis extends dg implements cw<bh> {

        /* renamed from: a reason: collision with root package name */
        public static final AffideliceauChablis f6008a = new AffideliceauChablis();

        AffideliceauChablis() {
            super(0);
        }

        public final /* bridge */ /* synthetic */ Object a_() {
            return bh.f6130a;
        }
    }

    public static final class CamembertauCalvados {
        private CamembertauCalvados() {
        }

        public /* synthetic */ CamembertauCalvados(byte b) {
            this();
        }

        public static Beaufort a(Context context) {
            BoulettedAvesnes boulettedAvesnes = BoulettedAvesnes.f6017a;
            return new Beaufort(BoulettedAvesnes.a(context), new BleudAuvergnebio(context), 0);
        }
    }

    static final /* synthetic */ class CamembertdeNormandie extends de implements cx<Throwable, bh> {
        CamembertdeNormandie(Mimolette24mois mimolette24mois) {
            super(1, mimolette24mois);
        }

        public final ee a() {
            return dk.a(Mimolette24mois.class);
        }

        public final String b() {
            return "e";
        }

        public final String c() {
            return "e(Ljava/lang/Throwable;)V";
        }

        public final /* bridge */ /* synthetic */ Object a(Object obj) {
            return bh.f6130a;
        }
    }

    static final class EcirdelAubrac extends dg implements cw<bh> {

        /* renamed from: a reason: collision with root package name */
        public static final EcirdelAubrac f6009a = new EcirdelAubrac();

        EcirdelAubrac() {
            super(0);
        }

        public final /* bridge */ /* synthetic */ Object a_() {
            return bh.f6130a;
        }
    }

    private Beaufort(Bofavre bofavre, BleudAuvergnebio bleudAuvergnebio) {
        this.b = bofavre;
        this.c = bleudAuvergnebio;
    }

    public /* synthetic */ Beaufort(Bofavre bofavre, BleudAuvergnebio bleudAuvergnebio, byte b2) {
        this(bofavre, bleudAuvergnebio);
    }

    public final void a(VacherinSuisse vacherinSuisse) {
        if (vacherinSuisse instanceof Abondance) {
            a((Abondance) vacherinSuisse);
        } else if (vacherinSuisse instanceof Bethmale) {
            a((Bethmale) vacherinSuisse);
        } else {
            if (vacherinSuisse instanceof VacherinduhautDoubsMontdOr) {
                a((VacherinduhautDoubsMontdOr) vacherinSuisse);
            }
        }
    }

    private final void a(Abondance abondance) {
        this.b.a(b(abondance)).a((cx<? super Throwable, bh>) new AbbayedeTamie<Object,bh>(Mimolette24mois.f6066a)).a((cw<bh>) AbbayedeTimadeuc.f6007a);
    }

    private static String b(Abondance abondance) {
        StringBuilder sb = new StringBuilder("{\"content\":[{\"type\":\"");
        sb.append(abondance.f());
        sb.append("\",\"timestamp_diff\":0}]}");
        return sb.toString();
    }

    private final void a(Bethmale bethmale) {
        this.b.b(b(bethmale)).a((cx<? super Throwable, bh>) new AbbayeduMontdesCats<Object,bh>(Mimolette24mois.f6066a)).a((cw<bh>) AffideliceauChablis.f6008a);
    }

    private final JSONObject b(Bethmale bethmale) {
        BriedeMelun a2 = bethmale.a();
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("event", bethmale.f());
        jSONObject.put("campaign", a2.g());
        jSONObject.put("advertiser", a2.f());
        jSONObject.put("advert", a2.b());
        jSONObject.put("ad_unit_id", a2.l().a());
        JSONObject a3 = this.c.a();
        a3.put("content", jSONObject);
        return a3;
    }

    private final void a(VacherinduhautDoubsMontdOr vacherinduhautDoubsMontdOr) {
        this.b.c(b(vacherinduhautDoubsMontdOr)).a((cx<? super Throwable, bh>) new CamembertdeNormandie<Object,bh>(Mimolette24mois.f6066a)).a((cw<bh>) EcirdelAubrac.f6009a);
    }

    private final JSONObject b(VacherinduhautDoubsMontdOr vacherinduhautDoubsMontdOr) {
        BriedeMelun a2 = vacherinduhautDoubsMontdOr.a();
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("campaign_id", a2.g());
        jSONObject.put("advert_id", a2.b());
        jSONObject.put("advertiser_id", a2.f());
        jSONObject.put("ad_unit_id", a2.l().a());
        jSONObject.put("url", vacherinduhautDoubsMontdOr.b());
        jSONObject.put("source", vacherinduhautDoubsMontdOr.c());
        if (vacherinduhautDoubsMontdOr.d() != null) {
            jSONObject.put("tracker_pattern", vacherinduhautDoubsMontdOr.d());
        }
        if (vacherinduhautDoubsMontdOr.e() != null) {
            jSONObject.put("tracker_url", vacherinduhautDoubsMontdOr.e());
        }
        JSONObject a3 = this.c.a();
        a3.put("content", jSONObject);
        return a3;
    }
}
