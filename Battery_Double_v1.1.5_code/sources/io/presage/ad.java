package io.presage;

public final class ad extends am {

    /* renamed from: a reason: collision with root package name */
    private final Throwable f6112a;

    /* JADX INFO: used method not loaded: io.presage.df.a(java.lang.Object, java.lang.Object):null, types can be incorrect */
    /* JADX WARNING: Code restructure failed: missing block: B:4:0x0010, code lost:
        if (io.presage.df.a((java.lang.Object) r1.f6112a, (java.lang.Object) ((io.presage.ad) r2).f6112a) != false) goto L_0x0015;
     */
    public final boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof ad) {
            }
            return false;
        }
        return true;
    }

    public final int hashCode() {
        Throwable th = this.f6112a;
        if (th != null) {
            return th.hashCode();
        }
        return 0;
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder("ErrorResponse(exception=");
        sb.append(this.f6112a);
        sb.append(")");
        return sb.toString();
    }

    public ad(Throwable th) {
        super(0);
        this.f6112a = th;
    }

    public final Throwable a() {
        return this.f6112a;
    }
}
