package io.presage;

public final class DelicedesFiouves {

    /* renamed from: a reason: collision with root package name */
    private final String f6047a;
    private final boolean b;

    public DelicedesFiouves(String str, boolean z) {
        this.f6047a = str;
        this.b = z;
    }

    public final String a() {
        return this.f6047a;
    }

    public final boolean b() {
        return this.b;
    }
}
