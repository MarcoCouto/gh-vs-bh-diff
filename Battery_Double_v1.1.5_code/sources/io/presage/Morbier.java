package io.presage;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.os.Process;

public final class Morbier {
    public static final boolean a(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 4);
            String b = b(context);
            String str = packageInfo.applicationInfo.processName;
            if (b == null) {
                return true;
            }
            return df.a((Object) b, (Object) str);
        } catch (Exception unused) {
            return true;
        }
    }

    private static final String b(Context context) {
        int myPid = Process.myPid();
        Object systemService = context.getSystemService("activity");
        if (systemService != null) {
            for (RunningAppProcessInfo runningAppProcessInfo : ((ActivityManager) systemService).getRunningAppProcesses()) {
                if (runningAppProcessInfo.pid == myPid) {
                    return runningAppProcessInfo.processName;
                }
            }
            return null;
        }
        throw new be("null cannot be cast to non-null type android.app.ActivityManager");
    }
}
