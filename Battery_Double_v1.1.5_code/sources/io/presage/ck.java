package io.presage;

public final class ck {
    private static final int b(int i, int i2) {
        int i3 = i % i2;
        return i3 >= 0 ? i3 : i3 + i2;
    }

    private static final int a(int i, int i2, int i3) {
        return b(b(i, 1) - b(i2, 1), 1);
    }

    public static final int a(int i, int i2) {
        return i >= i2 ? i2 : i2 - a(i2, i, 1);
    }
}
