package io.presage;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build.VERSION;
import android.telephony.TelephonyManager;
import com.facebook.places.model.PlaceFields;
import java.util.Locale;
import java.util.MissingResourceException;

public final class CoeurdArras {

    /* renamed from: a reason: collision with root package name */
    public static final CamembertauCalvados f6039a = new CamembertauCalvados(0);
    private final FourmedAmbert b = io.presage.FourmedAmbert.CamembertauCalvados.a(this.c);
    private final Context c;

    public static final class CamembertauCalvados {
        private CamembertauCalvados() {
        }

        public /* synthetic */ CamembertauCalvados(byte b) {
            this();
        }
    }

    public CoeurdArras(Context context) {
        this.c = context;
        io.presage.FourmedAmbert.CamembertauCalvados camembertauCalvados = FourmedAmbert.f6057a;
    }

    public final Context e() {
        return this.c;
    }

    public final boolean a(String str) {
        return MoelleuxduRevard.a(this.c, str);
    }

    public final String a() {
        return this.b.i();
    }

    public final String b() {
        if (VERSION.SDK_INT >= 24) {
            Resources resources = this.c.getResources();
            df.a((Object) resources, "context.resources");
            Configuration configuration = resources.getConfiguration();
            df.a((Object) configuration, "context.resources.configuration");
            Locale locale = configuration.getLocales().get(0);
            df.a((Object) locale, "context.resources.configuration.locales[0]");
            String language = locale.getLanguage();
            df.a((Object) language, "context.resources.config…ation.locales[0].language");
            return language;
        }
        Resources resources2 = this.c.getResources();
        df.a((Object) resources2, "context.resources");
        Locale locale2 = resources2.getConfiguration().locale;
        df.a((Object) locale2, "context.resources.configuration.locale");
        String language2 = locale2.getLanguage();
        df.a((Object) language2, "context.resources.configuration.locale.language");
        return language2;
    }

    public final String c() {
        String f = f();
        if (f == null || f.length() != 3) {
            return g();
        }
        return f;
    }

    private final String f() {
        try {
            Object systemService = this.c.getSystemService(PlaceFields.PHONE);
            if (systemService != null) {
                return new Locale("", ((TelephonyManager) systemService).getNetworkCountryIso()).getISO3Country();
            }
            throw new be("null cannot be cast to non-null type android.telephony.TelephonyManager");
        } catch (Throwable unused) {
            return null;
        }
    }

    private final String g() {
        try {
            if (VERSION.SDK_INT >= 24) {
                Resources resources = this.c.getResources();
                df.a((Object) resources, "context.resources");
                Configuration configuration = resources.getConfiguration();
                df.a((Object) configuration, "context.resources.configuration");
                Locale locale = configuration.getLocales().get(0);
                df.a((Object) locale, "context.resources.configuration.locales[0]");
                String iSO3Country = locale.getISO3Country();
                df.a((Object) iSO3Country, "context.resources.config…on.locales[0].isO3Country");
                return iSO3Country;
            }
            Resources resources2 = this.c.getResources();
            df.a((Object) resources2, "context.resources");
            Locale locale2 = resources2.getConfiguration().locale;
            df.a((Object) locale2, "context.resources.configuration.locale");
            String iSO3Country2 = locale2.getISO3Country();
            df.a((Object) iSO3Country2, "context.resources.configuration.locale.isO3Country");
            return iSO3Country2;
        } catch (MissingResourceException unused) {
            return "ZZZ";
        }
    }

    public final String d() {
        StringBuilder sb = new StringBuilder("3.2.2-moat/");
        sb.append(a());
        sb.append("/");
        sb.append(VERSION.RELEASE);
        return sb.toString();
    }
}
