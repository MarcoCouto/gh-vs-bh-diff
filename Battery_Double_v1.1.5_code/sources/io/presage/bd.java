package io.presage;

import java.io.Serializable;

final class bd<T> implements ay<T>, Serializable {

    /* renamed from: a reason: collision with root package name */
    private cw<? extends T> f6128a;
    private volatile Object b;
    private final Object c;

    private bd(cw<? extends T> cwVar) {
        this.f6128a = cwVar;
        this.b = bf.f6129a;
        this.c = this;
    }

    public /* synthetic */ bd(cw cwVar, byte b2) {
        this(cwVar);
    }

    public final T a() {
        T t;
        T t2 = this.b;
        if (t2 != bf.f6129a) {
            return t2;
        }
        synchronized (this.c) {
            t = this.b;
            if (t == bf.f6129a) {
                cw<? extends T> cwVar = this.f6128a;
                if (cwVar == null) {
                    df.a();
                }
                t = cwVar.a_();
                this.b = t;
                this.f6128a = null;
            }
        }
        return t;
    }

    private boolean b() {
        return this.b != bf.f6129a;
    }

    public final String toString() {
        return b() ? String.valueOf(a()) : "Lazy value not initialized yet.";
    }

    private final Object writeReplace() {
        return new aw(a());
    }
}
