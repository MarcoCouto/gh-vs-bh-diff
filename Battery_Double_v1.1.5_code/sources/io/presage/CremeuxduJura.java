package io.presage;

import org.json.JSONObject;

public final class CremeuxduJura {

    /* renamed from: a reason: collision with root package name */
    private final boolean f6043a;
    private final long b;
    private final JSONObject c;
    private final boolean d;
    private final String e;

    public final boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof CremeuxduJura) {
                CremeuxduJura cremeuxduJura = (CremeuxduJura) obj;
                if (this.f6043a == cremeuxduJura.f6043a) {
                    if ((this.b == cremeuxduJura.b) && df.a((Object) this.c, (Object) cremeuxduJura.c)) {
                        if (!(this.d == cremeuxduJura.d) || !df.a((Object) this.e, (Object) cremeuxduJura.e)) {
                            return false;
                        }
                    }
                }
            }
            return false;
        }
        return true;
    }

    public final int hashCode() {
        boolean z = this.f6043a;
        int i = 1;
        if (z) {
            z = true;
        }
        int i2 = (z ? 1 : 0) * true;
        long j = this.b;
        int i3 = (i2 + ((int) (j ^ (j >>> 32)))) * 31;
        JSONObject jSONObject = this.c;
        int i4 = 0;
        int hashCode = (i3 + (jSONObject != null ? jSONObject.hashCode() : 0)) * 31;
        boolean z2 = this.d;
        if (!z2) {
            i = z2;
        }
        int i5 = (hashCode + i) * 31;
        String str = this.e;
        if (str != null) {
            i4 = str.hashCode();
        }
        return i5 + i4;
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder("ProfigRequest(allowRequest=");
        sb.append(this.f6043a);
        sb.append(", jobScheduleWindow=");
        sb.append(this.b);
        sb.append(", request=");
        sb.append(this.c);
        sb.append(", profigEnabled=");
        sb.append(this.d);
        sb.append(", profigHash=");
        sb.append(this.e);
        sb.append(")");
        return sb.toString();
    }

    public CremeuxduJura(boolean z, long j, JSONObject jSONObject, boolean z2, String str) {
        this.f6043a = z;
        this.b = j;
        this.c = jSONObject;
        this.d = z2;
        this.e = str;
    }

    public final boolean a() {
        return this.f6043a;
    }

    public final long b() {
        return this.b;
    }

    public final JSONObject c() {
        return this.c;
    }

    public final boolean d() {
        return this.d;
    }

    public /* synthetic */ CremeuxduJura(long j, JSONObject jSONObject, boolean z) {
        this(false, j, jSONObject, z, null);
    }

    public final String e() {
        return this.e;
    }
}
