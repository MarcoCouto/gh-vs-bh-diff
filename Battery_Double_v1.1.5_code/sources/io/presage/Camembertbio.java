package io.presage;

import org.json.JSONObject;

public final class Camembertbio {

    /* renamed from: a reason: collision with root package name */
    public static final Camembertbio f6030a = new Camembertbio();

    private Camembertbio() {
    }

    public static String a(CancoillotteNature cancoillotteNature, BrillatSavarin brillatSavarin, String str) {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("name", brillatSavarin.a());
        jSONObject.put("ad_sync_type", "load");
        if (brillatSavarin.b() != null) {
            jSONObject.put("ad_unit_id", brillatSavarin.b());
        }
        if (str.length() > 0) {
            jSONObject.put("app_user_id", str);
        }
        jSONObject.put("is_moat_compliant", cancoillotteNature.e());
        jSONObject.put("is_omid_compliant", cancoillotteNature.f());
        jSONObject.put("omid_integration_version", 2);
        JSONObject jSONObject2 = new JSONObject();
        jSONObject2.put("width", cancoillotteNature.g());
        jSONObject2.put("height", cancoillotteNature.h());
        StringBuilder sb = new StringBuilder("{\"connectivity\":\"");
        sb.append(cancoillotteNature.a());
        sb.append("\",\"at\":\"");
        sb.append(cancoillotteNature.b());
        sb.append("\",\"country\":\"");
        sb.append(cancoillotteNature.c());
        sb.append("\",\"build\":30051,\"apps_publishers\":[\"");
        sb.append(cancoillotteNature.d());
        sb.append("\"],\"version\":\"");
        sb.append(cancoillotteNature.i());
        sb.append("\",\"device\":");
        sb.append(jSONObject2);
        sb.append(',');
        sb.append("\"content\":");
        sb.append(jSONObject);
        sb.append('}');
        return sb.toString();
    }
}
