package io.presage;

import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;

public final class ct {
    public static final String a(Reader reader) {
        StringWriter stringWriter = new StringWriter();
        a(reader, stringWriter);
        String stringWriter2 = stringWriter.toString();
        df.a((Object) stringWriter2, "buffer.toString()");
        return stringWriter2;
    }

    /* access modifiers changed from: private */
    public static long a(Reader reader, Writer writer) {
        char[] cArr = new char[8192];
        int read = reader.read(cArr);
        long j = 0;
        while (read >= 0) {
            writer.write(cArr, 0, read);
            j += (long) read;
            read = reader.read(cArr);
        }
        return j;
    }
}
