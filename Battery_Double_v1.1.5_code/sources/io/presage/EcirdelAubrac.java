package io.presage;

public final class EcirdelAubrac implements AbbayedeTamie {

    /* renamed from: a reason: collision with root package name */
    public static final CamembertauCalvados f6049a = new CamembertauCalvados(0);
    /* access modifiers changed from: private */
    public boolean b;
    /* access modifiers changed from: private */
    public cx<? super Throwable, bh> c;
    /* access modifiers changed from: private */
    public final cw<bh> d;

    static final class AbbayedeTamie implements Runnable {

        /* renamed from: a reason: collision with root package name */
        final /* synthetic */ EcirdelAubrac f6050a;
        final /* synthetic */ cw b;

        AbbayedeTamie(EcirdelAubrac ecirdelAubrac, cw cwVar) {
            this.f6050a = ecirdelAubrac;
            this.b = cwVar;
        }

        public final void run() {
            try {
                this.f6050a.d.a_();
                this.f6050a.b(this.b);
            } catch (Throwable th) {
                cx b2 = this.f6050a.c;
                if (b2 != null) {
                    this.f6050a.a(b2, th);
                }
            }
        }
    }

    public static final class CamembertauCalvados {
        private CamembertauCalvados() {
        }

        public /* synthetic */ CamembertauCalvados(byte b) {
            this();
        }

        public static EcirdelAubrac a(cw<bh> cwVar) {
            return new EcirdelAubrac(cwVar, 0);
        }
    }

    static final class CamembertdeNormandie implements Runnable {

        /* renamed from: a reason: collision with root package name */
        final /* synthetic */ EcirdelAubrac f6051a;
        final /* synthetic */ cw b;

        CamembertdeNormandie(EcirdelAubrac ecirdelAubrac, cw cwVar) {
            this.f6051a = ecirdelAubrac;
            this.b = cwVar;
        }

        public final void run() {
            if (!this.f6051a.b) {
                this.b.a_();
            }
        }
    }

    /* renamed from: io.presage.EcirdelAubrac$EcirdelAubrac reason: collision with other inner class name */
    static final class C0134EcirdelAubrac implements Runnable {

        /* renamed from: a reason: collision with root package name */
        final /* synthetic */ EcirdelAubrac f6052a;
        final /* synthetic */ cx b;
        final /* synthetic */ Object c;

        C0134EcirdelAubrac(EcirdelAubrac ecirdelAubrac, cx cxVar, Object obj) {
            this.f6052a = ecirdelAubrac;
            this.b = cxVar;
            this.c = obj;
        }

        public final void run() {
            if (!this.f6052a.b) {
                this.b.a(this.c);
            }
        }
    }

    private EcirdelAubrac(cw<bh> cwVar) {
        this.d = cwVar;
    }

    public /* synthetic */ EcirdelAubrac(cw cwVar, byte b2) {
        this(cwVar);
    }

    public final EcirdelAubrac a(cx<? super Throwable, bh> cxVar) {
        this.c = cxVar;
        return this;
    }

    public final AbbayedeTamie a(cw<bh> cwVar) {
        AffideliceauChablis affideliceauChablis = AffideliceauChablis.b;
        AffideliceauChablis.b().a(new AbbayedeTamie(this, cwVar));
        return this;
    }

    /* access modifiers changed from: private */
    public final void b(cw<bh> cwVar) {
        AffideliceauChablis affideliceauChablis = AffideliceauChablis.b;
        AffideliceauChablis.c().a(new CamembertdeNormandie(this, cwVar));
    }

    /* access modifiers changed from: private */
    public final <R> void a(cx<? super R, bh> cxVar, R r) {
        AffideliceauChablis affideliceauChablis = AffideliceauChablis.b;
        AffideliceauChablis.c().a(new C0134EcirdelAubrac(this, cxVar, r));
    }
}
