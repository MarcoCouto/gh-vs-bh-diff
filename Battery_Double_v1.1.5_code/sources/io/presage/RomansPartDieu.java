package io.presage;

public class RomansPartDieu {

    /* renamed from: a reason: collision with root package name */
    private final String f6088a;
    private final String b;

    public RomansPartDieu(String str, String str2) {
        this.f6088a = str;
        this.b = str2;
    }

    public final String a() {
        return this.f6088a;
    }

    public final String b() {
        return this.b;
    }
}
