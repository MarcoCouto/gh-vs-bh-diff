package io.presage.core.activity;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;

public class SBActivity extends Activity {
    private Handler IIIIIIII;

    /* access modifiers changed from: protected */
    public void onCreate(@Nullable Bundle bundle) {
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        super.onCreate(bundle);
        this.IIIIIIII = new Handler();
        LinearLayout linearLayout = new LinearLayout(this);
        linearLayout.setLayoutParams(new LayoutParams(-1, -1));
        linearLayout.setBackgroundColor(0);
        linearLayout.setClickable(true);
        linearLayout.setOnClickListener(new OnClickListener() {
            public final void onClick(View view) {
                SBActivity.this.finish();
            }
        });
        setContentView(linearLayout);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.IIIIIIII.postDelayed(new Runnable() {
            public final void run() {
                SBActivity.this.finish();
            }
        }, 100);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
    }
}
