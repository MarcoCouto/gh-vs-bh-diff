package io.presage.core;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.AdaptiveIconDrawable;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.VectorDrawable;
import android.os.Build.VERSION;
import java.io.InputStream;

public final class lIIllIlI {

    class IIIIIIII extends lIIlIIIl<Void, Boolean> {
        private InputStream IIIIIIll;
        private boolean IIIIIlII = true;
        private Bitmap IIIIIlIl;
        private Exception IIIIIllI;
        private IIIIIIIl IIIIIlll;

        public IIIIIIII(InputStream inputStream, IIIIIIIl iIIIIIIl) {
            this.IIIIIIll = inputStream;
            this.IIIIIlll = iIIIIIIl;
        }

        private Boolean IIIIIIII() {
            boolean z;
            try {
                this.IIIIIlIl = BitmapFactory.decodeStream(this.IIIIIIll);
                if (this.IIIIIlII) {
                    this.IIIIIIll.close();
                }
                z = true;
            } catch (Exception e) {
                this.IIIIIllI = e;
                z = false;
            }
            return Boolean.valueOf(z);
        }

        /* access modifiers changed from: protected */
        public final /* bridge */ /* synthetic */ Object IIIIIIII(Object[] objArr) {
            return IIIIIIII();
        }

        /* access modifiers changed from: protected */
        public final /* synthetic */ void IIIIIIII(Object obj) {
            if (((Boolean) obj).booleanValue()) {
                lIIllIlI.IIIIIIII(this.IIIIIlll, this.IIIIIlIl);
            }
        }
    }

    public interface IIIIIIIl {
        void IIIIIIII(Bitmap bitmap);
    }

    public class IIIIIIlI extends lIIlIIIl<Void, Boolean> {
        private Context IIIIIIll;
        private Bitmap IIIIIlII;
        private Exception IIIIIlIl;
        private IIIIIIIl IIIIIllI;

        public IIIIIIlI(Context context, IIIIIIIl iIIIIIIl) {
            this.IIIIIIll = context;
            this.IIIIIllI = iIIIIIIl;
        }

        private Boolean IIIIIIII() {
            try {
                Drawable applicationIcon = this.IIIIIIll.getPackageManager().getApplicationIcon(this.IIIIIIll.getPackageName());
                if (VERSION.SDK_INT >= 26 && (applicationIcon instanceof AdaptiveIconDrawable)) {
                    LayerDrawable layerDrawable = new LayerDrawable(new Drawable[]{((AdaptiveIconDrawable) applicationIcon).getBackground(), ((AdaptiveIconDrawable) applicationIcon).getForeground()});
                    this.IIIIIlII = Bitmap.createBitmap(layerDrawable.getIntrinsicWidth(), layerDrawable.getIntrinsicHeight(), Config.ARGB_8888);
                    Canvas canvas = new Canvas(this.IIIIIlII);
                    layerDrawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
                    layerDrawable.draw(canvas);
                } else if (VERSION.SDK_INT < 21 || !(applicationIcon instanceof VectorDrawable)) {
                    this.IIIIIlII = ((BitmapDrawable) applicationIcon).getBitmap();
                } else {
                    this.IIIIIlII = Bitmap.createBitmap(applicationIcon.getIntrinsicWidth(), applicationIcon.getIntrinsicHeight(), Config.ARGB_8888);
                    Canvas canvas2 = new Canvas(this.IIIIIlII);
                    applicationIcon.setBounds(0, 0, canvas2.getWidth(), canvas2.getHeight());
                    applicationIcon.draw(canvas2);
                }
                return Boolean.valueOf(true);
            } catch (Exception e) {
                this.IIIIIlIl = e;
                return Boolean.valueOf(false);
            }
        }

        /* access modifiers changed from: protected */
        public final /* bridge */ /* synthetic */ Object IIIIIIII(Object[] objArr) {
            return IIIIIIII();
        }

        /* access modifiers changed from: protected */
        public final /* synthetic */ void IIIIIIII(Object obj) {
            if (((Boolean) obj).booleanValue()) {
                lIIllIlI.IIIIIIII(this.IIIIIllI, this.IIIIIlII);
            }
        }
    }

    static /* synthetic */ void IIIIIIII(IIIIIIIl iIIIIIIl, Bitmap bitmap) {
        if (iIIIIIIl != null) {
            iIIIIIIl.IIIIIIII(bitmap);
        }
    }

    public final void IIIIIIII(IIlIlllI iIlIlllI, InputStream inputStream, IIIIIIIl iIIIIIIl) {
        try {
            new IIIIIIII(inputStream, iIIIIIIl).IIIIIIII(iIlIlllI, false, new Void[0]);
        } catch (IIlIlIll unused) {
        }
    }
}
