package io.presage.core;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

public final class lIlIIlIl {
    public static final long IIIIIIII(InputStream inputStream, OutputStream outputStream, boolean z, boolean z2) {
        if (inputStream == null || outputStream == null) {
            if (inputStream != null && z) {
                inputStream.close();
            }
            if (outputStream != null && z2) {
                outputStream.close();
            }
            return -1;
        }
        byte[] bArr = new byte[2048];
        int i = 0;
        while (true) {
            try {
                int read = inputStream.read(bArr);
                if (read == -1) {
                    break;
                }
                outputStream.write(bArr, 0, read);
                i += read;
            } finally {
                if (z) {
                    inputStream.close();
                }
                if (z2) {
                    outputStream.close();
                }
            }
        }
        return (long) i;
    }

    public static final byte[] IIIIIIII(InputStream inputStream, long j) {
        if (inputStream == null) {
            return null;
        }
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        long j2 = 0;
        if (j > 0) {
            byte[] bArr = new byte[2048];
            do {
                int read = inputStream.read(bArr);
                if (read == -1) {
                    break;
                }
                new Object[1][0] = Integer.valueOf(read);
                j2 += (long) read;
                byteArrayOutputStream.write(bArr, 0, read);
            } while (j2 < j);
        }
        byteArrayOutputStream.close();
        return byteArrayOutputStream.toByteArray();
    }

    public static final byte[] IIIIIIII(InputStream inputStream, boolean z) {
        if (inputStream == null) {
            return null;
        }
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            byte[] bArr = new byte[2048];
            while (true) {
                int read = inputStream.read(bArr);
                if (read == -1) {
                    break;
                }
                new Object[1][0] = Integer.valueOf(read);
                byteArrayOutputStream.write(bArr, 0, read);
            }
            byteArrayOutputStream.close();
            return byteArrayOutputStream.toByteArray();
        } finally {
            if (z) {
                inputStream.close();
            }
        }
    }
}
