package io.presage.core;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.os.Build.VERSION;
import io.presage.core.lIlIIIlI.IIlIIlII;

public final class lIlIIIII {
    private static final String IIIIIIII = IIlIIlII.IIIIIIIl;
    private static final String IIIIIIIl = IIlIIlII.IIIIIIII;
    private static final String IIIIIIlI = IIlIIlII.IIIIIIlI;

    public static String IIIIIIII() {
        return System.getProperty(IIIIIIlI);
    }

    public static boolean IIIIIIII(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        if (VERSION.SDK_INT >= 23) {
            Network activeNetwork = connectivityManager.getActiveNetwork();
            if (activeNetwork == null) {
                return false;
            }
            NetworkCapabilities networkCapabilities = connectivityManager.getNetworkCapabilities(activeNetwork);
            if (networkCapabilities == null) {
                return false;
            }
            return networkCapabilities.hasTransport(1);
        }
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.getType() == 1;
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x0037 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0038 A[RETURN] */
    public static boolean IIIIIIIl(Context context) {
        boolean z;
        if (!IIIIIIII(context)) {
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
            if (VERSION.SDK_INT >= 23) {
                NetworkCapabilities networkCapabilities = connectivityManager.getNetworkCapabilities(connectivityManager.getActiveNetwork());
                if (networkCapabilities != null) {
                    z = networkCapabilities.hasTransport(0);
                    return z;
                }
            } else {
                NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
                if (activeNetworkInfo != null && activeNetworkInfo.getType() == 0) {
                    z = true;
                    if (z) {
                    }
                }
            }
            z = false;
            if (z) {
            }
        }
    }

    public static String IIIIIIlI(Context context) {
        StringBuilder sb;
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        if (activeNetworkInfo == null) {
            return IIIIIIII;
        }
        if (VERSION.SDK_INT >= 23) {
            Network activeNetwork = connectivityManager.getActiveNetwork();
            if (activeNetwork == null) {
                return IIIIIIII;
            }
            NetworkCapabilities networkCapabilities = connectivityManager.getNetworkCapabilities(activeNetwork);
            if (networkCapabilities == null) {
                return IIIIIIII;
            }
            if (networkCapabilities.hasTransport(1)) {
                return activeNetworkInfo.getTypeName();
            }
            if (!networkCapabilities.hasTransport(0)) {
                return activeNetworkInfo.getTypeName();
            }
            sb = new StringBuilder();
        } else if (activeNetworkInfo.getType() == 1) {
            return activeNetworkInfo.getTypeName();
        } else {
            if (activeNetworkInfo.getType() != 0) {
                return activeNetworkInfo.getTypeName();
            }
            sb = new StringBuilder();
        }
        sb.append(activeNetworkInfo.getTypeName());
        sb.append(" - ");
        sb.append(activeNetworkInfo.getSubtypeName());
        return sb.toString();
    }

    public static String IIIIIIll(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        if (activeNetworkInfo == null) {
            return IIIIIIIl;
        }
        if (VERSION.SDK_INT < 23) {
            return activeNetworkInfo.getType() == 1 ? activeNetworkInfo.getTypeName() : activeNetworkInfo.getType() == 0 ? activeNetworkInfo.getSubtypeName() : activeNetworkInfo.getTypeName();
        }
        Network activeNetwork = connectivityManager.getActiveNetwork();
        if (activeNetwork == null) {
            return IIIIIIIl;
        }
        NetworkCapabilities networkCapabilities = connectivityManager.getNetworkCapabilities(activeNetwork);
        return networkCapabilities == null ? IIIIIIIl : networkCapabilities.hasTransport(1) ? activeNetworkInfo.getTypeName() : networkCapabilities.hasTransport(0) ? activeNetworkInfo.getSubtypeName() : activeNetworkInfo.getTypeName();
    }
}
