package io.presage.core;

import android.app.KeyguardManager;
import android.content.Context;
import android.graphics.Point;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.PowerManager;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import io.presage.core.IIIlIIlI.IIIIIIII;
import io.presage.core.IIIlIIlI.IIIIIIIl;
import io.presage.core.lIlIIIlI.IIIlIlII;

public final class lIIlllIl {
    private static final String IIIIIIII = IIIlIlII.IIIIIIII;
    private static final String IIIIIIIl = IIIlIlII.IIIIIIIl;
    private static final String IIIIIIlI = IIIlIlII.IIIIIIlI;

    public static IIIlIIlI IIIIIIII(Context context) {
        String str = Build.BRAND;
        String str2 = Build.MANUFACTURER;
        String str3 = Build.MODEL;
        String str4 = VERSION.RELEASE;
        int i = VERSION.SDK_INT;
        String property = System.getProperty(IIIIIIII);
        WindowManager windowManager = (WindowManager) context.getSystemService("window");
        DisplayMetrics displayMetrics = new DisplayMetrics();
        windowManager.getDefaultDisplay().getMetrics(displayMetrics);
        Point point = new Point();
        windowManager.getDefaultDisplay().getSize(point);
        IIIlIIlI iIIlIIlI = new IIIlIIlI(str, str2, str3, str4, i, property, new IIIIIIII(displayMetrics.density, Math.min(point.x, point.y), Math.max(point.x, point.y)), new IIIIIIIl(System.getProperty(IIIIIIIl), System.getProperty(IIIIIIlI)));
        return iIIlIIlI;
    }

    public static boolean IIIIIIIl(Context context) {
        return !IIIIIIll(context);
    }

    public static boolean IIIIIIlI(Context context) {
        KeyguardManager keyguardManager = (KeyguardManager) context.getSystemService("keyguard");
        return VERSION.SDK_INT >= 16 ? IIIIIIll(context) && !keyguardManager.isKeyguardLocked() : IIIIIIll(context) && !keyguardManager.inKeyguardRestrictedInputMode();
    }

    private static boolean IIIIIIll(Context context) {
        PowerManager powerManager = (PowerManager) context.getSystemService("power");
        return VERSION.SDK_INT >= 20 ? powerManager.isInteractive() : powerManager.isScreenOn();
    }
}
