package io.presage.core;

import android.support.annotation.NonNull;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

public final class IIlIlllI extends IIlIllIl implements ExecutorService {
    public IIlIlllI(ExecutorService executorService) {
        super(executorService);
    }

    public final boolean awaitTermination(long j, @NonNull TimeUnit timeUnit) {
        return this.IIIIIIII.awaitTermination(j, timeUnit);
    }

    @NonNull
    public final <T> List<Future<T>> invokeAll(@NonNull Collection<? extends Callable<T>> collection) {
        return this.IIIIIIII.invokeAll(collection);
    }

    @NonNull
    public final <T> List<Future<T>> invokeAll(@NonNull Collection<? extends Callable<T>> collection, long j, @NonNull TimeUnit timeUnit) {
        return this.IIIIIIII.invokeAll(collection, j, timeUnit);
    }

    @NonNull
    public final <T> T invokeAny(@NonNull Collection<? extends Callable<T>> collection) {
        return this.IIIIIIII.invokeAny(collection);
    }

    public final <T> T invokeAny(@NonNull Collection<? extends Callable<T>> collection, long j, @NonNull TimeUnit timeUnit) {
        return this.IIIIIIII.invokeAny(collection, j, timeUnit);
    }

    public final boolean isShutdown() {
        return this.IIIIIIII.isShutdown();
    }

    public final boolean isTerminated() {
        return this.IIIIIIII.isTerminated();
    }

    @NonNull
    public final List<Runnable> shutdownNow() {
        return this.IIIIIIII.shutdownNow();
    }

    @NonNull
    public final Future<?> submit(@NonNull Runnable runnable) {
        return this.IIIIIIII.submit(runnable);
    }

    @NonNull
    public final <T> Future<T> submit(@NonNull Runnable runnable, T t) {
        return this.IIIIIIII.submit(runnable, t);
    }

    @NonNull
    public final <T> Future<T> submit(@NonNull Callable<T> callable) {
        return this.IIIIIIII.submit(callable);
    }
}
