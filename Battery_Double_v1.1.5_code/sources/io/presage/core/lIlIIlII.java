package io.presage.core;

import android.os.StatFs;
import java.io.File;
import java.util.Arrays;
import java.util.Comparator;

public final class lIlIIlII {
    public static long IIIIIIII = 10485760;

    public static void IIIIIIII(File file) {
        if (file != null && file.exists()) {
            if (file.isDirectory()) {
                File[] listFiles = file.listFiles();
                if (listFiles != null) {
                    for (File IIIIIIII2 : listFiles) {
                        IIIIIIII(IIIIIIII2);
                    }
                }
                return;
            }
            file.delete();
        }
    }

    public static void IIIIIIII(File[] fileArr, final boolean z) {
        if (fileArr != null) {
            Arrays.sort(fileArr, new Comparator<File>() {
                public final /* synthetic */ int compare(Object obj, Object obj2) {
                    File file = (File) obj;
                    File file2 = (File) obj2;
                    if (file.lastModified() == file2.lastModified()) {
                        return 0;
                    }
                    int i = file.lastModified() > file2.lastModified() ? 1 : -1;
                    if (z) {
                        i *= -1;
                    }
                    return i;
                }
            });
        }
    }

    public static long IIIIIIIl(File file) {
        if (file == null) {
            return -1;
        }
        StatFs statFs = new StatFs(file.getAbsolutePath());
        return ((long) statFs.getAvailableBlocks()) * ((long) statFs.getBlockSize());
    }
}
