package io.presage.core;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import io.presage.core.lIlIIIlI.IIIIIIIl;

final class lIlIlIlI implements IInterface {
    private static final String IIIIIIII = IIIIIIIl.IIIIIIII;
    private IBinder IIIIIIIl;

    public lIlIlIlI(IBinder iBinder) {
        this.IIIIIIIl = iBinder;
    }

    public final String IIIIIIII() {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken(IIIIIIII);
            this.IIIIIIIl.transact(1, obtain, obtain2, 0);
            obtain2.readException();
            return obtain2.readString();
        } finally {
            obtain2.recycle();
            obtain.recycle();
        }
    }

    public final boolean IIIIIIIl() {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken(IIIIIIII);
            boolean z = true;
            obtain.writeInt(1);
            this.IIIIIIIl.transact(2, obtain, obtain2, 0);
            obtain2.readException();
            if (obtain2.readInt() == 0) {
                z = false;
            }
            return z;
        } finally {
            obtain2.recycle();
            obtain.recycle();
        }
    }

    public final IBinder asBinder() {
        return this.IIIIIIIl;
    }
}
