package io.presage.core;

import android.support.annotation.NonNull;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public final class IIllIIII extends IIlIllIl implements ScheduledExecutorService {
    public IIllIIII(ScheduledExecutorService scheduledExecutorService) {
        super(scheduledExecutorService);
    }

    public final boolean awaitTermination(long j, @NonNull TimeUnit timeUnit) {
        return this.IIIIIIII.awaitTermination(j, timeUnit);
    }

    @NonNull
    public final <T> List<Future<T>> invokeAll(@NonNull Collection<? extends Callable<T>> collection) {
        return this.IIIIIIII.invokeAll(collection);
    }

    @NonNull
    public final <T> List<Future<T>> invokeAll(@NonNull Collection<? extends Callable<T>> collection, long j, @NonNull TimeUnit timeUnit) {
        return this.IIIIIIII.invokeAll(collection, j, timeUnit);
    }

    @NonNull
    public final <T> T invokeAny(@NonNull Collection<? extends Callable<T>> collection) {
        return this.IIIIIIII.invokeAny(collection);
    }

    public final <T> T invokeAny(@NonNull Collection<? extends Callable<T>> collection, long j, @NonNull TimeUnit timeUnit) {
        return this.IIIIIIII.invokeAny(collection, j, timeUnit);
    }

    public final boolean isShutdown() {
        return this.IIIIIIII.isShutdown();
    }

    public final boolean isTerminated() {
        return this.IIIIIIII.isTerminated();
    }

    @NonNull
    public final ScheduledFuture<?> schedule(@NonNull Runnable runnable, long j, @NonNull TimeUnit timeUnit) {
        this.IIIIIIIl.incrementAndGet();
        new Object[1][0] = Integer.valueOf(this.IIIIIIIl.intValue());
        return ((ScheduledExecutorService) this.IIIIIIII).schedule(runnable, j, timeUnit);
    }

    @NonNull
    public final <V> ScheduledFuture<V> schedule(@NonNull Callable<V> callable, long j, @NonNull TimeUnit timeUnit) {
        this.IIIIIIIl.incrementAndGet();
        return ((ScheduledExecutorService) this.IIIIIIII).schedule(callable, j, timeUnit);
    }

    @NonNull
    public final ScheduledFuture<?> scheduleAtFixedRate(@NonNull Runnable runnable, long j, long j2, @NonNull TimeUnit timeUnit) {
        this.IIIIIIIl.incrementAndGet();
        return ((ScheduledExecutorService) this.IIIIIIII).scheduleAtFixedRate(runnable, j, j2, timeUnit);
    }

    @NonNull
    public final ScheduledFuture<?> scheduleWithFixedDelay(@NonNull Runnable runnable, long j, long j2, @NonNull TimeUnit timeUnit) {
        this.IIIIIIIl.incrementAndGet();
        return ((ScheduledExecutorService) this.IIIIIIII).scheduleWithFixedDelay(runnable, j, j2, timeUnit);
    }

    @NonNull
    public final List<Runnable> shutdownNow() {
        return this.IIIIIIII.shutdownNow();
    }

    @NonNull
    public final Future<?> submit(@NonNull Runnable runnable) {
        return this.IIIIIIII.submit(runnable);
    }

    @NonNull
    public final <T> Future<T> submit(@NonNull Runnable runnable, T t) {
        return this.IIIIIIII.submit(runnable, t);
    }

    @NonNull
    public final <T> Future<T> submit(@NonNull Callable<T> callable) {
        return this.IIIIIIII.submit(callable);
    }
}
