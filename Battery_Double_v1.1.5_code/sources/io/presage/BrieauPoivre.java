package io.presage;

import android.content.Context;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.mintegral.msdk.base.entity.CampaignUnit;
import io.presage.common.SdkType;
import io.presage.common.SdkType.CamembertauCalvados;
import java.util.Map;

public final class BrieauPoivre extends BleudeLaqueuille {

    /* renamed from: a reason: collision with root package name */
    private final Context f6024a;
    private final CoeurdArras b;
    private final Cheddar c;
    private final SdkType d;

    public /* synthetic */ BrieauPoivre(Context context) {
        CoeurdArras coeurdArras = new CoeurdArras(context);
        Cheddar cheddar = new Cheddar(context);
        Dauphin dauphin = Dauphin.f6045a;
        CamembertauCalvados camembertauCalvados = SdkType.f6143a;
        this(context, coeurdArras, cheddar, dauphin, CamembertauCalvados.a(context));
    }

    private BrieauPoivre(Context context, CoeurdArras coeurdArras, Cheddar cheddar, Dauphin dauphin, SdkType sdkType) {
        super(context, coeurdArras, dauphin);
        this.f6024a = context;
        this.b = coeurdArras;
        this.c = cheddar;
        this.d = sdkType;
    }

    public final Map<String, String> a() {
        Map<String, String> a2 = super.a();
        StringBuilder sb = new StringBuilder(RequestParameters.LEFT_BRACKETS);
        sb.append(this.b.a());
        sb.append(']');
        a2.put("Api-Key", sb.toString());
        a2.put("Sdk-Version", "[3.2.2-moat]");
        a2.put("Timezone", Cheddar.e());
        a2.put("Connectivity", this.c.i());
        a2.put("Sdk-Version-Type", CampaignUnit.JSON_KEY_ADS);
        a2.put("Sdk-Type", String.valueOf(this.d.a()));
        return a2;
    }
}
