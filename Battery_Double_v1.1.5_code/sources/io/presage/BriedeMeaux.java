package io.presage;

import java.util.Arrays;

public final class BriedeMeaux {

    /* renamed from: a reason: collision with root package name */
    public static final BriedeMeaux f6025a = new BriedeMeaux();
    private static String b = "https://%s-%s.presage.io/%s";

    private BriedeMeaux() {
    }

    public static String a() {
        return a("ad_sync", "sy", "v1");
    }

    public static String b() {
        return a("pl", "pl", "v2");
    }

    public static String a(boolean z) {
        String a2 = a(TtmlNode.TAG_P, "pad", "v3");
        return z ? en.b(a2, "https://", "http://") : a2;
    }

    public static String c() {
        return a("track", "tr", "v1");
    }

    public static String d() {
        return a("ad_history", "ah", "v1");
    }

    private static String a(String str, String str2, String str3) {
        dm dmVar = dm.f6156a;
        StringBuilder sb = new StringBuilder();
        sb.append(b);
        sb.append('/');
        sb.append(str);
        String format = String.format(sb.toString(), Arrays.copyOf(new Object[]{str2, str3, str3}, 3));
        df.a((Object) format, "java.lang.String.format(format, *args)");
        return format;
    }
}
