package io.presage;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public final class StVincentauChablis {

    /* renamed from: a reason: collision with root package name */
    private final Map<String, x> f6098a;
    private final Map<String, TommedeYenne> b;
    private final t c;

    public StVincentauChablis(Map<String, x> map, Map<String, TommedeYenne> map2, t tVar) {
        this.f6098a = map;
        this.b = map2;
        this.c = tVar;
    }

    private final List<x> d() {
        List<x> arrayList = new ArrayList<>();
        for (x xVar : this.f6098a.values()) {
            TommedeYenne tommedeYenne = (TommedeYenne) this.b.get(StMarcellin.b(xVar));
            if (tommedeYenne != null && tommedeYenne.d()) {
                arrayList.add(xVar);
            }
        }
        return arrayList;
    }

    public final void a() {
        for (x c2 : d()) {
            this.c.c(c2);
        }
    }

    public final void a(String str, boolean z, boolean z2, String str2, String str3) {
        for (x xVar : d()) {
            Reblochon reblochon = Reblochon.f6086a;
            z.a(xVar, Reblochon.a(str, z, z2, str2, str3));
        }
    }

    public final void b() {
        for (x xVar : d()) {
            Reblochon reblochon = Reblochon.f6086a;
            z.a(xVar, Reblochon.b());
        }
    }

    public final void c() {
        for (x d : d()) {
            this.c.d(d);
        }
    }

    public static void a(x xVar, String str, String str2) {
        Reblochon reblochon = Reblochon.f6086a;
        z.a(xVar, Reblochon.b(str, str2));
    }
}
