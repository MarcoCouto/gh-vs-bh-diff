package io.presage.mraid.browser.listeners;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import io.presage.TrouduCru;
import io.presage.df;

public final class OrientationListener$1 extends BroadcastReceiver {

    /* renamed from: a reason: collision with root package name */
    final /* synthetic */ TrouduCru f6178a;

    public OrientationListener$1(TrouduCru trouduCru) {
        this.f6178a = trouduCru;
    }

    public final void onReceive(Context context, Intent intent) {
        if (df.a((Object) "android.intent.action.CONFIGURATION_CHANGED", (Object) intent.getAction())) {
            Resources resources = context.getResources();
            df.a((Object) resources, "context.resources");
            int i = resources.getConfiguration().orientation;
            if (this.f6178a.f6108a != i) {
                this.f6178a.f6108a = i;
                this.f6178a.c();
            }
        }
    }
}
