package io.presage.mraid.browser.listeners;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import io.presage.TommeduJura;
import io.presage.df;

public final class CloseSystemDialogsListener$1 extends BroadcastReceiver {

    /* renamed from: a reason: collision with root package name */
    final /* synthetic */ TommeduJura f6177a;

    public CloseSystemDialogsListener$1(TommeduJura tommeduJura) {
        this.f6177a = tommeduJura;
    }

    public final void onReceive(Context context, Intent intent) {
        if (df.a((Object) "android.intent.action.CLOSE_SYSTEM_DIALOGS", (Object) intent.getAction()) || df.a((Object) "android.intent.action.SCREEN_OFF", (Object) intent.getAction())) {
            this.f6177a.c();
        }
    }
}
