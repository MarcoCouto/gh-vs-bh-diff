package io.presage.mraid.browser;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.FrameLayout;
import android.widget.Toast;
import io.fabric.sdk.android.services.settings.SettingsJsonConstants;
import io.presage.EcirdelAubrac;
import io.presage.Taleggio;
import io.presage.TetedeMoine;
import io.presage.TommeCrayeuse;
import io.presage.bh;
import io.presage.cw;
import io.presage.dg;
import io.presage.q;

public class ShortcutActivity extends Activity {

    /* renamed from: a reason: collision with root package name */
    public static final CamembertauCalvados f6174a = new CamembertauCalvados(0);
    private Taleggio b;

    public static final class CamembertauCalvados {

        /* renamed from: io.presage.mraid.browser.ShortcutActivity$CamembertauCalvados$CamembertauCalvados reason: collision with other inner class name */
        static final class C0136CamembertauCalvados extends dg implements cw<bh> {

            /* renamed from: a reason: collision with root package name */
            final /* synthetic */ Context f6175a;
            final /* synthetic */ q b;

            C0136CamembertauCalvados(Context context, q qVar) {
                this.f6175a = context;
                this.b = qVar;
                super(0);
            }

            public final /* synthetic */ Object a_() {
                b();
                return bh.f6130a;
            }

            private void b() {
                new TetedeMoine(this.f6175a, this.b).a();
            }
        }

        static final class CamembertdeNormandie extends dg implements cw<bh> {

            /* renamed from: a reason: collision with root package name */
            public static final CamembertdeNormandie f6176a = new CamembertdeNormandie();

            CamembertdeNormandie() {
                super(0);
            }

            public final /* bridge */ /* synthetic */ Object a_() {
                return bh.f6130a;
            }
        }

        private CamembertauCalvados() {
        }

        public /* synthetic */ CamembertauCalvados(byte b) {
            this();
        }

        public static void a(Context context, q qVar) {
            io.presage.EcirdelAubrac.CamembertauCalvados camembertauCalvados = EcirdelAubrac.f6049a;
            io.presage.EcirdelAubrac.CamembertauCalvados.a(new C0136CamembertauCalvados(context, qVar)).a((cw<bh>) CamembertdeNormandie.f6176a);
        }
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0037, code lost:
        if (r2 == null) goto L_0x0039;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x001f, code lost:
        if (r1 == null) goto L_0x0021;
     */
    public void onCreate(Bundle bundle) {
        String str;
        String str2;
        super.onCreate(bundle);
        Context context = this;
        FrameLayout frameLayout = new FrameLayout(context);
        Intent intent = getIntent();
        if (intent != null) {
            Bundle extras = intent.getExtras();
            if (extras != null) {
                str = extras.getString("args", "");
            }
        }
        str = "";
        Intent intent2 = getIntent();
        if (intent2 != null) {
            Bundle extras2 = intent2.getExtras();
            if (extras2 != null) {
                str2 = extras2.getString(SettingsJsonConstants.APP_IDENTIFIER_KEY, "");
            }
        }
        str2 = "";
        Taleggio taleggio = new Taleggio(new TommeCrayeuse(context), this);
        this.b = taleggio;
        if (!taleggio.a(str, str2, frameLayout)) {
            Toast.makeText(context, "Invalid shortcut", 0).show();
            finish();
            return;
        }
        setContentView(frameLayout);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        Taleggio taleggio = this.b;
        if (taleggio != null) {
            taleggio.a();
        }
    }
}
