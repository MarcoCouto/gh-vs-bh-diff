package io.presage;

import com.mintegral.msdk.base.entity.CampaignEx;
import io.presage.common.network.models.RewardItem;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.json.JSONArray;
import org.json.JSONObject;

public final class BriquetteduNord {

    /* renamed from: a reason: collision with root package name */
    public static final BriquetteduNord f6028a = new BriquetteduNord();

    private BriquetteduNord() {
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0085, code lost:
        if (r7 == null) goto L_0x0087;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0094, code lost:
        if (r7 == null) goto L_0x0096;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0068, code lost:
        if (r7 == null) goto L_0x006a;
     */
    public static CapGrisNez a(String str, String str2) {
        String str3;
        String str4;
        String str5;
        JSONObject jSONObject = new JSONObject(str);
        List arrayList = new ArrayList();
        JSONArray optJSONArray = jSONObject.optJSONArray("ad");
        if (optJSONArray == null) {
            optJSONArray = new JSONArray();
        }
        int length = optJSONArray.length();
        for (int i = 0; i < length; i++) {
            BriedeMelun briedeMelun = new BriedeMelun();
            JSONObject jSONObject2 = optJSONArray.getJSONObject(i);
            JSONObject optJSONObject = jSONObject2.optJSONObject("format");
            String optString = jSONObject2.optString("ad_content");
            df.a((Object) optString, "adJson.optString(\"ad_content\")");
            briedeMelun.c(optString);
            String optString2 = jSONObject2.optString(CampaignEx.JSON_KEY_IMPRESSION_URL);
            df.a((Object) optString2, "adJson.optString(\"impression_url\")");
            briedeMelun.e(optString2);
            String optString3 = jSONObject2.optString("id");
            df.a((Object) optString3, "adJson.optString(\"id\")");
            briedeMelun.b(optString3);
            JSONObject optJSONObject2 = jSONObject2.optJSONObject("advertiser");
            if (optJSONObject2 != null) {
                str3 = optJSONObject2.optString("id");
            }
            str3 = "";
            briedeMelun.f(str3);
            String optString4 = jSONObject2.optString("campaign_id");
            df.a((Object) optString4, "adJson.optString(\"campaign_id\")");
            briedeMelun.g(optString4);
            if (optJSONObject != null) {
                str4 = optJSONObject.optString("webview_base_url");
            }
            str4 = "";
            briedeMelun.h(str4);
            if (optJSONObject != null) {
                str5 = optJSONObject.optString("mraid_download_url");
            }
            str5 = "";
            briedeMelun.k(str5);
            briedeMelun.b(jSONObject2.optBoolean("moat", false));
            briedeMelun.c(jSONObject2.optBoolean("omid", false));
            briedeMelun.d(jSONObject2.optBoolean("is_video", false));
            briedeMelun.a(a(jSONObject2.optJSONObject("ad_unit"), jSONObject2.optString("id")));
            df.a((Object) jSONObject2, "adJson");
            briedeMelun.d(a("orientation", jSONObject2));
            briedeMelun.j(a(optJSONObject != null ? optJSONObject.optJSONArray("params") : null));
            briedeMelun.i(a(jSONObject2));
            briedeMelun.a(jSONObject2.optBoolean("has_transparency", false));
            String optString5 = jSONObject2.optString("sdk_close_button_url", "");
            df.a((Object) optString5, "adJson.optString(\"sdk_close_button_url\", \"\")");
            briedeMelun.l(optString5);
            String optString6 = jSONObject2.optString("landing_page_prefetch_url", "");
            df.a((Object) optString6, "adJson.optString(\"landing_page_prefetch_url\", \"\")");
            briedeMelun.m(optString6);
            briedeMelun.e(jSONObject2.optBoolean("landing_page_disable_javascript", false));
            String optString7 = jSONObject2.optString("landing_page_prefetch_whitelist", "");
            df.a((Object) optString7, "adJson.optString(\"landin…_prefetch_whitelist\", \"\")");
            briedeMelun.n(optString7);
            StringBuilder sb = new StringBuilder();
            sb.append(UUID.randomUUID().toString());
            sb.append(briedeMelun.g());
            briedeMelun.a(sb.toString());
            if (df.a((Object) briedeMelun.l().c(), (Object) str2)) {
                arrayList.add(briedeMelun);
            }
        }
        return new CapGrisNez(arrayList);
    }

    private static CarreMirabelle a(JSONObject jSONObject, String str) {
        CarreMirabelle carreMirabelle = new CarreMirabelle();
        if (jSONObject == null) {
            return carreMirabelle;
        }
        String optString = jSONObject.optString("id");
        if (optString == null) {
            optString = "";
        }
        carreMirabelle.a(optString);
        if (str == null) {
            str = "";
        }
        carreMirabelle.b(str);
        String optString2 = jSONObject.optString("type");
        if (optString2 == null) {
            optString2 = "";
        }
        carreMirabelle.c(optString2);
        if (df.a((Object) carreMirabelle.c(), (Object) "optin_video")) {
            String optString3 = jSONObject.optString("app_user_id");
            if (optString3 == null) {
                optString3 = "";
            }
            carreMirabelle.e(optString3);
            String optString4 = jSONObject.optString("reward_launch");
            if (optString4 == null) {
                optString4 = "";
            }
            carreMirabelle.d(optString4);
            RewardItem e = carreMirabelle.e();
            String optString5 = jSONObject.optString(CampaignEx.JSON_KEY_REWARD_NAME);
            if (optString5 == null) {
                optString5 = "";
            }
            e.setName(optString5);
            RewardItem e2 = carreMirabelle.e();
            String optString6 = jSONObject.optString("reward_value");
            if (optString6 == null) {
                optString6 = "";
            }
            e2.setValue(optString6);
        }
        return carreMirabelle;
    }

    private static String a(JSONObject jSONObject) {
        String optString = jSONObject.optString("client_tracker_pattern", "");
        if (df.a((Object) optString, (Object) "null")) {
            optString = "";
        }
        df.a((Object) optString, "clientTrackerPattern");
        return optString;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0034, code lost:
        if (r4 == null) goto L_0x0036;
     */
    private static String a(JSONArray jSONArray) {
        String str;
        if (jSONArray == null) {
            return "";
        }
        for (int length = jSONArray.length() - 1; length >= 0; length--) {
            JSONObject jSONObject = jSONArray.getJSONObject(length);
            if (df.a((Object) jSONObject.getString("name"), (Object) "zones")) {
                JSONArray optJSONArray = jSONObject.optJSONArray("value");
                if (optJSONArray != null) {
                    JSONObject optJSONObject = optJSONArray.optJSONObject(0);
                    if (optJSONObject != null) {
                        str = optJSONObject.optString("name");
                    }
                }
                str = "";
                return str;
            }
        }
        return "";
    }

    private static String a(String str, JSONObject jSONObject) {
        JSONArray optJSONArray = jSONObject.optJSONArray("params");
        if (optJSONArray == null) {
            return "";
        }
        int length = optJSONArray.length();
        for (int i = 0; i < length; i++) {
            JSONObject jSONObject2 = optJSONArray.getJSONObject(i);
            if (df.a((Object) jSONObject2.getString("name"), (Object) str)) {
                String string = jSONObject2.getString("value");
                df.a((Object) string, "paramElement.getString(\"value\")");
                return string;
            }
        }
        return "";
    }
}
