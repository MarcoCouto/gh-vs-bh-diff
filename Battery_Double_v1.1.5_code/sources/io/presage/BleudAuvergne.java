package io.presage;

import android.content.Context;
import io.presage.common.AdConfig;

public final class BleudAuvergne {

    /* renamed from: a reason: collision with root package name */
    public static final CamembertauCalvados f6011a = new CamembertauCalvados(0);
    /* access modifiers changed from: private */
    public final Bofavre b;
    /* access modifiers changed from: private */
    public final CendreduBeauzac c;

    public static final class CamembertauCalvados {
        private CamembertauCalvados() {
        }

        public /* synthetic */ CamembertauCalvados(byte b) {
            this();
        }
    }

    static final class CamembertdeNormandie extends dg implements cw<CapGrisNez> {

        /* renamed from: a reason: collision with root package name */
        final /* synthetic */ BleudAuvergne f6012a;
        final /* synthetic */ String b;
        final /* synthetic */ AdConfig c;
        final /* synthetic */ String d;

        CamembertdeNormandie(BleudAuvergne bleudAuvergne, String str, AdConfig adConfig, String str2) {
            this.f6012a = bleudAuvergne;
            this.b = str;
            this.c = adConfig;
            this.d = str2;
            super(0);
        }

        /* access modifiers changed from: private */
        /* renamed from: b */
        public CapGrisNez a_() {
            am a2 = this.f6012a.b.a(this.b, this.c, this.d);
            if (a2 instanceof an) {
                BriquetteduNord briquetteduNord = BriquetteduNord.f6028a;
                CapGrisNez a3 = BriquetteduNord.a(((an) a2).a(), this.b);
                Chaource.f6037a.a(a3.a(), this.f6012a.c, this.f6012a.b);
                return a3;
            } else if (a2 instanceof ad) {
                throw ((ad) a2).a();
            } else {
                throw new bc();
            }
        }
    }

    private BleudAuvergne(Bofavre bofavre, CendreduBeauzac cendreduBeauzac) {
        this.b = bofavre;
        this.c = cendreduBeauzac;
    }

    public BleudAuvergne(Context context) {
        BoulettedAvesnes boulettedAvesnes = BoulettedAvesnes.f6017a;
        Bofavre a2 = BoulettedAvesnes.a(context);
        io.presage.CendreduBeauzac.CamembertauCalvados camembertauCalvados = CendreduBeauzac.f6036a;
        this(a2, io.presage.CendreduBeauzac.CamembertauCalvados.a(context));
    }

    public final Aveyronnais<CapGrisNez> a(String str, AdConfig adConfig, String str2) {
        io.presage.Aveyronnais.CamembertauCalvados camembertauCalvados = Aveyronnais.f6003a;
        return io.presage.Aveyronnais.CamembertauCalvados.a(new CamembertdeNormandie(this, str, adConfig, str2));
    }
}
