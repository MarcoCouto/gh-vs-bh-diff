package io.presage;

import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.charset.Charset;
import java.util.Arrays;

class cq extends cp {
    public static final byte[] a(File file) {
        Throwable th;
        Closeable fileInputStream = new FileInputStream(file);
        try {
            FileInputStream fileInputStream2 = (FileInputStream) fileInputStream;
            int i = 0;
            long length = file.length();
            if (length <= 2147483647L) {
                int i2 = (int) length;
                byte[] bArr = new byte[i2];
                while (i2 > 0) {
                    int read = fileInputStream2.read(bArr, i, i2);
                    if (read < 0) {
                        break;
                    }
                    i2 -= read;
                    i += read;
                }
                if (i2 != 0) {
                    bArr = Arrays.copyOf(bArr, i);
                    df.a((Object) bArr, "java.util.Arrays.copyOf(this, newSize)");
                }
                cn.a(fileInputStream, null);
                return bArr;
            }
            StringBuilder sb = new StringBuilder("File ");
            sb.append(file);
            sb.append(" is too big (");
            sb.append(length);
            sb.append(" bytes) to fit in memory.");
            throw new OutOfMemoryError(sb.toString());
        } catch (Throwable th2) {
            cn.a(fileInputStream, th);
            throw th2;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x001b, code lost:
        throw r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0014, code lost:
        r3 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0018, code lost:
        io.presage.cn.a(r0, r2);
     */
    public static final void a(File file, byte[] bArr) {
        Closeable fileOutputStream = new FileOutputStream(file);
        ((FileOutputStream) fileOutputStream).write(bArr);
        bh bhVar = bh.f6130a;
        cn.a(fileOutputStream, null);
    }

    public static final String a(File file, Charset charset) {
        return new String(co.a(file), charset);
    }

    public static final void a(File file, String str, Charset charset) {
        byte[] bytes = str.getBytes(charset);
        df.a((Object) bytes, "(this as java.lang.String).getBytes(charset)");
        co.a(file, bytes);
    }
}
