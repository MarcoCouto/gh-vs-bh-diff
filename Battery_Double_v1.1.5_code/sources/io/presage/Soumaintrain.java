package io.presage;

import android.content.Context;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;

public final class Soumaintrain {

    /* renamed from: a reason: collision with root package name */
    private final Context f6095a;
    private final FrameLayout b;
    private final BriedeMelun c;

    public Soumaintrain(Context context, FrameLayout frameLayout, BriedeMelun briedeMelun) {
        this.f6095a = context;
        this.b = frameLayout;
        this.c = briedeMelun;
    }

    public final x a(TommedAuvergne tommedAuvergne) {
        LayoutParams b2 = a(tommedAuvergne, (LayoutParams) null);
        x a2 = z.a(this.f6095a, this.c);
        if (a2 == null) {
            return null;
        }
        a2.setTag(tommedAuvergne.c());
        StMarcellin.a(a2);
        this.b.addView(a2, b2);
        return a2;
    }

    public static void a(WebView webView, TommedAuvergne tommedAuvergne) {
        ViewGroup.LayoutParams layoutParams = webView.getLayoutParams();
        if (!(layoutParams instanceof LayoutParams)) {
            layoutParams = null;
        }
        webView.setLayoutParams(a(tommedAuvergne, (LayoutParams) layoutParams));
    }

    public final void a(WebView webView) {
        this.b.removeView(webView);
    }

    /* access modifiers changed from: private */
    public static LayoutParams a(TommedAuvergne tommedAuvergne, LayoutParams layoutParams) {
        if (layoutParams == null) {
            layoutParams = new LayoutParams(-1, -1);
        }
        b(tommedAuvergne, layoutParams);
        a(layoutParams, tommedAuvergne);
        return layoutParams;
    }

    private static void b(TommedAuvergne tommedAuvergne, LayoutParams layoutParams) {
        if (tommedAuvergne.g() != -1) {
            layoutParams.leftMargin = Montbriac.b(tommedAuvergne.g());
        }
        if (tommedAuvergne.f() != -1) {
            layoutParams.topMargin = Montbriac.b(tommedAuvergne.f());
        }
    }

    private static void a(LayoutParams layoutParams, TommedAuvergne tommedAuvergne) {
        int i = -1;
        layoutParams.width = tommedAuvergne.e() <= 0 ? -1 : Montbriac.b(tommedAuvergne.e());
        if (tommedAuvergne.d() > 0) {
            i = Montbriac.b(tommedAuvergne.d());
        }
        layoutParams.height = i;
    }
}
