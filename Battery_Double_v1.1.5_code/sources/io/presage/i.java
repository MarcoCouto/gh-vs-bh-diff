package io.presage;

import android.webkit.WebSettings;
import android.webkit.WebView;
import java.util.regex.Pattern;

public final class i implements j {

    /* renamed from: a reason: collision with root package name */
    private h f6165a;
    private final Pattern b = Pattern.compile(this.e.t());
    /* access modifiers changed from: private */
    public boolean c;
    private final x d;
    private final BriedeMelun e;

    public static final class CamembertauCalvados extends f {

        /* renamed from: a reason: collision with root package name */
        final /* synthetic */ i f6166a;

        CamembertauCalvados(i iVar, Pattern pattern) {
            this.f6166a = iVar;
            super(pattern);
        }

        public final void b() {
            this.f6166a.d();
        }

        public final void a(WebView webView, String str) {
            this.f6166a.c = true;
            this.f6166a.d();
        }

        public final void c() {
            this.f6166a.d();
        }
    }

    public i(x xVar, BriedeMelun briedeMelun) {
        this.d = xVar;
        this.e = briedeMelun;
        c();
    }

    private final void c() {
        x xVar = this.d;
        Pattern pattern = this.b;
        df.a((Object) pattern, "whitelistPattern");
        xVar.setClientAdapter(new CamembertauCalvados(this, pattern));
    }

    /* access modifiers changed from: private */
    public final void d() {
        h hVar = this.f6165a;
        if (hVar != null) {
            hVar.a();
        }
        e();
        OlivetauPoivre.d(this.d);
    }

    private final void e() {
        x xVar = this.d;
        Pattern pattern = this.b;
        df.a((Object) pattern, "whitelistPattern");
        xVar.setClientAdapter(new f(pattern));
    }

    public final void a(h hVar) {
        this.f6165a = hVar;
        if (this.e.s()) {
            WebSettings settings = this.d.getSettings();
            df.a((Object) settings, "webView.settings");
            settings.setJavaScriptEnabled(false);
        }
        this.d.loadUrl(this.e.r());
    }

    public final void b() {
        this.f6165a = null;
        e();
        OlivetauPoivre.d(this.d);
    }

    public final boolean a() {
        return this.c;
    }
}
