package io.presage;

import java.io.Serializable;

public abstract class dg<R> implements dd<R>, Serializable {

    /* renamed from: a reason: collision with root package name */
    private final int f6154a;

    public dg(int i) {
        this.f6154a = i;
    }

    public String toString() {
        String a2 = dk.a(this);
        df.a((Object) a2, "Reflection.renderLambdaToString(this)");
        return a2;
    }
}
