package io.presage;

import android.content.Context;
import io.presage.FourmedAmbert.CamembertauCalvados;

public final class FourmedeMontbrison {

    /* renamed from: a reason: collision with root package name */
    public static final FourmedeMontbrison f6060a = new FourmedeMontbrison();
    private static FourmedeHauteLoire b;

    private FourmedeMontbrison() {
    }

    public static void a(FourmedeHauteLoire fourmedeHauteLoire) {
        b = fourmedeHauteLoire;
    }

    public static FourmedeHauteLoire a(Context context) {
        if (b == null) {
            CamembertauCalvados camembertauCalvados = FourmedAmbert.f6057a;
            String d = CamembertauCalvados.a(context).d();
            FourmedeRochefort fourmedeRochefort = FourmedeRochefort.f6061a;
            b = FourmedeRochefort.a(d);
        }
        return b;
    }

    public static void b(Context context) {
        Comte18mois.f6041a.a(context).b(false);
    }
}
