package io.presage;

import java.util.NoSuchElementException;

class bm extends bl {
    public static final <T> boolean a(T[] tArr, T t) {
        return bi.b(tArr, t) >= 0;
    }

    public static final <T> int b(T[] tArr, T t) {
        int i = 0;
        if (t == null) {
            int length = tArr.length;
            while (i < length) {
                if (tArr[i] == null) {
                    return i;
                }
                i++;
            }
        } else {
            int length2 = tArr.length;
            while (i < length2) {
                if (df.a((Object) t, (Object) tArr[i])) {
                    return i;
                }
                i++;
            }
        }
        return -1;
    }

    public static final char a(char[] cArr) {
        switch (cArr.length) {
            case 0:
                throw new NoSuchElementException("Array is empty.");
            case 1:
                return cArr[0];
            default:
                throw new IllegalArgumentException("Array has more than one element.");
        }
    }
}
