package io.presage;

import io.presage.common.network.models.RewardItem;

public final class SaintFelicien extends RomansPartDieu {

    /* renamed from: a reason: collision with root package name */
    private final RewardItem f6091a;

    public SaintFelicien(String str, RewardItem rewardItem) {
        super(str, rewardItem.getName());
        this.f6091a = rewardItem;
    }

    public final RewardItem c() {
        return this.f6091a;
    }
}
