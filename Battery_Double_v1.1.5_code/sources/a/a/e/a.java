package a.a.e;

import java.util.Iterator;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/* compiled from: StartAppSDK */
public class a implements Iterable<Integer> {

    /* renamed from: a reason: collision with root package name */
    private final int f688a;
    private final int b;
    private final int c = 1;

    /* renamed from: a.a.e.a$a reason: collision with other inner class name */
    /* compiled from: StartAppSDK */
    public static final class C0001a {
        public static int a(int i, int i2) {
            return i > i2 ? i2 : i;
        }

        private C0001a() {
        }

        public /* synthetic */ C0001a(byte b) {
            this();
        }
    }

    static {
        new C0001a(0);
    }

    public a(int i, int i2) {
        this.f688a = i;
        this.b = com.startapp.a.a.a.a.a(i, i2);
    }

    public final int a() {
        return this.f688a;
    }

    public final int b() {
        return this.b;
    }

    public boolean c() {
        return this.f688a > this.b;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x001f, code lost:
        if (r2.b == r3.b) goto L_0x0021;
     */
    public boolean equals(@Nullable Object obj) {
        if (obj instanceof a) {
            if (!c() || !((a) obj).c()) {
                a aVar = (a) obj;
                if (this.f688a == aVar.f688a) {
                }
            }
            return true;
        }
        return false;
    }

    public int hashCode() {
        if (c()) {
            return -1;
        }
        return (((this.f688a * 31) + this.b) * 31) + 1;
    }

    @NotNull
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.f688a);
        sb.append("..");
        sb.append(this.b);
        sb.append(" step 1");
        return sb.toString();
    }

    public /* synthetic */ Iterator iterator() {
        return new b(this.f688a, this.b, 1);
    }
}
