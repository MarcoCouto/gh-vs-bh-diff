package a.a.e;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/* compiled from: StartAppSDK */
public final class c extends a {

    /* renamed from: a reason: collision with root package name */
    public static final a f690a = new a(0);
    /* access modifiers changed from: private */
    @NotNull
    public static final c b = new c(1, 0);

    /* compiled from: StartAppSDK */
    public static final class a {
        private a() {
        }

        public /* synthetic */ a(byte b) {
            this();
        }
    }

    public c(int i, int i2) {
        super(i, i2);
    }

    public final boolean c() {
        return a() > b();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0027, code lost:
        if (b() == r3.b()) goto L_0x0029;
     */
    public final boolean equals(@Nullable Object obj) {
        if (obj instanceof c) {
            if (!c() || !((c) obj).c()) {
                c cVar = (c) obj;
                if (a() == cVar.a()) {
                }
            }
            return true;
        }
        return false;
    }

    public final int hashCode() {
        if (c()) {
            return -1;
        }
        return (a() * 31) + b();
    }

    @NotNull
    public final String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(a());
        sb.append("..");
        sb.append(b());
        return sb.toString();
    }
}
