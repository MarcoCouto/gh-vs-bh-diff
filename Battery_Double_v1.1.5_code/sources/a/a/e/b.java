package a.a.e;

import a.a.a.f;
import java.util.NoSuchElementException;

/* compiled from: StartAppSDK */
public final class b extends f {

    /* renamed from: a reason: collision with root package name */
    private final int f689a;
    private boolean b;
    private int c;
    private final int d = 1;

    public b(int i, int i2, int i3) {
        boolean z = true;
        this.f689a = i2;
        if (this.d <= 0 ? i < i2 : i > i2) {
            z = false;
        }
        this.b = z;
        if (!this.b) {
            i = this.f689a;
        }
        this.c = i;
    }

    public final boolean hasNext() {
        return this.b;
    }

    public final int a() {
        int i = this.c;
        if (i != this.f689a) {
            this.c += this.d;
        } else if (this.b) {
            this.b = false;
        } else {
            throw new NoSuchElementException();
        }
        return i;
    }
}
