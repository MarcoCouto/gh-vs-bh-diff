package a.a.c;

import a.a.b.b;
import a.a.d.b.g;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import io.fabric.sdk.android.services.events.EventsFilesManager;
import java.io.Closeable;
import java.util.regex.Pattern;
import org.jetbrains.annotations.Nullable;

/* compiled from: StartAppSDK */
public final class a implements com.startapp.a.a.d.a {

    /* renamed from: a reason: collision with root package name */
    private final Pattern f682a = Pattern.compile("\\+");
    private final Pattern b = Pattern.compile("/");
    private final Pattern c = Pattern.compile(RequestParameters.EQUAL);
    private final Pattern d = Pattern.compile(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
    private final Pattern e = Pattern.compile("\\*");
    private final Pattern f = Pattern.compile("#");

    public static final void a(@Nullable Closeable closeable, @Nullable Throwable th) {
        if (closeable != null) {
            if (th == null) {
                closeable.close();
                return;
            }
            try {
                closeable.close();
            } catch (Throwable th2) {
                g.b(th, "$this$addSuppressed");
                g.b(th2, "exception");
                b.f681a.a(th, th2);
            }
        }
    }

    public final String a(String str) {
        return this.c.matcher(this.b.matcher(this.f682a.matcher(str).replaceAll(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR)).replaceAll("*")).replaceAll("#");
    }
}
