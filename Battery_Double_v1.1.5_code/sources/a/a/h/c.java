package a.a.h;

import a.a.d.b.g;
import com.facebook.internal.FacebookRequestErrorClassification;
import java.io.Serializable;
import java.util.Set;
import java.util.regex.Pattern;
import org.jetbrains.annotations.NotNull;

/* compiled from: StartAppSDK */
public final class c implements Serializable {
    private Set<? extends Object> _options;
    private final Pattern nativePattern;

    /* compiled from: StartAppSDK */
    public static final class a {
        private a() {
        }

        public /* synthetic */ a(byte b) {
            this();
        }

        public static boolean a(char c, char c2, boolean z) {
            if (c == c2) {
                return true;
            }
            if (!z) {
                return false;
            }
            return Character.toUpperCase(c) == Character.toUpperCase(c2) || Character.toLowerCase(c) == Character.toLowerCase(c2);
        }

        public static /* synthetic */ boolean a(String str, String str2) {
            g.b(str, "$this$startsWith");
            g.b(str2, "prefix");
            return str.startsWith(str2);
        }

        public static boolean a(String str, String str2, int i, int i2, boolean z) {
            g.b(str, "$this$regionMatches");
            g.b(str2, FacebookRequestErrorClassification.KEY_OTHER);
            if (!z) {
                return str.regionMatches(0, str2, i, i2);
            }
            return str.regionMatches(z, 0, str2, i, i2);
        }
    }

    /* compiled from: StartAppSDK */
    static final class b implements Serializable {
        private static final long serialVersionUID = 0;
        private final int flags;
        @NotNull
        private final String pattern;

        /* compiled from: StartAppSDK */
        public static final class a {
            private a() {
            }

            public /* synthetic */ a(byte b) {
                this();
            }
        }

        static {
            new a(0);
        }

        public b(@NotNull String str, int i) {
            g.b(str, "pattern");
            this.pattern = str;
            this.flags = i;
        }

        private final Object readResolve() {
            Pattern compile = Pattern.compile(this.pattern, this.flags);
            g.a((Object) compile, "Pattern.compile(pattern, flags)");
            return new c(compile);
        }
    }

    static {
        new a(0);
    }

    public c(@NotNull Pattern pattern) {
        g.b(pattern, "nativePattern");
        this.nativePattern = pattern;
    }

    public c(@NotNull String str) {
        g.b(str, "pattern");
        Pattern compile = Pattern.compile(str);
        g.a((Object) compile, "Pattern.compile(pattern)");
        this(compile);
    }

    public final boolean a(@NotNull CharSequence charSequence) {
        g.b(charSequence, "input");
        return this.nativePattern.matcher(charSequence).matches();
    }

    @NotNull
    public final String a(@NotNull CharSequence charSequence, @NotNull String str) {
        g.b(charSequence, "input");
        g.b(str, "replacement");
        String replaceAll = this.nativePattern.matcher(charSequence).replaceAll(str);
        g.a((Object) replaceAll, "nativePattern.matcher(in…).replaceAll(replacement)");
        return replaceAll;
    }

    @NotNull
    public final String toString() {
        String pattern = this.nativePattern.toString();
        g.a((Object) pattern, "nativePattern.toString()");
        return pattern;
    }

    private final Object writeReplace() {
        String pattern = this.nativePattern.pattern();
        g.a((Object) pattern, "nativePattern.pattern()");
        return new b(pattern, this.nativePattern.flags());
    }
}
