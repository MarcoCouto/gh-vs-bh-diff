package a.a.h;

import a.a.e;
import a.a.e.c;
import a.a.g;
import java.util.Iterator;
import java.util.NoSuchElementException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/* compiled from: StartAppSDK */
final class b implements a.a.g.a<c> {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public final CharSequence f694a;
    private final int b = 0;
    private final int c = 0;
    /* access modifiers changed from: private */
    public final a.a.d.a.b<CharSequence, Integer, e<Integer, Integer>> d;

    /* compiled from: StartAppSDK */
    public static final class a implements Iterator<c> {

        /* renamed from: a reason: collision with root package name */
        private int f695a = -1;
        private int b;
        private int c;
        @Nullable
        private c d;
        private /* synthetic */ b e;

        public final void remove() {
            throw new UnsupportedOperationException("Operation is not supported for read-only collection");
        }

        a(b bVar) {
            this.e = bVar;
            int length = bVar.f694a.length();
            if (length >= 0) {
                if (length >= 0) {
                    length = 0;
                }
                this.b = length;
                this.c = this.b;
                return;
            }
            StringBuilder sb = new StringBuilder("Cannot coerce value to an empty range: maximum ");
            sb.append(length);
            sb.append(" is less than minimum 0.");
            throw new IllegalArgumentException(sb.toString());
        }

        private final void a() {
            c cVar;
            int i = 0;
            if (this.c < 0) {
                this.f695a = 0;
                this.d = null;
                return;
            }
            if (this.c > this.e.f694a.length()) {
                this.d = new c(this.b, d.a(this.e.f694a));
                this.c = -1;
            } else {
                e eVar = (e) this.e.d.a(this.e.f694a, Integer.valueOf(this.c));
                if (eVar == null) {
                    this.d = new c(this.b, d.a(this.e.f694a));
                    this.c = -1;
                } else {
                    int intValue = ((Number) eVar.c()).intValue();
                    int intValue2 = ((Number) eVar.d()).intValue();
                    int i2 = this.b;
                    if (intValue <= Integer.MIN_VALUE) {
                        a.a.e.c.a aVar = c.f690a;
                        cVar = c.b;
                    } else {
                        cVar = new c(i2, intValue - 1);
                    }
                    this.d = cVar;
                    this.b = intValue + intValue2;
                    int i3 = this.b;
                    if (intValue2 == 0) {
                        i = 1;
                    }
                    this.c = i3 + i;
                }
            }
            this.f695a = 1;
        }

        public final boolean hasNext() {
            if (this.f695a == -1) {
                a();
            }
            return this.f695a == 1;
        }

        public final /* synthetic */ Object next() {
            if (this.f695a == -1) {
                a();
            }
            if (this.f695a != 0) {
                c cVar = this.d;
                if (cVar != null) {
                    this.d = null;
                    this.f695a = -1;
                    return cVar;
                }
                throw new g("null cannot be cast to non-null type kotlin.ranges.IntRange");
            }
            throw new NoSuchElementException();
        }
    }

    public b(@NotNull CharSequence charSequence, @NotNull a.a.d.a.b<? super CharSequence, ? super Integer, e<Integer, Integer>> bVar) {
        a.a.d.b.g.b(charSequence, "input");
        a.a.d.b.g.b(bVar, "getNextMatch");
        this.f694a = charSequence;
        this.d = bVar;
    }

    @NotNull
    public final Iterator<c> a() {
        return new a<>(this);
    }
}
