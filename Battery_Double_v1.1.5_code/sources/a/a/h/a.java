package a.a.h;

import a.a.d.b.g;
import com.google.android.exoplayer2.C;
import java.nio.charset.Charset;
import org.jetbrains.annotations.NotNull;

/* compiled from: StartAppSDK */
public final class a {
    @NotNull

    /* renamed from: a reason: collision with root package name */
    public static final Charset f693a;

    static {
        new a();
        Charset forName = Charset.forName("UTF-8");
        g.a((Object) forName, "Charset.forName(\"UTF-8\")");
        f693a = forName;
        g.a((Object) Charset.forName("UTF-16"), "Charset.forName(\"UTF-16\")");
        g.a((Object) Charset.forName("UTF-16BE"), "Charset.forName(\"UTF-16BE\")");
        g.a((Object) Charset.forName("UTF-16LE"), "Charset.forName(\"UTF-16LE\")");
        g.a((Object) Charset.forName(C.ASCII_NAME), "Charset.forName(\"US-ASCII\")");
        g.a((Object) Charset.forName("ISO-8859-1"), "Charset.forName(\"ISO-8859-1\")");
    }

    private a() {
    }
}
