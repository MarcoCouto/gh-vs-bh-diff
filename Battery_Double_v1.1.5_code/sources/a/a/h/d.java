package a.a.h;

import a.a.d.a.b;
import a.a.d.b.g;
import a.a.d.b.h;
import a.a.e;
import a.a.e.a.C0001a;
import a.a.e.c;
import com.facebook.internal.FacebookRequestErrorClassification;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import org.jetbrains.annotations.NotNull;

/* compiled from: StartAppSDK */
public final class d extends a.a.h.c.a {

    /* compiled from: StartAppSDK */
    static final class a extends h implements b<CharSequence, Integer, e<? extends Integer, ? extends Integer>> {
        final /* synthetic */ List $delimitersList;
        final /* synthetic */ boolean $ignoreCase = false;

        a(List list) {
            this.$delimitersList = list;
            super(2);
        }

        /* JADX WARNING: Removed duplicated region for block: B:57:0x011e  */
        /* JADX WARNING: Removed duplicated region for block: B:59:0x0135 A[RETURN] */
        public final /* synthetic */ Object a(Object obj, Object obj2) {
            e eVar;
            Object obj3;
            Object obj4;
            Object obj5;
            CharSequence charSequence = (CharSequence) obj;
            int intValue = ((Number) obj2).intValue();
            g.b(charSequence, "$receiver");
            Collection collection = this.$delimitersList;
            boolean z = this.$ignoreCase;
            if (z || collection.size() != 1) {
                if (intValue < 0) {
                    intValue = 0;
                }
                a.a.e.a cVar = new c(intValue, charSequence.length());
                if (charSequence instanceof String) {
                    int a2 = cVar.a();
                    int b = cVar.b();
                    if (a2 <= b) {
                        while (true) {
                            Iterator it = collection.iterator();
                            while (true) {
                                if (!it.hasNext()) {
                                    obj4 = null;
                                    break;
                                }
                                obj4 = it.next();
                                String str = (String) obj4;
                                if (d.a(str, (String) charSequence, a2, str.length(), z)) {
                                    break;
                                }
                            }
                            String str2 = (String) obj4;
                            if (str2 == null) {
                                if (a2 == b) {
                                    break;
                                }
                                a2++;
                            } else {
                                eVar = com.startapp.a.a.a.a.a(Integer.valueOf(a2), str2);
                                break;
                            }
                        }
                        if (eVar == null) {
                            return com.startapp.a.a.a.a.a(eVar.a(), Integer.valueOf(((String) eVar.b()).length()));
                        }
                        return null;
                    }
                } else {
                    int a3 = cVar.a();
                    int b2 = cVar.b();
                    if (a3 <= b2) {
                        while (true) {
                            Iterator it2 = collection.iterator();
                            while (true) {
                                if (!it2.hasNext()) {
                                    obj3 = null;
                                    break;
                                }
                                obj3 = it2.next();
                                String str3 = (String) obj3;
                                if (d.a(str3, charSequence, a3, str3.length(), z)) {
                                    break;
                                }
                            }
                            String str4 = (String) obj3;
                            if (str4 == null) {
                                if (a3 == b2) {
                                    break;
                                }
                                a3++;
                            } else {
                                eVar = com.startapp.a.a.a.a.a(Integer.valueOf(a3), str4);
                                break;
                            }
                        }
                        if (eVar == null) {
                        }
                    }
                }
            } else {
                Iterable iterable = collection;
                g.b(iterable, "$this$single");
                if (iterable instanceof List) {
                    List list = (List) iterable;
                    g.b(list, "$this$single");
                    switch (list.size()) {
                        case 0:
                            throw new NoSuchElementException("List is empty.");
                        case 1:
                            obj5 = list.get(0);
                            break;
                        default:
                            throw new IllegalArgumentException("List has more than one element.");
                    }
                } else {
                    Iterator it3 = iterable.iterator();
                    if (it3.hasNext()) {
                        Object next = it3.next();
                        if (!it3.hasNext()) {
                            obj5 = next;
                        } else {
                            throw new IllegalArgumentException("Collection has more than one element.");
                        }
                    } else {
                        throw new NoSuchElementException("Collection is empty.");
                    }
                }
                String str5 = (String) obj5;
                int a4 = d.a(charSequence, str5, intValue);
                if (a4 >= 0) {
                    eVar = com.startapp.a.a.a.a.a(Integer.valueOf(a4), str5);
                    if (eVar == null) {
                    }
                }
            }
            eVar = null;
            if (eVar == null) {
            }
        }
    }

    public static final int a(@NotNull CharSequence charSequence) {
        g.b(charSequence, "$this$lastIndex");
        return charSequence.length() - 1;
    }

    public static final boolean a(@NotNull CharSequence charSequence, @NotNull CharSequence charSequence2, int i, int i2, boolean z) {
        g.b(charSequence, "$this$regionMatchesImpl");
        g.b(charSequence2, FacebookRequestErrorClassification.KEY_OTHER);
        if (i < 0 || charSequence.length() - i2 < 0 || i > charSequence2.length() - i2) {
            return false;
        }
        for (int i3 = 0; i3 < i2; i3++) {
            if (!a.a.h.c.a.a(charSequence.charAt(i3), charSequence2.charAt(i + i3), z)) {
                return false;
            }
        }
        return true;
    }

    public static /* synthetic */ int a(CharSequence charSequence, int i, int i2) {
        boolean z;
        if ((i2 & 2) != 0) {
            i = 0;
        }
        g.b(charSequence, "$this$indexOf");
        boolean z2 = charSequence instanceof String;
        if (z2) {
            return ((String) charSequence).indexOf(46, i);
        }
        char[] cArr = {'.'};
        g.b(charSequence, "$this$indexOfAny");
        g.b(cArr, "chars");
        if (z2) {
            g.b(cArr, "$this$single");
            return ((String) charSequence).indexOf(cArr[0], i);
        }
        if (i < 0) {
            i = 0;
        }
        int a2 = a(charSequence);
        if (i <= a2) {
            while (true) {
                char charAt = charSequence.charAt(i);
                int i3 = 0;
                while (true) {
                    if (i3 > 0) {
                        z = false;
                        break;
                    } else if (a.a.h.c.a.a(cArr[i3], charAt, false)) {
                        z = true;
                        break;
                    } else {
                        i3++;
                    }
                }
                if (!z) {
                    if (i == a2) {
                        break;
                    }
                    i++;
                } else {
                    return i;
                }
            }
        }
        return -1;
    }

    public static final int a(@NotNull CharSequence charSequence, @NotNull String str, int i) {
        g.b(charSequence, "$this$indexOf");
        g.b(str, "string");
        boolean z = charSequence instanceof String;
        if (z) {
            return ((String) charSequence).indexOf(str, i);
        }
        CharSequence charSequence2 = str;
        int length = charSequence.length();
        if (i < 0) {
            i = 0;
        }
        a.a.e.a cVar = new c(i, C0001a.a(length, charSequence.length()));
        if (!z || !(charSequence2 instanceof String)) {
            int a2 = cVar.a();
            int b = cVar.b();
            if (a2 <= b) {
                while (!a(charSequence2, charSequence, a2, charSequence2.length(), false)) {
                    if (a2 != b) {
                        a2++;
                    }
                }
                return a2;
            }
        } else {
            int a3 = cVar.a();
            int b2 = cVar.b();
            if (a3 <= b2) {
                while (!a((String) charSequence2, (String) charSequence, a3, charSequence2.length(), false)) {
                    if (a3 != b2) {
                        a3++;
                    }
                }
                return a3;
            }
        }
        return -1;
    }

    public static /* synthetic */ List a(CharSequence charSequence, String[] strArr) {
        g.b(charSequence, "$this$split");
        g.b(strArr, "delimiters");
        int i = 0;
        String str = strArr[0];
        if (!(str.length() == 0)) {
            int a2 = a(charSequence, str, 0);
            if (a2 == -1) {
                return a.a.a.a.a(charSequence.toString());
            }
            ArrayList arrayList = new ArrayList(10);
            do {
                arrayList.add(charSequence.subSequence(i, a2).toString());
                i = str.length() + a2;
                a2 = a(charSequence, str, i);
            } while (a2 != -1);
            arrayList.add(charSequence.subSequence(i, charSequence.length()).toString());
            return arrayList;
        }
        a.a.g.a bVar = new b(charSequence, new a(a.a.a.a.a((T[]) strArr)));
        g.b(bVar, "$this$asIterable");
        Iterable<c> bVar2 = new a.a.g.b<>(bVar);
        Collection arrayList2 = new ArrayList(a.a.a.a.a(bVar2));
        for (c cVar : bVar2) {
            g.b(charSequence, "$this$substring");
            g.b(cVar, "range");
            arrayList2.add(charSequence.subSequence(cVar.a(), cVar.b() + 1).toString());
        }
        return (List) arrayList2;
    }
}
