package a.a.g;

import java.util.Iterator;
import org.jetbrains.annotations.NotNull;

/* compiled from: StartAppSDK */
public final class b implements Iterable<T> {

    /* renamed from: a reason: collision with root package name */
    private /* synthetic */ a f691a;

    public b(a aVar) {
        this.f691a = aVar;
    }

    @NotNull
    public final Iterator<T> iterator() {
        return this.f691a.a();
    }
}
