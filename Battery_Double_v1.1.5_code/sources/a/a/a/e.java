package a.a.a;

import a.a.d.b.d;
import a.a.d.b.g;
import com.facebook.share.internal.MessengerShareContentUtility;
import java.io.Serializable;
import java.util.Collection;
import java.util.Iterator;
import java.util.Set;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/* compiled from: StartAppSDK */
public final class e implements Serializable, Set {

    /* renamed from: a reason: collision with root package name */
    public static final e f680a = new e();
    private static final long serialVersionUID = 3406603774387020532L;

    public final /* synthetic */ boolean add(Object obj) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public final boolean addAll(Collection collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public final void clear() {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public final int hashCode() {
        return 0;
    }

    public final boolean isEmpty() {
        return true;
    }

    public final boolean remove(Object obj) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public final boolean removeAll(Collection collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public final boolean retainAll(Collection collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public final /* bridge */ int size() {
        return 0;
    }

    public final Object[] toArray() {
        return d.a(this);
    }

    public final <T> T[] toArray(T[] tArr) {
        return d.a(this, tArr);
    }

    @NotNull
    public final String toString() {
        return "[]";
    }

    private e() {
    }

    public final boolean contains(Object obj) {
        if (!(obj instanceof Void)) {
            return false;
        }
        g.b((Void) obj, "element");
        return false;
    }

    public final boolean equals(@Nullable Object obj) {
        return (obj instanceof Set) && ((Set) obj).isEmpty();
    }

    public final boolean containsAll(@NotNull Collection collection) {
        g.b(collection, MessengerShareContentUtility.ELEMENTS);
        return collection.isEmpty();
    }

    @NotNull
    public final Iterator iterator() {
        return b.f677a;
    }

    private final Object readResolve() {
        return f680a;
    }
}
