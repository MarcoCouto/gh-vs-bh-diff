package a.a.a;

import a.a.d.b.g;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/* compiled from: StartAppSDK */
public final class a {
    @NotNull
    public static <T> List<T> a(T t) {
        List<T> singletonList = Collections.singletonList(t);
        g.a((Object) singletonList, "java.util.Collections.singletonList(element)");
        return singletonList;
    }

    @NotNull
    public static <T> List<T> a(T[] tArr) {
        g.b(tArr, "$this$asList");
        List<T> asList = Arrays.asList(tArr);
        g.a((Object) asList, "ArraysUtilJVM.asList(this)");
        return asList;
    }

    public static <T> int a(Iterable<? extends T> iterable) {
        g.b(iterable, "$this$collectionSizeOrDefault");
        if (iterable instanceof Collection) {
            return ((Collection) iterable).size();
        }
        return 10;
    }

    @NotNull
    public static <K, V> Map<K, V> a() {
        return d.f679a;
    }

    public static <T> T a(List<? extends T> list) {
        g.b(list, "$this$first");
        if (!list.isEmpty()) {
            return list.get(0);
        }
        throw new NoSuchElementException("List is empty.");
    }

    @Nullable
    public static <T> T b(List<? extends T> list) {
        g.b(list, "$this$firstOrNull");
        if (list.isEmpty()) {
            return null;
        }
        return list.get(0);
    }

    @NotNull
    private static <T, C extends Collection<? super T>> C a(Iterable<? extends T> iterable, C c) {
        g.b(iterable, "$this$toCollection");
        g.b(c, "destination");
        for (Object add : iterable) {
            c.add(add);
        }
        return c;
    }

    @NotNull
    public static <T> List<T> b(Iterable<? extends T> iterable) {
        List<T> list;
        g.b(iterable, "$this$toList");
        boolean z = iterable instanceof Collection;
        if (z) {
            Collection collection = (Collection) iterable;
            switch (collection.size()) {
                case 0:
                    return c.f678a;
                case 1:
                    return a((T) iterable instanceof List ? ((List) iterable).get(0) : iterable.iterator().next());
                default:
                    return a(collection);
            }
        } else {
            g.b(iterable, "$this$toMutableList");
            if (z) {
                list = a((Collection) iterable);
            } else {
                list = (List) a(iterable, new ArrayList());
            }
            g.b(list, "$this$optimizeReadOnlyList");
            switch (list.size()) {
                case 0:
                    return c.f678a;
                case 1:
                    return a((T) list.get(0));
                default:
                    return list;
            }
        }
    }

    @NotNull
    private static <T> List<T> a(Collection<? extends T> collection) {
        g.b(collection, "$this$toMutableList");
        return new ArrayList<>(collection);
    }
}
