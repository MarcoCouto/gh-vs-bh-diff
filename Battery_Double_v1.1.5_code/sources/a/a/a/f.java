package a.a.a;

import java.util.Iterator;

/* compiled from: StartAppSDK */
public abstract class f implements Iterator<Integer> {
    public abstract int a();

    public void remove() {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public /* synthetic */ Object next() {
        return Integer.valueOf(a());
    }
}
