package a.a.d.b;

import a.a.f.b;
import java.io.Serializable;

/* compiled from: StartAppSDK */
public abstract class a implements a.a.f.a, Serializable {
    private static Object b = C0000a.f684a;

    /* renamed from: a reason: collision with root package name */
    private transient a.a.f.a f683a;
    protected final Object receiver;

    /* renamed from: a.a.d.b.a$a reason: collision with other inner class name */
    /* compiled from: StartAppSDK */
    static class C0000a implements Serializable {
        /* access modifiers changed from: private */

        /* renamed from: a reason: collision with root package name */
        public static final C0000a f684a = new C0000a();

        private C0000a() {
        }

        private Object readResolve() {
            return f684a;
        }
    }

    /* access modifiers changed from: protected */
    public abstract a.a.f.a d();

    public a() {
        this(b);
    }

    protected a(Object obj) {
        this.receiver = obj;
    }

    public final Object e() {
        return this.receiver;
    }

    public final a.a.f.a f() {
        a.a.f.a aVar = this.f683a;
        if (aVar != null) {
            return aVar;
        }
        a.a.f.a d = d();
        this.f683a = d;
        return d;
    }

    public b a() {
        throw new AbstractMethodError();
    }

    public String b() {
        throw new AbstractMethodError();
    }

    public String c() {
        throw new AbstractMethodError();
    }
}
