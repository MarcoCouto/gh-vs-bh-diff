package a.a.d.b;

import java.io.Serializable;
import org.jetbrains.annotations.NotNull;

/* compiled from: StartAppSDK */
public abstract class h<R> implements e<R>, Serializable {
    private final int arity;

    public h(int i) {
        this.arity = i;
    }

    @NotNull
    public String toString() {
        String a2 = m.a(this);
        g.a((Object) a2, "Reflection.renderLambdaToString(this)");
        return a2;
    }
}
