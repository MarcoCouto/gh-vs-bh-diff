package a.a.d.b;

import a.a.f.a;
import a.a.f.d;

/* compiled from: StartAppSDK */
public abstract class i extends a implements d {
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof i) {
            i iVar = (i) obj;
            return a().equals(iVar.a()) && b().equals(iVar.b()) && c().equals(iVar.c()) && g.a(e(), iVar.e());
        } else if (obj instanceof d) {
            return obj.equals(f());
        } else {
            return false;
        }
    }

    public int hashCode() {
        return (((a().hashCode() * 31) + b().hashCode()) * 31) + c().hashCode();
    }

    public String toString() {
        a f = f();
        if (f != this) {
            return f.toString();
        }
        StringBuilder sb = new StringBuilder("property ");
        sb.append(b());
        sb.append(" (Kotlin reflection is not available)");
        return sb.toString();
    }
}
