package a.a.d.b;

import com.startapp.a.a.e.b;
import com.startapp.a.a.g.c;
import java.io.Serializable;

/* compiled from: StartAppSDK */
public final class l {

    /* renamed from: a reason: collision with root package name */
    private final b f687a;
    private final c b;

    /* compiled from: StartAppSDK */
    public static final class a<T> implements Serializable {
        public T element;

        public final String toString() {
            return String.valueOf(this.element);
        }
    }

    public l(b bVar, c cVar) {
        this.b = cVar;
        this.f687a = bVar;
    }

    public final String a(com.startapp.a.a.g.a aVar, com.startapp.a.a.a.b bVar, long j) {
        try {
            String a2 = b.a(bVar);
            com.startapp.a.a.d.a b2 = this.b.b(aVar);
            StringBuilder sb = new StringBuilder();
            sb.append(j);
            sb.append("-");
            sb.append(aVar.a());
            sb.append("-");
            sb.append(b2.a(a2));
            return sb.toString();
        } catch (Throwable unused) {
            return null;
        }
    }
}
