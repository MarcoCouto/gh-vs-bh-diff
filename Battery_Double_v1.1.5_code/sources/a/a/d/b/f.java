package a.a.d.b;

import a.a.f.a;
import a.a.f.c;

/* compiled from: StartAppSDK */
public class f extends a implements e, c {
    private final int arity = 2;

    public f(Object obj) {
        super(obj);
    }

    /* access modifiers changed from: protected */
    public final a d() {
        return m.a(this);
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof f) {
            f fVar = (f) obj;
            if (a() != null ? a().equals(fVar.a()) : fVar.a() == null) {
                return b().equals(fVar.b()) && c().equals(fVar.c()) && g.a(e(), fVar.e());
            }
        } else if (obj instanceof c) {
            return obj.equals(f());
        } else {
            return false;
        }
    }

    public int hashCode() {
        return (((a() == null ? 0 : a().hashCode() * 31) + b().hashCode()) * 31) + c().hashCode();
    }

    public String toString() {
        a f = f();
        if (f != this) {
            return f.toString();
        }
        if ("<init>".equals(b())) {
            return "constructor (Kotlin reflection is not available)";
        }
        StringBuilder sb = new StringBuilder("function ");
        sb.append(b());
        sb.append(" (Kotlin reflection is not available)");
        return sb.toString();
    }
}
