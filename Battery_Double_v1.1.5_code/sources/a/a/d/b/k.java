package a.a.d.b;

import a.a.f.b;

/* compiled from: StartAppSDK */
public final class k extends j {
    private final String name;
    private final b owner;
    private final String signature;

    public k(b bVar, String str, String str2) {
        this.owner = bVar;
        this.name = str;
        this.signature = str2;
    }

    public final b a() {
        return this.owner;
    }

    public final String b() {
        return this.name;
    }

    public final String c() {
        return this.signature;
    }
}
