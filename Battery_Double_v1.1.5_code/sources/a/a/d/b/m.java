package a.a.d.b;

import a.a.a.a;
import a.a.f.c;
import a.a.f.d;

/* compiled from: StartAppSDK */
public final class m {
    public static c a(f fVar) {
        return fVar;
    }

    public static d a(j jVar) {
        return jVar;
    }

    static {
        a aVar = null;
        try {
            aVar = (a) Class.forName("truenet.kotlin.reflect.jvm.internal.ReflectionFactoryImpl").newInstance();
        } catch (ClassCastException | ClassNotFoundException | IllegalAccessException | InstantiationException unused) {
        }
        if (aVar == null) {
            new a();
        }
    }

    public static e a(Class cls) {
        return new c(cls);
    }

    public static String a(h hVar) {
        String obj = hVar.getClass().getGenericInterfaces()[0].toString();
        return obj.startsWith("truenet.kotlin.jvm.functions.") ? obj.substring(29) : obj;
    }
}
