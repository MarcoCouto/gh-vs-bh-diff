package a.a.d.b;

import com.startapp.a.a.a.a;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/* compiled from: StartAppSDK */
public final class c implements b, e<Object> {
    @NotNull

    /* renamed from: a reason: collision with root package name */
    private final Class<?> f685a;

    public c(@NotNull Class<?> cls) {
        g.b(cls, "jClass");
        this.f685a = cls;
    }

    @NotNull
    public final Class<?> a() {
        return this.f685a;
    }

    public final boolean equals(@Nullable Object obj) {
        return (obj instanceof c) && g.a((Object) a.a((e<T>) this), (Object) a.a((e) obj));
    }

    public final int hashCode() {
        return a.a((e<T>) this).hashCode();
    }

    @NotNull
    public final String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.f685a.toString());
        sb.append(" (Kotlin reflection is not available)");
        return sb.toString();
    }
}
