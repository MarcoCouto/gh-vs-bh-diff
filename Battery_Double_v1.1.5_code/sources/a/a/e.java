package a.a;

import a.a.d.b.g;
import java.io.Serializable;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/* compiled from: StartAppSDK */
public final class e<A, B> implements Serializable {
    private final A first;
    private final B second;

    public final A c() {
        return this.first;
    }

    public final B d() {
        return this.second;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:6:0x001a, code lost:
        if (a.a.d.b.g.a((java.lang.Object) r2.second, (java.lang.Object) r3.second) != false) goto L_0x001f;
     */
    public final boolean equals(@Nullable Object obj) {
        if (this != obj) {
            if (obj instanceof e) {
                e eVar = (e) obj;
                if (g.a((Object) this.first, (Object) eVar.first)) {
                }
            }
            return false;
        }
        return true;
    }

    public final int hashCode() {
        A a2 = this.first;
        int i = 0;
        int hashCode = (a2 != null ? a2.hashCode() : 0) * 31;
        B b = this.second;
        if (b != null) {
            i = b.hashCode();
        }
        return hashCode + i;
    }

    public e(A a2, B b) {
        this.first = a2;
        this.second = b;
    }

    public final A a() {
        return this.first;
    }

    public final B b() {
        return this.second;
    }

    @NotNull
    public final String toString() {
        StringBuilder sb = new StringBuilder("(");
        sb.append(this.first);
        sb.append(", ");
        sb.append(this.second);
        sb.append(')');
        return sb.toString();
    }
}
