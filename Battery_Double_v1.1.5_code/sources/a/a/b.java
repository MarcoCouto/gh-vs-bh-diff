package a.a;

import java.io.Serializable;
import org.jetbrains.annotations.NotNull;

/* compiled from: StartAppSDK */
public final class b<T> implements d<T>, Serializable {
    private final T value;

    public b(T t) {
        this.value = t;
    }

    public final T a() {
        return this.value;
    }

    @NotNull
    public final String toString() {
        return String.valueOf(a());
    }
}
