package a.a;

import a.a.d.a.a;
import a.a.d.b.g;
import java.io.Serializable;
import org.jetbrains.annotations.NotNull;

/* compiled from: StartAppSDK */
final class f<T> implements d<T>, Serializable {
    private volatile Object _value;
    private a<? extends T> initializer;
    private final Object lock;

    private f(@NotNull a<? extends T> aVar) {
        g.b(aVar, "initializer");
        this.initializer = aVar;
        this._value = h.f692a;
        this.lock = this;
    }

    public /* synthetic */ f(a aVar, byte b) {
        this(aVar);
    }

    public final T a() {
        T t;
        T t2 = this._value;
        if (t2 != h.f692a) {
            return t2;
        }
        synchronized (this.lock) {
            t = this._value;
            if (t == h.f692a) {
                a<? extends T> aVar = this.initializer;
                if (aVar == null) {
                    g.a();
                }
                t = aVar.a();
                this._value = t;
                this.initializer = null;
            }
        }
        return t;
    }

    private final Object writeReplace() {
        return new b(a());
    }

    @NotNull
    public final String toString() {
        return this._value != h.f692a ? String.valueOf(a()) : "Lazy value not initialized yet.";
    }
}
