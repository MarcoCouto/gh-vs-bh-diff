package android.support.v13;

public final class R {

    public static final class attr {
        public static final int coordinatorLayoutStyle = 2130968711;
        public static final int font = 2130968751;
        public static final int fontProviderAuthority = 2130968753;
        public static final int fontProviderCerts = 2130968754;
        public static final int fontProviderFetchStrategy = 2130968755;
        public static final int fontProviderFetchTimeout = 2130968756;
        public static final int fontProviderPackage = 2130968757;
        public static final int fontProviderQuery = 2130968758;
        public static final int fontStyle = 2130968759;
        public static final int fontWeight = 2130968761;
        public static final int keylines = 2130968789;
        public static final int layout_anchor = 2130968792;
        public static final int layout_anchorGravity = 2130968793;
        public static final int layout_behavior = 2130968794;
        public static final int layout_dodgeInsetEdges = 2130968797;
        public static final int layout_insetEdge = 2130968798;
        public static final int layout_keyline = 2130968799;
        public static final int statusBarBackground = 2130968881;

        private attr() {
        }
    }

    public static final class bool {
        public static final int abc_action_bar_embed_tabs = 2131034112;

        private bool() {
        }
    }

    public static final class color {
        public static final int notification_action_color_filter = 2131099811;
        public static final int notification_icon_bg_color = 2131099812;
        public static final int notification_material_background_media_default_color = 2131099813;
        public static final int primary_text_default_material_dark = 2131099818;
        public static final int ripple_material_light = 2131099823;
        public static final int secondary_text_default_material_dark = 2131099824;
        public static final int secondary_text_default_material_light = 2131099825;

        private color() {
        }
    }

    public static final class dimen {
        public static final int compat_button_inset_horizontal_material = 2131165289;
        public static final int compat_button_inset_vertical_material = 2131165290;
        public static final int compat_button_padding_horizontal_material = 2131165291;
        public static final int compat_button_padding_vertical_material = 2131165292;
        public static final int compat_control_corner_material = 2131165293;
        public static final int notification_action_icon_size = 2131165368;
        public static final int notification_action_text_size = 2131165369;
        public static final int notification_big_circle_margin = 2131165370;
        public static final int notification_content_margin_start = 2131165371;
        public static final int notification_large_icon_height = 2131165372;
        public static final int notification_large_icon_width = 2131165373;
        public static final int notification_main_column_padding_top = 2131165374;
        public static final int notification_media_narrow_margin = 2131165375;
        public static final int notification_right_icon_size = 2131165376;
        public static final int notification_right_side_padding_top = 2131165377;
        public static final int notification_small_icon_background_padding = 2131165378;
        public static final int notification_small_icon_size_as_large = 2131165379;
        public static final int notification_subtext_size = 2131165380;
        public static final int notification_top_pad = 2131165381;
        public static final int notification_top_pad_large_text = 2131165382;

        private dimen() {
        }
    }

    public static final class drawable {
        public static final int notification_action_background = 2131231131;
        public static final int notification_bg = 2131231132;
        public static final int notification_bg_low = 2131231133;
        public static final int notification_bg_low_normal = 2131231134;
        public static final int notification_bg_low_pressed = 2131231135;
        public static final int notification_bg_normal = 2131231136;
        public static final int notification_bg_normal_pressed = 2131231137;
        public static final int notification_icon_background = 2131231138;
        public static final int notification_template_icon_bg = 2131231139;
        public static final int notification_template_icon_low_bg = 2131231140;
        public static final int notification_tile_bg = 2131231141;
        public static final int notify_panel_notification_icon_bg = 2131231142;

        private drawable() {
        }
    }

    public static final class id {
        public static final int action0 = 2131296262;
        public static final int action_container = 2131296273;
        public static final int action_divider = 2131296275;
        public static final int action_image = 2131296276;
        public static final int action_text = 2131296287;
        public static final int actions = 2131296288;
        public static final int async = 2131296311;
        public static final int blocking = 2131296328;
        public static final int bottom = 2131296334;
        public static final int cancel_action = 2131296354;
        public static final int chronometer = 2131296363;
        public static final int end = 2131296399;
        public static final int end_padder = 2131296400;
        public static final int forever = 2131296413;
        public static final int icon = 2131296419;
        public static final int icon_group = 2131296420;
        public static final int info = 2131296430;
        public static final int italic = 2131296435;
        public static final int left = 2131296441;
        public static final int line1 = 2131296445;
        public static final int line3 = 2131296446;
        public static final int media_actions = 2131296457;
        public static final int none = 2131296572;
        public static final int normal = 2131296573;
        public static final int notification_background = 2131296574;
        public static final int notification_main_column = 2131296575;
        public static final int notification_main_column_container = 2131296576;
        public static final int right = 2131296593;
        public static final int right_icon = 2131296594;
        public static final int right_side = 2131296595;
        public static final int start = 2131296649;
        public static final int status_bar_latest_event_content = 2131296651;
        public static final int tag_transition_group = 2131296662;
        public static final int text = 2131296674;
        public static final int text2 = 2131296675;
        public static final int time = 2131296682;
        public static final int title = 2131296683;
        public static final int top = 2131296687;

        private id() {
        }
    }

    public static final class integer {
        public static final int cancel_button_image_alpha = 2131361796;
        public static final int status_bar_notification_info_maxnum = 2131361802;

        private integer() {
        }
    }

    public static final class layout {
        public static final int notification_action = 2131427427;
        public static final int notification_action_tombstone = 2131427428;
        public static final int notification_media_action = 2131427429;
        public static final int notification_media_cancel_action = 2131427430;
        public static final int notification_template_big_media = 2131427431;
        public static final int notification_template_big_media_custom = 2131427432;
        public static final int notification_template_big_media_narrow = 2131427433;
        public static final int notification_template_big_media_narrow_custom = 2131427434;
        public static final int notification_template_custom_big = 2131427435;
        public static final int notification_template_icon_group = 2131427436;
        public static final int notification_template_lines_media = 2131427437;
        public static final int notification_template_media = 2131427438;
        public static final int notification_template_media_custom = 2131427439;
        public static final int notification_template_part_chronometer = 2131427440;
        public static final int notification_template_part_time = 2131427441;

        private layout() {
        }
    }

    public static final class string {
        public static final int status_bar_notification_info_overflow = 2131624233;

        private string() {
        }
    }

    public static final class style {
        public static final int TextAppearance_Compat_Notification = 2131689737;
        public static final int TextAppearance_Compat_Notification_Info = 2131689738;
        public static final int TextAppearance_Compat_Notification_Info_Media = 2131689739;
        public static final int TextAppearance_Compat_Notification_Line2 = 2131689740;
        public static final int TextAppearance_Compat_Notification_Line2_Media = 2131689741;
        public static final int TextAppearance_Compat_Notification_Media = 2131689742;
        public static final int TextAppearance_Compat_Notification_Time = 2131689743;
        public static final int TextAppearance_Compat_Notification_Time_Media = 2131689744;
        public static final int TextAppearance_Compat_Notification_Title = 2131689745;
        public static final int TextAppearance_Compat_Notification_Title_Media = 2131689746;
        public static final int Widget_Compat_NotificationActionContainer = 2131689871;
        public static final int Widget_Compat_NotificationActionText = 2131689872;
        public static final int Widget_Support_CoordinatorLayout = 2131689884;

        private style() {
        }
    }

    public static final class styleable {
        public static final int[] CoordinatorLayout = {com.mansoon.BatteryDouble.R.attr.keylines, com.mansoon.BatteryDouble.R.attr.statusBarBackground};
        public static final int[] CoordinatorLayout_Layout = {16842931, com.mansoon.BatteryDouble.R.attr.layout_anchor, com.mansoon.BatteryDouble.R.attr.layout_anchorGravity, com.mansoon.BatteryDouble.R.attr.layout_behavior, com.mansoon.BatteryDouble.R.attr.layout_dodgeInsetEdges, com.mansoon.BatteryDouble.R.attr.layout_insetEdge, com.mansoon.BatteryDouble.R.attr.layout_keyline};
        public static final int CoordinatorLayout_Layout_android_layout_gravity = 0;
        public static final int CoordinatorLayout_Layout_layout_anchor = 1;
        public static final int CoordinatorLayout_Layout_layout_anchorGravity = 2;
        public static final int CoordinatorLayout_Layout_layout_behavior = 3;
        public static final int CoordinatorLayout_Layout_layout_dodgeInsetEdges = 4;
        public static final int CoordinatorLayout_Layout_layout_insetEdge = 5;
        public static final int CoordinatorLayout_Layout_layout_keyline = 6;
        public static final int CoordinatorLayout_keylines = 0;
        public static final int CoordinatorLayout_statusBarBackground = 1;
        public static final int[] FontFamily = {com.mansoon.BatteryDouble.R.attr.fontProviderAuthority, com.mansoon.BatteryDouble.R.attr.fontProviderCerts, com.mansoon.BatteryDouble.R.attr.fontProviderFetchStrategy, com.mansoon.BatteryDouble.R.attr.fontProviderFetchTimeout, com.mansoon.BatteryDouble.R.attr.fontProviderPackage, com.mansoon.BatteryDouble.R.attr.fontProviderQuery};
        public static final int[] FontFamilyFont = {16844082, 16844083, 16844095, 16844143, 16844144, com.mansoon.BatteryDouble.R.attr.font, com.mansoon.BatteryDouble.R.attr.fontStyle, com.mansoon.BatteryDouble.R.attr.fontVariationSettings, com.mansoon.BatteryDouble.R.attr.fontWeight, com.mansoon.BatteryDouble.R.attr.ttcIndex};
        public static final int FontFamilyFont_android_font = 0;
        public static final int FontFamilyFont_android_fontStyle = 2;
        public static final int FontFamilyFont_android_fontVariationSettings = 4;
        public static final int FontFamilyFont_android_fontWeight = 1;
        public static final int FontFamilyFont_android_ttcIndex = 3;
        public static final int FontFamilyFont_font = 5;
        public static final int FontFamilyFont_fontStyle = 6;
        public static final int FontFamilyFont_fontVariationSettings = 7;
        public static final int FontFamilyFont_fontWeight = 8;
        public static final int FontFamilyFont_ttcIndex = 9;
        public static final int FontFamily_fontProviderAuthority = 0;
        public static final int FontFamily_fontProviderCerts = 1;
        public static final int FontFamily_fontProviderFetchStrategy = 2;
        public static final int FontFamily_fontProviderFetchTimeout = 3;
        public static final int FontFamily_fontProviderPackage = 4;
        public static final int FontFamily_fontProviderQuery = 5;

        private styleable() {
        }
    }

    private R() {
    }
}
