package com.vungle.warren.utility;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.webkit.URLUtil;
import com.vungle.warren.ui.VungleWebViewActivity;

public class ExternalRouter {
    public static final String TAG = "ExternalRouter";

    public static boolean launch(String str, Context context) {
        if (str == null || str.trim().isEmpty()) {
            return false;
        }
        try {
            Intent parseUri = Intent.parseUri(str, 0);
            parseUri.setFlags(268435456);
            if (parseUri.resolveActivity(context.getPackageManager()) != null) {
                context.startActivity(parseUri);
                return true;
            }
            if (!URLUtil.isHttpsUrl(str)) {
                if (!URLUtil.isHttpUrl(str)) {
                    String str2 = TAG;
                    StringBuilder sb = new StringBuilder();
                    sb.append("Cannot open url ");
                    sb.append(str);
                    sb.append(str);
                    Log.d(str2, sb.toString());
                    return false;
                }
            }
            parseUri.setComponent(new ComponentName(context, VungleWebViewActivity.class));
            parseUri.putExtra(VungleWebViewActivity.INTENT_URL, str);
            context.startActivity(parseUri);
            return true;
        } catch (Exception e) {
            String str3 = TAG;
            StringBuilder sb2 = new StringBuilder();
            sb2.append("Error while opening url");
            sb2.append(e.getLocalizedMessage());
            Log.e(str3, sb2.toString());
            return false;
        }
    }
}
