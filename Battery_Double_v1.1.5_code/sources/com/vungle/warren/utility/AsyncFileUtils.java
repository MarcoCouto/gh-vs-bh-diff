package com.vungle.warren.utility;

import java.io.File;
import java.util.concurrent.ExecutorService;

public class AsyncFileUtils {
    private ExecutorService ioExecutorService;
    /* access modifiers changed from: private */
    public ExecutorService uiExecutorService;

    public interface FileExist {
        void status(boolean z);
    }

    public AsyncFileUtils(ExecutorService executorService, ExecutorService executorService2) {
        this.ioExecutorService = executorService;
        this.uiExecutorService = executorService2;
    }

    public void isFileExistAsync(final File file, final FileExist fileExist) {
        if (file == null) {
            fileExist.status(false);
        }
        this.ioExecutorService.execute(new Runnable() {
            public void run() {
                final boolean exists = file.exists();
                AsyncFileUtils.this.uiExecutorService.execute(new Runnable() {
                    public void run() {
                        fileExist.status(exists);
                    }
                });
            }
        });
    }
}
