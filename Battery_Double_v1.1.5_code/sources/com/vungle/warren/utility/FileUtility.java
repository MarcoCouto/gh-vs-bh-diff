package com.vungle.warren.utility;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.io.BufferedInputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class FileUtility {
    private static final String TAG = "FileUtility";

    public static void printDirectoryTree(File file) {
    }

    private static void printDirectoryTree(File file, int i, StringBuilder sb) {
        File[] listFiles;
        if (file != null) {
            if (file.isDirectory()) {
                sb.append(getIndentString(i));
                sb.append("+--");
                sb.append(file.getName());
                sb.append("/");
                sb.append("\n");
                if (file.listFiles() != null) {
                    for (File file2 : file.listFiles()) {
                        if (file2.isDirectory()) {
                            printDirectoryTree(file2, i + 1, sb);
                        } else {
                            printFile(file2, i + 1, sb);
                        }
                    }
                    return;
                }
                return;
            }
            throw new IllegalArgumentException("folder is not a Directory");
        }
    }

    private static void printFile(File file, int i, StringBuilder sb) {
        sb.append(getIndentString(i));
        sb.append("+--");
        sb.append(file.getName());
        sb.append("\n");
    }

    private static String getIndentString(int i) {
        StringBuilder sb = new StringBuilder();
        for (int i2 = 0; i2 < i; i2++) {
            sb.append("|  ");
        }
        return sb.toString();
    }

    public static void delete(File file) throws IOException {
        if (file != null && file.exists()) {
            if (file.isDirectory()) {
                File[] listFiles = file.listFiles();
                if (listFiles != null) {
                    for (File delete : listFiles) {
                        delete(delete);
                    }
                } else {
                    return;
                }
            }
            if (!file.delete()) {
                StringBuilder sb = new StringBuilder();
                sb.append("Failed to delete file: ");
                sb.append(file);
                throw new FileNotFoundException(sb.toString());
            }
        }
    }

    public static void closeQuietly(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException unused) {
            }
        }
    }

    @NonNull
    public static byte[] extractBytes(@Nullable File file) throws IOException {
        if (file == null) {
            return new byte[0];
        }
        int length = (int) file.length();
        if (length > 0) {
            byte[] bArr = new byte[length];
            BufferedInputStream bufferedInputStream = null;
            try {
                BufferedInputStream bufferedInputStream2 = new BufferedInputStream(new FileInputStream(file));
                try {
                    if (bufferedInputStream2.read(bArr) >= length) {
                        closeQuietly(bufferedInputStream2);
                        return bArr;
                    }
                    throw new IOException("Failed to read all bytes in the file, object recreation will fail");
                } catch (FileNotFoundException e) {
                    e = e;
                    bufferedInputStream = bufferedInputStream2;
                    try {
                        e.printStackTrace();
                        closeQuietly(bufferedInputStream);
                        return new byte[0];
                    } catch (Throwable th) {
                        th = th;
                        closeQuietly(bufferedInputStream);
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    bufferedInputStream = bufferedInputStream2;
                    closeQuietly(bufferedInputStream);
                    throw th;
                }
            } catch (FileNotFoundException e2) {
                e = e2;
                e.printStackTrace();
                closeQuietly(bufferedInputStream);
                return new byte[0];
            }
        }
        return new byte[0];
    }

    @NonNull
    public static Map<String, String> readMap(@NonNull String str) {
        File file = new File(str);
        if (file.exists()) {
            Object readSerializable = readSerializable(file);
            if (readSerializable instanceof HashMap) {
                return (HashMap) readSerializable;
            }
        }
        return new HashMap();
    }

    public static void writeMap(@NonNull String str, @NonNull HashMap<String, String> hashMap) {
        File file = new File(str);
        if (file.exists()) {
            file.delete();
        }
        if (!hashMap.isEmpty()) {
            writeSerializable(file, hashMap);
        }
    }

    @NonNull
    public static ArrayList<String> readAllLines(@NonNull String str) {
        File file = new File(str);
        if (file.exists()) {
            Object readSerializable = readSerializable(file);
            if (readSerializable instanceof ArrayList) {
                return (ArrayList) readSerializable;
            }
        }
        return new ArrayList<>();
    }

    public static void writeAllLines(@NonNull String str, @NonNull ArrayList<String> arrayList) {
        File file = new File(str);
        if (file.exists()) {
            file.delete();
        }
        if (!arrayList.isEmpty()) {
            writeSerializable(file, arrayList);
        }
    }

    public static void writeSerializable(File file, Serializable serializable) {
        ObjectOutputStream objectOutputStream = null;
        try {
            ObjectOutputStream objectOutputStream2 = new ObjectOutputStream(new FileOutputStream(file));
            try {
                objectOutputStream2.writeObject(serializable);
                closeQuietly(objectOutputStream2);
            } catch (IOException e) {
                e = e;
                objectOutputStream = objectOutputStream2;
                try {
                    e.printStackTrace();
                    closeQuietly(objectOutputStream);
                } catch (Throwable th) {
                    th = th;
                    closeQuietly(objectOutputStream);
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                objectOutputStream = objectOutputStream2;
                closeQuietly(objectOutputStream);
                throw th;
            }
        } catch (IOException e2) {
            e = e2;
            e.printStackTrace();
            closeQuietly(objectOutputStream);
        }
    }

    @Nullable
    public static Object readSerializable(File file) {
        ObjectInputStream objectInputStream;
        ObjectInputStream objectInputStream2 = null;
        try {
            objectInputStream = new ObjectInputStream(new FileInputStream(file));
            try {
                Object readObject = objectInputStream.readObject();
                closeQuietly(objectInputStream);
                return readObject;
            } catch (IOException e) {
                e = e;
                e.printStackTrace();
                closeQuietly(objectInputStream);
                return null;
            } catch (ClassNotFoundException e2) {
                e = e2;
                e.printStackTrace();
                closeQuietly(objectInputStream);
                return null;
            }
        } catch (IOException e3) {
            e = e3;
            objectInputStream = null;
            e.printStackTrace();
            closeQuietly(objectInputStream);
            return null;
        } catch (ClassNotFoundException e4) {
            e = e4;
            objectInputStream = null;
            e.printStackTrace();
            closeQuietly(objectInputStream);
            return null;
        } catch (Throwable th) {
            th = th;
            objectInputStream2 = objectInputStream;
            closeQuietly(objectInputStream2);
            throw th;
        }
    }
}
