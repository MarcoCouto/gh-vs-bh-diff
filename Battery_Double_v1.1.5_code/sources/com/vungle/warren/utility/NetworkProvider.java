package com.vungle.warren.utility;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.ConnectivityManager.NetworkCallback;
import android.net.Network;
import android.net.NetworkInfo;
import android.net.NetworkRequest.Builder;
import android.os.Build.VERSION;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.content.PermissionChecker;
import android.util.Log;
import com.vungle.warren.NetworkProviderReceiver;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

public class NetworkProvider {
    private static volatile NetworkProvider INSTANCE = null;
    static final long NETWORK_CHECK_DELAY = 30000;
    private static final String TAG = "NetworkProvider";
    static final int TYPE_NONE = -1;
    @Nullable
    private final ConnectivityManager cm;
    private final Context ctx;
    private AtomicInteger currentNetwork = new AtomicInteger();
    private boolean enabled = false;
    /* access modifiers changed from: private */
    public Handler handler = new Handler(Looper.getMainLooper());
    /* access modifiers changed from: private */
    public Set<NetworkListener> listeners = Collections.synchronizedSet(new HashSet());
    private NetworkCallback networkCallback;
    /* access modifiers changed from: private */
    public Runnable typeRunnable = new Runnable() {
        public void run() {
            if (!NetworkProvider.this.listeners.isEmpty()) {
                NetworkProvider.this.onNetworkChanged();
                NetworkProvider.this.handler.postDelayed(NetworkProvider.this.typeRunnable, NetworkProvider.NETWORK_CHECK_DELAY);
            }
        }
    };

    public interface NetworkListener {
        void onChanged(int i);
    }

    private NetworkProvider(Context context) {
        this.ctx = context.getApplicationContext();
        this.cm = (ConnectivityManager) this.ctx.getSystemService("connectivity");
        this.currentNetwork.set(getCurrentNetworkType());
        if (VERSION.SDK_INT < 21) {
            this.enabled = NetworkProviderReceiver.isEnabledBroadcastReceiver(context);
        } else {
            NetworkProviderReceiver.enableBroadcastReceiver(context, false);
        }
    }

    public static NetworkProvider getInstance(Context context) {
        if (INSTANCE == null) {
            synchronized (NetworkProvider.class) {
                if (INSTANCE == null) {
                    INSTANCE = new NetworkProvider(context);
                }
            }
        }
        return INSTANCE;
    }

    private synchronized void setEnableNetworkListener(boolean z) {
        if (this.enabled != z) {
            this.enabled = z;
            if (VERSION.SDK_INT < 21) {
                if (NetworkProviderReceiver.hasReceiver(this.ctx)) {
                    NetworkProviderReceiver.enableBroadcastReceiver(this.ctx, z);
                } else if (z) {
                    this.handler.postDelayed(this.typeRunnable, NETWORK_CHECK_DELAY);
                } else {
                    this.handler.removeCallbacks(this.typeRunnable);
                }
            } else if (this.cm != null) {
                if (z) {
                    try {
                        Builder builder = new Builder();
                        builder.addCapability(12);
                        this.cm.registerNetworkCallback(builder.build(), getNetworkCallback());
                    } catch (Exception e) {
                        Log.e(TAG, e.getMessage());
                    }
                } else {
                    this.cm.unregisterNetworkCallback(getNetworkCallback());
                }
            }
        } else {
            return;
        }
        return;
    }

    @RequiresApi(api = 21)
    private NetworkCallback getNetworkCallback() {
        if (this.networkCallback != null) {
            return this.networkCallback;
        }
        AnonymousClass1 r0 = new NetworkCallback() {
            @RequiresApi(api = 21)
            public void onAvailable(Network network) {
                super.onAvailable(network);
                NetworkProvider.this.onNetworkChanged();
            }

            @RequiresApi(api = 21)
            public void onLost(Network network) {
                super.onLost(network);
                NetworkProvider.this.onNetworkChanged();
            }
        };
        this.networkCallback = r0;
        return r0;
    }

    private void postToListeners(final int i) {
        this.handler.post(new Runnable() {
            public void run() {
                for (NetworkListener onChanged : NetworkProvider.this.listeners) {
                    onChanged.onChanged(i);
                }
            }
        });
    }

    public void onNetworkChanged() {
        getCurrentNetworkType();
    }

    public int getCurrentNetworkType() {
        int i = -1;
        if (this.cm == null || PermissionChecker.checkCallingOrSelfPermission(this.ctx, "android.permission.ACCESS_NETWORK_STATE") != 0) {
            this.currentNetwork.set(-1);
            return -1;
        }
        NetworkInfo activeNetworkInfo = this.cm.getActiveNetworkInfo();
        if (activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting()) {
            i = activeNetworkInfo.getType();
        }
        int andSet = this.currentNetwork.getAndSet(i);
        if (i != andSet) {
            String str = TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("on network changed: ");
            sb.append(andSet);
            sb.append("->");
            sb.append(i);
            Log.d(str, sb.toString());
            postToListeners(i);
        }
        setEnableNetworkListener(!this.listeners.isEmpty());
        return i;
    }

    public void addListener(NetworkListener networkListener) {
        this.listeners.add(networkListener);
        setEnableNetworkListener(true);
    }

    public void removeListener(NetworkListener networkListener) {
        this.listeners.remove(networkListener);
        setEnableNetworkListener(!this.listeners.isEmpty());
    }
}
