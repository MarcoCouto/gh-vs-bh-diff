package com.vungle.warren.utility;

import android.util.Log;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class UnzipUtility {
    private static final int BUFFER_SIZE = 4096;
    private static final String TAG = UnzipUtility.class.getCanonicalName();

    public interface Filter {
        boolean matches(String str);
    }

    static class ZipSecurityException extends IOException {
        ZipSecurityException(String str) {
            super(str);
        }
    }

    public static List<File> unzip(String str, String str2) throws IOException {
        return unzip(str, str2, null);
    }

    public static List<File> unzip(String str, String str2, Filter filter) throws IOException {
        File file = new File(str2);
        if (!file.exists()) {
            file.mkdir();
        }
        ZipInputStream zipInputStream = new ZipInputStream(new FileInputStream(str));
        ArrayList arrayList = new ArrayList();
        for (ZipEntry nextEntry = zipInputStream.getNextEntry(); nextEntry != null; nextEntry = zipInputStream.getNextEntry()) {
            StringBuilder sb = new StringBuilder();
            sb.append(str2);
            sb.append(File.separator);
            sb.append(nextEntry.getName());
            String sb2 = sb.toString();
            if (filter == null || filter.matches(sb2)) {
                validateFilename(sb2, str2);
                if (nextEntry.isDirectory()) {
                    File file2 = new File(sb2);
                    if (!file2.exists()) {
                        file2.mkdir();
                    }
                } else {
                    extractFile(zipInputStream, sb2);
                    arrayList.add(new File(sb2));
                }
            }
            zipInputStream.closeEntry();
        }
        zipInputStream.close();
        return arrayList;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0040, code lost:
        r0 = r0.getParentFile();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0048, code lost:
        if (r0.mkdir() == false) goto L_0x004a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x004a, code lost:
        r0 = r0.getParentFile();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x004f, code lost:
        extractFile(r6, r7);
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:23:0x0036 */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0040 A[Catch:{ all -> 0x0059 }] */
    private static void extractFile(ZipInputStream zipInputStream, String str) throws IOException {
        FileOutputStream fileOutputStream;
        File file = new File(str);
        if (file.exists()) {
            file.delete();
        }
        BufferedOutputStream bufferedOutputStream = null;
        try {
            fileOutputStream = new FileOutputStream(str);
            BufferedOutputStream bufferedOutputStream2 = new BufferedOutputStream(fileOutputStream);
            try {
                byte[] bArr = new byte[4096];
                while (true) {
                    int read = zipInputStream.read(bArr);
                    if (read == -1) {
                        break;
                    }
                    bufferedOutputStream2.write(bArr, 0, read);
                }
                FileUtility.closeQuietly(bufferedOutputStream2);
            } catch (FileNotFoundException unused) {
                bufferedOutputStream = bufferedOutputStream2;
                try {
                    if (!file.getParentFile().exists()) {
                    }
                    FileUtility.closeQuietly(bufferedOutputStream);
                    FileUtility.closeQuietly(fileOutputStream);
                } catch (Throwable th) {
                    th = th;
                    FileUtility.closeQuietly(bufferedOutputStream);
                    FileUtility.closeQuietly(fileOutputStream);
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                bufferedOutputStream = bufferedOutputStream2;
                FileUtility.closeQuietly(bufferedOutputStream);
                FileUtility.closeQuietly(fileOutputStream);
                throw th;
            }
        } catch (FileNotFoundException unused2) {
            fileOutputStream = null;
        } catch (Throwable th3) {
            th = th3;
            fileOutputStream = null;
            FileUtility.closeQuietly(bufferedOutputStream);
            FileUtility.closeQuietly(fileOutputStream);
            throw th;
        }
        FileUtility.closeQuietly(fileOutputStream);
    }

    private static String validateFilename(String str, String str2) throws IOException {
        String canonicalPath = new File(str).getCanonicalPath();
        if (canonicalPath.startsWith(new File(str2).getCanonicalPath())) {
            return canonicalPath;
        }
        Log.e(TAG, "File is outside extraction target directory.");
        throw new ZipSecurityException("File is outside extraction target directory.");
    }
}
