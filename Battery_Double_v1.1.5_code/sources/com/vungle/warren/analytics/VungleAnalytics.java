package com.vungle.warren.analytics;

import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;
import com.google.gson.JsonObject;
import com.vungle.warren.VungleApiClient;
import com.vungle.warren.VungleApiClient.ClearTextTrafficException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VungleAnalytics implements AdAnalytics {
    /* access modifiers changed from: private */
    public static final String TAG = "VungleAnalytics";
    private final VungleApiClient client;

    public VungleAnalytics(VungleApiClient vungleApiClient) {
        this.client = vungleApiClient;
    }

    public String[] ping(@NonNull String[] strArr) {
        if (strArr.length == 0) {
            return strArr;
        }
        ArrayList arrayList = new ArrayList();
        for (String str : strArr) {
            if (!TextUtils.isEmpty(str)) {
                try {
                    if (!this.client.pingTPAT(str)) {
                        arrayList.add(str);
                    }
                } catch (ClearTextTrafficException unused) {
                    String str2 = TAG;
                    StringBuilder sb = new StringBuilder();
                    sb.append("Cleartext Network Traffic is Blocked : ");
                    sb.append(str);
                    Log.e(str2, sb.toString());
                } catch (MalformedURLException unused2) {
                    String str3 = TAG;
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("Invalid Url : ");
                    sb2.append(str);
                    Log.e(str3, sb2.toString());
                }
            }
        }
        return (String[]) arrayList.toArray(new String[0]);
    }

    public void ri(JsonObject jsonObject) {
        if (jsonObject != null) {
            this.client.ri(jsonObject).enqueue(new Callback<JsonObject>() {
                public void onResponse(@NonNull Call<JsonObject> call, Response<JsonObject> response) {
                    Log.d(VungleAnalytics.TAG, "send RI success");
                }

                public void onFailure(Call<JsonObject> call, Throwable th) {
                    Log.d(VungleAnalytics.TAG, "send RI Failure");
                }
            });
        }
    }
}
