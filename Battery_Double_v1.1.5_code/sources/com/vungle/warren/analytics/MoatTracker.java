package com.vungle.warren.analytics;

import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;
import android.util.Pair;
import android.widget.VideoView;
import com.moat.analytics.mobile.vng.MoatAdEvent;
import com.moat.analytics.mobile.vng.MoatAdEventType;
import com.moat.analytics.mobile.vng.MoatFactory;
import com.moat.analytics.mobile.vng.MoatOptions;
import com.moat.analytics.mobile.vng.ReactiveVideoTracker;
import com.moat.analytics.mobile.vng.ReactiveVideoTrackerPlugin;
import com.vungle.warren.model.Advertisement;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Queue;
import org.json.JSONException;
import org.json.JSONObject;

public class MoatTracker implements AnalyticsVideoTracker {
    private static final String TAG = "MoatTracker";
    private static final String UNKNOWN = "unknown";
    public static final String VUNGLE_ID = "vunglenativevideo893259554489";
    private HashMap<String, String> adIds = new HashMap<>();
    private boolean configured = true;
    private boolean isMoatEnabled;
    private Queue<Pair<Integer, MoatAdEventType>> moatQuartileTrackers;
    private ReactiveVideoTracker tracker;
    private VideoView videoView;

    private MoatTracker(@NonNull VideoView videoView2, boolean z) {
        this.isMoatEnabled = z;
        this.videoView = videoView2;
    }

    public void configure(@NonNull String str, @NonNull Advertisement advertisement, @NonNull String str2) {
        String str3;
        this.tracker = (ReactiveVideoTracker) MoatFactory.create().createCustomTracker(new ReactiveVideoTrackerPlugin(VUNGLE_ID));
        boolean z = false;
        this.isMoatEnabled = this.isMoatEnabled && !TextUtils.isEmpty(str) && advertisement != null && advertisement.getMoatEnabled();
        if (this.isMoatEnabled) {
            MoatOptions moatOptions = new MoatOptions();
            moatOptions.disableAdIdCollection = true;
            moatOptions.disableLocationServices = true;
            moatOptions.loggingEnabled = false;
            this.moatQuartileTrackers = new LinkedList();
            this.moatQuartileTrackers.add(new Pair(Integer.valueOf(0), MoatAdEventType.AD_EVT_START));
            this.moatQuartileTrackers.add(new Pair(Integer.valueOf(25), MoatAdEventType.AD_EVT_FIRST_QUARTILE));
            this.moatQuartileTrackers.add(new Pair(Integer.valueOf(50), MoatAdEventType.AD_EVT_MID_POINT));
            this.moatQuartileTrackers.add(new Pair(Integer.valueOf(75), MoatAdEventType.AD_EVT_THIRD_QUARTILE));
            if (!advertisement.getMoatVastExtra().isEmpty()) {
                this.adIds.put("zMoatVASTIDs", advertisement.getMoatVastExtra());
            }
            String appID = advertisement.getAppID();
            String appID2 = advertisement.getAppID();
            String str4 = null;
            if (appID2 != null && appID2.length() > 3) {
                try {
                    JSONObject jSONObject = new JSONObject(appID2.substring(3));
                    appID = jSONObject.isNull("app_id") ? null : jSONObject.optString("app_id", null);
                } catch (JSONException e) {
                    Log.e(TAG, "JsonException : ", e);
                }
            }
            String campaign = advertisement.getCampaign();
            if (!TextUtils.isEmpty(campaign)) {
                int indexOf = campaign.indexOf(124);
                if (indexOf != -1) {
                    str4 = campaign.substring(0, indexOf);
                    int i = indexOf + 1;
                    int indexOf2 = campaign.indexOf(124, i);
                    str3 = indexOf2 != -1 ? campaign.substring(i, indexOf2) : campaign.substring(i);
                } else {
                    str4 = campaign;
                    str3 = null;
                }
            } else {
                str3 = null;
            }
            HashMap<String, String> hashMap = this.adIds;
            String str5 = "level1";
            if (TextUtils.isEmpty(appID)) {
                appID = "unknown";
            }
            hashMap.put(str5, appID);
            HashMap<String, String> hashMap2 = this.adIds;
            String str6 = "level2";
            if (TextUtils.isEmpty(str4)) {
                str4 = "unknown";
            }
            hashMap2.put(str6, str4);
            HashMap<String, String> hashMap3 = this.adIds;
            String str7 = "level3";
            if (TextUtils.isEmpty(str3)) {
                str3 = "unknown";
            }
            hashMap3.put(str7, str3);
            HashMap<String, String> hashMap4 = this.adIds;
            String str8 = "level4";
            if (TextUtils.isEmpty(str)) {
                str = "unknown";
            }
            hashMap4.put(str8, str);
            if (!TextUtils.isEmpty(str2)) {
                this.adIds.put("slicer1", str2);
            }
            this.configured = true;
        }
        if (this.configured && this.isMoatEnabled) {
            z = true;
        }
        this.configured = z;
    }

    public static MoatTracker connect(@NonNull VideoView videoView2, boolean z) {
        return new MoatTracker(videoView2, z);
    }

    public void onProgress(int i) {
        if (!this.configured) {
            return;
        }
        if (i >= 100) {
            this.tracker.dispatchEvent(new MoatAdEvent(MoatAdEventType.AD_EVT_COMPLETE, Integer.valueOf(this.videoView.getCurrentPosition())));
            this.tracker.stopTracking();
        } else if (!this.moatQuartileTrackers.isEmpty() && i >= ((Integer) ((Pair) this.moatQuartileTrackers.peek()).first).intValue()) {
            this.tracker.dispatchEvent(new MoatAdEvent((MoatAdEventType) ((Pair) this.moatQuartileTrackers.poll()).second, Integer.valueOf(i)));
        }
    }

    public void start(int i) {
        if (this.configured) {
            Log.d(TAG, "start");
            this.tracker.trackVideoAd(this.adIds, Integer.valueOf(i), this.videoView);
        }
    }

    public void stop() {
        if (this.configured) {
            int currentPosition = this.videoView != null ? this.videoView.getCurrentPosition() : 0;
            String str = TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("stopViewabilityTracker: ");
            sb.append(currentPosition);
            Log.d(str, sb.toString());
            this.tracker.dispatchEvent(new MoatAdEvent(MoatAdEventType.AD_EVT_STOPPED, Integer.valueOf(currentPosition)));
            this.tracker.stopTracking();
            Log.d(TAG, "stopViewabilityTracker: Success !!");
        }
    }
}
