package com.vungle.warren.analytics;

import android.support.annotation.NonNull;
import com.google.gson.JsonObject;

public interface AdAnalytics {
    String[] ping(@NonNull String[] strArr);

    void ri(JsonObject jsonObject);
}
