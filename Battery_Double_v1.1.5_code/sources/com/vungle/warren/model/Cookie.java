package com.vungle.warren.model;

import android.support.annotation.NonNull;
import java.util.HashMap;
import java.util.Map;

public class Cookie {
    public static final String APP_ID = "appId";
    public static final String CONFIG_COOKIE = "configSettings";
    public static final String CONSENT_COOKIE = "consentIsImportantToVungle";
    public static final String INCENTIVIZED_TEXT_COOKIE = "incentivizedTextSetByPub";
    public static final String USER_AGENT_ID_COOKIE = "userAgent";
    Map<String, Boolean> booleans = new HashMap();
    String identifier;
    Map<String, Integer> integers = new HashMap();
    Map<String, Long> longs = new HashMap();
    Map<String, String> strings = new HashMap();

    public Cookie(String str) {
        this.identifier = str;
    }

    public <T> void putValue(String str, T t) {
        if (t instanceof String) {
            this.strings.put(str, (String) t);
        } else if (t instanceof Boolean) {
            this.booleans.put(str, (Boolean) t);
        } else if (t instanceof Integer) {
            this.integers.put(str, (Integer) t);
        } else if (t instanceof Long) {
            this.longs.put(str, (Long) t);
        } else {
            throw new IllegalArgumentException("Value type is not supported!");
        }
    }

    public Integer getInt(String str) {
        return (Integer) this.integers.get(str);
    }

    public String getString(String str) {
        return (String) this.strings.get(str);
    }

    public Boolean getBoolean(String str) {
        return Boolean.valueOf(this.booleans.get(str) != null ? ((Boolean) this.booleans.get(str)).booleanValue() : false);
    }

    public Long getLong(String str) {
        return Long.valueOf(this.longs.get(str) != null ? ((Long) this.longs.get(str)).longValue() : 0);
    }

    @NonNull
    public String getId() {
        return this.identifier;
    }

    public boolean equals(Object obj) {
        boolean z = true;
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        Cookie cookie = (Cookie) obj;
        if (this.strings == null ? cookie.strings != null : !this.strings.equals(cookie.strings)) {
            return false;
        }
        if (this.booleans == null ? cookie.booleans != null : !this.booleans.equals(cookie.booleans)) {
            return false;
        }
        if (this.integers == null ? cookie.integers != null : !this.integers.equals(cookie.integers)) {
            return false;
        }
        if (this.longs == null ? cookie.longs != null : !this.longs.equals(cookie.longs)) {
            return false;
        }
        if (this.identifier != null) {
            z = this.identifier.equals(cookie.identifier);
        } else if (cookie.identifier != null) {
            z = false;
        }
        return z;
    }

    public int hashCode() {
        int i = 0;
        int hashCode = (((((((this.strings != null ? this.strings.hashCode() : 0) * 31) + (this.booleans != null ? this.booleans.hashCode() : 0)) * 31) + (this.integers != null ? this.integers.hashCode() : 0)) * 31) + (this.longs != null ? this.longs.hashCode() : 0)) * 31;
        if (this.identifier != null) {
            i = this.identifier.hashCode();
        }
        return hashCode + i;
    }
}
