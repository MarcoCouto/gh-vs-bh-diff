package com.vungle.warren.model;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Pair;
import com.facebook.share.internal.MessengerShareContentUtility;
import com.facebook.share.internal.ShareConstants;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.tapjoy.TJAdUnitConstants.String;
import com.vungle.warren.AdConfig;
import com.vungle.warren.analytics.AnalyticsEvent.Ad;
import com.vungle.warren.model.AdvertisementDBAdapter.AdvertisementColumns;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import okhttp3.HttpUrl;

public class Advertisement {
    public static final int DONE = 3;
    public static final int ERROR = 4;
    public static final String FILE_SCHEME = "file://";
    public static final String KEY_POSTROLL = "postroll";
    public static final String KEY_TEMPLATE = "template";
    public static final String KEY_VIDEO = "video";
    public static final int LANDSCAPE = 1;
    public static final int NEW = 0;
    public static final int PORTRAIT = 0;
    public static final int READY = 1;
    public static final int ROTATE = 2;
    public static final String START_MUTED = "START_MUTED";
    private static final String TAG = "Advertisement";
    public static final int TYPE_VUNGLE_LOCAL = 0;
    public static final int TYPE_VUNGLE_MRAID = 1;
    public static final int VIEWING = 2;
    AdConfig adConfig;
    String adMarketId;
    String adToken;
    @AdType
    int adType;
    String appID;
    String bidToken;
    Map<String, Pair<String, String>> cacheableAssets = new HashMap();
    String campaign;
    ArrayList<Checkpoint> checkpoints;
    String[] clickUrls;
    String[] closeUrls;
    int countdown;
    int ctaClickArea = -1;
    String ctaDestinationUrl;
    boolean ctaOverlayEnabled;
    String ctaUrl;
    int delay;
    boolean enableMoat;
    long expireTime;
    String identifier;
    String md5;
    String moatExtraVast;
    Map<String, String> mraidFiles = new HashMap();
    String[] muteUrls;
    String placementId;
    String[] postRollClickUrls;
    String[] postRollViewUrls;
    String postrollBundleUrl;
    boolean requiresNonMarketInstall;
    int retryCount;
    int showCloseDelay;
    int showCloseIncentivized;
    int state = 0;
    String templateId;
    Map<String, String> templateSettings;
    String templateType;
    String templateUrl;
    String[] unmuteUrls;
    String[] videoClickUrls;
    int videoHeight;
    String videoIdentifier;
    String videoUrl;
    int videoWidth;

    public @interface AdType {
    }

    public @interface CacheKey {
    }

    public @interface Orientation {
    }

    public @interface State {
    }

    public static class Checkpoint implements Comparable<Checkpoint> {
        private byte percentage;
        private String[] urls;

        public Checkpoint(JsonObject jsonObject) throws IllegalArgumentException {
            if (JsonUtil.hasNonNull(jsonObject, "checkpoint")) {
                this.percentage = (byte) ((int) (jsonObject.get("checkpoint").getAsFloat() * 100.0f));
                if (JsonUtil.hasNonNull(jsonObject, "urls")) {
                    JsonArray asJsonArray = jsonObject.getAsJsonArray("urls");
                    this.urls = new String[asJsonArray.size()];
                    for (int i = 0; i < asJsonArray.size(); i++) {
                        if (asJsonArray.get(i) == null || "null".equalsIgnoreCase(asJsonArray.get(i).toString())) {
                            this.urls[i] = "";
                        } else {
                            this.urls[i] = asJsonArray.get(i).getAsString();
                        }
                    }
                    return;
                }
                throw new IllegalArgumentException("Checkpoint missing reporting URL!");
            }
            throw new IllegalArgumentException("Checkpoint missing percentage!");
        }

        public Checkpoint(JsonArray jsonArray, byte b) {
            if (jsonArray.size() != 0) {
                this.urls = new String[jsonArray.size()];
                for (int i = 0; i < jsonArray.size(); i++) {
                    this.urls[i] = jsonArray.get(i).getAsString();
                }
                this.percentage = b;
                return;
            }
            throw new IllegalArgumentException("Empty URLS!");
        }

        public String[] getUrls() {
            return this.urls;
        }

        public byte getPercentage() {
            return this.percentage;
        }

        public int compareTo(@NonNull Checkpoint checkpoint) {
            return Float.compare((float) this.percentage, (float) checkpoint.percentage);
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof Checkpoint)) {
                return false;
            }
            Checkpoint checkpoint = (Checkpoint) obj;
            if (checkpoint.percentage != this.percentage || checkpoint.urls.length != this.urls.length) {
                return false;
            }
            for (int i = 0; i < this.urls.length; i++) {
                if (!checkpoint.urls[i].equals(this.urls[i])) {
                    return false;
                }
            }
            return true;
        }

        public int hashCode() {
            return super.hashCode();
        }
    }

    Advertisement() {
    }

    public String getPlacementId() {
        return this.placementId;
    }

    public void setPlacementId(String str) {
        this.placementId = str;
    }

    public boolean isCtaOverlayEnabled() {
        return this.ctaOverlayEnabled;
    }

    public boolean getCtaClickArea() {
        return this.ctaClickArea > 0 || this.ctaClickArea == -1;
    }

    public boolean isRequiresNonMarketInstall() {
        return this.requiresNonMarketInstall;
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x0062  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x007e  */
    /* JADX WARNING: Removed duplicated region for block: B:291:0x0776  */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x01e7  */
    /* JADX WARNING: Removed duplicated region for block: B:76:0x022b  */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x022e  */
    /* JADX WARNING: Removed duplicated region for block: B:80:0x023a  */
    public Advertisement(@NonNull JsonObject jsonObject) throws IllegalArgumentException {
        char c;
        String str;
        String str2;
        String str3;
        String str4;
        String str5;
        if (JsonUtil.hasNonNull(jsonObject, "ad_markup")) {
            JsonObject asJsonObject = jsonObject.getAsJsonObject("ad_markup");
            String str6 = "";
            if (JsonUtil.hasNonNull(asJsonObject, "adType")) {
                String asString = asJsonObject.get("adType").getAsString();
                int hashCode = asString.hashCode();
                if (hashCode != -1852456483) {
                    if (hashCode == -1851445271 && asString.equals("vungle_mraid")) {
                        c = 1;
                        String str7 = null;
                        switch (c) {
                            case 0:
                                this.adType = 0;
                                this.postrollBundleUrl = JsonUtil.hasNonNull(asJsonObject, "postBundle") ? asJsonObject.get("postBundle").getAsString() : "";
                                if (JsonUtil.hasNonNull(asJsonObject, "url")) {
                                    str6 = asJsonObject.get("url").getAsString();
                                }
                                this.templateSettings = new HashMap();
                                this.templateUrl = "";
                                this.templateId = "";
                                this.templateType = "";
                                break;
                            case 1:
                                this.adType = 1;
                                this.postrollBundleUrl = "";
                                if (JsonUtil.hasNonNull(asJsonObject, "templateSettings")) {
                                    this.templateSettings = new HashMap();
                                    JsonObject asJsonObject2 = asJsonObject.getAsJsonObject("templateSettings");
                                    if (JsonUtil.hasNonNull(asJsonObject2, "normal_replacements")) {
                                        for (Entry entry : asJsonObject2.getAsJsonObject("normal_replacements").entrySet()) {
                                            if (!TextUtils.isEmpty((CharSequence) entry.getKey())) {
                                                this.templateSettings.put(entry.getKey(), (entry.getValue() == null || ((JsonElement) entry.getValue()).isJsonNull()) ? null : ((JsonElement) entry.getValue()).getAsString());
                                            }
                                        }
                                    }
                                    if (JsonUtil.hasNonNull(asJsonObject2, "cacheable_replacements")) {
                                        for (Entry entry2 : asJsonObject2.getAsJsonObject("cacheable_replacements").entrySet()) {
                                            if (!TextUtils.isEmpty((CharSequence) entry2.getKey()) && entry2.getValue() != null && JsonUtil.hasNonNull((JsonElement) entry2.getValue(), "url") && JsonUtil.hasNonNull((JsonElement) entry2.getValue(), ShareConstants.MEDIA_EXTENSION)) {
                                                String asString2 = ((JsonElement) entry2.getValue()).getAsJsonObject().get("url").getAsString();
                                                this.cacheableAssets.put(entry2.getKey(), new Pair(asString2, ((JsonElement) entry2.getValue()).getAsJsonObject().get(ShareConstants.MEDIA_EXTENSION).getAsString()));
                                                if (((String) entry2.getKey()).equalsIgnoreCase("MAIN_VIDEO")) {
                                                    str6 = asString2;
                                                }
                                            }
                                        }
                                    }
                                    if (JsonUtil.hasNonNull(asJsonObject, "templateId")) {
                                        this.templateId = asJsonObject.get("templateId").getAsString();
                                        if (JsonUtil.hasNonNull(asJsonObject, MessengerShareContentUtility.TEMPLATE_TYPE)) {
                                            this.templateType = asJsonObject.get(MessengerShareContentUtility.TEMPLATE_TYPE).getAsString();
                                            if (JsonUtil.hasNonNull(asJsonObject, "templateURL")) {
                                                this.templateUrl = asJsonObject.get("templateURL").getAsString();
                                                break;
                                            } else {
                                                throw new IllegalArgumentException("Template URL missing!");
                                            }
                                        } else {
                                            throw new IllegalArgumentException("Template Type missing!");
                                        }
                                    } else {
                                        throw new IllegalArgumentException("Missing templateID!");
                                    }
                                } else {
                                    throw new IllegalArgumentException("Missing template adConfig!");
                                }
                            default:
                                StringBuilder sb = new StringBuilder();
                                sb.append("Unknown Ad Type ");
                                sb.append(asString);
                                sb.append("! Please add this ad type");
                                throw new IllegalArgumentException(sb.toString());
                        }
                        if (!TextUtils.isEmpty(str6)) {
                            this.videoUrl = str6;
                        } else {
                            this.videoUrl = "";
                        }
                        if (JsonUtil.hasNonNull(asJsonObject, "id")) {
                            this.identifier = asJsonObject.get("id").getAsString();
                            if (JsonUtil.hasNonNull(asJsonObject, "campaign")) {
                                this.campaign = asJsonObject.get("campaign").getAsString();
                                if (JsonUtil.hasNonNull(asJsonObject, "app_id")) {
                                    this.appID = asJsonObject.get("app_id").getAsString();
                                    if (!JsonUtil.hasNonNull(asJsonObject, "expiry") || asJsonObject.get("expiry").isJsonNull()) {
                                        this.expireTime = System.currentTimeMillis() / 1000;
                                    } else {
                                        long asLong = asJsonObject.get("expiry").getAsLong();
                                        if (asLong > 0) {
                                            this.expireTime = asLong;
                                        } else {
                                            this.expireTime = System.currentTimeMillis() / 1000;
                                        }
                                    }
                                    if (JsonUtil.hasNonNull(asJsonObject, "tpat")) {
                                        JsonObject asJsonObject3 = asJsonObject.getAsJsonObject("tpat");
                                        this.checkpoints = new ArrayList<>(5);
                                        switch (this.adType) {
                                            case 0:
                                                if (JsonUtil.hasNonNull(asJsonObject3, CampaignEx.JSON_NATIVE_VIDEO_PLAY_PERCENTAGE)) {
                                                    JsonArray asJsonArray = asJsonObject3.getAsJsonArray(CampaignEx.JSON_NATIVE_VIDEO_PLAY_PERCENTAGE);
                                                    for (int i = 0; i < asJsonArray.size(); i++) {
                                                        if (asJsonArray.get(i) != null) {
                                                            this.checkpoints.add(new Checkpoint(asJsonArray.get(i).getAsJsonObject()));
                                                        }
                                                    }
                                                    Collections.sort(this.checkpoints);
                                                    break;
                                                }
                                                break;
                                            case 1:
                                                for (int i2 = 0; i2 < 5; i2++) {
                                                    int i3 = i2 * 25;
                                                    String format = String.format(Locale.ENGLISH, "checkpoint.%d", new Object[]{Integer.valueOf(i3)});
                                                    this.checkpoints.add(i2, JsonUtil.hasNonNull(asJsonObject3, format) ? new Checkpoint(asJsonObject3.getAsJsonArray(format), (byte) i3) : null);
                                                }
                                                break;
                                            default:
                                                throw new IllegalArgumentException("Unknown Ad Type!");
                                        }
                                        if (JsonUtil.hasNonNull(asJsonObject3, "clickUrl")) {
                                            JsonArray asJsonArray2 = asJsonObject3.getAsJsonArray("clickUrl");
                                            this.clickUrls = new String[asJsonArray2.size()];
                                            Iterator it = asJsonArray2.iterator();
                                            int i4 = 0;
                                            while (it.hasNext()) {
                                                int i5 = i4 + 1;
                                                this.clickUrls[i4] = ((JsonElement) it.next()).getAsString();
                                                i4 = i5;
                                            }
                                        } else {
                                            this.clickUrls = new String[0];
                                        }
                                        if (JsonUtil.hasNonNull(asJsonObject3, "moat")) {
                                            JsonObject asJsonObject4 = asJsonObject3.getAsJsonObject("moat");
                                            this.enableMoat = asJsonObject4.get("is_enabled").getAsBoolean();
                                            this.moatExtraVast = asJsonObject4.get("extra_vast").getAsString();
                                        } else {
                                            this.enableMoat = false;
                                            this.moatExtraVast = "";
                                        }
                                        if (JsonUtil.hasNonNull(asJsonObject3, "video_click")) {
                                            JsonArray asJsonArray3 = asJsonObject3.getAsJsonArray("video_click");
                                            this.videoClickUrls = new String[asJsonArray3.size()];
                                            for (int i6 = 0; i6 < asJsonArray3.size(); i6++) {
                                                if (asJsonArray3.get(i6) == null || "null".equalsIgnoreCase(asJsonArray3.get(i6).toString())) {
                                                    this.videoClickUrls[i6] = "";
                                                } else {
                                                    this.videoClickUrls[i6] = asJsonArray3.get(i6).getAsString();
                                                }
                                            }
                                        } else {
                                            this.videoClickUrls = new String[0];
                                        }
                                        switch (this.adType) {
                                            case 0:
                                                str5 = "mute";
                                                str4 = "unmute";
                                                str3 = Ad.videoClose;
                                                str2 = Ad.postrollClick;
                                                str = Ad.postrollView;
                                                break;
                                            case 1:
                                                str5 = "video.mute";
                                                str4 = "video.unmute";
                                                str3 = "video.close";
                                                str2 = "postroll.click";
                                                str = "postroll.view";
                                                break;
                                            default:
                                                throw new IllegalArgumentException("Unknown AdType!");
                                        }
                                        if (JsonUtil.hasNonNull(asJsonObject3, str5)) {
                                            JsonArray asJsonArray4 = asJsonObject3.getAsJsonArray(str5);
                                            this.muteUrls = new String[asJsonArray4.size()];
                                            for (int i7 = 0; i7 < asJsonArray4.size(); i7++) {
                                                if (asJsonArray4.get(i7) == null || "null".equalsIgnoreCase(asJsonArray4.get(i7).toString())) {
                                                    this.muteUrls[i7] = "";
                                                } else {
                                                    this.muteUrls[i7] = asJsonArray4.get(i7).getAsString();
                                                }
                                            }
                                        } else {
                                            this.muteUrls = new String[0];
                                        }
                                        if (JsonUtil.hasNonNull(asJsonObject3, str4)) {
                                            JsonArray asJsonArray5 = asJsonObject3.getAsJsonArray(str4);
                                            this.unmuteUrls = new String[asJsonArray5.size()];
                                            for (int i8 = 0; i8 < asJsonArray5.size(); i8++) {
                                                if (asJsonArray5.get(i8) == null || "null".equalsIgnoreCase(asJsonArray5.get(i8).toString())) {
                                                    this.unmuteUrls[i8] = "";
                                                } else {
                                                    this.unmuteUrls[i8] = asJsonArray5.get(i8).getAsString();
                                                }
                                            }
                                        } else {
                                            this.unmuteUrls = new String[0];
                                        }
                                        if (JsonUtil.hasNonNull(asJsonObject3, str3)) {
                                            JsonArray asJsonArray6 = asJsonObject3.getAsJsonArray(str3);
                                            this.closeUrls = new String[asJsonArray6.size()];
                                            for (int i9 = 0; i9 < asJsonArray6.size(); i9++) {
                                                if (asJsonArray6.get(i9) == null || "null".equalsIgnoreCase(asJsonArray6.get(i9).toString())) {
                                                    this.closeUrls[i9] = "";
                                                } else {
                                                    this.closeUrls[i9] = asJsonArray6.get(i9).getAsString();
                                                }
                                            }
                                        } else {
                                            this.closeUrls = new String[0];
                                        }
                                        if (JsonUtil.hasNonNull(asJsonObject3, str2)) {
                                            JsonArray asJsonArray7 = asJsonObject3.getAsJsonArray(str2);
                                            this.postRollClickUrls = new String[asJsonArray7.size()];
                                            for (int i10 = 0; i10 < asJsonArray7.size(); i10++) {
                                                if (asJsonArray7.get(i10) == null || "null".equalsIgnoreCase(asJsonArray7.get(i10).toString())) {
                                                    this.postRollClickUrls[i10] = "";
                                                } else {
                                                    this.postRollClickUrls[i10] = asJsonArray7.get(i10).getAsString();
                                                }
                                            }
                                        } else {
                                            this.postRollClickUrls = new String[0];
                                        }
                                        if (JsonUtil.hasNonNull(asJsonObject3, str)) {
                                            JsonArray asJsonArray8 = asJsonObject3.getAsJsonArray(str);
                                            this.postRollViewUrls = new String[asJsonArray8.size()];
                                            for (int i11 = 0; i11 < asJsonArray8.size(); i11++) {
                                                if (asJsonArray8.get(i11) == null || "null".equalsIgnoreCase(asJsonArray8.get(i11).toString())) {
                                                    this.postRollViewUrls[i11] = "";
                                                } else {
                                                    this.postRollViewUrls[i11] = asJsonArray8.get(i11).getAsString();
                                                }
                                            }
                                        } else {
                                            this.postRollViewUrls = new String[0];
                                        }
                                    } else {
                                        this.checkpoints = new ArrayList<>();
                                        this.muteUrls = new String[0];
                                        this.closeUrls = new String[0];
                                        this.unmuteUrls = new String[0];
                                        this.postRollViewUrls = new String[0];
                                        this.postRollClickUrls = new String[0];
                                        this.clickUrls = new String[0];
                                        this.videoClickUrls = new String[0];
                                        this.enableMoat = false;
                                        this.moatExtraVast = "";
                                    }
                                    if (JsonUtil.hasNonNull(asJsonObject, AdvertisementColumns.COLUMN_DELAY)) {
                                        this.delay = asJsonObject.get(AdvertisementColumns.COLUMN_DELAY).getAsInt();
                                    } else {
                                        this.delay = 0;
                                    }
                                    if (JsonUtil.hasNonNull(asJsonObject, "showClose")) {
                                        this.showCloseDelay = asJsonObject.get("showClose").getAsInt();
                                    } else {
                                        this.showCloseDelay = 0;
                                    }
                                    if (JsonUtil.hasNonNull(asJsonObject, "showCloseIncentivized")) {
                                        this.showCloseIncentivized = asJsonObject.get("showCloseIncentivized").getAsInt();
                                    } else {
                                        this.showCloseIncentivized = 0;
                                    }
                                    if (JsonUtil.hasNonNull(asJsonObject, AdvertisementColumns.COLUMN_COUNTDOWN)) {
                                        this.countdown = asJsonObject.get(AdvertisementColumns.COLUMN_COUNTDOWN).getAsInt();
                                    } else {
                                        this.countdown = 0;
                                    }
                                    if (JsonUtil.hasNonNull(asJsonObject, String.VIDEO_WIDTH)) {
                                        this.videoWidth = asJsonObject.get(String.VIDEO_WIDTH).getAsInt();
                                        if (JsonUtil.hasNonNull(asJsonObject, String.VIDEO_HEIGHT)) {
                                            this.videoHeight = asJsonObject.get(String.VIDEO_HEIGHT).getAsInt();
                                            if (JsonUtil.hasNonNull(asJsonObject, AdvertisementColumns.COLUMN_MD5)) {
                                                this.md5 = asJsonObject.get(AdvertisementColumns.COLUMN_MD5).getAsString();
                                            } else {
                                                this.md5 = "";
                                            }
                                            if (JsonUtil.hasNonNull(asJsonObject, "cta_overlay")) {
                                                JsonObject asJsonObject5 = asJsonObject.getAsJsonObject("cta_overlay");
                                                if (JsonUtil.hasNonNull(asJsonObject5, String.ENABLED)) {
                                                    this.ctaOverlayEnabled = asJsonObject5.get(String.ENABLED).getAsBoolean();
                                                } else {
                                                    this.ctaOverlayEnabled = false;
                                                }
                                                if (!JsonUtil.hasNonNull(asJsonObject5, "click_area")) {
                                                    this.ctaClickArea = -1;
                                                } else if (asJsonObject5.get("click_area").getAsString().isEmpty()) {
                                                    this.ctaClickArea = -1;
                                                } else {
                                                    this.ctaClickArea = asJsonObject5.get("click_area").getAsInt();
                                                }
                                            } else {
                                                this.ctaOverlayEnabled = false;
                                                this.ctaClickArea = -1;
                                            }
                                            this.ctaDestinationUrl = JsonUtil.hasNonNull(asJsonObject, "callToActionDest") ? asJsonObject.get("callToActionDest").getAsString() : null;
                                            if (JsonUtil.hasNonNull(asJsonObject, "callToActionUrl")) {
                                                str7 = asJsonObject.get("callToActionUrl").getAsString();
                                            }
                                            this.ctaUrl = str7;
                                            if (JsonUtil.hasNonNull(asJsonObject, "retryCount")) {
                                                this.retryCount = asJsonObject.get("retryCount").getAsInt();
                                            } else {
                                                this.retryCount = 1;
                                            }
                                            if (JsonUtil.hasNonNull(asJsonObject, AdvertisementColumns.COLUMN_AD_TOKEN)) {
                                                this.adToken = asJsonObject.get(AdvertisementColumns.COLUMN_AD_TOKEN).getAsString();
                                                if (JsonUtil.hasNonNull(asJsonObject, "video_object_id")) {
                                                    this.videoIdentifier = asJsonObject.get("video_object_id").getAsString();
                                                } else {
                                                    this.videoIdentifier = "";
                                                }
                                                if (JsonUtil.hasNonNull(asJsonObject, "requires_sideloading")) {
                                                    this.requiresNonMarketInstall = asJsonObject.get("requires_sideloading").getAsBoolean();
                                                } else {
                                                    this.requiresNonMarketInstall = false;
                                                }
                                                if (JsonUtil.hasNonNull(asJsonObject, AdvertisementColumns.COLUMN_AD_MARKET_ID)) {
                                                    this.adMarketId = asJsonObject.get(AdvertisementColumns.COLUMN_AD_MARKET_ID).getAsString();
                                                } else {
                                                    this.adMarketId = "";
                                                }
                                                if (JsonUtil.hasNonNull(asJsonObject, AdvertisementColumns.COLUMN_BID_TOKEN)) {
                                                    this.bidToken = asJsonObject.get(AdvertisementColumns.COLUMN_BID_TOKEN).getAsString();
                                                } else {
                                                    this.bidToken = "";
                                                }
                                                this.adConfig = new AdConfig();
                                                return;
                                            }
                                            throw new IllegalArgumentException("AdToken missing!");
                                        }
                                        throw new IllegalArgumentException("Missing video height!");
                                    }
                                    throw new IllegalArgumentException("Missing video width!");
                                }
                                throw new IllegalArgumentException("Missing app Id, cannot process advertisement!");
                            }
                            throw new IllegalArgumentException("Missing campaign information, cannot process advertisement!");
                        }
                        throw new IllegalArgumentException("Missing identifier, cannot process advertisement!");
                    }
                } else if (asString.equals("vungle_local")) {
                    c = 0;
                    String str72 = null;
                    switch (c) {
                        case 0:
                            break;
                        case 1:
                            break;
                    }
                    if (!TextUtils.isEmpty(str6)) {
                    }
                    if (JsonUtil.hasNonNull(asJsonObject, "id")) {
                    }
                }
                c = 65535;
                String str722 = null;
                switch (c) {
                    case 0:
                        break;
                    case 1:
                        break;
                }
                if (!TextUtils.isEmpty(str6)) {
                }
                if (JsonUtil.hasNonNull(asJsonObject, "id")) {
                }
            } else {
                throw new IllegalArgumentException("Advertisement did not contain an adType!");
            }
        } else {
            throw new IllegalArgumentException("JSON does not contain ad markup!");
        }
    }

    @AdType
    public int getAdType() {
        return this.adType;
    }

    public List<Checkpoint> getCheckpoints() {
        return this.checkpoints;
    }

    public void configure(AdConfig adConfig2) {
        if (adConfig2 == null) {
            this.adConfig = new AdConfig();
        } else {
            this.adConfig = adConfig2;
        }
    }

    public AdConfig getAdConfig() {
        return this.adConfig;
    }

    @Orientation
    public int getOrientation() {
        return this.videoWidth > this.videoHeight ? 1 : 0;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof Advertisement)) {
            return false;
        }
        Advertisement advertisement = (Advertisement) obj;
        if (getId() == null || advertisement.getId() == null || !advertisement.getId().equals(getId()) || advertisement.adType != this.adType || advertisement.expireTime != this.expireTime || advertisement.delay != this.delay || advertisement.showCloseDelay != this.showCloseDelay || advertisement.showCloseIncentivized != this.showCloseIncentivized || advertisement.countdown != this.countdown || advertisement.videoWidth != this.videoWidth || advertisement.videoHeight != this.videoHeight || advertisement.ctaOverlayEnabled != this.ctaOverlayEnabled || advertisement.ctaClickArea != this.ctaClickArea || advertisement.retryCount != this.retryCount || advertisement.enableMoat != this.enableMoat || advertisement.requiresNonMarketInstall != this.requiresNonMarketInstall || !advertisement.identifier.equals(this.identifier) || !advertisement.campaign.equals(this.campaign) || !advertisement.videoUrl.equals(this.videoUrl) || !advertisement.md5.equals(this.md5) || !advertisement.postrollBundleUrl.equals(this.postrollBundleUrl) || !advertisement.ctaDestinationUrl.equals(this.ctaDestinationUrl) || !advertisement.ctaUrl.equals(this.ctaUrl) || !advertisement.adToken.equals(this.adToken) || !advertisement.videoIdentifier.equals(this.videoIdentifier) || !advertisement.moatExtraVast.equals(this.moatExtraVast) || advertisement.state != this.state || advertisement.muteUrls.length != this.muteUrls.length) {
            return false;
        }
        for (int i = 0; i < this.muteUrls.length; i++) {
            if (!advertisement.muteUrls[i].equals(this.muteUrls[i])) {
                return false;
            }
        }
        if (advertisement.unmuteUrls.length != this.unmuteUrls.length) {
            return false;
        }
        for (int i2 = 0; i2 < this.unmuteUrls.length; i2++) {
            if (!advertisement.unmuteUrls[i2].equals(this.unmuteUrls[i2])) {
                return false;
            }
        }
        if (advertisement.closeUrls.length != this.closeUrls.length) {
            return false;
        }
        for (int i3 = 0; i3 < this.closeUrls.length; i3++) {
            if (!advertisement.closeUrls[i3].equals(this.closeUrls[i3])) {
                return false;
            }
        }
        if (advertisement.postRollClickUrls.length != this.postRollClickUrls.length) {
            return false;
        }
        for (int i4 = 0; i4 < this.postRollClickUrls.length; i4++) {
            if (!advertisement.postRollClickUrls[i4].equals(this.postRollClickUrls[i4])) {
                return false;
            }
        }
        if (advertisement.postRollViewUrls.length != this.postRollViewUrls.length) {
            return false;
        }
        for (int i5 = 0; i5 < this.postRollViewUrls.length; i5++) {
            if (!advertisement.postRollViewUrls[i5].equals(this.postRollViewUrls[i5])) {
                return false;
            }
        }
        if (advertisement.videoClickUrls.length != this.videoClickUrls.length) {
            return false;
        }
        for (int i6 = 0; i6 < this.videoClickUrls.length; i6++) {
            if (!advertisement.videoClickUrls[i6].equals(this.videoClickUrls[i6])) {
                return false;
            }
        }
        if (advertisement.checkpoints.size() != this.checkpoints.size()) {
            return false;
        }
        for (int i7 = 0; i7 < this.checkpoints.size(); i7++) {
            if (!((Checkpoint) advertisement.checkpoints.get(i7)).equals(this.checkpoints.get(i7))) {
                return false;
            }
        }
        if (advertisement.adMarketId.equals(this.adMarketId) && advertisement.bidToken.equals(this.bidToken)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        return super.hashCode();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0086, code lost:
        if (r10.equals("video.mute") != false) goto L_0x008a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x00d1, code lost:
        if (r10.equals(com.vungle.warren.analytics.AnalyticsEvent.Ad.videoClose) != false) goto L_0x0107;
     */
    public String[] getTpatUrls(@NonNull String str) {
        char c = 4;
        switch (this.adType) {
            case 0:
                switch (str.hashCode()) {
                    case -1964722632:
                        if (str.equals("click_url")) {
                            c = 5;
                            break;
                        }
                    case -840405966:
                        if (str.equals("unmute")) {
                            c = 3;
                            break;
                        }
                    case 3363353:
                        if (str.equals("mute")) {
                            c = 2;
                            break;
                        }
                    case 109635558:
                        if (str.equals(Ad.postrollClick)) {
                            c = 1;
                            break;
                        }
                    case 1370600644:
                        if (str.equals("video_click")) {
                            c = 6;
                            break;
                        }
                    case 1370606900:
                        break;
                    case 1666667655:
                        if (str.equals(Ad.postrollView)) {
                            c = 0;
                            break;
                        }
                    default:
                        c = 65535;
                        break;
                }
                switch (c) {
                    case 0:
                        return this.postRollViewUrls;
                    case 1:
                        return this.postRollClickUrls;
                    case 2:
                        return this.muteUrls;
                    case 3:
                        return this.unmuteUrls;
                    case 4:
                        return this.closeUrls;
                    case 5:
                        return this.clickUrls;
                    case 6:
                        return this.videoClickUrls;
                    default:
                        StringBuilder sb = new StringBuilder();
                        sb.append("Unknown TPAT Event ");
                        sb.append(str);
                        throw new IllegalArgumentException(sb.toString());
                }
            case 1:
                if (str.startsWith("checkpoint")) {
                    String[] strArr = new String[0];
                    Checkpoint checkpoint = (Checkpoint) this.checkpoints.get(Integer.parseInt(str.split("\\.")[1]) / 25);
                    if (checkpoint != null) {
                        strArr = checkpoint.getUrls();
                    }
                    return strArr;
                }
                switch (str.hashCode()) {
                    case -1663300692:
                        break;
                    case -1293192841:
                        if (str.equals("postroll.click")) {
                            c = 2;
                            break;
                        }
                    case -481751803:
                        if (str.equals("video.unmute")) {
                            c = 5;
                            break;
                        }
                    case -32221499:
                        if (str.equals("video.close")) {
                            c = 0;
                            break;
                        }
                    case 906443463:
                        if (str.equals("clickUrl")) {
                            c = 3;
                            break;
                        }
                    case 1370600644:
                        if (str.equals("video_click")) {
                            c = 6;
                            break;
                        }
                    case 1621415126:
                        if (str.equals("postroll.view")) {
                            c = 1;
                            break;
                        }
                    default:
                        c = 65535;
                        break;
                }
                switch (c) {
                    case 0:
                        return this.closeUrls;
                    case 1:
                        return this.postRollViewUrls;
                    case 2:
                        return this.postRollClickUrls;
                    case 3:
                        return this.clickUrls;
                    case 4:
                        return this.muteUrls;
                    case 5:
                        return this.unmuteUrls;
                    case 6:
                        return this.videoClickUrls;
                    default:
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append("Unknown TPAT Event ");
                        sb2.append(str);
                        throw new IllegalArgumentException(sb2.toString());
                }
            default:
                throw new IllegalStateException("Unknown Advertisement Type!");
        }
    }

    @NonNull
    public String getId() {
        if (this.identifier == null) {
            return "";
        }
        return this.identifier;
    }

    public String getAdToken() {
        return this.adToken;
    }

    public String getAppID() {
        return this.appID;
    }

    /* access modifiers changed from: 0000 */
    public String getUrl() {
        return this.videoUrl;
    }

    public String getCampaign() {
        return this.campaign;
    }

    /* access modifiers changed from: 0000 */
    public String getTemplateId() {
        return this.templateId;
    }

    public String getTemplateType() {
        return this.templateType;
    }

    public int getShowCloseDelay(boolean z) {
        if (z) {
            return this.showCloseIncentivized * 1000;
        }
        return this.showCloseDelay * 1000;
    }

    public boolean getMoatEnabled() {
        return this.enableMoat;
    }

    public String getMoatVastExtra() {
        return this.moatExtraVast;
    }

    public long getExpireTime() {
        return this.expireTime * 1000;
    }

    public JsonObject createMRAIDArgs() {
        if (this.templateSettings != null) {
            HashMap hashMap = new HashMap(this.templateSettings);
            for (Entry entry : this.cacheableAssets.entrySet()) {
                hashMap.put(entry.getKey(), ((Pair) entry.getValue()).first);
            }
            if (!this.mraidFiles.isEmpty()) {
                hashMap.putAll(this.mraidFiles);
            }
            if (!"true".equalsIgnoreCase((String) hashMap.get(START_MUTED))) {
                hashMap.put(START_MUTED, (getAdConfig().getSettings() & 1) != 0 ? "true" : "false");
            }
            JsonObject jsonObject = new JsonObject();
            for (Entry entry2 : hashMap.entrySet()) {
                jsonObject.addProperty((String) entry2.getKey(), (String) entry2.getValue());
            }
            return jsonObject;
        }
        throw new IllegalArgumentException("Advertisement does not have MRAID Arguments!");
    }

    @Nullable
    public String getCTAURL(boolean z) {
        switch (this.adType) {
            case 0:
                return z ? this.ctaUrl : this.ctaDestinationUrl;
            case 1:
                return this.ctaUrl;
            default:
                StringBuilder sb = new StringBuilder();
                sb.append("Unknown AdType ");
                sb.append(this.adType);
                throw new IllegalArgumentException(sb.toString());
        }
    }

    public boolean hasPostroll() {
        return !TextUtils.isEmpty(this.postrollBundleUrl);
    }

    public Map<String, String> getDownloadableUrls() {
        HashMap hashMap = new HashMap();
        switch (this.adType) {
            case 0:
                hashMap.put("video", this.videoUrl);
                if (!TextUtils.isEmpty(this.postrollBundleUrl)) {
                    hashMap.put("postroll", this.postrollBundleUrl);
                    break;
                }
                break;
            case 1:
                hashMap.put("template", this.templateUrl);
                for (Entry entry : this.cacheableAssets.entrySet()) {
                    String str = (String) ((Pair) entry.getValue()).first;
                    if (!TextUtils.isEmpty(str) && HttpUrl.parse(str) != null) {
                        StringBuilder sb = new StringBuilder();
                        sb.append((String) entry.getKey());
                        sb.append(".");
                        sb.append((String) ((Pair) entry.getValue()).second);
                        hashMap.put(sb.toString(), str);
                    }
                }
                break;
            default:
                throw new IllegalStateException("Advertisement created without adType!");
        }
        return hashMap;
    }

    public void setMraidAssetDir(File file) {
        for (Entry entry : this.cacheableAssets.entrySet()) {
            StringBuilder sb = new StringBuilder();
            sb.append((String) entry.getKey());
            sb.append(".");
            sb.append((String) ((Pair) entry.getValue()).second);
            File file2 = new File(file, sb.toString());
            if (file2.exists()) {
                Map<String, String> map = this.mraidFiles;
                Object key = entry.getKey();
                StringBuilder sb2 = new StringBuilder();
                sb2.append(FILE_SCHEME);
                sb2.append(file2.getPath());
                map.put(key, sb2.toString());
            }
        }
    }

    public void setState(@State int i) {
        this.state = i;
    }

    @State
    public int getState() {
        return this.state;
    }

    public String getAdMarketId() {
        return this.adMarketId;
    }

    public String getBidToken() {
        return this.bidToken;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Advertisement{adType=");
        sb.append(this.adType);
        sb.append(", identifier='");
        sb.append(this.identifier);
        sb.append('\'');
        sb.append(", appID='");
        sb.append(this.appID);
        sb.append('\'');
        sb.append(", expireTime=");
        sb.append(this.expireTime);
        sb.append(", checkpoints=");
        sb.append(this.checkpoints);
        sb.append(", muteUrls=");
        sb.append(Arrays.toString(this.muteUrls));
        sb.append(", unmuteUrls=");
        sb.append(Arrays.toString(this.unmuteUrls));
        sb.append(", closeUrls=");
        sb.append(Arrays.toString(this.closeUrls));
        sb.append(", postRollClickUrls=");
        sb.append(Arrays.toString(this.postRollClickUrls));
        sb.append(", postRollViewUrls=");
        sb.append(Arrays.toString(this.postRollViewUrls));
        sb.append(", videoClickUrls=");
        sb.append(Arrays.toString(this.videoClickUrls));
        sb.append(", clickUrls=");
        sb.append(Arrays.toString(this.clickUrls));
        sb.append(", delay=");
        sb.append(this.delay);
        sb.append(", campaign='");
        sb.append(this.campaign);
        sb.append('\'');
        sb.append(", showCloseDelay=");
        sb.append(this.showCloseDelay);
        sb.append(", showCloseIncentivized=");
        sb.append(this.showCloseIncentivized);
        sb.append(", countdown=");
        sb.append(this.countdown);
        sb.append(", videoUrl='");
        sb.append(this.videoUrl);
        sb.append('\'');
        sb.append(", videoWidth=");
        sb.append(this.videoWidth);
        sb.append(", videoHeight=");
        sb.append(this.videoHeight);
        sb.append(", md5='");
        sb.append(this.md5);
        sb.append('\'');
        sb.append(", postrollBundleUrl='");
        sb.append(this.postrollBundleUrl);
        sb.append('\'');
        sb.append(", ctaOverlayEnabled=");
        sb.append(this.ctaOverlayEnabled);
        sb.append(", ctaClickArea=");
        sb.append(this.ctaClickArea);
        sb.append(", ctaDestinationUrl='");
        sb.append(this.ctaDestinationUrl);
        sb.append('\'');
        sb.append(", ctaUrl='");
        sb.append(this.ctaUrl);
        sb.append('\'');
        sb.append(", adConfig=");
        sb.append(this.adConfig);
        sb.append(", retryCount=");
        sb.append(this.retryCount);
        sb.append(", adToken='");
        sb.append(this.adToken);
        sb.append('\'');
        sb.append(", videoIdentifier='");
        sb.append(this.videoIdentifier);
        sb.append('\'');
        sb.append(", templateUrl='");
        sb.append(this.templateUrl);
        sb.append('\'');
        sb.append(", templateSettings=");
        sb.append(this.templateSettings);
        sb.append(", mraidFiles=");
        sb.append(this.mraidFiles);
        sb.append(", cacheableAssets=");
        sb.append(this.cacheableAssets);
        sb.append(", templateId='");
        sb.append(this.templateId);
        sb.append('\'');
        sb.append(", templateType='");
        sb.append(this.templateType);
        sb.append('\'');
        sb.append(", enableMoat=");
        sb.append(this.enableMoat);
        sb.append(", moatExtraVast='");
        sb.append(this.moatExtraVast);
        sb.append('\'');
        sb.append(", requiresNonMarketInstall=");
        sb.append(this.requiresNonMarketInstall);
        sb.append(", adMarketId='");
        sb.append(this.adMarketId);
        sb.append('\'');
        sb.append(", bidToken='");
        sb.append(this.bidToken);
        sb.append('\'');
        sb.append(", state=");
        sb.append(this.state);
        sb.append('}');
        return sb.toString();
    }
}
