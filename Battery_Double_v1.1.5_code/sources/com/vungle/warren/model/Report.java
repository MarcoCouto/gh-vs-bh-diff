package com.vungle.warren.model;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.vungle.warren.model.AdvertisementDBAdapter.AdvertisementColumns;
import com.vungle.warren.model.ReportDBAdapter.ReportColumns;
import com.vungle.warren.ui.JavascriptBridge.MraidHandler;
import io.fabric.sdk.android.services.events.EventsFilesManager;
import java.util.ArrayList;
import java.util.Iterator;

public class Report {
    public static final int FAILED = 3;
    public static final int NEW = 0;
    public static final int READY = 1;
    public static final int SENDING = 2;
    long adDuration;
    long adStartTime;
    String adToken;
    String adType;
    String advertisementID;
    String appId;
    String campaign;
    final ArrayList<String> clickedThrough;
    final ArrayList<String> errors;
    boolean incentivized;
    int ordinal;
    String placementId;
    int status;
    String templateId;
    int ttDownload;
    String url;
    final ArrayList<UserAction> userActions;
    String userID;
    long videoLength;
    int videoViewed;
    volatile boolean wasCTAClicked;

    public @interface Status {
    }

    public static class UserAction {
        private String action;
        private String id;
        private long timestamp;
        private String value;

        public UserAction(String str, String str2, long j) {
            this.action = str;
            this.value = str2;
            this.timestamp = j;
            StringBuilder sb = new StringBuilder();
            sb.append("");
            sb.append(this.id);
            this.id = sb.toString();
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof UserAction)) {
                return false;
            }
            UserAction userAction = (UserAction) obj;
            if (userAction.action.equals(this.action) && userAction.value.equals(this.value) && userAction.timestamp == this.timestamp) {
                return true;
            }
            return false;
        }

        public JsonObject toJson() {
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("action", this.action);
            if (this.value != null && !this.value.isEmpty()) {
                jsonObject.addProperty("value", this.value);
            }
            jsonObject.addProperty("timestamp_millis", (Number) Long.valueOf(this.timestamp));
            return jsonObject;
        }
    }

    Report() {
        this.status = 0;
        this.userActions = new ArrayList<>();
        this.clickedThrough = new ArrayList<>();
        this.errors = new ArrayList<>();
    }

    public Report(@NonNull Advertisement advertisement, @NonNull Placement placement, long j) {
        this(advertisement, placement, j, null);
    }

    public Report(@NonNull Advertisement advertisement, @NonNull Placement placement, long j, @Nullable String str) {
        this.status = 0;
        this.userActions = new ArrayList<>();
        this.clickedThrough = new ArrayList<>();
        this.errors = new ArrayList<>();
        this.placementId = placement.getId();
        this.adToken = advertisement.getAdToken();
        this.advertisementID = advertisement.getId();
        this.appId = advertisement.getAppID();
        this.incentivized = placement.isIncentivized();
        this.adStartTime = j;
        this.url = advertisement.getUrl();
        this.ttDownload = -1;
        this.campaign = advertisement.getCampaign();
        switch (advertisement.getAdType()) {
            case 0:
                this.adType = "vungle_local";
                break;
            case 1:
                this.adType = "vungle_mraid";
                break;
            default:
                throw new IllegalArgumentException("Unknown ad type, cannot process!");
        }
        this.templateId = advertisement.getTemplateId();
        if (str == null) {
            this.userID = "";
        } else {
            this.userID = str;
        }
        this.ordinal = advertisement.getAdConfig().getOrdinal();
    }

    public synchronized void recordAction(String str, String str2, long j) {
        this.userActions.add(new UserAction(str, str2, j));
        this.clickedThrough.add(str);
        if (str.equals(MraidHandler.DOWNLOAD_ACTION)) {
            this.wasCTAClicked = true;
        }
    }

    public synchronized void recordError(String str) {
        this.errors.add(str);
    }

    public void recordProgress(int i) {
        this.videoViewed = i;
    }

    public void setVideoLength(long j) {
        this.videoLength = j;
    }

    public void setAdDuration(int i) {
        this.adDuration = (long) i;
    }

    public String getPlacementId() {
        return this.placementId;
    }

    public String getAdvertisementID() {
        return this.advertisementID;
    }

    public boolean isCTAClicked() {
        return this.wasCTAClicked;
    }

    public String getUserID() {
        return this.userID;
    }

    public JsonObject toReportBody() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("placement_reference_id", this.placementId);
        jsonObject.addProperty(AdvertisementColumns.COLUMN_AD_TOKEN, this.adToken);
        jsonObject.addProperty("app_id", this.appId);
        jsonObject.addProperty("incentivized", (Number) Integer.valueOf(this.incentivized ? 1 : 0));
        jsonObject.addProperty(ReportColumns.COLUMN_AD_START_TIME, (Number) Long.valueOf(this.adStartTime));
        if (!TextUtils.isEmpty(this.url)) {
            jsonObject.addProperty("url", this.url);
        }
        jsonObject.addProperty("adDuration", (Number) Long.valueOf(this.adDuration));
        jsonObject.addProperty("ttDownload", (Number) Integer.valueOf(this.ttDownload));
        jsonObject.addProperty("campaign", this.campaign);
        jsonObject.addProperty("adType", this.adType);
        jsonObject.addProperty("templateId", this.templateId);
        JsonArray jsonArray = new JsonArray();
        JsonObject jsonObject2 = new JsonObject();
        jsonObject2.addProperty("startTime", (Number) Long.valueOf(this.adStartTime));
        if (this.videoViewed > 0) {
            jsonObject2.addProperty(ReportColumns.COLUMN_VIDEO_VIEWED, (Number) Integer.valueOf(this.videoViewed));
        }
        if (this.videoLength > 0) {
            jsonObject2.addProperty("videoLength", (Number) Long.valueOf(this.videoLength));
        }
        JsonArray jsonArray2 = new JsonArray();
        Iterator it = this.userActions.iterator();
        while (it.hasNext()) {
            jsonArray2.add((JsonElement) ((UserAction) it.next()).toJson());
        }
        jsonObject2.add("userActions", jsonArray2);
        jsonArray.add((JsonElement) jsonObject2);
        jsonObject.add("plays", jsonArray);
        JsonArray jsonArray3 = new JsonArray();
        Iterator it2 = this.errors.iterator();
        while (it2.hasNext()) {
            jsonArray3.add((String) it2.next());
        }
        jsonObject.add(ReportColumns.COLUMN_ERRORS, jsonArray3);
        JsonArray jsonArray4 = new JsonArray();
        Iterator it3 = this.clickedThrough.iterator();
        while (it3.hasNext()) {
            jsonArray4.add((String) it3.next());
        }
        jsonObject.add("clickedThrough", jsonArray4);
        if (this.incentivized && !TextUtils.isEmpty(this.userID)) {
            jsonObject.addProperty("user", this.userID);
        }
        if (this.ordinal > 0) {
            jsonObject.addProperty("ordinal_view", (Number) Integer.valueOf(this.ordinal));
        }
        return jsonObject;
    }

    @NonNull
    public String getId() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.placementId);
        sb.append(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
        sb.append(this.adStartTime);
        return sb.toString();
    }

    public long getAdStartTime() {
        return this.adStartTime;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof Report)) {
            return false;
        }
        Report report = (Report) obj;
        if (!report.placementId.equals(this.placementId) || !report.adToken.equals(this.adToken) || !report.appId.equals(this.appId) || report.incentivized != this.incentivized || report.adStartTime != this.adStartTime || !report.url.equals(this.url) || report.videoLength != this.videoLength || report.adDuration != this.adDuration || report.ttDownload != this.ttDownload || !report.campaign.equals(this.campaign) || !report.adType.equals(this.adType) || !report.templateId.equals(this.templateId) || report.wasCTAClicked != this.wasCTAClicked || !report.userID.equals(this.userID) || report.clickedThrough.size() != this.clickedThrough.size()) {
            return false;
        }
        for (int i = 0; i < this.clickedThrough.size(); i++) {
            if (!((String) report.clickedThrough.get(i)).equals(this.clickedThrough.get(i))) {
                return false;
            }
        }
        if (report.errors.size() != this.errors.size()) {
            return false;
        }
        for (int i2 = 0; i2 < this.errors.size(); i2++) {
            if (!((String) report.errors.get(i2)).equals(this.errors.get(i2))) {
                return false;
            }
        }
        if (report.userActions.size() != this.userActions.size()) {
            return false;
        }
        for (int i3 = 0; i3 < this.userActions.size(); i3++) {
            if (!((UserAction) report.userActions.get(i3)).equals(this.userActions.get(i3))) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        return super.hashCode();
    }

    @Status
    public int getStatus() {
        return this.status;
    }

    public void setStatus(@Status int i) {
        this.status = i;
    }
}
