package com.vungle.warren.model;

import android.content.ContentValues;
import android.support.annotation.NonNull;
import com.vungle.warren.persistence.ContentValuesUtil;
import com.vungle.warren.persistence.DBAdapter;
import com.vungle.warren.persistence.IdColumns;

public class PlacementDBAdapter implements DBAdapter<Placement> {
    public static final String CREATE_PLACEMENT_TABLE_QUERY = "CREATE TABLE IF NOT EXISTS placement(_id INTEGER PRIMARY KEY AUTOINCREMENT, item_id TEXT UNIQUE, incentivized SHORT, auto_cached SHORT, is_valid SHORT, wakeup_time NUMERIC )";

    public interface PlacementColumns extends IdColumns {
        public static final String COLUMN_AUTOCACHED = "auto_cached";
        public static final String COLUMN_INCENTIVIZED = "incentivized";
        public static final String COLUMN_IS_VALID = "is_valid";
        public static final String COLUMN_WAKEUP_TIME = "wakeup_time";
        public static final String TABLE_NAME = "placement";
    }

    public String tableName() {
        return "placement";
    }

    public ContentValues toContentValues(Placement placement) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("item_id", placement.identifier);
        contentValues.put("incentivized", Boolean.valueOf(placement.incentivized));
        contentValues.put(PlacementColumns.COLUMN_AUTOCACHED, Boolean.valueOf(placement.autoCached));
        contentValues.put(PlacementColumns.COLUMN_WAKEUP_TIME, Long.valueOf(placement.wakeupTime));
        contentValues.put(PlacementColumns.COLUMN_IS_VALID, Boolean.valueOf(placement.isValid));
        return contentValues;
    }

    @NonNull
    public Placement fromContentValues(ContentValues contentValues) {
        Placement placement = new Placement();
        placement.identifier = contentValues.getAsString("item_id");
        placement.wakeupTime = contentValues.getAsLong(PlacementColumns.COLUMN_WAKEUP_TIME).longValue();
        placement.incentivized = ContentValuesUtil.getBoolean(contentValues, "incentivized");
        placement.autoCached = ContentValuesUtil.getBoolean(contentValues, PlacementColumns.COLUMN_AUTOCACHED);
        placement.isValid = ContentValuesUtil.getBoolean(contentValues, PlacementColumns.COLUMN_IS_VALID);
        return placement;
    }
}
