package com.vungle.warren.model;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.UUID;

public class AdAsset {
    public final String adIdentifier;
    public long fileSize;
    public int fileType;
    public final String identifier;
    public final String localPath;
    public String parentId;
    public int retryCount;
    int retryTypeError;
    public final String serverPath;
    public int status;

    @Retention(RetentionPolicy.SOURCE)
    public @interface FileType {
        public static final int ASSET = 2;
        public static final int ZIP = 0;
        public static final int ZIP_ASSET = 1;
    }

    @Retention(RetentionPolicy.SOURCE)
    public @interface RetryTypeError {
        public static final int CANNOT_RETRY_ERROR = 1;
        public static final int CAN_RETRY_ERROR = 0;
    }

    @Retention(RetentionPolicy.SOURCE)
    public @interface Status {
        public static final int DOWNLOAD_FAILED = 2;
        public static final int DOWNLOAD_RUNNING = 1;
        public static final int DOWNLOAD_SUCCESS = 3;
        public static final int NEW = 0;
        public static final int PROCESSED = 4;
    }

    public AdAsset(@NonNull String str, @Nullable String str2, @NonNull String str3) {
        this(str, str2, str3, UUID.randomUUID().toString());
    }

    AdAsset(@NonNull String str, @Nullable String str2, @NonNull String str3, String str4) {
        this.identifier = str4;
        this.adIdentifier = str;
        this.serverPath = str2;
        this.localPath = str3;
        this.fileSize = -1;
        this.retryCount = 0;
        this.retryTypeError = 1;
    }

    public boolean equals(Object obj) {
        boolean z = true;
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        AdAsset adAsset = (AdAsset) obj;
        if (this.status != adAsset.status || this.fileType != adAsset.fileType || this.fileSize != adAsset.fileSize || this.retryCount != adAsset.retryCount || this.retryTypeError != adAsset.retryTypeError) {
            return false;
        }
        if (this.identifier == null ? adAsset.identifier != null : !this.identifier.equals(adAsset.identifier)) {
            return false;
        }
        if (this.adIdentifier == null ? adAsset.adIdentifier != null : !this.adIdentifier.equals(adAsset.adIdentifier)) {
            return false;
        }
        if (this.parentId == null ? adAsset.parentId != null : !this.parentId.equals(adAsset.parentId)) {
            return false;
        }
        if (this.serverPath == null ? adAsset.serverPath != null : !this.serverPath.equals(adAsset.serverPath)) {
            return false;
        }
        if (this.localPath != null) {
            z = this.localPath.equals(adAsset.localPath);
        } else if (adAsset.localPath != null) {
            z = false;
        }
        return z;
    }

    public int hashCode() {
        int i = 0;
        int hashCode = (((((((this.identifier != null ? this.identifier.hashCode() : 0) * 31) + (this.adIdentifier != null ? this.adIdentifier.hashCode() : 0)) * 31) + (this.parentId != null ? this.parentId.hashCode() : 0)) * 31) + (this.serverPath != null ? this.serverPath.hashCode() : 0)) * 31;
        if (this.localPath != null) {
            i = this.localPath.hashCode();
        }
        return ((((((((((hashCode + i) * 31) + this.status) * 31) + this.fileType) * 31) + ((int) (this.fileSize ^ (this.fileSize >>> 32)))) * 31) + this.retryCount) * 31) + this.retryTypeError;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("AdAsset{identifier='");
        sb.append(this.identifier);
        sb.append('\'');
        sb.append(", adIdentifier='");
        sb.append(this.adIdentifier);
        sb.append('\'');
        sb.append(", serverPath='");
        sb.append(this.serverPath);
        sb.append('\'');
        sb.append(", localPath='");
        sb.append(this.localPath);
        sb.append('\'');
        sb.append(", status=");
        sb.append(this.status);
        sb.append(", fileType=");
        sb.append(this.fileType);
        sb.append(", fileSize=");
        sb.append(this.fileSize);
        sb.append(", retryCount=");
        sb.append(this.retryCount);
        sb.append(", retryTypeError=");
        sb.append(this.retryTypeError);
        sb.append('}');
        return sb.toString();
    }
}
