package com.vungle.warren;

import com.vungle.warren.DownloadStrategy.VerificationCallback;
import com.vungle.warren.SDKDownloadClient.ValidationCheck;

public class DirectDownloadStrategy implements DownloadStrategy {
    private PublisherDirectDownload directDownload;

    public DirectDownloadStrategy(PublisherDirectDownload publisherDirectDownload) {
        this.directDownload = publisherDirectDownload;
    }

    public void isApplicationAvailable(String str, final VerificationCallback verificationCallback) {
        final DirectDownloadAdapter directDownloadAdapter = new DirectDownloadAdapter(this.directDownload, str);
        directDownloadAdapter.getSdkDownloadClient().setAppMarketValidation(new ValidationCheck() {
            public void validateAppPresenceInMarket(boolean z) {
                verificationCallback.onResult(z);
                directDownloadAdapter.getSdkDownloadClient().setAppMarketValidation(null);
            }
        });
        directDownloadAdapter.getSdkDownloadClient().sendValidation(str);
    }
}
