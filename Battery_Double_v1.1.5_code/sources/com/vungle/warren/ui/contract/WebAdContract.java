package com.vungle.warren.ui.contract;

import com.vungle.warren.ui.contract.AdContract.AdView;
import com.vungle.warren.ui.contract.AdContract.AdvertisementPresenter;

public interface WebAdContract {

    public interface WebAdPresenter extends AdvertisementPresenter<WebAdView> {
        void setAdVisibility(boolean z);
    }

    public interface WebAdView extends AdView<WebAdPresenter> {
        void setVisibility(boolean z);

        void updateWindow(boolean z);
    }
}
