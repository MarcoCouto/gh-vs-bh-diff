package com.vungle.warren.ui.contract;

import android.content.DialogInterface.OnClickListener;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.vungle.warren.ui.JavascriptBridge.MraidHandler;
import com.vungle.warren.ui.state.OptionsState;

public interface AdContract {

    public interface AdView<T extends AdvertisementPresenter> {
        void close();

        void destroyAdView();

        String getWebsiteUrl();

        void open(@NonNull String str);

        void pauseWeb();

        void resumeWeb();

        void setImmersiveMode();

        void setOrientation(int i);

        void setPresenter(@NonNull T t);

        void showCloseButton();

        void showDialog(@Nullable String str, @Nullable String str2, @NonNull String str3, @NonNull String str4, @Nullable OnClickListener onClickListener);

        void showWebsite(@NonNull String str);
    }

    public interface AdvertisementBus {
        public static final String ACTION = "AdvertisementBus";
        public static final String CLOSE_FLEX = "closeFlex";
        public static final String COMMAND = "command";
        public static final String PLACEMENT = "placement";
        public static final String STOP_ALL = "stopAll";
    }

    public interface AdvertisementPresenter<T extends AdView> extends MraidHandler {

        public interface EventListener {
            void onError(@NonNull Throwable th, @Nullable String str);

            void onNext(@NonNull String str, @Nullable String str2, @Nullable String str3);
        }

        void attach(@NonNull T t, @Nullable OptionsState optionsState);

        void detach(boolean z);

        void generateSaveState(@Nullable OptionsState optionsState);

        boolean handleExit(@Nullable String str);

        void onViewConfigurationChanged();

        void restoreFromSave(@Nullable OptionsState optionsState);

        void setEventListener(@Nullable EventListener eventListener);

        void start();

        void stop(boolean z, boolean z2);
    }
}
