package com.vungle.warren.ui.presenter;

import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.vungle.warren.analytics.AdAnalytics;
import com.vungle.warren.analytics.AnalyticsEvent.Ad;
import com.vungle.warren.analytics.AnalyticsVideoTracker;
import com.vungle.warren.download.APKDirectDownloadManager;
import com.vungle.warren.error.VungleException;
import com.vungle.warren.model.Advertisement;
import com.vungle.warren.model.Advertisement.Checkpoint;
import com.vungle.warren.model.Cookie;
import com.vungle.warren.model.Placement;
import com.vungle.warren.model.Report;
import com.vungle.warren.model.ReportDBAdapter.ReportColumns;
import com.vungle.warren.persistence.Repository;
import com.vungle.warren.persistence.Repository.SaveCallback;
import com.vungle.warren.ui.JavascriptBridge.MraidHandler;
import com.vungle.warren.ui.contract.AdContract.AdvertisementPresenter.EventListener;
import com.vungle.warren.ui.contract.LocalAdContract.LocalPresenter;
import com.vungle.warren.ui.contract.LocalAdContract.LocalView;
import com.vungle.warren.ui.state.OptionsState;
import com.vungle.warren.ui.view.WebViewAPI;
import com.vungle.warren.ui.view.WebViewAPI.WebClientErrorListener;
import com.vungle.warren.utility.AsyncFileUtils;
import com.vungle.warren.utility.AsyncFileUtils.FileExist;
import com.vungle.warren.utility.Scheduler;
import java.io.File;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Locale;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.atomic.AtomicBoolean;

public class LocalAdPresenter implements LocalPresenter, WebClientErrorListener {
    static final String EXTRA_INCENTIVIZED_SENT = "incentivized_sent";
    static final String EXTRA_IN_POST = "in_post_roll";
    static final String EXTRA_REPORT = "saved_report";
    static final String EXTRA_VIDEO_POSITION = "videoPosition";
    static final String HTTPS_VUNGLE_COM_PRIVACY = "https://vungle.com/privacy/";
    public static final int INCENTIVIZED_TRESHOLD = 75;
    static final String TAG = "LocalAdPresenter";
    private long adStartTime;
    /* access modifiers changed from: private */
    public LocalView adView;
    /* access modifiers changed from: private */
    public Advertisement advertisement;
    /* access modifiers changed from: private */
    public final AdAnalytics analytics;
    private File assetDir;
    /* access modifiers changed from: private */
    public EventListener bus;
    private AtomicBoolean busy = new AtomicBoolean(false);
    private LinkedList<Checkpoint> checkpointList = new LinkedList<>();
    private final HashMap<String, Cookie> cookies = new HashMap<>();
    private String dialogBody = "If you exit now, you will not get your reward";
    private String dialogClose = "Close";
    private String dialogContinue = "Continue";
    private String dialogTitle = "Are you sure?";
    private boolean directDownloadApkEnabled;
    private int duration;
    /* access modifiers changed from: private */
    public boolean inPost;
    private final ExecutorService ioExecutor;
    private AtomicBoolean isDestroying = new AtomicBoolean(false);
    private boolean muted;
    /* access modifiers changed from: private */
    public Placement placement;
    private int progress;
    private SaveCallback repoCallback = new SaveCallback() {
        boolean errorHappened = false;

        public void onSaved() {
        }

        public void onError(Exception exc) {
            if (!this.errorHappened) {
                this.errorHappened = true;
                if (LocalAdPresenter.this.bus != null) {
                    LocalAdPresenter.this.bus.onError(new VungleException(26), LocalAdPresenter.this.placement.getId());
                }
                LocalAdPresenter.this.closeAndReport();
            }
        }
    };
    private Report report;
    /* access modifiers changed from: private */
    public Repository repository;
    private final Scheduler scheduler;
    private AtomicBoolean sendReportIncentivized = new AtomicBoolean(false);
    private final ExecutorService uiExecutor;
    /* access modifiers changed from: private */
    public boolean userExitEnabled;
    private int videoPosition;
    /* access modifiers changed from: private */
    public final AnalyticsVideoTracker videoTracker;
    private final WebViewAPI webViewAPI;

    public LocalAdPresenter(@NonNull Advertisement advertisement2, @NonNull Placement placement2, @NonNull Repository repository2, @NonNull Scheduler scheduler2, @NonNull AdAnalytics adAnalytics, @NonNull AnalyticsVideoTracker analyticsVideoTracker, @NonNull WebViewAPI webViewAPI2, @Nullable OptionsState optionsState, @NonNull File file, @NonNull ExecutorService executorService, @NonNull ExecutorService executorService2) {
        this.advertisement = advertisement2;
        this.placement = placement2;
        this.scheduler = scheduler2;
        this.analytics = adAnalytics;
        this.videoTracker = analyticsVideoTracker;
        this.webViewAPI = webViewAPI2;
        this.repository = repository2;
        this.assetDir = file;
        this.ioExecutor = executorService;
        this.uiExecutor = executorService2;
        if (advertisement2.getCheckpoints() != null) {
            this.checkpointList.addAll(advertisement2.getCheckpoints());
            Collections.sort(this.checkpointList);
        }
        loadData(optionsState);
    }

    public void setEventListener(@Nullable EventListener eventListener) {
        this.bus = eventListener;
    }

    public void reportError(@NonNull String str) {
        this.report.recordError(str);
        this.repository.save(this.report, this.repoCallback);
        if (this.inPost || !this.advertisement.hasPostroll()) {
            if (this.bus != null) {
                this.bus.onError(new Throwable(str), this.placement.getId());
            }
            this.adView.close();
            return;
        }
        playPost();
    }

    public void reportAction(@NonNull String str, @Nullable String str2) {
        if (str.equals("videoLength")) {
            this.duration = Integer.parseInt(str2);
            this.report.setVideoLength((long) this.duration);
            this.repository.save(this.report, this.repoCallback);
            return;
        }
        char c = 65535;
        int hashCode = str.hashCode();
        if (hashCode != -840405966) {
            if (hashCode != 3363353) {
                if (hashCode == 1370606900 && str.equals(Ad.videoClose)) {
                    c = 2;
                }
            } else if (str.equals("mute")) {
                c = 0;
            }
        } else if (str.equals("unmute")) {
            c = 1;
        }
        switch (c) {
            case 0:
            case 1:
            case 2:
                this.analytics.ping(this.advertisement.getTpatUrls(str));
                break;
        }
        this.report.recordAction(str, str2, System.currentTimeMillis());
        this.repository.save(this.report, this.repoCallback);
    }

    public void onViewConfigurationChanged() {
        this.webViewAPI.notifyPropertiesChange(true);
    }

    public void attach(@NonNull LocalView localView, @Nullable OptionsState optionsState) {
        boolean z = false;
        this.isDestroying.set(false);
        this.adView = localView;
        localView.setPresenter(this);
        int settings = this.advertisement.getAdConfig().getSettings();
        if (settings > 0) {
            this.muted = (settings & 1) == 1;
            this.userExitEnabled = (settings & 2) == 2;
            if ((settings & 32) == 32) {
                z = true;
            }
            this.directDownloadApkEnabled = z;
        }
        int i = 4;
        if ((this.advertisement.getAdConfig().getSettings() & 16) != 16) {
            switch (this.advertisement.getOrientation()) {
                case 0:
                    i = 7;
                    break;
                case 1:
                    i = 6;
                    break;
                case 2:
                    break;
                default:
                    i = -1;
                    break;
            }
        }
        String str = TAG;
        StringBuilder sb = new StringBuilder();
        sb.append("requested orientation ");
        sb.append(i);
        Log.d(str, sb.toString());
        localView.setOrientation(i);
        prepare(optionsState);
    }

    public void detach(boolean z) {
        stop(z, true);
        this.adView.destroyAdView();
    }

    private void playPost() {
        File file = new File(this.assetDir.getPath());
        StringBuilder sb = new StringBuilder();
        sb.append(file.getPath());
        sb.append(File.separator);
        sb.append("index.html");
        final File file2 = new File(sb.toString());
        new AsyncFileUtils(this.ioExecutor, this.uiExecutor).isFileExistAsync(file2, new FileExist() {
            public void status(boolean z) {
                if (z) {
                    if (LocalAdPresenter.this.videoTracker != null) {
                        LocalAdPresenter.this.videoTracker.stop();
                    }
                    LocalView access$400 = LocalAdPresenter.this.adView;
                    StringBuilder sb = new StringBuilder();
                    sb.append(Advertisement.FILE_SCHEME);
                    sb.append(file2.getPath());
                    access$400.showWebsite(sb.toString());
                    LocalAdPresenter.this.analytics.ping(LocalAdPresenter.this.advertisement.getTpatUrls(Ad.postrollView));
                    LocalAdPresenter.this.inPost = true;
                    return;
                }
                if (LocalAdPresenter.this.bus != null) {
                    LocalAdPresenter.this.bus.onError(new VungleException(10), LocalAdPresenter.this.placement.getId());
                }
                LocalAdPresenter.this.closeAndReport();
            }
        });
    }

    private void prepare(@Nullable OptionsState optionsState) {
        String str;
        restoreFromSave(optionsState);
        Cookie cookie = (Cookie) this.cookies.get(Cookie.INCENTIVIZED_TEXT_COOKIE);
        if (cookie == null) {
            str = null;
        } else {
            str = cookie.getString("userID");
        }
        if (this.report == null) {
            this.adStartTime = System.currentTimeMillis();
            Report report2 = new Report(this.advertisement, this.placement, this.adStartTime, str);
            this.report = report2;
            this.repository.save(this.report, this.repoCallback);
        }
        this.webViewAPI.setErrorListener(this);
        this.adView.showCTAOverlay(this.advertisement.isCtaOverlayEnabled(), this.advertisement.getCtaClickArea());
        if (this.bus != null) {
            this.bus.onNext("start", null, this.placement.getId());
        }
    }

    private boolean needShowGDPR(@Nullable Cookie cookie) {
        return cookie != null && cookie.getBoolean("is_country_data_protected").booleanValue() && "unknown".equals(cookie.getString("consent_status"));
    }

    private void showGDPR(@NonNull final Cookie cookie) {
        AnonymousClass3 r5 = new OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                String str = "opted_out_by_timeout";
                switch (i) {
                    case -2:
                        str = "opted_out";
                        break;
                    case -1:
                        str = "opted_in";
                        break;
                }
                cookie.putValue("consent_status", str);
                cookie.putValue("timestamp", Long.valueOf(System.currentTimeMillis() / 1000));
                cookie.putValue("consent_source", "vungle_modal");
                LocalAdPresenter.this.repository.save(cookie, null);
                LocalAdPresenter.this.start();
            }
        };
        cookie.putValue("consent_status", "opted_out_by_timeout");
        cookie.putValue("timestamp", Long.valueOf(System.currentTimeMillis() / 1000));
        cookie.putValue("consent_source", "vungle_modal");
        this.repository.save(cookie, this.repoCallback);
        showDialog(cookie.getString("consent_title"), cookie.getString("consent_message"), cookie.getString("button_accept"), cookie.getString("button_deny"), r5);
    }

    public boolean handleExit(@Nullable String str) {
        if (this.inPost) {
            closeAndReport();
            return true;
        } else if (!this.userExitEnabled) {
            return false;
        } else {
            if (!this.placement.isIncentivized() || this.progress > 75) {
                reportAction(Ad.videoClose, null);
                if (this.advertisement.hasPostroll()) {
                    playPost();
                    return false;
                }
                closeAndReport();
                return true;
            }
            showIncetivizedDialog();
            return false;
        }
    }

    private void showIncetivizedDialog() {
        Cookie cookie = (Cookie) this.cookies.get(Cookie.INCENTIVIZED_TEXT_COOKIE);
        String str = this.dialogTitle;
        String str2 = this.dialogBody;
        String str3 = this.dialogContinue;
        String str4 = this.dialogClose;
        if (cookie != null) {
            str = cookie.getString("title") == null ? this.dialogTitle : cookie.getString("title");
            str2 = cookie.getString(TtmlNode.TAG_BODY) == null ? this.dialogBody : cookie.getString(TtmlNode.TAG_BODY);
            str3 = cookie.getString("continue") == null ? this.dialogContinue : cookie.getString("continue");
            str4 = cookie.getString("close") == null ? this.dialogClose : cookie.getString("close");
        }
        showDialog(str, str2, str3, str4, new OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                if (i == -2) {
                    LocalAdPresenter.this.reportAction(Ad.videoClose, null);
                    LocalAdPresenter.this.continueWithPostroll();
                }
            }
        });
    }

    private void showDialog(String str, String str2, String str3, String str4, OnClickListener onClickListener) {
        this.adView.pauseVideo();
        this.adView.showDialog(str, str2, str3, str4, onClickListener);
    }

    /* access modifiers changed from: private */
    public void continueWithPostroll() {
        if (this.advertisement.hasPostroll()) {
            playPost();
        } else {
            closeAndReport();
        }
    }

    private boolean isWebPageBlank() {
        String websiteUrl = this.adView.getWebsiteUrl();
        return TextUtils.isEmpty(websiteUrl) || "about:blank".equalsIgnoreCase(websiteUrl);
    }

    public void start() {
        this.adView.setImmersiveMode();
        this.adView.resumeWeb();
        Cookie cookie = (Cookie) this.cookies.get(Cookie.CONSENT_COOKIE);
        if (needShowGDPR(cookie)) {
            showGDPR(cookie);
            return;
        }
        if (this.inPost) {
            if (isWebPageBlank()) {
                playPost();
            }
        } else if (!this.adView.isVideoPlaying() && !this.adView.isDialogVisible()) {
            StringBuilder sb = new StringBuilder();
            sb.append(this.assetDir.getPath());
            sb.append(File.separator);
            sb.append("video");
            this.adView.playVideo(new File(sb.toString()), this.muted, this.videoPosition);
            int showCloseDelay = this.advertisement.getShowCloseDelay(this.placement.isIncentivized());
            if (showCloseDelay > 0) {
                this.scheduler.schedule(new Runnable() {
                    public void run() {
                        LocalAdPresenter.this.userExitEnabled = true;
                        if (!LocalAdPresenter.this.inPost) {
                            LocalAdPresenter.this.adView.showCloseButton();
                        }
                    }
                }, (long) showCloseDelay);
            } else {
                this.userExitEnabled = true;
                this.adView.showCloseButton();
            }
        }
    }

    public void stop(boolean z, boolean z2) {
        this.adView.pauseWeb();
        if (this.adView.isVideoPlaying()) {
            this.videoPosition = this.adView.getVideoPosition();
            this.adView.pauseVideo();
        }
        if (z || !z2) {
            if (this.inPost || z2) {
                this.adView.showWebsite("about:blank");
            }
        } else if (!this.isDestroying.getAndSet(true)) {
            String str = null;
            reportAction("close", null);
            this.scheduler.cancelAll();
            if (this.bus != null) {
                EventListener eventListener = this.bus;
                String str2 = TtmlNode.END;
                if (this.report.isCTAClicked()) {
                    str = "isCTAClicked";
                }
                eventListener.onNext(str2, str, this.placement.getId());
            }
        }
    }

    public void onProgressUpdate(int i, float f) {
        this.progress = (int) ((((float) i) / f) * 100.0f);
        this.videoPosition = i;
        if (this.bus != null) {
            EventListener eventListener = this.bus;
            StringBuilder sb = new StringBuilder();
            sb.append("percentViewed:");
            sb.append(this.progress);
            eventListener.onNext(sb.toString(), null, this.placement.getId());
        }
        reportAction("video_viewed", String.format(Locale.ENGLISH, "%d", new Object[]{Integer.valueOf(i)}));
        this.videoTracker.onProgress(this.progress);
        if (this.progress == 100) {
            this.videoTracker.stop();
            if (this.checkpointList.peekLast() != null && ((Checkpoint) this.checkpointList.peekLast()).getPercentage() == 100) {
                this.analytics.ping(((Checkpoint) this.checkpointList.pollLast()).getUrls());
            }
            continueWithPostroll();
        }
        this.report.recordProgress(this.videoPosition);
        this.repository.save(this.report, this.repoCallback);
        while (this.checkpointList.peek() != null && this.progress > ((Checkpoint) this.checkpointList.peek()).getPercentage()) {
            this.analytics.ping(((Checkpoint) this.checkpointList.poll()).getUrls());
        }
        Cookie cookie = (Cookie) this.cookies.get(Cookie.CONFIG_COOKIE);
        if (this.placement.isIncentivized() && this.progress > 75 && cookie != null && cookie.getBoolean("isReportIncentivizedEnabled").booleanValue() && !this.sendReportIncentivized.getAndSet(true)) {
            JsonObject jsonObject = new JsonObject();
            jsonObject.add("placement_reference_id", new JsonPrimitive(this.placement.getId()));
            jsonObject.add("app_id", new JsonPrimitive(this.advertisement.getAppID()));
            jsonObject.add(ReportColumns.COLUMN_AD_START_TIME, new JsonPrimitive((Number) Long.valueOf(this.report.getAdStartTime())));
            jsonObject.add("user", new JsonPrimitive(this.report.getUserID()));
            this.analytics.ri(jsonObject);
        }
    }

    public void onVideoStart(int i, float f) {
        reportAction("videoLength", String.format(Locale.ENGLISH, "%d", new Object[]{Integer.valueOf((int) f)}));
    }

    public void onMute(boolean z) {
        if (z) {
            reportAction("mute", "true");
        } else {
            reportAction("unmute", "false");
        }
    }

    public void onDownload() {
        download();
    }

    public boolean onMediaError(@NonNull String str) {
        reportError(str);
        return false;
    }

    public void onPrivacy() {
        this.adView.open(HTTPS_VUNGLE_COM_PRIVACY);
    }

    public void generateSaveState(@Nullable OptionsState optionsState) {
        if (optionsState != null) {
            this.repository.save(this.report, this.repoCallback);
            optionsState.put(EXTRA_REPORT, this.report == null ? null : this.report.getId());
            optionsState.put(EXTRA_INCENTIVIZED_SENT, this.sendReportIncentivized.get());
            optionsState.put(EXTRA_IN_POST, this.inPost);
            optionsState.put(EXTRA_VIDEO_POSITION, this.adView == null ? this.videoPosition : this.adView.getVideoPosition());
        }
    }

    public void restoreFromSave(@Nullable OptionsState optionsState) {
        if (optionsState != null) {
            if (optionsState.getBoolean(EXTRA_INCENTIVIZED_SENT, false)) {
                this.sendReportIncentivized.set(true);
            }
            this.inPost = optionsState.getBoolean(EXTRA_IN_POST, this.inPost);
            this.videoPosition = optionsState.getInt(EXTRA_VIDEO_POSITION, this.videoPosition).intValue();
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x0036  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x004d  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0054  */
    /* JADX WARNING: Removed duplicated region for block: B:22:? A[RETURN, SYNTHETIC] */
    public void onMraidAction(@NonNull String str) {
        char c;
        int hashCode = str.hashCode();
        if (hashCode == -314498168) {
            if (str.equals("privacy")) {
                c = 2;
                switch (c) {
                    case 0:
                        break;
                    case 1:
                        break;
                    case 2:
                        break;
                }
            }
        } else if (hashCode == 94756344) {
            if (str.equals("close")) {
                c = 0;
                switch (c) {
                    case 0:
                        break;
                    case 1:
                        break;
                    case 2:
                        break;
                }
            }
        } else if (hashCode == 1427818632 && str.equals(MraidHandler.DOWNLOAD_ACTION)) {
            c = 1;
            switch (c) {
                case 0:
                    closeAndReport();
                    return;
                case 1:
                    download();
                    closeAndReport();
                    return;
                case 2:
                    return;
                default:
                    StringBuilder sb = new StringBuilder();
                    sb.append("Unknown action ");
                    sb.append(str);
                    throw new IllegalArgumentException(sb.toString());
            }
        }
        c = 65535;
        switch (c) {
            case 0:
                break;
            case 1:
                break;
            case 2:
                break;
        }
    }

    private void download() {
        reportAction("cta", "");
        try {
            this.analytics.ping(this.advertisement.getTpatUrls(Ad.postrollClick));
            this.analytics.ping(this.advertisement.getTpatUrls("click_url"));
            this.analytics.ping(this.advertisement.getTpatUrls("video_click"));
            this.analytics.ping(new String[]{this.advertisement.getCTAURL(true)});
            reportAction(MraidHandler.DOWNLOAD_ACTION, null);
            String ctaurl = this.advertisement.getCTAURL(false);
            if (APKDirectDownloadManager.isDirectDownloadEnabled(this.directDownloadApkEnabled, this.advertisement.isRequiresNonMarketInstall())) {
                APKDirectDownloadManager.download(ctaurl);
            } else {
                this.adView.open(ctaurl);
            }
        } catch (ActivityNotFoundException unused) {
            Log.e(TAG, "Unable to find destination activity");
        }
    }

    /* access modifiers changed from: private */
    public void closeAndReport() {
        if (this.adView.isVideoPlaying()) {
            this.videoTracker.stop();
        }
        if (this.busy.get()) {
            Log.w(TAG, "Busy with closing");
            return;
        }
        this.busy.set(true);
        reportAction("close", null);
        this.scheduler.cancelAll();
        this.report.setAdDuration((int) (System.currentTimeMillis() - this.adStartTime));
        this.adView.close();
    }

    private void loadData(OptionsState optionsState) {
        this.cookies.put(Cookie.CONSENT_COOKIE, this.repository.load(Cookie.CONSENT_COOKIE, Cookie.class).get());
        this.cookies.put(Cookie.INCENTIVIZED_TEXT_COOKIE, this.repository.load(Cookie.INCENTIVIZED_TEXT_COOKIE, Cookie.class).get());
        this.cookies.put(Cookie.CONSENT_COOKIE, this.repository.load(Cookie.CONSENT_COOKIE, Cookie.class).get());
        this.cookies.put(Cookie.CONFIG_COOKIE, this.repository.load(Cookie.CONFIG_COOKIE, Cookie.class).get());
        if (optionsState != null) {
            String string = optionsState.getString(EXTRA_REPORT);
            Report report2 = TextUtils.isEmpty(string) ? null : (Report) this.repository.load(string, Report.class).get();
            if (report2 != null) {
                this.report = report2;
            }
        }
    }

    public void onReceivedError(String str) {
        if (this.report != null) {
            this.report.recordError(str);
            this.repository.save(this.report, this.repoCallback);
        }
    }
}
