package com.vungle.warren.ui.presenter;

import android.content.ActivityNotFoundException;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.vungle.warren.DirectDownloadAdapter;
import com.vungle.warren.DirectDownloadAdapter.CONTRACT_TYPE;
import com.vungle.warren.analytics.AdAnalytics;
import com.vungle.warren.download.APKDirectDownloadManager;
import com.vungle.warren.error.VungleException;
import com.vungle.warren.model.Advertisement;
import com.vungle.warren.model.Cookie;
import com.vungle.warren.model.Placement;
import com.vungle.warren.model.Report;
import com.vungle.warren.model.ReportDBAdapter.ReportColumns;
import com.vungle.warren.persistence.Repository;
import com.vungle.warren.persistence.Repository.SaveCallback;
import com.vungle.warren.ui.JavascriptBridge.MraidHandler;
import com.vungle.warren.ui.contract.AdContract.AdvertisementPresenter.EventListener;
import com.vungle.warren.ui.contract.WebAdContract.WebAdPresenter;
import com.vungle.warren.ui.contract.WebAdContract.WebAdView;
import com.vungle.warren.ui.state.OptionsState;
import com.vungle.warren.ui.view.WebViewAPI;
import com.vungle.warren.ui.view.WebViewAPI.MRAIDDelegate;
import com.vungle.warren.ui.view.WebViewAPI.WebClientErrorListener;
import com.vungle.warren.utility.AsyncFileUtils;
import com.vungle.warren.utility.AsyncFileUtils.FileExist;
import com.vungle.warren.utility.Scheduler;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.atomic.AtomicBoolean;

public class MRAIDAdPresenter implements WebAdPresenter, MRAIDDelegate, WebClientErrorListener {
    private static final String ACTION = "action";
    private static final String ACTION_WITH_VALUE = "actionWithValue";
    private static final String CANCEL_DOWNLOAD = "cancelDownload";
    private static final String CLOSE = "close";
    private static final String CONSENT_ACTION = "consentAction";
    private static final String EXTRA_INCENTIVIZED_SENT = "incentivized_sent";
    private static String EXTRA_REPORT = "saved_report";
    private static final String EXTRA_WEB_READY = "web_ready";
    protected static final double NINE_BY_SIXTEEN_ASPECT_RATIO = 0.5625d;
    private static final String OPEN = "open";
    private static final String OPEN_APP_IN_DEVICE = "openAppInDevice";
    private static final String OPEN_PRIVACY = "openPrivacy";
    private static final String START_DOWNLOAD_APP_ON_DEVICE = "startDownloadAppOnDevice";
    private static final String SUCCESSFUL_VIEW = "successfulView";
    private static final String TAG = MRAIDAdPresenter.class.getCanonicalName();
    private static final String TPAT = "tpat";
    private static final String USE_CUSTOM_CLOSE = "useCustomClose";
    private static final String USE_CUSTOM_PRIVACY = "useCustomPrivacy";
    private static final String VIDEO_VIEWED = "videoViewed";
    private long adStartTime;
    /* access modifiers changed from: private */
    public WebAdView adView;
    private Advertisement advertisement;
    private final AdAnalytics analytics;
    private File assetDir;
    /* access modifiers changed from: private */
    public boolean backEnabled;
    /* access modifiers changed from: private */
    public EventListener bus;
    private Map<String, Cookie> cookieMap = new HashMap();
    private DirectDownloadAdapter directDownloadAdapter;
    private boolean directDownloadApkEnabled;
    private long duration;
    private boolean hasSend80Percent = false;
    private boolean hasSendStart = false;
    private final ExecutorService ioExecutor;
    private AtomicBoolean isDestroying = new AtomicBoolean(false);
    /* access modifiers changed from: private */
    public Placement placement;
    /* access modifiers changed from: private */
    public SaveCallback repoCallback = new SaveCallback() {
        boolean errorHappened = false;

        public void onSaved() {
        }

        public void onError(Exception exc) {
            if (!this.errorHappened) {
                this.errorHappened = true;
                if (MRAIDAdPresenter.this.bus != null) {
                    MRAIDAdPresenter.this.bus.onError(new VungleException(26), MRAIDAdPresenter.this.placement.getId());
                }
                MRAIDAdPresenter.this.closeView();
            }
        }
    };
    /* access modifiers changed from: private */
    public Report report;
    /* access modifiers changed from: private */
    public Repository repository;
    private final Scheduler scheduler;
    private AtomicBoolean sendReportIncentivized = new AtomicBoolean(false);
    private final ExecutorService uiExecutor;
    private WebViewAPI webClient;

    public MRAIDAdPresenter(@NonNull Advertisement advertisement2, @NonNull Placement placement2, @NonNull Repository repository2, @NonNull Scheduler scheduler2, @NonNull AdAnalytics adAnalytics, @NonNull WebViewAPI webViewAPI, @Nullable DirectDownloadAdapter directDownloadAdapter2, @Nullable OptionsState optionsState, @NonNull File file, @NonNull ExecutorService executorService, @NonNull ExecutorService executorService2) {
        this.advertisement = advertisement2;
        this.repository = repository2;
        this.placement = placement2;
        this.scheduler = scheduler2;
        this.analytics = adAnalytics;
        this.webClient = webViewAPI;
        this.directDownloadAdapter = directDownloadAdapter2;
        this.assetDir = file;
        this.ioExecutor = executorService;
        this.uiExecutor = executorService2;
        load(optionsState);
    }

    public void setEventListener(@Nullable EventListener eventListener) {
        this.bus = eventListener;
    }

    public void reportAction(@NonNull String str, @Nullable String str2) {
        if (str.equals("videoLength")) {
            this.duration = Long.parseLong(str2);
            this.report.setVideoLength(this.duration);
            this.repository.save(this.report, this.repoCallback);
            return;
        }
        this.report.recordAction(str, str2, System.currentTimeMillis());
        this.repository.save(this.report, this.repoCallback);
    }

    public void onViewConfigurationChanged() {
        this.adView.updateWindow(this.advertisement.getTemplateType().equals("flexview"));
        this.webClient.notifyPropertiesChange(true);
    }

    public void attach(@NonNull WebAdView webAdView, @Nullable OptionsState optionsState) {
        boolean z = false;
        this.isDestroying.set(false);
        this.adView = webAdView;
        webAdView.setPresenter(this);
        int settings = this.advertisement.getAdConfig().getSettings();
        if (settings > 0) {
            this.backEnabled = (settings & 2) == 2;
            if ((settings & 32) == 32) {
                z = true;
            }
            this.directDownloadApkEnabled = z;
        }
        int i = 4;
        if ((this.advertisement.getAdConfig().getSettings() & 16) != 16) {
            switch (this.advertisement.getOrientation()) {
                case 0:
                    i = 7;
                    break;
                case 1:
                    i = 6;
                    break;
                case 2:
                    break;
                default:
                    i = -1;
                    break;
            }
        }
        webAdView.setOrientation(i);
        prepare(optionsState);
    }

    public void detach(boolean z) {
        stop(z, true);
        this.adView.destroyAdView();
    }

    private void prepare(@Nullable OptionsState optionsState) {
        this.webClient.setMRAIDDelegate(this);
        this.webClient.setDownloadAdapter(this.directDownloadAdapter);
        this.webClient.setErrorListener(this);
        StringBuilder sb = new StringBuilder();
        sb.append(this.assetDir.getPath());
        sb.append(File.separator);
        sb.append("template");
        loadMraid(new File(sb.toString()));
        Cookie cookie = (Cookie) this.cookieMap.get(Cookie.INCENTIVIZED_TEXT_COOKIE);
        if ("flexview".equals(this.advertisement.getTemplateType()) && this.advertisement.getAdConfig().getFlexViewCloseTime() > 0) {
            this.scheduler.schedule(new Runnable() {
                public void run() {
                    long currentTimeMillis = System.currentTimeMillis();
                    MRAIDAdPresenter.this.report.recordAction("mraidCloseByTimer", "", currentTimeMillis);
                    MRAIDAdPresenter.this.report.recordAction("mraidClose", "", currentTimeMillis);
                    MRAIDAdPresenter.this.repository.save(MRAIDAdPresenter.this.report, MRAIDAdPresenter.this.repoCallback);
                    MRAIDAdPresenter.this.closeView();
                }
            }, (long) (this.advertisement.getAdConfig().getFlexViewCloseTime() * 1000));
        }
        String string = cookie == null ? null : cookie.getString("userID");
        if (this.report == null) {
            this.adStartTime = System.currentTimeMillis();
            Report report2 = new Report(this.advertisement, this.placement, this.adStartTime, string);
            this.report = report2;
            this.repository.save(this.report, this.repoCallback);
        }
        Cookie cookie2 = (Cookie) this.cookieMap.get(Cookie.CONSENT_COOKIE);
        if (cookie2 != null) {
            boolean z = cookie2.getBoolean("is_country_data_protected").booleanValue() && "unknown".equals(cookie2.getString("consent_status"));
            this.webClient.setConsentStatus(z, cookie2.getString("consent_title"), cookie2.getString("consent_message"), cookie2.getString("button_accept"), cookie2.getString("button_deny"));
            if (z) {
                cookie2.putValue("consent_status", "opted_out_by_timeout");
                cookie2.putValue("timestamp", Long.valueOf(System.currentTimeMillis() / 1000));
                cookie2.putValue("consent_source", "vungle_modal");
                this.repository.save(cookie2, this.repoCallback);
            }
        }
        int showCloseDelay = this.advertisement.getShowCloseDelay(this.placement.isIncentivized());
        if (showCloseDelay > 0) {
            this.scheduler.schedule(new Runnable() {
                public void run() {
                    MRAIDAdPresenter.this.backEnabled = true;
                }
            }, (long) showCloseDelay);
        } else {
            this.backEnabled = true;
        }
        this.adView.updateWindow(this.advertisement.getTemplateType().equals("flexview"));
        if (this.bus != null) {
            this.bus.onNext("start", null, this.placement.getId());
        }
    }

    private void loadMraid(@NonNull File file) {
        File file2 = new File(file.getParent());
        StringBuilder sb = new StringBuilder();
        sb.append(file2.getPath());
        sb.append(File.separator);
        sb.append("index.html");
        final File file3 = new File(sb.toString());
        new AsyncFileUtils(this.ioExecutor, this.uiExecutor).isFileExistAsync(file3, new FileExist() {
            public void status(boolean z) {
                if (!z) {
                    if (MRAIDAdPresenter.this.bus != null) {
                        MRAIDAdPresenter.this.bus.onError(new VungleException(10), MRAIDAdPresenter.this.placement.getId());
                    }
                    MRAIDAdPresenter.this.adView.close();
                    return;
                }
                WebAdView access$700 = MRAIDAdPresenter.this.adView;
                StringBuilder sb = new StringBuilder();
                sb.append(Advertisement.FILE_SCHEME);
                sb.append(file3.getPath());
                access$700.showWebsite(sb.toString());
            }
        });
    }

    public void start() {
        this.webClient.setAdVisibility(true);
        this.adView.setImmersiveMode();
        this.adView.resumeWeb();
        setAdVisibility(true);
    }

    public void stop(boolean z, boolean z2) {
        this.adView.pauseWeb();
        setAdVisibility(false);
        if (!z && z2 && !this.isDestroying.getAndSet(true)) {
            String str = null;
            if (this.webClient != null) {
                this.webClient.setMRAIDDelegate(null);
            }
            if (this.bus != null) {
                EventListener eventListener = this.bus;
                String str2 = TtmlNode.END;
                if (this.report.isCTAClicked()) {
                    str = "isCTAClicked";
                }
                eventListener.onNext(str2, str, this.placement.getId());
            }
            if (this.directDownloadAdapter != null) {
                this.directDownloadAdapter.getSdkDownloadClient().cleanUp();
            }
        }
    }

    public void setAdVisibility(boolean z) {
        this.webClient.setAdVisibility(z);
    }

    public void generateSaveState(@Nullable OptionsState optionsState) {
        if (optionsState != null) {
            this.repository.save(this.report, this.repoCallback);
            optionsState.put(EXTRA_REPORT, this.report.getId());
            optionsState.put(EXTRA_INCENTIVIZED_SENT, this.sendReportIncentivized.get());
        }
    }

    public void restoreFromSave(@Nullable OptionsState optionsState) {
        if (optionsState != null) {
            boolean z = optionsState.getBoolean(EXTRA_INCENTIVIZED_SENT, false);
            if (z) {
                this.sendReportIncentivized.set(z);
            }
            if (this.report == null) {
                this.adView.close();
            }
        }
    }

    public boolean handleExit(@Nullable String str) {
        if (str == null) {
            if (this.backEnabled) {
                this.adView.showWebsite("javascript:window.vungle.mraidBridgeExt.requestMRAIDClose()");
            }
            return false;
        } else if (this.advertisement == null || this.placement == null) {
            Log.e(TAG, "Unable to close advertisement");
            return false;
        } else if (!this.placement.getId().equals(str)) {
            Log.e(TAG, "Cannot close FlexView Ad with invalid placement reference id");
            return false;
        } else if (!"flexview".equals(this.advertisement.getTemplateType())) {
            Log.e(TAG, "Cannot close a Non FlexView ad");
            return false;
        } else {
            this.adView.showWebsite("javascript:window.vungle.mraidBridgeExt.requestMRAIDClose()");
            reportAction("mraidCloseByApi", null);
            return true;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:100:0x01dd, code lost:
        if (r9.equals(com.tapjoy.TJAdUnitConstants.String.VISIBLE) == false) goto L_0x01f4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:0x0189, code lost:
        if (r9.equals("false") == false) goto L_0x01a0;
     */
    /* JADX WARNING: Removed duplicated region for block: B:109:0x01f8  */
    /* JADX WARNING: Removed duplicated region for block: B:111:0x020f A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:91:0x01a4  */
    /* JADX WARNING: Removed duplicated region for block: B:93:0x01bb A[RETURN] */
    public boolean processCommand(@NonNull String str, @NonNull JsonObject jsonObject) {
        char c;
        int i;
        String str2;
        String str3;
        char c2 = 2;
        switch (str.hashCode()) {
            case -1912374177:
                if (str.equals(SUCCESSFUL_VIEW)) {
                    c = 9;
                    break;
                }
            case -1891064718:
                if (str.equals(OPEN_APP_IN_DEVICE)) {
                    c = 12;
                    break;
                }
            case -1422950858:
                if (str.equals("action")) {
                    c = 4;
                    break;
                }
            case -1382780692:
                if (str.equals(START_DOWNLOAD_APP_ON_DEVICE)) {
                    c = 10;
                    break;
                }
            case -735200587:
                if (str.equals(ACTION_WITH_VALUE)) {
                    c = 2;
                    break;
                }
            case -660787472:
                if (str.equals(CONSENT_ACTION)) {
                    c = 1;
                    break;
                }
            case -511324706:
                if (str.equals(OPEN_PRIVACY)) {
                    c = 8;
                    break;
                }
            case -503430878:
                if (str.equals(CANCEL_DOWNLOAD)) {
                    c = 11;
                    break;
                }
            case -348095344:
                if (str.equals(USE_CUSTOM_PRIVACY)) {
                    c = 7;
                    break;
                }
            case 3417674:
                if (str.equals(OPEN)) {
                    c = 5;
                    break;
                }
            case 3566511:
                if (str.equals(TPAT)) {
                    c = 3;
                    break;
                }
            case 94756344:
                if (str.equals("close")) {
                    c = 0;
                    break;
                }
            case 1614272768:
                if (str.equals(USE_CUSTOM_CLOSE)) {
                    c = 6;
                    break;
                }
            default:
                c = 65535;
                break;
        }
        switch (c) {
            case 0:
                reportAction("mraidClose", null);
                closeView();
                return true;
            case 1:
                Cookie cookie = (Cookie) this.cookieMap.get(Cookie.CONSENT_COOKIE);
                if (cookie == null) {
                    cookie = new Cookie(Cookie.CONSENT_COOKIE);
                }
                cookie.putValue("consent_status", jsonObject.get("event").getAsString());
                cookie.putValue("consent_source", "vungle_modal");
                cookie.putValue("timestamp", Long.valueOf(System.currentTimeMillis() / 1000));
                this.repository.save(cookie, this.repoCallback);
                return true;
            case 2:
                String asString = jsonObject.get("event").getAsString();
                String asString2 = jsonObject.get("value").getAsString();
                this.report.recordAction(asString, asString2, System.currentTimeMillis());
                this.repository.save(this.report, this.repoCallback);
                if (asString.equals("videoViewed") && this.duration > 0) {
                    try {
                        i = (int) ((Float.parseFloat(asString2) / ((float) this.duration)) * 100.0f);
                    } catch (NumberFormatException unused) {
                        Log.e(TAG, "value for videoViewed is null !");
                        i = 0;
                    }
                    if (i > 0) {
                        if (this.bus != null) {
                            EventListener eventListener = this.bus;
                            StringBuilder sb = new StringBuilder();
                            sb.append("percentViewed:");
                            sb.append(i);
                            String sb2 = sb.toString();
                            if (this.placement == null) {
                                str2 = null;
                            } else {
                                str2 = this.placement.getId();
                            }
                            eventListener.onNext(sb2, null, str2);
                        }
                        if (!this.hasSendStart && i > 1) {
                            this.hasSendStart = true;
                            if (this.directDownloadAdapter != null) {
                                this.directDownloadAdapter.getSdkDownloadClient().sendADDisplayingNotify(false, CONTRACT_TYPE.CPI);
                            }
                        }
                        if (!this.hasSend80Percent && i > 80) {
                            this.hasSend80Percent = true;
                            if (this.directDownloadAdapter != null) {
                                this.directDownloadAdapter.getSdkDownloadClient().sendADDisplayingNotify(true, CONTRACT_TYPE.CPI);
                            }
                        }
                        Cookie cookie2 = (Cookie) this.cookieMap.get(Cookie.CONFIG_COOKIE);
                        if (this.placement.isIncentivized() && i > 75 && cookie2 != null && cookie2.getBoolean("isReportIncentivizedEnabled").booleanValue() && !this.sendReportIncentivized.getAndSet(true)) {
                            JsonObject jsonObject2 = new JsonObject();
                            jsonObject2.add("placement_reference_id", new JsonPrimitive(this.placement.getId()));
                            jsonObject2.add("app_id", new JsonPrimitive(this.advertisement.getAppID()));
                            jsonObject2.add(ReportColumns.COLUMN_AD_START_TIME, new JsonPrimitive((Number) Long.valueOf(this.report.getAdStartTime())));
                            jsonObject2.add("user", new JsonPrimitive(this.report.getUserID()));
                            this.analytics.ri(jsonObject2);
                        }
                    }
                }
                if (asString.equals("videoLength")) {
                    this.duration = Long.parseLong(asString2);
                    reportAction("videoLength", asString2);
                    this.webClient.notifyPropertiesChange(true);
                }
                this.adView.setVisibility(true);
                return true;
            case 3:
                this.analytics.ping(this.advertisement.getTpatUrls(jsonObject.get("event").getAsString()));
                return true;
            case 4:
                return true;
            case 5:
                reportAction(MraidHandler.DOWNLOAD_ACTION, null);
                reportAction("mraidOpen", null);
                String asString3 = jsonObject.get("url").getAsString();
                if (APKDirectDownloadManager.isDirectDownloadEnabled(this.directDownloadApkEnabled, this.advertisement.isRequiresNonMarketInstall())) {
                    APKDirectDownloadManager.download(asString3);
                } else {
                    this.adView.open(asString3);
                }
                return true;
            case 6:
                String asString4 = jsonObject.get("sdkCloseButton").getAsString();
                int hashCode = asString4.hashCode();
                if (hashCode == -1901805651) {
                    if (asString4.equals("invisible")) {
                        c2 = 1;
                        switch (c2) {
                            case 0:
                            case 1:
                            case 2:
                                break;
                        }
                    }
                } else if (hashCode != 3178655) {
                    if (hashCode == 466743410) {
                        break;
                    }
                } else if (asString4.equals("gone")) {
                    c2 = 0;
                    switch (c2) {
                        case 0:
                        case 1:
                        case 2:
                            return true;
                        default:
                            StringBuilder sb3 = new StringBuilder();
                            sb3.append("Unknown value ");
                            sb3.append(asString4);
                            throw new IllegalArgumentException(sb3.toString());
                    }
                }
                c2 = 65535;
                switch (c2) {
                    case 0:
                    case 1:
                    case 2:
                        break;
                }
            case 7:
                String asString5 = jsonObject.get(USE_CUSTOM_PRIVACY).getAsString();
                int hashCode2 = asString5.hashCode();
                if (hashCode2 == 3178655) {
                    if (asString5.equals("gone")) {
                        c2 = 0;
                        switch (c2) {
                            case 0:
                            case 1:
                            case 2:
                                break;
                        }
                    }
                } else if (hashCode2 != 3569038) {
                    if (hashCode2 == 97196323) {
                        break;
                    }
                } else if (asString5.equals("true")) {
                    c2 = 1;
                    switch (c2) {
                        case 0:
                        case 1:
                        case 2:
                            return true;
                        default:
                            StringBuilder sb4 = new StringBuilder();
                            sb4.append("Unknown value ");
                            sb4.append(asString5);
                            throw new IllegalArgumentException(sb4.toString());
                    }
                }
                c2 = 65535;
                switch (c2) {
                    case 0:
                    case 1:
                    case 2:
                        break;
                }
            case 8:
                this.adView.open(jsonObject.get("url").getAsString());
                return true;
            case 9:
                if (this.bus != null) {
                    EventListener eventListener2 = this.bus;
                    String str4 = SUCCESSFUL_VIEW;
                    if (this.placement == null) {
                        str3 = null;
                    } else {
                        str3 = this.placement.getId();
                    }
                    eventListener2.onNext(str4, null, str3);
                }
                Cookie cookie3 = (Cookie) this.cookieMap.get(Cookie.CONFIG_COOKIE);
                if (this.placement.isIncentivized() && cookie3 != null && cookie3.getBoolean("isReportIncentivizedEnabled").booleanValue() && !this.sendReportIncentivized.getAndSet(true)) {
                    JsonObject jsonObject3 = new JsonObject();
                    jsonObject3.add("placement_reference_id", new JsonPrimitive(this.placement.getId()));
                    jsonObject3.add("app_id", new JsonPrimitive(this.advertisement.getAppID()));
                    jsonObject3.add(ReportColumns.COLUMN_AD_START_TIME, new JsonPrimitive((Number) Long.valueOf(this.report.getAdStartTime())));
                    jsonObject3.add("user", new JsonPrimitive(this.report.getUserID()));
                    this.analytics.ri(jsonObject3);
                }
                return true;
            case 10:
                if (this.directDownloadAdapter != null) {
                    this.directDownloadAdapter.getSdkDownloadClient().sendDownloadRequest();
                }
                return true;
            case 11:
                if (this.directDownloadAdapter != null) {
                    this.directDownloadAdapter.getSdkDownloadClient().cancelDownloadRequest();
                }
                return true;
            case 12:
                if (this.directDownloadAdapter != null) {
                    this.directDownloadAdapter.getSdkDownloadClient().sendOpenPackageRequest();
                }
                return true;
            default:
                return false;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x0036  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x004d  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0051  */
    /* JADX WARNING: Removed duplicated region for block: B:22:? A[RETURN, SYNTHETIC] */
    public void onMraidAction(@NonNull String str) {
        char c;
        int hashCode = str.hashCode();
        if (hashCode == -314498168) {
            if (str.equals("privacy")) {
                c = 2;
                switch (c) {
                    case 0:
                        break;
                    case 1:
                        break;
                    case 2:
                        break;
                }
            }
        } else if (hashCode == 94756344) {
            if (str.equals("close")) {
                c = 0;
                switch (c) {
                    case 0:
                        break;
                    case 1:
                        break;
                    case 2:
                        break;
                }
            }
        } else if (hashCode == 1427818632 && str.equals(MraidHandler.DOWNLOAD_ACTION)) {
            c = 1;
            switch (c) {
                case 0:
                    closeView();
                    return;
                case 1:
                    download();
                    return;
                case 2:
                    return;
                default:
                    StringBuilder sb = new StringBuilder();
                    sb.append("Unknown action ");
                    sb.append(str);
                    throw new IllegalArgumentException(sb.toString());
            }
        }
        c = 65535;
        switch (c) {
            case 0:
                break;
            case 1:
                break;
            case 2:
                break;
        }
    }

    private void download() {
        reportAction("cta", "");
        try {
            this.analytics.ping(new String[]{this.advertisement.getCTAURL(true)});
            this.adView.open(this.advertisement.getCTAURL(false));
        } catch (ActivityNotFoundException unused) {
        }
    }

    /* access modifiers changed from: private */
    public void closeView() {
        this.report.setAdDuration((int) (System.currentTimeMillis() - this.adStartTime));
        this.repository.save(this.report, this.repoCallback);
        this.adView.close();
        this.scheduler.cancelAll();
    }

    private void load(OptionsState optionsState) {
        this.cookieMap.put(Cookie.INCENTIVIZED_TEXT_COOKIE, this.repository.load(Cookie.INCENTIVIZED_TEXT_COOKIE, Cookie.class).get());
        this.cookieMap.put(Cookie.CONSENT_COOKIE, this.repository.load(Cookie.CONSENT_COOKIE, Cookie.class).get());
        this.cookieMap.put(Cookie.CONFIG_COOKIE, this.repository.load(Cookie.CONFIG_COOKIE, Cookie.class).get());
        if (optionsState != null) {
            String string = optionsState.getString(EXTRA_REPORT);
            this.report = TextUtils.isEmpty(string) ? null : (Report) this.repository.load(string, Report.class).get();
        }
    }

    public void onReceivedError(String str) {
        if (this.report != null) {
            this.report.recordError(str);
            this.repository.save(this.report, this.repoCallback);
        }
    }
}
