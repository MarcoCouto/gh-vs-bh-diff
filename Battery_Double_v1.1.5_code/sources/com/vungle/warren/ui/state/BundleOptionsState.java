package com.vungle.warren.ui.state;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class BundleOptionsState implements OptionsState, Parcelable {
    public static final Creator<BundleOptionsState> CREATOR = new Creator<BundleOptionsState>() {
        public BundleOptionsState createFromParcel(Parcel parcel) {
            return new BundleOptionsState(parcel);
        }

        public BundleOptionsState[] newArray(int i) {
            return new BundleOptionsState[i];
        }
    };
    private Map<String, Boolean> bools = new HashMap();
    private Map<String, Integer> ints = new HashMap();
    private Map<String, String> strings = new HashMap();

    public int describeContents() {
        return 0;
    }

    public BundleOptionsState() {
    }

    public void put(String str, String str2) {
        this.strings.put(str, str2);
    }

    public void put(String str, boolean z) {
        this.bools.put(str, Boolean.valueOf(z));
    }

    public void put(String str, int i) {
        this.ints.put(str, Integer.valueOf(i));
    }

    public boolean getBoolean(String str, boolean z) {
        Boolean bool = (Boolean) this.bools.get(str);
        return bool == null ? z : bool.booleanValue();
    }

    public String getString(String str) {
        return (String) this.strings.get(str);
    }

    public Integer getInt(String str, int i) {
        Integer num = (Integer) this.ints.get(str);
        if (num != null) {
            i = num.intValue();
        }
        return Integer.valueOf(i);
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.strings.size());
        for (Entry entry : this.strings.entrySet()) {
            parcel.writeString((String) entry.getKey());
            parcel.writeValue(entry.getValue());
        }
        parcel.writeInt(this.bools.size());
        for (Entry entry2 : this.bools.entrySet()) {
            parcel.writeString((String) entry2.getKey());
            parcel.writeValue(entry2.getValue());
        }
    }

    protected BundleOptionsState(Parcel parcel) {
        int readInt = parcel.readInt();
        for (int i = 0; i < readInt; i++) {
            this.strings.put(parcel.readString(), (String) parcel.readValue(String.class.getClassLoader()));
        }
        int readInt2 = parcel.readInt();
        for (int i2 = 0; i2 < readInt2; i2++) {
            this.bools.put(parcel.readString(), (Boolean) parcel.readValue(Boolean.class.getClassLoader()));
        }
    }
}
