package com.vungle.warren.ui;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.util.Pair;
import com.vungle.warren.AdvertisementPresentationFactory;
import com.vungle.warren.AdvertisementPresentationFactory.FullScreenCallback;
import com.vungle.warren.Vungle;
import com.vungle.warren.error.VungleException;
import com.vungle.warren.ui.contract.AdContract.AdView;
import com.vungle.warren.ui.contract.AdContract.AdvertisementBus;
import com.vungle.warren.ui.contract.AdContract.AdvertisementPresenter;
import com.vungle.warren.ui.contract.AdContract.AdvertisementPresenter.EventListener;
import com.vungle.warren.ui.state.BundleOptionsState;
import com.vungle.warren.ui.state.OptionsState;
import com.vungle.warren.ui.view.FullAdWidget;
import java.util.concurrent.atomic.AtomicBoolean;

public class VungleActivity extends Activity {
    public static final String PLACEMENT_EXTRA = "placement";
    public static final String PRESENTER_STATE = "presenter_state";
    private static final String TAG = "VungleActivity";
    /* access modifiers changed from: private */
    public static EventListener bus;
    private BroadcastReceiver broadcastReceiver;
    private FullScreenCallback fullscreenCallback = new FullScreenCallback() {
        public void onResult(@Nullable Pair<AdView, AdvertisementPresenter> pair, @Nullable Exception exc) {
            if (pair == null || exc != null) {
                VungleActivity.this.deliverError(10, VungleActivity.this.placementId);
                VungleActivity.this.finish();
                return;
            }
            VungleActivity.this.presenter = (AdvertisementPresenter) pair.second;
            VungleActivity.this.presenter.setEventListener(VungleActivity.bus);
            VungleActivity.this.presenter.attach((AdView) pair.first, VungleActivity.this.state);
            if (VungleActivity.this.pendingStart.getAndSet(false)) {
                VungleActivity.this.presenter.start();
            }
        }
    };
    /* access modifiers changed from: private */
    public AtomicBoolean pendingStart = new AtomicBoolean(false);
    /* access modifiers changed from: private */
    public String placementId;
    /* access modifiers changed from: private */
    @Nullable
    public AdvertisementPresenter presenter;
    private AdvertisementPresentationFactory presenterFactory;
    /* access modifiers changed from: private */
    public OptionsState state;

    /* access modifiers changed from: protected */
    public boolean canRotate() {
        return true;
    }

    public static void setEventListener(EventListener eventListener) {
        bus = eventListener;
    }

    /* access modifiers changed from: protected */
    @SuppressLint({"SetJavaScriptEnabled"})
    public void onCreate(@Nullable Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        getWindow().setFlags(16777216, 16777216);
        this.placementId = getIntent().getStringExtra("placement");
        try {
            FullAdWidget fullAdWidget = new FullAdWidget(this, getWindow());
            if (!Vungle.isInitialized() || bus == null) {
                finish();
                return;
            }
            try {
                this.presenterFactory = new AdvertisementPresentationFactory(this.placementId, bundle, new OrientationDelegate() {
                    public void setOrientation(int i) {
                        VungleActivity.this.setRequestedOrientation(i);
                    }
                }, new CloseDelegate() {
                    public void close() {
                        VungleActivity.this.finish();
                    }
                });
                this.state = bundle == null ? null : (OptionsState) bundle.getParcelable(PRESENTER_STATE);
                this.presenterFactory.getFullScreenPresentation(this, fullAdWidget, this.state, this.fullscreenCallback);
                setContentView(fullAdWidget, fullAdWidget.getLayoutParams());
                connectBroadcastReceiver();
            } catch (InstantiationException e) {
                String str = TAG;
                StringBuilder sb = new StringBuilder();
                sb.append("error on crreating presentations");
                sb.append(e.getLocalizedMessage());
                Log.e(str, sb.toString());
                deliverError(10, this.placementId);
                finish();
            }
        } catch (InstantiationException e2) {
            if (bus != null) {
                bus.onError(e2, this.placementId);
            }
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        String stringExtra = getIntent().getStringExtra("placement");
        String stringExtra2 = intent.getStringExtra("placement");
        if (stringExtra != null && stringExtra2 != null && !stringExtra.equals(stringExtra2)) {
            String str = TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("Tried to play another placement ");
            sb.append(stringExtra2);
            sb.append(" while playing ");
            sb.append(stringExtra);
            Log.d(str, sb.toString());
            deliverError(15, stringExtra2);
        }
    }

    /* access modifiers changed from: private */
    public void deliverError(int i, String str) {
        if (bus != null) {
            bus.onError(new VungleException(i), str);
        }
    }

    private void connectBroadcastReceiver() {
        this.broadcastReceiver = new BroadcastReceiver() {
            /* JADX WARNING: Removed duplicated region for block: B:12:0x002d  */
            /* JADX WARNING: Removed duplicated region for block: B:14:0x0044  */
            /* JADX WARNING: Removed duplicated region for block: B:15:0x004a  */
            public void onReceive(Context context, Intent intent) {
                char c;
                String stringExtra = intent.getStringExtra("command");
                int hashCode = stringExtra.hashCode();
                if (hashCode == -1884364225) {
                    if (stringExtra.equals(AdvertisementBus.STOP_ALL)) {
                        c = 1;
                        switch (c) {
                            case 0:
                                break;
                            case 1:
                                break;
                        }
                    }
                } else if (hashCode == -482896367 && stringExtra.equals(AdvertisementBus.CLOSE_FLEX)) {
                    c = 0;
                    switch (c) {
                        case 0:
                            String stringExtra2 = intent.getStringExtra("placement");
                            if (VungleActivity.this.presenter != null) {
                                VungleActivity.this.presenter.handleExit(stringExtra2);
                                return;
                            }
                            return;
                        case 1:
                            VungleActivity.this.finish();
                            return;
                        default:
                            StringBuilder sb = new StringBuilder();
                            sb.append("No such command ");
                            sb.append(stringExtra);
                            throw new IllegalArgumentException(sb.toString());
                    }
                }
                c = 65535;
                switch (c) {
                    case 0:
                        break;
                    case 1:
                        break;
                }
            }
        };
        LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(this.broadcastReceiver, new IntentFilter(AdvertisementBus.ACTION));
    }

    public void onWindowFocusChanged(boolean z) {
        super.onWindowFocusChanged(z);
        if (!z) {
            if (this.presenter != null) {
                this.presenter.stop(isChangingConfigurations(), isFinishing());
            }
            this.pendingStart.set(false);
        } else if (this.presenter != null) {
            this.presenter.start();
        } else {
            this.pendingStart.set(true);
        }
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        if (configuration.orientation == 2) {
            Log.d(TAG, "landscape");
        } else if (configuration.orientation == 1) {
            Log.d(TAG, "portrait");
        }
        if (this.presenter != null) {
            this.presenter.onViewConfigurationChanged();
        }
    }

    @SuppressLint({"ResourceType"})
    public void onBackPressed() {
        if (this.presenter != null) {
            this.presenter.handleExit(null);
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        Log.d(TAG, "onSaveInstanceState");
        BundleOptionsState bundleOptionsState = new BundleOptionsState();
        if (this.presenter != null) {
            this.presenter.generateSaveState(bundleOptionsState);
            bundle.putParcelable(PRESENTER_STATE, bundleOptionsState);
        }
        this.presenterFactory.saveState(bundle);
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Bundle bundle) {
        super.onRestoreInstanceState(bundle);
        String str = TAG;
        StringBuilder sb = new StringBuilder();
        sb.append("onRestoreInstanceState(");
        sb.append(bundle);
        sb.append(")");
        Log.d(str, sb.toString());
        if (bundle != null && this.presenter != null) {
            this.presenter.restoreFromSave((OptionsState) bundle.getParcelable(PRESENTER_STATE));
        }
    }

    public void setRequestedOrientation(int i) {
        if (canRotate()) {
            super.setRequestedOrientation(i);
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(this.broadcastReceiver);
        if (this.presenter != null) {
            this.presenter.detach(isChangingConfigurations());
        } else if (this.presenterFactory != null) {
            this.presenterFactory.destroy();
            this.presenterFactory = null;
            if (bus != null) {
                bus.onError(new VungleException(25), this.placementId);
            }
        }
        super.onDestroy();
        System.gc();
    }
}
