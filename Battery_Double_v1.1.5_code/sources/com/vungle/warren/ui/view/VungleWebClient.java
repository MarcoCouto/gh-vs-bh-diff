package com.vungle.warren.ui.view;

import android.net.Uri;
import android.os.Build.VERSION;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.explorestack.iab.mraid.MRAIDNativeFeature;
import com.google.gson.JsonObject;
import com.integralads.avid.library.inmobi.utils.AvidJSONUtil;
import com.ironsource.sdk.constants.Constants.ParametersKeys;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.vungle.warren.DirectDownloadAdapter;
import com.vungle.warren.SDKDownloadClient.InstallStatusCheck;
import com.vungle.warren.model.Advertisement;
import com.vungle.warren.model.Placement;
import com.vungle.warren.ui.view.WebViewAPI.MRAIDDelegate;
import com.vungle.warren.ui.view.WebViewAPI.WebClientErrorListener;
import java.util.Locale;

public class VungleWebClient extends WebViewClient implements WebViewAPI {
    public static final String TAG = "VungleWebClient";
    private MRAIDDelegate MRAIDDelegate;
    private Advertisement advertisement;
    private boolean collectConsent;
    private DirectDownloadAdapter directDownloadAdapter;
    private WebClientErrorListener errorListener;
    private String gdprAccept;
    private String gdprBody;
    private String gdprDeny;
    private String gdprTitle;
    private Boolean isViewable;
    private WebView loadedWebView;
    private Placement placement;
    private boolean ready = false;

    public VungleWebClient(Advertisement advertisement2, Placement placement2) {
        this.advertisement = advertisement2;
        this.placement = placement2;
    }

    public void setConsentStatus(boolean z, @Nullable String str, @Nullable String str2, @Nullable String str3, @Nullable String str4) {
        this.collectConsent = z;
        this.gdprTitle = str;
        this.gdprBody = str2;
        this.gdprAccept = str3;
        this.gdprDeny = str4;
    }

    public void setMRAIDDelegate(MRAIDDelegate mRAIDDelegate) {
        this.MRAIDDelegate = mRAIDDelegate;
    }

    public boolean shouldOverrideUrlLoading(final WebView webView, String str) {
        String str2 = TAG;
        StringBuilder sb = new StringBuilder();
        sb.append("MRAID Command ");
        sb.append(str);
        Log.d(str2, sb.toString());
        if (TextUtils.isEmpty(str)) {
            Log.e(TAG, "Invalid URL ");
            return false;
        }
        Uri parse = Uri.parse(str);
        if (parse.getScheme() == null || !parse.getScheme().equals(CampaignEx.JSON_KEY_MRAID)) {
            return false;
        }
        String host = parse.getHost();
        if (host.equals("propertiesChangeCompleted") && !this.ready) {
            final JsonObject createMRAIDArgs = this.advertisement.createMRAIDArgs();
            if (this.directDownloadAdapter != null) {
                this.directDownloadAdapter.getSdkDownloadClient().setInstallStatusCheck(new InstallStatusCheck() {
                    public void isAppInstalled(boolean z, boolean z2) {
                        createMRAIDArgs.addProperty("isDirectDownload", Boolean.valueOf(true));
                        createMRAIDArgs.addProperty("isDisplayIAP", Boolean.valueOf(z2));
                        createMRAIDArgs.addProperty("isInstalled", Boolean.valueOf(z));
                        createMRAIDArgs.addProperty("locale", Locale.getDefault().toString());
                        createMRAIDArgs.addProperty("language", Locale.getDefault().getLanguage());
                        WebView webView = webView;
                        StringBuilder sb = new StringBuilder();
                        sb.append("javascript:window.vungle.mraidBridge.notifyReadyEvent(");
                        sb.append(createMRAIDArgs);
                        sb.append(")");
                        webView.loadUrl(sb.toString());
                    }
                });
                this.directDownloadAdapter.getSdkDownloadClient().installStatusRequest();
            } else {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("javascript:window.vungle.mraidBridge.notifyReadyEvent(");
                sb2.append(createMRAIDArgs);
                sb2.append(")");
                webView.loadUrl(sb2.toString());
            }
            this.ready = true;
        } else if (this.MRAIDDelegate != null) {
            JsonObject jsonObject = new JsonObject();
            for (String str3 : parse.getQueryParameterNames()) {
                jsonObject.addProperty(str3, parse.getQueryParameter(str3));
            }
            if (this.MRAIDDelegate.processCommand(host, jsonObject)) {
                webView.loadUrl("javascript:window.vungle.mraidBridge.notifyCommandComplete()");
            }
        }
        return true;
    }

    public void onPageFinished(WebView webView, String str) {
        super.onPageFinished(webView, str);
        switch (this.advertisement.getAdType()) {
            case 0:
                webView.loadUrl("javascript:function actionClicked(action){Android.performAction(action);};");
                return;
            case 1:
                this.loadedWebView = webView;
                this.loadedWebView.setVisibility(0);
                notifyPropertiesChange(true);
                return;
            default:
                throw new IllegalArgumentException("Unknown Client Type!");
        }
    }

    public void notifyPropertiesChange(boolean z) {
        if (this.loadedWebView != null) {
            JsonObject jsonObject = new JsonObject();
            JsonObject jsonObject2 = new JsonObject();
            jsonObject2.addProperty("width", (Number) Integer.valueOf(this.loadedWebView.getWidth()));
            jsonObject2.addProperty("height", (Number) Integer.valueOf(this.loadedWebView.getHeight()));
            JsonObject jsonObject3 = new JsonObject();
            jsonObject3.addProperty(AvidJSONUtil.KEY_X, (Number) Integer.valueOf(0));
            jsonObject3.addProperty(AvidJSONUtil.KEY_Y, (Number) Integer.valueOf(0));
            jsonObject3.addProperty("width", (Number) Integer.valueOf(this.loadedWebView.getWidth()));
            jsonObject3.addProperty("height", (Number) Integer.valueOf(this.loadedWebView.getHeight()));
            JsonObject jsonObject4 = new JsonObject();
            jsonObject4.addProperty("sms", Boolean.valueOf(false));
            jsonObject4.addProperty("tel", Boolean.valueOf(false));
            jsonObject4.addProperty(MRAIDNativeFeature.CALENDAR, Boolean.valueOf(false));
            jsonObject4.addProperty(MRAIDNativeFeature.STORE_PICTURE, Boolean.valueOf(false));
            jsonObject4.addProperty(MRAIDNativeFeature.INLINE_VIDEO, Boolean.valueOf(false));
            jsonObject.add("maxSize", jsonObject2);
            jsonObject.add("screenSize", jsonObject2);
            jsonObject.add("defaultPosition", jsonObject3);
            jsonObject.add("currentPosition", jsonObject3);
            jsonObject.add("supports", jsonObject4);
            jsonObject.addProperty("placementType", this.advertisement.getTemplateType());
            if (this.isViewable != null) {
                jsonObject.addProperty(ParametersKeys.IS_VIEWABLE, this.isViewable);
            }
            jsonObject.addProperty("os", "android");
            jsonObject.addProperty("osVersion", Integer.toString(VERSION.SDK_INT));
            jsonObject.addProperty("incentivized", Boolean.valueOf(this.placement.isIncentivized()));
            jsonObject.addProperty("enableBackImmediately", Boolean.valueOf(this.advertisement.getShowCloseDelay(this.placement.isIncentivized()) == 0));
            jsonObject.addProperty("version", "1.0");
            if (this.collectConsent) {
                jsonObject.addProperty("consentRequired", Boolean.valueOf(true));
                jsonObject.addProperty("consentTitleText", this.gdprTitle);
                jsonObject.addProperty("consentBodyText", this.gdprBody);
                jsonObject.addProperty("consentAcceptButtonText", this.gdprAccept);
                jsonObject.addProperty("consentDenyButtonText", this.gdprDeny);
            } else {
                jsonObject.addProperty("consentRequired", Boolean.valueOf(false));
            }
            String str = TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("loadJsjavascript:window.vungle.mraidBridge.notifyPropertiesChange(");
            sb.append(jsonObject);
            sb.append(",");
            sb.append(z);
            sb.append(")");
            Log.d(str, sb.toString());
            WebView webView = this.loadedWebView;
            StringBuilder sb2 = new StringBuilder();
            sb2.append("javascript:window.vungle.mraidBridge.notifyPropertiesChange(");
            sb2.append(jsonObject);
            sb2.append(",");
            sb2.append(z);
            sb2.append(")");
            webView.loadUrl(sb2.toString());
        }
    }

    public void setAdVisibility(boolean z) {
        this.isViewable = Boolean.valueOf(z);
        notifyPropertiesChange(false);
    }

    public void setDownloadAdapter(DirectDownloadAdapter directDownloadAdapter2) {
        this.directDownloadAdapter = directDownloadAdapter2;
    }

    public void setErrorListener(WebClientErrorListener webClientErrorListener) {
        this.errorListener = webClientErrorListener;
    }

    public void onReceivedError(WebView webView, int i, String str, String str2) {
        super.onReceivedError(webView, i, str, str2);
        if (VERSION.SDK_INT < 23) {
            String str3 = TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("Error desc ");
            sb.append(str);
            Log.e(str3, sb.toString());
            String str4 = TAG;
            StringBuilder sb2 = new StringBuilder();
            sb2.append("Error for URL ");
            sb2.append(str2);
            Log.e(str4, sb2.toString());
            StringBuilder sb3 = new StringBuilder();
            sb3.append(str2);
            sb3.append(" ");
            sb3.append(str);
            String sb4 = sb3.toString();
            if (this.errorListener != null) {
                this.errorListener.onReceivedError(sb4);
            }
        }
    }

    public void onReceivedError(WebView webView, WebResourceRequest webResourceRequest, WebResourceError webResourceError) {
        super.onReceivedError(webView, webResourceRequest, webResourceError);
        if (VERSION.SDK_INT >= 23) {
            String str = TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("Error desc ");
            sb.append(webResourceError.getDescription().toString());
            Log.e(str, sb.toString());
            String str2 = TAG;
            StringBuilder sb2 = new StringBuilder();
            sb2.append("Error for URL ");
            sb2.append(webResourceRequest.getUrl().toString());
            Log.e(str2, sb2.toString());
            StringBuilder sb3 = new StringBuilder();
            sb3.append(webResourceRequest.getUrl().toString());
            sb3.append(" ");
            sb3.append(webResourceError.getDescription().toString());
            String sb4 = sb3.toString();
            if (this.errorListener != null) {
                this.errorListener.onReceivedError(sb4);
            }
        }
    }
}
