package com.vungle.warren.ui.view;

import android.support.annotation.Nullable;
import com.google.gson.JsonObject;
import com.vungle.warren.DirectDownloadAdapter;

public interface WebViewAPI {

    public interface MRAIDDelegate {
        boolean processCommand(String str, JsonObject jsonObject);
    }

    public interface WebClientErrorListener {
        void onReceivedError(String str);
    }

    void notifyPropertiesChange(boolean z);

    void setAdVisibility(boolean z);

    void setConsentStatus(boolean z, @Nullable String str, @Nullable String str2, @Nullable String str3, @Nullable String str4);

    void setDownloadAdapter(DirectDownloadAdapter directDownloadAdapter);

    void setErrorListener(WebClientErrorListener webClientErrorListener);

    void setMRAIDDelegate(MRAIDDelegate mRAIDDelegate);
}
