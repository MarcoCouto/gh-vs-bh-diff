package com.vungle.warren.ui.view;

import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.DialogInterface.OnDismissListener;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.ContextThemeWrapper;
import com.vungle.warren.ui.CloseDelegate;
import com.vungle.warren.ui.OrientationDelegate;
import com.vungle.warren.ui.contract.AdContract.AdView;
import com.vungle.warren.ui.contract.AdContract.AdvertisementPresenter;
import com.vungle.warren.utility.ExternalRouter;
import java.util.concurrent.atomic.AtomicReference;

public abstract class BaseAdView<T extends AdvertisementPresenter> implements AdView<T> {
    protected final String TAG = getClass().getSimpleName();
    private final CloseDelegate closeDelegate;
    protected final Context context;
    protected Dialog currentDialog;
    protected Handler handler = new Handler(Looper.getMainLooper());
    private final OrientationDelegate orientationDelegate;
    protected final FullAdWidget view;

    private static class DialogClickListenerProxy implements OnClickListener, OnDismissListener {
        private AtomicReference<OnClickListener> ckickRef = new AtomicReference<>();
        private AtomicReference<OnDismissListener> dismissRef = new AtomicReference<>();

        public DialogClickListenerProxy(OnClickListener onClickListener, OnDismissListener onDismissListener) {
            this.ckickRef.set(onClickListener);
            this.dismissRef.set(onDismissListener);
        }

        public void onClick(DialogInterface dialogInterface, int i) {
            dialogInterface.dismiss();
            OnClickListener onClickListener = (OnClickListener) this.ckickRef.get();
            if (onClickListener != null) {
                onClickListener.onClick(dialogInterface, i);
            }
        }

        /* access modifiers changed from: private */
        public void autoRelease(Dialog dialog) {
            dialog.setOnDismissListener(this);
        }

        public void onDismiss(DialogInterface dialogInterface) {
            OnDismissListener onDismissListener = (OnDismissListener) this.dismissRef.get();
            if (onDismissListener != null) {
                onDismissListener.onDismiss(dialogInterface);
            }
            this.dismissRef.set(null);
            this.ckickRef.set(null);
        }
    }

    public BaseAdView(@NonNull Context context2, @NonNull FullAdWidget fullAdWidget, @NonNull OrientationDelegate orientationDelegate2, @NonNull CloseDelegate closeDelegate2) {
        this.view = fullAdWidget;
        this.context = context2;
        this.orientationDelegate = orientationDelegate2;
        this.closeDelegate = closeDelegate2;
    }

    public void setOrientation(int i) {
        this.orientationDelegate.setOrientation(i);
    }

    public String getWebsiteUrl() {
        return this.view.getUrl();
    }

    public void close() {
        this.closeDelegate.close();
    }

    public void destroyAdView() {
        this.view.release();
    }

    public void showCloseButton() {
        this.view.showCloseButton(true);
    }

    public void open(String str) {
        String str2 = this.TAG;
        StringBuilder sb = new StringBuilder();
        sb.append("Opening ");
        sb.append(str);
        Log.d(str2, sb.toString());
        if (!ExternalRouter.launch(str, this.context)) {
            String str3 = this.TAG;
            StringBuilder sb2 = new StringBuilder();
            sb2.append("Cannot open url ");
            sb2.append(str);
            Log.e(str3, sb2.toString());
        }
    }

    public void showDialog(@Nullable String str, @Nullable String str2, @NonNull String str3, @NonNull String str4, @Nullable final OnClickListener onClickListener) {
        Builder builder = new Builder(new ContextThemeWrapper(this.context, this.context.getApplicationInfo().theme));
        DialogClickListenerProxy dialogClickListenerProxy = new DialogClickListenerProxy(new OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                BaseAdView.this.currentDialog = null;
                if (onClickListener != null) {
                    onClickListener.onClick(dialogInterface, i);
                }
            }
        }, new OnDismissListener() {
            public void onDismiss(DialogInterface dialogInterface) {
                BaseAdView.this.currentDialog = null;
            }
        });
        if (!TextUtils.isEmpty(str)) {
            builder.setTitle(str);
        }
        if (!TextUtils.isEmpty(str2)) {
            builder.setMessage(str2);
        }
        builder.setPositiveButton(str3, dialogClickListenerProxy);
        builder.setNegativeButton(str4, dialogClickListenerProxy);
        builder.setCancelable(false);
        this.currentDialog = builder.create();
        dialogClickListenerProxy.autoRelease(this.currentDialog);
        this.currentDialog.show();
    }

    public void resumeWeb() {
        this.view.resumeWeb();
    }

    public void pauseWeb() {
        this.view.pauseWeb();
    }

    public void setImmersiveMode() {
        this.view.setImmersiveMode();
    }
}
