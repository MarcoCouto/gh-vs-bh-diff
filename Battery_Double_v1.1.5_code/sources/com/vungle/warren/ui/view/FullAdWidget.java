package com.vungle.warren.ui.view;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import android.os.Build.VERSION;
import android.support.v4.view.ViewCompat;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.VideoView;
import com.google.android.exoplayer2.util.MimeTypes;
import com.ironsource.sdk.constants.Constants;
import com.vungle.warren.ui.JavascriptBridge;
import com.vungle.warren.utility.ViewUtility;
import com.vungle.warren.utility.ViewUtility.Asset;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.HashMap;
import java.util.Map;

public class FullAdWidget extends RelativeLayout {
    protected static final double NINE_BY_SIXTEEN_ASPECT_RATIO = 0.5625d;
    private static final String TAG = "FullAdWidget";
    private final ImageView closeButton;
    private final ImageView ctaOverlay;
    /* access modifiers changed from: private */
    public GestureDetector gestureDetector;
    private final LayoutParams matchParentLayoutParams;
    /* access modifiers changed from: private */
    public final ImageView muteButton;
    /* access modifiers changed from: private */
    public OnItemClickListener onClickProxy;
    /* access modifiers changed from: private */
    public OnCompletionListener onCompletionListener;
    /* access modifiers changed from: private */
    public OnErrorListener onErrorListener;
    /* access modifiers changed from: private */
    public OnPreparedListener onPreparedListener;
    private final ImageView privacyOverlay;
    private final ProgressBar progressBar;
    /* access modifiers changed from: private */
    public OnClickListener proxyClickListener = new OnClickListener() {
        public void onClick(View view) {
            if (FullAdWidget.this.onClickProxy != null) {
                FullAdWidget.this.onClickProxy.onItemClicked(FullAdWidget.this.matchView(view));
            }
        }
    };
    private SimpleOnGestureListener singleTapOnVideoListener = new SimpleOnGestureListener() {
        public boolean onSingleTapConfirmed(MotionEvent motionEvent) {
            FullAdWidget.this.proxyClickListener.onClick(FullAdWidget.this.videoViewContainer);
            return true;
        }
    };
    /* access modifiers changed from: private */
    public int startPosition;
    public final VideoView videoView;
    /* access modifiers changed from: private */
    public final RelativeLayout videoViewContainer;
    private Map<View, Integer> viewToId = new HashMap();
    private final WebView webView;
    private final Window window;

    @Retention(RetentionPolicy.SOURCE)
    public @interface ViewEvent {
        public static final int CLOSE_CLICK = 1;
        public static final int CTA_CLICK = 2;
        public static final int MUTE_CLICK = 3;
        public static final int PRIVACY_CLICK = 4;
        public static final int VIDEO_CLICK = 5;
    }

    public static class AudioContextWrapper extends ContextWrapper {
        public AudioContextWrapper(Context context) {
            super(context);
        }

        public Object getSystemService(String str) {
            if (MimeTypes.BASE_TYPE_AUDIO.equals(str)) {
                return getApplicationContext().getSystemService(str);
            }
            return super.getSystemService(str);
        }
    }

    public interface OnItemClickListener {
        void onItemClicked(int i);
    }

    public FullAdWidget(Context context, Window window2) throws InstantiationException {
        super(context);
        this.window = window2;
        Resources resources = getResources();
        this.matchParentLayoutParams = new LayoutParams(-1, -1);
        setLayoutParams(this.matchParentLayoutParams);
        this.videoView = new VideoView(new AudioContextWrapper(context));
        LayoutParams layoutParams = new LayoutParams(-1, -1);
        layoutParams.addRule(13);
        this.videoView.setLayoutParams(layoutParams);
        this.videoViewContainer = new RelativeLayout(context);
        this.videoViewContainer.setTag("videoViewContainer");
        this.videoViewContainer.setLayoutParams(this.matchParentLayoutParams);
        this.videoViewContainer.addView(this.videoView, layoutParams);
        addView(this.videoViewContainer, this.matchParentLayoutParams);
        this.gestureDetector = new GestureDetector(context, this.singleTapOnVideoListener);
        this.webView = ViewUtility.getWebView(context);
        this.webView.setLayoutParams(this.matchParentLayoutParams);
        this.webView.setTag("webView");
        addView(this.webView, this.matchParentLayoutParams);
        this.progressBar = new ProgressBar(context, null, 16842872);
        LayoutParams layoutParams2 = new LayoutParams(-1, (int) TypedValue.applyDimension(1, 4.0f, resources.getDisplayMetrics()));
        layoutParams2.addRule(12);
        this.progressBar.setLayoutParams(layoutParams2);
        this.progressBar.setMax(100);
        this.progressBar.setIndeterminate(false);
        this.progressBar.setVisibility(4);
        addView(this.progressBar);
        int applyDimension = (int) TypedValue.applyDimension(1, 24.0f, resources.getDisplayMetrics());
        LayoutParams layoutParams3 = new LayoutParams(applyDimension, applyDimension);
        int applyDimension2 = (int) TypedValue.applyDimension(1, 12.0f, resources.getDisplayMetrics());
        layoutParams3.setMargins(applyDimension2, applyDimension2, applyDimension2, applyDimension2);
        this.muteButton = new ImageView(context);
        this.muteButton.setImageBitmap(ViewUtility.getBitmap(Asset.unMute, context));
        this.muteButton.setLayoutParams(layoutParams3);
        this.muteButton.setVisibility(8);
        addView(this.muteButton);
        LayoutParams layoutParams4 = new LayoutParams(applyDimension, applyDimension);
        layoutParams4.setMargins(applyDimension2, applyDimension2, applyDimension2, applyDimension2);
        this.closeButton = new ImageView(context);
        this.closeButton.setTag("closeButton");
        this.closeButton.setImageBitmap(ViewUtility.getBitmap(Asset.close, context));
        layoutParams4.addRule(11);
        this.closeButton.setLayoutParams(layoutParams4);
        this.closeButton.setVisibility(8);
        addView(this.closeButton);
        LayoutParams layoutParams5 = new LayoutParams(applyDimension, applyDimension);
        layoutParams5.addRule(12);
        layoutParams5.addRule(11);
        layoutParams5.setMargins(applyDimension2, applyDimension2, applyDimension2, applyDimension2);
        this.ctaOverlay = new ImageView(context);
        this.ctaOverlay.setTag("ctaOverlay");
        this.ctaOverlay.setLayoutParams(layoutParams5);
        this.ctaOverlay.setImageBitmap(ViewUtility.getBitmap(Asset.cta, getContext()));
        this.ctaOverlay.setVisibility(8);
        addView(this.ctaOverlay);
        LayoutParams layoutParams6 = new LayoutParams(applyDimension, applyDimension);
        layoutParams6.addRule(12);
        layoutParams6.addRule(9);
        layoutParams6.setMargins(applyDimension2, applyDimension2, applyDimension2, applyDimension2);
        this.privacyOverlay = new ImageView(context);
        this.privacyOverlay.setLayoutParams(layoutParams6);
        this.privacyOverlay.setVisibility(8);
        addView(this.privacyOverlay);
        bindListeners();
        prepare();
    }

    private void bindListeners() {
        bindListener(this.closeButton, 1);
        bindListener(this.ctaOverlay, 2);
        bindListener(this.muteButton, 3);
        bindListener(this.privacyOverlay, 4);
        this.viewToId.put(this.videoViewContainer, Integer.valueOf(5));
        this.videoViewContainer.setOnTouchListener(new OnTouchListener() {
            public boolean onTouch(View view, MotionEvent motionEvent) {
                FullAdWidget.this.gestureDetector.onTouchEvent(motionEvent);
                return true;
            }
        });
        this.videoView.setOnPreparedListener(new OnPreparedListener() {
            public void onPrepared(MediaPlayer mediaPlayer) {
                if (VERSION.SDK_INT >= 26) {
                    mediaPlayer.seekTo((long) FullAdWidget.this.startPosition, 3);
                }
                if (FullAdWidget.this.onPreparedListener != null) {
                    FullAdWidget.this.onPreparedListener.onPrepared(mediaPlayer);
                }
                FullAdWidget.this.muteButton.setVisibility(0);
            }
        });
        this.videoView.setOnErrorListener(new OnErrorListener() {
            public boolean onError(MediaPlayer mediaPlayer, int i, int i2) {
                if (FullAdWidget.this.onErrorListener != null) {
                    return FullAdWidget.this.onErrorListener.onError(mediaPlayer, i, i2);
                }
                return false;
            }
        });
        this.videoView.setOnCompletionListener(new OnCompletionListener() {
            public void onCompletion(MediaPlayer mediaPlayer) {
                if (FullAdWidget.this.onCompletionListener != null) {
                    FullAdWidget.this.onCompletionListener.onCompletion(mediaPlayer);
                }
                FullAdWidget.this.muteButton.setEnabled(false);
            }
        });
    }

    private void bindListener(View view, int i) {
        this.viewToId.put(view, Integer.valueOf(i));
        view.setOnClickListener(this.proxyClickListener);
    }

    private void prepare() {
        if (VERSION.SDK_INT >= 17) {
            this.webView.getSettings().setMediaPlaybackRequiresUserGesture(false);
        }
        this.videoViewContainer.setVisibility(8);
        this.webView.setVisibility(8);
    }

    public void linkWebView(WebViewClient webViewClient, JavascriptBridge javascriptBridge) {
        WebSettingsUtils.applyDefault(this.webView);
        this.webView.setWebViewClient(webViewClient);
        this.webView.addJavascriptInterface(javascriptBridge, Constants.JAVASCRIPT_INTERFACE_NAME);
    }

    public void showWebsite(String str) {
        String str2 = TAG;
        StringBuilder sb = new StringBuilder();
        sb.append("loadJs: ");
        sb.append(str);
        Log.d(str2, sb.toString());
        this.webView.loadUrl(str);
        this.webView.setVisibility(0);
        this.videoViewContainer.setVisibility(8);
        this.videoViewContainer.setOnClickListener(null);
        this.progressBar.setVisibility(8);
        this.closeButton.setVisibility(8);
        this.muteButton.setVisibility(8);
        this.ctaOverlay.setVisibility(8);
        this.privacyOverlay.setVisibility(8);
    }

    public void setCtaEnabled(boolean z) {
        this.ctaOverlay.setVisibility(z ? 0 : 8);
    }

    public boolean isVideoPlaying() {
        return this.videoView.isPlaying();
    }

    public void stopPlayback() {
        this.videoView.stopPlayback();
    }

    public void pausePlayback() {
        this.videoView.pause();
    }

    public boolean startPlayback(int i) {
        if (!this.videoView.isPlaying()) {
            this.videoView.requestFocus();
            this.startPosition = i;
            if (VERSION.SDK_INT < 26) {
                this.videoView.seekTo(this.startPosition);
            }
            this.videoView.start();
        }
        return this.videoView.isPlaying();
    }

    public void setImmersiveMode() {
        this.window.getDecorView().setSystemUiVisibility(5894);
    }

    public void updateWindow(boolean z) {
        LayoutParams layoutParams;
        if (z) {
            DisplayMetrics displayMetrics = new DisplayMetrics();
            this.window.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            int i = displayMetrics.heightPixels;
            int i2 = displayMetrics.widthPixels;
            int i3 = getResources().getConfiguration().orientation;
            if (i3 == 1) {
                this.window.setGravity(83);
                Window window2 = this.window;
                double d = (double) i2;
                Double.isNaN(d);
                double d2 = d * NINE_BY_SIXTEEN_ASPECT_RATIO;
                window2.setLayout(i2, (int) Math.round(d2));
                layoutParams = new LayoutParams(i2, (int) d2);
            } else if (i3 == 2) {
                Window window3 = this.window;
                double d3 = (double) i;
                Double.isNaN(d3);
                double d4 = d3 * NINE_BY_SIXTEEN_ASPECT_RATIO;
                window3.setLayout((int) Math.round(d4), i);
                this.window.setGravity(85);
                layoutParams = new LayoutParams((int) Math.round(d4), i);
                layoutParams.addRule(11, -1);
            } else {
                layoutParams = null;
            }
            this.webView.setLayoutParams(layoutParams);
            this.window.addFlags(288);
            return;
        }
        this.window.setFlags(1024, 1024);
        this.window.getDecorView().setBackgroundColor(ViewCompat.MEASURED_STATE_MASK);
    }

    public int getCurrentVideoPosition() {
        return this.videoView.getCurrentPosition();
    }

    public int getVideoDuration() {
        return this.videoView.getDuration();
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onClickProxy = onItemClickListener;
    }

    /* access modifiers changed from: private */
    public int matchView(View view) {
        Integer num = (Integer) this.viewToId.get(view);
        if (num == null) {
            return -1;
        }
        return num.intValue();
    }

    public void setMuted(boolean z) {
        Bitmap bitmap = ViewUtility.getBitmap(Asset.mute, getContext());
        Bitmap bitmap2 = ViewUtility.getBitmap(Asset.unMute, getContext());
        ImageView imageView = this.muteButton;
        if (!z) {
            bitmap = bitmap2;
        }
        imageView.setImageBitmap(bitmap);
    }

    public void playVideo(Uri uri, int i) {
        this.videoViewContainer.setVisibility(0);
        this.videoView.setVideoURI(uri);
        this.privacyOverlay.setImageBitmap(ViewUtility.getBitmap(Asset.privacy, getContext()));
        this.privacyOverlay.setVisibility(0);
        this.progressBar.setVisibility(0);
        this.progressBar.setMax(this.videoView.getDuration());
        startPlayback(i);
    }

    public void setOnPreparedListener(OnPreparedListener onPreparedListener2) {
        this.onPreparedListener = onPreparedListener2;
    }

    public void setOnErrorListener(OnErrorListener onErrorListener2) {
        this.onErrorListener = onErrorListener2;
    }

    public void setOnCompletionListener(OnCompletionListener onCompletionListener2) {
        this.onCompletionListener = onCompletionListener2;
    }

    public void release() {
        this.webView.removeJavascriptInterface(Constants.JAVASCRIPT_INTERFACE_NAME);
        this.webView.loadUrl("about:blank");
        removeView(this.webView);
        this.webView.destroy();
        this.videoView.stopPlayback();
        this.videoView.setOnCompletionListener(null);
        this.videoView.setOnErrorListener(null);
        this.videoView.setOnPreparedListener(null);
        this.videoView.suspend();
    }

    public void pauseWeb() {
        this.webView.onPause();
    }

    public void resumeWeb() {
        this.webView.onResume();
    }

    public String getUrl() {
        return this.webView.getUrl();
    }

    public void showCloseButton(boolean z) {
        this.closeButton.setVisibility(z ? 0 : 8);
    }

    public void setProgress(int i, float f) {
        this.progressBar.setMax((int) f);
        this.progressBar.setProgress(i);
    }
}
