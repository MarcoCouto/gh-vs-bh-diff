package com.vungle.warren.ui.view;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.webkit.WebView;
import android.widget.RelativeLayout.LayoutParams;
import com.ironsource.sdk.constants.Constants;
import com.vungle.warren.AdvertisementPresentationFactory;
import com.vungle.warren.AdvertisementPresentationFactory.ViewCallback;
import com.vungle.warren.DirectDownloadAdapter;
import com.vungle.warren.VungleNativeAd;
import com.vungle.warren.error.VungleException;
import com.vungle.warren.ui.CloseDelegate;
import com.vungle.warren.ui.JavascriptBridge;
import com.vungle.warren.ui.OrientationDelegate;
import com.vungle.warren.ui.contract.AdContract.AdvertisementBus;
import com.vungle.warren.ui.contract.AdContract.AdvertisementPresenter.EventListener;
import com.vungle.warren.ui.contract.WebAdContract.WebAdPresenter;
import com.vungle.warren.ui.contract.WebAdContract.WebAdView;
import com.vungle.warren.utility.ExternalRouter;
import java.util.concurrent.atomic.AtomicReference;

public class VungleNativeView extends WebView implements WebAdView, VungleNativeAd {
    private static final String TAG = "com.vungle.warren.ui.view.VungleNativeView";
    private BroadcastReceiver broadcastReceiver;
    private final DirectDownloadAdapter directDownloadAdapter;
    /* access modifiers changed from: private */
    public AtomicReference<Boolean> isAdVisible = new AtomicReference<>();
    /* access modifiers changed from: private */
    @NonNull
    public final EventListener listener;
    /* access modifiers changed from: private */
    public final String placementID;
    /* access modifiers changed from: private */
    public WebAdPresenter presenter;
    AdvertisementPresentationFactory presenterFactory;

    public View renderNativeView() {
        return this;
    }

    public void setImmersiveMode() {
    }

    public void setOrientation(int i) {
    }

    public void setPresenter(@NonNull WebAdPresenter webAdPresenter) {
    }

    public void updateWindow(boolean z) {
    }

    public void onResume() {
        super.onResume();
        Log.d(TAG, "Resuming Flex");
        setAdVisibility(true);
    }

    public void onPause() {
        super.onPause();
        setAdVisibility(false);
    }

    public VungleNativeView(Context context, String str, DirectDownloadAdapter directDownloadAdapter2, @NonNull EventListener eventListener) {
        super(context);
        this.listener = eventListener;
        this.placementID = str;
        this.directDownloadAdapter = directDownloadAdapter2;
        try {
            this.presenterFactory = new AdvertisementPresentationFactory(str, null, new OrientationDelegate() {
                public void setOrientation(int i) {
                }
            }, new CloseDelegate() {
                public void close() {
                    VungleNativeView.this.finishDisplayingAd();
                }
            });
        } catch (InstantiationException e) {
            String str2 = TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("error on creating presentation factory");
            sb.append(e.getLocalizedMessage());
            Log.e(str2, sb.toString());
        }
        setLayerType(2, null);
        if (directDownloadAdapter2 != null) {
            directDownloadAdapter2.getSdkDownloadClient().setAdWebView(this);
        }
    }

    /* access modifiers changed from: private */
    public void prepare(Bundle bundle) {
        WebSettingsUtils.applyDefault(this);
        addJavascriptInterface(new JavascriptBridge(this.presenter), Constants.JAVASCRIPT_INTERFACE_NAME);
        setLayoutParams(new LayoutParams(-1, -1));
        if (VERSION.SDK_INT < 17) {
            setAdVisibility(true);
        } else {
            getSettings().setMediaPlaybackRequiresUserGesture(false);
        }
    }

    public void finishDisplayingAd() {
        if (this.presenter != null) {
            this.presenter.stop(false, true);
        } else if (this.presenterFactory != null) {
            this.presenterFactory.destroy();
            this.presenterFactory = null;
            this.listener.onError(new VungleException(25), this.placementID);
        }
        destroyAdView();
    }

    public void setAdVisibility(boolean z) {
        if (this.presenter != null) {
            this.presenter.setAdVisibility(z);
        } else {
            this.isAdVisible.set(Boolean.valueOf(z));
        }
    }

    public void showWebsite(@NonNull String str) {
        loadUrl(str);
    }

    public String getWebsiteUrl() {
        return getUrl();
    }

    public void close() {
        if (this.presenter != null) {
            if (this.presenter.handleExit(null)) {
                finishDisplayingAd();
            }
        } else if (this.presenterFactory != null) {
            this.presenterFactory.destroy();
            this.presenterFactory = null;
            this.listener.onError(new VungleException(25), this.placementID);
        }
    }

    public void destroyAdView() {
        removeJavascriptInterface(Constants.JAVASCRIPT_INTERFACE_NAME);
        loadUrl("about:blank");
    }

    public void showCloseButton() {
        throw new UnsupportedOperationException("VungleNativeView does not implement a close button");
    }

    public void open(@NonNull String str) {
        String str2 = TAG;
        StringBuilder sb = new StringBuilder();
        sb.append("Opening ");
        sb.append(str);
        Log.d(str2, sb.toString());
        if (!ExternalRouter.launch(str, getContext())) {
            String str3 = TAG;
            StringBuilder sb2 = new StringBuilder();
            sb2.append("Cannot open url ");
            sb2.append(str);
            Log.e(str3, sb2.toString());
        }
    }

    public void showDialog(@Nullable String str, @Nullable String str2, @NonNull String str3, @NonNull String str4, @Nullable OnClickListener onClickListener) {
        throw new UnsupportedOperationException("VungleNativeView does not implement a dialog.");
    }

    public void resumeWeb() {
        onResume();
    }

    public void pauseWeb() {
        onPause();
    }

    public void setVisibility(boolean z) {
        setVisibility(z ? 0 : 4);
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.presenterFactory != null && this.presenter == null) {
            this.presenterFactory.getNativeViewPresentation(this.directDownloadAdapter, new ViewCallback() {
                public void onResult(@Nullable Pair<WebAdPresenter, VungleWebClient> pair, @Nullable Exception exc) {
                    if (pair == null || exc != null) {
                        VungleNativeView.this.listener.onError(new VungleException(10), VungleNativeView.this.placementID);
                        return;
                    }
                    VungleNativeView.this.presenter = (WebAdPresenter) pair.first;
                    VungleNativeView.this.setWebViewClient((VungleWebClient) pair.second);
                    VungleNativeView.this.presenter.setEventListener(VungleNativeView.this.listener);
                    VungleNativeView.this.presenter.attach(VungleNativeView.this, null);
                    VungleNativeView.this.prepare(null);
                    if (VungleNativeView.this.isAdVisible.get() != null) {
                        VungleNativeView.this.setAdVisibility(((Boolean) VungleNativeView.this.isAdVisible.get()).booleanValue());
                    }
                }
            });
        }
        this.broadcastReceiver = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                String stringExtra = intent.getStringExtra("command");
                if (((stringExtra.hashCode() == -1884364225 && stringExtra.equals(AdvertisementBus.STOP_ALL)) ? (char) 0 : 65535) == 0) {
                    VungleNativeView.this.finishDisplayingAd();
                }
            }
        };
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(this.broadcastReceiver, new IntentFilter(AdvertisementBus.ACTION));
        resumeWeb();
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(this.broadcastReceiver);
        super.onDetachedFromWindow();
        if (this.presenterFactory != null) {
            this.presenterFactory.destroy();
        }
        pauseWeb();
    }
}
