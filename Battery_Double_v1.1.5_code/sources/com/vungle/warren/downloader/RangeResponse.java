package com.vungle.warren.downloader;

import android.support.annotation.Nullable;
import android.text.TextUtils;

class RangeResponse {
    private static String TAG = "com.vungle.warren.downloader.RangeResponse";
    @Nullable
    public final String dimension;
    public final long rangeEnd;
    public final long rangeStart;
    public final long total;

    /* JADX WARNING: Removed duplicated region for block: B:32:0x0076 A[SYNTHETIC, Splitter:B:32:0x0076] */
    public RangeResponse(String str) {
        long j;
        long j2;
        long j3;
        long j4 = -1;
        String str2 = null;
        if (!TextUtils.isEmpty(str)) {
            String[] split = str.trim().split("\\s+");
            if (split.length >= 2) {
                if (split.length > 0) {
                    str2 = split[0];
                }
                if (split.length > 1 && !TextUtils.isEmpty(split[1])) {
                    String[] split2 = split[1].split("/");
                    if (split2.length == 2) {
                        if (!TextUtils.isEmpty(split2[0])) {
                            String[] split3 = split2[0].split("-");
                            if (split3.length == 2 && !TextUtils.isEmpty(split3[0]) && !TextUtils.isEmpty(split3[1])) {
                                try {
                                    j3 = Long.parseLong(split3[0]);
                                } catch (Throwable unused) {
                                    j3 = -1;
                                }
                                try {
                                    j2 = Long.parseLong(split3[1]);
                                } catch (Throwable unused2) {
                                    j2 = -1;
                                }
                                if (!TextUtils.isEmpty(split2[1])) {
                                    try {
                                        j = Long.parseLong(split2[1]);
                                    } catch (Throwable unused3) {
                                    }
                                    j4 = j3;
                                    this.rangeStart = j4;
                                    this.rangeEnd = j2;
                                    this.total = j;
                                    this.dimension = str2;
                                }
                                j = -1;
                                j4 = j3;
                                this.rangeStart = j4;
                                this.rangeEnd = j2;
                                this.total = j;
                                this.dimension = str2;
                            }
                        }
                        j3 = -1;
                        j2 = -1;
                        if (!TextUtils.isEmpty(split2[1])) {
                        }
                        j = -1;
                        j4 = j3;
                        this.rangeStart = j4;
                        this.rangeEnd = j2;
                        this.total = j;
                        this.dimension = str2;
                    }
                }
            }
        }
        j2 = -1;
        j = -1;
        this.rangeStart = j4;
        this.rangeEnd = j2;
        this.total = j;
        this.dimension = str2;
    }
}
