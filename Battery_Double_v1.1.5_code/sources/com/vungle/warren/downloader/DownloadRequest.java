package com.vungle.warren.downloader;

import android.support.annotation.NonNull;
import android.text.TextUtils;
import com.vungle.warren.downloader.AssetDownloader.NetworkType;
import io.fabric.sdk.android.services.events.EventsFilesManager;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

public class DownloadRequest {
    private AtomicBoolean connectedAtomic;
    public final String cookieString;
    final String id;
    public final int networkType;
    public final String path;
    public final boolean pauseOnConnectionLost;
    public final int priority;
    private AtomicInteger statusAtomic;
    public final String url;

    public @interface Priority {
        public static final int DEFAULT = 1;
        public static final int HIGH = 2;
        public static final int LOW = 0;
    }

    /* access modifiers changed from: 0000 */
    public boolean isConnected() {
        return this.connectedAtomic.get();
    }

    /* access modifiers changed from: protected */
    public void setConnected(boolean z) {
        this.connectedAtomic.set(z);
    }

    public DownloadRequest(@NonNull String str, String str2) {
        this(3, 1, str, str2, false, null);
    }

    public DownloadRequest(@NonNull String str, String str2, String str3) {
        this(3, 1, str, str2, false, str3);
    }

    public DownloadRequest(@NetworkType int i, @Priority int i2, @NonNull String str, @NonNull String str2, boolean z, String str3) {
        this.statusAtomic = new AtomicInteger(0);
        this.connectedAtomic = new AtomicBoolean(true);
        if (TextUtils.isEmpty(str2) || TextUtils.isEmpty(str)) {
            throw new IllegalArgumentException("Url or path is empty");
        }
        this.networkType = i;
        this.priority = i2;
        this.url = str;
        this.path = str2;
        StringBuilder sb = new StringBuilder();
        sb.append(str2);
        sb.append(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
        sb.append(str);
        this.id = UUID.nameUUIDFromBytes(sb.toString().getBytes()).toString();
        this.pauseOnConnectionLost = z;
        this.cookieString = str3;
    }

    @Status
    public int getStatus() {
        return this.statusAtomic.get();
    }

    @Status
    public int getAndSetStatus(int i) {
        return this.statusAtomic.getAndSet(i);
    }

    /* access modifiers changed from: 0000 */
    public boolean is(@Status int i) {
        return this.statusAtomic.get() == i;
    }

    /* access modifiers changed from: 0000 */
    public void set(@Status int i) {
        this.statusAtomic.set(i);
    }
}
