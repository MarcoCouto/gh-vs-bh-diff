package com.vungle.warren.downloader;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.media.session.PlaybackStateCompat;
import android.text.TextUtils;
import android.util.Log;
import com.vungle.warren.downloader.AssetDownloadListener.DownloadError;
import com.vungle.warren.downloader.AssetDownloadListener.Progress;
import com.vungle.warren.utility.FileUtility;
import com.vungle.warren.utility.NetworkProvider;
import com.vungle.warren.utility.NetworkProvider.NetworkListener;
import com.vungle.warren.utility.PriorityRunnable;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.io.File;
import java.io.IOException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import javax.net.ssl.SSLException;
import okhttp3.Headers;
import okhttp3.OkHttpClient;
import okhttp3.OkHttpClient.Builder;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okhttp3.internal.http.HttpHeaders;
import okhttp3.internal.http.RealResponseBody;
import okio.GzipSource;
import okio.Okio;
import okio.Sink;
import okio.Source;

@SuppressLint({"LogNotTimber"})
public class AssetDownloader {
    private static final String ACCEPT_ENCODING = "Accept-Encoding";
    private static final String ACCEPT_RANGES = "Accept-Ranges";
    private static final String BYTES = "bytes";
    private static final int CONNECTION_RETRY_TIMEOUT = 300;
    private static final String CONTENT_ENCODING = "Content-Encoding";
    private static final String CONTENT_RANGE = "Content-Range";
    private static final String CONTENT_TYPE = "Content-Type";
    private static final int DOWNLOAD_CHUNK_SIZE = 2048;
    private static final String ETAG = "ETag";
    private static final String GZIP = "gzip";
    private static final String IDENTITY = "identity";
    private static final String IF_RANGE = "If-Range";
    private static final String LAST_MODIFIED = "Last-Modified";
    private static final long MAX_PERCENT = 100;
    private static final int MAX_RECONNECT_ATTEMPTS = 10;
    private static final String META_POSTFIX_EXT = ".vng_meta";
    private static final int PROGRESS_STEP = 5;
    private static final String RANGE = "Range";
    private static final int RANGE_NOT_SATISFIABLE = 416;
    private static final int RETRY_COUNT_ON_CONNECTION_LOST = 5;
    /* access modifiers changed from: private */
    public static final String TAG = "AssetDownloader";
    private static final int TIMEOUT = 30;
    /* access modifiers changed from: private */
    public final ConcurrentHashMap<String, DownloadRequest> connections = new ConcurrentHashMap<>();
    private final ExecutorService downloadExecutor;
    /* access modifiers changed from: private */
    public final ConcurrentHashMap<String, AssetDownloadListener> listeners = new ConcurrentHashMap<>();
    int maxReconnectAttempts = 10;
    /* access modifiers changed from: private */
    public final NetworkListener networkListener = new NetworkListener() {
        public void onChanged(int i) {
            String access$100 = AssetDownloader.TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("Network changed: ");
            sb.append(i);
            Log.d(access$100, sb.toString());
            AssetDownloader.this.onNetworkChanged(i);
        }
    };
    /* access modifiers changed from: private */
    public final NetworkProvider networkProvider;
    /* access modifiers changed from: private */
    public final OkHttpClient okHttpClient;
    /* access modifiers changed from: private */
    public volatile int progressStep = 5;
    int reconnectTimeout = 300;
    int retryCountOnConnectionLost = 5;
    private final ExecutorService uiExecutor;

    public @interface NetworkType {
        public static final int ANY = 3;
        public static final int CELLULAR = 1;
        public static final int WIFI = 2;
    }

    private static abstract class DownloadPriorityRunnable extends PriorityRunnable {
        private final int priority;

        private DownloadPriorityRunnable(int i) {
            this.priority = i;
        }

        public Integer getPriority() {
            return Integer.valueOf(this.priority);
        }
    }

    private static class RequestException extends Exception {
        RequestException(String str) {
            super(str);
        }
    }

    public AssetDownloader(int i, NetworkProvider networkProvider2, ExecutorService executorService) {
        int max = Math.max(i, 1);
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(max, max, 1, TimeUnit.SECONDS, new PriorityBlockingQueue());
        threadPoolExecutor.allowCoreThreadTimeOut(true);
        this.downloadExecutor = threadPoolExecutor;
        this.networkProvider = networkProvider2;
        this.uiExecutor = executorService;
        this.okHttpClient = new Builder().readTimeout(30, TimeUnit.SECONDS).connectTimeout(30, TimeUnit.SECONDS).cache(null).followRedirects(true).followSslRedirects(true).build();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0042, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0077, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0010, code lost:
        return;
     */
    public synchronized void download(final DownloadRequest downloadRequest, final AssetDownloadListener assetDownloadListener) {
        if (downloadRequest != null) {
            DownloadRequest downloadRequest2 = (DownloadRequest) this.connections.get(downloadRequest.id);
            if (downloadRequest2 != null) {
                String str = TAG;
                StringBuilder sb = new StringBuilder();
                sb.append("Request with same url and path already present ");
                sb.append(downloadRequest2.id);
                Log.d(str, sb.toString());
                if (assetDownloadListener != null) {
                    this.uiExecutor.execute(new Runnable() {
                        public void run() {
                            assetDownloadListener.onError(new DownloadError(-1, new IllegalArgumentException("Request with same url and path already present"), 1), downloadRequest);
                        }
                    });
                }
            } else {
                for (DownloadRequest downloadRequest3 : this.connections.values()) {
                    if (downloadRequest3.path.equals(downloadRequest.path)) {
                        Log.d(TAG, "Already present request for same path");
                        if (assetDownloadListener != null) {
                            this.uiExecutor.execute(new Runnable() {
                                public void run() {
                                    assetDownloadListener.onError(new DownloadError(-1, new IllegalArgumentException("Already present request for same path"), 1), downloadRequest);
                                }
                            });
                        }
                    }
                }
                this.listeners.put(downloadRequest.id, assetDownloadListener);
                this.connections.put(downloadRequest.id, downloadRequest);
                this.networkProvider.addListener(this.networkListener);
                load(downloadRequest);
            }
        } else if (assetDownloadListener != null) {
            this.uiExecutor.execute(new Runnable() {
                public void run() {
                    assetDownloadListener.onError(new DownloadError(-1, new IllegalArgumentException("DownloadRequest is null"), 1), null);
                }
            });
        }
    }

    public List<DownloadRequest> getAllRequests() {
        return new ArrayList(this.connections.values());
    }

    private synchronized void load(final DownloadRequest downloadRequest) {
        downloadRequest.set(1);
        this.downloadExecutor.execute(new DownloadPriorityRunnable(downloadRequest.priority) {
            /* JADX INFO: finally extract failed */
            /* JADX WARNING: type inference failed for: r3v1 */
            /* JADX WARNING: type inference failed for: r3v2, types: [java.io.Closeable, java.io.File] */
            /* JADX WARNING: type inference failed for: r3v3 */
            /* JADX WARNING: type inference failed for: r22v0, types: [java.io.Closeable] */
            /* JADX WARNING: type inference failed for: r19v0, types: [java.io.Closeable] */
            /* JADX WARNING: type inference failed for: r9v2, types: [okhttp3.Response] */
            /* JADX WARNING: type inference failed for: r8v3, types: [java.io.File] */
            /* JADX WARNING: type inference failed for: r5v5, types: [okhttp3.Call] */
            /* JADX WARNING: type inference failed for: r22v1 */
            /* JADX WARNING: type inference failed for: r19v1 */
            /* JADX WARNING: type inference failed for: r9v4 */
            /* JADX WARNING: type inference failed for: r8v4 */
            /* JADX WARNING: type inference failed for: r5v23 */
            /* JADX WARNING: type inference failed for: r3v6 */
            /* JADX WARNING: type inference failed for: r19v2, types: [java.io.Closeable] */
            /* JADX WARNING: type inference failed for: r9v5, types: [okhttp3.Response] */
            /* JADX WARNING: type inference failed for: r8v6, types: [java.io.File] */
            /* JADX WARNING: type inference failed for: r7v9, types: [java.io.Closeable] */
            /* JADX WARNING: type inference failed for: r5v25, types: [okhttp3.Call] */
            /* JADX WARNING: type inference failed for: r22v2 */
            /* JADX WARNING: type inference failed for: r22v3 */
            /* JADX WARNING: type inference failed for: r22v4 */
            /* JADX WARNING: type inference failed for: r22v5 */
            /* JADX WARNING: type inference failed for: r9v9 */
            /* JADX WARNING: type inference failed for: r8v14 */
            /* JADX WARNING: type inference failed for: r7v11 */
            /* JADX WARNING: type inference failed for: r5v45 */
            /* JADX WARNING: type inference failed for: r19v3 */
            /* JADX WARNING: type inference failed for: r9v10 */
            /* JADX WARNING: type inference failed for: r8v15 */
            /* JADX WARNING: type inference failed for: r7v12 */
            /* JADX WARNING: type inference failed for: r5v46 */
            /* JADX WARNING: type inference failed for: r8v16 */
            /* JADX WARNING: type inference failed for: r7v13 */
            /* JADX WARNING: type inference failed for: r5v47 */
            /* JADX WARNING: type inference failed for: r9v11 */
            /* JADX WARNING: type inference failed for: r5v48 */
            /* JADX WARNING: type inference failed for: r7v14 */
            /* JADX WARNING: type inference failed for: r8v17 */
            /* JADX WARNING: type inference failed for: r19v4 */
            /* JADX WARNING: type inference failed for: r9v12 */
            /* JADX WARNING: type inference failed for: r8v18 */
            /* JADX WARNING: type inference failed for: r5v49 */
            /* JADX WARNING: type inference failed for: r22v6 */
            /* JADX WARNING: type inference failed for: r9v13 */
            /* JADX WARNING: type inference failed for: r8v19 */
            /* JADX WARNING: type inference failed for: r5v50 */
            /* JADX WARNING: type inference failed for: r19v5 */
            /* JADX WARNING: type inference failed for: r8v20 */
            /* JADX WARNING: type inference failed for: r5v51 */
            /* JADX WARNING: type inference failed for: r9v14 */
            /* JADX WARNING: type inference failed for: r5v52 */
            /* JADX WARNING: type inference failed for: r8v21 */
            /* JADX WARNING: type inference failed for: r13v18, types: [java.io.File] */
            /* JADX WARNING: type inference failed for: r8v22 */
            /* JADX WARNING: type inference failed for: r5v53 */
            /* JADX WARNING: type inference failed for: r7v15 */
            /* JADX WARNING: type inference failed for: r8v23 */
            /* JADX WARNING: type inference failed for: r5v54 */
            /* JADX WARNING: type inference failed for: r8v24 */
            /* JADX WARNING: type inference failed for: r8v25 */
            /* JADX WARNING: type inference failed for: r5v55 */
            /* JADX WARNING: type inference failed for: r8v26 */
            /* JADX WARNING: type inference failed for: r5v59, types: [okhttp3.Call] */
            /* JADX WARNING: type inference failed for: r8v27 */
            /* JADX WARNING: type inference failed for: r8v28 */
            /* JADX WARNING: type inference failed for: r9v25, types: [okhttp3.Response] */
            /* JADX WARNING: type inference failed for: r8v29 */
            /* JADX WARNING: type inference failed for: r7v16 */
            /* JADX WARNING: type inference failed for: r8v30 */
            /* JADX WARNING: type inference failed for: r8v31 */
            /* JADX WARNING: type inference failed for: r8v32 */
            /* JADX WARNING: type inference failed for: r7v17 */
            /* JADX WARNING: type inference failed for: r8v33 */
            /* JADX WARNING: type inference failed for: r8v34 */
            /* JADX WARNING: type inference failed for: r11v22 */
            /* JADX WARNING: type inference failed for: r11v23 */
            /* JADX WARNING: type inference failed for: r7v19 */
            /* JADX WARNING: type inference failed for: r19v6 */
            /* JADX WARNING: type inference failed for: r8v35 */
            /* JADX WARNING: type inference failed for: r11v24 */
            /* JADX WARNING: type inference failed for: r19v7 */
            /* JADX WARNING: type inference failed for: r22v7 */
            /* JADX WARNING: type inference failed for: r8v36 */
            /* JADX WARNING: type inference failed for: r11v25 */
            /* JADX WARNING: type inference failed for: r11v26 */
            /* JADX WARNING: type inference failed for: r11v27 */
            /* JADX WARNING: type inference failed for: r7v20 */
            /* JADX WARNING: type inference failed for: r19v8 */
            /* JADX WARNING: type inference failed for: r8v37 */
            /* JADX WARNING: type inference failed for: r11v28 */
            /* JADX WARNING: type inference failed for: r11v29 */
            /* JADX WARNING: type inference failed for: r3v47 */
            /* JADX WARNING: type inference failed for: r11v32 */
            /* JADX WARNING: type inference failed for: r8v48 */
            /* JADX WARNING: type inference failed for: r8v49 */
            /* JADX WARNING: type inference failed for: r8v51 */
            /* JADX WARNING: type inference failed for: r8v52 */
            /* JADX WARNING: type inference failed for: r8v55 */
            /* JADX WARNING: type inference failed for: r10v23, types: [okio.BufferedSource, java.io.Closeable] */
            /* JADX WARNING: type inference failed for: r19v9 */
            /* JADX WARNING: type inference failed for: r8v56 */
            /* JADX WARNING: type inference failed for: r7v25 */
            /* JADX WARNING: type inference failed for: r8v57 */
            /* JADX WARNING: type inference failed for: r19v10 */
            /* JADX WARNING: type inference failed for: r8v58 */
            /* JADX WARNING: type inference failed for: r19v11 */
            /* JADX WARNING: type inference failed for: r7v33, types: [okio.BufferedSink, java.io.Closeable] */
            /* JADX WARNING: type inference failed for: r8v59 */
            /* JADX WARNING: type inference failed for: r19v12 */
            /* JADX WARNING: type inference failed for: r8v60 */
            /* JADX WARNING: type inference failed for: r8v61 */
            /* JADX WARNING: type inference failed for: r22v8 */
            /* JADX WARNING: type inference failed for: r19v13 */
            /* JADX WARNING: type inference failed for: r8v62 */
            /* JADX WARNING: type inference failed for: r13v19 */
            /* JADX WARNING: type inference failed for: r27v0 */
            /* JADX WARNING: type inference failed for: r27v1 */
            /* JADX WARNING: type inference failed for: r8v67 */
            /* JADX WARNING: type inference failed for: r8v68 */
            /* JADX WARNING: type inference failed for: r8v74, types: [java.io.File] */
            /* JADX WARNING: type inference failed for: r27v2 */
            /* JADX WARNING: type inference failed for: r19v14 */
            /* JADX WARNING: type inference failed for: r8v78 */
            /* JADX WARNING: type inference failed for: r27v3 */
            /* JADX WARNING: type inference failed for: r22v9 */
            /* JADX WARNING: type inference failed for: r19v15 */
            /* JADX WARNING: type inference failed for: r8v79 */
            /* JADX WARNING: type inference failed for: r27v4 */
            /* JADX WARNING: type inference failed for: r27v5 */
            /* JADX WARNING: type inference failed for: r27v6 */
            /* JADX WARNING: type inference failed for: r27v7 */
            /* JADX WARNING: type inference failed for: r27v8 */
            /* JADX WARNING: type inference failed for: r13v27 */
            /* JADX WARNING: type inference failed for: r19v17 */
            /* JADX WARNING: type inference failed for: r8v87 */
            /* JADX WARNING: type inference failed for: r19v18 */
            /* JADX WARNING: type inference failed for: r8v88 */
            /* JADX WARNING: type inference failed for: r8v90 */
            /* JADX WARNING: type inference failed for: r8v94 */
            /* JADX WARNING: type inference failed for: r8v95 */
            /* JADX WARNING: type inference failed for: r8v97 */
            /* JADX WARNING: type inference failed for: r8v98 */
            /* JADX WARNING: type inference failed for: r5v91 */
            /* JADX WARNING: type inference failed for: r9v36 */
            /* JADX WARNING: type inference failed for: r19v19 */
            /* JADX WARNING: type inference failed for: r8v99 */
            /* JADX WARNING: type inference failed for: r7v37 */
            /* JADX WARNING: type inference failed for: r5v92 */
            /* JADX WARNING: type inference failed for: r9v37 */
            /* JADX WARNING: type inference failed for: r19v20 */
            /* JADX WARNING: type inference failed for: r22v10 */
            /* JADX WARNING: type inference failed for: r8v100 */
            /* JADX WARNING: type inference failed for: r22v11 */
            /* JADX WARNING: type inference failed for: r19v21 */
            /* JADX WARNING: type inference failed for: r9v41 */
            /* JADX WARNING: type inference failed for: r8v101 */
            /* JADX WARNING: type inference failed for: r5v93 */
            /* JADX WARNING: type inference failed for: r5v94 */
            /* JADX WARNING: type inference failed for: r9v42 */
            /* JADX WARNING: type inference failed for: r19v22 */
            /* JADX WARNING: type inference failed for: r7v38 */
            /* JADX WARNING: type inference failed for: r8v102 */
            /* JADX WARNING: type inference failed for: r5v95 */
            /* JADX WARNING: type inference failed for: r8v103 */
            /* JADX WARNING: type inference failed for: r9v43 */
            /* JADX WARNING: type inference failed for: r19v23 */
            /* JADX WARNING: type inference failed for: r22v12 */
            /* JADX WARNING: type inference failed for: r3v91 */
            /* JADX WARNING: type inference failed for: r3v92 */
            /* JADX WARNING: type inference failed for: r22v13 */
            /* JADX WARNING: type inference failed for: r19v24 */
            /* JADX WARNING: type inference failed for: r9v47 */
            /* JADX WARNING: type inference failed for: r8v107 */
            /* JADX WARNING: type inference failed for: r5v112 */
            /* JADX WARNING: type inference failed for: r3v93 */
            /* JADX WARNING: type inference failed for: r19v25 */
            /* JADX WARNING: type inference failed for: r19v26 */
            /* JADX WARNING: type inference failed for: r19v27 */
            /* JADX WARNING: type inference failed for: r19v28 */
            /* JADX WARNING: type inference failed for: r9v48 */
            /* JADX WARNING: type inference failed for: r9v49 */
            /* JADX WARNING: type inference failed for: r9v50 */
            /* JADX WARNING: type inference failed for: r9v51 */
            /* JADX WARNING: type inference failed for: r8v108 */
            /* JADX WARNING: type inference failed for: r8v109 */
            /* JADX WARNING: type inference failed for: r8v110 */
            /* JADX WARNING: type inference failed for: r8v111 */
            /* JADX WARNING: type inference failed for: r5v113 */
            /* JADX WARNING: type inference failed for: r5v114 */
            /* JADX WARNING: type inference failed for: r5v115 */
            /* JADX WARNING: type inference failed for: r5v116 */
            /* JADX WARNING: type inference failed for: r9v52 */
            /* JADX WARNING: type inference failed for: r8v112 */
            /* JADX WARNING: type inference failed for: r7v40 */
            /* JADX WARNING: type inference failed for: r5v117 */
            /* JADX WARNING: type inference failed for: r19v29 */
            /* JADX WARNING: type inference failed for: r9v53 */
            /* JADX WARNING: type inference failed for: r8v113 */
            /* JADX WARNING: type inference failed for: r7v41 */
            /* JADX WARNING: type inference failed for: r5v118 */
            /* JADX WARNING: type inference failed for: r8v114 */
            /* JADX WARNING: type inference failed for: r7v42 */
            /* JADX WARNING: type inference failed for: r5v119 */
            /* JADX WARNING: type inference failed for: r9v54 */
            /* JADX WARNING: type inference failed for: r5v120 */
            /* JADX WARNING: type inference failed for: r7v43 */
            /* JADX WARNING: type inference failed for: r8v115 */
            /* JADX WARNING: type inference failed for: r19v30 */
            /* JADX WARNING: type inference failed for: r9v55 */
            /* JADX WARNING: type inference failed for: r8v116 */
            /* JADX WARNING: type inference failed for: r5v121 */
            /* JADX WARNING: type inference failed for: r22v14 */
            /* JADX WARNING: type inference failed for: r9v56 */
            /* JADX WARNING: type inference failed for: r8v117 */
            /* JADX WARNING: type inference failed for: r5v122 */
            /* JADX WARNING: type inference failed for: r19v31 */
            /* JADX WARNING: type inference failed for: r8v118 */
            /* JADX WARNING: type inference failed for: r5v123 */
            /* JADX WARNING: type inference failed for: r9v57 */
            /* JADX WARNING: type inference failed for: r5v124 */
            /* JADX WARNING: type inference failed for: r8v119 */
            /* JADX WARNING: type inference failed for: r13v37 */
            /* JADX WARNING: type inference failed for: r8v120 */
            /* JADX WARNING: type inference failed for: r5v125 */
            /* JADX WARNING: type inference failed for: r7v44 */
            /* JADX WARNING: type inference failed for: r8v121 */
            /* JADX WARNING: type inference failed for: r5v126 */
            /* JADX WARNING: type inference failed for: r8v122 */
            /* JADX WARNING: type inference failed for: r5v127 */
            /* JADX WARNING: type inference failed for: r5v128 */
            /* JADX WARNING: type inference failed for: r5v129 */
            /* JADX WARNING: type inference failed for: r5v130 */
            /* JADX WARNING: type inference failed for: r5v131 */
            /* JADX WARNING: type inference failed for: r5v132 */
            /* JADX WARNING: type inference failed for: r5v133 */
            /* JADX WARNING: type inference failed for: r5v134 */
            /* JADX WARNING: type inference failed for: r5v135 */
            /* JADX WARNING: type inference failed for: r5v136 */
            /* JADX WARNING: type inference failed for: r5v137 */
            /* JADX WARNING: type inference failed for: r5v138 */
            /* JADX WARNING: type inference failed for: r5v139 */
            /* JADX WARNING: type inference failed for: r5v140 */
            /* JADX WARNING: type inference failed for: r5v141 */
            /* JADX WARNING: type inference failed for: r5v142 */
            /* JADX WARNING: type inference failed for: r5v143 */
            /* JADX WARNING: type inference failed for: r9v58 */
            /* JADX WARNING: type inference failed for: r9v59 */
            /* JADX WARNING: type inference failed for: r9v60 */
            /* JADX WARNING: type inference failed for: r9v61 */
            /* JADX WARNING: type inference failed for: r9v62 */
            /* JADX WARNING: type inference failed for: r9v63 */
            /* JADX WARNING: type inference failed for: r9v64 */
            /* JADX WARNING: type inference failed for: r9v65 */
            /* JADX WARNING: type inference failed for: r9v66 */
            /* JADX WARNING: type inference failed for: r9v67 */
            /* JADX WARNING: type inference failed for: r9v68 */
            /* JADX WARNING: type inference failed for: r9v69 */
            /* JADX WARNING: type inference failed for: r9v70 */
            /* JADX WARNING: type inference failed for: r9v71 */
            /* JADX WARNING: type inference failed for: r7v45 */
            /* JADX WARNING: type inference failed for: r8v123 */
            /* JADX WARNING: type inference failed for: r8v124 */
            /* JADX WARNING: type inference failed for: r7v46 */
            /* JADX WARNING: type inference failed for: r8v125 */
            /* JADX WARNING: type inference failed for: r11v56 */
            /* JADX WARNING: type inference failed for: r7v47 */
            /* JADX WARNING: type inference failed for: r19v32 */
            /* JADX WARNING: type inference failed for: r11v57 */
            /* JADX WARNING: type inference failed for: r11v58 */
            /* JADX WARNING: type inference failed for: r7v48 */
            /* JADX WARNING: type inference failed for: r11v59 */
            /* JADX WARNING: type inference failed for: r11v60 */
            /* JADX WARNING: type inference failed for: r11v61 */
            /* JADX WARNING: type inference failed for: r11v62 */
            /* JADX WARNING: type inference failed for: r11v63 */
            /* JADX WARNING: type inference failed for: r8v126 */
            /* JADX WARNING: type inference failed for: r8v127 */
            /* JADX WARNING: type inference failed for: r8v128 */
            /* JADX WARNING: type inference failed for: r8v129 */
            /* JADX WARNING: type inference failed for: r8v130 */
            /* JADX WARNING: type inference failed for: r8v131 */
            /* JADX WARNING: type inference failed for: r8v132 */
            /* JADX WARNING: type inference failed for: r8v133 */
            /* JADX WARNING: type inference failed for: r19v33 */
            /* JADX WARNING: type inference failed for: r8v134 */
            /* JADX WARNING: type inference failed for: r7v49 */
            /* JADX WARNING: type inference failed for: r7v50 */
            /* JADX WARNING: type inference failed for: r7v51 */
            /* JADX WARNING: type inference failed for: r8v135 */
            /* JADX WARNING: type inference failed for: r8v136 */
            /* JADX WARNING: type inference failed for: r27v9 */
            /* JADX WARNING: type inference failed for: r27v10 */
            /* JADX WARNING: type inference failed for: r27v11 */
            /* JADX WARNING: type inference failed for: r27v12 */
            /* JADX WARNING: type inference failed for: r27v13 */
            /* JADX WARNING: type inference failed for: r27v14 */
            /* JADX WARNING: type inference failed for: r27v15 */
            /* JADX WARNING: type inference failed for: r27v16 */
            /* JADX WARNING: type inference failed for: r5v144 */
            /* JADX WARNING: type inference failed for: r9v72 */
            /* JADX WARNING: type inference failed for: r19v34 */
            /* JADX WARNING: type inference failed for: r5v145 */
            /* JADX WARNING: type inference failed for: r9v73 */
            /* JADX WARNING: type inference failed for: r19v35 */
            /* JADX WARNING: type inference failed for: r22v15 */
            /* JADX WARNING: type inference failed for: r19v36 */
            /* JADX WARNING: type inference failed for: r9v74 */
            /* JADX WARNING: type inference failed for: r8v137 */
            /* JADX WARNING: type inference failed for: r5v146 */
            /* JADX WARNING: type inference failed for: r5v147 */
            /* JADX WARNING: type inference failed for: r9v75 */
            /* JADX WARNING: type inference failed for: r19v37 */
            /* JADX WARNING: type inference failed for: r7v52 */
            /* JADX WARNING: type inference failed for: r5v148 */
            /* JADX WARNING: type inference failed for: r8v138 */
            /* JADX WARNING: type inference failed for: r9v76 */
            /* JADX WARNING: type inference failed for: r19v38 */
            /* JADX WARNING: Code restructure failed: missing block: B:117:0x036a, code lost:
                r0 = move-exception;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:118:0x036b, code lost:
                r3 = r0;
                r25 = r7;
                r8 = r13;
                r5 = r5;
                r9 = r9;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:125:0x0385, code lost:
                if (com.vungle.warren.downloader.AssetDownloader.access$700(r1.this$0, r9, r3, r4) != false) goto L_0x0387;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:169:0x044e, code lost:
                r0 = th;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:170:0x044f, code lost:
                r11 = 0;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:171:0x0450, code lost:
                r3 = r0;
                r25 = r7;
                r19 = r11;
                r22 = r19;
                r8 = r13;
                r5 = r5;
                r9 = r9;
                r19 = r19;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:186:0x047d, code lost:
                r0 = th;
                r11 = r11;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:187:0x047f, code lost:
                r0 = th;
                r11 = r11;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:200:0x04ab, code lost:
                r0 = move-exception;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:201:0x04ac, code lost:
                r3 = r0;
                r25 = r7;
                r8 = r13;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:284:0x068e, code lost:
                if (r25 != false) goto L_0x06aa;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:285:0x0690, code lost:
                com.vungle.warren.downloader.AssetDownloader.access$1700(r1.this$0).remove(r4.id);
                com.vungle.warren.downloader.AssetDownloader.access$1800(r1.this$0).remove(r4.id);
             */
            /* JADX WARNING: Code restructure failed: missing block: B:286:0x06aa, code lost:
                r4 = com.vungle.warren.downloader.AssetDownloader.access$100();
                r5 = new java.lang.StringBuilder();
                r5.append("Removing connections and listener ");
                r5.append(com.vungle.warren.downloader.AssetDownloader.access$200(r1.this$0, r4));
                android.util.Log.d(r4, r5.toString());
             */
            /* JADX WARNING: Code restructure failed: missing block: B:287:0x06d4, code lost:
                if (com.vungle.warren.downloader.AssetDownloader.access$1700(r1.this$0).isEmpty() == false) goto L_0x06e5;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:288:0x06d6, code lost:
                com.vungle.warren.downloader.AssetDownloader.access$2000(r1.this$0).removeListener(com.vungle.warren.downloader.AssetDownloader.access$1900(r1.this$0));
             */
            /* JADX WARNING: Code restructure failed: missing block: B:320:0x073c, code lost:
                r0 = th;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:321:0x073d, code lost:
                r25 = r7;
                r8 = r13;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:323:0x0746, code lost:
                r0 = th;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:324:0x0747, code lost:
                r25 = r7;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:335:0x0772, code lost:
                r0 = th;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:336:0x0774, code lost:
                r0 = th;
                r8 = r8;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:337:0x0776, code lost:
                r0 = th;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:338:0x0777, code lost:
                r7 = r24;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:339:0x0779, code lost:
                r3 = r0;
                r10 = r7;
                r8 = r8;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:345:0x0789, code lost:
                r0 = th;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:346:0x078a, code lost:
                r25 = r7;
                r8 = r13;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:347:0x078d, code lost:
                r3 = r0;
                r5 = r5;
                r9 = r9;
                r8 = r8;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:383:?, code lost:
                r11 = com.vungle.warren.downloader.AssetDownloader.access$300(r1.this$0, r4);
                r4.setConnected(r11);
             */
            /* JADX WARNING: Code restructure failed: missing block: B:384:0x0817, code lost:
                if (r11 == false) goto L_0x0819;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:385:0x0819, code lost:
                r2.status = 5;
                com.vungle.warren.downloader.AssetDownloader.access$900(r1.this$0, r4.id, r2);
             */
            /* JADX WARNING: Code restructure failed: missing block: B:386:0x082a, code lost:
                if (r4.is(3) == false) goto L_0x082c;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:387:0x082c, code lost:
                r14 = r4 + 1;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:388:0x0832, code lost:
                if (r4 < r1.this$0.maxReconnectAttempts) goto L_0x0834;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:389:0x0834, code lost:
                r4 = 0;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:391:0x0839, code lost:
                if (r4 < r1.this$0.retryCountOnConnectionLost) goto L_0x083b;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:394:0x0844, code lost:
                r28 = r14;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:396:?, code lost:
                java.lang.Thread.sleep((long) java.lang.Math.max(0, r1.this$0.reconnectTimeout));
             */
            /* JADX WARNING: Code restructure failed: missing block: B:397:0x084b, code lost:
                r0 = e;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:398:0x084d, code lost:
                r0 = e;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:399:0x084e, code lost:
                r28 = r14;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:402:?, code lost:
                r0.printStackTrace();
             */
            /* JADX WARNING: Code restructure failed: missing block: B:404:0x085b, code lost:
                if (r4.is(3) == false) goto L_0x085e;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:406:0x085e, code lost:
                android.util.Log.d(com.vungle.warren.downloader.AssetDownloader.access$100(), "Trying to reconnect");
             */
            /* JADX WARNING: Code restructure failed: missing block: B:407:0x086f, code lost:
                if (com.vungle.warren.downloader.AssetDownloader.access$300(r1.this$0, r4) != false) goto L_0x0871;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:408:0x0871, code lost:
                android.util.Log.d(com.vungle.warren.downloader.AssetDownloader.access$100(), "Reconnected, starting download again");
             */
            /* JADX WARNING: Code restructure failed: missing block: B:410:?, code lost:
                r4.setConnected(true);
                r4.set(1);
             */
            /* JADX WARNING: Code restructure failed: missing block: B:411:0x0885, code lost:
                r4 = false;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:412:0x0888, code lost:
                r0 = move-exception;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:413:0x0889, code lost:
                r3 = r0;
                r22 = r7;
                r21 = false;
                r19 = r19;
                r9 = r9;
                r8 = r8;
                r5 = r5;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:415:?, code lost:
                r4.setConnected(false);
             */
            /* JADX WARNING: Code restructure failed: missing block: B:416:0x0896, code lost:
                r4 = r4 + 1;
                r14 = r28;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:417:0x089d, code lost:
                r28 = r14;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:419:0x08a1, code lost:
                r28 = r4;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:420:0x08a4, code lost:
                r4 = true;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:421:0x08a5, code lost:
                if (r4 != false) goto L_0x08a7;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:424:0x08ae, code lost:
                if (r4.is(3) == false) goto L_0x08b0;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:426:0x08b8, code lost:
                if (com.vungle.warren.downloader.AssetDownloader.access$1100(r1.this$0, r4) != false) goto L_0x08ba;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:428:?, code lost:
                r4.set(2);
                com.vungle.warren.downloader.AssetDownloader.access$1200(r1.this$0, r2, r4);
             */
            /* JADX WARNING: Code restructure failed: missing block: B:429:0x08c7, code lost:
                r25 = true;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:430:0x08ca, code lost:
                r0 = move-exception;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:431:0x08cb, code lost:
                r3 = r0;
                r21 = r4;
                r22 = r7;
                r25 = true;
                r19 = r19;
                r9 = r9;
                r8 = r8;
                r5 = r5;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:433:?, code lost:
                r4.set(5);
             */
            /* JADX WARNING: Code restructure failed: missing block: B:434:0x08e5, code lost:
                r6 = new com.vungle.warren.downloader.AssetDownloadListener.DownloadError(r10, r3, com.vungle.warren.downloader.AssetDownloader.access$1300(r1.this$0, r3, r11));
             */
            /* JADX WARNING: Code restructure failed: missing block: B:435:0x08e7, code lost:
                r0 = move-exception;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:436:0x08e8, code lost:
                r3 = r0;
                r21 = r4;
                r22 = r7;
                r19 = r19;
                r9 = r9;
                r8 = r8;
                r5 = r5;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:437:0x08ef, code lost:
                r12 = r4;
                r11 = r6;
                r4 = r28;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:438:0x08f4, code lost:
                r0 = th;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:442:0x08fa, code lost:
                if ((r3 instanceof com.vungle.warren.downloader.AssetDownloader.RequestException) != false) goto L_0x08fc;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:444:0x08fe, code lost:
                r12 = true;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:446:?, code lost:
                r11 = new com.vungle.warren.downloader.AssetDownloadListener.DownloadError(r10, r3, 1);
             */
            /* JADX WARNING: Code restructure failed: missing block: B:447:0x0903, code lost:
                r12 = true;
                r11 = new com.vungle.warren.downloader.AssetDownloadListener.DownloadError(r10, r3, 4);
             */
            /* JADX WARNING: Code restructure failed: missing block: B:453:0x091b, code lost:
                r5.cancel();
             */
            /* JADX WARNING: Code restructure failed: missing block: B:456:0x0941, code lost:
                if (r12 != false) goto L_0x0943;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:459:0x0949, code lost:
                switch(r4.getStatus()) {
                    case 2: goto L_0x0980;
                    case 3: goto L_0x095d;
                    case 4: goto L_0x0955;
                    case 5: goto L_0x094d;
                    default: goto L_0x094c;
                };
             */
            /* JADX WARNING: Code restructure failed: missing block: B:461:0x094d, code lost:
                com.vungle.warren.downloader.AssetDownloader.access$1500(r1.this$0, r11, r4);
             */
            /* JADX WARNING: Code restructure failed: missing block: B:462:0x0955, code lost:
                com.vungle.warren.downloader.AssetDownloader.access$1400(r1.this$0, r8, r4);
             */
            /* JADX WARNING: Code restructure failed: missing block: B:463:0x095d, code lost:
                com.vungle.warren.downloader.AssetDownloader.access$1600(r1.this$0, r4, r2);
             */
            /* JADX WARNING: Code restructure failed: missing block: B:464:0x0964, code lost:
                if (r25 != false) goto L_0x0980;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:465:0x0966, code lost:
                com.vungle.warren.downloader.AssetDownloader.access$1700(r1.this$0).remove(r4.id);
                com.vungle.warren.downloader.AssetDownloader.access$1800(r1.this$0).remove(r4.id);
             */
            /* JADX WARNING: Code restructure failed: missing block: B:466:0x0980, code lost:
                r5 = com.vungle.warren.downloader.AssetDownloader.access$100();
                r6 = new java.lang.StringBuilder();
                r6.append("Removing connections and listener ");
                r6.append(com.vungle.warren.downloader.AssetDownloader.access$200(r1.this$0, r4));
                android.util.Log.d(r5, r6.toString());
             */
            /* JADX WARNING: Code restructure failed: missing block: B:467:0x09a1, code lost:
                r0 = move-exception;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:469:0x09a4, code lost:
                r5 = com.vungle.warren.downloader.AssetDownloader.access$100();
                r6 = new java.lang.StringBuilder();
                r6.append("Not removing connections and listener ");
                r6.append(com.vungle.warren.downloader.AssetDownloader.access$200(r1.this$0, r4));
                android.util.Log.d(r5, r6.toString());
             */
            /* JADX WARNING: Code restructure failed: missing block: B:471:0x09ce, code lost:
                if (com.vungle.warren.downloader.AssetDownloader.access$1700(r1.this$0).isEmpty() != false) goto L_0x09d0;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:472:0x09d0, code lost:
                com.vungle.warren.downloader.AssetDownloader.access$2000(r1.this$0).removeListener(com.vungle.warren.downloader.AssetDownloader.access$1900(r1.this$0));
             */
            /* JADX WARNING: Code restructure failed: missing block: B:478:0x09ef, code lost:
                throw r0;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:488:0x0a08, code lost:
                r5.cancel();
             */
            /* JADX WARNING: Code restructure failed: missing block: B:491:0x0a2e, code lost:
                if (r21 != false) goto L_0x0a30;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:494:0x0a36, code lost:
                switch(r4.getStatus()) {
                    case 2: goto L_0x0a6d;
                    case 3: goto L_0x0a4a;
                    case 4: goto L_0x0a42;
                    case 5: goto L_0x0a3a;
                    default: goto L_0x0a39;
                };
             */
            /* JADX WARNING: Code restructure failed: missing block: B:496:0x0a3a, code lost:
                com.vungle.warren.downloader.AssetDownloader.access$1500(r1.this$0, r6, r4);
             */
            /* JADX WARNING: Code restructure failed: missing block: B:497:0x0a42, code lost:
                com.vungle.warren.downloader.AssetDownloader.access$1400(r1.this$0, r8, r4);
             */
            /* JADX WARNING: Code restructure failed: missing block: B:498:0x0a4a, code lost:
                com.vungle.warren.downloader.AssetDownloader.access$1600(r1.this$0, r4, r2);
             */
            /* JADX WARNING: Code restructure failed: missing block: B:499:0x0a51, code lost:
                if (r25 != false) goto L_0x0a6d;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:500:0x0a53, code lost:
                com.vungle.warren.downloader.AssetDownloader.access$1700(r1.this$0).remove(r4.id);
                com.vungle.warren.downloader.AssetDownloader.access$1800(r1.this$0).remove(r4.id);
             */
            /* JADX WARNING: Code restructure failed: missing block: B:501:0x0a6d, code lost:
                r2 = com.vungle.warren.downloader.AssetDownloader.access$100();
                r5 = new java.lang.StringBuilder();
                r5.append("Removing connections and listener ");
                r5.append(com.vungle.warren.downloader.AssetDownloader.access$200(r1.this$0, r4));
                android.util.Log.d(r2, r5.toString());
             */
            /* JADX WARNING: Code restructure failed: missing block: B:502:0x0a8e, code lost:
                r0 = move-exception;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:504:0x0a91, code lost:
                r2 = com.vungle.warren.downloader.AssetDownloader.access$100();
                r5 = new java.lang.StringBuilder();
                r5.append("Not removing connections and listener ");
                r5.append(com.vungle.warren.downloader.AssetDownloader.access$200(r1.this$0, r4));
                android.util.Log.d(r2, r5.toString());
             */
            /* JADX WARNING: Code restructure failed: missing block: B:506:0x0abb, code lost:
                if (com.vungle.warren.downloader.AssetDownloader.access$1700(r1.this$0).isEmpty() != false) goto L_0x0abd;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:507:0x0abd, code lost:
                com.vungle.warren.downloader.AssetDownloader.access$2000(r1.this$0).removeListener(com.vungle.warren.downloader.AssetDownloader.access$1900(r1.this$0));
             */
            /* JADX WARNING: Code restructure failed: missing block: B:513:0x0ad5, code lost:
                throw r0;
             */
            /* JADX WARNING: Failed to process nested try/catch */
            /* JADX WARNING: Multi-variable type inference failed. Error: jadx.core.utils.exceptions.JadxRuntimeException: No candidate types for var: r3v2, types: [java.io.Closeable, java.io.File]
  assigns: []
  uses: [?[OBJECT, ARRAY], java.io.Closeable, java.io.File]
  mth insns count: 1285
            	at jadx.core.dex.visitors.typeinference.TypeSearch.fillTypeCandidates(TypeSearch.java:237)
            	at java.util.ArrayList.forEach(Unknown Source)
            	at jadx.core.dex.visitors.typeinference.TypeSearch.run(TypeSearch.java:53)
            	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.runMultiVariableSearch(TypeInferenceVisitor.java:99)
            	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.visit(TypeInferenceVisitor.java:92)
            	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:27)
            	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$1(DepthTraversal.java:14)
            	at java.util.ArrayList.forEach(Unknown Source)
            	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
            	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$0(DepthTraversal.java:13)
            	at java.util.ArrayList.forEach(Unknown Source)
            	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:13)
            	at jadx.core.ProcessClass.process(ProcessClass.java:30)
            	at jadx.core.ProcessClass.lambda$processDependencies$0(ProcessClass.java:49)
            	at java.util.ArrayList.forEach(Unknown Source)
            	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:49)
            	at jadx.core.ProcessClass.process(ProcessClass.java:35)
            	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
            	at jadx.api.JavaClass.decompile(JavaClass.java:62)
            	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
             */
            /* JADX WARNING: Removed duplicated region for block: B:117:0x036a A[ExcHandler: all (r0v69 'th' java.lang.Throwable A[CUSTOM_DECLARE]), Splitter:B:87:0x02a8] */
            /* JADX WARNING: Removed duplicated region for block: B:169:0x044e A[ExcHandler: all (th java.lang.Throwable), Splitter:B:133:0x0394] */
            /* JADX WARNING: Removed duplicated region for block: B:186:0x047d A[ExcHandler: all (th java.lang.Throwable), PHI: r11 
  PHI: (r11v22 ?) = (r11v60 ?), (r11v62 ?) binds: [B:162:0x0429, B:181:0x0471] A[DONT_GENERATE, DONT_INLINE], Splitter:B:162:0x0429] */
            /* JADX WARNING: Removed duplicated region for block: B:336:0x0774 A[ExcHandler: all (th java.lang.Throwable), PHI: r8 r25 
  PHI: (r8v48 ?) = (r8v127 ?), (r8v129 ?), (r8v130 ?), (r8v132 ?) binds: [B:329:0x0759, B:330:?, B:332:0x0767, B:316:0x0732] A[DONT_GENERATE, DONT_INLINE]
  PHI: (r25v35 boolean) = (r25v36 boolean), (r25v36 boolean), (r25v36 boolean), (r25v42 boolean) binds: [B:329:0x0759, B:330:?, B:332:0x0767, B:316:0x0732] A[DONT_GENERATE, DONT_INLINE], Splitter:B:316:0x0732] */
            /* JADX WARNING: Removed duplicated region for block: B:345:0x0789 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:188:0x0481] */
            /* JADX WARNING: Removed duplicated region for block: B:382:0x0809 A[SYNTHETIC, Splitter:B:382:0x0809] */
            /* JADX WARNING: Removed duplicated region for block: B:406:0x085e A[Catch:{ all -> 0x08f4 }] */
            /* JADX WARNING: Removed duplicated region for block: B:422:0x08a7 A[SYNTHETIC, Splitter:B:422:0x08a7] */
            /* JADX WARNING: Removed duplicated region for block: B:439:0x08f7  */
            /* JADX WARNING: Removed duplicated region for block: B:453:0x091b  */
            /* JADX WARNING: Removed duplicated region for block: B:456:0x0941  */
            /* JADX WARNING: Removed duplicated region for block: B:488:0x0a08  */
            /* JADX WARNING: Removed duplicated region for block: B:491:0x0a2e  */
            /* JADX WARNING: Removed duplicated region for block: B:541:0x085d A[SYNTHETIC] */
            /* JADX WARNING: Unknown variable types count: 115 */
            public void run() {
                ? r3;
                boolean z;
                ? r22;
                boolean z2;
                ? r19;
                ? r9;
                ? r8;
                ? r5;
                Throwable th;
                ? r222;
                ? r192;
                ? r92;
                ? r82;
                ? r52;
                ? r193;
                int i;
                ? r93;
                ? r83;
                ? r7;
                ? r53;
                int i2;
                Throwable th2;
                Throwable th3;
                ? r223;
                boolean z3;
                DownloadError downloadError;
                ? r94;
                ? r84;
                ? r72;
                ? r54;
                ? r194;
                boolean z4;
                ? r95;
                ? r85;
                ? r73;
                ? r55;
                Throwable th4;
                boolean z5;
                ? r86;
                ? r74;
                ? r56;
                int i3;
                Throwable th5;
                int i4;
                ? r87;
                boolean z6;
                ? r195;
                ? r96;
                ? r88;
                ? r57;
                ? r89;
                ? r58;
                boolean z7;
                ? r810;
                ? r811;
                ? r59;
                ? r812;
                ? execute;
                long access$500;
                ? r813;
                int i5;
                ? r11;
                int i6;
                ? r112;
                ? r814;
                ? r196;
                ? r815;
                ? r816;
                String access$100;
                StringBuilder sb;
                Sink sink;
                ? r817;
                ? r818;
                ? r819;
                ? r820;
                ? r27;
                ? r272;
                ? r273;
                ? r274;
                ? r275;
                ? r821;
                ? r224;
                ? r197;
                ? r97;
                ? r510;
                Progress progress = new Progress();
                progress.timestampDownloadStart = System.currentTimeMillis();
                DownloadError downloadError2 = null;
                boolean z8 = false;
                boolean z9 = false;
                int i7 = 0;
                ? r32 = 0;
                while (!z8) {
                    String str = downloadRequest.url;
                    String str2 = downloadRequest.path;
                    String access$1002 = AssetDownloader.TAG;
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("Start load: ");
                    sb2.append(downloadRequest.id);
                    sb2.append(" url: ");
                    sb2.append(str);
                    Log.d(access$1002, sb2.toString());
                    try {
                        if (!downloadRequest.is(1)) {
                            try {
                                String access$1003 = AssetDownloader.TAG;
                                StringBuilder sb3 = new StringBuilder();
                                sb3.append("Abort download, wrong state ");
                                sb3.append(AssetDownloader.this.debugString(downloadRequest));
                                Log.w(access$1003, sb3.toString());
                                String access$1004 = AssetDownloader.TAG;
                                StringBuilder sb4 = new StringBuilder();
                                sb4.append("request is done ");
                                sb4.append(AssetDownloader.this.debugString(downloadRequest));
                                Log.d(access$1004, sb4.toString());
                                synchronized (AssetDownloader.this) {
                                    try {
                                        switch (downloadRequest.getStatus()) {
                                            case 2:
                                                break;
                                            case 3:
                                                AssetDownloader.this.onCancelled(downloadRequest, progress);
                                                break;
                                            case 4:
                                                AssetDownloader.this.onSuccess(r32, downloadRequest);
                                                break;
                                            case 5:
                                                AssetDownloader.this.onError(downloadError2, downloadRequest);
                                                break;
                                        }
                                        if (!z9) {
                                            AssetDownloader.this.connections.remove(downloadRequest.id);
                                            AssetDownloader.this.listeners.remove(downloadRequest.id);
                                        }
                                        String access$1005 = AssetDownloader.TAG;
                                        StringBuilder sb5 = new StringBuilder();
                                        sb5.append("Removing connections and listener ");
                                        sb5.append(AssetDownloader.this.debugString(downloadRequest));
                                        Log.d(access$1005, sb5.toString());
                                        if (AssetDownloader.this.connections.isEmpty()) {
                                            AssetDownloader.this.networkProvider.removeListener(AssetDownloader.this.networkListener);
                                        }
                                    } catch (Throwable th6) {
                                        while (true) {
                                            throw th6;
                                        }
                                    }
                                }
                                FileUtility.closeQuietly(r32);
                                FileUtility.closeQuietly(r32);
                                return;
                            } catch (Throwable th7) {
                                th = th7;
                                ? r511 = r32;
                                ? r822 = r511;
                                ? r98 = r822;
                                ? r198 = r98;
                                r222 = r198;
                                z = z9;
                                r510 = r511;
                                r821 = r822;
                                r97 = r98;
                                r197 = r198;
                                z2 = true;
                                th = th;
                                r22 = r224;
                                r19 = r197;
                                r9 = r97;
                                r8 = r821;
                                r5 = r510;
                                r9.body().close();
                                if (r5 != 0) {
                                }
                                String access$1006 = AssetDownloader.TAG;
                                StringBuilder sb6 = new StringBuilder();
                                sb6.append("request is done ");
                                sb6.append(AssetDownloader.this.debugString(downloadRequest));
                                Log.d(access$1006, sb6.toString());
                                synchronized (AssetDownloader.this) {
                                }
                                FileUtility.closeQuietly(r22);
                                FileUtility.closeQuietly(r19);
                                throw th;
                            }
                        } else if (AssetDownloader.this.isConnected(downloadRequest)) {
                            downloadRequest.setConnected(true);
                            ? file = new File(str2);
                            try {
                                StringBuilder sb7 = new StringBuilder();
                                sb7.append(str2);
                                sb7.append(AssetDownloader.META_POSTFIX_EXT);
                                File file2 = new File(sb7.toString());
                                if (file.getParentFile() != null) {
                                    try {
                                        if (!file.getParentFile().exists()) {
                                            file.getParentFile().mkdirs();
                                        }
                                    } catch (Throwable th8) {
                                        th = th8;
                                        ? r512 = r32;
                                        ? r99 = r512;
                                        ? r199 = r99;
                                        r222 = r199;
                                        z = z9;
                                        r821 = file;
                                        r510 = r512;
                                        r97 = r99;
                                        r197 = r199;
                                        z2 = true;
                                        th = th;
                                        r22 = r224;
                                        r19 = r197;
                                        r9 = r97;
                                        r8 = r821;
                                        r5 = r510;
                                        r9.body().close();
                                        if (r5 != 0) {
                                        }
                                        String access$10062 = AssetDownloader.TAG;
                                        StringBuilder sb62 = new StringBuilder();
                                        sb62.append("request is done ");
                                        sb62.append(AssetDownloader.this.debugString(downloadRequest));
                                        Log.d(access$10062, sb62.toString());
                                        synchronized (AssetDownloader.this) {
                                        }
                                        FileUtility.closeQuietly(r22);
                                        FileUtility.closeQuietly(r19);
                                        throw th;
                                    }
                                }
                                long length = file.exists() ? file.length() : 0;
                                String access$1007 = AssetDownloader.TAG;
                                StringBuilder sb8 = new StringBuilder();
                                sb8.append("already downloaded : ");
                                sb8.append(length);
                                sb8.append(", file exists = ");
                                sb8.append(file.exists());
                                sb8.append(AssetDownloader.this.debugString(downloadRequest));
                                Log.d(access$1007, sb8.toString());
                                Request.Builder addHeader = new Request.Builder().url(str).addHeader("Accept-Encoding", AssetDownloader.IDENTITY);
                                if (file.exists()) {
                                    try {
                                        if (file2.exists()) {
                                            Map readMap = FileUtility.readMap(file2.getPath());
                                            if (AssetDownloader.BYTES.equalsIgnoreCase((String) readMap.get(AssetDownloader.ACCEPT_RANGES)) && (AssetDownloader.IDENTITY.equalsIgnoreCase((String) readMap.get("Content-Encoding")) || readMap.get("Content-Encoding") == null)) {
                                                String str3 = AssetDownloader.RANGE;
                                                StringBuilder sb9 = new StringBuilder();
                                                sb9.append("bytes=");
                                                sb9.append(length);
                                                sb9.append("-");
                                                addHeader.addHeader(str3, sb9.toString());
                                                String str4 = (String) readMap.get("ETag");
                                                if (str4 == null) {
                                                    str4 = (String) readMap.get("Last-Modified");
                                                }
                                                if (str4 != null) {
                                                    addHeader.addHeader(AssetDownloader.IF_RANGE, str4);
                                                }
                                            }
                                        }
                                    } catch (Throwable th9) {
                                        th = th9;
                                        z7 = z9;
                                        r810 = file;
                                        r89 = r812;
                                        r58 = 0;
                                        ? r823 = r89;
                                        ? r513 = r58;
                                        ? r910 = 0;
                                        r96 = r910;
                                        r88 = r823;
                                        r57 = r513;
                                        r195 = 0;
                                        z2 = true;
                                        r19 = r195;
                                        r9 = r96;
                                        r8 = r88;
                                        r5 = r57;
                                        r22 = 0;
                                        r9.body().close();
                                        if (r5 != 0) {
                                        }
                                        String access$100622 = AssetDownloader.TAG;
                                        StringBuilder sb622 = new StringBuilder();
                                        sb622.append("request is done ");
                                        sb622.append(AssetDownloader.this.debugString(downloadRequest));
                                        Log.d(access$100622, sb622.toString());
                                        synchronized (AssetDownloader.this) {
                                        }
                                        FileUtility.closeQuietly(r22);
                                        FileUtility.closeQuietly(r19);
                                        throw th;
                                    }
                                }
                                ? newCall = AssetDownloader.this.okHttpClient.newCall(addHeader.build());
                                try {
                                    execute = newCall.execute();
                                    try {
                                        access$500 = AssetDownloader.this.getContentLength(execute);
                                        String access$1008 = AssetDownloader.TAG;
                                        StringBuilder sb10 = new StringBuilder();
                                        sb10.append("Response code: ");
                                        sb10.append(execute.code());
                                        sb10.append(" ");
                                        sb10.append(downloadRequest.id);
                                        Log.d(access$1008, sb10.toString());
                                        i = execute.code();
                                    } catch (Throwable th10) {
                                    }
                                } catch (Throwable th11) {
                                    z7 = z9;
                                    r89 = file;
                                    th = th11;
                                    r58 = newCall;
                                    ? r8232 = r89;
                                    ? r5132 = r58;
                                    ? r9102 = 0;
                                    r96 = r9102;
                                    r88 = r8232;
                                    r57 = r5132;
                                    r195 = 0;
                                    z2 = true;
                                    r19 = r195;
                                    r9 = r96;
                                    r8 = r88;
                                    r5 = r57;
                                    r22 = 0;
                                    r9.body().close();
                                    if (r5 != 0) {
                                    }
                                    String access$1006222 = AssetDownloader.TAG;
                                    StringBuilder sb6222 = new StringBuilder();
                                    sb6222.append("request is done ");
                                    sb6222.append(AssetDownloader.this.debugString(downloadRequest));
                                    Log.d(access$1006222, sb6222.toString());
                                    synchronized (AssetDownloader.this) {
                                    }
                                    FileUtility.closeQuietly(r22);
                                    FileUtility.closeQuietly(r19);
                                    throw th;
                                }
                                try {
                                    if (AssetDownloader.this.fullyDownloadedContent(file, execute, downloadRequest)) {
                                        try {
                                            downloadRequest.set(4);
                                            if (!(execute == 0 || execute.body() == null)) {
                                                execute.body().close();
                                            }
                                            if (newCall != 0) {
                                                newCall.cancel();
                                            }
                                            String access$1009 = AssetDownloader.TAG;
                                            StringBuilder sb11 = new StringBuilder();
                                            sb11.append("request is done ");
                                            sb11.append(AssetDownloader.this.debugString(downloadRequest));
                                            Log.d(access$1009, sb11.toString());
                                            synchronized (AssetDownloader.this) {
                                                try {
                                                    switch (downloadRequest.getStatus()) {
                                                        case 2:
                                                            break;
                                                        case 3:
                                                            AssetDownloader.this.onCancelled(downloadRequest, progress);
                                                            break;
                                                        case 4:
                                                            AssetDownloader.this.onSuccess(file, downloadRequest);
                                                            break;
                                                        case 5:
                                                            AssetDownloader.this.onError(downloadError2, downloadRequest);
                                                            break;
                                                    }
                                                    if (!z9) {
                                                        AssetDownloader.this.connections.remove(downloadRequest.id);
                                                        AssetDownloader.this.listeners.remove(downloadRequest.id);
                                                    }
                                                    String access$10010 = AssetDownloader.TAG;
                                                    StringBuilder sb12 = new StringBuilder();
                                                    sb12.append("Removing connections and listener ");
                                                    sb12.append(AssetDownloader.this.debugString(downloadRequest));
                                                    Log.d(access$10010, sb12.toString());
                                                    if (AssetDownloader.this.connections.isEmpty()) {
                                                        AssetDownloader.this.networkProvider.removeListener(AssetDownloader.this.networkListener);
                                                    }
                                                } catch (Throwable th12) {
                                                    while (true) {
                                                        throw th12;
                                                    }
                                                }
                                            }
                                            FileUtility.closeQuietly(null);
                                            FileUtility.closeQuietly(null);
                                            return;
                                        } catch (Throwable th13) {
                                        }
                                    } else {
                                        if (i == 206) {
                                        }
                                        if (i != AssetDownloader.RANGE_NOT_SATISFIABLE) {
                                            ? r113 = 0;
                                            if (execute.isSuccessful()) {
                                                if (i != 206) {
                                                    r11 = r113;
                                                    if (file.exists()) {
                                                        file.delete();
                                                    }
                                                    length = 0;
                                                }
                                                try {
                                                    file2.delete();
                                                    Headers headers = execute.headers();
                                                    String str5 = headers.get("Content-Encoding");
                                                    if (str5 != null) {
                                                        i5 = i7;
                                                        if (!"gzip".equalsIgnoreCase(str5)) {
                                                            if (!AssetDownloader.IDENTITY.equalsIgnoreCase(str5)) {
                                                                throw new IOException("Unknown Content-Encoding");
                                                            }
                                                        }
                                                    } else {
                                                        i5 = i7;
                                                    }
                                                    HashMap hashMap = new HashMap();
                                                    int i8 = i;
                                                    hashMap.put("ETag", headers.get("ETag"));
                                                    hashMap.put("Last-Modified", headers.get("Last-Modified"));
                                                    hashMap.put(AssetDownloader.ACCEPT_RANGES, headers.get(AssetDownloader.ACCEPT_RANGES));
                                                    hashMap.put("Content-Encoding", headers.get("Content-Encoding"));
                                                    FileUtility.writeMap(file2.getPath(), hashMap);
                                                    ResponseBody access$800 = AssetDownloader.this.decodeGzipIfNeeded(execute);
                                                    if (access$800 != null) {
                                                        ? source = access$800.source();
                                                        try {
                                                            access$100 = AssetDownloader.TAG;
                                                            sb = new StringBuilder();
                                                            sb.append("Start download from bytes: ");
                                                            sb.append(length);
                                                            z = z9;
                                                        } catch (Throwable th14) {
                                                            th = th14;
                                                            z = z9;
                                                            r88 = file;
                                                            th = th;
                                                            r195 = source;
                                                            r57 = newCall;
                                                            r96 = execute;
                                                            z2 = true;
                                                            r19 = r195;
                                                            r9 = r96;
                                                            r8 = r88;
                                                            r5 = r57;
                                                            r22 = 0;
                                                            r9.body().close();
                                                            if (r5 != 0) {
                                                            }
                                                            String access$10062222 = AssetDownloader.TAG;
                                                            StringBuilder sb62222 = new StringBuilder();
                                                            sb62222.append("request is done ");
                                                            sb62222.append(AssetDownloader.this.debugString(downloadRequest));
                                                            Log.d(access$10062222, sb62222.toString());
                                                            synchronized (AssetDownloader.this) {
                                                            }
                                                            FileUtility.closeQuietly(r22);
                                                            FileUtility.closeQuietly(r19);
                                                            throw th;
                                                        }
                                                        try {
                                                            sb.append(AssetDownloader.this.debugString(downloadRequest));
                                                            Log.d(access$100, sb.toString());
                                                            long j = access$500 + length;
                                                            String access$10011 = AssetDownloader.TAG;
                                                            StringBuilder sb13 = new StringBuilder();
                                                            sb13.append("final offset = ");
                                                            sb13.append(length);
                                                            Log.d(access$10011, sb13.toString());
                                                            if (length == 0) {
                                                                try {
                                                                    sink = Okio.sink((File) file);
                                                                } catch (Throwable th15) {
                                                                    th = th15;
                                                                    r195 = source;
                                                                    r88 = file;
                                                                    r57 = newCall;
                                                                    r96 = execute;
                                                                    z2 = true;
                                                                    r19 = r195;
                                                                    r9 = r96;
                                                                    r8 = r88;
                                                                    r5 = r57;
                                                                    r22 = 0;
                                                                    r9.body().close();
                                                                    if (r5 != 0) {
                                                                    }
                                                                    String access$100622222 = AssetDownloader.TAG;
                                                                    StringBuilder sb622222 = new StringBuilder();
                                                                    sb622222.append("request is done ");
                                                                    sb622222.append(AssetDownloader.this.debugString(downloadRequest));
                                                                    Log.d(access$100622222, sb622222.toString());
                                                                    synchronized (AssetDownloader.this) {
                                                                    }
                                                                    FileUtility.closeQuietly(r22);
                                                                    FileUtility.closeQuietly(r19);
                                                                    throw th;
                                                                }
                                                            } else {
                                                                sink = Okio.appendingSink(file);
                                                            }
                                                            ? buffer = Okio.buffer(sink);
                                                            try {
                                                                progress.status = 0;
                                                                progress.sizeBytes = access$800.contentLength();
                                                                progress.startBytes = length;
                                                                AssetDownloader.this.deliverProgress(downloadRequest.id, progress);
                                                                int i9 = 0;
                                                                long j2 = 0;
                                                                ? r13 = file;
                                                                while (true) {
                                                                    int i10 = i9;
                                                                    if (downloadRequest.is(1)) {
                                                                        try {
                                                                            r27 = r13;
                                                                        } catch (Throwable th16) {
                                                                            th = th16;
                                                                            r273 = r13;
                                                                            th3 = th;
                                                                            r223 = buffer;
                                                                            r192 = source;
                                                                            r82 = r273;
                                                                            r52 = newCall;
                                                                            r92 = execute;
                                                                            z2 = true;
                                                                            r22 = r222;
                                                                            r19 = r192;
                                                                            r9 = r92;
                                                                            r8 = r82;
                                                                            r5 = r52;
                                                                            r9.body().close();
                                                                            if (r5 != 0) {
                                                                            }
                                                                            String access$1006222222 = AssetDownloader.TAG;
                                                                            StringBuilder sb6222222 = new StringBuilder();
                                                                            sb6222222.append("request is done ");
                                                                            sb6222222.append(AssetDownloader.this.debugString(downloadRequest));
                                                                            Log.d(access$1006222222, sb6222222.toString());
                                                                            synchronized (AssetDownloader.this) {
                                                                            }
                                                                            FileUtility.closeQuietly(r22);
                                                                            FileUtility.closeQuietly(r19);
                                                                            throw th;
                                                                        }
                                                                        try {
                                                                            r274 = r27;
                                                                            r275 = r27;
                                                                            long read = source.read(buffer.buffer(), PlaybackStateCompat.ACTION_PLAY_FROM_SEARCH);
                                                                            if (read != -1) {
                                                                                buffer.emit();
                                                                                j2 += read;
                                                                                i9 = j > 0 ? (int) (((length + j2) * AssetDownloader.MAX_PERCENT) / j) : i10;
                                                                                if (downloadRequest.isConnected()) {
                                                                                    if (i9 > progress.progressPercent) {
                                                                                        progress.progressPercent = i9;
                                                                                        if (progress.progressPercent % AssetDownloader.this.progressStep == 0) {
                                                                                            progress.status = 1;
                                                                                            AssetDownloader.this.deliverProgress(downloadRequest.id, progress);
                                                                                        }
                                                                                    }
                                                                                    r13 = r27;
                                                                                } else {
                                                                                    throw new IOException("Request is not connected");
                                                                                }
                                                                            }
                                                                        } catch (Throwable th17) {
                                                                            th = th17;
                                                                            r273 = r275;
                                                                            th3 = th;
                                                                            r223 = buffer;
                                                                            r192 = source;
                                                                            r82 = r273;
                                                                            r52 = newCall;
                                                                            r92 = execute;
                                                                            z2 = true;
                                                                            r22 = r222;
                                                                            r19 = r192;
                                                                            r9 = r92;
                                                                            r8 = r82;
                                                                            r5 = r52;
                                                                            r9.body().close();
                                                                            if (r5 != 0) {
                                                                            }
                                                                            String access$10062222222 = AssetDownloader.TAG;
                                                                            StringBuilder sb62222222 = new StringBuilder();
                                                                            sb62222222.append("request is done ");
                                                                            sb62222222.append(AssetDownloader.this.debugString(downloadRequest));
                                                                            Log.d(access$10062222222, sb62222222.toString());
                                                                            synchronized (AssetDownloader.this) {
                                                                            }
                                                                            FileUtility.closeQuietly(r22);
                                                                            FileUtility.closeQuietly(r19);
                                                                            throw th;
                                                                        }
                                                                    } else {
                                                                        r27 = r13;
                                                                    }
                                                                }
                                                                try {
                                                                    buffer.flush();
                                                                    if (downloadRequest.is(1)) {
                                                                        r274 = r27;
                                                                        r275 = r27;
                                                                        downloadRequest.set(4);
                                                                        r274 = r27;
                                                                        r275 = r27;
                                                                    } else {
                                                                        progress.status = 6;
                                                                        AssetDownloader.this.deliverProgress(downloadRequest.id, progress);
                                                                        String access$10012 = AssetDownloader.TAG;
                                                                        StringBuilder sb14 = new StringBuilder();
                                                                        sb14.append("State has changed, cancelling download ");
                                                                        sb14.append(AssetDownloader.this.debugString(downloadRequest));
                                                                        Log.d(access$10012, sb14.toString());
                                                                    }
                                                                    if (!(execute == 0 || execute.body() == null)) {
                                                                        execute.body().close();
                                                                    }
                                                                    if (newCall != 0) {
                                                                        newCall.cancel();
                                                                    }
                                                                    String access$10013 = AssetDownloader.TAG;
                                                                    StringBuilder sb15 = new StringBuilder();
                                                                    sb15.append("request is done ");
                                                                    sb15.append(AssetDownloader.this.debugString(downloadRequest));
                                                                    Log.d(access$10013, sb15.toString());
                                                                    synchronized (AssetDownloader.this) {
                                                                        try {
                                                                            switch (downloadRequest.getStatus()) {
                                                                                case 2:
                                                                                    break;
                                                                                case 3:
                                                                                    AssetDownloader.this.onCancelled(downloadRequest, progress);
                                                                                    break;
                                                                                case 4:
                                                                                    AssetDownloader.this.onSuccess(r27, downloadRequest);
                                                                                    break;
                                                                                case 5:
                                                                                    AssetDownloader.this.onError(downloadError2, downloadRequest);
                                                                                    break;
                                                                            }
                                                                        } catch (Throwable th18) {
                                                                            while (true) {
                                                                                throw th18;
                                                                            }
                                                                        }
                                                                    }
                                                                    FileUtility.closeQuietly(buffer);
                                                                    FileUtility.closeQuietly(source);
                                                                    i7 = i5;
                                                                    z9 = z;
                                                                    z8 = true;
                                                                } catch (Throwable th19) {
                                                                    th = th19;
                                                                    r820 = r27;
                                                                    th3 = th;
                                                                    r223 = buffer;
                                                                    r192 = source;
                                                                    r52 = newCall;
                                                                    r92 = execute;
                                                                    r82 = r819;
                                                                    z2 = true;
                                                                    r22 = r222;
                                                                    r19 = r192;
                                                                    r9 = r92;
                                                                    r8 = r82;
                                                                    r5 = r52;
                                                                    r9.body().close();
                                                                    if (r5 != 0) {
                                                                    }
                                                                    String access$100622222222 = AssetDownloader.TAG;
                                                                    StringBuilder sb622222222 = new StringBuilder();
                                                                    sb622222222.append("request is done ");
                                                                    sb622222222.append(AssetDownloader.this.debugString(downloadRequest));
                                                                    Log.d(access$100622222222, sb622222222.toString());
                                                                    synchronized (AssetDownloader.this) {
                                                                    }
                                                                    FileUtility.closeQuietly(r22);
                                                                    FileUtility.closeQuietly(r19);
                                                                    throw th;
                                                                }
                                                            } catch (Throwable th20) {
                                                                th = th20;
                                                                r820 = file;
                                                                th3 = th;
                                                                r223 = buffer;
                                                                r192 = source;
                                                                r52 = newCall;
                                                                r92 = execute;
                                                                r82 = r819;
                                                                z2 = true;
                                                                r22 = r222;
                                                                r19 = r192;
                                                                r9 = r92;
                                                                r8 = r82;
                                                                r5 = r52;
                                                                r9.body().close();
                                                                if (r5 != 0) {
                                                                }
                                                                String access$1006222222222 = AssetDownloader.TAG;
                                                                StringBuilder sb6222222222 = new StringBuilder();
                                                                sb6222222222.append("request is done ");
                                                                sb6222222222.append(AssetDownloader.this.debugString(downloadRequest));
                                                                Log.d(access$1006222222222, sb6222222222.toString());
                                                                synchronized (AssetDownloader.this) {
                                                                }
                                                                FileUtility.closeQuietly(r22);
                                                                FileUtility.closeQuietly(r19);
                                                                throw th;
                                                            }
                                                        } catch (Throwable th21) {
                                                            th = th21;
                                                            r88 = file;
                                                            th = th;
                                                            r195 = source;
                                                            r57 = newCall;
                                                            r96 = execute;
                                                            z2 = true;
                                                            r19 = r195;
                                                            r9 = r96;
                                                            r8 = r88;
                                                            r5 = r57;
                                                            r22 = 0;
                                                            r9.body().close();
                                                            if (r5 != 0) {
                                                            }
                                                            String access$10062222222222 = AssetDownloader.TAG;
                                                            StringBuilder sb62222222222 = new StringBuilder();
                                                            sb62222222222.append("request is done ");
                                                            sb62222222222.append(AssetDownloader.this.debugString(downloadRequest));
                                                            Log.d(access$10062222222222, sb62222222222.toString());
                                                            synchronized (AssetDownloader.this) {
                                                            }
                                                            FileUtility.closeQuietly(r22);
                                                            FileUtility.closeQuietly(r19);
                                                            throw th;
                                                        }
                                                        r3 = 0;
                                                        r32 = r3;
                                                    } else {
                                                        z4 = z9;
                                                        ? r824 = file;
                                                        try {
                                                            r814 = r824;
                                                            throw new IOException("Response body is null");
                                                        } catch (Throwable th22) {
                                                        }
                                                    }
                                                } catch (Throwable th102) {
                                                }
                                            } else {
                                                z4 = z9;
                                                i5 = i7;
                                                int i11 = i;
                                                ? r825 = file;
                                                r814 = r825;
                                                StringBuilder sb16 = new StringBuilder();
                                                sb16.append("Code: ");
                                                r814 = r825;
                                                int i12 = i11;
                                                r814 = r825;
                                                sb16.append(i12);
                                                throw new RequestException(sb16.toString());
                                            }
                                        }
                                        try {
                                            if (file.exists()) {
                                                file.delete();
                                            }
                                            try {
                                                if (file2.exists()) {
                                                    file2.delete();
                                                }
                                                i6 = i7 + 1;
                                                if (i7 < AssetDownloader.this.maxReconnectAttempts) {
                                                    if (!(execute == 0 || execute.body() == null)) {
                                                        execute.body().close();
                                                    }
                                                    if (newCall != 0) {
                                                        newCall.cancel();
                                                    }
                                                    String access$10014 = AssetDownloader.TAG;
                                                    StringBuilder sb17 = new StringBuilder();
                                                    sb17.append("request is done ");
                                                    sb17.append(AssetDownloader.this.debugString(downloadRequest));
                                                    Log.d(access$10014, sb17.toString());
                                                    synchronized (AssetDownloader.this) {
                                                        try {
                                                            String access$10015 = AssetDownloader.TAG;
                                                            StringBuilder sb18 = new StringBuilder();
                                                            sb18.append("Not removing connections and listener ");
                                                            sb18.append(AssetDownloader.this.debugString(downloadRequest));
                                                            Log.d(access$10015, sb18.toString());
                                                            if (AssetDownloader.this.connections.isEmpty()) {
                                                                AssetDownloader.this.networkProvider.removeListener(AssetDownloader.this.networkListener);
                                                            }
                                                        } catch (Throwable th23) {
                                                            while (true) {
                                                                throw th23;
                                                            }
                                                        }
                                                    }
                                                    FileUtility.closeQuietly(null);
                                                    FileUtility.closeQuietly(null);
                                                    i7 = i6;
                                                    r3 = 0;
                                                    z8 = false;
                                                    r32 = r3;
                                                } else {
                                                    ? r114 = 0;
                                                    try {
                                                        r11 = r114;
                                                        StringBuilder sb19 = new StringBuilder();
                                                        sb19.append("Code: ");
                                                        sb19.append(i);
                                                        throw new RequestException(sb19.toString());
                                                    } catch (Throwable th24) {
                                                    }
                                                }
                                            } catch (Throwable th25) {
                                            }
                                        } catch (Throwable th252) {
                                        }
                                    }
                                } catch (Throwable th1022) {
                                }
                            } catch (Throwable th26) {
                                z7 = z9;
                                r810 = file;
                                th = th26;
                                r89 = r812;
                                r58 = 0;
                                ? r82322 = r89;
                                ? r51322 = r58;
                                ? r91022 = 0;
                                r96 = r91022;
                                r88 = r82322;
                                r57 = r51322;
                                r195 = 0;
                                z2 = true;
                                r19 = r195;
                                r9 = r96;
                                r8 = r88;
                                r5 = r57;
                                r22 = 0;
                                r9.body().close();
                                if (r5 != 0) {
                                }
                                String access$100622222222222 = AssetDownloader.TAG;
                                StringBuilder sb622222222222 = new StringBuilder();
                                sb622222222222.append("request is done ");
                                sb622222222222.append(AssetDownloader.this.debugString(downloadRequest));
                                Log.d(access$100622222222222, sb622222222222.toString());
                                synchronized (AssetDownloader.this) {
                                }
                                FileUtility.closeQuietly(r22);
                                FileUtility.closeQuietly(r19);
                                throw th;
                            }
                        } else {
                            z6 = z9;
                            i4 = i7;
                            try {
                                Log.d(AssetDownloader.TAG, "Request is not connected to reuired network");
                                throw new IOException("Not connected to correct network");
                            } catch (Throwable th27) {
                                th = th27;
                                th = th;
                                r58 = 0;
                                r89 = 0;
                                ? r823222 = r89;
                                ? r513222 = r58;
                                ? r910222 = 0;
                                r96 = r910222;
                                r88 = r823222;
                                r57 = r513222;
                                r195 = 0;
                                z2 = true;
                                r19 = r195;
                                r9 = r96;
                                r8 = r88;
                                r5 = r57;
                                r22 = 0;
                                r9.body().close();
                                if (r5 != 0) {
                                }
                                String access$1006222222222222 = AssetDownloader.TAG;
                                StringBuilder sb6222222222222 = new StringBuilder();
                                sb6222222222222.append("request is done ");
                                sb6222222222222.append(AssetDownloader.this.debugString(downloadRequest));
                                Log.d(access$1006222222222222, sb6222222222222.toString());
                                synchronized (AssetDownloader.this) {
                                }
                                FileUtility.closeQuietly(r22);
                                FileUtility.closeQuietly(r19);
                                throw th;
                            }
                        }
                    } catch (Throwable th28) {
                        th = th28;
                        z6 = z9;
                        th = th;
                        r58 = 0;
                        r89 = 0;
                        ? r8232222 = r89;
                        ? r5132222 = r58;
                        ? r9102222 = 0;
                        r96 = r9102222;
                        r88 = r8232222;
                        r57 = r5132222;
                        r195 = 0;
                        z2 = true;
                        r19 = r195;
                        r9 = r96;
                        r8 = r88;
                        r5 = r57;
                        r22 = 0;
                        r9.body().close();
                        if (r5 != 0) {
                        }
                        String access$10062222222222222 = AssetDownloader.TAG;
                        StringBuilder sb62222222222222 = new StringBuilder();
                        sb62222222222222.append("request is done ");
                        sb62222222222222.append(AssetDownloader.this.debugString(downloadRequest));
                        Log.d(access$10062222222222222, sb62222222222222.toString());
                        synchronized (AssetDownloader.this) {
                        }
                        FileUtility.closeQuietly(r22);
                        FileUtility.closeQuietly(r19);
                        throw th;
                    }
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public ResponseBody decodeGzipIfNeeded(Response response) {
        if (!"gzip".equalsIgnoreCase(response.header("Content-Encoding")) || !HttpHeaders.hasBody(response)) {
            return response.body();
        }
        return new RealResponseBody(response.header("Content-Type"), -1, Okio.buffer((Source) new GzipSource(response.body().source())));
    }

    /* access modifiers changed from: private */
    public void onCancelled(@NonNull final DownloadRequest downloadRequest, @Nullable final Progress progress) {
        this.connections.remove(downloadRequest.id);
        final AssetDownloadListener assetDownloadListener = (AssetDownloadListener) this.listeners.remove(downloadRequest.id);
        if (progress == null) {
            progress = new Progress();
        }
        progress.status = 3;
        if (assetDownloadListener != null) {
            this.uiExecutor.execute(new Runnable() {
                public void run() {
                    assetDownloadListener.onProgress(progress, downloadRequest);
                }
            });
        }
        String str = TAG;
        StringBuilder sb = new StringBuilder();
        sb.append("Cancelled ");
        sb.append(debugString(downloadRequest));
        Log.d(str, sb.toString());
    }

    /* access modifiers changed from: private */
    public int mapExceptionToReason(Throwable th, boolean z) {
        if (th instanceof RuntimeException) {
            return 4;
        }
        if (!z || (th instanceof SocketException) || (th instanceof SocketTimeoutException)) {
            return 0;
        }
        return ((th instanceof UnknownHostException) || (th instanceof SSLException)) ? 1 : 2;
    }

    /* access modifiers changed from: private */
    public long getContentLength(Response response) {
        if (response == null) {
            return -1;
        }
        String str = response.headers().get(HttpRequest.HEADER_CONTENT_LENGTH);
        if (TextUtils.isEmpty(str)) {
            return -1;
        }
        try {
            return Long.parseLong(str);
        } catch (Throwable unused) {
            return -1;
        }
    }

    /* access modifiers changed from: private */
    public boolean fullyDownloadedContent(File file, Response response, DownloadRequest downloadRequest) {
        boolean z = false;
        if (!file.exists() || file.length() <= 0 || "gzip".equalsIgnoreCase(response.headers().get("Content-Encoding"))) {
            return false;
        }
        int code = response.code();
        long contentLength = getContentLength(response);
        if (code == 200 && contentLength == file.length()) {
            String str = TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("200 code, data size matches file size ");
            sb.append(debugString(downloadRequest));
            Log.d(str, sb.toString());
            return responseVersionMatches(file, response);
        } else if (code != RANGE_NOT_SATISFIABLE) {
            return false;
        } else {
            String str2 = response.headers().get(CONTENT_RANGE);
            if (TextUtils.isEmpty(str2)) {
                return false;
            }
            RangeResponse rangeResponse = new RangeResponse(str2);
            if (BYTES.equalsIgnoreCase(rangeResponse.dimension) && rangeResponse.total > 0 && rangeResponse.total == file.length() && responseVersionMatches(file, response)) {
                z = true;
            }
            String str3 = TAG;
            StringBuilder sb2 = new StringBuilder();
            sb2.append("416 code, data size matches file size ");
            sb2.append(debugString(downloadRequest));
            Log.d(str3, sb2.toString());
            return z;
        }
    }

    /* access modifiers changed from: private */
    public synchronized void onSuccess(final File file, @NonNull final DownloadRequest downloadRequest) {
        this.connections.remove(downloadRequest.id);
        final AssetDownloadListener assetDownloadListener = (AssetDownloadListener) this.listeners.remove(downloadRequest.id);
        String str = TAG;
        StringBuilder sb = new StringBuilder();
        sb.append("OnComplete - Removing connections and listener ");
        sb.append(downloadRequest.id);
        Log.d(str, sb.toString());
        if (assetDownloadListener != null) {
            this.uiExecutor.execute(new Runnable() {
                public void run() {
                    assetDownloadListener.onSuccess(file, downloadRequest);
                }
            });
        }
        String str2 = TAG;
        StringBuilder sb2 = new StringBuilder();
        sb2.append("Finished ");
        sb2.append(debugString(downloadRequest));
        Log.d(str2, sb2.toString());
    }

    /* access modifiers changed from: private */
    public synchronized void onPaused(Progress progress, DownloadRequest downloadRequest) {
        String str = TAG;
        StringBuilder sb = new StringBuilder();
        sb.append("Pausing download ");
        sb.append(debugString(downloadRequest));
        Log.d(str, sb.toString());
        progress.status = 2;
        deliverProgress(downloadRequest.id, progress);
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0065, code lost:
        return;
     */
    public synchronized void onError(DownloadError downloadError, @Nullable final DownloadRequest downloadRequest) {
        if (downloadRequest != null) {
            final AssetDownloadListener assetDownloadListener = (AssetDownloadListener) this.listeners.remove(downloadRequest.id);
            this.connections.remove(downloadRequest.id);
            downloadRequest.set(5);
            final DownloadError downloadError2 = downloadError == null ? new DownloadError(-1, new RuntimeException(), 4) : downloadError;
            String str = TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("OnError - Removing connections and listener ");
            sb.append(downloadRequest.id);
            Log.d(str, sb.toString());
            if (assetDownloadListener != null) {
                String str2 = TAG;
                StringBuilder sb2 = new StringBuilder();
                sb2.append("On download error ");
                sb2.append(downloadError);
                Log.e(str2, sb2.toString());
                this.uiExecutor.execute(new Runnable() {
                    public void run() {
                        assetDownloadListener.onError(downloadError2, downloadRequest);
                    }
                });
            }
        }
    }

    /* access modifiers changed from: private */
    public boolean satisfiesPartialDownload(Response response, long j, DownloadRequest downloadRequest) {
        RangeResponse rangeResponse = new RangeResponse(response.headers().get(CONTENT_RANGE));
        boolean z = response.code() == 206 && BYTES.equalsIgnoreCase(rangeResponse.dimension) && rangeResponse.rangeStart >= 0 && j == rangeResponse.rangeStart;
        String str = TAG;
        StringBuilder sb = new StringBuilder();
        sb.append("satisfies partial download: ");
        sb.append(z);
        sb.append(" ");
        sb.append(debugString(downloadRequest));
        Log.d(str, sb.toString());
        return z;
    }

    /* access modifiers changed from: private */
    public String debugString(DownloadRequest downloadRequest) {
        StringBuilder sb = new StringBuilder();
        sb.append(" id - ");
        sb.append(downloadRequest.id);
        sb.append(", url - ");
        sb.append(downloadRequest.url);
        sb.append(", path - ");
        sb.append(downloadRequest.path);
        sb.append(", th - ");
        sb.append(Thread.currentThread().getName());
        return sb.toString();
    }

    /* access modifiers changed from: private */
    public boolean shouldPause(DownloadRequest downloadRequest) {
        if (!downloadRequest.pauseOnConnectionLost) {
            return false;
        }
        return !isConnected(downloadRequest);
    }

    /* access modifiers changed from: private */
    @TargetApi(21)
    public boolean isConnected(DownloadRequest downloadRequest) {
        int i;
        int currentNetworkType = this.networkProvider.getCurrentNetworkType();
        boolean z = true;
        if (currentNetworkType >= 0 && downloadRequest.networkType == 3) {
            return true;
        }
        switch (currentNetworkType) {
            case 0:
            case 4:
            case 7:
            case 17:
                i = 1;
                break;
            case 1:
            case 6:
            case 9:
                i = 2;
                break;
            default:
                i = -1;
                break;
        }
        if (i <= 0 || (downloadRequest.networkType & i) != i) {
            z = false;
        }
        String str = TAG;
        StringBuilder sb = new StringBuilder();
        sb.append("checking pause for type: ");
        sb.append(currentNetworkType);
        sb.append(" connected ");
        sb.append(z);
        sb.append(debugString(downloadRequest));
        Log.d(str, sb.toString());
        return z;
    }

    /* access modifiers changed from: private */
    public void deliverProgress(String str, Progress progress) {
        final Progress copy = Progress.copy(progress);
        String str2 = TAG;
        StringBuilder sb = new StringBuilder();
        sb.append("Progress ");
        sb.append(progress.progressPercent);
        sb.append(" status ");
        sb.append(progress.status);
        sb.append(" ");
        sb.append(str);
        Log.d(str2, sb.toString());
        final AssetDownloadListener assetDownloadListener = (AssetDownloadListener) this.listeners.get(str);
        final DownloadRequest downloadRequest = (DownloadRequest) this.connections.get(str);
        if (assetDownloadListener != null) {
            this.uiExecutor.execute(new Runnable() {
                public void run() {
                    assetDownloadListener.onProgress(copy, downloadRequest);
                }
            });
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0023, code lost:
        return;
     */
    public synchronized void cancel(@Nullable DownloadRequest downloadRequest) {
        if (downloadRequest != null) {
            int andSetStatus = downloadRequest.getAndSetStatus(3);
            if (!(andSetStatus == 1 || andSetStatus == 3)) {
                onCancelled(downloadRequest, null);
                if (this.connections.isEmpty()) {
                    this.networkProvider.removeListener(this.networkListener);
                }
            }
        }
    }

    public boolean cancelAndAwait(@Nullable DownloadRequest downloadRequest, long j) {
        Integer num;
        if (downloadRequest == null) {
            return true;
        }
        cancel(downloadRequest);
        String str = downloadRequest.id;
        long currentTimeMillis = System.currentTimeMillis() + Math.max(0, j);
        while (System.currentTimeMillis() < currentTimeMillis) {
            DownloadRequest downloadRequest2 = (DownloadRequest) this.connections.get(str);
            if (downloadRequest2 == null || downloadRequest2.getStatus() != 3) {
                String str2 = TAG;
                StringBuilder sb = new StringBuilder();
                sb.append("Request is not present or status changed - finish await ");
                if (downloadRequest2 == null) {
                    num = null;
                } else {
                    num = Integer.valueOf(downloadRequest2.getStatus());
                }
                sb.append(num);
                Log.d(str2, sb.toString());
                return true;
            }
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
                return false;
            }
        }
        return false;
    }

    public synchronized void cancelAll() {
        for (DownloadRequest cancel : this.connections.values()) {
            cancel(cancel);
        }
    }

    public void setProgressStep(int i) {
        if (i != 0) {
            this.progressStep = i;
        }
    }

    /* access modifiers changed from: private */
    public synchronized void onNetworkChanged(int i) {
        String str = TAG;
        StringBuilder sb = new StringBuilder();
        sb.append("Num of connections: ");
        sb.append(this.connections.values().size());
        Log.d(str, sb.toString());
        for (DownloadRequest downloadRequest : this.connections.values()) {
            if (downloadRequest.is(3)) {
                Log.d(TAG, "Result cancelled");
            } else {
                boolean isConnected = isConnected(downloadRequest);
                String str2 = TAG;
                StringBuilder sb2 = new StringBuilder();
                sb2.append("Connected = ");
                sb2.append(isConnected);
                sb2.append(" for ");
                sb2.append(i);
                Log.d(str2, sb2.toString());
                downloadRequest.setConnected(isConnected);
                if (downloadRequest.pauseOnConnectionLost && isConnected && downloadRequest.is(2)) {
                    load(downloadRequest);
                    String str3 = TAG;
                    StringBuilder sb3 = new StringBuilder();
                    sb3.append("resumed ");
                    sb3.append(downloadRequest.id);
                    Log.d(str3, sb3.toString());
                }
            }
        }
    }

    private boolean responseVersionMatches(File file, Response response) {
        StringBuilder sb = new StringBuilder();
        sb.append(file.getPath());
        sb.append(META_POSTFIX_EXT);
        Map readMap = FileUtility.readMap(new File(sb.toString()).getPath());
        Headers headers = response.headers();
        String str = headers.get("ETag");
        String str2 = headers.get("Last-Modified");
        String str3 = TAG;
        StringBuilder sb2 = new StringBuilder();
        sb2.append("server etag: ");
        sb2.append(str);
        Log.d(str3, sb2.toString());
        String str4 = TAG;
        StringBuilder sb3 = new StringBuilder();
        sb3.append("server lastModified: ");
        sb3.append(str2);
        Log.d(str4, sb3.toString());
        if (str != null && !str.equals(readMap.get("ETag"))) {
            String str5 = TAG;
            StringBuilder sb4 = new StringBuilder();
            sb4.append("etags miss match current: ");
            sb4.append((String) readMap.get("ETag"));
            Log.d(str5, sb4.toString());
            return false;
        } else if (str2 == null || str2.equals(readMap.get("Last-Modified"))) {
            return true;
        } else {
            String str6 = TAG;
            StringBuilder sb5 = new StringBuilder();
            sb5.append("lastModified miss match current: ");
            sb5.append((String) readMap.get("Last-Modified"));
            Log.d(str6, sb5.toString());
            return false;
        }
    }

    /* access modifiers changed from: 0000 */
    public synchronized void shutdown() {
        cancel(null);
        this.listeners.clear();
        this.connections.clear();
        this.uiExecutor.shutdownNow();
        this.downloadExecutor.shutdownNow();
        try {
            this.downloadExecutor.awaitTermination(2, TimeUnit.SECONDS);
            this.uiExecutor.awaitTermination(2, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return;
    }
}
