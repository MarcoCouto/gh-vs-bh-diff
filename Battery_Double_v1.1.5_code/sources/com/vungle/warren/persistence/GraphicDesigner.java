package com.vungle.warren.persistence;

import android.support.annotation.NonNull;
import android.util.Log;
import com.vungle.warren.persistence.CacheManager.Listener;
import com.vungle.warren.utility.FileUtility;
import java.io.File;
import java.io.IOException;

public class GraphicDesigner implements Designer, Listener {
    private static final String FOLDER_NAME = "vungle";
    private static final String TAG = "GraphicDesigner";
    private CacheManager cacheManager;

    public GraphicDesigner(@NonNull CacheManager cacheManager2) {
        this.cacheManager = cacheManager2;
        this.cacheManager.addListener(this);
        FileUtility.printDirectoryTree(getCacheDirectory());
    }

    public File getAssetDirectory(String str) throws IllegalStateException {
        StringBuilder sb = new StringBuilder();
        sb.append(getCacheDirectory().getPath());
        sb.append(File.separator);
        sb.append(str);
        File file = new File(sb.toString());
        if (!file.exists()) {
            file.mkdir();
        }
        return file;
    }

    public void deleteAssets(String str) throws IOException, IllegalStateException {
        File[] listFiles = getCacheDirectory().listFiles();
        if (listFiles != null) {
            for (File file : listFiles) {
                if (file.isDirectory() && file.getName().equals(str)) {
                    FileUtility.delete(file);
                }
            }
        }
    }

    public File getCacheDirectory() throws IllegalStateException {
        if (this.cacheManager != null) {
            StringBuilder sb = new StringBuilder();
            sb.append(this.cacheManager.getCache());
            sb.append(File.separator);
            sb.append(FOLDER_NAME);
            File file = new File(sb.toString());
            if (!file.exists()) {
                file.mkdir();
            }
            return file;
        }
        throw new IllegalStateException("Context has expired, cannot continue.");
    }

    public void clearCache() {
        if (this.cacheManager != null && this.cacheManager.getCache() != null) {
            StringBuilder sb = new StringBuilder();
            sb.append(this.cacheManager.getCache().getPath());
            sb.append(File.separator);
            sb.append(FOLDER_NAME);
            File file = new File(sb.toString());
            if (file.exists()) {
                try {
                    FileUtility.delete(file);
                } catch (IOException e) {
                    String str = TAG;
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("Failed to delete cached files. Reason: ");
                    sb2.append(e.getLocalizedMessage());
                    Log.e(str, sb2.toString());
                }
            }
            if (!file.exists()) {
                file.mkdir();
            }
        }
    }

    public void onCacheChanged() {
        if (this.cacheManager != null) {
            for (File file : this.cacheManager.getOldCaches()) {
                try {
                    StringBuilder sb = new StringBuilder();
                    sb.append(file.getPath());
                    sb.append(File.separator);
                    sb.append(FOLDER_NAME);
                    FileUtility.delete(new File(sb.toString()));
                } catch (IOException e) {
                    String str = TAG;
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("Failed to delete cached files. Reason: ");
                    sb2.append(e.getLocalizedMessage());
                    Log.e(str, sb2.toString());
                }
            }
        }
    }
}
