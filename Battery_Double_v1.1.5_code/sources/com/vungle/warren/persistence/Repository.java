package com.vungle.warren.persistence;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build.VERSION;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.support.v4.content.PermissionChecker;
import android.text.TextUtils;
import android.util.Log;
import com.vungle.warren.model.AdAsset;
import com.vungle.warren.model.AdAssetDBAdapter;
import com.vungle.warren.model.AdAssetDBAdapter.AdAssetColumns;
import com.vungle.warren.model.Advertisement;
import com.vungle.warren.model.Advertisement.State;
import com.vungle.warren.model.AdvertisementDBAdapter;
import com.vungle.warren.model.AdvertisementDBAdapter.AdvertisementColumns;
import com.vungle.warren.model.Cookie;
import com.vungle.warren.model.CookieDBAdapter;
import com.vungle.warren.model.Placement;
import com.vungle.warren.model.PlacementDBAdapter;
import com.vungle.warren.model.PlacementDBAdapter.PlacementColumns;
import com.vungle.warren.model.Report;
import com.vungle.warren.model.ReportDBAdapter;
import com.vungle.warren.model.ReportDBAdapter.ReportColumns;
import com.vungle.warren.persistence.DatabaseHelper.DBException;
import com.vungle.warren.persistence.DatabaseHelper.DatabaseFactory;
import com.vungle.warren.utility.FileUtility;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class Repository {
    /* access modifiers changed from: private */
    public static final String TAG = "Repository";
    public static int VERSION = 2;
    /* access modifiers changed from: private */
    public Map<Class, DBAdapter> adapters;
    private final Context appCtx;
    private final ExecutorService backgroundExecutor;
    @VisibleForTesting
    protected final DatabaseHelper dbHelper;
    /* access modifiers changed from: private */
    public final Designer designer;
    /* access modifiers changed from: private */
    public final ExecutorService uiExecutor;

    public static class FutureResult<T> implements Future<T> {
        private final Future<T> future;

        public FutureResult(Future<T> future2) {
            this.future = future2;
        }

        public boolean cancel(boolean z) {
            return this.future.cancel(z);
        }

        public boolean isCancelled() {
            return this.future.isCancelled();
        }

        public boolean isDone() {
            return this.future.isDone();
        }

        public T get() {
            try {
                return this.future.get();
            } catch (InterruptedException e) {
                e.printStackTrace();
                return null;
            } catch (ExecutionException e2) {
                Log.e(Repository.TAG, "error on execution", e2);
                return null;
            }
        }

        public T get(long j, @NonNull TimeUnit timeUnit) throws TimeoutException {
            try {
                return this.future.get(j, timeUnit);
            } catch (InterruptedException e) {
                e.printStackTrace();
                return null;
            } catch (ExecutionException e2) {
                Log.e(Repository.TAG, "error on execution", e2);
                return null;
            }
        }
    }

    public interface LoadCallback<T> {
        void onLoaded(T t);
    }

    public interface SaveCallback {
        void onError(Exception exc);

        void onSaved();
    }

    private static class VungleDatabaseCreator implements DatabaseFactory {
        private final Context context;

        public VungleDatabaseCreator(Context context2) {
            this.context = context2;
        }

        public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
            if (i == 1 && i2 == 2) {
                sQLiteDatabase.execSQL("ALTER TABLE report ADD COLUMN status INTEGER DEFAULT 1");
            }
        }

        public void create(SQLiteDatabase sQLiteDatabase) {
            dropOldFilesData();
            sQLiteDatabase.execSQL(AdvertisementDBAdapter.CREATE_ADVERTISEMENT_TABLE_QUERY);
            sQLiteDatabase.execSQL(PlacementDBAdapter.CREATE_PLACEMENT_TABLE_QUERY);
            sQLiteDatabase.execSQL(CookieDBAdapter.CREATE_COOKIE_TABLE_QUERY);
            sQLiteDatabase.execSQL(ReportDBAdapter.CREATE_REPORT_TABLE_QUERY);
            sQLiteDatabase.execSQL(AdAssetDBAdapter.CREATE_ASSET_TABLE_QUERY);
        }

        public void deleteData(SQLiteDatabase sQLiteDatabase) {
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS advertisement");
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS cookie");
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS placement");
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS report");
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS adAsset");
        }

        public void onDowngrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
            ArrayList<String> arrayList = new ArrayList<>();
            Cursor rawQuery = sQLiteDatabase.rawQuery("SELECT * FROM sqlite_master WHERE type='table'", null);
            while (rawQuery != null && rawQuery.moveToNext()) {
                String string = rawQuery.getString(1);
                if (!string.equals("android_metadata") && !string.startsWith("sqlite_")) {
                    arrayList.add(string);
                }
            }
            if (rawQuery != null) {
                rawQuery.close();
            }
            for (String str : arrayList) {
                StringBuilder sb = new StringBuilder();
                sb.append("DROP TABLE IF EXISTS ");
                sb.append(str);
                sQLiteDatabase.execSQL(sb.toString());
            }
            create(sQLiteDatabase);
        }

        private void deleteDatabase(String str) {
            this.context.deleteDatabase(str);
        }

        private void dropOldFilesData() {
            deleteDatabase("vungle");
            File externalFilesDir = this.context.getExternalFilesDir(null);
            if (((VERSION.SDK_INT >= 19 || PermissionChecker.checkCallingOrSelfPermission(this.context, "android.permission.WRITE_EXTERNAL_STORAGE") == 0) && Environment.getExternalStorageState().equals("mounted") && externalFilesDir != null) && externalFilesDir.exists()) {
                try {
                    FileUtility.delete(new File(externalFilesDir, ".vungle"));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            File filesDir = this.context.getFilesDir();
            if (filesDir.exists()) {
                try {
                    FileUtility.delete(new File(filesDir, "vungle"));
                } catch (IOException e2) {
                    e2.printStackTrace();
                }
            }
            try {
                StringBuilder sb = new StringBuilder();
                sb.append(this.context.getCacheDir());
                sb.append(File.separator);
                sb.append("downloads_vungle");
                FileUtility.delete(new File(sb.toString()));
            } catch (IOException e3) {
                e3.printStackTrace();
            }
        }
    }

    public Repository(Context context, Designer designer2, ExecutorService executorService, ExecutorService executorService2) {
        this(context, designer2, executorService, executorService2, VERSION);
    }

    public Repository(Context context, Designer designer2, ExecutorService executorService, ExecutorService executorService2, int i) {
        this.adapters = new HashMap<Class, DBAdapter>() {
            {
                put(Placement.class, new PlacementDBAdapter());
                put(Cookie.class, new CookieDBAdapter());
                put(Report.class, new ReportDBAdapter());
                put(Advertisement.class, new AdvertisementDBAdapter());
                put(AdAsset.class, new AdAssetDBAdapter());
            }
        };
        this.appCtx = context.getApplicationContext();
        this.backgroundExecutor = executorService;
        this.uiExecutor = executorService2;
        this.dbHelper = new DatabaseHelper(context, i, new VungleDatabaseCreator(this.appCtx));
        this.designer = designer2;
    }

    public void init() throws DBException {
        runAndWait(new Callable<Void>() {
            public Void call() throws Exception {
                Repository.this.dbHelper.init();
                ContentValues contentValues = new ContentValues();
                contentValues.put("state", Integer.valueOf(3));
                Query query = new Query(AdvertisementColumns.TABLE_NAME);
                query.selection = "state=?";
                query.args = new String[]{String.valueOf(2)};
                Repository.this.dbHelper.update(query, contentValues);
                return null;
            }
        });
    }

    /* access modifiers changed from: private */
    public <T> List<T> loadAllModels(Class<T> cls) {
        DBAdapter dBAdapter = (DBAdapter) this.adapters.get(cls);
        if (dBAdapter == null) {
            return Collections.EMPTY_LIST;
        }
        return extractModels(cls, this.dbHelper.query(new Query(dBAdapter.tableName())));
    }

    /* access modifiers changed from: private */
    @NonNull
    public <T> List<T> extractModels(Class<T> cls, Cursor cursor) {
        if (cursor == null || cursor.isClosed()) {
            return Collections.emptyList();
        }
        ArrayList arrayList = new ArrayList();
        try {
            DBAdapter dBAdapter = (DBAdapter) this.adapters.get(cls);
            while (cursor.moveToNext()) {
                ContentValues contentValues = new ContentValues();
                DatabaseUtils.cursorRowToContentValues(cursor, contentValues);
                arrayList.add(dBAdapter.fromContentValues(contentValues));
            }
            return arrayList;
        } finally {
            cursor.close();
        }
    }

    /* access modifiers changed from: private */
    public <T> T loadModel(String str, Class<T> cls) {
        DBAdapter dBAdapter = (DBAdapter) this.adapters.get(cls);
        Query query = new Query(dBAdapter.tableName());
        query.selection = "item_id = ? ";
        query.args = new String[]{str};
        Cursor query2 = this.dbHelper.query(query);
        if (query2 != null) {
            try {
                if (query2.moveToNext()) {
                    ContentValues contentValues = new ContentValues();
                    DatabaseUtils.cursorRowToContentValues(query2, contentValues);
                    return dBAdapter.fromContentValues(contentValues);
                }
                query2.close();
            } finally {
                query2.close();
            }
        }
        return null;
    }

    /* access modifiers changed from: private */
    public <T> void saveModel(T t) throws DBException {
        DBAdapter dBAdapter = (DBAdapter) this.adapters.get(t.getClass());
        this.dbHelper.insertWithConflict(dBAdapter.tableName(), dBAdapter.toContentValues(t), 5);
    }

    public <T> FutureResult<T> load(@NonNull final String str, @NonNull final Class<T> cls) {
        return new FutureResult<>(this.backgroundExecutor.submit(new Callable<T>() {
            public T call() {
                return Repository.this.loadModel(str, cls);
            }
        }));
    }

    public <T> void load(@NonNull final String str, @NonNull final Class<T> cls, @NonNull final LoadCallback<T> loadCallback) {
        this.backgroundExecutor.execute(new Runnable() {
            public void run() {
                final Object access$000 = Repository.this.loadModel(str, cls);
                Repository.this.uiExecutor.execute(new Runnable() {
                    public void run() {
                        loadCallback.onLoaded(access$000);
                    }
                });
            }
        });
    }

    public <T> void save(final T t) throws DBException {
        runAndWait(new Callable<Void>() {
            public Void call() throws Exception {
                Repository.this.saveModel(t);
                return null;
            }
        });
    }

    public <T> void save(final T t, @Nullable final SaveCallback saveCallback) {
        try {
            this.backgroundExecutor.submit(new Runnable() {
                public void run() {
                    try {
                        Repository.this.saveModel(t);
                    } catch (DBException e) {
                        if (saveCallback != null) {
                            Repository.this.uiExecutor.execute(new Runnable() {
                                public void run() {
                                    saveCallback.onError(e);
                                }
                            });
                        }
                    }
                    if (saveCallback != null) {
                        Repository.this.uiExecutor.execute(new Runnable() {
                            public void run() {
                                saveCallback.onSaved();
                            }
                        });
                    }
                }
            }).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e2) {
            e2.printStackTrace();
        }
    }

    public FutureResult<Advertisement> findValidAdvertisementForPlacement(final String str) {
        return new FutureResult<>(this.backgroundExecutor.submit(new Callable<Advertisement>() {
            public Advertisement call() {
                String str;
                String access$300 = Repository.TAG;
                StringBuilder sb = new StringBuilder();
                sb.append(" Searching for valid adv for pl ");
                sb.append(str);
                Log.i(access$300, sb.toString());
                Query query = new Query(AdvertisementColumns.TABLE_NAME);
                query.selection = "placement_id = ? AND (state = ? OR  state = ?) AND expire_time > ?";
                query.args = new String[]{str, String.valueOf(1), String.valueOf(0), String.valueOf(System.currentTimeMillis() / 1000)};
                query.limit = "1";
                query.orderBy = "state DESC";
                Cursor query2 = Repository.this.dbHelper.query(query);
                AdvertisementDBAdapter advertisementDBAdapter = (AdvertisementDBAdapter) Repository.this.adapters.get(Advertisement.class);
                ArrayList arrayList = new ArrayList();
                while (query2 != null && query2.moveToNext()) {
                    ContentValues contentValues = new ContentValues();
                    DatabaseUtils.cursorRowToContentValues(query2, contentValues);
                    arrayList.add(advertisementDBAdapter.fromContentValues(contentValues));
                }
                if (query2 != null) {
                    query2.close();
                }
                Advertisement advertisement = arrayList.size() > 0 ? (Advertisement) arrayList.get(0) : null;
                String access$3002 = Repository.TAG;
                if (advertisement == null) {
                    str = "Didn't find valid adv";
                } else {
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("Found valid adv ");
                    sb2.append(advertisement.getId());
                    str = sb2.toString();
                }
                Log.i(access$3002, str);
                return advertisement;
            }
        }));
    }

    public <T> FutureResult<List<T>> loadAll(final Class<T> cls) {
        return new FutureResult<>(this.backgroundExecutor.submit(new Callable<List<T>>() {
            public List<T> call() {
                return Repository.this.loadAllModels(cls);
            }
        }));
    }

    @Nullable
    public FutureResult<List<Report>> loadAllReportToSend() {
        return new FutureResult<>(this.backgroundExecutor.submit(new Callable<List<Report>>() {
            public List<Report> call() {
                List<Report> access$500 = Repository.this.loadAllModels(Report.class);
                for (Report report : access$500) {
                    report.setStatus(2);
                    try {
                        Repository.this.saveModel(report);
                    } catch (DBException unused) {
                        return null;
                    }
                }
                return access$500;
            }
        }));
    }

    @Nullable
    public FutureResult<List<Report>> loadReadyOrFailedReportToSend() {
        return new FutureResult<>(this.backgroundExecutor.submit(new Callable<List<Report>>() {
            public List<Report> call() {
                Query query = new Query(ReportColumns.TABLE_NAME);
                query.selection = "status = ?  OR status = ? ";
                query.args = new String[]{String.valueOf(1), String.valueOf(3)};
                List<Report> access$600 = Repository.this.extractModels(Report.class, Repository.this.dbHelper.query(query));
                for (Report report : access$600) {
                    report.setStatus(2);
                    try {
                        Repository.this.saveModel(report);
                    } catch (DBException unused) {
                        return null;
                    }
                }
                return access$600;
            }
        }));
    }

    public void updateAndSaveReportState(String str, String str2, int i, int i2) throws DBException {
        final int i3 = i2;
        final String str3 = str;
        final int i4 = i;
        final String str4 = str2;
        AnonymousClass11 r0 = new Callable<Void>() {
            public Void call() throws Exception {
                ContentValues contentValues = new ContentValues();
                contentValues.put("status", Integer.valueOf(i3));
                Query query = new Query(ReportColumns.TABLE_NAME);
                query.selection = "placementId = ?  AND status = ?  AND appId = ? ";
                query.args = new String[]{str3, String.valueOf(i4), str4};
                Repository.this.dbHelper.update(query, contentValues);
                return null;
            }
        };
        runAndWait(r0);
    }

    public FutureResult<List<AdAsset>> loadAllAdAssets(@NonNull final String str) {
        return new FutureResult<>(this.backgroundExecutor.submit(new Callable<List<AdAsset>>() {
            public List<AdAsset> call() {
                return Repository.this.loadAllAdAssetModels(str);
            }
        }));
    }

    /* access modifiers changed from: private */
    public List<AdAsset> loadAllAdAssetModels(@NonNull String str) {
        Query query = new Query(AdAssetColumns.TABLE_NAME);
        query.selection = "ad_identifier = ? ";
        query.args = new String[]{str};
        return extractModels(AdAsset.class, this.dbHelper.query(query));
    }

    public <T> void delete(final T t) throws DBException {
        runAndWait(new Callable<Void>() {
            public Void call() throws Exception {
                Repository.this.deleteModel(t);
                return null;
            }
        });
    }

    /* access modifiers changed from: private */
    public <T> void deleteModel(Class<T> cls, String str) throws DBException {
        Query query = new Query(((DBAdapter) this.adapters.get(cls)).tableName());
        query.selection = "item_id=?";
        query.args = new String[]{str};
        this.dbHelper.delete(query);
    }

    private void deleteAssetForAdId(String str) throws DBException {
        Query query = new Query(((DBAdapter) this.adapters.get(AdAsset.class)).tableName());
        query.selection = "ad_identifier=?";
        query.args = new String[]{str};
        this.dbHelper.delete(query);
    }

    /* access modifiers changed from: private */
    public <T> void deleteModel(T t) throws DBException {
        deleteModel(t.getClass(), ((DBAdapter) this.adapters.get(t.getClass())).toContentValues(t).getAsString("item_id"));
    }

    public void deleteAdvertisement(final String str) throws DBException {
        runAndWait(new Callable<Void>() {
            public Void call() throws Exception {
                Repository.this.deleteAdInternal(str);
                return null;
            }
        });
    }

    /* access modifiers changed from: private */
    public void deleteAdInternal(String str) throws DBException {
        if (!TextUtils.isEmpty(str)) {
            deleteAssetForAdId(str);
            deleteModel(Advertisement.class, str);
            try {
                this.designer.deleteAssets(str);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public FutureResult<Collection<Placement>> loadValidPlacements() {
        return new FutureResult<>(this.backgroundExecutor.submit(new Callable<Collection<Placement>>() {
            public Collection<Placement> call() {
                List access$600;
                synchronized (Repository.this) {
                    Query query = new Query("placement");
                    query.selection = "is_valid = ?";
                    query.args = new String[]{"1"};
                    access$600 = Repository.this.extractModels(Placement.class, Repository.this.dbHelper.query(query));
                }
                return access$600;
            }
        }));
    }

    /* access modifiers changed from: private */
    public List<String> loadValidPlacementIds() {
        Query query = new Query("placement");
        query.selection = "is_valid = ?";
        query.args = new String[]{"1"};
        query.columns = new String[]{"item_id"};
        Cursor query2 = this.dbHelper.query(query);
        ArrayList arrayList = new ArrayList();
        if (query2 != null) {
            while (query2 != null) {
                try {
                    if (!query2.moveToNext()) {
                        break;
                    }
                    arrayList.add(query2.getString(query2.getColumnIndex("item_id")));
                } catch (Throwable th) {
                    query2.close();
                    throw th;
                }
            }
            query2.close();
        }
        return arrayList;
    }

    public FutureResult<File> getAdvertisementAssetDirectory(final String str) {
        return new FutureResult<>(this.backgroundExecutor.submit(new Callable<File>() {
            public File call() throws Exception {
                return Repository.this.designer.getAssetDirectory(str);
            }
        }));
    }

    public FutureResult<Collection<String>> getValidPlacements() {
        return new FutureResult<>(this.backgroundExecutor.submit(new Callable<Collection<String>>() {
            public Collection<String> call() throws Exception {
                List access$1100;
                synchronized (Repository.this) {
                    access$1100 = Repository.this.loadValidPlacementIds();
                }
                return access$1100;
            }
        }));
    }

    public void setValidPlacements(@NonNull final List<Placement> list) throws DBException {
        runAndWait(new Callable<Void>() {
            public Void call() throws Exception {
                synchronized (Repository.class) {
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(PlacementColumns.COLUMN_IS_VALID, Boolean.valueOf(false));
                    Repository.this.dbHelper.update(new Query("placement"), contentValues);
                    for (Placement placement : list) {
                        Placement placement2 = (Placement) Repository.this.loadModel(placement.getId(), Placement.class);
                        if (!(placement2 == null || placement2.isIncentivized() == placement.isIncentivized())) {
                            String access$300 = Repository.TAG;
                            StringBuilder sb = new StringBuilder();
                            sb.append("Placements data for ");
                            sb.append(placement.getId());
                            sb.append(" is different from disc, deleting old");
                            Log.w(access$300, sb.toString());
                            for (String access$900 : Repository.this.getAdsForPlacement(placement.getId())) {
                                Repository.this.deleteAdInternal(access$900);
                            }
                            Repository.this.deleteModel(Placement.class, placement2.getId());
                        }
                        if (placement2 != null) {
                            placement.setWakeupTime(placement2.getWakeupTime());
                        }
                        placement.setValid(true);
                        Repository.this.saveModel(placement);
                    }
                }
                return null;
            }
        });
    }

    public FutureResult<List<String>> findAdsForPlacement(final String str) {
        return new FutureResult<>(this.backgroundExecutor.submit(new Callable<List<String>>() {
            public List<String> call() {
                return Repository.this.getAdsForPlacement(str);
            }
        }));
    }

    /* access modifiers changed from: private */
    public List<String> getAdsForPlacement(String str) {
        Query query = new Query(AdvertisementColumns.TABLE_NAME);
        query.columns = new String[]{"item_id"};
        query.selection = "placement_id=?";
        query.args = new String[]{str};
        Cursor query2 = this.dbHelper.query(query);
        ArrayList arrayList = new ArrayList();
        while (query2 != null && query2.moveToNext()) {
            arrayList.add(query2.getString(query2.getColumnIndex("item_id")));
        }
        if (query2 != null) {
            query2.close();
        }
        return arrayList;
    }

    public void clearAllData() {
        this.dbHelper.dropDb();
        this.designer.clearCache();
    }

    public void saveAndApplyState(@NonNull final Advertisement advertisement, @NonNull final String str, @State final int i) throws DBException {
        runAndWait(new Callable<Void>() {
            public Void call() throws Exception {
                String access$300 = Repository.TAG;
                StringBuilder sb = new StringBuilder();
                sb.append("Setting ");
                sb.append(i);
                sb.append(" for adv ");
                sb.append(advertisement.getId());
                sb.append(" and pl ");
                sb.append(str);
                Log.i(access$300, sb.toString());
                advertisement.setState(i);
                switch (i) {
                    case 0:
                    case 1:
                        advertisement.setPlacementId(str);
                        Repository.this.saveModel(advertisement);
                        break;
                    case 2:
                        advertisement.setPlacementId(null);
                        Repository.this.saveModel(advertisement);
                        break;
                    case 3:
                    case 4:
                        Repository.this.deleteAdInternal(advertisement.getId());
                        break;
                }
                return null;
            }
        });
    }

    private void runAndWait(Callable<Void> callable) throws DBException {
        try {
            this.backgroundExecutor.submit(callable).get();
        } catch (ExecutionException e) {
            if (!(e.getCause() instanceof DBException)) {
                e.printStackTrace();
                return;
            }
            throw ((DBException) e.getCause());
        } catch (InterruptedException e2) {
            e2.printStackTrace();
        }
    }
}
