package com.vungle.warren.persistence;

import android.content.ContentValues;

public class ContentValuesUtil {
    public static boolean getBoolean(ContentValues contentValues, String str) {
        String asString = contentValues.getAsString(str);
        try {
            return ((Boolean) asString).booleanValue();
        } catch (ClassCastException unused) {
            boolean z = true;
            if (asString instanceof CharSequence) {
                if (!Boolean.valueOf(asString.toString()).booleanValue() && !"1".equals(asString)) {
                    z = false;
                }
                return z;
            } else if (!(asString instanceof Number)) {
                return false;
            } else {
                if (((Number) asString).intValue() == 0) {
                    z = false;
                }
                return z;
            }
        }
    }
}
