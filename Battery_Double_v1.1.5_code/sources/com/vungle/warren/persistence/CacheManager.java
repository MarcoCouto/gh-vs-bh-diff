package com.vungle.warren.persistence;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build.VERSION;
import android.os.Environment;
import android.os.FileObserver;
import android.os.StatFs;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.PermissionChecker;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class CacheManager {
    private static final String COM_VUNGLE_SDK = "com.vungle.sdk";
    private static final String PATH_ID = "cache_path";
    private static final String PATH_IDS = "cache_paths";
    private static final String VUNGLE_DIR = "vungle_cache";
    private boolean changed;
    private final Context context;
    private File current;
    private HashSet<Listener> listeners = new HashSet<>();
    private ArrayList<FileObserver> observers = new ArrayList<>();
    private List<File> old = new ArrayList();
    private final SharedPreferences prefs;

    public interface Listener {
        void onCacheChanged();
    }

    public CacheManager(@NonNull Context context2) {
        this.context = context2;
        this.prefs = context2.getSharedPreferences(COM_VUNGLE_SDK, 0);
    }

    /* access modifiers changed from: private */
    public synchronized void selectFileDest() {
        boolean z;
        File file = null;
        if (this.current == null) {
            String string = this.prefs.getString(PATH_ID, null);
            this.current = string != null ? new File(string) : null;
        }
        File externalFilesDir = this.context.getExternalFilesDir(null);
        File filesDir = this.context.getFilesDir();
        File[] fileArr = new File[2];
        fileArr[0] = new File((VERSION.SDK_INT >= 19 || PermissionChecker.checkCallingOrSelfPermission(this.context, "android.permission.WRITE_EXTERNAL_STORAGE") == 0) && Environment.getExternalStorageState().equals("mounted") && externalFilesDir != null ? externalFilesDir : filesDir, VUNGLE_DIR);
        fileArr[1] = new File(filesDir, VUNGLE_DIR);
        Iterator it = Arrays.asList(fileArr).iterator();
        boolean z2 = false;
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            File file2 = (File) it.next();
            if (file2.exists() && file2.isFile() && !file2.delete()) {
                break;
            }
            if (!file2.exists()) {
                z = file2.mkdirs();
                z2 = z;
                continue;
            } else if (!file2.isDirectory() || !file2.canWrite()) {
                z = false;
                continue;
            } else {
                z = true;
                continue;
            }
            if (z) {
                file = file2;
                break;
            }
        }
        File cacheDir = this.context.getCacheDir();
        Set<String> stringSet = this.prefs.getStringSet(PATH_IDS, new HashSet());
        if (file != null) {
            stringSet.add(file.getPath());
        }
        stringSet.add(cacheDir.getPath());
        this.prefs.edit().putStringSet(PATH_IDS, stringSet).apply();
        this.old.clear();
        for (String str : stringSet) {
            if (file == null || !file.getPath().equals(str)) {
                this.old.add(new File(str));
            }
        }
        if (z2 || ((file != null && !file.equals(this.current)) || (this.current != null && !this.current.equals(file)))) {
            this.current = file;
            if (this.current != null) {
                this.prefs.edit().putString(PATH_ID, this.current.getPath()).apply();
            }
            Iterator it2 = this.listeners.iterator();
            while (it2.hasNext()) {
                ((Listener) it2.next()).onCacheChanged();
            }
            this.changed = true;
        }
        observeDirectory(externalFilesDir);
    }

    private void check() {
        if (this.current == null || !this.current.exists() || !this.current.isDirectory() || !this.current.canWrite()) {
            selectFileDest();
        }
    }

    private synchronized void observeDirectory(File file) {
        if (file != null) {
            this.observers.clear();
            this.observers.add(new FileObserver(file.getPath(), 1024) {
                public void onEvent(int i, @Nullable String str) {
                    stopWatching();
                    CacheManager.this.selectFileDest();
                }
            });
            while (file.getParent() != null) {
                final String name = file.getName();
                this.observers.add(new FileObserver(file.getParent(), 256) {
                    public void onEvent(int i, @Nullable String str) {
                        if (name.equals(str)) {
                            stopWatching();
                            CacheManager.this.selectFileDest();
                        }
                    }
                });
                file = file.getParentFile();
            }
            Iterator it = this.observers.iterator();
            while (it.hasNext()) {
                ((FileObserver) it.next()).startWatching();
            }
        }
    }

    @Nullable
    public synchronized File getCache() {
        check();
        return this.current;
    }

    public synchronized List<File> getOldCaches() {
        check();
        return this.old;
    }

    public synchronized void addListener(Listener listener) {
        check();
        this.listeners.add(listener);
        if (this.changed) {
            listener.onCacheChanged();
        }
    }

    public synchronized void removeListener(Listener listener) {
        this.listeners.remove(listener);
    }

    public long getBytesAvailable() {
        long j;
        File cache = getCache();
        if (cache == null) {
            return -1;
        }
        StatFs statFs = new StatFs(cache.getPath());
        if (VERSION.SDK_INT >= 18) {
            j = statFs.getBlockSizeLong() * statFs.getAvailableBlocksLong();
        } else {
            j = (long) (statFs.getBlockSize() * statFs.getAvailableBlocks());
        }
        return j;
    }
}
