package com.vungle.warren.download;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat.Builder;
import android.support.v4.content.FileProvider;
import android.text.TextUtils;
import android.util.Log;
import android.webkit.MimeTypeMap;
import android.widget.Toast;
import com.vungle.warren.downloader.AssetDownloadListener;
import com.vungle.warren.downloader.AssetDownloadListener.DownloadError;
import com.vungle.warren.downloader.AssetDownloadListener.Progress;
import com.vungle.warren.downloader.AssetDownloader;
import com.vungle.warren.downloader.DownloadRequest;
import com.vungle.warren.locale.LocaleString;
import com.vungle.warren.ui.VungleWebViewActivity;
import java.io.File;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

public class APKDirectDownloadManager {
    private static final String APK_POSTFIX = "apk";
    public static final int DIRECT_DOWNLOAD_FLAG_DISABLED = 0;
    public static final int DIRECT_DOWNLOAD_FLAG_ENABLED = 1;
    public static final int DIRECT_DOWNLOAD_FLAG_NOT_SET = -1;
    private static final String FOLDER_APK = "apk";
    private static final String FOLDER_NAME = "apks_vungle";
    private static final String NOTIFICATION_CHANNEL_ID = "1";
    private static final String NOTIFICATION_CHANNEL_NAME = "ApkDirectDownload";
    private static final String TAG = "DirectDownloadManager";
    /* access modifiers changed from: private */
    public static APKDirectDownloadManager _instance = new APKDirectDownloadManager();
    private int apkDirectDownloadStatus = -1;
    private WeakReference<Context> context;
    private AssetDownloader downloader;
    private Builder mBuilder;
    /* access modifiers changed from: private */
    public List<Integer> notificationList = new ArrayList();
    private NotificationManager notifyManager;

    public static void init(Context context2, AssetDownloader assetDownloader) {
        _instance.context = new WeakReference<>(context2);
        _instance.clearDownloadApkCache();
        if (_instance.downloader == null) {
            _instance.downloader = assetDownloader;
        }
    }

    public static boolean isDirectDownloadEnabled(boolean z, boolean z2) {
        if (!_instance.checkExternalStorageAvailable()) {
            return false;
        }
        int i = _instance.apkDirectDownloadStatus;
        if (i != -1) {
            if (i != 1) {
                return false;
            }
            return z2;
        } else if (z) {
            return z2;
        } else {
            return false;
        }
    }

    public static void setDirectDownloadStatus(int i) {
        _instance.apkDirectDownloadStatus = i;
    }

    public static void download(String str) {
        if (_instance != null && !TextUtils.isEmpty(str) && getContext() != null) {
            APKDirectDownloadManager aPKDirectDownloadManager = _instance;
            if (isApkUrl(str)) {
                String valueOf = String.valueOf(System.currentTimeMillis());
                final int hashCode = valueOf.hashCode();
                _instance.notificationList.add(Integer.valueOf(hashCode));
                final File apkDirectory = _instance.getApkDirectory(valueOf);
                if (apkDirectory == null) {
                    Log.e(TAG, "apk file is missing!");
                    _instance.openUrl(str);
                    return;
                }
                Toast.makeText(getContext(), LocaleString.getLocaleText(1), 1).show();
                DownloadRequest downloadRequest = new DownloadRequest(2, 1, str, apkDirectory.getAbsolutePath(), true, null);
                _instance.downloader.download(downloadRequest, new AssetDownloadListener() {
                    public void onError(DownloadError downloadError, DownloadRequest downloadRequest) {
                        String str;
                        String str2 = APKDirectDownloadManager.TAG;
                        StringBuilder sb = new StringBuilder();
                        sb.append("download onError :");
                        if (downloadError.cause == null) {
                            str = "null";
                        } else {
                            str = downloadError.cause.getMessage();
                        }
                        sb.append(str);
                        sb.append(" id:");
                        sb.append(downloadRequest.url);
                        Log.d(str2, sb.toString());
                        APKDirectDownloadManager._instance.dismissNotification(hashCode);
                        APKDirectDownloadManager._instance.notificationList.remove(Integer.valueOf(hashCode));
                    }

                    public void onProgress(Progress progress, DownloadRequest downloadRequest) {
                        String str = APKDirectDownloadManager.TAG;
                        StringBuilder sb = new StringBuilder();
                        sb.append("download progress :");
                        sb.append(progress.progressPercent);
                        sb.append("status:");
                        sb.append(downloadRequest.getStatus());
                        Log.d(str, sb.toString());
                        if (progress.status == 3) {
                            APKDirectDownloadManager._instance.dismissNotification(hashCode);
                        } else {
                            APKDirectDownloadManager._instance.notifyProgress(hashCode, progress);
                        }
                    }

                    public void onSuccess(File file, DownloadRequest downloadRequest) {
                        if (file == null) {
                            onError(new DownloadError(-1, new IOException("Downloaded file not found!"), -1), downloadRequest);
                            return;
                        }
                        String str = APKDirectDownloadManager.TAG;
                        StringBuilder sb = new StringBuilder();
                        sb.append("download complete :");
                        sb.append(file.getAbsolutePath());
                        Log.d(str, sb.toString());
                        APKDirectDownloadManager._instance.installApk(apkDirectory);
                        APKDirectDownloadManager._instance.notificationList.remove(Integer.valueOf(hashCode));
                    }
                });
            } else if (getContext() != null) {
                Intent intent = new Intent();
                intent.setClass(getContext(), VungleWebViewActivity.class);
                intent.putExtra(VungleWebViewActivity.INTENT_URL, str);
                intent.setFlags(268435456);
                getContext().startActivity(intent);
            }
        }
    }

    public static boolean isApkUrl(String str) {
        return MimeTypeMap.getFileExtensionFromUrl(str).toLowerCase().endsWith("apk");
    }

    private void clearDownloadApkCache() {
        new Thread(new Runnable() {
            public void run() {
                File access$500 = APKDirectDownloadManager.this.getCacheDirectory();
                if (access$500 != null) {
                    File[] listFiles = access$500.listFiles();
                    if (listFiles != null && listFiles.length != 0) {
                        for (int i = 0; i < listFiles.length; i++) {
                            if (listFiles[i].isFile() && listFiles[i].exists() && listFiles[i].isFile()) {
                                String str = APKDirectDownloadManager.TAG;
                                StringBuilder sb = new StringBuilder();
                                sb.append("Clear cache:");
                                sb.append(listFiles[i].getName());
                                Log.d(str, sb.toString());
                                listFiles[i].delete();
                            }
                        }
                    }
                }
            }
        }).start();
    }

    private void openUrl(String str) {
        try {
            Intent parseUri = Intent.parseUri(str, 0);
            parseUri.setFlags(268435456);
            Context context2 = getContext();
            if (context2 != null) {
                context2.startActivity(parseUri);
            }
        } catch (Exception e) {
            String str2 = TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("Unable to start activity ");
            sb.append(e.getLocalizedMessage());
            Log.e(str2, sb.toString());
        }
    }

    private File getApkDirectory(String str) {
        File cacheDirectory = getCacheDirectory();
        if (cacheDirectory == null) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(cacheDirectory.getPath());
        sb.append(File.separator);
        File file = new File(sb.toString());
        if (!file.exists()) {
            file.mkdir();
        }
        StringBuilder sb2 = new StringBuilder();
        sb2.append(cacheDirectory);
        sb2.append(File.separator);
        sb2.append(str);
        sb2.append(".");
        sb2.append("apk");
        return new File(sb2.toString());
    }

    private boolean checkExternalStorageAvailable() {
        try {
            if ("mounted".equals(Environment.getExternalStorageState()) && getCacheDirectory() != null) {
                return true;
            }
        } catch (Exception e) {
            String str = TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("Storage state error:");
            sb.append(e.getMessage());
            Log.e(str, sb.toString());
        }
        return false;
    }

    /* access modifiers changed from: private */
    public File getCacheDirectory() {
        if (getContext() == null || getContext().getExternalCacheDir() == null) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(getContext().getExternalCacheDir().getPath());
        sb.append(File.separator);
        sb.append(FOLDER_NAME);
        File file = new File(sb.toString());
        if (!file.exists()) {
            file.mkdir();
        }
        return file;
    }

    /* access modifiers changed from: private */
    public void installApk(File file) {
        if (getContext() == null) {
            Log.e(TAG, "context is null");
            return;
        }
        Intent intent = new Intent();
        intent.setAction("android.intent.action.VIEW");
        intent.addCategory("android.intent.category.DEFAULT");
        intent.setFlags(268435456);
        if (VERSION.SDK_INT > 23) {
            intent.setDataAndType(FileProvider.getUriForFile(getContext(), "com.vungle.download.provider", file), "application/vnd.android.package-archive");
            intent.addFlags(3);
        } else {
            intent.setDataAndType(Uri.fromFile(file), "application/vnd.android.package-archive");
        }
        if (getContext().getPackageManager().queryIntentActivities(intent, 0).size() > 0) {
            getContext().startActivity(intent);
        }
        String name = file.getName();
        String substring = name.substring(0, name.length() - 4);
        String str = TAG;
        StringBuilder sb = new StringBuilder();
        sb.append("identifier is");
        sb.append(substring);
        Log.d(str, sb.toString());
        dismissNotification(substring.hashCode());
    }

    /* access modifiers changed from: private */
    public void notifyProgress(int i, Progress progress) {
        String str = TAG;
        StringBuilder sb = new StringBuilder();
        sb.append("notify id is :");
        sb.append(i);
        sb.append(" progress:");
        sb.append(progress);
        Log.d(str, sb.toString());
        if (getContext() == null) {
            Log.e(TAG, "context is null.");
            return;
        }
        if (this.notifyManager == null) {
            Context context2 = getContext();
            getContext();
            this.notifyManager = (NotificationManager) context2.getSystemService("notification");
            this.mBuilder = new Builder(getContext());
            this.mBuilder.setSmallIcon(17301634);
            if (VERSION.SDK_INT >= 26) {
                this.notifyManager.createNotificationChannel(new NotificationChannel("1", NOTIFICATION_CHANNEL_NAME, 2));
                this.mBuilder.setChannelId("1");
            }
        }
        if (progress.status == 4) {
            this.mBuilder.setContentText(LocaleString.getLocaleText(5)).setProgress(0, 0, false);
        } else if (progress.status == 5 || progress.status == 2) {
            this.mBuilder.setContentTitle(LocaleString.getLocaleText(4)).setContentText(LocaleString.getLocaleText(2)).setProgress(0, 0, false);
        } else {
            this.mBuilder.setContentTitle(LocaleString.getLocaleText(4)).setContentText(LocaleString.getLocaleText(3));
            this.mBuilder.setProgress(100, Math.min(100, Math.max(progress.progressPercent, 0)), false);
        }
        this.notifyManager.notify(i, this.mBuilder.build());
    }

    /* access modifiers changed from: private */
    public void dismissNotification(int i) {
        if (this.notifyManager != null) {
            this.notifyManager.cancel(i);
        }
    }

    @Nullable
    private static Context getContext() {
        if (_instance == null || _instance.context == null) {
            return null;
        }
        return (Context) _instance.context.get();
    }
}
