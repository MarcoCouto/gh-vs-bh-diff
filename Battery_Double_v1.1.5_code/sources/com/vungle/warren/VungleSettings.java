package com.vungle.warren;

public final class VungleSettings {
    private static final long MEGABYTE = 1048576;
    private volatile boolean androidIdOptedOut;
    private volatile long minimumSpaceForAd;
    private volatile long minimumSpaceForInit;

    public static class Builder {
        /* access modifiers changed from: private */
        public volatile boolean androidIdOptedOut = false;
        /* access modifiers changed from: private */
        public volatile long minimumSpaceForAd = 52428800;
        /* access modifiers changed from: private */
        public volatile long minimumSpaceForInit = 53477376;

        public VungleSettings build() {
            return new VungleSettings(this);
        }

        public Builder setMinimumSpaceForInit(long j) {
            this.minimumSpaceForInit = j;
            return this;
        }

        public Builder setMinimumSpaceForAd(long j) {
            this.minimumSpaceForAd = j;
            return this;
        }

        public Builder setAndroidIdOptOut(boolean z) {
            this.androidIdOptedOut = z;
            return this;
        }
    }

    private VungleSettings(Builder builder) {
        this.minimumSpaceForAd = builder.minimumSpaceForAd;
        this.minimumSpaceForInit = builder.minimumSpaceForInit;
        this.androidIdOptedOut = builder.androidIdOptedOut;
    }

    public long getMinimumSpaceForAd() {
        return this.minimumSpaceForAd;
    }

    public long getMinimumSpaceForInit() {
        return this.minimumSpaceForInit;
    }

    public boolean getAndroidIdOptOut() {
        return this.androidIdOptedOut;
    }
}
