package com.vungle.warren;

import android.support.annotation.Nullable;

public class RuntimeValues {
    @Nullable
    volatile HeaderBiddingCallback headerBiddingCallback;
    @Nullable
    volatile InitCallback initCallback;
    @Nullable
    volatile PublisherDirectDownload publisherDirectDownload;
    @Nullable
    volatile VungleSettings settings;
}
