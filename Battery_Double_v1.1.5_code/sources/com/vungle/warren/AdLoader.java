package com.vungle.warren;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import android.util.Log;
import android.webkit.URLUtil;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.mintegral.msdk.base.entity.CampaignUnit;
import com.vungle.warren.DownloadStrategy.VerificationCallback;
import com.vungle.warren.downloader.AssetDownloadListener;
import com.vungle.warren.downloader.AssetDownloadListener.DownloadError;
import com.vungle.warren.downloader.AssetDownloadListener.Progress;
import com.vungle.warren.downloader.AssetDownloader;
import com.vungle.warren.downloader.DownloadRequest;
import com.vungle.warren.error.VungleError;
import com.vungle.warren.error.VungleException;
import com.vungle.warren.error.VungleException.ExceptionCode;
import com.vungle.warren.model.AdAsset;
import com.vungle.warren.model.Advertisement;
import com.vungle.warren.model.Placement;
import com.vungle.warren.persistence.CacheManager;
import com.vungle.warren.persistence.DatabaseHelper.DBException;
import com.vungle.warren.persistence.Repository;
import com.vungle.warren.persistence.Repository.SaveCallback;
import com.vungle.warren.tasks.DownloadJob;
import com.vungle.warren.tasks.JobRunner;
import com.vungle.warren.ui.HackMraid;
import com.vungle.warren.utility.FileUtility;
import com.vungle.warren.utility.SDKExecutors;
import com.vungle.warren.utility.UnzipUtility;
import com.vungle.warren.utility.UnzipUtility.Filter;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AdLoader {
    public static final long EXPONENTIAL_RATE = 2;
    public static final int RETRY_COUNT = 5;
    public static final long RETRY_DELAY = 2000;
    /* access modifiers changed from: private */
    public static final String TAG = AdLoader.class.getCanonicalName();
    /* access modifiers changed from: private */
    @NonNull
    public final CacheManager cacheManager;
    @NonNull
    private final AssetDownloader downloader;
    @Nullable
    private JobRunner jobRunner;
    /* access modifiers changed from: private */
    public final Map<String, Operation> loadOperations = new ConcurrentHashMap();
    private final Map<String, Operation> pendingOperations = new ConcurrentHashMap();
    /* access modifiers changed from: private */
    @NonNull
    public final Repository repository;
    /* access modifiers changed from: private */
    @NonNull
    public final RuntimeValues runtimeValues;
    /* access modifiers changed from: private */
    @NonNull
    public final SDKExecutors sdkExecutors;
    /* access modifiers changed from: private */
    @NonNull
    public final VungleStaticApi vungleApi;
    /* access modifiers changed from: private */
    @NonNull
    public final VungleApiClient vungleApiClient;

    @Retention(RetentionPolicy.SOURCE)
    public @interface ReschedulePolicy {
        public static final int EXPONENTIAL = 0;
        public static final int EXPONENTIAL_ENDLESS_AD = 1;
    }

    private class DownloadAdCallback implements DownloadCallback {
        private DownloadAdCallback() {
        }

        public void onDownloadCompleted(@NonNull String str, @NonNull String str2) {
            Advertisement advertisement;
            String access$400 = AdLoader.TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("download completed ");
            sb.append(str);
            Log.d(access$400, sb.toString());
            if (TextUtils.isEmpty(str2)) {
                advertisement = null;
            } else {
                advertisement = (Advertisement) AdLoader.this.repository.load(str2, Advertisement.class).get();
            }
            Placement placement = (Placement) AdLoader.this.repository.load(str, Placement.class).get();
            if (advertisement == null || placement == null) {
                onDownloadFailed(new IllegalStateException("Didn't find adv"), str, str2);
                return;
            }
            try {
                AdLoader.this.repository.saveAndApplyState(advertisement, str, 1);
                onReady(str, placement, advertisement);
            } catch (DBException unused) {
                onDownloadFailed(new VungleException(26), str, str2);
            }
        }

        public void onReady(@NonNull String str, @NonNull Placement placement, @NonNull Advertisement advertisement) {
            synchronized (AdLoader.this) {
                AdLoader.this.setLoading(str, false);
                HeaderBiddingCallback headerBiddingCallback = AdLoader.this.runtimeValues.headerBiddingCallback;
                if (headerBiddingCallback != null) {
                    headerBiddingCallback.adAvailableForBidToken(str, advertisement.getBidToken());
                }
                String access$400 = AdLoader.TAG;
                StringBuilder sb = new StringBuilder();
                sb.append("found already cached valid adv, calling onAdLoad ");
                sb.append(str);
                sb.append(" callback ");
                Log.i(access$400, sb.toString());
                if (placement.isAutoCached() && AdLoader.this.runtimeValues.initCallback != null) {
                    AdLoader.this.runtimeValues.initCallback.onAutoCacheAdAvailable(str);
                }
                Operation operation = (Operation) AdLoader.this.loadOperations.remove(str);
                if (operation != null) {
                    for (LoadAdCallback loadAdCallback : operation.loadAdCallbacks) {
                        if (loadAdCallback != null) {
                            loadAdCallback.onAdLoad(str);
                        }
                    }
                }
            }
        }

        /* JADX WARNING: Can't wrap try/catch for region: R(4:10|11|12|13) */
        /* JADX WARNING: Can't wrap try/catch for region: R(4:52|53|54|55) */
        /* JADX WARNING: Can't wrap try/catch for region: R(4:63|64|65|66) */
        /* JADX WARNING: Can't wrap try/catch for region: R(4:71|72|73|74) */
        /* JADX WARNING: Code restructure failed: missing block: B:82:0x015b, code lost:
            return;
         */
        /* JADX WARNING: Missing exception handler attribute for start block: B:12:0x0049 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:54:0x00dc */
        /* JADX WARNING: Missing exception handler attribute for start block: B:65:0x010f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:73:0x013d */
        /* JADX WARNING: Removed duplicated region for block: B:42:0x00b9  */
        /* JADX WARNING: Unknown top exception splitter block from list: {B:12:0x0049=Splitter:B:12:0x0049, B:54:0x00dc=Splitter:B:54:0x00dc, B:73:0x013d=Splitter:B:73:0x013d, B:65:0x010f=Splitter:B:65:0x010f} */
        public void onDownloadFailed(@NonNull Throwable th, String str, String str2) {
            Advertisement advertisement;
            int i;
            boolean z;
            boolean z2;
            synchronized (AdLoader.this) {
                Operation operation = (Operation) AdLoader.this.loadOperations.remove(str);
                Placement placement = (Placement) AdLoader.this.repository.load(str, Placement.class).get();
                if (str2 == null) {
                    advertisement = null;
                } else {
                    advertisement = (Advertisement) AdLoader.this.repository.load(str2, Advertisement.class).get();
                }
                int i2 = 0;
                if (placement == null) {
                    if (advertisement != null) {
                        AdLoader.this.repository.saveAndApplyState(advertisement, str, 4);
                        th = new VungleException(26);
                    }
                    for (LoadAdCallback onError : operation.loadAdCallbacks) {
                        onError.onError(str, th);
                    }
                    AdLoader.this.setLoading(str, false);
                    return;
                }
                if (th instanceof VungleException) {
                    int exceptionCode = ((VungleException) th).getExceptionCode();
                    if (!(exceptionCode == 1 || exceptionCode == 14)) {
                        if (exceptionCode != 20) {
                            if (exceptionCode != 25) {
                                switch (exceptionCode) {
                                    case 22:
                                        break;
                                    case 23:
                                        if (advertisement != null) {
                                            z2 = false;
                                            z = true;
                                            i = 0;
                                            break;
                                        }
                                        break;
                                }
                            }
                        }
                        z2 = false;
                        z = true;
                        i = 4;
                        StringBuilder sb = new StringBuilder();
                        sb.append("Failed to load ad/assets for ");
                        sb.append(str);
                        sb.append(". Cause:");
                        Log.e("Vungle", sb.toString(), th);
                        AdLoader.this.setLoading(str, false);
                        if (operation != null) {
                            switch (operation.policy) {
                                case 0:
                                    if (operation.retry < operation.retryLimit && z) {
                                        if (advertisement != null) {
                                            AdLoader.this.repository.saveAndApplyState(advertisement, str, i);
                                            th = new VungleException(26);
                                        }
                                        AdLoader.this.load(operation.delay(operation.retryDelay).retryDelay(operation.retryDelay * 2).retry(operation.retry + 1));
                                        return;
                                    }
                                case 1:
                                    if (!z2) {
                                        int i3 = operation.retry;
                                        if (i3 >= operation.retryLimit || !z) {
                                            i = 4;
                                        } else {
                                            i2 = i3 + 1;
                                        }
                                        if (advertisement != null) {
                                            AdLoader.this.repository.saveAndApplyState(advertisement, str, i);
                                            th = new VungleException(26);
                                        }
                                        AdLoader.this.load(operation.delay(operation.retryDelay).retryDelay(operation.retryDelay * 2).retry(i2));
                                        return;
                                    }
                            }
                            if (advertisement != null) {
                                AdLoader.this.repository.saveAndApplyState(advertisement, str, 4);
                                th = new VungleException(26);
                            }
                            for (LoadAdCallback loadAdCallback : operation.loadAdCallbacks) {
                                if (loadAdCallback != null) {
                                    loadAdCallback.onError(str, th);
                                }
                            }
                        }
                    }
                    z2 = true;
                    z = false;
                    i = 4;
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("Failed to load ad/assets for ");
                    sb2.append(str);
                    sb2.append(". Cause:");
                    Log.e("Vungle", sb2.toString(), th);
                    AdLoader.this.setLoading(str, false);
                    if (operation != null) {
                    }
                }
                z2 = false;
                z = false;
                i = 4;
                StringBuilder sb22 = new StringBuilder();
                sb22.append("Failed to load ad/assets for ");
                sb22.append(str);
                sb22.append(". Cause:");
                Log.e("Vungle", sb22.toString(), th);
                AdLoader.this.setLoading(str, false);
                if (operation != null) {
                }
            }
        }
    }

    interface DownloadCallback {
        void onDownloadCompleted(@NonNull String str, @NonNull String str2);

        void onDownloadFailed(@NonNull Throwable th, @Nullable String str, @Nullable String str2);

        void onReady(@NonNull String str, @NonNull Placement placement, @NonNull Advertisement advertisement);
    }

    static class Operation {
        boolean cancelable;
        final long delay;
        @Nullable
        final PublisherDirectDownload directDownload;
        final String id;
        @NonNull
        final Set<LoadAdCallback> loadAdCallbacks = new CopyOnWriteArraySet();
        @NonNull
        final AtomicBoolean loading;
        final int policy;
        final int retry;
        final long retryDelay;
        final int retryLimit;

        public Operation(String str, long j, long j2, int i, int i2, @Nullable PublisherDirectDownload publisherDirectDownload, int i3, boolean z, @Nullable LoadAdCallback... loadAdCallbackArr) {
            this.id = str;
            this.delay = j;
            this.retryDelay = j2;
            this.retryLimit = i;
            this.policy = i2;
            this.directDownload = publisherDirectDownload;
            this.retry = i3;
            this.loading = new AtomicBoolean();
            this.cancelable = z;
            if (loadAdCallbackArr != null) {
                this.loadAdCallbacks.addAll(Arrays.asList(loadAdCallbackArr));
            }
        }

        /* access modifiers changed from: 0000 */
        public Operation delay(long j) {
            Operation operation = new Operation(this.id, j, this.retryDelay, this.retryLimit, this.policy, this.directDownload, this.retry, this.cancelable, (LoadAdCallback[]) this.loadAdCallbacks.toArray(new LoadAdCallback[0]));
            return operation;
        }

        /* access modifiers changed from: 0000 */
        public Operation retryDelay(long j) {
            Operation operation = new Operation(this.id, this.delay, j, this.retryLimit, this.policy, this.directDownload, this.retry, this.cancelable, (LoadAdCallback[]) this.loadAdCallbacks.toArray(new LoadAdCallback[0]));
            return operation;
        }

        /* access modifiers changed from: 0000 */
        public Operation retry(int i) {
            Operation operation = new Operation(this.id, this.delay, this.retryDelay, this.retryLimit, this.policy, this.directDownload, i, this.cancelable, (LoadAdCallback[]) this.loadAdCallbacks.toArray(new LoadAdCallback[0]));
            return operation;
        }
    }

    private boolean recoverableServerCode(int i) {
        return i == 408 || (500 <= i && i < 600);
    }

    public void setJobRunner(@NonNull JobRunner jobRunner2) {
        this.jobRunner = jobRunner2;
    }

    public AdLoader(@NonNull SDKExecutors sDKExecutors, @NonNull Repository repository2, @NonNull VungleApiClient vungleApiClient2, @NonNull CacheManager cacheManager2, @NonNull AssetDownloader assetDownloader, @NonNull RuntimeValues runtimeValues2, @NonNull VungleStaticApi vungleStaticApi) {
        this.sdkExecutors = sDKExecutors;
        this.repository = repository2;
        this.vungleApiClient = vungleApiClient2;
        this.cacheManager = cacheManager2;
        this.downloader = assetDownloader;
        this.runtimeValues = runtimeValues2;
        this.vungleApi = vungleStaticApi;
    }

    /* access modifiers changed from: private */
    public boolean canReDownload(Advertisement advertisement) {
        if (advertisement == null || (advertisement.getState() != 0 && advertisement.getState() != 1)) {
            return false;
        }
        List<AdAsset> list = (List) this.repository.loadAllAdAssets(advertisement.getId()).get();
        if (list.size() == 0) {
            return false;
        }
        for (AdAsset adAsset : list) {
            if (adAsset.fileType == 1) {
                if (!fileIsValid(new File(adAsset.localPath), adAsset)) {
                    return false;
                }
            } else if (TextUtils.isEmpty(adAsset.serverPath)) {
                return false;
            }
        }
        return true;
    }

    public boolean canPlayAd(Advertisement advertisement) {
        boolean z = false;
        if (advertisement == null) {
            return false;
        }
        if (advertisement.getState() == 1 && hasAssetsFor(advertisement.getId())) {
            z = true;
        }
        return z;
    }

    public synchronized void clear() {
        HashSet<String> hashSet = new HashSet<>();
        hashSet.addAll(this.loadOperations.keySet());
        hashSet.addAll(this.pendingOperations.keySet());
        for (String str : hashSet) {
            onError((Operation) this.loadOperations.remove(str), 25);
            onError((Operation) this.pendingOperations.remove(str), 25);
        }
    }

    public boolean isLoading(String str) {
        Operation operation = (Operation) this.loadOperations.get(str);
        return operation != null && operation.loading.get();
    }

    /* access modifiers changed from: private */
    public void setLoading(String str, boolean z) {
        Operation operation = (Operation) this.loadOperations.get(str);
        if (operation != null) {
            operation.loading.set(z);
        }
    }

    public synchronized void loadPendingInternal(String str) {
        Operation operation = (Operation) this.pendingOperations.remove(str);
        if (operation != null) {
            load(operation.delay(0));
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0085, code lost:
        return;
     */
    public synchronized void load(@NonNull Operation operation) {
        if (this.jobRunner == null) {
            onError(operation, 9);
            return;
        }
        Operation operation2 = (Operation) this.pendingOperations.remove(operation.id);
        if (operation2 != null) {
            operation.loadAdCallbacks.addAll(operation2.loadAdCallbacks);
        }
        if (operation.delay <= 0) {
            Operation operation3 = (Operation) this.loadOperations.get(operation.id);
            if (operation3 != null) {
                operation.loadAdCallbacks.addAll(operation3.loadAdCallbacks);
                this.loadOperations.put(operation.id, operation);
                setLoading(operation.id, true);
            } else {
                this.loadOperations.put(operation.id, operation);
                loadAd(operation.id, new DownloadCallbackWrapper(this.sdkExecutors.getVungleExecutor(), new DownloadAdCallback()), operation.directDownload);
            }
        } else {
            this.pendingOperations.put(operation.id, operation);
            this.jobRunner.execute(DownloadJob.makeJobInfo(operation.id).setDelay(operation.delay).setUpdateCurrent(true));
        }
    }

    private void onError(@Nullable Operation operation, @ExceptionCode int i) {
        if (operation != null) {
            for (LoadAdCallback loadAdCallback : operation.loadAdCallbacks) {
                if (loadAdCallback != null) {
                    loadAdCallback.onError(operation.id, new VungleException(i));
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public Throwable reposeCodeToVungleException(int i) {
        if (recoverableServerCode(i)) {
            return new VungleException(22);
        }
        return new VungleException(21);
    }

    /* access modifiers changed from: private */
    public Throwable retrofitToVungleException(Throwable th) {
        if (th instanceof IllegalArgumentException) {
            return new VungleError(10);
        }
        return (!(th instanceof UnknownHostException) && (th instanceof IOException)) ? new VungleException(20) : th;
    }

    private void loadAd(@NonNull final String str, @NonNull final DownloadCallbackWrapper downloadCallbackWrapper, final PublisherDirectDownload publisherDirectDownload) {
        this.sdkExecutors.getVungleExecutor().execute(new Runnable() {
            public void run() {
                if (!AdLoader.this.vungleApi.isInitialized()) {
                    downloadCallbackWrapper.onDownloadFailed(new VungleException(9), str, null);
                    return;
                }
                Placement placement = (Placement) AdLoader.this.repository.load(str, Placement.class).get();
                if (placement == null) {
                    DownloadCallbackWrapper downloadCallbackWrapper = downloadCallbackWrapper;
                    StringBuilder sb = new StringBuilder();
                    sb.append("Placement ID ");
                    sb.append(str);
                    sb.append(" is not valid. Please check your configuration on the vungle dashboard.");
                    downloadCallbackWrapper.onDownloadFailed(new IllegalArgumentException(sb.toString()), str, null);
                    return;
                }
                Advertisement advertisement = (Advertisement) AdLoader.this.repository.findValidAdvertisementForPlacement(placement.getId()).get();
                if (AdLoader.this.canPlayAd(advertisement)) {
                    downloadCallbackWrapper.onReady(str, placement, advertisement);
                } else if (AdLoader.this.canReDownload(advertisement)) {
                    Log.d(AdLoader.TAG, "Found valid adv but not ready - downloading content");
                    if (AdLoader.this.cacheManager.getBytesAvailable() < AdLoader.this.runtimeValues.settings.getMinimumSpaceForAd()) {
                        if (advertisement.getState() != 4) {
                            try {
                                AdLoader.this.repository.saveAndApplyState(advertisement, str, 4);
                            } catch (DBException unused) {
                                downloadCallbackWrapper.onDownloadFailed(new VungleException(26), str, null);
                                return;
                            }
                        }
                        downloadCallbackWrapper.onDownloadFailed(new VungleException(19), str, null);
                        return;
                    }
                    AdLoader.this.setLoading(str, true);
                    if (advertisement.getState() != 0) {
                        try {
                            AdLoader.this.repository.saveAndApplyState(advertisement, str, 0);
                        } catch (DBException unused2) {
                            downloadCallbackWrapper.onDownloadFailed(new VungleException(26), str, null);
                            return;
                        }
                    }
                    AdLoader.this.downloadAdContent(str, advertisement, publisherDirectDownload, downloadCallbackWrapper);
                } else if (placement.getWakeupTime() > System.currentTimeMillis()) {
                    downloadCallbackWrapper.onDownloadFailed(new VungleException(1), str, null);
                    String access$400 = AdLoader.TAG;
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("Placement ");
                    sb2.append(placement.getId());
                    sb2.append(" is  snoozed");
                    Log.w(access$400, sb2.toString());
                    if (placement.isAutoCached()) {
                        String access$4002 = AdLoader.TAG;
                        StringBuilder sb3 = new StringBuilder();
                        sb3.append("Placement ");
                        sb3.append(placement.getId());
                        sb3.append(" is sleeping rescheduling it ");
                        Log.d(access$4002, sb3.toString());
                        AdLoader.this.loadEndless(placement.getId(), placement.getWakeupTime() - System.currentTimeMillis(), publisherDirectDownload);
                    }
                } else {
                    String access$4003 = AdLoader.TAG;
                    StringBuilder sb4 = new StringBuilder();
                    sb4.append("didn't find cached adv for ");
                    sb4.append(str);
                    sb4.append(" downloading ");
                    Log.i(access$4003, sb4.toString());
                    if (advertisement != null) {
                        try {
                            AdLoader.this.repository.saveAndApplyState(advertisement, str, 4);
                        } catch (DBException unused3) {
                            downloadCallbackWrapper.onDownloadFailed(new VungleException(26), str, null);
                            return;
                        }
                    }
                    if (AdLoader.this.runtimeValues.settings == null || AdLoader.this.cacheManager.getBytesAvailable() >= AdLoader.this.runtimeValues.settings.getMinimumSpaceForAd()) {
                        String access$4004 = AdLoader.TAG;
                        StringBuilder sb5 = new StringBuilder();
                        sb5.append("No adv for placement ");
                        sb5.append(placement.getId());
                        sb5.append(" getting new data ");
                        Log.d(access$4004, sb5.toString());
                        AdLoader.this.setLoading(str, true);
                        AdLoader.this.fetchAdMetadata(str, publisherDirectDownload, downloadCallbackWrapper, AdLoader.this.runtimeValues.headerBiddingCallback);
                    } else {
                        downloadCallbackWrapper.onDownloadFailed(new VungleException((placement == null || !placement.isAutoCached()) ? 17 : 18), str, null);
                    }
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void fetchAdMetadata(String str, PublisherDirectDownload publisherDirectDownload, @NonNull DownloadCallback downloadCallback, @Nullable HeaderBiddingCallback headerBiddingCallback) {
        Call requestAd = this.vungleApiClient.requestAd(str, headerBiddingCallback != null);
        final String str2 = str;
        final DownloadCallback downloadCallback2 = downloadCallback;
        final HeaderBiddingCallback headerBiddingCallback2 = headerBiddingCallback;
        final PublisherDirectDownload publisherDirectDownload2 = publisherDirectDownload;
        AnonymousClass2 r1 = new Callback<JsonObject>() {
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull final Response<JsonObject> response) {
                AdLoader.this.sdkExecutors.getVungleExecutor().execute(new Runnable() {
                    public void run() {
                        int i;
                        if (!response.isSuccessful()) {
                            long retryAfterHeaderValue = AdLoader.this.vungleApiClient.getRetryAfterHeaderValue(response);
                            Placement placement = (Placement) AdLoader.this.repository.load(str2, Placement.class).get();
                            if (retryAfterHeaderValue <= 0 || placement == null || !placement.isAutoCached()) {
                                Log.e(AdLoader.TAG, "Failed to retrieve advertisement information");
                                downloadCallback2.onDownloadFailed(AdLoader.this.reposeCodeToVungleException(response.code()), str2, null);
                                return;
                            }
                            AdLoader.this.loadEndless(str2, retryAfterHeaderValue);
                            downloadCallback2.onDownloadFailed(new VungleException(14), str2, null);
                            return;
                        }
                        Placement placement2 = (Placement) AdLoader.this.repository.load(str2, Placement.class).get();
                        if (placement2 == null) {
                            Log.e(AdLoader.TAG, "Placement metadata not found for requested advertisement.");
                            downloadCallback2.onDownloadFailed(new VungleException(2), str2, null);
                            return;
                        }
                        JsonObject jsonObject = (JsonObject) response.body();
                        if (jsonObject == null || !jsonObject.has(CampaignUnit.JSON_KEY_ADS) || jsonObject.get(CampaignUnit.JSON_KEY_ADS).isJsonNull()) {
                            downloadCallback2.onDownloadFailed(new VungleError(0), str2, null);
                        } else {
                            JsonArray asJsonArray = jsonObject.getAsJsonArray(CampaignUnit.JSON_KEY_ADS);
                            if (asJsonArray == null || asJsonArray.size() == 0) {
                                downloadCallback2.onDownloadFailed(new VungleException(1), str2, null);
                                return;
                            }
                            JsonObject asJsonObject = asJsonArray.get(0).getAsJsonObject();
                            try {
                                Advertisement advertisement = new Advertisement(asJsonObject);
                                if (headerBiddingCallback2 != null) {
                                    headerBiddingCallback2.onBidTokenAvailable(str2, advertisement.getBidToken());
                                }
                                AdLoader.this.repository.deleteAdvertisement(advertisement.getId());
                                File file = (File) AdLoader.this.repository.getAdvertisementAssetDirectory(advertisement.getId()).get();
                                for (Entry entry : advertisement.getDownloadableUrls().entrySet()) {
                                    if (!URLUtil.isHttpsUrl((String) entry.getValue())) {
                                        if (!URLUtil.isHttpUrl((String) entry.getValue())) {
                                            downloadCallback2.onDownloadFailed(new VungleError(10), str2, advertisement.getId());
                                            return;
                                        }
                                    }
                                    StringBuilder sb = new StringBuilder();
                                    sb.append(file.getPath());
                                    sb.append(File.separator);
                                    sb.append((String) entry.getKey());
                                    String sb2 = sb.toString();
                                    String str = (String) entry.getValue();
                                    if (!sb2.endsWith("postroll")) {
                                        if (!sb2.endsWith("template")) {
                                            i = 2;
                                            AdAsset adAsset = new AdAsset(advertisement.getId(), str, sb2);
                                            adAsset.status = 0;
                                            adAsset.fileType = i;
                                            AdLoader.this.repository.save(adAsset);
                                        }
                                    }
                                    i = 0;
                                    AdAsset adAsset2 = new AdAsset(advertisement.getId(), str, sb2);
                                    adAsset2.status = 0;
                                    adAsset2.fileType = i;
                                    AdLoader.this.repository.save(adAsset2);
                                }
                                AdLoader.this.repository.saveAndApplyState(advertisement, str2, 0);
                                AdLoader.this.downloadAdContent(str2, advertisement, publisherDirectDownload2, downloadCallback2);
                            } catch (IllegalArgumentException unused) {
                                JsonObject asJsonObject2 = asJsonObject.getAsJsonObject("ad_markup");
                                if (asJsonObject2.has("sleep")) {
                                    int asInt = asJsonObject2.get("sleep").getAsInt();
                                    placement2.snooze((long) asInt);
                                    try {
                                        AdLoader.this.repository.save(placement2);
                                        if (placement2.isAutoCached()) {
                                            AdLoader.this.loadEndless(str2, (long) (asInt * 1000));
                                        }
                                    } catch (DBException unused2) {
                                        downloadCallback2.onDownloadFailed(new VungleException(26), str2, null);
                                        return;
                                    }
                                }
                                downloadCallback2.onDownloadFailed(new VungleException(1), str2, null);
                            } catch (DBException unused3) {
                                downloadCallback2.onDownloadFailed(new VungleException(26), str2, null);
                            }
                        }
                    }
                });
            }

            public void onFailure(@NonNull Call<JsonObject> call, @NonNull Throwable th) {
                downloadCallback2.onDownloadFailed(AdLoader.this.retrofitToVungleException(th), str2, null);
            }
        };
        requestAd.enqueue(r1);
    }

    /* access modifiers changed from: private */
    public void downloadAdContent(final String str, final Advertisement advertisement, PublisherDirectDownload publisherDirectDownload, @NonNull final DownloadCallback downloadCallback) {
        DirectDownloadStrategy directDownloadStrategy = (publisherDirectDownload == null || TextUtils.isEmpty(advertisement.getAdMarketId())) ? null : new DirectDownloadStrategy(publisherDirectDownload);
        if (directDownloadStrategy != null) {
            directDownloadStrategy.isApplicationAvailable(advertisement.getAdMarketId(), new VerificationCallback() {
                public void onResult(final boolean z) {
                    AdLoader.this.sdkExecutors.getVungleExecutor().execute(new Runnable() {
                        public void run() {
                            if (z) {
                                Log.d(AdLoader.TAG, "fetchAdMetadata: downloading assets ");
                                AdLoader.this.downloadAdAssets(advertisement, downloadCallback, str);
                                return;
                            }
                            downloadCallback.onDownloadFailed(new VungleException(5), str, null);
                        }
                    });
                }
            });
        } else {
            downloadAdAssets(advertisement, downloadCallback, str);
        }
    }

    /* access modifiers changed from: private */
    public void downloadAdAssets(Advertisement advertisement, DownloadCallback downloadCallback, String str) {
        for (Entry entry : advertisement.getDownloadableUrls().entrySet()) {
            if (TextUtils.isEmpty((CharSequence) entry.getKey()) || TextUtils.isEmpty((CharSequence) entry.getValue())) {
                downloadCallback.onDownloadFailed(new VungleException(11), str, null);
                String str2 = TAG;
                StringBuilder sb = new StringBuilder();
                sb.append("Aborting, Failed to download Ad assets for: ");
                sb.append(advertisement.getId());
                Log.e(str2, sb.toString());
                return;
            } else if (!URLUtil.isValidUrl((String) entry.getValue())) {
                downloadCallback.onDownloadFailed(new VungleException(11), str, null);
                String str22 = TAG;
                StringBuilder sb2 = new StringBuilder();
                sb2.append("Aborting, Failed to download Ad assets for: ");
                sb2.append(advertisement.getId());
                Log.e(str22, sb2.toString());
                return;
            }
        }
        DownloadCallbackWrapper downloadCallbackWrapper = new DownloadCallbackWrapper(this.sdkExecutors.getUIExecutor(), downloadCallback);
        AtomicInteger atomicInteger = new AtomicInteger();
        ArrayList<DownloadRequest> arrayList = new ArrayList<>();
        AssetDownloadListener assetDownloadListener = getAssetDownloadListener(advertisement, str, downloadCallbackWrapper, atomicInteger);
        for (AdAsset adAsset : (List) this.repository.loadAllAdAssets(advertisement.getId()).get()) {
            if (adAsset.status == 3) {
                if (fileIsValid(new File(adAsset.localPath), adAsset)) {
                    continue;
                } else if (adAsset.fileType == 1) {
                    downloadCallbackWrapper.onDownloadFailed(new VungleException(24), str, advertisement.getId());
                    return;
                }
            }
            if (adAsset.status != 4 || adAsset.fileType != 0) {
                if (TextUtils.isEmpty(adAsset.serverPath)) {
                    downloadCallbackWrapper.onDownloadFailed(new VungleException(24), str, advertisement.getId());
                    return;
                }
                DownloadRequest downloadRequest = new DownloadRequest(adAsset.serverPath, adAsset.localPath, adAsset.identifier);
                if (adAsset.status == 1) {
                    this.downloader.cancelAndAwait(downloadRequest, 1000);
                }
                String str3 = TAG;
                StringBuilder sb3 = new StringBuilder();
                sb3.append("Starting download for ");
                sb3.append(adAsset);
                Log.d(str3, sb3.toString());
                adAsset.status = 1;
                try {
                    this.repository.save(adAsset);
                    arrayList.add(downloadRequest);
                } catch (DBException unused) {
                    downloadCallbackWrapper.onDownloadFailed(new VungleException(26), str, advertisement.getId());
                    return;
                }
            }
        }
        if (arrayList.size() == 0) {
            onAssetDownloadFinished(str, downloadCallbackWrapper, advertisement, Collections.EMPTY_LIST);
            return;
        }
        atomicInteger.set(arrayList.size());
        for (DownloadRequest download : arrayList) {
            this.downloader.download(download, assetDownloadListener);
        }
    }

    @NonNull
    private AssetDownloadListener getAssetDownloadListener(Advertisement advertisement, String str, DownloadCallback downloadCallback, AtomicInteger atomicInteger) {
        final AtomicInteger atomicInteger2 = atomicInteger;
        final String str2 = str;
        final DownloadCallback downloadCallback2 = downloadCallback;
        final Advertisement advertisement2 = advertisement;
        AnonymousClass4 r0 = new AssetDownloadListener() {
            List<DownloadError> errors = Collections.synchronizedList(new ArrayList());

            public void onProgress(@NonNull Progress progress, @NonNull DownloadRequest downloadRequest) {
            }

            public void onError(@NonNull final DownloadError downloadError, @Nullable final DownloadRequest downloadRequest) {
                AdLoader.this.sdkExecutors.getVungleExecutor().execute(new Runnable() {
                    public void run() {
                        Log.e(AdLoader.TAG, "Download Failed");
                        if (downloadRequest != null) {
                            String str = downloadRequest.cookieString;
                            AdAsset adAsset = TextUtils.isEmpty(str) ? null : (AdAsset) AdLoader.this.repository.load(str, AdAsset.class).get();
                            if (adAsset != null) {
                                AnonymousClass4.this.errors.add(downloadError);
                                adAsset.status = 2;
                                try {
                                    AdLoader.this.repository.save(adAsset);
                                } catch (DBException unused) {
                                    AnonymousClass4.this.errors.add(new DownloadError(-1, new VungleException(26), 4));
                                }
                            } else {
                                AnonymousClass4.this.errors.add(new DownloadError(-1, new IOException("Downloaded file not found!"), 1));
                            }
                        } else {
                            AnonymousClass4.this.errors.add(new DownloadError(-1, new RuntimeException("error in request"), 4));
                        }
                        if (atomicInteger2.decrementAndGet() <= 0) {
                            AdLoader.this.onAssetDownloadFinished(str2, downloadCallback2, advertisement2, AnonymousClass4.this.errors);
                        }
                    }
                });
            }

            public void onSuccess(@NonNull final File file, @NonNull final DownloadRequest downloadRequest) {
                AdLoader.this.sdkExecutors.getVungleExecutor().execute(new Runnable() {
                    public void run() {
                        AdAsset adAsset;
                        String str = downloadRequest.cookieString;
                        if (str == null) {
                            adAsset = null;
                        } else {
                            adAsset = (AdAsset) AdLoader.this.repository.load(str, AdAsset.class).get();
                        }
                        if (!file.exists()) {
                            AnonymousClass4.this.onError(new DownloadError(-1, new IOException("Downloaded file not found!"), 3), downloadRequest);
                        } else if (adAsset == null) {
                            AnonymousClass4.this.onError(new DownloadError(-1, new IOException("Downloaded file not found!"), 1), downloadRequest);
                        } else {
                            adAsset.fileType = AdLoader.this.isZip(file) ? 0 : 2;
                            adAsset.fileSize = file.length();
                            adAsset.status = 3;
                            try {
                                AdLoader.this.repository.save(adAsset);
                                if (atomicInteger2.decrementAndGet() <= 0) {
                                    AdLoader.this.onAssetDownloadFinished(str2, downloadCallback2, advertisement2, AnonymousClass4.this.errors);
                                }
                            } catch (DBException unused) {
                                AnonymousClass4.this.onError(new DownloadError(-1, new VungleException(26), 4), downloadRequest);
                            }
                        }
                    }
                });
            }
        };
        return r0;
    }

    /* access modifiers changed from: private */
    public boolean isZip(File file) {
        return file.getName().equals("postroll") || file.getName().equals("template");
    }

    /* access modifiers changed from: private */
    public void onAssetDownloadFinished(@NonNull String str, @NonNull DownloadCallback downloadCallback, @NonNull Advertisement advertisement, @NonNull List<DownloadError> list) {
        if (list.isEmpty()) {
            List<AdAsset> list2 = (List) this.repository.loadAllAdAssets(advertisement.getId()).get();
            if (list2.size() == 0) {
                downloadCallback.onDownloadFailed(new VungleException(24), str, advertisement.getId());
            }
            for (AdAsset adAsset : list2) {
                if (adAsset.status == 3) {
                    File file = new File(adAsset.localPath);
                    if (!fileIsValid(file, adAsset)) {
                        downloadCallback.onDownloadFailed(new VungleException(24), str, advertisement.getId());
                        return;
                    } else if (adAsset.fileType == 0) {
                        try {
                            unzipFile(advertisement, adAsset, file, list2);
                        } catch (DBException | IOException unused) {
                            downloadCallback.onDownloadFailed(new VungleException(24), str, advertisement.getId());
                            return;
                        }
                    }
                } else if (adAsset.fileType == 0 && adAsset.status != 4) {
                    downloadCallback.onDownloadFailed(new VungleException(24), str, advertisement.getId());
                    return;
                }
            }
            if (advertisement.getAdType() == 1) {
                File file2 = (File) this.repository.getAdvertisementAssetDirectory(advertisement.getId()).get();
                String str2 = TAG;
                StringBuilder sb = new StringBuilder();
                sb.append("saving MRAID for ");
                sb.append(advertisement.getId());
                Log.d(str2, sb.toString());
                advertisement.setMraidAssetDir(file2);
                try {
                    this.repository.save(advertisement);
                } catch (DBException unused2) {
                    downloadCallback.onDownloadFailed(new VungleException(26), str, advertisement.getId());
                    return;
                }
            }
            downloadCallback.onDownloadCompleted(str, advertisement.getId());
        } else {
            VungleException vungleException = null;
            Iterator it = list.iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                DownloadError downloadError = (DownloadError) it.next();
                if (VungleException.getExceptionCode(downloadError.cause) != 26) {
                    if (recoverableServerCode(downloadError.serverCode) && downloadError.reason == 1) {
                        vungleException = new VungleException(23);
                    } else if (downloadError.reason == 0) {
                        vungleException = new VungleException(23);
                    } else {
                        vungleException = new VungleException(24);
                    }
                    if (vungleException.getExceptionCode() == 24) {
                        break;
                    }
                } else {
                    vungleException = new VungleException(26);
                    break;
                }
            }
            downloadCallback.onDownloadFailed(vungleException, str, advertisement.getId());
        }
    }

    public void load(String str, LoadAdCallback loadAdCallback, PublisherDirectDownload publisherDirectDownload) {
        Operation operation = new Operation(str, 0, 2000, 5, 0, publisherDirectDownload, 0, false, loadAdCallback);
        load(operation);
    }

    public void loadEndless(String str, long j, PublisherDirectDownload publisherDirectDownload) {
        Operation operation = new Operation(str, j, 2000, 5, 1, publisherDirectDownload, 0, true, new LoadAdCallback[0]);
        load(operation);
    }

    public void loadEndless(String str, long j) {
        loadEndless(str, j, this.runtimeValues.publisherDirectDownload);
    }

    private void unzipFile(Advertisement advertisement, AdAsset adAsset, @NonNull final File file, List<AdAsset> list) throws IOException, DBException {
        File file2 = (File) this.repository.getAdvertisementAssetDirectory(advertisement.getId()).get();
        final ArrayList arrayList = new ArrayList();
        for (AdAsset adAsset2 : list) {
            if (adAsset2.fileType == 2) {
                arrayList.add(adAsset2.localPath);
            }
        }
        List<File> unzip = UnzipUtility.unzip(file.getPath(), file2.getPath(), new Filter() {
            public boolean matches(String str) {
                File file = new File(str);
                for (String file2 : arrayList) {
                    File file3 = new File(file2);
                    if (file3.equals(file)) {
                        return false;
                    }
                    String path = file.getPath();
                    StringBuilder sb = new StringBuilder();
                    sb.append(file3.getPath());
                    sb.append(File.separator);
                    if (path.startsWith(sb.toString())) {
                        return false;
                    }
                }
                return true;
            }
        });
        if (file.getName().equals("template")) {
            StringBuilder sb = new StringBuilder();
            sb.append(file2.getPath());
            sb.append(File.separator);
            sb.append("mraid.js");
            File file3 = new File(sb.toString());
            if (file3.exists()) {
                PrintWriter printWriter = new PrintWriter(new BufferedWriter(new FileWriter(file3, true)));
                printWriter.println(HackMraid.VALUE);
                printWriter.close();
            }
        }
        for (File file4 : unzip) {
            AdAsset adAsset3 = new AdAsset(advertisement.getId(), null, file4.getPath());
            adAsset3.fileSize = file4.length();
            adAsset3.fileType = 1;
            adAsset3.parentId = adAsset.identifier;
            adAsset3.status = 3;
            this.repository.save(adAsset3);
        }
        String str = TAG;
        StringBuilder sb2 = new StringBuilder();
        sb2.append("Uzipped ");
        sb2.append(file2);
        Log.d(str, sb2.toString());
        FileUtility.printDirectoryTree(file2);
        adAsset.status = 4;
        this.repository.save(adAsset, new SaveCallback() {
            public void onError(Exception exc) {
            }

            public void onSaved() {
                AdLoader.this.sdkExecutors.getVungleExecutor().execute(new Runnable() {
                    public void run() {
                        try {
                            FileUtility.delete(file);
                        } catch (IOException e) {
                            Log.e(AdLoader.TAG, "Error on deleting zip assets archive", e);
                        }
                    }
                });
            }
        });
    }

    public boolean hasAssetsFor(String str) throws IllegalStateException {
        List list = (List) this.repository.loadAllAdAssets(str).get();
        boolean z = false;
        if (list == null || list.size() == 0) {
            return false;
        }
        Iterator it = list.iterator();
        while (true) {
            if (!it.hasNext()) {
                z = true;
                break;
            }
            AdAsset adAsset = (AdAsset) it.next();
            if (adAsset.fileType != 0) {
                if (adAsset.status != 3 || !fileIsValid(new File(adAsset.localPath), adAsset)) {
                    break;
                }
            } else if (adAsset.status != 4) {
                break;
            }
        }
        return z;
    }

    private boolean fileIsValid(File file, AdAsset adAsset) {
        return file.exists() && file.length() == adAsset.fileSize;
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public Collection<Operation> getPendingOperations() {
        return this.pendingOperations.values();
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public Collection<Operation> getRunningOperations() {
        return this.loadOperations.values();
    }
}
