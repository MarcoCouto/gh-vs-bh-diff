package com.vungle.warren;

import java.util.concurrent.ExecutorService;

class PlayAdCallbackWrapper implements PlayAdCallback {
    private final ExecutorService executorService;
    /* access modifiers changed from: private */
    public final PlayAdCallback playAdCallback;

    public PlayAdCallbackWrapper(ExecutorService executorService2, PlayAdCallback playAdCallback2) {
        this.playAdCallback = playAdCallback2;
        this.executorService = executorService2;
    }

    public void onAdStart(final String str) {
        if (this.playAdCallback != null) {
            this.executorService.execute(new Runnable() {
                public void run() {
                    PlayAdCallbackWrapper.this.playAdCallback.onAdStart(str);
                }
            });
        }
    }

    public void onAdEnd(final String str, final boolean z, final boolean z2) {
        if (this.playAdCallback != null) {
            this.executorService.execute(new Runnable() {
                public void run() {
                    PlayAdCallbackWrapper.this.playAdCallback.onAdEnd(str, z, z2);
                }
            });
        }
    }

    public void onError(final String str, final Throwable th) {
        if (this.playAdCallback != null) {
            this.executorService.execute(new Runnable() {
                public void run() {
                    PlayAdCallbackWrapper.this.playAdCallback.onError(str, th);
                }
            });
        }
    }
}
