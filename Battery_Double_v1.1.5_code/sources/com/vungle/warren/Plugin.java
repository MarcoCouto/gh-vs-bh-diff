package com.vungle.warren;

import android.util.Log;
import com.vungle.warren.VungleApiClient.WrapperFramework;

public class Plugin {
    private static final String TAG = "Plugin";

    public static void addWrapperInfo(WrapperFramework wrapperFramework, String str) {
        if (wrapperFramework == null || wrapperFramework == WrapperFramework.none) {
            Log.e(TAG, "Wrapper is null or is not none");
        } else {
            StringBuilder sb = new StringBuilder();
            sb.append(VungleApiClient.HEADER_UA);
            sb.append(";");
            sb.append(wrapperFramework);
            VungleApiClient.HEADER_UA = sb.toString();
            if (str == null || str.isEmpty()) {
                Log.e(TAG, "Wrapper framework version is empty");
            } else {
                StringBuilder sb2 = new StringBuilder();
                sb2.append(VungleApiClient.HEADER_UA);
                sb2.append("/");
                sb2.append(str);
                VungleApiClient.HEADER_UA = sb2.toString();
            }
        }
        if (Vungle.isInitialized()) {
            Log.w(TAG, "VUNGLE WARNING: SDK already initialized, wou should set wrapper info before");
        }
    }
}
