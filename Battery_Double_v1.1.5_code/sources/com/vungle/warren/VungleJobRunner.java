package com.vungle.warren;

import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import com.vungle.warren.tasks.JobCreator;
import com.vungle.warren.tasks.JobInfo;
import com.vungle.warren.tasks.JobRunner;
import com.vungle.warren.tasks.runnable.JobRunnable;
import com.vungle.warren.tasks.utility.ThreadPriorityHelper;
import java.util.concurrent.Executor;

class VungleJobRunner implements JobRunner {
    private static final String TAG = VungleJobRunner.class.getSimpleName();
    private static Handler handler = new Handler(Looper.getMainLooper());
    private JobCreator creator;
    /* access modifiers changed from: private */
    public Executor executor;
    private final ThreadPriorityHelper threadPriorityHelper;

    VungleJobRunner(@NonNull JobCreator jobCreator, @NonNull Executor executor2, @Nullable ThreadPriorityHelper threadPriorityHelper2) {
        this.creator = jobCreator;
        this.executor = executor2;
        this.threadPriorityHelper = threadPriorityHelper2;
    }

    public void execute(@NonNull JobInfo jobInfo) {
        JobInfo copy = jobInfo.copy();
        String jobTag = copy.getJobTag();
        long delay = copy.getDelay();
        copy.setDelay(0);
        final JobRunnable jobRunnable = new JobRunnable(copy, this.creator, this, this.threadPriorityHelper);
        if (delay <= 0) {
            this.executor.execute(jobRunnable);
            return;
        }
        if (copy.getUpdateCurrent()) {
            String str = TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("replacing pending job with new ");
            sb.append(jobTag);
            Log.d(str, sb.toString());
            handler.removeCallbacksAndMessages(jobTag);
        }
        handler.postAtTime(new Runnable() {
            public void run() {
                VungleJobRunner.this.executor.execute(jobRunnable);
            }
        }, jobTag, SystemClock.uptimeMillis() + delay);
    }
}
