package com.vungle.warren;

import android.support.annotation.NonNull;
import com.vungle.warren.model.Advertisement;
import com.vungle.warren.model.Placement;
import java.util.concurrent.ExecutorService;

class DownloadCallbackWrapper implements DownloadCallback {
    /* access modifiers changed from: private */
    public final DownloadCallback downloadCallback;
    private final ExecutorService executorService;

    public DownloadCallbackWrapper(ExecutorService executorService2, DownloadCallback downloadCallback2) {
        this.downloadCallback = downloadCallback2;
        this.executorService = executorService2;
    }

    public void onDownloadCompleted(@NonNull final String str, @NonNull final String str2) {
        if (this.downloadCallback != null) {
            this.executorService.execute(new Runnable() {
                public void run() {
                    DownloadCallbackWrapper.this.downloadCallback.onDownloadCompleted(str, str2);
                }
            });
        }
    }

    public void onDownloadFailed(@NonNull final Throwable th, final String str, final String str2) {
        if (this.downloadCallback != null) {
            this.executorService.execute(new Runnable() {
                public void run() {
                    DownloadCallbackWrapper.this.downloadCallback.onDownloadFailed(th, str, str2);
                }
            });
        }
    }

    public void onReady(@NonNull final String str, @NonNull final Placement placement, @NonNull final Advertisement advertisement) {
        if (this.downloadCallback != null) {
            this.executorService.execute(new Runnable() {
                public void run() {
                    DownloadCallbackWrapper.this.downloadCallback.onReady(str, placement, advertisement);
                }
            });
        }
    }
}
