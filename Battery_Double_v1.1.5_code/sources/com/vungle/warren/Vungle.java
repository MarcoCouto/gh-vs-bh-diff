package com.vungle.warren;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences.Editor;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.tapjoy.TJAdUnitConstants.String;
import com.vungle.warren.AdConfig.AdSize;
import com.vungle.warren.VungleSettings.Builder;
import com.vungle.warren.analytics.VungleAnalytics;
import com.vungle.warren.download.APKDirectDownloadManager;
import com.vungle.warren.downloader.AssetDownloader;
import com.vungle.warren.downloader.DownloadRequest;
import com.vungle.warren.error.VungleError;
import com.vungle.warren.error.VungleException;
import com.vungle.warren.model.Advertisement;
import com.vungle.warren.model.Cookie;
import com.vungle.warren.model.JsonUtil;
import com.vungle.warren.model.Placement;
import com.vungle.warren.persistence.CacheManager;
import com.vungle.warren.persistence.CacheManager.Listener;
import com.vungle.warren.persistence.DatabaseHelper.DBException;
import com.vungle.warren.persistence.GraphicDesigner;
import com.vungle.warren.persistence.Repository;
import com.vungle.warren.persistence.Repository.LoadCallback;
import com.vungle.warren.tasks.CleanupJob;
import com.vungle.warren.tasks.JobRunner;
import com.vungle.warren.tasks.ReconfigJob;
import com.vungle.warren.tasks.ReconfigJob.ReconfigCall;
import com.vungle.warren.tasks.SendReportsJob;
import com.vungle.warren.tasks.VungleJobCreator;
import com.vungle.warren.tasks.utility.JobRunnerThreadPriorityHelper;
import com.vungle.warren.ui.VungleActivity;
import com.vungle.warren.ui.VungleFlexViewActivity;
import com.vungle.warren.ui.contract.AdContract.AdvertisementBus;
import com.vungle.warren.ui.contract.AdContract.AdvertisementPresenter.EventListener;
import com.vungle.warren.ui.view.VungleNativeView;
import com.vungle.warren.utility.NetworkProvider;
import com.vungle.warren.utility.SDKExecutors;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.atomic.AtomicBoolean;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.HttpException;
import retrofit2.Response;

public class Vungle {
    private static final String COM_VUNGLE_SDK = "com.vungle.sdk";
    /* access modifiers changed from: private */
    public static final String TAG = Vungle.class.getCanonicalName();
    static final Vungle _instance = new Vungle();
    /* access modifiers changed from: private */
    public static Listener cacheListener = new Listener() {
        public void onCacheChanged() {
            Vungle.stopPlaying();
            if (Vungle._instance != null && Vungle._instance.cacheManager.getCache() != null) {
                List<DownloadRequest> allRequests = Vungle._instance.downloader.getAllRequests();
                String path = Vungle._instance.cacheManager.getCache().getPath();
                for (DownloadRequest downloadRequest : allRequests) {
                    if (!downloadRequest.path.startsWith(path)) {
                        Vungle._instance.downloader.cancel(downloadRequest);
                    }
                }
            }
        }
    };
    /* access modifiers changed from: private */
    public static AtomicBoolean isDepInit = new AtomicBoolean(false);
    private static volatile boolean isInitialized = false;
    private static AtomicBoolean isInitializing = new AtomicBoolean(false);
    /* access modifiers changed from: private */
    public static ReconfigCall reconfigCall = new ReconfigCall() {
        public void reConfigVungle() {
            Vungle.reConfigure();
        }
    };
    static SDKExecutors sdkExecutors = new SDKExecutors();
    /* access modifiers changed from: private */
    public static final VungleStaticApi vungleApi = new VungleStaticApi() {
        public boolean isInitialized() {
            return Vungle.isInitialized();
        }

        public Collection<String> getValidPlacements() {
            return Vungle.getValidPlacements();
        }
    };
    /* access modifiers changed from: private */
    public AdLoader adLoader;
    /* access modifiers changed from: private */
    public volatile String appID;
    /* access modifiers changed from: private */
    public volatile CacheManager cacheManager;
    /* access modifiers changed from: private */
    public volatile Consent consent;
    /* access modifiers changed from: private */
    public volatile String consentVersion;
    /* access modifiers changed from: private */
    public Context context;
    /* access modifiers changed from: private */
    public volatile AssetDownloader downloader;
    volatile JobRunner jobRunner;
    /* access modifiers changed from: private */
    public Map<String, Boolean> playOperations = new ConcurrentHashMap();
    volatile Repository repository;
    /* access modifiers changed from: private */
    public volatile RuntimeValues runtimeValues = new RuntimeValues();
    /* access modifiers changed from: private */
    public volatile boolean shouldTransmitIMEI;
    /* access modifiers changed from: private */
    public volatile String userIMEI;
    volatile VungleApiClient vungleApiClient;

    public enum Consent {
        OPTED_IN,
        OPTED_OUT
    }

    private Vungle() {
    }

    @Deprecated
    public static void init(@NonNull Collection<String> collection, @NonNull String str, @NonNull Context context2, @NonNull InitCallback initCallback) throws IllegalArgumentException {
        init(str, context2, initCallback, new Builder().build());
    }

    public static void init(@NonNull String str, @NonNull Context context2, @NonNull InitCallback initCallback) throws IllegalArgumentException {
        init(str, context2, initCallback, new Builder().build());
    }

    public static void init(@NonNull final String str, @NonNull final Context context2, @NonNull InitCallback initCallback, @NonNull VungleSettings vungleSettings) throws IllegalArgumentException {
        if (initCallback != null) {
            _instance.runtimeValues.settings = vungleSettings;
            RuntimeValues runtimeValues2 = _instance.runtimeValues;
            if (!(initCallback instanceof InitCallbackWrapper)) {
                initCallback = new InitCallbackWrapper(sdkExecutors.getUIExecutor(), initCallback);
            }
            runtimeValues2.initCallback = initCallback;
            if (context2 == null || str == null || str.isEmpty()) {
                _instance.runtimeValues.initCallback.onError(new VungleException(6));
            } else if (!(context2 instanceof Application)) {
                _instance.runtimeValues.initCallback.onError(new VungleException(7));
            } else if (isInitialized()) {
                Log.d(TAG, "init already complete");
                _instance.runtimeValues.initCallback.onSuccess();
            } else if (isInitializing.getAndSet(true)) {
                Log.d(TAG, "init ongoing");
                _instance.runtimeValues.initCallback.onError(new VungleException(8));
            } else {
                sdkExecutors.getVungleExecutor().execute(new Runnable() {
                    public void run() {
                        if (!Vungle.isDepInit.getAndSet(true)) {
                            if (Vungle._instance.downloader == null) {
                                Vungle._instance.downloader = new AssetDownloader(4, NetworkProvider.getInstance(context2), Vungle.sdkExecutors.getUIExecutor());
                            }
                            if (Vungle._instance.cacheManager == null) {
                                Vungle._instance.cacheManager = new CacheManager(context2);
                            }
                            if (Vungle._instance.runtimeValues.settings == null || Vungle._instance.cacheManager.getBytesAvailable() >= Vungle._instance.runtimeValues.settings.getMinimumSpaceForInit()) {
                                Vungle._instance.cacheManager.addListener(Vungle.cacheListener);
                                GraphicDesigner graphicDesigner = new GraphicDesigner(Vungle._instance.cacheManager);
                                if (Vungle._instance.repository == null) {
                                    Vungle._instance.repository = new Repository(context2, graphicDesigner, Vungle.sdkExecutors.getIOExecutor(), Vungle.sdkExecutors.getUIExecutor());
                                }
                                Vungle._instance.context = context2;
                                Vungle._instance.appID = str;
                                try {
                                    Vungle._instance.repository.init();
                                    if (Vungle._instance.vungleApiClient == null) {
                                        Vungle._instance.vungleApiClient = new VungleApiClient(context2, str, Vungle._instance.cacheManager, Vungle._instance.repository);
                                    }
                                    if (!TextUtils.isEmpty(Vungle._instance.userIMEI)) {
                                        Vungle._instance.vungleApiClient.updateIMEI(Vungle._instance.userIMEI, Vungle._instance.shouldTransmitIMEI);
                                    }
                                    if (Vungle._instance.runtimeValues.settings != null) {
                                        Vungle._instance.vungleApiClient.setDefaultIdFallbackDisabled(Vungle._instance.runtimeValues.settings.getAndroidIdOptOut());
                                    }
                                    if (Vungle._instance.adLoader == null) {
                                        Vungle vungle = Vungle._instance;
                                        AdLoader adLoader = new AdLoader(Vungle.sdkExecutors, Vungle._instance.repository, Vungle._instance.vungleApiClient, Vungle._instance.cacheManager, Vungle._instance.downloader, Vungle._instance.runtimeValues, Vungle.vungleApi);
                                        vungle.adLoader = adLoader;
                                    }
                                    if (Vungle._instance.jobRunner == null) {
                                        VungleJobCreator vungleJobCreator = new VungleJobCreator(Vungle._instance.repository, graphicDesigner, Vungle._instance.vungleApiClient, new VungleAnalytics(Vungle._instance.vungleApiClient), Vungle.reconfigCall, Vungle._instance.adLoader, Vungle.vungleApi);
                                        Vungle._instance.jobRunner = new VungleJobRunner(vungleJobCreator, Vungle.sdkExecutors.getJobExecutor(), new JobRunnerThreadPriorityHelper());
                                    }
                                    Vungle._instance.adLoader.setJobRunner(Vungle._instance.jobRunner);
                                    if (Vungle._instance.consent == null || TextUtils.isEmpty(Vungle._instance.consentVersion)) {
                                        Cookie cookie = (Cookie) Vungle._instance.repository.load(Cookie.CONSENT_COOKIE, Cookie.class).get();
                                        Vungle._instance.consent = Vungle.getConsent(cookie);
                                        Vungle._instance.consentVersion = Vungle.getConsentMessageVersion(cookie);
                                    } else {
                                        Vungle.updateConsentStatus(Vungle._instance.consent, Vungle._instance.consentVersion);
                                    }
                                    Cookie cookie2 = (Cookie) Vungle._instance.repository.load("appId", Cookie.class).get();
                                    if (cookie2 == null) {
                                        cookie2 = new Cookie("appId");
                                    }
                                    cookie2.putValue("appId", str);
                                    try {
                                        Vungle._instance.repository.save(cookie2);
                                    } catch (DBException unused) {
                                        Vungle.onError(Vungle._instance.runtimeValues.initCallback, new VungleException(16));
                                        Vungle.deInit();
                                        return;
                                    }
                                } catch (DBException unused2) {
                                    Vungle.onError(Vungle._instance.runtimeValues.initCallback, new VungleException(26));
                                    Vungle.deInit();
                                    return;
                                }
                            } else {
                                Vungle.onError(Vungle._instance.runtimeValues.initCallback, new VungleException(16));
                                Vungle.deInit();
                                return;
                            }
                        }
                        Vungle._instance.configure(Vungle._instance.runtimeValues.initCallback);
                    }
                });
            }
        } else {
            throw new IllegalArgumentException("A valid InitCallback required to ensure API calls are being made after initialize is successful");
        }
    }

    /* access modifiers changed from: private */
    public static void onError(InitCallback initCallback, VungleException vungleException) {
        if (initCallback != null) {
            initCallback.onError(vungleException);
        }
    }

    static void reConfigure() {
        if (isInitialized()) {
            sdkExecutors.getVungleExecutor().execute(new Runnable() {
                public void run() {
                    Vungle._instance.configure(Vungle._instance.runtimeValues.initCallback);
                }
            });
        } else {
            init(_instance.appID, _instance.context, _instance.runtimeValues.initCallback);
        }
    }

    /* access modifiers changed from: private */
    public void configure(@NonNull InitCallback initCallback) {
        InitCallback initCallback2 = initCallback;
        try {
            Response config = this.vungleApiClient.config();
            if (config == null) {
                initCallback2.onError(new VungleException(2));
                isInitializing.set(false);
            } else if (!config.isSuccessful()) {
                long retryAfterHeaderValue = this.vungleApiClient.getRetryAfterHeaderValue(config);
                if (retryAfterHeaderValue > 0) {
                    this.jobRunner.execute(ReconfigJob.makeJobInfo(_instance.appID).setDelay(retryAfterHeaderValue));
                    initCallback2.onError(new VungleException(14));
                    isInitializing.set(false);
                    return;
                }
                initCallback2.onError(new VungleException(3));
                isInitializing.set(false);
            } else {
                if (!this.context.getSharedPreferences(COM_VUNGLE_SDK, 0).getBoolean("reported", false)) {
                    this.vungleApiClient.reportNew().enqueue(new Callback<JsonObject>() {
                        public void onFailure(Call<JsonObject> call, Throwable th) {
                        }

                        public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                            if (response.isSuccessful()) {
                                Editor edit = Vungle.this.context.getSharedPreferences(Vungle.COM_VUNGLE_SDK, 0).edit();
                                edit.putBoolean("reported", true);
                                edit.apply();
                                Log.d(Vungle.TAG, "Saving reported state to shared preferences");
                            }
                        }
                    });
                }
                JsonObject jsonObject = (JsonObject) config.body();
                JsonArray asJsonArray = jsonObject.getAsJsonArray("placements");
                if (asJsonArray.size() == 0) {
                    initCallback2.onError(new VungleException(0));
                    isInitializing.set(false);
                    return;
                }
                this.playOperations.clear();
                _instance.adLoader.clear();
                ArrayList arrayList = new ArrayList();
                Iterator it = asJsonArray.iterator();
                while (it.hasNext()) {
                    arrayList.add(new Placement(((JsonElement) it.next()).getAsJsonObject()));
                }
                _instance.repository.setValidPlacements(arrayList);
                if (jsonObject.has("gdpr")) {
                    Cookie cookie = (Cookie) _instance.repository.load(Cookie.CONSENT_COOKIE, Cookie.class).get();
                    if (cookie == null) {
                        cookie = new Cookie(Cookie.CONSENT_COOKIE);
                        cookie.putValue("consent_status", "unknown");
                        cookie.putValue("consent_source", "no_interaction");
                        cookie.putValue("timestamp", Long.valueOf(0));
                    }
                    JsonObject asJsonObject = jsonObject.getAsJsonObject("gdpr");
                    boolean z = JsonUtil.hasNonNull(asJsonObject, "is_country_data_protected") && asJsonObject.get("is_country_data_protected").getAsBoolean();
                    String asString = JsonUtil.hasNonNull(asJsonObject, "consent_title") ? asJsonObject.get("consent_title").getAsString() : "";
                    String asString2 = JsonUtil.hasNonNull(asJsonObject, "consent_message") ? asJsonObject.get("consent_message").getAsString() : "";
                    String asString3 = JsonUtil.hasNonNull(asJsonObject, "consent_message_version") ? asJsonObject.get("consent_message_version").getAsString() : "";
                    String asString4 = JsonUtil.hasNonNull(asJsonObject, "button_accept") ? asJsonObject.get("button_accept").getAsString() : "";
                    String asString5 = JsonUtil.hasNonNull(asJsonObject, "button_deny") ? asJsonObject.get("button_deny").getAsString() : "";
                    cookie.putValue("is_country_data_protected", Boolean.valueOf(z));
                    String str = "consent_title";
                    if (TextUtils.isEmpty(asString)) {
                        asString = "Targeted Ads";
                    }
                    cookie.putValue(str, asString);
                    String str2 = "consent_message";
                    if (TextUtils.isEmpty(asString2)) {
                        asString2 = "To receive more relevant ad content based on your interactions with our ads, click \"I Consent\" below. Either way, you will see the same amount of ads.";
                    }
                    cookie.putValue(str2, asString2);
                    if (!"publisher".equalsIgnoreCase(cookie.getString("consent_source"))) {
                        String str3 = "consent_message_version";
                        if (TextUtils.isEmpty(asString3)) {
                            asString3 = "";
                        }
                        cookie.putValue(str3, asString3);
                    }
                    String str4 = "button_accept";
                    if (TextUtils.isEmpty(asString4)) {
                        asString4 = "I Consent";
                    }
                    cookie.putValue(str4, asString4);
                    String str5 = "button_deny";
                    if (TextUtils.isEmpty(asString5)) {
                        asString5 = "I Do Not Consent";
                    }
                    cookie.putValue(str5, asString5);
                    _instance.repository.save(cookie);
                }
                if (!jsonObject.has("apk_direct_download") || !jsonObject.getAsJsonObject("apk_direct_download").has(String.ENABLED)) {
                    APKDirectDownloadManager.init(this.context, new AssetDownloader(4, NetworkProvider.getInstance(this.context), sdkExecutors.getUIExecutor()));
                    APKDirectDownloadManager.setDirectDownloadStatus(-1);
                } else {
                    boolean asBoolean = jsonObject.getAsJsonObject("apk_direct_download").get(String.ENABLED).getAsBoolean();
                    if (asBoolean) {
                        APKDirectDownloadManager.init(this.context, new AssetDownloader(4, NetworkProvider.getInstance(this.context), sdkExecutors.getUIExecutor()));
                    }
                    APKDirectDownloadManager.setDirectDownloadStatus(asBoolean ? 1 : 0);
                }
                if (jsonObject.has("ri")) {
                    Cookie cookie2 = (Cookie) _instance.repository.load(Cookie.CONFIG_COOKIE, Cookie.class).get();
                    if (cookie2 == null) {
                        cookie2 = new Cookie(Cookie.CONFIG_COOKIE);
                    }
                    cookie2.putValue("isReportIncentivizedEnabled", Boolean.valueOf(jsonObject.getAsJsonObject("ri").get(String.ENABLED).getAsBoolean()));
                    _instance.repository.save(cookie2);
                }
                if (jsonObject.has("attribution_reporting")) {
                    JsonObject asJsonObject2 = jsonObject.getAsJsonObject("attribution_reporting");
                    if (asJsonObject2.has("should_transmit_imei")) {
                        this.shouldTransmitIMEI = asJsonObject2.get("should_transmit_imei").getAsBoolean();
                    } else {
                        this.shouldTransmitIMEI = false;
                    }
                } else {
                    this.shouldTransmitIMEI = false;
                }
                if (jsonObject.has("config")) {
                    this.jobRunner.execute(ReconfigJob.makeJobInfo(this.appID).setDelay(jsonObject.getAsJsonObject("config").get("refresh_time").getAsLong()));
                }
                isInitialized = true;
                initCallback.onSuccess();
                isInitializing.set(false);
                Collection<Placement> collection = (Collection) this.repository.loadValidPlacements().get();
                this.jobRunner.execute(CleanupJob.makeJobInfo());
                if (collection != null) {
                    for (Placement placement : collection) {
                        if (placement.isAutoCached()) {
                            Log.d(TAG, "starting jobs for autocached advs");
                            this.adLoader.loadEndless(placement.getId(), 0);
                        }
                    }
                }
                this.jobRunner.execute(SendReportsJob.makeJobInfo(true));
            }
        } catch (Throwable th) {
            isInitialized = false;
            isInitializing.set(false);
            Log.e(TAG, Log.getStackTraceString(th));
            if (th instanceof HttpException) {
                initCallback2.onError(new VungleException(3));
            } else if (th instanceof DBException) {
                initCallback2.onError(new VungleException(26));
            } else {
                initCallback2.onError(new VungleException(2));
            }
        }
    }

    public static boolean isInitialized() {
        return (!isInitialized || _instance.repository == null || _instance.repository.getValidPlacements() == null || ((Collection) _instance.repository.getValidPlacements().get()).size() <= 0 || _instance.context == null) ? false : true;
    }

    public static void setIncentivizedFields(@Nullable String str, @Nullable String str2, @Nullable String str3, @Nullable String str4, @Nullable String str5) {
        ExecutorService vungleExecutor = sdkExecutors.getVungleExecutor();
        final String str6 = str2;
        final String str7 = str3;
        final String str8 = str4;
        final String str9 = str5;
        final String str10 = str;
        AnonymousClass5 r1 = new Runnable() {
            public void run() {
                if (!Vungle.isInitialized()) {
                    Log.e(Vungle.TAG, "Vungle is not initialized");
                    return;
                }
                Cookie cookie = (Cookie) Vungle._instance.repository.load(Cookie.INCENTIVIZED_TEXT_COOKIE, Cookie.class).get();
                if (cookie == null) {
                    cookie = new Cookie(Cookie.INCENTIVIZED_TEXT_COOKIE);
                }
                boolean z = false;
                if (!TextUtils.isEmpty(str6)) {
                    cookie.putValue("title", str6);
                    z = true;
                }
                if (!TextUtils.isEmpty(str7)) {
                    cookie.putValue(TtmlNode.TAG_BODY, str7);
                    z = true;
                }
                if (!TextUtils.isEmpty(str8)) {
                    cookie.putValue("continue", str8);
                    z = true;
                }
                if (!TextUtils.isEmpty(str9)) {
                    cookie.putValue("close", str9);
                    z = true;
                }
                if (!TextUtils.isEmpty(str10)) {
                    cookie.putValue("userID", str10);
                    z = true;
                }
                if (z) {
                    try {
                        Vungle._instance.repository.save(cookie);
                    } catch (DBException e) {
                        e.printStackTrace();
                    }
                }
            }
        };
        vungleExecutor.execute(r1);
    }

    public static boolean canPlayAd(@NonNull String str) {
        if (isInitialized()) {
            return canPlayAd((Advertisement) _instance.repository.findValidAdvertisementForPlacement(str).get());
        }
        Log.e(TAG, "Vungle is not initialized");
        return false;
    }

    static boolean canPlayAd(Advertisement advertisement) {
        return _instance.adLoader.canPlayAd(advertisement);
    }

    public static void playAd(@NonNull final String str, final AdConfig adConfig, @Nullable PlayAdCallback playAdCallback) {
        if (!isInitialized()) {
            if (playAdCallback != null) {
                playAdCallback.onError(str, new VungleException(9));
            }
            return;
        }
        final PlayAdCallbackWrapper playAdCallbackWrapper = new PlayAdCallbackWrapper(sdkExecutors.getUIExecutor(), playAdCallback);
        sdkExecutors.getVungleExecutor().execute(new Runnable() {
            public void run() {
                VungleException vungleException;
                Placement placement = (Placement) Vungle._instance.repository.load(str, Placement.class).get();
                if (Boolean.TRUE.equals(Vungle._instance.playOperations.get(str)) || Vungle._instance.adLoader.isLoading(str)) {
                    vungleException = new VungleException(8);
                } else {
                    vungleException = null;
                }
                if (placement == null) {
                    vungleException = new VungleException(13);
                }
                if (vungleException != null) {
                    playAdCallbackWrapper.onError(str, vungleException);
                    return;
                }
                final boolean z = false;
                final Advertisement advertisement = (Advertisement) Vungle._instance.repository.findValidAdvertisementForPlacement(str).get();
                try {
                    if (!Vungle.canPlayAd(advertisement)) {
                        if (advertisement != null && advertisement.getState() == 1) {
                            Vungle._instance.repository.saveAndApplyState(advertisement, str, 4);
                            if (placement.isAutoCached()) {
                                Vungle._instance.adLoader.loadEndless(placement.getId(), 0);
                            }
                        }
                        playAdCallbackWrapper.onError(str, new VungleException(10));
                        z = true;
                    } else {
                        advertisement.configure(adConfig);
                        Vungle._instance.repository.save(advertisement);
                    }
                    if (Vungle._instance.context != null) {
                        Vungle._instance.vungleApiClient.willPlayAd(placement.getId(), placement.isAutoCached(), z ? "" : advertisement.getAdToken()).enqueue(new Callback<JsonObject>() {
                            public void onResponse(Call<JsonObject> call, final Response<JsonObject> response) {
                                Vungle.sdkExecutors.getVungleExecutor().execute(new Runnable() {
                                    /* JADX WARNING: Removed duplicated region for block: B:21:0x004f  */
                                    /* JADX WARNING: Removed duplicated region for block: B:22:0x0057  */
                                    /* JADX WARNING: Removed duplicated region for block: B:29:0x0077  */
                                    /* JADX WARNING: Removed duplicated region for block: B:32:0x00a5  */
                                    public void run() {
                                        Advertisement advertisement = null;
                                        if (response.isSuccessful()) {
                                            JsonObject jsonObject = (JsonObject) response.body();
                                            if (jsonObject != null && jsonObject.has("ad")) {
                                                try {
                                                    Advertisement advertisement2 = new Advertisement(jsonObject.getAsJsonObject("ad"));
                                                    try {
                                                        advertisement2.configure(adConfig);
                                                        Vungle._instance.repository.saveAndApplyState(advertisement2, str, 0);
                                                        advertisement = advertisement2;
                                                    } catch (IllegalArgumentException unused) {
                                                        advertisement = advertisement2;
                                                        Log.v("Vungle", "Will Play Ad did not respond with a replacement. Move on.");
                                                        if (!z) {
                                                        }
                                                    } catch (Exception e) {
                                                        e = e;
                                                        advertisement = advertisement2;
                                                        Log.e("Vungle", "Error using will_play_ad!", e);
                                                        if (!z) {
                                                        }
                                                    } catch (VungleError e2) {
                                                        e = e2;
                                                        advertisement = advertisement2;
                                                        if (e.getErrorCode() == 6) {
                                                            Log.e("Vungle", "Error using will_play_ad!", e);
                                                        } else {
                                                            Log.e(Vungle.TAG, "will_play_ad was disabled by the configuration settings. This is expected.");
                                                        }
                                                        if (!z) {
                                                        }
                                                    }
                                                } catch (IllegalArgumentException unused2) {
                                                    Log.v("Vungle", "Will Play Ad did not respond with a replacement. Move on.");
                                                    if (!z) {
                                                    }
                                                } catch (Exception e3) {
                                                    e = e3;
                                                    Log.e("Vungle", "Error using will_play_ad!", e);
                                                    if (!z) {
                                                    }
                                                } catch (VungleError e4) {
                                                    e = e4;
                                                    if (e.getErrorCode() == 6) {
                                                    }
                                                    if (!z) {
                                                    }
                                                }
                                            }
                                        }
                                        if (!z) {
                                            Vungle.renderAd(str, playAdCallbackWrapper, str, advertisement);
                                        } else if (advertisement == null) {
                                            playAdCallbackWrapper.onError(str, new VungleException(1));
                                        } else {
                                            Vungle.renderAd(str, playAdCallbackWrapper, str, advertisement);
                                        }
                                    }
                                });
                            }

                            public void onFailure(Call<JsonObject> call, Throwable th) {
                                Vungle.sdkExecutors.getVungleExecutor().execute(new Runnable() {
                                    public void run() {
                                        if (z) {
                                            playAdCallbackWrapper.onError(str, new VungleException(1));
                                        } else {
                                            Vungle.renderAd(str, playAdCallbackWrapper, str, advertisement);
                                        }
                                    }
                                });
                            }
                        });
                    }
                } catch (DBException unused) {
                    playAdCallbackWrapper.onError(str, new VungleException(26));
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public static synchronized void renderAd(@NonNull String str, @Nullable final PlayAdCallback playAdCallback, final String str2, final Advertisement advertisement) {
        synchronized (Vungle.class) {
            boolean z = true;
            _instance.playOperations.put(str, Boolean.valueOf(true));
            VungleActivity.setEventListener(new EventListener() {
                int percentViewed = -1;
                boolean succesfulView = false;

                public void onNext(String str, String str2, String str3) {
                    boolean z;
                    try {
                        boolean z2 = false;
                        if (str.equals("start")) {
                            Vungle._instance.repository.saveAndApplyState(advertisement, str3, 2);
                            if (playAdCallback != null) {
                                playAdCallback.onAdStart(str3);
                            }
                            this.percentViewed = 0;
                            Placement placement = (Placement) Vungle._instance.repository.load(str2, Placement.class).get();
                            if (placement != null && placement.isAutoCached()) {
                                Vungle._instance.adLoader.loadEndless(str2, 0);
                            }
                        } else if (str.equals(TtmlNode.END)) {
                            StringBuilder sb = new StringBuilder();
                            sb.append("Cleaning up metadata and assets for placement ");
                            sb.append(str3);
                            sb.append(" and advertisement ");
                            sb.append(advertisement.getId());
                            Log.d("Vungle", sb.toString());
                            Vungle._instance.repository.saveAndApplyState(advertisement, str3, 3);
                            Vungle._instance.repository.updateAndSaveReportState(str3, advertisement.getAppID(), 0, 1);
                            VungleActivity.setEventListener(null);
                            Vungle._instance.playOperations.put(str3, Boolean.valueOf(false));
                            Vungle._instance.jobRunner.execute(SendReportsJob.makeJobInfo(false));
                            if (playAdCallback != null) {
                                PlayAdCallback playAdCallback = playAdCallback;
                                if (!this.succesfulView) {
                                    if (this.percentViewed < 80) {
                                        z = false;
                                        if (str2 != null && str2.equals("isCTAClicked")) {
                                            z2 = true;
                                        }
                                        playAdCallback.onAdEnd(str3, z, z2);
                                    }
                                }
                                z = true;
                                z2 = true;
                                playAdCallback.onAdEnd(str3, z, z2);
                            }
                        } else if (str.equals("successfulView")) {
                            this.succesfulView = true;
                        } else if (str.startsWith("percentViewed")) {
                            String[] split = str.split(":");
                            if (split.length == 2) {
                                this.percentViewed = Integer.parseInt(split[1]);
                            }
                        }
                    } catch (DBException unused) {
                        onError(new VungleException(26), str3);
                    }
                }

                public void onError(Throwable th, String str) {
                    if (!(VungleException.getExceptionCode(th) == 15 || VungleException.getExceptionCode(th) == 25)) {
                        try {
                            Vungle._instance.repository.saveAndApplyState(advertisement, str, 4);
                        } catch (DBException unused) {
                            th = new VungleException(26);
                        }
                    }
                    VungleActivity.setEventListener(null);
                    Vungle._instance.playOperations.put(str, Boolean.valueOf(false));
                    if (playAdCallback != null) {
                        playAdCallback.onError(str, th);
                    }
                }
            });
            if (advertisement == null || !"flexview".equals(advertisement.getTemplateType())) {
                z = false;
            }
            Intent intent = new Intent(_instance.context, z ? VungleFlexViewActivity.class : VungleActivity.class);
            intent.addFlags(268435456);
            intent.putExtra("placement", str);
            _instance.context.startActivity(intent);
        }
    }

    public static void loadAd(@NonNull String str, @Nullable LoadAdCallback loadAdCallback) {
        if (!isInitialized()) {
            if (loadAdCallback != null) {
                loadAdCallback.onError(str, new VungleException(9));
            }
            return;
        }
        _instance.adLoader.load(str, new LoadAdCallbackWrapper(sdkExecutors.getUIExecutor(), loadAdCallback), null);
    }

    private static void clearCache() {
        if (isInitialized()) {
            sdkExecutors.getVungleExecutor().execute(new Runnable() {
                public void run() {
                    Vungle._instance.repository.clearAllData();
                    Vungle._instance.configure(Vungle._instance.runtimeValues.initCallback);
                }
            });
        } else {
            Log.e(TAG, "Vungle not initialized");
        }
    }

    @Nullable
    @Deprecated
    public static VungleNativeAd getNativeAd(String str, PlayAdCallback playAdCallback) {
        return getNativeAd(str, null, playAdCallback);
    }

    @Nullable
    public static VungleNativeAd getNativeAd(String str, AdConfig adConfig, final PlayAdCallback playAdCallback) {
        VungleNativeAd vungleNativeAd;
        if (!isInitialized()) {
            Log.e(TAG, "Vungle is not initialized, returned VungleNativeAd = null");
            if (playAdCallback != null) {
                playAdCallback.onError(str, new VungleException(9));
            }
            return null;
        }
        Placement placement = (Placement) _instance.repository.load(str, Placement.class).get();
        if (placement == null) {
            Log.e(TAG, "No Placement for ID");
            if (playAdCallback != null) {
                playAdCallback.onError(str, new VungleException(13));
            }
            return null;
        }
        final Advertisement advertisement = (Advertisement) _instance.repository.findValidAdvertisementForPlacement(str).get();
        if (advertisement == null) {
            Log.e(TAG, "No Advertisement for ID");
            if (playAdCallback != null) {
                playAdCallback.onError(str, new VungleException(10));
            }
            return null;
        } else if (!canPlayAd(advertisement)) {
            if (advertisement != null && advertisement.getState() == 1) {
                try {
                    _instance.repository.saveAndApplyState(advertisement, str, 4);
                } catch (DBException unused) {
                    if (playAdCallback != null) {
                        playAdCallback.onError(str, new VungleException(26));
                    }
                }
                if (placement.isAutoCached()) {
                    _instance.adLoader.loadEndless(placement.getId(), 0, null);
                }
            }
            return null;
        } else if (Boolean.TRUE.equals(_instance.playOperations.get(str)) || _instance.adLoader.isLoading(str)) {
            Log.e(TAG, "Playing or Loading operation ongoing");
            if (playAdCallback != null) {
                playAdCallback.onError(str, new VungleException(8));
            }
            return null;
        } else if (advertisement.getAdType() != 1) {
            Log.e(TAG, "Invalid Ad Type for Native Ad.");
            if (playAdCallback != null) {
                playAdCallback.onError(str, new VungleException(10));
            }
            return null;
        } else {
            AdSize adSize = adConfig == null ? AdSize.VUNGLE_DEFAULT : adConfig.getAdSize();
            if ((!"mrec".equals(advertisement.getTemplateType()) || adSize == AdSize.VUNGLE_MREC) && (!"flexfeed".equals(advertisement.getTemplateType()) || adSize == AdSize.VUNGLE_DEFAULT)) {
                _instance.playOperations.put(str, Boolean.valueOf(true));
                try {
                    vungleNativeAd = new VungleNativeView(_instance.context.getApplicationContext(), str, null, new EventListener() {
                        int percentViewed = -1;
                        boolean succesfulView = false;

                        public void onNext(String str, String str2, String str3) {
                            boolean z;
                            try {
                                boolean z2 = false;
                                if (str.equals("start")) {
                                    Vungle._instance.repository.saveAndApplyState(advertisement, str3, 2);
                                    if (playAdCallback != null) {
                                        playAdCallback.onAdStart(str3);
                                    }
                                    this.percentViewed = 0;
                                    Placement placement = (Placement) Vungle._instance.repository.load(str3, Placement.class).get();
                                    if (placement != null && placement.isAutoCached()) {
                                        Vungle._instance.adLoader.loadEndless(str3, 0, null);
                                    }
                                } else if (str.equals(TtmlNode.END)) {
                                    StringBuilder sb = new StringBuilder();
                                    sb.append("Cleaning up metadata and assets for placement ");
                                    sb.append(str3);
                                    sb.append(" and advertisement ");
                                    sb.append(advertisement.getId());
                                    Log.d("Vungle", sb.toString());
                                    Vungle._instance.repository.saveAndApplyState(advertisement, str3, 3);
                                    Vungle._instance.repository.updateAndSaveReportState(str3, advertisement.getAppID(), 0, 1);
                                    Vungle._instance.jobRunner.execute(SendReportsJob.makeJobInfo(false));
                                    Vungle._instance.playOperations.put(str3, Boolean.valueOf(false));
                                    if (playAdCallback != null) {
                                        PlayAdCallback playAdCallback = playAdCallback;
                                        if (!this.succesfulView) {
                                            if (this.percentViewed < 80) {
                                                z = false;
                                                if (str2 != null && str2.equals("isCTAClicked")) {
                                                    z2 = true;
                                                }
                                                playAdCallback.onAdEnd(str3, z, z2);
                                            }
                                        }
                                        z = true;
                                        z2 = true;
                                        playAdCallback.onAdEnd(str3, z, z2);
                                    }
                                } else if (str.equals("successfulView")) {
                                    this.succesfulView = true;
                                } else if (str.startsWith("percentViewed")) {
                                    String[] split = str.split(":");
                                    if (split.length == 2) {
                                        this.percentViewed = Integer.parseInt(split[1]);
                                    }
                                }
                            } catch (DBException unused) {
                                onError(new VungleException(26), str3);
                            }
                        }

                        public void onError(Throwable th, String str) {
                            Vungle._instance.playOperations.put(str, Boolean.valueOf(false));
                            if (VungleException.getExceptionCode(th) != 25) {
                                try {
                                    Vungle._instance.repository.saveAndApplyState(advertisement, str, 4);
                                } catch (DBException unused) {
                                    th = new VungleException(26);
                                }
                            }
                            if (playAdCallback != null) {
                                playAdCallback.onError(str, th);
                            }
                        }
                    });
                } catch (Exception e) {
                    _instance.playOperations.put(str, Boolean.valueOf(false));
                    if (playAdCallback != null) {
                        playAdCallback.onError(str, new InstantiationException(e.getMessage()));
                    }
                    vungleNativeAd = null;
                }
                return vungleNativeAd;
            }
            Log.e(TAG, "Corresponding AdConfig#setAdSize must be passed for the type/size of native ad");
            return null;
        }
    }

    public static Collection<String> getValidPlacements() {
        if (isInitialized()) {
            return (Collection) _instance.repository.getValidPlacements().get();
        }
        Log.e(TAG, "Vungle is not initialized return empty placemetns list");
        return Collections.emptyList();
    }

    public static void updateConsentStatus(@NonNull final Consent consent2, @NonNull final String str) {
        _instance.consent = consent2;
        _instance.consentVersion = str;
        if (isDepInit.get() && _instance.repository != null) {
            _instance.repository.load(Cookie.CONSENT_COOKIE, Cookie.class, new LoadCallback<Cookie>() {
                public void onLoaded(Cookie cookie) {
                    if (cookie == null) {
                        cookie = new Cookie(Cookie.CONSENT_COOKIE);
                    }
                    cookie.putValue("consent_status", consent2 == Consent.OPTED_IN ? "opted_in" : "opted_out");
                    cookie.putValue("timestamp", Long.valueOf(System.currentTimeMillis() / 1000));
                    cookie.putValue("consent_source", "publisher");
                    cookie.putValue("consent_message_version", str == null ? "" : str);
                    Vungle._instance.repository.save(cookie, null);
                }
            });
        }
    }

    public static Consent getConsentStatus() {
        if (isInitialized()) {
            return _instance.consent;
        }
        Log.e(TAG, "Vungle is not initialized, consent is null");
        return null;
    }

    public static String getConsentMessageVersion() {
        if (isInitialized()) {
            return _instance.consentVersion;
        }
        Log.e(TAG, "Vungle is not initialized, please wait initialize or wait until Vungle is intialized to get Consent Message Version");
        return null;
    }

    /* access modifiers changed from: private */
    public static Consent getConsent(Cookie cookie) {
        if (cookie == null) {
            return null;
        }
        return "opted_in".equals(cookie.getString("consent_status")) ? Consent.OPTED_IN : Consent.OPTED_OUT;
    }

    /* access modifiers changed from: private */
    public static String getConsentMessageVersion(Cookie cookie) {
        if (cookie == null) {
            return null;
        }
        return cookie.getString("consent_message_version");
    }

    public static boolean closeFlexViewAd(@NonNull String str) {
        if (!isInitialized()) {
            Log.e(TAG, "Vungle is not initialized, can't close flex view ad");
            return false;
        }
        Intent intent = new Intent(AdvertisementBus.ACTION);
        intent.putExtra("placement", str);
        intent.putExtra("command", AdvertisementBus.CLOSE_FLEX);
        LocalBroadcastManager.getInstance(_instance.context).sendBroadcast(intent);
        return true;
    }

    public static void setUserLegacyID(String str) {
        if (isInitialized() || isInitializing.get()) {
            _instance.vungleApiClient.updateIMEI(str, _instance.shouldTransmitIMEI);
        } else {
            _instance.userIMEI = str;
        }
    }

    public static void setHeaderBiddingCallback(HeaderBiddingCallback headerBiddingCallback) {
        _instance.runtimeValues.headerBiddingCallback = new HeaderBiddingCallbackWrapper(sdkExecutors.getUIExecutor(), headerBiddingCallback);
    }

    protected static void mockDependencies(Context context2, String str, AssetDownloader assetDownloader, Repository repository2, VungleApiClient vungleApiClient2, JobRunner jobRunner2, SDKExecutors sDKExecutors, CacheManager cacheManager2, AdLoader adLoader2) {
        sdkExecutors = sDKExecutors;
        _instance.context = context2;
        _instance.appID = str;
        _instance.downloader = assetDownloader;
        _instance.repository = repository2;
        _instance.vungleApiClient = vungleApiClient2;
        _instance.jobRunner = jobRunner2;
        _instance.cacheManager = cacheManager2;
        _instance.adLoader = adLoader2;
    }

    protected static void deInit() {
        if (_instance.cacheManager != null) {
            _instance.cacheManager.removeListener(cacheListener);
        }
        _instance.context = null;
        _instance.appID = null;
        _instance.downloader = null;
        _instance.repository = null;
        _instance.vungleApiClient = null;
        _instance.jobRunner = null;
        _instance.cacheManager = null;
        _instance.adLoader = null;
        _instance.runtimeValues.initCallback = null;
        isInitialized = false;
        isDepInit.set(false);
        isInitializing.set(false);
    }

    /* access modifiers changed from: private */
    public static void stopPlaying() {
        if (_instance.context != null) {
            Intent intent = new Intent(AdvertisementBus.ACTION);
            intent.putExtra("command", AdvertisementBus.STOP_ALL);
            LocalBroadcastManager.getInstance(_instance.context).sendBroadcast(intent);
        }
    }
}
