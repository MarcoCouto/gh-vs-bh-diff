package com.vungle.warren.tasks.runnable;

import android.os.Bundle;
import android.os.Process;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import com.vungle.warren.tasks.JobCreator;
import com.vungle.warren.tasks.JobInfo;
import com.vungle.warren.tasks.JobRunner;
import com.vungle.warren.tasks.UnknownTagException;
import com.vungle.warren.tasks.utility.ThreadPriorityHelper;
import com.vungle.warren.utility.PriorityRunnable;

public class JobRunnable extends PriorityRunnable {
    private static final String TAG = "JobRunnable";
    private final JobCreator creator;
    private final JobRunner jobRunner;
    private final JobInfo jobinfo;
    private final ThreadPriorityHelper threadPriorityHelper;

    public JobRunnable(@NonNull JobInfo jobInfo, @NonNull JobCreator jobCreator, @NonNull JobRunner jobRunner2, @Nullable ThreadPriorityHelper threadPriorityHelper2) {
        this.jobinfo = jobInfo;
        this.creator = jobCreator;
        this.jobRunner = jobRunner2;
        this.threadPriorityHelper = threadPriorityHelper2;
    }

    public Integer getPriority() {
        return Integer.valueOf(this.jobinfo.getPriority());
    }

    public void run() {
        if (this.threadPriorityHelper != null) {
            try {
                int makeAndroidThreadPriority = this.threadPriorityHelper.makeAndroidThreadPriority(this.jobinfo);
                Process.setThreadPriority(makeAndroidThreadPriority);
                String str = TAG;
                StringBuilder sb = new StringBuilder();
                sb.append("Setting process thread prio = ");
                sb.append(makeAndroidThreadPriority);
                sb.append(" for ");
                sb.append(this.jobinfo.getJobTag());
                Log.d(str, sb.toString());
            } catch (Throwable unused) {
                Log.e(TAG, "Error on setting process thread priority");
            }
        }
        try {
            String jobTag = this.jobinfo.getJobTag();
            Bundle extras = this.jobinfo.getExtras();
            String str2 = TAG;
            StringBuilder sb2 = new StringBuilder();
            sb2.append("Start job ");
            sb2.append(jobTag);
            sb2.append("Thread ");
            sb2.append(Thread.currentThread().getName());
            Log.d(str2, sb2.toString());
            int onRunJob = this.creator.create(jobTag).onRunJob(extras, this.jobRunner);
            String str3 = TAG;
            StringBuilder sb3 = new StringBuilder();
            sb3.append("On job finished ");
            sb3.append(jobTag);
            sb3.append(" with result ");
            sb3.append(onRunJob);
            Log.d(str3, sb3.toString());
            if (onRunJob == 2) {
                long makeNextRescedule = this.jobinfo.makeNextRescedule();
                if (makeNextRescedule > 0) {
                    this.jobinfo.setDelay(makeNextRescedule);
                    this.jobRunner.execute(this.jobinfo);
                    String str4 = TAG;
                    StringBuilder sb4 = new StringBuilder();
                    sb4.append("Rescheduling ");
                    sb4.append(jobTag);
                    sb4.append(" in ");
                    sb4.append(makeNextRescedule);
                    Log.d(str4, sb4.toString());
                }
            }
        } catch (UnknownTagException e) {
            String str5 = TAG;
            StringBuilder sb5 = new StringBuilder();
            sb5.append("Cannot create job");
            sb5.append(e.getLocalizedMessage());
            Log.e(str5, sb5.toString());
        } catch (Throwable th) {
            Log.e(TAG, "Can't start job", th);
        }
    }
}
