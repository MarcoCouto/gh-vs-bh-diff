package com.vungle.warren.tasks;

import android.os.Bundle;
import android.support.annotation.NonNull;
import com.vungle.warren.AdLoader;
import com.vungle.warren.VungleStaticApi;
import java.util.Collection;

public class DownloadJob implements Job {
    private static final String PLACEMENT_KEY = "placement";
    static final String TAG = DownloadJob.class.getCanonicalName();
    private final AdLoader adLoader;
    private final VungleStaticApi vungleApi;

    public DownloadJob(@NonNull AdLoader adLoader2, @NonNull VungleStaticApi vungleStaticApi) {
        this.adLoader = adLoader2;
        this.vungleApi = vungleStaticApi;
    }

    public static JobInfo makeJobInfo(String str) {
        Bundle bundle = new Bundle();
        bundle.putString("placement", str);
        StringBuilder sb = new StringBuilder();
        sb.append(TAG);
        sb.append(" ");
        sb.append(str);
        return new JobInfo(sb.toString()).setUpdateCurrent(true).setExtras(bundle).setPriority(4);
    }

    public int onRunJob(Bundle bundle, JobRunner jobRunner) {
        String string = bundle.getString("placement", null);
        Collection validPlacements = this.vungleApi.getValidPlacements();
        if (string == null || !validPlacements.contains(string)) {
            return 1;
        }
        this.adLoader.loadPendingInternal(string);
        return 0;
    }
}
