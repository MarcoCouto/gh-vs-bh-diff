package com.vungle.warren.tasks;

import android.os.Bundle;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.vungle.warren.analytics.AdAnalytics;

public class AnalyticsJob implements Job {
    private static final long DEFAULT_DELAY = 30000;
    public static final String EXTRA_ACTION = "action_extra";
    private static final String EXTRA_BODY = "extra_body";
    private static final String EXTRA_URLS = "extra_urls";
    public static final String TAG = "AnalyticsJob";
    private final AdAnalytics adAnalytics;

    public @interface Action {
        public static final int PING = 1;
        public static final int RI = 0;
    }

    public static JobInfo makeJob(@Action int i, String str, String[] strArr) {
        Bundle bundle = new Bundle();
        bundle.putInt(EXTRA_ACTION, i);
        bundle.putString(EXTRA_BODY, str);
        bundle.putStringArray(EXTRA_URLS, strArr);
        return new JobInfo(TAG).setUpdateCurrent(false).setExtras(bundle).setReschedulePolicy(DEFAULT_DELAY, 1).setPriority(5);
    }

    public AnalyticsJob(AdAnalytics adAnalytics2) {
        this.adAnalytics = adAnalytics2;
    }

    public int onRunJob(Bundle bundle, JobRunner jobRunner) {
        switch (bundle.getInt(EXTRA_ACTION, -1)) {
            case 0:
                this.adAnalytics.ri(((JsonElement) new Gson().fromJson(bundle.getString(EXTRA_BODY), JsonElement.class)).getAsJsonObject());
                break;
            case 1:
                String[] stringArray = bundle.getStringArray(EXTRA_URLS);
                if (stringArray != null) {
                    String[] ping = this.adAnalytics.ping(stringArray);
                    if (ping.length != 0) {
                        bundle.putStringArray(EXTRA_URLS, ping);
                        return 2;
                    }
                }
                break;
        }
        return 0;
    }
}
