package com.vungle.warren.tasks.utility;

import android.support.annotation.NonNull;
import com.vungle.warren.tasks.JobInfo;

public interface ThreadPriorityHelper {
    int makeAndroidThreadPriority(@NonNull JobInfo jobInfo);
}
