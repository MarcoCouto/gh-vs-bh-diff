package com.vungle.warren.tasks;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import com.vungle.warren.AdLoader;
import com.vungle.warren.model.Advertisement;
import com.vungle.warren.model.Placement;
import com.vungle.warren.persistence.DatabaseHelper.DBException;
import com.vungle.warren.persistence.Designer;
import com.vungle.warren.persistence.Repository;
import com.vungle.warren.utility.FileUtility;
import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;

public class CleanupJob implements Job {
    static final String TAG = CleanupJob.class.getCanonicalName();
    private final AdLoader adLoader;
    private final Designer designer;
    private final Repository repository;

    CleanupJob(@NonNull Designer designer2, @NonNull Repository repository2, @NonNull AdLoader adLoader2) {
        this.designer = designer2;
        this.repository = repository2;
        this.adLoader = adLoader2;
    }

    public int onRunJob(Bundle bundle, JobRunner jobRunner) {
        if (this.designer == null || this.repository == null) {
            return 1;
        }
        Log.d(TAG, "CleanupJob: Current directory snapshot");
        FileUtility.printDirectoryTree(this.designer.getCacheDirectory());
        File[] listFiles = this.designer.getCacheDirectory().listFiles();
        List<Advertisement> list = (List) this.repository.loadAll(Advertisement.class).get();
        List<Placement> list2 = (List) this.repository.loadAll(Placement.class).get();
        if (list2.size() == 0) {
            return 0;
        }
        Collection collection = (Collection) this.repository.loadValidPlacements().get();
        HashSet hashSet = new HashSet();
        try {
            for (Placement placement : list2) {
                if (collection.isEmpty() || collection.contains(placement)) {
                    for (String str : (List) this.repository.findAdsForPlacement(placement.getId()).get()) {
                        Advertisement advertisement = (Advertisement) this.repository.load(str, Advertisement.class).get();
                        if (advertisement == null) {
                            String str2 = TAG;
                            StringBuilder sb = new StringBuilder();
                            sb.append("removing adv ");
                            sb.append(str);
                            sb.append(" from placement ");
                            sb.append(placement.getId());
                            Log.w(str2, sb.toString());
                            this.repository.deleteAdvertisement(placement.getId());
                        } else if (advertisement.getExpireTime() > System.currentTimeMillis() || advertisement.getState() == 2) {
                            hashSet.add(advertisement.getId());
                            String str3 = TAG;
                            StringBuilder sb2 = new StringBuilder();
                            sb2.append("setting valid adv ");
                            sb2.append(str);
                            sb2.append(" for placement ");
                            sb2.append(placement.getId());
                            Log.w(str3, sb2.toString());
                        } else {
                            this.repository.deleteAdvertisement(str);
                            if (placement.isAutoCached()) {
                                this.adLoader.loadEndless(placement.getId(), 1000);
                            }
                        }
                    }
                } else {
                    Log.d(TAG, String.format(Locale.ENGLISH, "Placement %s is no longer valid, deleting it and its advertisement", new Object[]{placement.getId()}));
                    this.repository.delete(placement);
                }
            }
            for (Advertisement advertisement2 : list) {
                if (advertisement2.getState() == 2) {
                    hashSet.add(advertisement2.getId());
                    String str4 = TAG;
                    StringBuilder sb3 = new StringBuilder();
                    sb3.append("found adv in viewing state ");
                    sb3.append(advertisement2.getId());
                    Log.d(str4, sb3.toString());
                } else if (!hashSet.contains(advertisement2.getId())) {
                    String str5 = TAG;
                    StringBuilder sb4 = new StringBuilder();
                    sb4.append("delete ad ");
                    sb4.append(advertisement2.getId());
                    Log.e(str5, sb4.toString());
                    this.repository.deleteAdvertisement(advertisement2.getId());
                }
            }
            for (File file : listFiles) {
                if (!hashSet.contains(file.getName())) {
                    Log.v(TAG, String.format(Locale.ENGLISH, "Deleting assets under directory %s", new Object[]{file.getName()}));
                    FileUtility.delete(file);
                }
            }
            return 0;
        } catch (DBException unused) {
            return 1;
        } catch (IOException e) {
            Log.e(TAG, "Failed to delete asset directory!", e);
            return 1;
        }
    }

    public static JobInfo makeJobInfo() {
        return new JobInfo(TAG).setPriority(0).setUpdateCurrent(true);
    }
}
