package com.vungle.warren;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.util.Pair;
import com.vungle.warren.analytics.JobDelegateAnalytics;
import com.vungle.warren.analytics.MoatTracker;
import com.vungle.warren.model.Advertisement;
import com.vungle.warren.model.Cookie;
import com.vungle.warren.model.Placement;
import com.vungle.warren.persistence.Repository;
import com.vungle.warren.tasks.JobRunner;
import com.vungle.warren.ui.CloseDelegate;
import com.vungle.warren.ui.JavascriptBridge;
import com.vungle.warren.ui.OrientationDelegate;
import com.vungle.warren.ui.contract.AdContract.AdView;
import com.vungle.warren.ui.contract.AdContract.AdvertisementPresenter;
import com.vungle.warren.ui.contract.WebAdContract.WebAdPresenter;
import com.vungle.warren.ui.presenter.LocalAdPresenter;
import com.vungle.warren.ui.presenter.MRAIDAdPresenter;
import com.vungle.warren.ui.state.OptionsState;
import com.vungle.warren.ui.view.FullAdWidget;
import com.vungle.warren.ui.view.LocalAdView;
import com.vungle.warren.ui.view.MRAIDAdView;
import com.vungle.warren.ui.view.VungleWebClient;
import com.vungle.warren.utility.HandlerScheduler;
import com.vungle.warren.utility.SDKExecutors;
import java.io.File;

public class AdvertisementPresentationFactory {
    private static final String EXTRA_ADVERTISEMENT = "ADV_FACTORY_ADVERTISEMENT";
    /* access modifiers changed from: private */
    public static final String TAG = "AdvertisementPresentationFactory";
    /* access modifiers changed from: private */
    public Advertisement advertisement;
    /* access modifiers changed from: private */
    public VungleApiClient apiClient = Vungle._instance.vungleApiClient;
    /* access modifiers changed from: private */
    public final CloseDelegate closeDelegate;
    /* access modifiers changed from: private */
    public final JobRunner jobRunner = Vungle._instance.jobRunner;
    /* access modifiers changed from: private */
    public final OrientationDelegate orientationDelegate;
    /* access modifiers changed from: private */
    public final String placementId;
    /* access modifiers changed from: private */
    public Repository repository = Vungle._instance.repository;
    /* access modifiers changed from: private */
    public final Bundle savedState;
    private AsyncTask<Void, Void, PresentationResultHolder> task;

    public interface FullScreenCallback {
        void onResult(@Nullable Pair<AdView, AdvertisementPresenter> pair, @Nullable Exception exc);
    }

    private static class PresentationResultHolder {
        /* access modifiers changed from: private */
        public AdView adView;
        /* access modifiers changed from: private */
        public AdvertisementPresenter advertisementPresenter;
        /* access modifiers changed from: private */
        public String appId;
        /* access modifiers changed from: private */
        public Exception ex;
        /* access modifiers changed from: private */
        public MoatTracker tracker;
        /* access modifiers changed from: private */
        public VungleWebClient webClient;

        PresentationResultHolder(Exception exc) {
            this.ex = exc;
        }

        PresentationResultHolder(AdView adView2, AdvertisementPresenter advertisementPresenter2, VungleWebClient vungleWebClient, MoatTracker moatTracker, String str) {
            this.adView = adView2;
            this.advertisementPresenter = advertisementPresenter2;
            this.webClient = vungleWebClient;
            this.tracker = moatTracker;
            this.appId = str;
        }
    }

    public interface ViewCallback {
        void onResult(@Nullable Pair<WebAdPresenter, VungleWebClient> pair, @Nullable Exception exc);
    }

    public AdvertisementPresentationFactory(String str, Bundle bundle, OrientationDelegate orientationDelegate2, CloseDelegate closeDelegate2) throws InstantiationException {
        this.orientationDelegate = orientationDelegate2;
        this.closeDelegate = closeDelegate2;
        if (TextUtils.isEmpty(str)) {
            throw new InstantiationException("Missing placementID! Cannot start advertisement.");
        } else if (this.repository == null || !Vungle.isInitialized()) {
            throw new InstantiationException("Vungle SDK is not initialized");
        } else {
            this.placementId = str;
            this.savedState = bundle;
        }
    }

    /* access modifiers changed from: private */
    public Pair<Advertisement, Placement> loadPresentationData(String str, Bundle bundle) throws Exception {
        Placement placement = (Placement) this.repository.load(str, Placement.class).get();
        if (placement != null) {
            if (bundle == null) {
                this.advertisement = (Advertisement) this.repository.findValidAdvertisementForPlacement(str).get();
            } else {
                String string = bundle.getString(EXTRA_ADVERTISEMENT);
                if (!TextUtils.isEmpty(string)) {
                    this.advertisement = (Advertisement) this.repository.load(string, Advertisement.class).get();
                }
            }
            if (this.advertisement == null) {
                throw new Exception("No ad found");
            } else if (((File) this.repository.getAdvertisementAssetDirectory(this.advertisement.getId()).get()).isDirectory()) {
                return new Pair<>(this.advertisement, placement);
            } else {
                StringBuilder sb = new StringBuilder();
                sb.append("No asset directory for ");
                sb.append(str);
                sb.append(" exists!");
                throw new Exception(sb.toString());
            }
        } else {
            StringBuilder sb2 = new StringBuilder();
            sb2.append("No placement for ID ");
            sb2.append(str);
            sb2.append(" found. Please use a valid placement ID");
            throw new Exception(sb2.toString());
        }
    }

    @SuppressLint({"StaticFieldLeak"})
    public void getFullScreenPresentation(@NonNull Context context, @NonNull FullAdWidget fullAdWidget, @Nullable OptionsState optionsState, @NonNull FullScreenCallback fullScreenCallback) {
        if (this.task != null) {
            this.task.cancel(true);
        }
        final SDKExecutors sDKExecutors = Vungle.sdkExecutors;
        final FullAdWidget fullAdWidget2 = fullAdWidget;
        final OptionsState optionsState2 = optionsState;
        final Context context2 = context;
        final FullScreenCallback fullScreenCallback2 = fullScreenCallback;
        AnonymousClass1 r2 = new AsyncTask<Void, Void, PresentationResultHolder>() {
            /* access modifiers changed from: protected */
            public PresentationResultHolder doInBackground(Void... voidArr) {
                try {
                    Pair access$200 = AdvertisementPresentationFactory.this.loadPresentationData(AdvertisementPresentationFactory.this.placementId, AdvertisementPresentationFactory.this.savedState);
                    AdvertisementPresentationFactory.this.advertisement = (Advertisement) access$200.first;
                    Placement placement = (Placement) access$200.second;
                    if (!Vungle.canPlayAd(AdvertisementPresentationFactory.this.advertisement)) {
                        Log.e(AdvertisementPresentationFactory.TAG, "Advertisement is null or assets are missing");
                        return new PresentationResultHolder(new Exception("Advertisement is null or assets are missing"));
                    }
                    JobDelegateAnalytics jobDelegateAnalytics = new JobDelegateAnalytics(AdvertisementPresentationFactory.this.jobRunner);
                    String str = null;
                    Cookie cookie = (Cookie) AdvertisementPresentationFactory.this.repository.load("appId", Cookie.class).get();
                    if (cookie != null && !TextUtils.isEmpty(cookie.getString("appId"))) {
                        str = cookie.getString("appId");
                    }
                    VungleWebClient vungleWebClient = new VungleWebClient(AdvertisementPresentationFactory.this.advertisement, placement);
                    File file = (File) AdvertisementPresentationFactory.this.repository.getAdvertisementAssetDirectory(AdvertisementPresentationFactory.this.advertisement.getId()).get();
                    switch (AdvertisementPresentationFactory.this.advertisement.getAdType()) {
                        case 0:
                            MoatTracker connect = MoatTracker.connect(fullAdWidget2.videoView, AdvertisementPresentationFactory.this.apiClient.getMoatEnabled());
                            LocalAdPresenter localAdPresenter = new LocalAdPresenter(AdvertisementPresentationFactory.this.advertisement, placement, AdvertisementPresentationFactory.this.repository, new HandlerScheduler(), jobDelegateAnalytics, connect, vungleWebClient, optionsState2, file, sDKExecutors.getIOExecutor(), sDKExecutors.getUIExecutor());
                            PresentationResultHolder presentationResultHolder = new PresentationResultHolder(new LocalAdView(context2, fullAdWidget2, AdvertisementPresentationFactory.this.orientationDelegate, AdvertisementPresentationFactory.this.closeDelegate), localAdPresenter, vungleWebClient, connect, str);
                            return presentationResultHolder;
                        case 1:
                            MRAIDAdPresenter mRAIDAdPresenter = new MRAIDAdPresenter(AdvertisementPresentationFactory.this.advertisement, placement, AdvertisementPresentationFactory.this.repository, new HandlerScheduler(), jobDelegateAnalytics, vungleWebClient, null, optionsState2, file, sDKExecutors.getIOExecutor(), sDKExecutors.getUIExecutor());
                            PresentationResultHolder presentationResultHolder2 = new PresentationResultHolder(new MRAIDAdView(context2, fullAdWidget2, AdvertisementPresentationFactory.this.orientationDelegate, AdvertisementPresentationFactory.this.closeDelegate), mRAIDAdPresenter, vungleWebClient, null, null);
                            return presentationResultHolder2;
                        default:
                            return new PresentationResultHolder(new Exception("No presenter available for ad type!"));
                    }
                } catch (Exception e) {
                    return new PresentationResultHolder(e);
                }
            }

            /* access modifiers changed from: protected */
            public void onPostExecute(PresentationResultHolder presentationResultHolder) {
                super.onPostExecute(presentationResultHolder);
                if (!isCancelled() && fullScreenCallback2 != null) {
                    if (presentationResultHolder.ex != null) {
                        Log.e(AdvertisementPresentationFactory.TAG, "Excpetion on creating presenter", presentationResultHolder.ex);
                        fullScreenCallback2.onResult(new Pair(null, null), presentationResultHolder.ex);
                        return;
                    }
                    fullAdWidget2.linkWebView(presentationResultHolder.webClient, new JavascriptBridge(presentationResultHolder.advertisementPresenter));
                    if (presentationResultHolder.tracker != null) {
                        presentationResultHolder.tracker.configure(AdvertisementPresentationFactory.this.placementId, AdvertisementPresentationFactory.this.advertisement, presentationResultHolder.appId);
                    }
                    fullScreenCallback2.onResult(new Pair(presentationResultHolder.adView, presentationResultHolder.advertisementPresenter), presentationResultHolder.ex);
                }
            }
        };
        this.task = r2;
        this.task.execute(new Void[0]);
    }

    @SuppressLint({"StaticFieldLeak"})
    public void getNativeViewPresentation(final DirectDownloadAdapter directDownloadAdapter, final ViewCallback viewCallback) {
        final SDKExecutors sDKExecutors = Vungle.sdkExecutors;
        this.task = new AsyncTask<Void, Void, PresentationResultHolder>() {
            /* access modifiers changed from: protected */
            public PresentationResultHolder doInBackground(Void... voidArr) {
                try {
                    Pair access$200 = AdvertisementPresentationFactory.this.loadPresentationData(AdvertisementPresentationFactory.this.placementId, AdvertisementPresentationFactory.this.savedState);
                    AdvertisementPresentationFactory.this.advertisement = (Advertisement) access$200.first;
                    if (AdvertisementPresentationFactory.this.advertisement.getAdType() != 1) {
                        return new PresentationResultHolder(new IllegalArgumentException("No presenter available for ad type!"));
                    }
                    Placement placement = (Placement) access$200.second;
                    if (!Vungle.canPlayAd(AdvertisementPresentationFactory.this.advertisement)) {
                        Log.e(AdvertisementPresentationFactory.TAG, "Advertisement is null or assets are missing");
                        return new PresentationResultHolder(new Exception("Advertisement is null or assets are missing"));
                    }
                    JobDelegateAnalytics jobDelegateAnalytics = new JobDelegateAnalytics(AdvertisementPresentationFactory.this.jobRunner);
                    VungleWebClient vungleWebClient = new VungleWebClient(AdvertisementPresentationFactory.this.advertisement, placement);
                    VungleWebClient vungleWebClient2 = vungleWebClient;
                    MRAIDAdPresenter mRAIDAdPresenter = new MRAIDAdPresenter(AdvertisementPresentationFactory.this.advertisement, placement, AdvertisementPresentationFactory.this.repository, new HandlerScheduler(), jobDelegateAnalytics, vungleWebClient2, directDownloadAdapter, null, (File) AdvertisementPresentationFactory.this.repository.getAdvertisementAssetDirectory(AdvertisementPresentationFactory.this.advertisement.getId()).get(), sDKExecutors.getIOExecutor(), sDKExecutors.getUIExecutor());
                    PresentationResultHolder presentationResultHolder = new PresentationResultHolder(null, mRAIDAdPresenter, vungleWebClient, null, null);
                    return presentationResultHolder;
                } catch (Exception e) {
                    return new PresentationResultHolder(e);
                }
            }

            /* access modifiers changed from: protected */
            public void onPostExecute(PresentationResultHolder presentationResultHolder) {
                super.onPostExecute(presentationResultHolder);
                if (!isCancelled() && viewCallback != null) {
                    viewCallback.onResult(new Pair((WebAdPresenter) presentationResultHolder.advertisementPresenter, presentationResultHolder.webClient), presentationResultHolder.ex);
                }
            }
        };
        this.task.execute(new Void[0]);
    }

    public void saveState(Bundle bundle) {
        bundle.putString(EXTRA_ADVERTISEMENT, this.advertisement == null ? null : this.advertisement.getId());
    }

    public void destroy() {
        if (this.task != null) {
            this.task.cancel(true);
        }
    }
}
