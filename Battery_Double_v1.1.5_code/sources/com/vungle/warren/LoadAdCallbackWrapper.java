package com.vungle.warren;

import java.util.concurrent.ExecutorService;

class LoadAdCallbackWrapper implements LoadAdCallback {
    private final ExecutorService executorService;
    /* access modifiers changed from: private */
    public final LoadAdCallback loadAdCallback;

    public LoadAdCallbackWrapper(ExecutorService executorService2, LoadAdCallback loadAdCallback2) {
        this.loadAdCallback = loadAdCallback2;
        this.executorService = executorService2;
    }

    public void onAdLoad(final String str) {
        if (this.loadAdCallback != null) {
            this.executorService.execute(new Runnable() {
                public void run() {
                    LoadAdCallbackWrapper.this.loadAdCallback.onAdLoad(str);
                }
            });
        }
    }

    public void onError(final String str, final Throwable th) {
        if (this.loadAdCallback != null) {
            this.executorService.execute(new Runnable() {
                public void run() {
                    LoadAdCallbackWrapper.this.loadAdCallback.onError(str, th);
                }
            });
        }
    }

    public boolean equals(Object obj) {
        boolean z = true;
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        LoadAdCallbackWrapper loadAdCallbackWrapper = (LoadAdCallbackWrapper) obj;
        if (this.loadAdCallback == null ? loadAdCallbackWrapper.loadAdCallback != null : !this.loadAdCallback.equals(loadAdCallbackWrapper.loadAdCallback)) {
            return false;
        }
        if (this.executorService != null) {
            z = this.executorService.equals(loadAdCallbackWrapper.executorService);
        } else if (loadAdCallbackWrapper.executorService != null) {
            z = false;
        }
        return z;
    }

    public int hashCode() {
        int i = 0;
        int hashCode = (this.loadAdCallback != null ? this.loadAdCallback.hashCode() : 0) * 31;
        if (this.executorService != null) {
            i = this.executorService.hashCode();
        }
        return hashCode + i;
    }
}
