package com.vungle.warren;

import android.annotation.SuppressLint;
import android.app.Application;
import android.app.UiModeManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.media.AudioManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.PowerManager;
import android.provider.Settings.Secure;
import android.provider.Settings.SettingNotFoundException;
import android.security.NetworkSecurityPolicy;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.annotation.VisibleForTesting;
import android.support.v4.content.PermissionChecker;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.WindowManager;
import android.webkit.URLUtil;
import android.webkit.WebSettings;
import com.appodealx.sdk.utils.RequestInfoKeys;
import com.facebook.places.model.PlaceFields;
import com.facebook.share.internal.ShareConstants;
import com.google.android.exoplayer2.util.MimeTypes;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.ads.identifier.AdvertisingIdClient.Info;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.mintegral.msdk.base.entity.CampaignUnit;
import com.moat.analytics.mobile.vng.MoatAnalytics;
import com.moat.analytics.mobile.vng.MoatOptions;
import com.tapjoy.TJAdUnitConstants;
import com.tapjoy.TJAdUnitConstants.String;
import com.tapjoy.TapjoyConstants;
import com.vungle.warren.error.VungleError;
import com.vungle.warren.error.VungleException;
import com.vungle.warren.model.AdvertisementDBAdapter.AdvertisementColumns;
import com.vungle.warren.model.Cookie;
import com.vungle.warren.model.JsonUtil;
import com.vungle.warren.network.VungleApi;
import com.vungle.warren.persistence.CacheManager;
import com.vungle.warren.persistence.DatabaseHelper.DBException;
import com.vungle.warren.persistence.Repository;
import com.vungle.warren.utility.ViewUtility;
import io.fabric.sdk.android.services.settings.AppSettingsData;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.Interceptor.Chain;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.OkHttpClient.Builder;
import okhttp3.Protocol;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okio.Buffer;
import okio.BufferedSink;
import okio.GzipSink;
import okio.Okio;
import okio.Sink;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class VungleApiClient {
    private static String BASE_URL = "https://ads.api.vungle.com/";
    static String HEADER_UA = (MANUFACTURER_AMAZON.equals(Build.MANUFACTURER) ? "VungleAmazon/6.4.11" : "VungleDroid/6.4.11");
    static final String MANUFACTURER_AMAZON = "Amazon";
    private static Set<Interceptor> logInterceptors = new HashSet();
    private static Set<Interceptor> networkInterceptors = new HashSet();
    private final String TAG = "VungleApiClient";
    private VungleApi api;
    private JsonObject appBody;
    private CacheManager cacheManager;
    private OkHttpClient client;
    /* access modifiers changed from: private */
    public Context context;
    private boolean defaultIdFallbackDisabled;
    /* access modifiers changed from: private */
    public JsonObject deviceBody;
    private boolean enableMoat;
    private VungleApi gzipApi;
    private String newEndpoint;
    private String reportAdEndpoint;
    private Repository repository;
    private String requestAdEndpoint;
    /* access modifiers changed from: private */
    public Map<String, Long> retryAfterDataMap = new ConcurrentHashMap();
    private String riEndpoint;
    private boolean shouldTransmitIMEI;
    private VungleApi timeoutApi;
    /* access modifiers changed from: private */
    public String uaString = System.getProperty("http.agent");
    private JsonObject userBody;
    private String userImei;
    private boolean willPlayAdEnabled;
    private String willPlayAdEndpoint;
    private int willPlayAdTimeout;

    public static class ClearTextTrafficException extends IOException {
        ClearTextTrafficException(String str) {
            super(str);
        }
    }

    static class GzipRequestInterceptor implements Interceptor {
        private static final String CONTENT_ENCODING = "Content-Encoding";
        private static final String GZIP = "gzip";

        GzipRequestInterceptor() {
        }

        @NonNull
        public Response intercept(@NonNull Chain chain) throws IOException {
            Request request = chain.request();
            if (request.body() == null || request.header("Content-Encoding") != null) {
                return chain.proceed(request);
            }
            return chain.proceed(request.newBuilder().header("Content-Encoding", "gzip").method(request.method(), gzip(request.body())).build());
        }

        private RequestBody gzip(final RequestBody requestBody) throws IOException {
            final Buffer buffer = new Buffer();
            BufferedSink buffer2 = Okio.buffer((Sink) new GzipSink(buffer));
            requestBody.writeTo(buffer2);
            buffer2.close();
            return new RequestBody() {
                public MediaType contentType() {
                    return requestBody.contentType();
                }

                public long contentLength() {
                    return buffer.size();
                }

                public void writeTo(@NonNull BufferedSink bufferedSink) throws IOException {
                    bufferedSink.write(buffer.snapshot());
                }
            };
        }
    }

    public enum WrapperFramework {
        admob,
        air,
        cocos2dx,
        corona,
        dfp,
        heyzap,
        marmalade,
        mopub,
        unity,
        fyber,
        ironsource,
        upsight,
        appodeal,
        aerserv,
        adtoapp,
        tapdaq,
        vunglehbs,
        none
    }

    private String getConnectionTypeDetail(int i) {
        switch (i) {
            case 1:
                return "GPRS";
            case 2:
                return "EDGE";
            case 3:
                return "UMTS";
            case 4:
                return "CDMA";
            case 5:
                return "EVDO_0";
            case 6:
                return "EVDO_A";
            case 7:
                return "1xRTT";
            case 8:
                return "HSDPA";
            case 9:
                return "HSUPA";
            case 10:
                return "HSPA";
            case 11:
                return "IDEN";
            case 12:
                return "EVDO_B";
            case 13:
                return "LTE";
            case 14:
                return "EHPRD";
            case 15:
                return "HSPAP";
            case 16:
                return "GSM";
            case 17:
                return "TD_SCDMA";
            case 18:
                return "IWLAN";
            default:
                return "UNKNOWN";
        }
    }

    VungleApiClient(Context context2, String str, CacheManager cacheManager2, Repository repository2) {
        this.context = context2.getApplicationContext();
        Builder addInterceptor = new Builder().addInterceptor(new Interceptor() {
            public Response intercept(Chain chain) throws IOException {
                Request request = chain.request();
                String encodedPath = request.url().encodedPath();
                Long l = (Long) VungleApiClient.this.retryAfterDataMap.get(encodedPath);
                if (l != null) {
                    long seconds = TimeUnit.MILLISECONDS.toSeconds(l.longValue() - System.currentTimeMillis());
                    if (seconds > 0) {
                        StringBuilder sb = new StringBuilder();
                        sb.append("");
                        sb.append(seconds);
                        return new Response.Builder().request(request).addHeader("Retry-After", sb.toString()).code(TJAdUnitConstants.DEFAULT_VOLUME_CHECK_INTERVAL).protocol(Protocol.HTTP_1_1).message("Server is busy").body(ResponseBody.create(MediaType.parse("application/json; charset=utf-8"), "{\"Error\":\"Retry-After\"}")).build();
                    }
                    VungleApiClient.this.retryAfterDataMap.remove(encodedPath);
                }
                Response proceed = chain.proceed(request);
                if (proceed != null) {
                    int code = proceed.code();
                    if (code == 429 || code == 500 || code == 502 || code == 503) {
                        String str = proceed.headers().get("Retry-After");
                        if (!TextUtils.isEmpty(str)) {
                            try {
                                long parseLong = Long.parseLong(str);
                                if (parseLong > 0) {
                                    VungleApiClient.this.retryAfterDataMap.put(encodedPath, Long.valueOf((parseLong * 1000) + System.currentTimeMillis()));
                                }
                            } catch (NumberFormatException unused) {
                                Log.d("VungleApiClient", "Retry-After value is not an valid value");
                            }
                        }
                    }
                }
                return proceed;
            }
        });
        this.client = addInterceptor.build();
        OkHttpClient build = addInterceptor.addInterceptor(new GzipRequestInterceptor()).build();
        Retrofit build2 = new Retrofit.Builder().baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create()).client(this.client).build();
        this.api = (VungleApi) build2.create(VungleApi.class);
        this.gzipApi = (VungleApi) build2.newBuilder().client(build).build().create(VungleApi.class);
        init(context2, str, cacheManager2, repository2);
    }

    public void updateIMEI(String str, boolean z) {
        this.userImei = str;
        this.shouldTransmitIMEI = z;
    }

    public void setDefaultIdFallbackDisabled(boolean z) {
        this.defaultIdFallbackDisabled = z;
    }

    private synchronized void init(final Context context2, String str, CacheManager cacheManager2, Repository repository2) {
        this.repository = repository2;
        this.shouldTransmitIMEI = false;
        this.cacheManager = cacheManager2;
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("id", str);
        jsonObject.addProperty(String.BUNDLE, context2.getPackageName());
        String str2 = null;
        try {
            str2 = context2.getPackageManager().getPackageInfo(context2.getPackageName(), 0).versionName;
        } catch (NameNotFoundException unused) {
        }
        String str3 = "ver";
        if (str2 == null) {
            str2 = "1.0";
        }
        jsonObject.addProperty(str3, str2);
        JsonObject jsonObject2 = new JsonObject();
        jsonObject2.addProperty("make", Build.MANUFACTURER);
        jsonObject2.addProperty("model", Build.MODEL);
        jsonObject2.addProperty("osv", VERSION.RELEASE);
        jsonObject2.addProperty("carrier", ((TelephonyManager) context2.getSystemService(PlaceFields.PHONE)).getNetworkOperatorName());
        jsonObject2.addProperty("os", MANUFACTURER_AMAZON.equals(Build.MANUFACTURER) ? "amazon" : "android");
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((WindowManager) context2.getSystemService("window")).getDefaultDisplay().getMetrics(displayMetrics);
        jsonObject2.addProperty("w", (Number) Integer.valueOf(displayMetrics.widthPixels));
        jsonObject2.addProperty("h", (Number) Integer.valueOf(displayMetrics.heightPixels));
        JsonObject jsonObject3 = new JsonObject();
        jsonObject3.add("vungle", new JsonObject());
        jsonObject2.add(RequestInfoKeys.EXT, jsonObject3);
        try {
            if (VERSION.SDK_INT >= 17) {
                this.uaString = getUserAgentFromCookie();
                initUserAgentLazy();
            } else if (Looper.getMainLooper() == Looper.myLooper()) {
                this.uaString = ViewUtility.getWebView(context2.getApplicationContext()).getSettings().getUserAgentString();
            } else {
                final CountDownLatch countDownLatch = new CountDownLatch(1);
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    public void run() {
                        try {
                            VungleApiClient.this.uaString = ViewUtility.getWebView(context2.getApplicationContext()).getSettings().getUserAgentString();
                        } catch (InstantiationException e) {
                            StringBuilder sb = new StringBuilder();
                            sb.append("Cannot Get UserAgent. Setting Default Device UserAgent.");
                            sb.append(e.getLocalizedMessage());
                            Log.e("VungleApiClient", sb.toString());
                        }
                        countDownLatch.countDown();
                    }
                });
                if (!countDownLatch.await(2, TimeUnit.SECONDS)) {
                    Log.e("VungleApiClient", "Unable to get User Agent String in specified time");
                }
            }
        } catch (Exception e) {
            String str4 = "VungleApiClient";
            StringBuilder sb = new StringBuilder();
            sb.append("Cannot Get UserAgent. Setting Default Device UserAgent.");
            sb.append(e.getLocalizedMessage());
            Log.e(str4, sb.toString());
        }
        jsonObject2.addProperty("ua", this.uaString);
        this.deviceBody = jsonObject2;
        this.appBody = jsonObject;
    }

    @RequiresApi(api = 17)
    private void initUserAgentLazy() {
        new Thread(new Runnable() {
            public void run() {
                try {
                    VungleApiClient.this.uaString = WebSettings.getDefaultUserAgent(VungleApiClient.this.context);
                    VungleApiClient.this.deviceBody.addProperty("ua", VungleApiClient.this.uaString);
                    VungleApiClient.this.addUserAgentInCookie(VungleApiClient.this.uaString);
                } catch (Exception e) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("Cannot Get UserAgent. Setting Default Device UserAgent.");
                    sb.append(e.getLocalizedMessage());
                    Log.e("VungleApiClient", sb.toString());
                }
            }
        }).start();
    }

    public retrofit2.Response<JsonObject> config() throws VungleException, IOException {
        JsonObject jsonObject = new JsonObject();
        jsonObject.add("device", getDeviceBody());
        jsonObject.add("app", this.appBody);
        jsonObject.add("user", getUserBody());
        JsonObject jsonObject2 = new JsonObject();
        jsonObject2.addProperty("is_auto_cached_enforced", Boolean.valueOf(false));
        jsonObject.add(ShareConstants.WEB_DIALOG_RESULT_PARAM_REQUEST_ID, jsonObject2);
        retrofit2.Response<JsonObject> execute = this.api.config(HEADER_UA, jsonObject).execute();
        if (!execute.isSuccessful()) {
            return execute;
        }
        JsonObject jsonObject3 = (JsonObject) execute.body();
        StringBuilder sb = new StringBuilder();
        sb.append("Config Response: ");
        sb.append(jsonObject3);
        Log.d("VungleApiClient", sb.toString());
        if (JsonUtil.hasNonNull(jsonObject3, "sleep")) {
            String asString = JsonUtil.hasNonNull(jsonObject3, String.VIDEO_INFO) ? jsonObject3.get(String.VIDEO_INFO).getAsString() : "";
            StringBuilder sb2 = new StringBuilder();
            sb2.append("Error Initializing Vungle. Please try again. ");
            sb2.append(asString);
            Log.e("VungleApiClient", sb2.toString());
            throw new VungleException(3);
        } else if (JsonUtil.hasNonNull(jsonObject3, "endpoints")) {
            JsonObject asJsonObject = jsonObject3.getAsJsonObject("endpoints");
            HttpUrl parse = HttpUrl.parse(asJsonObject.get(AppSettingsData.STATUS_NEW).getAsString());
            HttpUrl parse2 = HttpUrl.parse(asJsonObject.get(CampaignUnit.JSON_KEY_ADS).getAsString());
            HttpUrl parse3 = HttpUrl.parse(asJsonObject.get("will_play_ad").getAsString());
            HttpUrl parse4 = HttpUrl.parse(asJsonObject.get("report_ad").getAsString());
            HttpUrl parse5 = HttpUrl.parse(asJsonObject.get("ri").getAsString());
            if (parse == null || parse2 == null || parse3 == null || parse4 == null || parse5 == null) {
                Log.e("VungleApiClient", "Error Initializing Vungle. Please try again. ");
                throw new VungleException(3);
            }
            this.newEndpoint = parse.toString();
            this.requestAdEndpoint = parse2.toString();
            this.willPlayAdEndpoint = parse3.toString();
            this.reportAdEndpoint = parse4.toString();
            this.riEndpoint = parse5.toString();
            JsonObject asJsonObject2 = jsonObject3.getAsJsonObject("will_play_ad");
            this.willPlayAdTimeout = asJsonObject2.get("request_timeout").getAsInt();
            this.willPlayAdEnabled = asJsonObject2.get(String.ENABLED).getAsBoolean();
            this.enableMoat = jsonObject3.getAsJsonObject("viewability").get("moat").getAsBoolean();
            if (this.willPlayAdEnabled) {
                Log.v("VungleApiClient", "willPlayAd is enabled, generating a timeout client.");
                this.timeoutApi = (VungleApi) new Retrofit.Builder().client(this.client.newBuilder().readTimeout((long) this.willPlayAdTimeout, TimeUnit.MILLISECONDS).build()).addConverterFactory(GsonConverterFactory.create()).baseUrl("https://api.vungle.com/").build().create(VungleApi.class);
            }
            if (getMoatEnabled()) {
                MoatOptions moatOptions = new MoatOptions();
                moatOptions.disableAdIdCollection = true;
                moatOptions.disableLocationServices = true;
                moatOptions.loggingEnabled = true;
                MoatAnalytics.getInstance().start(moatOptions, (Application) this.context.getApplicationContext());
            }
            return execute;
        } else {
            Log.e("VungleApiClient", "Error Initializing Vungle. Please try again. ");
            throw new VungleException(3);
        }
    }

    public Call<JsonObject> reportNew() throws IllegalStateException {
        if (this.newEndpoint != null) {
            HashMap hashMap = new HashMap(2);
            JsonElement jsonElement = this.appBody.get("id");
            JsonElement jsonElement2 = this.deviceBody.get("ifa");
            hashMap.put("app_id", jsonElement != null ? jsonElement.getAsString() : "");
            hashMap.put("ifa", jsonElement2 != null ? jsonElement2.getAsString() : "");
            return this.api.reportNew(HEADER_UA, this.newEndpoint, hashMap);
        }
        throw new IllegalStateException("API Client not configured yet! Must call /config first.");
    }

    public Call<JsonObject> requestAd(String str, boolean z) throws IllegalStateException {
        if (this.requestAdEndpoint != null) {
            JsonObject jsonObject = new JsonObject();
            jsonObject.add("device", getDeviceBody());
            jsonObject.add("app", this.appBody);
            jsonObject.add("user", getUserBody());
            JsonObject jsonObject2 = new JsonObject();
            JsonArray jsonArray = new JsonArray();
            jsonArray.add(str);
            jsonObject2.add("placements", jsonArray);
            jsonObject2.addProperty("header_bidding", Boolean.valueOf(z));
            jsonObject.add(ShareConstants.WEB_DIALOG_RESULT_PARAM_REQUEST_ID, jsonObject2);
            return this.api.ads(HEADER_UA, this.requestAdEndpoint, jsonObject);
        }
        throw new IllegalStateException("API Client not configured yet! Must call /config first.");
    }

    public Call<JsonObject> willPlayAd(String str, boolean z, String str2) throws IllegalStateException, VungleError {
        if (this.willPlayAdEndpoint == null) {
            throw new IllegalStateException("API Client not configured yet! Must call /config first.");
        } else if (this.willPlayAdEnabled) {
            JsonObject jsonObject = new JsonObject();
            jsonObject.add("device", getDeviceBody());
            jsonObject.add("app", this.appBody);
            jsonObject.add("user", getUserBody());
            JsonObject jsonObject2 = new JsonObject();
            JsonObject jsonObject3 = new JsonObject();
            jsonObject3.addProperty("reference_id", str);
            jsonObject3.addProperty("is_auto_cached", Boolean.valueOf(z));
            jsonObject2.add("placement", jsonObject3);
            jsonObject2.addProperty(AdvertisementColumns.COLUMN_AD_TOKEN, str2);
            jsonObject.add(ShareConstants.WEB_DIALOG_RESULT_PARAM_REQUEST_ID, jsonObject2);
            return this.timeoutApi.willPlayAd(HEADER_UA, this.willPlayAdEndpoint, jsonObject);
        } else {
            throw new VungleError(6);
        }
    }

    public Call<JsonObject> reportAd(JsonObject jsonObject) {
        if (this.reportAdEndpoint != null) {
            JsonObject jsonObject2 = new JsonObject();
            jsonObject2.add("device", getDeviceBody());
            jsonObject2.add("app", this.appBody);
            jsonObject2.add(ShareConstants.WEB_DIALOG_RESULT_PARAM_REQUEST_ID, jsonObject);
            jsonObject2.add("user", getUserBody());
            return this.gzipApi.reportAd(HEADER_UA, this.reportAdEndpoint, jsonObject2);
        }
        throw new IllegalStateException("API Client not configured yet! Must call /config first.");
    }

    public Call<JsonObject> ri(JsonObject jsonObject) {
        if (this.riEndpoint != null) {
            JsonObject jsonObject2 = new JsonObject();
            jsonObject2.add("device", getDeviceBody());
            jsonObject2.add("app", this.appBody);
            jsonObject2.add(ShareConstants.WEB_DIALOG_RESULT_PARAM_REQUEST_ID, jsonObject);
            return this.api.ri(HEADER_UA, this.riEndpoint, jsonObject2);
        }
        throw new IllegalStateException("API Client not configured yet! Must call /config first.");
    }

    public boolean pingTPAT(String str) throws ClearTextTrafficException, MalformedURLException {
        if (TextUtils.isEmpty(str) || HttpUrl.parse(str) == null) {
            StringBuilder sb = new StringBuilder();
            sb.append("Invalid URL : ");
            sb.append(str);
            throw new MalformedURLException(sb.toString());
        }
        try {
            boolean z = VERSION.SDK_INT >= 24 ? NetworkSecurityPolicy.getInstance().isCleartextTrafficPermitted(new URL(str).getHost()) : VERSION.SDK_INT >= 23 ? NetworkSecurityPolicy.getInstance().isCleartextTrafficPermitted() : true;
            if (z || !URLUtil.isHttpUrl(str)) {
                if (!TextUtils.isEmpty(this.userImei) && this.shouldTransmitIMEI) {
                    str = str.replace("%imei%", this.userImei);
                }
                try {
                    this.api.pingTPAT(this.uaString, str).execute();
                    return true;
                } catch (IOException unused) {
                    return false;
                }
            } else {
                throw new ClearTextTrafficException("Clear Text Traffic is blocked");
            }
        } catch (MalformedURLException unused2) {
            StringBuilder sb2 = new StringBuilder();
            sb2.append("Invalid URL : ");
            sb2.append(str);
            throw new MalformedURLException(sb2.toString());
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:106:0x01b3  */
    /* JADX WARNING: Removed duplicated region for block: B:131:0x0260  */
    /* JADX WARNING: Removed duplicated region for block: B:169:0x0363  */
    /* JADX WARNING: Removed duplicated region for block: B:172:0x03ae  */
    /* JADX WARNING: Removed duplicated region for block: B:173:0x03b1  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x009b  */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x00b5  */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x00ee  */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x00f0  */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x010f  */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x015d  */
    /* JADX WARNING: Removed duplicated region for block: B:83:0x0160  */
    /* JADX WARNING: Removed duplicated region for block: B:97:0x018b  */
    @SuppressLint({"HardwareIds"})
    private JsonObject getDeviceBody() throws IllegalStateException {
        boolean z;
        String str;
        int intExtra;
        int intExtra2;
        int intExtra3;
        String str2;
        String str3;
        String str4;
        boolean z2;
        JsonObject jsonObject = new JsonObject();
        boolean z3 = false;
        try {
            if (MANUFACTURER_AMAZON.equals(Build.MANUFACTURER)) {
                try {
                    ContentResolver contentResolver = this.context.getContentResolver();
                    z = Secure.getInt(contentResolver, "limit_ad_tracking") == 1;
                    try {
                        str = Secure.getString(contentResolver, TapjoyConstants.TJC_ADVERTISING_ID);
                    } catch (SettingNotFoundException e) {
                        e = e;
                        try {
                            Log.w("VungleApiClient", "Error getting Amazon advertising info", e);
                            str = null;
                        } catch (Exception unused) {
                            str = null;
                            Log.e("VungleApiClient", "Cannot load Advertising ID");
                            if (str == null) {
                            }
                            this.deviceBody.addProperty("lmt", (Number) Integer.valueOf(!z ? 1 : 0));
                            boolean z4 = false;
                            for (PackageInfo packageInfo : this.context.getPackageManager().getInstalledPackages(128)) {
                            }
                            jsonObject.addProperty("is_google_play_services_available", Boolean.valueOf(z4));
                            Intent registerReceiver = this.context.registerReceiver(null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
                            intExtra = registerReceiver.getIntExtra("level", -1);
                            intExtra2 = registerReceiver.getIntExtra("scale", -1);
                            jsonObject.addProperty("battery_level", (Number) Float.valueOf(((float) intExtra) / ((float) intExtra2)));
                            intExtra3 = registerReceiver.getIntExtra("status", -1);
                            if (intExtra3 != -1) {
                            }
                            jsonObject.addProperty("battery_state", str2);
                            if (VERSION.SDK_INT >= 21) {
                            }
                            if (PermissionChecker.checkCallingOrSelfPermission(this.context, "android.permission.ACCESS_NETWORK_STATE") == 0) {
                            }
                            jsonObject.addProperty("locale", Locale.getDefault().toString());
                            jsonObject.addProperty("language", Locale.getDefault().getLanguage());
                            jsonObject.addProperty("time_zone", TimeZone.getDefault().getID());
                            if (this.context == null) {
                            }
                            jsonObject.addProperty("sd_card_available", (Number) Integer.valueOf(Environment.getExternalStorageState().equals("mounted") ? 1 : 0));
                            jsonObject.addProperty("os_name", Build.FINGERPRINT);
                            jsonObject.addProperty("vduid", "");
                            this.deviceBody.getAsJsonObject(RequestInfoKeys.EXT).getAsJsonObject("vungle").add(!MANUFACTURER_AMAZON.equals(Build.MANUFACTURER) ? "amazon" : "android", jsonObject);
                            return this.deviceBody;
                        }
                        if (str == null) {
                        }
                        this.deviceBody.addProperty("lmt", (Number) Integer.valueOf(!z ? 1 : 0));
                        boolean z42 = false;
                        while (r4.hasNext()) {
                        }
                        jsonObject.addProperty("is_google_play_services_available", Boolean.valueOf(z42));
                        Intent registerReceiver2 = this.context.registerReceiver(null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
                        intExtra = registerReceiver2.getIntExtra("level", -1);
                        intExtra2 = registerReceiver2.getIntExtra("scale", -1);
                        jsonObject.addProperty("battery_level", (Number) Float.valueOf(((float) intExtra) / ((float) intExtra2)));
                        intExtra3 = registerReceiver2.getIntExtra("status", -1);
                        if (intExtra3 != -1) {
                        }
                        jsonObject.addProperty("battery_state", str2);
                        if (VERSION.SDK_INT >= 21) {
                        }
                        if (PermissionChecker.checkCallingOrSelfPermission(this.context, "android.permission.ACCESS_NETWORK_STATE") == 0) {
                        }
                        jsonObject.addProperty("locale", Locale.getDefault().toString());
                        jsonObject.addProperty("language", Locale.getDefault().getLanguage());
                        jsonObject.addProperty("time_zone", TimeZone.getDefault().getID());
                        if (this.context == null) {
                        }
                        jsonObject.addProperty("sd_card_available", (Number) Integer.valueOf(Environment.getExternalStorageState().equals("mounted") ? 1 : 0));
                        jsonObject.addProperty("os_name", Build.FINGERPRINT);
                        jsonObject.addProperty("vduid", "");
                        this.deviceBody.getAsJsonObject(RequestInfoKeys.EXT).getAsJsonObject("vungle").add(!MANUFACTURER_AMAZON.equals(Build.MANUFACTURER) ? "amazon" : "android", jsonObject);
                        return this.deviceBody;
                    }
                } catch (SettingNotFoundException e2) {
                    e = e2;
                    z = true;
                    Log.w("VungleApiClient", "Error getting Amazon advertising info", e);
                    str = null;
                    if (str == null) {
                    }
                    this.deviceBody.addProperty("lmt", (Number) Integer.valueOf(!z ? 1 : 0));
                    boolean z422 = false;
                    while (r4.hasNext()) {
                    }
                    jsonObject.addProperty("is_google_play_services_available", Boolean.valueOf(z422));
                    Intent registerReceiver22 = this.context.registerReceiver(null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
                    intExtra = registerReceiver22.getIntExtra("level", -1);
                    intExtra2 = registerReceiver22.getIntExtra("scale", -1);
                    jsonObject.addProperty("battery_level", (Number) Float.valueOf(((float) intExtra) / ((float) intExtra2)));
                    intExtra3 = registerReceiver22.getIntExtra("status", -1);
                    if (intExtra3 != -1) {
                    }
                    jsonObject.addProperty("battery_state", str2);
                    if (VERSION.SDK_INT >= 21) {
                    }
                    if (PermissionChecker.checkCallingOrSelfPermission(this.context, "android.permission.ACCESS_NETWORK_STATE") == 0) {
                    }
                    jsonObject.addProperty("locale", Locale.getDefault().toString());
                    jsonObject.addProperty("language", Locale.getDefault().getLanguage());
                    jsonObject.addProperty("time_zone", TimeZone.getDefault().getID());
                    if (this.context == null) {
                    }
                    jsonObject.addProperty("sd_card_available", (Number) Integer.valueOf(Environment.getExternalStorageState().equals("mounted") ? 1 : 0));
                    jsonObject.addProperty("os_name", Build.FINGERPRINT);
                    jsonObject.addProperty("vduid", "");
                    this.deviceBody.getAsJsonObject(RequestInfoKeys.EXT).getAsJsonObject("vungle").add(!MANUFACTURER_AMAZON.equals(Build.MANUFACTURER) ? "amazon" : "android", jsonObject);
                    return this.deviceBody;
                }
                if (str == null) {
                    jsonObject.addProperty(MANUFACTURER_AMAZON.equals(Build.MANUFACTURER) ? "amazon_advertising_id" : "gaid", str);
                    this.deviceBody.addProperty("ifa", str);
                } else {
                    String string = Secure.getString(this.context.getContentResolver(), TapjoyConstants.TJC_ANDROID_ID);
                    JsonObject jsonObject2 = this.deviceBody;
                    String str5 = "ifa";
                    String str6 = this.defaultIdFallbackDisabled ? "" : !TextUtils.isEmpty(string) ? string : "";
                    jsonObject2.addProperty(str5, str6);
                    if (!TextUtils.isEmpty(string) && !this.defaultIdFallbackDisabled) {
                        jsonObject.addProperty(TapjoyConstants.TJC_ANDROID_ID, string);
                    }
                }
                this.deviceBody.addProperty("lmt", (Number) Integer.valueOf(!z ? 1 : 0));
                boolean z4222 = false;
                while (r4.hasNext()) {
                    if (packageInfo.packageName.equalsIgnoreCase("com.google.android.gms")) {
                        z4222 = true;
                    }
                }
                jsonObject.addProperty("is_google_play_services_available", Boolean.valueOf(z4222));
                Intent registerReceiver222 = this.context.registerReceiver(null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
                intExtra = registerReceiver222.getIntExtra("level", -1);
                intExtra2 = registerReceiver222.getIntExtra("scale", -1);
                if (intExtra > 0 && intExtra2 > 0) {
                    jsonObject.addProperty("battery_level", (Number) Float.valueOf(((float) intExtra) / ((float) intExtra2)));
                }
                intExtra3 = registerReceiver222.getIntExtra("status", -1);
                if (intExtra3 != -1) {
                    str2 = "UNKNOWN";
                } else if (intExtra3 == 2 || intExtra3 == 5) {
                    int intExtra4 = registerReceiver222.getIntExtra("plugged", -1);
                    if (intExtra4 != 4) {
                        switch (intExtra4) {
                            case 1:
                                str2 = "BATTERY_PLUGGED_AC";
                                break;
                            case 2:
                                str2 = "BATTERY_PLUGGED_USB";
                                break;
                            default:
                                str2 = "BATTERY_PLUGGED_OTHERS";
                                break;
                        }
                    } else {
                        str2 = "BATTERY_PLUGGED_WIRELESS";
                    }
                } else {
                    str2 = "NOT_CHARGING";
                }
                jsonObject.addProperty("battery_state", str2);
                if (VERSION.SDK_INT >= 21) {
                    PowerManager powerManager = (PowerManager) this.context.getSystemService("power");
                    jsonObject.addProperty("battery_saver_enabled", (Number) Integer.valueOf((powerManager == null || !powerManager.isPowerSaveMode()) ? 0 : 1));
                }
                if (PermissionChecker.checkCallingOrSelfPermission(this.context, "android.permission.ACCESS_NETWORK_STATE") == 0) {
                    String str7 = "NONE";
                    String str8 = "NONE";
                    ConnectivityManager connectivityManager = (ConnectivityManager) this.context.getSystemService("connectivity");
                    if (connectivityManager != null) {
                        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
                        if (activeNetworkInfo != null) {
                            switch (activeNetworkInfo.getType()) {
                                case 0:
                                    str7 = "MOBILE";
                                    str8 = getConnectionTypeDetail(activeNetworkInfo.getSubtype());
                                    break;
                                case 1:
                                case 6:
                                    str7 = "WIFI";
                                    str8 = "WIFI";
                                    break;
                                case 7:
                                    str7 = "BLUETOOTH";
                                    str8 = "BLUETOOTH";
                                    break;
                                case 9:
                                    str7 = "ETHERNET";
                                    str8 = "ETHERNET";
                                    break;
                                default:
                                    str7 = "UNKNOWN";
                                    str8 = "UNKNOWN";
                                    break;
                            }
                        }
                    }
                    jsonObject.addProperty(TapjoyConstants.TJC_CONNECTION_TYPE, str7);
                    jsonObject.addProperty("connection_type_detail", str8);
                    if (VERSION.SDK_INT >= 24) {
                        if (connectivityManager.isActiveNetworkMetered()) {
                            switch (connectivityManager.getRestrictBackgroundStatus()) {
                                case 1:
                                    str3 = "DISABLED";
                                    break;
                                case 2:
                                    str3 = "WHITELISTED";
                                    break;
                                case 3:
                                    str3 = "ENABLED";
                                    break;
                                default:
                                    str3 = "UNKNOWN";
                                    break;
                            }
                            jsonObject.addProperty("data_saver_status", str3);
                            jsonObject.addProperty("network_metered", (Number) Integer.valueOf(1));
                        } else {
                            jsonObject.addProperty("data_saver_status", "NOT_APPLICABLE");
                            jsonObject.addProperty("network_metered", (Number) Integer.valueOf(0));
                        }
                    }
                }
                jsonObject.addProperty("locale", Locale.getDefault().toString());
                jsonObject.addProperty("language", Locale.getDefault().getLanguage());
                jsonObject.addProperty("time_zone", TimeZone.getDefault().getID());
                if (this.context == null) {
                    AudioManager audioManager = (AudioManager) this.context.getSystemService(MimeTypes.BASE_TYPE_AUDIO);
                    if (audioManager != null) {
                        int streamMaxVolume = audioManager.getStreamMaxVolume(3);
                        int streamVolume = audioManager.getStreamVolume(3);
                        jsonObject.addProperty("volume_level", (Number) Float.valueOf(((float) streamVolume) / ((float) streamMaxVolume)));
                        jsonObject.addProperty("sound_enabled", (Number) Integer.valueOf(streamVolume > 0 ? 1 : 0));
                    }
                    File cache = this.cacheManager.getCache();
                    cache.getPath();
                    if (cache.exists() && cache.isDirectory()) {
                        jsonObject.addProperty("storage_bytes_available", (Number) Long.valueOf(this.cacheManager.getBytesAvailable()));
                    }
                    boolean z5 = MANUFACTURER_AMAZON.equals(Build.MANUFACTURER) ? this.context.getApplicationContext().getPackageManager().hasSystemFeature("amazon.hardware.fire_tv") : VERSION.SDK_INT < 23 ? this.context.getApplicationContext().getPackageManager().hasSystemFeature("com.google.android.tv") || !this.context.getApplicationContext().getPackageManager().hasSystemFeature("android.hardware.touchscreen") : ((UiModeManager) this.context.getSystemService("uimode")).getCurrentModeType() == 4;
                    jsonObject.addProperty("is_tv", Boolean.valueOf(z5));
                    jsonObject.addProperty("os_api_level", (Number) Integer.valueOf(VERSION.SDK_INT));
                    try {
                        if (VERSION.SDK_INT >= 26) {
                            if (this.context.checkCallingOrSelfPermission("android.permission.REQUEST_INSTALL_PACKAGES") == 0) {
                                z3 = this.context.getApplicationContext().getPackageManager().canRequestPackageInstalls();
                            }
                        } else if (Secure.getInt(this.context.getContentResolver(), "install_non_market_apps") == 1) {
                            z3 = true;
                        }
                    } catch (SettingNotFoundException e3) {
                        Log.e("VungleApiClient", "isInstallNonMarketAppsEnabled Settings not found", e3);
                    }
                    jsonObject.addProperty("is_sideload_enabled", Boolean.valueOf(z3));
                } else {
                    jsonObject.addProperty("volume_level", (Number) Integer.valueOf(0));
                    jsonObject.addProperty("sound_enabled", (Number) Integer.valueOf(0));
                }
                jsonObject.addProperty("sd_card_available", (Number) Integer.valueOf(Environment.getExternalStorageState().equals("mounted") ? 1 : 0));
                jsonObject.addProperty("os_name", Build.FINGERPRINT);
                jsonObject.addProperty("vduid", "");
                this.deviceBody.getAsJsonObject(RequestInfoKeys.EXT).getAsJsonObject("vungle").add(!MANUFACTURER_AMAZON.equals(Build.MANUFACTURER) ? "amazon" : "android", jsonObject);
                return this.deviceBody;
            }
            try {
                Info advertisingIdInfo = AdvertisingIdClient.getAdvertisingIdInfo(this.context);
                if (advertisingIdInfo != null) {
                    str4 = advertisingIdInfo.getId();
                    try {
                        z2 = advertisingIdInfo.isLimitAdTrackingEnabled();
                        try {
                            this.deviceBody.addProperty("ifa", str4);
                        } catch (NoClassDefFoundError e4) {
                            e = e4;
                        }
                    } catch (NoClassDefFoundError e5) {
                        e = e5;
                        z2 = true;
                        String str9 = "VungleApiClient";
                        try {
                            StringBuilder sb = new StringBuilder();
                            sb.append("Play services Not available: ");
                            sb.append(e.getLocalizedMessage());
                            Log.e(str9, sb.toString());
                            z = z2;
                            str = Secure.getString(this.context.getContentResolver(), TapjoyConstants.TJC_ADVERTISING_ID);
                        } catch (Exception unused2) {
                            String str10 = str4;
                            z = z2;
                            str = str10;
                            Log.e("VungleApiClient", "Cannot load Advertising ID");
                            if (str == null) {
                            }
                            this.deviceBody.addProperty("lmt", (Number) Integer.valueOf(!z ? 1 : 0));
                            boolean z42222 = false;
                            while (r4.hasNext()) {
                            }
                            jsonObject.addProperty("is_google_play_services_available", Boolean.valueOf(z42222));
                            Intent registerReceiver2222 = this.context.registerReceiver(null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
                            intExtra = registerReceiver2222.getIntExtra("level", -1);
                            intExtra2 = registerReceiver2222.getIntExtra("scale", -1);
                            jsonObject.addProperty("battery_level", (Number) Float.valueOf(((float) intExtra) / ((float) intExtra2)));
                            intExtra3 = registerReceiver2222.getIntExtra("status", -1);
                            if (intExtra3 != -1) {
                            }
                            jsonObject.addProperty("battery_state", str2);
                            if (VERSION.SDK_INT >= 21) {
                            }
                            if (PermissionChecker.checkCallingOrSelfPermission(this.context, "android.permission.ACCESS_NETWORK_STATE") == 0) {
                            }
                            jsonObject.addProperty("locale", Locale.getDefault().toString());
                            jsonObject.addProperty("language", Locale.getDefault().getLanguage());
                            jsonObject.addProperty("time_zone", TimeZone.getDefault().getID());
                            if (this.context == null) {
                            }
                            jsonObject.addProperty("sd_card_available", (Number) Integer.valueOf(Environment.getExternalStorageState().equals("mounted") ? 1 : 0));
                            jsonObject.addProperty("os_name", Build.FINGERPRINT);
                            jsonObject.addProperty("vduid", "");
                            this.deviceBody.getAsJsonObject(RequestInfoKeys.EXT).getAsJsonObject("vungle").add(!MANUFACTURER_AMAZON.equals(Build.MANUFACTURER) ? "amazon" : "android", jsonObject);
                            return this.deviceBody;
                        }
                        if (str == null) {
                        }
                        this.deviceBody.addProperty("lmt", (Number) Integer.valueOf(!z ? 1 : 0));
                        boolean z422222 = false;
                        while (r4.hasNext()) {
                        }
                        jsonObject.addProperty("is_google_play_services_available", Boolean.valueOf(z422222));
                        Intent registerReceiver22222 = this.context.registerReceiver(null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
                        intExtra = registerReceiver22222.getIntExtra("level", -1);
                        intExtra2 = registerReceiver22222.getIntExtra("scale", -1);
                        jsonObject.addProperty("battery_level", (Number) Float.valueOf(((float) intExtra) / ((float) intExtra2)));
                        intExtra3 = registerReceiver22222.getIntExtra("status", -1);
                        if (intExtra3 != -1) {
                        }
                        jsonObject.addProperty("battery_state", str2);
                        if (VERSION.SDK_INT >= 21) {
                        }
                        if (PermissionChecker.checkCallingOrSelfPermission(this.context, "android.permission.ACCESS_NETWORK_STATE") == 0) {
                        }
                        jsonObject.addProperty("locale", Locale.getDefault().toString());
                        jsonObject.addProperty("language", Locale.getDefault().getLanguage());
                        jsonObject.addProperty("time_zone", TimeZone.getDefault().getID());
                        if (this.context == null) {
                        }
                        jsonObject.addProperty("sd_card_available", (Number) Integer.valueOf(Environment.getExternalStorageState().equals("mounted") ? 1 : 0));
                        jsonObject.addProperty("os_name", Build.FINGERPRINT);
                        jsonObject.addProperty("vduid", "");
                        this.deviceBody.getAsJsonObject(RequestInfoKeys.EXT).getAsJsonObject("vungle").add(!MANUFACTURER_AMAZON.equals(Build.MANUFACTURER) ? "amazon" : "android", jsonObject);
                        return this.deviceBody;
                    } catch (Exception unused3) {
                        str = str4;
                        z = true;
                        Log.e("VungleApiClient", "Cannot load Advertising ID");
                        if (str == null) {
                        }
                        this.deviceBody.addProperty("lmt", (Number) Integer.valueOf(!z ? 1 : 0));
                        boolean z4222222 = false;
                        while (r4.hasNext()) {
                        }
                        jsonObject.addProperty("is_google_play_services_available", Boolean.valueOf(z4222222));
                        Intent registerReceiver222222 = this.context.registerReceiver(null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
                        intExtra = registerReceiver222222.getIntExtra("level", -1);
                        intExtra2 = registerReceiver222222.getIntExtra("scale", -1);
                        jsonObject.addProperty("battery_level", (Number) Float.valueOf(((float) intExtra) / ((float) intExtra2)));
                        intExtra3 = registerReceiver222222.getIntExtra("status", -1);
                        if (intExtra3 != -1) {
                        }
                        jsonObject.addProperty("battery_state", str2);
                        if (VERSION.SDK_INT >= 21) {
                        }
                        if (PermissionChecker.checkCallingOrSelfPermission(this.context, "android.permission.ACCESS_NETWORK_STATE") == 0) {
                        }
                        jsonObject.addProperty("locale", Locale.getDefault().toString());
                        jsonObject.addProperty("language", Locale.getDefault().getLanguage());
                        jsonObject.addProperty("time_zone", TimeZone.getDefault().getID());
                        if (this.context == null) {
                        }
                        jsonObject.addProperty("sd_card_available", (Number) Integer.valueOf(Environment.getExternalStorageState().equals("mounted") ? 1 : 0));
                        jsonObject.addProperty("os_name", Build.FINGERPRINT);
                        jsonObject.addProperty("vduid", "");
                        this.deviceBody.getAsJsonObject(RequestInfoKeys.EXT).getAsJsonObject("vungle").add(!MANUFACTURER_AMAZON.equals(Build.MANUFACTURER) ? "amazon" : "android", jsonObject);
                        return this.deviceBody;
                    }
                } else {
                    str4 = null;
                    z2 = true;
                }
                String str11 = str4;
                z = z2;
                str = str11;
            } catch (NoClassDefFoundError e6) {
                e = e6;
                str4 = null;
                z2 = true;
                String str92 = "VungleApiClient";
                StringBuilder sb2 = new StringBuilder();
                sb2.append("Play services Not available: ");
                sb2.append(e.getLocalizedMessage());
                Log.e(str92, sb2.toString());
                z = z2;
                str = Secure.getString(this.context.getContentResolver(), TapjoyConstants.TJC_ADVERTISING_ID);
                if (str == null) {
                }
                this.deviceBody.addProperty("lmt", (Number) Integer.valueOf(!z ? 1 : 0));
                boolean z42222222 = false;
                while (r4.hasNext()) {
                }
                jsonObject.addProperty("is_google_play_services_available", Boolean.valueOf(z42222222));
                Intent registerReceiver2222222 = this.context.registerReceiver(null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
                intExtra = registerReceiver2222222.getIntExtra("level", -1);
                intExtra2 = registerReceiver2222222.getIntExtra("scale", -1);
                jsonObject.addProperty("battery_level", (Number) Float.valueOf(((float) intExtra) / ((float) intExtra2)));
                intExtra3 = registerReceiver2222222.getIntExtra("status", -1);
                if (intExtra3 != -1) {
                }
                jsonObject.addProperty("battery_state", str2);
                if (VERSION.SDK_INT >= 21) {
                }
                if (PermissionChecker.checkCallingOrSelfPermission(this.context, "android.permission.ACCESS_NETWORK_STATE") == 0) {
                }
                jsonObject.addProperty("locale", Locale.getDefault().toString());
                jsonObject.addProperty("language", Locale.getDefault().getLanguage());
                jsonObject.addProperty("time_zone", TimeZone.getDefault().getID());
                if (this.context == null) {
                }
                jsonObject.addProperty("sd_card_available", (Number) Integer.valueOf(Environment.getExternalStorageState().equals("mounted") ? 1 : 0));
                jsonObject.addProperty("os_name", Build.FINGERPRINT);
                jsonObject.addProperty("vduid", "");
                this.deviceBody.getAsJsonObject(RequestInfoKeys.EXT).getAsJsonObject("vungle").add(!MANUFACTURER_AMAZON.equals(Build.MANUFACTURER) ? "amazon" : "android", jsonObject);
                return this.deviceBody;
            }
            if (str == null) {
            }
            this.deviceBody.addProperty("lmt", (Number) Integer.valueOf(!z ? 1 : 0));
            boolean z422222222 = false;
            while (r4.hasNext()) {
            }
            jsonObject.addProperty("is_google_play_services_available", Boolean.valueOf(z422222222));
            Intent registerReceiver22222222 = this.context.registerReceiver(null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
            intExtra = registerReceiver22222222.getIntExtra("level", -1);
            intExtra2 = registerReceiver22222222.getIntExtra("scale", -1);
            jsonObject.addProperty("battery_level", (Number) Float.valueOf(((float) intExtra) / ((float) intExtra2)));
            intExtra3 = registerReceiver22222222.getIntExtra("status", -1);
            if (intExtra3 != -1) {
            }
            jsonObject.addProperty("battery_state", str2);
            if (VERSION.SDK_INT >= 21) {
            }
            if (PermissionChecker.checkCallingOrSelfPermission(this.context, "android.permission.ACCESS_NETWORK_STATE") == 0) {
            }
            jsonObject.addProperty("locale", Locale.getDefault().toString());
            jsonObject.addProperty("language", Locale.getDefault().getLanguage());
            jsonObject.addProperty("time_zone", TimeZone.getDefault().getID());
            if (this.context == null) {
            }
            jsonObject.addProperty("sd_card_available", (Number) Integer.valueOf(Environment.getExternalStorageState().equals("mounted") ? 1 : 0));
            jsonObject.addProperty("os_name", Build.FINGERPRINT);
            jsonObject.addProperty("vduid", "");
            this.deviceBody.getAsJsonObject(RequestInfoKeys.EXT).getAsJsonObject("vungle").add(!MANUFACTURER_AMAZON.equals(Build.MANUFACTURER) ? "amazon" : "android", jsonObject);
            return this.deviceBody;
        } catch (Exception unused4) {
            str = null;
            z = true;
            Log.e("VungleApiClient", "Cannot load Advertising ID");
            if (str == null) {
            }
            this.deviceBody.addProperty("lmt", (Number) Integer.valueOf(!z ? 1 : 0));
            boolean z4222222222 = false;
            while (r4.hasNext()) {
            }
            jsonObject.addProperty("is_google_play_services_available", Boolean.valueOf(z4222222222));
            Intent registerReceiver222222222 = this.context.registerReceiver(null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
            intExtra = registerReceiver222222222.getIntExtra("level", -1);
            intExtra2 = registerReceiver222222222.getIntExtra("scale", -1);
            jsonObject.addProperty("battery_level", (Number) Float.valueOf(((float) intExtra) / ((float) intExtra2)));
            intExtra3 = registerReceiver222222222.getIntExtra("status", -1);
            if (intExtra3 != -1) {
            }
            jsonObject.addProperty("battery_state", str2);
            if (VERSION.SDK_INT >= 21) {
            }
            if (PermissionChecker.checkCallingOrSelfPermission(this.context, "android.permission.ACCESS_NETWORK_STATE") == 0) {
            }
            jsonObject.addProperty("locale", Locale.getDefault().toString());
            jsonObject.addProperty("language", Locale.getDefault().getLanguage());
            jsonObject.addProperty("time_zone", TimeZone.getDefault().getID());
            if (this.context == null) {
            }
            jsonObject.addProperty("sd_card_available", (Number) Integer.valueOf(Environment.getExternalStorageState().equals("mounted") ? 1 : 0));
            jsonObject.addProperty("os_name", Build.FINGERPRINT);
            jsonObject.addProperty("vduid", "");
            this.deviceBody.getAsJsonObject(RequestInfoKeys.EXT).getAsJsonObject("vungle").add(!MANUFACTURER_AMAZON.equals(Build.MANUFACTURER) ? "amazon" : "android", jsonObject);
            return this.deviceBody;
        }
    }

    private JsonObject getUserBody() {
        long j;
        String str;
        String str2;
        String str3;
        if (this.userBody == null) {
            this.userBody = new JsonObject();
        }
        Cookie cookie = (Cookie) this.repository.load(Cookie.CONSENT_COOKIE, Cookie.class).get();
        if (cookie != null) {
            str2 = cookie.getString("consent_status");
            str = cookie.getString("consent_source");
            j = cookie.getLong("timestamp").longValue();
            str3 = cookie.getString("consent_message_version");
        } else {
            str2 = "unknown";
            str = "no_interaction";
            j = 0;
            str3 = "";
        }
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("consent_status", str2);
        jsonObject.addProperty("consent_source", str);
        jsonObject.addProperty("consent_timestamp", (Number) Long.valueOf(j));
        String str4 = "consent_message_version";
        if (TextUtils.isEmpty(str3)) {
            str3 = "";
        }
        jsonObject.addProperty(str4, str3);
        this.userBody.add("gdpr", jsonObject);
        return this.userBody;
    }

    public boolean getMoatEnabled() {
        return this.enableMoat && VERSION.SDK_INT >= 16;
    }

    private String getUserAgentFromCookie() {
        Cookie cookie = (Cookie) this.repository.load("userAgent", Cookie.class).get();
        if (cookie == null) {
            return System.getProperty("http.agent");
        }
        String string = cookie.getString("userAgent");
        return TextUtils.isEmpty(string) ? System.getProperty("http.agent") : string;
    }

    /* access modifiers changed from: private */
    public void addUserAgentInCookie(String str) throws DBException {
        Cookie cookie = new Cookie("userAgent");
        cookie.putValue("userAgent", str);
        this.repository.save(cookie);
    }

    public long getRetryAfterHeaderValue(retrofit2.Response<JsonObject> response) {
        try {
            return Long.parseLong(response.headers().get("Retry-After")) * 1000;
        } catch (NumberFormatException unused) {
            return 0;
        }
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public void overrideApi(VungleApi vungleApi) {
        this.api = vungleApi;
    }
}
