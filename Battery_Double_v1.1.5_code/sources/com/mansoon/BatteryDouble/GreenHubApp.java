package com.mansoon.BatteryDouble;

import android.app.AlarmManager;
import android.app.Application;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.SystemClock;
import android.support.multidex.MultiDex;
import android.support.v4.app.NotificationCompat;
import com.facebook.appevents.AppEventsLogger;
import com.mansoon.BatteryDouble.managers.sampling.DataEstimator;
import com.mansoon.BatteryDouble.managers.storage.GreenHubDbMigration;
import com.mansoon.BatteryDouble.receivers.NotificationReceiver;
import com.mansoon.BatteryDouble.tasks.DeleteSessionsTask;
import com.mansoon.BatteryDouble.tasks.DeleteUsagesTask;
import com.mansoon.BatteryDouble.util.LogUtils;
import com.mansoon.BatteryDouble.util.SettingsUtils;
import io.realm.Realm;
import io.realm.RealmConfiguration.Builder;

public class GreenHubApp extends Application {
    private static final String TAG = LogUtils.makeLogTag(GreenHubApp.class);
    public static boolean isServiceRunning = false;
    public DataEstimator estimator;
    private AlarmManager mAlarmManager;
    private PendingIntent mNotificationIntent;

    /* access modifiers changed from: protected */
    public void attachBaseContext(Context context) {
        super.attachBaseContext(context);
        MultiDex.install(this);
    }

    public void onCreate() {
        super.onCreate();
        LogUtils.LOGI(TAG, "onCreate() called");
        Realm.init(this);
        Realm.setDefaultConfiguration(new Builder().schemaVersion(3).migration(new GreenHubDbMigration()).build());
        LogUtils.LOGI(TAG, "Estimator new instance");
        this.estimator = new DataEstimator();
        Context applicationContext = getApplicationContext();
        AppEventsLogger.activateApp((Application) this);
        if (SettingsUtils.isTosAccepted(applicationContext)) {
            LogUtils.LOGI(TAG, "startGreenHubService() called");
            startGreenHubService();
            int fetchDataHistoryInterval = SettingsUtils.fetchDataHistoryInterval(applicationContext);
            new DeleteUsagesTask().execute(new Integer[]{Integer.valueOf(fetchDataHistoryInterval)});
            new DeleteSessionsTask().execute(new Integer[]{Integer.valueOf(fetchDataHistoryInterval)});
            if (SettingsUtils.isPowerIndicatorShown(applicationContext)) {
                startStatusBarUpdater();
            }
        }
    }

    public void startGreenHubService() {
        if (!isServiceRunning) {
            LogUtils.LOGI(TAG, "GreenHubService starting...");
            final Context applicationContext = getApplicationContext();
            isServiceRunning = true;
            new Thread() {
                private IntentFilter intentFilter;

                public void run() {
                    this.intentFilter = new IntentFilter();
                    this.intentFilter.addAction("android.intent.action.BATTERY_CHANGED");
                    GreenHubApp.this.registerReceiver(GreenHubApp.this.estimator, this.intentFilter);
                    if (SettingsUtils.isSamplingScreenOn(applicationContext)) {
                        this.intentFilter.addAction("android.intent.action.SCREEN_ON");
                        GreenHubApp.this.registerReceiver(GreenHubApp.this.estimator, this.intentFilter);
                        this.intentFilter.addAction("android.intent.action.SCREEN_OFF");
                        GreenHubApp.this.registerReceiver(GreenHubApp.this.estimator, this.intentFilter);
                    }
                }
            }.start();
            return;
        }
        LogUtils.LOGI(TAG, "GreenHubService is already running...");
    }

    public void stopGreenHubService() {
        try {
            if (this.estimator != null) {
                unregisterReceiver(this.estimator);
                isServiceRunning = false;
            }
        } catch (IllegalArgumentException e) {
            LogUtils.LOGE(TAG, "Estimator receiver is not registered!");
            e.printStackTrace();
        }
    }

    public void startStatusBarUpdater() {
        this.mNotificationIntent = PendingIntent.getBroadcast(this, 0, new Intent(this, NotificationReceiver.class), 134217728);
        if (this.mAlarmManager == null) {
            this.mAlarmManager = (AlarmManager) getSystemService(NotificationCompat.CATEGORY_ALARM);
        }
        this.mAlarmManager.setInexactRepeating(3, SystemClock.elapsedRealtime() + 30000, 30000, this.mNotificationIntent);
    }

    public void stopStatusBarUpdater() {
        if (this.mAlarmManager != null) {
            this.mAlarmManager.cancel(this.mNotificationIntent);
        }
    }
}
