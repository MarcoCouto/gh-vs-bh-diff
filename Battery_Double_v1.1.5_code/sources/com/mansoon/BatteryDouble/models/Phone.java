package com.mansoon.BatteryDouble.models;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build.VERSION;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import com.facebook.places.model.PlaceFields;
import com.mansoon.BatteryDouble.util.PermissionsUtils;
import java.lang.reflect.Method;
import java.util.List;

public class Phone {
    public static String CALL_STATE_IDLE = "idle";
    public static String CALL_STATE_OFFHOOK = "offhook";
    public static String CALL_STATE_RINGING = "ringing";
    public static String PHONE_TYPE_CDMA = "cdma";
    public static String PHONE_TYPE_GSM = "gsm";
    public static String PHONE_TYPE_NONE = "none";
    public static String PHONE_TYPE_SIP = "sip";
    private static final String TAG = "Phone";

    public static String getCallState(Context context) {
        switch (((TelephonyManager) context.getSystemService(PlaceFields.PHONE)).getCallState()) {
            case 1:
                return CALL_STATE_RINGING;
            case 2:
                return CALL_STATE_OFFHOOK;
            default:
                return CALL_STATE_IDLE;
        }
    }

    @SuppressLint({"HardwareIds"})
    public static String getDeviceId(Context context) {
        String str = null;
        if (!PermissionsUtils.checkPermission(context, "android.permission.READ_PHONE_STATE")) {
            return null;
        }
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(PlaceFields.PHONE);
        if (telephonyManager != null) {
            str = telephonyManager.getDeviceId();
        }
        return str;
    }

    public static String getType(Context context) {
        switch (((TelephonyManager) context.getSystemService(PlaceFields.PHONE)).getPhoneType()) {
            case 1:
                return PHONE_TYPE_GSM;
            case 2:
                return PHONE_TYPE_CDMA;
            default:
                return PHONE_TYPE_NONE;
        }
    }

    public static String getMcc(Context context) {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(PlaceFields.PHONE);
        String networkOperator = telephonyManager.getNetworkOperator();
        if (networkOperator != null && networkOperator.length() >= 5) {
            return networkOperator.substring(0, 3);
        }
        String str = "gsm.operator.numeric";
        if (telephonyManager.getPhoneType() != 2) {
            str = "ro.cdma.home.operator.numeric";
        }
        String stringFromSystemProperty = Specifications.getStringFromSystemProperty(context, str);
        return (stringFromSystemProperty == null || stringFromSystemProperty.length() < 5) ? "Unknown" : stringFromSystemProperty.substring(0, 3);
    }

    public static String getMnc(Context context) {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(PlaceFields.PHONE);
        String networkOperator = telephonyManager.getNetworkOperator();
        if (networkOperator != null && networkOperator.length() >= 5) {
            return networkOperator.substring(3);
        }
        String str = "gsm.operator.numeric";
        if (telephonyManager.getPhoneType() != 2) {
            str = "ro.cdma.home.operator.numeric";
        }
        String stringFromSystemProperty = Specifications.getStringFromSystemProperty(context, str);
        return (stringFromSystemProperty == null || stringFromSystemProperty.length() < 5) ? "Unknown" : stringFromSystemProperty.substring(3);
    }

    public static String getNetworkOperator(Context context) {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(PlaceFields.PHONE);
        String networkOperators = getNetworkOperators(context);
        if (networkOperators != null && networkOperators.length() != 0) {
            return networkOperators;
        }
        String networkOperatorName = telephonyManager.getNetworkOperatorName();
        if (networkOperatorName != null && networkOperatorName.length() != 0) {
            return networkOperatorName;
        }
        String stringFromSystemProperty = Specifications.getStringFromSystemProperty(context, "ro.cdma.home.operator.alpha");
        return (stringFromSystemProperty == null || stringFromSystemProperty.length() == 0) ? "unknown" : stringFromSystemProperty;
    }

    private static String getNetworkOperators(Context context) {
        String str = "";
        if (!PermissionsUtils.checkPermission(context, "android.permission.READ_PHONE_STATE")) {
            return str;
        }
        if (VERSION.SDK_INT > 22) {
            SubscriptionManager from = SubscriptionManager.from(context);
            if (from != null) {
                List<SubscriptionInfo> activeSubscriptionInfoList = from.getActiveSubscriptionInfoList();
                if (activeSubscriptionInfoList != null) {
                    for (SubscriptionInfo carrierName : activeSubscriptionInfoList) {
                        CharSequence carrierName2 = carrierName.getCarrierName();
                        if (carrierName2 != null && carrierName2.length() > 0) {
                            StringBuilder sb = new StringBuilder();
                            sb.append(str);
                            sb.append(carrierName2);
                            sb.append(";");
                            str = sb.toString();
                        }
                    }
                    if (str.length() >= 1) {
                        str = str.substring(0, str.length() - 1);
                    }
                }
            }
        }
        return str;
    }

    static String getCountryCodeForMcc(Context context, int i) throws Exception {
        Method method = Class.forName("com.android.internal.telephony.MccTable").getMethod("countryCodeForMcc", new Class[]{Integer.TYPE});
        method.setAccessible(true);
        return (String) method.invoke(context, new Object[]{Integer.valueOf(i)});
    }
}
