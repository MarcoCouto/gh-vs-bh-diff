package com.mansoon.BatteryDouble.models;

import android.os.SystemClock;
import com.github.mikephil.charting.utils.Utils;
import com.mansoon.BatteryDouble.util.LogUtils;
import com.mintegral.msdk.base.entity.CampaignEx;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.concurrent.TimeUnit;

public class Cpu {
    private static final String TAG = LogUtils.makeLogTag(Cpu.class);

    public static synchronized long[] readUsagePoint() {
        long j;
        long j2;
        synchronized (Cpu.class) {
            try {
                RandomAccessFile randomAccessFile = new RandomAccessFile("/proc/stat", CampaignEx.JSON_KEY_AD_R);
                String[] split = randomAccessFile.readLine().split(" ");
                j2 = 0;
                j = 0;
                for (int i = 2; i <= 8; i++) {
                    if (i == 5) {
                        try {
                            j = Long.parseLong(split[i]);
                        } catch (IOException e) {
                            e = e;
                            e.printStackTrace();
                            long[] jArr = {j, j2};
                            return jArr;
                        }
                    } else {
                        j2 += Long.parseLong(split[i]);
                    }
                }
                randomAccessFile.close();
            } catch (IOException e2) {
                e = e2;
                j2 = 0;
                j = 0;
                e.printStackTrace();
                long[] jArr2 = {j, j2};
                return jArr2;
            }
            long[] jArr22 = {j, j2};
        }
        return jArr22;
    }

    public static double getUsage(long[] jArr, long[] jArr2) {
        if (jArr == null || jArr2 == null || jArr.length < 2 || jArr2.length < 2) {
            return Utils.DOUBLE_EPSILON;
        }
        double d = (double) ((jArr2[0] + jArr2[1]) - (jArr[0] + jArr[1]));
        double d2 = (double) (jArr2[1] - jArr[1]);
        Double.isNaN(d2);
        Double.isNaN(d);
        return d2 / d;
    }

    public static long getUptime() {
        return TimeUnit.MILLISECONDS.toSeconds(SystemClock.elapsedRealtime());
    }

    public static long getSleepTime() {
        long elapsedRealtime = SystemClock.elapsedRealtime() - SystemClock.uptimeMillis();
        if (elapsedRealtime < 0) {
            return 0;
        }
        return TimeUnit.MILLISECONDS.toSeconds(elapsedRealtime);
    }
}
