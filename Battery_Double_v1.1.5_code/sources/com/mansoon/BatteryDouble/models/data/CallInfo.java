package com.mansoon.BatteryDouble.models.data;

import io.realm.CallInfoRealmProxyInterface;
import io.realm.RealmObject;
import io.realm.internal.RealmObjectProxy;

public class CallInfo extends RealmObject implements CallInfoRealmProxyInterface {
    public String callStatus;
    public double incomingCallTime;
    public double nonCallTime;
    public double outgoingCallTime;

    public String realmGet$callStatus() {
        return this.callStatus;
    }

    public double realmGet$incomingCallTime() {
        return this.incomingCallTime;
    }

    public double realmGet$nonCallTime() {
        return this.nonCallTime;
    }

    public double realmGet$outgoingCallTime() {
        return this.outgoingCallTime;
    }

    public void realmSet$callStatus(String str) {
        this.callStatus = str;
    }

    public void realmSet$incomingCallTime(double d) {
        this.incomingCallTime = d;
    }

    public void realmSet$nonCallTime(double d) {
        this.nonCallTime = d;
    }

    public void realmSet$outgoingCallTime(double d) {
        this.outgoingCallTime = d;
    }

    public CallInfo() {
        if (this instanceof RealmObjectProxy) {
            ((RealmObjectProxy) this).realm$injectObjectContext();
        }
    }
}
