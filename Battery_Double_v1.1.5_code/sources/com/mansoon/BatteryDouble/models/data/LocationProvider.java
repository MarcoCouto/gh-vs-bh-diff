package com.mansoon.BatteryDouble.models.data;

import io.realm.LocationProviderRealmProxyInterface;
import io.realm.RealmObject;
import io.realm.internal.RealmObjectProxy;

public class LocationProvider extends RealmObject implements LocationProviderRealmProxyInterface {
    public String provider;

    public String realmGet$provider() {
        return this.provider;
    }

    public void realmSet$provider(String str) {
        this.provider = str;
    }

    public LocationProvider() {
        if (this instanceof RealmObjectProxy) {
            ((RealmObjectProxy) this).realm$injectObjectContext();
        }
    }

    public LocationProvider(String str) {
        if (this instanceof RealmObjectProxy) {
            ((RealmObjectProxy) this).realm$injectObjectContext();
        }
        realmSet$provider(str);
    }
}
