package com.mansoon.BatteryDouble.models.data;

import io.realm.BatteryDetailsRealmProxyInterface;
import io.realm.RealmObject;
import io.realm.internal.RealmObjectProxy;

public class BatteryDetails extends RealmObject implements BatteryDetailsRealmProxyInterface {
    public int capacity;
    public int chargeCounter;
    public String charger;
    public int currentAverage;
    public int currentNow;
    public long energyCounter;
    public String health;
    public String technology;
    public double temperature;
    public double voltage;

    public int realmGet$capacity() {
        return this.capacity;
    }

    public int realmGet$chargeCounter() {
        return this.chargeCounter;
    }

    public String realmGet$charger() {
        return this.charger;
    }

    public int realmGet$currentAverage() {
        return this.currentAverage;
    }

    public int realmGet$currentNow() {
        return this.currentNow;
    }

    public long realmGet$energyCounter() {
        return this.energyCounter;
    }

    public String realmGet$health() {
        return this.health;
    }

    public String realmGet$technology() {
        return this.technology;
    }

    public double realmGet$temperature() {
        return this.temperature;
    }

    public double realmGet$voltage() {
        return this.voltage;
    }

    public void realmSet$capacity(int i) {
        this.capacity = i;
    }

    public void realmSet$chargeCounter(int i) {
        this.chargeCounter = i;
    }

    public void realmSet$charger(String str) {
        this.charger = str;
    }

    public void realmSet$currentAverage(int i) {
        this.currentAverage = i;
    }

    public void realmSet$currentNow(int i) {
        this.currentNow = i;
    }

    public void realmSet$energyCounter(long j) {
        this.energyCounter = j;
    }

    public void realmSet$health(String str) {
        this.health = str;
    }

    public void realmSet$technology(String str) {
        this.technology = str;
    }

    public void realmSet$temperature(double d) {
        this.temperature = d;
    }

    public void realmSet$voltage(double d) {
        this.voltage = d;
    }

    public BatteryDetails() {
        if (this instanceof RealmObjectProxy) {
            ((RealmObjectProxy) this).realm$injectObjectContext();
        }
    }
}
