package com.mansoon.BatteryDouble.models;

import android.content.Context;
import android.os.Build.VERSION;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import com.facebook.places.model.PlaceFields;
import com.mansoon.BatteryDouble.util.LogUtils;
import com.mansoon.BatteryDouble.util.PermissionsUtils;
import java.lang.reflect.Method;
import java.util.List;

public class SimCard {
    private static final String TAG = "SimCard";

    public static String getSIMOperator(Context context) {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(PlaceFields.PHONE);
        String sIMOperators = getSIMOperators(context);
        if (sIMOperators != null && sIMOperators.length() > 0) {
            return sIMOperators;
        }
        String simOperatorName = telephonyManager.getSimOperatorName();
        return (simOperatorName == null || simOperatorName.length() <= 0) ? "unknown" : simOperatorName;
    }

    private static String getSIMOperators(Context context) {
        String str = "";
        if (!PermissionsUtils.checkPermission(context, "android.permission.READ_PHONE_STATE")) {
            return str;
        }
        if (VERSION.SDK_INT > 22) {
            List<SubscriptionInfo> activeSubscriptionInfoList = SubscriptionManager.from(context).getActiveSubscriptionInfoList();
            if (activeSubscriptionInfoList != null && activeSubscriptionInfoList.size() > 0) {
                for (SubscriptionInfo subscriptionId : activeSubscriptionInfoList) {
                    String simOperatorNameForSubscription = getSimOperatorNameForSubscription(context, subscriptionId.getSubscriptionId());
                    if (simOperatorNameForSubscription != null && simOperatorNameForSubscription.length() > 0) {
                        StringBuilder sb = new StringBuilder();
                        sb.append(str);
                        sb.append(simOperatorNameForSubscription);
                        sb.append(";");
                        str = sb.toString();
                    }
                }
                if (str.length() > 1) {
                    str = str.substring(0, str.length() - 1);
                }
            }
        }
        return str;
    }

    private static String getSimOperatorNameForSubscription(Context context, int i) {
        try {
            Method method = Class.forName(((TelephonyManager) context.getSystemService(PlaceFields.PHONE)).getClass().getName()).getMethod("getSimOperatorNameForSubscription", new Class[]{Integer.TYPE});
            method.setAccessible(true);
            return (String) method.invoke(context, new Object[]{Integer.valueOf(i)});
        } catch (Exception e) {
            if (e.getLocalizedMessage() != null) {
                String str = TAG;
                StringBuilder sb = new StringBuilder();
                sb.append("Failed getting sim operator with subid: ");
                sb.append(e.getLocalizedMessage());
                LogUtils.LOGD(str, sb.toString());
            }
            return null;
        }
    }
}
