package com.mansoon.BatteryDouble.network.handlers;

import android.content.Context;
import com.google.gson.Gson;
import com.mansoon.BatteryDouble.R;
import com.mansoon.BatteryDouble.events.StatusEvent;
import com.mansoon.BatteryDouble.models.Specifications;
import com.mansoon.BatteryDouble.models.data.Device;
import com.mansoon.BatteryDouble.network.services.GreenHubAPIService;
import com.mansoon.BatteryDouble.tasks.CheckNewMessagesTask;
import com.mansoon.BatteryDouble.util.GsonRealmBuilder;
import com.mansoon.BatteryDouble.util.LogUtils;
import com.mansoon.BatteryDouble.util.SettingsUtils;
import org.greenrobot.eventbus.EventBus;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit.Builder;
import retrofit2.converter.gson.GsonConverterFactory;

public class RegisterDeviceHandler {
    /* access modifiers changed from: private */
    public static final String TAG = LogUtils.makeLogTag(RegisterDeviceHandler.class);
    /* access modifiers changed from: private */
    public Context mContext;
    private GreenHubAPIService mService;

    public RegisterDeviceHandler(Context context) {
        this.mContext = context;
        Gson gson = GsonRealmBuilder.get();
        this.mService = (GreenHubAPIService) new Builder().baseUrl(SettingsUtils.fetchServerUrl(context)).addConverterFactory(GsonConverterFactory.create(gson)).build().create(GreenHubAPIService.class);
    }

    public Device registerClient() {
        Device device = new Device();
        device.realmSet$uuId(Specifications.getAndroidId(this.mContext));
        device.realmSet$model(Specifications.getModel());
        device.realmSet$manufacturer(Specifications.getManufacturer());
        device.realmSet$brand(Specifications.getBrand());
        device.realmSet$product(Specifications.getProductName());
        device.realmSet$osVersion(Specifications.getOsVersion());
        device.realmSet$kernelVersion(Specifications.getKernelVersion());
        device.realmSet$isRoot(Specifications.isRooted() ? 1 : 0);
        callRegistration(device);
        return device;
    }

    private void callRegistration(Device device) {
        LogUtils.LOGI(TAG, "callRegistration()");
        this.mService.createDevice(device).enqueue(new Callback<Integer>() {
            public void onResponse(Call<Integer> call, Response<Integer> response) {
                if (response == null || response.body() == null) {
                    if (response == null) {
                        LogUtils.LOGI(RegisterDeviceHandler.TAG, "response is null");
                    } else {
                        LogUtils.LOGI(RegisterDeviceHandler.TAG, "response body is null");
                    }
                    SettingsUtils.markDeviceAccepted(RegisterDeviceHandler.this.mContext, false);
                    EventBus.getDefault().post(new StatusEvent(RegisterDeviceHandler.this.mContext.getString(R.string.event_registration_failed)));
                    return;
                }
                if (((Integer) response.body()).intValue() > 0) {
                    EventBus.getDefault().post(new StatusEvent(RegisterDeviceHandler.this.mContext.getString(R.string.event_device_registered)));
                } else if (((Integer) response.body()).intValue() == 0) {
                    EventBus.getDefault().post(new StatusEvent(RegisterDeviceHandler.this.mContext.getString(R.string.event_already_registered)));
                }
                SettingsUtils.markDeviceAccepted(RegisterDeviceHandler.this.mContext, true);
                new CheckNewMessagesTask().execute(new Context[]{RegisterDeviceHandler.this.mContext});
            }

            public void onFailure(Call<Integer> call, Throwable th) {
                SettingsUtils.markDeviceAccepted(RegisterDeviceHandler.this.mContext, false);
                EventBus.getDefault().post(new StatusEvent(RegisterDeviceHandler.this.mContext.getString(R.string.event_registration_failed)));
                LogUtils.LOGI(RegisterDeviceHandler.TAG, th.getMessage());
            }
        });
    }
}
