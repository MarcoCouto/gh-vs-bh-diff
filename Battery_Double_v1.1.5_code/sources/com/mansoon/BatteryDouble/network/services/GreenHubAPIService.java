package com.mansoon.BatteryDouble.network.services;

import com.google.gson.JsonObject;
import com.mansoon.BatteryDouble.models.data.Device;
import com.mansoon.BatteryDouble.models.data.Upload;
import java.util.List;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface GreenHubAPIService {
    @POST("api/mobile/register")
    Call<Integer> createDevice(@Body Device device);

    @POST("api/mobile/upload")
    Call<Integer> createSample(@Body Upload upload);

    @GET("api/mobile/messages")
    Call<List<JsonObject>> getMessages(@Query("uuid") String str, @Query("message") int i);
}
