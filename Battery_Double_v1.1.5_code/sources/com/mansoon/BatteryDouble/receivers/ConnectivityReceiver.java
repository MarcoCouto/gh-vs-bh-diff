package com.mansoon.BatteryDouble.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.mansoon.BatteryDouble.events.RefreshEvent;
import com.mansoon.BatteryDouble.network.CommunicationManager;
import com.mansoon.BatteryDouble.tasks.CheckNewMessagesTask;
import com.mansoon.BatteryDouble.tasks.ServerStatusTask;
import com.mansoon.BatteryDouble.util.LogUtils;
import com.mansoon.BatteryDouble.util.SettingsUtils;
import com.tapjoy.TapjoyConstants;
import org.greenrobot.eventbus.EventBus;

public class ConnectivityReceiver extends BroadcastReceiver {
    private static final String TAG = LogUtils.makeLogTag("ConnectivityReceiver");

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0030  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x005a  */
    public void onReceive(Context context, Intent intent) {
        char c;
        String action = intent.getAction();
        int hashCode = action.hashCode();
        if (hashCode != -1530327060) {
            if (hashCode == -1172645946 && action.equals("android.net.conn.CONNECTIVITY_CHANGE")) {
                c = 0;
                switch (c) {
                    case 0:
                        NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
                        if (activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting()) {
                            CommunicationManager.uploadAttempts = 0;
                            new ServerStatusTask().execute(new Context[]{context});
                            if (SettingsUtils.isDeviceRegistered(context)) {
                                new CheckNewMessagesTask().execute(new Context[]{context});
                            }
                            if (CommunicationManager.isQueued && SettingsUtils.isServerUrlPresent(context)) {
                                new CommunicationManager(context, true).sendSamples();
                                CommunicationManager.isQueued = false;
                            }
                            if (activeNetworkInfo.getType() != 1) {
                                if (activeNetworkInfo.getType() == 0) {
                                    EventBus.getDefault().post(new RefreshEvent(TapjoyConstants.TJC_CONNECTION_TYPE_MOBILE, true));
                                    break;
                                }
                            } else {
                                EventBus.getDefault().post(new RefreshEvent("wifi", true));
                                break;
                            }
                        } else {
                            EventBus.getDefault().post(new RefreshEvent("wifi", false));
                            EventBus.getDefault().post(new RefreshEvent(TapjoyConstants.TJC_CONNECTION_TYPE_MOBILE, false));
                            return;
                        }
                        break;
                    case 1:
                        if (intent.getIntExtra("android.bluetooth.adapter.extra.STATE", -1) != 12) {
                            EventBus.getDefault().post(new RefreshEvent("bluetooth", false));
                            break;
                        } else {
                            EventBus.getDefault().post(new RefreshEvent("bluetooth", true));
                            break;
                        }
                }
            }
        } else if (action.equals("android.bluetooth.adapter.action.STATE_CHANGED")) {
            c = 1;
            switch (c) {
                case 0:
                    break;
                case 1:
                    break;
            }
        }
        c = 65535;
        switch (c) {
            case 0:
                break;
            case 1:
                break;
        }
    }
}
