package com.mansoon.BatteryDouble.ui;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout.OnTabSelectedListener;
import android.support.design.widget.TabLayout.Tab;
import android.support.design.widget.TabLayout.TabLayoutOnPageChangeListener;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityCompat.OnRequestPermissionsResultCallback;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.Toolbar.OnMenuItemClickListener;
import android.view.DragEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnDragListener;
import com.appodeal.ads.Appodeal;
import com.google.android.exoplayer2.extractor.ts.TsExtractor;
import com.mansoon.BatteryDouble.GreenHubApp;
import com.mansoon.BatteryDouble.R;
import com.mansoon.BatteryDouble.events.RefreshChartEvent;
import com.mansoon.BatteryDouble.events.StatusEvent;
import com.mansoon.BatteryDouble.managers.sampling.DataEstimator;
import com.mansoon.BatteryDouble.managers.storage.GreenHubDb;
import com.mansoon.BatteryDouble.tasks.CheckNewMessagesTask;
import com.mansoon.BatteryDouble.tasks.ServerStatusTask;
import com.mansoon.BatteryDouble.ui.adapters.TabAdapter;
import com.mansoon.BatteryDouble.ui.layouts.MainTabLayout;
import com.mansoon.BatteryDouble.util.LogUtils;
import com.mansoon.BatteryDouble.util.NetworkWatcher;
import com.mansoon.BatteryDouble.util.SettingsUtils;
import org.greenrobot.eventbus.EventBus;

public class MainActivity extends BaseActivity implements OnMenuItemClickListener, OnRequestPermissionsResultCallback {
    private static final String TAG = LogUtils.makeLogTag(MainActivity.class);
    public GreenHubDb database;
    private GreenHubApp mApp;
    /* access modifiers changed from: private */
    public LockableViewPager mViewPager;

    private void requestNewInterstitial() {
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_main);
        initAdLib();
        requestNewInterstitial();
        LogUtils.LOGI(TAG, "onCreate() called");
        loadComponents();
        Intent intent = getIntent();
        if (intent != null) {
            int intExtra = intent.getIntExtra("tab", -1);
            if (intExtra != -1) {
                this.mViewPager.setCurrentItem(intExtra);
            }
        }
        this.mViewPager.setOnDragListener(new OnDragListener() {
            public boolean onDrag(View view, DragEvent dragEvent) {
                return false;
            }
        });
        this.mViewPager.setSwipeable(false);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        Appodeal.onResume(this, 8);
        Appodeal.show(this, 8);
    }

    private void initAdLib() {
        Appodeal.initialize(this, getResources().getString(R.string.appodeal_app_id), TsExtractor.TS_STREAM_TYPE_E_AC3, true);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        this.database.getDefaultInstance();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        this.database.close();
        super.onStop();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        Toolbar actionBarToolbar = getActionBarToolbar();
        actionBarToolbar.inflateMenu(R.menu.menu_main);
        actionBarToolbar.setOnMenuItemClickListener(this);
        return true;
    }

    public boolean onMenuItemClick(MenuItem menuItem) {
        int itemId = menuItem.getItemId();
        if (itemId != R.id.action_summary) {
            switch (itemId) {
                case R.id.action_rating /*2131296282*/:
                    try {
                        startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=com.mansoon.BatteryDouble")));
                    } catch (ActivityNotFoundException unused) {
                        startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://play.google.com/store/apps/details?id=com.mansoon.BatteryDouble")));
                    }
                    return true;
                case R.id.action_settings /*2131296283*/:
                    startActivity(new Intent(this, SettingsActivity.class));
                    return true;
                default:
                    return false;
            }
        } else {
            try {
                Intent intent = new Intent("android.intent.action.POWER_USAGE_SUMMARY");
                if (getPackageManager().resolveActivity(intent, 0) != null) {
                    startActivity(intent);
                }
            } catch (ActivityNotFoundException e) {
                e.printStackTrace();
            }
            return true;
        }
    }

    public void onRequestPermissionsResult(int i, @NonNull String[] strArr, @NonNull int[] iArr) {
        switch (i) {
            case 1:
                setupPermission("android.permission.ACCESS_COARSE_LOCATION", 2);
                return;
            case 2:
                setupPermission("android.permission.ACCESS_FINE_LOCATION", 3);
                break;
        }
    }

    public DataEstimator getEstimator() {
        return this.mApp.estimator;
    }

    private void loadComponents() {
        Context applicationContext = getApplicationContext();
        this.database = new GreenHubDb();
        this.mApp = (GreenHubApp) getApplication();
        if (SettingsUtils.isTosAccepted(applicationContext)) {
            this.mApp.startGreenHubService();
            if (NetworkWatcher.hasInternet(applicationContext, 1)) {
                new ServerStatusTask().execute(new Context[]{applicationContext});
                if (SettingsUtils.isDeviceRegistered(applicationContext)) {
                    new CheckNewMessagesTask().execute(new Context[]{applicationContext});
                }
            }
        }
        loadViews();
    }

    private void loadViews() {
        this.mViewPager = (LockableViewPager) findViewById(R.id.viewpager);
        this.mViewPager.setOffscreenPageLimit(2);
        this.mViewPager.setAdapter(new TabAdapter(getFragmentManager()));
        MainTabLayout mainTabLayout = (MainTabLayout) findViewById(R.id.tab_layout);
        mainTabLayout.createTabs();
        mainTabLayout.addOnTabSelectedListener(new OnTabSelectedListener() {
            public void onTabReselected(Tab tab) {
            }

            public void onTabUnselected(Tab tab) {
            }

            public void onTabSelected(Tab tab) {
                MainActivity.this.mViewPager.setCurrentItem(tab.getPosition());
                MainActivity.this.getActionBarToolbar().setTitle(tab.getContentDescription());
                tab.getPosition();
                if (tab.getPosition() == 2) {
                    EventBus.getDefault().post(new RefreshChartEvent());
                }
                if (Appodeal.isLoaded(3)) {
                    Appodeal.show(MainActivity.this, 3);
                }
            }
        });
        this.mViewPager.addOnPageChangeListener(new TabLayoutOnPageChangeListener(mainTabLayout));
        if (VERSION.SDK_INT >= 23) {
            setupPermission("android.permission.READ_PHONE_STATE", 1);
        }
    }

    private void refreshStatus() {
        new Handler().postDelayed(new Runnable() {
            public void run() {
                EventBus.getDefault().post(new StatusEvent(MainActivity.this.getString(R.string.event_idle)));
            }
        }, 10000);
    }

    private void setupPermission(String str, int i) {
        if (ContextCompat.checkSelfPermission(this, str) != 0) {
            ActivityCompat.requestPermissions(this, new String[]{str}, i);
        }
    }
}
