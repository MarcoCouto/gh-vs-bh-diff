package com.mansoon.BatteryDouble.ui.views;

import android.content.Context;
import android.widget.TextView;
import com.github.mikephil.charting.components.MarkerView;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.utils.MPPointF;
import com.mansoon.BatteryDouble.R;
import com.mansoon.BatteryDouble.util.StringHelper;

public class ChartMarkerView extends MarkerView {
    private static final String TAG = "ChartMarkerView";
    private TextView mContent = ((TextView) findViewById(R.id.tvContent));
    private MPPointF mOffset;
    private int mType;

    public ChartMarkerView(Context context, int i) {
        super(context, i);
    }

    public ChartMarkerView(Context context, int i, int i2) {
        super(context, i);
        this.mType = i2;
    }

    public void refreshContent(Entry entry, Highlight highlight) {
        String str = "";
        switch (this.mType) {
            case 1:
                str = StringHelper.formatPercentageNumber(entry.getY());
                break;
            case 2:
                StringBuilder sb = new StringBuilder();
                sb.append(StringHelper.formatNumber(entry.getY()));
                sb.append(" ºC");
                str = sb.toString();
                break;
            case 3:
                StringBuilder sb2 = new StringBuilder();
                sb2.append(StringHelper.formatNumber(entry.getY()));
                sb2.append(" V");
                str = sb2.toString();
                break;
        }
        this.mContent.setText(str);
        super.refreshContent(entry, highlight);
    }

    public MPPointF getOffset() {
        if (this.mOffset == null) {
            this.mOffset = new MPPointF((float) (-(getWidth() / 2)), (float) (-getHeight()));
        }
        return this.mOffset;
    }
}
