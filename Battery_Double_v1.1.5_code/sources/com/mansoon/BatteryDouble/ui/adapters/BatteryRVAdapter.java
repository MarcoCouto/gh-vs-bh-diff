package com.mansoon.BatteryDouble.ui.adapters;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.mansoon.BatteryDouble.R;
import com.mansoon.BatteryDouble.models.ui.BatteryCard;
import java.util.ArrayList;

public class BatteryRVAdapter extends Adapter<DashboardViewHolder> {
    private ArrayList<BatteryCard> mBatteryCards;

    static class DashboardViewHolder extends ViewHolder {
        CardView cv;
        public ImageView icon;
        public View indicator;
        public TextView label;
        public TextView value;

        DashboardViewHolder(View view) {
            super(view);
            this.cv = (CardView) view.findViewById(R.id.cv);
            this.icon = (ImageView) view.findViewById(R.id.icon);
            this.label = (TextView) view.findViewById(R.id.label);
            this.value = (TextView) view.findViewById(R.id.value);
            this.indicator = view.findViewById(R.id.indicator);
        }
    }

    public BatteryRVAdapter(ArrayList<BatteryCard> arrayList) {
        this.mBatteryCards = arrayList;
    }

    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public DashboardViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        return new DashboardViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.battery_item_card_view, viewGroup, false));
    }

    public void onBindViewHolder(DashboardViewHolder dashboardViewHolder, int i) {
        dashboardViewHolder.icon.setImageResource(((BatteryCard) this.mBatteryCards.get(i)).icon);
        dashboardViewHolder.label.setText(((BatteryCard) this.mBatteryCards.get(i)).label);
        dashboardViewHolder.value.setText(((BatteryCard) this.mBatteryCards.get(i)).value);
        dashboardViewHolder.indicator.setBackgroundColor(((BatteryCard) this.mBatteryCards.get(i)).indicator);
    }

    public int getItemCount() {
        return this.mBatteryCards.size();
    }

    public void swap(ArrayList<BatteryCard> arrayList) {
        if (this.mBatteryCards != null) {
            this.mBatteryCards.clear();
            this.mBatteryCards.addAll(arrayList);
        } else {
            this.mBatteryCards = arrayList;
        }
        notifyDataSetChanged();
    }
}
