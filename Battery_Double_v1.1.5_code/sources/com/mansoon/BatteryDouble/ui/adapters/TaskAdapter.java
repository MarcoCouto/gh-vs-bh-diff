package com.mansoon.BatteryDouble.ui.adapters;

import android.content.Context;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView.Adapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.github.mikephil.charting.utils.Utils;
import com.mansoon.BatteryDouble.R;
import com.mansoon.BatteryDouble.events.OpenTaskDetailsEvent;
import com.mansoon.BatteryDouble.events.TaskRemovedEvent;
import com.mansoon.BatteryDouble.managers.TaskController;
import com.mansoon.BatteryDouble.models.ui.Task;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.greenrobot.eventbus.EventBus;

public class TaskAdapter extends Adapter<ViewHolder> {
    public static final String TAG = "TaskAdapter";
    /* access modifiers changed from: private */
    public Handler handler;
    /* access modifiers changed from: private */
    public List<Task> items;
    /* access modifiers changed from: private */
    public List<Task> itemsPendingRemoval;
    /* access modifiers changed from: private */
    public HashMap<Task, Runnable> pendingRunnables = new HashMap<>();
    private TaskController taskController;
    private boolean undoOn;

    static class ViewHolder extends android.support.v7.widget.RecyclerView.ViewHolder {
        TextView appPackage;
        TextView appVersion;
        TextView autoStart;
        TextView backgroundService;
        CheckBox checkBox;
        RelativeLayout details;
        ImageView icon;
        TextView memory;
        ImageView more;
        TextView name;
        Button undoButton;

        ViewHolder(View view) {
            super(view);
            this.name = (TextView) view.findViewById(R.id.taskName);
            this.memory = (TextView) view.findViewById(R.id.taskMemory);
            this.details = (RelativeLayout) view.findViewById(R.id.taskDetailsContainer);
            this.autoStart = (TextView) view.findViewById(R.id.taskAutoStart);
            this.backgroundService = (TextView) view.findViewById(R.id.taskBackgroundService);
            this.appPackage = (TextView) view.findViewById(R.id.taskPackage);
            this.appVersion = (TextView) view.findViewById(R.id.taskAppVersion);
            this.icon = (ImageView) view.findViewById(R.id.taskIcon);
            this.more = (ImageView) view.findViewById(R.id.taskShowDetails);
            this.undoButton = (Button) view.findViewById(R.id.undo_button);
            this.checkBox = (CheckBox) view.findViewById(R.id.checkBox);
        }
    }

    public TaskAdapter(@NonNull Context context, List<Task> list) {
        this.items = list;
        this.taskController = new TaskController(context);
        this.itemsPendingRemoval = new ArrayList();
        this.undoOn = true;
        this.handler = new Handler();
    }

    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        return new ViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_task, viewGroup, false));
    }

    public void onBindViewHolder(final ViewHolder viewHolder, int i) {
        String str;
        String str2;
        final Task task = (Task) this.items.get(i);
        if (this.itemsPendingRemoval.contains(task)) {
            viewHolder.itemView.setBackgroundColor(-12303292);
            viewHolder.itemView.setOnClickListener(null);
            viewHolder.itemView.setOnLongClickListener(null);
            viewHolder.name.setVisibility(8);
            viewHolder.memory.setVisibility(8);
            viewHolder.details.setVisibility(8);
            viewHolder.autoStart.setVisibility(8);
            viewHolder.backgroundService.setVisibility(8);
            viewHolder.appPackage.setVisibility(8);
            viewHolder.appVersion.setVisibility(8);
            viewHolder.icon.setVisibility(8);
            viewHolder.more.setVisibility(8);
            viewHolder.checkBox.setVisibility(8);
            viewHolder.checkBox.setOnCheckedChangeListener(null);
            viewHolder.undoButton.setVisibility(0);
            viewHolder.undoButton.setOnClickListener(new OnClickListener() {
                public void onClick(View view) {
                    Runnable runnable = (Runnable) TaskAdapter.this.pendingRunnables.get(task);
                    TaskAdapter.this.pendingRunnables.remove(task);
                    if (runnable != null) {
                        TaskAdapter.this.handler.removeCallbacks(runnable);
                    }
                    TaskAdapter.this.itemsPendingRemoval.remove(task);
                    TaskAdapter.this.notifyItemChanged(TaskAdapter.this.items.indexOf(task));
                }
            });
            return;
        }
        viewHolder.itemView.setBackgroundColor(0);
        viewHolder.itemView.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                if (viewHolder.details.getVisibility() == 8) {
                    viewHolder.more.setImageResource(R.drawable.ic_chevron_up_grey600_18dp);
                    viewHolder.details.setVisibility(0);
                } else if (viewHolder.details.getVisibility() == 0) {
                    viewHolder.more.setImageResource(R.drawable.ic_chevron_down_grey600_18dp);
                    viewHolder.details.setVisibility(8);
                }
            }
        });
        viewHolder.itemView.setOnLongClickListener(new OnLongClickListener() {
            public boolean onLongClick(View view) {
                EventBus.getDefault().post(new OpenTaskDetailsEvent(task));
                return true;
            }
        });
        viewHolder.name.setText(task.getLabel());
        if (task.getMemory() > Utils.DOUBLE_EPSILON) {
            StringBuilder sb = new StringBuilder();
            sb.append(task.getMemory());
            sb.append(" MB");
            str = sb.toString();
        } else {
            str = "N/A";
        }
        viewHolder.memory.setText(str);
        viewHolder.icon.setImageDrawable(this.taskController.iconForApp(task.getPackageInfo()));
        if (task.isAutoStart()) {
            viewHolder.autoStart.setVisibility(0);
        }
        if (task.hasBackgroundService()) {
            viewHolder.backgroundService.setVisibility(0);
        }
        StringBuilder sb2 = new StringBuilder();
        sb2.append("Package: ");
        sb2.append(task.getPackageInfo().packageName);
        viewHolder.appPackage.setText(sb2.toString());
        if (task.getPackageInfo().versionName == null) {
            str2 = "Not available";
        } else {
            str2 = task.getPackageInfo().versionName;
        }
        StringBuilder sb3 = new StringBuilder();
        sb3.append("Version: ");
        sb3.append(str2);
        viewHolder.appVersion.setText(sb3.toString());
        viewHolder.name.setVisibility(0);
        viewHolder.memory.setVisibility(0);
        viewHolder.appPackage.setVisibility(0);
        viewHolder.appVersion.setVisibility(0);
        viewHolder.icon.setVisibility(0);
        viewHolder.more.setVisibility(0);
        viewHolder.checkBox.setVisibility(0);
        viewHolder.checkBox.setChecked(task.isChecked());
        viewHolder.checkBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton compoundButton, boolean z) {
                task.setIsChecked(z);
            }
        });
        viewHolder.undoButton.setVisibility(8);
        viewHolder.undoButton.setOnClickListener(null);
    }

    public int getItemCount() {
        return this.items.size();
    }

    public void setUndoOn(boolean z) {
        this.undoOn = z;
    }

    public boolean isUndoOn() {
        return this.undoOn;
    }

    public void pendingRemoval(int i) {
        final Task task = (Task) this.items.get(i);
        if (!this.itemsPendingRemoval.contains(task)) {
            this.itemsPendingRemoval.add(task);
            notifyItemChanged(i);
            AnonymousClass5 r5 = new Runnable() {
                public void run() {
                    TaskAdapter.this.remove(TaskAdapter.this.items.indexOf(task));
                }
            };
            this.handler.postDelayed(r5, 1500);
            this.pendingRunnables.put(task, r5);
        }
    }

    public void remove(int i) {
        Task task = (Task) this.items.get(i);
        if (this.itemsPendingRemoval.contains(task)) {
            this.itemsPendingRemoval.remove(task);
        }
        if (this.items.contains(task)) {
            this.items.remove(i);
            notifyItemRemoved(i);
            this.taskController.killApp(task);
            EventBus.getDefault().post(new TaskRemovedEvent(i, task));
        }
    }

    public boolean isPendingRemoval(int i) {
        Task task;
        try {
            task = (Task) this.items.get(i);
        } catch (ArrayIndexOutOfBoundsException unused) {
            task = null;
        }
        return this.itemsPendingRemoval.contains(task);
    }

    public void swap(List<Task> list) {
        clear();
        if (this.items != null) {
            this.items.addAll(list);
        } else {
            this.items = list;
        }
        notifyDataSetChanged();
    }

    private void clear() {
        if (this.items != null) {
            this.items.clear();
        }
        if (this.itemsPendingRemoval != null) {
            this.itemsPendingRemoval.clear();
        }
        this.pendingRunnables.clear();
    }
}
