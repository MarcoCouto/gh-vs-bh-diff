package com.inmobi.sdk;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Build.VERSION;
import android.os.SystemClock;
import com.inmobi.a.o;
import com.inmobi.ads.a.d;
import com.inmobi.ads.a.f;
import com.inmobi.commons.a.a;
import com.inmobi.commons.core.d.c;
import com.inmobi.commons.core.e.b;
import com.inmobi.commons.core.utilities.Logger;
import com.inmobi.commons.core.utilities.Logger.InternalLogLevel;
import com.inmobi.commons.core.utilities.b.e;
import com.inmobi.commons.core.utilities.b.g;
import com.mintegral.msdk.base.entity.CampaignUnit;
import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.json.JSONObject;

public final class InMobiSdk {
    private static final ExecutorService COMPONENT_SERVICE = Executors.newSingleThreadExecutor();
    public static final String IM_GDPR_CONSENT_AVAILABLE = "gdpr_consent_available";
    /* access modifiers changed from: private */
    public static final String TAG = "InMobiSdk";

    public enum AgeGroup {
        BELOW_18("below18"),
        BETWEEN_18_AND_24("between18and24"),
        BETWEEN_25_AND_29("between25and29"),
        BETWEEN_30_AND_34("between30and34"),
        BETWEEN_35_AND_44("between35and44"),
        BETWEEN_45_AND_54("between45and54"),
        BETWEEN_55_AND_65("between55and65"),
        ABOVE_65("above65");
        

        /* renamed from: a reason: collision with root package name */
        private String f2478a;

        private AgeGroup(String str) {
            this.f2478a = str;
        }

        public final String toString() {
            return this.f2478a;
        }
    }

    public enum Education {
        HIGH_SCHOOL_OR_LESS("highschoolorless"),
        COLLEGE_OR_GRADUATE("collegeorgraduate"),
        POST_GRADUATE_OR_ABOVE("postgraduateorabove");
        

        /* renamed from: a reason: collision with root package name */
        private String f2479a;

        private Education(String str) {
            this.f2479a = str;
        }

        public final String toString() {
            return this.f2479a;
        }
    }

    public enum Gender {
        FEMALE("f"),
        MALE("m");
        

        /* renamed from: a reason: collision with root package name */
        private String f2480a;

        private Gender(String str) {
            this.f2480a = str;
        }

        public final String toString() {
            return this.f2480a;
        }
    }

    public enum LogLevel {
        NONE,
        ERROR,
        DEBUG
    }

    public static String getVersion() {
        return "7.3.0";
    }

    public static void init(Context context, String str) {
        init(context, str, null);
    }

    public static void init(Context context, String str, JSONObject jSONObject) {
        String str2;
        long elapsedRealtime = SystemClock.elapsedRealtime();
        try {
            e.a(jSONObject);
            if (VERSION.SDK_INT < 14) {
                Logger.a(InternalLogLevel.ERROR, TAG, "The minimum supported Android API level is 14, SDK could not be initialized.");
            } else if (context == null) {
                Logger.a(InternalLogLevel.ERROR, TAG, "Context supplied as null, SDK could not be initialized.");
            } else {
                if (str != null) {
                    if (str.trim().length() != 0) {
                        Intent intent = new Intent();
                        intent.setClassName(context.getPackageName(), "com.inmobi.rendering.InMobiAdActivity");
                        if (context.getPackageManager().resolveActivity(intent, 65536) == null) {
                            Logger.a(InternalLogLevel.ERROR, TAG, "The activity com.inmobi.rendering.InMobiAdActivity not present in AndroidManifest. SDK could not be initialized.");
                            return;
                        }
                        if (com.inmobi.commons.core.utilities.e.a(context, CampaignUnit.JSON_KEY_ADS, "android.permission.INTERNET")) {
                            if (com.inmobi.commons.core.utilities.e.a(context, CampaignUnit.JSON_KEY_ADS, "android.permission.ACCESS_NETWORK_STATE")) {
                                if (!com.inmobi.commons.core.utilities.e.a(context, CampaignUnit.JSON_KEY_ADS, "android.permission.ACCESS_COARSE_LOCATION") && !com.inmobi.commons.core.utilities.e.a(context, CampaignUnit.JSON_KEY_ADS, "android.permission.ACCESS_FINE_LOCATION")) {
                                    Logger.a(InternalLogLevel.ERROR, TAG, "Please grant the location permissions (ACCESS_COARSE_LOCATION or ACCESS_FINE_LOCATION, or both) for better ad targeting.");
                                }
                                str2 = str.trim();
                                try {
                                    if (!(str2.length() == 32 || str2.length() == 36)) {
                                        Logger.a(InternalLogLevel.DEBUG, TAG, "Invalid account id passed to init. Please provide a valid account id.");
                                    }
                                    if (a.a()) {
                                        try {
                                            b.a();
                                            b.a("root", "InitRequested", null);
                                            return;
                                        } catch (Exception e) {
                                            StringBuilder sb = new StringBuilder("Error in submitting telemetry event : (");
                                            sb.append(e.getMessage());
                                            sb.append(")");
                                            return;
                                        }
                                    } else {
                                        if (hasSdkVersionChanged(context)) {
                                            com.inmobi.commons.a.b.a(context, a.a(context));
                                            c.a(context, "sdk_version_store").a("sdk_version", "7.3.0");
                                            resetMediaCache(context.getApplicationContext());
                                        }
                                        a.a(context, str2);
                                        com.inmobi.commons.core.configs.b.a().b();
                                        b.a().b();
                                        if (c.a(context, "sdk_version_store").b("db_deletion_failed", false)) {
                                            List<String> b = a.b(context);
                                            for (String sendDbDeletionTelemetryEvent : b) {
                                                sendDbDeletionTelemetryEvent(sendDbDeletionTelemetryEvent);
                                            }
                                            if (b.isEmpty()) {
                                                com.inmobi.commons.a.b.a(context, false);
                                            }
                                        }
                                        g.b();
                                        initComponents();
                                        com.inmobi.commons.core.configs.b.a();
                                        com.inmobi.commons.core.configs.b.d();
                                        COMPONENT_SERVICE.execute(new Runnable() {
                                            /* JADX WARNING: Code restructure failed: missing block: B:46:0x00fa, code lost:
                                                return;
                                             */
                                            public final void run() {
                                                boolean z;
                                                try {
                                                    com.inmobi.ads.b.c();
                                                    f.a().b();
                                                    f a2 = f.a();
                                                    synchronized (f.e) {
                                                        d dVar = a2.f2115a;
                                                        List b = d.b();
                                                        if (!b.isEmpty()) {
                                                            Iterator it = b.iterator();
                                                            while (true) {
                                                                boolean z2 = true;
                                                                if (!it.hasNext()) {
                                                                    break;
                                                                }
                                                                com.inmobi.ads.a.a aVar = (com.inmobi.ads.a.a) it.next();
                                                                if (System.currentTimeMillis() <= aVar.h) {
                                                                    z2 = false;
                                                                }
                                                                if (z2) {
                                                                    f.a(aVar);
                                                                }
                                                            }
                                                            while (true) {
                                                                long j = 0;
                                                                for (com.inmobi.ads.a.a aVar2 : d.b()) {
                                                                    j += new File(aVar2.e).length();
                                                                }
                                                                new StringBuilder("MAX CACHESIZE ").append(a2.b.d);
                                                                if (j <= a2.b.d) {
                                                                    break;
                                                                }
                                                                List a3 = com.inmobi.commons.core.d.b.a().a("asset", d.f2114a, null, null, null, null, "ts ASC ", null);
                                                                com.inmobi.ads.a.a a4 = a3.size() == 0 ? null : d.a((ContentValues) a3.get(0));
                                                                if (a4 == null) {
                                                                    break;
                                                                }
                                                                f.a(a4);
                                                            }
                                                            File a5 = a.a(a.b());
                                                            if (a5.exists()) {
                                                                File[] listFiles = a5.listFiles();
                                                                if (listFiles != null) {
                                                                    for (File file : listFiles) {
                                                                        Iterator it2 = b.iterator();
                                                                        while (true) {
                                                                            if (!it2.hasNext()) {
                                                                                z = false;
                                                                                break;
                                                                            }
                                                                            if (file.getAbsolutePath().equals(((com.inmobi.ads.a.a) it2.next()).e)) {
                                                                                z = true;
                                                                                break;
                                                                            }
                                                                        }
                                                                        if (!z) {
                                                                            new StringBuilder("found Orphan file ").append(file.getAbsolutePath());
                                                                            file.delete();
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                } catch (Exception e) {
                                                    InMobiSdk.TAG;
                                                    StringBuilder sb = new StringBuilder("Error in starting Asset Cache : (");
                                                    sb.append(e.getMessage());
                                                    sb.append(")");
                                                }
                                            }
                                        });
                                        if (context instanceof Activity) {
                                            com.inmobi.commons.core.utilities.a a2 = com.inmobi.commons.core.utilities.a.a();
                                            if (a2 != null) {
                                                a2.a((com.inmobi.commons.core.utilities.a.b) new com.inmobi.commons.core.utilities.a.b() {
                                                    public final void a(boolean z) {
                                                        a.b(z);
                                                        if (z) {
                                                            try {
                                                                InMobiSdk.initComponents();
                                                            } catch (Exception e) {
                                                                InMobiSdk.TAG;
                                                                new StringBuilder("Encountered unexpected error in the onFocusChanged handler: ").append(e.getMessage());
                                                                Logger.a(InternalLogLevel.DEBUG, InMobiSdk.TAG, "SDK encountered an unexpected error; some components may not work as advertised");
                                                            }
                                                        } else {
                                                            InMobiSdk.deInitComponents();
                                                        }
                                                    }
                                                });
                                            }
                                        }
                                        try {
                                            b.a();
                                            b.a("root", "InitRequested", null);
                                        } catch (Exception e2) {
                                            StringBuilder sb2 = new StringBuilder("Error in submitting telemetry event : (");
                                            sb2.append(e2.getMessage());
                                            sb2.append(")");
                                        }
                                        InternalLogLevel internalLogLevel = InternalLogLevel.DEBUG;
                                        String str3 = TAG;
                                        StringBuilder sb3 = new StringBuilder("InMobi SDK initialized with account id: ");
                                        sb3.append(str2);
                                        Logger.a(internalLogLevel, str3, sb3.toString());
                                        try {
                                            HashMap hashMap = new HashMap();
                                            hashMap.put("initTime", Long.valueOf(SystemClock.elapsedRealtime() - elapsedRealtime));
                                            b.a();
                                            b.a("root", "SdkInitialized", hashMap);
                                        } catch (Exception e3) {
                                            StringBuilder sb4 = new StringBuilder("Error in submitting telemetry event : (");
                                            sb4.append(e3.getMessage());
                                            sb4.append(")");
                                        }
                                        printGrantedPermissions();
                                    }
                                } catch (Exception e4) {
                                    e = e4;
                                }
                            }
                        }
                        Logger.a(InternalLogLevel.ERROR, TAG, "Please grant the mandatory permissions : INTERNET and ACCESS_NETWORK_STATE, SDK could not be initialized.");
                        return;
                    }
                }
                Logger.a(InternalLogLevel.ERROR, TAG, "Account id cannot be null or empty. Please provide a valid account id.");
            }
        } catch (Exception e5) {
            e = e5;
            str2 = str;
            a.c();
            Logger.a(InternalLogLevel.ERROR, TAG, "SDK could not be initialized; an unexpected error was encountered");
            new StringBuilder("Encountered unexpected error while initializing the SDK: ").append(e.getMessage());
            InternalLogLevel internalLogLevel2 = InternalLogLevel.DEBUG;
            String str32 = TAG;
            StringBuilder sb32 = new StringBuilder("InMobi SDK initialized with account id: ");
            sb32.append(str2);
            Logger.a(internalLogLevel2, str32, sb32.toString());
            HashMap hashMap2 = new HashMap();
            hashMap2.put("initTime", Long.valueOf(SystemClock.elapsedRealtime() - elapsedRealtime));
            b.a();
            b.a("root", "SdkInitialized", hashMap2);
            printGrantedPermissions();
        }
    }

    public static void updateGDPRConsent(JSONObject jSONObject) {
        e.a(jSONObject);
    }

    public static void setApplicationMuted(boolean z) {
        a.a(z);
    }

    private static void sendDbDeletionTelemetryEvent(String str) {
        if (a.a()) {
            try {
                HashMap hashMap = new HashMap();
                hashMap.put("filename", str);
                StringBuilder sb = new StringBuilder("DB Deleted : ");
                sb.append(str);
                hashMap.put("description", sb.toString());
                b.a();
                b.a(CampaignUnit.JSON_KEY_ADS, "PersistentDataCleanFail", hashMap);
            } catch (Exception e) {
                StringBuilder sb2 = new StringBuilder("Error in submitting telemetry event : (");
                sb2.append(e.getMessage());
                sb2.append(")");
            }
        }
    }

    private static boolean hasSdkVersionChanged(Context context) {
        return com.inmobi.commons.a.b.a(context) == null || !com.inmobi.commons.a.b.a(context).equals("7.3.0");
    }

    private static void printGrantedPermissions() {
        COMPONENT_SERVICE.execute(new Runnable() {
            public final void run() {
                String[] strArr = {"android.permission.ACCESS_COARSE_LOCATION", "android.permission.ACCESS_FINE_LOCATION", "android.permission.ACCESS_WIFI_STATE", "android.permission.CHANGE_WIFI_STATE", "android.permission.VIBRATE", "com.google.android.gms.permission.ACTIVITY_RECOGNITION"};
                StringBuilder sb = new StringBuilder("Permissions granted to SDK are :\nandroid.permission.INTERNET\nandroid.permission.ACCESS_NETWORK_STATE");
                for (int i = 0; i < 6; i++) {
                    String str = strArr[i];
                    if (com.inmobi.commons.core.utilities.e.a(a.b(), CampaignUnit.JSON_KEY_ADS, str)) {
                        sb.append("\n");
                        sb.append(str);
                    }
                }
                Logger.a(InternalLogLevel.DEBUG, InMobiSdk.TAG, sb.toString());
            }
        });
    }

    /* access modifiers changed from: private */
    public static void initComponents() {
        try {
            COMPONENT_SERVICE.execute(new Runnable() {
                public final void run() {
                    try {
                        com.inmobi.commons.core.utilities.uid.c a2 = com.inmobi.commons.core.utilities.uid.c.a();
                        try {
                            com.inmobi.commons.core.utilities.uid.c.c();
                            a2.b();
                        } catch (Exception e) {
                            new StringBuilder("SDK encountered an unexpected error while initializing the UID helper component; ").append(e.getMessage());
                        }
                        com.inmobi.commons.core.utilities.uid.c.a().b();
                        com.inmobi.commons.core.configs.b.a().b();
                        com.inmobi.rendering.a.c.a().b();
                        com.inmobi.commons.core.a.a a3 = com.inmobi.commons.core.a.a.a();
                        com.inmobi.commons.core.a.a.b.set(false);
                        com.inmobi.commons.core.configs.b.a().a((com.inmobi.commons.core.configs.a) a3.c, (com.inmobi.commons.core.configs.b.c) a3);
                        a3.d = a3.c.f2322a;
                        a3.f2318a.execute(new Runnable() {
                            public final void run() {
                                a.this.f2318a.execute(new Runnable() {
                                    public final void run() {
                                        if (a.this.h.a("default") > 0) {
                                            a.a(a.this);
                                        }
                                    }
                                });
                            }
                        });
                        b.a().b();
                        com.inmobi.b.a a4 = com.inmobi.b.a.a();
                        com.inmobi.b.a.b.set(false);
                        e.c();
                        com.inmobi.commons.core.configs.b.a().a((com.inmobi.commons.core.configs.a) a4.c, (com.inmobi.commons.core.configs.b.c) a4);
                        a4.e = a4.c.b;
                        a4.f2312a.execute(new Runnable() {
                            public final void run() {
                                a.a(a.this);
                            }
                        });
                        com.inmobi.ads.b.a();
                        o.a().b();
                        com.inmobi.ads.d.b.d().a();
                        com.inmobi.ads.d.a.a("native").a();
                        f.a().b();
                    } catch (Exception e2) {
                        InMobiSdk.TAG;
                        new StringBuilder("Encountered unexpected error in starting SDK components: ").append(e2.getMessage());
                        Logger.a(InternalLogLevel.DEBUG, InMobiSdk.TAG, "SDK encountered unexpected error while starting internal components");
                    }
                }
            });
        } catch (Exception e) {
            new StringBuilder("Encountered unexpected error in starting SDK components: ").append(e.getMessage());
            Logger.a(InternalLogLevel.DEBUG, TAG, "SDK encountered unexpected error while starting internal components");
        }
    }

    /* access modifiers changed from: private */
    public static void deInitComponents() {
        try {
            COMPONENT_SERVICE.execute(new Runnable() {
                public final void run() {
                    try {
                        com.inmobi.commons.core.configs.b.a().c();
                        b a2 = b.a();
                        b.b.set(true);
                        a2.f2351a.execute(new Runnable() {
                            public final void run() {
                                if (b.this.j != null) {
                                    b.this.j.a();
                                    b.this.j = null;
                                }
                            }
                        });
                        com.inmobi.b.a a3 = com.inmobi.b.a.a();
                        com.inmobi.b.a.b.set(true);
                        a3.f2312a.execute(new Runnable() {
                            public final void run() {
                                if (a.this.i != null) {
                                    a.this.i.a();
                                    a.this.i = null;
                                }
                            }
                        });
                        o.a().c();
                        com.inmobi.ads.d.b.d().b();
                        com.inmobi.ads.d.a.a("native").b();
                        f a4 = f.a();
                        a4.d.set(true);
                        a4.c();
                    } catch (Exception e) {
                        InMobiSdk.TAG;
                        new StringBuilder("Encountered unexpected error in stopping SDK components; ").append(e.getMessage());
                        Logger.a(InternalLogLevel.ERROR, InMobiSdk.TAG, "SDK encountered unexpected error while stopping internal components");
                    }
                }
            });
        } catch (Exception e) {
            new StringBuilder("Encountered unexpected error in stopping SDK components; ").append(e.getMessage());
            Logger.a(InternalLogLevel.ERROR, TAG, "SDK encountered unexpected error while stopping internal components");
        }
    }

    private static void resetMediaCache(final Context context) {
        final File a2 = a.a(context);
        COMPONENT_SERVICE.execute(new Runnable() {
            public final void run() {
                a.a(a2);
                a.b(context);
            }
        });
        if (!a2.mkdir()) {
            a2.isDirectory();
        }
    }

    public static void setLogLevel(LogLevel logLevel) {
        switch (logLevel) {
            case NONE:
                Logger.a(InternalLogLevel.NONE);
                return;
            case ERROR:
                Logger.a(InternalLogLevel.ERROR);
                return;
            case DEBUG:
                Logger.a(InternalLogLevel.DEBUG);
                break;
        }
    }

    public static void setAge(int i) {
        g.a(i);
    }

    public static void setAgeGroup(AgeGroup ageGroup) {
        g.a(ageGroup.toString().toLowerCase(Locale.ENGLISH));
    }

    public static void setAreaCode(String str) {
        g.b(str);
    }

    public static void setPostalCode(String str) {
        g.c(str);
    }

    public static void setLocationWithCityStateCountry(String str, String str2, String str3) {
        g.d(str);
        g.e(str2);
        g.f(str3);
    }

    public static void setYearOfBirth(int i) {
        g.b(i);
    }

    public static void setGender(Gender gender) {
        g.g(gender.toString().toLowerCase(Locale.ENGLISH));
    }

    public static void setEducation(Education education) {
        g.h(education.toString().toLowerCase(Locale.ENGLISH));
    }

    public static void setLanguage(String str) {
        g.i(str);
    }

    public static void setInterests(String str) {
        g.j(str);
    }

    public static void setLocation(Location location) {
        g.a(location);
    }
}
