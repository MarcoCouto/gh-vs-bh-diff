package com.inmobi.a;

import com.facebook.share.internal.MessengerShareContentUtility;
import com.inmobi.commons.core.network.c;
import com.inmobi.commons.core.utilities.uid.d;
import io.fabric.sdk.android.services.network.HttpRequest;

/* compiled from: IceNetworkRequest */
public class j extends c {
    private static final String c = "j";

    /* renamed from: a reason: collision with root package name */
    int f2049a;
    int b;

    j(String str, int i, int i2, d dVar, k kVar) {
        super(HttpRequest.METHOD_POST, str, true, dVar);
        this.f2049a = i;
        this.b = i2;
        this.n.put(MessengerShareContentUtility.ATTACHMENT_PAYLOAD, kVar.a().toString());
    }
}
