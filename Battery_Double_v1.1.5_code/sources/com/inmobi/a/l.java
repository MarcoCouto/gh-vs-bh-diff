package com.inmobi.a;

import com.inmobi.a.b.a;
import com.mintegral.msdk.base.entity.CampaignEx;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: IceWifiSample */
public class l {
    private static final String d = "l";

    /* renamed from: a reason: collision with root package name */
    a f2051a;
    List<a> b;
    Map<String, String> c;
    private long e = Calendar.getInstance().getTimeInMillis();

    l() {
    }

    public final JSONObject a() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put(CampaignEx.JSON_KEY_ST_TS, this.e);
            if (this.c != null && !this.c.isEmpty()) {
                for (Entry entry : this.c.entrySet()) {
                    jSONObject.put((String) entry.getKey(), entry.getValue());
                }
            }
            if (this.f2051a != null) {
                jSONObject.put("c-ap", this.f2051a.a());
            }
            JSONArray jSONArray = new JSONArray();
            if (this.b != null) {
                for (int i = 0; i < this.b.size(); i++) {
                    jSONArray.put(((a) this.b.get(i)).a());
                }
                if (jSONArray.length() > 0) {
                    jSONObject.put("v-ap", jSONArray);
                }
            }
        } catch (JSONException unused) {
        }
        return jSONObject;
    }
}
