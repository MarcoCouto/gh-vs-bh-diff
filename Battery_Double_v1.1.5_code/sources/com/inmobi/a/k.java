package com.inmobi.a;

import com.inmobi.commons.core.utilities.b.h;
import com.mintegral.msdk.base.entity.CampaignEx;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: IceSample */
public class k {
    private static final String d = "k";

    /* renamed from: a reason: collision with root package name */
    Map<String, Object> f2050a;
    h b;
    List<l> c;
    private long e = Calendar.getInstance().getTimeInMillis();

    k() {
    }

    public final JSONObject a() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put(CampaignEx.JSON_KEY_ST_TS, this.e);
            if (this.f2050a != null && !this.f2050a.isEmpty()) {
                jSONObject.put("l", new JSONObject(this.f2050a));
            }
            if (this.b != null) {
                jSONObject.put("s", this.b.b());
            }
            JSONArray jSONArray = new JSONArray();
            for (int i = 0; i < this.c.size(); i++) {
                jSONArray.put(((l) this.c.get(i)).a());
            }
            if (jSONArray.length() > 0) {
                jSONObject.put("w", jSONArray);
            }
        } catch (JSONException unused) {
        }
        return jSONObject;
    }
}
