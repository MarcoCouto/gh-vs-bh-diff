package com.inmobi.a;

import android.content.Context;
import com.inmobi.a.p.a;
import java.util.ArrayList;
import java.util.List;

/* compiled from: CarbWorker */
public class g {

    /* renamed from: a reason: collision with root package name */
    private static final String f2043a = "g";
    private a b;
    /* access modifiers changed from: private */
    public boolean c = false;
    /* access modifiers changed from: private */
    public a d = new a();
    private e e = new e();

    g() {
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x002f  */
    public final synchronized void a(a aVar) {
        boolean z;
        this.b = aVar;
        if (!this.c) {
            long b2 = this.d.f2033a.b("carb_last_update_ts", 0);
            if (b2 != 0) {
                if (System.currentTimeMillis() - b2 < ((long) (this.b.d * 1000))) {
                    z = false;
                    if (z) {
                        this.c = true;
                        new Thread(new Runnable() {
                            public final void run() {
                                c a2 = g.a(g.this);
                                if (!a2.f2039a) {
                                    g.this.d.f2033a.a("carb_last_update_ts", System.currentTimeMillis());
                                    if (!(a2.d == 0)) {
                                        g.a(g.this, a2, g.a((List) a2.b));
                                    }
                                }
                                g.this.c = false;
                            }
                        }).start();
                    }
                }
            }
            z = true;
            if (z) {
            }
        }
    }

    private static boolean a(String str) {
        Context b2 = com.inmobi.commons.a.a.b();
        boolean z = false;
        if (b2 == null) {
            return false;
        }
        try {
            b2.getPackageManager().getPackageInfo(str, 256);
            z = true;
        } catch (Exception unused) {
        }
        return z;
    }

    static /* synthetic */ c a(g gVar) {
        b bVar = new b(gVar.b.b, gVar.b.e, gVar.b.f, o.a().d());
        bVar.u = gVar.b.h;
        bVar.q = gVar.b.g * 1000;
        bVar.r = gVar.b.g * 1000;
        return e.a(bVar);
    }

    static /* synthetic */ List a(List list) {
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < list.size(); i++) {
            if (!a(((d) list.get(i)).f2040a)) {
                arrayList.add(list.get(i));
            }
        }
        return arrayList;
    }

    static /* synthetic */ void a(g gVar, c cVar, List list) {
        f fVar = new f(gVar.b.c, gVar.b.e, gVar.b.f, o.a().d(), list, cVar);
        fVar.q = gVar.b.g * 1000;
        fVar.r = gVar.b.g * 1000;
        e.a(fVar);
    }
}
