package com.inmobi.a;

import com.inmobi.commons.core.network.c;
import com.inmobi.commons.core.utilities.uid.d;
import io.fabric.sdk.android.services.network.HttpRequest;

/* compiled from: CarbGetListNetworkRequest */
public final class b extends c {

    /* renamed from: a reason: collision with root package name */
    private int f2037a;
    private int b;

    b(String str, int i, int i2, d dVar) {
        super(HttpRequest.METHOD_POST, str, true, dVar);
        this.f2037a = i;
        this.b = i2;
    }
}
