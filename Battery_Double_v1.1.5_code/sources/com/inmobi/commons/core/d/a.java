package com.inmobi.commons.core.d;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/* compiled from: DbHelper */
public final class a extends SQLiteOpenHelper {

    /* renamed from: a reason: collision with root package name */
    public static final String f2348a = "com.im_7.3.0.db";

    public final void onCreate(SQLiteDatabase sQLiteDatabase) {
    }

    public final void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
    }

    public a(Context context) {
        super(context, f2348a, null, 1);
    }
}
