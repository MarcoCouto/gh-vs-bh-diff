package com.inmobi.commons.core.network;

import android.content.Context;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class WebViewNetworkTask {
    private static final String d = "WebViewNetworkTask";

    /* renamed from: a reason: collision with root package name */
    public c f2366a;
    public WebViewClient b;
    public NetworkTaskWebView c;

    public class NetworkTaskWebView extends WebView {

        /* renamed from: a reason: collision with root package name */
        public boolean f2367a;

        public NetworkTaskWebView(Context context) {
            super(context);
        }

        public void destroy() {
            this.f2367a = true;
            super.destroy();
        }
    }

    public WebViewNetworkTask(c cVar, WebViewClient webViewClient) {
        this.b = webViewClient;
        this.f2366a = cVar;
    }
}
