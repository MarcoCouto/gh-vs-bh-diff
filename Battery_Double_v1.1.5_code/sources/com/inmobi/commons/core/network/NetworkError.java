package com.inmobi.commons.core.network;

import com.explorestack.iab.vast.VastError;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.tapjoy.TJAdUnitConstants;

public final class NetworkError {

    /* renamed from: a reason: collision with root package name */
    public ErrorCode f2364a;
    public String b;

    public enum ErrorCode {
        NETWORK_UNAVAILABLE_ERROR(0),
        UNKNOWN_ERROR(-1),
        NETWORK_IO_ERROR(-2),
        OUT_OF_MEMORY_ERROR(-3),
        INVALID_ENCRYPTED_RESPONSE_RECEIVED(-4),
        RESPONSE_EXCEEDS_SPECIFIED_SIZE_LIMIT(-5),
        GZIP_DECOMPRESSION_FAILED(-6),
        BAD_REQUEST(-7),
        GDPR_COMPLIANCE_ENFORCED(-8),
        GENERIC_HTTP_2XX(-9),
        HTTP_NO_CONTENT(204),
        HTTP_NOT_MODIFIED(304),
        HTTP_SEE_OTHER(VastError.ERROR_CODE_WRAPPER_RESPONSE_NO_AD),
        HTTP_SERVER_NOT_FOUND(404),
        HTTP_MOVED_TEMP(302),
        HTTP_INTERNAL_SERVER_ERROR(TJAdUnitConstants.DEFAULT_VOLUME_CHECK_INTERVAL),
        HTTP_NOT_IMPLEMENTED(IronSourceError.ERROR_CODE_NO_CONFIGURATION_AVAILABLE),
        HTTP_BAD_GATEWAY(IronSourceError.ERROR_CODE_USING_CACHED_CONFIGURATION),
        HTTP_SERVER_NOT_AVAILABLE(503),
        HTTP_GATEWAY_TIMEOUT(504),
        HTTP_VERSION_NOT_SUPPORTED(IronSourceError.ERROR_CODE_KEY_NOT_SET);
        

        /* renamed from: a reason: collision with root package name */
        private int f2365a;

        private ErrorCode(int i) {
            this.f2365a = i;
        }

        public final int getValue() {
            return this.f2365a;
        }

        public static ErrorCode fromValue(int i) {
            ErrorCode[] values;
            if (400 <= i && 500 > i) {
                return BAD_REQUEST;
            }
            if (200 < i && 300 > i) {
                return GENERIC_HTTP_2XX;
            }
            for (ErrorCode errorCode : values()) {
                if (errorCode.f2365a == i) {
                    return errorCode;
                }
            }
            return null;
        }
    }

    public NetworkError(ErrorCode errorCode, String str) {
        this.f2364a = errorCode;
        this.b = str;
    }
}
