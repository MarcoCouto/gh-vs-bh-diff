package com.inmobi.commons.core.b;

import android.support.annotation.NonNull;
import java.util.List;

/* compiled from: EventPayload */
public final class c {

    /* renamed from: a reason: collision with root package name */
    final List<Integer> f2328a;
    public final String b;
    final boolean c;

    public c(@NonNull List<Integer> list, @NonNull String str, boolean z) {
        this.f2328a = list;
        this.b = str;
        this.c = z;
    }
}
