package com.inmobi.commons.core.configs;

import com.ironsource.sdk.constants.Constants.RequestParameters;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: RootConfig */
public final class h extends a {
    private static final Object i = new Object();

    /* renamed from: a reason: collision with root package name */
    int f2345a = 3;
    int b = 60;
    int c = 3;
    public int d = -1;
    b e = new b();
    JSONObject f = new JSONObject();
    public boolean g = false;
    private List<a> h = new ArrayList();

    /* compiled from: RootConfig */
    static final class a {

        /* renamed from: a reason: collision with root package name */
        String f2346a;
        long b;
        String c;
        String d;
        String e = "https://config.inmobi.cn/config-server/v1/config/secure.cfg";

        a() {
        }
    }

    /* compiled from: RootConfig */
    public static final class b {

        /* renamed from: a reason: collision with root package name */
        String f2347a = "7.3.0";
        String b = "https://www.inmobi.com/products/sdk/#downloads";
    }

    public final String a() {
        return "root";
    }

    public final JSONObject b() throws JSONException {
        boolean z;
        JSONObject b2 = super.b();
        JSONArray jSONArray = new JSONArray();
        b2.put("maxRetries", this.f2345a);
        b2.put("retryInterval", this.b);
        b2.put("waitTime", this.c);
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("version", this.e.f2347a);
        jSONObject.put("url", this.e.b);
        b2.put("latestSdkInfo", jSONObject);
        synchronized (i) {
            z = false;
            for (int i2 = 0; i2 < this.h.size(); i2++) {
                a aVar = (a) this.h.get(i2);
                JSONObject jSONObject2 = new JSONObject();
                jSONObject2.put("type", aVar.f2346a);
                jSONObject2.put("expiry", aVar.b);
                jSONObject2.put(RequestParameters.PROTOCOL, aVar.c);
                jSONObject2.put("url", aVar.d);
                if ("root".equals(aVar.f2346a)) {
                    jSONObject2.put("fallbackUrl", aVar.e);
                }
                jSONArray.put(jSONObject2);
            }
        }
        b2.put("components", jSONArray);
        b2.put("monetizationDisabled", this.g);
        JSONObject jSONObject3 = new JSONObject();
        String str = "transmitRequest";
        if (this.d == 1) {
            z = true;
        }
        jSONObject3.put(str, z);
        b2.put("gdpr", jSONObject3);
        return b2;
    }

    public final void a(JSONObject jSONObject) throws JSONException {
        super.a(jSONObject);
        this.f2345a = jSONObject.getInt("maxRetries");
        this.b = jSONObject.getInt("retryInterval");
        this.c = jSONObject.getInt("waitTime");
        JSONObject jSONObject2 = jSONObject.getJSONObject("latestSdkInfo");
        this.e.f2347a = jSONObject2.getString("version");
        this.e.b = jSONObject2.getString("url");
        JSONArray jSONArray = jSONObject.getJSONArray("components");
        synchronized (i) {
            this.h.clear();
            for (int i2 = 0; i2 < jSONArray.length(); i2++) {
                JSONObject jSONObject3 = jSONArray.getJSONObject(i2);
                a aVar = new a();
                aVar.f2346a = jSONObject3.getString("type");
                aVar.b = jSONObject3.getLong("expiry");
                aVar.c = jSONObject3.getString(RequestParameters.PROTOCOL);
                aVar.d = jSONObject3.getString("url");
                if ("root".equals(aVar.f2346a)) {
                    aVar.e = jSONObject3.getString("fallbackUrl");
                }
                this.h.add(aVar);
            }
        }
        this.g = jSONObject.getBoolean("monetizationDisabled");
        this.d = jSONObject.getJSONObject("gdpr").getBoolean("transmitRequest") ? 1 : 0;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:46:0x00b0, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x00b5, code lost:
        if (r9.d == -1) goto L_0x00b9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x00b8, code lost:
        return true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x00b9, code lost:
        return false;
     */
    public final boolean c() {
        if (this.h == null || this.f2345a < 0 || this.b < 0 || this.c < 0 || this.e.f2347a.trim().length() == 0 || (!this.e.b.startsWith("http://") && !this.e.b.startsWith("https://"))) {
            return false;
        }
        synchronized (i) {
            int i2 = 0;
            while (i2 < this.h.size()) {
                a aVar = (a) this.h.get(i2);
                if (aVar.f2346a.trim().length() == 0) {
                    return false;
                }
                if (Long.valueOf(aVar.b).longValue() >= 0) {
                    if (Long.valueOf(aVar.b).longValue() <= 864000) {
                        if (aVar.c.trim().length() == 0) {
                            return false;
                        }
                        if (c(aVar.d)) {
                            return false;
                        }
                        if ("root".equals(aVar.f2346a) && c(aVar.e)) {
                            return false;
                        }
                        i2++;
                    }
                }
            }
        }
    }

    private static boolean c(String str) {
        return str == null || str.trim().length() == 0 || (!str.startsWith("http://") && !str.startsWith("https://"));
    }

    public final a d() {
        return new h();
    }

    public final long a(String str) {
        synchronized (i) {
            for (int i2 = 0; i2 < this.h.size(); i2++) {
                a aVar = (a) this.h.get(i2);
                if (str.equals(aVar.f2346a)) {
                    long j = aVar.b;
                    return j;
                }
            }
            return 86400;
        }
    }

    public final String b(String str) {
        synchronized (i) {
            for (int i2 = 0; i2 < this.h.size(); i2++) {
                a aVar = (a) this.h.get(i2);
                if (str.equals(aVar.f2346a)) {
                    String str2 = aVar.d;
                    return str2;
                }
            }
            String str3 = "";
            return str3;
        }
    }

    public final String e() {
        synchronized (i) {
            for (a aVar : this.h) {
                if ("root".equals(aVar.f2346a)) {
                    String str = aVar.e;
                    return str;
                }
            }
            String str2 = "https://config.inmobi.cn/config-server/v1/config/secure.cfg";
            return str2;
        }
    }
}
