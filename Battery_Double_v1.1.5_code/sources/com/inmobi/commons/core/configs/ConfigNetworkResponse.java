package com.inmobi.commons.core.configs;

import android.support.v7.widget.helper.ItemTouchHelper.Callback;
import com.inmobi.commons.core.e.b;
import com.inmobi.commons.core.network.NetworkError.ErrorCode;
import com.inmobi.commons.core.network.d;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.tapjoy.TJAdUnitConstants;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import org.json.JSONException;
import org.json.JSONObject;

final class ConfigNetworkResponse {
    /* access modifiers changed from: private */
    public static final String b = "com.inmobi.commons.core.configs.ConfigNetworkResponse";

    /* renamed from: a reason: collision with root package name */
    Map<String, ConfigResponse> f2333a = new HashMap();
    private Map<String, a> c;
    private d d;
    private d e;
    private long f;

    public static class ConfigResponse {

        /* renamed from: a reason: collision with root package name */
        ConfigResponseStatus f2334a;
        a b;
        d c;

        public enum ConfigResponseStatus {
            SUCCESS(Callback.DEFAULT_DRAG_ANIMATION_DURATION),
            NOT_MODIFIED(304),
            PRODUCT_NOT_FOUND(404),
            INTERNAL_ERROR(TJAdUnitConstants.DEFAULT_VOLUME_CHECK_INTERVAL),
            UNKNOWN(-1);
            

            /* renamed from: a reason: collision with root package name */
            private int f2335a;

            private ConfigResponseStatus(int i) {
                this.f2335a = i;
            }

            public final int getValue() {
                return this.f2335a;
            }

            public static ConfigResponseStatus fromValue(int i) {
                ConfigResponseStatus[] values;
                for (ConfigResponseStatus configResponseStatus : values()) {
                    if (configResponseStatus.f2335a == i) {
                        return configResponseStatus;
                    }
                }
                return UNKNOWN;
            }
        }

        public ConfigResponse(JSONObject jSONObject, a aVar) {
            this.b = aVar;
            if (jSONObject != null) {
                try {
                    this.f2334a = ConfigResponseStatus.fromValue(jSONObject.getInt("status"));
                    if (this.f2334a == ConfigResponseStatus.SUCCESS) {
                        this.b.a(jSONObject.getJSONObject("content"));
                        if (!this.b.c()) {
                            this.c = new d(2, "The received config has failed validation.");
                            ConfigNetworkResponse.b;
                            StringBuilder sb = new StringBuilder("Config type:");
                            sb.append(this.b.a());
                            sb.append(" Error code:");
                            sb.append(this.c.f2341a);
                            sb.append(" Error message:");
                            sb.append(this.c.b);
                        }
                    } else if (this.f2334a == ConfigResponseStatus.NOT_MODIFIED) {
                        ConfigNetworkResponse.b;
                        StringBuilder sb2 = new StringBuilder("Config type:");
                        sb2.append(this.b.a());
                        sb2.append(" Config not modified");
                    } else {
                        this.c = new d(1, this.f2334a.toString());
                        ConfigNetworkResponse.b;
                        StringBuilder sb3 = new StringBuilder("Config type:");
                        sb3.append(this.b.a());
                        sb3.append(" Error code:");
                        sb3.append(this.c.f2341a);
                        sb3.append(" Error message:");
                        sb3.append(this.c.b);
                    }
                } catch (JSONException e) {
                    this.c = new d(2, e.getLocalizedMessage());
                    ConfigNetworkResponse.b;
                    StringBuilder sb4 = new StringBuilder("Config type:");
                    sb4.append(this.b.a());
                    sb4.append(" Error code:");
                    sb4.append(this.c.f2341a);
                    sb4.append(" Error message:");
                    sb4.append(this.c.b);
                }
            }
        }

        public final boolean a() {
            return this.c != null;
        }
    }

    ConfigNetworkResponse(Map<String, a> map, d dVar, long j) {
        this.c = map;
        this.d = dVar;
        this.f = j;
        c();
    }

    private void c() {
        if (!this.d.a()) {
            try {
                JSONObject jSONObject = new JSONObject(this.d.b());
                Iterator keys = jSONObject.keys();
                while (keys.hasNext()) {
                    String str = (String) keys.next();
                    JSONObject jSONObject2 = jSONObject.getJSONObject(str);
                    if (this.c.get(str) != null) {
                        this.f2333a.put(str, new ConfigResponse(jSONObject2, (a) this.c.get(str)));
                    }
                }
            } catch (JSONException e2) {
                this.e = new d(2, e2.getLocalizedMessage());
                StringBuilder sb = new StringBuilder("Error code:");
                sb.append(this.e.f2341a);
                sb.append(" Error message:");
                sb.append(this.e.b);
                try {
                    HashMap hashMap = new HashMap();
                    hashMap.put("name", a(this.c));
                    hashMap.put(IronSourceConstants.EVENTS_ERROR_CODE, "ParsingError");
                    hashMap.put(IronSourceConstants.EVENTS_ERROR_REASON, e2.getLocalizedMessage());
                    hashMap.put("latency", Long.valueOf(this.f));
                    b.a();
                    b.a("root", "InvalidConfig", hashMap);
                } catch (Exception e3) {
                    StringBuilder sb2 = new StringBuilder("Error in submitting telemetry event : (");
                    sb2.append(e3.getMessage());
                    sb2.append(")");
                }
            }
        } else {
            for (Entry entry : this.c.entrySet()) {
                ConfigResponse configResponse = new ConfigResponse(null, (a) entry.getValue());
                configResponse.c = new d(0, "Network error in fetching config.");
                this.f2333a.put(entry.getKey(), configResponse);
            }
            this.e = new d(0, this.d.b.b);
            StringBuilder sb3 = new StringBuilder("Error code:");
            sb3.append(this.e.f2341a);
            sb3.append(" Error message:");
            sb3.append(this.e.b);
            try {
                HashMap hashMap2 = new HashMap();
                hashMap2.put("name", a(this.c));
                hashMap2.put(IronSourceConstants.EVENTS_ERROR_CODE, String.valueOf(this.d.b.f2364a.getValue()));
                hashMap2.put(IronSourceConstants.EVENTS_ERROR_REASON, this.d.b.b);
                hashMap2.put("latency", Long.valueOf(this.f));
                b.a();
                b.a("root", "InvalidConfig", hashMap2);
            } catch (Exception e4) {
                StringBuilder sb4 = new StringBuilder("Error in submitting telemetry event : (");
                sb4.append(e4.getMessage());
                sb4.append(")");
            }
        }
    }

    public final boolean a() {
        if (!(this.d == null || this.d.b == null)) {
            if (this.d.b.f2364a != ErrorCode.BAD_REQUEST) {
                int value = this.d.b.f2364a.getValue();
                if (500 <= value && value < 600) {
                    return true;
                }
            }
            return true;
        }
        return false;
    }

    private static String a(Map<String, a> map) {
        String str = "";
        for (String str2 : map.keySet()) {
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append(str2);
            sb.append(",");
            str = sb.toString();
        }
        return str.substring(0, str.length() - 1);
    }
}
