package com.inmobi.commons.core.configs;

import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: PkConfig */
public final class g extends a {

    /* renamed from: a reason: collision with root package name */
    public String f2344a = "010001";
    public String b = "E72409364B865B757E1D6B8DB73011BBB1D20C1A9F931ADD3C4C09E2794CE102F8AA7F2D50EB88F9880A576E6C7B0E95712CAE9416F7BACB798564627846E93B";
    public String c = "1";
    private String d = "rsa";

    public final String a() {
        return "pk";
    }

    public final void a(JSONObject jSONObject) throws JSONException {
        super.a(jSONObject);
        this.f2344a = jSONObject.getString("e");
        this.b = jSONObject.getString("m");
        this.d = jSONObject.getString("alg");
        this.c = jSONObject.getString("ver");
    }

    public final JSONObject b() throws JSONException {
        JSONObject b2 = super.b();
        b2.put("e", this.f2344a);
        b2.put("m", this.b);
        b2.put("alg", this.d);
        b2.put("ver", this.c);
        return b2;
    }

    public final boolean c() {
        return (this.f2344a.trim().length() == 0 || this.b.trim().length() == 0 || this.d.trim().length() == 0 || this.c.trim().length() == 0) ? false : true;
    }

    public final a d() {
        return new g();
    }
}
