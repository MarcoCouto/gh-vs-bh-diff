package com.inmobi.rendering.mraid;

import android.content.Context;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.os.Vibrator;
import com.inmobi.rendering.RenderView;
import java.lang.ref.WeakReference;

/* compiled from: SystemTasksProcessor */
public class h {

    /* renamed from: a reason: collision with root package name */
    private static final String f2474a = "h";
    private RenderView b;
    private a c;

    /* compiled from: SystemTasksProcessor */
    static final class a extends Handler {

        /* renamed from: a reason: collision with root package name */
        private static final String f2475a = "h$a";
        private WeakReference<RenderView> b;

        public a(Looper looper, RenderView renderView) {
            super(looper);
            this.b = new WeakReference<>(renderView);
        }

        public final void handleMessage(Message message) {
            if (message.what == 1) {
                String str = (String) message.obj;
                RenderView renderView = (RenderView) this.b.get();
                if (renderView != null) {
                    renderView.a(str, "broadcastEvent('vibrateComplete');");
                }
            }
        }
    }

    public h(RenderView renderView) {
        this.b = renderView;
        HandlerThread handlerThread = new HandlerThread("SystemTasksHandlerThread");
        handlerThread.start();
        this.c = new a(handlerThread.getLooper(), renderView);
    }

    public final void a(Context context) {
        if (this.c != null && this.c.hasMessages(1)) {
            this.c.removeMessages(1);
            ((Vibrator) context.getSystemService("vibrator")).cancel();
        }
    }
}
