package com.inmobi.rendering;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Point;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewCompat;
import android.util.SparseArray;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import com.facebook.internal.NativeProtocol;
import com.inmobi.ads.AdContainer;
import com.inmobi.ads.AdContainer.RenderingProperties;
import com.inmobi.ads.AdContainer.RenderingProperties.PlacementType;
import com.inmobi.ads.NativeVideoView;
import com.inmobi.ads.NativeVideoWrapper;
import com.inmobi.ads.ah;
import com.inmobi.ads.ao;
import com.inmobi.ads.bd;
import com.inmobi.ads.be;
import com.inmobi.ads.c;
import com.inmobi.ads.c.l;
import com.inmobi.ads.cb;
import com.inmobi.commons.core.configs.b;
import com.inmobi.commons.core.utilities.Logger;
import com.inmobi.commons.core.utilities.Logger.InternalLogLevel;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.ironsource.sdk.constants.LocationConst;
import com.tapjoy.TJAdUnitConstants.String;
import java.util.HashMap;
import java.util.Map;

@SuppressLint({"ClickableViewAccessibility"})
@TargetApi(15)
public class InMobiAdActivity extends Activity {
    @SuppressLint({"UseSparseArrays"})
    public static Map<Integer, a> b = new HashMap();
    @SuppressLint({"UseSparseArrays"})
    public static Map<Integer, Intent> c = new HashMap();
    public static Integer d = Integer.valueOf(0);
    @SuppressLint({"UseSparseArrays"})
    public static Map<Integer, Object> e = new HashMap();
    public static Integer f = Integer.valueOf(0);
    /* access modifiers changed from: private */
    public static final String g = "InMobiAdActivity";
    private static SparseArray<AdContainer> h = new SparseArray<>();
    private static RenderView i;
    private static com.inmobi.rendering.RenderView.a j;

    /* renamed from: a reason: collision with root package name */
    public boolean f2403a = false;
    /* access modifiers changed from: private */
    public AdContainer k;
    /* access modifiers changed from: private */
    public RenderView l;
    private CustomView m;
    private CustomView n;
    /* access modifiers changed from: private */
    public NativeVideoView o;
    private int p;
    private int q;
    private boolean r = false;
    private boolean s = false;

    public interface a {
    }

    public static int a(AdContainer adContainer) {
        int hashCode = adContainer.hashCode();
        h.put(hashCode, adContainer);
        return hashCode;
    }

    public static void a(@NonNull Object obj) {
        h.remove(obj.hashCode());
    }

    public static void a(RenderView renderView) {
        i = renderView;
    }

    public static void a(com.inmobi.rendering.RenderView.a aVar) {
        j = aVar;
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (!this.f2403a) {
            if (100 == this.p) {
                if (!(this.l == null || this.l.getFullScreenEventsListener() == null)) {
                    try {
                        if (!this.r) {
                            this.r = true;
                            this.l.getFullScreenEventsListener().a(this.l);
                        }
                    } catch (Exception unused) {
                    }
                }
            } else if (this.q == 200 && 102 == this.p) {
                if (!(this.k == null || this.k.getFullScreenEventsListener() == null)) {
                    if (!this.r) {
                        this.r = true;
                        this.k.getFullScreenEventsListener().a(null);
                    }
                }
            } else if (201 == this.q) {
                if ((this.k instanceof bd) && this.o != null) {
                    final be beVar = (be) this.o.getTag();
                    if (beVar != null && this.s) {
                        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                            public final void run() {
                                if (InMobiAdActivity.this.k == null) {
                                    return;
                                }
                                if (InMobiAdActivity.this.k.getRenderingProperties().f2060a != PlacementType.PLACEMENT_TYPE_FULLSCREEN || !((Boolean) beVar.v.get("didCompleteQ4")).booleanValue()) {
                                    InMobiAdActivity.this.o.start();
                                }
                            }
                        }, 50);
                    }
                    if (this.k.getFullScreenEventsListener() != null) {
                        try {
                            if (!this.r) {
                                this.r = true;
                                this.k.getFullScreenEventsListener().a(beVar);
                            }
                        } catch (Exception e2) {
                            com.inmobi.commons.core.a.a.a().a(new com.inmobi.commons.core.e.a(e2));
                        }
                    }
                } else if (this.k instanceof ah) {
                    try {
                        if (!this.r) {
                            this.r = true;
                            this.k.getFullScreenEventsListener().a(null);
                        }
                    } catch (Exception e3) {
                        com.inmobi.commons.core.a.a.a().a(new com.inmobi.commons.core.e.a(e3));
                    }
                }
            }
            this.s = false;
        }
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        if (!this.f2403a && 102 == this.p && this.k != null) {
            cb viewableAd = this.k.getViewableAd();
            if (200 == this.q) {
                if (PlacementType.PLACEMENT_TYPE_FULLSCREEN == this.k.getRenderingProperties().f2060a) {
                    try {
                        viewableAd.a(this.m, this.n);
                    } catch (Exception e2) {
                        new StringBuilder("SDK encountered unexpected error in enabling impression tracking on this ad: ").append(e2.getMessage());
                        if (this.k.getFullScreenEventsListener() != null) {
                            this.k.getFullScreenEventsListener().a();
                        }
                    }
                }
            } else if (201 == this.q) {
                try {
                    c cVar = new c();
                    b.a().a((com.inmobi.commons.core.configs.a) cVar, (b.c) null);
                    if (viewableAd.b() != null) {
                        if (this.k instanceof bd) {
                            be beVar = (be) this.o.getTag();
                            if (beVar != null) {
                                l lVar = cVar.k;
                                int i2 = lVar.g;
                                if (beVar.G.containsKey(LocationConst.TIME)) {
                                    i2 = ((Integer) beVar.G.get(LocationConst.TIME)).intValue();
                                }
                                lVar.g = i2;
                                viewableAd.a(new View[0]);
                            }
                        } else if (this.k instanceof ah) {
                            try {
                                viewableAd.a(new View[0]);
                            } catch (Exception e3) {
                                new StringBuilder("SDK encountered unexpected error in enabling impression tracking on this ad: ").append(e3.getMessage());
                                if (this.k.getFullScreenEventsListener() != null) {
                                    this.k.getFullScreenEventsListener().a();
                                }
                            }
                        }
                    }
                } catch (Exception e4) {
                    new StringBuilder("SDK encountered unexpected error in enabling impression tracking on this ad: ").append(e4.getMessage());
                    if (this.k.getFullScreenEventsListener() != null) {
                        this.k.getFullScreenEventsListener().a();
                    }
                    com.inmobi.commons.core.a.a.a().a(new com.inmobi.commons.core.e.a(e4));
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    @TargetApi(23)
    public void onCreate(Bundle bundle) {
        c cVar;
        super.onCreate(bundle);
        if (!com.inmobi.commons.a.a.a()) {
            finish();
            Logger.a(InternalLogLevel.DEBUG, "InMobi", "Session not found, AdActivity will be closed");
            return;
        }
        this.r = false;
        this.p = getIntent().getIntExtra("com.inmobi.rendering.InMobiAdActivity.EXTRA_AD_ACTIVITY_TYPE", 102);
        if (this.p == 100) {
            String stringExtra = getIntent().getStringExtra("com.inmobi.rendering.InMobiAdActivity.IN_APP_BROWSER_URL");
            long longExtra = getIntent().getLongExtra("placementId", Long.MIN_VALUE);
            boolean booleanExtra = getIntent().getBooleanExtra("allowAutoRedirection", false);
            String stringExtra2 = getIntent().getStringExtra("impressionId");
            String stringExtra3 = getIntent().getStringExtra(RequestParameters.CREATIVE_ID);
            this.l = new RenderView(this, new RenderingProperties(PlacementType.PLACEMENT_TYPE_FULLSCREEN), null, stringExtra2);
            this.l.setPlacementId(longExtra);
            this.l.setCreativeId(stringExtra3);
            this.l.setAllowAutoRedirection(booleanExtra);
            com.inmobi.rendering.RenderView.a aVar = RenderView.f2411a;
            if (i != null) {
                aVar = i.getListener();
                cVar = i.getAdConfig();
            } else {
                cVar = new c();
                if (j != null) {
                    aVar = j;
                }
            }
            this.l.setIsInAppBrowser(true);
            this.l.a(aVar, cVar);
            RelativeLayout relativeLayout = new RelativeLayout(this);
            LayoutParams layoutParams = new LayoutParams(-1, -1);
            layoutParams.addRule(10);
            layoutParams.addRule(2, 65533);
            relativeLayout.setBackgroundColor(-1);
            relativeLayout.addView(this.l, layoutParams);
            float f2 = com.inmobi.commons.core.utilities.b.c.a().c;
            LinearLayout linearLayout = new LinearLayout(this);
            LayoutParams layoutParams2 = new LayoutParams(-1, (int) (48.0f * f2));
            linearLayout.setOrientation(0);
            linearLayout.setId(65533);
            linearLayout.setWeightSum(100.0f);
            linearLayout.setBackgroundResource(17301658);
            linearLayout.setBackgroundColor(-7829368);
            layoutParams2.addRule(12);
            relativeLayout.addView(linearLayout, layoutParams2);
            LinearLayout.LayoutParams layoutParams3 = new LinearLayout.LayoutParams(-1, -1);
            layoutParams3.weight = 25.0f;
            CustomView customView = new CustomView(this, f2, 2);
            customView.setOnTouchListener(new OnTouchListener() {
                public final boolean onTouch(View view, MotionEvent motionEvent) {
                    if (motionEvent.getAction() == 1) {
                        view.setBackgroundColor(-7829368);
                        InMobiAdActivity.this.f2403a = true;
                        InMobiAdActivity.this.finish();
                        return true;
                    } else if (motionEvent.getAction() != 0) {
                        return true;
                    } else {
                        view.setBackgroundColor(-16711681);
                        return true;
                    }
                }
            });
            linearLayout.addView(customView, layoutParams3);
            CustomView customView2 = new CustomView(this, f2, 3);
            customView2.setOnTouchListener(new OnTouchListener() {
                public final boolean onTouch(View view, MotionEvent motionEvent) {
                    if (motionEvent.getAction() == 1) {
                        view.setBackgroundColor(-7829368);
                        InMobiAdActivity.this.l.reload();
                        return true;
                    } else if (motionEvent.getAction() != 0) {
                        return true;
                    } else {
                        view.setBackgroundColor(-16711681);
                        return true;
                    }
                }
            });
            linearLayout.addView(customView2, layoutParams3);
            CustomView customView3 = new CustomView(this, f2, 4);
            customView3.setOnTouchListener(new OnTouchListener() {
                public final boolean onTouch(View view, MotionEvent motionEvent) {
                    if (motionEvent.getAction() == 1) {
                        view.setBackgroundColor(-7829368);
                        if (InMobiAdActivity.this.l.canGoBack()) {
                            InMobiAdActivity.this.l.goBack();
                        } else {
                            InMobiAdActivity.this.f2403a = true;
                            InMobiAdActivity.this.finish();
                        }
                        return true;
                    } else if (motionEvent.getAction() != 0) {
                        return true;
                    } else {
                        view.setBackgroundColor(-16711681);
                        return true;
                    }
                }
            });
            linearLayout.addView(customView3, layoutParams3);
            CustomView customView4 = new CustomView(this, f2, 6);
            customView4.setOnTouchListener(new OnTouchListener() {
                public final boolean onTouch(View view, MotionEvent motionEvent) {
                    if (motionEvent.getAction() == 1) {
                        view.setBackgroundColor(-7829368);
                        if (InMobiAdActivity.this.l.canGoForward()) {
                            InMobiAdActivity.this.l.goForward();
                        }
                        return true;
                    } else if (motionEvent.getAction() != 0) {
                        return true;
                    } else {
                        view.setBackgroundColor(-16711681);
                        return true;
                    }
                }
            });
            linearLayout.addView(customView4, layoutParams3);
            setContentView(relativeLayout);
            this.l.loadUrl(stringExtra);
            this.l.setFullScreenActivityContext(this);
            return;
        }
        if (this.p == 102) {
            if (getIntent().hasExtra("com.inmobi.rendering.InMobiAdActivity.EXTRA_AD_CONTAINER_INDEX")) {
                this.k = (AdContainer) h.get(getIntent().getIntExtra("com.inmobi.rendering.InMobiAdActivity.EXTRA_AD_CONTAINER_INDEX", -1));
                if (this.k == null) {
                    finish();
                    return;
                }
                this.q = getIntent().getIntExtra("com.inmobi.rendering.InMobiAdActivity.EXTRA_AD_CONTAINER_TYPE", 0);
                if (this.q == 0) {
                    if (this.k.getFullScreenEventsListener() != null) {
                        this.k.getFullScreenEventsListener().a();
                    }
                    finish();
                    return;
                }
                if (getIntent().getBooleanExtra("com.inmobi.rendering.InMobiAdActivity.EXTRA_AD_ACTIVITY_IS_FULL_SCREEN", false)) {
                    requestWindowFeature(1);
                    getWindow().setFlags(1024, 1024);
                }
                if ((200 != this.q || String.HTML.equals(this.k.getMarkupType())) && (201 != this.q || "inmobiJson".equals(this.k.getMarkupType()))) {
                    try {
                        this.k.setFullScreenActivityContext(this);
                        FrameLayout frameLayout = (FrameLayout) findViewById(16908290);
                        RelativeLayout relativeLayout2 = new RelativeLayout(getApplicationContext());
                        relativeLayout2.setId(65534);
                        float f3 = com.inmobi.commons.core.utilities.b.c.a().c;
                        if (String.HTML.equals(this.k.getMarkupType())) {
                            relativeLayout2.setBackgroundColor(0);
                            LayoutParams layoutParams4 = new LayoutParams(-1, -1);
                            layoutParams4.addRule(10);
                            int i2 = (int) (50.0f * f3);
                            LayoutParams layoutParams5 = new LayoutParams(i2, i2);
                            layoutParams5.addRule(11);
                            this.m = new CustomView(this, f3, 0);
                            this.m.setId(65532);
                            this.m.setOnClickListener(new OnClickListener() {
                                public final void onClick(View view) {
                                    InMobiAdActivity.this.f2403a = true;
                                    try {
                                        InMobiAdActivity.this.k.b();
                                    } catch (Exception e) {
                                        InMobiAdActivity.g;
                                        new StringBuilder("Encountered unexpected error in processing close request: ").append(e.getMessage());
                                        Logger.a(InternalLogLevel.DEBUG, "InMobi", "SDK encountered unexpected error in processing close request");
                                    }
                                }
                            });
                            this.n = new CustomView(this, f3, 1);
                            this.n.setId(65531);
                            this.n.setOnClickListener(new OnClickListener() {
                                public final void onClick(View view) {
                                    InMobiAdActivity.this.f2403a = true;
                                    try {
                                        InMobiAdActivity.this.k.b();
                                    } catch (Exception e) {
                                        InMobiAdActivity.g;
                                        new StringBuilder("Encountered unexpected error in processing close request: ").append(e.getMessage());
                                        Logger.a(InternalLogLevel.DEBUG, "InMobi", "SDK encountered unexpected error in processing close request");
                                    }
                                }
                            });
                            View a2 = this.k.getViewableAd().a();
                            if (a2 != null) {
                                ViewGroup viewGroup = (ViewGroup) a2.getParent();
                                if (viewGroup != null) {
                                    viewGroup.removeView(a2);
                                }
                                relativeLayout2.addView(a2, layoutParams4);
                                relativeLayout2.addView(this.m, layoutParams5);
                                relativeLayout2.addView(this.n, layoutParams5);
                                ((RenderView) this.k).a(((RenderView) this.k).o);
                                ((RenderView) this.k).b(((RenderView) this.k).l);
                            }
                        } else if ("inmobiJson".equals(this.k.getMarkupType())) {
                            PlacementType placementType = this.k.getRenderingProperties().f2060a;
                            relativeLayout2.setBackgroundColor(ViewCompat.MEASURED_STATE_MASK);
                            ao aoVar = (ao) this.k.getDataModel();
                            Point point = aoVar.d.c.f2152a;
                            b.a().a((com.inmobi.commons.core.configs.a) new c(), (b.c) null);
                            cb viewableAd = this.k.getViewableAd();
                            View b2 = aoVar.c ? viewableAd.b() : null;
                            if (b2 == null) {
                                b2 = viewableAd.a(null, relativeLayout2, false);
                            }
                            if (this.k instanceof bd) {
                                NativeVideoWrapper nativeVideoWrapper = (NativeVideoWrapper) this.k.getVideoContainerView();
                                if (nativeVideoWrapper != null) {
                                    this.o = nativeVideoWrapper.getVideoView();
                                    this.o.requestFocus();
                                    be beVar = (be) this.o.getTag();
                                    if (beVar.y != null) {
                                        beVar.a((be) beVar.y);
                                    }
                                    if (PlacementType.PLACEMENT_TYPE_INLINE == placementType) {
                                        beVar.v.put("placementType", PlacementType.PLACEMENT_TYPE_INLINE);
                                    } else {
                                        beVar.v.put("placementType", PlacementType.PLACEMENT_TYPE_FULLSCREEN);
                                    }
                                }
                            }
                            if (b2 != null) {
                                relativeLayout2.addView(b2, new LayoutParams(point.x, point.y));
                            }
                            this.k.setRequestedScreenOrientation();
                        } else {
                            if (this.k.getFullScreenEventsListener() != null) {
                                this.k.getFullScreenEventsListener().a();
                            }
                            finish();
                            return;
                        }
                        frameLayout.addView(relativeLayout2, new LayoutParams(-1, -1));
                    } catch (Exception e2) {
                        this.k.setFullScreenActivityContext(null);
                        if (this.k.getFullScreenEventsListener() != null) {
                            this.k.getFullScreenEventsListener().a();
                        }
                        finish();
                        com.inmobi.commons.core.a.a.a().a(new com.inmobi.commons.core.e.a(e2));
                    }
                } else {
                    if (this.k.getFullScreenEventsListener() != null) {
                        this.k.getFullScreenEventsListener().a();
                    }
                    finish();
                }
            }
        } else if (this.p == 103) {
            int intExtra = getIntent().getIntExtra("id", -1);
            if (intExtra != -1) {
                startActivityForResult((Intent) c.get(Integer.valueOf(intExtra)), intExtra);
            }
        } else if (this.p == 104) {
            int intExtra2 = getIntent().getIntExtra("id", -1);
            if (intExtra2 != -1) {
                String[] stringArrayExtra = getIntent().getStringArrayExtra(NativeProtocol.RESULT_ARGS_PERMISSIONS);
                if (stringArrayExtra != null && stringArrayExtra.length > 0 && VERSION.SDK_INT >= 23) {
                    com.inmobi.commons.core.utilities.a.b();
                    requestPermissions(stringArrayExtra, intExtra2);
                }
            }
        }
    }

    public void onStop() {
        super.onStop();
        if (!this.f2403a) {
            this.s = true;
            if (this.o != null) {
                this.o.pause();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (this.f2403a) {
            if (100 == this.p) {
                if (!(this.l == null || this.l.getFullScreenEventsListener() == null)) {
                    try {
                        this.l.getFullScreenEventsListener().b(this.l);
                        this.l.destroy();
                        this.l = null;
                    } catch (Exception unused) {
                    }
                }
            } else if (!(102 != this.p || this.k == null || this.k.getFullScreenEventsListener() == null)) {
                if (200 == this.q) {
                    try {
                        this.k.getFullScreenEventsListener().b(null);
                    } catch (Exception e2) {
                        new StringBuilder("Encountered unexpected error in onAdScreenDismissed handler: ").append(e2.getMessage());
                        Logger.a(InternalLogLevel.DEBUG, "InMobi", "SDK encountered unexpected error while finishing fullscreen view");
                    }
                } else if (201 == this.q && VERSION.SDK_INT >= 15) {
                    if (this.k instanceof bd) {
                        NativeVideoWrapper nativeVideoWrapper = (NativeVideoWrapper) ((bd) this.k).getVideoContainerView();
                        if (nativeVideoWrapper != null) {
                            try {
                                this.k.getFullScreenEventsListener().b((be) nativeVideoWrapper.getVideoView().getTag());
                            } catch (Exception e3) {
                                new StringBuilder("Encountered unexpected error in onAdScreenDismissed handler: ").append(e3.getMessage());
                                Logger.a(InternalLogLevel.DEBUG, "InMobi", "SDK encountered unexpected error while finishing fullscreen view");
                                com.inmobi.commons.core.a.a.a().a(new com.inmobi.commons.core.e.a(e3));
                            }
                        }
                    } else if (this.k instanceof ah) {
                        try {
                            this.k.getFullScreenEventsListener().b(null);
                        } catch (Exception e4) {
                            new StringBuilder("Encountered unexpected error in onAdScreenDismissed handler: ").append(e4.getMessage());
                            Logger.a(InternalLogLevel.DEBUG, "InMobi", "SDK encountered unexpected error while finishing fullscreen view");
                            com.inmobi.commons.core.a.a.a().a(new com.inmobi.commons.core.e.a(e4));
                        }
                    }
                }
            }
            if (this.k != null) {
                this.k.destroy();
                this.k = null;
            }
        } else if (!(100 == this.p || 102 != this.p || this.k == null)) {
            if (200 == this.q) {
                RenderView renderView = (RenderView) this.k;
                renderView.setFullScreenActivityContext(null);
                try {
                    renderView.b();
                } catch (Exception e5) {
                    new StringBuilder("Encountered unexpected error in processing close request: ").append(e5.getMessage());
                    Logger.a(InternalLogLevel.DEBUG, "InMobi", "SDK encountered unexpected error in processing close request");
                }
            } else if (201 == this.q && VERSION.SDK_INT >= 15) {
                if (this.k instanceof bd) {
                    bd bdVar = (bd) this.k;
                    if (this.o != null) {
                        be beVar = (be) this.o.getTag();
                        if (beVar != null) {
                            if (PlacementType.PLACEMENT_TYPE_FULLSCREEN == bdVar.b.f2060a) {
                                this.o.a();
                            }
                            if (this.k.getFullScreenEventsListener() != null) {
                                try {
                                    this.k.getFullScreenEventsListener().b(beVar);
                                } catch (Exception e6) {
                                    new StringBuilder("Encountered unexpected error in onAdScreenDismissed handler: ").append(e6.getMessage());
                                    Logger.a(InternalLogLevel.DEBUG, "InMobi", "SDK encountered unexpected error while finishing fullscreen view");
                                    com.inmobi.commons.core.a.a.a().a(new com.inmobi.commons.core.e.a(e6));
                                }
                            }
                        }
                    }
                } else if ((this.k instanceof ah) && this.k.getFullScreenEventsListener() != null) {
                    try {
                        this.k.getFullScreenEventsListener().b(null);
                    } catch (Exception e7) {
                        new StringBuilder("Encountered unexpected error in onAdScreenDismissed handler: ").append(e7.getMessage());
                        Logger.a(InternalLogLevel.DEBUG, "InMobi", "SDK encountered unexpected error while finishing fullscreen view");
                        com.inmobi.commons.core.a.a.a().a(new com.inmobi.commons.core.e.a(e7));
                    }
                }
            }
            a((Object) this.k);
            this.k.destroy();
            this.k = null;
        }
        super.onDestroy();
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        if (this.l != null) {
            RenderView renderView = this.l;
            if ("Resized".equals(renderView.d) && renderView.getResizeProperties() != null) {
                renderView.g.a();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        if (((a) b.remove(Integer.valueOf(i2))) != null) {
            c.remove(Integer.valueOf(i2));
            this.f2403a = true;
            finish();
        }
    }

    public void onMultiWindowModeChanged(boolean z) {
        super.onMultiWindowModeChanged(z);
        if (!z) {
            if (this.l != null) {
                this.l.setOrientationProperties(this.l.getOrientationProperties());
            }
            if (this.k != null) {
                this.k.setRequestedScreenOrientation();
            }
        }
    }

    public void onMultiWindowModeChanged(boolean z, Configuration configuration) {
        super.onMultiWindowModeChanged(z, configuration);
        onMultiWindowModeChanged(z);
    }

    public void onRequestPermissionsResult(int i2, @NonNull String[] strArr, @NonNull int[] iArr) {
        super.onRequestPermissionsResult(i2, strArr, iArr);
        com.inmobi.commons.core.utilities.a.c();
        e.remove(Integer.valueOf(i2));
        finish();
    }

    public void onBackPressed() {
        if (this.p == 102) {
            if (this.k != null && !this.k.c()) {
                boolean z = false;
                if (200 == this.q) {
                    RenderView renderView = (RenderView) this.k;
                    if (renderView != null) {
                        if (renderView.q != null) {
                            z = true;
                        }
                        if (z) {
                            renderView.a(renderView.q, "broadcastEvent('backButtonPressed')");
                        }
                        if (!renderView.p) {
                            this.f2403a = true;
                            try {
                                renderView.b();
                            } catch (Exception e2) {
                                new StringBuilder("Encountered unexpected error in processing close request: ").append(e2.getMessage());
                                Logger.a(InternalLogLevel.DEBUG, "InMobi", "SDK encountered unexpected error in processing close request");
                            }
                        }
                    }
                } else if (this.k instanceof bd) {
                    bd bdVar = (bd) this.k;
                    if (bdVar != null && !bdVar.h().b) {
                        this.f2403a = true;
                        if (this.o != null) {
                            be beVar = (be) this.o.getTag();
                            if (beVar != null) {
                                if (PlacementType.PLACEMENT_TYPE_FULLSCREEN == bdVar.b.f2060a) {
                                    this.o.a();
                                }
                                try {
                                    if (((Boolean) beVar.v.get("isFullScreen")).booleanValue()) {
                                        beVar.v.put("seekPosition", Integer.valueOf(this.o.getCurrentPosition()));
                                        if (!bdVar.l && ((Boolean) beVar.v.get("didRequestFullScreen")).booleanValue()) {
                                            beVar.v.put("didRequestFullScreen", Boolean.valueOf(false));
                                            if (beVar.y != null) {
                                                beVar.y.v.put("didRequestFullScreen", Boolean.valueOf(false));
                                            }
                                            bdVar.b();
                                            beVar.v.put("isFullScreen", Boolean.valueOf(false));
                                        }
                                    }
                                    return;
                                } catch (Exception e3) {
                                    new StringBuilder("Encountered unexpected error in onVideoClosed handler: ").append(e3.getMessage());
                                    Logger.a(InternalLogLevel.DEBUG, "InMobi", "SDK encountered unexpected error in closing video");
                                    com.inmobi.commons.core.a.a.a().a(new com.inmobi.commons.core.e.a(e3));
                                }
                            }
                            return;
                        }
                        finish();
                    }
                } else if (this.k instanceof ah) {
                    ah ahVar = (ah) this.k;
                    if (ahVar == null) {
                        finish();
                    } else if (!ahVar.h().b) {
                        ahVar.b();
                    }
                }
            }
        } else if (this.p == 100) {
            this.f2403a = true;
            finish();
        }
    }
}
