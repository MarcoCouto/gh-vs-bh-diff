package com.inmobi.ads;

import android.app.Activity;
import android.app.Application.ActivityLifecycleCallbacks;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.UiThread;
import android.view.View;
import com.inmobi.ads.AdContainer.RenderingProperties.PlacementType;
import com.inmobi.ads.InMobiAdRequestStatus.StatusCode;
import com.inmobi.ads.d.a;
import com.inmobi.ads.i.b;
import com.inmobi.commons.core.utilities.Logger;
import com.inmobi.commons.core.utilities.Logger.InternalLogLevel;
import com.inmobi.rendering.RenderView;
import java.util.Map;

/* compiled from: BannerAdUnit */
public class p extends i implements ActivityLifecycleCallbacks {
    private static final String C = "p";
    private static final String D = InMobiBanner.class.getSimpleName();
    boolean A = false;
    public String B;
    private boolean E;
    private int F = 0;
    public boolean z = false;

    public final String b() {
        return "banner";
    }

    /* access modifiers changed from: protected */
    public final void b(a aVar) {
    }

    public void onActivityCreated(Activity activity, Bundle bundle) {
    }

    public void onActivityPaused(Activity activity) {
    }

    public void onActivityResumed(Activity activity) {
    }

    public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
    }

    @NonNull
    public static p a(@NonNull Context context, bj bjVar, b bVar, int i) {
        i iVar = (i) a.f2236a.get(bjVar);
        p pVar = iVar instanceof p ? (p) iVar : null;
        if (pVar == null || 1 != i) {
            if (pVar == null) {
                new StringBuilder("Creating new adUnit for placement-ID : ").append(bjVar.f2198a);
                pVar = new p(context, bjVar.f2198a, bVar);
                if (i != 0) {
                    a.f2236a.put(bjVar, pVar);
                }
            } else {
                new StringBuilder("Found pre-fetching adUnit for placement-ID : ").append(bjVar.f2198a);
                super.a(context);
                a.f2236a.remove(bjVar);
                pVar.E = true;
            }
            pVar.a(bVar);
            pVar.a(bjVar.f);
            return pVar;
        }
        throw new IllegalStateException("There's already a pre-loading going on for the same placementID");
    }

    public static p a(Context context, bj bjVar, b bVar) {
        return new p(context, bjVar.f2198a, bVar);
    }

    private p(Context context, long j, b bVar) {
        super(context, j, bVar);
    }

    public final void a(Context context) {
        super.a(context);
    }

    /* access modifiers changed from: 0000 */
    public final void M() {
        RenderView renderView = (RenderView) j();
        if (renderView != null) {
            this.A = true;
            renderView.a();
        }
    }

    public final void c(boolean z2) {
        if (z2) {
            InternalLogLevel internalLogLevel = InternalLogLevel.DEBUG;
            String str = D;
            StringBuilder sb = new StringBuilder("Initiating Banner refresh for placement id: ");
            sb.append(this.d);
            Logger.a(internalLogLevel, str, sb.toString());
        }
        InternalLogLevel internalLogLevel2 = InternalLogLevel.DEBUG;
        String str2 = D;
        StringBuilder sb2 = new StringBuilder("Fetching a Banner ad for placement id: ");
        sb2.append(this.d);
        Logger.a(internalLogLevel2, str2, sb2.toString());
        this.w = false;
        this.z = z2;
        if (1 == this.f2257a) {
            Logger.a(InternalLogLevel.ERROR, C, "An ad load is already in progress. Please wait for the load to complete before requesting for another ad");
            if (!this.E) {
                a(new InMobiAdRequestStatus(StatusCode.REQUEST_PENDING), false);
            }
        } else if (2 == this.f2257a || 8 == this.f2257a) {
            a(new InMobiAdRequestStatus(StatusCode.AD_ACTIVE), false);
            InternalLogLevel internalLogLevel3 = InternalLogLevel.ERROR;
            String str3 = D;
            StringBuilder sb3 = new StringBuilder("An ad is currently being viewed by the user. Please wait for the user to close the ad before requesting for another ad for placement id: ");
            sb3.append(this.d);
            Logger.a(internalLogLevel3, str3, sb3.toString());
        } else {
            super.n();
        }
    }

    public final void q() {
        super.q();
    }

    /* access modifiers changed from: protected */
    @Nullable
    public final RenderView k() {
        RenderView k = super.k();
        if (this.A && k != null) {
            k.a();
        }
        return k;
    }

    public final String c() {
        return this.B;
    }

    /* access modifiers changed from: protected */
    public final PlacementType d() {
        return PlacementType.PLACEMENT_TYPE_INLINE;
    }

    /* access modifiers changed from: protected */
    @NonNull
    public final Map<String, String> e() {
        Map<String, String> e = super.e();
        e.put("u-rt", this.z ? "1" : "0");
        e.put("mk-ad-slot", this.B);
        return e;
    }

    @UiThread
    public final void c(long j, @NonNull a aVar) {
        try {
            super.c(j, aVar);
            InternalLogLevel internalLogLevel = InternalLogLevel.DEBUG;
            String str = D;
            StringBuilder sb = new StringBuilder("Banner ad fetch successful for placement id: ");
            sb.append(this.d);
            Logger.a(internalLogLevel, str, sb.toString());
            if (j == this.d && this.f2257a == 2) {
                a(false, k());
                try {
                    InternalLogLevel internalLogLevel2 = InternalLogLevel.DEBUG;
                    String str2 = D;
                    StringBuilder sb2 = new StringBuilder("Started loading banner ad markup in WebView for placement id: ");
                    sb2.append(this.d);
                    Logger.a(internalLogLevel2, str2, sb2.toString());
                    a(null, this.h, null, null);
                } catch (Exception e) {
                    A();
                    if (f() != null) {
                        f().a(new InMobiAdRequestStatus(StatusCode.INTERNAL_ERROR));
                    }
                    Logger.a(InternalLogLevel.ERROR, D, "Unable to load ad; SDK encountered an internal error");
                    new StringBuilder("Loading ad markup into container encountered an unexpected error: ").append(e.getMessage());
                }
            }
        } catch (Exception e2) {
            Logger.a(InternalLogLevel.ERROR, D, "Unable to load ad; SDK encountered an internal error");
            new StringBuilder("Handling ad fetch successful encountered an unexpected error: ").append(e2.getMessage());
        }
    }

    public final void a(RenderView renderView) {
        try {
            super.a(renderView);
            if (this.f2257a == 2) {
                A();
                this.f2257a = 4;
                D();
                InternalLogLevel internalLogLevel = InternalLogLevel.DEBUG;
                String str = D;
                StringBuilder sb = new StringBuilder("Successfully loaded Banner ad markup in the WebView for placement id: ");
                sb.append(this.d);
                Logger.a(internalLogLevel, str, sb.toString());
                if (f() != null) {
                    f().a();
                }
                s();
            }
        } catch (Exception e) {
            Logger.a(InternalLogLevel.ERROR, D, "Unable to load ad; SDK encountered an internal error");
            new StringBuilder("Loading ad markup into container encountered an unexpected error: ").append(e.getMessage());
        }
    }

    public final void b(RenderView renderView) {
        try {
            super.b(renderView);
            if (this.f2257a == 4) {
                this.f2257a = 7;
                d("AdRendered");
            }
        } catch (Exception e) {
            Logger.a(InternalLogLevel.ERROR, D, "Unable to load ad; SDK encountered an internal error");
            new StringBuilder("BannerAdUnit.onRenderViewVisible threw unexpected error: ").append(e.getMessage());
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0044, code lost:
        return;
     */
    public final synchronized void c(RenderView renderView) {
        try {
            super.c(renderView);
            if (this.f2257a == 7) {
                this.F++;
                this.f2257a = 8;
                InternalLogLevel internalLogLevel = InternalLogLevel.DEBUG;
                String str = D;
                StringBuilder sb = new StringBuilder("Successfully displayed banner ad for placement Id : ");
                sb.append(this.d);
                Logger.a(internalLogLevel, str, sb.toString());
                if (f() != null) {
                    f().d();
                }
            } else if (this.f2257a == 8) {
                this.F++;
            }
        } catch (Exception e) {
            Logger.a(InternalLogLevel.ERROR, D, "Unable to display ad; SDK encountered an internal error");
            new StringBuilder("BannerAdUnit.onAdScreenDisplayed threw unexpected error: ").append(e.getMessage());
        }
    }

    public final synchronized void d(RenderView renderView) {
        try {
            super.d(renderView);
            if (this.f2257a == 8) {
                int i = this.F - 1;
                this.F = i;
                if (i == 0) {
                    this.f2257a = 7;
                    if (f() != null) {
                        f().e();
                    }
                }
            }
        } catch (Exception e) {
            Logger.a(InternalLogLevel.ERROR, D, "Unable to dismiss ad; SDK encountered an internal error");
            new StringBuilder("BannerAdUnit.onAdScreenDismissed threw unexpected error: ").append(e.getMessage());
        }
    }

    public final void Q() {
        if (a() instanceof Activity) {
            ((Activity) a()).getApplication().unregisterActivityLifecycleCallbacks(this);
        }
    }

    public void onActivityStarted(Activity activity) {
        Context a2 = a();
        if (a2 != null && a2.equals(activity)) {
            P();
        }
    }

    public void onActivityStopped(Activity activity) {
        Context a2 = a();
        if (a2 != null && a2.equals(activity)) {
            O();
        }
    }

    public void onActivityDestroyed(Activity activity) {
        Context a2 = a();
        if (a2 != null && a2.equals(activity)) {
            ((Activity) a2).getApplication().unregisterActivityLifecycleCallbacks(this);
            t();
        }
    }

    /* access modifiers changed from: protected */
    public final int r() {
        if (1 == this.f2257a || 2 == this.f2257a) {
            this.s.post(new Runnable() {
                public final void run() {
                    p.this.a(new InMobiAdRequestStatus(StatusCode.REQUEST_PENDING), false);
                }
            });
            InternalLogLevel internalLogLevel = InternalLogLevel.ERROR;
            String str = D;
            StringBuilder sb = new StringBuilder("An ad load is already in progress. Please wait for the load to complete before requesting for another ad for placement id: ");
            sb.append(this.d);
            Logger.a(internalLogLevel, str, sb.toString());
            return 2;
        } else if (this.f2257a != 8) {
            return super.r();
        } else {
            this.s.post(new Runnable() {
                public final void run() {
                    p.this.a(new InMobiAdRequestStatus(StatusCode.AD_ACTIVE), false);
                }
            });
            InternalLogLevel internalLogLevel2 = InternalLogLevel.ERROR;
            String str2 = D;
            StringBuilder sb2 = new StringBuilder("An ad is currently being viewed by the user. Please wait for the user to close the ad before requesting for another ad for placement id: ");
            sb2.append(this.d);
            Logger.a(internalLogLevel2, str2, sb2.toString());
            return 3;
        }
    }

    public final void F() {
        if (1 == this.f2257a) {
            this.f2257a = 9;
            if (!this.n) {
                this.E = false;
                c(false);
            } else if (this.q != null) {
                this.q.a(this);
            }
        }
    }

    public final void b(InMobiAdRequestStatus inMobiAdRequestStatus) {
        if (1 == this.f2257a) {
            this.f2257a = 3;
            if (!this.n) {
                b f = f();
                if (f != null) {
                    this.E = false;
                    a(f, "VAR", "");
                    a(f, "ARN", "");
                    f.a(inMobiAdRequestStatus);
                    return;
                }
                if (this.q != null) {
                    this.q.a(this, inMobiAdRequestStatus);
                }
            } else if (this.q != null) {
                this.q.a(this, inMobiAdRequestStatus);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public final boolean N() {
        return this.f2257a == 8;
    }

    public final void O() {
        int i = this.f2257a;
        if (i == 4 || i == 7 || i == 8) {
            AdContainer j = j();
            if (j != null) {
                cb viewableAd = j.getViewableAd();
                if (viewableAd != null) {
                    viewableAd.d();
                }
            }
        }
    }

    public final void P() {
        int i = this.f2257a;
        if (i == 4 || i == 7 || i == 8) {
            AdContainer j = j();
            if (j != null) {
                cb viewableAd = j.getViewableAd();
                if (viewableAd != null) {
                    viewableAd.a(new View[0]);
                }
            }
        }
    }
}
