package com.inmobi.ads;

import java.util.List;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/* compiled from: IasVastHelper */
class x {

    /* renamed from: a reason: collision with root package name */
    private static final String f2306a = "x";

    x() {
    }

    static void a(Document document, by byVar) {
        List<Node> a2 = bv.a(document, "AVID");
        if (a2 != null) {
            for (Node a3 : a2) {
                a(a3, byVar);
            }
        }
    }

    private static void a(Node node, by byVar) {
        while (true) {
            if (!node.hasChildNodes()) {
                break;
            }
            String nodeName = node.getNodeName();
            char c = 65535;
            int hashCode = nodeName.hashCode();
            if (hashCode != -2077435339) {
                if (hashCode != -1320080837) {
                    if (hashCode != 2021392) {
                        if (hashCode == 1561251035 && nodeName.equals("JavaScriptResource")) {
                            c = 3;
                        }
                    } else if (nodeName.equals("AVID")) {
                        c = 0;
                    }
                } else if (nodeName.equals("Verification")) {
                    c = 2;
                }
            } else if (nodeName.equals("AdVerifications")) {
                c = 1;
            }
            switch (c) {
                case 0:
                    node = bv.a(node, "AdVerifications");
                    if (node == null) {
                        break;
                    } else {
                        continue;
                    }
                case 1:
                    if (node.hasChildNodes()) {
                        NodeList childNodes = node.getChildNodes();
                        int length = childNodes.getLength();
                        for (int i = 0; i < length; i++) {
                            a(childNodes.item(i), byVar);
                        }
                        return;
                    }
                    break;
                case 2:
                    node = bv.a(node, "JavaScriptResource");
                    if (node == null) {
                        break;
                    } else {
                        continue;
                    }
                case 3:
                    String a2 = bv.a(node);
                    if (a2 != null) {
                        byVar.a(new NativeTracker(a2, 0, TrackerEventType.TRACKER_EVENT_TYPE_IAS, null));
                        break;
                    }
                    break;
            }
        }
    }
}
