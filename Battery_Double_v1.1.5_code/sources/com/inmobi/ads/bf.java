package com.inmobi.ads;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.os.Build.VERSION;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils.TruncateAt;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.facebook.share.internal.ShareConstants;
import com.inmobi.ads.AdContainer.RenderingProperties;
import com.inmobi.ads.AdContainer.RenderingProperties.PlacementType;
import com.inmobi.rendering.CustomView;
import com.inmobi.rendering.InMobiAdActivity;
import com.inmobi.rendering.RenderView;
import com.squareup.picasso.Callback;
import com.startapp.android.publish.common.model.AdPreferences;
import java.lang.ref.WeakReference;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Stack;

/* compiled from: NativeViewFactory */
class bf {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public static final String f2181a = "bf";
    @NonNull
    private static final Map<Class, Integer> c;
    @Nullable
    private static volatile WeakReference<bf> e = null;
    /* access modifiers changed from: private */
    public static WeakReference<Context> f = null;
    private static int g = 1;
    private static int h = 1;
    /* access modifiers changed from: private */
    public int b;
    @NonNull
    private Map<Integer, d> d = new HashMap();

    /* compiled from: NativeViewFactory */
    private static final class a implements Runnable {

        /* renamed from: a reason: collision with root package name */
        private WeakReference<Context> f2194a;
        private WeakReference<ImageView> b;

        a(Context context, ImageView imageView) {
            this.f2194a = new WeakReference<>(context);
            this.b = new WeakReference<>(imageView);
        }

        public final void run() {
            Context context = (Context) this.f2194a.get();
            ImageView imageView = (ImageView) this.b.get();
            if (context != null && imageView != null) {
                bf.b(context, imageView);
            }
        }
    }

    @SuppressLint({"AppCompatCustomView"})
    /* compiled from: NativeViewFactory */
    private static final class b extends TextView {
        public final boolean onTouchEvent(MotionEvent motionEvent) {
            return false;
        }

        public b(Context context) {
            super(context);
        }

        /* access modifiers changed from: protected */
        public final void onSizeChanged(int i, int i2, int i3, int i4) {
            super.onSizeChanged(i, i2, i3, i4);
            int lineHeight = getLineHeight() > 0 ? i2 / getLineHeight() : 0;
            if (lineHeight > 0) {
                setSingleLine(false);
                setLines(lineHeight);
            }
            if (lineHeight == 1) {
                setSingleLine();
            }
        }
    }

    /* compiled from: NativeViewFactory */
    static class c implements InvocationHandler {

        /* renamed from: a reason: collision with root package name */
        private WeakReference<Context> f2195a;
        private WeakReference<ImageView> b;
        private ak c;

        c(Context context, ImageView imageView, ak akVar) {
            this.f2195a = new WeakReference<>(context);
            this.b = new WeakReference<>(imageView);
            this.c = akVar;
        }

        public final Object invoke(Object obj, Method method, Object[] objArr) {
            bf.f2181a;
            new StringBuilder("Method invoked in PicassoInvocationHandler: ").append(method);
            if (method != null && "onError".equalsIgnoreCase(method.getName())) {
                bf.a((Context) this.f2195a.get(), (ImageView) this.b.get(), this.c);
            }
            return null;
        }
    }

    /* compiled from: NativeViewFactory */
    private abstract class d {

        /* renamed from: a reason: collision with root package name */
        private int f2196a = 0;
        @NonNull
        LinkedList<View> b = new LinkedList<>();
        private int d = 0;

        /* access modifiers changed from: protected */
        public abstract View a(@NonNull Context context);

        public d() {
        }

        public boolean a(@NonNull View view) {
            bf.b(view);
            view.setOnClickListener(null);
            this.b.addLast(view);
            view.setScaleX(1.0f);
            view.setScaleY(1.0f);
            bf.this.b = bf.this.b + 1;
            return true;
        }

        public final View a(@NonNull Context context, ak akVar, c cVar) {
            View view;
            bf.f = new WeakReference(context);
            if (this.b.isEmpty()) {
                this.f2196a++;
                view = a(context);
            } else {
                this.d++;
                view = (View) this.b.removeFirst();
                bf.this.b = bf.this.b - 1;
            }
            a(view, akVar, cVar);
            return view;
        }

        public String toString() {
            StringBuilder sb = new StringBuilder("Size:");
            sb.append(this.b.size());
            sb.append(" Miss Count:");
            sb.append(this.f2196a);
            sb.append(" Hit Count:");
            sb.append(this.d);
            return sb.toString();
        }

        /* access modifiers changed from: protected */
        public void a(@NonNull View view, @NonNull ak akVar, @NonNull c cVar) {
            view.setVisibility(akVar.x);
            view.setOnClickListener(null);
        }
    }

    static {
        HashMap hashMap = new HashMap();
        c = hashMap;
        hashMap.put(aw.class, Integer.valueOf(0));
        c.put(bq.class, Integer.valueOf(1));
        c.put(bp.class, Integer.valueOf(2));
        c.put(NativeContainerLayout.class, Integer.valueOf(3));
        c.put(ImageView.class, Integer.valueOf(6));
        c.put(NativeVideoWrapper.class, Integer.valueOf(7));
        c.put(b.class, Integer.valueOf(4));
        c.put(Button.class, Integer.valueOf(5));
        c.put(NativeTimerView.class, Integer.valueOf(8));
        c.put(RenderView.class, Integer.valueOf(9));
        c.put(GifView.class, Integer.valueOf(10));
    }

    static void a(int i) {
        g = i;
    }

    static void b(int i) {
        h = i;
    }

    static int c(int i) {
        Context context = (Context) f.get();
        if ((context != null && (context instanceof InMobiAdActivity)) || g == 0) {
            return i;
        }
        double d2 = (double) i;
        double d3 = (double) g;
        Double.isNaN(d3);
        double d4 = d3 * 1.0d;
        double d5 = (double) h;
        Double.isNaN(d5);
        double d6 = d4 / d5;
        Double.isNaN(d2);
        return (int) (d2 * d6);
    }

    @SuppressLint({"UseSparseArrays"})
    private bf(Context context) {
        f = new WeakReference<>(context);
        this.d.put(Integer.valueOf(0), new d() {
            /* access modifiers changed from: protected */
            public final View a(@NonNull Context context) {
                return new aw(context.getApplicationContext());
            }

            /* access modifiers changed from: protected */
            public final void a(@NonNull View view, @NonNull ak akVar, @NonNull c cVar) {
                super.a(view, akVar, cVar);
                bf.a(view, akVar.c);
            }
        });
        this.d.put(Integer.valueOf(3), new d() {
            /* access modifiers changed from: protected */
            public final View a(@NonNull Context context) {
                return new NativeContainerLayout(context.getApplicationContext());
            }

            /* access modifiers changed from: protected */
            public final void a(@NonNull View view, @NonNull ak akVar, @NonNull c cVar) {
                super.a(view, akVar, cVar);
                bf.a(view, akVar.c);
            }
        });
        this.d.put(Integer.valueOf(1), new d() {
            /* access modifiers changed from: protected */
            public final View a(@NonNull Context context) {
                return new bq(context.getApplicationContext());
            }

            /* access modifiers changed from: protected */
            public final void a(@NonNull View view, @NonNull ak akVar, @NonNull c cVar) {
                super.a(view, akVar, cVar);
                bf.a(view, akVar.c);
            }

            public final boolean a(@NonNull View view) {
                ((bq) view).f2205a = null;
                return super.a(view);
            }
        });
        this.d.put(Integer.valueOf(2), new d() {
            /* access modifiers changed from: protected */
            public final View a(@NonNull Context context) {
                return new bp(context.getApplicationContext());
            }

            /* access modifiers changed from: protected */
            public final void a(@NonNull View view, @NonNull ak akVar, @NonNull c cVar) {
                super.a(view, akVar, cVar);
                bf.a(view, akVar.c);
            }
        });
        this.d.put(Integer.valueOf(6), new d() {
            /* access modifiers changed from: protected */
            public final View a(@NonNull Context context) {
                return new ImageView(context.getApplicationContext());
            }

            /* access modifiers changed from: protected */
            public final void a(@NonNull View view, @NonNull ak akVar, @NonNull c cVar) {
                super.a(view, akVar, cVar);
                bf.a((ImageView) view, akVar);
            }

            public final boolean a(@NonNull View view) {
                if (!(view instanceof ImageView)) {
                    return false;
                }
                ((ImageView) view).setImageDrawable(null);
                return super.a(view);
            }
        });
        this.d.put(Integer.valueOf(10), new d() {
            /* access modifiers changed from: protected */
            public final View a(@NonNull Context context) {
                return new GifView(context.getApplicationContext());
            }

            /* access modifiers changed from: protected */
            public final void a(@NonNull View view, @NonNull ak akVar, @NonNull c cVar) {
                super.a(view, akVar, cVar);
                bf.a((GifView) view, akVar);
            }

            public final boolean a(@NonNull View view) {
                if (!(view instanceof GifView)) {
                    return false;
                }
                ((GifView) view).setGif(null);
                return super.a(view);
            }
        });
        this.d.put(Integer.valueOf(7), new d() {
            /* access modifiers changed from: protected */
            public final View a(@NonNull Context context) {
                return new NativeVideoWrapper(context.getApplicationContext());
            }

            /* access modifiers changed from: protected */
            public final void a(@NonNull View view, @NonNull ak akVar, @NonNull c cVar) {
                super.a(view, akVar, cVar);
                bf.a((NativeVideoWrapper) view, akVar);
            }

            @TargetApi(15)
            public final boolean a(@NonNull View view) {
                if (!(view instanceof NativeVideoWrapper)) {
                    return false;
                }
                NativeVideoWrapper nativeVideoWrapper = (NativeVideoWrapper) view;
                nativeVideoWrapper.getProgressBar().setVisibility(8);
                nativeVideoWrapper.getPoster().setImageBitmap(null);
                nativeVideoWrapper.getVideoView().a();
                return super.a(view);
            }
        });
        this.d.put(Integer.valueOf(4), new d() {
            /* access modifiers changed from: protected */
            public final View a(@NonNull Context context) {
                return new b(context.getApplicationContext());
            }

            /* access modifiers changed from: protected */
            public final void a(@NonNull View view, @NonNull ak akVar, @NonNull c cVar) {
                super.a(view, akVar, cVar);
                bf.a((TextView) view, akVar);
            }

            public final boolean a(@NonNull View view) {
                if (!(view instanceof TextView)) {
                    return false;
                }
                bf.a((TextView) view);
                return super.a(view);
            }
        });
        this.d.put(Integer.valueOf(5), new d() {
            /* access modifiers changed from: protected */
            public final View a(@NonNull Context context) {
                return new Button(context.getApplicationContext());
            }

            /* access modifiers changed from: protected */
            public final void a(@NonNull View view, @NonNull ak akVar, @NonNull c cVar) {
                super.a(view, akVar, cVar);
                bf.b((Button) view, akVar);
            }

            public final boolean a(@NonNull View view) {
                if (!(view instanceof Button)) {
                    return false;
                }
                bf.a((TextView) (Button) view);
                return super.a(view);
            }
        });
        this.d.put(Integer.valueOf(8), new d() {
            /* access modifiers changed from: protected */
            public final View a(@NonNull Context context) {
                return new NativeTimerView(context.getApplicationContext());
            }

            /* access modifiers changed from: protected */
            public final void a(@NonNull View view, @NonNull ak akVar, @NonNull c cVar) {
                super.a(view, akVar, cVar);
                bf.a(bf.this, (NativeTimerView) view, akVar);
            }

            public final boolean a(@NonNull View view) {
                return (view instanceof NativeTimerView) && super.a(view);
            }
        });
        this.d.put(Integer.valueOf(9), new d() {
            /* access modifiers changed from: protected */
            public final View a(@NonNull Context context) {
                return new RenderView(context.getApplicationContext(), new RenderingProperties(PlacementType.PLACEMENT_TYPE_INLINE), null, null);
            }

            /* access modifiers changed from: protected */
            public final void a(@NonNull View view, @NonNull ak akVar, @NonNull c cVar) {
                super.a(view, akVar, cVar);
                bf.a((RenderView) view, akVar, cVar);
            }

            public final boolean a(@NonNull View view) {
                return (view instanceof RenderView) && !((RenderView) view).u && super.a(view);
            }
        });
    }

    public static bf a(Context context) {
        bf bfVar;
        bf bfVar2;
        bf bfVar3 = null;
        if (e == null) {
            bfVar = null;
        } else {
            bfVar = (bf) e.get();
        }
        if (bfVar == null) {
            synchronized (bf.class) {
                if (e != null) {
                    bfVar3 = (bf) e.get();
                }
                if (bfVar3 == null) {
                    bfVar2 = new bf(context);
                    e = new WeakReference<>(bfVar2);
                } else {
                    bfVar2 = bfVar3;
                }
            }
        }
        return bfVar;
    }

    public final void a(@NonNull View view) {
        if ((view instanceof aw) || (view instanceof NativeContainerLayout)) {
            NativeContainerLayout nativeContainerLayout = (NativeContainerLayout) view;
            if (nativeContainerLayout.getChildCount() != 0) {
                Stack stack = new Stack();
                stack.push(nativeContainerLayout);
                while (!stack.isEmpty()) {
                    NativeContainerLayout nativeContainerLayout2 = (NativeContainerLayout) stack.pop();
                    for (int childCount = nativeContainerLayout2.getChildCount() - 1; childCount >= 0; childCount--) {
                        View childAt = nativeContainerLayout2.getChildAt(childCount);
                        nativeContainerLayout2.removeViewAt(childCount);
                        if (childAt instanceof NativeContainerLayout) {
                            stack.push((NativeContainerLayout) childAt);
                        } else {
                            c(childAt);
                        }
                    }
                    c((View) nativeContainerLayout2);
                }
                return;
            }
        }
        c(view);
    }

    private void c(@NonNull View view) {
        int intValue = ((Integer) c.get(view.getClass())).intValue();
        if (-1 == intValue) {
            new StringBuilder("View type unknown, ignoring recycle:").append(view);
            return;
        }
        d dVar = (d) this.d.get(Integer.valueOf(intValue));
        if (dVar == null) {
            StringBuilder sb = new StringBuilder("Unsupported AssetType:");
            sb.append(intValue);
            sb.append(" failed to recycle view");
            return;
        }
        if (this.b >= 300) {
            d c2 = c();
            if (c2 != null && c2.b.size() > 0) {
                c2.b.removeFirst();
            }
        }
        dVar.a(view);
    }

    @Nullable
    private d c() {
        int i = 0;
        d dVar = null;
        for (Entry entry : this.d.entrySet()) {
            if (((d) entry.getValue()).b.size() > i) {
                d dVar2 = (d) entry.getValue();
                dVar = dVar2;
                i = dVar2.b.size();
            }
        }
        return dVar;
    }

    /* access modifiers changed from: private */
    public static void b(Context context, ImageView imageView) {
        Bitmap bitmap;
        if (imageView.getDrawable() == null) {
            float f2 = com.inmobi.commons.core.utilities.b.c.a().c;
            CustomView customView = new CustomView(context, f2, 0);
            if (VERSION.SDK_INT < 28) {
                customView.layout(0, 0, (int) (((float) c(40)) * f2), (int) (((float) c(40)) * f2));
                customView.setDrawingCacheEnabled(true);
                customView.buildDrawingCache();
                bitmap = customView.getDrawingCache();
            } else {
                customView.layout(0, 0, (int) (((float) c(40)) * f2), (int) (((float) c(40)) * f2));
                bitmap = Bitmap.createBitmap((int) (((float) c(40)) * f2), (int) (((float) c(40)) * f2), Config.ARGB_8888);
                customView.draw(new Canvas(bitmap));
            }
            imageView.setImageBitmap(bitmap);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:24:0x0054  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0057  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x005a  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x005d  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x005f A[SYNTHETIC] */
    private static void a(@NonNull TextView textView, String[] strArr) {
        char c2;
        int paintFlags = textView.getPaintFlags();
        int i = paintFlags;
        int i2 = 0;
        for (String str : strArr) {
            int hashCode = str.hashCode();
            if (hashCode != -1178781136) {
                if (hashCode != -1026963764) {
                    if (hashCode != -891985998) {
                        if (hashCode == 3029637 && str.equals(TtmlNode.BOLD)) {
                            c2 = 0;
                            switch (c2) {
                                case 0:
                                    i2 |= 1;
                                    break;
                                case 1:
                                    i2 |= 2;
                                    break;
                                case 2:
                                    i |= 16;
                                    break;
                                case 3:
                                    i |= 8;
                                    break;
                            }
                        }
                    } else if (str.equals("strike")) {
                        c2 = 2;
                        switch (c2) {
                            case 0:
                                break;
                            case 1:
                                break;
                            case 2:
                                break;
                            case 3:
                                break;
                        }
                    }
                } else if (str.equals(TtmlNode.UNDERLINE)) {
                    c2 = 3;
                    switch (c2) {
                        case 0:
                            break;
                        case 1:
                            break;
                        case 2:
                            break;
                        case 3:
                            break;
                    }
                }
            } else if (str.equals(TtmlNode.ITALIC)) {
                c2 = 1;
                switch (c2) {
                    case 0:
                        break;
                    case 1:
                        break;
                    case 2:
                        break;
                    case 3:
                        break;
                }
            }
            c2 = 65535;
            switch (c2) {
                case 0:
                    break;
                case 1:
                    break;
                case 2:
                    break;
                case 3:
                    break;
            }
        }
        textView.setTypeface(Typeface.DEFAULT, i2);
        textView.setPaintFlags(i);
    }

    @TargetApi(21)
    static void a(View view, @NonNull al alVar) {
        int i;
        int parseColor = Color.parseColor("#00000000");
        try {
            parseColor = Color.parseColor(alVar.e());
        } catch (IllegalArgumentException e2) {
            com.inmobi.commons.core.a.a.a().a(new com.inmobi.commons.core.e.a(e2));
        }
        view.setBackgroundColor(parseColor);
        if ("line".equals(alVar.a())) {
            GradientDrawable gradientDrawable = new GradientDrawable();
            gradientDrawable.setColor(parseColor);
            if ("curved".equals(alVar.b())) {
                gradientDrawable.setCornerRadius(alVar.c());
            }
            int parseColor2 = Color.parseColor("#ff000000");
            try {
                i = Color.parseColor(alVar.d());
            } catch (IllegalArgumentException e3) {
                com.inmobi.commons.core.a.a.a().a(new com.inmobi.commons.core.e.a(e3));
                i = parseColor2;
            }
            gradientDrawable.setStroke(1, i);
            if (VERSION.SDK_INT < 16) {
                view.setBackgroundDrawable(gradientDrawable);
                return;
            }
            view.setBackground(gradientDrawable);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0073, code lost:
        if (r0.equals("ICON") != false) goto L_0x008b;
     */
    @Nullable
    public final View a(@NonNull Context context, @NonNull ak akVar, @NonNull c cVar) {
        int i = 2;
        if (!(akVar instanceof am)) {
            String str = akVar.b;
            switch (str.hashCode()) {
                case 67056:
                    if (str.equals("CTA")) {
                        i = 4;
                        break;
                    }
                case 70564:
                    if (str.equals("GIF")) {
                        i = 7;
                        break;
                    }
                case 2241657:
                    break;
                case 2571565:
                    if (str.equals(AdPreferences.TYPE_TEXT)) {
                        i = 0;
                        break;
                    }
                case 69775675:
                    if (str.equals(ShareConstants.IMAGE_URL)) {
                        i = 1;
                        break;
                    }
                case 79826725:
                    if (str.equals("TIMER")) {
                        i = 5;
                        break;
                    }
                case 81665115:
                    if (str.equals(ShareConstants.VIDEO_URL)) {
                        i = 3;
                        break;
                    }
                case 1942407129:
                    if (str.equals("WEBVIEW")) {
                        i = 6;
                        break;
                    }
                default:
                    i = -1;
                    break;
            }
            switch (i) {
                case 0:
                    i = 4;
                    break;
                case 1:
                case 2:
                    i = 6;
                    break;
                case 3:
                    i = 7;
                    break;
                case 4:
                    i = 5;
                    break;
                case 5:
                    i = 8;
                    break;
                case 6:
                    i = 9;
                    break;
                case 7:
                    i = 10;
                    break;
                default:
                    i = -1;
                    break;
            }
        } else {
            am amVar = (am) akVar;
            if ("root".equalsIgnoreCase(amVar.d)) {
                i = 0;
            } else if (!"card_scrollable".equalsIgnoreCase(amVar.d)) {
                i = 3;
            } else if (amVar.A != 1) {
                i = 1;
            }
        }
        if (-1 != i) {
            return ((d) this.d.get(Integer.valueOf(i))).a(context, akVar, cVar);
        }
        StringBuilder sb = new StringBuilder("Unsupported AssetType:");
        sb.append(akVar.b);
        sb.append(" failed to instantiate view ");
        return null;
    }

    /* access modifiers changed from: private */
    @TargetApi(17)
    public static Button b(@NonNull Button button, @NonNull ak akVar) {
        a aVar = (a) akVar.c;
        button.setLayoutParams(new LayoutParams(c(aVar.f2152a.x), c(aVar.f2152a.y)));
        button.setText((CharSequence) akVar.e);
        button.setTextSize(1, (float) c(aVar.h()));
        int parseColor = Color.parseColor("#ff000000");
        try {
            parseColor = Color.parseColor(aVar.i());
        } catch (IllegalArgumentException e2) {
            com.inmobi.commons.core.a.a.a().a(new com.inmobi.commons.core.e.a(e2));
        }
        button.setTextColor(parseColor);
        int parseColor2 = Color.parseColor("#00000000");
        try {
            parseColor2 = Color.parseColor(aVar.e());
        } catch (IllegalArgumentException e3) {
            com.inmobi.commons.core.a.a.a().a(new com.inmobi.commons.core.e.a(e3));
        }
        button.setBackgroundColor(parseColor2);
        if (VERSION.SDK_INT >= 17) {
            button.setTextAlignment(4);
        }
        button.setGravity(17);
        a((TextView) button, aVar.j());
        a((View) button, (al) aVar);
        return button;
    }

    public static LayoutParams a(@NonNull ak akVar, @NonNull ViewGroup viewGroup) {
        Point point = akVar.c.f2152a;
        Point point2 = akVar.c.c;
        LayoutParams layoutParams = new LayoutParams(c(point.x), c(point.y));
        if (viewGroup instanceof NativeContainerLayout) {
            layoutParams = new com.inmobi.ads.NativeContainerLayout.a(c(point.x), c(point.y));
            com.inmobi.ads.NativeContainerLayout.a aVar = (com.inmobi.ads.NativeContainerLayout.a) layoutParams;
            int c2 = c(point2.x);
            int c3 = c(point2.y);
            aVar.f2087a = c2;
            aVar.b = c3;
        } else if (viewGroup instanceof LinearLayout) {
            layoutParams = new LinearLayout.LayoutParams(c(point.x), c(point.y));
            ((LinearLayout.LayoutParams) layoutParams).setMargins(c(point2.x), c(point2.y), 0, 0);
        } else if (viewGroup instanceof AbsListView) {
            return new AbsListView.LayoutParams(c(point.x), c(point.y));
        } else {
            if (viewGroup instanceof FrameLayout) {
                layoutParams = new FrameLayout.LayoutParams(c(point.x), c(point.y));
                ((FrameLayout.LayoutParams) layoutParams).setMargins(c(point2.x), c(point2.y), 0, 0);
            }
        }
        return layoutParams;
    }

    static /* synthetic */ void b(View view) {
        if (VERSION.SDK_INT < 16) {
            view.setBackgroundDrawable(null);
        } else {
            view.setBackground(null);
        }
    }

    static /* synthetic */ void a(ImageView imageView, ak akVar) {
        int i;
        int i2;
        int i3;
        String str = (String) akVar.e;
        if (str != null) {
            int c2 = c(akVar.c.f2152a.x);
            int c3 = c(akVar.c.f2152a.y);
            String f2 = akVar.c.f();
            char c4 = 65535;
            int hashCode = f2.hashCode();
            int i4 = 0;
            if (hashCode != -1362001767) {
                if (hashCode == 727618043 && f2.equals("aspectFill")) {
                    c4 = 1;
                }
            } else if (f2.equals("aspectFit")) {
                c4 = 0;
            }
            switch (c4) {
                case 0:
                    imageView.setScaleType(ScaleType.FIT_CENTER);
                    break;
                case 1:
                    imageView.setScaleType(ScaleType.CENTER_CROP);
                    break;
                default:
                    imageView.setScaleType(ScaleType.FIT_XY);
                    break;
            }
            Context context = (Context) f.get();
            if (context != null && c2 > 0 && c3 > 0 && str.trim().length() != 0) {
                bi.a(context).load(str).into(imageView, (Callback) bi.a((InvocationHandler) new c(context, imageView, akVar)));
                if ("cross_button".equalsIgnoreCase(akVar.d) && akVar.r.length() == 0) {
                    new Handler().postDelayed(new a(context, imageView), 2000);
                }
            }
            ak akVar2 = akVar.t;
            if (akVar2 == null || !"line".equals(akVar2.c.a())) {
                i3 = 0;
                i2 = 0;
                i = 0;
            } else {
                i3 = akVar2.c.c.x == akVar.c.c.x ? 1 : 0;
                i2 = c(akVar2.c.f2152a.x) == c(akVar.c.f2152a.x) + akVar.c.c.x ? 1 : 0;
                i = c(akVar2.c.c.y) == c(akVar.c.c.y) ? 1 : 0;
                if (c(akVar2.c.f2152a.y) == c(akVar.c.f2152a.y) + c(akVar.c.c.y)) {
                    i4 = 1;
                }
                if (c(akVar2.c.f2152a.x) == c(akVar.c.f2152a.x)) {
                    i3 = 1;
                    i2 = 1;
                }
            }
            if (VERSION.SDK_INT < 17) {
                imageView.setPadding(i3, i, i2, i4);
            } else {
                imageView.setPaddingRelative(i3, i, i2, i4);
            }
            a((View) imageView, akVar.c);
        }
    }

    static /* synthetic */ void a(GifView gifView, ak akVar) {
        gifView.setLayoutParams(new LayoutParams(c(akVar.c.f2152a.x), c(akVar.c.f2152a.y)));
        gifView.setContentMode(akVar.c.f());
        gifView.setGif(((aq) akVar).z);
        a((View) gifView, akVar.c);
    }

    static /* synthetic */ void a(NativeVideoWrapper nativeVideoWrapper, ak akVar) {
        if (VERSION.SDK_INT >= 15) {
            a((View) nativeVideoWrapper, akVar.c);
            if (akVar.w != null) {
                nativeVideoWrapper.setPosterImage((Bitmap) akVar.w);
            }
            nativeVideoWrapper.getProgressBar().setVisibility(0);
        }
    }

    static /* synthetic */ void a(TextView textView, ak akVar) {
        a aVar = (a) akVar.c;
        textView.setLayoutParams(new LayoutParams(c(aVar.f2152a.x), c(aVar.f2152a.y)));
        textView.setText((CharSequence) akVar.e);
        textView.setTypeface(Typeface.DEFAULT);
        switch (aVar.p) {
            case 1:
                textView.setGravity(8388629);
                break;
            case 2:
                textView.setGravity(17);
                break;
            default:
                textView.setGravity(8388627);
                break;
        }
        textView.setTextSize(1, (float) c(aVar.h()));
        int parseColor = Color.parseColor("#ff000000");
        try {
            parseColor = Color.parseColor(aVar.i());
        } catch (IllegalArgumentException e2) {
            com.inmobi.commons.core.a.a.a().a(new com.inmobi.commons.core.e.a(e2));
        }
        textView.setTextColor(parseColor);
        int parseColor2 = Color.parseColor("#00000000");
        try {
            parseColor2 = Color.parseColor(aVar.e());
        } catch (IllegalArgumentException e3) {
            com.inmobi.commons.core.a.a.a().a(new com.inmobi.commons.core.e.a(e3));
        }
        textView.setBackgroundColor(parseColor2);
        if (VERSION.SDK_INT >= 17) {
            textView.setTextAlignment(1);
        }
        a(textView, aVar.j());
        textView.setEllipsize(TruncateAt.END);
        textView.setHorizontallyScrolling(true);
        textView.setFocusable(true);
        textView.setFocusableInTouchMode(true);
        a((View) textView, (al) aVar);
    }

    static /* synthetic */ void a(TextView textView) {
        textView.setTypeface(Typeface.DEFAULT, 0);
        textView.setPaintFlags(textView.getPaintFlags() & -17);
        textView.setPaintFlags(textView.getPaintFlags() & -9);
    }

    static /* synthetic */ void a(bf bfVar, final NativeTimerView nativeTimerView, ak akVar) {
        long j;
        nativeTimerView.setVisibility(4);
        final bb bbVar = (bb) akVar;
        com.inmobi.ads.ba.a aVar = bbVar.A.f2176a;
        com.inmobi.ads.ba.a aVar2 = bbVar.A.b;
        if (aVar != null) {
            try {
                j = aVar.a();
            } catch (Exception e2) {
                com.inmobi.commons.core.a.a.a().a(new com.inmobi.commons.core.e.a(e2));
                return;
            }
        } else {
            j = 0;
        }
        long a2 = aVar2 != null ? aVar2.a() : 0;
        if (a2 >= 0) {
            nativeTimerView.setTimerValue(a2);
            new Handler().postDelayed(new Runnable() {
                public final void run() {
                    if (bf.f.get() != null) {
                        if (bbVar.z) {
                            nativeTimerView.setVisibility(0);
                        }
                        nativeTimerView.a();
                    }
                }
            }, j * 1000);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:25:0x0056 A[Catch:{ Exception -> 0x005f }] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x005a A[Catch:{ Exception -> 0x005f }] */
    static /* synthetic */ void a(RenderView renderView, ak akVar, c cVar) {
        try {
            bg bgVar = (bg) akVar;
            renderView.a(RenderView.f2411a, cVar);
            char c2 = 1;
            renderView.i = true;
            String str = (String) akVar.e;
            String str2 = bgVar.z;
            int hashCode = str2.hashCode();
            if (hashCode == -1081286672) {
                if (str2.equals("REF_IFRAME")) {
                    c2 = 2;
                    switch (c2) {
                        case 0:
                        case 1:
                            break;
                    }
                }
            } else if (hashCode == 84303) {
                if (str2.equals("URL")) {
                    c2 = 3;
                    switch (c2) {
                        case 0:
                        case 1:
                            break;
                    }
                }
            } else if (hashCode == 2228139) {
                if (str2.equals("HTML")) {
                    switch (c2) {
                        case 0:
                        case 1:
                            break;
                    }
                }
            } else if (hashCode == 83774455) {
                if (str2.equals("REF_HTML")) {
                    c2 = 0;
                    switch (c2) {
                        case 0:
                        case 1:
                            renderView.a(str);
                            return;
                        default:
                            renderView.b(str);
                            return;
                    }
                }
            }
            c2 = 65535;
            switch (c2) {
                case 0:
                case 1:
                    break;
            }
        } catch (Exception e2) {
            com.inmobi.commons.core.a.a.a().a(new com.inmobi.commons.core.e.a(e2));
        }
    }

    static /* synthetic */ void a(Context context, ImageView imageView, ak akVar) {
        if (!(context == null || imageView == null)) {
            String str = akVar.r;
            if ("cross_button".equalsIgnoreCase(akVar.d) && str.trim().length() == 0) {
                b(context, imageView);
            }
        }
        HashMap hashMap = new HashMap();
        hashMap.put("[ERRORCODE]", "603");
        akVar.a(TrackerEventType.TRACKER_EVENT_TYPE_ERROR, (Map<String, String>) hashMap);
    }
}
