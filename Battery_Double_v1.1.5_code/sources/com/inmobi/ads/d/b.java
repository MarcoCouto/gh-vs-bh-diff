package com.inmobi.ads.d;

import com.inmobi.ads.InMobiAdRequestStatus;
import com.inmobi.ads.InMobiAdRequestStatus.StatusCode;
import com.inmobi.ads.bj;
import com.inmobi.ads.bk;
import com.inmobi.ads.i;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

/* compiled from: InterstitialPreLoader */
public class b extends a {
    /* access modifiers changed from: private */
    public static final String d = "b";
    private static volatile b e;
    private static final Object f = new Object();
    /* access modifiers changed from: private */
    public static List<a> g = new LinkedList();

    /* compiled from: InterstitialPreLoader */
    static class a extends com.inmobi.ads.i.b {

        /* renamed from: a reason: collision with root package name */
        private bj f2246a;

        public final boolean i() {
            return false;
        }

        a(bj bjVar) {
            this.f2246a = bjVar;
        }

        public final void a(boolean z) {
            b.d;
        }

        public final void a() {
            b.d;
            b.g.remove(this);
        }

        public final void a(InMobiAdRequestStatus inMobiAdRequestStatus) {
            b.d;
            new StringBuilder("onAdLoadFailed called. Status:").append(inMobiAdRequestStatus.getMessage());
            i iVar = (i) a.f2236a.remove(this.f2246a);
            if (inMobiAdRequestStatus.getStatusCode() == StatusCode.NO_FILL) {
                iVar.d("PreLoadServerNoFill");
            }
            b.g.remove(this);
        }
    }

    public static b d() {
        b bVar = e;
        if (bVar == null) {
            synchronized (f) {
                bVar = e;
                if (bVar == null) {
                    bVar = new b();
                    e = bVar;
                }
            }
        }
        return bVar;
    }

    private b() {
        super("int");
    }

    public static void a(i iVar) {
        HashMap hashMap = new HashMap();
        hashMap.put("type", iVar.b());
        hashMap.put("plId", Long.valueOf(iVar.d));
        hashMap.put("clientRequestId", iVar.l);
    }

    public static void a(String str, i iVar) {
        HashMap hashMap = new HashMap();
        hashMap.put(IronSourceConstants.EVENTS_ERROR_CODE, str);
        hashMap.put("type", iVar.b());
        hashMap.put("plId", Long.valueOf(iVar.d));
        hashMap.put("clientRequestId", iVar.l);
    }

    static /* synthetic */ void a(b bVar) {
        if (b.c(bVar.c).f2225a && f2236a.size() >= b.c(bVar.c).c) {
            bk.a();
            ArrayList arrayList = (ArrayList) bk.a(bVar.c);
            Iterator it = f2236a.entrySet().iterator();
            while (it.hasNext()) {
                Entry entry = (Entry) it.next();
                if (!arrayList.contains(entry.getKey())) {
                    ((i) entry.getValue()).t();
                    it.remove();
                    StringBuilder sb = new StringBuilder("Removing extra ad unit from ad unit cache. Pid:");
                    sb.append(((bj) entry.getKey()).f2198a);
                    sb.append(" tp:");
                    sb.append(((bj) entry.getKey()).b);
                }
            }
        }
    }
}
