package com.inmobi.ads;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.tapjoy.TJAdUnitConstants.String;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: VastCompanionAd */
public class bu {
    static final ArrayList<String> f = new ArrayList<>(Arrays.asList(new String[]{"image/jpeg", "image/png"}));
    /* access modifiers changed from: private */
    public static final String h = "bu";

    /* renamed from: a reason: collision with root package name */
    int f2207a;
    int b;
    List<a> c = new ArrayList();
    List<NativeTracker> d = new ArrayList();
    String e;
    boolean g;
    @Nullable
    private String i;

    /* compiled from: VastCompanionAd */
    static final class a {

        /* renamed from: a reason: collision with root package name */
        int f2208a;
        String b;

        a(int i, String str) {
            this.f2208a = i;
            this.b = str;
        }

        /* JADX WARNING: Removed duplicated region for block: B:30:0x0063 A[Catch:{ JSONException -> 0x0075 }] */
        /* JADX WARNING: Removed duplicated region for block: B:31:0x0064 A[Catch:{ JSONException -> 0x0075 }] */
        /* JADX WARNING: Removed duplicated region for block: B:32:0x0066 A[Catch:{ JSONException -> 0x0075 }] */
        /* JADX WARNING: Removed duplicated region for block: B:33:0x0068 A[Catch:{ JSONException -> 0x0075 }] */
        static a a(JSONObject jSONObject) {
            char c;
            try {
                String string = jSONObject.getString("type");
                int i = 0;
                if (string != null) {
                    if (string.trim().length() != 0) {
                        String lowerCase = string.toLowerCase(Locale.US);
                        int hashCode = lowerCase.hashCode();
                        if (hashCode == -1191214428) {
                            if (lowerCase.equals("iframe")) {
                                c = 4;
                                switch (c) {
                                    case 2:
                                        break;
                                    case 3:
                                        break;
                                    case 4:
                                        break;
                                }
                            }
                        } else if (hashCode == -892481938) {
                            if (lowerCase.equals("static")) {
                                c = 2;
                                switch (c) {
                                    case 2:
                                        break;
                                    case 3:
                                        break;
                                    case 4:
                                        break;
                                }
                            }
                        } else if (hashCode == -284840886) {
                            if (lowerCase.equals("unknown")) {
                                c = 1;
                                switch (c) {
                                    case 2:
                                        break;
                                    case 3:
                                        break;
                                    case 4:
                                        break;
                                }
                            }
                        } else if (hashCode == 3213227) {
                            if (lowerCase.equals(String.HTML)) {
                                c = 3;
                                switch (c) {
                                    case 2:
                                        i = 1;
                                        break;
                                    case 3:
                                        i = 2;
                                        break;
                                    case 4:
                                        i = 3;
                                        break;
                                }
                            }
                        }
                        c = 65535;
                        switch (c) {
                            case 2:
                                break;
                            case 3:
                                break;
                            case 4:
                                break;
                        }
                    }
                }
                return new a(i, jSONObject.getString("content"));
            } catch (JSONException e) {
                bu.h;
                new StringBuilder("Error building resource from JSONObject; ").append(e.getMessage());
                com.inmobi.commons.core.a.a.a().a(new com.inmobi.commons.core.e.a(e));
                return null;
            }
        }

        public final String toString() {
            String str;
            JSONObject jSONObject = new JSONObject();
            String str2 = "type";
            try {
                switch (this.f2208a) {
                    case 1:
                        str = "static";
                        break;
                    case 2:
                        str = String.HTML;
                        break;
                    case 3:
                        str = "iframe";
                        break;
                    default:
                        str = "unknown";
                        break;
                }
                jSONObject.put(str2, str);
                jSONObject.put("content", this.b);
                return jSONObject.toString();
            } catch (JSONException e) {
                bu.h;
                new StringBuilder("Error serializing resource: ").append(e.getMessage());
                com.inmobi.commons.core.a.a.a().a(new com.inmobi.commons.core.e.a(e));
                return "";
            }
        }
    }

    public bu(int i2, int i3, String str, @Nullable String str2) {
        this.i = str2;
        this.f2207a = i2;
        this.b = i3;
        this.e = str;
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public final List<a> a(int i2) {
        ArrayList arrayList = new ArrayList();
        for (a aVar : this.c) {
            if (aVar.f2208a == i2) {
                arrayList.add(aVar);
            }
        }
        return arrayList;
    }

    /* access modifiers changed from: 0000 */
    public final void a(@NonNull a aVar) {
        this.c.add(aVar);
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public final List<NativeTracker> a(TrackerEventType trackerEventType) {
        ArrayList arrayList = new ArrayList();
        for (NativeTracker nativeTracker : this.d) {
            if (nativeTracker.b.equals(trackerEventType)) {
                arrayList.add(nativeTracker);
            }
        }
        return arrayList;
    }

    public final void a(@NonNull NativeTracker nativeTracker) {
        this.d.add(nativeTracker);
    }

    @Nullable
    static bu a(JSONObject jSONObject) {
        try {
            bu buVar = new bu(jSONObject.getInt("width"), jSONObject.getInt("height"), jSONObject.has("clickThroughUrl") ? jSONObject.getString("clickThroughUrl") : null, jSONObject.optString("id", null));
            try {
                JSONArray jSONArray = new JSONArray(jSONObject.getString("trackers"));
                for (int i2 = 0; i2 < jSONArray.length(); i2++) {
                    NativeTracker a2 = NativeTracker.a(new JSONObject(jSONArray.getString(i2)));
                    if (a2 != null) {
                        buVar.a(a2);
                    }
                }
            } catch (JSONException e2) {
                com.inmobi.commons.core.a.a.a().a(new com.inmobi.commons.core.e.a(e2));
            }
            try {
                JSONArray jSONArray2 = new JSONArray(jSONObject.getString("resources"));
                for (int i3 = 0; i3 < jSONArray2.length(); i3++) {
                    a a3 = a.a(new JSONObject(jSONArray2.getString(i3)));
                    if (a3 != null) {
                        buVar.a(a3);
                    }
                }
            } catch (JSONException e3) {
                com.inmobi.commons.core.a.a.a().a(new com.inmobi.commons.core.e.a(e3));
            }
            return buVar;
        } catch (JSONException e4) {
            new StringBuilder("Error building companion from JSON: ").append(e4.getMessage());
            com.inmobi.commons.core.a.a.a().a(new com.inmobi.commons.core.e.a(e4));
            return null;
        }
    }

    public String toString() {
        JSONObject jSONObject = new JSONObject();
        try {
            if (this.i != null) {
                jSONObject.put("id", this.i);
            }
            jSONObject.put("width", this.f2207a);
            jSONObject.put("height", this.b);
            jSONObject.put("clickThroughUrl", this.e);
            JSONArray jSONArray = new JSONArray();
            for (a aVar : this.c) {
                jSONArray.put(aVar.toString());
            }
            jSONObject.put("resources", jSONArray);
            JSONArray jSONArray2 = new JSONArray();
            for (NativeTracker nativeTracker : this.d) {
                jSONArray2.put(nativeTracker.toString());
            }
            jSONObject.put("trackers", jSONArray2);
            return jSONObject.toString();
        } catch (JSONException e2) {
            StringBuilder sb = new StringBuilder("Error serializing an ");
            sb.append(h);
            sb.append(" instance: ");
            sb.append(e2.getMessage());
            com.inmobi.commons.core.a.a.a().a(new com.inmobi.commons.core.e.a(e2));
            return "";
        }
    }
}
