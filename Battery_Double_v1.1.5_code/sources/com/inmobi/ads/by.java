package com.inmobi.ads;

import android.os.SystemClock;
import android.text.TextUtils;
import com.github.mikephil.charting.utils.Utils;
import com.inmobi.a.n;
import com.inmobi.ads.a.d;
import com.inmobi.ads.c.C0041c;
import com.inmobi.ads.c.k;
import com.inmobi.commons.core.a.a;
import com.inmobi.commons.core.network.NetworkError;
import com.inmobi.commons.core.network.NetworkError.ErrorCode;
import com.inmobi.commons.core.network.f;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/* compiled from: VastResponse */
public final class by implements bz {

    /* renamed from: a reason: collision with root package name */
    List<bw> f2214a;
    String b;
    String c;
    List<NativeTracker> d;
    List<bu> e;
    int f;
    private String g;
    private bu h;
    private k i;
    private bw j;

    private static boolean a(double d2, double d3, double d4) {
        return d4 > d2 && d4 <= d3;
    }

    public by(k kVar) {
        this.j = null;
        this.f2214a = new ArrayList();
        this.d = new ArrayList();
        this.e = new ArrayList();
        this.i = kVar;
        this.f = 0;
    }

    private by(List<NativeTracker> list, k kVar) {
        this(kVar);
        if (list.size() != 0) {
            this.d = new ArrayList(list);
        }
    }

    public by(String str, String str2, String str3, List<NativeTracker> list, List<bu> list2, k kVar) {
        this(list, kVar);
        if (list2.size() != 0) {
            this.e = new ArrayList(list2);
        }
        this.g = str;
        this.f2214a.add(new bw(str));
        this.b = str2;
        this.c = str3;
    }

    public final String a() {
        return this.c;
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x0035  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x003e  */
    public final String b() {
        bw bwVar;
        int i2;
        if (this.g != null) {
            return this.g;
        }
        d.a();
        List d2 = d.d();
        bw bwVar2 = null;
        if (!d2.isEmpty()) {
            Iterator it = this.f2214a.iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                bwVar = (bw) it.next();
                if (d2.contains(bwVar.f2210a)) {
                    break;
                }
            }
            if (bwVar == null) {
                this.j = bwVar;
                this.g = bwVar.f2210a;
                return this.g;
            }
            double d3 = (double) this.i.b;
            Double.isNaN(d3);
            double d4 = (d3 * 2.0d) / 1048576.0d;
            double d5 = (double) this.i.c;
            double d6 = 1.0d;
            Double.isNaN(d5);
            double d7 = (d5 * 1.0d) / 1048576.0d;
            for (bw bwVar3 : this.f2214a) {
                String[] split = this.b.split(":");
                try {
                    i2 = (Integer.parseInt(split[1]) * 60) + Integer.parseInt(split[2]);
                } catch (ArrayIndexOutOfBoundsException e2) {
                    i2 = 0;
                    a.a().a(new com.inmobi.commons.core.e.a(e2));
                }
                double d8 = (double) bwVar3.b;
                Double.isNaN(d8);
                double d9 = d8 * d6;
                double d10 = (double) i2;
                Double.isNaN(d10);
                double d11 = (d9 * d10) / 8192.0d;
                bwVar3.c = d11;
                double d12 = d11;
                bw bwVar4 = bwVar3;
                if (a((double) Utils.DOUBLE_EPSILON, d4, d12)) {
                    bwVar = a(bwVar, bwVar4, d12);
                } else {
                    double d13 = d12;
                    double d14 = d13;
                    if (a(d4, d7, d13)) {
                        bwVar2 = b(bwVar2, bwVar4, d14);
                    }
                }
                d6 = 1.0d;
            }
            a(bwVar, bwVar2);
            if (TextUtils.isEmpty(this.g)) {
                C0041c cVar = this.i.d;
                if (cVar.f2220a || this.f2214a.size() == 0) {
                    return this.g;
                }
                CountDownLatch countDownLatch = new CountDownLatch(this.f2214a.size());
                try {
                    a(cVar, countDownLatch);
                    countDownLatch.await((long) cVar.b, TimeUnit.MILLISECONDS);
                    for (bw bwVar5 : this.f2214a) {
                        double d15 = bwVar5.c;
                        double d16 = d15;
                        if (a((double) Utils.DOUBLE_EPSILON, d4, d15)) {
                            bwVar = a(bwVar, bwVar5, d16);
                        } else {
                            double d17 = d16;
                            double d18 = d17;
                            if (a(d4, d7, d17)) {
                                bwVar2 = b(bwVar2, bwVar5, d18);
                            }
                        }
                    }
                } catch (Exception e3) {
                    new StringBuilder("SDK encountered an unexpected error in getting vast header response; ").append(e3.getMessage());
                    a.a().a(new com.inmobi.commons.core.e.a(e3));
                    for (bw bwVar6 : this.f2214a) {
                        double d19 = bwVar6.c;
                        double d20 = d19;
                        if (a((double) Utils.DOUBLE_EPSILON, d4, d19)) {
                            bwVar = a(bwVar, bwVar6, d20);
                        } else {
                            double d21 = d20;
                            double d22 = d21;
                            if (a(d4, d7, d21)) {
                                bwVar2 = b(bwVar2, bwVar6, d22);
                            }
                        }
                    }
                } catch (Throwable th) {
                    for (bw bwVar7 : this.f2214a) {
                        double d23 = bwVar7.c;
                        double d24 = d23;
                        if (a((double) Utils.DOUBLE_EPSILON, d4, d23)) {
                            bwVar = a(bwVar, bwVar7, d24);
                        } else {
                            double d25 = d24;
                            double d26 = d25;
                            if (a(d4, d7, d25)) {
                                bwVar2 = b(bwVar2, bwVar7, d26);
                            }
                        }
                    }
                    a(bwVar, bwVar2);
                    throw th;
                }
                a(bwVar, bwVar2);
            }
            return this.g;
        }
        bwVar = null;
        if (bwVar == null) {
        }
    }

    private void a(C0041c cVar, CountDownLatch countDownLatch) {
        for (bw bxVar : this.f2214a) {
            bx bxVar2 = new bx(bxVar, cVar.b, countDownLatch);
            bxVar2.c = SystemClock.elapsedRealtime();
            bx.d.execute(new Runnable() {
                public final void run() {
                    bx bxVar;
                    try {
                        com.inmobi.commons.core.network.d a2 = new f(bx.this.f2211a).a();
                        if (a2 == null) {
                            return;
                        }
                        if (a2.a()) {
                            bx.this.a(a2);
                            return;
                        }
                        bxVar = bx.this;
                        try {
                            n.a().a(bxVar.f2211a.g());
                            n.a().b(a2.c());
                            n.a().c(SystemClock.elapsedRealtime() - bxVar.c);
                            if (bxVar.b.get() != null) {
                                double d = (double) a2.c;
                                Double.isNaN(d);
                                ((bw) bxVar.b.get()).c = (d * 1.0d) / 1048576.0d;
                            }
                            bxVar.a();
                        } catch (Exception e) {
                            new StringBuilder("Handling Vast Media Header Request success encountered an unexpected error: ").append(e.getMessage());
                            a.a().a(new com.inmobi.commons.core.e.a(e));
                            bxVar.a();
                        }
                    } catch (Exception e2) {
                        bx.e;
                        new StringBuilder("Network request failed with unexpected error: ").append(e2.getMessage());
                        NetworkError networkError = new NetworkError(ErrorCode.UNKNOWN_ERROR, "Network request failed with unknown error");
                        com.inmobi.commons.core.network.d dVar = new com.inmobi.commons.core.network.d();
                        dVar.b = networkError;
                        bx.this.a(dVar);
                    } catch (Throwable th) {
                        bxVar.a();
                        throw th;
                    }
                }
            });
        }
    }

    private void a(bw bwVar, bw bwVar2) {
        if (bwVar != null) {
            this.j = bwVar;
            this.g = bwVar.f2210a;
            return;
        }
        if (bwVar2 != null) {
            this.j = bwVar2;
            this.g = bwVar2.f2210a;
        }
    }

    public final List<bw> c() {
        return this.f2214a;
    }

    public final List<NativeTracker> d() {
        return this.d;
    }

    public final List<bu> e() {
        return this.e;
    }

    public final void a(bu buVar) {
        this.h = buVar;
    }

    public final bu f() {
        return this.h;
    }

    /* access modifiers changed from: 0000 */
    public final void a(NativeTracker nativeTracker) {
        this.d.add(nativeTracker);
    }

    private static bw a(bw bwVar, bw bwVar2, double d2) {
        return (bwVar != null && d2 <= bwVar.c) ? bwVar : bwVar2;
    }

    private static bw b(bw bwVar, bw bwVar2, double d2) {
        return (bwVar != null && d2 >= bwVar.c) ? bwVar : bwVar2;
    }
}
