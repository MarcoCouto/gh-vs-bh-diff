package com.inmobi.ads;

import android.os.Handler;
import android.os.Message;
import com.inmobi.commons.core.a.a;
import com.inmobi.rendering.RenderView;
import java.lang.ref.WeakReference;

/* compiled from: RenderTimeoutHandler */
final class bo extends Handler {

    /* renamed from: a reason: collision with root package name */
    private WeakReference<i> f2203a;

    bo(i iVar) {
        this.f2203a = new WeakReference<>(iVar);
    }

    public final void handleMessage(Message message) {
        i iVar = (i) this.f2203a.get();
        if (iVar != null && message.what == 0) {
            try {
                if (iVar instanceof ac) {
                    ac acVar = (ac) iVar;
                    if (acVar.u != null) {
                        acVar.u.stopLoading();
                        return;
                    }
                }
                RenderView renderView = (RenderView) iVar.j();
                if (renderView == null) {
                    iVar.B();
                    return;
                }
                renderView.stopLoading();
                iVar.B();
            } catch (Exception e) {
                a.a().a(new com.inmobi.commons.core.e.a(e));
            } finally {
                iVar.B();
            }
        }
    }
}
