package com.inmobi.ads;

import android.content.Context;
import android.widget.FrameLayout;

public abstract class NativeScrollableContainer extends FrameLayout {

    /* renamed from: a reason: collision with root package name */
    private final int f2090a;

    interface a {
        int a(int i);
    }

    /* access modifiers changed from: 0000 */
    public abstract void a(am amVar, ax axVar, int i, int i2, a aVar);

    public NativeScrollableContainer(Context context) {
        super(context);
        this.f2090a = 0;
    }

    public NativeScrollableContainer(Context context, int i) {
        super(context);
        this.f2090a = i;
    }

    public final int getType() {
        return this.f2090a;
    }
}
