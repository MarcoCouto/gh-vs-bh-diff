package com.inmobi.ads;

import android.graphics.Point;
import android.support.annotation.Nullable;
import com.github.mikephil.charting.utils.Utils;
import com.inmobi.commons.core.utilities.b.c;
import java.util.Iterator;
import java.util.List;

/* compiled from: VastBestFitCompanionFinder */
public final class bt {
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x00cb, code lost:
        if (r11 > r13) goto L_0x00cd;
     */
    @Nullable
    static bu a(be beVar, ak akVar) {
        double d;
        double d2;
        bz b = beVar.b();
        Point point = akVar.c.f2152a;
        List e = b.e();
        float f = c.a().c;
        double d3 = (double) (((float) point.y) / f);
        double d4 = (double) (((float) point.x) / f);
        Double.isNaN(d4);
        Double.isNaN(d3);
        double d5 = d4 / d3;
        Double.isNaN(d4);
        Double.isNaN(d3);
        double d6 = d4 * d3;
        Iterator it = e.iterator();
        double d7 = -1.0d;
        double d8 = Utils.DOUBLE_EPSILON;
        bu buVar = null;
        while (it.hasNext()) {
            bu buVar2 = (bu) it.next();
            Iterator it2 = it;
            bu buVar3 = buVar;
            bu buVar4 = buVar2;
            double d9 = (double) buVar2.f2207a;
            double d10 = d8;
            double d11 = (double) buVar2.b;
            Double.isNaN(d9);
            Double.isNaN(d11);
            if (d5 > d9 / d11) {
                Double.isNaN(d3);
                Double.isNaN(d11);
                double d12 = d3 / d11;
                Double.isNaN(d9);
                d = d12 * d9;
                d2 = d3;
            } else {
                Double.isNaN(d4);
                Double.isNaN(d9);
                double d13 = d4 / d9;
                Double.isNaN(d11);
                d2 = d13 * d11;
                d = d4;
            }
            if (d11 >= d2 * 0.33d && d9 >= 0.33d * d) {
                double d14 = d * d2;
                if (d14 > 0.5d * d6) {
                    if (d14 > d7) {
                        Double.isNaN(d11);
                        d8 = d11 / d2;
                        d7 = d14;
                        it = it2;
                        buVar = buVar4;
                    } else {
                        if (d14 == d7) {
                            float f2 = c.a().c;
                            Double.isNaN(d11);
                            d8 = d11 / d2;
                            if (d8 <= d10 || d10 >= ((double) f2)) {
                                double d15 = (double) f2;
                                if (d10 > d15) {
                                    if (d8 < d10) {
                                    }
                                }
                            }
                            buVar = buVar4;
                            it = it2;
                        }
                        buVar = buVar3;
                        d8 = d10;
                        it = it2;
                    }
                }
            }
            it = it2;
            buVar = buVar3;
            d8 = d10;
        }
        return buVar;
    }
}
