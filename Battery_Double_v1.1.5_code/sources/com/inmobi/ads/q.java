package com.inmobi.ads;

import android.os.Handler;
import android.os.Message;

/* compiled from: BannerRefreshHandler */
final class q extends Handler {

    /* renamed from: a reason: collision with root package name */
    private InMobiBanner f2298a;

    q(InMobiBanner inMobiBanner) {
        this.f2298a = inMobiBanner;
    }

    public final void handleMessage(Message message) {
        if (message.what == 1) {
            this.f2298a.a(true);
        }
    }
}
