package com.inmobi.ads;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Point;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.GravityCompat;
import android.view.View;
import android.view.View.OnAttachStateChangeListener;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.RelativeLayout.LayoutParams;
import com.facebook.share.internal.ShareConstants;
import com.inmobi.ads.AdContainer.RenderingProperties.PlacementType;
import com.inmobi.rendering.RenderView;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.Iterator;
import java.util.List;

/* compiled from: NativeLayoutInflater */
final class au implements a {
    /* access modifiers changed from: private */
    public static final String e = "au";
    private static Handler n = new Handler(Looper.getMainLooper());
    /* access modifiers changed from: 0000 */
    @Nullable

    /* renamed from: a reason: collision with root package name */
    public bh f2164a;
    int b = 0;
    /* access modifiers changed from: 0000 */
    public final n c;
    bf d;
    @NonNull
    private final WeakReference<Context> f;
    /* access modifiers changed from: private */
    @NonNull
    public final ao g;
    /* access modifiers changed from: private */
    @NonNull
    public final ah h;
    @NonNull
    private final c i;
    @NonNull
    private c j;
    /* access modifiers changed from: private */
    @NonNull
    public a k;
    /* access modifiers changed from: private */
    @Nullable
    public b l;
    private ax m;
    /* access modifiers changed from: private */
    public boolean o = false;
    private RenderView p;

    /* compiled from: NativeLayoutInflater */
    interface a {
        void a(View view, ak akVar);
    }

    /* compiled from: NativeLayoutInflater */
    interface b {
        void a(bb bbVar);
    }

    /* compiled from: NativeLayoutInflater */
    interface c {
        void a(int i, ak akVar);
    }

    au(@NonNull Context context, @NonNull c cVar, @NonNull ah ahVar, @NonNull ao aoVar, @NonNull c cVar2, @NonNull a aVar, @NonNull b bVar) {
        this.f = new WeakReference<>(context);
        this.h = ahVar;
        this.g = aoVar;
        this.j = cVar2;
        this.k = aVar;
        this.l = bVar;
        this.c = new n();
        this.i = cVar;
        this.d = bf.a(context);
    }

    private Context c() {
        return (Context) this.f.get();
    }

    public final aw a(@Nullable aw awVar, @NonNull ViewGroup viewGroup, RenderView renderView) {
        this.p = renderView;
        aw a2 = a(awVar, viewGroup);
        if (!this.o) {
            b(a2, this.g.d);
        }
        return a2;
    }

    /* access modifiers changed from: 0000 */
    public final aw b(@Nullable aw awVar, @NonNull final ViewGroup viewGroup, RenderView renderView) {
        this.p = renderView;
        final aw a2 = a(awVar, viewGroup);
        n.post(new Runnable() {
            public final void run() {
                if (!au.this.o) {
                    au.this.b(a2, au.this.g.d);
                }
            }
        });
        return a2;
    }

    private aw a(@Nullable aw awVar, @NonNull ViewGroup viewGroup) {
        aw awVar2 = awVar == null ? (aw) this.d.a(c(), (ak) this.g.d, this.i) : awVar;
        if (!(awVar2 == null || awVar == null)) {
            ViewParent parent = awVar2.getParent();
            if (parent instanceof ViewGroup) {
                ((ViewGroup) parent).removeView(awVar2);
            }
            bf bfVar = this.d;
            for (int childCount = awVar2.getChildCount() - 1; childCount >= 0; childCount--) {
                View childAt = awVar2.getChildAt(childCount);
                awVar2.removeViewAt(childCount);
                bfVar.a(childAt);
            }
            bf.a((View) awVar2, this.g.d.c);
        }
        bf.b(this.g.d.c.f2152a.x);
        awVar2.setLayoutParams(bf.a((ak) this.g.d, viewGroup));
        return awVar2;
    }

    public final ViewGroup a(@NonNull ViewGroup viewGroup, @NonNull am amVar) {
        ViewGroup viewGroup2 = (ViewGroup) this.d.a(c(), (ak) amVar, this.i);
        if (viewGroup2 != null) {
            viewGroup2.setLayoutParams(bf.a((ak) amVar, viewGroup));
        }
        return viewGroup2;
    }

    public final int a(int i2) {
        this.b = i2;
        this.j.a(i2, this.g.a(i2));
        return d();
    }

    private void a(View view, final ak akVar) {
        boolean z;
        final List a2 = this.c.a(view, akVar);
        if (a2 == null) {
            TrackerEventType trackerEventType = TrackerEventType.TRACKER_EVENT_TYPE_CREATIVE_VIEW;
            Iterator it = akVar.u.iterator();
            while (true) {
                if (it.hasNext()) {
                    if (trackerEventType == ((NativeTracker) it.next()).b) {
                        z = true;
                        break;
                    }
                } else {
                    z = false;
                    break;
                }
            }
            if (!z) {
                return;
            }
        }
        view.addOnAttachStateChangeListener(new OnAttachStateChangeListener() {
            public final void onViewAttachedToWindow(View view) {
                au.this.c.a(a2);
                au.this.h;
                ak a2 = ah.a(au.this.h.h(), akVar);
                ak akVar = akVar;
                TrackerEventType trackerEventType = TrackerEventType.TRACKER_EVENT_TYPE_CREATIVE_VIEW;
                ah e = au.this.h;
                if (a2 == null) {
                    a2 = akVar;
                }
                akVar.a(trackerEventType, e.a(a2));
            }

            public final void onViewDetachedFromWindow(View view) {
                view.removeOnAttachStateChangeListener(this);
                n d = au.this.c;
                List<a> list = a2;
                if (list != null) {
                    for (a aVar : list) {
                        aVar.f2295a.cancel();
                    }
                    d.f2292a.removeAll(list);
                }
            }
        });
    }

    /* access modifiers changed from: 0000 */
    /* JADX WARNING: Removed duplicated region for block: B:125:0x000d A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x00d6  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x00e5  */
    public final ViewGroup b(@NonNull ViewGroup viewGroup, am amVar) {
        View view;
        View view2;
        LayoutParams layoutParams;
        double d2;
        double d3;
        av avVar;
        ViewGroup viewGroup2 = viewGroup;
        a((ak) amVar, (View) viewGroup2);
        Iterator it = amVar.iterator();
        while (it.hasNext()) {
            ak akVar = (ak) it.next();
            if ("CONTAINER" != akVar.b) {
                if ("WEBVIEW".equals(akVar.b)) {
                    bg bgVar = (bg) akVar;
                    if (bgVar.A && this.p != null) {
                        view = this.p;
                        if (view.getParent() != null) {
                            ((ViewGroup) view.getParent()).removeView(view);
                        }
                        this.p = null;
                        if (view == null) {
                            view = this.d.a(c(), akVar, this.i);
                        }
                        view2 = view;
                        if (view2 == null) {
                            final WeakReference weakReference = new WeakReference(view2);
                            if (akVar.o != -1) {
                                view2.setVisibility(4);
                                n.postDelayed(new Runnable() {
                                    public final void run() {
                                        View view = (View) weakReference.get();
                                        if (view != null) {
                                            view.setVisibility(0);
                                        }
                                    }
                                }, (long) (akVar.o * 1000));
                            } else if (akVar.p != -1) {
                                n.postDelayed(new Runnable() {
                                    public final void run() {
                                        View view = (View) weakReference.get();
                                        if (view != null) {
                                            view.setVisibility(4);
                                        }
                                    }
                                }, (long) (akVar.p * 1000));
                            }
                            view2.setLayoutParams(bf.a(akVar, viewGroup2));
                            a(view2, akVar);
                            viewGroup2.addView(view2);
                            if (VERSION.SDK_INT >= 15 && ShareConstants.VIDEO_URL == akVar.b) {
                                final be beVar = (be) akVar;
                                NativeVideoView videoView = ((NativeVideoWrapper) view2).getVideoView();
                                if (VERSION.SDK_INT >= 15) {
                                    am amVar2 = (am) beVar.t;
                                    long currentTimeMillis = System.currentTimeMillis();
                                    if (!(amVar2 == null || 0 == amVar2.z)) {
                                        currentTimeMillis = amVar2.z;
                                    }
                                    if (amVar2 != null) {
                                        amVar2.z = currentTimeMillis;
                                    }
                                    videoView.setClickable(false);
                                    videoView.setId(Integer.MAX_VALUE);
                                    videoView.e = 0;
                                    videoView.f = 0;
                                    videoView.f2098a = Uri.parse(((bz) beVar.e).b());
                                    if (PlacementType.PLACEMENT_TYPE_FULLSCREEN == ((PlacementType) beVar.v.get("placementType"))) {
                                        avVar = new av();
                                    } else {
                                        avVar = av.a();
                                    }
                                    videoView.c = avVar;
                                    if (videoView.d != 0) {
                                        videoView.c.setAudioSessionId(videoView.d);
                                    } else {
                                        videoView.d = videoView.c.getAudioSessionId();
                                    }
                                    try {
                                        videoView.c.setDataSource(videoView.getContext().getApplicationContext(), videoView.f2098a, videoView.b);
                                        videoView.setTag(beVar);
                                        videoView.g = new d(videoView);
                                        videoView.setSurfaceTextureListener(videoView.l);
                                        videoView.setFocusable(true);
                                        videoView.setFocusableInTouchMode(true);
                                        videoView.requestFocus();
                                    } catch (IOException unused) {
                                        videoView.c.f2174a = -1;
                                        videoView.c.b = -1;
                                    }
                                    if (beVar.y != null) {
                                        beVar.a((be) beVar.y);
                                    }
                                    videoView.setQuartileCompletedListener(new c() {
                                        public final void a(int i) {
                                            if (au.this.f2164a != null) {
                                                au.this.f2164a.b(beVar, i);
                                                if (3 == i) {
                                                    try {
                                                        au.this.f2164a.d(beVar);
                                                    } catch (Exception e) {
                                                        au.e;
                                                        new StringBuilder("SDK encountered unexpected error in handling the onVideoCompleted event; ").append(e.getMessage());
                                                    }
                                                }
                                            }
                                        }
                                    });
                                    videoView.setPlaybackEventListener(new b() {
                                        @SuppressLint({"SwitchIntDef"})
                                        public final void a(int i) {
                                            if (au.this.f2164a != null) {
                                                if (i != 5) {
                                                    switch (i) {
                                                        case 0:
                                                            try {
                                                                au.this.f2164a.a();
                                                                return;
                                                            } catch (Exception e) {
                                                                au.e;
                                                                new StringBuilder("SDK encountered unexpected error in handling onVideoPrepared event; ").append(e.getMessage());
                                                                com.inmobi.commons.core.a.a.a().a(new com.inmobi.commons.core.e.a(e));
                                                                return;
                                                            }
                                                        case 1:
                                                            try {
                                                                au.this.f2164a.a(beVar);
                                                                return;
                                                            } catch (Exception e2) {
                                                                au.e;
                                                                new StringBuilder("SDK encountered unexpected error in handling onVideoPlayed event; ").append(e2.getMessage());
                                                                return;
                                                            }
                                                        case 2:
                                                            try {
                                                                au.this.f2164a.b(beVar);
                                                                return;
                                                            } catch (Exception e3) {
                                                                au.e;
                                                                new StringBuilder("SDK encountered unexpected error in handling onVideoPaused event; ").append(e3.getMessage());
                                                                return;
                                                            }
                                                        case 3:
                                                            try {
                                                                au.this.f2164a.c(beVar);
                                                                return;
                                                            } catch (Exception e4) {
                                                                au.e;
                                                                new StringBuilder("SDK encountered unexpected error in handling onVideoResumed event; ").append(e4.getMessage());
                                                                return;
                                                            }
                                                        default:
                                                            return;
                                                    }
                                                } else {
                                                    try {
                                                        au.this.f2164a.e(beVar);
                                                    } catch (Exception e5) {
                                                        au.e;
                                                        new StringBuilder("SDK encountered unexpected error in handling fireVideoQ4Beacons event; ").append(e5.getMessage());
                                                    }
                                                }
                                            }
                                        }
                                    });
                                    videoView.setMediaErrorListener(new a() {
                                        public final void a(int i) {
                                            if (au.this.f2164a != null) {
                                                try {
                                                    au.this.f2164a.a(beVar, i);
                                                } catch (Exception e) {
                                                    au.e;
                                                    new StringBuilder("SDK encountered unexpected error in handling the onVideoError event; ").append(e.getMessage());
                                                }
                                            }
                                        }
                                    });
                                    if (this.f2164a != null) {
                                        try {
                                            this.f2164a.a(videoView);
                                        } catch (Exception e2) {
                                            new StringBuilder("SDK encountered unexpected error in handling the onVideoViewCreated event; ").append(e2.getMessage());
                                        }
                                    }
                                }
                            }
                            a(akVar, view2);
                            if ("TIMER" == akVar.b) {
                                view2.setTag("timerView");
                                final bb bbVar = (bb) akVar;
                                ((NativeTimerView) view2).setTimerEventsListener(new b() {
                                    public final void a() {
                                        if (au.this.l != null) {
                                            au.this.l.a(bbVar);
                                        }
                                    }
                                });
                            }
                            if (VERSION.SDK_INT >= 15 && ShareConstants.VIDEO_URL == akVar.b) {
                                NativeVideoWrapper nativeVideoWrapper = (NativeVideoWrapper) view2;
                                nativeVideoWrapper.setVideoEventListener(this.f2164a);
                                be beVar2 = (be) nativeVideoWrapper.f2108a.getTag();
                                if (beVar2 != null) {
                                    try {
                                        String b2 = beVar2.b().b();
                                        MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();
                                        mediaMetadataRetriever.setDataSource(b2);
                                        int intValue = Integer.valueOf(mediaMetadataRetriever.extractMetadata(18)).intValue();
                                        int intValue2 = Integer.valueOf(mediaMetadataRetriever.extractMetadata(19)).intValue();
                                        mediaMetadataRetriever.release();
                                        Point point = beVar2.c.f2152a;
                                        double c2 = (double) bf.c(point.x);
                                        double c3 = (double) bf.c(point.y);
                                        Double.isNaN(c2);
                                        Double.isNaN(c3);
                                        double d4 = c2 / c3;
                                        double d5 = (double) intValue;
                                        double d6 = (double) intValue2;
                                        Double.isNaN(d5);
                                        Double.isNaN(d6);
                                        if (d4 > d5 / d6) {
                                            double c4 = (double) bf.c(point.y);
                                            Double.isNaN(c4);
                                            double d7 = c4 * 1.0d;
                                            Double.isNaN(d6);
                                            double d8 = d7 / d6;
                                            Double.isNaN(d5);
                                            d3 = d5 * d8;
                                            d2 = (double) bf.c(point.y);
                                        } else {
                                            double c5 = (double) bf.c(point.x);
                                            double c6 = (double) bf.c(point.x);
                                            Double.isNaN(c6);
                                            double d9 = c6 * 1.0d;
                                            Double.isNaN(d5);
                                            double d10 = d9 / d5;
                                            Double.isNaN(d6);
                                            d2 = d6 * d10;
                                            d3 = c5;
                                        }
                                        layoutParams = new LayoutParams((int) d3, (int) d2);
                                    } catch (Exception e3) {
                                        LayoutParams layoutParams2 = new LayoutParams(-1, -1);
                                        com.inmobi.commons.core.a.a.a().a(new com.inmobi.commons.core.e.a(e3));
                                        layoutParams = layoutParams2;
                                    }
                                    layoutParams.addRule(13);
                                    nativeVideoWrapper.f2108a.setLayoutParams(layoutParams);
                                }
                            }
                            if ("WEBVIEW" == akVar.b && (view2 instanceof RenderView)) {
                                RenderView renderView = (RenderView) view2;
                                bg bgVar2 = (bg) akVar;
                                renderView.setScrollable(bgVar2.B);
                                renderView.setReferenceContainer(this.h.n);
                                renderView.setRenderViewEventListener(this.h.u());
                                renderView.setPlacementId(this.h.e);
                                renderView.setAllowAutoRedirection(this.h.g);
                                renderView.setCreativeId(this.h.f);
                                renderView.setImpressionId(this.h.d);
                                if (!bgVar2.A) {
                                    ah ahVar = this.h;
                                    if (ahVar.y == 0 && ahVar.x == null && ahVar.w == null) {
                                        ahVar.x = renderView;
                                    }
                                }
                            }
                        }
                    } else if ("UNKNOWN".equals(bgVar.z)) {
                    }
                } else if (ShareConstants.IMAGE_URL.equals(akVar.b) && akVar.e == null) {
                }
                view = null;
                if (view == null) {
                }
                view2 = view;
                if (view2 == null) {
                }
            } else if (akVar.d.equalsIgnoreCase("card_scrollable")) {
                NativeScrollableContainer nativeScrollableContainer = (NativeScrollableContainer) this.d.a(c(), akVar, this.i);
                if (nativeScrollableContainer != null) {
                    this.m = ay.a(nativeScrollableContainer.getType(), this.g, this);
                    if (this.m != null) {
                        nativeScrollableContainer.a((am) akVar, this.m, this.b, d(), this);
                        nativeScrollableContainer.setLayoutParams(bf.a(akVar, viewGroup2));
                        a((View) nativeScrollableContainer, akVar);
                        viewGroup2.addView(nativeScrollableContainer);
                    }
                }
            } else {
                ViewGroup viewGroup3 = (ViewGroup) this.d.a(c(), akVar, this.i);
                if (viewGroup3 != null) {
                    ViewGroup b3 = b(viewGroup3, (am) akVar);
                    b3.setLayoutParams(bf.a(akVar, viewGroup2));
                    a((View) b3, akVar);
                    viewGroup2.addView(b3);
                }
            }
        }
        return viewGroup2;
    }

    private int d() {
        if (this.b == 0) {
            return GravityCompat.START;
        }
        if (this.g.b() - 1 == this.b) {
            return GravityCompat.END;
        }
        return 1;
    }

    /* access modifiers changed from: 0000 */
    public final void a() {
        this.o = true;
        this.f.clear();
        if (this.m != null) {
            this.m.destroy();
        }
    }

    private void a(final ak akVar, View view) {
        if (akVar.h) {
            view.setOnClickListener(new OnClickListener() {
                public final void onClick(View view) {
                    au.this.k.a(view, akVar);
                }
            });
        }
    }
}
