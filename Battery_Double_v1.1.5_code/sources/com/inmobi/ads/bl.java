package com.inmobi.ads;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.inmobi.ads.c.l;

/* compiled from: PollingVisibilityTracker */
class bl extends cf {
    private static final String d = "bl";
    @Nullable
    private l e;

    bl(@NonNull a aVar, @Nullable l lVar) {
        super(aVar);
        this.e = lVar;
    }

    /* access modifiers changed from: protected */
    public final int a() {
        if (this.e == null) {
            return 100;
        }
        return this.e.c;
    }

    /* access modifiers changed from: protected */
    public final void b() {
        g();
    }
}
