package com.inmobi.ads.a;

import android.support.annotation.Nullable;
import com.inmobi.ads.bn;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/* compiled from: AdAssetBatch */
public final class b {

    /* renamed from: a reason: collision with root package name */
    public List<a> f2112a = new ArrayList();
    Set<bn> b;
    Set<String> c = new HashSet();
    int d;
    int e;
    public String f;
    @Nullable
    public String g;
    public String h;
    private String i;
    private final WeakReference<g> j;

    public b(String str, String str2, Set<bn> set, g gVar) {
        this.i = str;
        this.f = str2;
        this.b = set;
        this.j = new WeakReference<>(gVar);
    }

    public b(String str, Set<bn> set, g gVar, String str2) {
        this.i = str;
        this.h = str2;
        this.b = set;
        this.j = new WeakReference<>(gVar);
    }

    @Nullable
    public final g a() {
        return (g) this.j.get();
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder("AdAssetBatch{mRawAssets=");
        sb.append(this.b);
        sb.append(", mBatchDownloadSuccessCount=");
        sb.append(this.d);
        sb.append(", mBatchDownloadFailureCount=");
        sb.append(this.e);
        sb.append('}');
        return sb.toString();
    }
}
