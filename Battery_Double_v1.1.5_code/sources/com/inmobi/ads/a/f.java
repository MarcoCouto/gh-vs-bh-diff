package com.inmobi.ads.a;

import android.annotation.TargetApi;
import android.os.Build.VERSION;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.webkit.URLUtil;
import com.inmobi.ads.a.a.C0039a;
import com.inmobi.ads.bi;
import com.inmobi.ads.bn;
import com.inmobi.ads.c.k;
import com.inmobi.commons.core.configs.b.c;
import com.inmobi.commons.core.network.d;
import com.inmobi.commons.core.utilities.g;
import com.squareup.picasso.Callback;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.io.BufferedOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;

/* compiled from: AssetStore */
public final class f implements c {
    public static final Object e = new Object();
    /* access modifiers changed from: private */
    public static final String f = "f";
    private static f o;
    private static final Object p = new Object();

    /* renamed from: a reason: collision with root package name */
    public d f2115a;
    public com.inmobi.ads.c.b b;
    public ExecutorService c;
    public AtomicBoolean d = new AtomicBoolean(false);
    private k g;
    private ExecutorService h;
    private a i;
    private HandlerThread j;
    private AtomicBoolean k = new AtomicBoolean(false);
    private ConcurrentHashMap<String, a> l;
    private com.inmobi.commons.core.utilities.g.b m;
    private com.inmobi.commons.core.utilities.g.b n;
    private List<b> q = new ArrayList();
    /* access modifiers changed from: private */
    public final e r = new e() {
        public final void a(@NonNull d dVar, @NonNull String str, @NonNull a aVar) {
            f.f;
            StringBuilder sb = new StringBuilder("Asset fetch succeeded for URL ");
            sb.append(aVar.d);
            sb.append(" Updating location on disk (file://");
            sb.append(str);
            sb.append(")");
            a a2 = new C0039a().a(aVar.d, str, dVar, f.this.b.f2218a, f.this.b.e).a();
            f.this.f2115a;
            d.b(a2);
            a2.k = aVar.k;
            a2.f2110a = aVar.f2110a;
            f.this.a(a2, true);
            try {
                f.c(f.this);
            } catch (Exception e) {
                f.f;
                com.inmobi.commons.core.a.a.a().a(new com.inmobi.commons.core.e.a(e));
            }
        }

        public final void a(@NonNull a aVar) {
            f.f;
            StringBuilder sb = new StringBuilder("Asset fetch failed for remote URL (");
            sb.append(aVar.d);
            sb.append(")");
            f.this.c(aVar.d);
            if (aVar.c <= 0) {
                f.f;
                f.this.a(aVar, false);
                f.this.f2115a;
                d.c(aVar);
            } else {
                f.f;
                aVar.f = System.currentTimeMillis();
                f.this.f2115a;
                d.b(aVar);
                if (!com.inmobi.commons.core.utilities.d.a()) {
                    f.this.a(aVar, false);
                }
            }
            try {
                f.c(f.this);
            } catch (Exception e) {
                f.f;
                com.inmobi.commons.core.a.a.a().a(new com.inmobi.commons.core.e.a(e));
            }
        }
    };

    /* compiled from: AssetStore */
    private static final class a extends Handler {
        /* access modifiers changed from: private */

        /* renamed from: a reason: collision with root package name */
        public WeakReference<f> f2122a;
        private final e b = new e() {
            public final void a(d dVar, String str, a aVar) {
                f fVar = (f) a.this.f2122a.get();
                if (fVar != null) {
                    f.f;
                    StringBuilder sb = new StringBuilder("Asset fetch succeeded for URL ");
                    sb.append(aVar.d);
                    sb.append(" Updating location on disk (file://");
                    sb.append(str);
                    sb.append(")");
                    a a2 = new C0039a().a(aVar.d, str, dVar, fVar.b.f2218a, fVar.b.e).a();
                    fVar.f2115a;
                    d.b(a2);
                    a2.k = aVar.k;
                    a2.f2110a = aVar.f2110a;
                    fVar.a(a2, true);
                    a.this.a();
                    return;
                }
                f.f;
            }

            public final void a(a aVar) {
                f fVar = (f) a.this.f2122a.get();
                if (fVar != null) {
                    f.f;
                    StringBuilder sb = new StringBuilder("Asset fetch failed for remote URL (");
                    sb.append(aVar.d);
                    sb.append(")");
                    fVar.c(aVar.d);
                    if (aVar.c > 0) {
                        aVar.c--;
                        aVar.f = System.currentTimeMillis();
                        fVar.f2115a;
                        d.b(aVar);
                        a.this.b();
                        return;
                    }
                    fVar.a(aVar, false);
                    a.this.a(aVar);
                    return;
                }
                f.f;
            }
        };

        a(@NonNull Looper looper, @NonNull f fVar) {
            super(looper);
            this.f2122a = new WeakReference<>(fVar);
        }

        public final void handleMessage(Message message) {
            try {
                f fVar = (f) this.f2122a.get();
                switch (message.what) {
                    case 1:
                        if (fVar != null) {
                            com.inmobi.ads.c.b h = fVar.b;
                            if (h == null) {
                                com.inmobi.ads.c cVar = new com.inmobi.ads.c();
                                com.inmobi.commons.core.configs.b.a().a((com.inmobi.commons.core.configs.a) cVar, (c) null);
                                h = cVar.n;
                            }
                            fVar.f2115a;
                            List e = d.e();
                            if (e.size() <= 0) {
                                f.f;
                                fVar.c();
                                return;
                            }
                            f.f;
                            a aVar = (a) e.get(0);
                            Iterator it = e.iterator();
                            while (true) {
                                if (it.hasNext()) {
                                    a aVar2 = (a) it.next();
                                    if (!fVar.l.containsKey(aVar.d)) {
                                        aVar = aVar2;
                                    }
                                }
                            }
                            Message obtain = Message.obtain();
                            obtain.what = 2;
                            long currentTimeMillis = System.currentTimeMillis() - aVar.f;
                            try {
                                if (currentTimeMillis < ((long) (h.b * 1000))) {
                                    sendMessageDelayed(obtain, ((long) (h.b * 1000)) - currentTimeMillis);
                                    return;
                                } else if (fVar.l.containsKey(aVar.d)) {
                                    sendMessageDelayed(obtain, (long) (h.b * 1000));
                                    return;
                                } else {
                                    f.f;
                                    Message obtain2 = Message.obtain();
                                    obtain2.what = 2;
                                    obtain2.obj = aVar.d;
                                    sendMessage(obtain2);
                                    return;
                                }
                            } catch (Exception e2) {
                                f.f;
                                new StringBuilder("Encountered unexpected error in Asset fetch handler").append(e2.getMessage());
                                return;
                            }
                        }
                        break;
                    case 2:
                        if (fVar != null) {
                            String str = (String) message.obj;
                            fVar.f2115a;
                            a b2 = d.b(str);
                            if (b2 == null) {
                                b();
                                return;
                            } else if (!b2.a()) {
                                int i = (fVar.b.f2218a - b2.c) + 1;
                                if (b2.c == 0) {
                                    b2.l = 11;
                                    fVar.a(b2, false);
                                    a(b2);
                                    return;
                                } else if (!com.inmobi.commons.core.utilities.d.a()) {
                                    fVar.a(b2, false);
                                    fVar.c();
                                    return;
                                } else if (fVar.a(b2, this.b)) {
                                    f.f;
                                    new StringBuilder("Cache miss in handler; attempting to cache asset: ").append(b2.d);
                                    f.f;
                                    StringBuilder sb = new StringBuilder("Download attempt # ");
                                    sb.append(i);
                                    sb.append(" in handler  to cache asset (");
                                    sb.append(b2.d);
                                    sb.append(")");
                                    return;
                                } else {
                                    f.f;
                                    new StringBuilder("Cache miss in handler; but already attempting: ").append(b2.d);
                                    b();
                                    return;
                                }
                            } else {
                                f.f;
                                a();
                                fVar.a(b2, true);
                                return;
                            }
                        }
                        break;
                    case 3:
                        break;
                    case 4:
                        if (fVar != null) {
                            a aVar3 = (a) message.obj;
                            fVar.f2115a;
                            d.c(aVar3);
                        }
                        b();
                        break;
                    default:
                        return;
                }
                b();
            } catch (Exception e3) {
                f.f;
                com.inmobi.commons.core.a.a.a().a(new com.inmobi.commons.core.e.a(e3));
            }
        }

        /* access modifiers changed from: private */
        public void a(a aVar) {
            try {
                Message obtain = Message.obtain();
                obtain.what = 4;
                obtain.obj = aVar;
                sendMessage(obtain);
            } catch (Exception e) {
                f.f;
                new StringBuilder("Encountered unexpected error in Asset fetch handler").append(e.getMessage());
            }
        }

        /* access modifiers changed from: private */
        public void a() {
            try {
                sendEmptyMessage(3);
            } catch (Exception e) {
                f.f;
                new StringBuilder("Encountered unexpected error in Asset fetch handler").append(e.getMessage());
            }
        }

        /* access modifiers changed from: private */
        public void b() {
            try {
                sendEmptyMessage(1);
            } catch (Exception e) {
                f.f;
                new StringBuilder("Encountered unexpected error in Asset fetch handler").append(e.getMessage());
            }
        }
    }

    /* compiled from: AssetStore */
    private class b implements InvocationHandler {
        private CountDownLatch b;
        private String c;

        b(CountDownLatch countDownLatch, String str) {
            this.b = countDownLatch;
            this.c = str;
        }

        public final Object invoke(Object obj, Method method, Object[] objArr) {
            f.f;
            new StringBuilder("Method invoked in PicassoInvocationHandler -").append(method);
            if (method != null) {
                if ("onSuccess".equalsIgnoreCase(method.getName())) {
                    f.this.a(this.c);
                    this.b.countDown();
                } else if ("onError".equalsIgnoreCase(method.getName())) {
                    f.this.b(this.c);
                    this.b.countDown();
                }
            }
            return null;
        }
    }

    private f() {
        com.inmobi.ads.c cVar = new com.inmobi.ads.c();
        com.inmobi.commons.core.configs.b.a().a((com.inmobi.commons.core.configs.a) cVar, (c) this);
        this.b = cVar.n;
        this.g = cVar.m;
        this.f2115a = d.a();
        this.c = Executors.newCachedThreadPool();
        this.h = Executors.newFixedThreadPool(1);
        this.j = new HandlerThread("assetFetcher");
        this.j.start();
        this.i = new a(this.j.getLooper(), this);
        this.m = new com.inmobi.commons.core.utilities.g.b() {
            public final void a(boolean z) {
                if (z) {
                    f.c(f.this);
                } else {
                    f.this.c();
                }
            }
        };
        if (VERSION.SDK_INT >= 23) {
            this.n = new com.inmobi.commons.core.utilities.g.b() {
                public final void a(boolean z) {
                    if (z) {
                        f.this.c();
                    } else {
                        f.c(f.this);
                    }
                }
            };
        }
        this.l = new ConcurrentHashMap<>(2, 0.9f, 2);
    }

    public static f a() {
        f fVar = o;
        if (fVar == null) {
            synchronized (p) {
                fVar = o;
                if (fVar == null) {
                    fVar = new f();
                    o = fVar;
                }
            }
        }
        return fVar;
    }

    public final void a(com.inmobi.commons.core.configs.a aVar) {
        com.inmobi.ads.c cVar = (com.inmobi.ads.c) aVar;
        this.b = cVar.n;
        this.g = cVar.m;
    }

    /* access modifiers changed from: private */
    public synchronized void a(String str) {
        boolean z;
        for (int i2 = 0; i2 < this.q.size(); i2++) {
            b bVar = (b) this.q.get(i2);
            Set<bn> set = bVar.b;
            Set<String> set2 = bVar.c;
            Iterator it = set.iterator();
            while (true) {
                if (it.hasNext()) {
                    if (((bn) it.next()).b.equals(str)) {
                        z = true;
                        break;
                    }
                } else {
                    z = false;
                    break;
                }
            }
            if (z && !set2.contains(str)) {
                bVar.c.add(str);
                bVar.d++;
            }
        }
    }

    /* access modifiers changed from: private */
    public synchronized void b(String str) {
        boolean z;
        for (int i2 = 0; i2 < this.q.size(); i2++) {
            b bVar = (b) this.q.get(i2);
            Iterator it = bVar.b.iterator();
            while (true) {
                if (it.hasNext()) {
                    if (((bn) it.next()).b.equals(str)) {
                        z = true;
                        break;
                    }
                } else {
                    z = false;
                    break;
                }
            }
            if (z) {
                bVar.e++;
            }
        }
    }

    private synchronized void b(a aVar) {
        boolean z;
        for (int i2 = 0; i2 < this.q.size(); i2++) {
            b bVar = (b) this.q.get(i2);
            Iterator it = bVar.b.iterator();
            while (true) {
                if (it.hasNext()) {
                    if (((bn) it.next()).b.equals(aVar.d)) {
                        z = true;
                        break;
                    }
                } else {
                    z = false;
                    break;
                }
            }
            if (z && !bVar.f2112a.contains(aVar)) {
                bVar.f2112a.add(aVar);
            }
        }
    }

    /* access modifiers changed from: private */
    public synchronized void b(b bVar) {
        if (!this.q.contains(bVar)) {
            this.q.add(bVar);
        }
    }

    private synchronized void a(List<b> list) {
        int size = list.size();
        for (int i2 = 0; i2 < size; i2++) {
            this.q.remove(list.get(i2));
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x011b, code lost:
        r8.l = 7;
        r8.c = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x0123, code lost:
        if (r0.exists() == false) goto L_0x0128;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x0125, code lost:
        r0.delete();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x0128, code lost:
        r4.disconnect();
        com.inmobi.commons.core.utilities.d.a((java.io.Closeable) r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x012f, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:?, code lost:
        com.inmobi.commons.core.a.a.a().a(new com.inmobi.commons.core.e.a(r0));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x0194, code lost:
        r8.l = 8;
        r10.f2113a.a(r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x019d, code lost:
        r8.l = 8;
        r10.f2113a.a(r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x01a6, code lost:
        r8.l = 3;
        r10.f2113a.a(r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x01b0, code lost:
        r8.l = 4;
        r10.f2113a.a(r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x01ba, code lost:
        r8.l = 4;
        r10.f2113a.a(r8);
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:53:? A[ExcHandler: IOException (unused java.io.IOException), SYNTHETIC, Splitter:B:11:0x0063] */
    /* JADX WARNING: Removed duplicated region for block: B:55:? A[ExcHandler: ProtocolException (unused java.net.ProtocolException), SYNTHETIC, Splitter:B:11:0x0063] */
    /* JADX WARNING: Removed duplicated region for block: B:57:? A[ExcHandler: MalformedURLException (unused java.net.MalformedURLException), SYNTHETIC, Splitter:B:11:0x0063] */
    /* JADX WARNING: Removed duplicated region for block: B:59:? A[ExcHandler: FileNotFoundException (unused java.io.FileNotFoundException), SYNTHETIC, Splitter:B:11:0x0063] */
    /* JADX WARNING: Removed duplicated region for block: B:61:? A[ExcHandler: SocketTimeoutException (unused java.net.SocketTimeoutException), SYNTHETIC, Splitter:B:11:0x0063] */
    public boolean a(a aVar, e eVar) {
        c cVar;
        long elapsedRealtime;
        long j2;
        boolean z;
        a aVar2 = aVar;
        if (((a) this.l.putIfAbsent(aVar2.d, aVar2)) != null) {
            return false;
        }
        cVar = new c(eVar);
        long j3 = this.g.c;
        ArrayList<String> arrayList = this.g.e;
        StringBuilder sb = new StringBuilder("Fetching asset (");
        sb.append(aVar2.d);
        sb.append(")");
        if (!com.inmobi.commons.core.utilities.d.a()) {
            aVar2.l = 8;
            cVar.f2113a.a(aVar2);
        } else if (aVar2.d.equals("") || !URLUtil.isValidUrl(aVar2.d)) {
            aVar2.l = 3;
            cVar.f2113a.a(aVar2);
        } else {
            String[] strArr = (String[]) arrayList.toArray(new String[arrayList.size()]);
            try {
                elapsedRealtime = SystemClock.elapsedRealtime();
                HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(aVar2.d).openConnection();
                httpURLConnection.setRequestMethod(HttpRequest.METHOD_GET);
                httpURLConnection.setConnectTimeout(60000);
                httpURLConnection.setReadTimeout(60000);
                if (httpURLConnection.getResponseCode() < 400) {
                    String contentType = httpURLConnection.getContentType();
                    int length = strArr.length;
                    int i2 = 0;
                    while (true) {
                        if (i2 >= length) {
                            z = false;
                            break;
                        }
                        String str = strArr[i2];
                        if (contentType != null && str.toLowerCase(Locale.ENGLISH).equals(contentType.toLowerCase(Locale.ENGLISH))) {
                            z = true;
                            break;
                        }
                        i2++;
                    }
                    if (!z) {
                        aVar2.l = 6;
                        aVar2.c = 0;
                        cVar.f2113a.a(aVar2);
                    }
                }
                long contentLength = (long) httpURLConnection.getContentLength();
                if (contentLength >= 0) {
                    StringBuilder sb2 = new StringBuilder("ContentSize: ");
                    sb2.append(contentLength);
                    sb2.append(" max size: ");
                    sb2.append(j3);
                    if (contentLength > j3) {
                        aVar2.l = 7;
                        aVar2.c = 0;
                        cVar.f2113a.a(aVar2);
                    }
                }
                httpURLConnection.connect();
                File a2 = com.inmobi.commons.a.a.a(aVar2.d);
                if (a2.exists()) {
                    a2.delete();
                }
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(a2));
                byte[] bArr = new byte[1024];
                j2 = 0;
                while (true) {
                    int read = inputStream.read(bArr);
                    if (read <= 0) {
                        bufferedOutputStream.flush();
                        httpURLConnection.disconnect();
                        com.inmobi.commons.core.utilities.d.a((Closeable) bufferedOutputStream);
                        long elapsedRealtime2 = SystemClock.elapsedRealtime();
                        c.a(elapsedRealtime, j2, elapsedRealtime2);
                        d dVar = new d();
                        dVar.d = httpURLConnection.getHeaderFields();
                        aVar2.k = c.a(aVar, a2, elapsedRealtime, elapsedRealtime2);
                        aVar2.f2110a = elapsedRealtime2 - elapsedRealtime;
                        cVar.f2113a.a(dVar, a2.getAbsolutePath(), aVar2);
                        break;
                    }
                    j2 += (long) read;
                    if (j2 > j3) {
                        break;
                    }
                    bufferedOutputStream.write(bArr, 0, read);
                }
            } catch (SocketTimeoutException unused) {
            } catch (FileNotFoundException unused2) {
            } catch (MalformedURLException unused3) {
            } catch (ProtocolException unused4) {
            } catch (IOException unused5) {
            } catch (Exception unused6) {
                aVar2.l = 0;
                cVar.f2113a.a(aVar2);
            }
        }
        return true;
        c.a(elapsedRealtime, j2, SystemClock.elapsedRealtime());
        cVar.f2113a.a(aVar2);
        return true;
    }

    /* access modifiers changed from: private */
    public void c(String str) {
        this.l.remove(str);
    }

    /* access modifiers changed from: private */
    public synchronized void a(@NonNull a aVar, boolean z) {
        b(aVar);
        c(aVar.d);
        if (z) {
            a(aVar.d);
            e();
            return;
        }
        b(aVar.d);
        f();
    }

    /* access modifiers changed from: private */
    public synchronized void e() {
        ArrayList arrayList = new ArrayList();
        for (int i2 = 0; i2 < this.q.size(); i2++) {
            b bVar = (b) this.q.get(i2);
            if (bVar.d == bVar.b.size()) {
                try {
                    g a2 = bVar.a();
                    if (a2 != null) {
                        a2.b(bVar);
                    }
                    arrayList.add(bVar);
                } catch (Exception e2) {
                    new StringBuilder("Encountered unexpected error in onAssetFetchSucceeded handler: ").append(e2.getMessage());
                    com.inmobi.commons.core.a.a.a().a(new com.inmobi.commons.core.e.a(e2));
                }
            }
        }
        a((List<b>) arrayList);
    }

    /* access modifiers changed from: private */
    public synchronized void f() {
        ArrayList arrayList = new ArrayList();
        for (int i2 = 0; i2 < this.q.size(); i2++) {
            b bVar = (b) this.q.get(i2);
            if (bVar.e > 0) {
                try {
                    g a2 = bVar.a();
                    if (a2 != null) {
                        a2.a(bVar);
                    }
                    arrayList.add(bVar);
                } catch (Exception e2) {
                    new StringBuilder("Encountered unexpected error in onAssetFetchFailed handler: ").append(e2.getMessage());
                    com.inmobi.commons.core.a.a.a().a(new com.inmobi.commons.core.e.a(e2));
                }
            }
        }
        a((List<b>) arrayList);
    }

    public final void b() {
        this.d.set(false);
        if (!com.inmobi.commons.core.utilities.d.a()) {
            g();
            h();
            return;
        }
        synchronized (e) {
            if (this.k.compareAndSet(false, true)) {
                if (this.j == null) {
                    this.j = new HandlerThread("assetFetcher");
                    this.j.start();
                }
                if (this.i == null) {
                    this.i = new a(this.j.getLooper(), this);
                }
                if (d.e().isEmpty()) {
                    c();
                } else {
                    g();
                    h();
                    this.i.sendEmptyMessage(1);
                }
            }
        }
    }

    public static void a(a aVar) {
        d.c(aVar);
        File file = new File(aVar.e);
        if (file.exists()) {
            file.delete();
        }
    }

    public final void a(final b bVar) {
        this.c.execute(new Runnable() {
            public final void run() {
                f.this.b(bVar);
                f.f;
                StringBuilder sb = new StringBuilder("Attempting to cache ");
                sb.append(bVar.b.size());
                sb.append("remote URLs ");
                ArrayList arrayList = new ArrayList();
                ArrayList<String> arrayList2 = new ArrayList<>();
                for (bn bnVar : bVar.b) {
                    if (bnVar.b.trim().length() <= 0 || bnVar.f2202a != 2) {
                        arrayList2.add(bnVar.b);
                    } else {
                        arrayList.add(bnVar.b);
                    }
                }
                f.a(f.this, (List) arrayList);
                f.this.e();
                f.this.f();
                for (String b2 : arrayList2) {
                    f.b(f.this, b2);
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void c(a aVar) {
        File file = new File(aVar.e);
        long min = Math.min(System.currentTimeMillis() + (aVar.h - aVar.f), System.currentTimeMillis() + (this.b.e * 1000));
        C0039a aVar2 = new C0039a();
        String str = aVar.d;
        String str2 = aVar.e;
        int i2 = this.b.f2218a;
        long j2 = aVar.i;
        aVar2.c = str;
        aVar2.d = str2;
        aVar2.b = i2;
        aVar2.g = min;
        aVar2.h = j2;
        a a2 = aVar2.a();
        a2.f = System.currentTimeMillis();
        d.b(a2);
        a2.k = c.a(aVar, file, aVar.f, aVar.f);
        a2.j = true;
        a(a2, true);
    }

    @TargetApi(23)
    private void g() {
        g.a();
        com.inmobi.commons.core.utilities.g.b bVar = this.m;
        if (VERSION.SDK_INT < 28) {
            g.a(bVar, "android.net.conn.CONNECTIVITY_CHANGE");
        } else {
            g.a(bVar, "SYSTEM_CONNECTIVITY_CHANGE");
        }
        if (VERSION.SDK_INT >= 23) {
            g.a();
            g.a(this.n, "android.os.action.DEVICE_IDLE_MODE_CHANGED");
        }
    }

    @TargetApi(23)
    private void h() {
        g.a().a(this.m);
        if (VERSION.SDK_INT >= 23) {
            g.a().a("android.os.action.DEVICE_IDLE_MODE_CHANGED", this.n);
        }
    }

    public final void c() {
        synchronized (e) {
            this.k.set(false);
            this.l.clear();
            if (this.j != null) {
                this.j.getLooper().quit();
                this.j.interrupt();
                this.j = null;
                this.i = null;
            }
        }
    }

    static /* synthetic */ void c(f fVar) {
        if (!fVar.d.get()) {
            fVar.b();
        }
    }

    static /* synthetic */ void b(f fVar, final String str) {
        a a2 = d.a(str);
        if (a2 == null || !a2.a()) {
            a a3 = new C0039a().a(str, fVar.b.f2218a, fVar.b.e).a();
            if (d.a(str) == null) {
                fVar.f2115a.a(a3);
            }
            fVar.h.execute(new Runnable() {
                public final void run() {
                    f.this.f2115a;
                    a a2 = d.a(str);
                    if (a2 != null) {
                        if (a2.a()) {
                            f.this.c(a2);
                        } else if (f.this.a(a2, f.this.r)) {
                            f.f;
                            new StringBuilder("Cache miss; attempting to cache asset: ").append(str);
                        } else {
                            f.f;
                            new StringBuilder("Cache miss; but already attempting: ").append(str);
                        }
                    }
                }
            });
            return;
        }
        StringBuilder sb = new StringBuilder("Cache hit; file exists location on disk (");
        sb.append(a2.e);
        sb.append(")");
        fVar.c(a2);
    }

    /* JADX WARNING: Incorrect type for immutable var: ssa=java.util.List, code=java.util.List<java.lang.String>, for r5v0, types: [java.util.List, java.util.List<java.lang.String>] */
    static /* synthetic */ void a(f fVar, List<String> list) {
        CountDownLatch countDownLatch = new CountDownLatch(list.size());
        for (String str : list) {
            try {
                bi.a(com.inmobi.commons.a.a.b()).load(str).fetch((Callback) bi.a((InvocationHandler) new b(countDownLatch, str)));
            } catch (Exception unused) {
                countDownLatch.countDown();
            }
        }
        try {
            countDownLatch.await();
        } catch (InterruptedException unused2) {
        }
    }
}
