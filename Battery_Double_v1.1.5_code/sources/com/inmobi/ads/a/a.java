package com.inmobi.ads.a;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.inmobi.commons.core.network.d;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;

/* compiled from: AdAsset */
public class a {
    /* access modifiers changed from: private */
    public static final String m = "a";

    /* renamed from: a reason: collision with root package name */
    public long f2110a = 0;
    int b;
    int c;
    public String d;
    public String e;
    long f;
    long g;
    public long h;
    long i;
    public boolean j;
    public String k;
    public int l;

    /* renamed from: com.inmobi.ads.a.a$a reason: collision with other inner class name */
    /* compiled from: AdAsset */
    public static final class C0039a {

        /* renamed from: a reason: collision with root package name */
        int f2111a = (new Random().nextInt() & Integer.MAX_VALUE);
        int b;
        String c;
        String d;
        long e = System.currentTimeMillis();
        long f = System.currentTimeMillis();
        long g;
        long h;

        public final C0039a a(String str, int i, long j) {
            this.c = str;
            this.b = i;
            this.g = System.currentTimeMillis() + j;
            return this;
        }

        private static long a(String str) {
            try {
                return new SimpleDateFormat("EEE,dd MMM yyyy HH:mm:ss z", Locale.ENGLISH).parse(str).getTime();
            } catch (ParseException e2) {
                com.inmobi.commons.core.a.a.a().a(new com.inmobi.commons.core.e.a(e2));
                return 0;
            }
        }

        @NonNull
        public final a a() {
            a aVar = new a(this.f2111a, this.c, this.d, this.b, this.e, this.f, this.g, this.h);
            return aVar;
        }

        /* JADX WARNING: Removed duplicated region for block: B:14:0x0053  */
        /* JADX WARNING: Removed duplicated region for block: B:50:0x00f1  */
        /* JADX WARNING: Removed duplicated region for block: B:58:0x0117  */
        /* JADX WARNING: Removed duplicated region for block: B:66:0x0139  */
        /* JADX WARNING: Removed duplicated region for block: B:71:0x014c  */
        public final C0039a a(String str, String str2, d dVar, int i, long j) {
            long j2;
            boolean z;
            long j3;
            boolean z2;
            long j4;
            long j5;
            long j6;
            String str3;
            String str4;
            String str5;
            String str6;
            Map<String, List<String>> map = dVar.d;
            long currentTimeMillis = System.currentTimeMillis();
            List list = (List) map.get(HttpRequest.HEADER_DATE);
            long j7 = 0;
            if (list != null && list.size() > 0) {
                String str7 = (String) ((List) map.get(HttpRequest.HEADER_DATE)).get(0);
                if (str7 != null) {
                    j2 = a(str7);
                    List list2 = (List) map.get(HttpRequest.HEADER_CACHE_CONTROL);
                    if (list2 != null && list2.size() > 0) {
                        str6 = (String) ((List) map.get(HttpRequest.HEADER_CACHE_CONTROL)).get(0);
                        if (str6 != null) {
                            j4 = 0;
                            j3 = 0;
                            z2 = false;
                            for (String trim : str6.split(",")) {
                                String trim2 = trim.trim();
                                if (!trim2.equals("no-cache") && !trim2.equals("no-store")) {
                                    if (trim2.startsWith("max-age=")) {
                                        try {
                                            j4 = Long.parseLong(trim2.substring(8));
                                        } catch (Exception e2) {
                                            a.m;
                                            e2.getMessage();
                                        }
                                    } else if (trim2.startsWith("stale-while-revalidate=")) {
                                        try {
                                            j3 = Long.parseLong(trim2.substring(23));
                                        } catch (Exception e3) {
                                            a.m;
                                            e3.getMessage();
                                        }
                                    } else if (trim2.equals("must-revalidate") || trim2.equals("proxy-revalidate")) {
                                        z2 = true;
                                    }
                                }
                            }
                            z = true;
                            List list3 = (List) map.get(HttpRequest.HEADER_EXPIRES);
                            if (list3 != null && list3.size() > 0) {
                                str5 = (String) ((List) map.get(HttpRequest.HEADER_EXPIRES)).get(0);
                                if (str5 != null) {
                                    j5 = a(str5);
                                    List list4 = (List) map.get(HttpRequest.HEADER_LAST_MODIFIED);
                                    if (list4 != null && list4.size() > 0) {
                                        str4 = (String) ((List) map.get(HttpRequest.HEADER_LAST_MODIFIED)).get(0);
                                        if (str4 != null) {
                                            a(str4);
                                        }
                                    }
                                    List list5 = (List) map.get(HttpRequest.HEADER_ETAG);
                                    if (list5 != null && list5.size() > 0) {
                                        ((List) map.get(HttpRequest.HEADER_ETAG)).get(0);
                                    }
                                    if (!z) {
                                        j7 = currentTimeMillis + (j4 * 1000);
                                        str3 = str;
                                        j6 = z2 ? j7 : j7 + (j3 * 1000);
                                    } else {
                                        if (j2 > 0 && j5 >= j2) {
                                            j7 = currentTimeMillis + (j5 - j2);
                                        }
                                        str3 = str;
                                        j6 = j7;
                                    }
                                    this.c = str3;
                                    this.d = str2;
                                    this.b = i;
                                    this.g = currentTimeMillis + (j * 1000);
                                    this.h = j7;
                                    this.g = Math.min(this.g, j6);
                                    return this;
                                }
                            }
                            j5 = 0;
                            List list42 = (List) map.get(HttpRequest.HEADER_LAST_MODIFIED);
                            str4 = (String) ((List) map.get(HttpRequest.HEADER_LAST_MODIFIED)).get(0);
                            if (str4 != null) {
                            }
                            List list52 = (List) map.get(HttpRequest.HEADER_ETAG);
                            ((List) map.get(HttpRequest.HEADER_ETAG)).get(0);
                            if (!z) {
                            }
                            this.c = str3;
                            this.d = str2;
                            this.b = i;
                            this.g = currentTimeMillis + (j * 1000);
                            this.h = j7;
                            this.g = Math.min(this.g, j6);
                            return this;
                        }
                    }
                    j4 = 0;
                    j3 = 0;
                    z2 = false;
                    z = false;
                    List list32 = (List) map.get(HttpRequest.HEADER_EXPIRES);
                    str5 = (String) ((List) map.get(HttpRequest.HEADER_EXPIRES)).get(0);
                    if (str5 != null) {
                    }
                    j5 = 0;
                    List list422 = (List) map.get(HttpRequest.HEADER_LAST_MODIFIED);
                    str4 = (String) ((List) map.get(HttpRequest.HEADER_LAST_MODIFIED)).get(0);
                    if (str4 != null) {
                    }
                    List list522 = (List) map.get(HttpRequest.HEADER_ETAG);
                    ((List) map.get(HttpRequest.HEADER_ETAG)).get(0);
                    if (!z) {
                    }
                    this.c = str3;
                    this.d = str2;
                    this.b = i;
                    this.g = currentTimeMillis + (j * 1000);
                    this.h = j7;
                    this.g = Math.min(this.g, j6);
                    return this;
                }
            }
            j2 = 0;
            List list22 = (List) map.get(HttpRequest.HEADER_CACHE_CONTROL);
            str6 = (String) ((List) map.get(HttpRequest.HEADER_CACHE_CONTROL)).get(0);
            if (str6 != null) {
            }
            j4 = 0;
            j3 = 0;
            z2 = false;
            z = false;
            List list322 = (List) map.get(HttpRequest.HEADER_EXPIRES);
            str5 = (String) ((List) map.get(HttpRequest.HEADER_EXPIRES)).get(0);
            if (str5 != null) {
            }
            j5 = 0;
            List list4222 = (List) map.get(HttpRequest.HEADER_LAST_MODIFIED);
            str4 = (String) ((List) map.get(HttpRequest.HEADER_LAST_MODIFIED)).get(0);
            if (str4 != null) {
            }
            List list5222 = (List) map.get(HttpRequest.HEADER_ETAG);
            ((List) map.get(HttpRequest.HEADER_ETAG)).get(0);
            if (!z) {
            }
            this.c = str3;
            this.d = str2;
            this.b = i;
            this.g = currentTimeMillis + (j * 1000);
            this.h = j7;
            this.g = Math.min(this.g, j6);
            return this;
        }
    }

    a(int i2, @NonNull String str, @Nullable String str2, int i3, long j2, long j3, long j4, long j5) {
        this.b = i2;
        this.d = str;
        this.e = str2;
        this.c = i3;
        this.f = j2;
        this.g = j3;
        this.h = j4;
        this.i = j5;
        this.j = false;
        this.k = null;
    }

    public final boolean a() {
        return (this.e == null || this.e.length() == 0 || !new File(this.e).exists()) ? false : true;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        return this.d.equals(((a) obj).d);
    }

    public int hashCode() {
        return this.d.hashCode();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("AdAsset{url='");
        sb.append(this.d);
        sb.append('\'');
        sb.append('}');
        return sb.toString();
    }
}
