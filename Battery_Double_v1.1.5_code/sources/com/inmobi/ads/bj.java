package com.inmobi.ads;

import android.content.ContentValues;
import com.inmobi.ads.InMobiAdRequest.MonetizationContext;
import com.inmobi.ads.d.a;
import com.vungle.warren.model.AdvertisementDBAdapter.AdvertisementColumns;
import java.util.Map;

/* compiled from: Placement */
public final class bj {

    /* renamed from: a reason: collision with root package name */
    public long f2198a;
    public String b;
    public Map<String, String> c;
    public String d;
    String e;
    public MonetizationContext f = MonetizationContext.MONETIZATION_CONTEXT_ACTIVITY;

    public static bj a(long j, Map<String, String> map, String str, String str2) {
        bj bjVar = new bj(j, a.a(map), str);
        bjVar.d = str2;
        bjVar.c = map;
        return bjVar;
    }

    private bj(long j, String str, String str2) {
        this.f2198a = j;
        this.b = str;
        this.e = str2;
        if (this.b == null) {
            this.b = "";
        }
    }

    public bj(ContentValues contentValues) {
        this.f2198a = contentValues.getAsLong(AdvertisementColumns.COLUMN_PLACEMENT_ID).longValue();
        this.b = contentValues.getAsString("tp_key");
        this.e = contentValues.getAsString("ad_type");
        this.f = MonetizationContext.a(contentValues.getAsString("m10_context"));
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        bj bjVar = (bj) obj;
        return this.f2198a == bjVar.f2198a && this.f == bjVar.f && this.b.equals(bjVar.b) && this.e.equals(bjVar.e);
    }

    public final int hashCode() {
        return (((((int) (this.f2198a ^ (this.f2198a >>> 32))) * 31) + this.e.hashCode()) * 30) + this.f.hashCode();
    }
}
