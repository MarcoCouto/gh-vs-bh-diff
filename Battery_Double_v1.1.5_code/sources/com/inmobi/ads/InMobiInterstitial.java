package com.inmobi.ads;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.support.v7.widget.RecyclerView;
import com.inmobi.ads.InMobiAdRequest.MonetizationContext;
import com.inmobi.ads.InMobiAdRequestStatus.StatusCode;
import com.inmobi.ads.ac;
import com.inmobi.ads.bj;
import com.inmobi.ads.i.b;
import com.inmobi.ads.i.e;
import com.inmobi.ads.listeners.InterstitialAdEventListener;
import com.inmobi.commons.core.utilities.Logger;
import com.inmobi.commons.core.utilities.Logger.InternalLogLevel;
import com.ironsource.sdk.constants.Constants.ParametersKeys;
import com.mintegral.msdk.base.entity.CampaignUnit;
import com.squareup.picasso.Picasso;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import org.json.JSONObject;

public final class InMobiInterstitial {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public static final String f2074a = "InMobiInterstitial";
    /* access modifiers changed from: private */
    public static ConcurrentHashMap<ac, ArrayList<WeakReference<InterstitialAdRequestListener>>> m = new ConcurrentHashMap<>(2, 0.9f, 3);
    @Nullable
    private ac b;
    /* access modifiers changed from: private */
    public a c;
    /* access modifiers changed from: private */
    public InterstitialAdListener2 d;
    /* access modifiers changed from: private */
    public InterstitialAdEventListener e;
    private Context f;
    /* access modifiers changed from: private */
    public long g;
    private boolean h;
    /* access modifiers changed from: private */
    public String i;
    /* access modifiers changed from: private */
    public Map<String, String> j;
    private boolean k;
    private boolean l;
    /* access modifiers changed from: private */
    public String n;
    private j o;
    /* access modifiers changed from: private */
    public JSONObject p;
    @NonNull
    private final b q;

    /* renamed from: com.inmobi.ads.InMobiInterstitial$3 reason: invalid class name */
    static /* synthetic */ class AnonymousClass3 {

        /* renamed from: a reason: collision with root package name */
        static final /* synthetic */ int[] f2078a = new int[StatusCode.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(12:0|1|2|3|4|5|6|7|8|9|10|(3:11|12|14)) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x0040 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0035 */
        static {
            f2078a[StatusCode.NETWORK_UNREACHABLE.ordinal()] = 1;
            f2078a[StatusCode.REQUEST_PENDING.ordinal()] = 2;
            f2078a[StatusCode.AD_ACTIVE.ordinal()] = 3;
            f2078a[StatusCode.EARLY_REFRESH_REQUEST.ordinal()] = 4;
            f2078a[StatusCode.MISSING_REQUIRED_DEPENDENCIES.ordinal()] = 5;
            try {
                f2078a[StatusCode.MONETIZATION_DISABLED.ordinal()] = 6;
            } catch (NoSuchFieldError unused) {
            }
        }
    }

    public interface InterstitialAdListener2 {
        void onAdDismissed(InMobiInterstitial inMobiInterstitial);

        void onAdDisplayFailed(InMobiInterstitial inMobiInterstitial);

        void onAdDisplayed(InMobiInterstitial inMobiInterstitial);

        void onAdInteraction(InMobiInterstitial inMobiInterstitial, Map<Object, Object> map);

        void onAdLoadFailed(InMobiInterstitial inMobiInterstitial, InMobiAdRequestStatus inMobiAdRequestStatus);

        void onAdLoadSucceeded(InMobiInterstitial inMobiInterstitial);

        void onAdReceived(InMobiInterstitial inMobiInterstitial);

        void onAdRewardActionCompleted(InMobiInterstitial inMobiInterstitial, Map<Object, Object> map);

        void onAdWillDisplay(InMobiInterstitial inMobiInterstitial);

        void onUserLeftApplication(InMobiInterstitial inMobiInterstitial);
    }

    public interface InterstitialAdRequestListener {
        void onAdRequestCompleted(InMobiAdRequestStatus inMobiAdRequestStatus, InMobiInterstitial inMobiInterstitial);
    }

    @VisibleForTesting
    static final class a extends Handler {

        /* renamed from: a reason: collision with root package name */
        private WeakReference<InMobiInterstitial> f2079a;

        a(InMobiInterstitial inMobiInterstitial) {
            super(Looper.getMainLooper());
            this.f2079a = new WeakReference<>(inMobiInterstitial);
        }

        public final void handleMessage(Message message) {
            InMobiInterstitial inMobiInterstitial = (InMobiInterstitial) this.f2079a.get();
            if (inMobiInterstitial != null) {
                try {
                    Map map = null;
                    switch (message.what) {
                        case 1:
                            InMobiAdRequestStatus inMobiAdRequestStatus = (InMobiAdRequestStatus) message.obj;
                            if (inMobiInterstitial.e != null) {
                                inMobiInterstitial.e.onAdLoadFailed(inMobiInterstitial, inMobiAdRequestStatus);
                            }
                            if (inMobiInterstitial.d != null) {
                                inMobiInterstitial.d.onAdLoadFailed(inMobiInterstitial, inMobiAdRequestStatus);
                                return;
                            }
                            break;
                        case 2:
                            if (message.getData().getBoolean(ParametersKeys.AVAILABLE)) {
                                if (inMobiInterstitial.e != null) {
                                    inMobiInterstitial.e.onAdReceived(inMobiInterstitial);
                                }
                                if (inMobiInterstitial.d != null) {
                                    inMobiInterstitial.d.onAdReceived(inMobiInterstitial);
                                    return;
                                }
                            }
                            break;
                        case 3:
                            if (inMobiInterstitial.e != null) {
                                inMobiInterstitial.e.onAdLoadSucceeded(inMobiInterstitial);
                            }
                            if (inMobiInterstitial.d != null) {
                                inMobiInterstitial.d.onAdLoadSucceeded(inMobiInterstitial);
                                return;
                            }
                            break;
                        case 4:
                            if (message.obj != null) {
                                map = (Map) message.obj;
                            }
                            if (inMobiInterstitial.e != null) {
                                inMobiInterstitial.e.onRewardsUnlocked(inMobiInterstitial, map);
                            }
                            if (inMobiInterstitial.d != null) {
                                inMobiInterstitial.d.onAdRewardActionCompleted(inMobiInterstitial, map);
                                return;
                            }
                            break;
                        case 5:
                            if (inMobiInterstitial.e != null) {
                                inMobiInterstitial.e.onAdDisplayFailed(inMobiInterstitial);
                            }
                            if (inMobiInterstitial.d != null) {
                                inMobiInterstitial.d.onAdDisplayFailed(inMobiInterstitial);
                                return;
                            }
                            break;
                        case 6:
                            if (inMobiInterstitial.e != null) {
                                inMobiInterstitial.e.onAdWillDisplay(inMobiInterstitial);
                            }
                            if (inMobiInterstitial.d != null) {
                                inMobiInterstitial.d.onAdWillDisplay(inMobiInterstitial);
                                return;
                            }
                            break;
                        case 7:
                            if (inMobiInterstitial.e != null) {
                                inMobiInterstitial.e.onAdDisplayed(inMobiInterstitial);
                            }
                            if (inMobiInterstitial.d != null) {
                                inMobiInterstitial.d.onAdDisplayed(inMobiInterstitial);
                                return;
                            }
                            break;
                        case 9:
                            if (message.obj != null) {
                                map = (Map) message.obj;
                            }
                            if (inMobiInterstitial.e != null) {
                                inMobiInterstitial.e.onAdClicked(inMobiInterstitial, map);
                            }
                            if (inMobiInterstitial.d != null) {
                                inMobiInterstitial.d.onAdInteraction(inMobiInterstitial, map);
                                return;
                            }
                            break;
                        case 10:
                            if (inMobiInterstitial.e != null) {
                                inMobiInterstitial.e.onAdDismissed(inMobiInterstitial);
                            }
                            if (inMobiInterstitial.d != null) {
                                inMobiInterstitial.d.onAdDismissed(inMobiInterstitial);
                                return;
                            }
                            break;
                        case 11:
                            if (inMobiInterstitial.e != null) {
                                inMobiInterstitial.e.onUserLeftApplication(inMobiInterstitial);
                            }
                            if (inMobiInterstitial.d != null) {
                                inMobiInterstitial.d.onUserLeftApplication(inMobiInterstitial);
                                return;
                            }
                            break;
                        case 12:
                            if (inMobiInterstitial.e != null) {
                                inMobiInterstitial.e.onRequestPayloadCreated((byte[]) message.obj);
                                return;
                            }
                            break;
                        case 13:
                            if (inMobiInterstitial.e != null) {
                                inMobiInterstitial.e.onRequestPayloadCreationFailed((InMobiAdRequestStatus) message.obj);
                                return;
                            }
                            break;
                        default:
                            InMobiInterstitial.f2074a;
                            break;
                    }
                } catch (Exception e) {
                    Logger.a(InternalLogLevel.ERROR, InMobiInterstitial.f2074a, "Publisher handler caused unexpected error");
                    InMobiInterstitial.f2074a;
                    new StringBuilder("Callback threw unexpected error: ").append(e.getMessage());
                }
            }
        }
    }

    /* synthetic */ InMobiInterstitial(Context context, long j2, byte b2) {
        this(context, j2);
    }

    @Deprecated
    public InMobiInterstitial(Context context, long j2, InterstitialAdListener2 interstitialAdListener2) {
        this.h = false;
        this.l = false;
        this.n = "";
        this.q = new b() {
            public final void a(boolean z) {
                Message obtain = Message.obtain();
                obtain.what = 2;
                Bundle bundle = new Bundle();
                bundle.putBoolean(ParametersKeys.AVAILABLE, z);
                obtain.setData(bundle);
                InMobiInterstitial.this.c.sendMessage(obtain);
            }

            public final void a(i iVar) {
                InMobiInterstitial.this.a("AR", "");
                InMobiInterstitial.this.n = iVar.x;
                InMobiInterstitial.this.p = iVar.i;
                InMobiInterstitial.this.c.sendEmptyMessage(3);
            }

            public final void a(InMobiAdRequestStatus inMobiAdRequestStatus) {
                switch (AnonymousClass3.f2078a[inMobiAdRequestStatus.getStatusCode().ordinal()]) {
                    case 1:
                        InMobiInterstitial.this.a("ART", "NetworkNotAvailable");
                        break;
                    case 2:
                        InMobiInterstitial.this.a("ART", "LoadInProgress");
                        break;
                    case 3:
                        InMobiInterstitial.this.a("ART", "ReloadNotPermitted");
                        break;
                    case 4:
                        InMobiInterstitial.this.a("ART", "FrequentRequests");
                        break;
                    case 5:
                        InMobiInterstitial.this.a("ART", "MissingRequiredDependencies");
                        break;
                    case 6:
                        InMobiInterstitial.this.a("ART", "MonetizationDisabled");
                        break;
                    default:
                        InMobiInterstitial.this.a("AF", "");
                        break;
                }
                if (!InMobiInterstitial.c()) {
                    Message obtain = Message.obtain();
                    obtain.what = 1;
                    obtain.obj = inMobiAdRequestStatus;
                    InMobiInterstitial.this.c.sendMessage(obtain);
                }
            }

            public final void b() {
                InMobiInterstitial.this.c.sendEmptyMessage(5);
            }

            public final void c() {
                InMobiInterstitial.this.c.sendEmptyMessage(6);
            }

            public final void d() {
                InMobiInterstitial.this.a("AVD", "");
                InMobiInterstitial.this.c.sendEmptyMessage(7);
            }

            public final void e() {
                InMobiInterstitial.this.a("AVCD", "");
                InMobiInterstitial.this.c.sendEmptyMessage(10);
                com.inmobi.ads.d.b d = com.inmobi.ads.d.b.d();
                bj a2 = bj.a(InMobiInterstitial.this.g, InMobiInterstitial.this.j, "int", InMobiInterstitial.this.i);
                if (com.inmobi.ads.d.b.b.c(d.c).f2225a) {
                    new Handler(Looper.getMainLooper()).post(new Runnable(a2) {

                        /* renamed from: a reason: collision with root package name */
                        final /* synthetic */ bj f2245a;

                        {
                            this.f2245a = r2;
                        }

                        public final void run() {
                            try {
                                b.a(b.this);
                                if (!a.f2236a.containsKey(this.f2245a)) {
                                    b.d;
                                    StringBuilder sb = new StringBuilder("preLoadAdUnit. pid:");
                                    sb.append(this.f2245a.f2198a);
                                    sb.append(" tp:");
                                    sb.append(this.f2245a.b);
                                    if (this.f2245a.c == null && this.f2245a.b != null) {
                                        HashMap hashMap = new HashMap();
                                        hashMap.put("tp", this.f2245a.b);
                                        this.f2245a.c = hashMap;
                                    }
                                    a aVar = new a(this.f2245a);
                                    b.g.add(aVar);
                                    ac a2 = com.inmobi.ads.ac.a.a(com.inmobi.commons.a.a.b(), this.f2245a, aVar);
                                    a2.e = this.f2245a.d;
                                    a2.f = this.f2245a.c;
                                    a2.n = true;
                                    a.f2236a.put(this.f2245a, a2);
                                    a2.e(aVar);
                                }
                            } catch (Exception e) {
                                b.d;
                                new StringBuilder("SDK encountered an unexpected error preloading ad units; ").append(e.getMessage());
                                com.inmobi.commons.core.a.a.a().a(new com.inmobi.commons.core.e.a(e));
                            }
                        }
                    });
                }
            }

            public final void a(@NonNull Map<Object, Object> map) {
                InMobiInterstitial.this.a("AVCL", "");
                Message obtain = Message.obtain();
                obtain.what = 9;
                obtain.obj = map;
                InMobiInterstitial.this.c.sendMessage(obtain);
            }

            public final void f() {
                InMobiInterstitial.this.c.sendEmptyMessage(11);
            }

            public final void b(@NonNull Map<Object, Object> map) {
                Message obtain = Message.obtain();
                obtain.what = 4;
                obtain.obj = map;
                InMobiInterstitial.this.c.sendMessage(obtain);
            }

            /* access modifiers changed from: 0000 */
            public final void a(byte[] bArr) {
                Message obtain = Message.obtain();
                obtain.what = 12;
                obtain.obj = bArr;
                InMobiInterstitial.this.c.sendMessage(obtain);
            }

            /* access modifiers changed from: 0000 */
            public final void b(InMobiAdRequestStatus inMobiAdRequestStatus) {
                Message obtain = Message.obtain();
                obtain.what = 13;
                obtain.obj = inMobiAdRequestStatus;
                InMobiInterstitial.this.c.sendMessage(obtain);
            }
        };
        if (!com.inmobi.commons.a.a.a()) {
            Logger.a(InternalLogLevel.ERROR, f2074a, "Please initialize the SDK before creating an Interstitial ad");
        } else if (interstitialAdListener2 == null) {
            Logger.a(InternalLogLevel.ERROR, f2074a, "The Interstitial ad cannot be created as no event listener was supplied. Please attach a listener to proceed");
        } else if (context == null) {
            Logger.a(InternalLogLevel.ERROR, f2074a, "Unable to create Interstitial ad with null context object.");
        } else {
            this.h = true;
            this.f = context.getApplicationContext();
            this.g = j2;
            this.d = interstitialAdListener2;
            this.c = new a(this);
        }
    }

    public InMobiInterstitial(Context context, long j2, InterstitialAdEventListener interstitialAdEventListener) {
        this.h = false;
        this.l = false;
        this.n = "";
        this.q = new b() {
            public final void a(boolean z) {
                Message obtain = Message.obtain();
                obtain.what = 2;
                Bundle bundle = new Bundle();
                bundle.putBoolean(ParametersKeys.AVAILABLE, z);
                obtain.setData(bundle);
                InMobiInterstitial.this.c.sendMessage(obtain);
            }

            public final void a(i iVar) {
                InMobiInterstitial.this.a("AR", "");
                InMobiInterstitial.this.n = iVar.x;
                InMobiInterstitial.this.p = iVar.i;
                InMobiInterstitial.this.c.sendEmptyMessage(3);
            }

            public final void a(InMobiAdRequestStatus inMobiAdRequestStatus) {
                switch (AnonymousClass3.f2078a[inMobiAdRequestStatus.getStatusCode().ordinal()]) {
                    case 1:
                        InMobiInterstitial.this.a("ART", "NetworkNotAvailable");
                        break;
                    case 2:
                        InMobiInterstitial.this.a("ART", "LoadInProgress");
                        break;
                    case 3:
                        InMobiInterstitial.this.a("ART", "ReloadNotPermitted");
                        break;
                    case 4:
                        InMobiInterstitial.this.a("ART", "FrequentRequests");
                        break;
                    case 5:
                        InMobiInterstitial.this.a("ART", "MissingRequiredDependencies");
                        break;
                    case 6:
                        InMobiInterstitial.this.a("ART", "MonetizationDisabled");
                        break;
                    default:
                        InMobiInterstitial.this.a("AF", "");
                        break;
                }
                if (!InMobiInterstitial.c()) {
                    Message obtain = Message.obtain();
                    obtain.what = 1;
                    obtain.obj = inMobiAdRequestStatus;
                    InMobiInterstitial.this.c.sendMessage(obtain);
                }
            }

            public final void b() {
                InMobiInterstitial.this.c.sendEmptyMessage(5);
            }

            public final void c() {
                InMobiInterstitial.this.c.sendEmptyMessage(6);
            }

            public final void d() {
                InMobiInterstitial.this.a("AVD", "");
                InMobiInterstitial.this.c.sendEmptyMessage(7);
            }

            public final void e() {
                InMobiInterstitial.this.a("AVCD", "");
                InMobiInterstitial.this.c.sendEmptyMessage(10);
                com.inmobi.ads.d.b d = com.inmobi.ads.d.b.d();
                bj a2 = bj.a(InMobiInterstitial.this.g, InMobiInterstitial.this.j, "int", InMobiInterstitial.this.i);
                if (com.inmobi.ads.d.b.b.c(d.c).f2225a) {
                    new Handler(Looper.getMainLooper()).post(new Runnable(a2) {

                        /* renamed from: a reason: collision with root package name */
                        final /* synthetic */ bj f2245a;

                        {
                            this.f2245a = r2;
                        }

                        public final void run() {
                            try {
                                b.a(b.this);
                                if (!a.f2236a.containsKey(this.f2245a)) {
                                    b.d;
                                    StringBuilder sb = new StringBuilder("preLoadAdUnit. pid:");
                                    sb.append(this.f2245a.f2198a);
                                    sb.append(" tp:");
                                    sb.append(this.f2245a.b);
                                    if (this.f2245a.c == null && this.f2245a.b != null) {
                                        HashMap hashMap = new HashMap();
                                        hashMap.put("tp", this.f2245a.b);
                                        this.f2245a.c = hashMap;
                                    }
                                    a aVar = new a(this.f2245a);
                                    b.g.add(aVar);
                                    ac a2 = com.inmobi.ads.ac.a.a(com.inmobi.commons.a.a.b(), this.f2245a, aVar);
                                    a2.e = this.f2245a.d;
                                    a2.f = this.f2245a.c;
                                    a2.n = true;
                                    a.f2236a.put(this.f2245a, a2);
                                    a2.e(aVar);
                                }
                            } catch (Exception e) {
                                b.d;
                                new StringBuilder("SDK encountered an unexpected error preloading ad units; ").append(e.getMessage());
                                com.inmobi.commons.core.a.a.a().a(new com.inmobi.commons.core.e.a(e));
                            }
                        }
                    });
                }
            }

            public final void a(@NonNull Map<Object, Object> map) {
                InMobiInterstitial.this.a("AVCL", "");
                Message obtain = Message.obtain();
                obtain.what = 9;
                obtain.obj = map;
                InMobiInterstitial.this.c.sendMessage(obtain);
            }

            public final void f() {
                InMobiInterstitial.this.c.sendEmptyMessage(11);
            }

            public final void b(@NonNull Map<Object, Object> map) {
                Message obtain = Message.obtain();
                obtain.what = 4;
                obtain.obj = map;
                InMobiInterstitial.this.c.sendMessage(obtain);
            }

            /* access modifiers changed from: 0000 */
            public final void a(byte[] bArr) {
                Message obtain = Message.obtain();
                obtain.what = 12;
                obtain.obj = bArr;
                InMobiInterstitial.this.c.sendMessage(obtain);
            }

            /* access modifiers changed from: 0000 */
            public final void b(InMobiAdRequestStatus inMobiAdRequestStatus) {
                Message obtain = Message.obtain();
                obtain.what = 13;
                obtain.obj = inMobiAdRequestStatus;
                InMobiInterstitial.this.c.sendMessage(obtain);
            }
        };
        if (!com.inmobi.commons.a.a.a()) {
            Logger.a(InternalLogLevel.ERROR, f2074a, "Please initialize the SDK before creating an Interstitial ad");
        } else if (interstitialAdEventListener == null) {
            Logger.a(InternalLogLevel.ERROR, f2074a, "The Interstitial ad cannot be created as no event listener was supplied. Please attach a listener to proceed");
        } else if (context == null) {
            Logger.a(InternalLogLevel.ERROR, f2074a, "Unable to create Interstitial ad with null context object.");
        } else {
            this.h = true;
            this.f = context.getApplicationContext();
            this.g = j2;
            this.e = interstitialAdEventListener;
            this.c = new a(this);
        }
    }

    private InMobiInterstitial(Context context, long j2) {
        this.h = false;
        this.l = false;
        this.n = "";
        this.q = new b() {
            public final void a(boolean z) {
                Message obtain = Message.obtain();
                obtain.what = 2;
                Bundle bundle = new Bundle();
                bundle.putBoolean(ParametersKeys.AVAILABLE, z);
                obtain.setData(bundle);
                InMobiInterstitial.this.c.sendMessage(obtain);
            }

            public final void a(i iVar) {
                InMobiInterstitial.this.a("AR", "");
                InMobiInterstitial.this.n = iVar.x;
                InMobiInterstitial.this.p = iVar.i;
                InMobiInterstitial.this.c.sendEmptyMessage(3);
            }

            public final void a(InMobiAdRequestStatus inMobiAdRequestStatus) {
                switch (AnonymousClass3.f2078a[inMobiAdRequestStatus.getStatusCode().ordinal()]) {
                    case 1:
                        InMobiInterstitial.this.a("ART", "NetworkNotAvailable");
                        break;
                    case 2:
                        InMobiInterstitial.this.a("ART", "LoadInProgress");
                        break;
                    case 3:
                        InMobiInterstitial.this.a("ART", "ReloadNotPermitted");
                        break;
                    case 4:
                        InMobiInterstitial.this.a("ART", "FrequentRequests");
                        break;
                    case 5:
                        InMobiInterstitial.this.a("ART", "MissingRequiredDependencies");
                        break;
                    case 6:
                        InMobiInterstitial.this.a("ART", "MonetizationDisabled");
                        break;
                    default:
                        InMobiInterstitial.this.a("AF", "");
                        break;
                }
                if (!InMobiInterstitial.c()) {
                    Message obtain = Message.obtain();
                    obtain.what = 1;
                    obtain.obj = inMobiAdRequestStatus;
                    InMobiInterstitial.this.c.sendMessage(obtain);
                }
            }

            public final void b() {
                InMobiInterstitial.this.c.sendEmptyMessage(5);
            }

            public final void c() {
                InMobiInterstitial.this.c.sendEmptyMessage(6);
            }

            public final void d() {
                InMobiInterstitial.this.a("AVD", "");
                InMobiInterstitial.this.c.sendEmptyMessage(7);
            }

            public final void e() {
                InMobiInterstitial.this.a("AVCD", "");
                InMobiInterstitial.this.c.sendEmptyMessage(10);
                com.inmobi.ads.d.b d = com.inmobi.ads.d.b.d();
                bj a2 = bj.a(InMobiInterstitial.this.g, InMobiInterstitial.this.j, "int", InMobiInterstitial.this.i);
                if (com.inmobi.ads.d.b.b.c(d.c).f2225a) {
                    new Handler(Looper.getMainLooper()).post(new Runnable(a2) {

                        /* renamed from: a reason: collision with root package name */
                        final /* synthetic */ bj f2245a;

                        {
                            this.f2245a = r2;
                        }

                        public final void run() {
                            try {
                                b.a(b.this);
                                if (!a.f2236a.containsKey(this.f2245a)) {
                                    b.d;
                                    StringBuilder sb = new StringBuilder("preLoadAdUnit. pid:");
                                    sb.append(this.f2245a.f2198a);
                                    sb.append(" tp:");
                                    sb.append(this.f2245a.b);
                                    if (this.f2245a.c == null && this.f2245a.b != null) {
                                        HashMap hashMap = new HashMap();
                                        hashMap.put("tp", this.f2245a.b);
                                        this.f2245a.c = hashMap;
                                    }
                                    a aVar = new a(this.f2245a);
                                    b.g.add(aVar);
                                    ac a2 = com.inmobi.ads.ac.a.a(com.inmobi.commons.a.a.b(), this.f2245a, aVar);
                                    a2.e = this.f2245a.d;
                                    a2.f = this.f2245a.c;
                                    a2.n = true;
                                    a.f2236a.put(this.f2245a, a2);
                                    a2.e(aVar);
                                }
                            } catch (Exception e) {
                                b.d;
                                new StringBuilder("SDK encountered an unexpected error preloading ad units; ").append(e.getMessage());
                                com.inmobi.commons.core.a.a.a().a(new com.inmobi.commons.core.e.a(e));
                            }
                        }
                    });
                }
            }

            public final void a(@NonNull Map<Object, Object> map) {
                InMobiInterstitial.this.a("AVCL", "");
                Message obtain = Message.obtain();
                obtain.what = 9;
                obtain.obj = map;
                InMobiInterstitial.this.c.sendMessage(obtain);
            }

            public final void f() {
                InMobiInterstitial.this.c.sendEmptyMessage(11);
            }

            public final void b(@NonNull Map<Object, Object> map) {
                Message obtain = Message.obtain();
                obtain.what = 4;
                obtain.obj = map;
                InMobiInterstitial.this.c.sendMessage(obtain);
            }

            /* access modifiers changed from: 0000 */
            public final void a(byte[] bArr) {
                Message obtain = Message.obtain();
                obtain.what = 12;
                obtain.obj = bArr;
                InMobiInterstitial.this.c.sendMessage(obtain);
            }

            /* access modifiers changed from: 0000 */
            public final void b(InMobiAdRequestStatus inMobiAdRequestStatus) {
                Message obtain = Message.obtain();
                obtain.what = 13;
                obtain.obj = inMobiAdRequestStatus;
                InMobiInterstitial.this.c.sendMessage(obtain);
            }
        };
        this.h = true;
        this.f = context;
        this.g = j2;
        this.c = new a(this);
    }

    @Deprecated
    public final void setInterstitialAdListener(InterstitialAdListener2 interstitialAdListener2) {
        this.d = interstitialAdListener2;
    }

    public final void setListener(InterstitialAdEventListener interstitialAdEventListener) {
        if (interstitialAdEventListener == null) {
            Logger.a(InternalLogLevel.ERROR, f2074a, "Please pass a non-null listener to the interstitial.");
        } else {
            this.e = interstitialAdEventListener;
        }
    }

    public final void setKeywords(String str) {
        if (this.h) {
            this.i = str;
        }
    }

    private boolean a(boolean z) {
        if (!this.h) {
            Logger.a(InternalLogLevel.ERROR, f2074a, "InMobiInterstitial is not initialized, your call is ignored.");
        } else if (!z ? this.e == null : this.d == null && this.e == null) {
            Logger.a(InternalLogLevel.ERROR, f2074a, "Listener supplied is null, your call is ignored.");
        } else if (this.f != null) {
            return true;
        } else {
            Logger.a(InternalLogLevel.ERROR, f2074a, "Context supplied is null, your call is ignored.");
        }
        return false;
    }

    private boolean a(InMobiAdRequestStatus inMobiAdRequestStatus) {
        if (this.b == null || this.b.y) {
            return true;
        }
        if (this.e != null) {
            this.e.onAdLoadFailed(this, inMobiAdRequestStatus);
        }
        return false;
    }

    public final void getSignals() {
        if (a(false) && a(new InMobiAdRequestStatus(StatusCode.GET_SIGNALS_CALLED_AFTER_LOAD))) {
            if (this.b == null) {
                this.b = d();
            }
            a("ARR", "");
            a(this.b);
            this.b.y = true;
            this.b.o();
        }
    }

    @VisibleForTesting
    @NonNull
    private ac d() {
        return com.inmobi.ads.ac.a.b(this.f, bj.a(this.g, this.j, "int", this.i), this.q);
    }

    public final void load(byte[] bArr) {
        if (a(false) && a(new InMobiAdRequestStatus(StatusCode.LOAD_WITH_RESPONSE_CALLED_AFTER_LOAD))) {
            this.l = true;
            if (this.b == null) {
                this.b = d();
            }
            a(this.b);
            this.b.y = true;
            ac acVar = this.b;
            if (acVar.d(this.q)) {
                acVar.a(bArr);
            }
        }
    }

    public final void load() {
        try {
            if (a(true)) {
                if (this.b == null || !this.b.y) {
                    bj a2 = bj.a(this.g, this.j, "int", this.i);
                    com.inmobi.ads.d.b d2 = com.inmobi.ads.d.b.d();
                    i iVar = null;
                    if (!com.inmobi.ads.d.b.b.c(d2.c).f2225a) {
                        StringBuilder sb = new StringBuilder("No cached ad unit found as config is disabled. pid:");
                        sb.append(a2.f2198a);
                        sb.append(" tp:");
                        sb.append(a2.b);
                    } else {
                        d2.a(a2);
                        i iVar2 = (i) com.inmobi.ads.d.b.f2236a.get(a2);
                        if (iVar2 == null) {
                            StringBuilder sb2 = new StringBuilder("No cached ad unit found for pid:");
                            sb2.append(a2.f2198a);
                            sb2.append(" tp:");
                            sb2.append(a2.b);
                        } else if (iVar2.h()) {
                            StringBuilder sb3 = new StringBuilder("Expired cached ad unit found for pid:");
                            sb3.append(a2.f2198a);
                            sb3.append(" tp:");
                            sb3.append(a2.b);
                            iVar2.t();
                            com.inmobi.ads.d.b.f2236a.remove(a2);
                            com.inmobi.ads.d.b.a("AdUnitExpired", iVar2);
                        } else {
                            StringBuilder sb4 = new StringBuilder("Cached ad unit found for pid:");
                            sb4.append(a2.f2198a);
                            sb4.append(" tp:");
                            sb4.append(a2.b);
                            com.inmobi.ads.d.b.a((i) com.inmobi.ads.d.b.f2236a.remove(a2));
                            iVar = iVar2;
                        }
                    }
                    this.l = true;
                    if (iVar != null) {
                        this.b = (ac) iVar;
                    } else {
                        this.b = com.inmobi.ads.ac.a.a(this.f, a2, this.q);
                    }
                    a("ARR", "");
                    a(this.b);
                    this.b.y = false;
                    ac acVar = this.b;
                    InternalLogLevel internalLogLevel = InternalLogLevel.DEBUG;
                    String str = f2074a;
                    StringBuilder sb5 = new StringBuilder("Fetching an Interstitial ad for placement id: ");
                    sb5.append(acVar.d);
                    Logger.a(internalLogLevel, str, sb5.toString());
                    this.n = "";
                    this.p = this.b.b;
                    acVar.a(this.q);
                    acVar.e(this.q);
                } else {
                    if (this.e != null) {
                        this.e.onAdLoadFailed(this, new InMobiAdRequestStatus(StatusCode.LOAD_CALLED_AFTER_GET_SIGNALS));
                    }
                }
            }
        } catch (Exception e2) {
            Logger.a(InternalLogLevel.ERROR, f2074a, "Unable to load ad; SDK encountered an unexpected error");
            new StringBuilder("Load failed with unexpected error: ").append(e2.getMessage());
            com.inmobi.commons.core.a.a.a().a(new com.inmobi.commons.core.e.a(e2));
        }
    }

    @Deprecated
    public static void requestAd(Context context, InMobiAdRequest inMobiAdRequest, InterstitialAdRequestListener interstitialAdRequestListener) {
        if (!com.inmobi.commons.a.a.a()) {
            Logger.a(InternalLogLevel.ERROR, f2074a, "Please initialize the SDK before calling requestAd. Ignoring request");
        } else if (interstitialAdRequestListener == null) {
            Logger.a(InternalLogLevel.ERROR, f2074a, "Please supply a non null InterstitialAdRequestListener. Ignoring request");
        } else if (inMobiAdRequest == null) {
            Logger.a(InternalLogLevel.ERROR, f2074a, "Please supply a non null InMobiAdRequest. Ignoring request");
        } else if (context == null) {
            Logger.a(InternalLogLevel.ERROR, f2074a, "Please supply a non null Context. Ignoring request");
        } else {
            ac acVar = null;
            try {
                RecyclerView.class.getName();
                Picasso.class.getName();
                AnonymousClass1 r1 = new e() {
                    public final void a(@NonNull i iVar) {
                        try {
                            if (iVar instanceof ac) {
                                ArrayList arrayList = (ArrayList) InMobiInterstitial.m.get(iVar);
                                if (arrayList != null) {
                                    InMobiInterstitial.m.remove(iVar);
                                    Handler handler = new Handler(Looper.getMainLooper());
                                    Iterator it = arrayList.iterator();
                                    while (it.hasNext()) {
                                        WeakReference weakReference = (WeakReference) it.next();
                                        if (weakReference != null) {
                                            final InterstitialAdRequestListener interstitialAdRequestListener = (InterstitialAdRequestListener) weakReference.get();
                                            if (interstitialAdRequestListener != null) {
                                                final InMobiInterstitial inMobiInterstitial = new InMobiInterstitial(iVar.a(), iVar.d, 0);
                                                inMobiInterstitial.setKeywords(iVar.e);
                                                inMobiInterstitial.setExtras(iVar.f);
                                                handler.post(new Runnable() {
                                                    public final void run() {
                                                        try {
                                                            interstitialAdRequestListener.onAdRequestCompleted(new InMobiAdRequestStatus(StatusCode.NO_ERROR), inMobiInterstitial);
                                                        } catch (Exception unused) {
                                                            Logger.a(InternalLogLevel.ERROR, InMobiInterstitial.f2074a, "Publisher handler caused unexpected error");
                                                        }
                                                    }
                                                });
                                            }
                                        }
                                    }
                                }
                            }
                        } catch (Exception e) {
                            InMobiInterstitial.f2074a;
                            new StringBuilder("SDK encountered unexpected error in onAdPrefetchSucceeded ").append(e.getMessage());
                        }
                    }

                    public final void a(@NonNull i iVar, @NonNull final InMobiAdRequestStatus inMobiAdRequestStatus) {
                        try {
                            if (iVar instanceof ac) {
                                ArrayList arrayList = (ArrayList) InMobiInterstitial.m.get(iVar);
                                if (arrayList != null && arrayList.size() > 0) {
                                    WeakReference weakReference = (WeakReference) arrayList.get(arrayList.size() - 1);
                                    if (weakReference != null) {
                                        arrayList.remove(weakReference);
                                        if (arrayList.size() == 0) {
                                            InMobiInterstitial.m.remove(iVar);
                                        }
                                        final InterstitialAdRequestListener interstitialAdRequestListener = (InterstitialAdRequestListener) weakReference.get();
                                        if (interstitialAdRequestListener != null) {
                                            new Handler(Looper.getMainLooper()).post(new Runnable() {
                                                public final void run() {
                                                    try {
                                                        interstitialAdRequestListener.onAdRequestCompleted(inMobiAdRequestStatus, null);
                                                    } catch (Exception unused) {
                                                        Logger.a(InternalLogLevel.ERROR, InMobiInterstitial.f2074a, "Publisher handler caused unexpected error");
                                                    }
                                                }
                                            });
                                        }
                                    }
                                }
                            }
                        } catch (Exception e) {
                            InMobiInterstitial.f2074a;
                            new StringBuilder("SDK encountered unexpected error in onAdPrefetchFailed ").append(e.getMessage());
                        }
                    }
                };
                try {
                    HashMap hashMap = new HashMap();
                    hashMap.put("description", "requestAd Api called");
                    try {
                        com.inmobi.commons.core.e.b.a();
                        com.inmobi.commons.core.e.b.a(CampaignUnit.JSON_KEY_ADS, "GenericEvents", hashMap);
                    } catch (Exception e2) {
                        StringBuilder sb = new StringBuilder("Error in submitting telemetry event : (");
                        sb.append(e2.getMessage());
                        sb.append(")");
                    }
                    Iterator it = m.entrySet().iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            break;
                        }
                        ac acVar2 = (ac) ((Entry) it.next()).getKey();
                        if (acVar2 != null && acVar2.d == inMobiAdRequest.f2062a) {
                            acVar = acVar2;
                            break;
                        }
                    }
                    if (acVar != null) {
                        ArrayList arrayList = (ArrayList) m.get(acVar);
                        arrayList.add(new WeakReference(interstitialAdRequestListener));
                        ac a2 = a(context, inMobiAdRequest, (e) r1);
                        m.put(a2, arrayList);
                        a2.q();
                        return;
                    }
                    ac a3 = a(context, inMobiAdRequest, (e) r1);
                    a3.q = r1;
                    ArrayList arrayList2 = new ArrayList();
                    arrayList2.add(new WeakReference(interstitialAdRequestListener));
                    m.put(a3, arrayList2);
                    a3.q();
                } catch (Exception e3) {
                    new StringBuilder("SDK encountered unexpected error in requestAd").append(e3.getMessage());
                }
            } catch (NoClassDefFoundError unused) {
                Logger.a(InternalLogLevel.ERROR, f2074a, "Some of the dependency libraries for Interstitial not found. Ignoring request");
                interstitialAdRequestListener.onAdRequestCompleted(new InMobiAdRequestStatus(StatusCode.MISSING_REQUIRED_DEPENDENCIES), null);
            }
        }
    }

    private static ac a(Context context, InMobiAdRequest inMobiAdRequest, e eVar) {
        ac a2 = com.inmobi.ads.ac.a.a(context.getApplicationContext(), bj.a(inMobiAdRequest.f2062a, inMobiAdRequest.f, "int", inMobiAdRequest.e), null);
        a2.f = inMobiAdRequest.f;
        a2.e = inMobiAdRequest.e;
        a2.a(MonetizationContext.MONETIZATION_CONTEXT_ACTIVITY);
        a2.n = true;
        a2.q = eVar;
        return a2;
    }

    @VisibleForTesting
    private void a(@NonNull ac acVar) {
        acVar.a(this.f);
        acVar.f = this.j;
        acVar.e = this.i;
        acVar.a(MonetizationContext.MONETIZATION_CONTEXT_ACTIVITY);
        if (this.k) {
            AdContainer j2 = acVar.j();
            if (j2 != null) {
                acVar.z = true;
                j2.a();
            }
        }
        acVar.n = false;
    }

    public final void show() {
        try {
            if (!this.l) {
                Logger.a(InternalLogLevel.ERROR, f2074a, "load() must be called before trying to show the ad");
                return;
            }
            if (this.h && this.b != null) {
                a("AVR", "");
                this.b.f(this.q);
            }
        } catch (Exception e2) {
            Logger.a(InternalLogLevel.ERROR, f2074a, "Unable to show ad; SDK encountered an unexpected error");
            new StringBuilder("Show failed with unexpected error: ").append(e2.getMessage());
            com.inmobi.commons.core.a.a.a().a(new com.inmobi.commons.core.e.a(e2));
        }
    }

    @Deprecated
    public final void show(int i2, int i3) {
        show();
    }

    public final boolean isReady() {
        return this.h && this.b != null && this.b.M();
    }

    public final JSONObject getAdMetaInfo() {
        if (this.p == null) {
            return new JSONObject();
        }
        return this.p;
    }

    public final String getCreativeId() {
        return this.n;
    }

    public final void setExtras(Map<String, String> map) {
        if (this.h) {
            this.j = map;
        }
    }

    public final void disableHardwareAcceleration() {
        if (this.h) {
            this.k = true;
        }
    }

    /* access modifiers changed from: private */
    public void a(String str, String str2) {
        if (this.o == null) {
            this.o = new k(this.b);
        }
        this.o.a(this.q, str, str2);
    }

    static /* synthetic */ boolean c() {
        return Message.obtain() == null;
    }
}
