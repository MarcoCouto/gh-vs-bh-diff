package com.inmobi.ads;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import com.inmobi.rendering.RenderView;
import java.lang.ref.WeakReference;

/* compiled from: ViewableAd */
public abstract class cb {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    AdContainer f2230a;
    @Nullable
    a b;
    @Nullable
    protected WeakReference<View> c;

    /* compiled from: ViewableAd */
    protected static abstract class a {

        /* renamed from: a reason: collision with root package name */
        private boolean f2231a;

        public abstract View a(View view, ViewGroup viewGroup, boolean z, RenderView renderView);

        protected a() {
        }

        public boolean b() {
            return this.f2231a;
        }

        public void a() {
            if (!this.f2231a) {
                this.f2231a = true;
            }
        }
    }

    @Nullable
    public View a() {
        return null;
    }

    @Nullable
    public abstract View a(View view, ViewGroup viewGroup, boolean z);

    public abstract void a(int i);

    public abstract void a(Context context, int i);

    public abstract void a(@Nullable View... viewArr);

    public abstract void d();

    public cb() {
    }

    public cb(@NonNull AdContainer adContainer) {
        this.f2230a = adContainer;
    }

    @Nullable
    public a f() {
        return this.b;
    }

    /* access modifiers changed from: protected */
    public final void a(View view) {
        this.c = new WeakReference<>(view);
    }

    @NonNull
    public c c() {
        return new c();
    }

    @Nullable
    public View b() {
        if (this.c == null) {
            return null;
        }
        return (View) this.c.get();
    }

    public void e() {
        if (this.c != null) {
            this.c.clear();
        }
    }
}
