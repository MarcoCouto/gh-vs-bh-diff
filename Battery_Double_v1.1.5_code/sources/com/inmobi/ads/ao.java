package com.inmobi.ads;

import android.annotation.TargetApi;
import android.graphics.Point;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.webkit.URLUtil;
import com.facebook.appevents.codeless.internal.Constants;
import com.facebook.share.internal.ShareConstants;
import com.google.android.exoplayer2.util.MimeTypes;
import com.inmobi.ads.AdContainer.RenderingProperties.PlacementType;
import com.inmobi.commons.core.e.b;
import com.inmobi.commons.core.utilities.b.c;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.entity.CampaignUnit;
import com.startapp.android.publish.common.model.AdPreferences;
import com.vungle.warren.model.AdvertisementDBAdapter.AdvertisementColumns;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: NativeDataModel */
public class ao {
    private static final String l = "ao";

    /* renamed from: a reason: collision with root package name */
    int f2154a;
    public boolean b;
    public boolean c;
    public am d;
    JSONArray e;
    final ao f;
    @Nullable
    Map<String, String> g;
    Map<String, List<ak>> h;
    a i;
    boolean j;
    bg k;
    private JSONObject m;
    private JSONObject n;
    private Map<String, ak> o;
    private Map<String, String> p;
    @Nullable
    private by q;
    private c r;
    private PlacementType s;
    private boolean t;

    /* compiled from: NativeDataModel */
    class a {

        /* renamed from: a reason: collision with root package name */
        JSONObject f2155a;
        @NonNull
        C0040a b = new C0040a();
        ak c;

        /* renamed from: com.inmobi.ads.ao$a$a reason: collision with other inner class name */
        /* compiled from: NativeDataModel */
        class C0040a {

            /* renamed from: a reason: collision with root package name */
            public String f2156a;
            public String b;
            public String c;
            public String d;
            public float e;
            public String f;
            public boolean g;

            C0040a() {
            }
        }

        a() {
        }
    }

    ao() {
        this.f = null;
    }

    public ao(@NonNull PlacementType placementType, @NonNull JSONObject jSONObject, @Nullable c cVar, @Nullable by byVar) {
        this(placementType, jSONObject, null, false, cVar, byVar);
    }

    public ao(@NonNull PlacementType placementType, @NonNull JSONObject jSONObject, @Nullable ao aoVar, boolean z, @Nullable c cVar, @Nullable by byVar) {
        this(placementType, jSONObject, aoVar, z, cVar, byVar, 0);
    }

    private ao(@NonNull PlacementType placementType, @NonNull JSONObject jSONObject, @Nullable ao aoVar, boolean z, @Nullable c cVar, @Nullable by byVar, byte b2) {
        this.s = placementType;
        this.f = aoVar;
        if (cVar == null) {
            cVar = new c();
        }
        this.r = cVar;
        this.m = jSONObject;
        this.f2154a = 0;
        this.b = false;
        this.q = byVar;
        this.o = new HashMap();
        this.p = new HashMap();
        this.h = new HashMap();
        this.i = new a();
        this.t = z;
        f();
    }

    /* access modifiers changed from: 0000 */
    public final JSONObject a() {
        try {
            return this.e.getJSONObject(0);
        } catch (JSONException e2) {
            com.inmobi.commons.core.a.a.a().a(new com.inmobi.commons.core.e.a(e2));
            return null;
        }
    }

    /* access modifiers changed from: 0000 */
    public final int b() {
        if (this.d == null) {
            return 0;
        }
        Iterator it = this.d.iterator();
        while (it.hasNext()) {
            ak akVar = (ak) it.next();
            if (akVar.d.equalsIgnoreCase("card_scrollable")) {
                return ((am) akVar).C;
            }
        }
        return 0;
    }

    /* access modifiers changed from: 0000 */
    public final am a(int i2) {
        Iterator it = this.d.iterator();
        while (it.hasNext()) {
            ak akVar = (ak) it.next();
            if (akVar.d.equalsIgnoreCase("card_scrollable")) {
                am amVar = (am) akVar;
                if (i2 >= amVar.C) {
                    return null;
                }
                return (am) amVar.a(i2);
            }
        }
        return null;
    }

    static am a(@NonNull ak akVar) {
        if (akVar instanceof am) {
            am amVar = (am) akVar;
            if (a(amVar)) {
                return amVar;
            }
        }
        for (am amVar2 = (am) akVar.t; amVar2 != null; amVar2 = (am) amVar2.t) {
            if (a(amVar2)) {
                return amVar2;
            }
        }
        return null;
    }

    private void d() {
        List list;
        a aVar;
        for (ak akVar : c(ShareConstants.IMAGE_URL)) {
            if (!URLUtil.isValidUrl((String) akVar.e)) {
                ak a2 = a(this, akVar);
                if (a2 == null) {
                    StringBuilder sb = new StringBuilder("Could not find referenced asset for asset (");
                    sb.append(akVar.d);
                    sb.append(")");
                } else if (a2.b.equals(akVar.b)) {
                    akVar.e = a2.e;
                } else if (ShareConstants.VIDEO_URL.equals(a2.b) && 1 != a2.m && 2 == a2.m) {
                    be beVar = (be) a2;
                    bz b2 = beVar.b();
                    bu a3 = bt.a(beVar, akVar);
                    if (a3 == null) {
                        list = null;
                    } else {
                        list = a3.a(1);
                    }
                    if (list != null) {
                        Iterator it = list.iterator();
                        while (true) {
                            if (!it.hasNext()) {
                                break;
                            }
                            aVar = (a) it.next();
                            if (URLUtil.isValidUrl(aVar.b)) {
                                break;
                            }
                        }
                    }
                    aVar = null;
                    if (a3 != null && aVar != null) {
                        b2.a(a3);
                        new StringBuilder("Setting image asset value: ").append(aVar.b);
                        akVar.e = aVar.b;
                        akVar.a(a3.a(TrackerEventType.TRACKER_EVENT_TYPE_CREATIVE_VIEW));
                        akVar.a(beVar.u, TrackerEventType.TRACKER_EVENT_TYPE_ERROR);
                    } else if (b2.e().size() > 0) {
                        a(beVar);
                        a(a3 == null ? "NoBestFitCompanion" : "NoValidResource", "Static", "URL", (String) null, (String) null);
                    }
                }
            }
        }
    }

    private static void a(String str, String str2, String str3, String str4, String str5) {
        try {
            HashMap hashMap = new HashMap();
            hashMap.put(IronSourceConstants.EVENTS_ERROR_CODE, str);
            hashMap.put("type", str2);
            hashMap.put("dataType", str3);
            hashMap.put("clientRequestId", null);
            hashMap.put("impId", null);
            b.a();
            b.a(CampaignUnit.JSON_KEY_ADS, "EndCardCompanionFailure", hashMap);
        } catch (Exception e2) {
            StringBuilder sb = new StringBuilder("Error in sendTelemetryEventForCompanionFailure : (");
            sb.append(e2.getMessage());
            sb.append(")");
        }
    }

    private void e() {
        String str;
        for (ak akVar : c("WEBVIEW")) {
            bg bgVar = (bg) akVar;
            if (!"URL".equals(bgVar.z) && !"HTML".equals(bgVar.z)) {
                ak a2 = a(this, akVar);
                if (a2 == null) {
                    StringBuilder sb = new StringBuilder("Could not find referenced asset for asset (");
                    sb.append(akVar.d);
                    sb.append(")");
                } else if (a2.b.equals(akVar.b)) {
                    akVar.e = a2.e;
                } else if (ShareConstants.VIDEO_URL.equals(a2.b) && 2 == a2.m) {
                    be beVar = (be) a2;
                    bz b2 = beVar.b();
                    bu a3 = bt.a(beVar, akVar);
                    if (a3 == null) {
                        str = null;
                    } else {
                        str = a(a3, bgVar);
                    }
                    boolean equals = "REF_IFRAME".equals(bgVar.z);
                    boolean equals2 = "REF_HTML".equals(bgVar.z);
                    if (a3 == null || ((equals && !URLUtil.isValidUrl(str)) || (equals2 && str == null))) {
                        if (b2.e().size() > 0) {
                            a(beVar);
                            a(a3 == null ? "NoBestFitCompanion" : "NoValidResource", "Rich", bgVar.z, (String) null, (String) null);
                        }
                        bgVar.z = "UNKNOWN";
                    } else {
                        b2.a(a3);
                        akVar.e = str;
                        akVar.a(a3.a(TrackerEventType.TRACKER_EVENT_TYPE_CREATIVE_VIEW));
                    }
                }
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x0062 A[Catch:{ JSONException -> 0x029b }] */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0063 A[Catch:{ JSONException -> 0x029b }] */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0065 A[Catch:{ JSONException -> 0x029b }] */
    private void f() {
        int i2;
        int i3;
        boolean z;
        char c2;
        try {
            this.n = this.m.optJSONObject("styleRefs");
            if (!this.m.isNull("orientation")) {
                String trim = this.m.getString("orientation").toLowerCase(Locale.US).trim();
                int hashCode = trim.hashCode();
                if (hashCode == -1626174665) {
                    if (trim.equals("unspecified")) {
                        c2 = 1;
                        switch (c2) {
                            case 2:
                                break;
                            case 3:
                                break;
                        }
                    }
                } else if (hashCode == 729267099) {
                    if (trim.equals("portrait")) {
                        c2 = 2;
                        switch (c2) {
                            case 2:
                                break;
                            case 3:
                                break;
                        }
                    }
                } else if (hashCode == 1430647483) {
                    if (trim.equals("landscape")) {
                        c2 = 3;
                        switch (c2) {
                            case 2:
                                i2 = 1;
                                break;
                            case 3:
                                i2 = 2;
                                break;
                        }
                    }
                }
                c2 = 65535;
                switch (c2) {
                    case 2:
                        break;
                    case 3:
                        break;
                }
            }
            i2 = 0;
            this.f2154a = i2;
            this.j = this.m.optBoolean("shouldAutoOpenLandingPage", true);
            this.b = this.m.optBoolean("disableBackButton", false);
            this.d = (am) a(this.m.getJSONObject("rootContainer"), "CONTAINER", "/rootContainer", null);
            try {
                if (!this.m.isNull("passThroughJson")) {
                    this.i.f2155a = this.m.getJSONObject("passThroughJson");
                }
                if (!this.m.isNull("adContent")) {
                    JSONObject jSONObject = this.m.getJSONObject("adContent");
                    if (jSONObject != null) {
                        a aVar = this.i;
                        aVar.getClass();
                        C0040a aVar2 = new C0040a();
                        aVar2.f2156a = jSONObject.optString("title", null);
                        aVar2.b = jSONObject.optString("description", null);
                        aVar2.d = jSONObject.optString("ctaText", null);
                        aVar2.c = jSONObject.optString("iconUrl", null);
                        aVar2.e = (float) jSONObject.optLong(CampaignEx.JSON_KEY_STAR, 0);
                        aVar2.f = jSONObject.optString("landingPageUrl", null);
                        aVar2.g = jSONObject.optBoolean("isApp");
                        this.i.b = aVar2;
                    }
                }
                ak akVar = new ak();
                if (!this.m.isNull("onClick")) {
                    JSONObject jSONObject2 = this.m.getJSONObject("onClick");
                    String str = "";
                    String str2 = "";
                    try {
                        if (!jSONObject2.isNull("itemUrl")) {
                            str = jSONObject2.getString("itemUrl");
                            z = true;
                        } else {
                            z = false;
                        }
                        if (!jSONObject2.isNull("action")) {
                            str2 = jSONObject2.getString("action");
                            z = true;
                        }
                        akVar.a(str);
                        akVar.b(jSONObject2.optString("fallbackUrl"));
                        akVar.j = str2;
                        akVar.h = z;
                        akVar.w = jSONObject2.optString("appBundleId");
                    } catch (JSONException unused) {
                    }
                    if (!jSONObject2.isNull("openMode")) {
                        akVar.i = d(jSONObject2.getString("openMode"));
                        akVar.b(jSONObject2.optString("fallbackUrl"));
                    }
                }
                if (!this.m.isNull("trackers")) {
                    akVar.a(b(this.m));
                }
                this.i.c = akVar;
            } catch (JSONException e2) {
                com.inmobi.commons.core.a.a.a().a(new com.inmobi.commons.core.e.a(e2));
            }
            this.c = false;
            if (this.m.has("rewards")) {
                this.g = new HashMap();
            }
            if (!this.m.isNull("rewards")) {
                JSONObject jSONObject3 = this.m.getJSONObject("rewards");
                if (jSONObject3 != null) {
                    Iterator keys = jSONObject3.keys();
                    while (keys.hasNext()) {
                        String str3 = (String) keys.next();
                        this.g.put(str3, jSONObject3.getString(str3));
                    }
                }
            }
            d();
            e();
            for (Entry entry : this.p.entrySet()) {
                String str4 = (String) entry.getValue();
                ak akVar2 = (ak) this.o.get(entry.getKey());
                if (4 == akVar2.n) {
                    ak akVar3 = (ak) this.o.get(str4);
                    if (ShareConstants.VIDEO_URL.equals(akVar3.b)) {
                        String[] split = ((by) ((be) akVar3).b()).b.split(":");
                        try {
                            i3 = Integer.parseInt(split[2]) + (Integer.parseInt(split[1]) * 60);
                        } catch (ArrayIndexOutOfBoundsException e3) {
                            com.inmobi.commons.core.a.a.a().a(new com.inmobi.commons.core.e.a(e3));
                            i3 = 0;
                        }
                        if (i3 == 0) {
                            akVar2.o = i3 / 4;
                        } else {
                            int i4 = akVar2.o;
                            if (i4 == 50) {
                                akVar2.o = i3 / 2;
                            } else if (i4 == 75) {
                                akVar2.o = (i3 * 3) / 4;
                            } else if (i4 != 100) {
                                akVar2.o = i3 / 4;
                            } else {
                                akVar2.o = i3;
                            }
                        }
                        ((be) akVar3).z.add(akVar2);
                    }
                }
            }
            if (this.m.isNull("pages")) {
                this.e = new JSONArray();
            } else {
                this.e = this.m.getJSONArray("pages");
            }
        } catch (JSONException e4) {
            com.inmobi.commons.core.a.a.a().a(new com.inmobi.commons.core.e.a(e4));
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x003f  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0041 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0042 A[RETURN] */
    static int a(String str) {
        char c2;
        String trim = str.toLowerCase(Locale.US).trim();
        int hashCode = trim.hashCode();
        if (hashCode == -1412832500) {
            if (trim.equals("companion")) {
                c2 = 3;
                switch (c2) {
                    case 1:
                    case 2:
                        break;
                    case 3:
                        break;
                }
            }
        } else if (hashCode == 0) {
            if (trim.equals("")) {
                c2 = 1;
                switch (c2) {
                    case 1:
                    case 2:
                        break;
                    case 3:
                        break;
                }
            }
        } else if (hashCode == 112202875 && trim.equals("video")) {
            c2 = 2;
            switch (c2) {
                case 1:
                case 2:
                    return 1;
                case 3:
                    return 2;
                default:
                    return 0;
            }
        }
        c2 = 65535;
        switch (c2) {
            case 1:
            case 2:
                break;
            case 3:
                break;
        }
    }

    /* access modifiers changed from: 0000 */
    public final boolean c() {
        am amVar;
        if (this.d == null) {
            return false;
        }
        Iterator it = this.d.iterator();
        while (true) {
            if (!it.hasNext()) {
                amVar = null;
                break;
            }
            ak akVar = (ak) it.next();
            if (akVar.d.equalsIgnoreCase("card_scrollable")) {
                amVar = (am) akVar;
                break;
            }
        }
        if (amVar == null) {
            return g();
        }
        if (b() <= 0) {
            return false;
        }
        return g();
    }

    private boolean g() {
        List<ak> c2 = c(ShareConstants.VIDEO_URL);
        if (c2 == null || c2.size() <= 0) {
            return true;
        }
        for (ak akVar : c2) {
            akVar.f2151a.length();
            be beVar = (be) akVar;
            if (beVar.b() == null) {
                return false;
            }
            List c3 = beVar.b().c();
            if (c3 == null || c3.size() == 0) {
                return false;
            }
            String b2 = beVar.b().b();
            if (b2 != null) {
                if (b2.length() == 0) {
                }
            }
            HashMap hashMap = new HashMap();
            hashMap.put("[ERRORCODE]", "403");
            beVar.a(TrackerEventType.TRACKER_EVENT_TYPE_ERROR, (Map<String, String>) hashMap);
            return false;
        }
        return true;
    }

    /* JADX WARNING: type inference failed for: r15v2, types: [com.inmobi.ads.ak, com.inmobi.ads.am] */
    /* JADX WARNING: type inference failed for: r1v13 */
    /* JADX WARNING: type inference failed for: r8v39, types: [com.inmobi.ads.am] */
    /* JADX WARNING: type inference failed for: r8v40, types: [com.inmobi.ads.am] */
    /* JADX WARNING: type inference failed for: r15v5 */
    /* JADX WARNING: type inference failed for: r1v27 */
    /* JADX WARNING: type inference failed for: r2v18, types: [com.inmobi.ads.az, com.inmobi.ads.ak] */
    /* JADX WARNING: type inference failed for: r1v30 */
    /* JADX WARNING: type inference failed for: r1v32, types: [com.inmobi.ads.ak, com.inmobi.ads.ar] */
    /* JADX WARNING: type inference failed for: r1v37, types: [com.inmobi.ads.ak, com.inmobi.ads.bb] */
    /* JADX WARNING: type inference failed for: r1v43, types: [com.inmobi.ads.ak] */
    /* JADX WARNING: type inference failed for: r21v2 */
    /* JADX WARNING: type inference failed for: r8v43, types: [com.inmobi.ads.aq] */
    /* JADX WARNING: type inference failed for: r8v44, types: [com.inmobi.ads.as] */
    /* JADX WARNING: type inference failed for: r8v45, types: [com.inmobi.ads.aq] */
    /* JADX WARNING: type inference failed for: r8v46, types: [com.inmobi.ads.as] */
    /* JADX WARNING: type inference failed for: r1v60, types: [com.inmobi.ads.bg] */
    /* JADX WARNING: type inference failed for: r21v3 */
    /* JADX WARNING: type inference failed for: r1v61 */
    /* JADX WARNING: type inference failed for: r1v65 */
    /* JADX WARNING: type inference failed for: r3v58, types: [com.inmobi.ads.ak] */
    /* JADX WARNING: type inference failed for: r21v4 */
    /* JADX WARNING: type inference failed for: r8v64, types: [com.inmobi.ads.an] */
    /* JADX WARNING: type inference failed for: r8v65, types: [com.inmobi.ads.an] */
    /* JADX WARNING: type inference failed for: r8v66, types: [com.inmobi.ads.am] */
    /* JADX WARNING: type inference failed for: r1v108 */
    /* JADX WARNING: type inference failed for: r1v109 */
    /* JADX WARNING: type inference failed for: r1v110 */
    /* JADX WARNING: type inference failed for: r1v111 */
    /* JADX WARNING: type inference failed for: r8v67, types: [com.inmobi.ads.aq] */
    /* JADX WARNING: type inference failed for: r8v68, types: [com.inmobi.ads.as] */
    /* JADX WARNING: type inference failed for: r8v69, types: [com.inmobi.ads.aq] */
    /* JADX WARNING: type inference failed for: r8v70, types: [com.inmobi.ads.as] */
    /* JADX WARNING: type inference failed for: r8v71, types: [com.inmobi.ads.an] */
    /* JADX WARNING: type inference failed for: r8v72, types: [com.inmobi.ads.an] */
    /* JADX WARNING: Code restructure failed: missing block: B:101:?, code lost:
        r2 = r15.getJSONObject("assetOnclick").optString("fallbackUrl");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:102:0x01b3, code lost:
        if (r18 == null) goto L_0x01d9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:104:0x01b9, code lost:
        if (r18.size() != 0) goto L_0x01bc;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:107:0x01be, code lost:
        r4 = r8;
        r8 = r8;
        r5 = r11;
        r11 = r1;
        r6 = r12;
        r31 = r13;
        r32 = r25;
        r1 = r53;
        r33 = r5;
        r5 = r15;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:109:?, code lost:
        r8 = new com.inmobi.ads.an
        r15 = r1;
        r8 = r8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:110:0x01d9, code lost:
        r4 = r8;
        r33 = r11;
        r6 = r12;
        r31 = r13;
        r5 = r15;
        r32 = r25;
        r15 = r53;
        r8 = new com.inmobi.ads.an
        r8 = r8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:111:0x01f1, code lost:
        r3.g = r15;
        a((com.inmobi.ads.ak) r3, r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:112:0x01f6, code lost:
        if (r2 == null) goto L_0x01fb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:113:0x01f8, code lost:
        r3.b(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:114:0x01fb, code lost:
        r21 = r3;
        r46 = r4;
        r48 = r6;
        r47 = r33;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:115:0x0205, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:116:0x0206, code lost:
        r46 = r4;
        r48 = r6;
        r47 = r33;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:117:0x020e, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:118:0x020f, code lost:
        r31 = r13;
        r32 = r25;
        r46 = r8;
        r47 = r11;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:119:0x0219, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:120:0x021a, code lost:
        r31 = r23;
        r32 = r25;
        r47 = r11;
        r48 = r12;
        r46 = r24;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:121:0x0226, code lost:
        r31 = r23;
        r32 = r25;
        r47 = r11;
        r48 = r12;
        r1 = 0;
        r46 = r24;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:122:0x0234, code lost:
        r14 = r11;
        r1 = r12;
        r31 = r23;
        r13 = r24;
        r32 = r25;
        r11 = r5;
        r5 = r15;
        r15 = r53;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:125:0x0242, code lost:
        r34 = r13;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:127:?, code lost:
        r7.h.get(com.facebook.share.internal.ShareConstants.VIDEO_URL);
        r44 = r7.s(r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:128:0x0257, code lost:
        r45 = r14;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:130:?, code lost:
        r35 = new com.inmobi.ads.be.a(r2.x, r2.y, r3.x, r3.y, r4.x, r4.y, r11.x, r11.y, r44);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:131:0x0276, code lost:
        if (r8 == null) goto L_0x0297;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:134:0x027c, code lost:
        if (a(r8, (com.inmobi.ads.al) r35) != false) goto L_0x0297;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:135:0x027e, code lost:
        com.inmobi.commons.core.e.b.a();
        com.inmobi.commons.core.e.b.a(com.mintegral.msdk.base.entity.CampaignUnit.JSON_KEY_ADS, "InvalidVideoGeometry", new java.util.HashMap());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:136:0x028e, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:137:0x028f, code lost:
        r48 = r1;
        r46 = r34;
        r47 = r45;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:140:0x0299, code lost:
        if (r7.q != null) goto L_0x02a0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:142:?, code lost:
        r0 = r7.a(r5, r0, r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:144:?, code lost:
        r0 = r7.q;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:147:0x02a8, code lost:
        if (com.inmobi.ads.AdContainer.RenderingProperties.PlacementType.PLACEMENT_TYPE_INLINE != r7.s) goto L_0x0341;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:148:0x02aa, code lost:
        if (r6 == null) goto L_0x0303;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:151:0x02ba, code lost:
        if (((java.lang.Boolean) r6.v.get("didRequestFullScreen")).booleanValue() != false) goto L_0x02ce;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:153:0x02be, code lost:
        if (r7.t == false) goto L_0x02c1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:155:0x02c1, code lost:
        r2 = 0;
        r4 = Integer.MAX_VALUE;
        r13 = true;
        r14 = true;
        r15 = false;
        r16 = true;
        r17 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:156:0x02ce, code lost:
        r2 = r5.optBoolean("loop", false);
        r3 = r5.optBoolean("showProgress", true);
        r13 = r5.optBoolean("soundOn", true);
        r14 = r5.optBoolean("showMute", true);
        r16 = r3;
        r17 = r5.optBoolean("autoPlay", true);
        r49 = ((com.inmobi.ads.be) r6).E;
        r15 = r2;
        r2 = (int) r5.optDouble("pauseAfter", com.github.mikephil.charting.utils.Utils.DOUBLE_EPSILON);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:157:0x0303, code lost:
        r2 = r5.optBoolean("loop", true);
        r3 = r5.optBoolean("showProgress", false);
        r11 = r5.optBoolean("soundOn", false);
        r4 = r5.optBoolean("showMute", false);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:158:0x0333, code lost:
        r15 = r2;
        r16 = r3;
        r2 = (int) r5.optDouble("pauseAfter", com.github.mikephil.charting.utils.Utils.DOUBLE_EPSILON);
        r17 = r5.optBoolean("autoPlay", true);
        r13 = r11;
        r49 = r5.optInt("completeAfter", Integer.MAX_VALUE);
        r14 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:159:0x033e, code lost:
        r4 = r49;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:162:?, code lost:
        r2 = r5.optBoolean("loop", false);
        r3 = r5.optBoolean("showProgress", true);
        r4 = r5.optBoolean("soundOn", true);
        r11 = r5.optBoolean("showMute", true);
        r15 = r2;
        r16 = r3;
        r2 = (int) r5.optDouble("pauseAfter", com.github.mikephil.charting.utils.Utils.DOUBLE_EPSILON);
        r17 = r5.optBoolean("autoPlay", true);
        r13 = r4;
        r4 = r5.optInt("completeAfter", Integer.MAX_VALUE);
        r14 = r11;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:163:0x037a, code lost:
        r3 = new java.util.HashMap();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:164:0x0385, code lost:
        if (r5.isNull("videoViewabilityConfig") != false) goto L_0x03a7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:167:?, code lost:
        r8 = r5.getJSONObject("videoViewabilityConfig");
        r9 = r8.keys();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:169:0x0395, code lost:
        if (r9.hasNext() == false) goto L_0x03a7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:170:0x0397, code lost:
        r11 = (java.lang.String) r9.next();
        r3.put(r11, r8.get(r11));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:171:0x03a4, code lost:
        r5 = r51;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:174:0x03af, code lost:
        r8 = r8;
        r9 = r1;
        r20 = r7.r.i.m;
        r12 = r0;
        r46 = r34;
        r47 = r45;
        r0 = Integer.MAX_VALUE;
        r7 = r53;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:176:?, code lost:
        r8 = new com.inmobi.ads.be(r9, r10, r35, r12, r13, r14, r15, r16, r17, r18, r51, r20);
        r8.G = new java.util.HashMap(r3);
        r3 = r8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:177:0x03d0, code lost:
        if (r4 > 0) goto L_0x03d3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:179:0x03d3, code lost:
        r0 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:180:0x03d4, code lost:
        r3.E = r0;
        r8.g = r7;
        r8.y = r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:181:0x03da, code lost:
        if (r6 == null) goto L_0x03de;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:182:0x03dc, code lost:
        r6.y = r8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:183:0x03de, code lost:
        if (r2 == 0) goto L_0x03e5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:184:0x03e0, code lost:
        r8.F = r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:185:0x03e5, code lost:
        r48 = r1;
        r1 = r8;
        r7 = r50;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:186:0x03ec, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:187:0x03ed, code lost:
        r46 = r34;
        r47 = r45;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:188:0x03f2, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:189:0x03f3, code lost:
        r47 = r14;
        r46 = r34;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:190:0x03f8, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:191:0x03f9, code lost:
        r46 = r13;
        r47 = r14;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:192:0x03fd, code lost:
        r48 = r1;
        r7 = r50;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:193:0x0403, code lost:
        r47 = r11;
        r1 = r12;
        r31 = r23;
        r46 = r24;
        r32 = r25;
        r7 = r53;
        r11 = r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:194:0x0411, code lost:
        if (r0 != null) goto L_0x0414;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:195:0x0413, code lost:
        return null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:197:?, code lost:
        r8 = com.inmobi.ads.bg.c(h(r51));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:198:0x0422, code lost:
        if ("URL".equals(r8) == false) goto L_0x042d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:201:0x0428, code lost:
        if (android.webkit.URLUtil.isValidUrl(r0) != false) goto L_0x042d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:202:0x042a, code lost:
        return null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:203:0x042b, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:204:0x042d, code lost:
        r15 = r1;
        r12 = r51;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:206:?, code lost:
        r1 = new com.inmobi.ads.bg
        r1.z = r8;
        r1.g = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:207:0x0451, code lost:
        if (r12.optBoolean("preload", false) == false) goto L_0x045e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:208:0x0453, code lost:
        r1.A = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:209:0x0455, code lost:
        r7 = r50;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:211:?, code lost:
        r7.k = r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:212:0x0459, code lost:
        r1 = r1;
        r48 = r15;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:213:0x045e, code lost:
        r7 = r50;
        r21 = r1;
        r48 = r15;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:214:0x0466, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:215:0x0467, code lost:
        r7 = r50;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:216:0x046b, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:217:0x046c, code lost:
        r7 = r50;
        r48 = r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:218:0x0472, code lost:
        r47 = r11;
        r31 = r23;
        r46 = r24;
        r32 = r25;
        r14 = r53;
        r11 = r5;
        r49 = r15;
        r15 = r12;
        r12 = r49;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:220:?, code lost:
        r11 = a(r2, r3, r4, r5, r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:221:0x048e, code lost:
        if (p(r51) == false) goto L_0x04c3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:225:0x049c, code lost:
        if (r12.getJSONObject("assetOnclick").isNull("openMode") != false) goto L_0x04b1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:226:0x049e, code lost:
        r26 = d(r12.getJSONObject("assetOnclick").getString("openMode"));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:227:0x04b1, code lost:
        r26 = 2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:228:0x04b3, code lost:
        r0 = r12.getJSONObject("assetOnclick").optString("fallbackUrl");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:229:0x04c0, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:230:0x04c3, code lost:
        r0 = null;
        r26 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:231:0x04c7, code lost:
        if (r18 == null) goto L_0x0505;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:234:0x04cd, code lost:
        if (r18.size() != 0) goto L_0x04d0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:240:0x04d8, code lost:
        if (com.facebook.share.internal.ShareConstants.IMAGE_URL.equals(r52) == false) goto L_0x04f0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:242:0x04e0, code lost:
        r8 = r8;
        r5 = r12;
        r2 = r14;
        r3 = r15;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:244:?, code lost:
        r8 = new com.inmobi.ads.as
        r8 = r8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:245:0x04f0, code lost:
        r5 = r12;
        r2 = r14;
        r3 = r15;
        r8 = new com.inmobi.ads.aq
        r8 = r8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:246:0x0505, code lost:
        r5 = r12;
        r2 = r14;
        r3 = r15;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:247:0x0510, code lost:
        if (com.facebook.share.internal.ShareConstants.IMAGE_URL.equals(r52) == false) goto L_0x0522;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:248:0x0512, code lost:
        r8 = new com.inmobi.ads.as
        r8 = r8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:249:0x0522, code lost:
        r8 = new com.inmobi.ads.aq(r3, r10, r11, c(r51), r26, r51);
        r8 = r8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:250:0x0531, code lost:
        r1.g = r2;
        a((com.inmobi.ads.ak) r1, r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:251:0x0536, code lost:
        if (r0 == null) goto L_0x053f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:252:0x0538, code lost:
        r1.b(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:253:0x053b, code lost:
        r48 = r3;
        r1 = r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:254:0x053f, code lost:
        r21 = r1;
        r48 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:255:0x0545, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:256:0x0546, code lost:
        r48 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:257:0x054a, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:258:0x054b, code lost:
        r6 = r52;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:259:0x054d, code lost:
        r48 = r15;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:260:0x0551, code lost:
        r47 = r11;
        r31 = r23;
        r46 = r24;
        r32 = r25;
        r13 = 0;
        r11 = r5;
        r5 = r15;
        r15 = r53;
        r8 = r5;
        r5 = r11;
        r11 = r14;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:262:?, code lost:
        r0 = a(r2, r3, r4, r5, r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:263:0x0570, code lost:
        if (r8.has("startOffset") == false) goto L_0x057d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:264:0x0572, code lost:
        r1 = r7.q(r8.getJSONObject("startOffset"));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:265:0x057d, code lost:
        r1 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:267:0x0585, code lost:
        if (r8.has("timerDuration") == false) goto L_0x0592;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:268:0x0587, code lost:
        r2 = r7.q(r8.getJSONObject("timerDuration"));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:269:0x0592, code lost:
        r2 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:270:0x0594, code lost:
        r3 = r8.optBoolean("displayTimer", true);
        r1 = new com.inmobi.ads.bb
        r1.z = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:271:0x05ac, code lost:
        if (r8.has("assetOnFinish") == false) goto L_0x05f9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:272:0x05ae, code lost:
        r0 = (org.json.JSONObject) r8.get("assetOnFinish");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:273:0x05bc, code lost:
        if (r0.has("action") == false) goto L_0x05f9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:274:0x05be, code lost:
        r0 = r0.getString("action").toUpperCase(java.util.Locale.US).trim();
        r2 = r0.hashCode();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:275:0x05d5, code lost:
        if (r2 == 2142494) goto L_0x05e7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:277:0x05da, code lost:
        if (r2 == 2402104) goto L_0x05dd;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:280:0x05e3, code lost:
        if (r0.equals("NONE") == false) goto L_0x05f1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:281:0x05e5, code lost:
        r0 = 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:283:0x05ed, code lost:
        if (r0.equals("EXIT") == false) goto L_0x05f1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:284:0x05ef, code lost:
        r0 = 2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:285:0x05f1, code lost:
        r0 = 65535;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:287:0x05f3, code lost:
        if (r0 == 2) goto L_0x05f6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:289:0x05f6, code lost:
        r13 = 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:290:0x05f7, code lost:
        r1.k = r13;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:291:0x05f9, code lost:
        r1.g = r15;
        r1 = r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:292:0x05fc, code lost:
        r47 = r11;
        r13 = r14;
        r8 = r15;
        r31 = r23;
        r46 = r24;
        r32 = r25;
        r15 = r53;
        r11 = r5;
        r1 = new com.inmobi.ads.ar
        r1.g = r15;
        r1 = r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:293:0x061c, code lost:
        r47 = r11;
        r13 = r14;
        r31 = r23;
        r46 = r24;
        r32 = r25;
        r15 = r53;
        r11 = r5;
        r2 = new com.inmobi.ads.az
        r2.g = r15;
        r1 = r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:294:0x0637, code lost:
        r48 = r12;
        r1 = r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:295:0x063b, code lost:
        r47 = r11;
        r6 = r15;
        r31 = r23;
        r46 = r24;
        r32 = r25;
        r15 = r53;
        r11 = r5;
        r5 = r14;
        r5 = r11;
        r11 = r6;
        r0 = a(r2, r3, r4, r5, r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:296:0x0653, code lost:
        if (r8 == null) goto L_0x066a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:298:0x0659, code lost:
        if (a(r8, r0) != false) goto L_0x066a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:299:0x065b, code lost:
        com.inmobi.commons.core.e.b.a();
        com.inmobi.commons.core.e.b.a(com.mintegral.msdk.base.entity.CampaignUnit.JSON_KEY_ADS, "InvalidContainerGeometry", new java.util.HashMap());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:301:0x066e, code lost:
        if (p(r51) == false) goto L_0x069d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:303:0x067c, code lost:
        if (r11.getJSONObject("assetOnclick").isNull("openMode") != false) goto L_0x068f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:304:0x067e, code lost:
        r1 = d(r11.getJSONObject("assetOnclick").getString("openMode"));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:305:0x068f, code lost:
        r1 = 2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:306:0x0690, code lost:
        r2 = r11.getJSONObject("assetOnclick").optString("fallbackUrl");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:307:0x069d, code lost:
        r2 = null;
        r1 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:309:0x06a6, code lost:
        if (r9.has("transitionEffect") == false) goto L_0x06dc;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:310:0x06a8, code lost:
        r3 = r9.getString("transitionEffect").trim();
        r4 = r3.hashCode();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:311:0x06b9, code lost:
        if (r4 == 3151468) goto L_0x06cb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:313:0x06be, code lost:
        if (r4 == 106426293) goto L_0x06c1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:316:0x06c7, code lost:
        if (r3.equals("paged") == false) goto L_0x06d5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:317:0x06c9, code lost:
        r3 = 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:319:0x06d1, code lost:
        if (r3.equals("free") == false) goto L_0x06d5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:320:0x06d3, code lost:
        r3 = 2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:321:0x06d5, code lost:
        r3 = 65535;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:323:0x06d7, code lost:
        if (r3 == 2) goto L_0x06da;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:325:0x06da, code lost:
        r3 = 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:327:0x06dd, code lost:
        r3 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:328:0x06de, code lost:
        if (r18 == null) goto L_0x0701;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:330:0x06e4, code lost:
        if (r18.size() != 0) goto L_0x06e7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:333:0x06e9, code lost:
        r8 = r8;
        r6 = r11;
        r48 = r12;
        r4 = 0;
        r13 = r1;
        r1 = r15;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:335:?, code lost:
        r8 = new com.inmobi.ads.am
        r15 = r8;
        r5 = r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:336:0x0701, code lost:
        r6 = r11;
        r48 = r12;
        r5 = r15;
        r4 = 0;
        r8 = new com.inmobi.ads.am
        r8 = r8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:337:0x0717, code lost:
        r15.g = r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:338:0x0719, code lost:
        if (r2 == null) goto L_0x071e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:339:0x071b, code lost:
        r15.b(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:340:0x071e, code lost:
        a((com.inmobi.ads.ak) r15, r6);
        r1 = r6.getJSONArray("assetValue");
        r2 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:342:0x072c, code lost:
        if (r2 >= r1.length()) goto L_0x082a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:343:0x072e, code lost:
        r3 = new java.lang.StringBuilder();
        r3.append(r5);
        r3.append(".assetValue[");
        r3.append(r2);
        r3.append(com.ironsource.sdk.constants.Constants.RequestParameters.RIGHT_BRACKETS);
        r3 = r3.toString();
        r6 = r1.getJSONObject(r2);
        r8 = f(r6).toLowerCase(java.util.Locale.US).trim();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:344:0x075d, code lost:
        switch(r8.hashCode()) {
            case -938102371: goto L_0x07bf;
            case -410956671: goto L_0x07b5;
            case 98832: goto L_0x07ab;
            case 102340: goto L_0x07a0;
            case 3226745: goto L_0x0796;
            case 3556653: goto L_0x078c;
            case 100313435: goto L_0x0782;
            case 110364485: goto L_0x0777;
            case 112202875: goto L_0x076d;
            case 1224424441: goto L_0x0762;
            default: goto L_0x0760;
        };
     */
    /* JADX WARNING: Code restructure failed: missing block: B:347:0x0768, code lost:
        if (r8.equals(com.ironsource.sdk.constants.Constants.ParametersKeys.WEB_VIEW) == false) goto L_0x07c9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:348:0x076a, code lost:
        r8 = 9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:350:0x0773, code lost:
        if (r8.equals("video") == false) goto L_0x07c9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:351:0x0775, code lost:
        r8 = 4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:353:0x077d, code lost:
        if (r8.equals("timer") == false) goto L_0x07c9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:354:0x077f, code lost:
        r8 = 8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:356:0x0788, code lost:
        if (r8.equals(com.facebook.share.internal.MessengerShareContentUtility.MEDIA_IMAGE) == false) goto L_0x07c9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:357:0x078a, code lost:
        r8 = 3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:359:0x0792, code lost:
        if (r8.equals(com.google.android.exoplayer2.util.MimeTypes.BASE_TYPE_TEXT) == false) goto L_0x07c9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:360:0x0794, code lost:
        r8 = 5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:362:0x079c, code lost:
        if (r8.equals(io.fabric.sdk.android.services.settings.SettingsJsonConstants.APP_ICON_KEY) == false) goto L_0x07c9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:363:0x079e, code lost:
        r8 = 2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:365:0x07a6, code lost:
        if (r8.equals("gif") == false) goto L_0x07c9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:366:0x07a8, code lost:
        r8 = 10;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:368:0x07b1, code lost:
        if (r8.equals("cta") == false) goto L_0x07c9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:369:0x07b3, code lost:
        r8 = 6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:371:0x07bb, code lost:
        if (r8.equals("container") == false) goto L_0x07c9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:372:0x07bd, code lost:
        r8 = 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:374:0x07c5, code lost:
        if (r8.equals(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_STAR) == false) goto L_0x07c9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:375:0x07c7, code lost:
        r8 = 7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:376:0x07c9, code lost:
        r8 = 65535;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:377:0x07ca, code lost:
        switch(r8) {
            case 2: goto L_0x07e8;
            case 3: goto L_0x07e5;
            case 4: goto L_0x07e2;
            case 5: goto L_0x07df;
            case 6: goto L_0x07dc;
            case 7: goto L_0x07d9;
            case 8: goto L_0x07d6;
            case 9: goto L_0x07d3;
            case 10: goto L_0x07d0;
            default: goto L_0x07cd;
        };
     */
    /* JADX WARNING: Code restructure failed: missing block: B:378:0x07cd, code lost:
        r8 = "CONTAINER";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:379:0x07d0, code lost:
        r8 = "GIF";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:380:0x07d3, code lost:
        r8 = "WEBVIEW";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:381:0x07d6, code lost:
        r8 = "TIMER";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:382:0x07d9, code lost:
        r8 = "RATING";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:383:0x07dc, code lost:
        r8 = "CTA";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:384:0x07df, code lost:
        r8 = com.startapp.android.publish.common.model.AdPreferences.TYPE_TEXT;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:385:0x07e2, code lost:
        r8 = com.facebook.share.internal.ShareConstants.VIDEO_URL;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:386:0x07e5, code lost:
        r8 = com.facebook.share.internal.ShareConstants.IMAGE_URL;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:387:0x07e8, code lost:
        r8 = "ICON";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:388:0x07ea, code lost:
        r8 = r7.a(r6, r8, r3, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:389:0x07ee, code lost:
        if (r8 != null) goto L_0x07fb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:390:0x07f0, code lost:
        new java.lang.StringBuilder("Cannot build asset from JSON: ").append(r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:391:0x07fb, code lost:
        r8.g = r3;
        r8.t = r15;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:392:0x0803, code lost:
        if (r15.C >= 16) goto L_0x0826;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:394:0x080a, code lost:
        if (r15.C != r15.B.length) goto L_0x081c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:395:0x080c, code lost:
        r3 = new com.inmobi.ads.ak[(r15.B.length * 2)];
        java.lang.System.arraycopy(r15.B, r4, r3, r4, r15.C);
        r15.B = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:396:0x081c, code lost:
        r3 = r15.B;
        r6 = r15.C;
        r15.C = r6 + 1;
        r3[r6] = r8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:397:0x0826, code lost:
        r2 = r2 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:398:0x082a, code lost:
        r1 = r15;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:399:0x082c, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:400:0x082e, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:401:0x082f, code lost:
        r48 = r12;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:408:0x084d, code lost:
        r1.n = r47;
        r1.o = r31;
        r1.p = r32;
        r2 = r46;
        r1.q = r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:409:0x085d, code lost:
        if (r2 == null) goto L_0x086d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:412:0x0865, code lost:
        r3 = r48;
        r7.p.put(r3, r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:413:0x086d, code lost:
        r3 = r48;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:418:0x087d, code lost:
        r7.o.put(r3, r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:419:0x0882, code lost:
        r2 = r52;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:420:0x088a, code lost:
        if (r7.h.containsKey(r2) != false) goto L_0x088c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:421:0x088c, code lost:
        ((java.util.List) r7.h.get(r2)).add(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:422:0x0898, code lost:
        r0 = new java.util.ArrayList();
        r0.add(r1);
        r7.h.put(r2, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:0x0157, code lost:
        r1 = 65535;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:0x0158, code lost:
        switch(r1) {
            case 0: goto L_0x063b;
            case 1: goto L_0x061c;
            case 2: goto L_0x05fc;
            case 3: goto L_0x0551;
            case 4: goto L_0x0472;
            case 5: goto L_0x0472;
            case 6: goto L_0x0403;
            case 7: goto L_0x0234;
            case 8: goto L_0x0226;
            case 9: goto L_0x0167;
            default: goto L_0x015b;
        };
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:0x015b, code lost:
        r47 = r11;
        r48 = r12;
        r31 = r23;
        r46 = r24;
        r32 = r25;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:86:0x016b, code lost:
        if (p(r51) != false) goto L_0x016e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:87:0x016d, code lost:
        return null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:88:0x016e, code lost:
        r13 = r23;
        r8 = r24;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:90:?, code lost:
        r1 = c(r2, r3, r4, r5, r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:91:0x0185, code lost:
        if (r15.getJSONObject("assetOnclick").isNull("openMode") != false) goto L_0x01a5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:95:0x0197, code lost:
        r26 = d(r15.getJSONObject("assetOnclick").getString("openMode"));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:96:0x019a, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:97:0x019b, code lost:
        r46 = r8;
        r47 = r11;
        r48 = r12;
        r31 = r13;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:98:0x01a5, code lost:
        r26 = 2;
     */
    /* JADX WARNING: Multi-variable type inference failed. Error: jadx.core.utils.exceptions.JadxRuntimeException: No candidate types for var: r1v27
  assigns: []
  uses: []
  mth insns count: 975
    	at jadx.core.dex.visitors.typeinference.TypeSearch.fillTypeCandidates(TypeSearch.java:237)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.typeinference.TypeSearch.run(TypeSearch.java:53)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.runMultiVariableSearch(TypeInferenceVisitor.java:99)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.visit(TypeInferenceVisitor.java:92)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:27)
    	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$1(DepthTraversal.java:14)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
    	at jadx.core.ProcessClass.process(ProcessClass.java:30)
    	at jadx.core.ProcessClass.lambda$processDependencies$0(ProcessClass.java:49)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:49)
    	at jadx.core.ProcessClass.process(ProcessClass.java:35)
    	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
    	at jadx.api.JavaClass.decompile(JavaClass.java:62)
    	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
     */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0086  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0088  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x008f  */
    /* JADX WARNING: Removed duplicated region for block: B:408:0x084d  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x00ef A[Catch:{ JSONException -> 0x0832 }] */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x00f1 A[Catch:{ JSONException -> 0x0832 }] */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x00fb A[Catch:{ JSONException -> 0x0832 }] */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x0105 A[Catch:{ JSONException -> 0x0832 }] */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x010f A[Catch:{ JSONException -> 0x0832 }] */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x0119 A[Catch:{ JSONException -> 0x0832 }] */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x0123 A[Catch:{ JSONException -> 0x0832 }] */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x012d A[Catch:{ JSONException -> 0x0832 }] */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x0137 A[Catch:{ JSONException -> 0x0832 }] */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x0142 A[Catch:{ JSONException -> 0x0832 }] */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x014d A[Catch:{ JSONException -> 0x0832 }] */
    /* JADX WARNING: Unknown variable types count: 16 */
    @TargetApi(15)
    private ak a(@NonNull JSONObject jSONObject, String str, String str2, @Nullable al alVar) {
        char c2;
        JSONArray o2;
        ak akVar;
        String str3;
        char c3;
        String str4;
        ak akVar2;
        ao aoVar = this;
        JSONObject jSONObject2 = jSONObject;
        String str5 = str;
        String str6 = str2;
        al alVar2 = alVar;
        String d2 = d(jSONObject);
        String e2 = e(jSONObject);
        JSONObject i2 = i(jSONObject);
        be beVar = null;
        if (!a(i2, str5)) {
            new StringBuilder("Asset style JSON: ").append(i2);
            return null;
        }
        Point j2 = j(jSONObject);
        Point a2 = aoVar.a(jSONObject2, j2);
        Point k2 = k(jSONObject);
        Point b2 = aoVar.b(jSONObject2, k2);
        List b3 = b(jSONObject);
        int l2 = l(jSONObject);
        int a3 = a(jSONObject2, true);
        int a4 = a(jSONObject2, false);
        String m2 = m(jSONObject);
        String trim = g(jSONObject).trim();
        int i3 = a3;
        int hashCode = trim.hashCode();
        String str7 = m2;
        int i4 = a4;
        if (hashCode == -925155509) {
            if (trim.equals("reference")) {
                c2 = 2;
                if (c2 == 2) {
                }
                o2 = o(jSONObject);
                if (o2 == null) {
                }
                str3 = null;
                akVar = null;
                switch (str.hashCode()) {
                    case -1919329183:
                        break;
                    case -1884772963:
                        break;
                    case 67056:
                        break;
                    case 70564:
                        break;
                    case 2241657:
                        break;
                    case 2571565:
                        break;
                    case 69775675:
                        break;
                    case 79826725:
                        break;
                    case 81665115:
                        break;
                    case 1942407129:
                        break;
                }
            }
        } else if (hashCode == 1728122231 && trim.equals(Constants.PATH_TYPE_ABSOLUTE)) {
            c2 = 1;
            boolean z = c2 == 2;
            o2 = o(jSONObject);
            if (o2 == null && o2.length() != 0) {
                try {
                    str4 = o2.getString(0);
                    try {
                        if (TextUtils.isEmpty(str4)) {
                            return null;
                        }
                        if (z) {
                            akVar = aoVar.b(str4);
                            if (akVar == null) {
                                try {
                                    if (aoVar.f != null) {
                                        akVar2 = aoVar.f.b(str4);
                                    }
                                } catch (JSONException e3) {
                                    e = e3;
                                    String str8 = str4;
                                    com.inmobi.commons.core.a.a.a().a(new com.inmobi.commons.core.e.a(e));
                                    str3 = str8;
                                    switch (str.hashCode()) {
                                        case -1919329183:
                                            break;
                                        case -1884772963:
                                            break;
                                        case 67056:
                                            break;
                                        case 70564:
                                            break;
                                        case 2241657:
                                            break;
                                        case 2571565:
                                            break;
                                        case 69775675:
                                            break;
                                        case 79826725:
                                            break;
                                        case 81665115:
                                            break;
                                        case 1942407129:
                                            break;
                                    }
                                }
                            }
                            akVar2 = akVar;
                        } else {
                            akVar2 = null;
                        }
                        akVar = akVar2;
                        str3 = str4;
                    } catch (JSONException e4) {
                        e = e4;
                        akVar = null;
                        String str82 = str4;
                        com.inmobi.commons.core.a.a.a().a(new com.inmobi.commons.core.e.a(e));
                        str3 = str82;
                        switch (str.hashCode()) {
                            case -1919329183:
                                break;
                            case -1884772963:
                                break;
                            case 67056:
                                break;
                            case 70564:
                                break;
                            case 2241657:
                                break;
                            case 2571565:
                                break;
                            case 69775675:
                                break;
                            case 79826725:
                                break;
                            case 81665115:
                                break;
                            case 1942407129:
                                break;
                        }
                    }
                } catch (JSONException e5) {
                    e = e5;
                    str4 = null;
                    akVar = null;
                    String str822 = str4;
                    com.inmobi.commons.core.a.a.a().a(new com.inmobi.commons.core.e.a(e));
                    str3 = str822;
                    switch (str.hashCode()) {
                        case -1919329183:
                            break;
                        case -1884772963:
                            break;
                        case 67056:
                            break;
                        case 70564:
                            break;
                        case 2241657:
                            break;
                        case 2571565:
                            break;
                        case 69775675:
                            break;
                        case 79826725:
                            break;
                        case 81665115:
                            break;
                        case 1942407129:
                            break;
                    }
                }
            } else {
                str3 = null;
                akVar = null;
            }
            switch (str.hashCode()) {
                case -1919329183:
                    if (str5.equals("CONTAINER")) {
                        c3 = 0;
                        break;
                    }
                case -1884772963:
                    if (str5.equals("RATING")) {
                        c3 = 8;
                        break;
                    }
                case 67056:
                    if (str5.equals("CTA")) {
                        c3 = 9;
                        break;
                    }
                case 70564:
                    if (str5.equals("GIF")) {
                        c3 = 5;
                        break;
                    }
                case 2241657:
                    if (str5.equals("ICON")) {
                        c3 = 2;
                        break;
                    }
                case 2571565:
                    if (str5.equals(AdPreferences.TYPE_TEXT)) {
                        c3 = 1;
                        break;
                    }
                case 69775675:
                    if (str5.equals(ShareConstants.IMAGE_URL)) {
                        c3 = 4;
                        break;
                    }
                case 79826725:
                    if (str5.equals("TIMER")) {
                        c3 = 3;
                        break;
                    }
                case 81665115:
                    if (str5.equals(ShareConstants.VIDEO_URL)) {
                        c3 = 7;
                        break;
                    }
                case 1942407129:
                    if (str5.equals("WEBVIEW")) {
                        c3 = 6;
                        break;
                    }
            }
        }
        c2 = 65535;
        if (c2 == 2) {
        }
        o2 = o(jSONObject);
        if (o2 == null) {
        }
        str3 = null;
        akVar = null;
        try {
            switch (str.hashCode()) {
                case -1919329183:
                    break;
                case -1884772963:
                    break;
                case 67056:
                    break;
                case 70564:
                    break;
                case 2241657:
                    break;
                case 2571565:
                    break;
                case 69775675:
                    break;
                case 79826725:
                    break;
                case 81665115:
                    break;
                case 1942407129:
                    break;
            }
        } catch (JSONException e6) {
            e = e6;
            int i5 = l2;
            String str9 = d2;
            int i6 = i3;
            String str10 = str7;
            int i7 = i4;
            com.inmobi.commons.core.a.a.a().a(new com.inmobi.commons.core.e.a(e));
            ? r1 = beVar;
            if (r1 != null) {
            }
            return r1;
        }
    }

    private static void a(@NonNull ak akVar, @NonNull JSONObject jSONObject) throws JSONException {
        String str = "";
        String str2 = "";
        boolean z = false;
        if (p(jSONObject)) {
            if (jSONObject.getJSONObject("assetOnclick").isNull("itemUrl")) {
                new StringBuilder("Missing itemUrl on asset ").append(jSONObject.toString());
            } else {
                str = jSONObject.getJSONObject("assetOnclick").getString("itemUrl");
                z = true;
            }
            if (!jSONObject.getJSONObject("assetOnclick").isNull("action")) {
                str2 = jSONObject.getJSONObject("assetOnclick").getString("action");
                z = true;
            }
        }
        akVar.a(str);
        akVar.j = str2;
        akVar.h = z;
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public final ak b(String str) {
        if (str == null || str.length() == 0) {
            return null;
        }
        if (this.o.get(str) != null) {
            return (ak) this.o.get(str);
        }
        if (this.f != null) {
            return (ak) this.f.o.get(str);
        }
        return null;
    }

    public final List<ak> c(String str) {
        if (this.h.containsKey(str)) {
            return (List) this.h.get(str);
        }
        return Collections.emptyList();
    }

    private static boolean a(JSONObject jSONObject, String str) {
        if (jSONObject.isNull("geometry")) {
            return false;
        }
        try {
            if (!a(jSONObject.getJSONArray("geometry"))) {
                return false;
            }
            char c2 = 65535;
            switch (str.hashCode()) {
                case -1919329183:
                    if (str.equals("CONTAINER")) {
                        c2 = 1;
                        break;
                    }
                    break;
                case 67056:
                    if (str.equals("CTA")) {
                        c2 = 9;
                        break;
                    }
                    break;
                case 70564:
                    if (str.equals("GIF")) {
                        c2 = 7;
                        break;
                    }
                    break;
                case 2241657:
                    if (str.equals("ICON")) {
                        c2 = 2;
                        break;
                    }
                    break;
                case 2571565:
                    if (str.equals(AdPreferences.TYPE_TEXT)) {
                        c2 = 8;
                        break;
                    }
                    break;
                case 69775675:
                    if (str.equals(ShareConstants.IMAGE_URL)) {
                        c2 = 3;
                        break;
                    }
                    break;
                case 79826725:
                    if (str.equals("TIMER")) {
                        c2 = 5;
                        break;
                    }
                    break;
                case 81665115:
                    if (str.equals(ShareConstants.VIDEO_URL)) {
                        c2 = 4;
                        break;
                    }
                    break;
                case 1942407129:
                    if (str.equals("WEBVIEW")) {
                        c2 = 6;
                        break;
                    }
                    break;
            }
            switch (c2) {
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                case 7:
                    return true;
                case 8:
                case 9:
                    if (jSONObject.isNull(MimeTypes.BASE_TYPE_TEXT)) {
                        return false;
                    }
                    try {
                        if (((int) Double.parseDouble(jSONObject.getJSONObject(MimeTypes.BASE_TYPE_TEXT).getString("size"))) > 0) {
                            return true;
                        }
                        return false;
                    } catch (NumberFormatException e2) {
                        com.inmobi.commons.core.a.a.a().a(new com.inmobi.commons.core.e.a(e2));
                        return false;
                    }
                default:
                    return false;
            }
            com.inmobi.commons.core.a.a.a().a(new com.inmobi.commons.core.e.a(e));
            return false;
        } catch (JSONException e3) {
            com.inmobi.commons.core.a.a.a().a(new com.inmobi.commons.core.e.a(e3));
            return false;
        }
    }

    private static boolean a(JSONArray jSONArray) {
        try {
            int i2 = jSONArray.getInt(2);
            int i3 = jSONArray.getInt(3);
            if (i2 <= 0 || i3 <= 0) {
                return false;
            }
            return true;
        } catch (JSONException e2) {
            com.inmobi.commons.core.a.a.a().a(new com.inmobi.commons.core.e.a(e2));
            return false;
        }
    }

    private static NativeTracker a(int i2, TrackerEventType trackerEventType, JSONObject jSONObject) throws JSONException {
        String trim = jSONObject.isNull("url") ? "" : jSONObject.getString("url").trim();
        HashMap hashMap = new HashMap();
        if (TrackerEventType.TRACKER_EVENT_TYPE_VIDEO_RENDER == trackerEventType) {
            JSONArray optJSONArray = jSONObject.optJSONArray(EventEntry.TABLE_NAME);
            if ((trim.length() == 0 || ((trim.startsWith("http") && !URLUtil.isValidUrl(trim)) || !trim.startsWith("http"))) && optJSONArray == null) {
                return null;
            }
            ArrayList arrayList = new ArrayList();
            if (optJSONArray != null) {
                for (int i3 = 0; i3 < optJSONArray.length(); i3++) {
                    TrackerEventType a2 = NativeTracker.a(optJSONArray.getString(i3));
                    if (a2 == TrackerEventType.TRACKER_EVENT_TYPE_CREATIVE_VIEW || a2 == TrackerEventType.TRACKER_EVENT_TYPE_PLAY || a2 == TrackerEventType.TRACKER_EVENT_TYPE_RENDER) {
                        arrayList.add(a2);
                    }
                }
            }
            hashMap.put("referencedEvents", arrayList);
        } else if (trim.length() == 0 || !URLUtil.isValidUrl(trim)) {
            return null;
        }
        HashMap hashMap2 = new HashMap();
        try {
            if (!jSONObject.isNull("params")) {
                JSONObject jSONObject2 = jSONObject.getJSONObject("params");
                Iterator keys = jSONObject2.keys();
                while (keys.hasNext()) {
                    String str = (String) keys.next();
                    hashMap2.put(str, jSONObject2.getString(str));
                }
            }
        } catch (JSONException e2) {
            com.inmobi.commons.core.a.a.a().a(new com.inmobi.commons.core.e.a(e2));
        }
        NativeTracker nativeTracker = new NativeTracker(trim, i2, trackerEventType, hashMap2);
        nativeTracker.d = new HashMap(hashMap);
        return nativeTracker;
    }

    private static List<NativeTracker> a(JSONObject jSONObject) {
        LinkedList linkedList = new LinkedList();
        try {
            JSONObject jSONObject2 = jSONObject.getJSONObject("passThroughJson");
            HashMap hashMap = new HashMap();
            if (!jSONObject2.isNull("macros")) {
                JSONObject jSONObject3 = jSONObject2.getJSONObject("macros");
                Iterator keys = jSONObject3.keys();
                while (keys.hasNext()) {
                    String str = (String) keys.next();
                    hashMap.put(str, jSONObject3.getString(str));
                }
            }
            if (!jSONObject2.isNull("urls")) {
                JSONArray jSONArray = jSONObject2.getJSONArray("urls");
                int length = jSONArray.length();
                for (int i2 = 0; i2 < length; i2++) {
                    linkedList.add(new NativeTracker(jSONArray.getString(i2), 0, TrackerEventType.TRACKER_EVENT_TYPE_IAS, hashMap));
                }
            }
            if (linkedList.isEmpty()) {
                linkedList.add(new NativeTracker("", 0, TrackerEventType.TRACKER_EVENT_TYPE_IAS, hashMap));
            }
        } catch (Exception e2) {
            new StringBuilder("Failed to parse IAS tracker : ").append(e2.getMessage());
            com.inmobi.commons.core.a.a.a().a(new com.inmobi.commons.core.e.a(e2));
        }
        return linkedList;
    }

    /* JADX WARNING: Removed duplicated region for block: B:100:0x0172 A[Catch:{ JSONException -> 0x019e }] */
    /* JADX WARNING: Removed duplicated region for block: B:101:0x0175 A[Catch:{ JSONException -> 0x019e }] */
    /* JADX WARNING: Removed duplicated region for block: B:102:0x0178 A[Catch:{ JSONException -> 0x019e }] */
    /* JADX WARNING: Removed duplicated region for block: B:103:0x017b A[Catch:{ JSONException -> 0x019e }] */
    /* JADX WARNING: Removed duplicated region for block: B:104:0x017e A[Catch:{ JSONException -> 0x019e }] */
    /* JADX WARNING: Removed duplicated region for block: B:121:0x0199 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0076 A[Catch:{ JSONException -> 0x019e }] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0079 A[Catch:{ JSONException -> 0x019e }] */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x007c A[Catch:{ JSONException -> 0x019e }] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x007f A[Catch:{ JSONException -> 0x019e }] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0089 A[Catch:{ JSONException -> 0x019e }] */
    private static List<NativeTracker> b(@NonNull JSONObject jSONObject) {
        char c2;
        String str;
        char c3;
        TrackerEventType trackerEventType;
        if (jSONObject.isNull("trackers")) {
            return null;
        }
        LinkedList linkedList = new LinkedList();
        try {
            JSONArray jSONArray = jSONObject.getJSONArray("trackers");
            int length = jSONArray.length();
            if (length == 0) {
                return linkedList;
            }
            for (int i2 = 0; i2 < length; i2++) {
                JSONObject jSONObject2 = jSONArray.getJSONObject(i2);
                if (!jSONObject2.isNull("trackerType")) {
                    String trim = jSONObject2.getString("trackerType").toUpperCase(Locale.US).trim();
                    int hashCode = trim.hashCode();
                    char c4 = 2;
                    if (hashCode != -1430070305) {
                        if (hashCode != -158113182) {
                            if (hashCode == 1110926088) {
                                if (trim.equals("URL_WEBVIEW_PING")) {
                                    c2 = 2;
                                    switch (c2) {
                                        case 1:
                                            str = "url_ping";
                                            break;
                                        case 2:
                                            str = "webview_ping";
                                            break;
                                        case 3:
                                            str = "html_script";
                                            break;
                                        default:
                                            str = "unknown";
                                            break;
                                    }
                                    if (!"url_ping".equals(str)) {
                                        int optInt = jSONObject2.optInt("eventId", 0);
                                        if (!jSONObject2.isNull("uiEvent")) {
                                            String string = jSONObject2.getString("uiEvent");
                                            String trim2 = string.toUpperCase(Locale.US).trim();
                                            switch (trim2.hashCode()) {
                                                case -1881262698:
                                                    if (trim2.equals("RENDER")) {
                                                        c3 = 3;
                                                        break;
                                                    }
                                                case -825499301:
                                                    if (trim2.equals("FALLBACK_URL_CLICK")) {
                                                        c3 = 8;
                                                        break;
                                                    }
                                                case -45894975:
                                                    if (trim2.equals("IAS_VIEWABILITY")) {
                                                        c3 = 7;
                                                        break;
                                                    }
                                                case 2342118:
                                                    if (trim2.equals("LOAD")) {
                                                        c3 = 1;
                                                        break;
                                                    }
                                                case 2634405:
                                                    if (trim2.equals("VIEW")) {
                                                        c3 = 4;
                                                        break;
                                                    }
                                                case 64212328:
                                                    if (trim2.equals("CLICK")) {
                                                        c3 = 5;
                                                        break;
                                                    }
                                                case 1963885793:
                                                    if (trim2.equals("VIDEO_VIEWABILITY")) {
                                                        c3 = 6;
                                                        break;
                                                    }
                                                case 2008409463:
                                                    if (trim2.equals("CLIENT_FILL")) {
                                                        c3 = 2;
                                                        break;
                                                    }
                                            }
                                            c3 = 65535;
                                            switch (c3) {
                                                case 1:
                                                    trackerEventType = TrackerEventType.TRACKER_EVENT_TYPE_LOAD;
                                                    break;
                                                case 2:
                                                    trackerEventType = TrackerEventType.TRACKER_EVENT_TYPE_CLIENT_FILL;
                                                    break;
                                                case 3:
                                                    trackerEventType = TrackerEventType.TRACKER_EVENT_TYPE_RENDER;
                                                    break;
                                                case 4:
                                                    trackerEventType = TrackerEventType.TRACKER_EVENT_TYPE_PAGE_VIEW;
                                                    break;
                                                case 5:
                                                    trackerEventType = TrackerEventType.TRACKER_EVENT_TYPE_CLICK;
                                                    break;
                                                case 6:
                                                    trackerEventType = TrackerEventType.TRACKER_EVENT_TYPE_VIDEO_RENDER;
                                                    break;
                                                case 7:
                                                    trackerEventType = TrackerEventType.TRACKER_EVENT_TYPE_IAS;
                                                    break;
                                                case 8:
                                                    trackerEventType = TrackerEventType.TRACKER_EVENT_TYPE_FALLBACK_URL;
                                                    break;
                                                default:
                                                    String trim3 = string.toUpperCase(Locale.US).trim();
                                                    int hashCode2 = trim3.hashCode();
                                                    if (hashCode2 == -1836567951) {
                                                        if (trim3.equals("DOWNLOADER_DOWNLOADED")) {
                                                            c4 = 3;
                                                            switch (c4) {
                                                                case 1:
                                                                    break;
                                                                case 2:
                                                                    break;
                                                                case 3:
                                                                    break;
                                                                case 4:
                                                                    break;
                                                            }
                                                        }
                                                    } else if (hashCode2 == -1099027408) {
                                                        if (trim3.equals("DOWNLOADER_DOWNLOADING")) {
                                                            switch (c4) {
                                                                case 1:
                                                                    break;
                                                                case 2:
                                                                    break;
                                                                case 3:
                                                                    break;
                                                                case 4:
                                                                    break;
                                                            }
                                                        }
                                                    } else if (hashCode2 == 1331888222) {
                                                        if (trim3.equals("DOWNLOADER_ERROR")) {
                                                            c4 = 4;
                                                            switch (c4) {
                                                                case 1:
                                                                    break;
                                                                case 2:
                                                                    break;
                                                                case 3:
                                                                    break;
                                                                case 4:
                                                                    break;
                                                            }
                                                        }
                                                    } else if (hashCode2 == 1346121898) {
                                                        if (trim3.equals("DOWNLOADER_INITIALIZED")) {
                                                            c4 = 1;
                                                            switch (c4) {
                                                                case 1:
                                                                    trackerEventType = TrackerEventType.TRACKER_EVENT_TYPE_DOWNLOADER_INIT;
                                                                    break;
                                                                case 2:
                                                                    trackerEventType = TrackerEventType.TRACKER_EVENT_TYPE_DOWNLOADER_DOWNLOADING;
                                                                    break;
                                                                case 3:
                                                                    trackerEventType = TrackerEventType.TRACKER_EVENT_TYPE_DOWNLOADER_DOWNLOADED;
                                                                    break;
                                                                case 4:
                                                                    trackerEventType = TrackerEventType.TRACKER_EVENT_TYPE_DOWNLOADER_ERROR;
                                                                    break;
                                                                default:
                                                                    trackerEventType = TrackerEventType.TRACKER_EVENT_TYPE_UNKNOWN;
                                                                    break;
                                                            }
                                                        }
                                                    }
                                                    c4 = 65535;
                                                    switch (c4) {
                                                        case 1:
                                                            break;
                                                        case 2:
                                                            break;
                                                        case 3:
                                                            break;
                                                        case 4:
                                                            break;
                                                    }
                                            }
                                            if (TrackerEventType.TRACKER_EVENT_TYPE_UNKNOWN != trackerEventType) {
                                                if (TrackerEventType.TRACKER_EVENT_TYPE_IAS != trackerEventType) {
                                                    NativeTracker a2 = a(optInt, trackerEventType, jSONObject2);
                                                    if (a2 != null) {
                                                        linkedList.add(a2);
                                                    }
                                                } else {
                                                    linkedList.addAll(a(jSONObject2));
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        } else if (trim.equals("URL_PING")) {
                            c2 = 1;
                            switch (c2) {
                                case 1:
                                    break;
                                case 2:
                                    break;
                                case 3:
                                    break;
                            }
                            if (!"url_ping".equals(str)) {
                            }
                        }
                    } else if (trim.equals("HTML_SCRIPT")) {
                        c2 = 3;
                        switch (c2) {
                            case 1:
                                break;
                            case 2:
                                break;
                            case 3:
                                break;
                        }
                        if (!"url_ping".equals(str)) {
                        }
                    }
                    c2 = 65535;
                    switch (c2) {
                        case 1:
                            break;
                        case 2:
                            break;
                        case 3:
                            break;
                    }
                    if (!"url_ping".equals(str)) {
                    }
                }
            }
            return linkedList;
        } catch (JSONException e2) {
            com.inmobi.commons.core.a.a.a().a(new com.inmobi.commons.core.e.a(e2));
            return linkedList;
        }
    }

    @Nullable
    private bz a(@NonNull JSONObject jSONObject, @NonNull String str, ak akVar) {
        if (f(jSONObject).equalsIgnoreCase(ShareConstants.VIDEO_URL)) {
            try {
                JSONArray jSONArray = jSONObject.getJSONArray("assetValue");
                if (jSONArray != null) {
                    if (jSONArray.length() != 0) {
                        if (akVar == null || !(akVar instanceof be)) {
                            return new bv(this.r.m).a(str);
                        }
                        return (bz) akVar.e;
                    }
                }
                return null;
            } catch (JSONException e2) {
                com.inmobi.commons.core.a.a.a().a(new com.inmobi.commons.core.e.a(e2));
            }
        }
        return null;
    }

    private static String c(@NonNull JSONObject jSONObject) {
        try {
            if ((f(jSONObject).equalsIgnoreCase("ICON") || f(jSONObject).equalsIgnoreCase(ShareConstants.IMAGE_URL) || f(jSONObject).equalsIgnoreCase("GIF")) && jSONObject.getJSONArray("assetValue").getString(0).length() != 0) {
                return jSONObject.getJSONArray("assetValue").getString(0);
            }
        } catch (JSONException e2) {
            com.inmobi.commons.core.a.a.a().a(new com.inmobi.commons.core.e.a(e2));
        }
        return "";
    }

    private static String d(@NonNull JSONObject jSONObject) {
        try {
            return jSONObject.getString("assetId");
        } catch (JSONException e2) {
            com.inmobi.commons.core.a.a.a().a(new com.inmobi.commons.core.e.a(e2));
            return Integer.toString(jSONObject.hashCode());
        }
    }

    private static String e(@NonNull JSONObject jSONObject) {
        try {
            return jSONObject.getString("assetName");
        } catch (JSONException e2) {
            com.inmobi.commons.core.a.a.a().a(new com.inmobi.commons.core.e.a(e2));
            return "";
        }
    }

    private static String f(@NonNull JSONObject jSONObject) {
        try {
            return jSONObject.getString("assetType");
        } catch (JSONException e2) {
            com.inmobi.commons.core.a.a.a().a(new com.inmobi.commons.core.e.a(e2));
            return "";
        }
    }

    private static String g(@NonNull JSONObject jSONObject) {
        try {
            return jSONObject.getString("valueType");
        } catch (JSONException e2) {
            com.inmobi.commons.core.a.a.a().a(new com.inmobi.commons.core.e.a(e2));
            return "";
        }
    }

    private static String h(@NonNull JSONObject jSONObject) {
        try {
            return jSONObject.getString("dataType");
        } catch (JSONException e2) {
            com.inmobi.commons.core.a.a.a().a(new com.inmobi.commons.core.e.a(e2));
            return "";
        }
    }

    @NonNull
    private JSONObject i(@NonNull JSONObject jSONObject) {
        JSONObject jSONObject2;
        JSONObject jSONObject3;
        try {
            if (jSONObject.isNull("assetStyle")) {
                jSONObject2 = null;
            } else {
                jSONObject2 = jSONObject.getJSONObject("assetStyle");
            }
            if (jSONObject2 == null) {
                if (jSONObject.isNull("assetStyleRef")) {
                    return new JSONObject();
                }
                String string = jSONObject.getString("assetStyleRef");
                if (this.n == null) {
                    jSONObject3 = new JSONObject();
                } else {
                    jSONObject3 = this.n.getJSONObject(string);
                }
                jSONObject2 = jSONObject3;
            }
            return jSONObject2;
        } catch (JSONException e2) {
            com.inmobi.commons.core.a.a.a().a(new com.inmobi.commons.core.e.a(e2));
            return new JSONObject();
        }
    }

    private Point j(@NonNull JSONObject jSONObject) {
        Point point = new Point();
        try {
            JSONObject i2 = i(jSONObject);
            if (i2.isNull("geometry")) {
                return point;
            }
            JSONArray jSONArray = i2.getJSONArray("geometry");
            point.x = c.a(jSONArray.getInt(0));
            point.y = c.a(jSONArray.getInt(1));
            return point;
        } catch (JSONException e2) {
            com.inmobi.commons.core.a.a.a().a(new com.inmobi.commons.core.e.a(e2));
        }
    }

    private Point a(@NonNull JSONObject jSONObject, @NonNull Point point) {
        try {
            JSONObject i2 = i(jSONObject);
            if (i2.isNull("finalGeometry")) {
                return point;
            }
            Point point2 = new Point();
            JSONArray jSONArray = i2.getJSONArray("finalGeometry");
            point2.x = c.a(jSONArray.getInt(0));
            point2.y = c.a(jSONArray.getInt(1));
            point = point2;
            return point;
        } catch (JSONException unused) {
        }
    }

    private Point k(@NonNull JSONObject jSONObject) {
        Point point = new Point();
        try {
            JSONObject i2 = i(jSONObject);
            if (i2.isNull("geometry")) {
                return point;
            }
            JSONArray jSONArray = i2.getJSONArray("geometry");
            point.x = c.a(jSONArray.getInt(2));
            point.y = c.a(jSONArray.getInt(3));
            return point;
        } catch (JSONException e2) {
            com.inmobi.commons.core.a.a.a().a(new com.inmobi.commons.core.e.a(e2));
        }
    }

    private Point b(@NonNull JSONObject jSONObject, @NonNull Point point) {
        try {
            JSONObject i2 = i(jSONObject);
            if (i2.isNull("finalGeometry")) {
                return point;
            }
            Point point2 = new Point();
            JSONArray jSONArray = i2.getJSONArray("finalGeometry");
            point2.x = c.a(jSONArray.getInt(2));
            point2.y = c.a(jSONArray.getInt(3));
            point = point2;
            return point;
        } catch (JSONException unused) {
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:23:0x0056 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0057  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0059 A[RETURN] */
    private static int l(@NonNull JSONObject jSONObject) {
        char c2 = 2;
        try {
            JSONObject n2 = n(jSONObject);
            if (!n2.isNull("type")) {
                String lowerCase = n2.getString("type").trim().toLowerCase(Locale.US);
                int hashCode = lowerCase.hashCode();
                if (hashCode == -921832806) {
                    if (lowerCase.equals("percentage")) {
                        c2 = 3;
                        switch (c2) {
                            case 2:
                                break;
                            case 3:
                                break;
                        }
                    }
                } else if (hashCode == -284840886) {
                    if (lowerCase.equals("unknown")) {
                        c2 = 1;
                        switch (c2) {
                            case 2:
                                break;
                            case 3:
                                break;
                        }
                    }
                } else if (hashCode == 1728122231) {
                    if (lowerCase.equals(Constants.PATH_TYPE_ABSOLUTE)) {
                        switch (c2) {
                            case 2:
                                return 3;
                            case 3:
                                return 4;
                            default:
                                return 1;
                        }
                    }
                }
                c2 = 65535;
                switch (c2) {
                    case 2:
                        break;
                    case 3:
                        break;
                }
            } else {
                return 2;
            }
        } catch (JSONException e2) {
            com.inmobi.commons.core.a.a.a().a(new com.inmobi.commons.core.e.a(e2));
            return 2;
        }
    }

    private static int a(@NonNull JSONObject jSONObject, boolean z) {
        try {
            JSONObject n2 = n(jSONObject);
            if (n2.isNull(z ? AdvertisementColumns.COLUMN_DELAY : "hideAfterDelay")) {
                return -1;
            }
            int i2 = n2.getInt(z ? AdvertisementColumns.COLUMN_DELAY : "hideAfterDelay");
            if (3 == l(jSONObject)) {
                return i2;
            }
            if (4 != l(jSONObject) || i2 <= 0 || i2 > 100) {
                return -1;
            }
            int[] iArr = {25, 50, 75, 100};
            double d2 = Double.MAX_VALUE;
            int i3 = -1;
            for (int i4 = 0; i4 < 4; i4++) {
                int i5 = i2 - iArr[i4];
                double d3 = (double) (i5 * i5);
                if (d3 < d2) {
                    i3 = i4;
                    d2 = d3;
                }
            }
            return iArr[i3];
        } catch (JSONException e2) {
            com.inmobi.commons.core.a.a.a().a(new com.inmobi.commons.core.e.a(e2));
            return -1;
        }
    }

    private static String m(@NonNull JSONObject jSONObject) {
        try {
            JSONObject n2 = n(jSONObject);
            if (n2.isNull("reference")) {
                return "";
            }
            return n2.getString("reference");
        } catch (JSONException e2) {
            com.inmobi.commons.core.a.a.a().a(new com.inmobi.commons.core.e.a(e2));
            return "";
        }
    }

    private static JSONObject n(@NonNull JSONObject jSONObject) {
        if (jSONObject.isNull("display")) {
            return new JSONObject();
        }
        try {
            return jSONObject.getJSONObject("display");
        } catch (JSONException e2) {
            com.inmobi.commons.core.a.a.a().a(new com.inmobi.commons.core.e.a(e2));
            return new JSONObject();
        }
    }

    private static JSONArray o(@NonNull JSONObject jSONObject) {
        try {
            return jSONObject.getJSONArray("assetValue");
        } catch (JSONException e2) {
            com.inmobi.commons.core.a.a.a().a(new com.inmobi.commons.core.e.a(e2));
            return new JSONArray();
        }
    }

    private static boolean p(@NonNull JSONObject jSONObject) {
        return !jSONObject.isNull("assetOnclick");
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x0053 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0054 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0055 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0056 A[RETURN] */
    private static int d(@NonNull String str) {
        char c2;
        String trim = str.toUpperCase(Locale.US).trim();
        int hashCode = trim.hashCode();
        if (hashCode == -2084521848) {
            if (trim.equals("DOWNLOAD")) {
                c2 = 4;
                switch (c2) {
                    case 2:
                        break;
                    case 3:
                        break;
                    case 4:
                        break;
                }
            }
        } else if (hashCode == -1038134325) {
            if (trim.equals("EXTERNAL")) {
                c2 = 1;
                switch (c2) {
                    case 2:
                        break;
                    case 3:
                        break;
                    case 4:
                        break;
                }
            }
        } else if (hashCode == 69805756) {
            if (trim.equals("INAPP")) {
                c2 = 2;
                switch (c2) {
                    case 2:
                        break;
                    case 3:
                        break;
                    case 4:
                        break;
                }
            }
        } else if (hashCode == 1411860198 && trim.equals("DEEPLINK")) {
            c2 = 3;
            switch (c2) {
                case 2:
                    return 1;
                case 3:
                    return 3;
                case 4:
                    return 4;
                default:
                    return 2;
            }
        }
        c2 = 65535;
        switch (c2) {
            case 2:
                break;
            case 3:
                break;
            case 4:
                break;
        }
    }

    private static String e(@NonNull String str) {
        char c2;
        String trim = str.toLowerCase(Locale.US).trim();
        switch (trim.hashCode()) {
            case -1178781136:
                if (trim.equals(TtmlNode.ITALIC)) {
                    c2 = 3;
                    break;
                }
            case -1026963764:
                if (trim.equals(TtmlNode.UNDERLINE)) {
                    c2 = 5;
                    break;
                }
            case -891985998:
                if (trim.equals("strike")) {
                    c2 = 4;
                    break;
                }
            case 3029637:
                if (trim.equals(TtmlNode.BOLD)) {
                    c2 = 2;
                    break;
                }
            case 3387192:
                if (trim.equals("none")) {
                    c2 = 1;
                    break;
                }
            default:
                c2 = 65535;
                break;
        }
        switch (c2) {
            case 2:
                return TtmlNode.BOLD;
            case 3:
                return TtmlNode.ITALIC;
            case 4:
                return "strike";
            case 5:
                return TtmlNode.UNDERLINE;
            default:
                return "none";
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0031  */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0034  */
    private static String f(@NonNull String str) {
        char c2;
        String trim = str.toLowerCase(Locale.US).trim();
        int hashCode = trim.hashCode();
        if (hashCode != 3321844) {
            if (hashCode == 3387192 && trim.equals("none")) {
                c2 = 1;
                return c2 != 2 ? "none" : "line";
            }
        } else if (trim.equals("line")) {
            c2 = 2;
            if (c2 != 2) {
            }
        }
        c2 = 65535;
        if (c2 != 2) {
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0031  */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0034  */
    private static String g(@NonNull String str) {
        char c2;
        String trim = str.toLowerCase(Locale.US).trim();
        int hashCode = trim.hashCode();
        if (hashCode != -1349116587) {
            if (hashCode == 1787472634 && trim.equals("straight")) {
                c2 = 1;
                return c2 != 2 ? "straight" : "curved";
            }
        } else if (trim.equals("curved")) {
            c2 = 2;
            if (c2 != 2) {
            }
        }
        c2 = 65535;
        if (c2 != 2) {
        }
    }

    private com.inmobi.ads.ba.a q(JSONObject jSONObject) {
        com.inmobi.ads.ba.a aVar = new com.inmobi.ads.ba.a(jSONObject.optLong(Constants.PATH_TYPE_ABSOLUTE), jSONObject.optLong("percentage"), jSONObject.optString("reference"), this);
        return aVar;
    }

    private com.inmobi.ads.ba.a r(JSONObject jSONObject) {
        if (jSONObject == null) {
            return null;
        }
        com.inmobi.ads.ba.a aVar = new com.inmobi.ads.ba.a(jSONObject.optLong(Constants.PATH_TYPE_ABSOLUTE), jSONObject.optLong("percentage"), jSONObject.optString("reference"), this);
        return aVar;
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x0077  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x007c  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0091  */
    private al a(@NonNull Point point, @NonNull Point point2, @NonNull Point point3, @NonNull Point point4, @NonNull JSONObject jSONObject) throws JSONException {
        String str;
        String str2;
        String str3;
        String trim;
        String str4;
        String trim2;
        String str5;
        String str6;
        String str7;
        Point point5 = point;
        Point point6 = point2;
        Point point7 = point3;
        Point point8 = point4;
        JSONObject jSONObject2 = jSONObject;
        if (jSONObject2.isNull("border")) {
            str5 = "none";
            str6 = "straight";
            str7 = "#ff000000";
        } else {
            JSONObject jSONObject3 = jSONObject2.getJSONObject("border");
            if (jSONObject3.isNull("style")) {
                str5 = "none";
                str6 = "straight";
                str7 = "#ff000000";
            } else {
                String f2 = f(jSONObject3.getString("style"));
                if (jSONObject3.isNull("corner")) {
                    str4 = "straight";
                } else {
                    str4 = g(jSONObject3.getString("corner"));
                }
                if (jSONObject3.isNull("color")) {
                    trim2 = "#ff000000";
                } else {
                    trim2 = jSONObject3.getString("color").trim();
                }
                str = trim2;
                str3 = f2;
                str2 = str4;
                if (!jSONObject2.isNull("backgroundColor")) {
                    trim = "#00000000";
                } else {
                    trim = jSONObject2.getString("backgroundColor").trim();
                }
                String str8 = trim;
                String str9 = "fill";
                if (!jSONObject2.isNull("contentMode")) {
                    String trim3 = jSONObject2.getString("contentMode").trim();
                    char c2 = 65535;
                    int hashCode = trim3.hashCode();
                    if (hashCode != -1626174665) {
                        if (hashCode != -1362001767) {
                            if (hashCode != 3143043) {
                                if (hashCode == 727618043 && trim3.equals("aspectFill")) {
                                    c2 = 3;
                                }
                            } else if (trim3.equals("fill")) {
                                c2 = 2;
                            }
                        } else if (trim3.equals("aspectFit")) {
                            c2 = 4;
                        }
                    } else if (trim3.equals("unspecified")) {
                        c2 = 1;
                    }
                    switch (c2) {
                        case 2:
                            str9 = "fill";
                            break;
                        case 3:
                            str9 = "aspectFill";
                            break;
                        case 4:
                            str9 = "aspectFit";
                            break;
                        default:
                            str9 = "unspecified";
                            break;
                    }
                }
                al alVar = new al(point5.x, point5.y, point6.x, point6.y, point7.x, point7.y, point8.x, point8.y, str9, str3, str2, str, str8, s(jSONObject2));
                return alVar;
            }
        }
        str3 = str5;
        str2 = str6;
        str = str7;
        if (!jSONObject2.isNull("backgroundColor")) {
        }
        String str82 = trim;
        String str92 = "fill";
        if (!jSONObject2.isNull("contentMode")) {
        }
        al alVar2 = new al(point5.x, point5.y, point6.x, point6.y, point7.x, point7.y, point8.x, point8.y, str92, str3, str2, str, str82, s(jSONObject2));
        return alVar2;
    }

    private ba s(JSONObject jSONObject) throws JSONException {
        com.inmobi.ads.ba.a aVar = null;
        com.inmobi.ads.ba.a r2 = !jSONObject.isNull("startOffset") ? r(jSONObject.optJSONObject("startOffset")) : null;
        if (!jSONObject.isNull("timerDuration")) {
            aVar = r(jSONObject.optJSONObject("timerDuration"));
        }
        return new ba(r2, aVar);
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x0077  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x007c  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00a0  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00a7  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x00bb  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00c0  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00d5  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00de  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x0113  */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x0155  */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x0157  */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x015d  */
    private a b(@NonNull Point point, @NonNull Point point2, @NonNull Point point3, @NonNull Point point4, @NonNull JSONObject jSONObject) throws JSONException {
        String str;
        String str2;
        String str3;
        String trim;
        JSONObject jSONObject2;
        int i2;
        String trim2;
        String[] strArr;
        int i3;
        ao aoVar;
        char c2;
        String[] strArr2;
        String str4;
        String trim3;
        String str5;
        String str6;
        String str7;
        Point point5 = point;
        Point point6 = point2;
        Point point7 = point3;
        Point point8 = point4;
        JSONObject jSONObject3 = jSONObject;
        if (jSONObject3.isNull("border")) {
            str5 = "none";
            str6 = "straight";
            str7 = "#ff000000";
        } else {
            JSONObject jSONObject4 = jSONObject3.getJSONObject("border");
            if (jSONObject4.isNull("style")) {
                str5 = "none";
                str6 = "straight";
                str7 = "#ff000000";
            } else {
                String f2 = f(jSONObject4.getString("style"));
                if (jSONObject4.isNull("corner")) {
                    str4 = "straight";
                } else {
                    str4 = g(jSONObject4.getString("corner"));
                }
                if (jSONObject4.isNull("color")) {
                    trim3 = "#ff000000";
                } else {
                    trim3 = jSONObject4.getString("color").trim();
                }
                str = trim3;
                str3 = f2;
                str2 = str4;
                if (!jSONObject3.isNull("backgroundColor")) {
                    trim = "#00000000";
                } else {
                    trim = jSONObject3.getString("backgroundColor").trim();
                }
                String str8 = trim;
                jSONObject2 = jSONObject3.getJSONObject(MimeTypes.BASE_TYPE_TEXT);
                int parseDouble = (int) Double.parseDouble(jSONObject2.getString("size"));
                if (!jSONObject2.isNull("length")) {
                    i2 = Integer.MAX_VALUE;
                } else {
                    i2 = Integer.parseInt(jSONObject2.getString("length"));
                }
                if (!jSONObject2.isNull("color")) {
                    trim2 = "#ff000000";
                } else {
                    trim2 = jSONObject2.getString("color").trim();
                }
                String str9 = trim2;
                if (!jSONObject2.isNull("style")) {
                    strArr2 = new String[]{"none"};
                } else {
                    int length = jSONObject2.getJSONArray("style").length();
                    if (length == 0) {
                        strArr2 = new String[]{"none"};
                    } else {
                        String[] strArr3 = new String[length];
                        for (int i4 = 0; i4 < length; i4++) {
                            strArr3[i4] = e(jSONObject2.getJSONArray("style").getString(i4));
                        }
                        strArr = strArr3;
                        if (!jSONObject2.isNull("align")) {
                            String trim4 = jSONObject2.getString("align").trim();
                            int hashCode = trim4.hashCode();
                            int i5 = 2;
                            if (hashCode != -1364013605) {
                                if (hashCode != 3317767) {
                                    if (hashCode == 108511772 && trim4.equals("right")) {
                                        c2 = 2;
                                        switch (c2) {
                                            case 2:
                                                i5 = 1;
                                                break;
                                            case 3:
                                                break;
                                            default:
                                                i5 = 0;
                                                break;
                                        }
                                        aoVar = this;
                                        i3 = i5;
                                    }
                                } else if (trim4.equals("left")) {
                                    c2 = 1;
                                    switch (c2) {
                                        case 2:
                                            break;
                                        case 3:
                                            break;
                                    }
                                    aoVar = this;
                                    i3 = i5;
                                }
                            } else if (trim4.equals("centre")) {
                                c2 = 3;
                                switch (c2) {
                                    case 2:
                                        break;
                                    case 3:
                                        break;
                                }
                                aoVar = this;
                                i3 = i5;
                            }
                            c2 = 65535;
                            switch (c2) {
                                case 2:
                                    break;
                                case 3:
                                    break;
                            }
                            aoVar = this;
                            i3 = i5;
                        } else {
                            i3 = 0;
                            aoVar = this;
                        }
                        a aVar = new a(point5.x, point5.y, point6.x, point6.y, point7.x, point7.y, point8.x, point8.y, str3, str2, str, str8, parseDouble, i3, i2, str9, strArr, aoVar.s(jSONObject3));
                        return aVar;
                    }
                }
                strArr = strArr2;
                if (!jSONObject2.isNull("align")) {
                }
                a aVar2 = new a(point5.x, point5.y, point6.x, point6.y, point7.x, point7.y, point8.x, point8.y, str3, str2, str, str8, parseDouble, i3, i2, str9, strArr, aoVar.s(jSONObject3));
                return aVar2;
            }
        }
        str3 = str5;
        str2 = str6;
        str = str7;
        if (!jSONObject3.isNull("backgroundColor")) {
        }
        String str82 = trim;
        jSONObject2 = jSONObject3.getJSONObject(MimeTypes.BASE_TYPE_TEXT);
        try {
            int parseDouble2 = (int) Double.parseDouble(jSONObject2.getString("size"));
            if (!jSONObject2.isNull("length")) {
            }
            if (!jSONObject2.isNull("color")) {
            }
            String str92 = trim2;
            if (!jSONObject2.isNull("style")) {
            }
            strArr = strArr2;
            if (!jSONObject2.isNull("align")) {
            }
            a aVar22 = new a(point5.x, point5.y, point6.x, point6.y, point7.x, point7.y, point8.x, point8.y, str3, str2, str, str82, parseDouble2, i3, i2, str92, strArr, aoVar.s(jSONObject3));
            return aVar22;
        } catch (NumberFormatException e2) {
            JSONException jSONException = new JSONException(e2.getMessage());
            jSONException.initCause(e2);
            com.inmobi.commons.core.a.a.a().a(new com.inmobi.commons.core.e.a(e2));
            throw jSONException;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x0077  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x007c  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00a0  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00a5  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00ba  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x00c5  */
    private a c(@NonNull Point point, @NonNull Point point2, @NonNull Point point3, @NonNull Point point4, @NonNull JSONObject jSONObject) throws JSONException {
        String str;
        String str2;
        String str3;
        String trim;
        JSONObject jSONObject2;
        String trim2;
        String[] strArr;
        ao aoVar;
        String[] strArr2;
        String str4;
        String trim3;
        String str5;
        String str6;
        String str7;
        Point point5 = point;
        Point point6 = point2;
        Point point7 = point3;
        Point point8 = point4;
        JSONObject jSONObject3 = jSONObject;
        if (jSONObject3.isNull("border")) {
            str5 = "none";
            str6 = "straight";
            str7 = "#ff000000";
        } else {
            JSONObject jSONObject4 = jSONObject3.getJSONObject("border");
            if (jSONObject4.isNull("style")) {
                str5 = "none";
                str6 = "straight";
                str7 = "#ff000000";
            } else {
                String f2 = f(jSONObject4.getString("style"));
                if (jSONObject4.isNull("corner")) {
                    str4 = "straight";
                } else {
                    str4 = g(jSONObject4.getString("corner"));
                }
                if (jSONObject4.isNull("color")) {
                    trim3 = "#ff000000";
                } else {
                    trim3 = jSONObject4.getString("color").trim();
                }
                str = trim3;
                str3 = f2;
                str2 = str4;
                if (!jSONObject3.isNull("backgroundColor")) {
                    trim = "#00000000";
                } else {
                    trim = jSONObject3.getString("backgroundColor").trim();
                }
                String str8 = trim;
                jSONObject2 = jSONObject3.getJSONObject(MimeTypes.BASE_TYPE_TEXT);
                int parseDouble = (int) Double.parseDouble(jSONObject2.getString("size"));
                if (!jSONObject2.isNull("color")) {
                    trim2 = "#ff000000";
                } else {
                    trim2 = jSONObject2.getString("color").trim();
                }
                String str9 = trim2;
                if (!jSONObject2.isNull("style")) {
                    strArr2 = new String[]{"none"};
                } else {
                    int length = jSONObject2.getJSONArray("style").length();
                    if (length == 0) {
                        strArr2 = new String[]{"none"};
                    } else {
                        String[] strArr3 = new String[length];
                        for (int i2 = 0; i2 < length; i2++) {
                            strArr3[i2] = e(jSONObject2.getJSONArray("style").getString(i2));
                        }
                        aoVar = this;
                        strArr = strArr3;
                        a aVar = new a(point5.x, point5.y, point6.x, point6.y, point7.x, point7.y, point8.x, point8.y, str3, str2, str, str8, parseDouble, str9, strArr, aoVar.s(jSONObject3));
                        return aVar;
                    }
                }
                strArr = strArr2;
                aoVar = this;
                a aVar2 = new a(point5.x, point5.y, point6.x, point6.y, point7.x, point7.y, point8.x, point8.y, str3, str2, str, str8, parseDouble, str9, strArr, aoVar.s(jSONObject3));
                return aVar2;
            }
        }
        str3 = str5;
        str2 = str6;
        str = str7;
        if (!jSONObject3.isNull("backgroundColor")) {
        }
        String str82 = trim;
        jSONObject2 = jSONObject3.getJSONObject(MimeTypes.BASE_TYPE_TEXT);
        try {
            int parseDouble2 = (int) Double.parseDouble(jSONObject2.getString("size"));
            if (!jSONObject2.isNull("color")) {
            }
            String str92 = trim2;
            if (!jSONObject2.isNull("style")) {
            }
            strArr = strArr2;
            aoVar = this;
            a aVar22 = new a(point5.x, point5.y, point6.x, point6.y, point7.x, point7.y, point8.x, point8.y, str3, str2, str, str82, parseDouble2, str92, strArr, aoVar.s(jSONObject3));
            return aVar22;
        } catch (NumberFormatException e2) {
            JSONException jSONException = new JSONException(e2.getMessage());
            jSONException.initCause(e2);
            com.inmobi.commons.core.a.a.a().a(new com.inmobi.commons.core.e.a(e2));
            throw jSONException;
        }
    }

    private static boolean a(@NonNull am amVar) {
        return "card_scrollable".equalsIgnoreCase(amVar.d);
    }

    private static void a(be beVar) {
        beVar.x = 8;
        HashMap hashMap = new HashMap();
        hashMap.put("[ERRORCODE]", "601");
        beVar.a(TrackerEventType.TRACKER_EVENT_TYPE_ERROR, (Map<String, String>) hashMap);
    }

    @Nullable
    private static String a(bu buVar, bg bgVar) {
        if ("REF_HTML".equals(bgVar.z)) {
            List a2 = buVar.a(2);
            if (a2.size() > 0) {
                return ((a) a2.get(0)).b;
            }
            List a3 = buVar.a(3);
            if (a3.size() > 0) {
                String str = ((a) a3.get(0)).b;
                if (URLUtil.isValidUrl(str)) {
                    bgVar.z = "REF_IFRAME";
                    return str;
                }
                a("MalformedURL", "Rich", "REF_HTML", (String) null, (String) null);
            }
        } else if ("REF_IFRAME".equals(bgVar.z)) {
            List a4 = buVar.a(3);
            if (a4.size() > 0) {
                String str2 = ((a) a4.get(0)).b;
                if (URLUtil.isValidUrl(str2)) {
                    bgVar.z = "REF_IFRAME";
                    return str2;
                }
                a("MalformedURL", "Rich", "REF_IFRAME", (String) null, (String) null);
            } else {
                List a5 = buVar.a(2);
                if (a5.size() > 0) {
                    bgVar.z = "REF_HTML";
                    return ((a) a5.get(0)).b;
                }
            }
        }
        return null;
    }

    private static ak a(@NonNull ao aoVar, @NonNull ak akVar) {
        while (true) {
            String str = (String) akVar.e;
            if (str == null || str.length() == 0) {
                return null;
            }
            String[] split = str.split("\\|");
            ak b2 = aoVar.b(split[0]);
            if (b2 == null) {
                aoVar = aoVar.f;
            } else if (b2.equals(akVar)) {
                return null;
            } else {
                if (1 == split.length) {
                    b2.m = 1;
                    return b2;
                }
                b2.m = a(split[1]);
                StringBuilder sb = new StringBuilder("Referenced asset (");
                sb.append(b2.d);
                sb.append(")");
                return b2;
            }
        }
        return null;
    }

    private static boolean a(al alVar, al alVar2) {
        if (alVar.f2152a.x + alVar.c.x >= alVar2.f2152a.x + alVar2.c.x && alVar.f2152a.y + alVar.c.y >= alVar2.f2152a.y + alVar2.c.y) {
            return true;
        }
        return false;
    }
}
