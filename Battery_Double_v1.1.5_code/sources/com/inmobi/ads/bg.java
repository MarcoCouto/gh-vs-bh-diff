package com.inmobi.ads;

import android.support.annotation.NonNull;
import com.tapjoy.TJAdUnitConstants.String;

/* compiled from: NativeWebViewAsset */
public final class bg extends ak {
    boolean A = false;
    boolean B = false;
    String z;

    bg(String str, String str2, al alVar, String str3, boolean z2) {
        super(str, str2, "WEBVIEW", alVar);
        this.e = str3;
        this.B = z2;
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x0049  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x004c  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x004f  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0052  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0055  */
    static String c(@NonNull String str) {
        char c;
        String trim = str.trim();
        int hashCode = trim.hashCode();
        if (hashCode == -1900324833) {
            if (trim.equals("reference_html")) {
                c = 4;
                switch (c) {
                    case 1:
                        break;
                    case 2:
                        break;
                    case 3:
                        break;
                    case 4:
                        break;
                }
            }
        } else if (hashCode == -835221992) {
            if (trim.equals("reference_iframe")) {
                c = 3;
                switch (c) {
                    case 1:
                        break;
                    case 2:
                        break;
                    case 3:
                        break;
                    case 4:
                        break;
                }
            }
        } else if (hashCode == 116079) {
            if (trim.equals("url")) {
                c = 1;
                switch (c) {
                    case 1:
                        break;
                    case 2:
                        break;
                    case 3:
                        break;
                    case 4:
                        break;
                }
            }
        } else if (hashCode == 3213227 && trim.equals(String.HTML)) {
            c = 2;
            switch (c) {
                case 1:
                    return "URL";
                case 2:
                    return "HTML";
                case 3:
                    return "REF_IFRAME";
                case 4:
                    return "REF_HTML";
                default:
                    return "UNKNOWN";
            }
        }
        c = 65535;
        switch (c) {
            case 1:
                break;
            case 2:
                break;
            case 3:
                break;
            case 4:
                break;
        }
    }
}
