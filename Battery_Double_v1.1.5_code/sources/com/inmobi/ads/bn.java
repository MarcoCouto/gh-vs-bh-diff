package com.inmobi.ads;

import android.support.annotation.NonNull;

/* compiled from: RawAsset */
public final class bn {

    /* renamed from: a reason: collision with root package name */
    public final int f2202a;
    @NonNull
    public final String b;

    public bn(int i, @NonNull String str) {
        this.f2202a = i;
        this.b = str;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof bn)) {
            return false;
        }
        bn bnVar = (bn) obj;
        return this.f2202a == bnVar.f2202a && this.b.equals(bnVar.b);
    }

    public final int hashCode() {
        return (this.f2202a * 31) + this.b.hashCode();
    }
}
