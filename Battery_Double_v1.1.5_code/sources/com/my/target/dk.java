package com.my.target;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/* compiled from: HttpImageRequest */
public final class dk extends dm<Bitmap> {
    private final boolean ea;

    @NonNull
    public static dk cp() {
        return new dk(false);
    }

    @NonNull
    public static dk cq() {
        return new dk(true);
    }

    private dk(boolean z) {
        this.ea = z;
    }

    /* access modifiers changed from: protected */
    @Nullable
    /* renamed from: d */
    public Bitmap c(@NonNull String str, @NonNull Context context) {
        he N = he.N(context);
        if (N == null) {
            StringBuilder sb = new StringBuilder();
            sb.append("unable to open disk cache and get image ");
            sb.append(str);
            ah.a(sb.toString());
            if (this.ea) {
                this.ec = false;
                this.c = "image request (caching only) error: can't cache image";
                ah.a(this.c);
                return null;
            }
        } else if (!this.ea) {
            this.ed = N.getBitmap(str);
            if (this.ed != null) {
                this.cP = true;
                return (Bitmap) this.ed;
            }
        } else if (N.ag(str) != null) {
            ah.a("image request (caching only): image already cached");
            this.cP = true;
            return null;
        }
        a(N, str);
        return (Bitmap) this.ed;
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x0092  */
    /* JADX WARNING: Removed duplicated region for block: B:19:? A[RETURN, SYNTHETIC] */
    private void a(@Nullable he heVar, @NonNull String str) {
        HttpURLConnection httpURLConnection;
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("send image request: ");
            sb.append(str);
            ah.a(sb.toString());
            httpURLConnection = (HttpURLConnection) new URL(str).openConnection();
            try {
                httpURLConnection.setReadTimeout(10000);
                httpURLConnection.setConnectTimeout(10000);
                httpURLConnection.setInstanceFollowRedirects(true);
                httpURLConnection.setRequestProperty("connection", "close");
                httpURLConnection.connect();
                this.responseCode = httpURLConnection.getResponseCode();
                if (this.responseCode == 200) {
                    InputStream inputStream = httpURLConnection.getInputStream();
                    if (heVar != null) {
                        a(heVar, inputStream, str);
                    } else {
                        a(inputStream);
                    }
                } else {
                    this.ec = false;
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("image request error: response code ");
                    sb2.append(this.responseCode);
                    this.c = sb2.toString();
                    ah.a(this.c);
                }
            } catch (Throwable th) {
                th = th;
                this.ec = false;
                this.c = th.getMessage();
                StringBuilder sb3 = new StringBuilder();
                sb3.append("image request error: ");
                sb3.append(this.c);
                ah.a(sb3.toString());
                if (httpURLConnection != null) {
                }
            }
        } catch (Throwable th2) {
            th = th2;
            httpURLConnection = null;
            this.ec = false;
            this.c = th.getMessage();
            StringBuilder sb32 = new StringBuilder();
            sb32.append("image request error: ");
            sb32.append(this.c);
            ah.a(sb32.toString());
            if (httpURLConnection != null) {
            }
        }
        if (httpURLConnection != null) {
            httpURLConnection.disconnect();
        }
    }

    private void a(@NonNull he heVar, @NonNull InputStream inputStream, @NonNull String str) {
        File a2 = heVar.a(inputStream, str);
        if (a2 == null) {
            this.ec = false;
            this.c = "image request error: can't save image to disk cache";
            ah.a(this.c);
        } else if (!this.ea) {
            this.ed = BitmapFactory.decodeFile(a2.getAbsolutePath());
        }
    }

    private void a(@NonNull InputStream inputStream) {
        BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream, 8192);
        this.ed = BitmapFactory.decodeStream(bufferedInputStream);
        try {
            bufferedInputStream.close();
        } catch (IOException e) {
            ah.a(e.getMessage());
        }
    }
}
