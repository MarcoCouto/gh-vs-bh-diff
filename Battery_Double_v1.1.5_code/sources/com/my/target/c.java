package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.my.target.cu;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: AdResponseParser */
public abstract class c<T extends cu> {
    @Nullable
    public abstract T a(@NonNull String str, @NonNull bz bzVar, @Nullable T t, @NonNull a aVar, @NonNull Context context);

    static boolean isVast(@NonNull String str) {
        String trim = str.trim();
        return trim.startsWith("<VAST") || trim.startsWith("<?xml");
    }

    /* access modifiers changed from: protected */
    @Nullable
    public JSONObject a(@Nullable String str, @NonNull Context context) {
        if (str != null) {
            str = str.trim();
        }
        if (str == null || "".equals(str)) {
            ah.a("parsing ad response: empty data");
        } else {
            ah.a("Converting to JSON...");
            try {
                JSONObject jSONObject = new JSONObject(str);
                ah.a("done");
                if (a(jSONObject)) {
                    return jSONObject;
                }
                ah.a("invalid json version");
            } catch (Exception unused) {
            }
        }
        return null;
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    private boolean a(@NonNull JSONObject jSONObject) {
        String string = jSONObject.getString("version");
        StringBuilder sb = new StringBuilder();
        sb.append("json version: ");
        sb.append(string);
        ah.a(sb.toString());
        int indexOf = string.indexOf(".");
        if (indexOf > 0) {
            try {
                if (Integer.parseInt(string.substring(0, indexOf), 10) == 2) {
                    return true;
                }
            } catch (JSONException unused) {
            }
        }
        return false;
    }
}
