package com.my.target;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.my.target.common.models.VideoData;
import java.util.ArrayList;
import java.util.List;

/* compiled from: NativeAdBanner */
public class co extends cg {
    @NonNull
    private final List<cp> nativeAdCards = new ArrayList();
    @Nullable
    private cn<VideoData> videoBanner;
    private float viewabilityRate = 1.0f;
    private float viewabilitySquare = 0.5f;

    @NonNull
    public static co newBanner() {
        return new co();
    }

    public void setViewabilitySquare(float f) {
        this.viewabilitySquare = f;
    }

    public float getViewabilityRate() {
        return this.viewabilityRate;
    }

    public void setViewabilityRate(float f) {
        this.viewabilityRate = f;
    }

    private co() {
    }

    public void setVideoBanner(@Nullable cn<VideoData> cnVar) {
        this.videoBanner = cnVar;
    }

    @Nullable
    public cn<VideoData> getVideoBanner() {
        return this.videoBanner;
    }

    public void addNativeAdCard(@NonNull cp cpVar) {
        this.nativeAdCards.add(cpVar);
    }

    @NonNull
    public List<cp> getNativeAdCards() {
        return new ArrayList(this.nativeAdCards);
    }

    public float getViewabilitySquare() {
        return this.viewabilitySquare;
    }
}
