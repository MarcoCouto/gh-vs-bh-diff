package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.my.target.b.C0072b;

/* compiled from: NativeAppwallAdFactory */
public final class z extends b<db> {

    /* compiled from: NativeAppwallAdFactory */
    static class a implements com.my.target.b.a<db> {
        public boolean a() {
            return false;
        }

        private a() {
        }

        @NonNull
        public c<db> b() {
            return aa.f();
        }

        @Nullable
        public d<db> c() {
            return ab.m();
        }

        @NonNull
        public e d() {
            return e.e();
        }
    }

    public interface b extends C0072b {
    }

    @NonNull
    public static b<db> a(@NonNull a aVar) {
        return new z(aVar);
    }

    private z(@NonNull a aVar) {
        super(new a(), aVar);
    }

    /* access modifiers changed from: protected */
    @Nullable
    public String a(@NonNull bz bzVar, @NonNull dj djVar, @NonNull Context context) {
        if (this.adConfig.getCachePeriod() > 0) {
            ah.a("NativeAppwallAdFactory: check cached data");
            String str = null;
            he N = he.N(context);
            if (N != null) {
                str = N.a(this.adConfig.getSlotId(), this.adConfig.getCachePeriod());
            }
            if (str != null) {
                ah.a("NativeAppwallAdFactory: cached data loaded successfully");
                bzVar.n(true);
                return str;
            }
            ah.a("NativeAppwallAdFactory: no cached data");
        }
        return super.a(bzVar, djVar, context);
    }
}
