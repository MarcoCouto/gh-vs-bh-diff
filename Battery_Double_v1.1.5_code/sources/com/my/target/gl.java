package com.my.target;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;

/* compiled from: FramedImageView */
public class gl extends FrameLayout {
    @NonNull
    private final ImageView hM;

    public gl(@NonNull Context context) {
        super(context);
        this.hM = new ImageView(context);
        LayoutParams layoutParams = new LayoutParams(-2, -2);
        layoutParams.gravity = 17;
        addView(this.hM, layoutParams);
    }

    public void setImageBitmap(@NonNull Bitmap bitmap) {
        this.hM.setImageBitmap(bitmap);
    }
}
