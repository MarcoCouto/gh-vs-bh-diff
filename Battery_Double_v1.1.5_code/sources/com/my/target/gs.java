package com.my.target;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewCompat;
import android.text.TextUtils.TruncateAt;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import java.util.HashMap;

/* compiled from: CardItemView */
public class gs extends ViewGroup implements OnTouchListener {
    private static final int CTA_ID = hm.dV();
    private static final int MEDIA_ID = hm.dV();
    private static final int RATING_ID = hm.dV();
    private static final int ij = hm.dV();
    private static final int iv = hm.dV();
    private static final int jX = hm.dV();
    @NonNull
    private final Button ctaButton;
    @NonNull
    private final TextView descriptionView;
    @NonNull
    private final TextView iD;
    private final boolean jB;
    @NonNull
    private final HashMap<View, Boolean> jC = new HashMap<>();
    @Nullable
    private OnClickListener jD;
    @NonNull
    private final fx js;
    private int kK;
    private int kL;
    private int padding;
    @NonNull
    private final fy starsView;
    @NonNull
    private final TextView titleView;
    @NonNull
    private final hm uiUtils;

    public gs(boolean z, @NonNull Context context) {
        super(context);
        this.jB = z;
        this.uiUtils = hm.R(context);
        this.js = new fx(context);
        this.titleView = new TextView(context);
        this.descriptionView = new TextView(context);
        this.ctaButton = new Button(context);
        this.starsView = new fy(context);
        this.iD = new TextView(context);
        initView();
    }

    @NonNull
    public Button getCtaButtonView() {
        return this.ctaButton;
    }

    @NonNull
    public TextView getDescriptionTextView() {
        return this.descriptionView;
    }

    @NonNull
    public TextView getDomainTextView() {
        return this.iD;
    }

    @NonNull
    public fx getSmartImageView() {
        return this.js;
    }

    @NonNull
    public fy getRatingView() {
        return this.starsView;
    }

    @NonNull
    public TextView getTitleTextView() {
        return this.titleView;
    }

    @SuppressLint({"ClickableViewAccessibility"})
    public void a(@Nullable OnClickListener onClickListener, @Nullable ca caVar) {
        this.jD = onClickListener;
        if (onClickListener == null || caVar == null) {
            super.setOnClickListener(null);
            this.ctaButton.setOnClickListener(null);
            return;
        }
        setOnTouchListener(this);
        this.js.setOnTouchListener(this);
        this.titleView.setOnTouchListener(this);
        this.descriptionView.setOnTouchListener(this);
        this.starsView.setOnTouchListener(this);
        this.iD.setOnTouchListener(this);
        this.ctaButton.setOnTouchListener(this);
        boolean z = false;
        this.jC.put(this.js, Boolean.valueOf(caVar.de || caVar.dn));
        this.jC.put(this, Boolean.valueOf(caVar.dm || caVar.dn));
        this.jC.put(this.titleView, Boolean.valueOf(caVar.db || caVar.dn));
        this.jC.put(this.descriptionView, Boolean.valueOf(caVar.dc || caVar.dn));
        this.jC.put(this.starsView, Boolean.valueOf(caVar.df || caVar.dn));
        this.jC.put(this.iD, Boolean.valueOf(caVar.dk || caVar.dn));
        HashMap<View, Boolean> hashMap = this.jC;
        Button button = this.ctaButton;
        if (caVar.dh || caVar.dn) {
            z = true;
        }
        hashMap.put(button, Boolean.valueOf(z));
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (!this.jC.containsKey(view)) {
            return false;
        }
        boolean booleanValue = ((Boolean) this.jC.get(view)).booleanValue();
        view.setClickable(booleanValue);
        int action = motionEvent.getAction();
        if (action != 3) {
            switch (action) {
                case 0:
                    if (booleanValue) {
                        if (view != this.ctaButton) {
                            setBackgroundColor(-3806472);
                            break;
                        } else {
                            this.ctaButton.setPressed(true);
                            break;
                        }
                    }
                    break;
                case 1:
                    if (this.jD != null) {
                        this.jD.onClick(view);
                    }
                    if (booleanValue) {
                        if (view != this.ctaButton) {
                            hm.a(this, 0, 0, -3355444, this.uiUtils.E(1), 0);
                            break;
                        } else {
                            this.ctaButton.setPressed(false);
                            break;
                        }
                    }
                    break;
            }
        } else if (booleanValue) {
            if (view == this.ctaButton) {
                this.ctaButton.setPressed(false);
            } else {
                hm.a(this, 0, 0, -3355444, this.uiUtils.E(1), 0);
            }
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int i3;
        int size = MeasureSpec.getSize(i);
        int size2 = MeasureSpec.getSize(i2);
        int i4 = 0;
        boolean z = !this.jB && getResources().getConfiguration().orientation == 2;
        if (size != 0) {
            i4 = Integer.MIN_VALUE;
        }
        a(size, size2, z, i4);
        if (z) {
            i3 = (size2 - this.titleView.getMeasuredHeight()) - this.padding;
        } else {
            i3 = ((((size2 - this.ctaButton.getMeasuredHeight()) - (this.kK * 2)) - Math.max(this.starsView.getMeasuredHeight(), this.iD.getMeasuredHeight())) - this.descriptionView.getMeasuredHeight()) - this.titleView.getMeasuredHeight();
        }
        if (i3 <= size) {
            size = i3;
        }
        this.js.measure(MeasureSpec.makeMeasureSpec(size, 1073741824), MeasureSpec.makeMeasureSpec(size, 1073741824));
        setMeasuredDimension(size, size2);
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        int i5 = (i3 - i) - (this.padding * 2);
        boolean z2 = !this.jB && getResources().getConfiguration().orientation == 2;
        this.js.layout(0, 0, this.js.getMeasuredWidth(), this.js.getMeasuredHeight());
        if (z2) {
            this.titleView.setTypeface(null, 1);
            this.titleView.layout(0, this.js.getBottom(), i5, this.js.getBottom() + this.titleView.getMeasuredHeight());
            hm.a(this, 0, 0);
            this.descriptionView.layout(0, 0, 0, 0);
            this.ctaButton.layout(0, 0, 0, 0);
            this.starsView.layout(0, 0, 0, 0);
            this.iD.layout(0, 0, 0, 0);
            return;
        }
        this.titleView.setTypeface(null, 0);
        hm.a(this, 0, 0, -3355444, this.uiUtils.E(1), 0);
        this.titleView.layout(this.padding + this.kL, this.js.getBottom(), this.titleView.getMeasuredWidth() + this.padding + this.kL, this.js.getBottom() + this.titleView.getMeasuredHeight());
        this.descriptionView.layout(this.padding + this.kL, this.titleView.getBottom(), this.descriptionView.getMeasuredWidth() + this.padding + this.kL, this.titleView.getBottom() + this.descriptionView.getMeasuredHeight());
        int measuredWidth = (i5 - this.ctaButton.getMeasuredWidth()) / 2;
        this.ctaButton.layout(measuredWidth, (i4 - this.ctaButton.getMeasuredHeight()) - this.kL, this.ctaButton.getMeasuredWidth() + measuredWidth, i4 - this.kL);
        int measuredWidth2 = (i5 - this.starsView.getMeasuredWidth()) / 2;
        this.starsView.layout(measuredWidth2, (this.ctaButton.getTop() - this.kL) - this.starsView.getMeasuredHeight(), this.starsView.getMeasuredWidth() + measuredWidth2, this.ctaButton.getTop() - this.kL);
        int measuredWidth3 = (i5 - this.iD.getMeasuredWidth()) / 2;
        this.iD.layout(measuredWidth3, (this.ctaButton.getTop() - this.iD.getMeasuredHeight()) - this.kL, this.iD.getMeasuredWidth() + measuredWidth3, this.ctaButton.getTop() - this.kL);
    }

    private void initView() {
        hm.a(this, 0, 0, -3355444, this.uiUtils.E(1), 0);
        this.padding = this.uiUtils.E(2);
        this.kL = this.uiUtils.E(12);
        this.js.setId(MEDIA_ID);
        this.ctaButton.setId(CTA_ID);
        this.ctaButton.setPadding(this.uiUtils.E(15), this.uiUtils.E(10), this.uiUtils.E(15), this.uiUtils.E(10));
        this.ctaButton.setMinimumWidth(this.uiUtils.E(100));
        this.ctaButton.setTransformationMethod(null);
        this.ctaButton.setSingleLine();
        if (this.jB) {
            this.ctaButton.setTextSize(20.0f);
        } else {
            this.ctaButton.setTextSize(18.0f);
        }
        this.ctaButton.setEllipsize(TruncateAt.END);
        if (VERSION.SDK_INT >= 21) {
            this.ctaButton.setElevation((float) this.uiUtils.E(2));
        }
        this.kK = this.uiUtils.E(12);
        hm.a(this.ctaButton, -16733198, -16746839, this.uiUtils.E(2));
        this.ctaButton.setTextColor(-1);
        this.titleView.setId(ij);
        if (this.jB) {
            this.titleView.setTextSize(20.0f);
        } else {
            this.titleView.setTextSize(18.0f);
        }
        this.titleView.setTextColor(ViewCompat.MEASURED_STATE_MASK);
        this.titleView.setTypeface(null, 1);
        this.titleView.setLines(1);
        this.titleView.setEllipsize(TruncateAt.END);
        this.descriptionView.setId(jX);
        this.descriptionView.setTextColor(-7829368);
        this.descriptionView.setLines(2);
        if (this.jB) {
            this.descriptionView.setTextSize(20.0f);
        } else {
            this.descriptionView.setTextSize(18.0f);
        }
        this.descriptionView.setEllipsize(TruncateAt.END);
        this.starsView.setId(RATING_ID);
        if (this.jB) {
            this.starsView.setStarSize(this.uiUtils.E(24));
        } else {
            this.starsView.setStarSize(this.uiUtils.E(18));
        }
        this.starsView.setStarsPadding((float) this.uiUtils.E(4));
        this.iD.setId(iv);
        hm.a((View) this, "card_view");
        hm.a((View) this.titleView, "card_title_text");
        hm.a((View) this.descriptionView, "card_description_text");
        hm.a((View) this.iD, "card_domain_text");
        hm.a((View) this.ctaButton, "card_cta_button");
        hm.a((View) this.starsView, "card_stars_view");
        hm.a((View) this.js, "card_image");
        addView(this.js);
        addView(this.descriptionView);
        addView(this.titleView);
        addView(this.ctaButton);
        addView(this.starsView);
        addView(this.iD);
    }

    private void a(int i, int i2, boolean z, int i3) {
        int i4 = i2 - (this.padding * 2);
        int i5 = i - (this.padding * 2);
        if (z) {
            this.titleView.measure(MeasureSpec.makeMeasureSpec(i, i3), MeasureSpec.makeMeasureSpec(i4, Integer.MIN_VALUE));
            this.descriptionView.measure(0, 0);
            this.starsView.measure(0, 0);
            this.iD.measure(0, 0);
            this.ctaButton.measure(0, 0);
            return;
        }
        this.titleView.measure(MeasureSpec.makeMeasureSpec(i5 - (this.kL * 2), i3), MeasureSpec.makeMeasureSpec(i4, Integer.MIN_VALUE));
        this.descriptionView.measure(MeasureSpec.makeMeasureSpec(i5 - (this.kL * 2), i3), MeasureSpec.makeMeasureSpec(i4, Integer.MIN_VALUE));
        this.starsView.measure(MeasureSpec.makeMeasureSpec(i5, i3), MeasureSpec.makeMeasureSpec(i4, Integer.MIN_VALUE));
        this.iD.measure(MeasureSpec.makeMeasureSpec(i5, i3), MeasureSpec.makeMeasureSpec(i4, Integer.MIN_VALUE));
        this.ctaButton.measure(MeasureSpec.makeMeasureSpec(i5 - (this.kL * 2), i3), MeasureSpec.makeMeasureSpec(i4 - (this.kL * 2), Integer.MIN_VALUE));
    }
}
