package com.my.target;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.Display;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import com.my.target.common.models.ImageData;
import com.my.target.nativeads.views.MediaAdView;

/* compiled from: InterstitialImageView */
public class fv extends RelativeLayout {
    private static final int hS = hm.dV();
    @NonNull
    private final fp ageRestrictionLabel;
    @NonNull
    private final fu eH;
    @NonNull
    private final LayoutParams hT = new LayoutParams(-2, -2);
    @Nullable
    private ImageData hU;
    @Nullable
    private ImageData hV;
    @NonNull
    private final fx imageView;
    @NonNull
    private final hm uiUtils;

    public fv(Context context) {
        super(context);
        setBackgroundColor(0);
        this.uiUtils = hm.R(context);
        this.imageView = new fx(context);
        this.imageView.setId(hS);
        LayoutParams layoutParams = new LayoutParams(-2, -2);
        layoutParams.addRule(13);
        this.imageView.setLayoutParams(layoutParams);
        addView(this.imageView);
        this.eH = new fu(context);
        this.eH.a(fg.u((int) TypedValue.applyDimension(1, 28.0f, context.getResources().getDisplayMetrics())), false);
        this.hT.addRule(7, hS);
        this.hT.addRule(6, hS);
        this.eH.setLayoutParams(this.hT);
        this.ageRestrictionLabel = new fp(context);
        addView(this.eH);
        addView(this.ageRestrictionLabel);
    }

    @NonNull
    public fu getCloseButton() {
        return this.eH;
    }

    @NonNull
    public ImageView getImageView() {
        return this.imageView;
    }

    public void a(@Nullable ImageData imageData, @Nullable ImageData imageData2, @Nullable ImageData imageData3) {
        this.hV = imageData;
        this.hU = imageData2;
        Bitmap bitmap = imageData3 != null ? imageData3.getBitmap() : null;
        if (bitmap != null) {
            this.eH.a(bitmap, true);
            LayoutParams layoutParams = this.hT;
            int i = -this.eH.getMeasuredWidth();
            this.hT.leftMargin = i;
            layoutParams.bottomMargin = i;
        }
        dr();
    }

    public void setAgeRestrictions(@NonNull String str) {
        if (!TextUtils.isEmpty(str)) {
            this.ageRestrictionLabel.f(1, -7829368);
            this.ageRestrictionLabel.setPadding(this.uiUtils.E(2), 0, 0, 0);
            LayoutParams layoutParams = new LayoutParams(-2, -2);
            int E = this.uiUtils.E(10);
            layoutParams.topMargin = E;
            layoutParams.leftMargin = E;
            layoutParams.addRule(5, hS);
            layoutParams.addRule(6, hS);
            this.ageRestrictionLabel.setLayoutParams(layoutParams);
            this.ageRestrictionLabel.setTextColor(MediaAdView.COLOR_PLACEHOLDER_GRAY);
            this.ageRestrictionLabel.a(1, MediaAdView.COLOR_PLACEHOLDER_GRAY, this.uiUtils.E(3));
            this.ageRestrictionLabel.setBackgroundColor(1711276032);
            this.ageRestrictionLabel.setText(str);
            return;
        }
        this.ageRestrictionLabel.setVisibility(8);
    }

    /* access modifiers changed from: protected */
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        dr();
    }

    private void dr() {
        WindowManager windowManager = (WindowManager) getContext().getApplicationContext().getSystemService("window");
        if (windowManager != null) {
            Display defaultDisplay = windowManager.getDefaultDisplay();
            ImageData imageData = ((float) defaultDisplay.getWidth()) / ((float) defaultDisplay.getHeight()) > 1.0f ? this.hV : this.hU;
            if (imageData == null) {
                imageData = this.hV != null ? this.hV : this.hU;
            }
            if (imageData != null) {
                this.imageView.setImageData(imageData);
            }
        }
    }
}
