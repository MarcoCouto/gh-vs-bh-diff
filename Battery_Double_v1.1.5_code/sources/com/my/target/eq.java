package com.my.target;

import android.content.Context;
import android.media.AudioManager;
import android.os.Build.VERSION;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.android.exoplayer2.util.MimeTypes;
import com.my.target.et.b;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/* compiled from: InterstitialMediaPresenter */
public class eq {
    @NonNull
    private final ho N;
    /* access modifiers changed from: private */
    public boolean ae;
    /* access modifiers changed from: private */
    public boolean allowClose;
    /* access modifiers changed from: private */
    public float duration;
    /* access modifiers changed from: private */
    @Nullable
    public b eE;
    @NonNull
    private final a eS;
    /* access modifiers changed from: private */
    @NonNull
    public final gm eT;
    /* access modifiers changed from: private */
    public boolean eU = true;
    @Nullable
    private Set<df> eV;
    /* access modifiers changed from: private */
    public boolean eW;
    /* access modifiers changed from: private */
    public boolean eX;
    /* access modifiers changed from: private */
    public boolean eY;
    /* access modifiers changed from: private */
    @NonNull
    public final cn videoBanner;

    /* compiled from: InterstitialMediaPresenter */
    public class a implements com.my.target.gp.a {
        public void A() {
        }

        public void B() {
        }

        public void x() {
        }

        public a() {
        }

        public void cN() {
            if (!eq.this.ae) {
                eq.this.j(eq.this.eT.getView().getContext());
            }
            eq.this.eT.play();
        }

        public void cO() {
            if (!eq.this.ae) {
                eq.this.L();
                hl.a((List<dg>) eq.this.videoBanner.getStatHolder().N("volumeOff"), eq.this.eT.getView().getContext());
                eq.this.ae = true;
                return;
            }
            eq.this.cJ();
            hl.a((List<dg>) eq.this.videoBanner.getStatHolder().N("volumeOn"), eq.this.eT.getView().getContext());
            eq.this.ae = false;
        }

        public void cP() {
            eq.this.i(eq.this.eT.getView().getContext());
            hl.a((List<dg>) eq.this.videoBanner.getStatHolder().N("playbackPaused"), eq.this.eT.getView().getContext());
            eq.this.eT.pause();
        }

        public void cQ() {
            hl.a((List<dg>) eq.this.videoBanner.getStatHolder().N("playbackResumed"), eq.this.eT.getView().getContext());
            eq.this.eT.resume();
            if (eq.this.ae) {
                eq.this.L();
            } else {
                eq.this.cJ();
            }
        }

        public void e(float f) {
            eq.this.eT.z(f <= 0.0f);
        }

        public void y() {
            if (eq.this.allowClose && eq.this.videoBanner.getAllowCloseDelay() == 0.0f) {
                eq.this.eT.dF();
            }
            eq.this.eT.dG();
        }

        public void z() {
            if (eq.this.eW) {
                eq.this.eT.pause();
            }
        }

        public void a(float f, float f2) {
            eq.this.eT.setTimeChanged(f);
            eq.this.eY = false;
            if (eq.this.eU) {
                eq.this.cM();
                hl.a((List<dg>) eq.this.videoBanner.getStatHolder().N("playbackStarted"), eq.this.eT.getView().getContext());
                eq.this.j(0.0f);
                eq.this.eU = false;
            }
            if (!eq.this.eX) {
                eq.this.eX = true;
            }
            if (eq.this.allowClose && eq.this.videoBanner.isAutoPlay() && eq.this.videoBanner.getAllowCloseDelay() <= f) {
                eq.this.eT.dF();
            }
            if (f <= eq.this.duration) {
                if (f != 0.0f) {
                    eq.this.j(f);
                }
                if (f == eq.this.duration) {
                    onComplete();
                    return;
                }
                return;
            }
            a(eq.this.duration, eq.this.duration);
        }

        public void d(String str) {
            StringBuilder sb = new StringBuilder();
            sb.append("Video playing error: ");
            sb.append(str);
            ah.a(sb.toString());
            eq.this.cK();
            if (eq.this.eE != null) {
                eq.this.eE.aj();
            }
        }

        public void onComplete() {
            if (!eq.this.eY) {
                eq.this.eY = true;
                ah.a("Video playing complete:");
                eq.this.cK();
                if (eq.this.eE != null) {
                    eq.this.eE.ai();
                }
                eq.this.eT.dF();
                eq.this.eT.finish();
            }
        }

        public void onAudioFocusChange(final int i) {
            boolean z = VERSION.SDK_INT >= 23 ? Looper.getMainLooper().isCurrentThread() : Thread.currentThread() == Looper.getMainLooper().getThread();
            if (z) {
                eq.this.s(i);
            } else {
                ai.c(new Runnable() {
                    public void run() {
                        eq.this.s(i);
                    }
                });
            }
        }
    }

    @NonNull
    public static eq a(@NonNull cn cnVar, @NonNull gm gmVar) {
        return new eq(cnVar, gmVar);
    }

    private eq(@NonNull cn cnVar, @NonNull gm gmVar) {
        this.videoBanner = cnVar;
        this.eS = new a();
        this.eT = gmVar;
        gmVar.setMediaListener(this.eS);
        this.N = ho.c(cnVar.getStatHolder());
        this.N.setView(gmVar.getPromoMediaView());
    }

    public void a(@NonNull cn cnVar, @NonNull Context context) {
        this.allowClose = cnVar.isAllowClose();
        if (this.allowClose && cnVar.getAllowCloseDelay() == 0.0f && cnVar.isAutoPlay()) {
            ah.a("banner is allowed to close");
            this.eT.dF();
        }
        this.duration = cnVar.getDuration();
        this.ae = cnVar.isAutoMute();
        if (this.ae) {
            this.eT.z(0);
            return;
        }
        if (cnVar.isAutoPlay()) {
            j(context);
        }
        this.eT.z(2);
    }

    public void a(cm cmVar) {
        this.eT.dF();
        this.eT.a(cmVar);
    }

    /* access modifiers changed from: private */
    public void L() {
        i(this.eT.getView().getContext());
        this.eT.z(0);
    }

    /* access modifiers changed from: private */
    public void cJ() {
        if (this.eT.isPlaying()) {
            j(this.eT.getView().getContext());
        }
        this.eT.z(2);
    }

    private void K() {
        this.eT.z(1);
    }

    /* access modifiers changed from: private */
    public void j(@NonNull Context context) {
        AudioManager audioManager = (AudioManager) context.getApplicationContext().getSystemService(MimeTypes.BASE_TYPE_AUDIO);
        if (audioManager != null) {
            audioManager.requestAudioFocus(this.eS, 3, 2);
        }
    }

    /* access modifiers changed from: private */
    public void i(@NonNull Context context) {
        AudioManager audioManager = (AudioManager) context.getApplicationContext().getSystemService(MimeTypes.BASE_TYPE_AUDIO);
        if (audioManager != null) {
            audioManager.abandonAudioFocus(this.eS);
        }
    }

    public void stop() {
        i(this.eT.getView().getContext());
    }

    public void destroy() {
        i(this.eT.getView().getContext());
        this.eT.destroy();
    }

    /* access modifiers changed from: private */
    public void cK() {
        this.eU = true;
        this.eT.dF();
        i(this.eT.getView().getContext());
        this.eT.stop(this.videoBanner.isAllowReplay());
    }

    public void pause() {
        this.eT.pause();
        i(this.eT.getView().getContext());
        if (this.eT.isPlaying() && !this.eT.isPaused()) {
            hl.a((List<dg>) this.videoBanner.getStatHolder().N("playbackPaused"), this.eT.getView().getContext());
        }
    }

    public void cL() {
        this.eT.stop(true);
        i(this.eT.getView().getContext());
        if (this.eX) {
            hl.a((List<dg>) this.videoBanner.getStatHolder().N("closedByUser"), this.eT.getView().getContext());
        }
    }

    public void r(boolean z) {
        this.eW = z;
    }

    public void a(@Nullable b bVar) {
        this.eE = bVar;
    }

    /* access modifiers changed from: private */
    public void cM() {
        if (this.eV != null) {
            this.eV.clear();
        }
        this.eV = this.videoBanner.getStatHolder().ck();
    }

    /* access modifiers changed from: private */
    public void j(float f) {
        this.N.k(f);
        if (this.eV != null && !this.eV.isEmpty()) {
            Iterator it = this.eV.iterator();
            while (it.hasNext()) {
                df dfVar = (df) it.next();
                if (dfVar.cf() <= f) {
                    hl.a((dg) dfVar, this.eT.getView().getContext());
                    it.remove();
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void s(int i) {
        switch (i) {
            case -3:
                ah.a("Audiofocus loss can duck, set volume to 0.3");
                if (!this.ae) {
                    K();
                    return;
                }
                return;
            case -2:
            case -1:
                pause();
                ah.a("Audiofocus loss, pausing");
                return;
            case 1:
            case 2:
            case 4:
                ah.a("Audiofocus gain, unmuting");
                if (!this.ae) {
                    cJ();
                    return;
                }
                return;
            default:
                return;
        }
    }
}
