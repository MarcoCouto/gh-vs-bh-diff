package com.my.target;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.webkit.ConsoleMessage;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: BannerWebView */
public class fo extends WebView {
    /* access modifiers changed from: private */
    public boolean cb;
    /* access modifiers changed from: private */
    @Nullable
    public JSONObject dJ;
    @NonNull
    private final WebViewClient hm;
    @NonNull
    private final WebChromeClient hn;
    /* access modifiers changed from: private */
    @Nullable
    public a ho;
    /* access modifiers changed from: private */
    public boolean hp;

    /* compiled from: BannerWebView */
    public interface a {
        void X(@NonNull String str);

        void a(@NonNull bq bqVar);

        void onError(@NonNull String str);
    }

    /* compiled from: BannerWebView */
    class b extends WebChromeClient {
        private b() {
        }

        public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
            String message = consoleMessage.message();
            int lineNumber = consoleMessage.lineNumber();
            StringBuilder sb = new StringBuilder();
            sb.append("js console message: ");
            sb.append(message);
            sb.append(" at line: ");
            sb.append(lineNumber);
            ah.a(sb.toString());
            bq a2 = bf.a(consoleMessage);
            if (a2 == null) {
                return false;
            }
            if (fo.this.ho != null) {
                fo.this.ho.a(a2);
            }
            return true;
        }
    }

    /* compiled from: BannerWebView */
    class c extends WebViewClient {
        private c() {
        }

        public boolean shouldOverrideUrlLoading(WebView webView, String str) {
            if (fo.this.hp && str != null && !str.startsWith("adman://onEvent,")) {
                fo.this.ab(str);
                fo.this.dm();
            }
            return true;
        }

        @TargetApi(24)
        public boolean shouldOverrideUrlLoading(WebView webView, WebResourceRequest webResourceRequest) {
            if (fo.this.hp) {
                Uri url = webResourceRequest.getUrl();
                if (url != null) {
                    String uri = url.toString();
                    if (uri != null && !uri.startsWith("adman://onEvent,")) {
                        fo.this.ab(uri);
                        fo.this.dm();
                    }
                }
            }
            return true;
        }

        public void onPageStarted(WebView webView, String str, Bitmap bitmap) {
            ah.a("load page started");
            super.onPageStarted(webView, str, bitmap);
        }

        public void onPageFinished(WebView webView, String str) {
            if (!fo.this.cb) {
                fo.this.cb = true;
                ah.a("page loaded");
                super.onPageFinished(webView, str);
                if (fo.this.dJ != null) {
                    try {
                        fo.this.a((bi) new bj(fo.this.dJ));
                    } catch (JSONException e) {
                        StringBuilder sb = new StringBuilder();
                        sb.append("js call executing error ");
                        sb.append(e.getMessage());
                        ah.a(sb.toString());
                    }
                }
            }
        }

        public void onReceivedError(WebView webView, int i, String str, String str2) {
            StringBuilder sb = new StringBuilder();
            sb.append("load failed. error: ");
            sb.append(i);
            sb.append(" description: ");
            sb.append(str);
            sb.append(" url: ");
            sb.append(str2);
            ah.a(sb.toString());
            super.onReceivedError(webView, i, str, str2);
            if (fo.this.ho != null) {
                a a2 = fo.this.ho;
                if (str == null) {
                    str = "unknown JS error";
                }
                a2.onError(str);
            }
        }

        @TargetApi(23)
        public void onReceivedError(WebView webView, WebResourceRequest webResourceRequest, WebResourceError webResourceError) {
            super.onReceivedError(webView, webResourceRequest, webResourceError);
            CharSequence description = webResourceError.getDescription();
            String charSequence = description != null ? description.toString() : null;
            int errorCode = webResourceError.getErrorCode();
            String uri = webResourceRequest.getUrl().toString();
            StringBuilder sb = new StringBuilder();
            sb.append("load failed. error: ");
            sb.append(errorCode);
            sb.append(" description: ");
            sb.append(charSequence);
            sb.append(" url: ");
            sb.append(uri);
            ah.a(sb.toString());
            if (fo.this.ho != null) {
                a a2 = fo.this.ho;
                if (charSequence == null) {
                    charSequence = "Unknown JS error";
                }
                a2.onError(charSequence);
            }
        }

        public void onScaleChanged(WebView webView, float f, float f2) {
            super.onScaleChanged(webView, f, f2);
            StringBuilder sb = new StringBuilder();
            sb.append("scale new: ");
            sb.append(f2);
            sb.append(" old: ");
            sb.append(f);
            ah.a(sb.toString());
        }
    }

    /* compiled from: BannerWebView */
    static class d extends GestureDetector {
        @NonNull
        private final View hs;
        @Nullable
        private a ht;

        /* compiled from: BannerWebView */
        interface a {
            void dn();
        }

        d(@NonNull Context context, @NonNull View view) {
            this(context, view, new SimpleOnGestureListener());
        }

        private d(@NonNull Context context, @NonNull View view, @NonNull SimpleOnGestureListener simpleOnGestureListener) {
            super(context, simpleOnGestureListener);
            this.hs = view;
            setIsLongpressEnabled(false);
        }

        /* access modifiers changed from: 0000 */
        public void a(@NonNull MotionEvent motionEvent) {
            switch (motionEvent.getAction()) {
                case 0:
                    onTouchEvent(motionEvent);
                    return;
                case 1:
                    if (this.ht != null) {
                        ah.a("Gestures: user clicked");
                        this.ht.dn();
                        return;
                    }
                    ah.a("View's onUserClick() is not registered.");
                    return;
                case 2:
                    if (a(motionEvent, this.hs)) {
                        onTouchEvent(motionEvent);
                        return;
                    }
                    return;
                default:
                    return;
            }
        }

        /* access modifiers changed from: 0000 */
        public void a(@Nullable a aVar) {
            this.ht = aVar;
        }

        private boolean a(@Nullable MotionEvent motionEvent, @Nullable View view) {
            boolean z = false;
            if (motionEvent == null || view == null) {
                return false;
            }
            float x = motionEvent.getX();
            float y = motionEvent.getY();
            if (x >= 0.0f && x <= ((float) view.getWidth()) && y >= 0.0f && y <= ((float) view.getHeight())) {
                z = true;
            }
            return z;
        }
    }

    public fo(@NonNull Context context) {
        this(context, null);
    }

    public fo(@NonNull Context context, @Nullable AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    @SuppressLint({"SetJavaScriptEnabled"})
    public fo(@NonNull Context context, @Nullable AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.hn = new b();
        this.hm = new c();
        final d dVar = new d(getContext(), this);
        dVar.a((a) new a() {
            public void dn() {
                fo.this.hp = true;
            }
        });
        setOnTouchListener(new OnTouchListener() {
            @SuppressLint({"ClickableViewAccessibility"})
            public boolean onTouch(View view, MotionEvent motionEvent) {
                dVar.a(motionEvent);
                return false;
            }
        });
        setHorizontalScrollBarEnabled(false);
        setVerticalScrollBarEnabled(false);
        WebSettings settings = getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setDomStorageEnabled(true);
        settings.setAppCacheEnabled(true);
        settings.setSupportZoom(false);
        settings.setAppCachePath(getContext().getCacheDir().getAbsolutePath());
        settings.setAllowFileAccess(false);
        settings.setAllowContentAccess(false);
        if (VERSION.SDK_INT >= 16) {
            settings.setAllowFileAccessFromFileURLs(false);
            settings.setAllowUniversalAccessFromFileURLs(false);
        }
        setWebChromeClient(this.hn);
        setWebViewClient(this.hm);
    }

    public void setBannerWebViewListener(@Nullable a aVar) {
        this.ho = aVar;
    }

    public void e(@Nullable JSONObject jSONObject, @NonNull String str) {
        this.cb = false;
        this.hp = false;
        loadDataWithBaseURL("https://ad.mail.ru/", str, WebRequest.CONTENT_TYPE_HTML, "UTF-8", null);
        this.dJ = jSONObject;
    }

    public void a(@NonNull bi biVar) {
        StringBuilder sb = new StringBuilder();
        sb.append("javascript:AdmanJS.execute(");
        sb.append(biVar.aE().toString());
        sb.append(")");
        String sb2 = sb.toString();
        ah.a(sb2);
        loadUrl(sb2);
    }

    /* access modifiers changed from: private */
    public void dm() {
        this.hp = false;
    }

    /* access modifiers changed from: private */
    public void ab(@NonNull String str) {
        if (this.ho != null) {
            this.ho.X(str);
        }
    }
}
