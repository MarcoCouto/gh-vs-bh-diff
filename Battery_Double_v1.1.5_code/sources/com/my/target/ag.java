package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/* compiled from: StandardAdResultProcessor */
public class ag extends d<dc> {
    @NonNull
    public static ag n() {
        return new ag();
    }

    private ag() {
    }

    @Nullable
    public dc a(@NonNull dc dcVar, @NonNull a aVar, @NonNull Context context) {
        if (dcVar.cc() != null) {
            return dcVar;
        }
        cs bx = dcVar.bx();
        if (bx == null || !bx.bt()) {
            return null;
        }
        return dcVar;
    }
}
