package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.my.target.common.models.ImageData;
import java.util.ArrayList;

/* compiled from: InterstitialSliderAdResultProcessor */
public class u extends d<cy> {
    @NonNull
    public static u j() {
        return new u();
    }

    private u() {
    }

    @Nullable
    public cy a(@NonNull cy cyVar, @NonNull a aVar, @NonNull Context context) {
        ArrayList arrayList = new ArrayList();
        for (cl clVar : cyVar.bI()) {
            if (clVar.getPortraitImages().size() > 0) {
                ImageData imageData = (ImageData) clVar.getPortraitImages().get(0);
                arrayList.add(imageData);
                clVar.setOptimalPortraitImage(imageData);
            }
            if (clVar.getLandscapeImages().size() > 0) {
                ImageData imageData2 = (ImageData) clVar.getLandscapeImages().get(0);
                arrayList.add(imageData2);
                clVar.setOptimalLandscapeImage(imageData2);
            }
        }
        if (arrayList.size() > 0) {
            if (cyVar.getCloseIcon() != null) {
                arrayList.add(cyVar.getCloseIcon());
            }
            hg.e(arrayList).C(true).P(context);
            he dO = he.dO();
            if (dO == null) {
                ah.a("Disk cache is not available");
                return null;
            }
            for (cl clVar2 : cyVar.bI()) {
                ImageData optimalLandscapeImage = clVar2.getOptimalLandscapeImage();
                if (optimalLandscapeImage == null || dO.ag(optimalLandscapeImage.getUrl()) == null) {
                    ImageData optimalPortraitImage = clVar2.getOptimalPortraitImage();
                    if (optimalPortraitImage == null || dO.ag(optimalPortraitImage.getUrl()) == null) {
                        cyVar.d(clVar2);
                    }
                }
            }
            if (cyVar.getBannersCount() > 0) {
                return cyVar;
            }
        }
        return null;
    }
}
