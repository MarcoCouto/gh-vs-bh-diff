package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.my.target.ee.a;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: StandardAdResponseParser */
public class af extends c<dc> implements a {
    @Nullable
    private String mraidJs;
    @Nullable
    private String v;

    @NonNull
    public static c<dc> f() {
        return new af();
    }

    private af() {
    }

    @Nullable
    public cu a(@NonNull JSONObject jSONObject, @NonNull bz bzVar, @NonNull a aVar, @NonNull Context context) {
        try {
            JSONArray jSONArray = new JSONArray();
            jSONArray.put(jSONObject);
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put("banners", jSONArray);
            JSONObject jSONObject3 = new JSONObject();
            jSONObject3.put(aVar.getFormat(), jSONObject2);
            dc bZ = dc.bZ();
            cr a2 = a(jSONObject, bZ, jSONObject3, bzVar, aVar, context);
            if (a2 == null) {
                return null;
            }
            bZ.a(a2);
            return bZ;
        } catch (JSONException unused) {
            return null;
        }
    }

    @Nullable
    public dc a(@NonNull String str, @NonNull bz bzVar, @Nullable dc dcVar, @NonNull a aVar, @NonNull Context context) {
        JSONObject a2 = a(str, context);
        if (a2 == null) {
            return null;
        }
        if (dcVar == null) {
            dcVar = dc.bZ();
        }
        if (a2.has("html_wrapper")) {
            this.v = a2.optString("html_wrapper");
            a2.remove("html_wrapper");
        }
        this.mraidJs = a2.optString("mraid.js");
        JSONObject optJSONObject = a2.optJSONObject(aVar.getFormat());
        if (optJSONObject == null) {
            if (aVar.isMediationEnabled()) {
                JSONObject optJSONObject2 = a2.optJSONObject("mediation");
                if (optJSONObject2 != null) {
                    cs f = ee.a(this, bzVar, aVar, context).f(optJSONObject2);
                    if (f != null) {
                        dcVar.a(f);
                        return dcVar;
                    }
                }
            }
            return null;
        }
        JSONArray optJSONArray = optJSONObject.optJSONArray("banners");
        if (optJSONArray == null || optJSONArray.length() <= 0) {
            return null;
        }
        ek.cC().a(optJSONObject, dcVar);
        if (optJSONArray.length() > 0) {
            JSONObject optJSONObject3 = optJSONArray.optJSONObject(0);
            if (optJSONObject3 != null) {
                cr a3 = a(optJSONObject3, dcVar, a2, bzVar, aVar, context);
                if (a3 != null) {
                    dcVar.a(a3);
                    return dcVar;
                }
            }
        }
        return null;
    }

    private cr a(@NonNull JSONObject jSONObject, @NonNull dc dcVar, @NonNull JSONObject jSONObject2, @NonNull bz bzVar, @NonNull a aVar, @NonNull Context context) {
        ej j = ej.j(bzVar, aVar, context);
        cr newBanner = cr.newBanner();
        if (!j.a(jSONObject, newBanner, this.mraidJs)) {
            return null;
        }
        if (this.v != null) {
            dcVar.J(this.v);
            dcVar.c(jSONObject2);
            return newBanner;
        }
        dp.P("Required field").Q("Section has no HTML_WRAPPER field, required for viewType = html").r(aVar.getSlotId()).R(bzVar.getUrl()).q(context);
        return null;
    }
}
