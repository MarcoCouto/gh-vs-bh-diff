package com.my.target;

import android.graphics.Rect;
import android.support.annotation.NonNull;
import android.view.View;
import com.github.mikephil.charting.utils.Utils;

/* compiled from: VisibilityChecker */
public class hp {
    public static double j(@NonNull View view) {
        if (view.getVisibility() != 0 || view.getParent() == null || view.getAlpha() < 0.5f) {
            return Utils.DOUBLE_EPSILON;
        }
        Rect rect = new Rect();
        if (!view.getGlobalVisibleRect(rect)) {
            return Utils.DOUBLE_EPSILON;
        }
        double width = (double) (rect.width() * rect.height());
        double width2 = (double) (view.getWidth() * view.getHeight());
        Double.isNaN(width2);
        double d = width2 / 100.0d;
        Double.isNaN(width);
        return width / d;
    }
}
