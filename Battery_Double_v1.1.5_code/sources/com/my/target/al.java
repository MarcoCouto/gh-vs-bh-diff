package com.my.target;

import android.content.Context;
import android.media.AudioManager;
import android.media.AudioManager.OnAudioFocusChangeListener;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout.LayoutParams;
import com.google.android.exoplayer2.util.MimeTypes;
import com.my.target.common.models.ImageData;
import com.my.target.common.models.VideoData;
import com.my.target.ga.d;
import com.my.target.nativeads.views.MediaAdView;
import java.lang.ref.WeakReference;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

/* compiled from: NativeAdVideoController */
public class al implements com.my.target.fj.a, d, com.my.target.hq.a {
    @NonNull
    private final VideoData Q;
    @NonNull
    private final OnAudioFocusChangeListener R;
    @NonNull
    private final co S;
    @NonNull
    private final HashSet<df> T = new HashSet<>();
    @NonNull
    private final ho U;
    @Nullable
    private OnClickListener V;
    @Nullable
    private WeakReference<MediaAdView> W;
    @Nullable
    private WeakReference<fj> X;
    @Nullable
    private WeakReference<ga> Y;
    @Nullable
    private WeakReference<Context> Z;
    private boolean aa;
    /* access modifiers changed from: private */
    public boolean ab;
    private boolean ac;
    private boolean ae;
    @Nullable
    private b af;
    @Nullable
    private hs ag;
    private boolean ah;
    private long ai;
    private int state;
    @NonNull
    private final cn videoBanner;

    /* compiled from: NativeAdVideoController */
    class a implements OnAudioFocusChangeListener {
        private a() {
        }

        public void onAudioFocusChange(int i) {
            switch (i) {
                case -3:
                    al.this.K();
                    return;
                case -2:
                case -1:
                    al.this.M();
                    ah.a("Audiofocus loss, pausing");
                    return;
                case 1:
                case 2:
                case 4:
                    if (al.this.ab) {
                        ah.a("Audiofocus gain, unmuting");
                        al.this.N();
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    /* compiled from: NativeAdVideoController */
    public interface b {
        void P();

        void Q();

        void R();
    }

    public void A() {
    }

    public void onComplete() {
    }

    public al(@NonNull co coVar, @NonNull cn cnVar, @NonNull VideoData videoData) {
        this.videoBanner = cnVar;
        this.S = coVar;
        this.Q = videoData;
        this.aa = this.videoBanner.isAutoPlay();
        this.ae = this.videoBanner.isAutoMute();
        dh statHolder = this.videoBanner.getStatHolder();
        this.U = ho.c(statHolder);
        this.T.addAll(statHolder.ck());
        this.R = new a();
    }

    public void a(@Nullable OnClickListener onClickListener) {
        this.V = onClickListener;
    }

    public void a(@NonNull MediaAdView mediaAdView, @Nullable Context context) {
        StringBuilder sb = new StringBuilder();
        sb.append("register video ad with view ");
        sb.append(mediaAdView);
        ah.a(sb.toString());
        unregister();
        this.W = new WeakReference<>(mediaAdView);
        this.Z = new WeakReference<>(context);
        fn fnVar = new fn(mediaAdView.getContext());
        mediaAdView.addView(fnVar, 0);
        this.U.setView(fnVar);
        if (!this.ab) {
            if (this.aa) {
                B();
            } else {
                x();
            }
        }
        mediaAdView.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                al.this.b(view);
            }
        });
    }

    public void unregister() {
        MediaAdView mediaAdView;
        w();
        this.U.setView(null);
        J();
        if (this.W != null) {
            mediaAdView = (MediaAdView) this.W.get();
            if (mediaAdView != null && (mediaAdView.getChildAt(0) instanceof fn)) {
                mediaAdView.removeViewAt(0);
            }
        } else {
            mediaAdView = null;
        }
        if (!this.ab) {
            if (mediaAdView != null) {
                mediaAdView.setOnClickListener(null);
            }
            this.W = null;
        }
    }

    public void v() {
        MediaAdView I = I();
        if (I == null) {
            ah.a("Trying to play video in unregistered view");
            J();
        } else if (I.getWindowVisibility() != 0) {
            if (this.state == 1) {
                if (this.ag != null) {
                    this.ai = this.ag.getPosition();
                }
                J();
                this.state = 4;
                this.ah = false;
                B();
            } else {
                J();
            }
        } else if (!this.ah) {
            this.ah = true;
            fn fnVar = null;
            if (I.getChildAt(0) instanceof fn) {
                fnVar = (fn) I.getChildAt(0);
            }
            if (fnVar == null) {
                J();
                return;
            }
            if (!(this.ag == null || this.Q == this.ag.dZ())) {
                J();
            }
            if (!this.aa) {
                I.getImageView().setVisibility(0);
                I.getPlayButtonView().setVisibility(0);
                I.getProgressBarView().setVisibility(8);
            }
            if (this.aa && !this.ab) {
                if (this.ag == null || !this.ag.isPaused()) {
                    a(fnVar, true);
                } else {
                    this.ag.resume();
                }
                L();
            }
        }
    }

    public void w() {
        if (this.ah && !this.ab) {
            this.ah = false;
            if (this.state == 1 && this.ag != null) {
                this.ag.pause();
                this.state = 2;
            }
        }
    }

    public void x() {
        Context context;
        this.ac = false;
        MediaAdView I = I();
        if (I != null) {
            ImageView imageView = I.getImageView();
            ImageData image = this.videoBanner.getImage();
            if (image != null) {
                imageView.setImageBitmap(image.getBitmap());
            }
            imageView.setVisibility(0);
            I.getPlayButtonView().setVisibility(0);
            I.getProgressBarView().setVisibility(8);
            context = I.getContext();
        } else {
            context = null;
        }
        if (this.ab && this.Y != null) {
            ga gaVar = (ga) this.Y.get();
            if (gaVar != null) {
                gaVar.du();
                context = gaVar.getContext();
            }
        }
        if (context != null) {
            i(context);
        }
    }

    public void e(float f) {
        if (this.Y != null) {
            ga gaVar = (ga) this.Y.get();
            if (gaVar == null) {
                return;
            }
            if (f > 0.0f) {
                gaVar.x(false);
            } else {
                gaVar.x(true);
            }
        }
    }

    public void y() {
        if (this.state != 1) {
            this.state = 1;
            MediaAdView I = I();
            if (I != null) {
                I.getImageView().setVisibility(4);
                I.getProgressBarView().setVisibility(8);
                I.getPlayButtonView().setVisibility(8);
            }
            if (this.ab && this.Y != null) {
                ga gaVar = (ga) this.Y.get();
                if (gaVar != null) {
                    gaVar.dx();
                }
            }
        }
    }

    public void z() {
        Context context;
        MediaAdView I = I();
        if (I != null) {
            context = I.getContext();
            I.getPlayButtonView().setVisibility(0);
            I.getProgressBarView().setVisibility(8);
        } else {
            context = null;
        }
        M();
        if (I != null) {
            i(context);
        }
        if (this.af != null) {
            this.af.Q();
        }
    }

    public void B() {
        this.state = 4;
        MediaAdView I = I();
        if (I != null) {
            I.getProgressBarView().setVisibility(0);
            I.getImageView().setVisibility(0);
            I.getPlayButtonView().setVisibility(8);
        }
        if (this.ab && this.Y != null) {
            ga gaVar = (ga) this.Y.get();
            if (gaVar != null) {
                gaVar.dv();
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x0059  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x006a  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0093  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0025  */
    public void a(float f, float f2) {
        Context context;
        float duration;
        this.ai = 0;
        if (this.W != null) {
            View view = (View) this.W.get();
            if (view != null) {
                context = view.getContext();
                y();
                this.U.k(f);
                if (!this.ac) {
                    if (this.af != null) {
                        this.af.P();
                    }
                    if (context != null) {
                        sendStat("playbackStarted", context);
                        this.T.clear();
                        this.T.addAll(this.videoBanner.getStatHolder().ck());
                        a(0.0f, context);
                    }
                    this.ac = true;
                }
                duration = this.videoBanner.getDuration();
                if (this.Y != null) {
                    ga gaVar = (ga) this.Y.get();
                    if (gaVar != null) {
                        gaVar.a(f, duration);
                    }
                }
                if (f > duration) {
                    if (f > 0.0f && context != null) {
                        a(f, context);
                    }
                    if (f == duration) {
                        x();
                        this.state = 3;
                        this.aa = false;
                        if (this.ag != null) {
                            this.ag.stop();
                        }
                        if (this.af != null) {
                            this.af.R();
                            return;
                        }
                        return;
                    }
                    return;
                }
                a(duration, duration);
                return;
            }
        }
        context = null;
        y();
        this.U.k(f);
        if (!this.ac) {
        }
        duration = this.videoBanner.getDuration();
        if (this.Y != null) {
        }
        if (f > duration) {
        }
    }

    public void d(String str) {
        this.state = 3;
        x();
    }

    public void a(@Nullable b bVar) {
        this.af = bVar;
    }

    public void a(@NonNull fj fjVar, @NonNull FrameLayout frameLayout) {
        a(fjVar, frameLayout, new ga(frameLayout.getContext()));
    }

    public void a(boolean z) {
        if (this.ag != null && !z) {
            this.ai = this.ag.getPosition();
            J();
            z();
        }
    }

    public void C() {
        ah.a("Dismiss dialog");
        this.X = null;
        this.ab = false;
        L();
        MediaAdView I = I();
        if (I != null) {
            i(I.getContext());
            switch (this.state) {
                case 1:
                    this.state = 4;
                    y();
                    if (this.videoBanner.isAutoPlay()) {
                        this.aa = true;
                    }
                    View childAt = I.getChildAt(0);
                    if (childAt instanceof fn) {
                        a((fn) childAt, true);
                        break;
                    }
                    break;
                case 2:
                case 3:
                    this.aa = false;
                    x();
                    break;
                case 4:
                    this.aa = true;
                    B();
                    View childAt2 = I.getChildAt(0);
                    if (childAt2 instanceof fn) {
                        a((fn) childAt2, true);
                        break;
                    }
                    break;
                default:
                    this.aa = false;
                    break;
            }
            sendStat("fullscreenOff", I.getContext());
            this.Y = null;
        }
    }

    public void D() {
        if (this.X != null) {
            fj fjVar = (fj) this.X.get();
            if (fjVar != null) {
                Context context = fjVar.getContext();
                O();
                sendStat("playbackResumed", context);
            }
        }
        if (this.af != null) {
            this.af.P();
        }
    }

    public void E() {
        O();
        if (this.Y != null) {
            ga gaVar = (ga) this.Y.get();
            if (gaVar != null) {
                gaVar.getMediaAdView().getImageView().setVisibility(8);
                gaVar.dy();
            }
        }
        if (this.af != null) {
            this.af.P();
        }
    }

    public void a(View view) {
        if (this.state == 1) {
            if (this.ag != null) {
                this.ag.pause();
            }
            z();
        }
        if (this.V != null) {
            this.V.onClick(view);
        }
    }

    public void F() {
        if (this.state == 1) {
            M();
            this.state = 2;
            if (this.af != null) {
                this.af.Q();
            }
            if (this.X != null) {
                fj fjVar = (fj) this.X.get();
                if (fjVar != null) {
                    sendStat("playbackPaused", fjVar.getContext());
                }
            }
        }
    }

    public void G() {
        fj fjVar = this.X == null ? null : (fj) this.X.get();
        if (fjVar != null && fjVar.isShowing()) {
            fjVar.dismiss();
        }
    }

    public void H() {
        MediaAdView I = I();
        if (I == null || this.ag == null) {
            this.ae = !this.ae;
            return;
        }
        Context context = I.getContext();
        if (this.ag.isMuted()) {
            this.ag.cJ();
            sendStat("volumeOn", context);
            this.ae = false;
            return;
        }
        this.ag.L();
        sendStat("volumeOff", context);
        this.ae = true;
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public void a(fj fjVar, FrameLayout frameLayout, ga gaVar) {
        this.state = 4;
        this.X = new WeakReference<>(fjVar);
        gaVar.setLayoutParams(new LayoutParams(-1, -1));
        frameLayout.addView(gaVar);
        this.Y = new WeakReference<>(gaVar);
        gaVar.a(this.S, this.Q);
        gaVar.setVideoDialogViewListener(this);
        gaVar.x(this.ae);
        sendStat("fullscreenOn", frameLayout.getContext());
        a(gaVar.getAdVideoView(), this.ae);
    }

    private void a(@NonNull fn fnVar, boolean z) {
        if (this.ag == null) {
            this.ag = hs.T(fnVar.getContext());
            this.ag.a(this);
        }
        if (z) {
            L();
        } else {
            N();
        }
        this.ag.a(this.Q, fnVar);
        if (this.ai > 0) {
            this.ag.seekTo(this.ai);
        }
    }

    @Nullable
    private MediaAdView I() {
        if (this.W != null) {
            return (MediaAdView) this.W.get();
        }
        return null;
    }

    private void J() {
        if (this.ag != null) {
            this.ag.a(null);
            this.ag.destroy();
            this.ag = null;
        }
    }

    /* access modifiers changed from: private */
    public void K() {
        if (this.ag != null && !this.ae) {
            this.ag.K();
        }
    }

    private void L() {
        if (this.ag != null) {
            this.ag.L();
        }
    }

    /* access modifiers changed from: private */
    public void b(@NonNull View view) {
        this.ab = true;
        Context context = this.Z != null ? (Context) this.Z.get() : null;
        if (context == null) {
            context = view.getContext();
        }
        j(context);
        if (this.state == 1) {
            this.state = 4;
        }
        try {
            fj.a(this, context).show();
        } catch (Exception e) {
            e.printStackTrace();
            ah.b("Unable to start video dialog! Check myTarget MediaAdView, maybe it was created with non-Activity context");
            C();
        }
    }

    /* access modifiers changed from: private */
    public void M() {
        if (this.ab && this.Y != null) {
            this.state = 2;
            ga gaVar = (ga) this.Y.get();
            if (gaVar != null) {
                if (this.ag != null) {
                    this.ag.pause();
                }
                gaVar.dw();
            }
        }
    }

    private void i(@NonNull Context context) {
        AudioManager audioManager = (AudioManager) context.getSystemService(MimeTypes.BASE_TYPE_AUDIO);
        if (audioManager != null) {
            audioManager.abandonAudioFocus(this.R);
        }
    }

    private void a(float f, @NonNull Context context) {
        if (!this.T.isEmpty()) {
            Iterator it = this.T.iterator();
            while (it.hasNext()) {
                df dfVar = (df) it.next();
                if (dfVar.cf() <= f) {
                    hl.a((dg) dfVar, context);
                    it.remove();
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void N() {
        if (this.ag != null) {
            this.ag.cJ();
        }
    }

    private void j(@NonNull Context context) {
        AudioManager audioManager = (AudioManager) context.getSystemService(MimeTypes.BASE_TYPE_AUDIO);
        if (audioManager != null) {
            audioManager.requestAudioFocus(this.R, 3, 2);
        }
    }

    private void sendStat(@NonNull String str, @NonNull Context context) {
        hl.a((List<dg>) this.videoBanner.getStatHolder().N(str), context);
    }

    private void O() {
        if (this.ag != null && this.ag.isPaused()) {
            this.ag.resume();
        } else if (this.ab && this.Y != null) {
            a(((ga) this.Y.get()).getAdVideoView(), true);
        }
        B();
    }
}
