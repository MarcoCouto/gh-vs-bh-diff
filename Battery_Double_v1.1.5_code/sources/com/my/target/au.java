package com.my.target;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import com.my.target.ads.InterstitialAd;
import com.my.target.ads.InterstitialAd.InterstitialAdListener;
import com.my.target.common.MyTargetActivity;
import java.util.List;

/* compiled from: InterstitialAdImageEngine */
public class au extends as {
    @NonNull
    private final cl aY;

    /* compiled from: InterstitialAdImageEngine */
    static class a implements com.my.target.es.a {
        @NonNull
        private final au aZ;

        a(@NonNull au auVar) {
            this.aZ = auVar;
        }

        public void b(@Nullable cg cgVar, @Nullable String str, @NonNull Context context) {
            this.aZ.m(context);
        }

        public void ag() {
            this.aZ.ag();
        }

        public void a(@NonNull cg cgVar, @NonNull Context context) {
            this.aZ.a(cgVar, context);
        }
    }

    @NonNull
    static au a(@NonNull InterstitialAd interstitialAd, @NonNull cl clVar) {
        return new au(interstitialAd, clVar);
    }

    private au(InterstitialAd interstitialAd, @NonNull cl clVar) {
        super(interstitialAd);
        this.aY = clVar;
    }

    public void a(@NonNull fj fjVar, @NonNull FrameLayout frameLayout) {
        super.a(fjVar, frameLayout);
        b(frameLayout);
    }

    public void onActivityCreate(@NonNull MyTargetActivity myTargetActivity, @NonNull Intent intent, @NonNull FrameLayout frameLayout) {
        super.onActivityCreate(myTargetActivity, intent, frameLayout);
        b(frameLayout);
    }

    /* access modifiers changed from: protected */
    public boolean af() {
        return this.aY.isAllowBackButton();
    }

    private void b(@NonNull ViewGroup viewGroup) {
        ep t = ep.t(viewGroup.getContext());
        t.a(new a(this));
        t.e(this.aY);
        viewGroup.addView(t.cI(), new LayoutParams(-1, -1));
    }

    /* access modifiers changed from: 0000 */
    public void ag() {
        dismiss();
    }

    /* access modifiers changed from: 0000 */
    public void m(@NonNull Context context) {
        hd.dM().b(this.aY, context);
        InterstitialAdListener listener = this.ad.getListener();
        if (listener != null) {
            listener.onClick(this.ad);
        }
        dismiss();
    }

    /* access modifiers changed from: 0000 */
    public void a(cg cgVar, Context context) {
        hl.a((List<dg>) cgVar.getStatHolder().N("playbackStarted"), context);
    }
}
