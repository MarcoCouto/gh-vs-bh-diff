package com.my.target;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.ImageView;
import com.my.target.common.MyTargetPrivacy;
import com.my.target.common.models.ImageData;
import com.my.target.mediation.MediationAdapter;
import com.my.target.mediation.MediationNativeAdAdapter;
import com.my.target.mediation.MediationNativeAdAdapter.MediationNativeAdListener;
import com.my.target.mediation.MediationNativeAdConfig;
import com.my.target.mediation.MyTargetNativeAdAdapter;
import com.my.target.nativeads.NativeAd;
import com.my.target.nativeads.NativeAd.NativeAdListener;
import com.my.target.nativeads.banners.NativePromoBanner;
import com.my.target.nativeads.views.MediaAdView;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/* compiled from: MediationNativeAdEngine */
public class az extends ax<MediationNativeAdAdapter> implements ao {
    @Nullable
    private WeakReference<MediaAdView> W;
    @NonNull
    final NativeAd ad;
    @NonNull
    private final a adConfig;
    @Nullable
    NativePromoBanner banner;
    @Nullable
    private WeakReference<View> bw;

    /* compiled from: MediationNativeAdEngine */
    class a implements MediationNativeAdListener {
        @NonNull
        private final ct bt;

        a(ct ctVar) {
            this.bt = ctVar;
        }

        public void onLoad(@NonNull NativePromoBanner nativePromoBanner, @NonNull MediationNativeAdAdapter mediationNativeAdAdapter) {
            if (az.this.bl == mediationNativeAdAdapter) {
                StringBuilder sb = new StringBuilder();
                sb.append("MediationNativeAdEngine: data from ");
                sb.append(this.bt.getName());
                sb.append(" ad network loaded successfully");
                ah.a(sb.toString());
                az.this.a(this.bt, true);
                az.this.banner = nativePromoBanner;
                NativeAdListener listener = az.this.ad.getListener();
                if (listener != null) {
                    listener.onLoad(nativePromoBanner, az.this.ad);
                }
            }
        }

        public void onNoAd(@NonNull String str, @NonNull MediationNativeAdAdapter mediationNativeAdAdapter) {
            if (az.this.bl == mediationNativeAdAdapter) {
                StringBuilder sb = new StringBuilder();
                sb.append("MediationNativeAdEngine: no data from ");
                sb.append(this.bt.getName());
                sb.append(" ad network");
                ah.a(sb.toString());
                az.this.a(this.bt, false);
            }
        }

        public void onClick(@NonNull MediationNativeAdAdapter mediationNativeAdAdapter) {
            if (az.this.bl == mediationNativeAdAdapter) {
                Context context = az.this.getContext();
                if (context != null) {
                    hl.a((List<dg>) this.bt.getStatHolder().N("click"), context);
                }
                NativeAdListener listener = az.this.ad.getListener();
                if (listener != null) {
                    listener.onClick(az.this.ad);
                }
            }
        }

        public void onShow(@NonNull MediationNativeAdAdapter mediationNativeAdAdapter) {
            if (az.this.bl == mediationNativeAdAdapter) {
                Context context = az.this.getContext();
                if (context != null) {
                    hl.a((List<dg>) this.bt.getStatHolder().N("playbackStarted"), context);
                }
                NativeAdListener listener = az.this.ad.getListener();
                if (listener != null) {
                    listener.onShow(az.this.ad);
                }
            }
        }

        public void onVideoPlay(@NonNull MediationNativeAdAdapter mediationNativeAdAdapter) {
            if (az.this.bl == mediationNativeAdAdapter) {
                NativeAdListener listener = az.this.ad.getListener();
                if (listener != null) {
                    listener.onVideoPlay(az.this.ad);
                }
            }
        }

        public void onVideoPause(@NonNull MediationNativeAdAdapter mediationNativeAdAdapter) {
            if (az.this.bl == mediationNativeAdAdapter) {
                NativeAdListener listener = az.this.ad.getListener();
                if (listener != null) {
                    listener.onVideoPause(az.this.ad);
                }
            }
        }

        public void onVideoComplete(@NonNull MediationNativeAdAdapter mediationNativeAdAdapter) {
            if (az.this.bl == mediationNativeAdAdapter) {
                NativeAdListener listener = az.this.ad.getListener();
                if (listener != null) {
                    listener.onVideoComplete(az.this.ad);
                }
            }
        }
    }

    /* compiled from: MediationNativeAdEngine */
    static class b extends a implements MediationNativeAdConfig {
        private final int adChoicesPlacement;
        private final boolean autoLoadImages;
        private final boolean autoLoadVideo;

        @NonNull
        public static b a(@NonNull String str, @Nullable String str2, @NonNull Map<String, String> map, int i, int i2, boolean z, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6, boolean z7, int i3) {
            b bVar = new b(str, str2, map, i, i2, z, z2, z3, z4, z5, z6, z7, i3);
            return bVar;
        }

        public boolean isAutoLoadImages() {
            return this.autoLoadImages;
        }

        public boolean isAutoLoadVideo() {
            return this.autoLoadVideo;
        }

        public int getAdChoicesPlacement() {
            return this.adChoicesPlacement;
        }

        private b(@NonNull String str, @Nullable String str2, @NonNull Map<String, String> map, int i, int i2, boolean z, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6, boolean z7, int i3) {
            super(str, str2, map, i, i2, z, z2, z3, z4, z5);
            this.autoLoadImages = z6;
            this.autoLoadVideo = z7;
            this.adChoicesPlacement = i3;
        }
    }

    @NonNull
    public static final az a(@NonNull NativeAd nativeAd, @NonNull cs csVar, @NonNull a aVar) {
        return new az(nativeAd, csVar, aVar);
    }

    private az(@NonNull NativeAd nativeAd, @NonNull cs csVar, @NonNull a aVar) {
        super(csVar);
        this.ad = nativeAd;
        this.adConfig = aVar;
    }

    @Nullable
    public NativePromoBanner Z() {
        return this.banner;
    }

    public void registerView(@NonNull View view, @Nullable List<View> list, int i) {
        List list2;
        View view2;
        if (this.bl == null) {
            ah.b("MediationNativeAdEngine error: can't register view, adapter is not set");
        } else if (this.banner == null) {
            ah.b("MediationNativeAdEngine error: can't register view, banner is null or not loaded yet");
        } else {
            unregisterView();
            if (list == null) {
                list2 = null;
            } else {
                list2 = new ArrayList(list);
            }
            if (!(this.bl instanceof MyTargetNativeAdAdapter) && (view instanceof ViewGroup)) {
                MediaAdView c = c((ViewGroup) view);
                if (c != null) {
                    this.W = new WeakReference<>(c);
                    try {
                        view2 = ((MediationNativeAdAdapter) this.bl).getMediaView(view.getContext());
                    } catch (Throwable th) {
                        StringBuilder sb = new StringBuilder();
                        sb.append("MediationNativeAdEngine error: ");
                        sb.append(th.toString());
                        ah.b(sb.toString());
                        view2 = null;
                    }
                    if (view2 != null) {
                        this.bw = new WeakReference<>(view2);
                    }
                    a(c, view2, this.banner.getImage(), this.banner.hasVideo(), list2);
                }
            }
            try {
                ((MediationNativeAdAdapter) this.bl).registerView(view, list2, i);
            } catch (Throwable th2) {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("MediationNativeAdEngine error: ");
                sb2.append(th2.toString());
                ah.b(sb2.toString());
            }
        }
    }

    public void unregisterView() {
        if (this.bl == null) {
            ah.b("MediationNativeAdEngine error: can't unregister view, adapter is not set");
            return;
        }
        View view = this.bw != null ? (View) this.bw.get() : null;
        if (view != null) {
            this.bw.clear();
            ViewParent parent = view.getParent();
            if (parent instanceof ViewGroup) {
                ((ViewGroup) parent).removeView(view);
            }
        }
        MediaAdView mediaAdView = this.W != null ? (MediaAdView) this.W.get() : null;
        if (mediaAdView != null) {
            this.W.clear();
            ImageData image = this.banner != null ? this.banner.getImage() : null;
            fx fxVar = (fx) mediaAdView.getImageView();
            if (image != null) {
                hg.b(image, (ImageView) fxVar);
            }
            fxVar.setImageData(null);
            mediaAdView.setPlaceHolderDimension(0, 0);
        }
        this.bw = null;
        this.W = null;
        try {
            ((MediationNativeAdAdapter) this.bl).unregisterView();
        } catch (Throwable th) {
            StringBuilder sb = new StringBuilder();
            sb.append("MediationNativeAdEngine error: ");
            sb.append(th.toString());
            ah.b(sb.toString());
        }
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    /* renamed from: ap */
    public MediationNativeAdAdapter al() {
        return new MyTargetNativeAdAdapter();
    }

    /* access modifiers changed from: 0000 */
    public boolean a(@NonNull MediationAdapter mediationAdapter) {
        return mediationAdapter instanceof MediationNativeAdAdapter;
    }

    /* access modifiers changed from: 0000 */
    public void a(@NonNull MediationNativeAdAdapter mediationNativeAdAdapter, @NonNull ct ctVar, @NonNull Context context) {
        MediationNativeAdAdapter mediationNativeAdAdapter2 = mediationNativeAdAdapter;
        b a2 = b.a(ctVar.getPlacementId(), ctVar.getPayload(), ctVar.bv(), this.adConfig.getCustomParams().getAge(), this.adConfig.getCustomParams().getGender(), MyTargetPrivacy.isConsentSpecified(), MyTargetPrivacy.isUserConsent(), MyTargetPrivacy.isUserAgeRestricted(), this.adConfig.isTrackingLocationEnabled(), this.adConfig.isTrackingEnvironmentEnabled(), this.adConfig.isAutoLoadImages(), this.adConfig.isAutoLoadVideo(), this.ad.getAdChoicesPlacement());
        if (mediationNativeAdAdapter2 instanceof MyTargetNativeAdAdapter) {
            cu bw2 = ctVar.bw();
            if (bw2 instanceof da) {
                ((MyTargetNativeAdAdapter) mediationNativeAdAdapter2).setSection((da) bw2);
            }
        }
        try {
            mediationNativeAdAdapter2.load(a2, new a(ctVar), context);
        } catch (Throwable th) {
            StringBuilder sb = new StringBuilder();
            sb.append("MediationNativeAdEngine error: ");
            sb.append(th.toString());
            ah.b(sb.toString());
        }
    }

    /* access modifiers changed from: 0000 */
    public void am() {
        NativeAdListener listener = this.ad.getListener();
        if (listener != null) {
            listener.onNoAd("No data for available ad networks", this.ad);
        }
    }

    private void a(@NonNull MediaAdView mediaAdView, @Nullable View view, @Nullable ImageData imageData, boolean z, @Nullable List<View> list) {
        if (imageData == null && !z) {
            mediaAdView.setPlaceHolderDimension(0, 0);
        } else if (imageData == null || imageData.getWidth() <= 0 || imageData.getHeight() <= 0) {
            mediaAdView.setPlaceHolderDimension(16, 10);
        } else {
            mediaAdView.setPlaceHolderDimension(imageData.getWidth(), imageData.getHeight());
        }
        if (view != null) {
            ah.a("MediationNativeAdEngine: got MediaView from adapter");
            mediaAdView.addView(view);
            if (list != null) {
                int indexOf = list.indexOf(mediaAdView);
                if (indexOf >= 0) {
                    list.remove(indexOf);
                    list.add(view);
                    return;
                }
                return;
            }
            return;
        }
        e(mediaAdView, imageData);
    }

    private void e(@NonNull MediaAdView mediaAdView, @Nullable ImageData imageData) {
        fx fxVar = (fx) mediaAdView.getImageView();
        if (imageData != null) {
            Bitmap bitmap = imageData.getBitmap();
            if (bitmap != null) {
                fxVar.setImageBitmap(bitmap);
                return;
            }
            fxVar.setImageBitmap(null);
            hg.a(imageData, (ImageView) fxVar);
            return;
        }
        fxVar.setImageBitmap(null);
    }

    @Nullable
    private MediaAdView c(@NonNull ViewGroup viewGroup) {
        if (viewGroup instanceof MediaAdView) {
            return (MediaAdView) viewGroup;
        }
        int childCount = viewGroup.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = viewGroup.getChildAt(i);
            if (childAt instanceof ViewGroup) {
                MediaAdView c = c((ViewGroup) childAt);
                if (c != null) {
                    return c;
                }
            }
        }
        return null;
    }
}
