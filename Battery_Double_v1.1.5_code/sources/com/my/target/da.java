package com.my.target;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.LruCache;
import java.util.ArrayList;
import java.util.List;

/* compiled from: NativeAdSection */
public class da extends cu {
    @NonNull
    private static final LruCache<String, String> dH = new LruCache<>(10);
    @NonNull
    private final ArrayList<co> banners = new ArrayList<>();

    @NonNull
    public static LruCache<String, String> bP() {
        return dH;
    }

    @NonNull
    public static da bQ() {
        return new da();
    }

    private da() {
    }

    @Nullable
    public co bR() {
        if (this.banners.size() > 0) {
            return (co) this.banners.get(0);
        }
        return null;
    }

    public void a(@NonNull co coVar) {
        this.banners.add(coVar);
        dH.put(coVar.getId(), coVar.getId());
    }

    @NonNull
    public List<co> bI() {
        return new ArrayList(this.banners);
    }

    public int getBannersCount() {
        return this.banners.size();
    }
}
