package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import org.json.JSONArray;
import org.json.JSONObject;

/* compiled from: InstreamResearchResponseParser */
public class m extends c<cc> {
    private final int duration;

    @NonNull
    public static c<cc> a(int i) {
        return new m(i);
    }

    private m(int i) {
        this.duration = i;
    }

    @Nullable
    public cc a(@NonNull String str, @NonNull bz bzVar, @Nullable cc ccVar, @NonNull a aVar, @NonNull Context context) {
        JSONObject a2 = a(str, context);
        if (a2 == null) {
            return null;
        }
        JSONObject optJSONObject = a2.optJSONObject(aVar.getFormat());
        if (optJSONObject == null) {
            return null;
        }
        JSONArray optJSONArray = optJSONObject.optJSONArray("banners");
        if (optJSONArray == null || optJSONArray.length() <= 0) {
            return null;
        }
        JSONObject optJSONObject2 = optJSONArray.optJSONObject(0);
        if (optJSONObject2 == null) {
            return null;
        }
        cb newBanner = cb.newBanner();
        String optString = optJSONObject2.optString("id");
        if (TextUtils.isEmpty(optString)) {
            optString = optJSONObject2.optString("bannerID", newBanner.getId());
        }
        newBanner.setId(optString);
        String optString2 = optJSONObject2.optString("type");
        if (!TextUtils.isEmpty(optString2)) {
            newBanner.setType(optString2);
        }
        if (optJSONObject2.optJSONArray("statistics") != null) {
            ei.i(bzVar, aVar, context).a(newBanner.getStatHolder(), optJSONObject2, optString, (float) this.duration);
        }
        if (!newBanner.getStatHolder().cm()) {
            return null;
        }
        cc bl = cc.bl();
        bl.a(newBanner);
        return bl;
    }
}
