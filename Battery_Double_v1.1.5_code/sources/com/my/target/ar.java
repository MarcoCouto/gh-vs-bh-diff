package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.my.target.aj.b;
import com.my.target.b.C0072b;
import com.my.target.common.models.AudioData;
import com.my.target.instreamads.InstreamAudioAd;
import com.my.target.instreamads.InstreamAudioAd.InstreamAdCompanionBanner;
import com.my.target.instreamads.InstreamAudioAd.InstreamAudioAdBanner;
import com.my.target.instreamads.InstreamAudioAd.InstreamAudioAdListener;
import com.my.target.instreamads.InstreamAudioAdPlayer;
import com.yandex.mobile.ads.video.models.vmap.AdBreak.BreakId;
import java.util.ArrayList;
import java.util.List;

/* compiled from: InstreamAudioAdEngine */
public class ar {
    /* access modifiers changed from: private */
    @Nullable
    public cz<AudioData> aC;
    /* access modifiers changed from: private */
    @Nullable
    public cn<AudioData> aD;
    @Nullable
    private List<cn<AudioData>> aF;
    private float aG;
    private int aH;
    private int aI;
    /* access modifiers changed from: private */
    public boolean aJ;
    /* access modifiers changed from: private */
    @NonNull
    public final InstreamAudioAd aN;
    @NonNull
    private final cw aO;
    /* access modifiers changed from: private */
    @NonNull
    public final aj aP;
    /* access modifiers changed from: private */
    @Nullable
    public InstreamAudioAdBanner aQ;
    @Nullable
    private List<InstreamAdCompanionBanner> aR;
    @NonNull
    private final a adConfig;
    @NonNull
    private final hd clickHandler;
    private int loadingTimeoutSeconds;
    @NonNull
    private float[] midpoints = new float[0];

    /* compiled from: InstreamAudioAdEngine */
    class a implements b {
        private a() {
        }

        public void b(@NonNull cn cnVar) {
            if (ar.this.aC != null && ar.this.aD == cnVar && ar.this.aQ != null) {
                if (!ar.this.aJ) {
                    ar.this.aJ = true;
                    Context context = ar.this.aP.getContext();
                    if (context == null) {
                        ah.a("can't send stat: context is null");
                    } else {
                        hl.a((List<dg>) ar.this.aC.w("impression"), context);
                    }
                }
                InstreamAudioAdListener listener = ar.this.aN.getListener();
                if (listener != null) {
                    listener.onBannerStart(ar.this.aN, ar.this.aQ);
                }
            }
        }

        public void c(@NonNull cn cnVar) {
            if (ar.this.aC != null && ar.this.aD == cnVar && ar.this.aQ != null) {
                InstreamAudioAdListener listener = ar.this.aN.getListener();
                if (listener != null) {
                    listener.onBannerComplete(ar.this.aN, ar.this.aQ);
                }
            }
        }

        public void d(@NonNull cn cnVar) {
            if (ar.this.aC != null && ar.this.aD == cnVar && ar.this.aQ != null) {
                InstreamAudioAdListener listener = ar.this.aN.getListener();
                if (listener != null) {
                    listener.onBannerComplete(ar.this.aN, ar.this.aQ);
                }
                ar.this.ae();
            }
        }

        public void a(float f, float f2, @NonNull cn cnVar) {
            if (ar.this.aC != null && ar.this.aD == cnVar && ar.this.aQ != null) {
                InstreamAudioAdListener listener = ar.this.aN.getListener();
                if (listener != null) {
                    listener.onBannerTimeLeftChange(f, f2, ar.this.aN);
                }
            }
        }

        public void a(@NonNull String str, @NonNull cn cnVar) {
            if (ar.this.aC != null && ar.this.aD == cnVar) {
                InstreamAudioAdListener listener = ar.this.aN.getListener();
                if (listener != null) {
                    listener.onError(str, ar.this.aN);
                }
                ar.this.ae();
            }
        }
    }

    @NonNull
    public static ar a(@NonNull InstreamAudioAd instreamAudioAd, @NonNull cw cwVar, @NonNull a aVar) {
        return new ar(instreamAudioAd, cwVar, aVar);
    }

    private ar(@NonNull InstreamAudioAd instreamAudioAd, @NonNull cw cwVar, @NonNull a aVar) {
        this.aN = instreamAudioAd;
        this.aO = cwVar;
        this.adConfig = aVar;
        this.aP = aj.p();
        this.aP.a((b) new a());
        this.clickHandler = hd.dM();
    }

    public void setVolume(float f) {
        this.aP.setVolume(f);
    }

    @Nullable
    public InstreamAudioAdPlayer getPlayer() {
        return this.aP.getPlayer();
    }

    public void setPlayer(@Nullable InstreamAudioAdPlayer instreamAudioAdPlayer) {
        this.aP.setPlayer(instreamAudioAdPlayer);
    }

    public void a(@NonNull float[] fArr) {
        this.midpoints = fArr;
    }

    public void e(int i) {
        this.loadingTimeoutSeconds = i;
    }

    public void start(@NonNull String str) {
        stop();
        this.aC = this.aO.y(str);
        if (this.aC != null) {
            this.aP.setConnectionTimeout(this.aC.bJ());
            this.aJ = false;
            this.aI = this.aC.bK();
            this.aH = -1;
            this.aF = this.aC.bI();
            ae();
            return;
        }
        StringBuilder sb = new StringBuilder();
        sb.append("no section with name ");
        sb.append(str);
        ah.a(sb.toString());
    }

    public void startMidroll(float f) {
        boolean z;
        stop();
        float[] fArr = this.midpoints;
        int length = fArr.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                z = false;
                break;
            } else if (Float.compare(fArr[i], f) == 0) {
                z = true;
                break;
            } else {
                i++;
            }
        }
        if (z) {
            this.aC = this.aO.y(BreakId.MIDROLL);
            if (this.aC != null) {
                this.aP.setConnectionTimeout(this.aC.bJ());
                this.aJ = false;
                this.aI = this.aC.bK();
                this.aH = -1;
                this.aG = f;
                a(this.aC, f);
                return;
            }
            return;
        }
        ah.a("attempt to start wrong midpoint, use one of InstreamAd.getMidPoints()");
    }

    public void pause() {
        if (this.aC != null) {
            this.aP.pause();
        }
    }

    public void resume() {
        if (this.aC != null) {
            this.aP.resume();
        }
    }

    public void stop() {
        if (this.aC != null) {
            this.aP.stop();
            a(this.aC);
        }
    }

    public void skip() {
        a((cn) this.aD, "closedByUser");
        stop();
    }

    public void skipBanner() {
        a((cn) this.aD, "closedByUser");
        this.aP.stop();
        ae();
    }

    public void handleCompanionClick(@NonNull InstreamAdCompanionBanner instreamAdCompanionBanner) {
        Context context = this.aP.getContext();
        if (context == null) {
            ah.a("can't handle click: context is null");
            return;
        }
        ch a2 = a(instreamAdCompanionBanner);
        if (a2 == null) {
            ah.a("can't handle click: companion banner not found");
        } else {
            this.clickHandler.b(a2, context);
        }
    }

    public void handleCompanionClick(@NonNull InstreamAdCompanionBanner instreamAdCompanionBanner, @NonNull Context context) {
        ch a2 = a(instreamAdCompanionBanner);
        if (a2 == null) {
            ah.a("can't handle click: companion banner not found");
        } else {
            this.clickHandler.b(a2, context);
        }
    }

    public void handleCompanionShow(@NonNull InstreamAdCompanionBanner instreamAdCompanionBanner) {
        Context context = this.aP.getContext();
        if (context == null) {
            ah.a("can't handle show: context is null");
            return;
        }
        ch a2 = a(instreamAdCompanionBanner);
        if (a2 == null) {
            ah.a("can't handle show: companion banner not found");
        } else {
            hl.a((List<dg>) a2.getStatHolder().N("playbackStarted"), context);
        }
    }

    public void destroy() {
        this.aP.destroy();
    }

    public float getVolume() {
        return this.aP.getVolume();
    }

    private void a(@NonNull cz<AudioData> czVar, float f) {
        ArrayList arrayList = new ArrayList();
        for (cn cnVar : czVar.bI()) {
            if (cnVar.getPoint() == f) {
                arrayList.add(cnVar);
            }
        }
        int size = arrayList.size();
        if (size <= 0 || this.aH >= size - 1) {
            ArrayList f2 = czVar.f(f);
            if (f2.size() > 0) {
                a(f2, czVar, f);
                return;
            }
            StringBuilder sb = new StringBuilder();
            sb.append("There is no one midpoint service for point: ");
            sb.append(f);
            ah.a(sb.toString());
            b(czVar, f);
            return;
        }
        this.aF = arrayList;
        ae();
    }

    /* access modifiers changed from: private */
    public void ae() {
        if (this.aC != null) {
            if (this.aI == 0 || this.aF == null) {
                b(this.aC, this.aG);
                return;
            }
            int i = this.aH + 1;
            if (i < this.aF.size()) {
                this.aH = i;
                cn<AudioData> cnVar = (cn) this.aF.get(i);
                if ("statistics".equals(cnVar.getType())) {
                    a((cn) cnVar, "playbackStarted");
                    ae();
                } else {
                    if (this.aI > 0) {
                        this.aI--;
                    }
                    this.aD = cnVar;
                    this.aQ = InstreamAudioAdBanner.newBanner(cnVar);
                    this.aR = new ArrayList(this.aQ.companionBanners);
                    this.aP.a(cnVar);
                }
            } else {
                b(this.aC, this.aG);
            }
        }
    }

    private void b(@NonNull cz<AudioData> czVar, float f) {
        bz bM = czVar.bM();
        if (bM == null) {
            a(czVar);
        } else if (BreakId.MIDROLL.equals(czVar.getName())) {
            bM.o(true);
            bM.setPoint(f);
            ArrayList arrayList = new ArrayList();
            arrayList.add(bM);
            StringBuilder sb = new StringBuilder();
            sb.append("using doAfter service for point: ");
            sb.append(f);
            ah.a(sb.toString());
            a(arrayList, czVar, f);
        } else {
            a(bM, czVar);
        }
    }

    private void a(@NonNull cz<AudioData> czVar) {
        if (czVar == this.aC) {
            if (BreakId.MIDROLL.equals(czVar.getName())) {
                this.aC.p(this.aI);
            }
            this.aC = null;
            this.aJ = false;
            this.aD = null;
            this.aQ = null;
            this.aH = -1;
            InstreamAudioAdListener listener = this.aN.getListener();
            if (listener != null) {
                listener.onComplete(czVar.getName(), this.aN);
            }
        }
    }

    private void a(@NonNull bz bzVar, @NonNull final cz<AudioData> czVar) {
        Context context = this.aP.getContext();
        if (context == null) {
            ah.a("can't load doAfter service: context is null");
            return;
        }
        StringBuilder sb = new StringBuilder();
        sb.append("loading doAfter service: ");
        sb.append(bzVar.getUrl());
        ah.a(sb.toString());
        i.a(bzVar, this.adConfig, this.loadingTimeoutSeconds).a((C0072b<T>) new i.b() {
            public void onResult(@Nullable cw cwVar, @Nullable String str) {
                ar.this.a(czVar, cwVar, str);
            }
        }).a(context);
    }

    /* access modifiers changed from: private */
    public void a(@NonNull cz<AudioData> czVar, @Nullable cw cwVar, @Nullable String str) {
        if (cwVar == null) {
            if (str != null) {
                StringBuilder sb = new StringBuilder();
                sb.append("loading doAfter service failed: ");
                sb.append(str);
                ah.a(sb.toString());
            }
            if (czVar == this.aC) {
                b(czVar, this.aG);
            }
            return;
        }
        cz y = cwVar.y(czVar.getName());
        if (y != null) {
            czVar.b(y);
        }
        if (czVar == this.aC) {
            this.aF = czVar.bI();
            ae();
        }
    }

    private void a(@NonNull ArrayList<bz> arrayList, @NonNull final cz<AudioData> czVar, final float f) {
        Context context = this.aP.getContext();
        if (context == null) {
            ah.a("can't load midpoint services: context is null");
            return;
        }
        StringBuilder sb = new StringBuilder();
        sb.append("loading midpoint services for point: ");
        sb.append(f);
        ah.a(sb.toString());
        i.a((List<bz>) arrayList, this.adConfig, this.loadingTimeoutSeconds).a((C0072b<T>) new i.b() {
            public void onResult(@Nullable cw cwVar, @Nullable String str) {
                ar.this.a(czVar, cwVar, str, f);
            }
        }).a(context);
    }

    /* access modifiers changed from: private */
    public void a(@NonNull cz<AudioData> czVar, @Nullable cw cwVar, @Nullable String str, float f) {
        if (cwVar == null) {
            if (str != null) {
                StringBuilder sb = new StringBuilder();
                sb.append("loading midpoint services failed: ");
                sb.append(str);
                ah.a(sb.toString());
            }
            if (czVar == this.aC && f == this.aG) {
                b(czVar, f);
            }
            return;
        }
        cz y = cwVar.y(czVar.getName());
        if (y != null) {
            czVar.b(y);
        }
        if (czVar == this.aC && f == this.aG) {
            a(czVar, f);
        }
    }

    private void a(@Nullable cn cnVar, @NonNull String str) {
        if (cnVar == null) {
            ah.a("can't send stat: banner is null");
            return;
        }
        Context context = this.aP.getContext();
        if (context == null) {
            ah.a("can't send stat: context is null");
        } else {
            hl.a((List<dg>) cnVar.getStatHolder().N(str), context);
        }
    }

    @Nullable
    private ch a(@NonNull InstreamAdCompanionBanner instreamAdCompanionBanner) {
        if (this.aR == null || this.aQ == null || this.aD == null) {
            ah.a("can't find companion banner: no playing banner");
            return null;
        }
        ArrayList companionBanners = this.aD.getCompanionBanners();
        int indexOf = this.aR.indexOf(instreamAdCompanionBanner);
        if (indexOf >= 0 && indexOf < companionBanners.size()) {
            return (ch) companionBanners.get(indexOf);
        }
        ah.a("can't find companion banner: provided instreamAdCompanionBanner not found in current playing banner");
        return null;
    }
}
