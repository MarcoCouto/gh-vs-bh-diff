package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.support.v7.widget.LinearSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.ImageView;
import android.widget.TextView;
import com.my.target.common.models.ImageData;
import java.util.ArrayList;
import java.util.List;

/* compiled from: CarouselRecyclerView */
public class gu extends RecyclerView {
    private final OnClickListener cardClickListener;
    /* access modifiers changed from: private */
    @Nullable
    public List<cj> cards;
    @NonNull
    private final gt kS;
    @NonNull
    private final OnClickListener kT;
    @NonNull
    private final LinearSnapHelper kU;
    /* access modifiers changed from: private */
    @Nullable
    public com.my.target.gv.a kV;
    /* access modifiers changed from: private */
    public boolean kW;
    /* access modifiers changed from: private */
    public boolean moving;

    /* compiled from: CarouselRecyclerView */
    static class a extends Adapter<b> {
        @Nullable
        OnClickListener cardClickListener;
        @NonNull
        final Context context;
        @NonNull
        final List<cj> interstitialAdCards;
        private final boolean jB;
        OnClickListener kT;

        a(@NonNull List<cj> list, @NonNull Context context2) {
            this.interstitialAdCards = list;
            this.context = context2;
            this.jB = (context2.getResources().getConfiguration().screenLayout & 15) >= 3;
        }

        @NonNull
        /* renamed from: b */
        public b onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            return new b(new gs(this.jB, this.context));
        }

        /* renamed from: a */
        public void onBindViewHolder(@NonNull b bVar, int i) {
            gs dJ = bVar.dJ();
            cj cjVar = (cj) getInterstitialAdCards().get(i);
            a(cjVar, dJ);
            dJ.a(this.cardClickListener, cjVar.getClickArea());
            dJ.getCtaButtonView().setOnClickListener(this.kT);
        }

        /* renamed from: a */
        public void onViewRecycled(@NonNull b bVar) {
            gs dJ = bVar.dJ();
            dJ.a(null, null);
            dJ.getCtaButtonView().setOnClickListener(null);
        }

        public int getItemCount() {
            return getInterstitialAdCards().size();
        }

        public int getItemViewType(int i) {
            if (i == 0) {
                return 1;
            }
            return i == getItemCount() - 1 ? 2 : 0;
        }

        /* access modifiers changed from: 0000 */
        public void c(OnClickListener onClickListener) {
            this.kT = onClickListener;
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        public List<cj> getInterstitialAdCards() {
            return this.interstitialAdCards;
        }

        /* access modifiers changed from: 0000 */
        public void b(@Nullable OnClickListener onClickListener) {
            this.cardClickListener = onClickListener;
        }

        private void a(@NonNull cj cjVar, @NonNull gs gsVar) {
            ImageData image = cjVar.getImage();
            if (image != null) {
                fx smartImageView = gsVar.getSmartImageView();
                smartImageView.setPlaceholderWidth(image.getWidth());
                smartImageView.setPlaceholderHeight(image.getHeight());
                hg.a(image, (ImageView) smartImageView);
            }
            gsVar.getTitleTextView().setText(cjVar.getTitle());
            gsVar.getDescriptionTextView().setText(cjVar.getDescription());
            gsVar.getCtaButtonView().setText(cjVar.getCtaText());
            TextView domainTextView = gsVar.getDomainTextView();
            String domain = cjVar.getDomain();
            fy ratingView = gsVar.getRatingView();
            if ("web".equals(cjVar.getNavigationType())) {
                ratingView.setVisibility(8);
                domainTextView.setVisibility(0);
                domainTextView.setText(domain);
                return;
            }
            domainTextView.setVisibility(8);
            float rating = cjVar.getRating();
            if (rating > 0.0f) {
                ratingView.setVisibility(0);
                ratingView.setRating(rating);
                return;
            }
            ratingView.setVisibility(8);
        }
    }

    /* compiled from: CarouselRecyclerView */
    static class b extends ViewHolder {
        private final gs kY;

        b(gs gsVar) {
            super(gsVar);
            this.kY = gsVar;
        }

        /* access modifiers changed from: 0000 */
        public gs dJ() {
            return this.kY;
        }
    }

    public gu(Context context) {
        this(context, null);
    }

    public gu(Context context, @Nullable AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public gu(Context context, @Nullable AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.cardClickListener = new OnClickListener() {
            public void onClick(View view) {
                if (!gu.this.moving) {
                    View findContainingItemView = gu.this.getCardLayoutManager().findContainingItemView(view);
                    if (findContainingItemView != null) {
                        if (!gu.this.getCardLayoutManager().g(findContainingItemView) && !gu.this.kW) {
                            gu.this.h(findContainingItemView);
                        } else if (!(!view.isClickable() || gu.this.kV == null || gu.this.cards == null)) {
                            gu.this.kV.a((cj) gu.this.cards.get(gu.this.getCardLayoutManager().getPosition(findContainingItemView)));
                        }
                    }
                }
            }
        };
        this.kT = new OnClickListener() {
            public void onClick(View view) {
                ViewParent parent = view.getParent();
                while (parent != null && !(parent instanceof gs)) {
                    parent = parent.getParent();
                }
                if (gu.this.kV != null && gu.this.cards != null && parent != null) {
                    gu.this.kV.a((cj) gu.this.cards.get(gu.this.getCardLayoutManager().getPosition((View) parent)));
                }
            }
        };
        setOverScrollMode(2);
        this.kS = new gt(context);
        this.kU = new LinearSnapHelper();
        this.kU.attachToRecyclerView(this);
    }

    @VisibleForTesting
    @NonNull
    public LinearSnapHelper getSnapHelper() {
        return this.kU;
    }

    public void d(List<cj> list) {
        a aVar = new a(list, getContext());
        this.cards = list;
        aVar.b(this.cardClickListener);
        aVar.c(this.kT);
        setCardLayoutManager(this.kS);
        setAdapter(aVar);
    }

    public void setCarouselListener(@Nullable com.my.target.gv.a aVar) {
        this.kV = aVar;
    }

    public void setSideSlidesMargins(int i) {
        getCardLayoutManager().A(i);
    }

    public void onScrollStateChanged(int i) {
        super.onScrollStateChanged(i);
        this.moving = i != 0;
        if (!this.moving) {
            checkCardChanged();
        }
    }

    public void B(boolean z) {
        if (z) {
            this.kU.attachToRecyclerView(this);
        } else {
            this.kU.attachToRecyclerView(null);
        }
    }

    @VisibleForTesting(otherwise = 3)
    public gt getCardLayoutManager() {
        return this.kS;
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        if (i3 > i4) {
            this.kW = true;
        }
        super.onLayout(z, i, i2, i3, i4);
    }

    /* access modifiers changed from: protected */
    public void h(@NonNull View view) {
        int[] calculateDistanceToFinalSnap = this.kU.calculateDistanceToFinalSnap(getCardLayoutManager(), view);
        if (calculateDistanceToFinalSnap != null) {
            smoothScrollBy(calculateDistanceToFinalSnap[0], 0);
        }
    }

    private void setCardLayoutManager(@NonNull gt gtVar) {
        gtVar.a(new com.my.target.gt.a() {
            /* renamed from: do reason: not valid java name */
            public void m615do() {
                gu.this.checkCardChanged();
            }
        });
        super.setLayoutManager(gtVar);
    }

    @NonNull
    private List<cj> getVisibleCards() {
        ArrayList arrayList = new ArrayList();
        if (this.cards == null) {
            return arrayList;
        }
        int findFirstCompletelyVisibleItemPosition = getCardLayoutManager().findFirstCompletelyVisibleItemPosition();
        int findLastCompletelyVisibleItemPosition = getCardLayoutManager().findLastCompletelyVisibleItemPosition();
        if (findFirstCompletelyVisibleItemPosition > findLastCompletelyVisibleItemPosition || findFirstCompletelyVisibleItemPosition < 0 || findLastCompletelyVisibleItemPosition >= this.cards.size()) {
            return arrayList;
        }
        while (findFirstCompletelyVisibleItemPosition <= findLastCompletelyVisibleItemPosition) {
            arrayList.add(this.cards.get(findFirstCompletelyVisibleItemPosition));
            findFirstCompletelyVisibleItemPosition++;
        }
        return arrayList;
    }

    /* access modifiers changed from: private */
    public void checkCardChanged() {
        if (this.kV != null) {
            this.kV.b(getVisibleCards());
        }
    }
}
