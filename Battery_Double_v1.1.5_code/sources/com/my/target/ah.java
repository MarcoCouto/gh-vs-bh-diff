package com.my.target;

import android.support.annotation.Nullable;
import android.util.Log;

/* compiled from: Tracer */
public final class ah {
    public static boolean enabled = false;

    public static void a(@Nullable String str) {
        if (enabled) {
            String str2 = "[myTarget]";
            if (str == null) {
                str = "null";
            }
            Log.d(str2, str);
        }
    }

    public static void b(@Nullable String str) {
        if (enabled) {
            String str2 = "[myTarget]";
            if (str == null) {
                str = "null";
            }
            Log.e(str2, str);
        }
    }

    public static void c(@Nullable String str) {
        String str2 = "[myTarget]";
        if (str == null) {
            str = "null";
        }
        Log.i(str2, str);
    }
}
