package com.my.target;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLayoutChangeListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.my.target.common.models.ImageData;
import java.lang.ref.WeakReference;

/* compiled from: AdChoicesHelper */
public class hb {
    @Nullable
    private final by adChoices;
    @Nullable
    private b lL;
    @Nullable
    private a lM;
    /* access modifiers changed from: private */
    @Nullable
    public WeakReference<fl> lN;
    /* access modifiers changed from: private */
    public int lO;

    /* compiled from: AdChoicesHelper */
    static class a implements OnClickListener {
        @NonNull
        private final String cG;

        a(@NonNull String str) {
            this.cG = str;
        }

        public void onClick(View view) {
            Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(this.cG));
            Context context = view.getContext();
            if (!(context instanceof Activity)) {
                intent.addFlags(268435456);
            }
            try {
                context.startActivity(intent);
            } catch (Exception e) {
                StringBuilder sb = new StringBuilder();
                sb.append("Unable to open AdChoices link: ");
                sb.append(e.getMessage());
                ah.a(sb.toString());
            }
        }
    }

    /* compiled from: AdChoicesHelper */
    class b implements OnLayoutChangeListener {
        private b() {
        }

        public void onLayoutChange(View view, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
            int i9;
            int i10;
            int i11;
            int i12;
            fl flVar = hb.this.lN != null ? (fl) hb.this.lN.get() : null;
            if (flVar != null) {
                int measuredWidth = view.getMeasuredWidth();
                int measuredHeight = view.getMeasuredHeight();
                int measuredWidth2 = flVar.getMeasuredWidth();
                int measuredHeight2 = flVar.getMeasuredHeight();
                switch (hb.this.lO) {
                    case 1:
                        i11 = view.getPaddingLeft();
                        i9 = view.getPaddingTop();
                        int paddingLeft = view.getPaddingLeft() + measuredWidth2;
                        i12 = view.getPaddingTop() + measuredHeight2;
                        i10 = paddingLeft;
                        break;
                    case 2:
                        int paddingLeft2 = (measuredWidth - measuredWidth2) - view.getPaddingLeft();
                        i9 = (measuredHeight - view.getPaddingBottom()) - measuredHeight2;
                        i10 = measuredWidth - view.getPaddingRight();
                        i12 = measuredHeight - view.getPaddingBottom();
                        i11 = paddingLeft2;
                        break;
                    case 3:
                        i11 = view.getPaddingLeft();
                        i9 = (measuredHeight - view.getPaddingBottom()) - measuredHeight2;
                        i10 = view.getPaddingLeft() + measuredWidth2;
                        i12 = measuredHeight - view.getPaddingBottom();
                        break;
                    default:
                        int paddingLeft3 = (measuredWidth - measuredWidth2) - view.getPaddingLeft();
                        i9 = view.getPaddingTop();
                        int paddingRight = measuredWidth - view.getPaddingRight();
                        i12 = view.getPaddingTop() + measuredHeight2;
                        i10 = paddingRight;
                        i11 = paddingLeft3;
                        break;
                }
                flVar.layout(i11, i9, i10, i12);
            }
        }
    }

    public static hb a(@Nullable by byVar) {
        return new hb(byVar);
    }

    private hb(@Nullable by byVar) {
        this.adChoices = byVar;
        if (byVar != null) {
            this.lM = new a(byVar.aS());
            this.lL = new b();
        }
    }

    public void a(@NonNull ViewGroup viewGroup, @Nullable fl flVar, int i) {
        this.lO = i;
        if (this.adChoices != null) {
            if (flVar == null) {
                Context context = viewGroup.getContext();
                fl flVar2 = new fl(context);
                flVar2.setId(hm.dV());
                hm.a((View) flVar2, "ad_choices");
                flVar2.setFixedHeight(hm.a(20, context));
                int a2 = hm.a(2, context);
                flVar2.setPadding(a2, a2, a2, a2);
                flVar = flVar2;
            }
            this.lN = new WeakReference<>(flVar);
            a(viewGroup, flVar, this.adChoices);
        } else if (flVar != null) {
            flVar.setImageBitmap(null);
            flVar.setVisibility(8);
        }
    }

    public void i(@NonNull View view) {
        if (this.lL != null) {
            view.removeOnLayoutChangeListener(this.lL);
        }
        fl flVar = this.lN != null ? (fl) this.lN.get() : null;
        if (flVar != null) {
            if (this.adChoices != null) {
                hg.b(this.adChoices.getIcon(), (ImageView) flVar);
            }
            flVar.setOnClickListener(null);
            flVar.setImageBitmap(null);
            flVar.setVisibility(8);
            this.lN.clear();
        }
        this.lN = null;
    }

    private void a(@NonNull ViewGroup viewGroup, @NonNull fl flVar, @NonNull by byVar) {
        flVar.setVisibility(0);
        flVar.setOnClickListener(this.lM);
        viewGroup.addOnLayoutChangeListener(this.lL);
        if (flVar.getParent() == null) {
            try {
                viewGroup.addView(flVar);
            } catch (Exception e) {
                StringBuilder sb = new StringBuilder();
                sb.append("Unable to add AdChoices View: ");
                sb.append(e.getMessage());
                ah.a(sb.toString());
            }
        }
        ImageData icon = byVar.getIcon();
        Bitmap bitmap = icon.getBitmap();
        if (icon.getBitmap() != null) {
            flVar.setImageBitmap(bitmap);
        } else {
            hg.a(icon, (ImageView) flVar);
        }
    }
}
