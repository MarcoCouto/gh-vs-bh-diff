package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import com.my.target.nativeads.NativeAd;
import com.my.target.nativeads.NativeAd.NativeAdListener;
import com.my.target.nativeads.banners.NativePromoBanner;
import java.util.ArrayList;
import java.util.List;

/* compiled from: NativeAdEngine */
public class bb implements ao {
    @NonNull
    private final NativeAd ad;
    @NonNull
    private final co an;
    @NonNull
    private final a bA = new a();
    @NonNull
    private final am bB;
    @NonNull
    private final com.my.target.gd.a bC;
    @Nullable
    private final NativePromoBanner bD;
    private boolean bE;
    @NonNull
    private final ArrayList<cp> bf = new ArrayList<>();
    @NonNull
    private final hd clickHandler = hd.dM();
    @NonNull
    private final hk k;

    /* compiled from: NativeAdEngine */
    class a implements Runnable {
        private a() {
        }

        public void run() {
            bb.this.ar();
        }
    }

    /* compiled from: NativeAdEngine */
    public static class b implements com.my.target.am.a {
        @NonNull
        private final bb bG;

        b(@NonNull bb bbVar) {
            this.bG = bbVar;
        }

        public void onClick(@Nullable View view) {
            this.bG.f(view);
        }

        public void P() {
            this.bG.au();
        }

        public void Q() {
            this.bG.at();
        }

        public void R() {
            this.bG.as();
        }

        public void b(@NonNull View view, int i) {
            this.bG.a(view, i);
        }

        public void b(@NonNull View view, @NonNull int[] iArr) {
            this.bG.a(view, iArr);
        }
    }

    /* compiled from: NativeAdEngine */
    static class c implements com.my.target.gd.a {
        @NonNull
        private final a bA;
        @NonNull
        private final am bB;
        @NonNull
        private final hk k;

        c(@NonNull a aVar, @NonNull hk hkVar, @NonNull am amVar) {
            this.bA = aVar;
            this.k = hkVar;
            this.bB = amVar;
        }

        public void c(boolean z) {
            if (z) {
                this.k.d(this.bA);
                return;
            }
            this.bB.b(false);
            this.k.e(this.bA);
        }
    }

    @NonNull
    public static bb a(@NonNull NativeAd nativeAd, @NonNull co coVar) {
        return new bb(nativeAd, coVar);
    }

    private bb(@NonNull NativeAd nativeAd, @NonNull co coVar) {
        this.ad = nativeAd;
        this.an = coVar;
        this.bD = NativePromoBanner.newBanner(coVar);
        this.bB = am.a(coVar, (com.my.target.am.a) new b(this));
        float viewabilityRate = coVar.getViewabilityRate();
        if (viewabilityRate == 1.0f) {
            this.k = hk.mn;
        } else {
            this.k = hk.C((int) (viewabilityRate * 1000.0f));
        }
        this.bC = new c(this.bA, this.k, this.bB);
    }

    @Nullable
    public NativePromoBanner Z() {
        return this.bD;
    }

    public void registerView(@NonNull View view, @Nullable List<View> list, int i) {
        unregisterView();
        this.bB.a(view, list, this.bC, i);
        if (this.bE && this.bB.T() != 1) {
            return;
        }
        if (this.bB.Y() || this.bB.S()) {
            this.k.d(this.bA);
        }
    }

    public void unregisterView() {
        this.bB.unregisterView();
        this.k.e(this.bA);
    }

    /* access modifiers changed from: 0000 */
    public void ar() {
        int U = this.bB.U();
        Context context = this.bB.getContext();
        if (U == -1 || context == null) {
            this.k.e(this.bA);
            this.bB.V();
        } else if (!this.bE || this.bB.T() == 1) {
            if (U == 1) {
                if (!this.bE) {
                    this.bE = true;
                    o(context);
                }
                if (this.bB.T() == 1) {
                    this.bB.b(true);
                } else {
                    this.k.e(this.bA);
                    this.bB.W();
                }
            } else if (this.bB.T() == 1) {
                this.bB.b(false);
            }
        } else {
            this.k.e(this.bA);
            this.bB.W();
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(@NonNull View view, @NonNull int[] iArr) {
        for (int i : iArr) {
            cp cpVar = (cp) this.an.getNativeAdCards().get(i);
            if (this.bE && !this.bf.contains(cpVar)) {
                if (cpVar != null) {
                    dh statHolder = cpVar.getStatHolder();
                    Context context = view.getContext();
                    if (context != null) {
                        hl.a((List<dg>) statHolder.N("playbackStarted"), context);
                    }
                }
                this.bf.add(cpVar);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(@NonNull View view, int i) {
        ah.a("Click on native card received");
        List nativeAdCards = this.an.getNativeAdCards();
        if (i >= 0 && i < nativeAdCards.size()) {
            a((cg) (cp) nativeAdCards.get(i), view);
        }
        dh statHolder = this.an.getStatHolder();
        Context context = view.getContext();
        if (context != null) {
            hl.a((List<dg>) statHolder.N("click"), context);
        }
    }

    /* access modifiers changed from: 0000 */
    public void as() {
        NativeAdListener listener = this.ad.getListener();
        if (listener != null) {
            listener.onVideoComplete(this.ad);
        }
    }

    /* access modifiers changed from: 0000 */
    public void at() {
        NativeAdListener listener = this.ad.getListener();
        if (listener != null) {
            listener.onVideoPause(this.ad);
        }
    }

    /* access modifiers changed from: 0000 */
    public void au() {
        NativeAdListener listener = this.ad.getListener();
        if (listener != null) {
            listener.onVideoPlay(this.ad);
        }
    }

    /* access modifiers changed from: 0000 */
    public void f(@Nullable View view) {
        ah.a("Click received by native ad");
        if (view != null) {
            a((cg) this.an, view);
        }
    }

    private void o(@NonNull Context context) {
        hl.a((List<dg>) this.an.getStatHolder().N("playbackStarted"), context);
        NativeAdListener listener = this.ad.getListener();
        if (listener != null) {
            listener.onShow(this.ad);
        }
        int T = this.bB.T();
        if (T == 2 || T == 3) {
            int[] X = this.bB.X();
            if (X != null) {
                for (int i : X) {
                    cp cpVar = (cp) this.an.getNativeAdCards().get(i);
                    if (this.bE && !this.bf.contains(cpVar) && cpVar != null) {
                        hl.a((List<dg>) cpVar.getStatHolder().N("playbackStarted"), context);
                        this.bf.add(cpVar);
                    }
                }
            }
        }
    }

    private void a(@Nullable cg cgVar, @NonNull View view) {
        if (cgVar != null) {
            Context context = view.getContext();
            if (context != null) {
                this.clickHandler.b(cgVar, context);
            }
        }
        NativeAdListener listener = this.ad.getListener();
        if (listener != null) {
            listener.onClick(this.ad);
        }
    }
}
