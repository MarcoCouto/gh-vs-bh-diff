package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.my.target.common.models.ImageData;
import com.my.target.common.models.VideoData;
import java.util.ArrayList;
import java.util.List;

/* compiled from: NativeAdResultProcessor */
public class x extends d<da> {
    @NonNull
    public static x k() {
        return new x();
    }

    private x() {
    }

    @Nullable
    public da a(@NonNull da daVar, @NonNull a aVar, @NonNull Context context) {
        List<co> bI = daVar.bI();
        if (bI.isEmpty()) {
            cs bx = daVar.bx();
            if (bx == null || !bx.bt()) {
                return null;
            }
            return daVar;
        }
        ArrayList arrayList = new ArrayList();
        for (co coVar : bI) {
            cn videoBanner = coVar.getVideoBanner();
            if (videoBanner != null) {
                VideoData videoData = (VideoData) videoBanner.getMediaData();
                if (videoData != null && aVar.isAutoLoadVideo() && videoData.isCacheable()) {
                    videoData.setData((String) Cdo.cv().f(videoData.getUrl(), context));
                }
            }
            ImageData image = coVar.getImage();
            if (image != null) {
                image.useCache(true);
                arrayList.add(image);
            }
            ImageData icon = coVar.getIcon();
            if (icon != null) {
                icon.useCache(true);
                arrayList.add(icon);
            }
            for (cp image2 : coVar.getNativeAdCards()) {
                ImageData image3 = image2.getImage();
                if (image3 != null) {
                    image3.useCache(true);
                    arrayList.add(image3);
                }
            }
            by adChoices = coVar.getAdChoices();
            if (adChoices != null) {
                ImageData icon2 = adChoices.getIcon();
                icon2.useCache(true);
                arrayList.add(icon2);
            }
        }
        if (aVar.isAutoLoadImages() && arrayList.size() > 0) {
            hg.e(arrayList).P(context);
        }
        return daVar;
    }
}
