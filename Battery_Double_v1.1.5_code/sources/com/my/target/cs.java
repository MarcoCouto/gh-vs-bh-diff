package com.my.target;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.util.ArrayList;

/* compiled from: Mediation */
public class cs {
    @NonNull
    private final ArrayList<ct> ds = new ArrayList<>();
    private int dt = 60;

    @NonNull
    public static final cs bq() {
        return new cs();
    }

    public int br() {
        return this.dt;
    }

    public void l(int i) {
        this.dt = i;
    }

    private cs() {
    }

    public void b(@NonNull ct ctVar) {
        int size = this.ds.size();
        for (int i = 0; i < size; i++) {
            if (ctVar.getPriority() > ((ct) this.ds.get(i)).getPriority()) {
                this.ds.add(i, ctVar);
                return;
            }
        }
        this.ds.add(ctVar);
    }

    @Nullable
    public ct bs() {
        if (this.ds.isEmpty()) {
            return null;
        }
        return (ct) this.ds.remove(0);
    }

    public boolean bt() {
        return !this.ds.isEmpty();
    }
}
