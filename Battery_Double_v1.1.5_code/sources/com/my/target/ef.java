package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import org.json.JSONArray;
import org.json.JSONObject;

/* compiled from: NativeAdBannerParser */
public class ef {
    @NonNull
    private final a adConfig;
    @Nullable
    private String cj;
    @NonNull
    private final Context context;
    @NonNull
    private final bz eq;
    @NonNull
    private final dv er;

    @NonNull
    public static ef g(@NonNull bz bzVar, @NonNull a aVar, @NonNull Context context2) {
        return new ef(bzVar, aVar, context2);
    }

    private ef(@NonNull bz bzVar, @NonNull a aVar, @NonNull Context context2) {
        this.eq = bzVar;
        this.adConfig = aVar;
        this.context = context2;
        this.er = dv.b(bzVar, aVar, context2);
    }

    public void a(@NonNull JSONObject jSONObject, @NonNull co coVar) {
        this.er.a(jSONObject, (cg) coVar);
        this.cj = coVar.getId();
        JSONObject optJSONObject = jSONObject.optJSONObject("viewability");
        if (optJSONObject != null) {
            if (optJSONObject.has("percent")) {
                int optInt = optJSONObject.optInt("percent");
                if (optInt < 5 || optInt > 100) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("invalid viewability percent ");
                    sb.append(optInt);
                    f("Bad value", sb.toString());
                } else {
                    coVar.setViewabilitySquare(((float) optInt) / 100.0f);
                }
            }
            if (optJSONObject.has("rate")) {
                double optDouble = optJSONObject.optDouble("rate");
                if (optDouble >= 0.5d) {
                    coVar.setViewabilityRate((float) optDouble);
                } else {
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("invalid viewability rate ");
                    sb2.append(optDouble);
                    f("Bad value", sb2.toString());
                }
            }
        }
        JSONArray optJSONArray = jSONObject.optJSONArray("cards");
        if (optJSONArray != null) {
            int length = optJSONArray.length();
            for (int i = 0; i < length; i++) {
                JSONObject optJSONObject2 = optJSONArray.optJSONObject(i);
                if (optJSONObject2 != null) {
                    cp b = b(optJSONObject2, coVar);
                    if (b != null) {
                        coVar.addNativeAdCard(b);
                    }
                }
            }
        }
        if (coVar.getNativeAdCards().isEmpty()) {
            JSONObject optJSONObject3 = jSONObject.optJSONObject("video");
            if (optJSONObject3 != null && hh.dQ()) {
                cn newVideoBanner = cn.newVideoBanner();
                newVideoBanner.setId(coVar.getId());
                if (dw.c(this.eq, this.adConfig, this.context).a(optJSONObject3, newVideoBanner)) {
                    coVar.setVideoBanner(newVideoBanner);
                }
            }
        }
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    @VisibleForTesting
    public cp b(@NonNull JSONObject jSONObject, @NonNull co coVar) {
        cp newCard = cp.newCard(coVar);
        this.er.a(jSONObject, (cg) newCard);
        if (TextUtils.isEmpty(newCard.getTrackingLink())) {
            f("Required field", "no tracking link in nativeAdCard");
            return null;
        } else if (newCard.getImage() == null) {
            f("Required field", "no image in nativeAdCard");
            return null;
        } else {
            newCard.setId(jSONObject.optString("cardID", newCard.getId()));
            return newCard;
        }
    }

    private void f(String str, String str2) {
        dp.P(str).Q(str2).r(this.adConfig.getSlotId()).S(this.cj).R(this.eq.getUrl()).q(this.context);
    }
}
