package com.my.target;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.GradientDrawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.widget.TextView;

@SuppressLint({"AppCompatCustomView"})
/* compiled from: BorderedTextView */
public class fp extends TextView {
    @NonNull
    private final GradientDrawable hu;
    private final int hv;

    public fp(@NonNull Context context) {
        this(context, null);
    }

    public fp(@NonNull Context context, @Nullable AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public fp(@NonNull Context context, @Nullable AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.hu = new GradientDrawable();
        this.hu.setStroke(0, -13421773);
        this.hu.setColor(0);
        this.hv = (int) TypedValue.applyDimension(1, 2.0f, context.getResources().getDisplayMetrics());
    }

    public void f(int i, int i2) {
        a(i, i2, 0);
    }

    public void a(int i, int i2, int i3) {
        this.hu.setStroke(i, i2);
        this.hu.setCornerRadius((float) i3);
        invalidate();
    }

    public void setBackgroundColor(int i) {
        this.hu.setColor(i);
        invalidate();
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        this.hu.setBounds(getPaddingLeft() - this.hv, getPaddingTop(), getWidth(), getHeight());
        this.hu.draw(canvas);
        super.onDraw(canvas);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        setMeasuredDimension(getMeasuredWidth() + (this.hv * 2), getMeasuredHeight());
    }
}
