package com.my.target;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.LightingColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.MotionEvent;
import android.view.View;

/* compiled from: IconButton */
public class fu extends View {
    private final float density;
    @NonNull
    private final Paint hN = new Paint();
    @NonNull
    private final ColorFilter hO;
    @Nullable
    private Bitmap hP;
    private int hQ;
    private int hR;
    private final int padding;
    @NonNull
    private final Rect rect;

    public fu(@NonNull Context context) {
        super(context);
        this.hN.setFilterBitmap(true);
        this.density = context.getResources().getDisplayMetrics().density;
        this.padding = hm.a(10, context);
        this.rect = new Rect();
        this.hO = new LightingColorFilter(-3355444, 1);
    }

    public int getPadding() {
        return this.padding;
    }

    public void a(@Nullable Bitmap bitmap, boolean z) {
        this.hP = bitmap;
        if (this.hP == null) {
            this.hR = 0;
            this.hQ = 0;
        } else if (z) {
            float f = 1.0f;
            if (this.density > 1.0f) {
                f = 2.0f;
            }
            this.hR = (int) ((((float) this.hP.getHeight()) / f) * this.density);
            this.hQ = (int) ((((float) this.hP.getWidth()) / f) * this.density);
        } else {
            this.hQ = this.hP.getWidth();
            this.hR = this.hP.getHeight();
        }
        setMeasuredDimension(this.hQ + (this.padding * 2), this.hR + (this.padding * 2));
        requestLayout();
    }

    @SuppressLint({"ClickableViewAccessibility"})
    public boolean onTouchEvent(MotionEvent motionEvent) {
        int action = motionEvent.getAction();
        if (action != 3) {
            switch (action) {
                case 0:
                    this.hN.setColorFilter(this.hO);
                    invalidate();
                    return true;
                case 1:
                    if (motionEvent.getX() >= 0.0f && motionEvent.getX() <= ((float) getMeasuredWidth()) && motionEvent.getY() >= 0.0f && motionEvent.getY() <= ((float) getMeasuredHeight())) {
                        performClick();
                        break;
                    }
                default:
                    return super.onTouchEvent(motionEvent);
            }
        }
        this.hN.setColorFilter(null);
        invalidate();
        return true;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        setMeasuredDimension(getMeasuredWidth(), getMeasuredHeight());
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (this.hP != null) {
            this.rect.left = this.padding;
            this.rect.top = this.padding;
            this.rect.right = this.hQ + this.padding;
            this.rect.bottom = this.hR + this.padding;
            canvas.drawBitmap(this.hP, null, this.rect, this.hN);
        }
    }
}
