package com.my.target;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.my.target.common.models.AudioData;
import com.yandex.mobile.ads.video.models.vmap.AdBreak.BreakId;
import java.util.ArrayList;
import java.util.HashMap;

/* compiled from: InstreamAudioAdSection */
public class cw extends cu {
    @NonNull
    private final HashMap<String, cz<AudioData>> dy = new HashMap<>();

    @NonNull
    public static cw bB() {
        return new cw();
    }

    private cw() {
        this.dy.put(BreakId.PREROLL, cz.A(BreakId.PREROLL));
        this.dy.put(BreakId.PAUSEROLL, cz.A(BreakId.PAUSEROLL));
        this.dy.put(BreakId.MIDROLL, cz.A(BreakId.MIDROLL));
        this.dy.put("postroll", cz.A("postroll"));
    }

    @Nullable
    public cz<AudioData> y(@NonNull String str) {
        return (cz) this.dy.get(str);
    }

    @NonNull
    public ArrayList<cz<AudioData>> bC() {
        return new ArrayList<>(this.dy.values());
    }

    public int getBannersCount() {
        int i = 0;
        for (cz bannersCount : this.dy.values()) {
            i += bannersCount.getBannersCount();
        }
        return i;
    }

    public boolean bA() {
        for (cz czVar : this.dy.values()) {
            if (czVar.getBannersCount() <= 0) {
                if (czVar.bO()) {
                }
            }
            return true;
        }
        return false;
    }
}
