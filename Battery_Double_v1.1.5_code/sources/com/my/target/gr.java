package com.my.target;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewCompat;
import android.text.TextUtils.TruncateAt;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

@SuppressLint({"ViewConstructor"})
/* compiled from: VideoStyleView */
public class gr extends ViewGroup implements gm {
    private boolean allowClose;
    @Nullable
    private String closeActionText;
    @Nullable
    private String closeDelayActionText;
    @NonNull
    private final Button ctaButton;
    /* access modifiers changed from: private */
    @Nullable
    public com.my.target.eq.a eS;
    @NonNull
    private final TextView hh;
    /* access modifiers changed from: private */
    @NonNull
    public final LinearLayout iA;
    @NonNull
    private final TextView iB;
    @NonNull
    private final FrameLayout iC;
    @NonNull
    private final TextView iD;
    @NonNull
    private final fu iF;
    /* access modifiers changed from: private */
    @NonNull
    public final Runnable iJ = new c();
    @Nullable
    private final Bitmap iM;
    @Nullable
    private final Bitmap iN;
    /* access modifiers changed from: private */
    public int iO;
    private final int iP;
    private boolean iQ;
    /* access modifiers changed from: private */
    @NonNull
    public final gp jK;
    private float jQ;
    /* access modifiers changed from: private */
    @Nullable
    public com.my.target.gn.a jR;
    @NonNull
    private final gb kA;
    /* access modifiers changed from: private */
    @NonNull
    public final gl kB;
    /* access modifiers changed from: private */
    @NonNull
    public final gl kC;
    @NonNull
    private final gl kD;
    @NonNull
    private final d kE = new d();
    @NonNull
    private final a kF = new a();
    private final int kG;
    private float kH;
    private boolean kI;
    @NonNull
    private final b ky;
    @NonNull
    private final TextView kz;
    private final int padding;
    @NonNull
    private final fy starsRatingView;
    @NonNull
    private final hm uiUtils;

    /* compiled from: VideoStyleView */
    class a implements OnClickListener {
        private a() {
        }

        public void onClick(View view) {
            if (view == gr.this.iA) {
                if (gr.this.eS != null) {
                    gr.this.eS.cN();
                }
                gr.this.dz();
            } else if (view == gr.this.kB) {
                if (gr.this.jK.isPlaying() && gr.this.eS != null) {
                    gr.this.eS.cP();
                }
            } else if (view == gr.this.kC) {
                if (gr.this.eS != null) {
                    if (gr.this.isPaused()) {
                        gr.this.eS.cQ();
                    } else {
                        gr.this.eS.cN();
                    }
                }
                gr.this.dz();
            }
        }
    }

    /* compiled from: VideoStyleView */
    class b implements OnClickListener {
        b() {
        }

        public void onClick(View view) {
            if (view.isEnabled() && gr.this.jR != null) {
                gr.this.jR.da();
            }
        }
    }

    /* compiled from: VideoStyleView */
    class c implements Runnable {
        private c() {
        }

        public void run() {
            if (gr.this.iO == 2 || gr.this.iO == 0) {
                gr.this.dz();
            }
        }
    }

    /* compiled from: VideoStyleView */
    class d implements OnClickListener {
        private d() {
        }

        public void onClick(View view) {
            gr.this.removeCallbacks(gr.this.iJ);
            if (gr.this.iO == 2) {
                gr.this.dz();
                return;
            }
            if (gr.this.iO == 0 || gr.this.iO == 3) {
                gr.this.dA();
            }
            gr.this.postDelayed(gr.this.iJ, 4000);
        }
    }

    @NonNull
    public View getView() {
        return this;
    }

    public gr(@NonNull Context context, boolean z) {
        super(context);
        this.kz = new TextView(context);
        this.hh = new TextView(context);
        this.starsRatingView = new fy(context);
        this.ctaButton = new Button(context);
        this.iB = new TextView(context);
        this.iC = new FrameLayout(context);
        this.kB = new gl(context);
        this.kC = new gl(context);
        this.kD = new gl(context);
        this.iD = new TextView(context);
        this.jK = new gp(context, hm.R(context), false, z);
        this.kA = new gb(context);
        this.iF = new fu(context);
        this.iA = new LinearLayout(context);
        this.uiUtils = hm.R(context);
        hm.a((View) this.kz, "dismiss_button");
        hm.a((View) this.hh, "title_text");
        hm.a((View) this.starsRatingView, "stars_view");
        hm.a((View) this.ctaButton, "cta_button");
        hm.a((View) this.iB, "replay_text");
        hm.a((View) this.iC, "shadow");
        hm.a((View) this.kB, "pause_button");
        hm.a((View) this.kC, "play_button");
        hm.a((View) this.kD, "replay_button");
        hm.a((View) this.iD, "domain_text");
        hm.a((View) this.jK, "media_view");
        hm.a((View) this.kA, "video_progress_wheel");
        hm.a((View) this.iF, "sound_button");
        this.iP = this.uiUtils.E(28);
        this.padding = this.uiUtils.E(16);
        this.kG = this.uiUtils.E(4);
        this.iM = fh.v(this.uiUtils.E(28));
        this.iN = fh.w(this.uiUtils.E(28));
        this.ky = new b();
        dl();
    }

    public void setBanner(@NonNull cm cmVar) {
        this.jK.a(cmVar, 1);
        cn videoBanner = cmVar.getVideoBanner();
        if (videoBanner != null) {
            this.kA.setMax(cmVar.getDuration());
            this.iQ = videoBanner.isAllowReplay();
            this.kH = cmVar.getAllowCloseDelay();
            this.allowClose = cmVar.isAllowClose();
            this.ctaButton.setText(cmVar.getCtaText());
            this.hh.setText(cmVar.getTitle());
            if ("store".equals(cmVar.getNavigationType())) {
                if (cmVar.getRating() > 0.0f) {
                    this.starsRatingView.setVisibility(0);
                    this.starsRatingView.setRating(cmVar.getRating());
                } else {
                    this.starsRatingView.setVisibility(8);
                }
                this.iD.setVisibility(8);
            } else {
                this.starsRatingView.setVisibility(8);
                this.iD.setVisibility(0);
                this.iD.setText(cmVar.getDomain());
            }
            this.closeActionText = videoBanner.getCloseActionText();
            this.closeDelayActionText = videoBanner.getCloseDelayActionText();
            this.kz.setText(this.closeActionText);
            if (videoBanner.isAllowClose()) {
                if (videoBanner.getAllowCloseDelay() > 0.0f) {
                    this.kH = videoBanner.getAllowCloseDelay();
                    this.kz.setEnabled(false);
                    this.kz.setTextColor(-3355444);
                    this.kz.setPadding(this.kG, this.kG, this.kG, this.kG);
                    hm.a(this.kz, -2013265920, -2013265920, -3355444, this.uiUtils.E(1), this.uiUtils.E(4));
                    this.kz.setTextSize(2, 12.0f);
                } else {
                    this.kz.setPadding(this.padding, this.padding, this.padding, this.padding);
                    this.kz.setVisibility(0);
                }
            }
            this.iB.setText(videoBanner.getReplayActionText());
            Bitmap J = fh.J(getContext());
            if (J != null) {
                this.kD.setImageBitmap(J);
            }
            if (videoBanner.isAutoPlay()) {
                this.jK.dI();
                dz();
            } else {
                dw();
            }
            this.jQ = videoBanner.getDuration();
            fu fuVar = this.iF;
            fuVar.setOnClickListener(new OnClickListener() {
                public void onClick(View view) {
                    if (gr.this.eS != null) {
                        gr.this.eS.cO();
                    }
                }
            });
            fuVar.a(this.iM, false);
            fuVar.setContentDescription("sound_on");
        }
    }

    public void dF() {
        this.kz.setText(this.closeActionText);
        this.kz.setTextSize(2, 16.0f);
        this.kz.setVisibility(0);
        this.kz.setTextColor(-1);
        this.kz.setEnabled(true);
        this.kz.setPadding(this.padding, this.padding, this.padding, this.padding);
        hm.a(this.kz, -2013265920, -1, -1, this.uiUtils.E(1), this.uiUtils.E(4));
        this.kI = true;
    }

    public void finish() {
        this.kA.setVisibility(8);
        du();
    }

    @NonNull
    public gp getPromoMediaView() {
        return this.jK;
    }

    public void setTimeChanged(float f) {
        if (!this.kI && this.allowClose && this.kH > 0.0f && this.kH >= f) {
            if (this.kz.getVisibility() != 0) {
                this.kz.setVisibility(0);
            }
            if (this.closeDelayActionText != null) {
                int ceil = (int) Math.ceil((double) (this.kH - f));
                String valueOf = String.valueOf(ceil);
                if (this.kH > 9.0f && ceil <= 9) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("0");
                    sb.append(valueOf);
                    valueOf = sb.toString();
                }
                this.kz.setText(this.closeDelayActionText.replace("%d", valueOf));
            }
        }
        if (this.kA.getVisibility() != 0) {
            this.kA.setVisibility(0);
        }
        this.kA.setProgress(f / this.jQ);
        this.kA.setDigit((int) Math.ceil((double) (this.jQ - f)));
    }

    public void destroy() {
        this.jK.destroy();
    }

    public void a(@NonNull cm cmVar) {
        this.jK.setOnClickListener(null);
        this.iF.setVisibility(8);
        dF();
        dz();
    }

    public final void z(boolean z) {
        fu fuVar = this.iF;
        if (z) {
            fuVar.a(this.iN, false);
            fuVar.setContentDescription("sound_off");
            return;
        }
        fuVar.a(this.iM, false);
        fuVar.setContentDescription("sound_on");
    }

    public void setInterstitialPromoViewListener(@Nullable com.my.target.gn.a aVar) {
        this.jR = aVar;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        this.iF.measure(MeasureSpec.makeMeasureSpec(this.iP, 1073741824), MeasureSpec.makeMeasureSpec(this.iP, 1073741824));
        this.kA.measure(MeasureSpec.makeMeasureSpec(this.iP, 1073741824), MeasureSpec.makeMeasureSpec(this.iP, 1073741824));
        int size = MeasureSpec.getSize(i);
        int size2 = MeasureSpec.getSize(i2);
        this.jK.measure(MeasureSpec.makeMeasureSpec(size, Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(size2, Integer.MIN_VALUE));
        int i3 = size - (this.padding << 1);
        int i4 = size2 - (this.padding << 1);
        this.kz.measure(MeasureSpec.makeMeasureSpec(i3 / 2, Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(i4, Integer.MIN_VALUE));
        this.kB.measure(MeasureSpec.makeMeasureSpec(i3, Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(i4, Integer.MIN_VALUE));
        this.kC.measure(MeasureSpec.makeMeasureSpec(i3, Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(i4, Integer.MIN_VALUE));
        this.iA.measure(MeasureSpec.makeMeasureSpec(i3, Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(i4, Integer.MIN_VALUE));
        this.starsRatingView.measure(MeasureSpec.makeMeasureSpec(i3, Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(i4, Integer.MIN_VALUE));
        this.iC.measure(MeasureSpec.makeMeasureSpec(this.jK.getMeasuredWidth(), 1073741824), MeasureSpec.makeMeasureSpec(this.jK.getMeasuredHeight(), 1073741824));
        this.ctaButton.measure(MeasureSpec.makeMeasureSpec(i3, Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(i4, Integer.MIN_VALUE));
        this.hh.measure(MeasureSpec.makeMeasureSpec(i3, Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(i4, Integer.MIN_VALUE));
        this.iD.measure(MeasureSpec.makeMeasureSpec(i3, Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(i4, Integer.MIN_VALUE));
        if (size > size2) {
            int measuredWidth = this.ctaButton.getMeasuredWidth();
            int measuredWidth2 = this.hh.getMeasuredWidth();
            if (this.kA.getMeasuredWidth() + measuredWidth2 + Math.max(this.starsRatingView.getMeasuredWidth(), this.iD.getMeasuredWidth()) + measuredWidth + (this.padding * 3) > i3) {
                int measuredWidth3 = (i3 - this.kA.getMeasuredWidth()) - (this.padding * 3);
                int i5 = measuredWidth3 / 3;
                this.ctaButton.measure(MeasureSpec.makeMeasureSpec(i5, Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(i4, Integer.MIN_VALUE));
                this.starsRatingView.measure(MeasureSpec.makeMeasureSpec(i5, Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(i4, Integer.MIN_VALUE));
                this.iD.measure(MeasureSpec.makeMeasureSpec(i5, Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(i4, Integer.MIN_VALUE));
                this.hh.measure(MeasureSpec.makeMeasureSpec(((measuredWidth3 - this.ctaButton.getMeasuredWidth()) - this.iD.getMeasuredWidth()) - this.starsRatingView.getMeasuredWidth(), Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(i4, Integer.MIN_VALUE));
            }
        } else {
            if (this.hh.getMeasuredHeight() + this.starsRatingView.getMeasuredHeight() + this.iD.getMeasuredHeight() + this.ctaButton.getMeasuredHeight() + (this.padding * 3) > (size2 - this.jK.getMeasuredHeight()) / 2) {
                this.ctaButton.setPadding(this.padding, this.padding / 2, this.padding, this.padding / 2);
                this.ctaButton.measure(MeasureSpec.makeMeasureSpec(i3, Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(i4, Integer.MIN_VALUE));
            }
        }
        setMeasuredDimension(size, size2);
    }

    public boolean isPlaying() {
        return this.jK.isPlaying();
    }

    public boolean isPaused() {
        return this.jK.isPaused();
    }

    @NonNull
    public View getCloseButton() {
        return this.kz;
    }

    public void play() {
        this.jK.dI();
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        int i5 = i3 - i;
        int i6 = i4 - i2;
        int measuredWidth = this.jK.getMeasuredWidth();
        int measuredHeight = this.jK.getMeasuredHeight();
        int i7 = (i5 - measuredWidth) >> 1;
        int i8 = (i6 - measuredHeight) >> 1;
        this.jK.layout(i7, i8, measuredWidth + i7, measuredHeight + i8);
        this.iC.layout(this.jK.getLeft(), this.jK.getTop(), this.jK.getRight(), this.jK.getBottom());
        int measuredWidth2 = this.kC.getMeasuredWidth();
        int i9 = i3 >> 1;
        int i10 = measuredWidth2 >> 1;
        int i11 = i4 >> 1;
        int measuredHeight2 = this.kC.getMeasuredHeight() >> 1;
        this.kC.layout(i9 - i10, i11 - measuredHeight2, i10 + i9, measuredHeight2 + i11);
        int measuredWidth3 = this.kB.getMeasuredWidth();
        int i12 = measuredWidth3 >> 1;
        int measuredHeight3 = this.kB.getMeasuredHeight() >> 1;
        this.kB.layout(i9 - i12, i11 - measuredHeight3, i12 + i9, measuredHeight3 + i11);
        int measuredWidth4 = this.iA.getMeasuredWidth();
        int i13 = measuredWidth4 >> 1;
        int measuredHeight4 = this.iA.getMeasuredHeight() >> 1;
        this.iA.layout(i9 - i13, i11 - measuredHeight4, i9 + i13, i11 + measuredHeight4);
        this.kz.layout(this.padding, this.padding, this.padding + this.kz.getMeasuredWidth(), this.padding + this.kz.getMeasuredHeight());
        if (i5 > i6) {
            int max = Math.max(this.ctaButton.getMeasuredHeight(), Math.max(this.hh.getMeasuredHeight(), this.starsRatingView.getMeasuredHeight()));
            this.ctaButton.layout((i5 - this.padding) - this.ctaButton.getMeasuredWidth(), ((i6 - this.padding) - this.ctaButton.getMeasuredHeight()) - ((max - this.ctaButton.getMeasuredHeight()) >> 1), i5 - this.padding, (i6 - this.padding) - ((max - this.ctaButton.getMeasuredHeight()) >> 1));
            this.iF.layout((this.ctaButton.getRight() - this.iF.getMeasuredWidth()) + this.iF.getPadding(), (((this.jK.getBottom() - (this.padding << 1)) - this.iF.getMeasuredHeight()) - max) + this.iF.getPadding(), this.ctaButton.getRight() + this.iF.getPadding(), ((this.jK.getBottom() - (this.padding << 1)) - max) + this.iF.getPadding());
            this.starsRatingView.layout((this.ctaButton.getLeft() - this.padding) - this.starsRatingView.getMeasuredWidth(), ((i6 - this.padding) - this.starsRatingView.getMeasuredHeight()) - ((max - this.starsRatingView.getMeasuredHeight()) >> 1), this.ctaButton.getLeft() - this.padding, (i6 - this.padding) - ((max - this.starsRatingView.getMeasuredHeight()) >> 1));
            this.iD.layout((this.ctaButton.getLeft() - this.padding) - this.iD.getMeasuredWidth(), ((i6 - this.padding) - this.iD.getMeasuredHeight()) - ((max - this.iD.getMeasuredHeight()) >> 1), this.ctaButton.getLeft() - this.padding, (i6 - this.padding) - ((max - this.iD.getMeasuredHeight()) >> 1));
            int min = Math.min(this.starsRatingView.getLeft(), this.iD.getLeft());
            this.hh.layout((min - this.padding) - this.hh.getMeasuredWidth(), ((i6 - this.padding) - this.hh.getMeasuredHeight()) - ((max - this.hh.getMeasuredHeight()) >> 1), min - this.padding, (i6 - this.padding) - ((max - this.hh.getMeasuredHeight()) >> 1));
            this.kA.layout(this.padding, ((i6 - this.padding) - this.kA.getMeasuredHeight()) - ((max - this.kA.getMeasuredHeight()) >> 1), this.padding + this.kA.getMeasuredWidth(), (i6 - this.padding) - ((max - this.kA.getMeasuredHeight()) >> 1));
            return;
        }
        this.iF.layout(((this.jK.getRight() - this.padding) - this.iF.getMeasuredWidth()) + this.iF.getPadding(), ((this.jK.getBottom() - this.padding) - this.iF.getMeasuredHeight()) + this.iF.getPadding(), (this.jK.getRight() - this.padding) + this.iF.getPadding(), (this.jK.getBottom() - this.padding) + this.iF.getPadding());
        int i14 = this.padding;
        int measuredHeight5 = this.hh.getMeasuredHeight() + this.starsRatingView.getMeasuredHeight() + this.iD.getMeasuredHeight() + this.ctaButton.getMeasuredHeight();
        int bottom = getBottom() - this.jK.getBottom();
        if ((i14 * 3) + measuredHeight5 > bottom) {
            i14 = (bottom - measuredHeight5) / 3;
        }
        int i15 = i5 >> 1;
        this.hh.layout(i15 - (this.hh.getMeasuredWidth() >> 1), this.jK.getBottom() + i14, (this.hh.getMeasuredWidth() >> 1) + i15, this.jK.getBottom() + i14 + this.hh.getMeasuredHeight());
        this.starsRatingView.layout(i15 - (this.starsRatingView.getMeasuredWidth() >> 1), this.hh.getBottom() + i14, (this.starsRatingView.getMeasuredWidth() >> 1) + i15, this.hh.getBottom() + i14 + this.starsRatingView.getMeasuredHeight());
        this.iD.layout(i15 - (this.iD.getMeasuredWidth() >> 1), this.hh.getBottom() + i14, (this.iD.getMeasuredWidth() >> 1) + i15, this.hh.getBottom() + i14 + this.iD.getMeasuredHeight());
        this.ctaButton.layout(i15 - (this.ctaButton.getMeasuredWidth() >> 1), this.starsRatingView.getBottom() + i14, i15 + (this.ctaButton.getMeasuredWidth() >> 1), this.starsRatingView.getBottom() + i14 + this.ctaButton.getMeasuredHeight());
        this.kA.layout(this.padding, (this.jK.getBottom() - this.padding) - this.kA.getMeasuredHeight(), this.padding + this.kA.getMeasuredWidth(), this.jK.getBottom() - this.padding);
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        boolean z = this.jQ <= 0.0f || isHardwareAccelerated();
        if (this.jR != null) {
            this.jR.s(z);
        }
    }

    private void dl() {
        setBackgroundColor(ViewCompat.MEASURED_STATE_MASK);
        int i = this.padding;
        this.iF.setId(ix);
        this.jK.setOnClickListener(this.kE);
        this.jK.setBackgroundColor(ViewCompat.MEASURED_STATE_MASK);
        this.jK.initView();
        this.iC.setBackgroundColor(-1728053248);
        this.iC.setVisibility(8);
        this.kz.setTextSize(2, 16.0f);
        this.kz.setTransformationMethod(null);
        this.kz.setEllipsize(TruncateAt.END);
        this.kz.setVisibility(8);
        if (VERSION.SDK_INT >= 17) {
            this.kz.setTextAlignment(4);
        }
        this.kz.setTextColor(-1);
        hm.a(this.kz, -2013265920, -1, -1, this.uiUtils.E(1), this.uiUtils.E(4));
        this.hh.setMaxLines(2);
        this.hh.setEllipsize(TruncateAt.END);
        this.hh.setTextSize(2, 18.0f);
        this.hh.setTextColor(-1);
        hm.a(this.ctaButton, -2013265920, -1, -1, this.uiUtils.E(1), this.uiUtils.E(4));
        this.ctaButton.setTextColor(-1);
        this.ctaButton.setTransformationMethod(null);
        this.ctaButton.setGravity(1);
        this.ctaButton.setTextSize(2, 16.0f);
        this.ctaButton.setMinimumWidth(this.uiUtils.E(100));
        this.ctaButton.setPadding(i, i, i, i);
        this.hh.setShadowLayer((float) this.uiUtils.E(1), (float) this.uiUtils.E(1), (float) this.uiUtils.E(1), ViewCompat.MEASURED_STATE_MASK);
        this.iD.setTextColor(-3355444);
        this.iD.setMaxEms(10);
        this.iD.setShadowLayer((float) this.uiUtils.E(1), (float) this.uiUtils.E(1), (float) this.uiUtils.E(1), ViewCompat.MEASURED_STATE_MASK);
        this.iA.setOnClickListener(this.kF);
        this.iA.setGravity(17);
        this.iA.setVisibility(8);
        this.iA.setPadding(this.uiUtils.E(8), 0, this.uiUtils.E(8), 0);
        this.iB.setSingleLine();
        this.iB.setEllipsize(TruncateAt.END);
        this.iB.setTypeface(this.iB.getTypeface(), 1);
        this.iB.setTextColor(-1);
        this.iB.setTextSize(2, 16.0f);
        LayoutParams layoutParams = new LayoutParams(-2, -2);
        layoutParams.leftMargin = this.uiUtils.E(4);
        this.kD.setPadding(this.uiUtils.E(16), this.uiUtils.E(16), this.uiUtils.E(16), this.uiUtils.E(16));
        this.kB.setOnClickListener(this.kF);
        this.kB.setVisibility(8);
        this.kB.setPadding(this.uiUtils.E(16), this.uiUtils.E(16), this.uiUtils.E(16), this.uiUtils.E(16));
        this.kC.setOnClickListener(this.kF);
        this.kC.setVisibility(8);
        this.kC.setPadding(this.uiUtils.E(16), this.uiUtils.E(16), this.uiUtils.E(16), this.uiUtils.E(16));
        Bitmap K = fh.K(getContext());
        if (K != null) {
            this.kC.setImageBitmap(K);
        }
        Bitmap L = fh.L(getContext());
        if (L != null) {
            this.kB.setImageBitmap(L);
        }
        hm.a(this.kB, -2013265920, -1, -1, this.uiUtils.E(1), this.uiUtils.E(4));
        hm.a(this.kC, -2013265920, -1, -1, this.uiUtils.E(1), this.uiUtils.E(4));
        hm.a(this.kD, -2013265920, -1, -1, this.uiUtils.E(1), this.uiUtils.E(4));
        this.starsRatingView.setStarSize(this.uiUtils.E(12));
        this.kA.setVisibility(8);
        addView(this.jK);
        addView(this.iC);
        addView(this.iF);
        addView(this.kz);
        addView(this.kA);
        addView(this.iA);
        addView(this.kB);
        addView(this.kC);
        addView(this.starsRatingView);
        addView(this.iD);
        addView(this.ctaButton);
        addView(this.hh);
        this.iA.addView(this.kD);
        this.iA.addView(this.iB, layoutParams);
    }

    public void setMediaListener(@Nullable com.my.target.eq.a aVar) {
        this.eS = aVar;
        this.jK.setInterstitialPromoViewListener(aVar);
    }

    public void resume() {
        this.jK.resume();
    }

    public void setClickArea(@NonNull ca caVar) {
        StringBuilder sb = new StringBuilder();
        sb.append("Apply click area ");
        sb.append(caVar.bk());
        sb.append(" to view");
        ah.a(sb.toString());
        if (caVar.dn) {
            setOnClickListener(this.ky);
        }
        if (caVar.dh || caVar.dn) {
            this.ctaButton.setOnClickListener(this.ky);
        } else {
            this.ctaButton.setOnClickListener(null);
            this.ctaButton.setEnabled(false);
        }
        if (caVar.db || caVar.dn) {
            this.hh.setOnClickListener(this.ky);
        } else {
            this.hh.setOnClickListener(null);
        }
        if (caVar.df || caVar.dn) {
            this.starsRatingView.setOnClickListener(this.ky);
        } else {
            this.starsRatingView.setOnClickListener(null);
        }
        if (caVar.dk || caVar.dn) {
            this.iD.setOnClickListener(this.ky);
        } else {
            this.iD.setOnClickListener(null);
        }
        if (caVar.dm || caVar.dn) {
            setOnClickListener(this.ky);
        }
    }

    public void stop(boolean z) {
        this.jK.A(true);
    }

    public void pause() {
        if (this.iO == 0 || this.iO == 2) {
            dw();
            this.jK.pause();
        }
    }

    public void z(int i) {
        this.jK.z(i);
    }

    public void dG() {
        this.jK.dG();
        dx();
    }

    /* access modifiers changed from: private */
    public void dz() {
        this.iO = 0;
        this.iA.setVisibility(8);
        this.kC.setVisibility(8);
        this.kB.setVisibility(8);
        this.iC.setVisibility(8);
    }

    private void dw() {
        this.iO = 1;
        this.iA.setVisibility(8);
        this.kC.setVisibility(0);
        this.kB.setVisibility(8);
        this.iC.setVisibility(0);
    }

    private void dx() {
        this.iA.setVisibility(8);
        this.kC.setVisibility(8);
        if (this.iO != 2) {
            this.kB.setVisibility(8);
        }
    }

    private void du() {
        this.iO = 4;
        if (this.iQ) {
            this.iA.setVisibility(0);
            this.iC.setVisibility(0);
        }
        this.kC.setVisibility(8);
        this.kB.setVisibility(8);
    }

    /* access modifiers changed from: private */
    public void dA() {
        this.iO = 2;
        this.iA.setVisibility(8);
        this.kC.setVisibility(8);
        this.kB.setVisibility(0);
        this.iC.setVisibility(8);
    }
}
