package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import com.ironsource.sdk.constants.Constants.ParametersKeys;
import com.my.target.b.C0072b;
import com.my.target.common.BaseAd;
import com.my.target.l.b;
import java.util.ArrayList;
import java.util.List;

public class InstreamResearch extends BaseAd {
    @Nullable
    private cb banner;
    @NonNull
    private final Context context;
    private final int duration;
    private int lastPosition = -1;
    @Nullable
    private InstreamResearchListener listener;
    @Nullable
    private ac researchProgressTracker;
    @Nullable
    private ad researchViewabilityTracker;
    private int state = 0;

    public interface InstreamResearchListener {
        void onLoad(@NonNull InstreamResearch instreamResearch);

        void onNoData(@NonNull InstreamResearch instreamResearch, @Nullable String str);
    }

    private String getReadableState(int i) {
        switch (i) {
            case 0:
                return "idle";
            case 1:
                return ParametersKeys.VIDEO_STATUS_STARTED;
            case 2:
                return "paused";
            case 3:
                return "completed";
            default:
                return "unknown";
        }
    }

    @NonNull
    public static InstreamResearch newResearch(int i, int i2, @NonNull Context context2) {
        return new InstreamResearch(i, i2, context2);
    }

    private InstreamResearch(int i, int i2, @NonNull Context context2) {
        super(i, "instreamresearch");
        this.duration = i2;
        this.context = context2;
        ah.c("InstreamResearch created. Version: 5.4.7");
    }

    public void load() {
        l.a(this.adConfig, this.duration).a((C0072b<T>) new b() {
            /* renamed from: a */
            public void onResult(@Nullable cc ccVar, @Nullable String str) {
                InstreamResearch.this.handleResult(ccVar, str);
            }
        }).a(this.context);
    }

    public void registerPlayerView(@NonNull View view) {
        if (this.researchViewabilityTracker != null) {
            this.researchViewabilityTracker.setView(view);
        }
    }

    public void unregisterPlayerView() {
        if (this.researchViewabilityTracker != null) {
            this.researchViewabilityTracker.setView(null);
        }
    }

    public void trackPause() {
        if (this.state != 1) {
            StringBuilder sb = new StringBuilder();
            sb.append("Unable to track pause, wrong state ");
            sb.append(getReadableState(this.state));
            ah.b(sb.toString());
            return;
        }
        trackEvent("playbackPaused");
        this.state = 2;
    }

    public void trackResume() {
        if (this.state != 2) {
            StringBuilder sb = new StringBuilder();
            sb.append("VideoAdTracker error: unable to track resume, wrong state ");
            sb.append(getReadableState(this.state));
            ah.b(sb.toString());
            return;
        }
        trackEvent("playbackResumed");
        this.state = 1;
    }

    public void trackProgress(float f) {
        if (this.state < 1) {
            trackEvent("playbackStarted");
            this.state = 1;
        }
        if (this.state > 1) {
            StringBuilder sb = new StringBuilder();
            sb.append("Unable to track progress while state is: ");
            sb.append(getReadableState(this.state));
            ah.a(sb.toString());
            return;
        }
        int round = Math.round(f);
        if (round < this.lastPosition) {
            trackEvent("rewind");
        } else if (round == this.lastPosition) {
            return;
        }
        this.lastPosition = round;
        if (this.researchViewabilityTracker != null) {
            this.researchViewabilityTracker.c(round);
        }
        if (this.researchProgressTracker != null) {
            this.researchProgressTracker.a(round, this.duration, this.context);
        }
    }

    public void trackMute(boolean z) {
        trackEvent(z ? "volumeOff" : "volumeOn");
    }

    public void trackFullscreen(boolean z) {
        trackEvent(z ? "fullscreenOn" : "fullscreenOff");
    }

    public void setListener(@Nullable InstreamResearchListener instreamResearchListener) {
        this.listener = instreamResearchListener;
    }

    /* access modifiers changed from: private */
    public void handleResult(@Nullable cc ccVar, @Nullable String str) {
        if (ccVar != null) {
            this.banner = ccVar.bm();
            if (this.banner != null) {
                this.researchProgressTracker = ac.a(this.banner.getStatHolder());
                this.researchViewabilityTracker = ad.b(this.banner.getStatHolder());
                if (this.listener != null) {
                    this.listener.onLoad(this);
                }
                return;
            }
        }
        if (this.listener != null) {
            this.listener.onNoData(this, str);
        }
    }

    private void trackEvent(@NonNull String str) {
        if (this.banner != null) {
            ArrayList N = this.banner.getStatHolder().N(str);
            if (!N.isEmpty()) {
                hl.a((List<dg>) N, this.context);
            }
        }
    }
}
