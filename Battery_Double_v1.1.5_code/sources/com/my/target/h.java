package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.util.Iterator;

/* compiled from: InstreamAdResultProcessor */
public class h extends d<cv> {
    @NonNull
    public static h g() {
        return new h();
    }

    private h() {
    }

    @Nullable
    public cv a(@NonNull cv cvVar, @NonNull a aVar, @NonNull Context context) {
        Iterator it = cvVar.bz().iterator();
        while (it.hasNext()) {
            ((cz) it.next()).bN();
        }
        return cvVar;
    }
}
