package com.my.target;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewCompat;
import android.text.TextUtils.TruncateAt;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import com.my.target.common.models.ImageData;
import com.my.target.common.models.VideoData;
import com.my.target.nativeads.views.MediaAdView;

/* compiled from: VideoDialogView */
public class ga extends ViewGroup {
    static final int MEDIA_ID = hm.dV();
    static final int RATING_ID = hm.dV();
    static final int ij = hm.dV();
    static final int ip = hm.dV();
    static final int iq = hm.dV();
    static final int ir = hm.dV();
    static final int is = hm.dV();
    static final int it = hm.dV();
    static final int iu = hm.dV();
    static final int iv = hm.dV();
    static final int iw = hm.dV();
    static final int ix = hm.dV();
    static final int iy = hm.dV();
    @NonNull
    private final Button ctaButton;
    @NonNull
    private final fn hc;
    @NonNull
    private final TextView hh;
    @NonNull
    private final LinearLayout iA;
    @NonNull
    private final TextView iB;
    @NonNull
    private final FrameLayout iC;
    @NonNull
    private final TextView iD;
    @NonNull
    private final gc iE;
    @NonNull
    private final fu iF;
    @NonNull
    private final ft iG;
    @NonNull
    private final ft iH;
    @NonNull
    private final ft iI;
    /* access modifiers changed from: private */
    @NonNull
    public final Runnable iJ = new b();
    @NonNull
    private final c iK = new c();
    @NonNull
    private final OnClickListener iL = new a();
    @Nullable
    private final Bitmap iM;
    @Nullable
    private final Bitmap iN;
    /* access modifiers changed from: private */
    public int iO;
    private final int iP;
    private boolean iQ;
    /* access modifiers changed from: private */
    @Nullable
    public d iR;
    @NonNull
    private final Button iz;
    @NonNull
    private final MediaAdView mediaAdView;
    private final int padding;
    @NonNull
    private final fy starsRatingView;
    @NonNull
    private final hm uiUtils;

    /* compiled from: VideoDialogView */
    class a implements OnClickListener {
        private a() {
        }

        public void onClick(View view) {
            if (ga.this.iR != null) {
                int id = view.getId();
                if (id == ga.iq) {
                    ga.this.iR.a(view);
                } else if (id == ga.ir) {
                    ga.this.iR.E();
                } else if (id == ga.it) {
                    ga.this.iR.F();
                } else if (id == ga.is) {
                    ga.this.iR.D();
                } else if (id == ga.ip) {
                    ga.this.iR.G();
                } else if (id == ga.ix) {
                    ga.this.iR.H();
                }
            }
        }
    }

    /* compiled from: VideoDialogView */
    class b implements Runnable {
        private b() {
        }

        public void run() {
            if (ga.this.iO == 2) {
                ga.this.dz();
            }
        }
    }

    /* compiled from: VideoDialogView */
    class c implements OnClickListener {
        private c() {
        }

        public void onClick(View view) {
            ga.this.removeCallbacks(ga.this.iJ);
            if (ga.this.iO == 2) {
                ga.this.dz();
                return;
            }
            if (ga.this.iO == 0) {
                ga.this.dA();
            }
            ga.this.postDelayed(ga.this.iJ, 4000);
        }
    }

    /* compiled from: VideoDialogView */
    public interface d {
        void D();

        void E();

        void F();

        void G();

        void H();

        void a(View view);
    }

    public ga(@NonNull Context context) {
        super(context);
        this.iz = new Button(context);
        this.hh = new TextView(context);
        this.starsRatingView = new fy(context);
        this.ctaButton = new Button(context);
        this.iB = new TextView(context);
        this.iC = new FrameLayout(context);
        this.iG = new ft(context);
        this.iH = new ft(context);
        this.iI = new ft(context);
        this.iD = new TextView(context);
        this.mediaAdView = new MediaAdView(context);
        this.iE = new gc(context);
        this.iF = new fu(context);
        this.iA = new LinearLayout(context);
        this.uiUtils = hm.R(context);
        this.hc = new fn(context);
        this.iM = fi.v(this.uiUtils.E(28));
        this.iN = fi.w(this.uiUtils.E(28));
        hm.a((View) this.iz, "dismiss_button");
        hm.a((View) this.hh, "title_text");
        hm.a((View) this.starsRatingView, "stars_view");
        hm.a((View) this.ctaButton, "cta_button");
        hm.a((View) this.iB, "replay_text");
        hm.a((View) this.iC, "shadow");
        hm.a((View) this.iG, "pause_button");
        hm.a((View) this.iH, "play_button");
        hm.a((View) this.iI, "replay_button");
        hm.a((View) this.iD, "domain_text");
        hm.a((View) this.mediaAdView, "media_view");
        hm.a((View) this.iE, "video_progress_wheel");
        hm.a((View) this.iF, "sound_button");
        this.iP = this.uiUtils.E(28);
        this.padding = this.uiUtils.E(16);
        dl();
    }

    @NonNull
    public MediaAdView getMediaAdView() {
        return this.mediaAdView;
    }

    public void a(@NonNull co coVar, @NonNull VideoData videoData) {
        cn videoBanner = coVar.getVideoBanner();
        if (videoBanner != null) {
            this.iE.setMax(coVar.getDuration());
            this.iQ = videoBanner.isAllowReplay();
            this.ctaButton.setText(coVar.getCtaText());
            this.hh.setText(coVar.getTitle());
            if ("store".equals(coVar.getNavigationType())) {
                this.iD.setVisibility(8);
                if (coVar.getVotes() == 0 || coVar.getRating() <= 0.0f) {
                    this.starsRatingView.setVisibility(8);
                } else {
                    this.starsRatingView.setVisibility(0);
                    this.starsRatingView.setRating(coVar.getRating());
                }
            } else {
                this.starsRatingView.setVisibility(8);
                this.iD.setVisibility(0);
                this.iD.setText(coVar.getDomain());
            }
            this.iz.setText(videoBanner.getCloseActionText());
            this.iB.setText(videoBanner.getReplayActionText());
            Bitmap J = fi.J(getContext());
            if (J != null) {
                this.iI.setImageBitmap(J);
            }
            this.mediaAdView.setPlaceHolderDimension(videoData.getWidth(), videoData.getHeight());
            ImageData image = coVar.getImage();
            if (image != null) {
                this.mediaAdView.getImageView().setImageBitmap(image.getBitmap());
            }
        }
    }

    public void setVideoDialogViewListener(@Nullable d dVar) {
        this.iR = dVar;
    }

    public void a(float f, float f2) {
        if (this.iE.getVisibility() != 0) {
            this.iE.setVisibility(0);
        }
        this.iE.setProgress(f / f2);
        this.iE.setDigit((int) Math.ceil((double) (f2 - f)));
    }

    public void du() {
        if (this.iO != 4) {
            this.iO = 4;
            this.mediaAdView.getImageView().setVisibility(0);
            this.mediaAdView.getProgressBarView().setVisibility(8);
            if (this.iQ) {
                this.iA.setVisibility(0);
                this.iC.setVisibility(0);
            }
            this.iH.setVisibility(8);
            this.iG.setVisibility(8);
            this.iE.setVisibility(8);
        }
    }

    public void dv() {
        if (this.iO != 3) {
            this.iO = 3;
            this.mediaAdView.getProgressBarView().setVisibility(0);
            this.iA.setVisibility(8);
            this.iH.setVisibility(8);
            this.iG.setVisibility(8);
            this.iC.setVisibility(8);
        }
    }

    public void dw() {
        if (this.iO != 1) {
            this.iO = 1;
            this.mediaAdView.getImageView().setVisibility(0);
            this.mediaAdView.getProgressBarView().setVisibility(8);
            this.iA.setVisibility(8);
            this.iH.setVisibility(0);
            this.iG.setVisibility(8);
            this.iC.setVisibility(0);
        }
    }

    public void dx() {
        if (this.iO != 0 && this.iO != 2) {
            this.iO = 0;
            this.mediaAdView.getImageView().setVisibility(8);
            this.mediaAdView.getProgressBarView().setVisibility(8);
            this.iA.setVisibility(8);
            this.iH.setVisibility(8);
            if (this.iO != 2) {
                this.iG.setVisibility(8);
            }
        }
    }

    public void x(boolean z) {
        if (z) {
            this.iF.a(this.iN, false);
            this.iF.setContentDescription("sound off");
            return;
        }
        this.iF.a(this.iM, false);
        this.iF.setContentDescription("sound on");
    }

    public void dy() {
        this.mediaAdView.getImageView().setVisibility(0);
    }

    @NonNull
    public fn getAdVideoView() {
        return this.hc;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        this.iF.measure(MeasureSpec.makeMeasureSpec(this.iP, 1073741824), MeasureSpec.makeMeasureSpec(this.iP, 1073741824));
        this.iE.measure(MeasureSpec.makeMeasureSpec(this.iP, 1073741824), MeasureSpec.makeMeasureSpec(this.iP, 1073741824));
        int size = MeasureSpec.getSize(i);
        int size2 = MeasureSpec.getSize(i2);
        this.mediaAdView.measure(MeasureSpec.makeMeasureSpec(size, Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(size2, Integer.MIN_VALUE));
        int i3 = size - (this.padding << 1);
        int i4 = size2 - (this.padding << 1);
        this.iz.measure(MeasureSpec.makeMeasureSpec(i3 / 2, Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(i4, Integer.MIN_VALUE));
        this.iG.measure(MeasureSpec.makeMeasureSpec(i3, Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(i4, Integer.MIN_VALUE));
        this.iH.measure(MeasureSpec.makeMeasureSpec(i3, Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(i4, Integer.MIN_VALUE));
        this.iA.measure(MeasureSpec.makeMeasureSpec(i3, Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(i4, Integer.MIN_VALUE));
        this.starsRatingView.measure(MeasureSpec.makeMeasureSpec(i3, Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(i4, Integer.MIN_VALUE));
        this.iC.measure(MeasureSpec.makeMeasureSpec(this.mediaAdView.getMeasuredWidth(), 1073741824), MeasureSpec.makeMeasureSpec(this.mediaAdView.getMeasuredHeight(), 1073741824));
        this.ctaButton.measure(MeasureSpec.makeMeasureSpec(i3, Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(i4, Integer.MIN_VALUE));
        this.hh.measure(MeasureSpec.makeMeasureSpec(i3, Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(i4, Integer.MIN_VALUE));
        this.iD.measure(MeasureSpec.makeMeasureSpec(i3, Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(i4, Integer.MIN_VALUE));
        if (size > size2) {
            int measuredWidth = this.ctaButton.getMeasuredWidth();
            int measuredWidth2 = this.hh.getMeasuredWidth();
            if (this.iE.getMeasuredWidth() + measuredWidth2 + Math.max(this.starsRatingView.getMeasuredWidth(), this.iD.getMeasuredWidth()) + measuredWidth + (this.padding * 3) > i3) {
                int measuredWidth3 = (i3 - this.iE.getMeasuredWidth()) - (this.padding * 3);
                int i5 = measuredWidth3 / 3;
                this.ctaButton.measure(MeasureSpec.makeMeasureSpec(i5, Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(i4, Integer.MIN_VALUE));
                this.starsRatingView.measure(MeasureSpec.makeMeasureSpec(i5, Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(i4, Integer.MIN_VALUE));
                this.iD.measure(MeasureSpec.makeMeasureSpec(i5, Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(i4, Integer.MIN_VALUE));
                this.hh.measure(MeasureSpec.makeMeasureSpec(((measuredWidth3 - this.ctaButton.getMeasuredWidth()) - this.iD.getMeasuredWidth()) - this.starsRatingView.getMeasuredWidth(), Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(i4, Integer.MIN_VALUE));
            }
        }
        setMeasuredDimension(size, size2);
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        int i5 = i3 - i;
        int i6 = i4 - i2;
        int measuredWidth = this.mediaAdView.getMeasuredWidth();
        int measuredHeight = this.mediaAdView.getMeasuredHeight();
        int i7 = (i5 - measuredWidth) >> 1;
        int i8 = (i6 - measuredHeight) >> 1;
        this.mediaAdView.layout(i7, i8, measuredWidth + i7, measuredHeight + i8);
        this.iC.layout(this.mediaAdView.getLeft(), this.mediaAdView.getTop(), this.mediaAdView.getRight(), this.mediaAdView.getBottom());
        int measuredWidth2 = this.iH.getMeasuredWidth();
        int i9 = i3 >> 1;
        int i10 = measuredWidth2 >> 1;
        int i11 = i4 >> 1;
        int measuredHeight2 = this.iH.getMeasuredHeight() >> 1;
        this.iH.layout(i9 - i10, i11 - measuredHeight2, i10 + i9, measuredHeight2 + i11);
        int measuredWidth3 = this.iG.getMeasuredWidth();
        int i12 = measuredWidth3 >> 1;
        int measuredHeight3 = this.iG.getMeasuredHeight() >> 1;
        this.iG.layout(i9 - i12, i11 - measuredHeight3, i12 + i9, measuredHeight3 + i11);
        int measuredWidth4 = this.iA.getMeasuredWidth();
        int i13 = measuredWidth4 >> 1;
        int measuredHeight4 = this.iA.getMeasuredHeight() >> 1;
        this.iA.layout(i9 - i13, i11 - measuredHeight4, i9 + i13, i11 + measuredHeight4);
        this.iz.layout(this.padding, this.padding, this.padding + this.iz.getMeasuredWidth(), this.padding + this.iz.getMeasuredHeight());
        if (i5 > i6) {
            int max = Math.max(this.ctaButton.getMeasuredHeight(), Math.max(this.hh.getMeasuredHeight(), this.starsRatingView.getMeasuredHeight()));
            this.ctaButton.layout((i5 - this.padding) - this.ctaButton.getMeasuredWidth(), ((i6 - this.padding) - this.ctaButton.getMeasuredHeight()) - ((max - this.ctaButton.getMeasuredHeight()) >> 1), i5 - this.padding, (i6 - this.padding) - ((max - this.ctaButton.getMeasuredHeight()) >> 1));
            this.iF.layout((this.ctaButton.getRight() - this.iF.getMeasuredWidth()) + this.iF.getPadding(), (((this.mediaAdView.getBottom() - (this.padding << 1)) - this.iF.getMeasuredHeight()) - max) + this.iF.getPadding(), this.ctaButton.getRight() + this.iF.getPadding(), ((this.mediaAdView.getBottom() - (this.padding << 1)) - max) + this.iF.getPadding());
            this.starsRatingView.layout((this.ctaButton.getLeft() - this.padding) - this.starsRatingView.getMeasuredWidth(), ((i6 - this.padding) - this.starsRatingView.getMeasuredHeight()) - ((max - this.starsRatingView.getMeasuredHeight()) >> 1), this.ctaButton.getLeft() - this.padding, (i6 - this.padding) - ((max - this.starsRatingView.getMeasuredHeight()) >> 1));
            this.iD.layout((this.ctaButton.getLeft() - this.padding) - this.iD.getMeasuredWidth(), ((i6 - this.padding) - this.iD.getMeasuredHeight()) - ((max - this.iD.getMeasuredHeight()) >> 1), this.ctaButton.getLeft() - this.padding, (i6 - this.padding) - ((max - this.iD.getMeasuredHeight()) >> 1));
            int min = Math.min(this.starsRatingView.getLeft(), this.iD.getLeft());
            this.hh.layout((min - this.padding) - this.hh.getMeasuredWidth(), ((i6 - this.padding) - this.hh.getMeasuredHeight()) - ((max - this.hh.getMeasuredHeight()) >> 1), min - this.padding, (i6 - this.padding) - ((max - this.hh.getMeasuredHeight()) >> 1));
            this.iE.layout(this.padding, ((i6 - this.padding) - this.iE.getMeasuredHeight()) - ((max - this.iE.getMeasuredHeight()) >> 1), this.padding + this.iE.getMeasuredWidth(), (i6 - this.padding) - ((max - this.iE.getMeasuredHeight()) >> 1));
            return;
        }
        this.iF.layout(((this.mediaAdView.getRight() - this.padding) - this.iF.getMeasuredWidth()) + this.iF.getPadding(), ((this.mediaAdView.getBottom() - this.padding) - this.iF.getMeasuredHeight()) + this.iF.getPadding(), (this.mediaAdView.getRight() - this.padding) + this.iF.getPadding(), (this.mediaAdView.getBottom() - this.padding) + this.iF.getPadding());
        int i14 = i5 >> 1;
        this.hh.layout(i14 - (this.hh.getMeasuredWidth() >> 1), this.mediaAdView.getBottom() + this.padding, (this.hh.getMeasuredWidth() >> 1) + i14, this.mediaAdView.getBottom() + this.padding + this.hh.getMeasuredHeight());
        this.starsRatingView.layout(i14 - (this.starsRatingView.getMeasuredWidth() >> 1), this.hh.getBottom() + this.padding, (this.starsRatingView.getMeasuredWidth() >> 1) + i14, this.hh.getBottom() + this.padding + this.starsRatingView.getMeasuredHeight());
        this.iD.layout(i14 - (this.iD.getMeasuredWidth() >> 1), this.hh.getBottom() + this.padding, (this.iD.getMeasuredWidth() >> 1) + i14, this.hh.getBottom() + this.padding + this.iD.getMeasuredHeight());
        this.ctaButton.layout(i14 - (this.ctaButton.getMeasuredWidth() >> 1), this.starsRatingView.getBottom() + this.padding, i14 + (this.ctaButton.getMeasuredWidth() >> 1), this.starsRatingView.getBottom() + this.padding + this.ctaButton.getMeasuredHeight());
        this.iE.layout(this.padding, (this.mediaAdView.getBottom() - this.padding) - this.iE.getMeasuredHeight(), this.padding + this.iE.getMeasuredWidth(), this.mediaAdView.getBottom() - this.padding);
    }

    /* access modifiers changed from: private */
    public void dz() {
        if (this.iO != 0) {
            this.iO = 0;
            this.mediaAdView.getImageView().setVisibility(8);
            this.mediaAdView.getProgressBarView().setVisibility(8);
            this.iA.setVisibility(8);
            this.iH.setVisibility(8);
            this.iG.setVisibility(8);
            this.iC.setVisibility(8);
        }
    }

    /* access modifiers changed from: private */
    public void dA() {
        if (this.iO != 2) {
            this.iO = 2;
            this.mediaAdView.getImageView().setVisibility(8);
            this.mediaAdView.getProgressBarView().setVisibility(8);
            this.iA.setVisibility(8);
            this.iH.setVisibility(8);
            this.iG.setVisibility(0);
            this.iC.setVisibility(8);
        }
    }

    private void dl() {
        setBackgroundColor(ViewCompat.MEASURED_STATE_MASK);
        int i = this.padding;
        this.iF.setId(ix);
        LayoutParams layoutParams = new LayoutParams(-2, -2);
        layoutParams.addRule(13, -1);
        this.mediaAdView.setId(MEDIA_ID);
        this.mediaAdView.setLayoutParams(layoutParams);
        this.mediaAdView.setId(iw);
        this.mediaAdView.setOnClickListener(this.iK);
        this.mediaAdView.setBackgroundColor(ViewCompat.MEASURED_STATE_MASK);
        this.iC.setBackgroundColor(-1728053248);
        this.iC.setVisibility(8);
        this.iz.setId(ip);
        this.iz.setTextSize(2, 16.0f);
        this.iz.setTransformationMethod(null);
        this.iz.setEllipsize(TruncateAt.END);
        this.iz.setMaxLines(2);
        this.iz.setPadding(i, i, i, i);
        this.iz.setTextColor(-1);
        hm.a(this.iz, -2013265920, -1, -1, this.uiUtils.E(1), this.uiUtils.E(4));
        this.hh.setId(ij);
        this.hh.setMaxLines(2);
        this.hh.setEllipsize(TruncateAt.END);
        this.hh.setTextSize(2, 18.0f);
        this.hh.setTextColor(-1);
        hm.a(this.ctaButton, -2013265920, -1, -1, this.uiUtils.E(1), this.uiUtils.E(4));
        this.ctaButton.setId(iq);
        this.ctaButton.setTextColor(-1);
        this.ctaButton.setTransformationMethod(null);
        this.ctaButton.setGravity(1);
        this.ctaButton.setTextSize(2, 16.0f);
        this.ctaButton.setMinimumWidth(this.uiUtils.E(100));
        this.ctaButton.setPadding(i, i, i, i);
        this.hh.setShadowLayer((float) this.uiUtils.E(1), (float) this.uiUtils.E(1), (float) this.uiUtils.E(1), ViewCompat.MEASURED_STATE_MASK);
        this.iD.setId(iv);
        this.iD.setTextColor(-3355444);
        this.iD.setMaxEms(10);
        this.iD.setShadowLayer((float) this.uiUtils.E(1), (float) this.uiUtils.E(1), (float) this.uiUtils.E(1), ViewCompat.MEASURED_STATE_MASK);
        this.iA.setId(ir);
        this.iA.setOnClickListener(this.iL);
        this.iA.setGravity(17);
        this.iA.setVisibility(8);
        this.iA.setPadding(this.uiUtils.E(8), 0, this.uiUtils.E(8), 0);
        this.iB.setSingleLine();
        this.iB.setEllipsize(TruncateAt.END);
        this.iB.setTypeface(this.iB.getTypeface(), 1);
        this.iB.setTextColor(-1);
        this.iB.setTextSize(2, 16.0f);
        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-2, -2);
        layoutParams2.leftMargin = this.uiUtils.E(4);
        this.iI.setPadding(this.uiUtils.E(16), this.uiUtils.E(16), this.uiUtils.E(16), this.uiUtils.E(16));
        this.iG.setId(it);
        this.iG.setOnClickListener(this.iL);
        this.iG.setVisibility(8);
        this.iG.setPadding(this.uiUtils.E(16), this.uiUtils.E(16), this.uiUtils.E(16), this.uiUtils.E(16));
        this.iH.setId(is);
        this.iH.setOnClickListener(this.iL);
        this.iH.setVisibility(8);
        this.iH.setPadding(this.uiUtils.E(16), this.uiUtils.E(16), this.uiUtils.E(16), this.uiUtils.E(16));
        this.iC.setId(iy);
        Bitmap K = fi.K(getContext());
        if (K != null) {
            this.iH.setImageBitmap(K);
        }
        Bitmap L = fi.L(getContext());
        if (L != null) {
            this.iG.setImageBitmap(L);
        }
        hm.a(this.iG, -2013265920, -1, -1, this.uiUtils.E(1), this.uiUtils.E(4));
        hm.a(this.iH, -2013265920, -1, -1, this.uiUtils.E(1), this.uiUtils.E(4));
        hm.a(this.iI, -2013265920, -1, -1, this.uiUtils.E(1), this.uiUtils.E(4));
        this.starsRatingView.setId(RATING_ID);
        this.starsRatingView.setStarSize(this.uiUtils.E(12));
        this.iE.setId(iu);
        this.iE.setVisibility(8);
        this.mediaAdView.addView(this.hc, new ViewGroup.LayoutParams(-1, -1));
        addView(this.mediaAdView);
        addView(this.iC);
        addView(this.iF);
        addView(this.iz);
        addView(this.iE);
        addView(this.iA);
        addView(this.iG);
        addView(this.iH);
        addView(this.starsRatingView);
        addView(this.iD);
        addView(this.ctaButton);
        addView(this.hh);
        this.iA.addView(this.iI);
        this.iA.addView(this.iB, layoutParams2);
        this.ctaButton.setOnClickListener(this.iL);
        this.iz.setOnClickListener(this.iL);
        this.iF.setOnClickListener(this.iL);
    }
}
