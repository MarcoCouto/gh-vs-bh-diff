package com.my.target;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.media.AudioManager.OnAudioFocusChangeListener;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView.ScaleType;
import android.widget.ProgressBar;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.my.target.common.models.ImageData;
import com.my.target.common.models.VideoData;

@SuppressLint({"ViewConstructor"})
/* compiled from: PromoMediaAdView */
public class gp extends ViewGroup {
    private boolean allowReplay = true;
    @NonNull
    private final fn hc;
    @NonNull
    private final fx imageView;
    private final boolean jB;
    @NonNull
    private final b kg;
    @NonNull
    private final FrameLayout kh;
    @NonNull
    private final ProgressBar ki;
    @Nullable
    private hq kj;
    @Nullable
    private VideoData kk;
    /* access modifiers changed from: private */
    @Nullable
    public a kl;
    private int km;
    private int kn;
    @Nullable
    private Bitmap ko;
    @NonNull
    private final fu playButton;
    @NonNull
    private final hm uiUtils;
    private final boolean useExoPlayer;

    /* compiled from: PromoMediaAdView */
    public interface a extends OnAudioFocusChangeListener, com.my.target.hq.a {
        void cN();

        void cP();

        void cQ();
    }

    /* compiled from: PromoMediaAdView */
    class b implements OnClickListener {
        private b() {
        }

        public void onClick(View view) {
            if (gp.this.kl != null) {
                if (!gp.this.isPlaying() && !gp.this.isPaused()) {
                    gp.this.kl.cN();
                } else if (!gp.this.isPaused()) {
                    gp.this.kl.cP();
                } else {
                    gp.this.kl.cQ();
                }
            }
        }
    }

    public gp(@NonNull Context context, @NonNull hm hmVar, boolean z, boolean z2) {
        super(context);
        this.uiUtils = hmVar;
        this.jB = z;
        this.useExoPlayer = z2;
        this.imageView = new fx(context);
        this.playButton = new fu(context);
        this.ki = new ProgressBar(context, null, 16842874);
        this.kh = new FrameLayout(context);
        hm.a(this.kh, 0, 868608760);
        this.hc = new fn(context);
        this.kg = new b();
    }

    @NonNull
    public FrameLayout getClickableLayout() {
        return this.kh;
    }

    @Nullable
    public hq getVideoPlayer() {
        return this.kj;
    }

    public void initView() {
        hm.a((View) this.playButton, "play_button");
        hm.a((View) this.imageView, "media_image");
        hm.a((View) this.hc, "video_texture");
        this.imageView.setScaleType(ScaleType.CENTER_INSIDE);
        this.imageView.setAdjustViewBounds(true);
        addView(this.hc);
        this.ki.setVisibility(8);
        addView(this.imageView);
        addView(this.ki);
        addView(this.playButton);
        addView(this.kh);
    }

    public void a(cm cmVar) {
        c(cmVar);
    }

    public void destroy() {
        if (this.kj != null) {
            this.kj.destroy();
        }
        this.kj = null;
    }

    public void setInterstitialPromoViewListener(@Nullable a aVar) {
        this.kl = aVar;
        if (this.kj != null) {
            this.kj.a(aVar);
        }
    }

    public void dH() {
        this.imageView.setOnClickListener(this.kg);
        this.playButton.setOnClickListener(this.kg);
        setOnClickListener(this.kg);
    }

    public boolean isPlaying() {
        return this.kj != null && this.kj.isPlaying();
    }

    public boolean isPaused() {
        return this.kj != null && this.kj.isPaused();
    }

    public void resume() {
        if (this.kj != null) {
            if (this.kk != null) {
                this.kj.resume();
                this.imageView.setVisibility(8);
            }
            this.playButton.setVisibility(8);
        }
    }

    public void pause() {
        if (this.kj != null) {
            this.kj.pause();
            this.imageView.setVisibility(0);
            Bitmap screenShot = this.hc.getScreenShot();
            if (screenShot != null && this.kj.isStarted()) {
                this.imageView.setImageBitmap(screenShot);
            }
            if (this.allowReplay) {
                this.playButton.setVisibility(0);
            }
        }
    }

    public void z(int i) {
        if (this.kj != null) {
            switch (i) {
                case 0:
                    this.kj.L();
                    return;
                case 1:
                    this.kj.K();
                    return;
                default:
                    this.kj.cJ();
                    return;
            }
        }
    }

    public void dG() {
        this.imageView.setVisibility(8);
        this.ki.setVisibility(8);
    }

    /* access modifiers changed from: 0000 */
    public void a(@NonNull cm cmVar, int i) {
        if (cmVar.getVideoBanner() != null) {
            b(cmVar, i);
        } else {
            c(cmVar);
        }
    }

    /* access modifiers changed from: 0000 */
    public void A(boolean z) {
        if (this.kj != null) {
            this.kj.stop();
        }
        this.imageView.setVisibility(0);
        this.imageView.setImageBitmap(this.ko);
        this.allowReplay = z;
        if (z) {
            this.playButton.setVisibility(0);
            return;
        }
        this.imageView.setOnClickListener(null);
        this.playButton.setOnClickListener(null);
        setOnClickListener(null);
    }

    /* access modifiers changed from: 0000 */
    public void dI() {
        this.playButton.setVisibility(8);
        this.ki.setVisibility(0);
        if (this.kk != null && this.kj != null) {
            this.kj.a(this.kl);
            this.kj.a(this.kk, this.hc);
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int mode = MeasureSpec.getMode(i);
        int size = MeasureSpec.getSize(i);
        int mode2 = MeasureSpec.getMode(i2);
        int size2 = MeasureSpec.getSize(i2);
        if (this.km == 0 || this.kn == 0) {
            super.onMeasure(MeasureSpec.makeMeasureSpec(0, 1073741824), MeasureSpec.makeMeasureSpec(0, 1073741824));
            return;
        }
        if (mode2 == 0 && size2 == 0) {
            size2 = this.km;
            size = this.kn;
            mode = Integer.MIN_VALUE;
            mode2 = Integer.MIN_VALUE;
        }
        if (size2 == 0 || mode2 == 0) {
            size2 = (int) ((((float) size) / ((float) this.kn)) * ((float) this.km));
        }
        if (size == 0 || mode == 0) {
            size = (int) ((((float) size2) / ((float) this.km)) * ((float) this.kn));
        }
        float f = ((float) this.kn) / ((float) this.km);
        float f2 = ((float) size) / f;
        float f3 = (float) size2;
        if (f2 > f3) {
            size = (int) (f * f3);
        } else {
            size2 = (int) f2;
        }
        for (int i3 = 0; i3 < getChildCount(); i3++) {
            View childAt = getChildAt(i3);
            if (childAt.getVisibility() != 8) {
                int i4 = (childAt == this.imageView || childAt == this.kh || childAt == this.hc) ? 1073741824 : Integer.MIN_VALUE;
                childAt.measure(MeasureSpec.makeMeasureSpec(size, i4), MeasureSpec.makeMeasureSpec(size2, i4));
            }
        }
        setMeasuredDimension(size, size2);
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        for (int i5 = 0; i5 < getChildCount(); i5++) {
            View childAt = getChildAt(i5);
            if (childAt.getVisibility() != 8) {
                int measuredWidth = childAt.getMeasuredWidth();
                int measuredHeight = childAt.getMeasuredHeight();
                int i6 = ((i3 - i) - measuredWidth) / 2;
                int i7 = ((i4 - i2) - measuredHeight) / 2;
                childAt.layout(i6, i7, measuredWidth + i6, measuredHeight + i7);
            }
        }
    }

    private void b(@NonNull cm cmVar, int i) {
        int i2;
        this.kh.setVisibility(8);
        cn videoBanner = cmVar.getVideoBanner();
        if (videoBanner != null) {
            this.kk = (VideoData) videoBanner.getMediaData();
            if (this.kk != null) {
                if (!this.useExoPlayer || !hh.dQ()) {
                    this.kj = hr.dW();
                } else {
                    this.kj = hs.T(getContext());
                }
                this.kj.a(this.kl);
                this.kn = this.kk.getWidth();
                this.km = this.kk.getHeight();
                ImageData preview = videoBanner.getPreview();
                if (preview != null) {
                    this.ko = preview.getData();
                    if (this.kn <= 0 || this.km <= 0) {
                        this.kn = preview.getWidth();
                        this.km = preview.getHeight();
                    }
                    this.imageView.setImageBitmap(this.ko);
                } else {
                    ImageData image = cmVar.getImage();
                    if (image != null) {
                        if (this.kn <= 0 || this.km <= 0) {
                            this.kn = image.getWidth();
                            this.km = image.getHeight();
                        }
                        this.ko = image.getData();
                        this.imageView.setImageBitmap(this.ko);
                    }
                }
                if (i != 1) {
                    ImageData playIcon = cmVar.getPlayIcon();
                    if (playIcon == null || playIcon.getData() == null) {
                        if (this.jB) {
                            i2 = this.uiUtils.E(IronSourceConstants.USING_CACHE_FOR_INIT_EVENT);
                        } else {
                            i2 = this.uiUtils.E(96);
                        }
                        this.playButton.a(fh.x(i2), false);
                    } else {
                        this.playButton.a(playIcon.getData(), true);
                    }
                }
            }
        }
    }

    private void c(@NonNull cm cmVar) {
        this.kh.setVisibility(0);
        setOnClickListener(null);
        this.playButton.setVisibility(8);
        ImageData image = cmVar.getImage();
        if (image != null && image.getData() != null) {
            this.kn = image.getWidth();
            this.km = image.getHeight();
            if (this.kn == 0 || this.km == 0) {
                this.kn = image.getData().getWidth();
                this.km = image.getData().getHeight();
            }
            this.imageView.setImageBitmap(image.getData());
            this.imageView.setClickable(false);
        }
    }
}
