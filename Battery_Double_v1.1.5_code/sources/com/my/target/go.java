package com.my.target;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.text.TextUtils.TruncateAt;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.explorestack.iab.vast.VastError;
import com.my.target.common.models.ImageData;
import com.my.target.nativeads.views.MediaAdView;
import java.util.ArrayList;

@SuppressLint({"ViewConstructor"})
/* compiled from: PanelView */
public class go extends ViewGroup {
    private static final int CTA_ID = hm.dV();
    private static final int RATING_ID = hm.dV();
    private static final int ij = hm.dV();
    private static final int iv = hm.dV();
    private static final int jG = hm.dV();
    private static final int jX = hm.dV();
    private static final int jY = hm.dV();
    @NonNull
    private final Button ctaButton;
    @NonNull
    private final fx iconImageView;
    /* access modifiers changed from: private */
    @NonNull
    public final TextView jZ;
    @NonNull
    private final fz ka;
    /* access modifiers changed from: private */
    @NonNull
    public final TextView kb;
    private final int kc;
    private final int kd;
    private final int ke;
    /* access modifiers changed from: private */
    @NonNull
    public final LinearLayout ratingLayout;
    @NonNull
    private final fy starsView;
    @NonNull
    private final hm uiUtils;
    /* access modifiers changed from: private */
    @NonNull
    public final TextView urlLabel;
    @NonNull
    private final TextView votesLabel;

    public go(@NonNull Context context, @NonNull hm hmVar) {
        super(context);
        this.uiUtils = hmVar;
        this.ctaButton = new Button(context);
        this.ctaButton.setId(CTA_ID);
        hm.a((View) this.ctaButton, "cta_button");
        this.iconImageView = new fx(context);
        this.iconImageView.setId(jG);
        hm.a((View) this.iconImageView, "icon_image");
        this.ka = new fz(context);
        this.ka.setId(ij);
        this.jZ = new TextView(context);
        this.jZ.setId(jX);
        hm.a((View) this.jZ, "description_text");
        this.kb = new TextView(context);
        hm.a((View) this.kb, "disclaimer_text");
        this.ratingLayout = new LinearLayout(context);
        this.starsView = new fy(context);
        this.starsView.setId(RATING_ID);
        hm.a((View) this.starsView, "stars_view");
        this.votesLabel = new TextView(context);
        this.votesLabel.setId(jY);
        hm.a((View) this.votesLabel, "votes_text");
        this.urlLabel = new TextView(context);
        hm.a((View) this.urlLabel, "domain_text");
        this.urlLabel.setId(iv);
        this.kc = hmVar.E(16);
        this.ke = hmVar.E(8);
        this.kd = hmVar.E(64);
    }

    public void initView() {
        setBackgroundColor(1711276032);
        this.jZ.setTextColor(-2236963);
        this.jZ.setEllipsize(TruncateAt.END);
        this.urlLabel.setTextColor(-6710887);
        this.urlLabel.setVisibility(8);
        GradientDrawable gradientDrawable = new GradientDrawable();
        gradientDrawable.setColor(0);
        gradientDrawable.setStroke(1, -3355444);
        this.kb.setPadding(this.uiUtils.E(4), this.uiUtils.E(4), this.uiUtils.E(4), this.uiUtils.E(4));
        this.kb.setBackgroundDrawable(gradientDrawable);
        this.kb.setTextSize(2, 12.0f);
        this.kb.setTextColor(-3355444);
        this.kb.setVisibility(8);
        this.ratingLayout.setOrientation(0);
        this.ratingLayout.setGravity(16);
        this.ratingLayout.setVisibility(8);
        this.votesLabel.setTextColor(-6710887);
        this.votesLabel.setGravity(16);
        this.votesLabel.setTextSize(2, 14.0f);
        this.ctaButton.setPadding(this.uiUtils.E(15), 0, this.uiUtils.E(15), 0);
        this.ctaButton.setMinimumWidth(this.uiUtils.E(100));
        this.ctaButton.setTransformationMethod(null);
        this.ctaButton.setTextSize(2, 22.0f);
        this.ctaButton.setMaxEms(10);
        this.ctaButton.setSingleLine();
        this.ctaButton.setEllipsize(TruncateAt.END);
        fp rightBorderedView = this.ka.getRightBorderedView();
        rightBorderedView.f(1, -7829368);
        rightBorderedView.setPadding(this.uiUtils.E(2), 0, 0, 0);
        rightBorderedView.setTextColor(MediaAdView.COLOR_PLACEHOLDER_GRAY);
        rightBorderedView.a(1, MediaAdView.COLOR_PLACEHOLDER_GRAY, this.uiUtils.E(3));
        rightBorderedView.setBackgroundColor(1711276032);
        this.starsView.setStarSize(this.uiUtils.E(12));
        this.ratingLayout.addView(this.starsView);
        this.ratingLayout.addView(this.votesLabel);
        this.ratingLayout.setVisibility(8);
        this.urlLabel.setVisibility(8);
        addView(this.ka);
        addView(this.ratingLayout);
        addView(this.urlLabel);
        addView(this.jZ);
        addView(this.kb);
        addView(this.iconImageView);
        addView(this.ctaButton);
    }

    public void setBanner(@NonNull cm cmVar) {
        this.ka.getLeftText().setText(cmVar.getTitle());
        this.jZ.setText(cmVar.getDescription());
        String disclaimer = cmVar.getDisclaimer();
        if (!TextUtils.isEmpty(disclaimer)) {
            this.kb.setVisibility(0);
            this.kb.setText(disclaimer);
        } else {
            this.kb.setVisibility(8);
        }
        ImageData icon = cmVar.getIcon();
        if (icon != null) {
            this.iconImageView.setVisibility(0);
            this.iconImageView.setImageData(icon);
        } else {
            this.iconImageView.setVisibility(8);
        }
        this.ctaButton.setText(cmVar.getCtaText());
        if (!"".equals(cmVar.getAgeRestrictions())) {
            this.ka.getRightBorderedView().setText(cmVar.getAgeRestrictions());
        } else {
            this.ka.getRightBorderedView().setVisibility(8);
        }
        int ctaButtonColor = cmVar.getCtaButtonColor();
        int ctaButtonTouchColor = cmVar.getCtaButtonTouchColor();
        int ctaButtonTextColor = cmVar.getCtaButtonTextColor();
        hm.a(this.ctaButton, ctaButtonColor, ctaButtonTouchColor, this.uiUtils.E(2));
        this.ctaButton.setTextColor(ctaButtonTextColor);
        if ("store".equals(cmVar.getNavigationType())) {
            if (cmVar.getVotes() == 0 || cmVar.getRating() <= 0.0f) {
                this.ratingLayout.setEnabled(false);
                this.ratingLayout.setVisibility(8);
            } else {
                this.ratingLayout.setEnabled(true);
                this.starsView.setRating(cmVar.getRating());
                this.votesLabel.setText(String.valueOf(cmVar.getVotes()));
            }
            this.urlLabel.setEnabled(false);
        } else {
            String domain = cmVar.getDomain();
            if (!TextUtils.isEmpty(domain)) {
                this.urlLabel.setEnabled(true);
                this.urlLabel.setText(domain);
            } else {
                this.urlLabel.setEnabled(false);
                this.urlLabel.setVisibility(8);
            }
            this.ratingLayout.setEnabled(false);
        }
        if (cmVar.getVideoBanner() == null || !cmVar.getVideoBanner().isAutoPlay()) {
            this.ratingLayout.setVisibility(8);
            this.urlLabel.setVisibility(8);
        }
    }

    public void a(View... viewArr) {
        if (getVisibility() == 0) {
            b(viewArr);
        }
    }

    public void a(@NonNull ca caVar, @NonNull OnClickListener onClickListener) {
        if (caVar.dn) {
            setOnClickListener(onClickListener);
            this.ctaButton.setOnClickListener(onClickListener);
            return;
        }
        if (caVar.dh) {
            this.ctaButton.setOnClickListener(onClickListener);
        } else {
            this.ctaButton.setEnabled(false);
        }
        if (caVar.dm) {
            setOnClickListener(onClickListener);
        } else {
            setOnClickListener(null);
        }
        if (caVar.db) {
            this.ka.getLeftText().setOnClickListener(onClickListener);
        } else {
            this.ka.getLeftText().setOnClickListener(null);
        }
        if (caVar.di) {
            this.ka.getRightBorderedView().setOnClickListener(onClickListener);
        } else {
            this.ka.getRightBorderedView().setOnClickListener(null);
        }
        if (caVar.dd) {
            this.iconImageView.setOnClickListener(onClickListener);
        } else {
            this.iconImageView.setOnClickListener(null);
        }
        if (caVar.dc) {
            this.jZ.setOnClickListener(onClickListener);
        } else {
            this.jZ.setOnClickListener(null);
        }
        if (caVar.df) {
            this.starsView.setOnClickListener(onClickListener);
        } else {
            this.starsView.setOnClickListener(null);
        }
        if (caVar.dg) {
            this.votesLabel.setOnClickListener(onClickListener);
        } else {
            this.votesLabel.setOnClickListener(null);
        }
        if (caVar.dk) {
            this.urlLabel.setOnClickListener(onClickListener);
        } else {
            this.urlLabel.setOnClickListener(null);
        }
    }

    private void b(View... viewArr) {
        a(0, viewArr);
    }

    /* access modifiers changed from: 0000 */
    public void c(View... viewArr) {
        if (getVisibility() == 0) {
            a((int) VastError.ERROR_CODE_GENERAL_WRAPPER, viewArr);
        }
    }

    /* access modifiers changed from: 0000 */
    public void d(View... viewArr) {
        e(viewArr);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int size = MeasureSpec.getSize(i);
        int i3 = size - (this.kc * 2);
        int size2 = (MeasureSpec.getSize(i2) / 4) - (this.ke * 2);
        int min = Math.min(size2, this.kd);
        this.iconImageView.measure(MeasureSpec.makeMeasureSpec(min, 1073741824), MeasureSpec.makeMeasureSpec(min, 1073741824));
        this.ctaButton.measure(MeasureSpec.makeMeasureSpec(i3, Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(min - (this.ke * 2), 1073741824));
        int measuredWidth = ((i3 - this.iconImageView.getMeasuredWidth()) - this.ctaButton.getMeasuredWidth()) - (this.kc * 2);
        this.ka.measure(MeasureSpec.makeMeasureSpec(measuredWidth, Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(size2, Integer.MIN_VALUE));
        this.ratingLayout.measure(MeasureSpec.makeMeasureSpec(measuredWidth, Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(size2, Integer.MIN_VALUE));
        this.urlLabel.measure(MeasureSpec.makeMeasureSpec(measuredWidth, Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(size2, Integer.MIN_VALUE));
        this.jZ.measure(MeasureSpec.makeMeasureSpec(measuredWidth, Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(size2 - this.ka.getMeasuredHeight(), Integer.MIN_VALUE));
        this.kb.measure(MeasureSpec.makeMeasureSpec(measuredWidth, Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(size2, Integer.MIN_VALUE));
        int measuredHeight = this.ka.getMeasuredHeight() + Math.max(this.jZ.getMeasuredHeight(), this.ratingLayout.getMeasuredHeight()) + (this.ke * 2);
        if (this.kb.getVisibility() == 0) {
            measuredHeight += this.kb.getMeasuredHeight();
        }
        setMeasuredDimension(size, Math.max(this.ctaButton.getMeasuredHeight(), Math.max(this.iconImageView.getMeasuredHeight(), measuredHeight)) + (this.ke * 2));
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        int measuredWidth = getMeasuredWidth();
        int measuredHeight = getMeasuredHeight();
        int measuredHeight2 = this.iconImageView.getMeasuredHeight();
        int measuredWidth2 = this.iconImageView.getMeasuredWidth();
        int i5 = (measuredHeight - measuredHeight2) / 2;
        this.iconImageView.layout(this.kc, i5, this.kc + measuredWidth2, measuredHeight2 + i5);
        int measuredWidth3 = this.ctaButton.getMeasuredWidth();
        int measuredHeight3 = this.ctaButton.getMeasuredHeight();
        int i6 = (measuredHeight - measuredHeight3) / 2;
        this.ctaButton.layout((measuredWidth - measuredWidth3) - this.kc, i6, measuredWidth - this.kc, measuredHeight3 + i6);
        int i7 = this.kc + measuredWidth2 + this.kc;
        this.ka.layout(i7, this.ke, this.ka.getMeasuredWidth() + i7, this.ke + this.ka.getMeasuredHeight());
        this.ratingLayout.layout(i7, this.ka.getBottom(), this.ratingLayout.getMeasuredWidth() + i7, this.ka.getBottom() + this.ratingLayout.getMeasuredHeight());
        this.urlLabel.layout(i7, this.ka.getBottom(), this.urlLabel.getMeasuredWidth() + i7, this.ka.getBottom() + this.urlLabel.getMeasuredHeight());
        this.jZ.layout(i7, this.ka.getBottom(), this.jZ.getMeasuredWidth() + i7, this.ka.getBottom() + this.jZ.getMeasuredHeight());
        this.kb.layout(i7, this.jZ.getBottom(), this.kb.getMeasuredWidth() + i7, this.jZ.getBottom() + this.kb.getMeasuredHeight());
    }

    private void a(int i, @NonNull View... viewArr) {
        int height = this.iconImageView.getHeight();
        int height2 = getHeight();
        int width = this.ctaButton.getWidth();
        int height3 = this.ctaButton.getHeight();
        int width2 = this.iconImageView.getWidth();
        this.iconImageView.setPivotX(0.0f);
        this.iconImageView.setPivotY(((float) height) / 2.0f);
        this.ctaButton.setPivotX((float) width);
        this.ctaButton.setPivotY(((float) height3) / 2.0f);
        float f = ((float) height2) * 0.3f;
        ArrayList arrayList = new ArrayList();
        arrayList.add(ObjectAnimator.ofFloat(this.ctaButton, View.SCALE_X, new float[]{0.7f}));
        arrayList.add(ObjectAnimator.ofFloat(this.ctaButton, View.SCALE_Y, new float[]{0.7f}));
        arrayList.add(ObjectAnimator.ofFloat(this.iconImageView, View.SCALE_X, new float[]{0.7f}));
        arrayList.add(ObjectAnimator.ofFloat(this.iconImageView, View.SCALE_Y, new float[]{0.7f}));
        arrayList.add(ObjectAnimator.ofFloat(this.jZ, View.ALPHA, new float[]{0.0f}));
        arrayList.add(ObjectAnimator.ofFloat(this.kb, View.ALPHA, new float[]{0.0f}));
        if (this.ratingLayout.isEnabled()) {
            arrayList.add(ObjectAnimator.ofFloat(this.ratingLayout, View.ALPHA, new float[]{1.0f}));
        }
        arrayList.add(ObjectAnimator.ofFloat(this, View.ALPHA, new float[]{0.6f}));
        float f2 = -(((float) width2) * 0.3f);
        arrayList.add(ObjectAnimator.ofFloat(this.ka, View.TRANSLATION_X, new float[]{f2}));
        arrayList.add(ObjectAnimator.ofFloat(this.ratingLayout, View.TRANSLATION_X, new float[]{f2}));
        arrayList.add(ObjectAnimator.ofFloat(this.urlLabel, View.TRANSLATION_X, new float[]{f2}));
        arrayList.add(ObjectAnimator.ofFloat(this.jZ, View.TRANSLATION_X, new float[]{f2}));
        arrayList.add(ObjectAnimator.ofFloat(this.kb, View.TRANSLATION_X, new float[]{f2}));
        arrayList.add(ObjectAnimator.ofFloat(this, View.TRANSLATION_Y, new float[]{f}));
        float f3 = (-f) / 2.0f;
        arrayList.add(ObjectAnimator.ofFloat(this.ctaButton, View.TRANSLATION_Y, new float[]{f3}));
        arrayList.add(ObjectAnimator.ofFloat(this.iconImageView, View.TRANSLATION_Y, new float[]{f3}));
        for (View ofFloat : viewArr) {
            arrayList.add(ObjectAnimator.ofFloat(ofFloat, View.TRANSLATION_Y, new float[]{f}));
        }
        if (this.ratingLayout.isEnabled()) {
            this.ratingLayout.setVisibility(0);
        }
        if (this.urlLabel.isEnabled()) {
            this.urlLabel.setVisibility(0);
        }
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.addListener(new AnimatorListener() {
            public void onAnimationCancel(Animator animator) {
            }

            public void onAnimationRepeat(Animator animator) {
            }

            public void onAnimationStart(Animator animator) {
            }

            public void onAnimationEnd(Animator animator) {
                go.this.kb.setVisibility(8);
                go.this.jZ.setVisibility(8);
            }
        });
        animatorSet.playTogether(arrayList);
        animatorSet.setDuration((long) i);
        animatorSet.start();
    }

    private void e(@NonNull View... viewArr) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(ObjectAnimator.ofFloat(this.ctaButton, View.SCALE_Y, new float[]{1.0f}));
        arrayList.add(ObjectAnimator.ofFloat(this.ctaButton, View.SCALE_X, new float[]{1.0f}));
        arrayList.add(ObjectAnimator.ofFloat(this.iconImageView, View.SCALE_Y, new float[]{1.0f}));
        arrayList.add(ObjectAnimator.ofFloat(this.iconImageView, View.SCALE_X, new float[]{1.0f}));
        arrayList.add(ObjectAnimator.ofFloat(this.jZ, View.ALPHA, new float[]{1.0f}));
        arrayList.add(ObjectAnimator.ofFloat(this.kb, View.ALPHA, new float[]{1.0f}));
        if (this.ratingLayout.isEnabled()) {
            arrayList.add(ObjectAnimator.ofFloat(this.ratingLayout, View.ALPHA, new float[]{0.0f}));
        }
        arrayList.add(ObjectAnimator.ofFloat(this, View.ALPHA, new float[]{1.0f}));
        arrayList.add(ObjectAnimator.ofFloat(this.ka, View.TRANSLATION_X, new float[]{0.0f}));
        arrayList.add(ObjectAnimator.ofFloat(this.ratingLayout, View.TRANSLATION_X, new float[]{0.0f}));
        arrayList.add(ObjectAnimator.ofFloat(this.urlLabel, View.TRANSLATION_X, new float[]{0.0f}));
        arrayList.add(ObjectAnimator.ofFloat(this.jZ, View.TRANSLATION_X, new float[]{0.0f}));
        arrayList.add(ObjectAnimator.ofFloat(this.kb, View.TRANSLATION_X, new float[]{0.0f}));
        arrayList.add(ObjectAnimator.ofFloat(this, View.TRANSLATION_Y, new float[]{0.0f}));
        arrayList.add(ObjectAnimator.ofFloat(this.ctaButton, View.TRANSLATION_Y, new float[]{0.0f}));
        arrayList.add(ObjectAnimator.ofFloat(this.iconImageView, View.TRANSLATION_Y, new float[]{0.0f}));
        for (View ofFloat : viewArr) {
            arrayList.add(ObjectAnimator.ofFloat(ofFloat, View.TRANSLATION_Y, new float[]{0.0f}));
        }
        if (!TextUtils.isEmpty(this.kb.getText().toString())) {
            this.kb.setVisibility(0);
        }
        this.jZ.setVisibility(0);
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(arrayList);
        animatorSet.addListener(new AnimatorListener() {
            public void onAnimationCancel(Animator animator) {
            }

            public void onAnimationRepeat(Animator animator) {
            }

            public void onAnimationStart(Animator animator) {
            }

            public void onAnimationEnd(Animator animator) {
                if (go.this.ratingLayout.isEnabled()) {
                    go.this.ratingLayout.setVisibility(8);
                }
                if (go.this.urlLabel.isEnabled()) {
                    go.this.urlLabel.setVisibility(8);
                }
            }
        });
        animatorSet.setDuration(300);
        animatorSet.start();
    }
}
