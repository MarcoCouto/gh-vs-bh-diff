package com.my.target;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Point;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.support.v7.widget.PagerSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.LayoutParams;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.text.TextUtils;
import android.view.Display;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import com.my.target.common.models.ImageData;
import java.util.List;

/* compiled from: SliderRecyclerView */
public class ha extends RecyclerView {
    @NonNull
    private final OnClickListener cardClickListener = new a();
    /* access modifiers changed from: private */
    public int displayedCardNum = -1;
    /* access modifiers changed from: private */
    @NonNull
    public final gz lE = new gz(getContext());
    /* access modifiers changed from: private */
    @Nullable
    public List<cl> lF;
    /* access modifiers changed from: private */
    @Nullable
    public c lG;
    /* access modifiers changed from: private */
    public boolean moving;
    /* access modifiers changed from: private */
    @NonNull
    public final PagerSnapHelper snapHelper;

    /* compiled from: SliderRecyclerView */
    class a implements OnClickListener {
        private a() {
        }

        public void onClick(View view) {
            if (!ha.this.moving) {
                View findContainingItemView = ha.this.lE.findContainingItemView(view);
                if (findContainingItemView != null) {
                    if (!ha.this.lE.g(findContainingItemView)) {
                        int[] calculateDistanceToFinalSnap = ha.this.snapHelper.calculateDistanceToFinalSnap(ha.this.lE, findContainingItemView);
                        if (calculateDistanceToFinalSnap != null) {
                            ha.this.smoothScrollBy(calculateDistanceToFinalSnap[0], 0);
                        }
                    } else if (!(ha.this.lG == null || ha.this.lF == null)) {
                        ha.this.lG.f((cl) ha.this.lF.get(ha.this.lE.getPosition(findContainingItemView)));
                    }
                }
            }
        }
    }

    /* compiled from: SliderRecyclerView */
    static class b extends Adapter<d> {
        private final int backgroundColor;
        @Nullable
        private OnClickListener cardClickListener;
        @NonNull
        private final List<cl> lI;
        @NonNull
        private final Resources lJ;

        b(@NonNull List<cl> list, int i, @NonNull Resources resources) {
            this.lI = list;
            this.backgroundColor = i;
            this.lJ = resources;
        }

        public int getItemViewType(int i) {
            if (i == 0) {
                return 1;
            }
            return i == this.lI.size() - 1 ? 2 : 0;
        }

        @NonNull
        /* renamed from: c */
        public d onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            gy gyVar = new gy(viewGroup.getContext(), this.backgroundColor);
            gyVar.setLayoutParams(new LayoutParams(-1, -1));
            return new d(gyVar);
        }

        /* renamed from: a */
        public void onBindViewHolder(@NonNull d dVar, int i) {
            a((cl) this.lI.get(i), dVar.dL());
            dVar.dL().setOnClickListener(this.cardClickListener);
        }

        public int getItemCount() {
            return this.lI.size();
        }

        /* renamed from: a */
        public void onViewRecycled(@NonNull d dVar) {
            dVar.dL().setOnClickListener(null);
            super.onViewRecycled(dVar);
        }

        /* access modifiers changed from: 0000 */
        public void setClickListener(@Nullable OnClickListener onClickListener) {
            this.cardClickListener = onClickListener;
        }

        private void a(@NonNull cl clVar, @NonNull gy gyVar) {
            ImageData optimalLandscapeImage = clVar.getOptimalLandscapeImage();
            ImageData optimalPortraitImage = clVar.getOptimalPortraitImage();
            ImageData imageData = this.lJ.getConfiguration().orientation == 2 ? optimalLandscapeImage : optimalPortraitImage;
            if (imageData != null) {
                optimalLandscapeImage = imageData;
            } else if (optimalLandscapeImage == null) {
                optimalLandscapeImage = optimalPortraitImage;
            }
            if (optimalLandscapeImage != null) {
                gyVar.setImage(optimalLandscapeImage);
            }
            if (!TextUtils.isEmpty(clVar.getAgeRestrictions())) {
                gyVar.setAgeRestrictions(clVar.getAgeRestrictions());
            }
        }
    }

    /* compiled from: SliderRecyclerView */
    public interface c {
        void a(int i, @NonNull cl clVar);

        void f(@NonNull cl clVar);
    }

    /* compiled from: SliderRecyclerView */
    static class d extends ViewHolder {
        @NonNull
        private final gy lK;

        d(@NonNull gy gyVar) {
            super(gyVar);
            this.lK = gyVar;
        }

        /* access modifiers changed from: 0000 */
        public gy dL() {
            return this.lK;
        }
    }

    public ha(@NonNull Context context) {
        super(context);
        setHasFixedSize(true);
        this.snapHelper = new PagerSnapHelper();
        this.snapHelper.attachToRecyclerView(this);
    }

    public void a(@NonNull List<cl> list, int i) {
        WindowManager windowManager = (WindowManager) getContext().getSystemService("window");
        Display defaultDisplay = windowManager != null ? windowManager.getDefaultDisplay() : null;
        Point point = new Point();
        if (defaultDisplay != null) {
            defaultDisplay.getSize(point);
        }
        a(list, i, point.x, point.y);
        setLayoutManager(this.lE);
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public void setBanners(@NonNull List<cl> list) {
        this.lF = list;
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public void a(@NonNull List<cl> list, int i, int i2, int i3) {
        this.lF = list;
        if (!list.isEmpty()) {
            cl clVar = (cl) list.get(0);
            if (i2 > i3) {
                ImageData optimalLandscapeImage = clVar.getOptimalLandscapeImage();
                if (optimalLandscapeImage != null) {
                    this.lE.k(optimalLandscapeImage.getWidth(), optimalLandscapeImage.getHeight());
                }
            } else {
                ImageData optimalPortraitImage = clVar.getOptimalPortraitImage();
                if (optimalPortraitImage != null) {
                    this.lE.k(optimalPortraitImage.getWidth(), optimalPortraitImage.getHeight());
                }
            }
            b bVar = new b(list, i, getResources());
            bVar.setClickListener(this.cardClickListener);
            super.setAdapter(bVar);
            if (this.lG != null) {
                this.lG.a(0, (cl) list.get(0));
            }
        }
    }

    public void setSliderCardListener(@Nullable c cVar) {
        this.lG = cVar;
    }

    public void onScrollStateChanged(int i) {
        super.onScrollStateChanged(i);
        this.moving = i != 0;
        if (!this.moving) {
            checkCardChanged();
        }
    }

    /* access modifiers changed from: protected */
    public void onConfigurationChanged(Configuration configuration) {
        if (this.lF != null && !this.lF.isEmpty()) {
            cl clVar = (cl) this.lF.get(0);
            if (configuration.orientation == 2) {
                ImageData optimalLandscapeImage = clVar.getOptimalLandscapeImage();
                if (optimalLandscapeImage != null) {
                    this.lE.k(optimalLandscapeImage.getWidth(), optimalLandscapeImage.getHeight());
                }
            } else {
                ImageData optimalPortraitImage = clVar.getOptimalPortraitImage();
                if (optimalPortraitImage != null) {
                    this.lE.k(optimalPortraitImage.getWidth(), optimalPortraitImage.getHeight());
                }
            }
        }
        super.onConfigurationChanged(configuration);
        Adapter adapter = getAdapter();
        if (adapter != null) {
            adapter.notifyDataSetChanged();
        }
        postDelayed(new Runnable() {
            public void run() {
                ha.this.lE.scrollToPositionWithOffset(ha.this.displayedCardNum, ha.this.lE.dK());
            }
        }, 100);
    }

    private void checkCardChanged() {
        int findFirstCompletelyVisibleItemPosition = this.lE.findFirstCompletelyVisibleItemPosition();
        if (findFirstCompletelyVisibleItemPosition >= 0 && this.displayedCardNum != findFirstCompletelyVisibleItemPosition) {
            this.displayedCardNum = findFirstCompletelyVisibleItemPosition;
            if (!(this.lG == null || this.lF == null)) {
                this.lG.a(this.displayedCardNum, (cl) this.lF.get(this.displayedCardNum));
            }
        }
    }
}
