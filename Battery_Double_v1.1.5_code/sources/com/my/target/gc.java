package com.my.target;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.Style;
import android.graphics.RectF;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.View.MeasureSpec;

/* compiled from: VideoProgressWheelNative */
public class gc extends View {
    @NonNull
    private final Paint iT = new Paint();
    @NonNull
    private final Paint iU = new Paint();
    @NonNull
    private final Paint iV = new Paint();
    @NonNull
    private RectF iW = new RectF();
    private long iX = 0;
    private float iY = 0.0f;
    private float iZ = 0.0f;
    private float ja = 230.0f;
    private boolean jb = false;
    private int jc;
    @NonNull
    private final hm uiUtils;

    public gc(@NonNull Context context) {
        super(context);
        this.uiUtils = hm.R(context);
    }

    public void setDigit(int i) {
        this.jc = i;
    }

    public void setProgress(float f) {
        if (this.jb) {
            this.iY = 0.0f;
            this.jb = false;
        }
        if (f > 1.0f) {
            f = 1.0f;
        } else if (f < 0.0f) {
            f = 0.0f;
        }
        if (f != this.iZ) {
            if (this.iY == this.iZ) {
                this.iX = SystemClock.uptimeMillis();
            }
            this.iZ = Math.min(f * 360.0f, 360.0f);
            invalidate();
        }
    }

    public void setMax(float f) {
        if (f > 0.0f) {
            this.ja = 360.0f / f;
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int E = this.uiUtils.E(28) + getPaddingLeft() + getPaddingRight();
        int E2 = this.uiUtils.E(28) + getPaddingTop() + getPaddingBottom();
        int mode = MeasureSpec.getMode(i);
        int size = MeasureSpec.getSize(i);
        int mode2 = MeasureSpec.getMode(i2);
        int size2 = MeasureSpec.getSize(i2);
        if (mode == 1073741824) {
            E = size;
        } else if (mode == Integer.MIN_VALUE) {
            E = Math.min(E, size);
        }
        if (mode2 == 1073741824 || mode == 1073741824) {
            E2 = size2;
        } else if (mode2 == Integer.MIN_VALUE) {
            E2 = Math.min(E2, size2);
        }
        setMeasuredDimension(E, E2);
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        i(i, i2);
        dB();
        invalidate();
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        boolean z;
        super.onDraw(canvas);
        canvas.drawOval(this.iW, this.iU);
        if (this.iY != this.iZ) {
            this.iY = Math.min(this.iY + ((((float) (SystemClock.uptimeMillis() - this.iX)) / 1000.0f) * this.ja), this.iZ);
            this.iX = SystemClock.uptimeMillis();
            z = true;
        } else {
            z = false;
        }
        canvas.drawArc(this.iW, -90.0f, isInEditMode() ? 360.0f : this.iY, false, this.iT);
        this.iV.setColor(-1);
        this.iV.setTextSize((float) this.uiUtils.E(12));
        this.iV.setTextAlign(Align.CENTER);
        this.iV.setAntiAlias(true);
        canvas.drawText(String.valueOf(this.jc), (float) ((int) this.iW.centerX()), (float) ((int) (this.iW.centerY() - ((this.iV.descent() + this.iV.ascent()) / 2.0f))), this.iV);
        if (z) {
            invalidate();
        }
    }

    /* access modifiers changed from: protected */
    public void onVisibilityChanged(@NonNull View view, int i) {
        super.onVisibilityChanged(view, i);
        if (i == 0) {
            this.iX = SystemClock.uptimeMillis();
        }
    }

    private void dB() {
        this.iT.setColor(-1);
        this.iT.setAntiAlias(true);
        this.iT.setStyle(Style.STROKE);
        this.iT.setStrokeWidth((float) this.uiUtils.E(1));
        this.iU.setColor(-2013265920);
        this.iU.setAntiAlias(true);
        this.iU.setStyle(Style.FILL);
        this.iU.setStrokeWidth((float) this.uiUtils.E(4));
    }

    private void i(int i, int i2) {
        int paddingTop = getPaddingTop();
        int paddingBottom = getPaddingBottom();
        this.iW = new RectF((float) (getPaddingLeft() + this.uiUtils.E(1)), (float) (paddingTop + this.uiUtils.E(1)), (float) ((i - getPaddingRight()) - this.uiUtils.E(1)), (float) ((i2 - paddingBottom) - this.uiUtils.E(1)));
    }
}
