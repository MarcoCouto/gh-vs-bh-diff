package com.my.target;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Pair;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.json.JSONObject;

/* compiled from: NativeAppwallAdSection */
public class db extends cu {
    @NonNull
    private final ArrayList<cq> banners = new ArrayList<>();
    private boolean cP;
    @NonNull
    private final ArrayList<Pair<String, String>> dI = new ArrayList<>();
    @Nullable
    private JSONObject dJ;
    @Nullable
    private String dK;
    @Nullable
    private String dL;
    @Nullable
    private String dM;
    @Nullable
    private String dN;
    @Nullable
    private String dO;
    @NonNull
    private final String name;
    @Nullable
    private String title;

    @NonNull
    public static db C(@NonNull String str) {
        return new db(str);
    }

    private db(@NonNull String str) {
        this.name = str;
    }

    public void c(@Nullable JSONObject jSONObject) {
        this.dJ = jSONObject;
    }

    @Nullable
    public JSONObject bS() {
        return this.dJ;
    }

    public boolean aX() {
        return this.cP;
    }

    public void n(boolean z) {
        this.cP = z;
    }

    @NonNull
    public String getName() {
        return this.name;
    }

    public void a(@NonNull cq cqVar) {
        this.banners.add(cqVar);
    }

    @NonNull
    public List<cq> bI() {
        return new ArrayList(this.banners);
    }

    public int getBannersCount() {
        return this.banners.size();
    }

    public void setTitle(@Nullable String str) {
        this.title = str;
    }

    public void D(@Nullable String str) {
        this.dK = str;
    }

    public void E(@Nullable String str) {
        this.dL = str;
    }

    public void F(@Nullable String str) {
        this.dM = str;
    }

    public void G(@Nullable String str) {
        this.dN = str;
    }

    public void H(@Nullable String str) {
        this.dO = str;
    }

    @NonNull
    public ArrayList<Pair<String, String>> bT() {
        return this.dI;
    }

    @Nullable
    public String getTitle() {
        return this.title;
    }

    @Nullable
    public String bU() {
        return this.dK;
    }

    @Nullable
    public String bV() {
        return this.dL;
    }

    @Nullable
    public String bW() {
        return this.dM;
    }

    @Nullable
    public String bX() {
        return this.dN;
    }

    @Nullable
    public String bY() {
        return this.dO;
    }

    @Nullable
    public String I(@NonNull String str) {
        Iterator it = this.dI.iterator();
        while (it.hasNext()) {
            Pair pair = (Pair) it.next();
            if (str.equals(pair.first)) {
                return (String) pair.second;
            }
        }
        return null;
    }
}
