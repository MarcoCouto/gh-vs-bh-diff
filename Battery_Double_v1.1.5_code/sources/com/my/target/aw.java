package com.my.target;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import com.my.target.ads.InterstitialSliderAd;
import com.my.target.ads.InterstitialSliderAd.InterstitialSliderAdListener;
import com.my.target.common.MyTargetActivity;
import com.my.target.common.MyTargetActivity.ActivityEngine;
import com.my.target.common.models.ImageData;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

/* compiled from: InterstitialSliderAdEngine */
public class aw implements ActivityEngine, com.my.target.fj.a {
    @Nullable
    private WeakReference<fj> X;
    @Nullable
    private WeakReference<MyTargetActivity> aT;
    private boolean ah;
    /* access modifiers changed from: private */
    @NonNull
    public final InterstitialSliderAd bd;
    @NonNull
    private final cy be;
    /* access modifiers changed from: private */
    @NonNull
    public final ArrayList<Object> bf = new ArrayList<>();
    /* access modifiers changed from: private */
    @Nullable
    public WeakReference<eu> bg;

    /* compiled from: InterstitialSliderAdEngine */
    class a implements com.my.target.eu.a {
        private a() {
        }

        public void a(@NonNull cl clVar) {
            eu euVar = aw.this.bg != null ? (eu) aw.this.bg.get() : null;
            if (euVar != null) {
                hd.dM().b(clVar, euVar.getView().getContext());
                InterstitialSliderAdListener listener = aw.this.bd.getListener();
                if (listener != null) {
                    listener.onClick(aw.this.bd);
                }
            }
        }

        public void ag() {
            aw.this.dismiss();
        }

        public void b(@NonNull cl clVar) {
            eu euVar = aw.this.bg != null ? (eu) aw.this.bg.get() : null;
            if (euVar != null) {
                Context context = euVar.getView().getContext();
                if (!aw.this.bf.contains(clVar)) {
                    aw.this.bf.add(clVar);
                    hl.a((List<dg>) clVar.getStatHolder().N("playbackStarted"), context);
                }
            }
        }
    }

    public void a(boolean z) {
    }

    public boolean onActivityBackPressed() {
        return true;
    }

    public boolean onActivityOptionsItemSelected(MenuItem menuItem) {
        return false;
    }

    public void onActivityPause() {
    }

    public void onActivityResume() {
    }

    public void onActivityStart() {
    }

    public void onActivityStop() {
    }

    @NonNull
    public static aw a(@NonNull InterstitialSliderAd interstitialSliderAd, @NonNull cy cyVar) {
        return new aw(interstitialSliderAd, cyVar);
    }

    private aw(@NonNull InterstitialSliderAd interstitialSliderAd, @NonNull cy cyVar) {
        this.bd = interstitialSliderAd;
        this.be = cyVar;
    }

    public void k(@NonNull Context context) {
        if (this.ah) {
            ah.a("Unable to open Interstitial Ad twice, please dismiss currently showing ad first");
            return;
        }
        this.ah = true;
        MyTargetActivity.activityEngine = this;
        Intent intent = new Intent(context, MyTargetActivity.class);
        if (!(context instanceof Activity)) {
            intent.addFlags(268435456);
        }
        context.startActivity(intent);
    }

    public void l(@NonNull Context context) {
        if (this.ah) {
            ah.a("Unable to open Interstitial Ad twice, please dismiss currently showing ad first");
            return;
        }
        this.ah = true;
        fj fjVar = this.X == null ? null : (fj) this.X.get();
        if (fjVar == null || !fjVar.isShowing()) {
            fj.a(this, context).show();
        } else {
            ah.c("InterstitialSliderAdEngine.showDialog: dialog already showing");
        }
    }

    public void dismiss() {
        this.ah = false;
        fj fjVar = null;
        MyTargetActivity myTargetActivity = this.aT == null ? null : (MyTargetActivity) this.aT.get();
        if (myTargetActivity != null) {
            myTargetActivity.finish();
            return;
        }
        if (this.X != null) {
            fjVar = (fj) this.X.get();
        }
        if (fjVar != null && fjVar.isShowing()) {
            fjVar.dismiss();
        }
    }

    public void destroy() {
        dismiss();
    }

    public void onActivityCreate(@NonNull MyTargetActivity myTargetActivity, @NonNull Intent intent, @NonNull FrameLayout frameLayout) {
        this.aT = new WeakReference<>(myTargetActivity);
        myTargetActivity.setTheme(16973830);
        myTargetActivity.getWindow().setFlags(1024, 1024);
        a(frameLayout);
        InterstitialSliderAdListener listener = this.bd.getListener();
        if (listener != null) {
            listener.onDisplay(this.bd);
        }
    }

    public void onActivityDestroy() {
        this.ah = false;
        ak();
        this.bg = null;
        this.aT = null;
        InterstitialSliderAdListener listener = this.bd.getListener();
        if (listener != null) {
            listener.onDismiss(this.bd);
        }
    }

    public void a(@NonNull fj fjVar, @NonNull FrameLayout frameLayout) {
        this.X = new WeakReference<>(fjVar);
        if (this.bd.isHideStatusBarInDialog()) {
            fjVar.dk();
        }
        a(frameLayout);
        InterstitialSliderAdListener listener = this.bd.getListener();
        if (listener != null) {
            listener.onDisplay(this.bd);
        }
    }

    public void C() {
        this.ah = false;
        ak();
        this.bg = null;
        this.X = null;
        InterstitialSliderAdListener listener = this.bd.getListener();
        if (listener != null) {
            listener.onDismiss(this.bd);
        }
    }

    private void ak() {
        for (cl clVar : this.be.bI()) {
            for (ImageData bitmap : clVar.getLandscapeImages()) {
                bitmap.setBitmap(null);
            }
            for (ImageData bitmap2 : clVar.getPortraitImages()) {
                bitmap2.setBitmap(null);
            }
        }
    }

    private void a(@NonNull FrameLayout frameLayout) {
        eu v = eu.v(frameLayout.getContext());
        this.bg = new WeakReference<>(v);
        v.a((com.my.target.eu.a) new a());
        v.a(this.be);
        frameLayout.addView(v.getView(), new LayoutParams(-1, -1));
    }
}
