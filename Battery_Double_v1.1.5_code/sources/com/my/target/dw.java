package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.explorestack.iab.vast.tags.VastAttributes;
import com.github.mikephil.charting.utils.Utils;
import com.my.target.common.models.ImageData;
import com.my.target.common.models.VideoData;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONObject;

/* compiled from: CommonVideoParser */
public class dw {
    @NonNull
    private final a adConfig;
    @Nullable
    private String cj;
    @NonNull
    private final Context context;
    @NonNull
    private final bz eq;
    @NonNull
    private final dv er;

    @NonNull
    public static dw c(@NonNull bz bzVar, @NonNull a aVar, @NonNull Context context2) {
        return new dw(bzVar, aVar, context2);
    }

    private dw(@NonNull bz bzVar, @NonNull a aVar, @NonNull Context context2) {
        this.eq = bzVar;
        this.adConfig = aVar;
        this.context = context2;
        this.er = dv.b(bzVar, aVar, context2);
    }

    public boolean a(@NonNull JSONObject jSONObject, @NonNull cn<VideoData> cnVar) {
        this.er.a(jSONObject, (cg) cnVar);
        if ("statistics".equals(cnVar.getType())) {
            c(jSONObject, cnVar);
            return true;
        }
        this.cj = cnVar.getId();
        float duration = cnVar.getDuration();
        if (duration <= 0.0f) {
            StringBuilder sb = new StringBuilder();
            sb.append("wrong videoBanner duration ");
            sb.append(duration);
            f("Bad value", sb.toString());
            return false;
        }
        cnVar.setCloseActionText(jSONObject.optString("closeActionText", "Close"));
        cnVar.setReplayActionText(jSONObject.optString("replayActionText", cnVar.getReplayActionText()));
        cnVar.setCloseDelayActionText(jSONObject.optString("closeDelayActionText", cnVar.getCloseDelayActionText()));
        cnVar.setAllowReplay(jSONObject.optBoolean("allowReplay", cnVar.isAllowReplay()));
        cnVar.setAutoMute(jSONObject.optBoolean("automute", cnVar.isAutoMute()));
        cnVar.setAllowClose(jSONObject.optBoolean("allowClose", cnVar.isAllowClose()));
        cnVar.setAllowCloseDelay((float) jSONObject.optDouble("allowCloseDelay", Utils.DOUBLE_EPSILON));
        cnVar.setShowPlayerControls(jSONObject.optBoolean("showPlayerControls", cnVar.isShowPlayerControls()));
        cnVar.setAutoPlay(jSONObject.optBoolean("autoplay", cnVar.isAutoPlay()));
        cnVar.setHasCtaButton(jSONObject.optBoolean("hasCtaButton", cnVar.isHasCtaButton()));
        cnVar.setAllowPause(jSONObject.optBoolean("hasPause", cnVar.isAllowPause()));
        String optString = jSONObject.optString("previewLink");
        if (!TextUtils.isEmpty(optString)) {
            cnVar.setPreview(ImageData.newImageData(optString, jSONObject.optInt("previewWidth"), jSONObject.optInt("previewHeight")));
        }
        JSONArray optJSONArray = jSONObject.optJSONArray("mediafiles");
        if (optJSONArray == null || optJSONArray.length() == 0) {
            ah.a("mediafiles array is empty");
            f("Required field", "unable to find mediaFiles in MediaBanner");
            return false;
        }
        b(jSONObject, cnVar);
        ArrayList arrayList = new ArrayList();
        int length = optJSONArray.length();
        for (int i = 0; i < length; i++) {
            JSONObject optJSONObject = optJSONArray.optJSONObject(i);
            if (optJSONObject != null) {
                VideoData e = e(optJSONObject);
                if (e != null) {
                    arrayList.add(e);
                }
            }
        }
        if (arrayList.size() > 0) {
            VideoData chooseBest = VideoData.chooseBest(arrayList, this.adConfig.getVideoQuality());
            if (chooseBest != null) {
                cnVar.setMediaData(chooseBest);
                return true;
            }
        }
        return false;
    }

    private void b(@NonNull JSONObject jSONObject, @NonNull cn<VideoData> cnVar) {
        c(jSONObject, cnVar);
        Boolean bc = this.eq.bc();
        if (bc != null) {
            cnVar.setAllowClose(bc.booleanValue());
        }
        Boolean bd = this.eq.bd();
        if (bd != null) {
            cnVar.setAllowPause(bd.booleanValue());
        }
        float allowCloseDelay = this.eq.getAllowCloseDelay();
        if (allowCloseDelay >= 0.0f) {
            cnVar.setAllowCloseDelay(allowCloseDelay);
        }
    }

    private void c(@NonNull JSONObject jSONObject, @NonNull cn<VideoData> cnVar) {
        double point = (double) this.eq.getPoint();
        if (point < Utils.DOUBLE_EPSILON) {
            point = jSONObject.optDouble("point");
        }
        if (Double.isNaN(point)) {
            point = -1.0d;
        } else if (point < Utils.DOUBLE_EPSILON) {
            StringBuilder sb = new StringBuilder();
            sb.append("Wrong value ");
            sb.append(point);
            sb.append(" for point");
            f("Bad value", sb.toString());
        }
        double pointP = (double) this.eq.getPointP();
        if (pointP < Utils.DOUBLE_EPSILON) {
            pointP = jSONObject.optDouble("pointP");
        }
        if (Double.isNaN(pointP)) {
            pointP = -1.0d;
        } else if (pointP < Utils.DOUBLE_EPSILON) {
            StringBuilder sb2 = new StringBuilder();
            sb2.append("Wrong value ");
            sb2.append(pointP);
            sb2.append(" for pointP");
            f("Bad value", sb2.toString());
        }
        if (point < Utils.DOUBLE_EPSILON && pointP < Utils.DOUBLE_EPSILON) {
            pointP = 50.0d;
            point = -1.0d;
        }
        cnVar.setPoint((float) point);
        cnVar.setPointP((float) pointP);
    }

    @Nullable
    private VideoData e(@NonNull JSONObject jSONObject) {
        String optString = jSONObject.optString("src");
        int optInt = jSONObject.optInt("width");
        int optInt2 = jSONObject.optInt("height");
        if (TextUtils.isEmpty(optString) || optInt <= 0 || optInt2 <= 0) {
            StringBuilder sb = new StringBuilder();
            sb.append("bad mediafile object, src = ");
            sb.append(optString);
            sb.append(", width = ");
            sb.append(optInt);
            sb.append(", height = ");
            sb.append(optInt2);
            f("Bad value", sb.toString());
            return null;
        }
        VideoData newVideoData = VideoData.newVideoData(optString, optInt, optInt2);
        newVideoData.setBitrate(jSONObject.optInt(VastAttributes.BITRATE));
        if (!newVideoData.getUrl().endsWith(VideoData.M3U8) || hh.dR()) {
            return newVideoData;
        }
        ah.a("HLS Video does not supported, add com.google.android.exoplayer:exoplayer-hls dependency to play HLS video ");
        return null;
    }

    private void f(String str, String str2) {
        dp.P(str).Q(str2).r(this.adConfig.getSlotId()).S(this.cj).R(this.eq.getUrl()).q(this.context);
    }
}
