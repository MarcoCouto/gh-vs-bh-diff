package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.WorkerThread;
import android.text.TextUtils;
import java.util.HashMap;
import java.util.Map;

/* compiled from: FPDataProvider */
public abstract class fb {
    @NonNull
    private final Map<String, String> map = new HashMap();

    @WorkerThread
    public abstract void collectData(@NonNull Context context);

    /* access modifiers changed from: protected */
    @NonNull
    public synchronized Map<String, String> getMap() {
        return this.map;
    }

    /* access modifiers changed from: protected */
    public synchronized boolean addParam(@NonNull String str, @Nullable String str2) {
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        if (str2 == null) {
            return removeParam(str);
        }
        this.map.put(str, str2);
        return true;
    }

    /* access modifiers changed from: protected */
    public synchronized boolean removeParam(@NonNull String str) {
        if (!this.map.containsKey(str)) {
            return false;
        }
        this.map.remove(str);
        return true;
    }

    /* access modifiers changed from: protected */
    public synchronized void removeAll() {
        this.map.clear();
    }

    /* access modifiers changed from: protected */
    @Nullable
    public synchronized String getParam(@NonNull String str) {
        return (String) this.map.get(str);
    }

    @NonNull
    public synchronized Map<String, String> getData() {
        HashMap hashMap;
        hashMap = new HashMap();
        hashMap.putAll(this.map);
        return hashMap;
    }

    public synchronized void putDataTo(@NonNull Map<String, String> map2) {
        map2.putAll(this.map);
    }
}
