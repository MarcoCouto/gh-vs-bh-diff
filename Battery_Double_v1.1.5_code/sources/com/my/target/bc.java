package com.my.target;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.LinearLayout;
import com.my.target.common.MyTargetActivity;
import com.my.target.common.MyTargetActivity.ActivityEngine;
import com.my.target.fj.a;
import com.my.target.nativeads.NativeAppwallAd;
import com.my.target.nativeads.NativeAppwallAd.AppwallAdListener;
import com.my.target.nativeads.factories.NativeAppwallViewsFactory;
import com.my.target.nativeads.views.AppwallAdView;
import java.lang.ref.WeakReference;

/* compiled from: NativeAppwallAdEngine */
public class bc implements ActivityEngine, a {
    @Nullable
    private WeakReference<fj> X;
    @Nullable
    private WeakReference<MyTargetActivity> aT;
    private boolean ah;
    @NonNull
    private final NativeAppwallAd bH;

    public void a(boolean z) {
    }

    public boolean onActivityBackPressed() {
        return true;
    }

    public void onActivityPause() {
    }

    public void onActivityResume() {
    }

    public void onActivityStart() {
    }

    public void onActivityStop() {
    }

    @NonNull
    public static bc a(@NonNull NativeAppwallAd nativeAppwallAd) {
        return new bc(nativeAppwallAd);
    }

    private bc(@NonNull NativeAppwallAd nativeAppwallAd) {
        this.bH = nativeAppwallAd;
    }

    public void k(Context context) {
        if (this.ah) {
            ah.a("Unable to open Appwall Ad twice, please dismiss currently showing ad first");
            return;
        }
        this.ah = true;
        MyTargetActivity.activityEngine = this;
        Intent intent = new Intent(context, MyTargetActivity.class);
        if (!(context instanceof Activity)) {
            intent.addFlags(268435456);
        }
        context.startActivity(intent);
    }

    public void l(Context context) {
        if (this.ah) {
            ah.a("Unable to open Appwall Ad twice, please dismiss currently showing ad first");
            return;
        }
        this.ah = true;
        fj fjVar = this.X == null ? null : (fj) this.X.get();
        if (fjVar == null || !fjVar.isShowing()) {
            fj.a(this, context).show();
        } else {
            ah.c("NativeAppwallAdEngine.showDialog: dialog already showing");
        }
    }

    public void dismiss() {
        this.ah = false;
        fj fjVar = null;
        MyTargetActivity myTargetActivity = this.aT == null ? null : (MyTargetActivity) this.aT.get();
        if (myTargetActivity != null) {
            myTargetActivity.finish();
            return;
        }
        if (this.X != null) {
            fjVar = (fj) this.X.get();
        }
        if (fjVar != null && fjVar.isShowing()) {
            fjVar.dismiss();
        }
    }

    public void destroy() {
        dismiss();
    }

    public void onActivityCreate(@NonNull MyTargetActivity myTargetActivity, @NonNull Intent intent, @NonNull FrameLayout frameLayout) {
        this.aT = new WeakReference<>(myTargetActivity);
        a(myTargetActivity);
        if (myTargetActivity.getActionBar() == null) {
            LinearLayout linearLayout = new LinearLayout(myTargetActivity);
            linearLayout.setOrientation(1);
            linearLayout.setLayoutParams(new LayoutParams(-1, -1));
            frameLayout.addView(linearLayout);
            d(linearLayout);
            e(linearLayout);
        } else {
            e(frameLayout);
        }
        AppwallAdListener listener = this.bH.getListener();
        if (listener != null) {
            listener.onDisplay(this.bH);
        }
    }

    public void onActivityDestroy() {
        this.ah = false;
        this.aT = null;
        AppwallAdListener listener = this.bH.getListener();
        if (listener != null) {
            listener.onDismiss(this.bH);
        }
    }

    public boolean onActivityOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == 16908332 && this.aT != null) {
            MyTargetActivity myTargetActivity = (MyTargetActivity) this.aT.get();
            if (myTargetActivity != null) {
                myTargetActivity.finish();
                return true;
            }
        }
        return false;
    }

    public void a(@NonNull fj fjVar, @NonNull FrameLayout frameLayout) {
        this.X = new WeakReference<>(fjVar);
        if (this.bH.isHideStatusBarInDialog()) {
            fjVar.dk();
        }
        LinearLayout linearLayout = new LinearLayout(frameLayout.getContext());
        linearLayout.setOrientation(1);
        linearLayout.setLayoutParams(new LayoutParams(-1, -1));
        frameLayout.addView(linearLayout);
        d(linearLayout);
        e(linearLayout);
        AppwallAdListener listener = this.bH.getListener();
        if (listener != null) {
            listener.onDisplay(this.bH);
        }
    }

    public void C() {
        this.ah = false;
        this.X = null;
        AppwallAdListener listener = this.bH.getListener();
        if (listener != null) {
            listener.onDismiss(this.bH);
        }
    }

    private void a(@NonNull MyTargetActivity myTargetActivity) {
        Window window = myTargetActivity.getWindow();
        if (VERSION.SDK_INT >= 21) {
            window.addFlags(Integer.MIN_VALUE);
            myTargetActivity.setTheme(16974392);
            ActionBar actionBar = myTargetActivity.getActionBar();
            if (actionBar != null) {
                actionBar.setTitle(this.bH.getTitle());
                actionBar.setIcon(17170445);
                actionBar.setDisplayShowTitleEnabled(true);
                actionBar.setDisplayHomeAsUpEnabled(true);
                actionBar.setBackgroundDrawable(new ColorDrawable(this.bH.getTitleBackgroundColor()));
                a(actionBar, this.bH.getTitleTextColor());
                actionBar.setElevation((float) hm.R(myTargetActivity).E(4));
            }
            window.setStatusBarColor(this.bH.getTitleSupplementaryColor());
        } else if (VERSION.SDK_INT >= 14) {
            myTargetActivity.setTheme(16974105);
            ActionBar actionBar2 = myTargetActivity.getActionBar();
            if (actionBar2 != null) {
                actionBar2.setTitle(this.bH.getTitle());
                actionBar2.setBackgroundDrawable(new ColorDrawable(this.bH.getTitleBackgroundColor()));
                a(actionBar2, this.bH.getTitleTextColor());
                actionBar2.setIcon(17170445);
                actionBar2.setDisplayShowTitleEnabled(true);
                actionBar2.setDisplayHomeAsUpEnabled(true);
            }
        }
    }

    private void a(@NonNull ActionBar actionBar, int i) {
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(actionBar.getTitle());
        spannableStringBuilder.setSpan(new ForegroundColorSpan(i), 0, actionBar.getTitle().length(), 18);
        actionBar.setTitle(spannableStringBuilder);
    }

    private void d(@NonNull ViewGroup viewGroup) {
        fm fmVar = new fm(viewGroup.getContext());
        fmVar.setTitle(this.bH.getTitle());
        fmVar.setStripeColor(this.bH.getTitleSupplementaryColor());
        fmVar.setMainColor(this.bH.getTitleBackgroundColor());
        fmVar.setTitleColor(this.bH.getTitleTextColor());
        fmVar.setLayoutParams(new ViewGroup.LayoutParams(-1, hm.R(viewGroup.getContext()).E(52)));
        viewGroup.addView(fmVar);
        fmVar.setOnCloseClickListener(new fm.a() {
            public void ag() {
                bc.this.dismiss();
            }
        });
    }

    private void e(@NonNull ViewGroup viewGroup) {
        AppwallAdView appwallView = NativeAppwallViewsFactory.getAppwallView(this.bH, viewGroup.getContext());
        this.bH.registerAppwallAdView(appwallView);
        appwallView.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        viewGroup.addView(appwallView);
    }
}
