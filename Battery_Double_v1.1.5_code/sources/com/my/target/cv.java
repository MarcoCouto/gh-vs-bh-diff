package com.my.target;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.my.target.common.models.VideoData;
import com.yandex.mobile.ads.video.models.vmap.AdBreak.BreakId;
import java.util.ArrayList;
import java.util.HashMap;

/* compiled from: InstreamAdSection */
public class cv extends cu {
    @NonNull
    private final HashMap<String, cz<VideoData>> dx = new HashMap<>();

    @NonNull
    public static cv by() {
        return new cv();
    }

    private cv() {
        this.dx.put(BreakId.PREROLL, cz.z(BreakId.PREROLL));
        this.dx.put(BreakId.PAUSEROLL, cz.z(BreakId.PAUSEROLL));
        this.dx.put(BreakId.MIDROLL, cz.z(BreakId.MIDROLL));
        this.dx.put("postroll", cz.z("postroll"));
    }

    @Nullable
    public cz<VideoData> x(@NonNull String str) {
        return (cz) this.dx.get(str);
    }

    @NonNull
    public ArrayList<cz<VideoData>> bz() {
        return new ArrayList<>(this.dx.values());
    }

    public int getBannersCount() {
        int i = 0;
        for (cz bannersCount : this.dx.values()) {
            i += bannersCount.getBannersCount();
        }
        return i;
    }

    public boolean bA() {
        for (cz czVar : this.dx.values()) {
            if (czVar.getBannersCount() <= 0) {
                if (czVar.bO()) {
                }
            }
            return true;
        }
        return false;
    }
}
