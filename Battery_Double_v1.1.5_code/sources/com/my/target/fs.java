package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View.MeasureSpec;
import android.widget.FrameLayout;

/* compiled from: ContainerAdView */
public class fs extends FrameLayout {
    private int hJ;
    private int hK;
    private boolean hL;
    private int maxWidth;

    public fs(@NonNull Context context) {
        super(context);
    }

    public void setFlexibleWidth(boolean z) {
        this.hL = z;
    }

    public void setMaxWidth(int i) {
        this.maxWidth = i;
    }

    public void g(int i, int i2) {
        this.hJ = i;
        this.hK = i2;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        if (this.hJ <= 0 || this.hK <= 0) {
            super.onMeasure(i, i2);
            return;
        }
        int size = MeasureSpec.getSize(i);
        if (!this.hL || this.hJ >= size) {
            super.onMeasure(MeasureSpec.makeMeasureSpec(this.hJ, Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(this.hK, 1073741824));
            return;
        }
        if (this.maxWidth > 0) {
            size = Math.min(size, this.maxWidth);
        }
        super.onMeasure(MeasureSpec.makeMeasureSpec(size, Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(this.hK, 1073741824));
    }
}
