package com.my.target;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnTouchListener;
import android.webkit.WebView;

/* compiled from: MraidWebView */
public class fw extends WebView {
    @Nullable
    private a hW;
    private boolean hX;
    /* access modifiers changed from: private */
    public boolean hY;
    private int orientation;

    /* compiled from: MraidWebView */
    public interface a {
        void aJ();

        void onVisibilityChanged(boolean z);
    }

    /* compiled from: MraidWebView */
    static class b extends GestureDetector {
        @NonNull
        private final View hs;
        @Nullable
        private a ib;

        /* compiled from: MraidWebView */
        public interface a {
            void dn();
        }

        public b(@NonNull Context context, @NonNull View view) {
            this(context, view, new SimpleOnGestureListener());
        }

        private b(@NonNull Context context, @NonNull View view, @NonNull SimpleOnGestureListener simpleOnGestureListener) {
            super(context, simpleOnGestureListener);
            this.hs = view;
            setIsLongpressEnabled(false);
        }

        public void a(@NonNull MotionEvent motionEvent) {
            switch (motionEvent.getAction()) {
                case 0:
                    onTouchEvent(motionEvent);
                    return;
                case 1:
                    if (this.ib != null) {
                        ah.a("Gestures: user clicked");
                        this.ib.dn();
                        return;
                    }
                    ah.a("View's onUserClick() is not registered.");
                    return;
                case 2:
                    if (a(motionEvent, this.hs)) {
                        onTouchEvent(motionEvent);
                        return;
                    }
                    return;
                default:
                    return;
            }
        }

        public void a(@Nullable a aVar) {
            this.ib = aVar;
        }

        private boolean a(@Nullable MotionEvent motionEvent, @Nullable View view) {
            boolean z = false;
            if (motionEvent == null || view == null) {
                return false;
            }
            float x = motionEvent.getX();
            float y = motionEvent.getY();
            if (x >= 0.0f && x <= ((float) view.getWidth()) && y >= 0.0f && y <= ((float) view.getHeight())) {
                z = true;
            }
            return z;
        }
    }

    @SuppressLint({"SetJavaScriptEnabled"})
    public fw(@NonNull Context context) {
        super(context);
        this.hX = getVisibility() == 0;
        getSettings().setJavaScriptEnabled(true);
        getSettings().setDomStorageEnabled(true);
        getSettings().setAppCacheEnabled(true);
        getSettings().setAppCachePath(getContext().getCacheDir().getAbsolutePath());
        getSettings().setAllowFileAccess(false);
        getSettings().setAllowContentAccess(false);
        if (VERSION.SDK_INT >= 16) {
            getSettings().setAllowFileAccessFromFileURLs(false);
            getSettings().setAllowUniversalAccessFromFileURLs(false);
        }
        final b bVar = new b(getContext(), this);
        bVar.a((a) new a() {
            public void dn() {
                fw.this.hY = true;
            }
        });
        setOnTouchListener(new OnTouchListener() {
            @SuppressLint({"ClickableViewAccessibility"})
            public boolean onTouch(View view, MotionEvent motionEvent) {
                bVar.a(motionEvent);
                return false;
            }
        });
    }

    public boolean ds() {
        return this.hY;
    }

    public void w(boolean z) {
        StringBuilder sb = new StringBuilder();
        sb.append("MraidWebView: pause, finishing ");
        sb.append(z);
        ah.a(sb.toString());
        if (z) {
            stopLoading();
            loadUrl("");
        }
        onPause();
    }

    public void setVisibilityChangedListener(@Nullable a aVar) {
        this.hW = aVar;
    }

    public boolean isVisible() {
        return this.hX;
    }

    @VisibleForTesting
    public void setClicked(boolean z) {
        this.hY = z;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        h(MeasureSpec.getSize(i), MeasureSpec.getSize(i2));
        super.onMeasure(i, i2);
    }

    /* access modifiers changed from: protected */
    public void onVisibilityChanged(View view, int i) {
        super.onVisibilityChanged(view, i);
        boolean z = i == 0;
        if (z != this.hX) {
            this.hX = z;
            if (this.hW != null) {
                this.hW.onVisibilityChanged(this.hX);
            }
        }
    }

    private void h(int i, int i2) {
        int i3 = ((float) i) / ((float) i2) > 1.0f ? 2 : 1;
        if (i3 != this.orientation) {
            this.orientation = i3;
            if (this.hW != null) {
                this.hW.aJ();
            }
        }
    }
}
