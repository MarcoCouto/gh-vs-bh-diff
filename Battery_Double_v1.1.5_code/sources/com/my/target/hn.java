package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import java.net.URLDecoder;

/* compiled from: UrlResolver */
public class hn {
    private static final String[] mA = {"http://play.google.com", "https://play.google.com", "http://market.android.com", "https://market.android.com", "market://", "samsungapps://"};
    /* access modifiers changed from: private */
    @Nullable
    public a mB;
    /* access modifiers changed from: private */
    @NonNull
    public final String url;

    /* compiled from: UrlResolver */
    public interface a {
        void ad(@Nullable String str);
    }

    @NonNull
    public static String decode(@NonNull String str) {
        try {
            return URLDecoder.decode(str, "UTF-8");
        } catch (Throwable th) {
            StringBuilder sb = new StringBuilder();
            sb.append("Unable to decode url ");
            sb.append(th.getMessage());
            ah.a(sb.toString());
            return str;
        }
    }

    public static boolean ak(@Nullable String str) {
        if (!TextUtils.isEmpty(str)) {
            for (String startsWith : mA) {
                if (str.startsWith(startsWith)) {
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean al(@NonNull String str) {
        return str.startsWith("samsungapps://");
    }

    public static boolean am(@Nullable String str) {
        return !TextUtils.isEmpty(str) && str.startsWith("https");
    }

    @NonNull
    public static hn an(@NonNull String str) {
        return new hn(str);
    }

    private hn(@NonNull String str) {
        this.url = str;
    }

    @NonNull
    public hn a(@Nullable a aVar) {
        this.mB = aVar;
        return this;
    }

    public void S(@NonNull Context context) {
        final Context applicationContext = context.getApplicationContext();
        ai.b(new Runnable() {
            public void run() {
                final String str = (String) dn.cu().f(hn.this.url, applicationContext);
                if (hn.this.mB != null) {
                    ai.c(new Runnable() {
                        public void run() {
                            if (hn.this.mB != null) {
                                hn.this.mB.ad(str);
                                hn.this.mB = null;
                            }
                        }
                    });
                }
            }
        });
    }
}
