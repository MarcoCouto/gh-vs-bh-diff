package com.my.target;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.webkit.ConsoleMessage;
import android.webkit.JsResult;
import android.widget.FrameLayout.LayoutParams;
import com.my.target.bu.b;
import com.my.target.ev.a;
import com.smaato.sdk.core.api.VideoType;
import java.lang.ref.WeakReference;
import java.util.ArrayList;

/* compiled from: InterstitialMraidPresenter */
public class er implements b, ev {
    @Nullable
    private ck aU;
    private boolean ca;
    @NonNull
    private final Context context;
    @Nullable
    private fw cq;
    @Nullable
    private a eK;
    @NonNull
    private final fr fc;
    @NonNull
    private final bx fd;
    @NonNull
    private final bu fe;
    @NonNull
    private final WeakReference<Activity> ff;
    @NonNull
    private String fg;
    @Nullable
    private Integer fh;
    private boolean fi;
    private bw fj;
    private boolean fk;

    private boolean b(int i, int i2) {
        return (i & i2) != 0;
    }

    @NonNull
    public static er u(@NonNull Context context2) {
        return new er(context2);
    }

    private er(@NonNull bu buVar, @NonNull Context context2) {
        this.fi = true;
        this.fj = bw.aM();
        this.fe = buVar;
        this.context = context2.getApplicationContext();
        if (context2 instanceof Activity) {
            this.ff = new WeakReference<>((Activity) context2);
        } else {
            this.ff = new WeakReference<>(null);
        }
        this.fg = "loading";
        this.fd = bx.p(context2);
        this.fc = new fr(context2);
        this.fc.setOnCloseListener(new fr.a() {
            public void onClose() {
                er.this.cT();
            }
        });
        buVar.a((b) this);
    }

    private er(@NonNull Context context2) {
        this(bu.h(VideoType.INTERSTITIAL), context2);
    }

    public void a(@NonNull cx cxVar, @NonNull ck ckVar) {
        this.aU = ckVar;
        String source = ckVar.getSource();
        if (source != null) {
            Z(source);
        }
    }

    public void a(@Nullable a aVar) {
        this.eK = aVar;
    }

    public void stop() {
        this.ca = true;
        if (this.cq != null) {
            this.cq.w(false);
        }
    }

    public void pause() {
        this.ca = true;
        if (this.cq != null) {
            this.cq.w(false);
        }
    }

    public void resume() {
        this.ca = false;
        if (this.cq != null) {
            this.cq.onResume();
        }
    }

    public void destroy() {
        if (!this.ca) {
            this.ca = true;
            if (this.cq != null) {
                this.cq.w(true);
            }
        }
        ViewParent parent = this.fc.getParent();
        if (parent instanceof ViewGroup) {
            ((ViewGroup) parent).removeView(this.fc);
        }
        this.fe.detach();
        if (this.cq != null) {
            this.cq.destroy();
            this.cq = null;
        }
        this.fc.removeAllViews();
    }

    @NonNull
    public View cI() {
        return this.fc;
    }

    public boolean a(boolean z, bw bwVar) {
        if (!a(bwVar)) {
            StringBuilder sb = new StringBuilder();
            sb.append("Unable to force orientation to ");
            sb.append(bwVar);
            this.fe.a("setOrientationProperties", sb.toString());
            return false;
        }
        this.fi = z;
        this.fj = bwVar;
        return cR();
    }

    public void c(@NonNull bu buVar) {
        this.fg = "default";
        cU();
        ArrayList arrayList = new ArrayList();
        if (cV()) {
            arrayList.add("'inlineVideo'");
        }
        arrayList.add("'vpaid'");
        buVar.a(arrayList);
        buVar.j(VideoType.INTERSTITIAL);
        buVar.k(buVar.isVisible());
        aa("default");
        buVar.aH();
        buVar.a(this.fd);
        if (this.eK != null && this.aU != null) {
            this.eK.a(this.aU, this.context);
        }
    }

    public void onVisibilityChanged(boolean z) {
        this.fe.k(z);
    }

    public boolean a(@NonNull String str, @NonNull JsResult jsResult) {
        StringBuilder sb = new StringBuilder();
        sb.append("JS Alert: ");
        sb.append(str);
        ah.a(sb.toString());
        jsResult.confirm();
        return true;
    }

    public boolean a(@NonNull ConsoleMessage consoleMessage, @NonNull bu buVar) {
        StringBuilder sb = new StringBuilder();
        sb.append("Console message: ");
        sb.append(consoleMessage.message());
        ah.a(sb.toString());
        return true;
    }

    public void onClose() {
        cT();
    }

    public void l(boolean z) {
        if (z != (!this.fc.dp())) {
            this.fc.setCloseVisible(!z);
        }
    }

    public void b(@NonNull Uri uri) {
        if (this.eK != null) {
            this.eK.b(this.aU, uri.toString(), this.fc.getContext());
        }
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public void Z(@NonNull String str) {
        this.cq = new fw(this.context);
        this.fe.a(this.cq);
        this.fc.addView(this.cq, new LayoutParams(-1, -1));
        this.fe.i(str);
    }

    public void aJ() {
        cU();
    }

    public void aK() {
        this.fk = true;
    }

    public boolean o(@NonNull String str) {
        boolean z = false;
        if (!this.fk) {
            this.fe.a("vpaidEvent", "Calling VPAID command before VPAID init");
            return false;
        }
        boolean z2 = this.eK != null;
        if (this.aU != null) {
            z = true;
        }
        if (z2 && z) {
            this.eK.a(this.aU, str, this.context);
        }
        return true;
    }

    public boolean b(float f, float f2) {
        if (!this.fk) {
            this.fe.a("playheadEvent", "Calling VPAID command before VPAID init");
            return false;
        }
        if (f >= 0.0f && f2 >= 0.0f && this.eK != null && this.aU != null) {
            this.eK.a(this.aU, f, f2, this.context);
        }
        return true;
    }

    public boolean c(@Nullable Uri uri) {
        ah.a("Expand method not used with interstitials");
        return false;
    }

    public boolean a(int i, int i2, int i3, int i4, boolean z, int i5) {
        ah.a("setResizeProperties method not used with interstitials");
        return false;
    }

    public boolean aL() {
        ah.a("resize method not used with interstitials");
        return false;
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public boolean t(int i) {
        Activity activity = (Activity) this.ff.get();
        if (activity == null || !a(this.fj)) {
            StringBuilder sb = new StringBuilder();
            sb.append("Attempted to lock orientation to unsupported value: ");
            sb.append(this.fj.toString());
            this.fe.a("setOrientationProperties", sb.toString());
            return false;
        }
        if (this.fh == null) {
            this.fh = Integer.valueOf(activity.getRequestedOrientation());
        }
        activity.setRequestedOrientation(i);
        return true;
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public boolean cR() {
        if (!"none".equals(this.fj.toString())) {
            return t(this.fj.aN());
        }
        if (this.fi) {
            cS();
            return true;
        }
        Activity activity = (Activity) this.ff.get();
        if (activity != null) {
            return t(hm.a(activity));
        }
        this.fe.a("setOrientationProperties", "Unable to set MRAID expand orientation to 'none'; expected passed in Activity Context.");
        return false;
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public void cS() {
        Activity activity = (Activity) this.ff.get();
        if (!(activity == null || this.fh == null)) {
            activity.setRequestedOrientation(this.fh.intValue());
        }
        this.fh = null;
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public boolean a(bw bwVar) {
        boolean z = true;
        if ("none".equals(bwVar.toString())) {
            return true;
        }
        Activity activity = (Activity) this.ff.get();
        if (activity == null) {
            return false;
        }
        try {
            ActivityInfo activityInfo = activity.getPackageManager().getActivityInfo(new ComponentName(activity, activity.getClass()), 0);
            int i = activityInfo.screenOrientation;
            if (i != -1) {
                if (i != bwVar.aN()) {
                    z = false;
                }
                return z;
            }
            if (!b(activityInfo.configChanges, 128) || !b(activityInfo.configChanges, 1024)) {
                z = false;
            }
            return z;
        } catch (NameNotFoundException unused) {
            return false;
        }
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public void cT() {
        if (this.cq != null && !"loading".equals(this.fg) && !"hidden".equals(this.fg)) {
            cS();
            if ("default".equals(this.fg)) {
                this.fc.setVisibility(4);
                aa("hidden");
            }
        }
    }

    private void cU() {
        DisplayMetrics displayMetrics = this.context.getResources().getDisplayMetrics();
        this.fd.a(displayMetrics.widthPixels, displayMetrics.heightPixels);
        this.fd.a(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        this.fd.b(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        this.fd.c(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
    }

    private boolean cV() {
        Activity activity = (Activity) this.ff.get();
        if (activity == null || this.cq == null) {
            return false;
        }
        return hm.a(activity, (View) this.cq);
    }

    private void aa(@NonNull String str) {
        StringBuilder sb = new StringBuilder();
        sb.append("MRAID state set to ");
        sb.append(str);
        ah.a(sb.toString());
        this.fg = str;
        this.fe.k(str);
        if ("hidden".equals(str)) {
            ah.a("InterstitialMraidPresenter: Mraid on close");
            if (this.eK != null) {
                this.eK.ag();
            }
        }
    }
}
