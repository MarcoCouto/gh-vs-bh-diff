package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.RelativeLayout.LayoutParams;
import com.my.target.ads.MyTargetView;
import com.my.target.common.MyTargetPrivacy;
import com.my.target.mediation.MediationAdapter;
import com.my.target.mediation.MediationStandardAdAdapter;
import com.my.target.mediation.MediationStandardAdAdapter.MediationStandardAdListener;
import com.my.target.mediation.MyTargetStandardAdAdapter;
import java.util.List;

/* compiled from: MediationStandardAdEngine */
public class ba extends ax<MediationStandardAdAdapter> implements ap {
    @NonNull
    private final a adConfig;
    @Nullable
    com.my.target.ap.a by;
    @NonNull
    final MyTargetView myTargetView;

    /* compiled from: MediationStandardAdEngine */
    class a implements MediationStandardAdListener {
        @NonNull
        private final ct bt;

        a(ct ctVar) {
            this.bt = ctVar;
        }

        public void onLoad(@NonNull View view, @NonNull MediationStandardAdAdapter mediationStandardAdAdapter) {
            if (ba.this.bl == mediationStandardAdAdapter) {
                StringBuilder sb = new StringBuilder();
                sb.append("MediationStandardAdEngine: data from ");
                sb.append(this.bt.getName());
                sb.append(" ad network loaded successfully");
                ah.a(sb.toString());
                ba.this.a(this.bt, true);
                ba.this.e(view);
                if (ba.this.by != null) {
                    ba.this.by.aa();
                }
            }
        }

        public void onNoAd(@NonNull String str, @NonNull MediationStandardAdAdapter mediationStandardAdAdapter) {
            if (ba.this.bl == mediationStandardAdAdapter) {
                StringBuilder sb = new StringBuilder();
                sb.append("MediationStandardAdEngine: no data from ");
                sb.append(this.bt.getName());
                sb.append(" ad network");
                ah.a(sb.toString());
                ba.this.a(this.bt, false);
            }
        }

        public void onShow(@NonNull MediationStandardAdAdapter mediationStandardAdAdapter) {
            if (ba.this.bl == mediationStandardAdAdapter) {
                Context context = ba.this.getContext();
                if (context != null) {
                    hl.a((List<dg>) this.bt.getStatHolder().N("playbackStarted"), context);
                }
                if (ba.this.by != null) {
                    ba.this.by.ab();
                }
            }
        }

        public void onClick(@NonNull MediationStandardAdAdapter mediationStandardAdAdapter) {
            if (ba.this.bl == mediationStandardAdAdapter) {
                Context context = ba.this.getContext();
                if (context != null) {
                    hl.a((List<dg>) this.bt.getStatHolder().N("click"), context);
                }
                if (ba.this.by != null) {
                    ba.this.by.onClick();
                }
            }
        }
    }

    public void pause() {
    }

    public void resume() {
    }

    public void start() {
    }

    public void stop() {
    }

    @NonNull
    public static final ba a(@NonNull MyTargetView myTargetView2, @NonNull cs csVar, @NonNull a aVar) {
        return new ba(myTargetView2, csVar, aVar);
    }

    public void a(@Nullable com.my.target.ap.a aVar) {
        this.by = aVar;
    }

    private ba(@NonNull MyTargetView myTargetView2, @NonNull cs csVar, @NonNull a aVar) {
        super(csVar);
        this.myTargetView = myTargetView2;
        this.adConfig = aVar;
    }

    public void prepare() {
        super.n(this.myTargetView.getContext());
    }

    public void destroy() {
        if (this.bl == null) {
            ah.b("MediationStandardAdEngine error: can't destroy ad, adapter is not set");
            return;
        }
        this.myTargetView.removeAllViews();
        try {
            ((MediationStandardAdAdapter) this.bl).destroy();
        } catch (Throwable th) {
            StringBuilder sb = new StringBuilder();
            sb.append("MediationStandardAdEngine error: ");
            sb.append(th.toString());
            ah.b(sb.toString());
        }
        this.bl = null;
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    /* renamed from: aq */
    public MediationStandardAdAdapter al() {
        return new MyTargetStandardAdAdapter();
    }

    /* access modifiers changed from: 0000 */
    public void a(@NonNull MediationStandardAdAdapter mediationStandardAdAdapter, @NonNull ct ctVar, @NonNull Context context) {
        a a2 = a.a(ctVar.getPlacementId(), ctVar.getPayload(), ctVar.bv(), this.adConfig.getCustomParams().getAge(), this.adConfig.getCustomParams().getGender(), MyTargetPrivacy.isConsentSpecified(), MyTargetPrivacy.isUserConsent(), MyTargetPrivacy.isUserAgeRestricted(), this.adConfig.isTrackingLocationEnabled(), this.adConfig.isTrackingEnvironmentEnabled());
        if (mediationStandardAdAdapter instanceof MyTargetStandardAdAdapter) {
            cu bw = ctVar.bw();
            if (bw instanceof dc) {
                ((MyTargetStandardAdAdapter) mediationStandardAdAdapter).setSection((dc) bw);
            }
        }
        try {
            mediationStandardAdAdapter.load(a2, this.myTargetView.getAdSize(), new a(ctVar), context);
        } catch (Throwable th) {
            StringBuilder sb = new StringBuilder();
            sb.append("MediationStandardAdEngine error: ");
            sb.append(th.toString());
            ah.b(sb.toString());
        }
    }

    /* access modifiers changed from: 0000 */
    public void am() {
        if (this.by != null) {
            this.by.e("No data for available ad networks");
        }
    }

    /* access modifiers changed from: 0000 */
    public boolean a(@NonNull MediationAdapter mediationAdapter) {
        return mediationAdapter instanceof MediationStandardAdAdapter;
    }

    /* access modifiers changed from: 0000 */
    public void e(@NonNull View view) {
        LayoutParams layoutParams = new LayoutParams(-1, -2);
        layoutParams.addRule(13);
        view.setLayoutParams(layoutParams);
        this.myTargetView.removeAllViews();
        this.myTargetView.addView(view);
    }
}
