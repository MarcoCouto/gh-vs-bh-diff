package com.my.target;

import android.support.annotation.NonNull;

/* compiled from: ResearchTimerStat */
public class ce extends dg {
    private int dp;
    private int dq;
    private int dr = 1;

    public static ce t(@NonNull String str) {
        return new ce(str);
    }

    public int bn() {
        return this.dp;
    }

    public void i(int i) {
        this.dp = i;
    }

    public int bo() {
        return this.dq;
    }

    public void j(int i) {
        this.dq = i;
    }

    public int bp() {
        return this.dr;
    }

    public void k(int i) {
        this.dr = i;
    }

    private ce(@NonNull String str) {
        super("playheadTimerValue", str);
    }
}
