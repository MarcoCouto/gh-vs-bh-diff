package com.my.target;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/* compiled from: StandardViewPresenter */
public interface ex {

    /* compiled from: StandardViewPresenter */
    public interface a {
        void a(@NonNull cg cgVar);

        void a(@NonNull cg cgVar, @Nullable String str);
    }

    void a(@Nullable a aVar);

    void destroy();

    void pause();

    void resume();

    void start();

    void stop();
}
