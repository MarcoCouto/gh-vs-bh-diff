package com.my.target;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewCompat;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import com.my.target.common.models.ImageData;
import com.my.target.common.models.VideoData;

@SuppressLint({"ViewConstructor"})
/* compiled from: DefaultView */
public class gj extends RelativeLayout implements gm {
    private static final int MEDIA_ID = hm.dV();
    private static final int jE = hm.dV();
    private static final int jF = hm.dV();
    private static final int jG = hm.dV();
    private static final int jH = hm.dV();
    @NonNull
    private final fu eH;
    /* access modifiers changed from: private */
    @Nullable
    public com.my.target.eq.a eS;
    private final int hQ;
    /* access modifiers changed from: private */
    @NonNull
    public final fu iF;
    @Nullable
    private final Bitmap iM;
    @Nullable
    private final Bitmap iN;
    @NonNull
    private final fx iconImageView;
    @NonNull
    private final a jI;
    @NonNull
    private final gq jJ;
    @NonNull
    private final gp jK;
    /* access modifiers changed from: private */
    @NonNull
    public final go jL;
    @NonNull
    private final gb jM;
    private final int jN;
    private final int jO;
    private final int jP;
    private float jQ;
    /* access modifiers changed from: private */
    @Nullable
    public com.my.target.gn.a jR;
    @NonNull
    private final hm uiUtils;

    /* compiled from: DefaultView */
    class a implements OnClickListener {
        a() {
        }

        public void onClick(View view) {
            if (view.isEnabled() && gj.this.jR != null) {
                gj.this.jR.da();
            }
        }
    }

    public void finish() {
    }

    @NonNull
    public View getView() {
        return this;
    }

    public gj(@NonNull Context context, boolean z) {
        super(context);
        boolean z2 = (getContext().getResources().getConfiguration().screenLayout & 15) >= 3;
        this.uiUtils = hm.R(context);
        this.iconImageView = new fx(context);
        this.iconImageView.setId(jG);
        this.jJ = new gq(context, this.uiUtils, z2);
        this.jJ.setId(jE);
        this.jK = new gp(context, this.uiUtils, z2, z);
        this.jK.setId(MEDIA_ID);
        this.eH = new fu(context);
        this.eH.setId(jH);
        this.jM = new gb(context);
        LayoutParams layoutParams = new LayoutParams(-1, -1);
        layoutParams.addRule(3, MEDIA_ID);
        LayoutParams layoutParams2 = new LayoutParams(-1, -2);
        layoutParams2.addRule(14, -1);
        this.jL = new go(context, this.uiUtils);
        LayoutParams layoutParams3 = new LayoutParams(-1, -2);
        layoutParams3.addRule(12, -1);
        this.jL.setLayoutParams(layoutParams3);
        this.jL.setId(jF);
        this.iF = new fu(context);
        this.iF.setId(ix);
        this.iM = fh.v(this.uiUtils.E(28));
        this.iN = fh.w(this.uiUtils.E(28));
        this.jI = new a();
        this.hQ = this.uiUtils.E(64);
        this.jN = this.uiUtils.E(20);
        hm.a((View) this.iconImageView, "icon_image");
        hm.a((View) this.iF, "sound_button");
        hm.a((View) this.jJ, "vertical_view");
        hm.a((View) this.jK, "media_view");
        hm.a((View) this.jL, "panel_view");
        hm.a((View) this.eH, "close_button");
        hm.a((View) this.jM, "progress_wheel");
        addView(this.jL, 0);
        addView(this.iconImageView, 0);
        addView(this.jJ, 0, layoutParams);
        addView(this.jK, 0, layoutParams2);
        addView(this.iF);
        addView(this.eH);
        addView(this.jM);
        this.jO = this.uiUtils.E(28);
        this.jP = this.uiUtils.E(10);
    }

    public void setBanner(@NonNull cm cmVar) {
        int i;
        int i2;
        this.jM.setVisibility(8);
        LayoutParams layoutParams = new LayoutParams(this.jO, this.uiUtils.E(28));
        layoutParams.addRule(9);
        layoutParams.topMargin = this.uiUtils.E(10);
        layoutParams.leftMargin = this.uiUtils.E(10);
        this.jM.setLayoutParams(layoutParams);
        LayoutParams layoutParams2 = new LayoutParams(-2, -2);
        layoutParams2.addRule(11);
        this.eH.setVisibility(8);
        cn videoBanner = cmVar.getVideoBanner();
        if (videoBanner == null) {
            this.iF.setVisibility(8);
        }
        this.eH.setLayoutParams(layoutParams2);
        WindowManager windowManager = (WindowManager) getContext().getSystemService("window");
        DisplayMetrics displayMetrics = new DisplayMetrics();
        if (windowManager != null) {
            windowManager.getDefaultDisplay().getMetrics(displayMetrics);
        }
        boolean z = displayMetrics.widthPixels + displayMetrics.heightPixels < 1280 || b(cmVar);
        this.jL.initView();
        this.jL.setBanner(cmVar);
        this.jJ.a(displayMetrics.widthPixels, displayMetrics.heightPixels, z);
        this.jJ.setBanner(cmVar);
        this.jK.initView();
        this.jK.a(cmVar, 0);
        ImageData closeIcon = cmVar.getCloseIcon();
        if (closeIcon == null || closeIcon.getData() == null) {
            Bitmap u = fg.u(this.uiUtils.E(28));
            if (u != null) {
                this.eH.a(u, false);
            }
        } else {
            this.eH.a(closeIcon.getData(), true);
        }
        ImageData icon = cmVar.getIcon();
        if (icon != null) {
            i2 = icon.getWidth();
            i = icon.getHeight();
        } else {
            i2 = 0;
            i = 0;
        }
        LayoutParams layoutParams3 = new LayoutParams(-2, -2);
        layoutParams3.bottomMargin = this.uiUtils.E(4);
        if (!(i2 == 0 || i == 0)) {
            int E = (int) (((float) this.uiUtils.E(64)) * (((float) i) / ((float) i2)));
            layoutParams3.width = this.hQ;
            layoutParams3.height = E;
            if (!z) {
                layoutParams3.bottomMargin = (-E) / 2;
            }
        }
        layoutParams3.addRule(8, MEDIA_ID);
        if (VERSION.SDK_INT >= 17) {
            layoutParams3.setMarginStart(this.uiUtils.E(20));
        } else {
            layoutParams3.leftMargin = this.uiUtils.E(20);
        }
        this.iconImageView.setLayoutParams(layoutParams3);
        if (icon != null) {
            this.iconImageView.setImageBitmap(icon.getData());
        }
        if (videoBanner != null && videoBanner.isAutoPlay()) {
            this.jK.dI();
            post(new Runnable() {
                public void run() {
                    gj.this.jL.a(gj.this.iF);
                }
            });
        }
        if (videoBanner != null) {
            this.jQ = videoBanner.getDuration();
        }
        fu fuVar = this.iF;
        fuVar.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                if (gj.this.eS != null) {
                    gj.this.eS.cO();
                }
            }
        });
        fuVar.a(this.iM, false);
        fuVar.setContentDescription("sound_on");
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x002b A[ADDED_TO_REGION] */
    private boolean b(@NonNull cm cmVar) {
        int i;
        int i2;
        cn videoBanner = cmVar.getVideoBanner();
        boolean z = false;
        if (videoBanner != null) {
            VideoData videoData = (VideoData) videoBanner.getMediaData();
            if (videoData != null) {
                i2 = videoData.getHeight();
                i = videoData.getWidth();
                if (i2 > 0 || i <= 0) {
                    return false;
                }
                if (i2 > i || ((float) i) / ((float) i2) < 1.4f) {
                    z = true;
                }
                return z;
            }
        } else {
            ImageData image = cmVar.getImage();
            if (image != null) {
                i2 = image.getHeight();
                i = image.getWidth();
                if (i2 > 0) {
                }
                return false;
            }
        }
        i = 0;
        i2 = 0;
        if (i2 > 0) {
        }
        return false;
    }

    public void destroy() {
        this.jK.destroy();
    }

    public void a(@NonNull cm cmVar) {
        this.iF.setVisibility(8);
        this.eH.setVisibility(0);
        stop(false);
        this.jK.a(cmVar);
    }

    public boolean isPaused() {
        return this.jK.isPaused();
    }

    @NonNull
    public View getCloseButton() {
        return this.eH;
    }

    public void play() {
        this.jL.c(this.iF);
        this.jK.dI();
    }

    public void resume() {
        this.jL.c(this.iF);
        this.jK.resume();
    }

    @NonNull
    public gp getPromoMediaView() {
        return this.jK;
    }

    public void stop(boolean z) {
        this.jM.setVisibility(8);
        this.jL.d(this.iF);
        this.jK.A(z);
    }

    public void setClickArea(@NonNull ca caVar) {
        StringBuilder sb = new StringBuilder();
        sb.append("Apply click area ");
        sb.append(caVar.bk());
        sb.append(" to view");
        ah.a(sb.toString());
        if (caVar.dd || caVar.dn) {
            this.iconImageView.setOnClickListener(this.jI);
        } else {
            this.iconImageView.setOnClickListener(null);
        }
        this.jJ.a(caVar, this.jI);
        this.jL.a(caVar, (OnClickListener) this.jI);
        if (caVar.de || caVar.dn) {
            this.jK.getClickableLayout().setOnClickListener(new OnClickListener() {
                public void onClick(View view) {
                    if (gj.this.jR != null) {
                        gj.this.jR.da();
                    }
                }
            });
            return;
        }
        this.jK.getClickableLayout().setOnClickListener(null);
        this.jK.getClickableLayout().setEnabled(false);
    }

    public void setMediaListener(com.my.target.eq.a aVar) {
        this.eS = aVar;
        this.jK.setInterstitialPromoViewListener(aVar);
        this.jK.dH();
    }

    public void dF() {
        this.eH.setVisibility(0);
    }

    public void setTimeChanged(float f) {
        this.jM.setVisibility(0);
        if (this.jQ > 0.0f) {
            this.jM.setProgress(f / this.jQ);
        }
        this.jM.setDigit((int) ((this.jQ - f) + 1.0f));
    }

    public boolean isPlaying() {
        return this.jK.isPlaying();
    }

    public void pause() {
        this.jL.d(this.iF);
        this.jK.pause();
    }

    public void z(int i) {
        this.jK.z(i);
    }

    public void dG() {
        this.jK.dG();
    }

    public final void z(boolean z) {
        if (z) {
            this.iF.a(this.iN, false);
            this.iF.setContentDescription("sound_off");
            return;
        }
        this.iF.a(this.iM, false);
        this.iF.setContentDescription("sound_on");
    }

    public void setInterstitialPromoViewListener(@Nullable com.my.target.gn.a aVar) {
        this.jR = aVar;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int size = MeasureSpec.getSize(i);
        int size2 = MeasureSpec.getSize(i2);
        this.iF.measure(i, i2);
        this.eH.measure(i, i2);
        this.jM.measure(MeasureSpec.makeMeasureSpec(this.jO, 1073741824), MeasureSpec.makeMeasureSpec(this.jO, 1073741824));
        if (size2 > size) {
            this.jK.measure(MeasureSpec.makeMeasureSpec(size, Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(size2, Integer.MIN_VALUE));
            this.jJ.measure(MeasureSpec.makeMeasureSpec(size, 1073741824), MeasureSpec.makeMeasureSpec(size2 - this.jK.getMeasuredHeight(), Integer.MIN_VALUE));
            this.iconImageView.measure(MeasureSpec.makeMeasureSpec(this.hQ, Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(size2, Integer.MIN_VALUE));
        } else {
            this.jK.measure(MeasureSpec.makeMeasureSpec(size, Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(size2, Integer.MIN_VALUE));
            this.jL.measure(MeasureSpec.makeMeasureSpec(size, 1073741824), MeasureSpec.makeMeasureSpec(size2, Integer.MIN_VALUE));
        }
        setMeasuredDimension(i, i2);
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        this.eH.layout(i3 - this.eH.getMeasuredWidth(), 0, i3, this.eH.getMeasuredHeight());
        this.jM.layout(this.jP, this.jP, this.jM.getMeasuredWidth() + this.jP, this.jM.getMeasuredHeight() + this.jP);
        if (i4 > i3) {
            if (this.iF.getTranslationY() > 0.0f) {
                this.iF.setTranslationY(0.0f);
            }
            setBackgroundColor(-1);
            int measuredWidth = (i3 - this.jK.getMeasuredWidth()) / 2;
            this.jK.layout(measuredWidth, 0, this.jK.getMeasuredWidth() + measuredWidth, this.jK.getMeasuredHeight());
            this.jJ.layout(0, this.jK.getBottom(), i3, i4);
            int i5 = this.jN;
            if (this.jK.getMeasuredHeight() != 0) {
                i5 = this.jK.getBottom() - (this.iconImageView.getMeasuredHeight() / 2);
            }
            this.iconImageView.layout(this.jN, i5, this.jN + this.iconImageView.getMeasuredWidth(), this.iconImageView.getMeasuredHeight() + i5);
            this.jL.layout(0, 0, 0, 0);
            this.iF.layout(i3 - this.iF.getMeasuredWidth(), this.jK.getBottom() - this.iF.getMeasuredHeight(), i3, this.jK.getBottom());
            return;
        }
        setBackgroundColor(ViewCompat.MEASURED_STATE_MASK);
        int measuredWidth2 = (i3 - this.jK.getMeasuredWidth()) / 2;
        int measuredHeight = (i4 - this.jK.getMeasuredHeight()) / 2;
        this.jK.layout(measuredWidth2, measuredHeight, this.jK.getMeasuredWidth() + measuredWidth2, this.jK.getMeasuredHeight() + measuredHeight);
        this.iconImageView.layout(0, 0, 0, 0);
        this.jJ.layout(0, 0, 0, 0);
        this.jL.layout(0, i4 - this.jL.getMeasuredHeight(), i3, i4);
        this.iF.layout(i3 - this.iF.getMeasuredWidth(), this.jL.getTop() - this.iF.getMeasuredHeight(), i3, this.jL.getTop());
        if (this.jK.isPlaying()) {
            this.jL.a(this.iF);
        }
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        boolean z = this.jQ <= 0.0f || isHardwareAccelerated();
        if (this.jR != null) {
            this.jR.s(z);
        }
    }
}
