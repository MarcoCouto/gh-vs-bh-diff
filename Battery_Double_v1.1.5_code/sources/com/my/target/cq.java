package com.my.target;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.my.target.common.models.ImageData;

/* compiled from: NativeAppwallAdBanner */
public class cq extends cg {
    private boolean appInstalled;
    @Nullable
    private ImageData bubbleIcon;
    @Nullable
    private String bubbleId;
    private int coins;
    @Nullable
    private ImageData coinsIcon;
    private int coinsIconBgColor = -552418;
    private int coinsIconTextColor = -1;
    @Nullable
    private ImageData crossNotifIcon;
    @Nullable
    private ImageData gotoAppIcon;
    private boolean hasNotification;
    private boolean isBanner;
    private boolean isItemHighlight;
    private boolean isMain;
    private boolean isRequireCategoryHighlight;
    private boolean isRequireWifi;
    private boolean isSubItem;
    @Nullable
    private ImageData itemHighlightIcon;
    @Nullable
    private ImageData labelIcon;
    @Nullable
    private String labelType;
    private int mrgsId;
    @Nullable
    private String paidType;
    @Nullable
    private String status;
    @Nullable
    private ImageData statusIcon;

    @NonNull
    public static cq newBanner() {
        return new cq();
    }

    private cq() {
    }

    public void setAppInstalled(boolean z) {
        this.appInstalled = z;
    }

    public boolean isAppInstalled() {
        return this.appInstalled;
    }

    public void setHasNotification(boolean z) {
        this.hasNotification = z;
    }

    public boolean isHasNotification() {
        return this.hasNotification;
    }

    @Nullable
    public ImageData getCoinsIcon() {
        return this.coinsIcon;
    }

    public void setCoinsIcon(@Nullable ImageData imageData) {
        this.coinsIcon = imageData;
    }

    @Nullable
    public ImageData getLabelIcon() {
        return this.labelIcon;
    }

    public void setLabelIcon(@Nullable ImageData imageData) {
        this.labelIcon = imageData;
    }

    @Nullable
    public ImageData getGotoAppIcon() {
        return this.gotoAppIcon;
    }

    public void setGotoAppIcon(@Nullable ImageData imageData) {
        this.gotoAppIcon = imageData;
    }

    @Nullable
    public ImageData getStatusIcon() {
        return this.statusIcon;
    }

    public void setStatusIcon(@Nullable ImageData imageData) {
        this.statusIcon = imageData;
    }

    @Nullable
    public ImageData getBubbleIcon() {
        return this.bubbleIcon;
    }

    public void setBubbleIcon(@Nullable ImageData imageData) {
        this.bubbleIcon = imageData;
    }

    @Nullable
    public ImageData getItemHighlightIcon() {
        return this.itemHighlightIcon;
    }

    public void setItemHighlightIcon(@Nullable ImageData imageData) {
        this.itemHighlightIcon = imageData;
    }

    @Nullable
    public ImageData getCrossNotifIcon() {
        return this.crossNotifIcon;
    }

    public void setCrossNotifIcon(@Nullable ImageData imageData) {
        this.crossNotifIcon = imageData;
    }

    @Nullable
    public String getBubbleId() {
        return this.bubbleId;
    }

    public void setBubbleId(@Nullable String str) {
        this.bubbleId = str;
    }

    @Nullable
    public String getLabelType() {
        return this.labelType;
    }

    public void setLabelType(@Nullable String str) {
        this.labelType = str;
    }

    @Nullable
    public String getStatus() {
        return this.status;
    }

    public void setStatus(@Nullable String str) {
        this.status = str;
    }

    @Nullable
    public String getPaidType() {
        return this.paidType;
    }

    public void setPaidType(@Nullable String str) {
        this.paidType = str;
    }

    public int getMrgsId() {
        return this.mrgsId;
    }

    public void setMrgsId(int i) {
        this.mrgsId = i;
    }

    public int getCoins() {
        return this.coins;
    }

    public void setCoins(int i) {
        this.coins = i;
    }

    public int getCoinsIconBgColor() {
        return this.coinsIconBgColor;
    }

    public void setCoinsIconBgColor(int i) {
        this.coinsIconBgColor = i;
    }

    public int getCoinsIconTextColor() {
        return this.coinsIconTextColor;
    }

    public void setCoinsIconTextColor(int i) {
        this.coinsIconTextColor = i;
    }

    public boolean isMain() {
        return this.isMain;
    }

    public void setMain(boolean z) {
        this.isMain = z;
    }

    public boolean isRequireCategoryHighlight() {
        return this.isRequireCategoryHighlight;
    }

    public void setRequireCategoryHighlight(boolean z) {
        this.isRequireCategoryHighlight = z;
    }

    public boolean isItemHighlight() {
        return this.isItemHighlight;
    }

    public void setItemHighlight(boolean z) {
        this.isItemHighlight = z;
    }

    public boolean isBanner() {
        return this.isBanner;
    }

    public void setBanner(boolean z) {
        this.isBanner = z;
    }

    public boolean isRequireWifi() {
        return this.isRequireWifi;
    }

    public void setRequireWifi(boolean z) {
        this.isRequireWifi = z;
    }

    public boolean isSubItem() {
        return this.isSubItem;
    }

    public void setSubItem(boolean z) {
        this.isSubItem = z;
    }
}
