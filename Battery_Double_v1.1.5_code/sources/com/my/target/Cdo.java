package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.io.File;
import java.net.HttpURLConnection;
import java.net.URL;

/* renamed from: com.my.target.do reason: invalid class name */
/* compiled from: HttpVideoRequest */
public final class Cdo extends dm<String> {
    @NonNull
    public static Cdo cv() {
        return new Cdo();
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x00b8  */
    @Nullable
    /* renamed from: b */
    public String c(@NonNull String str, @NonNull Context context) {
        HttpURLConnection httpURLConnection;
        he N = he.N(context);
        if (N != null) {
            this.ed = N.af(str);
            if (this.ed != null) {
                this.cP = true;
                return (String) this.ed;
            }
            try {
                StringBuilder sb = new StringBuilder();
                sb.append("send video request: ");
                sb.append(str);
                ah.a(sb.toString());
                httpURLConnection = (HttpURLConnection) new URL(str).openConnection();
                try {
                    httpURLConnection.setReadTimeout(10000);
                    httpURLConnection.setConnectTimeout(10000);
                    httpURLConnection.setInstanceFollowRedirects(true);
                    httpURLConnection.setRequestProperty("connection", "close");
                    httpURLConnection.connect();
                    this.responseCode = httpURLConnection.getResponseCode();
                    if (this.responseCode == 200) {
                        File b = N.b(httpURLConnection.getInputStream(), str);
                        if (b != null) {
                            this.ed = b.getAbsolutePath();
                        } else {
                            this.ec = false;
                            this.c = "video request error: can't save video to disk cache";
                            ah.a(this.c);
                        }
                    } else {
                        this.ec = false;
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append("video request error: response code ");
                        sb2.append(this.responseCode);
                        this.c = sb2.toString();
                        ah.a(this.c);
                    }
                } catch (Throwable th) {
                    th = th;
                    this.ec = false;
                    this.c = th.getMessage();
                    StringBuilder sb3 = new StringBuilder();
                    sb3.append("video request error: ");
                    sb3.append(this.c);
                    ah.a(sb3.toString());
                    if (httpURLConnection != null) {
                    }
                    return (String) this.ed;
                }
            } catch (Throwable th2) {
                th = th2;
                httpURLConnection = null;
                this.ec = false;
                this.c = th.getMessage();
                StringBuilder sb32 = new StringBuilder();
                sb32.append("video request error: ");
                sb32.append(this.c);
                ah.a(sb32.toString());
                if (httpURLConnection != null) {
                }
                return (String) this.ed;
            }
            if (httpURLConnection != null) {
                httpURLConnection.disconnect();
            }
            return (String) this.ed;
        }
        StringBuilder sb4 = new StringBuilder();
        sb4.append("unable to open disk cache and load/save video ");
        sb4.append(str);
        ah.a(sb4.toString());
        this.ec = false;
        return null;
    }
}
