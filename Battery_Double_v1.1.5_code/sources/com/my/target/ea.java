package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.my.target.common.models.ImageData;
import org.json.JSONArray;
import org.json.JSONObject;

/* compiled from: InterstitialAdImageBannerParser */
public class ea {
    @NonNull
    private final a adConfig;
    @NonNull
    private final Context context;
    @NonNull
    private final bz eq;

    @NonNull
    public static ea e(@NonNull bz bzVar, @NonNull a aVar, @NonNull Context context2) {
        return new ea(bzVar, aVar, context2);
    }

    private ea(@NonNull bz bzVar, @NonNull a aVar, @NonNull Context context2) {
        this.eq = bzVar;
        this.adConfig = aVar;
        this.context = context2;
    }

    public boolean b(@NonNull JSONObject jSONObject, @NonNull cl clVar) {
        JSONArray optJSONArray = jSONObject.optJSONArray("portrait");
        JSONArray optJSONArray2 = jSONObject.optJSONArray("landscape");
        boolean z = false;
        if ((optJSONArray == null || optJSONArray.length() <= 0) && (optJSONArray2 == null || optJSONArray2.length() <= 0)) {
            b("No images in InterstitialAdImageBanner", "Required field", clVar.getId());
            return false;
        }
        if (optJSONArray != null) {
            int length = optJSONArray.length();
            for (int i = 0; i < length; i++) {
                JSONObject optJSONObject = optJSONArray.optJSONObject(i);
                if (optJSONObject != null) {
                    ImageData b = b(optJSONObject, clVar.getId());
                    if (b != null) {
                        clVar.addPortraitImage(b);
                    }
                }
            }
        }
        if (optJSONArray2 != null) {
            int length2 = optJSONArray2.length();
            for (int i2 = 0; i2 < length2; i2++) {
                JSONObject optJSONObject2 = optJSONArray2.optJSONObject(i2);
                if (optJSONObject2 != null) {
                    ImageData b2 = b(optJSONObject2, clVar.getId());
                    if (b2 != null) {
                        clVar.addLandscapeImage(b2);
                    }
                }
            }
        }
        if (!clVar.getLandscapeImages().isEmpty() || !clVar.getPortraitImages().isEmpty()) {
            z = true;
        }
        return z;
    }

    @Nullable
    private ImageData b(@NonNull JSONObject jSONObject, @NonNull String str) {
        String optString = jSONObject.optString("imageLink");
        if (TextUtils.isEmpty(optString)) {
            b("InterstitialAdImageBanner no imageLink for image", "Required field", str);
            return null;
        }
        int optInt = jSONObject.optInt("width");
        int optInt2 = jSONObject.optInt("height");
        if (optInt > 0 && optInt2 > 0) {
            return ImageData.newImageData(optString, optInt, optInt2);
        }
        StringBuilder sb = new StringBuilder();
        sb.append("InterstitialAdImageBanner  image has wrong dimensions, w = ");
        sb.append(optInt);
        sb.append(", h = ");
        sb.append(optInt2);
        b(sb.toString(), "Required field", str);
        return null;
    }

    private void b(@NonNull String str, @NonNull String str2, @NonNull String str3) {
        dp.P(str2).Q(str).S(str3).r(this.adConfig.getSlotId()).R(this.eq.getUrl()).q(this.context);
    }
}
