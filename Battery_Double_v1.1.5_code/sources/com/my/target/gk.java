package com.my.target;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import com.my.target.common.models.ImageData;

@SuppressLint({"ViewConstructor"})
/* compiled from: FooterView */
public class gk extends RelativeLayout {
    private final boolean jB;
    @NonNull
    private final RelativeLayout jT;
    @NonNull
    private final ImageView jU;
    @NonNull
    private final ImageView jV;
    @NonNull
    private final OnClickListener jW;
    @NonNull
    private final hm uiUtils;

    /* compiled from: FooterView */
    static class a implements OnClickListener {
        @NonNull
        private final Context context;

        private a(@NonNull Context context2) {
            this.context = context2;
        }

        public void onClick(View view) {
            try {
                Intent intent = new Intent("android.intent.action.VIEW", Uri.parse("https://target.my.com/"));
                if (!(this.context instanceof Activity)) {
                    intent.addFlags(268435456);
                }
                this.context.startActivity(intent);
            } catch (Throwable th) {
                ah.a(th.getMessage());
            }
        }
    }

    public gk(@NonNull Context context, @NonNull hm hmVar, boolean z) {
        super(context);
        this.jT = new RelativeLayout(context);
        this.jU = new ImageView(context);
        hm.a((View) this.jU, "logo_image");
        this.jV = new ImageView(context);
        hm.a((View) this.jV, "store_image");
        this.uiUtils = hmVar;
        this.jB = z;
        this.jW = new a(context);
    }

    public void setBanner(cm cmVar) {
        ImageData storeIcon = cmVar.getStoreIcon();
        if ("store".equals(cmVar.getNavigationType()) && storeIcon != null && storeIcon.getData() != null) {
            this.jV.setImageBitmap(storeIcon.getData());
        }
    }

    /* access modifiers changed from: 0000 */
    public void initView() {
        LayoutParams layoutParams = new LayoutParams(-1, -2);
        layoutParams.addRule(12, -1);
        this.jT.setLayoutParams(layoutParams);
        this.jU.setImageBitmap(fh.I(getContext()));
        this.jT.addView(this.jU);
        this.jT.addView(this.jV);
        addView(this.jT);
    }

    /* access modifiers changed from: 0000 */
    public void a(int i, boolean z) {
        int i2 = i / 3;
        if (this.jB) {
            i2 = i / 5;
        }
        LayoutParams layoutParams = new LayoutParams(-2, i2);
        if (z) {
            layoutParams.setMargins(this.uiUtils.E(24), this.uiUtils.E(4), this.uiUtils.E(24), this.uiUtils.E(8));
        } else {
            layoutParams.setMargins(this.uiUtils.E(24), this.uiUtils.E(16), this.uiUtils.E(24), this.uiUtils.E(16));
        }
        layoutParams.addRule(15, -1);
        if (VERSION.SDK_INT >= 17) {
            layoutParams.addRule(20);
        } else {
            layoutParams.addRule(9);
        }
        this.jV.setScaleType(ScaleType.FIT_START);
        this.jV.setLayoutParams(layoutParams);
        LayoutParams layoutParams2 = new LayoutParams(-2, i2);
        if (z) {
            layoutParams2.setMargins(this.uiUtils.E(8), this.uiUtils.E(4), this.uiUtils.E(8), this.uiUtils.E(8));
        } else {
            layoutParams2.setMargins(this.uiUtils.E(24), this.uiUtils.E(16), this.uiUtils.E(24), this.uiUtils.E(16));
        }
        layoutParams2.addRule(15, -1);
        if (VERSION.SDK_INT >= 17) {
            layoutParams2.addRule(21);
        } else {
            layoutParams2.addRule(11);
        }
        this.jU.setScaleType(ScaleType.FIT_CENTER);
        this.jU.setLayoutParams(layoutParams2);
        this.jU.setOnClickListener(this.jW);
    }
}
