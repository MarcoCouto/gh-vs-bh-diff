package com.my.target;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.support.annotation.NonNull;
import java.net.CookieStore;
import java.net.HttpCookie;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

@TargetApi(9)
/* compiled from: MyTargetCookieStore */
class dr implements CookieStore {
    @NonNull
    private final SharedPreferences em;
    @NonNull
    private final Map<URI, Set<HttpCookie>> en = new HashMap();

    @NonNull
    private static URI a(@NonNull URI uri, @NonNull HttpCookie httpCookie) {
        String str;
        if (httpCookie.getDomain() == null) {
            return uri;
        }
        String domain = httpCookie.getDomain();
        if (domain.charAt(0) == '.') {
            domain = domain.substring(1);
        }
        try {
            if (uri.getScheme() == null) {
                str = "http";
            } else {
                str = uri.getScheme();
            }
            return new URI(str, domain, httpCookie.getPath() == null ? "/" : httpCookie.getPath(), null);
        } catch (URISyntaxException e) {
            ah.a(e.getMessage());
            return uri;
        }
    }

    dr(@NonNull Context context) {
        this.em = context.getSharedPreferences("mytarget_httpcookie_prefs", 0);
        cy();
    }

    public synchronized void add(URI uri, HttpCookie httpCookie) {
        URI a2 = a(uri, httpCookie);
        Set set = (Set) this.en.get(a2);
        if (set == null) {
            set = new HashSet();
            this.en.put(a2, set);
        }
        set.remove(httpCookie);
        set.add(httpCookie);
        b(a2, httpCookie);
    }

    @NonNull
    public synchronized List<HttpCookie> get(URI uri) {
        return a(uri);
    }

    @NonNull
    public synchronized List<HttpCookie> getCookies() {
        ArrayList arrayList;
        arrayList = new ArrayList();
        for (URI a2 : this.en.keySet()) {
            arrayList.addAll(a(a2));
        }
        return arrayList;
    }

    @NonNull
    public synchronized List<URI> getURIs() {
        return new ArrayList(this.en.keySet());
    }

    public synchronized boolean remove(URI uri, HttpCookie httpCookie) {
        boolean z;
        Set set = (Set) this.en.get(uri);
        z = set != null && set.remove(httpCookie);
        if (z) {
            c(uri, httpCookie);
        }
        return z;
    }

    public synchronized boolean removeAll() {
        this.en.clear();
        cz();
        return true;
    }

    private void cy() {
        for (Entry entry : this.em.getAll().entrySet()) {
            try {
                URI uri = new URI(((String) entry.getKey()).split("\\|", 2)[0]);
                HttpCookie T = new ds().T((String) entry.getValue());
                Set set = (Set) this.en.get(uri);
                if (set == null) {
                    set = new HashSet();
                    this.en.put(uri, set);
                }
                set.add(T);
            } catch (URISyntaxException e) {
                ah.a(e.getMessage());
            }
        }
    }

    private void b(@NonNull URI uri, @NonNull HttpCookie httpCookie) {
        Editor edit = this.em.edit();
        StringBuilder sb = new StringBuilder();
        sb.append(uri.toString());
        sb.append("|");
        sb.append(httpCookie.getName());
        edit.putString(sb.toString(), new ds().a(httpCookie));
        edit.apply();
    }

    @NonNull
    private List<HttpCookie> a(@NonNull URI uri) {
        HashSet hashSet = new HashSet();
        for (Entry entry : this.en.entrySet()) {
            URI uri2 = (URI) entry.getKey();
            if (d(uri2.getHost(), uri.getHost()) && e(uri2.getPath(), uri.getPath())) {
                hashSet.addAll((Collection) entry.getValue());
            }
        }
        ArrayList arrayList = new ArrayList();
        Iterator it = hashSet.iterator();
        while (it.hasNext()) {
            HttpCookie httpCookie = (HttpCookie) it.next();
            if (httpCookie.hasExpired()) {
                arrayList.add(httpCookie);
                it.remove();
            }
        }
        if (!arrayList.isEmpty()) {
            a(uri, (List<HttpCookie>) arrayList);
        }
        return new ArrayList(hashSet);
    }

    private boolean d(@NonNull String str, @NonNull String str2) {
        if (!str2.equals(str)) {
            StringBuilder sb = new StringBuilder();
            sb.append(".");
            sb.append(str);
            if (!str2.endsWith(sb.toString())) {
                return false;
            }
        }
        return true;
    }

    private boolean e(@NonNull String str, @NonNull String str2) {
        if (str2.equals(str) || ((str2.startsWith(str) && str.charAt(str.length() - 1) == '/') || (str2.startsWith(str) && str2.substring(str.length()).charAt(0) == '/'))) {
            return true;
        }
        return false;
    }

    private void a(@NonNull URI uri, @NonNull List<HttpCookie> list) {
        Editor edit = this.em.edit();
        for (HttpCookie httpCookie : list) {
            StringBuilder sb = new StringBuilder();
            sb.append(uri.toString());
            sb.append("|");
            sb.append(httpCookie.getName());
            edit.remove(sb.toString());
        }
        edit.apply();
    }

    private void c(@NonNull URI uri, @NonNull HttpCookie httpCookie) {
        Editor edit = this.em.edit();
        StringBuilder sb = new StringBuilder();
        sb.append(uri.toString());
        sb.append("|");
        sb.append(httpCookie.getName());
        edit.remove(sb.toString());
        edit.apply();
    }

    private void cz() {
        this.em.edit().clear().apply();
    }
}
