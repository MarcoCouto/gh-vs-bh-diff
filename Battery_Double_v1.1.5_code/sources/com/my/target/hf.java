package com.my.target;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.vungle.warren.model.AdvertisementDBAdapter.AdvertisementColumns;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.util.Locale;

/* compiled from: EncryptionUtils */
public class hf {
    @Nullable
    public static String ah(@NonNull String str) {
        try {
            MessageDigest instance = MessageDigest.getInstance(AdvertisementColumns.COLUMN_MD5);
            instance.update(str.getBytes(Charset.forName("UTF-8")));
            byte[] digest = instance.digest();
            StringBuilder sb = new StringBuilder();
            for (byte valueOf : digest) {
                sb.append(String.format("%02X", new Object[]{Byte.valueOf(valueOf)}));
            }
            return sb.toString().toLowerCase(Locale.ROOT);
        } catch (Exception unused) {
            return null;
        }
    }
}
