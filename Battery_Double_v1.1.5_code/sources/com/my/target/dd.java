package com.my.target;

import android.support.annotation.NonNull;

/* compiled from: MrcStat */
public class dd extends di {
    private float dR = 0.0f;
    private float duration = -1.0f;

    @NonNull
    public static dd K(@NonNull String str) {
        return new dd("mrcStat", str);
    }

    public float getDuration() {
        return this.duration;
    }

    public void setDuration(float f) {
        this.duration = f;
    }

    public float cd() {
        return this.dR;
    }

    public void g(float f) {
        this.dR = f;
    }

    protected dd(@NonNull String str, @NonNull String str2) {
        super(str, str2);
    }
}
