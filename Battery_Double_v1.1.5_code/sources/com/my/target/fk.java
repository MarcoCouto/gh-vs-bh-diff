package com.my.target;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.MeasureSpec;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import com.my.target.hq.a;
import com.my.target.instreamads.InstreamAdPlayer;
import com.my.target.instreamads.InstreamAdPlayer.AdPlayerListener;

/* compiled from: InstreamAdVideoPlayer */
public class fk extends FrameLayout implements a, InstreamAdPlayer {
    @Nullable
    private hs ag;
    @NonNull
    private final fn hc;
    @Nullable
    private AdPlayerListener hd;
    private boolean he;
    private boolean hf;
    private int placeholderHeight;
    private int placeholderWidth;

    public void B() {
    }

    public void a(float f, float f2) {
    }

    @NonNull
    public View getView() {
        return this;
    }

    public fk(@NonNull Context context, @Nullable AttributeSet attributeSet, int i) {
        this(context, attributeSet, i, new fn(context));
    }

    @VisibleForTesting
    fk(@NonNull Context context, @Nullable AttributeSet attributeSet, int i, @NonNull fn fnVar) {
        super(context, attributeSet, i);
        this.hc = fnVar;
        LayoutParams layoutParams = new LayoutParams(-1, -2);
        layoutParams.gravity = 17;
        addView(fnVar, layoutParams);
    }

    @Nullable
    public AdPlayerListener getAdPlayerListener() {
        return this.hd;
    }

    public fk(@NonNull Context context) {
        this(context, null);
    }

    public fk(@NonNull Context context, @Nullable AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public int getPlaceholderWidth() {
        return this.placeholderWidth;
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public int getPlaceholderHeight() {
        return this.placeholderHeight;
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public void setVideoPlayer(@Nullable hs hsVar) {
        this.ag = hsVar;
    }

    public float getAdVideoDuration() {
        if (this.ag != null) {
            return this.ag.getDuration();
        }
        return 0.0f;
    }

    public float getAdVideoPosition() {
        if (this.ag != null) {
            return ((float) this.ag.getPosition()) / 1000.0f;
        }
        return 0.0f;
    }

    public void setAdPlayerListener(@Nullable AdPlayerListener adPlayerListener) {
        this.hd = adPlayerListener;
    }

    public void playAdVideo(@NonNull Uri uri, int i, int i2) {
        this.placeholderWidth = i;
        this.placeholderHeight = i2;
        this.he = false;
        if (this.ag == null) {
            this.ag = hs.T(getContext());
            this.ag.a(this);
        }
        this.hc.e(i, i2);
        this.ag.a(uri, this.hc);
    }

    public void playAdVideo(@NonNull Uri uri, int i, int i2, float f) {
        playAdVideo(uri, i, i2);
        if (this.ag != null) {
            this.ag.seekTo((long) (f * 1000.0f));
        }
    }

    public void pauseAdVideo() {
        if (this.ag != null) {
            this.ag.pause();
        }
    }

    public void resumeAdVideo() {
        if (this.ag != null) {
            this.ag.resume();
        }
    }

    public void stopAdVideo() {
        if (this.ag != null) {
            this.ag.stop();
        }
    }

    public void destroy() {
        if (this.ag != null) {
            this.ag.destroy();
        }
    }

    public void setVolume(float f) {
        if (this.ag != null) {
            this.ag.setVolume(f);
        }
    }

    public void x() {
        if (this.hd != null) {
            this.hd.onAdVideoStopped();
        }
    }

    public void e(float f) {
        if (this.hd != null) {
            this.hd.onVolumeChanged(f);
        }
    }

    public void y() {
        if (!this.he && this.hd != null) {
            this.hd.onAdVideoStarted();
            this.he = true;
        }
    }

    public void A() {
        if (this.hf) {
            if (this.hd != null) {
                this.hd.onAdVideoResumed();
            }
            this.hf = false;
        }
    }

    public void z() {
        this.hf = true;
        if (this.hd != null) {
            this.hd.onAdVideoPaused();
        }
    }

    public void d(String str) {
        if (this.hd != null) {
            this.hd.onAdVideoError(str);
        }
    }

    public void onComplete() {
        if (this.hd != null) {
            this.hd.onAdVideoCompleted();
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int i3;
        int round;
        int round2;
        int mode = MeasureSpec.getMode(i);
        int size = MeasureSpec.getSize(i);
        int mode2 = MeasureSpec.getMode(i2);
        int size2 = MeasureSpec.getSize(i2);
        if (mode == 0) {
            mode = Integer.MIN_VALUE;
        }
        if (mode2 == 0) {
            mode2 = Integer.MIN_VALUE;
        }
        if (this.placeholderHeight == 0 || this.placeholderWidth == 0) {
            super.onMeasure(i, i2);
            return;
        }
        float f = ((float) this.placeholderWidth) / ((float) this.placeholderHeight);
        float f2 = 0.0f;
        if (size2 != 0) {
            f2 = ((float) size) / ((float) size2);
        }
        if (!(mode == 1073741824 && mode2 == 1073741824)) {
            if (mode == Integer.MIN_VALUE && mode2 == Integer.MIN_VALUE) {
                if (f < f2) {
                    round = Math.round(((float) size2) * f);
                    if (size > 0 && round > size) {
                        round2 = Math.round(((float) size) / f);
                    }
                    size = round;
                } else {
                    i3 = Math.round(((float) size) / f);
                    if (size2 > 0 && i3 > size2) {
                        size = Math.round(((float) size2) * f);
                    }
                    size2 = i3;
                }
            } else if (mode == Integer.MIN_VALUE && mode2 == 1073741824) {
                round = Math.round(((float) size2) * f);
                if (size > 0 && round > size) {
                    round2 = Math.round(((float) size) / f);
                }
                size = round;
            } else if (mode == 1073741824 && mode2 == Integer.MIN_VALUE) {
                i3 = Math.round(((float) size) / f);
                if (size2 > 0 && i3 > size2) {
                    size = Math.round(((float) size2) * f);
                }
                size2 = i3;
            } else {
                size = 0;
                size2 = 0;
            }
            size2 = round2;
        }
        super.onMeasure(MeasureSpec.makeMeasureSpec(size, 1073741824), MeasureSpec.makeMeasureSpec(size2, 1073741824));
    }
}
