package com.my.target;

import android.support.annotation.NonNull;

/* compiled from: OvvStat */
public class de extends di {
    private boolean dS;

    @NonNull
    public static de L(@NonNull String str) {
        return new de("ovvStat", str);
    }

    private de(@NonNull String str, @NonNull String str2) {
        super(str, str2);
    }

    public boolean ce() {
        return this.dS;
    }

    public void q(boolean z) {
        this.dS = z;
    }
}
