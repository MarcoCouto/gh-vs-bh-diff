package com.my.target;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.my.target.common.models.ImageData;
import com.my.target.common.models.VideoData;
import java.util.ArrayList;
import java.util.List;

/* compiled from: InterstitialAdPromoBanner */
public class cm extends ci {
    private int ctaButtonColor = -16733198;
    private int ctaButtonTextColor = -1;
    private int ctaButtonTouchColor = -16746839;
    @Nullable
    private ci endCard;
    private int footerColor = -39322;
    @NonNull
    private final List<cj> interstitialAdCards = new ArrayList();
    @Nullable
    private ImageData playIcon;
    @Nullable
    private ImageData storeIcon;
    private int style;
    @Nullable
    private cn<VideoData> videoBanner;

    @NonNull
    public static cm newBanner() {
        return new cm();
    }

    private cm() {
    }

    public void setVideoBanner(@Nullable cn<VideoData> cnVar) {
        this.videoBanner = cnVar;
    }

    @Nullable
    public cn<VideoData> getVideoBanner() {
        return this.videoBanner;
    }

    @Nullable
    public ImageData getPlayIcon() {
        return this.playIcon;
    }

    public void setPlayIcon(@Nullable ImageData imageData) {
        this.playIcon = imageData;
    }

    @Nullable
    public ImageData getStoreIcon() {
        return this.storeIcon;
    }

    public void setStoreIcon(@Nullable ImageData imageData) {
        this.storeIcon = imageData;
    }

    public void setFooterColor(int i) {
        this.footerColor = i;
    }

    public void setCtaButtonColor(int i) {
        this.ctaButtonColor = i;
    }

    public void setCtaButtonTouchColor(int i) {
        this.ctaButtonTouchColor = i;
    }

    public void setCtaButtonTextColor(int i) {
        this.ctaButtonTextColor = i;
    }

    public int getFooterColor() {
        return this.footerColor;
    }

    public int getCtaButtonColor() {
        return this.ctaButtonColor;
    }

    public int getCtaButtonTouchColor() {
        return this.ctaButtonTouchColor;
    }

    public int getCtaButtonTextColor() {
        return this.ctaButtonTextColor;
    }

    @NonNull
    public List<cj> getInterstitialAdCards() {
        return this.interstitialAdCards;
    }

    public void addInterstitialAdCard(@NonNull cj cjVar) {
        this.interstitialAdCards.add(cjVar);
    }

    public int getStyle() {
        return this.style;
    }

    public void setStyle(int i) {
        this.style = i;
    }

    public void setEndCard(@Nullable ci ciVar) {
        this.endCard = ciVar;
    }

    @Nullable
    public ci getEndCard() {
        return this.endCard;
    }
}
