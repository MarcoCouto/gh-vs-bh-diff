package com.my.target;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import org.json.JSONArray;
import org.json.JSONObject;

/* compiled from: NativeAppwallAdResponseParser */
public class aa extends c<db> {
    @NonNull
    public static c<db> f() {
        return new aa();
    }

    private aa() {
    }

    @Nullable
    public db a(@NonNull String str, @NonNull bz bzVar, @Nullable db dbVar, @NonNull a aVar, @NonNull Context context) {
        int i;
        Context context2 = context;
        JSONObject a2 = a(str, context2);
        if (a2 == null) {
            return null;
        }
        JSONArray names = a2.names();
        eh h = eh.h(bzVar, aVar, context2);
        boolean z = false;
        db dbVar2 = dbVar;
        int i2 = 0;
        while (true) {
            if (i2 >= names.length()) {
                break;
            }
            String optString = names.optString(i2);
            if ("appwall".equals(optString) || "showcaseApps".equals(optString) || "showcaseGames".equals(optString) || "showcase".equals(optString)) {
                i = i2;
                dbVar2 = a(optString, a2, h, bzVar, aVar, context);
                if (dbVar2 != null && !dbVar2.bI().isEmpty()) {
                    z = true;
                    break;
                }
            } else {
                i = i2;
            }
            i2 = i + 1;
        }
        if (!z) {
            return null;
        }
        dbVar2.n(bzVar.aX());
        dbVar2.c(a2);
        return dbVar2;
    }

    @Nullable
    private db a(@NonNull String str, @NonNull JSONObject jSONObject, @NonNull eh ehVar, @NonNull bz bzVar, @NonNull a aVar, @NonNull Context context) {
        JSONObject optJSONObject = jSONObject.optJSONObject(str);
        if (optJSONObject == null) {
            return null;
        }
        JSONArray optJSONArray = optJSONObject.optJSONArray("banners");
        if (optJSONArray == null || optJSONArray.length() <= 0) {
            return null;
        }
        db C = db.C(str);
        ehVar.a(optJSONObject, C);
        eg a2 = eg.a(C, bzVar, aVar, context);
        for (int i = 0; i < optJSONArray.length(); i++) {
            JSONObject optJSONObject2 = optJSONArray.optJSONObject(i);
            if (optJSONObject2 != null) {
                cq newBanner = cq.newBanner();
                a2.a(optJSONObject2, newBanner);
                String bundleId = newBanner.getBundleId();
                if (!TextUtils.isEmpty(bundleId)) {
                    newBanner.setAppInstalled(a(context, bundleId));
                }
                C.a(newBanner);
            }
        }
        return C;
    }

    private boolean a(@NonNull Context context, @NonNull String str) {
        PackageManager packageManager = context.getPackageManager();
        Intent launchIntentForPackage = packageManager.getLaunchIntentForPackage(str);
        if (launchIntentForPackage == null) {
            return false;
        }
        return !packageManager.queryIntentActivities(launchIntentForPackage, 65536).isEmpty();
    }
}
