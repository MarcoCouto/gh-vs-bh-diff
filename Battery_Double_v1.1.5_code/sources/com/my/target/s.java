package com.my.target;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.my.target.b.C0072b;

/* compiled from: InterstitialSliderAdFactory */
public final class s extends b<cy> {

    /* compiled from: InterstitialSliderAdFactory */
    static class a implements com.my.target.b.a<cy> {
        public boolean a() {
            return false;
        }

        private a() {
        }

        @NonNull
        public c<cy> b() {
            return t.f();
        }

        @Nullable
        public d<cy> c() {
            return u.j();
        }

        @NonNull
        public e d() {
            return e.e();
        }
    }

    public interface b extends C0072b {
    }

    @NonNull
    public static b<cy> a(@NonNull a aVar) {
        return new s(aVar);
    }

    private s(@NonNull a aVar) {
        super(new a(), aVar);
    }
}
