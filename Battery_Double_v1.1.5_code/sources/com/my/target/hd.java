package com.my.target;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.customtabs.CustomTabsIntent;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.Window;
import android.widget.FrameLayout;
import com.my.target.common.MyTargetActivity;
import com.my.target.common.MyTargetActivity.ActivityEngine;
import java.util.List;
import java.util.WeakHashMap;

/* compiled from: ClickHandler */
public class hd {
    /* access modifiers changed from: private */
    @NonNull
    public static final WeakHashMap<cg, Boolean> lQ = new WeakHashMap<>();

    /* compiled from: ClickHandler */
    static abstract class a {
        @NonNull
        protected final cg lT;

        /* access modifiers changed from: protected */
        public abstract boolean M(@NonNull Context context);

        @NonNull
        static a b(@NonNull cg cgVar) {
            return new b(cgVar);
        }

        @NonNull
        static a a(@NonNull String str, @NonNull cg cgVar) {
            if (hn.ak(str)) {
                return new c(str, cgVar);
            }
            return new d(str, cgVar);
        }

        protected a(@NonNull cg cgVar) {
            this.lT = cgVar;
        }
    }

    /* compiled from: ClickHandler */
    static class b extends a {
        private b(@NonNull cg cgVar) {
            super(cgVar);
        }

        /* access modifiers changed from: protected */
        public boolean M(@NonNull Context context) {
            if (!"store".equals(this.lT.getNavigationType())) {
                return false;
            }
            String bundleId = this.lT.getBundleId();
            if (bundleId == null) {
                return false;
            }
            Intent launchIntentForPackage = context.getPackageManager().getLaunchIntentForPackage(bundleId);
            if (launchIntentForPackage == null) {
                return false;
            }
            if (a(bundleId, this.lT.getDeeplink(), context)) {
                hl.a((List<dg>) this.lT.getStatHolder().N("deeplinkClick"), context);
                return true;
            } else if (!b(bundleId, this.lT.getUrlscheme(), context) && !a(launchIntentForPackage, context)) {
                return false;
            } else {
                hl.a((List<dg>) this.lT.getStatHolder().N("click"), context);
                String trackingLink = this.lT.getTrackingLink();
                if (trackingLink != null && !hn.ak(trackingLink)) {
                    hn.an(trackingLink).S(context);
                }
                return true;
            }
        }

        private boolean a(@NonNull String str, @Nullable String str2, @NonNull Context context) {
            if (str2 == null) {
                return false;
            }
            try {
                Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(str2));
                if (!(context instanceof Activity)) {
                    intent.addFlags(268435456);
                }
                intent.setPackage(str);
                context.startActivity(intent);
                return true;
            } catch (Exception unused) {
                return false;
            }
        }

        private boolean b(@NonNull String str, @Nullable String str2, @NonNull Context context) {
            if (str2 == null) {
                return false;
            }
            try {
                Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(str2));
                if (!(context instanceof Activity)) {
                    intent.addFlags(268435456);
                }
                intent.setPackage(str);
                context.startActivity(intent);
                return true;
            } catch (Exception unused) {
                return false;
            }
        }

        private boolean a(@NonNull Intent intent, @NonNull Context context) {
            try {
                if (!(context instanceof Activity)) {
                    intent.addFlags(268435456);
                }
                context.startActivity(intent);
                return true;
            } catch (Exception unused) {
                return false;
            }
        }
    }

    /* compiled from: ClickHandler */
    static class c extends d {
        private c(@NonNull String str, @NonNull cg cgVar) {
            super(str, cgVar);
        }

        /* access modifiers changed from: protected */
        public boolean M(@NonNull Context context) {
            if (hn.al(this.url)) {
                if (j(this.url, context)) {
                    return true;
                }
            } else if (k(this.url, context)) {
                return true;
            }
            return super.M(context);
        }

        private boolean j(@NonNull String str, @NonNull Context context) {
            try {
                Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(str));
                if (!(context instanceof Activity)) {
                    intent.addFlags(268435456);
                }
                context.startActivity(intent);
                return true;
            } catch (Exception unused) {
                return false;
            }
        }

        private boolean k(@NonNull String str, @NonNull Context context) {
            try {
                if (this.lT.isUsePlayStoreAction()) {
                    Intent launchIntentForPackage = context.getPackageManager().getLaunchIntentForPackage("com.android.vending");
                    if (launchIntentForPackage != null) {
                        launchIntentForPackage.setComponent(new ComponentName("com.android.vending", "com.google.android.finsky.activities.LaunchUrlHandlerActivity"));
                        if (!(context instanceof Activity)) {
                            launchIntentForPackage.addFlags(268435456);
                        }
                        launchIntentForPackage.setData(Uri.parse(str));
                        context.startActivity(launchIntentForPackage);
                    }
                } else {
                    Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(str));
                    if (!(context instanceof Activity)) {
                        intent.addFlags(268435456);
                    }
                    context.startActivity(intent);
                }
                return true;
            } catch (Exception unused) {
                return false;
            }
        }
    }

    /* compiled from: ClickHandler */
    static class d extends a {
        @NonNull
        protected final String url;

        private d(@NonNull String str, @NonNull cg cgVar) {
            super(cgVar);
            this.url = str;
        }

        /* access modifiers changed from: protected */
        public boolean M(@NonNull Context context) {
            if (this.lT.isOpenInBrowser()) {
                return n(this.url, context);
            }
            if (VERSION.SDK_INT >= 18 && m(this.url, context)) {
                return true;
            }
            if ("store".equals(this.lT.getNavigationType()) || (VERSION.SDK_INT >= 28 && !hn.am(this.url))) {
                return n(this.url, context);
            }
            return l(this.url, context);
        }

        private boolean l(@NonNull String str, @NonNull Context context) {
            e.ae(str).k(context);
            return true;
        }

        @TargetApi(18)
        private boolean m(@NonNull String str, @NonNull Context context) {
            try {
                Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(str));
                Bundle bundle = new Bundle();
                bundle.putBinder(CustomTabsIntent.EXTRA_SESSION, null);
                if (!(context instanceof Activity)) {
                    intent.addFlags(268435456);
                }
                intent.setPackage("com.android.chrome");
                intent.putExtras(bundle);
                context.startActivity(intent);
                return true;
            } catch (ActivityNotFoundException unused) {
                return false;
            }
        }

        private boolean n(@NonNull String str, @NonNull Context context) {
            try {
                Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(str));
                if (!(context instanceof Activity)) {
                    intent.addFlags(268435456);
                }
                context.startActivity(intent);
                return true;
            } catch (Exception unused) {
                return false;
            }
        }
    }

    /* compiled from: ClickHandler */
    static class e implements ActivityEngine {
        @NonNull
        private final String lU;
        @Nullable
        private ge lV;

        public boolean onActivityOptionsItemSelected(MenuItem menuItem) {
            return false;
        }

        public void onActivityPause() {
        }

        public void onActivityResume() {
        }

        public void onActivityStart() {
        }

        public void onActivityStop() {
        }

        @NonNull
        public static e ae(@NonNull String str) {
            return new e(str);
        }

        private e(@NonNull String str) {
            this.lU = str;
        }

        public void k(@NonNull Context context) {
            MyTargetActivity.activityEngine = this;
            Intent intent = new Intent(context, MyTargetActivity.class);
            if (!(context instanceof Activity)) {
                intent.addFlags(268435456);
            }
            context.startActivity(intent);
        }

        public void onActivityCreate(@NonNull final MyTargetActivity myTargetActivity, @NonNull Intent intent, @NonNull FrameLayout frameLayout) {
            myTargetActivity.setTheme(16973837);
            if (VERSION.SDK_INT >= 21) {
                Window window = myTargetActivity.getWindow();
                window.addFlags(Integer.MIN_VALUE);
                window.setStatusBarColor(-12232092);
            }
            this.lV = new ge(myTargetActivity);
            frameLayout.addView(this.lV);
            this.lV.dl();
            this.lV.setUrl(this.lU);
            this.lV.setListener(new com.my.target.ge.b() {
                public void ag() {
                    myTargetActivity.finish();
                }
            });
        }

        public void onActivityDestroy() {
            if (this.lV != null) {
                this.lV.destroy();
                this.lV = null;
            }
        }

        public boolean onActivityBackPressed() {
            if (this.lV == null || !this.lV.canGoBack()) {
                return true;
            }
            this.lV.goBack();
            return false;
        }
    }

    @NonNull
    public static hd dM() {
        return new hd();
    }

    private hd() {
    }

    public void b(@NonNull cg cgVar, @NonNull Context context) {
        c(cgVar, cgVar.getTrackingLink(), context);
    }

    public void c(@NonNull cg cgVar, @Nullable String str, @NonNull Context context) {
        if (!lQ.containsKey(cgVar) && !a.b(cgVar).M(context)) {
            if (str != null) {
                a(str, cgVar, context);
            }
            hl.a((List<dg>) cgVar.getStatHolder().N("click"), context);
        }
    }

    private void a(@NonNull String str, @NonNull final cg cgVar, @NonNull final Context context) {
        if (cgVar.isDirectLink() || hn.ak(str)) {
            b(str, cgVar, context);
            return;
        }
        lQ.put(cgVar, Boolean.valueOf(true));
        hn.an(str).a((com.my.target.hn.a) new com.my.target.hn.a() {
            public void ad(@Nullable String str) {
                if (!TextUtils.isEmpty(str)) {
                    hd.this.b(str, cgVar, context);
                }
                hd.lQ.remove(cgVar);
            }
        }).S(context);
    }

    /* access modifiers changed from: private */
    public void b(@NonNull String str, @NonNull cg cgVar, @NonNull Context context) {
        a.a(str, cgVar).M(context);
    }
}
