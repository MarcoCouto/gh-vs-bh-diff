package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import com.my.target.common.models.ImageData;
import com.my.target.ha.c;
import java.util.List;

/* compiled from: SliderAdView */
public class gx extends RelativeLayout {
    private static final int lo = hm.dV();
    private static final int lp = hm.dV();
    private static final int lq = hm.dV();
    @NonNull
    private final gw lr;
    @NonNull
    private final fu ls;
    @NonNull
    private final ha lt;
    @NonNull
    private final FrameLayout lu;
    @NonNull
    private final LayoutParams lv;
    @NonNull
    private final LayoutParams lw = new LayoutParams(-2, -2);
    @NonNull
    private final LayoutParams lx;
    private int orientation;
    @NonNull
    private final hm uiUtils;

    public gx(Context context) {
        super(context);
        this.uiUtils = hm.R(context);
        this.lr = new gw(context);
        this.ls = new fu(context);
        this.lu = new FrameLayout(context);
        this.lt = new ha(context);
        this.lt.setId(lo);
        this.ls.setId(lq);
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-1, -1);
        layoutParams.gravity = 17;
        this.lt.setLayoutParams(layoutParams);
        this.lv = new LayoutParams(-2, -2);
        this.lv.addRule(14, -1);
        this.lv.addRule(12, -1);
        this.lr.setId(lp);
        this.lx = new LayoutParams(-1, -1);
        this.lx.addRule(2, lp);
        this.lu.addView(this.lt);
        addView(this.lu);
        addView(this.lr);
        addView(this.ls);
    }

    public void setCloseClickListener(@Nullable OnClickListener onClickListener) {
        this.ls.setOnClickListener(onClickListener);
    }

    public void setFSSliderCardListener(@Nullable c cVar) {
        this.lt.setSliderCardListener(cVar);
    }

    public void a(@NonNull cy cyVar, @NonNull List<cl> list) {
        ImageData closeIcon = cyVar.getCloseIcon();
        if (closeIcon == null || closeIcon.getBitmap() == null) {
            this.ls.a(fh.y(this.uiUtils.E(36)), false);
        } else {
            this.ls.a(closeIcon.getBitmap(), true);
        }
        setBackgroundColor(cyVar.getBackgroundColor());
        int size = list.size();
        if (size > 1) {
            this.lr.c(size, cyVar.bH(), cyVar.bG());
        } else {
            this.lr.setVisibility(8);
        }
        this.lt.a(list, cyVar.getBackgroundColor());
    }

    public void B(int i) {
        this.lr.B(i);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        j(MeasureSpec.getSize(i), MeasureSpec.getSize(i2));
        super.onMeasure(i, i2);
    }

    private void j(int i, int i2) {
        int i3 = ((float) i) / ((float) i2) > 1.0f ? 2 : 1;
        if (i3 != this.orientation) {
            setLayoutOrientation(i3);
        }
    }

    private void setLayoutOrientation(int i) {
        this.orientation = i;
        if (this.orientation == 1) {
            this.lv.setMargins(0, this.uiUtils.E(12), 0, this.uiUtils.E(16));
            this.lx.topMargin = this.uiUtils.E(56);
            this.lw.setMargins(0, 0, 0, 0);
        } else {
            this.lv.setMargins(0, this.uiUtils.E(6), 0, this.uiUtils.E(8));
            this.lx.topMargin = this.uiUtils.E(28);
            this.lw.setMargins(this.uiUtils.E(-4), this.uiUtils.E(-8), 0, 0);
        }
        this.lu.setLayoutParams(this.lx);
        this.lr.setLayoutParams(this.lv);
        this.ls.setLayoutParams(this.lw);
    }
}
