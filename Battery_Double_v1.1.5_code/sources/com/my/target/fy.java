package com.my.target;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.Path.FillType;
import android.graphics.Rect;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.View.MeasureSpec;
import com.github.mikephil.charting.utils.Utils;

/* compiled from: StarsRatingView */
public class fy extends View {
    private static final Paint ic = new Paint();
    private int ie;

    /* renamed from: if reason: not valid java name */
    private float f676if;
    @Nullable
    private Bitmap ig;
    private boolean ih;
    private float rating;

    static {
        ic.setAntiAlias(true);
        ic.setStyle(Style.FILL);
    }

    public fy(@NonNull Context context) {
        super(context);
    }

    public void setStarSize(int i) {
        this.ie = i;
    }

    public void setRating(float f) {
        setContentDescription(Float.toString(f));
        if (f > 5.0f || f < 0.0f) {
            StringBuilder sb = new StringBuilder();
            sb.append("Rating is out of bounds: ");
            sb.append(f);
            ah.a(sb.toString());
            this.rating = 0.0f;
        } else {
            this.rating = f;
        }
        invalidate();
    }

    public void setStarsPadding(float f) {
        this.f676if = f;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int i3;
        if (this.ie > 0) {
            i3 = this.ie;
        } else {
            i3 = MeasureSpec.getSize(i2);
            this.ie = i3;
        }
        setMeasuredDimension((int) (((float) (i3 * 5)) + (this.f676if * 4.0f)), i3);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        if (this.ig != null) {
            canvas.drawBitmap(this.ig, 0.0f, 0.0f, null);
        } else if (this.ie > 0 && !this.ih) {
            this.ih = true;
            post(new Runnable() {
                public void run() {
                    fy.this.dt();
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public void dt() {
        if (this.ie > 0) {
            int floor = (int) Math.floor((double) this.rating);
            int ceil = (int) Math.ceil((double) (5.0f - this.rating));
            float f = (float) floor;
            boolean z = this.rating - f >= 0.2f;
            try {
                this.ig = Bitmap.createBitmap((int) ((((float) this.ie) + this.f676if) * 5.0f), this.ie, Config.ARGB_8888);
                Canvas canvas = new Canvas(this.ig);
                a(0, this.ie, -552162, canvas, floor);
                int i = (int) (((float) 0) + ((((float) this.ie) + this.f676if) * f));
                a(i, this.ie, -3355444, canvas, ceil);
                if (z) {
                    int i2 = this.ie;
                    double d = (double) this.rating;
                    double floor2 = Math.floor((double) this.rating);
                    Double.isNaN(d);
                    a(i, i2, (float) (d - floor2), canvas);
                }
                invalidate();
                this.ih = false;
            } catch (OutOfMemoryError unused) {
                ah.a("Unable to create rating bitmap because of OOME");
            }
        }
    }

    private void a(int i, int i2, int i3, @NonNull Canvas canvas, int i4) {
        ic.setColor(i3);
        canvas.drawPath(a(i, (float) (i2 / 2), i4), ic);
    }

    @NonNull
    private Path a(int i, float f, int i2) {
        float f2 = f;
        Path path = new Path();
        path.setFillType(FillType.EVEN_ODD);
        int i3 = i2;
        int i4 = 0;
        while (i4 < i3) {
            float f3 = (float) i4;
            float f4 = ((float) i) + f2 + (f3 * f2 * 2.0f) + (f3 * this.f676if);
            float f5 = 0.45f * f2;
            double d = (double) f4;
            double d2 = (double) f2;
            double sin = Math.sin(Utils.DOUBLE_EPSILON);
            Double.isNaN(d2);
            double d3 = sin * d2;
            Double.isNaN(d);
            float f6 = (float) (d + d3);
            float f7 = 2.0f * f2;
            double cos = Math.cos(Utils.DOUBLE_EPSILON);
            Double.isNaN(d2);
            double d4 = cos * d2;
            Double.isNaN(d2);
            path.moveTo(f6, f7 - ((float) (d4 + d2)));
            double d5 = (double) f5;
            double sin2 = Math.sin(0.6283185307179586d);
            Double.isNaN(d5);
            double d6 = sin2 * d5;
            Double.isNaN(d);
            int i5 = i4;
            float f8 = (float) (d + d6);
            double cos2 = Math.cos(0.6283185307179586d);
            Double.isNaN(d5);
            double d7 = cos2 * d5;
            Double.isNaN(d2);
            path.lineTo(f8, f7 - ((float) (d2 + d7)));
            int i6 = 1;
            while (i6 < 5) {
                double d8 = (double) i6;
                Double.isNaN(d8);
                double d9 = 1.2566370614359172d * d8;
                double sin3 = Math.sin(d9);
                Double.isNaN(d2);
                double d10 = sin3 * d2;
                Double.isNaN(d);
                float f9 = (float) (d10 + d);
                double cos3 = Math.cos(d9);
                Double.isNaN(d2);
                double d11 = cos3 * d2;
                Double.isNaN(d2);
                double d12 = d;
                path.lineTo(f9, f7 - ((float) (d2 + d11)));
                double d13 = d9 + 0.6283185307179586d;
                double sin4 = Math.sin(d13);
                Double.isNaN(d5);
                double d14 = sin4 * d5;
                Double.isNaN(d12);
                float f10 = (float) (d12 + d14);
                double cos4 = Math.cos(d13);
                Double.isNaN(d5);
                double d15 = cos4 * d5;
                Double.isNaN(d2);
                path.lineTo(f10, f7 - ((float) (d15 + d2)));
                i6++;
                d = d12;
                int i7 = i;
            }
            i4 = i5 + 1;
            i3 = i2;
        }
        path.close();
        return path;
    }

    private void a(int i, int i2, float f, @NonNull Canvas canvas) {
        ic.setColor(-552162);
        Path a2 = a(0, (float) (i2 / 2), 1);
        float f2 = ((float) i2) * f;
        Rect rect = new Rect(i, 0, (int) (((float) i) + f2), i2);
        Bitmap createBitmap = Bitmap.createBitmap((int) f2, i2, Config.ARGB_8888);
        new Canvas(createBitmap).drawPath(a2, ic);
        canvas.drawBitmap(createBitmap, null, rect, ic);
    }
}
