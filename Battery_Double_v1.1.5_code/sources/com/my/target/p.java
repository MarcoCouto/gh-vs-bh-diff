package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import com.my.target.common.models.VideoData;
import com.my.target.ee.a;
import com.tapjoy.TJAdUnitConstants.String;
import com.yandex.mobile.ads.video.tracking.Tracker.Events;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONObject;

/* compiled from: InterstitialAdResponseParser */
public class p extends c<cx> implements a {
    @Nullable
    private String mraidJs;

    @NonNull
    public static c<cx> f() {
        return new p();
    }

    private p() {
    }

    @Nullable
    public cx a(@NonNull String str, @NonNull bz bzVar, @Nullable cx cxVar, @NonNull a aVar, @NonNull Context context) {
        if (c.isVast(str)) {
            return b(str, bzVar, aVar, cxVar, context);
        }
        return a(str, bzVar, aVar, cxVar, context);
    }

    @Nullable
    public cu a(@NonNull JSONObject jSONObject, @NonNull bz bzVar, @NonNull a aVar, @NonNull Context context) {
        ci b = b(jSONObject, bzVar, aVar, context);
        if (b == null) {
            return null;
        }
        cx bD = cx.bD();
        bD.a(b);
        return bD;
    }

    @Nullable
    private cx a(@NonNull String str, @NonNull bz bzVar, @NonNull a aVar, @Nullable cx cxVar, @NonNull Context context) {
        JSONObject a2 = a(str, context);
        if (a2 == null) {
            return cxVar;
        }
        if (cxVar == null) {
            cxVar = cx.bD();
        }
        this.mraidJs = a2.optString("mraid.js");
        JSONObject optJSONObject = a2.optJSONObject(aVar.getFormat());
        if (optJSONObject == null) {
            if (aVar.isMediationEnabled()) {
                JSONObject optJSONObject2 = a2.optJSONObject("mediation");
                if (optJSONObject2 != null) {
                    cs f = ee.a(this, bzVar, aVar, context).f(optJSONObject2);
                    if (f != null) {
                        cxVar.a(f);
                    }
                }
            }
            return cxVar;
        }
        JSONArray optJSONArray = optJSONObject.optJSONArray("banners");
        if (optJSONArray == null || optJSONArray.length() <= 0) {
            return cxVar;
        }
        JSONObject optJSONObject3 = optJSONArray.optJSONObject(0);
        if (optJSONObject3 != null) {
            ci b = b(optJSONObject3, bzVar, aVar, context);
            if (b != null) {
                cxVar.a(b);
            }
        }
        return cxVar;
    }

    private void a(@NonNull bz bzVar, @NonNull a aVar, @NonNull JSONObject jSONObject, @NonNull Context context) {
        bz d = dt.a(bzVar, aVar, context).d(jSONObject);
        if (d != null) {
            bzVar.b(d);
        }
    }

    @Nullable
    private cx b(@NonNull String str, @NonNull bz bzVar, @NonNull a aVar, @Nullable cx cxVar, @NonNull Context context) {
        em a2 = em.a(aVar, bzVar, context);
        a2.V(str);
        return !a2.cD().isEmpty() ? a(cxVar, a2, bzVar) : cxVar;
    }

    @NonNull
    private cx a(@Nullable cx cxVar, @NonNull em<VideoData> emVar, @NonNull bz bzVar) {
        if (cxVar == null) {
            cxVar = cx.bD();
        }
        cn cnVar = (cn) emVar.cD().get(0);
        cm newBanner = cm.newBanner();
        newBanner.setCtaText(cnVar.getCtaText());
        newBanner.setVideoBanner(cnVar);
        newBanner.setStyle(1);
        newBanner.setTrackingLink(cnVar.getTrackingLink());
        Boolean bi = bzVar.bi();
        if (bi != null) {
            newBanner.setDirectLink(bi.booleanValue());
        }
        Boolean bj = bzVar.bj();
        if (bj != null) {
            newBanner.setOpenInBrowser(bj.booleanValue());
        }
        for (dg b : cnVar.getStatHolder().N("click")) {
            newBanner.getStatHolder().b(b);
        }
        cxVar.d(emVar.aZ());
        cxVar.a(newBanner);
        Iterator it = cnVar.getCompanionBanners().iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            ch chVar = (ch) it.next();
            ci ciVar = null;
            if (chVar.getHtmlResource() != null) {
                ciVar = ck.fromCompanion(chVar);
                continue;
            } else if (chVar.getStaticResource() != null) {
                ciVar = cl.fromCompanion(chVar);
                continue;
            } else {
                continue;
            }
            if (ciVar != null) {
                newBanner.setEndCard(ciVar);
                break;
            }
        }
        return cxVar;
    }

    @Nullable
    private ci b(@NonNull JSONObject jSONObject, @NonNull bz bzVar, @NonNull a aVar, @NonNull Context context) {
        char c;
        ci ciVar;
        String optString = jSONObject.optString("type", "");
        dz d = dz.d(bzVar, aVar, context);
        boolean z = false;
        switch (optString.hashCode()) {
            case -1396342996:
                if (optString.equals("banner")) {
                    c = 2;
                    break;
                }
            case -974458767:
                if (optString.equals("additionalData")) {
                    c = 0;
                    break;
                }
            case 3213227:
                if (optString.equals(String.HTML)) {
                    c = 4;
                    break;
                }
            case 106940687:
                if (optString.equals(NotificationCompat.CATEGORY_PROMO)) {
                    c = 3;
                    break;
                }
            case 110066619:
                if (optString.equals(Events.CREATIVE_FULLSCREEN)) {
                    c = 1;
                    break;
                }
            default:
                c = 65535;
                break;
        }
        switch (c) {
            case 0:
                a(bzVar, aVar, jSONObject, context);
                break;
            case 1:
            case 2:
                cl newBanner = cl.newBanner();
                z = d.a(jSONObject, newBanner);
                ciVar = newBanner;
                break;
            case 3:
                cm newBanner2 = cm.newBanner();
                z = d.a(jSONObject, newBanner2, this.mraidJs);
                ciVar = newBanner2;
                break;
            case 4:
                ck newBanner3 = ck.newBanner();
                z = d.a(jSONObject, newBanner3, this.mraidJs);
                ciVar = newBanner3;
                break;
        }
        ciVar = 0;
        if (z) {
            return ciVar;
        }
        return null;
    }
}
