package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import com.my.target.common.models.ImageData;
import org.json.JSONObject;

/* compiled from: NativeAppwallAdBannerParser */
public class eg {
    @NonNull
    private final dv er;
    @NonNull
    private final db section;

    public static eg a(@NonNull db dbVar, @NonNull bz bzVar, @NonNull a aVar, @NonNull Context context) {
        return new eg(dbVar, bzVar, aVar, context);
    }

    private eg(@NonNull db dbVar, @NonNull bz bzVar, @NonNull a aVar, @NonNull Context context) {
        this.section = dbVar;
        this.er = dv.b(bzVar, aVar, context);
    }

    public void a(@NonNull JSONObject jSONObject, @NonNull cq cqVar) {
        this.er.a(jSONObject, (cg) cqVar);
        cqVar.setHasNotification(jSONObject.optBoolean("hasNotification", cqVar.isHasNotification()));
        cqVar.setBanner(jSONObject.optBoolean("Banner", cqVar.isBanner()));
        cqVar.setRequireCategoryHighlight(jSONObject.optBoolean("RequireCategoryHighlight", cqVar.isRequireCategoryHighlight()));
        cqVar.setItemHighlight(jSONObject.optBoolean("ItemHighlight", cqVar.isItemHighlight()));
        cqVar.setMain(jSONObject.optBoolean("Main", cqVar.isMain()));
        cqVar.setRequireWifi(jSONObject.optBoolean("RequireWifi", cqVar.isRequireWifi()));
        cqVar.setSubItem(jSONObject.optBoolean("subitem", cqVar.isSubItem()));
        cqVar.setBubbleId(jSONObject.optString("bubble_id", cqVar.getBubbleId()));
        cqVar.setLabelType(jSONObject.optString("labelType", cqVar.getLabelType()));
        cqVar.setStatus(jSONObject.optString("status", cqVar.getStatus()));
        cqVar.setPaidType(jSONObject.optString("paidType", cqVar.getPaidType()));
        cqVar.setMrgsId(jSONObject.optInt("mrgs_id"));
        cqVar.setCoins(jSONObject.optInt("coins"));
        cqVar.setCoinsIconBgColor(ed.a(jSONObject, "coins_icon_bgcolor", cqVar.getCoinsIconBgColor()));
        cqVar.setCoinsIconTextColor(ed.a(jSONObject, "coins_icon_textcolor", cqVar.getCoinsIconTextColor()));
        String optString = jSONObject.optString("icon_hd");
        if (!TextUtils.isEmpty(optString)) {
            cqVar.setIcon(ImageData.newImageData(optString));
        }
        String optString2 = jSONObject.optString("coins_icon_hd");
        if (!TextUtils.isEmpty(optString2)) {
            cqVar.setCoinsIcon(ImageData.newImageData(optString2));
        }
        String optString3 = jSONObject.optString("cross_notif_icon_hd");
        if (!TextUtils.isEmpty(optString3)) {
            cqVar.setCrossNotifIcon(ImageData.newImageData(optString3));
        }
        String bV = this.section.bV();
        if (!TextUtils.isEmpty(bV)) {
            cqVar.setBubbleIcon(ImageData.newImageData(bV));
        }
        String bX = this.section.bX();
        if (!TextUtils.isEmpty(bX)) {
            cqVar.setGotoAppIcon(ImageData.newImageData(bX));
        }
        String bW = this.section.bW();
        if (!TextUtils.isEmpty(bW)) {
            cqVar.setLabelIcon(ImageData.newImageData(bW));
        }
        String status = cqVar.getStatus();
        if (status != null) {
            String I = this.section.I(status);
            if (!TextUtils.isEmpty(I)) {
                cqVar.setStatusIcon(ImageData.newImageData(I));
            }
        }
        String bY = this.section.bY();
        if (cqVar.isItemHighlight() && !TextUtils.isEmpty(bY)) {
            cqVar.setItemHighlightIcon(ImageData.newImageData(bY));
        }
    }
}
