package com.my.target;

import android.support.annotation.NonNull;

/* compiled from: NativeAdCard */
public class cp extends cg {
    @NonNull
    public static cp newCard(@NonNull co coVar) {
        cp cpVar = new cp();
        cpVar.ctaText = coVar.ctaText;
        cpVar.navigationType = coVar.navigationType;
        cpVar.urlscheme = coVar.urlscheme;
        cpVar.bundleId = coVar.bundleId;
        cpVar.directLink = coVar.directLink;
        cpVar.openInBrowser = coVar.openInBrowser;
        cpVar.usePlayStoreAction = coVar.usePlayStoreAction;
        cpVar.deeplink = coVar.deeplink;
        cpVar.clickArea = coVar.clickArea;
        cpVar.rating = coVar.rating;
        cpVar.votes = coVar.votes;
        cpVar.domain = coVar.domain;
        cpVar.category = coVar.category;
        cpVar.subCategory = coVar.subCategory;
        return cpVar;
    }

    private cp() {
    }
}
