package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import org.json.JSONArray;
import org.json.JSONObject;

/* compiled from: StatsParser */
public class el {
    @NonNull
    private final a adConfig;
    @Nullable
    private String cj;
    @NonNull
    private final Context context;
    @NonNull
    private final bz eq;

    @NonNull
    public static el k(@NonNull bz bzVar, @NonNull a aVar, @NonNull Context context2) {
        return new el(bzVar, aVar, context2);
    }

    protected el(@NonNull bz bzVar, @NonNull a aVar, @NonNull Context context2) {
        this.eq = bzVar;
        this.adConfig = aVar;
        this.context = context2;
    }

    public void a(@NonNull dh dhVar, @NonNull JSONObject jSONObject, @NonNull String str, float f) {
        dhVar.a(this.eq.bh(), f);
        JSONArray optJSONArray = jSONObject.optJSONArray("statistics");
        if (optJSONArray != null) {
            int length = optJSONArray.length();
            if (length > 0) {
                this.cj = str;
                for (int i = 0; i < length; i++) {
                    JSONObject optJSONObject = optJSONArray.optJSONObject(i);
                    if (optJSONObject != null) {
                        dg a2 = a(optJSONObject, f);
                        if (a2 != null) {
                            dhVar.b(a2);
                        }
                    }
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    @Nullable
    public dg a(@NonNull JSONObject jSONObject, float f) {
        String optString = jSONObject.optString("type");
        String optString2 = jSONObject.optString("url");
        if (TextUtils.isEmpty(optString) || TextUtils.isEmpty(optString2)) {
            f("Required field", "failed to parse stat: no type or url");
            return null;
        }
        char c = 65535;
        int hashCode = optString.hashCode();
        if (hashCode != 1669348544) {
            if (hashCode == 1788134515 && optString.equals("playheadReachedValue")) {
                c = 0;
            }
        } else if (optString.equals("playheadViewabilityValue")) {
            c = 1;
        }
        switch (c) {
            case 0:
                return a(jSONObject, optString2, f);
            case 1:
                return b(jSONObject, optString2, f);
            default:
                return dg.c(optString, optString2);
        }
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public df a(@NonNull JSONObject jSONObject, @NonNull String str, float f) {
        df M = df.M(str);
        if (jSONObject.has("pvalue")) {
            float optDouble = (float) jSONObject.optDouble("pvalue", (double) M.cg());
            if (optDouble >= 0.0f && optDouble <= 100.0f) {
                if (f > 0.0f) {
                    M.h((optDouble * f) / 100.0f);
                } else {
                    M.i(optDouble);
                }
                return M;
            }
        }
        if (jSONObject.has("value")) {
            float optDouble2 = (float) jSONObject.optDouble("value", (double) M.cf());
            if (optDouble2 >= 0.0f) {
                M.h(optDouble2);
                return M;
            }
        }
        return null;
    }

    @Nullable
    private dg b(@NonNull JSONObject jSONObject, @NonNull String str, float f) {
        int optInt = jSONObject.optInt("viewablePercent", -1);
        if (optInt < 0 || optInt > 100) {
            f("Bad value", "failed to parse viewabilityStat: invalid viewable percent value");
            return null;
        }
        if (jSONObject.has("ovv")) {
            de L = de.L(str);
            L.q(optInt);
            L.q(jSONObject.optBoolean("ovv", false));
            if (jSONObject.has("pvalue")) {
                float optDouble = (float) jSONObject.optDouble("pvalue", (double) L.cg());
                if (optDouble >= 0.0f && optDouble <= 100.0f) {
                    if (f > 0.0f) {
                        L.h((optDouble * f) / 100.0f);
                    } else {
                        L.i(optDouble);
                    }
                    return L;
                }
            }
            if (jSONObject.has("value")) {
                float optDouble2 = (float) jSONObject.optDouble("value", (double) L.cf());
                if (optDouble2 >= 0.0f) {
                    L.h(optDouble2);
                    return L;
                }
            }
        } else if (jSONObject.has(IronSourceConstants.EVENTS_DURATION)) {
            dd K = dd.K(str);
            K.q(optInt);
            float optDouble3 = (float) jSONObject.optDouble(IronSourceConstants.EVENTS_DURATION, (double) K.getDuration());
            if (optDouble3 >= 0.0f) {
                K.setDuration(optDouble3);
                return K;
            }
        } else {
            f("Bad value", "failed to parse viewabilityStat: no ovv or duration");
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public void f(@NonNull String str, @NonNull String str2) {
        dp.P(str).Q(str2).r(this.adConfig.getSlotId()).S(this.cj).R(this.eq.getUrl()).q(this.context);
    }
}
