package com.my.target;

import android.support.annotation.NonNull;

/* compiled from: ViewabilityStat */
public abstract class di extends dg {
    private float dT = -1.0f;
    private int dZ;
    private float value = -1.0f;

    di(@NonNull String str, @NonNull String str2) {
        super(str, str2);
    }

    public float cf() {
        return this.value;
    }

    public void h(float f) {
        this.value = f;
    }

    public float cg() {
        return this.dT;
    }

    public void i(float f) {
        this.dT = f;
    }

    public int cn() {
        return this.dZ;
    }

    public void q(int i) {
        this.dZ = i;
    }
}
