package com.my.target;

import android.support.annotation.NonNull;

/* compiled from: InterstitialAdCard */
public class cj extends cg {
    @NonNull
    public static cj newCard(@NonNull ci ciVar) {
        cj cjVar = new cj();
        cjVar.ctaText = ciVar.ctaText;
        cjVar.navigationType = ciVar.navigationType;
        cjVar.urlscheme = ciVar.urlscheme;
        cjVar.bundleId = ciVar.bundleId;
        cjVar.directLink = ciVar.directLink;
        cjVar.openInBrowser = ciVar.openInBrowser;
        cjVar.usePlayStoreAction = ciVar.usePlayStoreAction;
        cjVar.deeplink = ciVar.deeplink;
        cjVar.clickArea = ciVar.clickArea;
        cjVar.rating = ciVar.rating;
        cjVar.votes = ciVar.votes;
        cjVar.domain = ciVar.domain;
        cjVar.category = ciVar.category;
        cjVar.subCategory = ciVar.subCategory;
        return cjVar;
    }

    private cj() {
        this.clickArea = ca.da;
    }
}
