package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

/* compiled from: InterstitialPresenter */
public interface es {

    /* compiled from: InterstitialPresenter */
    public interface a {
        void a(@NonNull cg cgVar, @NonNull Context context);

        void ag();

        void b(@Nullable cg cgVar, @Nullable String str, @NonNull Context context);
    }

    @NonNull
    View cI();

    void destroy();

    void pause();

    void resume();

    void stop();
}
