package com.my.target.ads;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.my.target.ah;
import com.my.target.aw;
import com.my.target.b.C0072b;
import com.my.target.common.BaseAd;
import com.my.target.cy;
import com.my.target.s;
import com.my.target.s.b;

public final class InterstitialSliderAd extends BaseAd {
    @NonNull
    private final Context context;
    @Nullable
    private aw engine;
    private boolean hideStatusBarInDialog = false;
    @Nullable
    private InterstitialSliderAdListener listener;

    public interface InterstitialSliderAdListener {
        void onClick(@NonNull InterstitialSliderAd interstitialSliderAd);

        void onDismiss(@NonNull InterstitialSliderAd interstitialSliderAd);

        void onDisplay(@NonNull InterstitialSliderAd interstitialSliderAd);

        void onLoad(@NonNull InterstitialSliderAd interstitialSliderAd);

        void onNoAd(@NonNull String str, @NonNull InterstitialSliderAd interstitialSliderAd);
    }

    public boolean isHideStatusBarInDialog() {
        return this.hideStatusBarInDialog;
    }

    public void setHideStatusBarInDialog(boolean z) {
        this.hideStatusBarInDialog = z;
    }

    @Nullable
    public InterstitialSliderAdListener getListener() {
        return this.listener;
    }

    public void setListener(@Nullable InterstitialSliderAdListener interstitialSliderAdListener) {
        this.listener = interstitialSliderAdListener;
    }

    public InterstitialSliderAd(int i, @NonNull Context context2) {
        super(i, "fullscreenslider");
        this.context = context2;
        ah.c("InterstitialSliderAd created. Version: 5.4.7");
    }

    public final void load() {
        s.a(this.adConfig).a((C0072b<T>) new b() {
            public void onResult(@Nullable cy cyVar, @Nullable String str) {
                InterstitialSliderAd.this.handleResult(cyVar, str);
            }
        }).a(this.context);
    }

    public void show() {
        if (this.engine == null) {
            ah.c("InterstitialSliderAd.show: No ad");
        } else {
            this.engine.k(this.context);
        }
    }

    public void showDialog() {
        if (this.engine == null) {
            ah.c("InterstitialSliderAd.showDialog: No ad");
        } else {
            this.engine.l(this.context);
        }
    }

    public void dismiss() {
        if (this.engine != null) {
            this.engine.dismiss();
        }
    }

    public void destroy() {
        if (this.engine != null) {
            this.engine.destroy();
            this.engine = null;
        }
        this.listener = null;
    }

    /* access modifiers changed from: private */
    public void handleResult(@Nullable cy cyVar, @Nullable String str) {
        if (this.listener == null) {
            return;
        }
        if (cyVar == null) {
            InterstitialSliderAdListener interstitialSliderAdListener = this.listener;
            if (str == null) {
                str = "no ad";
            }
            interstitialSliderAdListener.onNoAd(str, this);
            return;
        }
        this.engine = aw.a(this, cyVar);
        this.listener.onLoad(this);
    }
}
