package com.my.target;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.WorkerThread;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Comparator;

/* compiled from: DiskCache */
public final class he {
    @NonNull
    private static final FilenameFilter lY = new FilenameFilter() {
        public boolean accept(File file, String str) {
            return str.startsWith("mytrg_");
        }
    };
    @NonNull
    private static final FilenameFilter lZ = new FilenameFilter() {
        public boolean accept(File file, String str) {
            return str.endsWith(".mp4");
        }
    };
    @Nullable
    private static volatile he ma;
    @NonNull
    private final File mb;

    @Nullable
    @WorkerThread
    public static he N(@NonNull Context context) {
        he heVar = ma;
        if (heVar == null) {
            synchronized (he.class) {
                heVar = ma;
                if (heVar == null) {
                    File cacheDir = context.getCacheDir();
                    boolean z = true;
                    if (cacheDir != null && !cacheDir.exists()) {
                        z = cacheDir.mkdir();
                    }
                    if (!z) {
                        ah.c("DiskCache: unable to create cache dir");
                        return null;
                    }
                    File file = new File(cacheDir, "mytargetcache");
                    if (!file.exists()) {
                        z = file.mkdir();
                    }
                    if (!z) {
                        ah.c("DiskCache: unable to create cache dir");
                        return null;
                    } else if (file.isDirectory() && file.canWrite()) {
                        he heVar2 = new he(file);
                        ma = heVar2;
                        heVar = heVar2;
                    }
                }
            }
        }
        return heVar;
    }

    @Nullable
    public static he dO() {
        return ma;
    }

    private he(@NonNull File file) {
        this.mb = file;
    }

    @WorkerThread
    public synchronized void sync() {
        long currentTimeMillis = System.currentTimeMillis();
        try {
            if (this.mb.lastModified() + 604800000 < currentTimeMillis) {
                File[] listFiles = this.mb.listFiles(lY);
                if (listFiles != null) {
                    for (File file : listFiles) {
                        if (file.isFile() && file.lastModified() + 604800000 < currentTimeMillis) {
                            StringBuilder sb = new StringBuilder();
                            sb.append("DiskCache: remove expired file ");
                            sb.append(file.getPath());
                            ah.a(sb.toString());
                            if (!file.delete()) {
                                StringBuilder sb2 = new StringBuilder();
                                sb2.append("DiskCache: unable to delete file ");
                                sb2.append(file.getAbsolutePath());
                                ah.a(sb2.toString());
                            }
                        }
                    }
                }
                if (!this.mb.setLastModified(currentTimeMillis)) {
                    StringBuilder sb3 = new StringBuilder();
                    sb3.append("DiskCache: unable to set last modified to dir ");
                    sb3.append(this.mb.getAbsolutePath());
                    ah.a(sb3.toString());
                }
            }
            File[] listFiles2 = this.mb.listFiles(lZ);
            if (listFiles2 != null && listFiles2.length > 10) {
                Arrays.sort(listFiles2, new Comparator<File>() {
                    /* renamed from: a */
                    public int compare(File file, File file2) {
                        return Long.valueOf(file2.lastModified()).compareTo(Long.valueOf(file.lastModified()));
                    }
                });
                for (int length = listFiles2.length - 1; length >= 10; length--) {
                    String path = listFiles2[length].getPath();
                    StringBuilder sb4 = new StringBuilder();
                    sb4.append("DiskCache: remove redundant video ");
                    sb4.append(path);
                    ah.a(sb4.toString());
                    if (!listFiles2[length].delete()) {
                        StringBuilder sb5 = new StringBuilder();
                        sb5.append("DiskCache: unable to remove file ");
                        sb5.append(path);
                        ah.a(sb5.toString());
                    }
                }
            }
        } catch (Exception e) {
            StringBuilder sb6 = new StringBuilder();
            sb6.append("DiskCache exception: ");
            sb6.append(e);
            ah.c(sb6.toString());
        }
        return;
    }

    /* JADX WARNING: Removed duplicated region for block: B:23:0x0063 A[SYNTHETIC, Splitter:B:23:0x0063] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0081 A[SYNTHETIC, Splitter:B:32:0x0081] */
    @Nullable
    @WorkerThread
    public synchronized File a(@NonNull InputStream inputStream, @NonNull String str) {
        File i;
        OutputStream outputStream;
        sync();
        i = i(str, ".img");
        StringBuilder sb = new StringBuilder();
        sb.append("DiskCache save image: ");
        sb.append(i.getPath());
        ah.a(sb.toString());
        try {
            outputStream = new FileOutputStream(i);
            try {
                a(inputStream, outputStream);
                try {
                    outputStream.close();
                } catch (Exception e) {
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("DiskCache exception: ");
                    sb2.append(e);
                    ah.c(sb2.toString());
                }
            } catch (Exception e2) {
                e = e2;
                try {
                    StringBuilder sb3 = new StringBuilder();
                    sb3.append("DiskCache exception: ");
                    sb3.append(e);
                    ah.c(sb3.toString());
                    if (outputStream != null) {
                    }
                    return null;
                } catch (Throwable th) {
                    th = th;
                    if (outputStream != null) {
                    }
                    throw th;
                }
            }
        } catch (Exception e3) {
            e = e3;
            outputStream = null;
            StringBuilder sb32 = new StringBuilder();
            sb32.append("DiskCache exception: ");
            sb32.append(e);
            ah.c(sb32.toString());
            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (Exception e4) {
                    StringBuilder sb4 = new StringBuilder();
                    sb4.append("DiskCache exception: ");
                    sb4.append(e4);
                    ah.c(sb4.toString());
                }
            }
            return null;
        } catch (Throwable th2) {
            th = th2;
            outputStream = null;
            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (Exception e5) {
                    StringBuilder sb5 = new StringBuilder();
                    sb5.append("DiskCache exception: ");
                    sb5.append(e5);
                    ah.c(sb5.toString());
                }
            }
            throw th;
        }
        return i;
    }

    /* JADX WARNING: Removed duplicated region for block: B:23:0x0063 A[SYNTHETIC, Splitter:B:23:0x0063] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0081 A[SYNTHETIC, Splitter:B:32:0x0081] */
    @Nullable
    @WorkerThread
    public synchronized File b(@NonNull InputStream inputStream, @NonNull String str) {
        File i;
        OutputStream outputStream;
        sync();
        i = i(str, ".mp4");
        StringBuilder sb = new StringBuilder();
        sb.append("DiskCache save video: ");
        sb.append(i.getPath());
        ah.a(sb.toString());
        try {
            outputStream = new FileOutputStream(i);
            try {
                a(inputStream, outputStream);
                try {
                    outputStream.close();
                } catch (IOException e) {
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("DiskCache exception: ");
                    sb2.append(e);
                    ah.c(sb2.toString());
                }
            } catch (Exception e2) {
                e = e2;
                try {
                    StringBuilder sb3 = new StringBuilder();
                    sb3.append("DiskCache exception: ");
                    sb3.append(e);
                    ah.c(sb3.toString());
                    if (outputStream != null) {
                    }
                    return null;
                } catch (Throwable th) {
                    th = th;
                    if (outputStream != null) {
                    }
                    throw th;
                }
            }
        } catch (Exception e3) {
            e = e3;
            outputStream = null;
            StringBuilder sb32 = new StringBuilder();
            sb32.append("DiskCache exception: ");
            sb32.append(e);
            ah.c(sb32.toString());
            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (IOException e4) {
                    StringBuilder sb4 = new StringBuilder();
                    sb4.append("DiskCache exception: ");
                    sb4.append(e4);
                    ah.c(sb4.toString());
                }
            }
            return null;
        } catch (Throwable th2) {
            th = th2;
            outputStream = null;
            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (IOException e5) {
                    StringBuilder sb5 = new StringBuilder();
                    sb5.append("DiskCache exception: ");
                    sb5.append(e5);
                    ah.c(sb5.toString());
                }
            }
            throw th;
        }
        return i;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0070, code lost:
        return r6;
     */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x008b A[SYNTHETIC, Splitter:B:23:0x008b] */
    @Nullable
    @WorkerThread
    public synchronized File a(int i, @NonNull String str, boolean z) {
        OutputStreamWriter outputStreamWriter;
        sync();
        File i2 = i(Integer.toString(i), ".json");
        StringBuilder sb = new StringBuilder();
        sb.append("DiskCache save text: ");
        sb.append(i2.getPath());
        ah.a(sb.toString());
        long currentTimeMillis = System.currentTimeMillis();
        if (i2.exists() && z) {
            currentTimeMillis = i2.lastModified();
        }
        try {
            outputStreamWriter = new OutputStreamWriter(new FileOutputStream(i2), Charset.forName("UTF-8").newEncoder());
            try {
                outputStreamWriter.write(str);
                outputStreamWriter.close();
                if (!i2.setLastModified(currentTimeMillis)) {
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("DiskCache: unable to set last modified to file ");
                    sb2.append(i2.getPath());
                    ah.a(sb2.toString());
                }
            } catch (Exception e) {
                e = e;
                StringBuilder sb3 = new StringBuilder();
                sb3.append("DiskCache exception: ");
                sb3.append(e);
                ah.c(sb3.toString());
                if (outputStreamWriter != null) {
                    try {
                        outputStreamWriter.close();
                    } catch (IOException unused) {
                    }
                }
                return null;
            }
        } catch (Exception e2) {
            e = e2;
            outputStreamWriter = null;
            StringBuilder sb32 = new StringBuilder();
            sb32.append("DiskCache exception: ");
            sb32.append(e);
            ah.c(sb32.toString());
            if (outputStreamWriter != null) {
            }
            return null;
        }
    }

    @Nullable
    @WorkerThread
    public String af(@NonNull String str) {
        return h(str, ".mp4");
    }

    @Nullable
    @WorkerThread
    public String ag(@NonNull String str) {
        return h(str, ".img");
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(2:18|19) */
    /* JADX WARNING: Code restructure failed: missing block: B:19:?, code lost:
        r4 = new java.lang.StringBuilder();
        r4.append("DiskCache OOME, called twice: ");
        r4.append(r0);
        com.my.target.ah.c(r4.toString());
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:18:0x0063 */
    @Nullable
    @WorkerThread
    public synchronized Bitmap getBitmap(@NonNull String str) {
        sync();
        File i = i(str, ".img");
        if (i.exists()) {
            StringBuilder sb = new StringBuilder();
            sb.append("DiskCache get image: ");
            sb.append(i.getPath());
            ah.a(sb.toString());
            try {
                return BitmapFactory.decodeFile(i.getAbsolutePath());
            } catch (OutOfMemoryError e) {
                System.gc();
                ah.c("DiskCache OOME, trying once again");
                Options options = new Options();
                options.inSampleSize = 2;
                return BitmapFactory.decodeFile(i.getAbsolutePath(), options);
            } catch (Exception e2) {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("DiskCache exception: ");
                sb2.append(e2);
                ah.c(sb2.toString());
            }
        }
        return null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0060, code lost:
        return null;
     */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x0107 A[SYNTHETIC, Splitter:B:48:0x0107] */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x0121 A[SYNTHETIC, Splitter:B:55:0x0121] */
    @Nullable
    @WorkerThread
    public synchronized String a(int i, long j) {
        FileInputStream fileInputStream;
        String sb;
        String sb2;
        sync();
        File i2 = i(Integer.toString(i), ".json");
        if (i2.exists()) {
            if (!i2.isFile() || i2.lastModified() + j >= System.currentTimeMillis()) {
                StringBuilder sb3 = new StringBuilder();
                sb3.append("DiskCache get text: ");
                sb3.append(i2.getPath());
                ah.a(sb3.toString());
                try {
                    fileInputStream = new FileInputStream(i2);
                    try {
                        StringBuilder sb4 = new StringBuilder();
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(fileInputStream, "UTF-8"));
                        char[] cArr = new char[1024];
                        while (true) {
                            int read = bufferedReader.read(cArr);
                            if (read == -1) {
                                break;
                            }
                            sb4.append(new String(cArr, 0, read));
                        }
                        if (sb4.length() == 0) {
                            ah.a("DiskCache error: cache file is empty");
                            try {
                                fileInputStream.close();
                            } catch (IOException e) {
                                StringBuilder sb5 = new StringBuilder();
                                sb5.append("DiskCache exception: ");
                                sb5.append(e);
                                sb = sb5.toString();
                                ah.c(sb);
                                return null;
                            }
                        } else {
                            sb2 = sb4.toString();
                            try {
                                fileInputStream.close();
                            } catch (IOException e2) {
                                StringBuilder sb6 = new StringBuilder();
                                sb6.append("DiskCache exception: ");
                                sb6.append(e2);
                                ah.c(sb6.toString());
                            }
                        }
                    } catch (Exception e3) {
                        e = e3;
                        try {
                            StringBuilder sb7 = new StringBuilder();
                            sb7.append("DiskCache exception: ");
                            sb7.append(e);
                            ah.c(sb7.toString());
                            if (fileInputStream != null) {
                            }
                            return null;
                        } catch (Throwable th) {
                            th = th;
                            if (fileInputStream != null) {
                            }
                            throw th;
                        }
                    }
                } catch (Exception e4) {
                    e = e4;
                    fileInputStream = null;
                    StringBuilder sb72 = new StringBuilder();
                    sb72.append("DiskCache exception: ");
                    sb72.append(e);
                    ah.c(sb72.toString());
                    if (fileInputStream != null) {
                        try {
                            fileInputStream.close();
                        } catch (IOException e5) {
                            StringBuilder sb8 = new StringBuilder();
                            sb8.append("DiskCache exception: ");
                            sb8.append(e5);
                            sb = sb8.toString();
                            ah.c(sb);
                            return null;
                        }
                    }
                    return null;
                } catch (Throwable th2) {
                    th = th2;
                    fileInputStream = null;
                    if (fileInputStream != null) {
                        try {
                            fileInputStream.close();
                        } catch (IOException e6) {
                            StringBuilder sb9 = new StringBuilder();
                            sb9.append("DiskCache exception: ");
                            sb9.append(e6);
                            ah.c(sb9.toString());
                        }
                    }
                    throw th;
                }
            } else {
                StringBuilder sb10 = new StringBuilder();
                sb10.append("DiskCache: remove expired file ");
                sb10.append(i2.getPath());
                ah.a(sb10.toString());
                if (!i2.delete()) {
                    StringBuilder sb11 = new StringBuilder();
                    sb11.append("DiskCache: unable to delete file ");
                    sb11.append(i2.getAbsolutePath());
                    ah.a(sb11.toString());
                }
            }
        }
        return sb2;
    }

    @Nullable
    @WorkerThread
    private synchronized String h(@NonNull String str, @NonNull String str2) {
        sync();
        File i = i(str, str2);
        if (i.exists()) {
            StringBuilder sb = new StringBuilder();
            sb.append("DiskCache get path: ");
            sb.append(i.getPath());
            ah.a(sb.toString());
            try {
                return i.getAbsolutePath();
            } catch (Exception e) {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("DiskCache exception: ");
                sb2.append(e);
                ah.c(sb2.toString());
            }
        }
        return null;
    }

    private int a(@NonNull InputStream inputStream, @NonNull OutputStream outputStream) throws Exception {
        byte[] bArr = new byte[8192];
        BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream, 8192);
        BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(outputStream, 8192);
        int i = 0;
        while (true) {
            try {
                int read = bufferedInputStream.read(bArr, 0, 8192);
                if (read == -1) {
                    break;
                }
                bufferedOutputStream.write(bArr, 0, read);
                i += read;
            } finally {
                try {
                    bufferedOutputStream.close();
                } catch (IOException e) {
                    ah.a(e.getMessage());
                }
                try {
                    bufferedInputStream.close();
                } catch (IOException e2) {
                    ah.a(e2.getMessage());
                }
            }
        }
        bufferedOutputStream.flush();
        try {
        } catch (IOException e3) {
            ah.a(e3.getMessage());
        }
        try {
            bufferedInputStream.close();
        } catch (IOException e4) {
            ah.a(e4.getMessage());
        }
        return i;
    }

    @NonNull
    private File i(@NonNull String str, @NonNull String str2) {
        StringBuilder sb = new StringBuilder();
        sb.append("mytrg_");
        sb.append(hf.ah(str));
        sb.append(str2);
        String sb2 = sb.toString();
        StringBuilder sb3 = new StringBuilder();
        sb3.append(this.mb.getAbsolutePath());
        sb3.append(File.separator);
        sb3.append(sb2);
        return new File(sb3.toString());
    }
}
