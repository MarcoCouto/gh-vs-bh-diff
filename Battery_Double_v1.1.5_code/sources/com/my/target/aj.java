package com.my.target;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.helper.ItemTouchHelper.Callback;
import com.my.target.common.models.AudioData;
import com.my.target.instreamads.InstreamAudioAdPlayer;
import com.my.target.instreamads.InstreamAudioAdPlayer.AdPlayerListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/* compiled from: InstreamAdAudioController */
public class aj {
    /* access modifiers changed from: private */
    @NonNull
    public final hk A = hk.C(Callback.DEFAULT_DRAG_ANIMATION_DURATION);
    /* access modifiers changed from: private */
    @NonNull
    public final c B = new c();
    @NonNull
    private final Stack<df> C = new Stack<>();
    /* access modifiers changed from: private */
    @Nullable
    public b D;
    /* access modifiers changed from: private */
    @Nullable
    public cn<AudioData> E;
    private int F;
    private float G;
    private int H;
    /* access modifiers changed from: private */
    public boolean I;
    /* access modifiers changed from: private */
    public int J = 0;
    /* access modifiers changed from: private */
    @Nullable
    public InstreamAudioAdPlayer player;
    /* access modifiers changed from: private */
    public float volume = 1.0f;
    @NonNull
    private final a z = new a();

    /* compiled from: InstreamAdAudioController */
    class a implements AdPlayerListener {
        private float volume;

        private a() {
            this.volume = 1.0f;
        }

        public void onAdAudioStarted() {
            aj.this.J = 1;
            if (!aj.this.I && aj.this.player != null) {
                aj.this.b(aj.this.player.getAdAudioDuration());
            }
            aj.this.A.d(aj.this.B);
        }

        public void onAdAudioPaused() {
            Context context = aj.this.getContext();
            if (!(aj.this.E == null || context == null)) {
                hl.a((List<dg>) aj.this.E.getStatHolder().N("playbackPaused"), context);
            }
            aj.this.A.e(aj.this.B);
        }

        public void onAdAudioResumed() {
            Context context = aj.this.getContext();
            if (!(aj.this.E == null || context == null)) {
                hl.a((List<dg>) aj.this.E.getStatHolder().N("playbackResumed"), context);
            }
            aj.this.A.d(aj.this.B);
        }

        public void onAdAudioStopped() {
            if (aj.this.J == 1) {
                if (!(aj.this.E == null || aj.this.D == null)) {
                    Context context = aj.this.getContext();
                    if (context != null) {
                        hl.a((List<dg>) aj.this.E.getStatHolder().N("playbackStopped"), context);
                    }
                    aj.this.D.c(aj.this.E);
                }
                aj.this.J = 0;
            }
            aj.this.A.e(aj.this.B);
        }

        public void onAdAudioError(@NonNull String str) {
            if (!(aj.this.E == null || aj.this.D == null)) {
                aj.this.D.a(str, aj.this.E);
            }
            aj.this.A.e(aj.this.B);
        }

        public void onAdAudioCompleted() {
            if (aj.this.J != 2) {
                if (!(aj.this.E == null || aj.this.D == null)) {
                    aj.this.q();
                    cn e = aj.this.E;
                    aj.this.E = null;
                    if (e != null) {
                        aj.this.c(e.getDuration());
                        aj.this.D.d(e);
                    }
                }
                aj.this.J = 2;
            }
            aj.this.A.e(aj.this.B);
        }

        public void onVolumeChanged(float f) {
            if (f != this.volume) {
                if (this.volume > 0.0f && f <= 0.0f) {
                    Context context = aj.this.getContext();
                    if (!(context == null || aj.this.E == null)) {
                        hl.a((List<dg>) aj.this.E.getStatHolder().N("volumeOff"), context);
                        this.volume = f;
                        aj.this.volume = f;
                    }
                } else if (this.volume == 0.0f && f > 0.0f) {
                    Context context2 = aj.this.getContext();
                    if (!(context2 == null || aj.this.E == null)) {
                        hl.a((List<dg>) aj.this.E.getStatHolder().N("volumeOn"), context2);
                        this.volume = f;
                        aj.this.volume = f;
                    }
                }
            }
        }
    }

    /* compiled from: InstreamAdAudioController */
    public interface b {
        void a(float f, float f2, @NonNull cn cnVar);

        void a(@NonNull String str, @NonNull cn cnVar);

        void b(@NonNull cn cnVar);

        void c(@NonNull cn cnVar);

        void d(@NonNull cn cnVar);
    }

    /* compiled from: InstreamAdAudioController */
    class c implements Runnable {
        private c() {
        }

        public void run() {
            aj.this.q();
        }
    }

    @NonNull
    public static aj p() {
        return new aj();
    }

    private aj() {
    }

    public void setVolume(float f) {
        if (this.player != null) {
            this.player.setVolume(f);
        }
        this.volume = f;
    }

    @Nullable
    public InstreamAudioAdPlayer getPlayer() {
        return this.player;
    }

    public void setPlayer(@Nullable InstreamAudioAdPlayer instreamAudioAdPlayer) {
        if (this.player != null) {
            this.player.setAdPlayerListener(null);
        }
        this.player = instreamAudioAdPlayer;
        if (instreamAudioAdPlayer != null) {
            instreamAudioAdPlayer.setAdPlayerListener(this.z);
        }
    }

    public void a(@Nullable b bVar) {
        this.D = bVar;
    }

    @Nullable
    public Context getContext() {
        if (this.player == null) {
            return null;
        }
        return this.player.getCurrentContext();
    }

    public void a(@NonNull cn<AudioData> cnVar) {
        this.E = cnVar;
        this.I = false;
        cnVar.getStatHolder().a(this.C);
        AudioData audioData = (AudioData) cnVar.getMediaData();
        if (audioData != null) {
            Uri parse = Uri.parse(audioData.getUrl());
            if (this.player != null) {
                this.player.setVolume(this.volume);
                this.player.playAdAudio(parse);
            }
        }
    }

    public void pause() {
        if (this.player != null) {
            this.player.pauseAdAudio();
        }
    }

    public void resume() {
        if (this.player != null) {
            this.player.resumeAdAudio();
        }
    }

    public void stop() {
        if (this.J == 1) {
            if (!(this.E == null || this.D == null)) {
                Context context = getContext();
                if (context != null) {
                    hl.a((List<dg>) this.E.getStatHolder().N("playbackStopped"), context);
                }
                this.D.c(this.E);
            }
            this.J = 0;
        }
        if (this.player != null) {
            this.player.stopAdAudio();
        }
    }

    public void destroy() {
        if (this.player != null) {
            this.player.destroy();
        }
        this.player = null;
    }

    public void setConnectionTimeout(int i) {
        this.H = i;
    }

    public float getVolume() {
        return this.volume;
    }

    /* access modifiers changed from: private */
    public void q() {
        float f;
        float f2;
        float f3;
        float duration = this.E != null ? this.E.getDuration() : 0.0f;
        if (this.E == null) {
            this.A.e(this.B);
            return;
        }
        if (this.J != 1 || this.player == null) {
            f3 = 0.0f;
            f2 = 0.0f;
            f = 0.0f;
        } else {
            f3 = this.player.getAdAudioDuration();
            f2 = this.player.getAdAudioPosition();
            f = duration - f2;
        }
        if (this.J != 1 || this.G == f2 || f3 <= 0.0f) {
            this.F++;
        } else {
            a(f, f2, duration);
        }
        if (this.F >= (this.H * 1000) / Callback.DEFAULT_DRAG_ANIMATION_DURATION) {
            r();
        }
    }

    private void a(float f, float f2, float f3) {
        this.F = 0;
        this.G = f2;
        if (f2 < f3) {
            c(f2);
            if (this.D != null && this.E != null) {
                this.D.a(f, f3, this.E);
                return;
            }
            return;
        }
        a(f3);
    }

    private void a(float f) {
        c(f);
        if (!(this.D == null || this.E == null)) {
            this.D.a(0.0f, f, this.E);
        }
        s();
    }

    private void r() {
        StringBuilder sb = new StringBuilder();
        sb.append("video freeze more then ");
        sb.append(this.H);
        sb.append(" seconds, stopping");
        ah.a(sb.toString());
        this.A.e(this.B);
        if (this.D != null && this.E != null) {
            this.D.a("Timeout", this.E);
        }
    }

    /* access modifiers changed from: private */
    public void b(float f) {
        if (!(this.E == null || this.D == null)) {
            this.D.b(this.E);
        }
        Context context = getContext();
        if (!(context == null || this.E == null)) {
            hl.a((List<dg>) this.E.getStatHolder().N("playbackStarted"), context);
        }
        if (!(this.D == null || this.E == null)) {
            this.D.a(0.0f, f, this.E);
        }
        c(0.0f);
        this.I = true;
    }

    private void s() {
        this.A.e(this.B);
        if (this.J != 2) {
            this.J = 2;
            if (this.player != null) {
                this.player.stopAdAudio();
            }
            if (this.E != null && this.D != null) {
                cn<AudioData> cnVar = this.E;
                this.E = null;
                this.D.d(cnVar);
            }
        }
    }

    /* access modifiers changed from: private */
    public void c(float f) {
        ArrayList arrayList = new ArrayList();
        while (this.C.size() > 0 && ((df) this.C.peek()).cf() <= f) {
            arrayList.add(this.C.pop());
        }
        if (this.player != null) {
            hl.a((List<dg>) arrayList, this.player.getCurrentContext());
        }
    }
}
