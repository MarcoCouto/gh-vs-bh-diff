package com.my.target;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.my.target.common.models.ImageData;

/* compiled from: AdBanner */
public abstract class cg {
    @Nullable
    protected by adChoices;
    @NonNull
    protected String advertisingLabel = "";
    @NonNull
    protected String ageRestrictions = "";
    @Nullable
    protected String bundleId;
    @NonNull
    protected String category = "";
    @NonNull
    protected ca clickArea = ca.cZ;
    @Nullable
    protected String ctaText;
    @Nullable
    protected String deeplink;
    @NonNull
    protected String description = "";
    protected boolean directLink = false;
    @NonNull
    protected String disclaimer = "";
    @NonNull
    protected String domain = "";
    protected float duration;
    protected int height;
    @Nullable
    protected ImageData icon;
    @NonNull
    protected String id = "";
    @Nullable
    protected ImageData image;
    @NonNull
    protected String navigationType = "web";
    protected boolean openInBrowser = false;
    protected float rating;
    @NonNull
    private final dh statHolder = dh.ch();
    @NonNull
    protected String subCategory = "";
    @NonNull
    protected String title = "";
    @Nullable
    protected String trackingLink;
    @NonNull
    protected String type = "";
    @Nullable
    protected String urlscheme;
    protected boolean usePlayStoreAction;
    protected int votes;
    protected int width;

    protected cg() {
    }

    public void setIcon(@Nullable ImageData imageData) {
        this.icon = imageData;
    }

    public void setImage(@Nullable ImageData imageData) {
        this.image = imageData;
    }

    public void setType(@NonNull String str) {
        this.type = str;
    }

    @NonNull
    public String getAgeRestrictions() {
        return this.ageRestrictions;
    }

    public void setAgeRestrictions(@NonNull String str) {
        this.ageRestrictions = str;
    }

    @Nullable
    public String getBundleId() {
        return this.bundleId;
    }

    public void setBundleId(@Nullable String str) {
        this.bundleId = str;
    }

    @NonNull
    public String getCategory() {
        return this.category;
    }

    public void setCategory(@NonNull String str) {
        this.category = str;
    }

    @NonNull
    public String getCtaText() {
        if (this.ctaText == null) {
            return "store".equals(this.navigationType) ? "Install" : "Visit";
        }
        return this.ctaText;
    }

    public void setCtaText(@NonNull String str) {
        this.ctaText = str;
    }

    @Nullable
    public String getDeeplink() {
        return this.deeplink;
    }

    public void setDeeplink(@Nullable String str) {
        this.deeplink = str;
    }

    @NonNull
    public String getDescription() {
        return this.description;
    }

    public void setDescription(@NonNull String str) {
        this.description = str;
    }

    @NonNull
    public String getDisclaimer() {
        return this.disclaimer;
    }

    public void setDisclaimer(@NonNull String str) {
        this.disclaimer = str;
    }

    @NonNull
    public String getDomain() {
        return this.domain;
    }

    public void setDomain(@NonNull String str) {
        this.domain = str;
    }

    public int getHeight() {
        return this.height;
    }

    public void setHeight(int i) {
        this.height = i;
    }

    @Nullable
    public ImageData getIcon() {
        return this.icon;
    }

    @NonNull
    public String getId() {
        return this.id;
    }

    public void setId(@NonNull String str) {
        this.id = str;
    }

    @Nullable
    public ImageData getImage() {
        return this.image;
    }

    @NonNull
    public String getNavigationType() {
        return this.navigationType;
    }

    public void setNavigationType(@NonNull String str) {
        this.navigationType = str;
    }

    public float getRating() {
        return this.rating;
    }

    public void setRating(float f) {
        this.rating = f;
    }

    @NonNull
    public String getSubCategory() {
        return this.subCategory;
    }

    public void setSubCategory(@NonNull String str) {
        this.subCategory = str;
    }

    @NonNull
    public String getTitle() {
        return this.title;
    }

    public void setTitle(@NonNull String str) {
        this.title = str;
    }

    @Nullable
    public String getTrackingLink() {
        return this.trackingLink;
    }

    public void setTrackingLink(@Nullable String str) {
        this.trackingLink = str;
    }

    @NonNull
    public String getType() {
        return this.type;
    }

    @Nullable
    public String getUrlscheme() {
        return this.urlscheme;
    }

    public void setUrlscheme(@Nullable String str) {
        this.urlscheme = str;
    }

    public int getVotes() {
        return this.votes;
    }

    public void setVotes(int i) {
        this.votes = i;
    }

    public int getWidth() {
        return this.width;
    }

    public void setWidth(int i) {
        this.width = i;
    }

    public boolean isDirectLink() {
        return this.directLink;
    }

    public void setDirectLink(boolean z) {
        this.directLink = z;
    }

    public boolean isOpenInBrowser() {
        return this.openInBrowser;
    }

    public void setOpenInBrowser(boolean z) {
        this.openInBrowser = z;
    }

    public boolean isUsePlayStoreAction() {
        return this.usePlayStoreAction;
    }

    public void setUsePlayStoreAction(boolean z) {
        this.usePlayStoreAction = z;
    }

    @NonNull
    public dh getStatHolder() {
        return this.statHolder;
    }

    public void setAdvertisingLabel(@NonNull String str) {
        this.advertisingLabel = str;
    }

    @NonNull
    public String getAdvertisingLabel() {
        return this.advertisingLabel;
    }

    public float getDuration() {
        return this.duration;
    }

    public void setDuration(float f) {
        this.duration = f;
    }

    public void setClickArea(@NonNull ca caVar) {
        this.clickArea = caVar;
    }

    @NonNull
    public ca getClickArea() {
        return this.clickArea;
    }

    public void setAdChoices(@Nullable by byVar) {
        this.adChoices = byVar;
    }

    @Nullable
    public by getAdChoices() {
        return this.adChoices;
    }
}
