package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.tapjoy.TJAdUnitConstants.String;
import org.json.JSONObject;

/* compiled from: StandardAdBannerParser */
public class ej {
    @NonNull
    private final a adConfig;
    @NonNull
    private final Context context;
    @NonNull
    private final bz eq;
    @NonNull
    private final dv er;

    @NonNull
    public static ej j(@NonNull bz bzVar, @NonNull a aVar, @NonNull Context context2) {
        return new ej(bzVar, aVar, context2);
    }

    private ej(@NonNull bz bzVar, @NonNull a aVar, @NonNull Context context2) {
        this.eq = bzVar;
        this.adConfig = aVar;
        this.context = context2;
        this.er = dv.b(bzVar, aVar, context2);
    }

    public boolean a(@NonNull JSONObject jSONObject, @NonNull cr crVar, @Nullable String str) {
        String str2;
        this.er.a(jSONObject, (cg) crVar);
        if (jSONObject.has("timeout")) {
            int optInt = jSONObject.optInt("timeout");
            if (optInt >= 5) {
                crVar.setTimeout(optInt);
            } else {
                StringBuilder sb = new StringBuilder();
                sb.append("Wrong banner timeout: ");
                sb.append(optInt);
                b("Required field", sb.toString(), crVar.getId());
            }
        }
        if (!crVar.getType().equals(String.HTML)) {
            return true;
        }
        String optString = jSONObject.optString("source", null);
        if (optString == null) {
            dp.P("Required field").Q("Banner with type 'html' has no source field").S(crVar.getId()).R(this.eq.getUrl()).r(this.adConfig.getSlotId()).q(this.context);
            return false;
        }
        String decode = hn.decode(optString);
        if (!TextUtils.isEmpty(str)) {
            crVar.setMraidJs(str);
            str2 = dv.g(str, decode);
            if (str2 != null) {
                crVar.setMraidSource(str2);
                crVar.setType(CampaignEx.JSON_KEY_MRAID);
                return this.er.a(str2, jSONObject);
            }
        }
        str2 = decode;
        return this.er.a(str2, jSONObject);
    }

    private void b(@NonNull String str, @NonNull String str2, @NonNull String str3) {
        dp.P(str).Q(str2).r(this.adConfig.getSlotId()).S(str3).R(this.eq.getUrl()).q(this.context);
    }
}
