package com.my.target;

import android.os.Handler;
import android.os.Looper;
import android.support.annotation.AnyThread;
import android.support.annotation.NonNull;
import java.util.HashSet;
import java.util.Iterator;
import java.util.WeakHashMap;

/* compiled from: Repeater */
public class hk {
    @NonNull
    private static final Handler handler = new Handler(Looper.getMainLooper());
    @NonNull
    public static final hk mn = new hk(1000);
    private final int mo;
    @NonNull
    private final WeakHashMap<Runnable, Boolean> mp = new WeakHashMap<>();
    @NonNull
    private final Runnable mq = new Runnable() {
        public void run() {
            hk.this.dT();
        }
    };

    @NonNull
    public static final hk C(int i) {
        return new hk(i);
    }

    private hk(int i) {
        this.mo = i;
    }

    @AnyThread
    public void d(@NonNull Runnable runnable) {
        synchronized (this) {
            int size = this.mp.size();
            if (this.mp.put(runnable, Boolean.valueOf(true)) == null && size == 0) {
                dU();
            }
        }
    }

    @AnyThread
    public void e(@NonNull Runnable runnable) {
        synchronized (this) {
            this.mp.remove(runnable);
            if (this.mp.size() == 0) {
                handler.removeCallbacks(this.mq);
            }
        }
    }

    /* access modifiers changed from: private */
    public void dT() {
        synchronized (this) {
            Iterator it = new HashSet(this.mp.keySet()).iterator();
            while (it.hasNext()) {
                ((Runnable) it.next()).run();
            }
            if (this.mp.keySet().size() > 0) {
                dU();
            }
        }
    }

    private void dU() {
        handler.postDelayed(this.mq, (long) this.mo);
    }
}
