package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.ViewGroup;
import android.widget.RelativeLayout.LayoutParams;
import com.explorestack.iab.vast.VastError;
import com.google.android.gms.dynamite.descriptors.com.google.android.gms.ads.dynamite.ModuleDescriptor;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.my.target.ew.c;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* compiled from: StandardAdEngine */
public class bd implements ap {
    @NonNull
    private final ArrayList<df> aV = new ArrayList<>();
    @NonNull
    private final a adConfig;
    /* access modifiers changed from: private */
    @NonNull
    public final ViewGroup bJ;
    @NonNull
    private final cr bK;
    @NonNull
    private final com.my.target.ex.a bL = new a();
    @Nullable
    private ex bM;
    /* access modifiers changed from: private */
    @Nullable
    public com.my.target.ap.a by;
    @NonNull
    private final Context context;
    @NonNull
    private final dc section;

    /* compiled from: StandardAdEngine */
    class a implements com.my.target.ex.a {
        a() {
        }

        public void a(@NonNull cg cgVar, @Nullable String str) {
            if (bd.this.by != null) {
                bd.this.by.onClick();
            }
            hd dM = hd.dM();
            if (TextUtils.isEmpty(str)) {
                dM.b(cgVar, bd.this.bJ.getContext());
            } else {
                dM.c(cgVar, str, bd.this.bJ.getContext());
            }
        }

        public void a(@NonNull cg cgVar) {
            hl.a((List<dg>) cgVar.getStatHolder().N("playbackStarted"), bd.this.bJ.getContext());
            if (bd.this.by != null) {
                bd.this.by.ab();
            }
        }
    }

    /* compiled from: StandardAdEngine */
    static class b implements c {
        private bd bO;

        public b(bd bdVar) {
            this.bO = bdVar;
        }

        public void aa() {
            this.bO.aa();
        }

        public void e(@NonNull String str) {
            this.bO.e(str);
        }

        public void ac() {
            this.bO.ac();
        }

        public void ad() {
            this.bO.ad();
        }

        public void a(@NonNull String str, @NonNull cr crVar, @NonNull Context context) {
            this.bO.a(str, crVar, context);
        }

        public void a(float f, float f2, @NonNull cr crVar, @NonNull Context context) {
            this.bO.a(f, f2, context);
        }
    }

    @NonNull
    public static bd a(@NonNull ViewGroup viewGroup, @NonNull cr crVar, @NonNull dc dcVar, @NonNull a aVar) {
        return new bd(viewGroup, crVar, dcVar, aVar);
    }

    public void a(@Nullable com.my.target.ap.a aVar) {
        this.by = aVar;
    }

    private bd(@NonNull ViewGroup viewGroup, @NonNull cr crVar, @NonNull dc dcVar, @NonNull a aVar) {
        this.bJ = viewGroup;
        this.bK = crVar;
        this.section = dcVar;
        this.adConfig = aVar;
        this.context = viewGroup.getContext();
        this.aV.addAll(crVar.getStatHolder().ck());
    }

    public void prepare() {
        if (CampaignEx.JSON_KEY_MRAID.equals(this.bK.getType())) {
            av();
        } else {
            aw();
        }
    }

    public void destroy() {
        if (this.bM != null) {
            this.bM.destroy();
            this.bM = null;
        }
    }

    public void start() {
        if (this.bM != null) {
            this.bM.start();
        }
    }

    public void resume() {
        if (this.bM != null) {
            this.bM.resume();
        }
    }

    public void pause() {
        if (this.bM != null) {
            this.bM.pause();
        }
    }

    public void stop() {
        if (this.bM != null) {
            this.bM.stop();
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(float f, float f2, @NonNull Context context2) {
        if (!this.aV.isEmpty()) {
            float f3 = f2 - f;
            ArrayList arrayList = new ArrayList();
            Iterator it = this.aV.iterator();
            while (it.hasNext()) {
                df dfVar = (df) it.next();
                float cf = dfVar.cf();
                if (cf < 0.0f && dfVar.cg() >= 0.0f) {
                    cf = (f2 / 100.0f) * dfVar.cg();
                }
                if (cf >= 0.0f && cf <= f3) {
                    arrayList.add(dfVar);
                    it.remove();
                }
            }
            hl.a((List<dg>) arrayList, context2);
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(String str, cr crVar, Context context2) {
        hl.a((List<dg>) crVar.getStatHolder().N(str), context2);
    }

    private void av() {
        ew ewVar;
        String format = this.adConfig.getFormat();
        if (this.bM instanceof ew) {
            ewVar = (ew) this.bM;
        } else {
            if (this.bM != null) {
                this.bM.a(null);
                this.bM.destroy();
            }
            ew f = ew.f(this.bJ);
            f.a(this.bL);
            this.bM = f;
            a(f.db(), format);
            ewVar = f;
        }
        ewVar.a((c) new b(this));
        ewVar.a(this.bK);
    }

    private void aw() {
        ey eyVar;
        String format = this.adConfig.getFormat();
        if (this.bM instanceof ey) {
            eyVar = (ey) this.bM;
        } else {
            if (this.bM != null) {
                this.bM.a(null);
                this.bM.destroy();
            }
            ey a2 = ey.a(format, this.section, this.context);
            a2.a(this.bL);
            this.bM = a2;
            a(a2.db(), format);
            eyVar = a2;
        }
        eyVar.a((com.my.target.ey.a) new com.my.target.ey.a() {
            public void aa() {
                if (bd.this.by != null) {
                    bd.this.by.aa();
                }
            }

            public void e(@NonNull String str) {
                if (bd.this.by != null) {
                    bd.this.by.e(str);
                }
            }
        });
        eyVar.a(this.bK);
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0032  */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x004e  */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x005e  */
    private void a(@NonNull fs fsVar, @NonNull String str) {
        char c;
        hm R = hm.R(this.bJ.getContext());
        int hashCode = str.hashCode();
        if (hashCode != -1476994234) {
            if (hashCode == -1177968780 && str.equals("standard_728x90")) {
                c = 1;
                switch (c) {
                    case 0:
                        fsVar.g(R.E(VastError.ERROR_CODE_GENERAL_WRAPPER), R.E(250));
                        break;
                    case 1:
                        fsVar.g(R.E(728), R.E(90));
                        break;
                    default:
                        fsVar.g(R.E(ModuleDescriptor.MODULE_VERSION), R.E(50));
                        fsVar.setFlexibleWidth(true);
                        fsVar.setMaxWidth(R.E(640));
                        break;
                }
                LayoutParams layoutParams = new LayoutParams(-2, -2);
                layoutParams.addRule(13);
                fsVar.setLayoutParams(layoutParams);
                this.bJ.removeAllViews();
                this.bJ.addView(fsVar);
            }
        } else if (str.equals("standard_300x250")) {
            c = 0;
            switch (c) {
                case 0:
                    break;
                case 1:
                    break;
            }
            LayoutParams layoutParams2 = new LayoutParams(-2, -2);
            layoutParams2.addRule(13);
            fsVar.setLayoutParams(layoutParams2);
            this.bJ.removeAllViews();
            this.bJ.addView(fsVar);
        }
        c = 65535;
        switch (c) {
            case 0:
                break;
            case 1:
                break;
        }
        LayoutParams layoutParams22 = new LayoutParams(-2, -2);
        layoutParams22.addRule(13);
        fsVar.setLayoutParams(layoutParams22);
        this.bJ.removeAllViews();
        this.bJ.addView(fsVar);
    }

    /* access modifiers changed from: private */
    public void ad() {
        if (this.by != null) {
            this.by.ad();
        }
    }

    /* access modifiers changed from: private */
    public void ac() {
        if (this.by != null) {
            this.by.ac();
        }
    }

    /* access modifiers changed from: private */
    public void e(@NonNull String str) {
        if (this.by != null) {
            this.by.e(str);
        }
    }

    /* access modifiers changed from: private */
    public void aa() {
        if (this.by != null) {
            this.by.aa();
        }
    }
}
