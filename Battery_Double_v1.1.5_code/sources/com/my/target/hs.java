package com.my.target;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.support.v7.widget.helper.ItemTouchHelper.Callback;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player.EventListener;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.my.target.common.models.VideoData;

/* compiled from: ExoVideoPlayer */
public class hs implements EventListener, hq {
    @NonNull
    private final hk A;
    @Nullable
    private VideoData kk;
    @Nullable
    private com.my.target.hq.a mH;
    @NonNull
    private final SimpleExoPlayer mL;
    @NonNull
    private final a mM;
    private boolean mN;
    @Nullable
    private MediaSource source;
    private boolean started;

    /* compiled from: ExoVideoPlayer */
    public static class a implements Runnable {
        @Nullable
        private com.my.target.hq.a mH;
        @Nullable
        private SimpleExoPlayer mO;

        public void run() {
            if (this.mH != null && this.mO != null) {
                this.mH.a(((float) this.mO.getCurrentPosition()) / 1000.0f, ((float) this.mO.getDuration()) / 1000.0f);
            }
        }

        /* access modifiers changed from: 0000 */
        public void a(@Nullable com.my.target.hq.a aVar) {
            this.mH = aVar;
        }

        /* access modifiers changed from: 0000 */
        public void a(@Nullable SimpleExoPlayer simpleExoPlayer) {
            this.mO = simpleExoPlayer;
        }
    }

    public void onLoadingChanged(boolean z) {
    }

    public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {
    }

    public void onPositionDiscontinuity(int i) {
    }

    public void onRepeatModeChanged(int i) {
    }

    public void onSeekProcessed() {
    }

    public void onShuffleModeEnabledChanged(boolean z) {
    }

    public void onTimelineChanged(Timeline timeline, Object obj, int i) {
    }

    public void onTracksChanged(TrackGroupArray trackGroupArray, TrackSelectionArray trackSelectionArray) {
    }

    public static hs T(@NonNull Context context) {
        return new hs(context);
    }

    @VisibleForTesting
    hs(@NonNull SimpleExoPlayer simpleExoPlayer, @NonNull a aVar) {
        this.A = hk.C(Callback.DEFAULT_DRAG_ANIMATION_DURATION);
        this.mL = simpleExoPlayer;
        this.mM = aVar;
        this.mL.addListener(this);
        aVar.a(this.mL);
    }

    private hs(@NonNull Context context) {
        this(ExoPlayerFactory.newSimpleInstance(context, (TrackSelector) new DefaultTrackSelector()), new a());
    }

    @Nullable
    public VideoData dZ() {
        return this.kk;
    }

    public boolean isMuted() {
        return this.mL.getVolume() == 0.0f;
    }

    public long getPosition() {
        return this.mL.getCurrentPosition();
    }

    public void seekTo(long j) {
        this.mL.seekTo(j);
    }

    public boolean isStarted() {
        return this.started;
    }

    public void a(@Nullable com.my.target.hq.a aVar) {
        this.mH = aVar;
        this.mM.a(aVar);
    }

    public void destroy() {
        this.kk = null;
        this.started = false;
        this.mN = false;
        this.mL.setVideoTextureView(null);
        this.mL.stop();
        this.mL.release();
        this.mL.removeListener(this);
        this.A.e(this.mM);
    }

    public boolean isPaused() {
        return this.started && this.mN;
    }

    public void a(@NonNull VideoData videoData, @NonNull fn fnVar) {
        ah.a("Play video in ExoPlayer");
        this.kk = videoData;
        fnVar.e(videoData.getWidth(), videoData.getHeight());
        this.mN = false;
        if (this.mH != null) {
            this.mH.B();
        }
        this.mL.setVideoTextureView(fnVar.getTextureView());
        if (this.kk != videoData || !this.started) {
            this.source = ht.a(videoData, fnVar.getContext());
            this.mL.prepare(this.source);
        }
        this.mL.setPlayWhenReady(true);
    }

    public void a(@NonNull Uri uri, @NonNull fn fnVar) {
        ah.a("Play video in ExoPlayer");
        this.mN = false;
        if (this.mH != null) {
            this.mH.B();
        }
        this.mL.setVideoTextureView(fnVar.getTextureView());
        if (!this.started) {
            this.source = ht.a(uri, fnVar.getContext());
            this.mL.prepare(this.source);
        }
        this.mL.setPlayWhenReady(true);
    }

    public void stop() {
        this.mL.stop();
    }

    public void K() {
        this.mL.setVolume(0.2f);
    }

    public void L() {
        this.mL.setVolume(0.0f);
        if (this.mH != null) {
            this.mH.e(0.0f);
        }
    }

    public void setVolume(float f) {
        this.mL.setVolume(f);
        if (this.mH != null) {
            this.mH.e(f);
        }
    }

    public void cJ() {
        this.mL.setVolume(1.0f);
        if (this.mH != null) {
            this.mH.e(1.0f);
        }
    }

    public void resume() {
        if (this.started) {
            this.mL.setPlayWhenReady(true);
        } else if (this.source != null) {
            this.mL.prepare(this.source, true, true);
        }
    }

    public void pause() {
        if (this.started && !this.mN) {
            this.mL.setPlayWhenReady(false);
        }
    }

    public float getDuration() {
        return ((float) this.mL.getDuration()) / 1000.0f;
    }

    public boolean isPlaying() {
        return this.started && !this.mN;
    }

    public void onPlayerStateChanged(boolean z, int i) {
        if (i != 1) {
            switch (i) {
                case 3:
                    if (z) {
                        if (this.mH != null) {
                            this.mH.y();
                        }
                        if (!this.started) {
                            this.started = true;
                        } else if (this.mN) {
                            this.mN = false;
                            if (this.mH != null) {
                                this.mH.A();
                            }
                        }
                        this.A.d(this.mM);
                        return;
                    }
                    if (!this.mN && this.mH != null) {
                        this.mN = true;
                        this.mH.z();
                    }
                    this.A.e(this.mM);
                    return;
                case 4:
                    this.mN = false;
                    this.started = false;
                    float duration = ((float) this.mL.getDuration()) / 1000.0f;
                    if (this.mH != null) {
                        this.mH.a(duration, duration);
                        this.mH.onComplete();
                    }
                    this.A.e(this.mM);
                    return;
                default:
                    return;
            }
        } else {
            if (this.started) {
                this.started = false;
                if (this.mH != null) {
                    this.mH.x();
                }
            }
            this.A.e(this.mM);
        }
    }

    public void onPlayerError(ExoPlaybackException exoPlaybackException) {
        this.mN = false;
        this.started = false;
        if (this.mH != null) {
            String message = exoPlaybackException.getMessage();
            if (message == null) {
                message = "Unknown video error";
            }
            this.mH.d(message);
        }
        this.mL.release();
    }
}
