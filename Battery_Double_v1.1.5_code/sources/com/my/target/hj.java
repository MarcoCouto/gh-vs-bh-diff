package com.my.target;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

@SuppressLint({"ApplySharedPref"})
/* compiled from: PrefsCache */
public class hj {
    @Nullable
    private static volatile hj ml;
    @NonNull
    private final SharedPreferences mm;

    @NonNull
    public static hj Q(@NonNull Context context) {
        hj hjVar = ml;
        if (hjVar == null) {
            synchronized (hj.class) {
                hjVar = ml;
                if (hjVar == null) {
                    hjVar = new hj(context.getSharedPreferences("mytarget_prefs", 0));
                    ml = hjVar;
                }
            }
        }
        return hjVar;
    }

    private hj(@NonNull SharedPreferences sharedPreferences) {
        this.mm = sharedPreferences;
    }

    @Nullable
    public String dS() {
        return getString("mrgsDeviceId");
    }

    public void ai(@Nullable String str) {
        putString("mrgsDeviceId", str);
    }

    @NonNull
    private synchronized String getString(@NonNull String str) {
        try {
        } catch (Throwable th) {
            StringBuilder sb = new StringBuilder();
            sb.append("PrefsCache exception: ");
            sb.append(th);
            ah.c(sb.toString());
            return "";
        }
        return this.mm.getString(str, "");
    }

    private synchronized void putString(@NonNull String str, @Nullable String str2) {
        try {
            Editor edit = this.mm.edit();
            edit.putString(str, str2);
            edit.commit();
        } catch (Throwable th) {
            StringBuilder sb = new StringBuilder();
            sb.append("PrefsCache exception: ");
            sb.append(th);
            ah.c(sb.toString());
        }
        return;
    }
}
