package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.URI;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/* compiled from: MyTargetCookieManager */
public final class dq {
    @Nullable
    private static volatile dq ek;
    @NonNull
    private final CookieHandler el;

    @NonNull
    public static dq r(@NonNull Context context) {
        dq dqVar = ek;
        if (dqVar == null) {
            synchronized (dq.class) {
                dqVar = ek;
                if (dqVar == null) {
                    dqVar = new dq(new CookieManager(new dr(context.getApplicationContext()), null));
                    ek = dqVar;
                }
            }
        }
        return dqVar;
    }

    dq(@NonNull CookieManager cookieManager) {
        this.el = cookieManager;
    }

    public void a(@NonNull URLConnection uRLConnection) {
        Map headerFields = uRLConnection.getHeaderFields();
        try {
            this.el.put(URI.create(uRLConnection.getURL().toString()), headerFields);
        } catch (Exception e) {
            StringBuilder sb = new StringBuilder();
            sb.append("unable to set cookies from urlconnection: ");
            sb.append(e.getMessage());
            ah.a(sb.toString());
        }
    }

    public void b(@NonNull URLConnection uRLConnection) {
        HashMap hashMap = new HashMap();
        try {
            a(uRLConnection, this.el.get(URI.create(uRLConnection.getURL().toString()), hashMap));
        } catch (Exception e) {
            StringBuilder sb = new StringBuilder();
            sb.append("unable to set cookies to urlconnection ");
            sb.append(e.getMessage());
            ah.a(sb.toString());
        }
    }

    private void a(@NonNull URLConnection uRLConnection, Map<String, List<String>> map) {
        Iterator it = map.entrySet().iterator();
        for (boolean hasNext = it.hasNext(); hasNext; hasNext = it.hasNext()) {
            Entry entry = (Entry) it.next();
            String str = (String) entry.getKey();
            Iterator it2 = ((List) entry.getValue()).iterator();
            for (boolean hasNext2 = it2.hasNext(); hasNext2; hasNext2 = it2.hasNext()) {
                uRLConnection.addRequestProperty(str, (String) it2.next());
            }
        }
    }
}
