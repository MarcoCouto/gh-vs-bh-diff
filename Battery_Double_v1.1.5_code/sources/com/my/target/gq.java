package com.my.target;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.text.TextUtils.TruncateAt;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import com.my.target.nativeads.views.MediaAdView;

@SuppressLint({"ViewConstructor"})
/* compiled from: VerticalView */
public class gq extends RelativeLayout {
    private static final int CTA_ID = hm.dV();
    private static final int kq = hm.dV();
    private static final int kr = hm.dV();
    private static final int ks = hm.dV();
    @NonNull
    private final fp ageRestrictionLabel;
    @NonNull
    private final Button ctaButton;
    private final boolean jB;
    /* access modifiers changed from: private */
    @NonNull
    public final gi kt;
    @NonNull
    private final gk ku;
    @NonNull
    private final hm uiUtils;

    public gq(@NonNull Context context, @NonNull hm hmVar, boolean z) {
        super(context);
        this.uiUtils = hmVar;
        this.jB = z;
        this.ku = new gk(context, hmVar, z);
        hm.a((View) this.ku, "footer_layout");
        this.kt = new gi(context, hmVar, z);
        hm.a((View) this.kt, "body_layout");
        this.ctaButton = new Button(context);
        hm.a((View) this.ctaButton, "cta_button");
        this.ageRestrictionLabel = new fp(context);
        hm.a((View) this.ageRestrictionLabel, "age_bordering");
    }

    public void a(int i, int i2, boolean z) {
        int i3;
        int max = Math.max(i2, i) / 8;
        this.kt.y(z);
        this.ku.initView();
        View view = new View(getContext());
        view.setBackgroundColor(-5592406);
        view.setLayoutParams(new LayoutParams(-1, 1));
        this.ku.setId(kr);
        this.ku.a(max, z);
        this.ctaButton.setId(CTA_ID);
        this.ctaButton.setPadding(this.uiUtils.E(15), 0, this.uiUtils.E(15), 0);
        this.ctaButton.setMinimumWidth(this.uiUtils.E(100));
        this.ctaButton.setTransformationMethod(null);
        this.ctaButton.setSingleLine();
        this.ctaButton.setEllipsize(TruncateAt.END);
        this.ageRestrictionLabel.setId(kq);
        this.ageRestrictionLabel.f(1, -7829368);
        this.ageRestrictionLabel.setPadding(this.uiUtils.E(2), 0, 0, 0);
        this.ageRestrictionLabel.setTextColor(MediaAdView.COLOR_PLACEHOLDER_GRAY);
        this.ageRestrictionLabel.setMaxEms(5);
        this.ageRestrictionLabel.a(1, MediaAdView.COLOR_PLACEHOLDER_GRAY, this.uiUtils.E(3));
        this.ageRestrictionLabel.setBackgroundColor(1711276032);
        this.kt.setId(ks);
        if (z) {
            this.kt.setPadding(this.uiUtils.E(4), this.uiUtils.E(4), this.uiUtils.E(4), this.uiUtils.E(4));
        } else {
            this.kt.setPadding(this.uiUtils.E(16), this.uiUtils.E(16), this.uiUtils.E(16), this.uiUtils.E(16));
        }
        LayoutParams layoutParams = new LayoutParams(-1, -1);
        layoutParams.addRule(2, kr);
        this.kt.setLayoutParams(layoutParams);
        LayoutParams layoutParams2 = new LayoutParams(-2, -2);
        layoutParams2.setMargins(this.uiUtils.E(16), z ? this.uiUtils.E(8) : this.uiUtils.E(16), this.uiUtils.E(16), this.uiUtils.E(4));
        if (VERSION.SDK_INT >= 17) {
            layoutParams2.addRule(21, -1);
        } else {
            layoutParams2.addRule(11, -1);
        }
        this.ageRestrictionLabel.setLayoutParams(layoutParams2);
        if (this.jB) {
            i3 = this.uiUtils.E(64);
        } else {
            i3 = this.uiUtils.E(52);
        }
        LayoutParams layoutParams3 = new LayoutParams(-2, i3);
        layoutParams3.addRule(14, -1);
        layoutParams3.addRule(8, ks);
        if (z) {
            double d = (double) (-this.uiUtils.E(52));
            Double.isNaN(d);
            layoutParams3.bottomMargin = (int) (d / 1.5d);
        } else {
            layoutParams3.bottomMargin = (-this.uiUtils.E(52)) / 2;
        }
        this.ctaButton.setLayoutParams(layoutParams3);
        LayoutParams layoutParams4 = new LayoutParams(-1, max);
        layoutParams4.addRule(12, -1);
        this.ku.setLayoutParams(layoutParams4);
        addView(this.kt);
        addView(view);
        addView(this.ageRestrictionLabel);
        addView(this.ku);
        addView(this.ctaButton);
        setClickable(true);
        if (this.jB) {
            this.ctaButton.setTextSize(2, 32.0f);
        } else {
            this.ctaButton.setTextSize(2, 22.0f);
        }
    }

    public void setBanner(@NonNull cm cmVar) {
        this.kt.setBanner(cmVar);
        this.ku.setBanner(cmVar);
        this.ctaButton.setText(cmVar.getCtaText());
        this.ku.setBackgroundColor(cmVar.getFooterColor());
        if (TextUtils.isEmpty(cmVar.getAgeRestrictions())) {
            this.ageRestrictionLabel.setVisibility(8);
        } else {
            this.ageRestrictionLabel.setText(cmVar.getAgeRestrictions());
        }
        int ctaButtonColor = cmVar.getCtaButtonColor();
        int ctaButtonTouchColor = cmVar.getCtaButtonTouchColor();
        int ctaButtonTextColor = cmVar.getCtaButtonTextColor();
        hm.a(this.ctaButton, ctaButtonColor, ctaButtonTouchColor, this.uiUtils.E(2));
        this.ctaButton.setTextColor(ctaButtonTextColor);
    }

    @SuppressLint({"ClickableViewAccessibility"})
    public void a(@NonNull final ca caVar, @NonNull final OnClickListener onClickListener) {
        this.kt.a(caVar, onClickListener);
        if (caVar.dn) {
            this.ctaButton.setOnClickListener(onClickListener);
            return;
        }
        if (caVar.dh) {
            this.ctaButton.setOnClickListener(onClickListener);
            this.ctaButton.setEnabled(true);
        } else {
            this.ctaButton.setOnClickListener(null);
            this.ctaButton.setEnabled(false);
        }
        this.ageRestrictionLabel.setOnTouchListener(new OnTouchListener() {
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (!caVar.di) {
                    return true;
                }
                int action = motionEvent.getAction();
                if (action != 3) {
                    switch (action) {
                        case 0:
                            gq.this.kt.setBackgroundColor(-3806472);
                            break;
                        case 1:
                            gq.this.kt.setBackgroundColor(-1);
                            onClickListener.onClick(view);
                            break;
                    }
                } else {
                    gq.this.setBackgroundColor(-1);
                }
                return true;
            }
        });
    }
}
