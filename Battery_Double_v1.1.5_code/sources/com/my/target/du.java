package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.explorestack.iab.vast.tags.VastAttributes;
import com.github.mikephil.charting.utils.Utils;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.my.target.common.models.AudioData;
import com.my.target.common.models.ShareButtonData;
import org.json.JSONArray;
import org.json.JSONObject;

/* compiled from: AudioBannerParser */
public class du {
    @NonNull
    private final a adConfig;
    @NonNull
    private final Context context;
    @NonNull
    private final bz eq;
    @NonNull
    private final dv er;

    public static du a(@NonNull cz czVar, @NonNull bz bzVar, @NonNull a aVar, @NonNull Context context2) {
        return new du(czVar, bzVar, aVar, context2);
    }

    private du(@NonNull cz czVar, @NonNull bz bzVar, @NonNull a aVar, @NonNull Context context2) {
        this.eq = bzVar;
        this.adConfig = aVar;
        this.context = context2;
        this.er = dv.b(bzVar, aVar, context2);
    }

    public boolean a(@NonNull JSONObject jSONObject, @NonNull cn<AudioData> cnVar) {
        this.er.a(jSONObject, (cg) cnVar);
        if (cnVar.getType().equals("statistics")) {
            c(jSONObject, cnVar);
            return true;
        }
        float optDouble = (float) jSONObject.optDouble(IronSourceConstants.EVENTS_DURATION, Utils.DOUBLE_EPSILON);
        if (optDouble <= 0.0f) {
            StringBuilder sb = new StringBuilder();
            sb.append("unable to set duration ");
            sb.append(optDouble);
            b("Required field", sb.toString(), cnVar.getId());
            return false;
        }
        cnVar.setAutoPlay(jSONObject.optBoolean("autoplay", cnVar.isAutoPlay()));
        cnVar.setHasCtaButton(jSONObject.optBoolean("hasCtaButton", cnVar.isHasCtaButton()));
        cnVar.setAdText(jSONObject.optString("adText", cnVar.getAdText()));
        b(jSONObject, cnVar);
        JSONArray optJSONArray = jSONObject.optJSONArray("companionAds");
        if (optJSONArray != null) {
            int length = optJSONArray.length();
            for (int i = 0; i < length; i++) {
                JSONObject optJSONObject = optJSONArray.optJSONObject(i);
                if (optJSONObject != null) {
                    ch a2 = a(optJSONObject, cnVar.getId());
                    if (a2 != null) {
                        cnVar.addCompanion(a2);
                    }
                }
            }
        }
        JSONArray optJSONArray2 = jSONObject.optJSONArray("shareButtons");
        if (optJSONArray2 != null) {
            int length2 = optJSONArray2.length();
            for (int i2 = 0; i2 < length2; i2++) {
                JSONObject optJSONObject2 = optJSONArray2.optJSONObject(i2);
                if (optJSONObject2 != null) {
                    ShareButtonData newData = ShareButtonData.newData();
                    newData.setName(optJSONObject2.optString("name"));
                    newData.setUrl(optJSONObject2.optString("url"));
                    newData.setImageUrl(optJSONObject2.optString("imageUrl"));
                    cnVar.addShareButtonData(newData);
                }
            }
        }
        return d(jSONObject, cnVar);
    }

    private void b(@NonNull JSONObject jSONObject, @NonNull cn<AudioData> cnVar) {
        c(jSONObject, cnVar);
        Boolean bc = this.eq.bc();
        if (bc != null) {
            cnVar.setAllowClose(bc.booleanValue());
        } else {
            cnVar.setAllowClose(jSONObject.optBoolean("allowClose", cnVar.isAllowClose()));
        }
        Boolean be = this.eq.be();
        if (be != null) {
            cnVar.setAllowSeek(be.booleanValue());
        } else {
            cnVar.setAllowSeek(jSONObject.optBoolean("allowSeek", cnVar.isAllowSeek()));
        }
        Boolean bf = this.eq.bf();
        if (bf != null) {
            cnVar.setAllowSkip(bf.booleanValue());
        } else {
            cnVar.setAllowSkip(jSONObject.optBoolean("allowSkip", cnVar.isAllowSkip()));
        }
        Boolean bg = this.eq.bg();
        if (bg != null) {
            cnVar.setAllowTrackChange(bg.booleanValue());
        } else {
            cnVar.setAllowTrackChange(jSONObject.optBoolean("allowTrackChange", cnVar.isAllowTrackChange()));
        }
        Boolean bd = this.eq.bd();
        if (bd != null) {
            cnVar.setAllowPause(bd.booleanValue());
        } else {
            cnVar.setAllowPause(jSONObject.optBoolean("hasPause", cnVar.isAllowPause()));
        }
        float allowCloseDelay = this.eq.getAllowCloseDelay();
        if (allowCloseDelay >= 0.0f) {
            cnVar.setAllowCloseDelay(allowCloseDelay);
        } else {
            cnVar.setAllowCloseDelay((float) jSONObject.optDouble("allowCloseDelay", (double) cnVar.getAllowCloseDelay()));
        }
    }

    private void c(@NonNull JSONObject jSONObject, @NonNull cn<AudioData> cnVar) {
        double point = (double) this.eq.getPoint();
        if (point < Utils.DOUBLE_EPSILON) {
            point = jSONObject.optDouble("point");
        }
        if (Double.isNaN(point)) {
            point = -1.0d;
        } else if (point < Utils.DOUBLE_EPSILON) {
            StringBuilder sb = new StringBuilder();
            sb.append("Wrong value ");
            sb.append(point);
            sb.append(" for point");
            b("Bad value", sb.toString(), cnVar.getId());
        }
        double pointP = (double) this.eq.getPointP();
        if (pointP < Utils.DOUBLE_EPSILON) {
            pointP = jSONObject.optDouble("pointP");
        }
        if (Double.isNaN(pointP)) {
            pointP = -1.0d;
        } else if (pointP < Utils.DOUBLE_EPSILON) {
            StringBuilder sb2 = new StringBuilder();
            sb2.append("Wrong value ");
            sb2.append(pointP);
            sb2.append(" for pointP");
            b("Bad value", sb2.toString(), cnVar.getId());
        }
        if (point < Utils.DOUBLE_EPSILON && pointP < Utils.DOUBLE_EPSILON) {
            pointP = 50.0d;
            point = -1.0d;
        }
        cnVar.setPoint((float) point);
        cnVar.setPointP((float) pointP);
    }

    @Nullable
    private ch a(@NonNull JSONObject jSONObject, @NonNull String str) {
        ch newBanner = ch.newBanner();
        this.er.a(jSONObject, (cg) newBanner);
        if (newBanner.getWidth() == 0 || newBanner.getHeight() == 0) {
            StringBuilder sb = new StringBuilder();
            sb.append("Unable to add companion banner with width ");
            sb.append(newBanner.getWidth());
            sb.append(" and height ");
            sb.append(newBanner.getHeight());
            b("Required field", sb.toString(), str);
            return null;
        }
        newBanner.setAssetWidth(jSONObject.optInt(VastAttributes.ASSET_WIDTH));
        newBanner.setAssetHeight(jSONObject.optInt(VastAttributes.ASSET_HEIGHT));
        newBanner.setExpandedWidth(jSONObject.optInt(VastAttributes.EXPANDED_WIDTH));
        newBanner.setExpandedHeight(jSONObject.optInt(VastAttributes.EXPANDED_HEIGHT));
        newBanner.setStaticResource(jSONObject.optString("staticResource"));
        newBanner.setIframeResource(jSONObject.optString("iframeResource"));
        newBanner.setHtmlResource(jSONObject.optString("htmlResource"));
        newBanner.setApiFramework(jSONObject.optString(VastAttributes.API_FRAMEWORK));
        newBanner.setAdSlotID(jSONObject.optString(VastAttributes.AD_SLOT_ID));
        String optString = jSONObject.optString(VastAttributes.REQUIRED);
        if (optString != null) {
            if ("all".equals(optString) || "any".equals(optString) || "none".equals(optString)) {
                newBanner.setRequired(optString);
            } else {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("Wrong companion required attribute:");
                sb2.append(optString);
                b("Bad value", sb2.toString(), str);
            }
        }
        return newBanner;
    }

    private boolean d(@NonNull JSONObject jSONObject, @NonNull cn<AudioData> cnVar) {
        JSONArray optJSONArray = jSONObject.optJSONArray("mediafiles");
        if (optJSONArray == null || optJSONArray.length() <= 0) {
            ah.a("mediafiles array is empty");
            return false;
        }
        for (int i = 0; i < optJSONArray.length(); i++) {
            JSONObject optJSONObject = optJSONArray.optJSONObject(i);
            if (optJSONObject != null) {
                String optString = optJSONObject.optString("src");
                if (!TextUtils.isEmpty(optString)) {
                    AudioData newAudioData = AudioData.newAudioData(optString);
                    newAudioData.setBitrate(optJSONObject.optInt(VastAttributes.BITRATE));
                    cnVar.setMediaData(newAudioData);
                    return true;
                }
                StringBuilder sb = new StringBuilder();
                sb.append("bad mediafile object, src = ");
                sb.append(optString);
                b("Bad value", sb.toString(), cnVar.getId());
            }
        }
        return false;
    }

    private void b(@NonNull String str, @NonNull String str2, @Nullable String str3) {
        dp.P(str).Q(str2).r(this.adConfig.getSlotId()).S(str3).R(this.eq.getUrl()).q(this.context);
    }
}
