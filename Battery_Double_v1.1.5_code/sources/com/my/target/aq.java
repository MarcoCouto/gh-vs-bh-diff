package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.my.target.ak.c;
import com.my.target.b.C0072b;
import com.my.target.common.models.VideoData;
import com.my.target.f.b;
import com.my.target.instreamads.InstreamAd;
import com.my.target.instreamads.InstreamAd.InstreamAdBanner;
import com.my.target.instreamads.InstreamAd.InstreamAdListener;
import com.my.target.instreamads.InstreamAdPlayer;
import com.yandex.mobile.ads.video.models.vmap.AdBreak.BreakId;
import java.util.ArrayList;
import java.util.List;

/* compiled from: InstreamAdEngine */
public class aq {
    @NonNull
    private final cv aA;
    /* access modifiers changed from: private */
    @NonNull
    public final ak aB;
    /* access modifiers changed from: private */
    @Nullable
    public cz<VideoData> aC;
    /* access modifiers changed from: private */
    @Nullable
    public cn<VideoData> aD;
    /* access modifiers changed from: private */
    @Nullable
    public InstreamAdBanner aE;
    @Nullable
    private List<cn<VideoData>> aF;
    private float aG;
    private int aH;
    private int aI;
    /* access modifiers changed from: private */
    public boolean aJ;
    @NonNull
    private final a adConfig;
    /* access modifiers changed from: private */
    @NonNull
    public final InstreamAd az;
    @NonNull
    private final hd clickHandler;
    private int loadingTimeoutSeconds;
    @NonNull
    private float[] midpoints = new float[0];

    /* compiled from: InstreamAdEngine */
    class a implements c {
        private a() {
        }

        public void b(@NonNull cn cnVar) {
            if (aq.this.aC != null && aq.this.aD == cnVar && aq.this.aE != null) {
                if (!aq.this.aJ) {
                    aq.this.aJ = true;
                    Context context = aq.this.aB.getContext();
                    if (context == null) {
                        ah.a("can't send stat: context is null");
                    } else {
                        hl.a((List<dg>) aq.this.aC.w("impression"), context);
                    }
                }
                InstreamAdListener listener = aq.this.az.getListener();
                if (listener != null) {
                    listener.onBannerStart(aq.this.az, aq.this.aE);
                }
            }
        }

        public void e(@NonNull cn cnVar) {
            if (aq.this.aC != null && aq.this.aD == cnVar && aq.this.aE != null) {
                InstreamAdListener listener = aq.this.az.getListener();
                if (listener != null) {
                    listener.onBannerPause(aq.this.az, aq.this.aE);
                }
            }
        }

        public void f(@NonNull cn cnVar) {
            if (aq.this.aC != null && aq.this.aD == cnVar && aq.this.aE != null) {
                InstreamAdListener listener = aq.this.az.getListener();
                if (listener != null) {
                    listener.onBannerResume(aq.this.az, aq.this.aE);
                }
            }
        }

        public void c(@NonNull cn cnVar) {
            if (aq.this.aC != null && aq.this.aD == cnVar && aq.this.aE != null) {
                InstreamAdListener listener = aq.this.az.getListener();
                if (listener != null) {
                    listener.onBannerComplete(aq.this.az, aq.this.aE);
                }
            }
        }

        public void d(@NonNull cn cnVar) {
            if (aq.this.aC != null && aq.this.aD == cnVar && aq.this.aE != null) {
                InstreamAdListener listener = aq.this.az.getListener();
                if (listener != null) {
                    listener.onBannerComplete(aq.this.az, aq.this.aE);
                }
                aq.this.ae();
            }
        }

        public void a(float f, float f2, @NonNull cn cnVar) {
            if (aq.this.aC != null && aq.this.aD == cnVar && aq.this.aE != null) {
                InstreamAdListener listener = aq.this.az.getListener();
                if (listener != null) {
                    listener.onBannerTimeLeftChange(f, f2, aq.this.az);
                }
            }
        }

        public void a(@NonNull String str, @NonNull cn cnVar) {
            if (aq.this.aC != null && aq.this.aD == cnVar) {
                InstreamAdListener listener = aq.this.az.getListener();
                if (listener != null) {
                    listener.onError(str, aq.this.az);
                }
                aq.this.ae();
            }
        }
    }

    @NonNull
    public static aq a(@NonNull InstreamAd instreamAd, @NonNull cv cvVar, @NonNull a aVar) {
        return new aq(instreamAd, cvVar, aVar);
    }

    private aq(@NonNull InstreamAd instreamAd, @NonNull cv cvVar, @NonNull a aVar) {
        this.az = instreamAd;
        this.aA = cvVar;
        this.adConfig = aVar;
        this.aB = ak.t();
        this.aB.a((c) new a());
        this.clickHandler = hd.dM();
    }

    public void setVolume(float f) {
        this.aB.setVolume(f);
    }

    @Nullable
    public InstreamAdPlayer getPlayer() {
        return this.aB.getPlayer();
    }

    public void setPlayer(@Nullable InstreamAdPlayer instreamAdPlayer) {
        this.aB.setPlayer(instreamAdPlayer);
    }

    public void swapPlayer(@Nullable InstreamAdPlayer instreamAdPlayer) {
        this.aB.swapPlayer(instreamAdPlayer);
    }

    public void a(@NonNull float[] fArr) {
        this.midpoints = fArr;
    }

    public void e(int i) {
        this.loadingTimeoutSeconds = i;
    }

    public void setFullscreen(boolean z) {
        String str = "fullscreenOff";
        if (z) {
            str = "fullscreenOn";
        }
        a((cn) this.aD, str);
    }

    public void start(@NonNull String str) {
        stop();
        this.aC = this.aA.x(str);
        if (this.aC != null) {
            this.aB.setConnectionTimeout(this.aC.bJ());
            this.aJ = false;
            this.aI = this.aC.bK();
            this.aH = -1;
            this.aF = this.aC.bI();
            ae();
            return;
        }
        StringBuilder sb = new StringBuilder();
        sb.append("no section with name ");
        sb.append(str);
        ah.a(sb.toString());
    }

    public void startMidroll(float f) {
        boolean z;
        stop();
        float[] fArr = this.midpoints;
        int length = fArr.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                z = false;
                break;
            } else if (Float.compare(fArr[i], f) == 0) {
                z = true;
                break;
            } else {
                i++;
            }
        }
        if (z) {
            this.aC = this.aA.x(BreakId.MIDROLL);
            if (this.aC != null) {
                this.aB.setConnectionTimeout(this.aC.bJ());
                this.aJ = false;
                this.aI = this.aC.bK();
                this.aH = -1;
                this.aG = f;
                a(this.aC, f);
                return;
            }
            return;
        }
        ah.a("attempt to start wrong midpoint, use one of InstreamAd.getMidPoints()");
    }

    public void pause() {
        if (this.aC != null) {
            this.aB.pause();
        }
    }

    public void resume() {
        if (this.aC != null) {
            this.aB.resume();
        }
    }

    public void stop() {
        if (this.aC != null) {
            this.aB.stop();
            a((cz) this.aC);
        }
    }

    public void skip() {
        a((cn) this.aD, "closedByUser");
        stop();
    }

    public void skipBanner() {
        a((cn) this.aD, "closedByUser");
        this.aB.stop();
        ae();
    }

    public void handleClick() {
        if (this.aD == null) {
            ah.a("can't handle click: no playing banner");
            return;
        }
        Context context = this.aB.getContext();
        if (context == null) {
            ah.a("can't handle click: context is null");
        } else {
            this.clickHandler.b(this.aD, context);
        }
    }

    public void destroy() {
        this.aB.destroy();
    }

    public float getVolume() {
        return this.aB.getVolume();
    }

    private void a(@NonNull cz<VideoData> czVar, float f) {
        ArrayList arrayList = new ArrayList();
        for (cn cnVar : czVar.bI()) {
            if (cnVar.getPoint() == f) {
                arrayList.add(cnVar);
            }
        }
        int size = arrayList.size();
        if (size <= 0 || this.aH >= size - 1) {
            ArrayList f2 = czVar.f(f);
            if (f2.size() > 0) {
                a(f2, czVar, f);
                return;
            }
            StringBuilder sb = new StringBuilder();
            sb.append("There is no one midpoint service for point: ");
            sb.append(f);
            ah.a(sb.toString());
            b(czVar, f);
            return;
        }
        this.aF = arrayList;
        ae();
    }

    /* access modifiers changed from: private */
    public void ae() {
        if (this.aC != null) {
            if (this.aI == 0 || this.aF == null) {
                b(this.aC, this.aG);
                return;
            }
            int i = this.aH + 1;
            if (i < this.aF.size()) {
                this.aH = i;
                cn<VideoData> cnVar = (cn) this.aF.get(i);
                if ("statistics".equals(cnVar.getType())) {
                    a((cn) cnVar, "playbackStarted");
                    ae();
                } else {
                    if (this.aI > 0) {
                        this.aI--;
                    }
                    this.aD = cnVar;
                    this.aE = InstreamAdBanner.newBanner(cnVar);
                    this.aB.a(cnVar);
                }
            } else {
                b(this.aC, this.aG);
            }
        }
    }

    private void b(@NonNull cz<VideoData> czVar, float f) {
        bz bM = czVar.bM();
        if (bM == null) {
            a((cz) czVar);
        } else if (BreakId.MIDROLL.equals(czVar.getName())) {
            bM.o(true);
            bM.setPoint(f);
            ArrayList arrayList = new ArrayList();
            arrayList.add(bM);
            StringBuilder sb = new StringBuilder();
            sb.append("using doAfter service for point: ");
            sb.append(f);
            ah.a(sb.toString());
            a(arrayList, czVar, f);
        } else {
            a(bM, czVar);
        }
    }

    private void a(@NonNull cz czVar) {
        if (czVar == this.aC) {
            if (BreakId.MIDROLL.equals(czVar.getName())) {
                this.aC.p(this.aI);
            }
            this.aC = null;
            this.aJ = false;
            this.aD = null;
            this.aE = null;
            this.aH = -1;
            InstreamAdListener listener = this.az.getListener();
            if (listener != null) {
                listener.onComplete(czVar.getName(), this.az);
            }
        }
    }

    private void a(@NonNull bz bzVar, @NonNull final cz<VideoData> czVar) {
        Context context = this.aB.getContext();
        if (context == null) {
            ah.a("can't load doAfter service: context is null");
            return;
        }
        StringBuilder sb = new StringBuilder();
        sb.append("loading doAfter service: ");
        sb.append(bzVar.getUrl());
        ah.a(sb.toString());
        f.a(bzVar, this.adConfig, this.loadingTimeoutSeconds).a((C0072b<T>) new b() {
            public void onResult(@Nullable cv cvVar, @Nullable String str) {
                aq.this.a(czVar, cvVar, str);
            }
        }).a(context);
    }

    /* access modifiers changed from: private */
    public void a(@NonNull cz<VideoData> czVar, @Nullable cv cvVar, @Nullable String str) {
        if (cvVar == null) {
            if (str != null) {
                StringBuilder sb = new StringBuilder();
                sb.append("loading doAfter service failed: ");
                sb.append(str);
                ah.a(sb.toString());
            }
            if (czVar == this.aC) {
                b(czVar, this.aG);
            }
            return;
        }
        cz x = cvVar.x(czVar.getName());
        if (x != null) {
            czVar.b(x);
        }
        if (czVar == this.aC) {
            this.aF = czVar.bI();
            ae();
        }
    }

    private void a(@NonNull ArrayList<bz> arrayList, @NonNull final cz<VideoData> czVar, final float f) {
        Context context = this.aB.getContext();
        if (context == null) {
            ah.a("can't load midpoint services: context is null");
            return;
        }
        StringBuilder sb = new StringBuilder();
        sb.append("loading midpoint services for point: ");
        sb.append(f);
        ah.a(sb.toString());
        f.a((List<bz>) arrayList, this.adConfig, this.loadingTimeoutSeconds).a((C0072b<T>) new b() {
            public void onResult(@Nullable cv cvVar, @Nullable String str) {
                aq.this.a(czVar, cvVar, str, f);
            }
        }).a(context);
    }

    /* access modifiers changed from: private */
    public void a(@NonNull cz<VideoData> czVar, @Nullable cv cvVar, @Nullable String str, float f) {
        if (cvVar == null) {
            if (str != null) {
                StringBuilder sb = new StringBuilder();
                sb.append("loading midpoint services failed: ");
                sb.append(str);
                ah.a(sb.toString());
            }
            if (czVar == this.aC && f == this.aG) {
                b(czVar, f);
            }
            return;
        }
        cz x = cvVar.x(czVar.getName());
        if (x != null) {
            czVar.b(x);
        }
        if (czVar == this.aC && f == this.aG) {
            a(czVar, f);
        }
    }

    private void a(@Nullable cn cnVar, @NonNull String str) {
        if (cnVar == null) {
            ah.a("can't send stat: banner is null");
            return;
        }
        Context context = this.aB.getContext();
        if (context == null) {
            ah.a("can't send stat: context is null");
        } else {
            hl.a((List<dg>) cnVar.getStatHolder().N(str), context);
        }
    }
}
