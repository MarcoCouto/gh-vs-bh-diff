package com.my.target;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import java.util.HashMap;
import java.util.Map;

/* compiled from: MediationAdNetwork */
public final class ct {
    @NonNull
    private final String bm;
    @Nullable
    private String bn;
    @NonNull
    private final String du;
    @NonNull
    private final HashMap<String, String> dv = new HashMap<>();
    @Nullable
    private cu dw;
    @NonNull
    private final String name;
    private int priority = 0;
    @NonNull
    private final dh statHolder = dh.ch();
    private int timeout = 10000;

    @NonNull
    public static ct a(@NonNull String str, @NonNull String str2, @NonNull String str3) {
        return new ct(str, str2, str3);
    }

    @NonNull
    public String getName() {
        return this.name;
    }

    @NonNull
    public String getPlacementId() {
        return this.bm;
    }

    @NonNull
    public String bu() {
        return this.du;
    }

    @NonNull
    public dh getStatHolder() {
        return this.statHolder;
    }

    @NonNull
    public Map<String, String> bv() {
        return new HashMap(this.dv);
    }

    @Nullable
    public String getPayload() {
        return this.bn;
    }

    public void v(@Nullable String str) {
        this.bn = str;
    }

    public int getTimeout() {
        return this.timeout;
    }

    public void setTimeout(int i) {
        this.timeout = i;
    }

    public int getPriority() {
        return this.priority;
    }

    public void setPriority(int i) {
        this.priority = i;
    }

    @Nullable
    public cu bw() {
        return this.dw;
    }

    public void a(@Nullable cu cuVar) {
        this.dw = cuVar;
    }

    private ct(@NonNull String str, @NonNull String str2, @NonNull String str3) {
        this.name = str;
        this.bm = str2;
        this.du = str3;
    }

    public void b(@NonNull String str, @Nullable String str2) {
        if (!TextUtils.isEmpty(str)) {
            if (str2 == null) {
                this.dv.remove(str);
            } else {
                this.dv.put(str, str2);
            }
        }
    }
}
