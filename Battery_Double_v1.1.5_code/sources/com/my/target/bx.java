package com.my.target;

import android.content.Context;
import android.graphics.Rect;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;

/* compiled from: MraidScreenMetrics */
public class bx {
    @NonNull
    private final Rect cA;
    @NonNull
    private final Rect cB;
    @NonNull
    private final Rect cC;
    @NonNull
    private final Rect cD;
    @NonNull
    private final Rect cE;
    @NonNull
    private final Rect cF;
    @NonNull
    private final Rect cy;
    @NonNull
    private final Rect cz;
    @NonNull
    private final hm uiUtils;

    public static bx p(@NonNull Context context) {
        return new bx(context);
    }

    @VisibleForTesting
    bx(@NonNull hm hmVar) {
        this.uiUtils = hmVar;
        this.cy = new Rect();
        this.cz = new Rect();
        this.cA = new Rect();
        this.cB = new Rect();
        this.cC = new Rect();
        this.cD = new Rect();
        this.cE = new Rect();
        this.cF = new Rect();
    }

    private bx(@NonNull Context context) {
        this(hm.R(context));
    }

    public void a(int i, int i2) {
        this.cy.set(0, 0, i, i2);
        a(this.cy, this.cz);
    }

    public void a(int i, int i2, int i3, int i4) {
        this.cC.set(i, i2, i3, i4);
        a(this.cC, this.cD);
    }

    public void b(int i, int i2, int i3, int i4) {
        this.cA.set(i, i2, i3, i4);
        a(this.cA, this.cB);
    }

    public void c(int i, int i2, int i3, int i4) {
        this.cE.set(i, i2, i3, i4);
        a(this.cE, this.cF);
    }

    @NonNull
    public Rect aO() {
        return this.cB;
    }

    @NonNull
    public Rect aP() {
        return this.cD;
    }

    @NonNull
    public Rect aQ() {
        return this.cF;
    }

    @NonNull
    public Rect aR() {
        return this.cz;
    }

    private void a(@NonNull Rect rect, @NonNull Rect rect2) {
        rect2.set(this.uiUtils.G(rect.left), this.uiUtils.G(rect.top), this.uiUtils.G(rect.right), this.uiUtils.G(rect.bottom));
    }
}
