package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.github.mikephil.charting.utils.Utils;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.my.target.common.models.ImageData;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: CommonBannerParser */
public class dv {
    @NonNull
    private final a adConfig;
    @Nullable
    private String cj;
    @NonNull
    private final Context context;
    @NonNull
    private final el ep;
    @NonNull
    private final bz eq;

    @NonNull
    public static dv b(@NonNull bz bzVar, @NonNull a aVar, @NonNull Context context2) {
        return new dv(bzVar, aVar, context2);
    }

    @Nullable
    public static String g(@Nullable String str, @Nullable String str2) {
        if (TextUtils.isEmpty(str) || TextUtils.isEmpty(str2)) {
            return null;
        }
        StringBuilder sb = new StringBuilder(str2);
        if (sb.length() > 0) {
            Matcher matcher = Pattern.compile("<script\\s+[^>]*\\bsrc\\s*=\\s*(\\\\?[\\\"\\'])mraid\\.js\\1[^>]*>\\s*<\\/script>\\n*", 2).matcher(str2);
            if (matcher.find()) {
                int start = matcher.start();
                sb.delete(start, matcher.end());
                StringBuilder sb2 = new StringBuilder();
                sb2.append("<script src=\"");
                sb2.append(str);
                sb2.append("\"></script>");
                sb.insert(start, sb2.toString());
                return sb.toString();
            }
        }
        return null;
    }

    private dv(@NonNull bz bzVar, @NonNull a aVar, @NonNull Context context2) {
        this.eq = bzVar;
        this.adConfig = aVar;
        this.context = context2;
        this.ep = el.k(bzVar, aVar, context2);
    }

    public void a(@NonNull JSONObject jSONObject, @NonNull cg cgVar) {
        this.cj = jSONObject.optString("id");
        if (TextUtils.isEmpty(this.cj)) {
            this.cj = jSONObject.optString("bannerID", cgVar.getId());
        }
        cgVar.setId(this.cj);
        String optString = jSONObject.optString("type");
        if (!TextUtils.isEmpty(optString)) {
            cgVar.setType(optString);
        }
        cgVar.setWidth(jSONObject.optInt("width", cgVar.getWidth()));
        cgVar.setHeight(jSONObject.optInt("height", cgVar.getHeight()));
        String optString2 = jSONObject.optString("ageRestrictions");
        if (!TextUtils.isEmpty(optString2)) {
            cgVar.setAgeRestrictions(optString2);
        }
        String optString3 = jSONObject.optString("deeplink");
        if (!TextUtils.isEmpty(optString3)) {
            cgVar.setDeeplink(optString3);
        }
        String optString4 = jSONObject.optString("trackingLink");
        if (!TextUtils.isEmpty(optString4)) {
            cgVar.setTrackingLink(optString4);
        }
        String optString5 = jSONObject.optString("bundle_id");
        if (!TextUtils.isEmpty(optString5)) {
            cgVar.setBundleId(optString5);
        }
        String optString6 = jSONObject.optString("urlscheme");
        if (!TextUtils.isEmpty(optString6)) {
            cgVar.setUrlscheme(optString6);
        }
        cgVar.setOpenInBrowser(jSONObject.optBoolean("openInBrowser", cgVar.isOpenInBrowser()));
        cgVar.setUsePlayStoreAction(jSONObject.optBoolean("usePlayStoreAction", cgVar.isUsePlayStoreAction()));
        cgVar.setDirectLink(jSONObject.optBoolean("directLink", cgVar.isDirectLink()));
        String optString7 = jSONObject.optString("navigationType");
        if (!TextUtils.isEmpty(optString7)) {
            if ("deeplink".equals(optString7)) {
                cgVar.setNavigationType("store");
            } else {
                cgVar.setNavigationType(optString7);
            }
        }
        String optString8 = jSONObject.optString("title");
        if (!TextUtils.isEmpty(optString8)) {
            cgVar.setTitle(optString8);
        }
        String optString9 = jSONObject.optString("description");
        if (!TextUtils.isEmpty(optString9)) {
            cgVar.setDescription(optString9);
        }
        String optString10 = jSONObject.optString("disclaimer");
        if (!TextUtils.isEmpty(optString10)) {
            cgVar.setDisclaimer(optString10);
        }
        cgVar.setVotes(jSONObject.optInt("votes", cgVar.getVotes()));
        String optString11 = jSONObject.optString("category");
        if (!TextUtils.isEmpty(optString11)) {
            cgVar.setCategory(optString11);
        }
        String optString12 = jSONObject.optString("subcategory");
        if (!TextUtils.isEmpty(optString12)) {
            cgVar.setSubCategory(optString12);
        }
        String optString13 = jSONObject.optString("domain");
        if (!TextUtils.isEmpty(optString13)) {
            cgVar.setDomain(optString13);
        }
        cgVar.setDuration((float) jSONObject.optDouble(IronSourceConstants.EVENTS_DURATION, (double) cgVar.getDuration()));
        if (jSONObject.has(CampaignEx.JSON_KEY_STAR)) {
            float optDouble = (float) jSONObject.optDouble(CampaignEx.JSON_KEY_STAR, -1.0d);
            double d = (double) optDouble;
            if (d > 5.0d || d < Utils.DOUBLE_EPSILON) {
                StringBuilder sb = new StringBuilder();
                sb.append("unable to parse rating ");
                sb.append(optDouble);
                f("Bad value", sb.toString());
            } else {
                cgVar.setRating(optDouble);
            }
        }
        cgVar.setCtaText(jSONObject.optString("ctaText", cgVar.getCtaText()));
        String optString14 = jSONObject.optString("iconLink");
        int optInt = jSONObject.optInt("iconWidth");
        int optInt2 = jSONObject.optInt("iconHeight");
        if (!TextUtils.isEmpty(optString14)) {
            cgVar.setIcon(ImageData.newImageData(optString14, optInt, optInt2));
        }
        String optString15 = jSONObject.optString("imageLink");
        int optInt3 = jSONObject.optInt("imageWidth");
        int optInt4 = jSONObject.optInt("imageHeight");
        if (!TextUtils.isEmpty(optString15)) {
            cgVar.setImage(ImageData.newImageData(optString15, optInt3, optInt4));
        }
        if (jSONObject.has("clickArea")) {
            int optInt5 = jSONObject.optInt("clickArea");
            if (optInt5 <= 0) {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("Bad ClickArea mask ");
                sb2.append(optInt5);
                f("Bad value", sb2.toString());
            } else {
                cgVar.setClickArea(ca.h(optInt5));
            }
        } else if (jSONObject.has("extendedClickArea")) {
            if (jSONObject.optBoolean("extendedClickArea", true)) {
                cgVar.setClickArea(ca.cZ);
            } else {
                cgVar.setClickArea(ca.da);
            }
        }
        cgVar.setAdvertisingLabel(jSONObject.optString("advertisingLabel", ""));
        JSONObject optJSONObject = jSONObject.optJSONObject("adChoices");
        if (optJSONObject != null) {
            String optString16 = optJSONObject.optString("iconLink");
            String optString17 = optJSONObject.optString("clickLink");
            if (!TextUtils.isEmpty(optString16) && !TextUtils.isEmpty(optString17)) {
                cgVar.setAdChoices(by.a(ImageData.newImageData(optString16), optString17));
            }
        }
        this.ep.a(cgVar.getStatHolder(), jSONObject, this.cj, cgVar.getDuration());
    }

    public boolean a(@NonNull String str, @NonNull JSONObject jSONObject) {
        jSONObject.remove("source");
        try {
            jSONObject.put("source", str);
            return true;
        } catch (JSONException unused) {
            f("Json error", "Unable to re-encode source of html banner");
            return false;
        }
    }

    private void f(@NonNull String str, @NonNull String str2) {
        dp.P(str).Q(str2).r(this.adConfig.getSlotId()).S(this.cj).R(this.eq.getUrl()).q(this.context);
    }
}
