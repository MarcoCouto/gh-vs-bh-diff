package com.my.target;

import android.support.annotation.NonNull;

/* compiled from: JsAdClickEvent */
public class bn extends bl {
    @NonNull
    private final String cj;
    @NonNull
    private final String format;
    @NonNull
    private final String url;

    public bn(@NonNull String str, @NonNull String str2, @NonNull String str3) {
        super("onAdClick");
        this.cj = str;
        this.format = str2;
        this.url = str3;
    }

    @NonNull
    public String getUrl() {
        return this.url;
    }
}
