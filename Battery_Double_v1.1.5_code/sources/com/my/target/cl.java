package com.my.target;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.my.target.common.models.ImageData;
import java.util.ArrayList;
import java.util.List;

/* compiled from: InterstitialAdImageBanner */
public class cl extends ci {
    @NonNull
    private final List<ImageData> landscapeImages = new ArrayList();
    @Nullable
    private ImageData optimalLandscapeImage;
    @Nullable
    private ImageData optimalPortraitImage;
    @NonNull
    private final List<ImageData> portraitImages = new ArrayList();

    @NonNull
    public static cl newBanner() {
        return new cl();
    }

    @NonNull
    public static cl fromCompanion(@NonNull ch chVar) {
        cl newBanner = newBanner();
        newBanner.setId(chVar.getId());
        String staticResource = chVar.getStaticResource();
        if (staticResource != null) {
            newBanner.addPortraitImage(ImageData.newImageData(staticResource, chVar.getWidth(), chVar.getHeight()));
            newBanner.getStatHolder().a(chVar.getStatHolder(), 0.0f);
            newBanner.trackingLink = chVar.trackingLink;
        }
        return newBanner;
    }

    private cl() {
    }

    @NonNull
    public List<ImageData> getPortraitImages() {
        return new ArrayList(this.portraitImages);
    }

    @NonNull
    public List<ImageData> getLandscapeImages() {
        return new ArrayList(this.landscapeImages);
    }

    @Nullable
    public ImageData getOptimalPortraitImage() {
        return this.optimalPortraitImage;
    }

    public void setOptimalPortraitImage(@Nullable ImageData imageData) {
        this.optimalPortraitImage = imageData;
    }

    @Nullable
    public ImageData getOptimalLandscapeImage() {
        return this.optimalLandscapeImage;
    }

    public void setOptimalLandscapeImage(@Nullable ImageData imageData) {
        this.optimalLandscapeImage = imageData;
    }

    public void addPortraitImage(@NonNull ImageData imageData) {
        this.portraitImages.add(imageData);
    }

    public void addLandscapeImage(@NonNull ImageData imageData) {
        this.landscapeImages.add(imageData);
    }
}
