package com.my.target;

import android.content.Context;
import android.os.Looper;
import android.support.annotation.AnyThread;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.my.target.cu;
import java.util.List;

/* compiled from: AdFactory */
public abstract class b<T extends cu> {
    @NonNull
    protected final a adConfig;
    @NonNull
    protected final a<T> b;
    @Nullable
    protected String c;
    /* access modifiers changed from: private */
    @Nullable
    public C0072b<T> d;

    /* compiled from: AdFactory */
    public interface a<T extends cu> {
        boolean a();

        @NonNull
        c<T> b();

        @Nullable
        d<T> c();

        @NonNull
        e d();
    }

    /* renamed from: com.my.target.b$b reason: collision with other inner class name */
    /* compiled from: AdFactory */
    public interface C0072b<T extends cu> {
        void onResult(@Nullable T t, @Nullable String str);
    }

    public b(@NonNull a<T> aVar, @NonNull a aVar2) {
        this.b = aVar;
        this.adConfig = aVar2;
    }

    @AnyThread
    @NonNull
    public final b<T> a(@NonNull C0072b<T> bVar) {
        this.d = bVar;
        return this;
    }

    @AnyThread
    @NonNull
    public b<T> a(@NonNull Context context) {
        final Context applicationContext = context.getApplicationContext();
        ai.a(new Runnable() {
            public void run() {
                b.this.a(b.this.b(applicationContext), b.this.c);
            }
        });
        return this;
    }

    /* access modifiers changed from: protected */
    @Nullable
    public T b(@NonNull Context context) {
        bz a2 = this.b.d().a(this.adConfig, context);
        dj co = dj.co();
        String a3 = a(a2, co, context);
        if (a3 == null) {
            return null;
        }
        c b2 = this.b.b();
        cu a4 = b2.a(a3, a2, null, this.adConfig, context);
        if (this.b.a()) {
            a4 = a((List<bz>) a2.aY(), (T) a4, b2, co, context);
        }
        return a((T) a4, context);
    }

    /* access modifiers changed from: protected */
    @Nullable
    public T a(@Nullable T t, @NonNull Context context) {
        if (t == null) {
            return t;
        }
        d c2 = this.b.c();
        return c2 != null ? c2.a(t, this.adConfig, context) : t;
    }

    /* access modifiers changed from: protected */
    @Nullable
    public String a(@NonNull bz bzVar, @NonNull dj djVar, @NonNull Context context) {
        djVar.f(bzVar.getUrl(), context);
        if (djVar.cs()) {
            return (String) djVar.ct();
        }
        this.c = djVar.aF();
        return null;
    }

    /* access modifiers changed from: protected */
    @Nullable
    public T a(@NonNull List<bz> list, @Nullable T t, @NonNull c<T> cVar, @NonNull dj djVar, @NonNull Context context) {
        if (list.size() <= 0) {
            return t;
        }
        T t2 = t;
        for (bz a2 : list) {
            t2 = a(a2, t2, cVar, djVar, context);
        }
        return t2;
    }

    /* access modifiers changed from: protected */
    @Nullable
    public T a(@NonNull bz bzVar, @Nullable T t, @NonNull c<T> cVar, @NonNull dj djVar, @NonNull Context context) {
        T t2;
        bz bzVar2 = bzVar;
        Context context2 = context;
        djVar.f(bzVar.getUrl(), context2);
        if (!djVar.cs()) {
            return t;
        }
        hl.a((List<dg>) bzVar.r("serviceRequested"), context2);
        int i = 0;
        int bannersCount = t != null ? t.getBannersCount() : 0;
        String str = (String) djVar.ct();
        if (str != null) {
            Context context3 = context;
            t2 = a((List<bz>) bzVar.aY(), (T) cVar.a(str, bzVar, t, this.adConfig, context3), cVar, djVar, context3);
        } else {
            t2 = t;
        }
        if (t2 != null) {
            i = t2.getBannersCount();
        }
        if (bannersCount == i) {
            hl.a((List<dg>) bzVar.r("serviceAnswerEmpty"), context2);
            bz aW = bzVar.aW();
            if (aW != null) {
                t2 = a(aW, t2, cVar, djVar, context);
            }
        }
        return t2;
    }

    /* access modifiers changed from: protected */
    public void a(@Nullable final T t, @Nullable final String str) {
        if (this.d != null) {
            if (Looper.myLooper() == Looper.getMainLooper()) {
                this.d.onResult(t, str);
                this.d = null;
            } else {
                ai.c(new Runnable() {
                    public void run() {
                        if (b.this.d != null) {
                            b.this.d.onResult(t, str);
                            b.this.d = null;
                        }
                    }
                });
            }
        }
    }
}
