package com.my.target;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.GravityCompat;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import com.my.target.common.models.ImageData;

/* compiled from: InterstitialHtmlPresenter */
public class eo implements ev, com.my.target.fo.a {
    @Nullable
    private ck aU;
    @NonNull
    private final fo eG;
    @NonNull
    private final fu eH;
    @NonNull
    private final FrameLayout eI;
    @Nullable
    private b eJ;
    @Nullable
    private com.my.target.ev.a eK;
    private long eL;
    private long eM;

    /* compiled from: InterstitialHtmlPresenter */
    static class a implements OnClickListener {
        @NonNull
        private final eo eN;

        a(@NonNull eo eoVar) {
            this.eN = eoVar;
        }

        public void onClick(View view) {
            com.my.target.ev.a cH = this.eN.cH();
            if (cH != null) {
                cH.ag();
            }
        }
    }

    /* compiled from: InterstitialHtmlPresenter */
    static class b implements Runnable {
        @NonNull
        private final fu eH;

        b(@NonNull fu fuVar) {
            this.eH = fuVar;
        }

        public void run() {
            ah.a("banner became just closeable");
            this.eH.setVisibility(0);
        }
    }

    public void a(@NonNull bq bqVar) {
    }

    public void stop() {
    }

    @NonNull
    public static eo s(@NonNull Context context) {
        return new eo(context);
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public com.my.target.ev.a cH() {
        return this.eK;
    }

    private eo(@NonNull Context context) {
        this.eG = new fo(context);
        this.eH = new fu(context);
        this.eI = new FrameLayout(context);
        this.eH.setContentDescription("Close");
        hm.a((View) this.eH, "close_button");
        LayoutParams layoutParams = new LayoutParams(-2, -2);
        layoutParams.gravity = GravityCompat.END;
        this.eH.setVisibility(8);
        this.eH.setLayoutParams(layoutParams);
        LayoutParams layoutParams2 = new LayoutParams(-1, -1);
        layoutParams2.gravity = 1;
        this.eG.setLayoutParams(layoutParams2);
        this.eI.addView(this.eG);
        if (this.eH.getParent() == null) {
            this.eI.addView(this.eH);
        }
        Bitmap u = fg.u(hm.R(context).E(28));
        if (u != null) {
            this.eH.a(u, false);
        }
    }

    public void a(@NonNull cx cxVar, @NonNull ck ckVar) {
        this.aU = ckVar;
        this.eJ = new b(this.eH);
        this.eG.setBannerWebViewListener(this);
        String source = ckVar.getSource();
        if (source != null) {
            this.eG.e(null, source);
            ImageData closeIcon = ckVar.getCloseIcon();
            if (closeIcon != null) {
                this.eH.a(closeIcon.getBitmap(), false);
            }
            this.eH.setOnClickListener(new a(this));
            if (ckVar.getAllowCloseDelay() > 0.0f) {
                StringBuilder sb = new StringBuilder();
                sb.append("banner will be allowed to close in ");
                sb.append(ckVar.getAllowCloseDelay());
                sb.append(" seconds");
                ah.a(sb.toString());
                a((long) (ckVar.getAllowCloseDelay() * 1000.0f));
            } else {
                ah.a("banner is allowed to close");
                this.eH.setVisibility(0);
            }
            if (this.eK != null) {
                this.eK.a(ckVar, cI().getContext());
            }
            return;
        }
        Y("failed to load, null source");
    }

    public void a(@Nullable com.my.target.ev.a aVar) {
        this.eK = aVar;
    }

    public void pause() {
        if (this.eL > 0) {
            long currentTimeMillis = System.currentTimeMillis() - this.eL;
            if (currentTimeMillis <= 0 || currentTimeMillis >= this.eM) {
                this.eM = 0;
            } else {
                this.eM -= currentTimeMillis;
            }
        }
    }

    public void resume() {
        if (this.eM > 0) {
            a(this.eM);
        }
    }

    public void destroy() {
        this.eI.removeView(this.eG);
        this.eG.destroy();
    }

    @NonNull
    public View cI() {
        return this.eI;
    }

    public void onError(@NonNull String str) {
        Y(str);
    }

    public void X(@NonNull String str) {
        if (this.eK != null) {
            this.eK.b(this.aU, str, cI().getContext());
        }
    }

    private void a(long j) {
        this.eG.removeCallbacks(this.eJ);
        this.eL = System.currentTimeMillis();
        this.eG.postDelayed(this.eJ, j);
    }

    private void Y(@NonNull String str) {
        if (this.eK != null) {
            this.eK.e(str);
        }
    }
}
