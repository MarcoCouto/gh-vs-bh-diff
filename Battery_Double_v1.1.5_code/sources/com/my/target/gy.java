package com.my.target;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import com.my.target.common.models.ImageData;
import com.my.target.nativeads.views.MediaAdView;

@SuppressLint({"ViewConstructor"})
/* compiled from: SliderImageView */
public class gy extends FrameLayout {
    @Nullable
    private fp ageRestrictionLabel;
    @NonNull
    private final fx imageView = new fx(getContext());
    @NonNull
    private final RelativeLayout lA;
    private final int ly;
    private final int lz;
    @NonNull
    private final hm uiUtils;

    public gy(@NonNull Context context, int i) {
        super(context);
        this.uiUtils = hm.R(context);
        this.lA = new RelativeLayout(context);
        LayoutParams layoutParams = new LayoutParams(-1, -2);
        layoutParams.addRule(13, -1);
        this.imageView.setLayoutParams(layoutParams);
        this.imageView.setScaleType(ScaleType.FIT_XY);
        this.ly = this.uiUtils.E(8);
        this.lz = this.uiUtils.E(8);
        FrameLayout.LayoutParams layoutParams2 = new FrameLayout.LayoutParams(-2, -2);
        layoutParams2.gravity = 17;
        addView(this.lA, layoutParams2);
        this.lA.addView(this.imageView);
        this.lA.setBackgroundColor(i);
        setClipToPadding(false);
        if (VERSION.SDK_INT >= 21) {
            this.lA.setElevation((float) this.uiUtils.E(4));
        }
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    @NonNull
    public fx getImageView() {
        return this.imageView;
    }

    public void setImage(@NonNull ImageData imageData) {
        this.imageView.setPlaceholderWidth(imageData.getWidth());
        this.imageView.setPlaceholderHeight(imageData.getHeight());
        hg.a(imageData, (ImageView) this.imageView);
        if (getResources().getConfiguration().orientation == 2) {
            setPadding(this.ly, this.ly, this.ly, this.ly);
        } else {
            setPadding(this.lz, this.lz, this.lz, this.lz);
        }
    }

    public void setAgeRestrictions(@NonNull String str) {
        if (this.ageRestrictionLabel == null) {
            this.ageRestrictionLabel = new fp(getContext());
            this.ageRestrictionLabel.f(1, -7829368);
            this.ageRestrictionLabel.setPadding(this.uiUtils.E(2), 0, 0, 0);
            LayoutParams layoutParams = new LayoutParams(-2, -2);
            layoutParams.setMargins(this.uiUtils.E(8), this.uiUtils.E(20), this.uiUtils.E(8), this.uiUtils.E(20));
            this.ageRestrictionLabel.setLayoutParams(layoutParams);
            this.ageRestrictionLabel.setTextColor(MediaAdView.COLOR_PLACEHOLDER_GRAY);
            this.ageRestrictionLabel.a(1, MediaAdView.COLOR_PLACEHOLDER_GRAY, this.uiUtils.E(3));
            this.ageRestrictionLabel.setBackgroundColor(1711276032);
            this.lA.addView(this.ageRestrictionLabel);
        }
        this.ageRestrictionLabel.setText(str);
    }
}
