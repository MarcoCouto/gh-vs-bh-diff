package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.view.View;

/* compiled from: ViewabilityView */
public class gd extends View {
    @Nullable
    private a bC;
    @NonNull
    private final b jd = new b();

    /* compiled from: ViewabilityView */
    public interface a {
        void c(boolean z);
    }

    /* compiled from: ViewabilityView */
    static class b {
        private boolean cc;
        private boolean cd;

        b() {
        }

        /* access modifiers changed from: 0000 */
        public void f(boolean z) {
            this.cc = z;
        }

        /* access modifiers changed from: 0000 */
        public void setFocused(boolean z) {
            this.cd = z;
        }

        /* access modifiers changed from: 0000 */
        public boolean dC() {
            return this.cc && this.cd;
        }
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    @NonNull
    public b getViewabilityState() {
        return this.jd;
    }

    public gd(Context context) {
        super(context);
    }

    public boolean dC() {
        return this.jd.dC();
    }

    public void setViewabilityListener(@Nullable a aVar) {
        this.bC = aVar;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        setMeasuredDimension(1, 1);
    }

    public void onWindowFocusChanged(boolean z) {
        super.onWindowFocusChanged(z);
        this.jd.setFocused(z);
        if (this.jd.dC()) {
            if (this.bC != null) {
                this.bC.c(true);
            }
        } else if (!z && this.bC != null) {
            this.bC.c(false);
        }
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        d(true);
    }

    private void d(boolean z) {
        this.jd.f(z);
        this.jd.setFocused(hasWindowFocus());
        if (this.jd.dC()) {
            if (this.bC != null) {
                this.bC.c(true);
            }
        } else if (!z && this.bC != null) {
            this.bC.c(false);
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        d(false);
    }
}
