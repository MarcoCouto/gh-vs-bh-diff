package com.my.target;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.Nullable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;

/* compiled from: PageDotsView */
public class gw extends LinearLayout {
    @Nullable
    private Bitmap lj;
    @Nullable
    private Bitmap lk;
    @Nullable
    private ImageView[] ll;
    private int lm;
    private boolean ln;

    public gw(Context context) {
        super(context);
    }

    public void c(int i, int i2, int i3) {
        hm R = hm.R(getContext());
        this.lj = fh.d(R.E(12), i3);
        this.lk = fh.d(R.E(12), i2);
        this.ll = new ImageView[i];
        for (int i4 = 0; i4 < i; i4++) {
            this.ll[i4] = new ImageView(getContext());
            LayoutParams layoutParams = new LayoutParams(-2, -2);
            layoutParams.setMargins(R.E(5), R.E(5), R.E(5), R.E(5));
            this.ll[i4].setLayoutParams(layoutParams);
            this.ll[i4].setImageBitmap(this.lk);
            addView(this.ll[i4]);
        }
        this.ln = true;
    }

    public void B(int i) {
        if (this.ln && this.ll != null && i >= 0 && i < this.ll.length && this.lm < this.ll.length) {
            this.ll[this.lm].setImageBitmap(this.lk);
            this.ll[i].setImageBitmap(this.lj);
            this.lm = i;
        }
    }
}
