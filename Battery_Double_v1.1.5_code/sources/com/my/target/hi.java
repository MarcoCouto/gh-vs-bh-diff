package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: NotificationHandler */
public class hi {
    /* access modifiers changed from: private */
    @NonNull
    public final a adConfig;
    @NonNull
    private final db section;

    @NonNull
    public static hi a(@NonNull db dbVar, @NonNull a aVar) {
        return new hi(dbVar, aVar);
    }

    private hi(@NonNull db dbVar, @NonNull a aVar) {
        this.section = dbVar;
        this.adConfig = aVar;
    }

    public void a(@NonNull final cq cqVar, boolean z, @NonNull Context context) {
        if (cqVar.isHasNotification() != z) {
            cqVar.setHasNotification(z);
            final Context applicationContext = context.getApplicationContext();
            ai.a(new Runnable() {
                public void run() {
                    String a2 = hi.this.b(cqVar);
                    if (a2 != null) {
                        he N = he.N(applicationContext);
                        if (N == null) {
                            StringBuilder sb = new StringBuilder();
                            sb.append("unable to open disk cache and save text data for slotId: ");
                            sb.append(hi.this.adConfig.getSlotId());
                            ah.a(sb.toString());
                            return;
                        }
                        N.a(hi.this.adConfig.getSlotId(), a2, true);
                    }
                }
            });
        }
    }

    /* access modifiers changed from: private */
    @Nullable
    public String b(@NonNull cq cqVar) {
        String id = cqVar.getId();
        try {
            JSONObject bS = this.section.bS();
            if (bS == null) {
                StringBuilder sb = new StringBuilder();
                sb.append("unable to change cached notification for banner ");
                sb.append(id);
                sb.append(": no raw data in section");
                ah.a(sb.toString());
                return null;
            }
            JSONObject jSONObject = bS.getJSONObject(this.section.getName());
            if (jSONObject == null) {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("unable to change cached notification for banner ");
                sb2.append(id);
                sb2.append(": no section object in raw data");
                ah.a(sb2.toString());
                return null;
            }
            JSONArray jSONArray = jSONObject.getJSONArray("banners");
            if (jSONArray == null) {
                StringBuilder sb3 = new StringBuilder();
                sb3.append("unable to change cached notification for banner ");
                sb3.append(id);
                sb3.append(": no banners array in section object");
                ah.a(sb3.toString());
                return null;
            }
            int length = jSONArray.length();
            int i = 0;
            while (i < length) {
                JSONObject jSONObject2 = (JSONObject) jSONArray.get(i);
                String string = jSONObject2.getString("bannerID");
                if (string == null || !string.equals(id)) {
                    i++;
                } else {
                    jSONObject2.put("hasNotification", cqVar.isHasNotification());
                    StringBuilder sb4 = new StringBuilder();
                    sb4.append("notification changed in raw data for banner ");
                    sb4.append(id);
                    ah.a(sb4.toString());
                    return bS.toString();
                }
            }
            return null;
        } catch (JSONException e) {
            StringBuilder sb5 = new StringBuilder();
            sb5.append("error updating cached notification for section ");
            sb5.append(this.section.getName());
            sb5.append(" and banner ");
            sb5.append(id);
            sb5.append(": ");
            sb5.append(e);
            ah.a(sb5.toString());
        }
    }
}
