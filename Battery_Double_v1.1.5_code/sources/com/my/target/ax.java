package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.my.target.mediation.MediationAdConfig;
import com.my.target.mediation.MediationAdapter;
import java.lang.ref.WeakReference;
import java.util.List;
import java.util.Map;

/* compiled from: MediationEngine */
public abstract class ax<T extends MediationAdapter> {
    @NonNull
    private final cs bi;
    @Nullable
    private WeakReference<Context> bj;
    @Nullable
    private b bk;
    @Nullable
    T bl;
    @Nullable
    private hk k;

    /* compiled from: MediationEngine */
    static class a implements MediationAdConfig {
        @NonNull
        private final String bm;
        @Nullable
        private final String bn;
        private final int bo;
        private final int bp;
        @NonNull
        private final Map<String, String> bq;
        private final boolean br;
        private final boolean bs;
        private final boolean trackingEnvironmentEnabled;
        private final boolean trackingLocationEnabled;
        private final boolean userAgeRestricted;

        @NonNull
        public static a a(@NonNull String str, @Nullable String str2, @NonNull Map<String, String> map, int i, int i2, boolean z, boolean z2, boolean z3, boolean z4, boolean z5) {
            a aVar = new a(str, str2, map, i, i2, z, z2, z3, z4, z5);
            return aVar;
        }

        @NonNull
        public String getPlacementId() {
            return this.bm;
        }

        @Nullable
        public String getPayload() {
            return this.bn;
        }

        @NonNull
        public Map<String, String> getServerParams() {
            return this.bq;
        }

        public int getAge() {
            return this.bp;
        }

        public int getGender() {
            return this.bo;
        }

        public boolean isUserConsent() {
            return this.br;
        }

        public boolean isUserConsentSpecified() {
            return this.bs;
        }

        public boolean isUserAgeRestricted() {
            return this.userAgeRestricted;
        }

        public boolean isTrackingLocationEnabled() {
            return this.trackingLocationEnabled;
        }

        public boolean isTrackingEnvironmentEnabled() {
            return this.trackingEnvironmentEnabled;
        }

        a(@NonNull String str, @Nullable String str2, @NonNull Map<String, String> map, int i, int i2, boolean z, boolean z2, boolean z3, boolean z4, boolean z5) {
            this.bm = str;
            this.bn = str2;
            this.bq = map;
            this.bp = i;
            this.bo = i2;
            this.bs = z;
            this.br = z2;
            this.userAgeRestricted = z3;
            this.trackingLocationEnabled = z4;
            this.trackingEnvironmentEnabled = z5;
        }
    }

    /* compiled from: MediationEngine */
    class b implements Runnable {
        @NonNull
        final ct bt;

        b(ct ctVar) {
            this.bt = ctVar;
        }

        public void run() {
            StringBuilder sb = new StringBuilder();
            sb.append("MediationEngine: timeout for ");
            sb.append(this.bt.getName());
            sb.append(" ad network");
            ah.a(sb.toString());
            Context context = ax.this.getContext();
            if (context != null) {
                hl.a((List<dg>) this.bt.getStatHolder().N("networkTimeout"), context);
            }
            ax.this.a(this.bt, false);
        }
    }

    /* access modifiers changed from: 0000 */
    public abstract void a(@NonNull T t, @NonNull ct ctVar, @NonNull Context context);

    /* access modifiers changed from: 0000 */
    public abstract boolean a(@NonNull MediationAdapter mediationAdapter);

    /* access modifiers changed from: 0000 */
    @NonNull
    public abstract T al();

    /* access modifiers changed from: 0000 */
    public abstract void am();

    ax(@NonNull cs csVar) {
        this.bi = csVar;
    }

    public void n(@NonNull Context context) {
        this.bj = new WeakReference<>(context);
        an();
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public Context getContext() {
        if (this.bj == null) {
            return null;
        }
        return (Context) this.bj.get();
    }

    /* access modifiers changed from: 0000 */
    public void a(@NonNull ct ctVar, boolean z) {
        if (this.bk != null && this.bk.bt == ctVar) {
            if (this.k != null) {
                this.k.e(this.bk);
                this.k = null;
            }
            this.bk = null;
            if (z) {
                Context context = getContext();
                if (context != null) {
                    hl.a((List<dg>) ctVar.getStatHolder().N("networkFilled"), context);
                }
            } else {
                an();
            }
        }
    }

    private void an() {
        if (this.bl != null) {
            try {
                this.bl.destroy();
            } catch (Throwable th) {
                StringBuilder sb = new StringBuilder();
                sb.append("MediationEngine error: ");
                sb.append(th.toString());
                ah.b(sb.toString());
            }
            this.bl = null;
        }
        Context context = getContext();
        if (context == null) {
            ah.b("MediationEngine: can't configure next ad network, context is null");
            return;
        }
        ct bs = this.bi.bs();
        if (bs == null) {
            ah.a("MediationEngine: no ad networks available");
            am();
            return;
        }
        StringBuilder sb2 = new StringBuilder();
        sb2.append("MediationEngine: prepare adapter for ");
        sb2.append(bs.getName());
        sb2.append(" ad network");
        ah.a(sb2.toString());
        this.bl = a(bs);
        if (this.bl == null || !a((MediationAdapter) this.bl)) {
            ah.b("MediationEngine: can't create adapter, class not found or invalid");
            an();
        } else {
            ah.a("MediationEngine: adapter created");
            this.bk = new b<>(bs);
            int timeout = bs.getTimeout();
            if (timeout > 0) {
                this.k = hk.C(timeout);
                this.k.d(this.bk);
            }
            hl.a((List<dg>) bs.getStatHolder().N("networkRequested"), context);
            a(this.bl, bs, context);
        }
    }

    @Nullable
    private T a(@NonNull ct ctVar) {
        if ("myTarget".equals(ctVar.getName())) {
            return al();
        }
        return f(ctVar.bu());
    }

    @Nullable
    private T f(@NonNull String str) {
        try {
            return (MediationAdapter) Class.forName(str).getConstructor(new Class[0]).newInstance(new Object[0]);
        } catch (Throwable th) {
            StringBuilder sb = new StringBuilder();
            sb.append("MediationEngine error: ");
            sb.append(th.toString());
            ah.b(sb.toString());
            return null;
        }
    }
}
