package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.my.target.b.C0072b;

/* compiled from: InterstitialAdFactory */
public final class o extends b<cx> {
    @Nullable
    private final cx section;

    /* compiled from: InterstitialAdFactory */
    static class a implements com.my.target.b.a<cx> {
        public boolean a() {
            return true;
        }

        private a() {
        }

        @NonNull
        public c<cx> b() {
            return p.f();
        }

        @Nullable
        public d<cx> c() {
            return q.i();
        }

        @NonNull
        public e d() {
            return e.e();
        }
    }

    public interface b extends C0072b {
    }

    @NonNull
    public static b<cx> a(@NonNull a aVar) {
        return new o(aVar, null);
    }

    @NonNull
    public static b<cx> a(@NonNull cx cxVar, @NonNull a aVar) {
        return new o(aVar, cxVar);
    }

    private o(@NonNull a aVar, @Nullable cx cxVar) {
        super(new a(), aVar);
        this.section = cxVar;
    }

    /* access modifiers changed from: protected */
    @Nullable
    /* renamed from: e */
    public cx b(@NonNull Context context) {
        if (this.section != null) {
            return (cx) a(this.section, context);
        }
        return (cx) super.b(context);
    }
}
