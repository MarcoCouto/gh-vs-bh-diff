package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.my.target.common.models.ImageData;
import java.util.ArrayList;

/* compiled from: NativeAppwallAdResultProcessor */
public class ab extends d<db> {
    @NonNull
    public static ab m() {
        return new ab();
    }

    private ab() {
    }

    @Nullable
    public db a(@NonNull db dbVar, @NonNull a aVar, @NonNull Context context) {
        if (aVar.getCachePeriod() > 0 && !dbVar.aX() && dbVar.bS() != null) {
            he N = he.N(context);
            int slotId = aVar.getSlotId();
            if (N != null) {
                N.a(slotId, dbVar.bS().toString(), false);
            } else {
                StringBuilder sb = new StringBuilder();
                sb.append("unable to open disk cache and save data for slotId ");
                sb.append(slotId);
                ah.a(sb.toString());
            }
        }
        if (aVar.isAutoLoadImages()) {
            ArrayList arrayList = new ArrayList();
            for (cq cqVar : dbVar.bI()) {
                ImageData statusIcon = cqVar.getStatusIcon();
                ImageData coinsIcon = cqVar.getCoinsIcon();
                ImageData gotoAppIcon = cqVar.getGotoAppIcon();
                ImageData icon = cqVar.getIcon();
                ImageData labelIcon = cqVar.getLabelIcon();
                ImageData bubbleIcon = cqVar.getBubbleIcon();
                ImageData itemHighlightIcon = cqVar.getItemHighlightIcon();
                ImageData crossNotifIcon = cqVar.getCrossNotifIcon();
                if (statusIcon != null) {
                    arrayList.add(statusIcon);
                }
                if (coinsIcon != null) {
                    arrayList.add(coinsIcon);
                }
                if (gotoAppIcon != null) {
                    arrayList.add(gotoAppIcon);
                }
                if (icon != null) {
                    arrayList.add(icon);
                }
                if (labelIcon != null) {
                    arrayList.add(labelIcon);
                }
                if (bubbleIcon != null) {
                    arrayList.add(bubbleIcon);
                }
                if (itemHighlightIcon != null) {
                    arrayList.add(itemHighlightIcon);
                }
                if (crossNotifIcon != null) {
                    arrayList.add(crossNotifIcon);
                }
            }
            if (arrayList.size() > 0) {
                hg.e(arrayList).P(context);
            }
        }
        return dbVar;
    }
}
