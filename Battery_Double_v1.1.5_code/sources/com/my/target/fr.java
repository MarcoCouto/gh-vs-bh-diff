package com.my.target;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.ViewConfiguration;
import android.widget.FrameLayout;

/* compiled from: CloseableLayout */
public class fr extends FrameLayout {
    private final int hA;
    @NonNull
    private final Rect hB = new Rect();
    @NonNull
    private final Rect hC = new Rect();
    @NonNull
    private final Rect hD = new Rect();
    @NonNull
    private final Rect hE = new Rect();
    @Nullable
    private a hF;
    private boolean hG;
    private boolean hH;
    private int hI = 8388661;
    private final int hw;
    @NonNull
    private final BitmapDrawable hx;
    private final int hy;
    private final int hz;
    @NonNull
    private final hm uiUtils;

    /* compiled from: CloseableLayout */
    public interface a {
        void onClose();
    }

    public fr(@NonNull Context context) {
        super(context);
        this.uiUtils = hm.R(context);
        this.hx = new BitmapDrawable(fg.u(this.uiUtils.E(30)));
        this.hx.setState(EMPTY_STATE_SET);
        this.hx.setCallback(this);
        this.hw = ViewConfiguration.get(context).getScaledTouchSlop();
        this.hy = hm.a(50, context);
        this.hz = hm.a(30, context);
        this.hA = hm.a(8, context);
        setWillNotDraw(false);
    }

    public void a(int i, Rect rect, Rect rect2) {
        Gravity.apply(i, this.hz, this.hz, rect, rect2);
    }

    public void setOnCloseListener(@Nullable a aVar) {
        this.hF = aVar;
    }

    public void setCloseVisible(boolean z) {
        if (this.hx.setVisible(z, false)) {
            invalidate(this.hC);
        }
    }

    @SuppressLint({"ClickableViewAccessibility"})
    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (!b((int) motionEvent.getX(), (int) motionEvent.getY(), this.hw)) {
            super.onTouchEvent(motionEvent);
            return false;
        }
        int action = motionEvent.getAction();
        if (action != 3) {
            switch (action) {
                case 0:
                    this.hH = true;
                    break;
                case 1:
                    if (this.hH) {
                        dq();
                        this.hH = false;
                        break;
                    }
                    break;
            }
        } else {
            this.hH = false;
        }
        return true;
    }

    public void draw(Canvas canvas) {
        super.draw(canvas);
        if (this.hG) {
            this.hG = false;
            this.hB.set(0, 0, getWidth(), getHeight());
            b(this.hy, this.hB, this.hC);
            this.hE.set(this.hC);
            this.hE.inset(this.hA, this.hA);
            b(this.hz, this.hE, this.hD);
            this.hx.setBounds(this.hD);
        }
        if (this.hx.isVisible()) {
            this.hx.draw(canvas);
        }
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        if (motionEvent.getAction() != 0) {
            return false;
        }
        return b((int) motionEvent.getX(), (int) motionEvent.getY(), 0);
    }

    public boolean dp() {
        return this.hx.isVisible();
    }

    public void setCloseGravity(int i) {
        this.hI = i;
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public void setCloseBounds(@NonNull Rect rect) {
        this.hC.set(rect);
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public boolean b(int i, int i2, int i3) {
        return i >= this.hC.left - i3 && i2 >= this.hC.top - i3 && i < this.hC.right + i3 && i2 < this.hC.bottom + i3;
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        this.hG = true;
    }

    private void b(int i, Rect rect, Rect rect2) {
        Gravity.apply(this.hI, i, i, rect, rect2);
    }

    private void dq() {
        playSoundEffect(0);
        if (this.hF != null) {
            this.hF.onClose();
        }
    }
}
