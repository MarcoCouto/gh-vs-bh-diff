package com.my.target;

public final class R {

    public static final class id {
        public static final int nativeads_ad_view = 2131296554;
        public static final int nativeads_advertising = 2131296555;
        public static final int nativeads_age_restrictions = 2131296556;
        public static final int nativeads_call_to_action = 2131296557;
        public static final int nativeads_description = 2131296558;
        public static final int nativeads_disclaimer = 2131296559;
        public static final int nativeads_domain = 2131296560;
        public static final int nativeads_icon = 2131296561;
        public static final int nativeads_media_view = 2131296562;
        public static final int nativeads_rating = 2131296563;
        public static final int nativeads_title = 2131296564;
        public static final int nativeads_votes = 2131296565;

        private id() {
        }
    }

    private R() {
    }
}
