package com.my.target;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ClipDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.LayerDrawable;
import android.net.Uri;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewCompat;
import android.text.TextUtils;
import android.text.TextUtils.TruncateAt;
import android.util.TypedValue;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import java.net.URI;
import java.net.URISyntaxException;

/* compiled from: WebViewBrowser */
public class ge extends LinearLayout {
    public static final int je = hm.dV();
    public static final int jf = hm.dV();
    /* access modifiers changed from: private */
    @NonNull
    public final ImageButton jg;
    @NonNull
    private final LinearLayout jh;
    /* access modifiers changed from: private */
    @NonNull
    public final TextView ji;
    /* access modifiers changed from: private */
    @NonNull
    public final TextView jj;
    @NonNull
    private final FrameLayout jk;
    /* access modifiers changed from: private */
    @NonNull
    public final View jl;
    @NonNull
    private final FrameLayout jm;
    /* access modifiers changed from: private */
    @NonNull
    public final ImageButton jn;
    @NonNull
    private final RelativeLayout jo;
    @NonNull
    private final WebView jp;
    /* access modifiers changed from: private */
    @Nullable
    public b jq;
    /* access modifiers changed from: private */
    @NonNull
    public final ProgressBar progressBar;
    @NonNull
    private final hm uiUtils;

    /* compiled from: WebViewBrowser */
    class a implements OnClickListener {
        private a() {
        }

        public void onClick(View view) {
            if (view == ge.this.jg) {
                if (ge.this.jq != null) {
                    ge.this.jq.ag();
                }
            } else if (view == ge.this.jn) {
                ge.this.dE();
            }
        }
    }

    /* compiled from: WebViewBrowser */
    public interface b {
        void ag();
    }

    public void setListener(@Nullable b bVar) {
        this.jq = bVar;
    }

    public ge(@NonNull Context context) {
        super(context);
        this.jo = new RelativeLayout(context);
        this.jp = new WebView(context);
        this.jg = new ImageButton(context);
        this.jh = new LinearLayout(context);
        this.ji = new TextView(context);
        this.jj = new TextView(context);
        this.jk = new FrameLayout(context);
        this.jm = new FrameLayout(context);
        this.jn = new ImageButton(context);
        this.progressBar = new ProgressBar(context, null, 16842872);
        this.jl = new View(context);
        this.uiUtils = hm.R(context);
    }

    public boolean canGoBack() {
        return this.jp.canGoBack();
    }

    public void goBack() {
        this.jp.goBack();
    }

    public void destroy() {
        this.jp.setWebChromeClient(null);
        this.jp.setWebViewClient(null);
        this.jp.destroy();
    }

    public void setUrl(@NonNull String str) {
        this.jp.loadUrl(str);
        this.ji.setText(ac(str));
    }

    @SuppressLint({"SetJavaScriptEnabled"})
    public void dl() {
        WebSettings settings = this.jp.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setLoadWithOverviewMode(true);
        settings.setUseWideViewPort(true);
        settings.setBuiltInZoomControls(true);
        settings.setDisplayZoomControls(false);
        if (VERSION.SDK_INT >= 16) {
            settings.setAllowFileAccessFromFileURLs(false);
            settings.setAllowUniversalAccessFromFileURLs(false);
        }
        settings.setDomStorageEnabled(true);
        settings.setAppCacheEnabled(true);
        settings.setAppCachePath(getContext().getCacheDir().getAbsolutePath());
        this.jp.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView webView, String str) {
                webView.loadUrl(str);
                ge.this.ji.setText(ge.this.ac(str));
                return true;
            }
        });
        this.jp.setWebChromeClient(new WebChromeClient() {
            public void onReceivedTitle(WebView webView, String str) {
                super.onReceivedTitle(webView, str);
                ge.this.jj.setText(webView.getTitle());
                ge.this.jj.setVisibility(0);
            }

            public void onProgressChanged(WebView webView, int i) {
                if (i < 100 && ge.this.progressBar.getVisibility() == 8) {
                    ge.this.progressBar.setVisibility(0);
                    ge.this.jl.setVisibility(8);
                }
                ge.this.progressBar.setProgress(i);
                if (i >= 100) {
                    ge.this.progressBar.setVisibility(8);
                    ge.this.jl.setVisibility(0);
                }
            }
        });
        dD();
    }

    private void dD() {
        setOrientation(1);
        setGravity(16);
        a aVar = new a();
        this.jp.setLayoutParams(new LayoutParams(-1, -1));
        TypedValue typedValue = new TypedValue();
        int E = this.uiUtils.E(50);
        if (getContext().getTheme().resolveAttribute(16843499, typedValue, true)) {
            E = TypedValue.complexToDimensionPixelSize(typedValue.data, getResources().getDisplayMetrics());
        }
        this.jo.setLayoutParams(new LayoutParams(-1, E));
        this.jk.setLayoutParams(new LayoutParams(E, E));
        this.jk.setId(je);
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-1, -1);
        layoutParams.gravity = 17;
        this.jg.setLayoutParams(layoutParams);
        this.jg.setImageBitmap(fg.c(E / 4, this.uiUtils.E(2)));
        this.jg.setContentDescription("Close");
        this.jg.setOnClickListener(aVar);
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(E, E);
        if (VERSION.SDK_INT >= 18) {
            layoutParams2.addRule(21);
        } else {
            layoutParams2.addRule(11);
        }
        this.jm.setLayoutParams(layoutParams2);
        this.jm.setId(jf);
        FrameLayout.LayoutParams layoutParams3 = new FrameLayout.LayoutParams(-1, -1);
        layoutParams3.gravity = 17;
        this.jn.setLayoutParams(layoutParams3);
        this.jn.setImageBitmap(fg.H(getContext()));
        this.jn.setScaleType(ScaleType.CENTER_INSIDE);
        this.jn.setContentDescription("Open outside");
        this.jn.setOnClickListener(aVar);
        hm.a(this.jg, 0, -3355444);
        hm.a(this.jn, 0, -3355444);
        RelativeLayout.LayoutParams layoutParams4 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams4.addRule(15, -1);
        layoutParams4.addRule(1, je);
        layoutParams4.addRule(0, jf);
        this.jh.setLayoutParams(layoutParams4);
        this.jh.setOrientation(1);
        this.jh.setPadding(this.uiUtils.E(4), this.uiUtils.E(4), this.uiUtils.E(4), this.uiUtils.E(4));
        LayoutParams layoutParams5 = new LayoutParams(-2, -2);
        this.jj.setVisibility(8);
        this.jj.setLayoutParams(layoutParams5);
        this.jj.setTextColor(ViewCompat.MEASURED_STATE_MASK);
        this.jj.setTextSize(2, 18.0f);
        this.jj.setSingleLine();
        this.jj.setEllipsize(TruncateAt.MIDDLE);
        this.ji.setLayoutParams(new LayoutParams(-2, -2));
        this.ji.setSingleLine();
        this.ji.setTextSize(2, 12.0f);
        this.ji.setEllipsize(TruncateAt.MIDDLE);
        ClipDrawable clipDrawable = new ClipDrawable(new ColorDrawable(-16537100), GravityCompat.START, 1);
        LayerDrawable layerDrawable = (LayerDrawable) this.progressBar.getProgressDrawable();
        layerDrawable.setDrawableByLayerId(16908288, new ColorDrawable(-1968642));
        layerDrawable.setDrawableByLayerId(16908301, clipDrawable);
        this.progressBar.setProgressDrawable(layerDrawable);
        this.progressBar.setLayoutParams(new LayoutParams(-1, this.uiUtils.E(2)));
        this.progressBar.setProgress(0);
        this.jh.addView(this.jj);
        this.jh.addView(this.ji);
        this.jk.addView(this.jg);
        this.jm.addView(this.jn);
        this.jo.addView(this.jk);
        this.jo.addView(this.jh);
        this.jo.addView(this.jm);
        addView(this.jo);
        this.jl.setBackgroundColor(-5592406);
        RelativeLayout.LayoutParams layoutParams6 = new RelativeLayout.LayoutParams(-1, 1);
        this.jl.setVisibility(8);
        this.jl.setLayoutParams(layoutParams6);
        addView(this.progressBar);
        addView(this.jl);
        addView(this.jp);
    }

    /* access modifiers changed from: private */
    public String ac(String str) {
        try {
            URI uri = new URI(str);
            StringBuilder sb = new StringBuilder();
            sb.append(uri.getScheme());
            sb.append("://");
            sb.append(uri.getHost());
            return sb.toString();
        } catch (URISyntaxException e) {
            e.printStackTrace();
            return str;
        }
    }

    /* access modifiers changed from: private */
    public void dE() {
        String url = this.jp.getUrl();
        if (!TextUtils.isEmpty(url)) {
            try {
                Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(url));
                if (!(getContext() instanceof Activity)) {
                    intent.addFlags(268435456);
                }
                getContext().startActivity(intent);
            } catch (Exception unused) {
                StringBuilder sb = new StringBuilder();
                sb.append("unable to open url ");
                sb.append(url);
                ah.a(sb.toString());
            }
        }
    }
}
