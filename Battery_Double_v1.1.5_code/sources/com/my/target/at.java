package com.my.target;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.my.target.ads.InterstitialAd;
import com.my.target.ads.InterstitialAd.InterstitialAdListener;
import com.my.target.common.MyTargetActivity;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* compiled from: InterstitialAdHtmlEngine */
public class at extends as {
    @NonNull
    private final ck aU;
    @NonNull
    private final ArrayList<df> aV = new ArrayList<>();
    @Nullable
    private WeakReference<ev> aW;
    @NonNull
    private final cx section;

    /* compiled from: InterstitialAdHtmlEngine */
    public static class a implements com.my.target.ev.a {
        @NonNull
        private final ck aU;
        @NonNull
        private final at aX;
        @NonNull
        private final InterstitialAd ad;

        a(@NonNull at atVar, @NonNull InterstitialAd interstitialAd, @NonNull ck ckVar) {
            this.aX = atVar;
            this.ad = interstitialAd;
            this.aU = ckVar;
        }

        public void b(@Nullable cg cgVar, @Nullable String str, @NonNull Context context) {
            hd dM = hd.dM();
            if (TextUtils.isEmpty(str)) {
                dM.b(this.aU, context);
            } else {
                dM.c(this.aU, str, context);
            }
            InterstitialAdListener listener = this.ad.getListener();
            if (listener != null) {
                listener.onClick(this.ad);
            }
        }

        public void e(@NonNull String str) {
            this.aX.dismiss();
        }

        public void ag() {
            this.aX.dismiss();
        }

        public void a(@NonNull cg cgVar, @NonNull Context context) {
            this.aX.a(cgVar, context);
        }

        public void a(@NonNull cg cgVar, @NonNull String str, @NonNull Context context) {
            this.aX.a(cgVar, str, context);
        }

        public void a(@NonNull cg cgVar, float f, float f2, @NonNull Context context) {
            this.aX.a(f, f2, context);
        }
    }

    @NonNull
    static at a(@NonNull InterstitialAd interstitialAd, @NonNull ck ckVar, @NonNull cx cxVar) {
        return new at(interstitialAd, ckVar, cxVar);
    }

    private at(InterstitialAd interstitialAd, @NonNull ck ckVar, @NonNull cx cxVar) {
        super(interstitialAd);
        this.aU = ckVar;
        this.section = cxVar;
        this.aV.addAll(ckVar.getStatHolder().ck());
    }

    public void a(@NonNull fj fjVar, @NonNull FrameLayout frameLayout) {
        super.a(fjVar, frameLayout);
        b(frameLayout);
    }

    public void a(boolean z) {
        super.a(z);
        if (this.aW != null) {
            ev evVar = (ev) this.aW.get();
            if (evVar == null) {
                return;
            }
            if (z) {
                evVar.resume();
            } else {
                evVar.pause();
            }
        }
    }

    public void C() {
        super.C();
        if (this.aW != null) {
            ev evVar = (ev) this.aW.get();
            if (evVar != null) {
                evVar.destroy();
            }
        }
        this.aW = null;
    }

    public void onActivityCreate(@NonNull MyTargetActivity myTargetActivity, @NonNull Intent intent, @NonNull FrameLayout frameLayout) {
        super.onActivityCreate(myTargetActivity, intent, frameLayout);
        b(frameLayout);
    }

    public void onActivityPause() {
        super.onActivityPause();
        if (this.aW != null) {
            ev evVar = (ev) this.aW.get();
            if (evVar != null) {
                evVar.pause();
            }
        }
    }

    public void onActivityResume() {
        super.onActivityResume();
        if (this.aW != null) {
            ev evVar = (ev) this.aW.get();
            if (evVar != null) {
                evVar.resume();
            }
        }
    }

    public void onActivityDestroy() {
        super.onActivityDestroy();
        if (this.aW != null) {
            ev evVar = (ev) this.aW.get();
            if (evVar != null) {
                evVar.destroy();
            }
        }
        this.aW = null;
    }

    /* access modifiers changed from: protected */
    public boolean af() {
        return this.aU.isAllowBackButton();
    }

    /* access modifiers changed from: 0000 */
    public void a(float f, float f2, @NonNull Context context) {
        if (!this.aV.isEmpty()) {
            float f3 = f2 - f;
            ArrayList arrayList = new ArrayList();
            Iterator it = this.aV.iterator();
            while (it.hasNext()) {
                df dfVar = (df) it.next();
                float cf = dfVar.cf();
                if (cf < 0.0f && dfVar.cg() >= 0.0f) {
                    cf = (f2 / 100.0f) * dfVar.cg();
                }
                if (cf >= 0.0f && cf <= f3) {
                    arrayList.add(dfVar);
                    it.remove();
                }
            }
            hl.a((List<dg>) arrayList, context);
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(@NonNull cg cgVar, @NonNull String str, @NonNull Context context) {
        hl.a((List<dg>) cgVar.getStatHolder().N(str), context);
    }

    /* access modifiers changed from: 0000 */
    public void a(@NonNull cg cgVar, @NonNull Context context) {
        hl.a((List<dg>) cgVar.getStatHolder().N("playbackStarted"), context);
    }

    private void b(@NonNull ViewGroup viewGroup) {
        ev evVar;
        if (CampaignEx.JSON_KEY_MRAID.equals(this.aU.getType())) {
            evVar = er.u(viewGroup.getContext());
        } else {
            evVar = eo.s(viewGroup.getContext());
        }
        this.aW = new WeakReference<>(evVar);
        evVar.a(new a(this, this.ad, this.aU));
        evVar.a(this.section, this.aU);
        viewGroup.addView(evVar.cI(), new LayoutParams(-1, -1));
    }
}
