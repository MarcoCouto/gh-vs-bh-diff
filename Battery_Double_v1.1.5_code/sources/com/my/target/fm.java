package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.TypedValue;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;

/* compiled from: AdTitle */
public class fm extends RelativeLayout {
    @NonNull
    private final TextView hh;
    @NonNull
    private final View hi;
    /* access modifiers changed from: private */
    @Nullable
    public a hj;
    @Nullable
    private String title;

    /* compiled from: AdTitle */
    public interface a {
        void ag();
    }

    public fm(@NonNull Context context) {
        super(context);
        this.hh = new TextView(context);
        this.hh.setTextColor(-1);
        this.hh.setTypeface(null, 1);
        this.hh.setTextSize(2, 20.0f);
        LayoutParams layoutParams = new LayoutParams(-2, -2);
        layoutParams.leftMargin = (int) TypedValue.applyDimension(1, 8.0f, getContext().getResources().getDisplayMetrics());
        layoutParams.addRule(15);
        layoutParams.addRule(1, 256);
        addView(this.hh, layoutParams);
        setBackgroundColor(-7829368);
        LayoutParams layoutParams2 = new LayoutParams(-1, (int) (context.getResources().getDisplayMetrics().density + 0.5f));
        layoutParams2.addRule(12);
        this.hi = new View(context);
        this.hi.setBackgroundColor(-10066330);
        addView(this.hi, layoutParams2);
        fu fuVar = new fu(context);
        fuVar.a(ff.G(context), false);
        fuVar.setId(256);
        fuVar.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                if (fm.this.hj != null) {
                    fm.this.hj.ag();
                }
            }
        });
        LayoutParams layoutParams3 = new LayoutParams(-2, -2);
        layoutParams3.leftMargin = (int) TypedValue.applyDimension(1, 5.0f, getContext().getResources().getDisplayMetrics());
        layoutParams3.addRule(15);
        layoutParams3.addRule(9);
        fuVar.setLayoutParams(layoutParams3);
        addView(fuVar);
    }

    @Nullable
    public String getTitle() {
        return this.title;
    }

    public void setTitle(@Nullable String str) {
        this.title = str;
        this.hh.setText(str);
    }

    public void setStripeColor(int i) {
        this.hi.setBackgroundColor(i);
    }

    public void setMainColor(int i) {
        setBackgroundColor(i);
    }

    public void setTitleColor(int i) {
        this.hh.setTextColor(i);
    }

    public void setOnCloseClickListener(@Nullable a aVar) {
        this.hj = aVar;
    }
}
