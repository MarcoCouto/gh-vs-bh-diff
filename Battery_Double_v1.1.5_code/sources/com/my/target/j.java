package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.my.target.common.models.AudioData;
import com.yandex.mobile.ads.video.models.vmap.AdBreak.BreakId;
import java.util.ArrayList;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONObject;

/* compiled from: InstreamAudioAdResponseParser */
public class j extends c<cw> {
    @NonNull
    public static c<cw> f() {
        return new j();
    }

    @Nullable
    public cw a(@NonNull String str, @NonNull bz bzVar, @Nullable cw cwVar, @NonNull a aVar, @NonNull Context context) {
        if (isVast(str)) {
            return b(str, bzVar, cwVar, aVar, context);
        }
        return c(str, bzVar, cwVar, aVar, context);
    }

    @NonNull
    private cw b(@NonNull String str, @NonNull bz bzVar, @Nullable cw cwVar, @NonNull a aVar, @NonNull Context context) {
        em a2 = em.a(aVar, bzVar, context);
        a2.V(str);
        String bb = bzVar.bb();
        if (bb == null) {
            bb = BreakId.PREROLL;
        }
        if (cwVar == null) {
            cwVar = cw.bB();
        }
        cz y = cwVar.y(bb);
        if (y == null) {
            return cwVar;
        }
        if (!a2.cD().isEmpty()) {
            a(a2, y, bzVar);
        } else {
            bz cE = a2.cE();
            if (cE != null) {
                cE.s(y.getName());
                int position = bzVar.getPosition();
                if (position >= 0) {
                    cE.setPosition(position);
                } else {
                    cE.setPosition(y.getBannersCount());
                }
                y.c(cE);
            }
        }
        return cwVar;
    }

    private void a(@NonNull em<AudioData> emVar, @NonNull cz<AudioData> czVar, @NonNull bz bzVar) {
        czVar.d(emVar.aZ());
        int position = bzVar.getPosition();
        Iterator it = emVar.cD().iterator();
        while (it.hasNext()) {
            cn cnVar = (cn) it.next();
            Boolean bc = bzVar.bc();
            if (bc != null) {
                cnVar.setAllowClose(bc.booleanValue());
            }
            float allowCloseDelay = bzVar.getAllowCloseDelay();
            if (allowCloseDelay > 0.0f) {
                cnVar.setAllowCloseDelay(allowCloseDelay);
            }
            Boolean bd = bzVar.bd();
            if (bd != null) {
                cnVar.setAllowPause(bd.booleanValue());
            }
            Boolean be = bzVar.be();
            if (be != null) {
                cnVar.setAllowSeek(be.booleanValue());
            }
            Boolean bf = bzVar.bf();
            if (bf != null) {
                cnVar.setAllowSkip(bf.booleanValue());
            }
            Boolean bg = bzVar.bg();
            if (bg != null) {
                cnVar.setAllowTrackChange(bg.booleanValue());
            }
            Boolean bi = bzVar.bi();
            if (bi != null) {
                cnVar.setDirectLink(bi.booleanValue());
            }
            Boolean bj = bzVar.bj();
            if (bj != null) {
                cnVar.setOpenInBrowser(bj.booleanValue());
            }
            cnVar.setCloseActionText("Close");
            float point = bzVar.getPoint();
            if (point >= 0.0f) {
                cnVar.setPoint(point);
            }
            float pointP = bzVar.getPointP();
            if (pointP >= 0.0f) {
                cnVar.setPointP(pointP);
            }
            if (position >= 0) {
                int i = position + 1;
                czVar.a(cnVar, position);
                position = i;
            } else {
                czVar.g(cnVar);
            }
        }
    }

    @Nullable
    private cw c(@NonNull String str, @NonNull bz bzVar, @Nullable cw cwVar, @NonNull a aVar, @NonNull Context context) {
        JSONObject a2 = a(str, context);
        if (a2 == null) {
            return cwVar;
        }
        JSONObject optJSONObject = a2.optJSONObject(aVar.getFormat());
        if (optJSONObject == null) {
            return cwVar;
        }
        if (cwVar == null) {
            cwVar = cw.bB();
        }
        dy.cB().a(optJSONObject, cwVar);
        dt a3 = dt.a(bzVar, aVar, context);
        JSONObject optJSONObject2 = optJSONObject.optJSONObject("sections");
        if (optJSONObject2 != null) {
            String bb = bzVar.bb();
            if (bb != null) {
                cz y = cwVar.y(bb);
                if (y != null) {
                    a(optJSONObject2, a3, y, du.a(y, bzVar, aVar, context), bzVar);
                }
            } else {
                Iterator it = cwVar.bC().iterator();
                while (it.hasNext()) {
                    cz czVar = (cz) it.next();
                    a(optJSONObject2, a3, czVar, du.a(czVar, bzVar, aVar, context), bzVar);
                }
            }
        }
        return cwVar;
    }

    private void a(@NonNull JSONObject jSONObject, @NonNull dt dtVar, @NonNull cz<AudioData> czVar, @NonNull du duVar, @NonNull bz bzVar) {
        cz<AudioData> czVar2 = czVar;
        JSONObject jSONObject2 = jSONObject;
        JSONArray optJSONArray = jSONObject.optJSONArray(czVar.getName());
        if (optJSONArray != null) {
            int position = bzVar.getPosition();
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            int i = position;
            for (int i2 = 0; i2 < optJSONArray.length(); i2++) {
                JSONObject optJSONObject = optJSONArray.optJSONObject(i2);
                if (optJSONObject != null) {
                    if ("additionalData".equals(optJSONObject.optString("type"))) {
                        a(bzVar, dtVar, optJSONObject, czVar, arrayList2, arrayList);
                    } else {
                        cn newAudioBanner = cn.newAudioBanner();
                        if (duVar.a(optJSONObject, newAudioBanner)) {
                            if (bzVar.aV()) {
                                newAudioBanner.setPoint(bzVar.getPoint());
                                newAudioBanner.setPointP(bzVar.getPointP());
                            }
                            if (i >= 0) {
                                int i3 = i + 1;
                                czVar2.a(newAudioBanner, i);
                                i = i3;
                            } else {
                                czVar2.g(newAudioBanner);
                            }
                        }
                    }
                }
                du duVar2 = duVar;
            }
            a(arrayList2, arrayList);
        }
    }

    private void a(@NonNull bz bzVar, @NonNull dt dtVar, @NonNull JSONObject jSONObject, @NonNull cz czVar, @NonNull ArrayList<bz> arrayList, @NonNull ArrayList<bz> arrayList2) {
        bz d = dtVar.d(jSONObject);
        if (d != null) {
            d.s(czVar.getName());
            if (d.aU() != -1) {
                arrayList2.add(d);
                return;
            }
            arrayList.add(d);
            if (!d.aV() && !d.aT()) {
                bzVar.b(d);
                int position = bzVar.getPosition();
                if (position >= 0) {
                    d.setPosition(position);
                } else {
                    d.setPosition(czVar.getBannersCount());
                }
            }
            czVar.c(d);
        }
    }

    private void a(@NonNull ArrayList<bz> arrayList, @NonNull ArrayList<bz> arrayList2) {
        Iterator it = arrayList2.iterator();
        while (it.hasNext()) {
            bz bzVar = (bz) it.next();
            Iterator it2 = arrayList.iterator();
            while (true) {
                if (!it2.hasNext()) {
                    break;
                }
                bz bzVar2 = (bz) it2.next();
                if (bzVar.aU() == bzVar2.getId()) {
                    bzVar2.a(bzVar);
                    break;
                }
            }
        }
    }
}
