package com.my.target;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.util.ArrayList;
import java.util.Iterator;

/* compiled from: AdSection */
public abstract class cu {
    @Nullable
    private cs bi;
    @NonNull
    private final ArrayList<dg> cK = new ArrayList<>();

    public abstract int getBannersCount();

    public void d(@NonNull ArrayList<dg> arrayList) {
        this.cK.addAll(arrayList);
    }

    @NonNull
    public ArrayList<dg> aZ() {
        return new ArrayList<>(this.cK);
    }

    @NonNull
    public ArrayList<dg> w(@NonNull String str) {
        ArrayList<dg> arrayList = new ArrayList<>();
        Iterator it = this.cK.iterator();
        while (it.hasNext()) {
            dg dgVar = (dg) it.next();
            if (str.equals(dgVar.getType())) {
                arrayList.add(dgVar);
            }
        }
        return arrayList;
    }

    @Nullable
    public cs bx() {
        return this.bi;
    }

    public void a(@Nullable cs csVar) {
        this.bi = csVar;
    }
}
