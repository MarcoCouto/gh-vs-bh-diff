package com.my.target;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.Window;
import android.widget.FrameLayout;

/* compiled from: AdDialog */
public class fj extends Dialog {
    @NonNull
    private final a hb;

    /* compiled from: AdDialog */
    public interface a {
        void C();

        void a(@NonNull fj fjVar, @NonNull FrameLayout frameLayout);

        void a(boolean z);
    }

    @NonNull
    public static fj a(@NonNull a aVar, @NonNull Context context) {
        return new fj(aVar, context);
    }

    private fj(@NonNull a aVar, @NonNull Context context) {
        super(context);
        this.hb = aVar;
    }

    public void dk() {
        Window window = getWindow();
        if (window != null) {
            window.setFlags(1024, 1024);
        }
    }

    public void dismiss() {
        super.dismiss();
        this.hb.C();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        requestWindowFeature(1);
        FrameLayout frameLayout = new FrameLayout(getContext());
        setContentView(frameLayout);
        Window window = getWindow();
        if (window != null) {
            window.setBackgroundDrawable(new ColorDrawable(0));
            window.setLayout(-1, -1);
        }
        this.hb.a(this, frameLayout);
        super.onCreate(bundle);
    }

    public void onWindowFocusChanged(boolean z) {
        this.hb.a(z);
        super.onWindowFocusChanged(z);
    }
}
