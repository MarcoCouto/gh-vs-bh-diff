package com.my.target;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/* compiled from: InterstitialAdSection */
public class cx extends cu {
    @Nullable
    private ci dz;

    @NonNull
    public static cx bD() {
        return new cx();
    }

    private cx() {
    }

    @Nullable
    public ci bE() {
        return this.dz;
    }

    public void a(@Nullable ci ciVar) {
        this.dz = ciVar;
    }

    public int getBannersCount() {
        return this.dz == null ? 0 : 1;
    }
}
