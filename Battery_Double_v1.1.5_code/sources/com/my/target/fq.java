package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView.LayoutParams;
import android.view.View;

/* compiled from: CardRecyclerLayoutManager */
public class fq extends LinearLayoutManager {

    /* compiled from: CardRecyclerLayoutManager */
    public interface a {
        /* renamed from: do reason: not valid java name */
        void m612do();
    }

    public fq(@NonNull Context context) {
        super(context, 0, false);
    }

    public boolean g(@Nullable View view) {
        return findViewByPosition(findFirstCompletelyVisibleItemPosition()) == view;
    }

    public void measureChildWithMargins(View view, int i, int i2) {
        LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
        view.measure(LinearLayoutManager.getChildMeasureSpec((int) (((float) getWidth()) * 0.75f), getWidthMode(), getPaddingLeft() + getPaddingRight() + layoutParams.leftMargin + layoutParams.rightMargin + i, layoutParams.width, canScrollHorizontally()), LinearLayoutManager.getChildMeasureSpec(getHeight(), getHeightMode(), getPaddingTop() + getPaddingBottom() + layoutParams.topMargin + layoutParams.bottomMargin + i2, layoutParams.height, canScrollVertically()));
    }
}
