package com.my.target;

import android.content.Context;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

/* compiled from: HttpStatRequest */
public class dn extends dm<String> {
    private int cN;

    @NonNull
    public static dn cu() {
        return new dn();
    }

    /* access modifiers changed from: protected */
    @Nullable
    /* renamed from: b */
    public String c(@NonNull String str, @NonNull Context context) {
        this.cN = 0;
        g(str, context);
        return (String) this.ed;
    }

    /* JADX WARNING: Removed duplicated region for block: B:30:0x00ba  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00bf  */
    /* JADX WARNING: Removed duplicated region for block: B:34:? A[RETURN, SYNTHETIC] */
    private void g(@NonNull String str, @NonNull Context context) {
        HttpURLConnection httpURLConnection;
        this.ed = str;
        StringBuilder sb = new StringBuilder();
        sb.append("send stat request: ");
        sb.append(str);
        ah.a(sb.toString());
        dq r = dq.r(context);
        String str2 = null;
        try {
            httpURLConnection = (HttpURLConnection) new URL(str).openConnection();
            try {
                httpURLConnection.setReadTimeout(10000);
                httpURLConnection.setConnectTimeout(10000);
                httpURLConnection.setRequestMethod(HttpRequest.METHOD_GET);
                httpURLConnection.addRequestProperty("User-Agent", System.getProperty("http.agent"));
                httpURLConnection.setInstanceFollowRedirects(false);
                httpURLConnection.setRequestProperty("connection", "close");
                r.b(httpURLConnection);
                this.responseCode = httpURLConnection.getResponseCode();
                InputStream inputStream = httpURLConnection.getInputStream();
                if (inputStream != null) {
                    inputStream.close();
                }
                if (!(this.responseCode == 200 || this.responseCode == 204 || this.responseCode == 404)) {
                    if (this.responseCode != 403) {
                        if (this.responseCode == 302 || this.responseCode == 301 || this.responseCode == 303) {
                            this.cN++;
                            str2 = a(httpURLConnection);
                        }
                        if (httpURLConnection != null) {
                            httpURLConnection.disconnect();
                        }
                        if (str2 == null) {
                            StringBuilder sb2 = new StringBuilder();
                            sb2.append("redirected to: ");
                            sb2.append(str2);
                            ah.a(sb2.toString());
                            g(str2, context);
                            return;
                        }
                        return;
                    }
                }
                r.a(httpURLConnection);
            } catch (Throwable th) {
                th = th;
                this.ec = false;
                this.c = th.getMessage();
                StringBuilder sb3 = new StringBuilder();
                sb3.append("stat request error: ");
                sb3.append(this.c);
                ah.a(sb3.toString());
                if (httpURLConnection != null) {
                }
                if (str2 == null) {
                }
            }
        } catch (Throwable th2) {
            th = th2;
            httpURLConnection = null;
            this.ec = false;
            this.c = th.getMessage();
            StringBuilder sb32 = new StringBuilder();
            sb32.append("stat request error: ");
            sb32.append(this.c);
            ah.a(sb32.toString());
            if (httpURLConnection != null) {
            }
            if (str2 == null) {
            }
        }
        if (httpURLConnection != null) {
        }
        if (str2 == null) {
        }
    }

    @Nullable
    private String a(@NonNull HttpURLConnection httpURLConnection) {
        if (this.cN <= 10) {
            try {
                String uri = httpURLConnection.getURL().toURI().resolve(new URI(httpURLConnection.getHeaderField("Location"))).toString();
                boolean z = true;
                if (VERSION.SDK_INT >= 28 && !hn.am(uri)) {
                    z = false;
                }
                if (!hn.ak(uri) && z) {
                    return uri;
                }
                this.ed = uri;
            } catch (URISyntaxException unused) {
                return null;
            }
        }
        return null;
    }
}
