package com.my.target;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/* compiled from: StandardAdBanner */
public class cr extends cg {
    @Nullable
    private String mraidJs;
    @Nullable
    private String mraidSource;
    private int timeout = 60;

    @NonNull
    public static cr newBanner() {
        return new cr();
    }

    private cr() {
    }

    public void setMraidSource(@Nullable String str) {
        this.mraidSource = str;
    }

    @Nullable
    public String getMraidSource() {
        return this.mraidSource;
    }

    public void setMraidJs(@Nullable String str) {
        this.mraidJs = str;
    }

    @Nullable
    public String getMraidJs() {
        return this.mraidJs;
    }

    public int getTimeout() {
        return this.timeout;
    }

    public void setTimeout(int i) {
        this.timeout = i;
    }
}
