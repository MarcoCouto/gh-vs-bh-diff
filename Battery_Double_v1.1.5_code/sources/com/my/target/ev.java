package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/* compiled from: InterstitialWebPresenter */
public interface ev extends es {

    /* compiled from: InterstitialWebPresenter */
    public interface a extends com.my.target.es.a {
        void a(@NonNull cg cgVar, float f, float f2, @NonNull Context context);

        void a(@NonNull cg cgVar, @NonNull String str, @NonNull Context context);

        void e(@NonNull String str);
    }

    void a(@NonNull cx cxVar, @NonNull ck ckVar);

    void a(@Nullable a aVar);
}
