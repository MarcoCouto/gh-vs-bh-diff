package com.my.target;

import android.support.annotation.NonNull;

/* compiled from: ResearchViewabilityStat */
public class cf extends dd {
    private int dp;
    private int dq;

    public static cf u(@NonNull String str) {
        return new cf(str);
    }

    private cf(@NonNull String str) {
        super("playheadViewabilityValue", str);
    }

    public int bn() {
        return this.dp;
    }

    public void i(int i) {
        this.dp = i;
    }

    public int bo() {
        return this.dq;
    }

    public void j(int i) {
        this.dq = i;
    }
}
