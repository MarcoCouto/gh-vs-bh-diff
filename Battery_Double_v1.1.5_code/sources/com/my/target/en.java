package com.my.target;

import android.support.annotation.NonNull;
import com.my.target.et.b;
import java.util.ArrayList;
import java.util.List;

/* compiled from: InterstitialCarouselPresenter */
public class en {
    /* access modifiers changed from: private */
    @NonNull
    public final gn eC;
    /* access modifiers changed from: private */
    @NonNull
    public final ArrayList<cj> eD = new ArrayList<>();
    /* access modifiers changed from: private */
    public b eE;

    /* compiled from: InterstitialCarouselPresenter */
    class a implements com.my.target.gv.a {
        private a() {
        }

        public void a(@NonNull cj cjVar) {
            if (en.this.eE != null) {
                en.this.eE.b(cjVar, null, en.this.eC.getView().getContext());
            }
        }

        public void b(@NonNull List<cj> list) {
            for (cj cjVar : list) {
                if (!en.this.eD.contains(cjVar)) {
                    en.this.eD.add(cjVar);
                    hl.a((List<dg>) cjVar.getStatHolder().N("playbackStarted"), en.this.eC.getView().getContext());
                }
            }
        }
    }

    public static en a(@NonNull List<cj> list, @NonNull gv gvVar) {
        return new en(list, gvVar);
    }

    private en(@NonNull List<cj> list, @NonNull gv gvVar) {
        int[] numbersOfCurrentShowingCards;
        this.eC = gvVar;
        gvVar.setCarouselListener(new a());
        for (int i : gvVar.getNumbersOfCurrentShowingCards()) {
            if (i < list.size() && i >= 0) {
                cj cjVar = (cj) list.get(i);
                this.eD.add(cjVar);
                hl.a((List<dg>) cjVar.getStatHolder().N("playbackStarted"), gvVar.getView().getContext());
            }
        }
    }

    public void a(b bVar) {
        this.eE = bVar;
    }
}
