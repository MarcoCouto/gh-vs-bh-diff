package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/* compiled from: HttpRequest */
public abstract class dm<T> {
    @Nullable
    protected String c;
    protected boolean cP;
    protected boolean ec;
    @Nullable
    protected T ed;
    protected int responseCode = -1;

    /* access modifiers changed from: protected */
    @Nullable
    public abstract T c(@NonNull String str, @NonNull Context context);

    public boolean cs() {
        return this.ec;
    }

    @Nullable
    public T ct() {
        return this.ed;
    }

    @Nullable
    public String aF() {
        return this.c;
    }

    @Nullable
    public final T f(@NonNull String str, @NonNull Context context) {
        this.ec = true;
        this.cP = false;
        this.responseCode = -1;
        this.ed = null;
        this.c = null;
        return c(str, context);
    }
}
