package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import com.github.mikephil.charting.utils.Utils;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* compiled from: ViewabilityTracker */
public class ho {
    @NonNull
    private final ArrayList<de> dW;
    @NonNull
    private final ArrayList<dd> dX;
    private float s = -1.0f;
    @Nullable
    private WeakReference<View> viewWeakReference;

    public static ho c(@NonNull dh dhVar) {
        return new ho(dhVar.ci(), dhVar.cj());
    }

    public void setView(@Nullable View view) {
        if (view != null || this.viewWeakReference == null) {
            this.viewWeakReference = new WeakReference<>(view);
        } else {
            this.viewWeakReference.clear();
        }
    }

    protected ho(@NonNull ArrayList<de> arrayList, @NonNull ArrayList<dd> arrayList2) {
        this.dX = arrayList2;
        this.dW = arrayList;
    }

    public void k(float f) {
        if (Math.abs(f - this.s) >= 1.0f) {
            Context context = null;
            double d = Utils.DOUBLE_EPSILON;
            if (this.viewWeakReference != null) {
                View view = (View) this.viewWeakReference.get();
                if (view != null) {
                    d = hp.j(view);
                    context = view.getContext();
                }
            }
            a(d, f, context);
            this.s = f;
        }
    }

    /* access modifiers changed from: protected */
    public void a(double d, float f, @Nullable Context context) {
        if (this.dW.isEmpty() && this.dX.isEmpty()) {
            return;
        }
        if (context == null) {
            Iterator it = this.dX.iterator();
            while (it.hasNext()) {
                ((dd) it.next()).g(-1.0f);
            }
            return;
        }
        ArrayList arrayList = new ArrayList();
        while (!this.dW.isEmpty() && ((de) this.dW.get(this.dW.size() - 1)).cf() <= f) {
            de deVar = (de) this.dW.remove(this.dW.size() - 1);
            int cn = deVar.cn();
            boolean ce = deVar.ce();
            double d2 = (double) cn;
            if ((d2 <= d && ce) || (d2 > d && !ce)) {
                arrayList.add(deVar);
            }
        }
        Iterator it2 = this.dX.iterator();
        while (it2.hasNext()) {
            dd ddVar = (dd) it2.next();
            if (((double) ddVar.cn()) > d) {
                ddVar.g(-1.0f);
            } else if (ddVar.cd() < 0.0f || f <= ddVar.cd()) {
                ddVar.g(f);
            } else if (f - ddVar.cd() >= ddVar.getDuration()) {
                arrayList.add(ddVar);
                it2.remove();
            }
        }
        if (!arrayList.isEmpty()) {
            hl.a((List<dg>) arrayList, context);
        }
    }

    public void destroy() {
        if (this.viewWeakReference != null) {
            this.viewWeakReference.clear();
        }
        this.dX.clear();
        this.dW.clear();
        this.viewWeakReference = null;
    }
}
