package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.util.Iterator;

/* compiled from: InstreamAudioAdResultProcessor */
public class k extends d<cw> {
    @NonNull
    public static k h() {
        return new k();
    }

    private k() {
    }

    @Nullable
    public cw a(@NonNull cw cwVar, @NonNull a aVar, @NonNull Context context) {
        Iterator it = cwVar.bC().iterator();
        while (it.hasNext()) {
            ((cz) it.next()).bN();
        }
        return cwVar;
    }
}
