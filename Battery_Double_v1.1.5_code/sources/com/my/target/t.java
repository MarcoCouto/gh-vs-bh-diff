package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import org.json.JSONArray;
import org.json.JSONObject;

/* compiled from: InterstitialSliderAdResponseParser */
public class t extends c<cy> {
    @NonNull
    public static c<cy> f() {
        return new t();
    }

    private t() {
    }

    @Nullable
    public cy a(@NonNull String str, @NonNull bz bzVar, @Nullable cy cyVar, @NonNull a aVar, @NonNull Context context) {
        JSONObject a2 = a(str, context);
        if (a2 == null) {
            return null;
        }
        JSONObject optJSONObject = a2.optJSONObject(aVar.getFormat());
        if (optJSONObject == null) {
            return null;
        }
        if (cyVar == null) {
            cyVar = cy.bF();
        }
        ec.f(bzVar, aVar, context).a(optJSONObject, cyVar);
        JSONArray optJSONArray = optJSONObject.optJSONArray("banners");
        if (optJSONArray == null || optJSONArray.length() <= 0) {
            return null;
        }
        eb a3 = eb.a(cyVar, bzVar, aVar, context);
        for (int i = 0; i < optJSONArray.length(); i++) {
            JSONObject optJSONObject2 = optJSONArray.optJSONObject(i);
            if (optJSONObject2 != null) {
                cl newBanner = cl.newBanner();
                if (a3.a(optJSONObject2, newBanner)) {
                    cyVar.c(newBanner);
                }
            }
        }
        if (cyVar.getBannersCount() > 0) {
            return cyVar;
        }
        return null;
    }
}
