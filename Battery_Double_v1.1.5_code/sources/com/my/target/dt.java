package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.github.mikephil.charting.utils.Utils;
import org.json.JSONArray;
import org.json.JSONObject;

/* compiled from: AdditionalDataParser */
public class dt {
    @NonNull
    private final a adConfig;
    @NonNull
    private final Context context;
    @NonNull
    private final bz eo;
    @NonNull
    private final el ep;

    public static dt a(@NonNull bz bzVar, @NonNull a aVar, @NonNull Context context2) {
        return new dt(bzVar, aVar, context2);
    }

    private dt(@NonNull bz bzVar, @NonNull a aVar, @NonNull Context context2) {
        this.eo = bzVar;
        this.adConfig = aVar;
        this.context = context2;
        this.ep = el.k(bzVar, aVar, context2);
    }

    @Nullable
    public bz d(@NonNull JSONObject jSONObject) {
        int ba = this.eo.ba();
        if (ba >= 5) {
            ah.a("got additional data, but max redirects limit exceeded");
            return null;
        }
        int optInt = jSONObject.optInt("id", this.eo.getId());
        String optString = jSONObject.optString("url");
        if (TextUtils.isEmpty(optString)) {
            StringBuilder sb = new StringBuilder();
            sb.append("No url in additionalData Id = ");
            sb.append(optInt);
            f("Required field", sb.toString());
            return null;
        }
        bz q = bz.q(optString);
        q.f(ba + 1);
        q.setId(optInt);
        q.m(jSONObject.optBoolean("doAfter", q.aT()));
        q.g(jSONObject.optInt("doOnEmptyResponseFromId", q.aU()));
        boolean optBoolean = jSONObject.optBoolean("isMidrollPoint", q.aV());
        q.o(optBoolean);
        q.setAllowCloseDelay((float) jSONObject.optDouble("allowCloseDelay", (double) q.getAllowCloseDelay()));
        if (jSONObject.has("allowClose")) {
            q.a(Boolean.valueOf(jSONObject.optBoolean("allowClose")));
        }
        if (jSONObject.has("hasPause")) {
            q.b(Boolean.valueOf(jSONObject.optBoolean("hasPause")));
        }
        if (jSONObject.has("allowSeek")) {
            q.c(Boolean.valueOf(jSONObject.optBoolean("allowSeek")));
        }
        if (jSONObject.has("allowSkip")) {
            q.d(Boolean.valueOf(jSONObject.optBoolean("allowSkip")));
        }
        if (jSONObject.has("allowTrackChange")) {
            q.e(Boolean.valueOf(jSONObject.optBoolean("allowTrackChange")));
        }
        if (jSONObject.has("openInBrowser")) {
            q.g(Boolean.valueOf(jSONObject.optBoolean("openInBrowser")));
        }
        if (jSONObject.has("directLink")) {
            q.f(Boolean.valueOf(jSONObject.optBoolean("directLink")));
        }
        double optDouble = jSONObject.optDouble("point");
        if (Double.isNaN(optDouble)) {
            optDouble = -1.0d;
        } else if (optDouble < Utils.DOUBLE_EPSILON) {
            StringBuilder sb2 = new StringBuilder();
            sb2.append("Wrong value ");
            sb2.append(optDouble);
            sb2.append(" for point in additionalData object");
            f("Bad value", sb2.toString());
        }
        double optDouble2 = jSONObject.optDouble("pointP");
        if (Double.isNaN(optDouble2)) {
            optDouble2 = -1.0d;
        } else if (optDouble2 < Utils.DOUBLE_EPSILON) {
            StringBuilder sb3 = new StringBuilder();
            sb3.append("Wrong value ");
            sb3.append(optDouble2);
            sb3.append(" for pointP in additionalData object");
            f("Bad value", sb3.toString());
        }
        if (optBoolean && optDouble < Utils.DOUBLE_EPSILON && optDouble2 < Utils.DOUBLE_EPSILON) {
            optDouble2 = 50.0d;
            optDouble = -1.0d;
        }
        q.setPoint((float) optDouble);
        q.setPointP((float) optDouble2);
        q.d(this.eo.aZ());
        JSONArray optJSONArray = jSONObject.optJSONArray("serviceStatistics");
        if (optJSONArray != null) {
            for (int i = 0; i < optJSONArray.length(); i++) {
                JSONObject optJSONObject = optJSONArray.optJSONObject(i);
                if (optJSONObject != null) {
                    dg a2 = this.ep.a(optJSONObject, -1.0f);
                    if (a2 != null) {
                        q.a(a2);
                    }
                }
            }
        }
        this.ep.a(q.bh(), jSONObject, String.valueOf(q.getId()), -1.0f);
        return q;
    }

    private void f(@NonNull String str, @NonNull String str2) {
        dp.P(str).Q(str2).r(this.adConfig.getSlotId()).R(this.eo.getUrl()).q(this.context);
    }
}
