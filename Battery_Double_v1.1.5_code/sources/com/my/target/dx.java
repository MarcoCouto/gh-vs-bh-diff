package com.my.target;

import android.support.annotation.NonNull;
import com.ironsource.sdk.precache.DownloadManager;
import java.util.Iterator;
import org.json.JSONObject;

/* compiled from: InstreamAdSectionParser */
public class dx {
    @NonNull
    public static dx cA() {
        return new dx();
    }

    private dx() {
    }

    public void a(@NonNull JSONObject jSONObject, @NonNull cv cvVar) {
        JSONObject optJSONObject = jSONObject.optJSONObject(DownloadManager.SETTINGS);
        if (optJSONObject != null) {
            Iterator it = cvVar.bz().iterator();
            while (it.hasNext()) {
                cz czVar = (cz) it.next();
                JSONObject optJSONObject2 = optJSONObject.optJSONObject(czVar.getName());
                if (optJSONObject2 != null) {
                    a(optJSONObject2, czVar);
                }
            }
        }
    }

    private void a(@NonNull JSONObject jSONObject, @NonNull cz czVar) {
        czVar.o(jSONObject.optInt("connectionTimeout", czVar.bJ()));
        int optInt = jSONObject.optInt("maxBannersShow", czVar.bK());
        if (optInt == 0) {
            optInt = -1;
        }
        czVar.p(optInt);
    }
}
