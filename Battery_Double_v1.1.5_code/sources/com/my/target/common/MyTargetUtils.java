package com.my.target.common;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.WorkerThread;
import com.my.target.fc;
import com.my.target.hl;
import java.util.Map;

public class MyTargetUtils {
    public static void sendStat(@NonNull String str, @NonNull Context context) {
        hl.o(str, context);
    }

    @WorkerThread
    public static Map<String, String> collectInfo(@NonNull Context context) {
        fc.di().collectData(context);
        return fc.di().getData();
    }
}
