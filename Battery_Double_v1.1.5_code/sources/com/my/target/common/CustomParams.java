package com.my.target.common;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.my.target.ah;
import com.my.target.fb;
import com.my.target.hc;

public final class CustomParams extends fb {
    @Nullable
    private String[] emails;
    @Nullable
    private String[] icqIds;
    @Nullable
    private String[] okIds;
    @Nullable
    private String[] vkIds;

    public interface Gender {
        public static final int FEMALE = 2;
        public static final int MALE = 1;
        public static final int UNKNOWN = 0;
        public static final int UNSPECIFIED = -1;
    }

    public void collectData(@NonNull Context context) {
    }

    @Nullable
    public String getMrgsAppId() {
        return getParam("mrgs_app_id");
    }

    public void setMrgsAppId(@Nullable String str) {
        addParam("mrgs_app_id", str);
    }

    @Nullable
    public String getMrgsUserId() {
        return getParam("mrgs_user_id");
    }

    public void setMrgsUserId(@Nullable String str) {
        addParam("mrgs_user_id", str);
    }

    @Nullable
    public String getMrgsId() {
        return getParam("mrgs_device_id");
    }

    public void setMrgsId(@Nullable String str) {
        addParam("mrgs_device_id", str);
    }

    @Nullable
    public String getEmail() {
        if (this.emails == null || this.emails.length <= 0) {
            return null;
        }
        return this.emails[0];
    }

    public void setEmail(@Nullable String str) {
        if (str == null) {
            this.emails = null;
        } else {
            this.emails = new String[]{str};
        }
        addParam("email", str);
    }

    @Nullable
    public String[] getEmails() {
        if (this.emails == null) {
            return null;
        }
        return (String[]) this.emails.clone();
    }

    public void setEmails(@Nullable String[] strArr) {
        if (strArr == null) {
            this.emails = null;
            removeParam("email");
            return;
        }
        this.emails = new String[strArr.length];
        System.arraycopy(strArr, 0, this.emails, 0, strArr.length);
        addParam("email", hc.a(strArr));
    }

    @Nullable
    public String getIcqId() {
        if (this.icqIds == null || this.icqIds.length <= 0) {
            return null;
        }
        return this.icqIds[0];
    }

    public void setIcqId(@Nullable String str) {
        if (str == null) {
            this.icqIds = null;
        } else {
            this.icqIds = new String[]{str};
        }
        addParam("icq_id", str);
    }

    @Nullable
    public String[] getIcqIds() {
        if (this.icqIds == null) {
            return null;
        }
        return (String[]) this.icqIds.clone();
    }

    public void setIcqIds(@Nullable String[] strArr) {
        if (strArr == null) {
            this.icqIds = null;
            removeParam("icq_id");
            return;
        }
        this.icqIds = new String[strArr.length];
        System.arraycopy(strArr, 0, this.icqIds, 0, strArr.length);
        addParam("icq_id", hc.a(strArr));
    }

    @Nullable
    public String getOkId() {
        if (this.okIds == null || this.okIds.length <= 0) {
            return null;
        }
        return this.okIds[0];
    }

    public void setOkId(@Nullable String str) {
        if (str == null) {
            this.okIds = null;
        } else {
            this.okIds = new String[]{str};
        }
        addParam("ok_id", str);
    }

    @Nullable
    public String[] getOkIds() {
        if (this.okIds == null) {
            return null;
        }
        return (String[]) this.okIds.clone();
    }

    public void setOkIds(@Nullable String[] strArr) {
        if (strArr == null) {
            this.okIds = null;
            removeParam("ok_id");
            return;
        }
        this.okIds = new String[strArr.length];
        System.arraycopy(strArr, 0, this.okIds, 0, strArr.length);
        addParam("ok_id", hc.a(strArr));
    }

    @Nullable
    public String getVKId() {
        if (this.vkIds == null || this.vkIds.length <= 0) {
            return null;
        }
        return this.vkIds[0];
    }

    public void setVKId(@Nullable String str) {
        if (str == null) {
            this.vkIds = null;
        } else {
            this.vkIds = new String[]{str};
        }
        addParam("vk_id", str);
    }

    @Nullable
    public String[] getVKIds() {
        if (this.vkIds == null) {
            return null;
        }
        return (String[]) this.vkIds.clone();
    }

    public void setVKIds(@Nullable String[] strArr) {
        if (strArr == null) {
            this.vkIds = null;
            removeParam("vk_id");
            return;
        }
        this.vkIds = new String[strArr.length];
        System.arraycopy(strArr, 0, this.vkIds, 0, strArr.length);
        addParam("vk_id", hc.a(strArr));
    }

    public void setLang(@Nullable String str) {
        addParam("lang", str);
    }

    @Nullable
    public String getLang() {
        return getParam("lang");
    }

    public int getGender() {
        String param = getParam("g");
        if (param == null) {
            return -1;
        }
        try {
            return Integer.parseInt(param);
        } catch (NumberFormatException unused) {
            return -1;
        }
    }

    public void setGender(int i) {
        switch (i) {
            case 0:
            case 1:
            case 2:
                StringBuilder sb = new StringBuilder();
                sb.append("gender param is set to ");
                sb.append(i);
                ah.a(sb.toString());
                addParam("g", String.valueOf(i));
                return;
            default:
                removeParam("g");
                ah.a("gender param removed");
                return;
        }
    }

    public int getAge() {
        String param = getParam("a");
        if (param == null) {
            return 0;
        }
        try {
            return Integer.parseInt(param);
        } catch (NumberFormatException unused) {
            return 0;
        }
    }

    public void setAge(int i) {
        if (i >= 0) {
            StringBuilder sb = new StringBuilder();
            sb.append("age param set to ");
            sb.append(i);
            ah.a(sb.toString());
            addParam("a", String.valueOf(i));
            return;
        }
        ah.a("age param removed");
        removeParam("a");
    }

    public void setCustomParam(@NonNull String str, @Nullable String str2) {
        addParam(str, str2);
    }

    @Nullable
    public String getCustomParam(@NonNull String str) {
        return getParam(str);
    }
}
