package com.my.target.common;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.MenuItem;
import android.widget.FrameLayout;

public class MyTargetActivity extends Activity {
    @Nullable
    public static ActivityEngine activityEngine;
    @Nullable
    private ActivityEngine engine;
    @Nullable
    private FrameLayout rootLayout;

    public interface ActivityEngine {
        boolean onActivityBackPressed();

        void onActivityCreate(@NonNull MyTargetActivity myTargetActivity, @NonNull Intent intent, @NonNull FrameLayout frameLayout);

        void onActivityDestroy();

        boolean onActivityOptionsItemSelected(MenuItem menuItem);

        void onActivityPause();

        void onActivityResume();

        void onActivityStart();

        void onActivityStop();
    }

    public void onBackPressed() {
        if (this.engine == null || this.engine.onActivityBackPressed()) {
            super.onBackPressed();
        }
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (this.engine == null || !this.engine.onActivityOptionsItemSelected(menuItem)) {
            return super.onOptionsItemSelected(menuItem);
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Intent intent = getIntent();
        this.engine = activityEngine;
        activityEngine = null;
        if (this.engine == null || intent == null) {
            finish();
            return;
        }
        this.rootLayout = new FrameLayout(this);
        this.engine.onActivityCreate(this, intent, this.rootLayout);
        setContentView(this.rootLayout);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        if (this.engine != null) {
            this.engine.onActivityStart();
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.engine != null) {
            this.engine.onActivityResume();
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (this.engine != null) {
            this.engine.onActivityPause();
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        if (this.engine != null) {
            this.engine.onActivityStop();
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.engine != null) {
            this.engine.onActivityDestroy();
        }
    }
}
