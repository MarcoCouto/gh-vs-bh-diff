package com.my.target.common;

import android.support.annotation.Nullable;

public class MyTargetPrivacy {
    private static boolean userAgeRestricted;
    @Nullable
    private static Boolean userConsent;

    public static boolean isUserConsent() {
        return userConsent == null || userConsent.booleanValue();
    }

    public static boolean isConsentSpecified() {
        return userConsent != null;
    }

    public static void setUserConsent(boolean z) {
        userConsent = Boolean.valueOf(z);
    }

    public static boolean isUserAgeRestricted() {
        return userAgeRestricted;
    }

    public static void setUserAgeRestricted(boolean z) {
        userAgeRestricted = z;
    }
}
