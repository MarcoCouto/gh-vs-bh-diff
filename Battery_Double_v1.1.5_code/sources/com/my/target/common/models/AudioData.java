package com.my.target.common.models;

import android.support.annotation.NonNull;
import com.my.target.cd;

public final class AudioData extends cd<String> {
    private int bitrate;

    @NonNull
    public static AudioData newAudioData(@NonNull String str) {
        return new AudioData(str);
    }

    private AudioData(@NonNull String str) {
        super(str);
    }

    public int getBitrate() {
        return this.bitrate;
    }

    public void setBitrate(int i) {
        this.bitrate = i;
    }
}
