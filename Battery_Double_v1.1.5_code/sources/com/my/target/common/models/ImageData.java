package com.my.target.common.models;

import android.graphics.Bitmap;
import android.os.Build.VERSION;
import android.support.annotation.AnyThread;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.LruCache;
import com.my.target.ah;
import com.my.target.cd;

public final class ImageData extends cd<Bitmap> {
    private static final int DEFAULT_CACHE_SIZE = 31457280;
    private static final int MIN_CACHE_SIZE = 5242880;
    @NonNull
    private static volatile LruCache<ImageData, Bitmap> memcache = new BitmapCache(DEFAULT_CACHE_SIZE);
    private volatile boolean useCache;

    static class BitmapCache extends LruCache<ImageData, Bitmap> {
        public BitmapCache(int i) {
            super(i);
        }

        /* access modifiers changed from: protected */
        public int sizeOf(ImageData imageData, Bitmap bitmap) {
            if (VERSION.SDK_INT >= 19) {
                return bitmap.getAllocationByteCount();
            }
            return bitmap.getByteCount();
        }
    }

    @AnyThread
    public static void setCacheSize(int i) {
        if (i < MIN_CACHE_SIZE) {
            ah.a("setting cache size ignored: size should be >=5242880");
            return;
        }
        if (VERSION.SDK_INT >= 21) {
            memcache.resize(i);
        } else {
            memcache = new BitmapCache(i);
        }
    }

    @AnyThread
    public static void clearCache() {
        memcache.evictAll();
    }

    @NonNull
    public static ImageData newImageData(@NonNull String str) {
        return new ImageData(str);
    }

    @NonNull
    public static ImageData newImageData(@NonNull String str, int i, int i2) {
        return new ImageData(str, i, i2);
    }

    public boolean isUseCache() {
        return this.useCache;
    }

    @Nullable
    public Bitmap getBitmap() {
        return getData();
    }

    public void setBitmap(@Nullable Bitmap bitmap) {
        setData(bitmap);
    }

    @Nullable
    public Bitmap getData() {
        if (this.useCache) {
            return (Bitmap) memcache.get(this);
        }
        return (Bitmap) super.getData();
    }

    public void setData(@Nullable Bitmap bitmap) {
        if (!this.useCache) {
            super.setData(bitmap);
        } else if (bitmap == null) {
            memcache.remove(this);
        } else {
            memcache.put(this, bitmap);
        }
    }

    private ImageData(@NonNull String str) {
        super(str);
    }

    private ImageData(@NonNull String str, int i, int i2) {
        super(str);
        this.width = i;
        this.height = i2;
    }

    public void useCache(boolean z) {
        if (z != this.useCache) {
            this.useCache = z;
            if (z) {
                Bitmap bitmap = (Bitmap) super.getData();
                if (bitmap != null) {
                    super.setData(null);
                    memcache.put(this, bitmap);
                }
            } else {
                super.setData((Bitmap) memcache.remove(this));
            }
        }
    }

    @NonNull
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("ImageData{url='");
        sb.append(this.url);
        sb.append('\'');
        sb.append(", width=");
        sb.append(this.width);
        sb.append(", height=");
        sb.append(this.height);
        sb.append(", bitmap=");
        sb.append(getData());
        sb.append('}');
        return sb.toString();
    }
}
