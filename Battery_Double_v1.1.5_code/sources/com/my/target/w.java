package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.my.target.ee.a;
import org.json.JSONArray;
import org.json.JSONObject;

/* compiled from: NativeAdResponseParser */
public class w extends c<da> implements a {
    @NonNull
    public static c<da> f() {
        return new w();
    }

    private w() {
    }

    @Nullable
    public cu a(@NonNull JSONObject jSONObject, @NonNull bz bzVar, @NonNull a aVar, @NonNull Context context) {
        da bQ = da.bQ();
        ef g = ef.g(bzVar, aVar, context);
        co newBanner = co.newBanner();
        g.a(jSONObject, newBanner);
        bQ.a(newBanner);
        return bQ;
    }

    @Nullable
    public da a(@NonNull String str, @NonNull bz bzVar, @Nullable da daVar, @NonNull a aVar, @NonNull Context context) {
        JSONObject a2 = a(str, context);
        if (a2 == null) {
            return null;
        }
        if (daVar == null) {
            daVar = da.bQ();
        }
        JSONObject optJSONObject = a2.optJSONObject(aVar.getFormat());
        if (optJSONObject == null) {
            if (aVar.isMediationEnabled()) {
                JSONObject optJSONObject2 = a2.optJSONObject("mediation");
                if (optJSONObject2 != null) {
                    cs f = ee.a(this, bzVar, aVar, context).f(optJSONObject2);
                    if (f != null) {
                        daVar.a(f);
                        return daVar;
                    }
                }
            }
            return null;
        }
        JSONArray optJSONArray = optJSONObject.optJSONArray("banners");
        if (optJSONArray == null || optJSONArray.length() <= 0) {
            return null;
        }
        ef g = ef.g(bzVar, aVar, context);
        int bannersCount = aVar.getBannersCount();
        if (bannersCount > 0) {
            int length = optJSONArray.length();
            if (bannersCount > length) {
                bannersCount = length;
            }
        } else {
            bannersCount = 1;
        }
        for (int i = 0; i < bannersCount; i++) {
            JSONObject optJSONObject3 = optJSONArray.optJSONObject(i);
            if (optJSONObject3 != null) {
                co newBanner = co.newBanner();
                g.a(optJSONObject3, newBanner);
                daVar.a(newBanner);
            }
        }
        if (daVar.getBannersCount() > 0) {
            return daVar;
        }
        return null;
    }
}
