package com.my.target;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import com.my.target.nativeads.banners.NativePromoBanner;
import java.util.List;

/* compiled from: INativeAdEngine */
public interface ao {
    @Nullable
    NativePromoBanner Z();

    void registerView(@NonNull View view, @Nullable List<View> list, int i);

    void unregisterView();
}
