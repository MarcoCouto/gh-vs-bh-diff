package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.view.ViewGroup;
import android.widget.FrameLayout.LayoutParams;
import com.mintegral.msdk.base.entity.CampaignEx;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: WebViewPresenter */
public class ey implements ex, com.my.target.fo.a {
    @Nullable
    private cr bK;
    @NonNull
    private final Context context;
    @NonNull
    private final fo eG;
    @Nullable
    private com.my.target.ex.a fD;
    @NonNull
    private final String format;
    @NonNull
    private final fs fx;
    @Nullable
    private a gb;
    @NonNull
    private final dc section;

    /* compiled from: WebViewPresenter */
    public interface a {
        void aa();

        void e(@NonNull String str);
    }

    @NonNull
    public static ey a(@NonNull String str, @NonNull dc dcVar, @NonNull Context context2) {
        return new ey(str, dcVar, context2);
    }

    @VisibleForTesting
    ey(@NonNull fo foVar, @NonNull fs fsVar, @NonNull String str, @NonNull dc dcVar, @NonNull Context context2) {
        this.eG = foVar;
        this.fx = fsVar;
        this.context = context2;
        this.format = str;
        this.section = dcVar;
        LayoutParams layoutParams = new LayoutParams(-1, -1);
        layoutParams.gravity = 1;
        fsVar.addView(this.eG);
        this.eG.setLayoutParams(layoutParams);
        this.eG.setBannerWebViewListener(this);
    }

    private ey(@NonNull String str, @NonNull dc dcVar, @NonNull Context context2) {
        this(new fo(context2), new fs(context2), str, dcVar, context2);
    }

    public void a(@NonNull cr crVar) {
        this.bK = crVar;
        JSONObject bS = this.section.bS();
        String cb = this.section.cb();
        if (bS == null) {
            Y("failed to load, null raw data");
        } else if (cb == null) {
            Y("failed to load, null html");
        } else {
            this.eG.e(bS, cb);
        }
    }

    public void start() {
        try {
            this.eG.a((bi) new bk(this.format, null, this.context.getResources().getConfiguration().orientation));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void stop() {
        try {
            this.eG.a((bi) new bh("stop"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void pause() {
        try {
            this.eG.a((bi) new bh(CampaignEx.JSON_NATIVE_VIDEO_PAUSE));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void resume() {
        try {
            this.eG.a((bi) new bh(CampaignEx.JSON_NATIVE_VIDEO_RESUME));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void destroy() {
        a((a) null);
        a((com.my.target.ex.a) null);
        if (this.eG.getParent() != null) {
            ((ViewGroup) this.eG.getParent()).removeView(this.eG);
        }
        this.eG.destroy();
    }

    @NonNull
    public fs db() {
        return this.fx;
    }

    public void a(@NonNull bq bqVar) {
        char c;
        String type = bqVar.getType();
        switch (type.hashCode()) {
            case -2124458952:
                if (type.equals("onComplete")) {
                    c = 6;
                    break;
                }
            case -1349867671:
                if (type.equals("onError")) {
                    c = 3;
                    break;
                }
            case -1338265852:
                if (type.equals("onReady")) {
                    c = 0;
                    break;
                }
            case -1013111741:
                if (type.equals("onNoAd")) {
                    c = 7;
                    break;
                }
            case -1012956973:
                if (type.equals("onStat")) {
                    c = 14;
                    break;
                }
            case 157935686:
                if (type.equals("onAdClick")) {
                    c = 13;
                    break;
                }
            case 159970502:
                if (type.equals("onAdError")) {
                    c = 4;
                    break;
                }
            case 169625780:
                if (type.equals("onAdPause")) {
                    c = 10;
                    break;
                }
            case 172943136:
                if (type.equals("onAdStart")) {
                    c = 8;
                    break;
                }
            case 567029179:
                if (type.equals("onAdComplete")) {
                    c = 12;
                    break;
                }
            case 747469392:
                if (type.equals("onSizeChange")) {
                    c = 15;
                    break;
                }
            case 975410564:
                if (type.equals("onAdStop")) {
                    c = 9;
                    break;
                }
            case 1024326959:
                if (type.equals("onAdResume")) {
                    c = 11;
                    break;
                }
            case 1109243225:
                if (type.equals("onExpand")) {
                    c = 1;
                    break;
                }
            case 1252938159:
                if (type.equals("onCloseClick")) {
                    c = 5;
                    break;
                }
            case 2103168704:
                if (type.equals("onRequestNewAds")) {
                    c = 16;
                    break;
                }
            case 2137867948:
                if (type.equals("onCollapse")) {
                    c = 2;
                    break;
                }
            default:
                c = 65535;
                break;
        }
        switch (c) {
            case 0:
                dg();
                return;
            case 3:
            case 4:
                bp bpVar = (bp) bqVar;
                String str = "JS error";
                if (bpVar.aF() != null) {
                    StringBuilder sb = new StringBuilder();
                    sb.append(str);
                    sb.append(": ");
                    sb.append(bpVar.aF());
                    str = sb.toString();
                }
                dp.P("JS error").Q(str).R(this.eG.getUrl()).S(this.bK != null ? this.bK.getId() : null).q(this.context);
                if (bqVar.getType().equals("onError")) {
                    Y("JS error");
                    return;
                }
                return;
            case 6:
                Y("Ad completed");
                return;
            case 7:
                Y("No ad");
                return;
            case 8:
                df();
                return;
            case 13:
                ab(((bn) bqVar).getUrl());
                return;
            case 14:
                c(((bt) bqVar).aG());
                return;
            default:
                return;
        }
    }

    public void a(@Nullable com.my.target.ex.a aVar) {
        this.fD = aVar;
    }

    public void a(@Nullable a aVar) {
        this.gb = aVar;
    }

    public void onError(@NonNull String str) {
        Y(str);
    }

    public void X(@NonNull String str) {
        if (this.bK != null) {
            ab(str);
        }
    }

    private void ab(@Nullable String str) {
        if (this.fD != null && this.bK != null) {
            this.fD.a(this.bK, str);
        }
    }

    private void c(@Nullable List<String> list) {
        hl.b(list, this.context);
    }

    private void df() {
        if (this.fD != null && this.bK != null) {
            this.fD.a(this.bK);
        }
    }

    private void Y(@NonNull String str) {
        if (this.gb != null) {
            this.gb.e(str);
        }
    }

    private void dg() {
        if (this.gb != null) {
            this.gb.aa();
        }
    }
}
