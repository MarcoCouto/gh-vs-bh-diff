package com.my.target;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.Nullable;

/* compiled from: AdChoicesView */
public class fl extends fx {
    private int hg = -1;

    public fl(Context context) {
        super(context);
    }

    public void setFixedHeight(int i) {
        this.hg = i;
    }

    public void setImageBitmap(@Nullable Bitmap bitmap) {
        int i;
        int i2;
        int i3 = 0;
        if (this.hg >= 0) {
            i = (this.hg - getPaddingTop()) - getPaddingBottom();
            if (i < 0) {
                i = 0;
            }
        } else {
            i = -1;
        }
        if (i >= 0) {
            if (bitmap != null) {
                int width = bitmap.getWidth();
                i2 = width;
                i3 = bitmap.getHeight();
            } else {
                i2 = 0;
            }
            float f = 0.0f;
            if (i3 > 0) {
                f = ((float) i2) / ((float) i3);
            }
            setMeasuredDimension(((int) (((float) i) * f)) + getPaddingLeft() + getPaddingRight(), this.hg);
        }
        super.setImageBitmap(bitmap);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        if (this.hg >= 0) {
            setMeasuredDimension(getMeasuredWidth(), getMeasuredHeight());
        } else {
            super.onMeasure(i, i2);
        }
    }
}
