package com.my.target;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.webkit.ConsoleMessage;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: JsEventDeserializer */
public class bf {
    @Nullable
    public static bq a(@NonNull ConsoleMessage consoleMessage) {
        String message = consoleMessage.message();
        if (!TextUtils.isEmpty(message) && g(message)) {
            try {
                return b(new JSONObject(message.substring("adman://onEvent,".length())));
            } catch (JSONException e) {
                ah.b(e.getMessage());
            }
        }
        return null;
    }

    private static boolean g(@NonNull String str) {
        return str.startsWith("adman://onEvent,");
    }

    @Nullable
    private static bq b(@NonNull JSONObject jSONObject) throws JSONException {
        String string = jSONObject.getString("event");
        String str = null;
        JSONObject jSONObject2 = jSONObject.has("data") ? jSONObject.getJSONObject("data") : null;
        char c = 65535;
        int i = 0;
        switch (string.hashCode()) {
            case -1349867671:
                if (string.equals("onError")) {
                    c = 0;
                    break;
                }
                break;
            case -1012956973:
                if (string.equals("onStat")) {
                    c = 4;
                    break;
                }
                break;
            case 157935686:
                if (string.equals("onAdClick")) {
                    c = 5;
                    break;
                }
                break;
            case 172943136:
                if (string.equals("onAdStart")) {
                    c = 2;
                    break;
                }
                break;
            case 747469392:
                if (string.equals("onSizeChange")) {
                    c = 3;
                    break;
                }
                break;
            case 1109243225:
                if (string.equals("onExpand")) {
                    c = 1;
                    break;
                }
                break;
        }
        switch (c) {
            case 0:
                if (jSONObject2 == null) {
                    return new bp();
                }
                String str2 = "jsError";
                if (jSONObject2.has("error")) {
                    StringBuilder sb = new StringBuilder();
                    sb.append(str2);
                    sb.append(" error: ");
                    sb.append(jSONObject2.getString("error"));
                    str2 = sb.toString();
                }
                if (jSONObject2.has("message")) {
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append(str2);
                    sb2.append(" message: ");
                    sb2.append(jSONObject2.getString("message"));
                    str2 = sb2.toString();
                }
                return new bp(str2);
            case 1:
                if (jSONObject2 == null || !jSONObject2.has("width") || !jSONObject2.has("height")) {
                    return new br();
                }
                return new br(jSONObject2.getInt("width"), jSONObject2.getInt("height"));
            case 2:
                if (jSONObject2 == null || !jSONObject2.has("format") || !jSONObject2.has("banners")) {
                    return null;
                }
                String string2 = jSONObject2.getString("format");
                JSONArray jSONArray = jSONObject2.getJSONArray("banners");
                int length = jSONArray.length();
                String[] strArr = new String[length];
                while (i < length) {
                    strArr[i] = jSONArray.getString(i);
                    i++;
                }
                return new bo(strArr, string2);
            case 3:
                if (jSONObject2 == null || !jSONObject2.has("width") || !jSONObject2.has("height")) {
                    return null;
                }
                return new bs(jSONObject2.getInt("width"), jSONObject2.getInt("height"));
            case 4:
                if (jSONObject2 == null || !jSONObject2.has("stats")) {
                    return null;
                }
                JSONArray jSONArray2 = jSONObject2.getJSONArray("stats");
                int length2 = jSONArray2.length();
                ArrayList arrayList = new ArrayList();
                while (i < length2) {
                    arrayList.add(jSONArray2.getString(i));
                    i++;
                }
                if (jSONObject2.has("type")) {
                    str = jSONObject2.getString("type");
                }
                return new bt(arrayList, str);
            case 5:
                if (jSONObject2 == null || !jSONObject2.has("format") || !jSONObject2.has(RequestParameters.BANNER_ID)) {
                    return null;
                }
                return new bn(jSONObject2.getString(RequestParameters.BANNER_ID), jSONObject2.getString("format"), jSONObject2.optString("url"));
            default:
                return new bm(string);
        }
    }
}
