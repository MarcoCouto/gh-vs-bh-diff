package com.my.target;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.util.List;

/* compiled from: JsStatEvent */
public class bt extends bl {
    @NonNull
    private final List<String> cl;
    @Nullable
    private String cm;

    public bt(@NonNull List<String> list, @Nullable String str) {
        super("onStat");
        this.cl = list;
        this.cm = str;
    }

    @NonNull
    public List<String> aG() {
        return this.cl;
    }
}
