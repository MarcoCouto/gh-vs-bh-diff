package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.view.View;
import android.view.View.OnClickListener;
import com.my.target.es.a;

/* compiled from: InterstitialImagePresenter */
public class ep implements es {
    @Nullable
    a eO;
    @NonNull
    private final fv eP;

    public void destroy() {
    }

    public void pause() {
    }

    public void resume() {
    }

    public void stop() {
    }

    public static ep t(Context context) {
        return new ep(new fv(context));
    }

    @VisibleForTesting
    ep(@NonNull fv fvVar) {
        this.eP = fvVar;
    }

    @NonNull
    public View cI() {
        return this.eP;
    }

    public void e(@NonNull final cl clVar) {
        this.eP.a(clVar.getOptimalLandscapeImage(), clVar.getOptimalPortraitImage(), clVar.getCloseIcon());
        this.eP.setAgeRestrictions(clVar.getAgeRestrictions());
        this.eP.getImageView().setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                if (ep.this.eO != null) {
                    ep.this.eO.b(clVar, null, view.getContext());
                }
            }
        });
        this.eP.getCloseButton().setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                if (ep.this.eO != null) {
                    ep.this.eO.ag();
                }
            }
        });
        if (this.eO != null) {
            this.eO.a(clVar, this.eP.getContext());
        }
    }

    public void a(@Nullable a aVar) {
        this.eO = aVar;
    }
}
