package com.my.target;

import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/* compiled from: AsyncCommand */
public abstract class ai {
    /* access modifiers changed from: private */
    public static final Handler handler = new Handler(Looper.getMainLooper());
    private static final Executor w = Executors.newSingleThreadExecutor();
    private static final Executor x = Executors.newSingleThreadExecutor();
    private static final Executor y = new Executor() {
        public void execute(@NonNull Runnable runnable) {
            ai.handler.post(runnable);
        }
    };

    public static void a(@NonNull Runnable runnable) {
        w.execute(runnable);
    }

    public static void b(@NonNull Runnable runnable) {
        x.execute(runnable);
    }

    public static void c(@NonNull Runnable runnable) {
        y.execute(runnable);
    }
}
