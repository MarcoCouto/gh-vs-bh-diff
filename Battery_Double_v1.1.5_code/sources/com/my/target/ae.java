package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.my.target.b.C0072b;

/* compiled from: StandardAdFactory */
public final class ae extends b<dc> {
    @Nullable
    private final dc section;

    public interface a extends C0072b {
    }

    /* compiled from: StandardAdFactory */
    static class b implements com.my.target.b.a<dc> {
        public boolean a() {
            return false;
        }

        private b() {
        }

        @NonNull
        public c<dc> b() {
            return af.f();
        }

        @Nullable
        public d<dc> c() {
            return ag.n();
        }

        @NonNull
        public e d() {
            return e.e();
        }
    }

    @NonNull
    public static b<dc> a(@NonNull a aVar) {
        return new ae(aVar, null);
    }

    @NonNull
    public static b<dc> a(@NonNull dc dcVar, @NonNull a aVar) {
        return new ae(aVar, dcVar);
    }

    private ae(@NonNull a aVar, @Nullable dc dcVar) {
        super(new b(), aVar);
        this.section = dcVar;
    }

    /* access modifiers changed from: protected */
    @Nullable
    /* renamed from: h */
    public dc b(@NonNull Context context) {
        if (this.section != null) {
            return (dc) a(this.section, context);
        }
        return (dc) super.b(context);
    }
}
