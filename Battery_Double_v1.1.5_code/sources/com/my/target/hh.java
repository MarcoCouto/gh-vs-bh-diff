package com.my.target;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

/* compiled from: MediaUtils */
public class hh {
    @NonNull
    public static float[] a(float f, @NonNull float[] fArr) {
        float[] fArr2 = new float[fArr.length];
        for (int i = 0; i < fArr.length; i++) {
            fArr2[i] = (f / 100.0f) * fArr[i];
        }
        return fArr2;
    }

    @NonNull
    public static <T extends cd> float[] a(@NonNull cz<T> czVar, @Nullable float[] fArr, float f) {
        float f2;
        float f3;
        HashSet hashSet = new HashSet();
        if (fArr != null && fArr.length > 0) {
            Arrays.sort(fArr);
        }
        List<cn> bI = czVar.bI();
        int i = 0;
        int i2 = 0;
        for (cn cnVar : bI) {
            if (fArr == null) {
                float f4 = cnVar.getPointP() > 0.0f ? (cnVar.getPointP() / 100.0f) * f : (cnVar.getPoint() < 0.0f || cnVar.getPoint() > f) ? f / 2.0f : cnVar.getPoint();
                f3 = ((float) Math.round(f4 * 10.0f)) / 10.0f;
                cnVar.setPoint(f3);
            } else if (i2 < fArr.length) {
                f3 = fArr[i2];
                if (!cnVar.getType().equals("statistics")) {
                    i2++;
                }
                if (f3 > f) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("Cannot set midroll position ");
                    sb.append(f3);
                    sb.append(": out of duration");
                    ah.a(sb.toString());
                    cnVar.setPoint(-1.0f);
                } else {
                    cnVar.setPoint(f3);
                }
            } else {
                cnVar.setPoint(-1.0f);
            }
            hashSet.add(Float.valueOf(f3));
        }
        if (fArr == null || fArr.length > bI.size()) {
            Iterator it = czVar.bL().iterator();
            while (it.hasNext()) {
                bz bzVar = (bz) it.next();
                if (fArr == null) {
                    float f5 = bzVar.getPointP() >= 0.0f ? (bzVar.getPointP() / 100.0f) * f : (bzVar.getPoint() < 0.0f || bzVar.getPoint() > f) ? f / 2.0f : bzVar.getPoint();
                    f2 = ((float) Math.round(f5 * 10.0f)) / 10.0f;
                    bzVar.setPoint(f2);
                } else if (i2 < fArr.length) {
                    int i3 = i2 + 1;
                    float f6 = fArr[i2];
                    if (f6 > f) {
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append("Cannot set midroll position ");
                        sb2.append(f6);
                        sb2.append(": out of duration");
                        ah.a(sb2.toString());
                        bzVar.setPoint(-1.0f);
                        i2 = i3;
                    } else {
                        bzVar.setPoint(f6);
                        float f7 = f6;
                        i2 = i3;
                        f2 = f7;
                    }
                } else {
                    bzVar.setPoint(-1.0f);
                }
                hashSet.add(Float.valueOf(f2));
            }
        }
        float[] fArr2 = new float[hashSet.size()];
        Iterator it2 = hashSet.iterator();
        while (it2.hasNext()) {
            int i4 = i + 1;
            fArr2[i] = ((Float) it2.next()).floatValue();
            i = i4;
        }
        Arrays.sort(fArr2);
        return fArr2;
    }

    public static boolean dQ() {
        try {
            Class.forName("com.google.android.exoplayer2.ExoPlayer");
            Class.forName(hs.class.getName());
            return true;
        } catch (ClassNotFoundException | NoClassDefFoundError unused) {
            ah.a("ExoPlayer doesn't exist, add ExoPlayer dependency to play video");
            return false;
        }
    }

    public static boolean dR() {
        try {
            Class.forName("com.google.android.exoplayer2.source.hls.HlsMediaSource$Factory");
            return true;
        } catch (ClassNotFoundException unused) {
            return false;
        }
    }
}
