package com.my.target;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewCompat;
import android.text.TextUtils;
import android.text.TextUtils.TruncateAt;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.my.target.common.models.ImageData;
import io.fabric.sdk.android.services.settings.SettingsJsonConstants;
import java.util.HashMap;
import java.util.List;

/* compiled from: CarouselView */
public class gv extends ViewGroup implements OnTouchListener, gn {
    private static final int CTA_ID = hm.dV();
    private static final int ij = hm.dV();
    private static final int jG = hm.dV();
    private static final int jX = hm.dV();
    private static final int kZ = hm.dV();
    private static final int la = hm.dV();
    private static final int lb = hm.dV();
    @NonNull
    private final Button ctaButton;
    @NonNull
    private final fu eH;
    @NonNull
    private final TextView hh;
    @NonNull
    private final fx iconImageView;
    private final boolean jB;
    @NonNull
    private final HashMap<View, Boolean> jC;
    /* access modifiers changed from: private */
    @Nullable
    public com.my.target.gn.a jR;
    private final int kK;
    @NonNull
    private final TextView lc;
    @NonNull
    private final TextView ld;
    @NonNull
    private final gu le;
    private final int lf;
    private final int lg;
    private final double lh;
    @NonNull
    private final hm uiUtils;

    /* compiled from: CarouselView */
    public interface a {
        void a(@NonNull cj cjVar);

        void b(@NonNull List<cj> list);
    }

    @NonNull
    public View getView() {
        return this;
    }

    public gv(@NonNull Context context) {
        super(context);
        hm.a(this, -1, -3806472);
        this.jB = (context.getResources().getConfiguration().screenLayout & 15) >= 3;
        this.lh = this.jB ? 0.5d : 0.7d;
        this.eH = new fu(context);
        this.uiUtils = hm.R(context);
        this.hh = new TextView(context);
        this.lc = new TextView(context);
        this.ld = new TextView(context);
        this.iconImageView = new fx(context);
        this.ctaButton = new Button(context);
        this.le = new gu(context);
        this.eH.setId(kZ);
        this.eH.setContentDescription("close");
        this.eH.setVisibility(4);
        this.iconImageView.setId(jG);
        this.iconImageView.setContentDescription(SettingsJsonConstants.APP_ICON_KEY);
        this.hh.setId(ij);
        this.hh.setLines(1);
        this.hh.setEllipsize(TruncateAt.END);
        this.lc.setId(lb);
        this.lc.setLines(1);
        this.lc.setEllipsize(TruncateAt.END);
        this.ld.setId(jX);
        this.ld.setTextColor(ViewCompat.MEASURED_STATE_MASK);
        this.ctaButton.setId(CTA_ID);
        this.ctaButton.setPadding(this.uiUtils.E(15), this.uiUtils.E(10), this.uiUtils.E(15), this.uiUtils.E(10));
        this.ctaButton.setMinimumWidth(this.uiUtils.E(100));
        this.ctaButton.setMaxEms(12);
        this.ctaButton.setTransformationMethod(null);
        this.ctaButton.setSingleLine();
        this.ctaButton.setTextSize(18.0f);
        this.ctaButton.setEllipsize(TruncateAt.END);
        if (VERSION.SDK_INT >= 21) {
            this.ctaButton.setElevation((float) this.uiUtils.E(2));
        }
        hm.a(this.ctaButton, -16733198, -16746839, this.uiUtils.E(2));
        this.ctaButton.setTextColor(-1);
        this.le.setId(la);
        this.le.setPadding(0, 0, 0, this.uiUtils.E(8));
        this.le.setSideSlidesMargins(this.uiUtils.E(10));
        if (this.jB) {
            this.lf = this.uiUtils.E(18);
            this.kK = this.lf;
            DisplayMetrics displayMetrics = new DisplayMetrics();
            WindowManager windowManager = (WindowManager) context.getSystemService("window");
            if (windowManager != null) {
                windowManager.getDefaultDisplay().getMetrics(displayMetrics);
            }
            this.hh.setTextSize((float) this.uiUtils.F(24));
            this.ld.setTextSize((float) this.uiUtils.F(20));
            this.lc.setTextSize((float) this.uiUtils.F(20));
            this.lg = this.uiUtils.E(96);
            this.hh.setTypeface(null, 1);
        } else {
            this.kK = this.uiUtils.E(12);
            this.lf = this.uiUtils.E(10);
            this.hh.setTextSize(22.0f);
            this.ld.setTextSize(18.0f);
            this.lc.setTextSize(18.0f);
            this.lg = this.uiUtils.E(64);
        }
        hm.a((View) this, "ad_view");
        hm.a((View) this.hh, "title_text");
        hm.a((View) this.ld, "description_text");
        hm.a((View) this.iconImageView, "icon_image");
        hm.a((View) this.eH, "close_button");
        hm.a((View) this.lc, "category_text");
        addView(this.le);
        addView(this.iconImageView);
        addView(this.hh);
        addView(this.lc);
        addView(this.ld);
        addView(this.eH);
        addView(this.ctaButton);
        this.jC = new HashMap<>();
    }

    public void setBanner(@NonNull cm cmVar) {
        ImageData closeIcon = cmVar.getCloseIcon();
        if (closeIcon == null || closeIcon.getData() == null) {
            Bitmap u = fg.u(this.uiUtils.E(28));
            if (u != null) {
                this.eH.a(u, false);
            }
        } else {
            this.eH.a(closeIcon.getData(), true);
        }
        this.ctaButton.setText(cmVar.getCtaText());
        ImageData icon = cmVar.getIcon();
        if (icon != null) {
            this.iconImageView.setPlaceholderHeight(icon.getHeight());
            this.iconImageView.setPlaceholderWidth(icon.getWidth());
            hg.a(icon, (ImageView) this.iconImageView);
        }
        this.hh.setTextColor(ViewCompat.MEASURED_STATE_MASK);
        this.hh.setText(cmVar.getTitle());
        String category = cmVar.getCategory();
        String subCategory = cmVar.getSubCategory();
        String str = "";
        if (!TextUtils.isEmpty(category)) {
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append(category);
            str = sb.toString();
        }
        if (!TextUtils.isEmpty(str) && !TextUtils.isEmpty(subCategory)) {
            StringBuilder sb2 = new StringBuilder();
            sb2.append(str);
            sb2.append(", ");
            str = sb2.toString();
        }
        if (!TextUtils.isEmpty(subCategory)) {
            StringBuilder sb3 = new StringBuilder();
            sb3.append(str);
            sb3.append(subCategory);
            str = sb3.toString();
        }
        if (!TextUtils.isEmpty(str)) {
            this.lc.setText(str);
            this.lc.setVisibility(0);
        } else {
            this.lc.setVisibility(8);
        }
        this.ld.setText(cmVar.getDescription());
        this.le.d(cmVar.getInterstitialAdCards());
    }

    public void setInterstitialPromoViewListener(@Nullable com.my.target.gn.a aVar) {
        this.jR = aVar;
    }

    public void setCarouselListener(@Nullable a aVar) {
        this.le.setCarouselListener(aVar);
    }

    public void dF() {
        this.eH.setVisibility(0);
    }

    @SuppressLint({"ClickableViewAccessibility"})
    public void setClickArea(@NonNull ca caVar) {
        boolean z = true;
        if (caVar.dn) {
            setOnClickListener(new OnClickListener() {
                public void onClick(View view) {
                    if (gv.this.jR != null) {
                        gv.this.jR.da();
                    }
                }
            });
            hm.a(this, -1, -3806472);
            setClickable(true);
            return;
        }
        this.hh.setOnTouchListener(this);
        this.lc.setOnTouchListener(this);
        this.iconImageView.setOnTouchListener(this);
        this.ld.setOnTouchListener(this);
        this.ctaButton.setOnTouchListener(this);
        setOnTouchListener(this);
        this.jC.put(this.hh, Boolean.valueOf(caVar.db));
        this.jC.put(this.lc, Boolean.valueOf(caVar.dl));
        this.jC.put(this.iconImageView, Boolean.valueOf(caVar.dd));
        this.jC.put(this.ld, Boolean.valueOf(caVar.dc));
        HashMap<View, Boolean> hashMap = this.jC;
        Button button = this.ctaButton;
        if (!caVar.dm && !caVar.dh) {
            z = false;
        }
        hashMap.put(button, Boolean.valueOf(z));
        this.jC.put(this, Boolean.valueOf(caVar.dm));
    }

    @NonNull
    public View getCloseButton() {
        return this.eH;
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (!this.jC.containsKey(view)) {
            return false;
        }
        if (!((Boolean) this.jC.get(view)).booleanValue()) {
            return true;
        }
        int action = motionEvent.getAction();
        if (action != 3) {
            switch (action) {
                case 0:
                    setBackgroundColor(-3806472);
                    break;
                case 1:
                    setBackgroundColor(-1);
                    if (this.jR != null) {
                        this.jR.da();
                        break;
                    }
                    break;
            }
        } else {
            setBackgroundColor(-1);
        }
        return true;
    }

    @NonNull
    public int[] getNumbersOfCurrentShowingCards() {
        int findFirstVisibleItemPosition = this.le.getCardLayoutManager().findFirstVisibleItemPosition();
        int findLastCompletelyVisibleItemPosition = this.le.getCardLayoutManager().findLastCompletelyVisibleItemPosition();
        int i = 0;
        if (findFirstVisibleItemPosition == -1 || findLastCompletelyVisibleItemPosition == -1) {
            return new int[0];
        }
        int i2 = (findLastCompletelyVisibleItemPosition - findFirstVisibleItemPosition) + 1;
        int[] iArr = new int[i2];
        while (i < i2) {
            int i3 = findFirstVisibleItemPosition + 1;
            iArr[i] = findFirstVisibleItemPosition;
            i++;
            findFirstVisibleItemPosition = i3;
        }
        return iArr;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int size = MeasureSpec.getSize(i);
        int size2 = MeasureSpec.getSize(i2);
        this.eH.measure(MeasureSpec.makeMeasureSpec(size, Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(size2, Integer.MIN_VALUE));
        this.iconImageView.measure(MeasureSpec.makeMeasureSpec(this.lg, Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(this.lg, Integer.MIN_VALUE));
        if (size2 > size || this.jB) {
            this.ctaButton.setVisibility(8);
            int measuredHeight = this.eH.getMeasuredHeight();
            if (this.jB) {
                measuredHeight = this.lf;
            }
            this.hh.measure(MeasureSpec.makeMeasureSpec((size - (this.lf * 2)) - this.iconImageView.getMeasuredWidth(), Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(size2, Integer.MIN_VALUE));
            this.lc.measure(MeasureSpec.makeMeasureSpec((size - (this.lf * 2)) - this.iconImageView.getMeasuredWidth(), Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(size2, Integer.MIN_VALUE));
            this.ld.measure(MeasureSpec.makeMeasureSpec(size - (this.lf * 2), Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(size2, Integer.MIN_VALUE));
            int max = ((size2 - measuredHeight) - Math.max(this.hh.getMeasuredHeight() + this.lc.getMeasuredHeight(), this.iconImageView.getMeasuredHeight() - (this.lf * 2))) - this.ld.getMeasuredHeight();
            int i3 = size - this.lf;
            if (size2 > size && ((double) (((float) max) / ((float) size2))) > this.lh) {
                double d = (double) size2;
                double d2 = this.lh;
                Double.isNaN(d);
                max = (int) (d * d2);
            }
            if (this.jB) {
                this.le.measure(MeasureSpec.makeMeasureSpec(i3, 1073741824), MeasureSpec.makeMeasureSpec(max - (this.lf * 2), Integer.MIN_VALUE));
            } else {
                this.le.measure(MeasureSpec.makeMeasureSpec(i3, 1073741824), MeasureSpec.makeMeasureSpec(max - (this.lf * 2), 1073741824));
            }
        } else {
            this.ctaButton.setVisibility(0);
            this.ctaButton.measure(MeasureSpec.makeMeasureSpec(size, Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(size2, Integer.MIN_VALUE));
            int measuredWidth = this.ctaButton.getMeasuredWidth();
            int i4 = size / 2;
            if (measuredWidth > i4 - (this.lf * 2)) {
                this.ctaButton.measure(MeasureSpec.makeMeasureSpec(i4 - (this.lf * 2), Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(size2, Integer.MIN_VALUE));
            }
            this.hh.measure(MeasureSpec.makeMeasureSpec((((size - this.iconImageView.getMeasuredWidth()) - measuredWidth) - this.kK) - this.lf, Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(size2, Integer.MIN_VALUE));
            this.lc.measure(MeasureSpec.makeMeasureSpec((((size - this.iconImageView.getMeasuredWidth()) - measuredWidth) - this.kK) - this.lf, Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(size2, Integer.MIN_VALUE));
            this.le.measure(MeasureSpec.makeMeasureSpec(size - this.lf, Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec((((size2 - Math.max(this.iconImageView.getMeasuredHeight(), Math.max(this.ctaButton.getMeasuredHeight(), this.hh.getMeasuredHeight() + this.lc.getMeasuredHeight()))) - (this.lf * 2)) - this.le.getPaddingBottom()) - this.le.getPaddingTop(), Integer.MIN_VALUE));
        }
        setMeasuredDimension(size, size2);
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        int i5 = i3 - i;
        int i6 = i4 - i2;
        this.eH.layout(i3 - this.eH.getMeasuredWidth(), i2, i3, this.eH.getMeasuredHeight() + i2);
        if (i6 > i5 || this.jB) {
            int bottom = this.eH.getBottom();
            int measuredHeight = this.le.getMeasuredHeight() + Math.max(this.hh.getMeasuredHeight() + this.lc.getMeasuredHeight(), this.iconImageView.getMeasuredHeight()) + this.ld.getMeasuredHeight() + (this.lf * 2);
            if (measuredHeight < i6) {
                int i7 = (i6 - measuredHeight) / 2;
                if (i7 > bottom) {
                    bottom = i7;
                }
            }
            this.iconImageView.layout(this.lf + i, bottom, this.iconImageView.getMeasuredWidth() + i + this.lf, i2 + this.iconImageView.getMeasuredHeight() + bottom);
            this.hh.layout(this.iconImageView.getRight(), bottom, this.iconImageView.getRight() + this.hh.getMeasuredWidth(), this.hh.getMeasuredHeight() + bottom);
            this.lc.layout(this.iconImageView.getRight(), this.hh.getBottom(), this.iconImageView.getRight() + this.lc.getMeasuredWidth(), this.hh.getBottom() + this.lc.getMeasuredHeight());
            int max = Math.max(Math.max(this.iconImageView.getBottom(), this.lc.getBottom()), this.hh.getBottom());
            this.ld.layout(this.lf + i, max, this.lf + i + this.ld.getMeasuredWidth(), this.ld.getMeasuredHeight() + max);
            int max2 = Math.max(max, this.ld.getBottom()) + this.lf;
            this.le.layout(i + this.lf, max2, i3, this.le.getMeasuredHeight() + max2);
            this.le.B(!this.jB);
            return;
        }
        this.le.B(false);
        this.iconImageView.layout(this.lf, (i4 - this.lf) - this.iconImageView.getMeasuredHeight(), this.lf + this.iconImageView.getMeasuredWidth(), i4 - this.lf);
        int max3 = ((Math.max(this.iconImageView.getMeasuredHeight(), this.ctaButton.getMeasuredHeight()) - this.hh.getMeasuredHeight()) - this.lc.getMeasuredHeight()) / 2;
        if (max3 < 0) {
            max3 = 0;
        }
        this.lc.layout(this.iconImageView.getRight(), ((i4 - this.lf) - max3) - this.lc.getMeasuredHeight(), this.iconImageView.getRight() + this.lc.getMeasuredWidth(), (i4 - this.lf) - max3);
        this.hh.layout(this.iconImageView.getRight(), this.lc.getTop() - this.hh.getMeasuredHeight(), this.iconImageView.getRight() + this.hh.getMeasuredWidth(), this.lc.getTop());
        int max4 = (Math.max(this.iconImageView.getMeasuredHeight(), this.hh.getMeasuredHeight() + this.lc.getMeasuredHeight()) - this.ctaButton.getMeasuredHeight()) / 2;
        if (max4 < 0) {
            max4 = 0;
        }
        this.ctaButton.layout((i3 - this.lf) - this.ctaButton.getMeasuredWidth(), ((i4 - this.lf) - max4) - this.ctaButton.getMeasuredHeight(), i3 - this.lf, (i4 - this.lf) - max4);
        this.le.layout(this.lf, this.lf, i3, this.lf + this.le.getMeasuredHeight());
        this.ld.layout(0, 0, 0, 0);
    }
}
