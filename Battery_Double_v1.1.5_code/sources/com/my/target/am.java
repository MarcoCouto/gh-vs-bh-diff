package com.my.target;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import com.my.target.al.b;
import com.my.target.common.models.ImageData;
import com.my.target.common.models.VideoData;
import com.my.target.nativeads.views.MediaAdView;
import com.my.target.nativeads.views.PromoCardRecyclerView;
import java.lang.ref.WeakReference;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

/* compiled from: NativeAdViewController */
public class am {
    @Nullable
    private WeakReference<MediaAdView> W;
    private final boolean ak;
    private final boolean al;
    @NonNull
    private final a am;
    @NonNull
    private final co an;
    @NonNull
    private final hb ao;
    private int ap = 0;
    @Nullable
    private WeakReference<View> aq;
    @Nullable
    private WeakReference<gh> ar;
    @Nullable
    private WeakReference<gd> as;
    @Nullable
    private HashSet<WeakReference<View>> at;
    @Nullable
    private al au;
    private boolean av;
    @Nullable
    private Parcelable aw;
    private boolean ax;
    private boolean ay;

    /* compiled from: NativeAdViewController */
    public interface a extends OnClickListener, b, com.my.target.gh.a {
    }

    public static am a(@NonNull co coVar, @NonNull a aVar) {
        return new am(coVar, aVar);
    }

    private am(@NonNull co coVar, @NonNull a aVar) {
        boolean z = false;
        this.am = aVar;
        this.an = coVar;
        this.ak = coVar.getNativeAdCards().size() > 0;
        cn videoBanner = coVar.getVideoBanner();
        if (!(videoBanner == null || videoBanner.getMediaData() == null)) {
            z = true;
        }
        this.al = z;
        this.ao = hb.a(coVar.getAdChoices());
    }

    @Nullable
    public Context getContext() {
        if (this.aq != null) {
            View view = (View) this.aq.get();
            if (view != null) {
                return view.getContext();
            }
        }
        return null;
    }

    public boolean S() {
        return this.ax;
    }

    public int T() {
        return this.ap;
    }

    public void b(boolean z) {
        if (this.au == null) {
            return;
        }
        if (z) {
            this.au.v();
        } else {
            this.au.w();
        }
    }

    public int U() {
        View view = this.aq != null ? (View) this.aq.get() : null;
        if (view == null) {
            return -1;
        }
        if (view.getVisibility() != 0 || view.getParent() == null || view.getAlpha() < 0.5f) {
            return 0;
        }
        Rect rect = new Rect();
        if (view.getGlobalVisibleRect(rect)) {
            double width = (double) (rect.width() * rect.height());
            double width2 = (double) (view.getWidth() * view.getHeight());
            double viewabilitySquare = (double) this.an.getViewabilitySquare();
            Double.isNaN(width2);
            Double.isNaN(viewabilitySquare);
            if (width >= width2 * viewabilitySquare) {
                return 1;
            }
        }
        return 0;
    }

    public void V() {
        if (this.au != null) {
            this.au.unregister();
        }
    }

    public void unregisterView() {
        View view = this.aq != null ? (View) this.aq.get() : null;
        V();
        if (this.ar != null) {
            gh ghVar = (gh) this.ar.get();
            if (ghVar != null) {
                ghVar.setPromoCardSliderListener(null);
                this.aw = ghVar.getState();
                ghVar.dispose();
            }
            this.ar = null;
        }
        if (this.W != null) {
            MediaAdView mediaAdView = (MediaAdView) this.W.get();
            if (mediaAdView != null) {
                c(mediaAdView);
            }
            this.W = null;
        }
        W();
        if (this.at != null) {
            Iterator it = this.at.iterator();
            while (it.hasNext()) {
                WeakReference weakReference = (WeakReference) it.next();
                if (weakReference != null) {
                    View view2 = (View) weakReference.get();
                    if (view2 != null) {
                        view2.setOnClickListener(null);
                    }
                }
            }
            this.at = null;
        } else if (view != null) {
            d(view);
        }
        if (view != null) {
            this.ao.i(view);
        }
        if (this.aq != null) {
            this.aq.clear();
            this.aq = null;
        }
    }

    public void W() {
        if (this.as != null) {
            gd gdVar = (gd) this.as.get();
            if (gdVar != null) {
                gdVar.setViewabilityListener(null);
            }
            this.as.clear();
            this.as = null;
        }
    }

    @Nullable
    public int[] X() {
        if (this.ap == 2) {
            if (this.ar != null) {
                gh ghVar = (gh) this.ar.get();
                if (ghVar != null) {
                    return ghVar.getVisibleCardNumbers();
                }
            }
        } else if (this.ap == 3 && this.W != null) {
            MediaAdView mediaAdView = (MediaAdView) this.W.get();
            if (mediaAdView != null) {
                gg b = b(mediaAdView);
                if (b != null) {
                    return b.getVisibleCardNumbers();
                }
            }
        }
        return null;
    }

    public void a(@NonNull View view, @Nullable List<View> list, @Nullable com.my.target.gd.a aVar, int i) {
        if (list != null) {
            this.at = new HashSet<>();
            for (View view2 : list) {
                this.at.add(new WeakReference(view2));
                if (view2 instanceof MediaAdView) {
                    this.av = true;
                } else {
                    view2.setOnClickListener(this.am);
                }
            }
        }
        this.aq = new WeakReference<>(view);
        if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            int childCount = viewGroup.getChildCount();
            gd gdVar = null;
            fl flVar = null;
            for (int i2 = 0; i2 < childCount; i2++) {
                View childAt = viewGroup.getChildAt(i2);
                if (childAt instanceof fl) {
                    flVar = (fl) childAt;
                } else if (childAt instanceof gd) {
                    gdVar = (gd) childAt;
                }
            }
            a(aVar, gdVar, viewGroup);
            this.ao.a(viewGroup, flVar, i);
        }
        c(view);
    }

    public boolean Y() {
        if (this.as != null) {
            gd gdVar = (gd) this.as.get();
            if (gdVar != null) {
                return gdVar.dC();
            }
        }
        return false;
    }

    private void a(@Nullable com.my.target.gd.a aVar, @Nullable gd gdVar, @NonNull ViewGroup viewGroup) {
        if (gdVar == null) {
            gdVar = new gd(viewGroup.getContext());
            gdVar.setId(hm.dV());
            hm.a((View) gdVar, "viewability_view");
            try {
                viewGroup.addView(gdVar);
            } catch (Exception e) {
                StringBuilder sb = new StringBuilder();
                sb.append("Unable to add Viewability View: ");
                sb.append(e.getMessage());
                ah.a(sb.toString());
                this.ax = true;
                return;
            }
        }
        gdVar.setViewabilityListener(aVar);
        this.as = new WeakReference<>(gdVar);
    }

    private void c(@NonNull View view) {
        if (view instanceof ViewGroup) {
            a((ViewGroup) view);
        } else if (!(view instanceof fl) && this.at == null) {
            view.setOnClickListener(this.am);
        }
    }

    private void a(@NonNull ViewGroup viewGroup) {
        if (this.ak && (viewGroup instanceof PromoCardRecyclerView)) {
            a((gh) (PromoCardRecyclerView) viewGroup);
        } else if (viewGroup instanceof MediaAdView) {
            a((MediaAdView) viewGroup);
        } else {
            if (this.at == null) {
                viewGroup.setOnClickListener(this.am);
            }
            for (int i = 0; i < viewGroup.getChildCount(); i++) {
                View childAt = viewGroup.getChildAt(i);
                if (childAt != null) {
                    c(childAt);
                }
            }
        }
    }

    private void a(@NonNull gh ghVar) {
        this.ap = 2;
        ghVar.setPromoCardSliderListener(this.am);
        if (this.aw != null) {
            ghVar.restoreState(this.aw);
        }
        this.ar = new WeakReference<>(ghVar);
    }

    private void a(@NonNull MediaAdView mediaAdView) {
        this.W = new WeakReference<>(mediaAdView);
        ImageData image = this.an.getImage();
        if (this.ak) {
            a(mediaAdView, image);
            return;
        }
        d(mediaAdView, image);
        if (this.al) {
            a(mediaAdView, (b) this.am);
        } else {
            b(mediaAdView, image);
        }
    }

    private void a(@NonNull MediaAdView mediaAdView, @Nullable ImageData imageData) {
        c(mediaAdView, imageData);
        if (this.ap != 2) {
            this.ap = 3;
            Context context = mediaAdView.getContext();
            gg b = b(mediaAdView);
            if (b == null) {
                b = new gg(context);
                mediaAdView.addView(b, new LayoutParams(-1, -1));
            }
            if (this.aw != null) {
                b.restoreState(this.aw);
            }
            b.setClickable(this.at == null || this.av);
            b.setupCards(this.an.getNativeAdCards());
            b.setPromoCardSliderListener(this.am);
            mediaAdView.setBackgroundColor(0);
            b.setVisibility(0);
        }
    }

    private void b(@NonNull MediaAdView mediaAdView, @Nullable ImageData imageData) {
        c(mediaAdView, imageData);
        this.ap = 0;
        mediaAdView.getImageView().setVisibility(0);
        mediaAdView.getPlayButtonView().setVisibility(8);
        mediaAdView.getProgressBarView().setVisibility(8);
        if (this.at == null || this.av) {
            mediaAdView.setOnClickListener(this.am);
        }
    }

    private void c(@NonNull MediaAdView mediaAdView, @Nullable ImageData imageData) {
        if (imageData != null) {
            int width = imageData.getWidth();
            int height = imageData.getHeight();
            if (this.ay || width <= 0 || height <= 0) {
                mediaAdView.setPlaceHolderDimension(16, 9);
                this.ay = true;
                return;
            }
            mediaAdView.setPlaceHolderDimension(width, height);
            return;
        }
        mediaAdView.setPlaceHolderDimension(0, 0);
    }

    @Nullable
    private gg b(@NonNull MediaAdView mediaAdView) {
        for (int i = 0; i < mediaAdView.getChildCount(); i++) {
            View childAt = mediaAdView.getChildAt(i);
            if (childAt instanceof gg) {
                return (gg) childAt;
            }
        }
        return null;
    }

    private void d(@NonNull MediaAdView mediaAdView, @Nullable ImageData imageData) {
        fx fxVar = (fx) mediaAdView.getImageView();
        if (imageData != null) {
            Bitmap bitmap = imageData.getBitmap();
            if (bitmap != null) {
                fxVar.setImageBitmap(bitmap);
                return;
            }
            fxVar.setImageBitmap(null);
            hg.a(imageData, (ImageView) fxVar);
            return;
        }
        fxVar.setImageBitmap(null);
    }

    private void a(@NonNull MediaAdView mediaAdView, @NonNull b bVar) {
        VideoData videoData;
        this.ap = 1;
        cn videoBanner = this.an.getVideoBanner();
        if (videoBanner != null) {
            mediaAdView.setPlaceHolderDimension(videoBanner.getWidth(), videoBanner.getHeight());
            videoData = (VideoData) videoBanner.getMediaData();
        } else {
            videoData = null;
        }
        if (this.au == null && videoData != null) {
            this.ap = 1;
            this.au = new al(this.an, videoBanner, videoData);
        }
        if (this.au != null) {
            this.au.a(bVar);
            a(mediaAdView, this.au);
        }
    }

    private void a(@NonNull MediaAdView mediaAdView, @NonNull al alVar) {
        Context context;
        alVar.a((OnClickListener) this.am);
        if (this.aq != null) {
            View view = (View) this.aq.get();
            if (view != null) {
                context = view.getContext();
                alVar.a(mediaAdView, context);
            }
        }
        context = null;
        alVar.a(mediaAdView, context);
    }

    private void d(@NonNull View view) {
        if (!(view instanceof ViewGroup)) {
            view.setOnClickListener(null);
        } else if (!(view instanceof RecyclerView) && !(view instanceof MediaAdView)) {
            ViewGroup viewGroup = (ViewGroup) view;
            for (int i = 0; i < viewGroup.getChildCount(); i++) {
                View childAt = viewGroup.getChildAt(i);
                if (childAt != null) {
                    d(childAt);
                }
            }
            viewGroup.setOnClickListener(null);
        }
    }

    private void c(@NonNull MediaAdView mediaAdView) {
        ImageData image = this.an.getImage();
        fx fxVar = (fx) mediaAdView.getImageView();
        if (image != null) {
            hg.b(image, (ImageView) fxVar);
        }
        mediaAdView.getProgressBarView().setVisibility(8);
        mediaAdView.getPlayButtonView().setVisibility(8);
        fxVar.setImageData(null);
        mediaAdView.setPlaceHolderDimension(0, 0);
        mediaAdView.setOnClickListener(null);
        mediaAdView.setBackgroundColor(MediaAdView.COLOR_PLACEHOLDER_GRAY);
        gg b = b(mediaAdView);
        if (b != null) {
            this.aw = b.getState();
            b.dispose();
            b.setVisibility(8);
        }
    }
}
