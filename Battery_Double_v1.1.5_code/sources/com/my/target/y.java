package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import com.my.target.common.MyTargetPrivacy;
import com.my.target.e.a;
import java.util.Map;

/* compiled from: NativeAdServiceBuilder */
public class y extends a {
    @NonNull
    public static y l() {
        return new y();
    }

    private y() {
    }

    /* access modifiers changed from: protected */
    @NonNull
    public Map<String, String> b(@NonNull a aVar, @NonNull Context context) {
        Map<String, String> b = super.b(aVar, context);
        if (MyTargetPrivacy.isConsentSpecified() && !MyTargetPrivacy.isUserConsent()) {
            return b;
        }
        Map snapshot = da.bP().snapshot();
        if (snapshot != null && snapshot.size() > 0) {
            StringBuilder sb = new StringBuilder();
            boolean z = false;
            for (String str : snapshot.keySet()) {
                if (z) {
                    sb.append(",");
                } else {
                    z = true;
                }
                sb.append(str);
            }
            String sb2 = sb.toString();
            b.put("exb", sb2);
            StringBuilder sb3 = new StringBuilder();
            sb3.append("Exclude list: ");
            sb3.append(sb2);
            ah.a(sb3.toString());
        }
        return b;
    }
}
