package com.my.target;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.my.target.common.models.VideoData;

/* compiled from: AdVideoPlayer */
public interface hq {

    /* compiled from: AdVideoPlayer */
    public interface a {
        void A();

        void B();

        void a(float f, float f2);

        void d(String str);

        void e(float f);

        void onComplete();

        void x();

        void y();

        void z();
    }

    void K();

    void L();

    void a(@NonNull VideoData videoData, @NonNull fn fnVar);

    void a(@Nullable a aVar);

    void cJ();

    void destroy();

    boolean isPaused();

    boolean isPlaying();

    boolean isStarted();

    void pause();

    void resume();

    void stop();
}
