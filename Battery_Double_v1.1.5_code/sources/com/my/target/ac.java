package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* compiled from: ResearchProgressTracker */
public class ac {
    @NonNull
    private final ArrayList<ce> o;
    @NonNull
    private final ArrayList<df> p;
    private int q = -1;

    @NonNull
    public static ac a(@NonNull dh dhVar) {
        return new ac(dhVar);
    }

    private ac(@NonNull dh dhVar) {
        ArrayList<ce> arrayList = new ArrayList<>();
        Iterator it = dhVar.N("playheadTimerValue").iterator();
        while (it.hasNext()) {
            dg dgVar = (dg) it.next();
            if (dgVar instanceof ce) {
                arrayList.add((ce) dgVar);
            }
        }
        this.o = arrayList;
        this.p = new ArrayList<>();
        dhVar.a(this.p);
    }

    public void a(int i, int i2, @NonNull Context context) {
        if (i2 >= 0 && i >= 0 && i != this.q) {
            this.q = i;
            if (!this.o.isEmpty() && i != 0) {
                Iterator it = this.o.iterator();
                while (it.hasNext()) {
                    a(i, (ce) it.next(), context);
                }
            }
            ArrayList arrayList = new ArrayList();
            while (!this.p.isEmpty() && ((df) this.p.get(this.p.size() - 1)).cf() <= ((float) i)) {
                arrayList.add((df) this.p.remove(this.p.size() - 1));
            }
            if (!arrayList.isEmpty()) {
                hl.a((List<dg>) arrayList, context);
            }
        }
    }

    private void a(int i, @NonNull ce ceVar, @NonNull Context context) {
        int bn = ceVar.bn();
        int bo = ceVar.bo();
        if ((bn <= i && (bo == 0 || bo >= i)) && (i - bn) % ceVar.bp() == 0) {
            String replace = ceVar.getUrl().replace("[CONTENTPLAYHEAD]", String.valueOf(i));
            if (!TextUtils.isEmpty(replace)) {
                hl.o(replace, context);
            }
        }
    }
}
