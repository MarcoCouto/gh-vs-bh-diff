package com.my.target;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

/* compiled from: InterstitialPromoView */
public interface gn {
    public static final int ix = hm.dV();

    /* compiled from: InterstitialPromoView */
    public interface a {
        void da();

        void s(boolean z);
    }

    void dF();

    @NonNull
    View getCloseButton();

    @NonNull
    View getView();

    void setBanner(@NonNull cm cmVar);

    void setClickArea(@NonNull ca caVar);

    void setInterstitialPromoViewListener(@Nullable a aVar);
}
