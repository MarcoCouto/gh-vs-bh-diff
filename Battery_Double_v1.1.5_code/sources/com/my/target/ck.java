package com.my.target;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/* compiled from: InterstitialAdHtmlBanner */
public class ck extends ci {
    @Nullable
    private String source;

    @NonNull
    public static ck newBanner() {
        return new ck();
    }

    @NonNull
    public static ck fromCompanion(@NonNull ch chVar) {
        ck newBanner = newBanner();
        newBanner.setId(chVar.getId());
        newBanner.setSource(chVar.getHtmlResource());
        newBanner.getStatHolder().a(chVar.getStatHolder(), 0.0f);
        newBanner.trackingLink = chVar.trackingLink;
        return newBanner;
    }

    public void setSource(@Nullable String str) {
        this.source = str;
    }

    @Nullable
    public String getSource() {
        return this.source;
    }

    private ck() {
    }
}
