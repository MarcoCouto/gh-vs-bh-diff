package com.my.target;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.Nullable;
import android.view.TextureView;
import android.view.View.MeasureSpec;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;

/* compiled from: AdVideoView */
public class fn extends FrameLayout {
    private final TextureView hl;
    private int videoHeight;
    private int videoWidth;

    public fn(Context context) {
        super(context);
        this.hl = new TextureView(context);
        dl();
    }

    public void e(int i, int i2) {
        this.videoWidth = i;
        this.videoHeight = i2;
        requestLayout();
        invalidate();
    }

    private void dl() {
        LayoutParams layoutParams = new LayoutParams(-2, -2);
        layoutParams.gravity = 17;
        addView(this.hl, layoutParams);
    }

    public TextureView getTextureView() {
        return this.hl;
    }

    @Nullable
    public Bitmap getScreenShot() {
        try {
            return this.hl.getBitmap(getWidth(), getHeight());
        } catch (Throwable th) {
            th.printStackTrace();
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int i3;
        int i4;
        int size = MeasureSpec.getSize(i);
        int size2 = MeasureSpec.getSize(i2);
        int mode = MeasureSpec.getMode(i);
        int mode2 = MeasureSpec.getMode(i2);
        if (this.videoWidth <= 0 || this.videoHeight <= 0) {
            super.onMeasure(i, i2);
            return;
        }
        float f = ((float) this.videoWidth) / ((float) this.videoHeight);
        if (mode == 0 && mode2 == 0) {
            size = this.videoWidth;
            size2 = this.videoHeight;
        } else if (mode == 0) {
            size = (int) (((float) size2) * f);
        } else if (mode2 == 0) {
            size2 = (int) (((float) size) / f);
        } else {
            float f2 = (float) size;
            float f3 = f2 / ((float) this.videoWidth);
            float f4 = (float) size2;
            if (Math.min(f3, f4 / ((float) this.videoHeight)) != f3 || f <= 0.0f) {
                i3 = size2;
                int i5 = size;
                size = (int) (f4 * f);
                i4 = i5;
            } else {
                int i6 = (int) (f2 / f);
                i3 = size2;
                size2 = i6;
                i4 = size;
            }
            this.hl.measure(MeasureSpec.makeMeasureSpec(size, 1073741824), MeasureSpec.makeMeasureSpec(size2, 1073741824));
            setMeasuredDimension(i4, i3);
        }
        i4 = size;
        i3 = size2;
        this.hl.measure(MeasureSpec.makeMeasureSpec(size, 1073741824), MeasureSpec.makeMeasureSpec(size2, 1073741824));
        setMeasuredDimension(i4, i3);
    }
}
