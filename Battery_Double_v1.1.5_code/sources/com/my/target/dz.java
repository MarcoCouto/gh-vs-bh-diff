package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.my.target.common.models.ImageData;
import org.json.JSONArray;
import org.json.JSONObject;

/* compiled from: InterstitialAdBannerParser */
public class dz {
    @NonNull
    private final a adConfig;
    @NonNull
    private final Context context;
    @NonNull
    private final bz eq;
    @NonNull
    private final dv er;

    @NonNull
    public static dz d(@NonNull bz bzVar, @NonNull a aVar, @NonNull Context context2) {
        return new dz(bzVar, aVar, context2);
    }

    private dz(@NonNull bz bzVar, @NonNull a aVar, @NonNull Context context2) {
        this.eq = bzVar;
        this.adConfig = aVar;
        this.context = context2;
        this.er = dv.b(bzVar, aVar, context2);
    }

    public boolean a(@NonNull JSONObject jSONObject, @NonNull cl clVar) {
        b(jSONObject, clVar);
        return ea.e(this.eq, this.adConfig, this.context).b(jSONObject, clVar);
    }

    public boolean a(@NonNull JSONObject jSONObject, @NonNull cm cmVar, @Nullable String str) {
        b(jSONObject, cmVar);
        cmVar.setFooterColor(ed.a(jSONObject, "footerColor", cmVar.getFooterColor()));
        cmVar.setCtaButtonColor(ed.a(jSONObject, "ctaButtonColor", cmVar.getCtaButtonColor()));
        cmVar.setCtaButtonTouchColor(ed.a(jSONObject, "ctaButtonTouchColor", cmVar.getCtaButtonTouchColor()));
        cmVar.setCtaButtonTextColor(ed.a(jSONObject, "ctaButtonTextColor", cmVar.getCtaButtonTextColor()));
        cmVar.setStyle(jSONObject.optInt("style", cmVar.getStyle()));
        cmVar.setCloseOnClick(jSONObject.optBoolean("closeOnClick", cmVar.isCloseOnClick()));
        String optString = jSONObject.optString("play_icon_hd");
        if (!TextUtils.isEmpty(optString)) {
            cmVar.setPlayIcon(ImageData.newImageData(optString));
        }
        String optString2 = jSONObject.optString("store_icon_hd");
        if (!TextUtils.isEmpty(optString2)) {
            cmVar.setStoreIcon(ImageData.newImageData(optString2));
        }
        JSONArray optJSONArray = jSONObject.optJSONArray("cards");
        if (optJSONArray != null) {
            int length = optJSONArray.length();
            for (int i = 0; i < length; i++) {
                JSONObject optJSONObject = optJSONArray.optJSONObject(i);
                if (optJSONObject != null) {
                    cj a2 = a(optJSONObject, (ci) cmVar);
                    if (a2 != null) {
                        cmVar.addInterstitialAdCard(a2);
                    }
                }
            }
        }
        if (cmVar.getInterstitialAdCards().isEmpty()) {
            JSONObject optJSONObject2 = jSONObject.optJSONObject("video");
            if (optJSONObject2 != null) {
                cn newVideoBanner = cn.newVideoBanner();
                newVideoBanner.setId(cmVar.getId());
                if (dw.c(this.eq, this.adConfig, this.context).a(optJSONObject2, newVideoBanner)) {
                    cmVar.setVideoBanner(newVideoBanner);
                    if (newVideoBanner.isAutoPlay()) {
                        cmVar.setAllowClose(newVideoBanner.isAllowClose());
                        cmVar.setAllowCloseDelay(newVideoBanner.getAllowCloseDelay());
                    }
                }
                JSONObject optJSONObject3 = jSONObject.optJSONObject(CampaignEx.JSON_NATIVE_VIDEO_ENDCARD);
                if (optJSONObject3 != null) {
                    ck newBanner = ck.newBanner();
                    if (a(optJSONObject3, newBanner, str)) {
                        cmVar.setEndCard(newBanner);
                    }
                }
            }
        }
        return true;
    }

    public boolean a(@NonNull JSONObject jSONObject, @NonNull ck ckVar, @Nullable String str) {
        String str2;
        String optString = jSONObject.optString("source", null);
        if (optString == null) {
            b("Required field", "Banner with type 'html' has no source field", ckVar.getId());
            return false;
        }
        String decode = hn.decode(optString);
        b(jSONObject, ckVar);
        if (!TextUtils.isEmpty(str)) {
            str2 = dv.g(str, decode);
            if (str2 != null) {
                ckVar.setType(CampaignEx.JSON_KEY_MRAID);
                ckVar.setSource(str2);
                return this.er.a(str2, jSONObject);
            }
        }
        str2 = decode;
        ckVar.setSource(str2);
        return this.er.a(str2, jSONObject);
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    @VisibleForTesting
    public cj a(@NonNull JSONObject jSONObject, @NonNull ci ciVar) {
        cj newCard = cj.newCard(ciVar);
        newCard.setClickArea(ciVar.getClickArea());
        this.er.a(jSONObject, (cg) newCard);
        if (TextUtils.isEmpty(newCard.getTrackingLink())) {
            b("Required field", "no tracking link in interstitialAdCard", ciVar.getId());
            return null;
        } else if (newCard.getImage() == null) {
            b("Required field", "no image in interstitialAdCard", ciVar.getId());
            return null;
        } else {
            newCard.setId(jSONObject.optString("cardID", newCard.getId()));
            return newCard;
        }
    }

    private void b(@NonNull JSONObject jSONObject, @NonNull ci ciVar) {
        this.er.a(jSONObject, (cg) ciVar);
        ciVar.setAllowBackButton(jSONObject.optBoolean("allowBackButton", ciVar.isAllowBackButton()));
        ciVar.setAllowCloseDelay((float) jSONObject.optDouble("allowCloseDelay", (double) ciVar.getAllowCloseDelay()));
        String optString = jSONObject.optString("close_icon_hd");
        if (!TextUtils.isEmpty(optString)) {
            ciVar.setCloseIcon(ImageData.newImageData(optString));
        }
    }

    private void b(@NonNull String str, @NonNull String str2, @NonNull String str3) {
        dp.P(str).Q(str2).r(this.adConfig.getSlotId()).S(str3).R(this.eq.getUrl()).q(this.context);
    }
}
