package com.my.target;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.UiThread;
import android.support.annotation.WorkerThread;
import android.widget.ImageView;
import com.my.target.common.models.ImageData;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.WeakHashMap;

/* compiled from: ImageLoader */
public class hg {
    /* access modifiers changed from: private */
    @NonNull
    public static final WeakHashMap<ImageView, ImageData> md = new WeakHashMap<>();
    private boolean ea;
    @NonNull
    private final List<ImageData> me;
    /* access modifiers changed from: private */
    @Nullable
    public a mf;

    /* compiled from: ImageLoader */
    public interface a {
        void aa();
    }

    @NonNull
    public static hg a(@NonNull ImageData imageData) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(imageData);
        return new hg(arrayList);
    }

    @NonNull
    public static hg e(@NonNull List<ImageData> list) {
        return new hg(list);
    }

    @UiThread
    public static void a(@NonNull final ImageData imageData, @NonNull ImageView imageView) {
        if (Looper.myLooper() != Looper.getMainLooper()) {
            ah.b("[ImageLoader] method loadAndDisplay called from worker thread");
        } else if (md.get(imageView) != imageData) {
            md.remove(imageView);
            if (imageData.getBitmap() != null) {
                a(imageData.getBitmap(), imageView);
                return;
            }
            md.put(imageView, imageData);
            final WeakReference weakReference = new WeakReference(imageView);
            a(imageData).a((a) new a() {
                public void aa() {
                    ImageView imageView = (ImageView) weakReference.get();
                    if (imageView != null && imageData == ((ImageData) hg.md.get(imageView))) {
                        hg.md.remove(imageView);
                        Bitmap bitmap = imageData.getBitmap();
                        if (bitmap != null) {
                            hg.a(bitmap, imageView);
                        }
                    }
                }
            }).O(imageView.getContext());
        }
    }

    @UiThread
    public static void b(@NonNull ImageData imageData, @NonNull ImageView imageView) {
        if (Looper.myLooper() != Looper.getMainLooper()) {
            ah.b("[ImageLoader] method cancel called from worker thread");
            return;
        }
        if (md.get(imageView) == imageData) {
            md.remove(imageView);
        }
    }

    /* access modifiers changed from: private */
    public static void a(@NonNull Bitmap bitmap, @NonNull ImageView imageView) {
        if (imageView instanceof fx) {
            ((fx) imageView).b(bitmap, true);
        } else {
            imageView.setImageBitmap(bitmap);
        }
    }

    @NonNull
    public hg a(@Nullable a aVar) {
        this.mf = aVar;
        return this;
    }

    @NonNull
    public hg C(boolean z) {
        this.ea = z;
        return this;
    }

    private hg(@NonNull List<ImageData> list) {
        this.me = list;
    }

    public void O(@NonNull Context context) {
        if (this.me.isEmpty()) {
            finish();
            return;
        }
        final Context applicationContext = context.getApplicationContext();
        ai.a(new Runnable() {
            public void run() {
                hg.this.P(applicationContext);
                hg.this.finish();
            }
        });
    }

    @WorkerThread
    public void P(@NonNull Context context) {
        if (Looper.myLooper() == Looper.getMainLooper()) {
            ah.b("[ImageLoader] method loadSync called from main thread");
            return;
        }
        Context applicationContext = context.getApplicationContext();
        dk cq = this.ea ? dk.cq() : dk.cp();
        for (ImageData imageData : this.me) {
            if (imageData.getBitmap() == null) {
                Bitmap bitmap = (Bitmap) cq.f(imageData.getUrl(), applicationContext);
                if (bitmap != null) {
                    imageData.setData(bitmap);
                    if (imageData.getHeight() == 0 || imageData.getWidth() == 0) {
                        imageData.setHeight(bitmap.getHeight());
                        imageData.setWidth(bitmap.getWidth());
                    }
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void finish() {
        if (this.mf != null) {
            ai.c(new Runnable() {
                public void run() {
                    if (hg.this.mf != null) {
                        hg.this.mf.aa();
                        hg.this.mf = null;
                    }
                }
            });
        }
    }
}
