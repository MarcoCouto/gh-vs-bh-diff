package com.my.target.mediation;

public interface MediationNativeAdConfig extends MediationAdConfig {
    int getAdChoicesPlacement();

    boolean isAutoLoadImages();

    boolean isAutoLoadVideo();
}
