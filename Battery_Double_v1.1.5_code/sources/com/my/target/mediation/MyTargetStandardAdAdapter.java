package com.my.target.mediation;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.my.target.ads.MyTargetView;
import com.my.target.ads.MyTargetView.MyTargetViewListener;
import com.my.target.ah;
import com.my.target.common.CustomParams;
import com.my.target.dc;
import com.my.target.mediation.MediationStandardAdAdapter.MediationStandardAdListener;
import java.util.Map.Entry;

public final class MyTargetStandardAdAdapter implements MediationStandardAdAdapter {
    @Nullable
    private MyTargetView myTargetView;
    @Nullable
    private dc section;

    class AdListener implements MyTargetViewListener {
        @NonNull
        private final MediationStandardAdListener mediationListener;

        AdListener(MediationStandardAdListener mediationStandardAdListener) {
            this.mediationListener = mediationStandardAdListener;
        }

        public void onLoad(@NonNull MyTargetView myTargetView) {
            ah.a("MyTargetStandardAdAdapter: ad loaded");
            this.mediationListener.onLoad(myTargetView, MyTargetStandardAdAdapter.this);
        }

        public void onNoAd(@NonNull String str, @NonNull MyTargetView myTargetView) {
            StringBuilder sb = new StringBuilder();
            sb.append("MyTargetStandardAdAdapter: no ad (");
            sb.append(str);
            sb.append(")");
            ah.a(sb.toString());
            this.mediationListener.onNoAd(str, MyTargetStandardAdAdapter.this);
        }

        public void onShow(@NonNull MyTargetView myTargetView) {
            ah.a("MyTargetStandardAdAdapter: ad shown");
            this.mediationListener.onShow(MyTargetStandardAdAdapter.this);
        }

        public void onClick(@NonNull MyTargetView myTargetView) {
            ah.a("MyTargetStandardAdAdapter: ad clicked");
            this.mediationListener.onClick(MyTargetStandardAdAdapter.this);
        }
    }

    public void setSection(@Nullable dc dcVar) {
        this.section = dcVar;
    }

    public void load(@NonNull MediationAdConfig mediationAdConfig, int i, @NonNull MediationStandardAdListener mediationStandardAdListener, @NonNull Context context) {
        String placementId = mediationAdConfig.getPlacementId();
        try {
            int parseInt = Integer.parseInt(placementId);
            this.myTargetView = new MyTargetView(context);
            this.myTargetView.init(parseInt, i, false);
            this.myTargetView.setMediationEnabled(false);
            this.myTargetView.setListener(new AdListener(mediationStandardAdListener));
            this.myTargetView.setTrackingLocationEnabled(mediationAdConfig.isTrackingLocationEnabled());
            this.myTargetView.setTrackingEnvironmentEnabled(mediationAdConfig.isTrackingEnvironmentEnabled());
            CustomParams customParams = this.myTargetView.getCustomParams();
            if (customParams != null) {
                customParams.setAge(mediationAdConfig.getAge());
                customParams.setGender(mediationAdConfig.getGender());
                for (Entry entry : mediationAdConfig.getServerParams().entrySet()) {
                    customParams.setCustomParam((String) entry.getKey(), (String) entry.getValue());
                }
            }
            String payload = mediationAdConfig.getPayload();
            if (this.section != null) {
                ah.a("MyTargetStandardAdAdapter: got banner from mediation response");
                this.myTargetView.handleSection(this.section);
            } else if (TextUtils.isEmpty(payload)) {
                StringBuilder sb = new StringBuilder();
                sb.append("MyTargetStandardAdAdapter: load id ");
                sb.append(parseInt);
                ah.a(sb.toString());
                this.myTargetView.load();
            } else {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("MyTargetStandardAdAdapter: load id ");
                sb2.append(parseInt);
                sb2.append(" from BID ");
                sb2.append(payload);
                ah.a(sb2.toString());
                this.myTargetView.loadFromBid(payload);
            }
        } catch (NumberFormatException unused) {
            StringBuilder sb3 = new StringBuilder();
            sb3.append("failed to request ad, unable to convert slotId ");
            sb3.append(placementId);
            sb3.append(" to int");
            String sb4 = sb3.toString();
            StringBuilder sb5 = new StringBuilder();
            sb5.append("MyTargetStandardAdAdapter error: ");
            sb5.append(sb4);
            ah.b(sb5.toString());
            mediationStandardAdListener.onNoAd(sb4, this);
        }
    }

    public void destroy() {
        if (this.myTargetView != null) {
            this.myTargetView.setListener(null);
            this.myTargetView.destroy();
            this.myTargetView = null;
        }
    }
}
