package com.my.target.mediation;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import com.my.target.ah;
import com.my.target.common.CustomParams;
import com.my.target.da;
import com.my.target.mediation.MediationNativeAdAdapter.MediationNativeAdListener;
import com.my.target.nativeads.NativeAd;
import com.my.target.nativeads.NativeAd.NativeAdListener;
import com.my.target.nativeads.banners.NativePromoBanner;
import java.util.List;
import java.util.Map.Entry;

public final class MyTargetNativeAdAdapter implements MediationNativeAdAdapter {
    @Nullable
    private NativeAd ad;
    @Nullable
    private da section;

    class AdListener implements NativeAdListener {
        @NonNull
        private final MediationNativeAdListener mediationListener;

        AdListener(MediationNativeAdListener mediationNativeAdListener) {
            this.mediationListener = mediationNativeAdListener;
        }

        public void onLoad(@NonNull NativePromoBanner nativePromoBanner, @NonNull NativeAd nativeAd) {
            ah.a("MyTargetNativeAdAdapter: ad loaded");
            this.mediationListener.onLoad(nativePromoBanner, MyTargetNativeAdAdapter.this);
        }

        public void onNoAd(@NonNull String str, @NonNull NativeAd nativeAd) {
            StringBuilder sb = new StringBuilder();
            sb.append("MyTargetNativeAdAdapter: no ad (");
            sb.append(str);
            sb.append(")");
            ah.a(sb.toString());
            this.mediationListener.onNoAd(str, MyTargetNativeAdAdapter.this);
        }

        public void onClick(@NonNull NativeAd nativeAd) {
            ah.a("MyTargetNativeAdAdapter: ad clicked");
            this.mediationListener.onClick(MyTargetNativeAdAdapter.this);
        }

        public void onShow(@NonNull NativeAd nativeAd) {
            ah.a("MyTargetNativeAdAdapter: ad shown");
            this.mediationListener.onShow(MyTargetNativeAdAdapter.this);
        }

        public void onVideoPlay(@NonNull NativeAd nativeAd) {
            ah.a("MyTargetNativeAdAdapter: video playing");
            this.mediationListener.onVideoPlay(MyTargetNativeAdAdapter.this);
        }

        public void onVideoPause(@NonNull NativeAd nativeAd) {
            ah.a("MyTargetNativeAdAdapter: video paused");
            this.mediationListener.onVideoPause(MyTargetNativeAdAdapter.this);
        }

        public void onVideoComplete(@NonNull NativeAd nativeAd) {
            ah.a("MyTargetNativeAdAdapter: video completed");
            this.mediationListener.onVideoComplete(MyTargetNativeAdAdapter.this);
        }
    }

    @Nullable
    public View getMediaView(@NonNull Context context) {
        return null;
    }

    public void setSection(@Nullable da daVar) {
        this.section = daVar;
    }

    public void load(@NonNull MediationNativeAdConfig mediationNativeAdConfig, @NonNull MediationNativeAdListener mediationNativeAdListener, @NonNull Context context) {
        String placementId = mediationNativeAdConfig.getPlacementId();
        try {
            int parseInt = Integer.parseInt(placementId);
            this.ad = new NativeAd(parseInt, context);
            this.ad.setMediationEnabled(false);
            this.ad.setListener(new AdListener(mediationNativeAdListener));
            this.ad.setTrackingLocationEnabled(mediationNativeAdConfig.isTrackingLocationEnabled());
            this.ad.setTrackingEnvironmentEnabled(mediationNativeAdConfig.isTrackingEnvironmentEnabled());
            this.ad.setAutoLoadImages(mediationNativeAdConfig.isAutoLoadImages());
            this.ad.setAutoLoadVideo(mediationNativeAdConfig.isAutoLoadVideo());
            CustomParams customParams = this.ad.getCustomParams();
            customParams.setAge(mediationNativeAdConfig.getAge());
            customParams.setGender(mediationNativeAdConfig.getGender());
            for (Entry entry : mediationNativeAdConfig.getServerParams().entrySet()) {
                customParams.setCustomParam((String) entry.getKey(), (String) entry.getValue());
            }
            String payload = mediationNativeAdConfig.getPayload();
            if (this.section != null) {
                ah.a("MyTargetNativeAdAdapter: got banner from mediation response");
                this.ad.handleSection(this.section);
            } else if (TextUtils.isEmpty(payload)) {
                StringBuilder sb = new StringBuilder();
                sb.append("MyTargetNativeAdAdapter: load id ");
                sb.append(parseInt);
                ah.a(sb.toString());
                this.ad.load();
            } else {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("MyTargetNativeAdAdapter: load id ");
                sb2.append(parseInt);
                sb2.append(" from BID ");
                sb2.append(payload);
                ah.a(sb2.toString());
                this.ad.loadFromBid(payload);
            }
        } catch (NumberFormatException unused) {
            StringBuilder sb3 = new StringBuilder();
            sb3.append("failed to request ad, unable to convert slotId ");
            sb3.append(placementId);
            sb3.append(" to int");
            String sb4 = sb3.toString();
            StringBuilder sb5 = new StringBuilder();
            sb5.append("MyTargetNativeAdAdapter error: ");
            sb5.append(sb4);
            ah.b(sb5.toString());
            mediationNativeAdListener.onNoAd(sb4, this);
        }
    }

    public void registerView(@NonNull View view, @Nullable List<View> list, int i) {
        if (this.ad != null) {
            this.ad.setAdChoicesPlacement(i);
            this.ad.registerView(view, list);
        }
    }

    public void unregisterView() {
        if (this.ad != null) {
            this.ad.unregisterView();
        }
    }

    public void destroy() {
        if (this.ad != null) {
            this.ad.unregisterView();
            this.ad.setListener(null);
            this.ad = null;
        }
    }
}
