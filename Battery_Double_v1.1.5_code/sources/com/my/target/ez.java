package com.my.target;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Point;
import android.os.Build;
import android.os.Build.VERSION;
import android.provider.Settings.Secure;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.WorkerThread;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;
import com.facebook.places.model.PlaceFields;
import com.google.android.gms.common.internal.AccountType;
import com.ironsource.sdk.constants.Constants;
import com.tapjoy.TapjoyConstants;
import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.util.Locale;
import java.util.Map.Entry;
import java.util.TimeZone;

/* compiled from: DeviceParamsDataProvider */
public final class ez extends fb {
    private float density = 0.0f;
    private boolean gc = false;
    @NonNull
    private String gd = "";
    @NonNull
    private String ge = "";
    @NonNull
    private String gf = "";
    @NonNull
    private String gg = "";
    @NonNull
    private String gh = "";
    @NonNull
    private String gi = "";
    @NonNull
    private String gj = "";
    @NonNull
    private String gk = "";
    @NonNull
    private String gl = "";
    @NonNull
    private String gm = "";
    @NonNull
    private String gn = "";
    private int go = 0;
    @NonNull
    private String gp = "";
    @NonNull
    private String gq = "";
    @NonNull
    private String gr = "";
    @NonNull
    private String gs = "";
    private int height = 0;
    @NonNull
    private String timezone = "";
    private int width = 0;

    @WorkerThread
    @SuppressLint({"HardwareIds"})
    public synchronized void collectData(@NonNull Context context) {
        if (!this.gc) {
            ah.a("collect application info...");
            ai.b(new Runnable() {
                public void run() {
                    ez.this.addParam("rooted", String.valueOf(ez.this.dh() ? 1 : 0));
                }
            });
            this.gd = Build.DEVICE;
            this.gj = Build.MANUFACTURER;
            this.gk = Build.MODEL;
            this.gf = VERSION.RELEASE;
            this.gg = context.getPackageName();
            this.gl = Locale.getDefault().getLanguage();
            try {
                PackageInfo packageInfo = context.getPackageManager().getPackageInfo(this.gg, 0);
                this.gh = packageInfo.versionName;
                this.gi = Integer.toString(packageInfo.versionCode);
            } catch (NameNotFoundException unused) {
                ah.a("lol");
            }
            ContentResolver contentResolver = context.getContentResolver();
            if (contentResolver != null) {
                this.ge = Secure.getString(contentResolver, TapjoyConstants.TJC_ANDROID_ID);
                if (this.ge == null) {
                    this.ge = "";
                }
            }
            this.gm = context.getResources().getConfiguration().locale.getLanguage();
            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(PlaceFields.PHONE);
            if (telephonyManager != null) {
                this.gq = telephonyManager.getNetworkOperatorName();
                if (telephonyManager.getSimState() == 5) {
                    this.gr = telephonyManager.getSimOperator();
                }
                String networkOperator = telephonyManager.getNetworkOperator();
                if (TextUtils.isEmpty(networkOperator) || networkOperator.length() <= 3) {
                    this.gp = networkOperator;
                } else {
                    this.gp = networkOperator.substring(3);
                    this.gn = networkOperator.substring(0, 3);
                }
            }
            x(context);
            DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
            this.go = displayMetrics.densityDpi;
            this.density = displayMetrics.density;
            TimeZone timeZone = TimeZone.getDefault();
            StringBuilder sb = new StringBuilder();
            sb.append(timeZone.getDisplayName(false, 0));
            sb.append(" ");
            sb.append(timeZone.getID());
            this.timezone = sb.toString();
            String y = y(context);
            if (y != null) {
                this.gs = y;
            }
            addParam(TapjoyConstants.TJC_ANDROID_ID, this.ge);
            addParam("device", this.gd);
            addParam("os", Constants.JAVASCRIPT_INTERFACE_NAME);
            addParam("manufacture", this.gj);
            addParam("osver", this.gf);
            addParam("app", this.gg);
            addParam("appver", this.gh);
            addParam("appbuild", this.gi);
            addParam("lang", this.gl);
            addParam("app_lang", this.gm);
            addParam("sim_loc", this.gn);
            addParam("euname", this.gk);
            StringBuilder sb2 = new StringBuilder();
            sb2.append("");
            sb2.append(this.width);
            addParam("w", sb2.toString());
            StringBuilder sb3 = new StringBuilder();
            sb3.append("");
            sb3.append(this.height);
            addParam("h", sb3.toString());
            StringBuilder sb4 = new StringBuilder();
            sb4.append("");
            sb4.append(this.go);
            addParam("dpi", sb4.toString());
            StringBuilder sb5 = new StringBuilder();
            sb5.append("");
            sb5.append(this.density);
            addParam("density", sb5.toString());
            addParam("operator_id", this.gp);
            addParam("operator_name", this.gq);
            addParam("sim_operator_id", this.gr);
            addParam(TapjoyConstants.TJC_DEVICE_TIMEZONE, this.timezone);
            addParam("mrgs_device_id", this.gs);
            for (Entry entry : getMap().entrySet()) {
                StringBuilder sb6 = new StringBuilder();
                sb6.append((String) entry.getKey());
                sb6.append(" = ");
                sb6.append((String) entry.getValue());
                ah.a(sb6.toString());
            }
            this.gc = true;
            ah.a("collected");
        }
    }

    @NonNull
    public String w(@NonNull Context context) {
        Account[] accountArr;
        try {
            accountArr = AccountManager.get(context).getAccountsByType(AccountType.GOOGLE);
        } catch (Throwable unused) {
            accountArr = null;
        }
        return (accountArr == null || accountArr.length <= 0) ? "" : accountArr[0].name;
    }

    private void x(@NonNull Context context) {
        WindowManager windowManager = (WindowManager) context.getSystemService("window");
        if (windowManager != null) {
            Display defaultDisplay = windowManager.getDefaultDisplay();
            Point point = new Point();
            if (VERSION.SDK_INT >= 17) {
                defaultDisplay.getRealSize(point);
            } else {
                defaultDisplay.getSize(point);
            }
            this.width = point.x;
            this.height = point.y;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x001d  */
    @Nullable
    private String y(@NonNull Context context) {
        hj hjVar;
        String str = null;
        try {
            hjVar = hj.Q(context);
            try {
                str = hjVar.dS();
            } catch (Throwable th) {
                th = th;
                ah.a("PreferencesManager error");
                th.printStackTrace();
                if (TextUtils.isEmpty(str)) {
                }
                return str;
            }
        } catch (Throwable th2) {
            th = th2;
            hjVar = null;
            ah.a("PreferencesManager error");
            th.printStackTrace();
            if (TextUtils.isEmpty(str)) {
            }
            return str;
        }
        if (TextUtils.isEmpty(str)) {
            String str2 = Build.SERIAL;
            String str3 = "";
            if (z(context)) {
                str3 = w(context);
            }
            StringBuilder sb = new StringBuilder();
            sb.append(this.ge);
            sb.append(str2);
            sb.append(str3);
            str = hf.ah(sb.toString());
            if (hjVar != null) {
                hjVar.ai(str);
            }
        }
        return str;
    }

    private boolean z(@NonNull Context context) {
        int i;
        try {
            i = context.checkCallingOrSelfPermission("android.permission.GET_ACCOUNTS");
        } catch (Throwable th) {
            StringBuilder sb = new StringBuilder();
            sb.append("unable to check android.permission.GET_ACCOUNTS permission! Unexpected throwable in Context.checkCallingOrSelfPermission() method: ");
            sb.append(th.getMessage());
            ah.a(sb.toString());
            i = -1;
        }
        return i == 0;
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x00be, code lost:
        if (r8 != null) goto L_0x00c0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x00c4, code lost:
        r0 = th;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:43:0x00bc */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x00b9 A[SYNTHETIC, Splitter:B:41:0x00b9] */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x00c4 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:49:0x00c0] */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x00d0 A[SYNTHETIC, Splitter:B:59:0x00d0] */
    public boolean dh() {
        Process process;
        BufferedReader bufferedReader;
        Throwable th;
        String str = Build.TAGS;
        boolean z = str != null && str.contains("test-keys");
        if (!z) {
            String[] strArr = {"/system/app/Superuser.apk", "/sbin/su", "/system/bin/su", "/system/xbin/su", "/data/local/xbin/su", "/data/local/bin/su", "/system/sd/xbin/su", "/system/bin/failsafe/su", "/data/local/su", "/su/bin/su"};
            int length = strArr.length;
            int i = 0;
            while (true) {
                if (i >= length) {
                    break;
                } else if (new File(strArr[i]).exists()) {
                    z = true;
                    break;
                } else {
                    i++;
                }
            }
        }
        if (z) {
            return z;
        }
        Runtime runtime = Runtime.getRuntime();
        for (String exec : new String[]{"/system/xbin/which su", "/system/bin/which su", "which su"}) {
            try {
                process = runtime.exec(exec);
                try {
                    bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
                    try {
                        StringBuilder sb = new StringBuilder();
                        while (true) {
                            String readLine = bufferedReader.readLine();
                            if (readLine == null) {
                                break;
                            }
                            sb.append(readLine);
                        }
                        process.destroy();
                        if (!TextUtils.isEmpty(sb.toString())) {
                            bufferedReader.close();
                            if (process != null) {
                                try {
                                    process.destroy();
                                } catch (Exception unused) {
                                }
                            }
                            return true;
                        }
                    } catch (Exception unused2) {
                    } catch (Throwable th2) {
                        th = th2;
                        if (bufferedReader != null) {
                        }
                        try {
                            throw th;
                        } catch (Exception unused3) {
                        } catch (Throwable th3) {
                        }
                    }
                } catch (Exception unused4) {
                    bufferedReader = null;
                } catch (Throwable th4) {
                    Throwable th5 = th4;
                    bufferedReader = null;
                    th = th5;
                    if (bufferedReader != null) {
                        bufferedReader.close();
                    }
                    throw th;
                }
                try {
                    bufferedReader.close();
                } catch (Exception unused5) {
                } catch (Throwable th32) {
                }
                if (process == null) {
                }
            } catch (Exception unused6) {
                process = null;
                if (process == null) {
                }
                process.destroy();
            } catch (Throwable th6) {
                th = th6;
                process = null;
                if (process != null) {
                    try {
                        process.destroy();
                    } catch (Exception unused7) {
                    }
                }
                throw th;
            }
            try {
                process.destroy();
            } catch (Exception unused8) {
            }
        }
        return z;
    }
}
