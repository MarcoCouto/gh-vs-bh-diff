package com.my.target;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/* compiled from: InstreamResearchSection */
public class cc extends cu {
    @Nullable
    private cb banner;

    @NonNull
    public static cc bl() {
        return new cc();
    }

    @Nullable
    public cb bm() {
        return this.banner;
    }

    public void a(@Nullable cb cbVar) {
        this.banner = cbVar;
    }

    public int getBannersCount() {
        return this.banner != null ? 1 : 0;
    }
}
