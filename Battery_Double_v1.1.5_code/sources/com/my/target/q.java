package com.my.target;

import android.content.Context;
import android.graphics.Point;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.Display;
import android.view.WindowManager;
import com.my.target.common.models.ImageData;
import com.my.target.common.models.VideoData;
import java.util.ArrayList;
import java.util.List;

/* compiled from: InterstitialAdResultProcessor */
public class q extends d<cx> {
    @NonNull
    public static q i() {
        return new q();
    }

    private q() {
    }

    @Nullable
    public cx a(@NonNull cx cxVar, @NonNull a aVar, @NonNull Context context) {
        ci bE = cxVar.bE();
        if (bE == null) {
            cs bx = cxVar.bx();
            if (bx == null || !bx.bt()) {
                return null;
            }
            return cxVar;
        }
        boolean z = true;
        if (bE instanceof cm) {
            a((cm) bE, context);
        } else if (bE instanceof cl) {
            z = a((cl) bE, context);
        } else if (bE instanceof ck) {
            a((ck) bE, context);
        } else {
            z = false;
        }
        if (z) {
            return cxVar;
        }
        return null;
    }

    private void a(@NonNull cm cmVar, @NonNull Context context) {
        ArrayList arrayList = new ArrayList();
        cn videoBanner = cmVar.getVideoBanner();
        if (videoBanner != null) {
            if (videoBanner.getPreview() != null) {
                arrayList.add(videoBanner.getPreview());
            }
            VideoData videoData = (VideoData) videoBanner.getMediaData();
            if (videoData != null && videoData.isCacheable()) {
                videoData.setData((String) Cdo.cv().f(videoData.getUrl(), context));
            }
        }
        if (cmVar.getImage() != null) {
            arrayList.add(cmVar.getImage());
        }
        if (cmVar.getIcon() != null) {
            arrayList.add(cmVar.getIcon());
        }
        if (cmVar.getCloseIcon() != null) {
            arrayList.add(cmVar.getCloseIcon());
        }
        if (cmVar.getPlayIcon() != null) {
            arrayList.add(cmVar.getPlayIcon());
        }
        if (cmVar.getStoreIcon() != null) {
            arrayList.add(cmVar.getStoreIcon());
        }
        List<cj> interstitialAdCards = cmVar.getInterstitialAdCards();
        if (!interstitialAdCards.isEmpty()) {
            for (cj image : interstitialAdCards) {
                ImageData image2 = image.getImage();
                if (image2 != null) {
                    arrayList.add(image2);
                }
            }
        }
        ci endCard = cmVar.getEndCard();
        if (endCard != null && (endCard instanceof cl) && !a((cl) endCard, context)) {
            cmVar.setEndCard(null);
        }
        if (arrayList.size() > 0) {
            hg.e(arrayList).P(context);
        }
    }

    private boolean a(@NonNull cl clVar, @NonNull Context context) {
        ArrayList arrayList = new ArrayList();
        Point f = f(context);
        int i = f.x;
        int i2 = f.y;
        ImageData a2 = a(clVar.getPortraitImages(), Math.min(i, i2), Math.max(i, i2));
        if (a2 != null) {
            arrayList.add(a2);
            clVar.setOptimalPortraitImage(a2);
        }
        ImageData a3 = a(clVar.getLandscapeImages(), Math.max(i, i2), Math.min(i, i2));
        if (a3 != null) {
            arrayList.add(a3);
            clVar.setOptimalLandscapeImage(a3);
        }
        if (!(a2 == null && a3 == null)) {
            ImageData closeIcon = clVar.getCloseIcon();
            if (closeIcon != null) {
                arrayList.add(closeIcon);
            }
        }
        if (arrayList.size() > 0) {
            hg.e(arrayList).P(context);
            if (a2 != null && a2.getBitmap() != null) {
                return true;
            }
            if (!(a3 == null || a3.getBitmap() == null)) {
                return true;
            }
        }
        return false;
    }

    private void a(@NonNull ck ckVar, @NonNull Context context) {
        if (ckVar.getCloseIcon() != null) {
            hg.a(ckVar.getCloseIcon()).P(context);
        }
    }

    @Nullable
    private ImageData a(@NonNull List<ImageData> list, int i, int i2) {
        float f;
        float f2;
        ImageData imageData = null;
        if (list.size() == 0) {
            return null;
        }
        if (i2 == 0 || i == 0) {
            ah.a("[InterstitialAdResultProcessor] display size is zero");
            return null;
        }
        float f3 = (float) i;
        float f4 = (float) i2;
        float f5 = f3 / f4;
        float f6 = 0.0f;
        for (ImageData imageData2 : list) {
            if (imageData2.getWidth() > 0 && imageData2.getHeight() > 0) {
                float width = ((float) imageData2.getWidth()) / ((float) imageData2.getHeight());
                if (f5 < width) {
                    float width2 = (float) imageData2.getWidth();
                    if (width2 > f3) {
                        width2 = f3;
                    }
                    float f7 = width2;
                    f = width2 / width;
                    f2 = f7;
                } else {
                    f = (float) imageData2.getHeight();
                    if (f > f4) {
                        f = f4;
                    }
                    f2 = width * f;
                }
                float f8 = f2 * f;
                if (f8 <= f6) {
                    break;
                }
                imageData = imageData2;
                f6 = f8;
            }
        }
        return imageData;
    }

    @NonNull
    private Point f(@NonNull Context context) {
        WindowManager windowManager = (WindowManager) context.getSystemService("window");
        Display defaultDisplay = windowManager != null ? windowManager.getDefaultDisplay() : null;
        Point point = new Point();
        if (defaultDisplay != null) {
            if (VERSION.SDK_INT >= 17) {
                defaultDisplay.getRealSize(point);
            } else {
                defaultDisplay.getSize(point);
            }
        }
        return point;
    }
}
