package com.my.target;

import android.content.Context;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.WorkerThread;
import java.util.Map;

/* compiled from: FingerprintDataProvider */
public final class fc extends fb {
    @NonNull
    private static final fc gW = new fc();
    @NonNull
    private final ez gX = new ez();
    @NonNull
    private final fa gY = new fa();
    @NonNull
    private final fd gZ = new fd();
    @NonNull
    private final fe ha = new fe();

    @NonNull
    public static fc di() {
        return gW;
    }

    @NonNull
    public fa dj() {
        return this.gY;
    }

    private fc() {
    }

    @WorkerThread
    public synchronized void collectData(@NonNull Context context) {
        if (Looper.myLooper() == Looper.getMainLooper()) {
            ah.a("FingerprintDataProvider: You must not call collectData method from main thread");
            return;
        }
        removeAll();
        this.gX.collectData(context);
        this.gY.collectData(context);
        this.gZ.collectData(context);
        this.ha.collectData(context);
        Map map = getMap();
        this.gX.putDataTo(map);
        this.gY.putDataTo(map);
        this.gZ.putDataTo(map);
        this.ha.putDataTo(map);
    }
}
