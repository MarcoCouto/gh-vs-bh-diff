package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView.LayoutParams;
import android.support.v7.widget.RecyclerView.State;
import android.view.View;
import android.view.View.MeasureSpec;

/* compiled from: CarouselLayoutManager */
class gt extends LinearLayoutManager {

    /* renamed from: io reason: collision with root package name */
    private final int f3193io;
    private int kM;
    @Nullable
    private a kN;
    private int kO;
    private int kP;
    private int kQ;
    private int kR;

    /* compiled from: CarouselLayoutManager */
    public interface a {
        /* renamed from: do reason: not valid java name */
        void m614do();
    }

    gt(Context context) {
        super(context, 0, false);
        this.f3193io = hm.R(context).E(4);
    }

    public void measureChildWithMargins(View view, int i, int i2) {
        int height = getHeight();
        int width = getWidth();
        if (height != this.kR || width != this.kQ || this.kO <= 0 || this.kP <= 0) {
            view.measure(MeasureSpec.makeMeasureSpec(getWidth(), Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(getHeight(), Integer.MIN_VALUE));
            float width2 = ((float) getWidth()) / ((float) view.getMeasuredWidth());
            if (width2 > 1.0f) {
                double d = (double) ((float) width);
                double floor = Math.floor((double) width2) + 0.5d;
                Double.isNaN(d);
                this.kO = (int) (d / floor);
            } else {
                this.kO = (int) (((float) width) / 1.5f);
            }
            this.kP = height;
            this.kQ = width;
            this.kR = height;
        }
        LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
        if (view != getChildAt(0)) {
            layoutParams.leftMargin = hm.a(this.kM / 2, view.getContext());
        }
        if (view != getChildAt(getChildCount())) {
            layoutParams.rightMargin = hm.a(this.kM / 2, view.getContext());
        }
        view.measure(getChildMeasureSpec(width, getWidthMode(), 0, this.kO, canScrollHorizontally()), getChildMeasureSpec(height, getHeightMode(), this.f3193io, height - (this.f3193io * 2), canScrollVertically()));
    }

    public void a(@Nullable a aVar) {
        this.kN = aVar;
    }

    public void A(int i) {
        this.kM = i;
    }

    public boolean g(@NonNull View view) {
        int findFirstCompletelyVisibleItemPosition = findFirstCompletelyVisibleItemPosition();
        int position = getPosition(view);
        return findFirstCompletelyVisibleItemPosition <= position && position <= findLastCompletelyVisibleItemPosition();
    }

    public void onLayoutCompleted(State state) {
        super.onLayoutCompleted(state);
        if (this.kN != null) {
            this.kN.m614do();
        }
    }
}
