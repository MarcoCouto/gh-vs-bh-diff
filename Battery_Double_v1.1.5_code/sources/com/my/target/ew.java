package com.my.target;

import android.app.Activity;
import android.content.Context;
import android.graphics.Rect;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.View.OnLayoutChangeListener;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.webkit.ConsoleMessage;
import android.webkit.JsResult;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import com.ironsource.sdk.constants.Constants.ParametersKeys;
import com.tapjoy.TJAdUnitConstants.String;
import com.yandex.mobile.ads.video.tracking.Tracker.Events;
import java.lang.ref.WeakReference;
import java.util.ArrayList;

/* compiled from: MraidPresenter */
public class ew implements ex, com.my.target.fj.a {
    @Nullable
    cr bK;
    @NonNull
    final Context context;
    @Nullable
    fw cq;
    @NonNull
    final com.my.target.fr.a fA;
    @Nullable
    bu fB;
    @Nullable
    private fw fC;
    @Nullable
    com.my.target.ex.a fD;
    @Nullable
    c fE;
    boolean fF;
    @Nullable
    private Uri fG;
    @Nullable
    fj fH;
    @Nullable
    ViewGroup fI;
    @Nullable
    private e fJ;
    @Nullable
    f fK;
    @Nullable
    fr fc;
    @NonNull
    final bx fd;
    @NonNull
    private final bu fe;
    @NonNull
    private final WeakReference<Activity> ff;
    @NonNull
    String fg;
    boolean fk;
    @NonNull
    private final fs fx;
    @NonNull
    private final a fy;
    @NonNull
    private final com.my.target.bu.b fz;

    /* compiled from: MraidPresenter */
    class a implements OnLayoutChangeListener {
        @NonNull
        private final bu fe;

        a(bu buVar) {
            this.fe = buVar;
        }

        public void onLayoutChange(View view, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
            ew.this.fK = null;
            ew.this.cU();
            this.fe.a(ew.this.fd);
        }
    }

    /* compiled from: MraidPresenter */
    class b implements com.my.target.fr.a {
        private b() {
        }

        public void onClose() {
            if (ew.this.fH != null) {
                ew.this.fH.dismiss();
            }
        }
    }

    /* compiled from: MraidPresenter */
    public interface c {
        void a(float f, float f2, @NonNull cr crVar, @NonNull Context context);

        void a(@NonNull String str, @NonNull cr crVar, @NonNull Context context);

        void aa();

        void ac();

        void ad();

        void e(@NonNull String str);
    }

    @VisibleForTesting
    /* compiled from: MraidPresenter */
    static class d implements Runnable {
        @NonNull
        private cr bK;
        @NonNull
        private Context context;
        @NonNull
        bu fB;
        @NonNull
        private Uri fG;
        /* access modifiers changed from: private */
        @NonNull
        public fj fH;

        d(@NonNull cr crVar, @NonNull fj fjVar, @NonNull Uri uri, @NonNull bu buVar, @NonNull Context context2) {
            this.bK = crVar;
            this.context = context2.getApplicationContext();
            this.fH = fjVar;
            this.fG = uri;
            this.fB = buVar;
        }

        public void run() {
            dj co = dj.co();
            co.f(this.fG.toString(), this.context);
            final String g = dv.g(this.bK.getMraidJs(), (String) co.ct());
            ai.c(new Runnable() {
                public void run() {
                    if (!TextUtils.isEmpty(g)) {
                        d.this.fB.i(g);
                        return;
                    }
                    d.this.fB.a(Events.CREATIVE_EXPAND, "Failed to handling mraid");
                    d.this.fH.dismiss();
                }
            });
        }
    }

    /* compiled from: MraidPresenter */
    class e implements com.my.target.bu.b {
        @NonNull
        private final bu fO;
        private final String fP;

        public void aJ() {
        }

        e(bu buVar, @NonNull String str) {
            this.fO = buVar;
            this.fP = str;
        }

        public void c(@NonNull bu buVar) {
            StringBuilder sb = new StringBuilder();
            sb.append("onPageLoaded callback from ");
            sb.append(buVar == ew.this.fB ? " second " : " primary ");
            sb.append(ParametersKeys.WEB_VIEW);
            ah.a(sb.toString());
            ArrayList arrayList = new ArrayList();
            if (ew.this.cV()) {
                arrayList.add("'inlineVideo'");
            }
            arrayList.add("'vpaid'");
            buVar.a(arrayList);
            buVar.j(this.fP);
            buVar.k(buVar.isVisible());
            if (ew.this.fH == null || !ew.this.fH.isShowing()) {
                ew.this.aa("default");
            } else {
                ew.this.aa("expanded");
            }
            buVar.aH();
            if (buVar != ew.this.fB && ew.this.fE != null) {
                ew.this.fE.aa();
            }
        }

        public void onVisibilityChanged(boolean z) {
            if (!z || ew.this.fH == null) {
                this.fO.k(z);
            }
        }

        public boolean a(@NonNull String str, @NonNull JsResult jsResult) {
            StringBuilder sb = new StringBuilder();
            sb.append("JS Alert: ");
            sb.append(str);
            ah.a(sb.toString());
            jsResult.confirm();
            return true;
        }

        public boolean a(@NonNull ConsoleMessage consoleMessage, @NonNull bu buVar) {
            StringBuilder sb = new StringBuilder();
            sb.append("Console message: from ");
            sb.append(buVar == ew.this.fB ? " second " : " primary ");
            sb.append("webview: ");
            sb.append(consoleMessage.message());
            ah.a(sb.toString());
            return true;
        }

        public void onClose() {
            if (ew.this.fH != null) {
                ew.this.fH.dismiss();
            }
        }

        public void l(boolean z) {
            ew.this.fF = z;
            if (ew.this.fg.equals("expanded") && ew.this.fc != null) {
                ew.this.fc.setCloseVisible(!z);
                if (!z) {
                    ew.this.fc.setOnCloseListener(ew.this.fA);
                }
            }
        }

        public boolean a(boolean z, bw bwVar) {
            ah.a("Orientation properties isn't supported in standard banners");
            return false;
        }

        public void b(@NonNull Uri uri) {
            if (ew.this.fD != null && ew.this.bK != null) {
                ew.this.fD.a(ew.this.bK, uri.toString());
            }
        }

        public void aK() {
            ew.this.fk = true;
        }

        public boolean o(@NonNull String str) {
            if (!ew.this.fk) {
                this.fO.a("vpaidEvent", "Calling VPAID command before VPAID init");
                return false;
            }
            if (!(ew.this.fE == null || ew.this.bK == null)) {
                ew.this.fE.a(str, ew.this.bK, ew.this.context);
            }
            return true;
        }

        public boolean b(float f, float f2) {
            if (!ew.this.fk) {
                this.fO.a("playheadEvent", "Calling VPAID command before VPAID init");
                return false;
            }
            if (f >= 0.0f && f2 >= 0.0f && ew.this.fE != null && ew.this.bK != null) {
                ew.this.fE.a(f, f2, ew.this.bK, ew.this.context);
            }
            return true;
        }

        public boolean c(@Nullable Uri uri) {
            return ew.this.d(uri);
        }

        public boolean a(int i, int i2, int i3, int i4, boolean z, int i5) {
            int i6 = i;
            int i7 = i2;
            boolean z2 = z;
            ew.this.fK = new f();
            if (ew.this.fI == null) {
                ah.a("Unable to set resize properties: container view for resize is not defined");
                this.fO.a("setResizeProperties", "container view for resize is not defined");
                ew.this.fK = null;
                return false;
            } else if (i6 < 50 || i7 < 50) {
                ah.a("Unable to set resize properties: properties cannot be less than closeable container");
                this.fO.a("setResizeProperties", "properties cannot be less than closeable container");
                ew.this.fK = null;
                return false;
            } else {
                hm R = hm.R(ew.this.context);
                ew.this.fK.t(z2);
                ew.this.fK.a(R.E(i), R.E(i7), R.E(i3), R.E(i4), i5);
                if (!z2) {
                    Rect rect = new Rect();
                    ew.this.fI.getGlobalVisibleRect(rect);
                    if (!ew.this.fK.c(rect)) {
                        StringBuilder sb = new StringBuilder();
                        sb.append("Unable to set resize properties: allowOffscreen is false, maxSize is (");
                        sb.append(rect.width());
                        sb.append(",");
                        sb.append(rect.height());
                        sb.append(") resize properties: (");
                        sb.append(ew.this.fK.dd());
                        sb.append(",");
                        sb.append(ew.this.fK.de());
                        sb.append(")");
                        ah.a(sb.toString());
                        this.fO.a("setResizeProperties", "resize properties with allowOffscreen false out of viewport");
                        ew.this.fK = null;
                        return false;
                    }
                }
                return true;
            }
        }

        public boolean aL() {
            if (!ew.this.fg.equals("default")) {
                StringBuilder sb = new StringBuilder();
                sb.append("Unable to resize: wrong state for resize: ");
                sb.append(ew.this.fg);
                ah.a(sb.toString());
                StringBuilder sb2 = new StringBuilder();
                sb2.append("wrong state for resize ");
                sb2.append(ew.this.fg);
                this.fO.a("resize", sb2.toString());
                return false;
            } else if (ew.this.fK == null) {
                ah.a("Unable to resize: resize properties not set");
                this.fO.a("resize", "resize properties not set");
                return false;
            } else if (ew.this.fI == null || ew.this.cq == null) {
                ah.a("Unable to resize: views not initialized");
                this.fO.a("resize", "views not initialized");
                return false;
            } else if (!ew.this.fK.a(ew.this.fI, ew.this.cq)) {
                ah.a("Unable to resize: views not visible");
                this.fO.a("resize", "views not visible");
                return false;
            } else {
                ew.this.fc = new fr(ew.this.context);
                ew.this.fK.a(ew.this.fc);
                if (!ew.this.fK.b(ew.this.fc)) {
                    ah.a("Unable to resize: close button is out of visible range");
                    this.fO.a("resize", "close button is out of visible range");
                    ew.this.fc = null;
                    return false;
                }
                ViewGroup viewGroup = (ViewGroup) ew.this.cq.getParent();
                if (viewGroup != null) {
                    viewGroup.removeView(ew.this.cq);
                }
                ew.this.fc.addView(ew.this.cq, new LayoutParams(-1, -1));
                ew.this.fc.setOnCloseListener(new com.my.target.fr.a() {
                    public void onClose() {
                        e.this.dc();
                    }
                });
                ew.this.fI.addView(ew.this.fc);
                ew.this.aa("resized");
                if (ew.this.fE != null) {
                    ew.this.fE.ac();
                }
                return true;
            }
        }

        /* access modifiers changed from: 0000 */
        public void dc() {
            if (ew.this.fc != null && ew.this.cq != null) {
                if (ew.this.fc.getParent() != null) {
                    ((ViewGroup) ew.this.fc.getParent()).removeView(ew.this.fc);
                    ew.this.fc.removeAllViews();
                    ew.this.b(ew.this.cq);
                    ew.this.aa("default");
                    ew.this.fc.setOnCloseListener(null);
                    ew.this.fc = null;
                }
                if (ew.this.fE != null) {
                    ew.this.fE.ad();
                }
            }
        }
    }

    /* compiled from: MraidPresenter */
    public static class f {
        private boolean fR = true;
        private int fS;
        private int fT;
        private int fU;
        private int fV;
        private int fW;
        @Nullable
        private Rect fX;
        @Nullable
        private Rect fY;
        private int fZ;
        private int ga;

        public int dd() {
            return this.fU;
        }

        /* access modifiers changed from: 0000 */
        public void t(boolean z) {
            this.fR = z;
        }

        /* access modifiers changed from: 0000 */
        public void a(int i, int i2, int i3, int i4, int i5) {
            this.fU = i;
            this.fV = i2;
            this.fS = i3;
            this.fT = i4;
            this.fW = i5;
        }

        /* access modifiers changed from: 0000 */
        public boolean c(@NonNull Rect rect) {
            return this.fU <= rect.width() && this.fV <= rect.height();
        }

        /* access modifiers changed from: 0000 */
        public void a(@NonNull fr frVar) {
            if (this.fY == null || this.fX == null) {
                ah.a("Setup views before resizing");
                return;
            }
            this.fZ = (this.fY.top - this.fX.top) + this.fT;
            this.ga = (this.fY.left - this.fX.left) + this.fS;
            if (!this.fR) {
                if (this.fZ + this.fV > this.fX.height()) {
                    ah.a("Try to reposition creative vertically because of resize allowOffscreen:false and out of max size properties");
                    this.fZ = this.fX.height() - this.fV;
                }
                if (this.ga + this.fU > this.fX.width()) {
                    ah.a("Try to reposition creative horizontally because of resize allowOffscreen:false and out of max size properties");
                    this.ga = this.fX.width() - this.fU;
                }
            }
            LayoutParams layoutParams = new LayoutParams(this.fU, this.fV);
            layoutParams.topMargin = this.fZ;
            layoutParams.leftMargin = this.ga;
            frVar.setLayoutParams(layoutParams);
            frVar.setCloseGravity(this.fW);
            frVar.setCloseVisible(false);
        }

        public int de() {
            return this.fV;
        }

        /* access modifiers changed from: 0000 */
        public boolean a(@NonNull ViewGroup viewGroup, @NonNull fw fwVar) {
            this.fX = new Rect();
            this.fY = new Rect();
            return viewGroup.getGlobalVisibleRect(this.fX) && fwVar.getGlobalVisibleRect(this.fY);
        }

        /* access modifiers changed from: 0000 */
        public boolean b(@NonNull fr frVar) {
            if (this.fX == null) {
                return false;
            }
            Rect rect = new Rect(this.ga, this.fZ, this.fX.right, this.fX.bottom);
            Rect rect2 = new Rect(this.ga, this.fZ, this.ga + this.fU, this.fZ + this.fV);
            Rect rect3 = new Rect();
            frVar.a(this.fW, rect2, rect3);
            return rect.contains(rect3);
        }
    }

    @NonNull
    public static ew f(@NonNull ViewGroup viewGroup) {
        return new ew(viewGroup);
    }

    @VisibleForTesting
    ew(@NonNull bu buVar, @NonNull fw fwVar, @NonNull fs fsVar, @NonNull ViewGroup viewGroup) {
        this.fA = new b();
        this.fe = buVar;
        this.cq = fwVar;
        this.fx = fsVar;
        this.context = viewGroup.getContext();
        if (this.context instanceof Activity) {
            this.ff = new WeakReference<>((Activity) this.context);
            this.fI = (ViewGroup) ((Activity) this.context).getWindow().getDecorView().findViewById(16908290);
        } else {
            this.ff = new WeakReference<>(null);
            View rootView = viewGroup.getRootView();
            if (rootView != null) {
                this.fI = (ViewGroup) rootView.findViewById(16908290);
                if (this.fI == null) {
                    this.fI = (ViewGroup) rootView;
                }
            }
        }
        this.fg = "loading";
        this.fd = bx.p(this.context);
        b(fwVar);
        this.fz = new e(buVar, String.INLINE);
        buVar.a(this.fz);
        this.fy = new a(buVar);
        fwVar.addOnLayoutChangeListener(this.fy);
    }

    private ew(@NonNull ViewGroup viewGroup) {
        this(bu.h(String.INLINE), new fw(viewGroup.getContext()), new fs(viewGroup.getContext()), viewGroup);
    }

    public void a(@NonNull fj fjVar, @NonNull FrameLayout frameLayout) {
        this.fH = fjVar;
        this.fc = new fr(this.context);
        a(this.fc, frameLayout);
    }

    public void a(boolean z) {
        if (this.fB != null) {
            this.fB.k(z);
        } else {
            this.fe.k(z);
        }
        if (this.fC == null) {
            return;
        }
        if (z) {
            this.fC.onResume();
        } else {
            this.fC.w(false);
        }
    }

    public void C() {
        this.fx.setVisibility(0);
        if (this.fG != null) {
            this.fG = null;
            if (this.fB != null) {
                this.fB.k(false);
                this.fB.k("hidden");
                this.fB.detach();
                this.fB = null;
                this.fe.k(true);
            }
            if (this.fC != null) {
                this.fC.w(true);
                if (this.fC.getParent() != null) {
                    ((ViewGroup) this.fC.getParent()).removeView(this.fC);
                }
                this.fC.destroy();
                this.fC = null;
            }
        } else if (this.cq != null) {
            if (this.cq.getParent() != null) {
                ((ViewGroup) this.cq.getParent()).removeView(this.cq);
            }
            b(this.cq);
        }
        if (!(this.fc == null || this.fc.getParent() == null)) {
            ((ViewGroup) this.fc.getParent()).removeView(this.fc);
        }
        this.fc = null;
        aa("default");
        if (this.fE != null) {
            this.fE.ad();
        }
        cU();
        this.fe.a(this.fd);
        this.cq.onResume();
    }

    public void a(@NonNull cr crVar) {
        this.bK = crVar;
        String mraidSource = crVar.getMraidSource();
        if (mraidSource == null || this.cq == null) {
            Y("failed to load, failed MRAID initialization");
            return;
        }
        this.fe.a(this.cq);
        this.fe.i(mraidSource);
    }

    public void start() {
        if (this.fD != null && this.bK != null) {
            this.fD.a(this.bK);
        }
    }

    public void stop() {
        if ((this.fH == null || this.fB != null) && this.cq != null) {
            this.cq.w(true);
        }
    }

    public void pause() {
        if ((this.fH == null || this.fB != null) && this.cq != null) {
            this.cq.w(false);
        }
    }

    public void resume() {
        if ((this.fH == null || this.fB != null) && this.cq != null) {
            this.cq.onResume();
        }
    }

    public void destroy() {
        aa("hidden");
        a((c) null);
        a((com.my.target.ex.a) null);
        this.fe.detach();
        if (this.fc != null) {
            this.fc.removeAllViews();
            this.fc.setOnCloseListener(null);
            ViewParent parent = this.fc.getParent();
            if (parent != null) {
                ((ViewGroup) parent).removeView(this.fc);
            }
            this.fc = null;
        }
        if (this.cq != null) {
            this.cq.w(true);
            if (this.cq.getParent() != null) {
                ((ViewGroup) this.cq.getParent()).removeView(this.cq);
            }
            this.cq.destroy();
            this.cq = null;
        }
        if (this.fB != null) {
            this.fB.detach();
            this.fB = null;
        }
        if (this.fC != null) {
            this.fC.w(true);
            if (this.fC.getParent() != null) {
                ((ViewGroup) this.fC.getParent()).removeView(this.fC);
            }
            this.fC.destroy();
            this.fC = null;
        }
    }

    @NonNull
    public fs db() {
        return this.fx;
    }

    public void a(@Nullable com.my.target.ex.a aVar) {
        this.fD = aVar;
    }

    public void a(@Nullable c cVar) {
        this.fE = cVar;
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public void a(@NonNull fr frVar, @NonNull FrameLayout frameLayout) {
        this.fx.setVisibility(8);
        frameLayout.addView(frVar, new ViewGroup.LayoutParams(-1, -1));
        if (this.fG != null) {
            this.fB = bu.h(String.INLINE);
            this.fC = new fw(this.context);
            a(this.fB, this.fC, frVar);
        } else if (!(this.cq == null || this.cq.getParent() == null)) {
            ((ViewGroup) this.cq.getParent()).removeView(this.cq);
            frVar.addView(this.cq, new ViewGroup.LayoutParams(-1, -1));
            aa("expanded");
        }
        frVar.setCloseVisible(!this.fF);
        frVar.setOnCloseListener(this.fA);
        if (this.fE != null && this.fG == null) {
            this.fE.ac();
        }
        ah.a("MRAIDMRAID dialog create");
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public void a(@NonNull bu buVar, @NonNull fw fwVar, @NonNull fr frVar) {
        this.fJ = new e(buVar, String.INLINE);
        buVar.a((com.my.target.bu.b) this.fJ);
        frVar.addView(fwVar, new ViewGroup.LayoutParams(-1, -1));
        buVar.a(fwVar);
        if (this.fH == null) {
            return;
        }
        if (this.bK == null || this.fG == null) {
            this.fH.dismiss();
            return;
        }
        d dVar = new d(this.bK, this.fH, this.fG, buVar, this.context);
        ai.a(dVar);
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public void cU() {
        int[] iArr = new int[2];
        DisplayMetrics displayMetrics = this.context.getResources().getDisplayMetrics();
        this.fd.a(displayMetrics.widthPixels, displayMetrics.heightPixels);
        if (this.fI != null) {
            this.fI.getLocationOnScreen(iArr);
            this.fd.c(iArr[0], iArr[1], iArr[0] + this.fI.getMeasuredWidth(), iArr[1] + this.fI.getMeasuredHeight());
        }
        if (!this.fg.equals("expanded") && !this.fg.equals("resized")) {
            this.fx.getLocationOnScreen(iArr);
            this.fd.a(iArr[0], iArr[1], iArr[0] + this.fx.getMeasuredWidth(), iArr[1] + this.fx.getMeasuredHeight());
        }
        if (this.fC != null) {
            this.fC.getLocationOnScreen(iArr);
            this.fd.b(iArr[0], iArr[1], iArr[0] + this.fC.getMeasuredWidth(), iArr[1] + this.fC.getMeasuredHeight());
        } else if (this.cq != null) {
            this.cq.getLocationOnScreen(iArr);
            this.fd.b(iArr[0], iArr[1], iArr[0] + this.cq.getMeasuredWidth(), iArr[1] + this.cq.getMeasuredHeight());
        }
    }

    /* access modifiers changed from: 0000 */
    public void b(@NonNull fw fwVar) {
        LayoutParams layoutParams = new LayoutParams(-1, -1);
        layoutParams.gravity = 1;
        this.fx.addView(fwVar);
        fwVar.setLayoutParams(layoutParams);
    }

    /* access modifiers changed from: 0000 */
    public boolean cV() {
        Activity activity = (Activity) this.ff.get();
        if (activity == null || this.cq == null) {
            return false;
        }
        return hm.a(activity, (View) this.cq);
    }

    /* access modifiers changed from: 0000 */
    public void aa(@NonNull String str) {
        StringBuilder sb = new StringBuilder();
        sb.append("MRAID state set to ");
        sb.append(str);
        ah.a(sb.toString());
        this.fg = str;
        this.fe.k(str);
        if (this.fB != null) {
            this.fB.k(str);
        }
        if ("hidden".equals(str)) {
            ah.a("MraidPresenter: Mraid on close");
        }
    }

    /* access modifiers changed from: 0000 */
    public boolean d(@Nullable Uri uri) {
        if (this.cq == null) {
            ah.a("Cannot expand: webview destroyed");
            return false;
        } else if (!this.fg.equals("default") && !this.fg.equals("resized")) {
            return false;
        } else {
            this.fG = uri;
            fj.a(this, this.context).show();
            return true;
        }
    }

    private void Y(@NonNull String str) {
        if (this.fE != null) {
            this.fE.e(str);
        }
    }
}
