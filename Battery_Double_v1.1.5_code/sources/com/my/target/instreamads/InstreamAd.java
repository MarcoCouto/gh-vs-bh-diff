package com.my.target.instreamads;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.my.target.ah;
import com.my.target.aq;
import com.my.target.b.C0072b;
import com.my.target.cn;
import com.my.target.common.BaseAd;
import com.my.target.cv;
import com.my.target.cz;
import com.my.target.f;
import com.my.target.f.b;
import com.my.target.fk;
import com.my.target.hh;
import com.yandex.mobile.ads.video.models.vmap.AdBreak.BreakId;
import java.util.concurrent.atomic.AtomicBoolean;

public final class InstreamAd extends BaseAd {
    private static final int DEFAULT_LOADING_TIMEOUT_SECONDS = 10;
    private static final int MIN_LOADING_TIMEOUT_SECONDS = 5;
    @NonNull
    private final Context context;
    @Nullable
    private aq engine;
    private boolean isFullscreen;
    @Nullable
    private InstreamAdListener listener;
    @NonNull
    private final AtomicBoolean loadOnce = new AtomicBoolean();
    private int loadingTimeoutSeconds = 10;
    @Nullable
    private float[] midpoints;
    @Nullable
    private InstreamAdPlayer player;
    @Nullable
    private cv section;
    @Nullable
    private float[] userMidpoints;
    private float videoDuration;
    private float volume = 1.0f;

    public static final class InstreamAdBanner {
        public final boolean allowClose;
        public final float allowCloseDelay;
        public final boolean allowPause;
        @Nullable
        public final String ctaText;
        public final float duration;
        public final int videoHeight;
        public final int videoWidth;

        @NonNull
        public static InstreamAdBanner newBanner(@NonNull cn cnVar) {
            InstreamAdBanner instreamAdBanner = new InstreamAdBanner(cnVar.isAllowClose(), cnVar.getAllowCloseDelay(), cnVar.getDuration(), cnVar.getWidth(), cnVar.getHeight(), cnVar.getCtaText(), cnVar.isAllowPause());
            return instreamAdBanner;
        }

        private InstreamAdBanner(boolean z, float f, float f2, int i, int i2, @Nullable String str, boolean z2) {
            this.allowClose = z;
            this.allowCloseDelay = f;
            this.duration = f2;
            this.videoHeight = i2;
            this.videoWidth = i;
            this.ctaText = str;
            this.allowPause = z2;
        }
    }

    public interface InstreamAdListener {
        void onBannerComplete(@NonNull InstreamAd instreamAd, @NonNull InstreamAdBanner instreamAdBanner);

        void onBannerPause(@NonNull InstreamAd instreamAd, @NonNull InstreamAdBanner instreamAdBanner);

        void onBannerResume(@NonNull InstreamAd instreamAd, @NonNull InstreamAdBanner instreamAdBanner);

        void onBannerStart(@NonNull InstreamAd instreamAd, @NonNull InstreamAdBanner instreamAdBanner);

        void onBannerTimeLeftChange(float f, float f2, @NonNull InstreamAd instreamAd);

        void onComplete(@NonNull String str, @NonNull InstreamAd instreamAd);

        void onError(@NonNull String str, @NonNull InstreamAd instreamAd);

        void onLoad(@NonNull InstreamAd instreamAd);

        void onNoAd(@NonNull String str, @NonNull InstreamAd instreamAd);
    }

    public InstreamAd(int i, @NonNull Context context2) {
        super(i, "instreamads");
        this.context = context2;
        setTrackingEnvironmentEnabled(false);
        ah.c("InstreamAd created. Version: 5.4.7");
    }

    @Nullable
    public InstreamAdListener getListener() {
        return this.listener;
    }

    public void setListener(@Nullable InstreamAdListener instreamAdListener) {
        this.listener = instreamAdListener;
    }

    public int getLoadingTimeout() {
        return this.loadingTimeoutSeconds;
    }

    public void setLoadingTimeout(int i) {
        if (i < 5) {
            ah.a("unable to set ad loading timeout < 5, set to 5 seconds");
            this.loadingTimeoutSeconds = 5;
        } else {
            StringBuilder sb = new StringBuilder();
            sb.append("ad loading timeout set to ");
            sb.append(i);
            sb.append(" seconds");
            ah.a(sb.toString());
            this.loadingTimeoutSeconds = i;
        }
        if (this.engine != null) {
            this.engine.e(this.loadingTimeoutSeconds);
        }
    }

    public int getVideoQuality() {
        return this.adConfig.getVideoQuality();
    }

    public void setVideoQuality(int i) {
        this.adConfig.setVideoQuality(i);
    }

    @Nullable
    public InstreamAdPlayer getPlayer() {
        return this.player;
    }

    public void setPlayer(@Nullable InstreamAdPlayer instreamAdPlayer) {
        this.player = instreamAdPlayer;
        if (this.engine != null) {
            this.engine.setPlayer(instreamAdPlayer);
        }
    }

    public void swapPlayer(@Nullable InstreamAdPlayer instreamAdPlayer) {
        this.player = instreamAdPlayer;
        if (this.engine != null) {
            this.engine.swapPlayer(instreamAdPlayer);
        }
    }

    public boolean isFullscreen() {
        return this.isFullscreen;
    }

    public void setFullscreen(boolean z) {
        this.isFullscreen = z;
        if (this.engine != null) {
            this.engine.setFullscreen(z);
        }
    }

    public float getVolume() {
        if (this.engine != null) {
            return this.engine.getVolume();
        }
        return this.volume;
    }

    public void setVolume(float f) {
        if (Float.compare(f, 0.0f) < 0 || Float.compare(f, 1.0f) > 0) {
            StringBuilder sb = new StringBuilder();
            sb.append("unable to set volume");
            sb.append(f);
            sb.append(", volume must be in range [0..1]");
            ah.a(sb.toString());
            return;
        }
        this.volume = f;
        if (this.engine != null) {
            this.engine.setVolume(f);
        }
    }

    @NonNull
    public float[] getMidPoints() {
        if (this.midpoints == null) {
            return new float[0];
        }
        return (float[]) this.midpoints.clone();
    }

    public void load() {
        if (!this.loadOnce.compareAndSet(false, true)) {
            StringBuilder sb = new StringBuilder();
            sb.append(this);
            sb.append(" instance just loaded once, don't call load() more than one time per instance");
            ah.b(sb.toString());
            return;
        }
        f.a(this.adConfig, this.loadingTimeoutSeconds).a((C0072b<T>) new b() {
            public void onResult(@Nullable cv cvVar, @Nullable String str) {
                InstreamAd.this.handleResult(cvVar, str);
            }
        }).a(this.context);
    }

    public void configureMidpoints(float f) {
        configureMidpoints(f, null);
    }

    public void configureMidpoints(float f, @Nullable float[] fArr) {
        if (f <= 0.0f) {
            ah.a("midpoints are not configured, duration is not set or <= zero");
        } else if (this.midpoints != null) {
            ah.a("midpoints already configured");
        } else {
            this.userMidpoints = fArr;
            this.videoDuration = f;
            if (this.section != null) {
                cz x = this.section.x(BreakId.MIDROLL);
                if (x != null) {
                    this.midpoints = hh.a(x, this.userMidpoints, f);
                    if (this.engine != null) {
                        this.engine.a(this.midpoints);
                    }
                }
            }
        }
    }

    public void configureMidpointsPercents(float f, @Nullable float[] fArr) {
        if (fArr == null) {
            configureMidpoints(f);
        } else {
            configureMidpoints(f, hh.a(f, fArr));
        }
    }

    public void useDefaultPlayer() {
        setPlayer(new fk(this.context));
    }

    public void pause() {
        if (this.engine != null) {
            this.engine.pause();
        }
    }

    public void resume() {
        if (this.engine != null) {
            this.engine.resume();
        }
    }

    public void stop() {
        if (this.engine != null) {
            this.engine.stop();
        }
    }

    public void skip() {
        if (this.engine != null) {
            this.engine.skip();
        }
    }

    public void skipBanner() {
        if (this.engine != null) {
            this.engine.skipBanner();
        }
    }

    public void handleClick() {
        if (this.engine != null) {
            this.engine.handleClick();
        }
    }

    public void destroy() {
        this.listener = null;
        if (this.engine != null) {
            this.engine.destroy();
        }
    }

    public void startPreroll() {
        start(BreakId.PREROLL);
    }

    public void startPostroll() {
        start("postroll");
    }

    public void startPauseroll() {
        start(BreakId.PAUSEROLL);
    }

    public void startMidroll(float f) {
        if (this.engine == null) {
            ah.a("Unable to start ad: not loaded yet");
        } else if (this.engine.getPlayer() == null) {
            ah.a("Unable to start ad: player has not set");
        } else {
            this.engine.startMidroll(f);
        }
    }

    /* access modifiers changed from: private */
    public void handleResult(@Nullable cv cvVar, @Nullable String str) {
        if (this.listener == null) {
            return;
        }
        if (cvVar == null || !cvVar.bA()) {
            InstreamAdListener instreamAdListener = this.listener;
            if (str == null) {
                str = "no ad";
            }
            instreamAdListener.onNoAd(str, this);
            return;
        }
        this.section = cvVar;
        this.engine = aq.a(this, this.section, this.adConfig);
        this.engine.e(this.loadingTimeoutSeconds);
        this.engine.setVolume(this.volume);
        if (this.player != null) {
            this.engine.setPlayer(this.player);
        }
        configureMidpoints(this.videoDuration, this.userMidpoints);
        this.listener.onLoad(this);
    }

    private void start(@NonNull String str) {
        if (this.engine == null) {
            ah.a("Unable to start ad: not loaded yet");
        } else if (this.engine.getPlayer() == null) {
            ah.a("Unable to start ad: player has not set");
        } else {
            this.engine.start(str);
        }
    }
}
