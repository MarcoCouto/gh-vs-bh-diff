package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Pair;
import com.ironsource.sdk.precache.DownloadManager;
import org.json.JSONArray;
import org.json.JSONObject;

/* compiled from: NativeAppwallAdSectionParser */
public class eh {
    public static eh h(@NonNull bz bzVar, @NonNull a aVar, @NonNull Context context) {
        return new eh(bzVar, aVar, context);
    }

    private eh(@NonNull bz bzVar, @NonNull a aVar, @NonNull Context context) {
    }

    public void a(@NonNull JSONObject jSONObject, @NonNull db dbVar) {
        JSONObject optJSONObject = jSONObject.optJSONObject(DownloadManager.SETTINGS);
        if (optJSONObject != null) {
            b(optJSONObject, dbVar);
        }
    }

    private void b(@NonNull JSONObject jSONObject, @NonNull db dbVar) {
        dbVar.setTitle(jSONObject.optString("title", dbVar.getTitle()));
        dbVar.D(jSONObject.optString("icon_hd", dbVar.bU()));
        dbVar.E(jSONObject.optString("bubble_icon_hd", dbVar.bV()));
        dbVar.F(jSONObject.optString("label_icon_hd", dbVar.bW()));
        dbVar.G(jSONObject.optString("goto_app_icon_hd", dbVar.bX()));
        dbVar.H(jSONObject.optString("item_highlight_icon", dbVar.bY()));
        JSONArray optJSONArray = jSONObject.optJSONArray("icon_status");
        if (optJSONArray != null) {
            int length = optJSONArray.length();
            for (int i = 0; i < length; i++) {
                JSONObject optJSONObject = optJSONArray.optJSONObject(i);
                if (optJSONObject != null) {
                    dbVar.bT().add(new Pair(optJSONObject.optString("value"), optJSONObject.optString("icon_hd")));
                }
            }
        }
    }
}
