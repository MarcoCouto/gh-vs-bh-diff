package com.my.target;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.helper.ItemTouchHelper.Callback;
import com.my.target.common.models.VideoData;
import com.my.target.instreamads.InstreamAdPlayer;
import com.my.target.instreamads.InstreamAdPlayer.AdPlayerListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/* compiled from: InstreamAdVideoController */
public class ak {
    /* access modifiers changed from: private */
    @NonNull
    public final hk A = hk.C(Callback.DEFAULT_DRAG_ANIMATION_DURATION);
    @NonNull
    private final Stack<df> C = new Stack<>();
    /* access modifiers changed from: private */
    @Nullable
    public cn<VideoData> E;
    private int F;
    private float G;
    private int H;
    /* access modifiers changed from: private */
    public boolean I;
    /* access modifiers changed from: private */
    public int J = 0;
    @NonNull
    private final a L = new a();
    /* access modifiers changed from: private */
    @NonNull
    public final b M = new b();
    @Nullable
    private ho N;
    /* access modifiers changed from: private */
    @Nullable
    public c O;
    /* access modifiers changed from: private */
    @Nullable
    public InstreamAdPlayer player;
    /* access modifiers changed from: private */
    public float volume = 1.0f;

    /* compiled from: InstreamAdVideoController */
    class a implements AdPlayerListener {
        private float volume;

        private a() {
            this.volume = 1.0f;
        }

        public void onAdVideoStarted() {
            ak.this.J = 1;
            if (!ak.this.I && ak.this.player != null) {
                ak.this.b(ak.this.player.getAdVideoDuration());
            }
            ak.this.A.d(ak.this.M);
        }

        public void onAdVideoPaused() {
            Context context = ak.this.getContext();
            if (!(ak.this.E == null || context == null)) {
                hl.a((List<dg>) ak.this.E.getStatHolder().N("playbackPaused"), context);
            }
            ak.this.A.e(ak.this.M);
            if (ak.this.E != null && ak.this.O != null) {
                ak.this.O.e(ak.this.E);
            }
        }

        public void onAdVideoResumed() {
            Context context = ak.this.getContext();
            if (!(ak.this.E == null || context == null)) {
                hl.a((List<dg>) ak.this.E.getStatHolder().N("playbackResumed"), context);
            }
            ak.this.A.d(ak.this.M);
            if (ak.this.E != null && ak.this.O != null) {
                ak.this.O.f(ak.this.E);
            }
        }

        public void onAdVideoStopped() {
            if (ak.this.J == 1) {
                if (!(ak.this.E == null || ak.this.O == null)) {
                    Context context = ak.this.getContext();
                    if (context != null) {
                        hl.a((List<dg>) ak.this.E.getStatHolder().N("playbackStopped"), context);
                    }
                    ak.this.O.c(ak.this.E);
                }
                ak.this.J = 0;
            }
            ak.this.A.e(ak.this.M);
        }

        public void onAdVideoError(@NonNull String str) {
            if (!(ak.this.E == null || ak.this.O == null)) {
                ak.this.O.a(str, ak.this.E);
            }
            ak.this.A.e(ak.this.M);
        }

        public void onAdVideoCompleted() {
            if (ak.this.J != 2) {
                if (!(ak.this.E == null || ak.this.O == null)) {
                    ak.this.q();
                    if (ak.this.E != null) {
                        cn e = ak.this.E;
                        ak.this.u();
                        if (e != null) {
                            ak.this.c(e.getDuration());
                            ak.this.O.d(e);
                        }
                    }
                }
                ak.this.J = 2;
            }
            ak.this.A.e(ak.this.M);
        }

        public void onVolumeChanged(float f) {
            if (f != this.volume) {
                if (this.volume > 0.0f && f <= 0.0f) {
                    Context context = ak.this.getContext();
                    if (!(context == null || ak.this.E == null)) {
                        hl.a((List<dg>) ak.this.E.getStatHolder().N("volumeOff"), context);
                        this.volume = f;
                        ak.this.volume = f;
                    }
                } else if (this.volume == 0.0f && f > 0.0f) {
                    Context context2 = ak.this.getContext();
                    if (!(context2 == null || ak.this.E == null)) {
                        hl.a((List<dg>) ak.this.E.getStatHolder().N("volumeOn"), context2);
                        this.volume = f;
                        ak.this.volume = f;
                    }
                }
            }
        }
    }

    /* compiled from: InstreamAdVideoController */
    class b implements Runnable {
        private b() {
        }

        public void run() {
            ak.this.q();
        }
    }

    /* compiled from: InstreamAdVideoController */
    public interface c {
        void a(float f, float f2, @NonNull cn cnVar);

        void a(@NonNull String str, @NonNull cn cnVar);

        void b(@NonNull cn cnVar);

        void c(@NonNull cn cnVar);

        void d(@NonNull cn cnVar);

        void e(@NonNull cn cnVar);

        void f(@NonNull cn cnVar);
    }

    @NonNull
    public static ak t() {
        return new ak();
    }

    private ak() {
    }

    public void setVolume(float f) {
        if (this.player != null) {
            this.player.setVolume(f);
        }
        this.volume = f;
    }

    @Nullable
    public InstreamAdPlayer getPlayer() {
        return this.player;
    }

    public void setPlayer(@Nullable InstreamAdPlayer instreamAdPlayer) {
        if (this.player != null) {
            this.player.setAdPlayerListener(null);
        }
        this.player = instreamAdPlayer;
        if (instreamAdPlayer != null) {
            if (this.N != null) {
                this.N.setView(instreamAdPlayer.getView());
            }
            instreamAdPlayer.setAdPlayerListener(this.L);
        } else if (this.N != null) {
            this.N.setView(null);
        }
    }

    public void swapPlayer(@Nullable InstreamAdPlayer instreamAdPlayer) {
        if (this.player != null) {
            this.player.setAdPlayerListener(null);
            this.player.stopAdVideo();
        }
        this.player = instreamAdPlayer;
        if (instreamAdPlayer != null) {
            if (this.N != null) {
                this.N.setView(instreamAdPlayer.getView());
            }
            instreamAdPlayer.setAdPlayerListener(this.L);
        } else if (this.N != null) {
            this.N.setView(null);
        }
        if (this.E != null) {
            VideoData videoData = (VideoData) this.E.getMediaData();
            if (videoData != null) {
                Uri parse = Uri.parse(videoData.getUrl());
                if (instreamAdPlayer != null) {
                    instreamAdPlayer.setVolume(this.volume);
                    instreamAdPlayer.playAdVideo(parse, videoData.getWidth(), videoData.getHeight(), this.G);
                }
            }
        }
    }

    public void a(@Nullable c cVar) {
        this.O = cVar;
    }

    @Nullable
    public Context getContext() {
        if (this.player == null) {
            return null;
        }
        return this.player.getView().getContext();
    }

    public void a(@NonNull cn<VideoData> cnVar) {
        this.E = cnVar;
        this.I = false;
        cnVar.getStatHolder().a(this.C);
        this.N = ho.c(cnVar.getStatHolder());
        if (this.player != null) {
            this.N.setView(this.player.getView());
        }
        VideoData videoData = (VideoData) cnVar.getMediaData();
        if (videoData != null) {
            Uri parse = Uri.parse(videoData.getUrl());
            if (this.player != null) {
                this.player.setVolume(this.volume);
                this.player.playAdVideo(parse, videoData.getWidth(), videoData.getHeight());
            }
        }
    }

    public void pause() {
        if (this.player != null) {
            this.player.pauseAdVideo();
        }
    }

    public void resume() {
        if (this.player != null) {
            this.player.resumeAdVideo();
        }
    }

    public void stop() {
        if (this.J == 1) {
            if (!(this.E == null || this.O == null)) {
                Context context = getContext();
                if (context != null) {
                    hl.a((List<dg>) this.E.getStatHolder().N("playbackStopped"), context);
                }
                this.O.c(this.E);
            }
            this.J = 0;
        }
        if (this.player != null) {
            this.player.stopAdVideo();
        }
        u();
    }

    public void destroy() {
        if (this.player != null) {
            this.player.destroy();
        }
        this.player = null;
        u();
    }

    public void setConnectionTimeout(int i) {
        this.H = i;
    }

    public float getVolume() {
        return this.volume;
    }

    /* access modifiers changed from: private */
    public void q() {
        float f;
        float f2;
        float f3;
        float duration = this.E != null ? this.E.getDuration() : 0.0f;
        if (this.E == null) {
            this.A.e(this.M);
            return;
        }
        if (this.J != 1 || this.player == null) {
            f3 = 0.0f;
            f2 = 0.0f;
            f = 0.0f;
        } else {
            f3 = this.player.getAdVideoDuration();
            f2 = this.player.getAdVideoPosition();
            f = duration - f2;
        }
        if (this.J != 1 || this.G == f2 || f3 <= 0.0f) {
            this.F++;
        } else {
            a(f, f2, duration);
        }
        if (this.F >= (this.H * 1000) / Callback.DEFAULT_DRAG_ANIMATION_DURATION) {
            r();
        }
    }

    private void a(float f, float f2, float f3) {
        this.F = 0;
        this.G = f2;
        if (f2 < f3) {
            c(f2);
            if (this.N != null) {
                this.N.k(f2);
            }
            if (this.O != null && this.E != null) {
                this.O.a(f, f3, this.E);
                return;
            }
            return;
        }
        d(f3);
    }

    private void d(float f) {
        c(f);
        this.G = f;
        if (this.N != null) {
            this.N.k(f);
        }
        if (!(this.O == null || this.E == null)) {
            this.O.a(0.0f, f, this.E);
        }
        s();
    }

    private void r() {
        StringBuilder sb = new StringBuilder();
        sb.append("video freeze more then ");
        sb.append(this.H);
        sb.append(" seconds, stopping");
        ah.a(sb.toString());
        this.A.e(this.M);
        if (this.O != null && this.E != null) {
            this.O.a("Timeout", this.E);
        }
    }

    /* access modifiers changed from: private */
    public void b(float f) {
        if (!(this.E == null || this.O == null)) {
            this.O.b(this.E);
        }
        Context context = getContext();
        if (!(context == null || this.E == null)) {
            hl.a((List<dg>) this.E.getStatHolder().N("playbackStarted"), context);
        }
        if (!(this.O == null || this.E == null)) {
            this.O.a(f, f, this.E);
        }
        c(0.0f);
        this.I = true;
    }

    private void s() {
        this.A.e(this.M);
        if (this.J != 2) {
            this.J = 2;
            if (this.player != null) {
                this.player.stopAdVideo();
            }
            cn<VideoData> cnVar = this.E;
            u();
            if (cnVar != null && this.O != null) {
                this.O.d(cnVar);
            }
        }
    }

    /* access modifiers changed from: private */
    public void u() {
        this.E = null;
        if (this.N != null) {
            this.N.destroy();
            this.N = null;
        }
    }

    /* access modifiers changed from: private */
    public void c(float f) {
        ArrayList arrayList = new ArrayList();
        while (this.C.size() > 0 && ((df) this.C.peek()).cf() <= f) {
            arrayList.add(this.C.pop());
        }
        if (this.player != null) {
            hl.a((List<dg>) arrayList, this.player.getView().getContext());
        }
    }
}
