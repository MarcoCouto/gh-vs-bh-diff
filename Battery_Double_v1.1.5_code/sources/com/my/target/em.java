package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import android.util.Xml;
import com.explorestack.iab.vast.tags.VastAttributes;
import com.explorestack.iab.vast.tags.VastTagName;
import com.google.android.exoplayer2.source.chunk.ChunkedTrackBlacklistUtil;
import com.google.android.exoplayer2.util.MimeTypes;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.my.target.cd;
import com.my.target.common.models.AudioData;
import com.my.target.common.models.VideoData;
import com.yandex.mobile.ads.video.tracking.Tracker.Events;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Locale;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

/* compiled from: VastParser */
public class em<T extends cd> {
    private static final String[] eu = {MimeTypes.VIDEO_MP4, "application/vnd.apple.mpegurl", "application/x-mpegurl"};
    private static final String[] ev = {"linkTxt"};
    @NonNull
    private final a adConfig;
    @NonNull
    private final ArrayList<dg> cK = new ArrayList<>();
    @NonNull
    private final Context context;
    @Nullable
    private String ctaText;
    private boolean eA;
    @Nullable
    private bz eB;
    @NonNull
    private final bz eo;
    @NonNull
    private final ArrayList<df> ew = new ArrayList<>();
    @NonNull
    private final ArrayList<ch> ex = new ArrayList<>();
    @NonNull
    private final ArrayList<dg> ey = new ArrayList<>();
    @NonNull
    private final ArrayList<cn<T>> ez = new ArrayList<>();

    @NonNull
    public static <T extends cd> em<T> a(@NonNull a aVar, @NonNull bz bzVar, @NonNull Context context2) {
        return new em<>(aVar, bzVar, context2);
    }

    private static void a(@NonNull XmlPullParser xmlPullParser) {
        if (e(xmlPullParser) == 2) {
            int i = 1;
            while (i != 0) {
                switch (b(xmlPullParser)) {
                    case 2:
                        i++;
                        break;
                    case 3:
                        i--;
                        break;
                }
            }
        }
    }

    private static int b(@NonNull XmlPullParser xmlPullParser) {
        try {
            return xmlPullParser.next();
        } catch (XmlPullParserException e) {
            ah.a(e.getMessage());
            return Integer.MIN_VALUE;
        } catch (IOException e2) {
            ah.a(e2.getMessage());
            return Integer.MIN_VALUE;
        }
    }

    @NonNull
    private static String c(@NonNull XmlPullParser xmlPullParser) {
        String str = "";
        if (b(xmlPullParser) == 4) {
            str = xmlPullParser.getText();
            d(xmlPullParser);
        } else {
            StringBuilder sb = new StringBuilder();
            sb.append("No text: ");
            sb.append(xmlPullParser.getName());
            ah.a(sb.toString());
        }
        return str.trim();
    }

    @Nullable
    private static String a(@NonNull String str, @NonNull XmlPullParser xmlPullParser) {
        return xmlPullParser.getAttributeValue(null, str);
    }

    @NonNull
    private static String U(@NonNull String str) {
        return str.replaceAll("&amp;", RequestParameters.AMPERSAND).replaceAll("&lt;", "<").replaceAll("&gt;", ">").trim();
    }

    private static int d(@NonNull XmlPullParser xmlPullParser) {
        try {
            return xmlPullParser.nextTag();
        } catch (XmlPullParserException e) {
            ah.a(e.getMessage());
            return Integer.MIN_VALUE;
        } catch (IOException e2) {
            ah.a(e2.getMessage());
            return Integer.MIN_VALUE;
        }
    }

    private static int e(@NonNull XmlPullParser xmlPullParser) {
        try {
            return xmlPullParser.getEventType();
        } catch (XmlPullParserException e) {
            ah.a(e.getMessage());
            return Integer.MIN_VALUE;
        }
    }

    private em(@NonNull a aVar, @NonNull bz bzVar, @NonNull Context context2) {
        this.adConfig = aVar;
        this.eo = bzVar;
        this.context = context2;
    }

    @NonNull
    public ArrayList<cn<T>> cD() {
        return this.ez;
    }

    @NonNull
    public ArrayList<dg> aZ() {
        return this.cK;
    }

    public void V(@NonNull String str) {
        XmlPullParser newPullParser = Xml.newPullParser();
        try {
            newPullParser.setFeature("http://xmlpull.org/v1/doc/features.html#process-namespaces", false);
            newPullParser.setInput(new StringReader(str));
            cF();
            int e = e(newPullParser);
            while (e != 1 && e != Integer.MIN_VALUE) {
                if (e == 2 && VastTagName.VAST.equalsIgnoreCase(newPullParser.getName())) {
                    f(newPullParser);
                }
                e = b(newPullParser);
            }
        } catch (XmlPullParserException e2) {
            StringBuilder sb = new StringBuilder();
            sb.append("Unable to parse VAST: ");
            sb.append(e2.getMessage());
            ah.a(sb.toString());
        }
    }

    @Nullable
    public bz cE() {
        return this.eB;
    }

    private void b(@Nullable String str, @NonNull String str2, @Nullable String str3) {
        dp.P(str2).Q(str3).r(this.adConfig.getSlotId()).S(str).R(this.eo.getUrl()).q(this.context);
    }

    private void cF() {
        ArrayList aZ = this.eo.aZ();
        if (aZ != null) {
            this.cK.addAll(aZ);
        }
        ArrayList companionBanners = this.eo.getCompanionBanners();
        if (companionBanners != null) {
            this.ex.addAll(companionBanners);
        }
    }

    private void cG() {
        Iterator it = this.ez.iterator();
        while (it.hasNext()) {
            cn cnVar = (cn) it.next();
            dh statHolder = cnVar.getStatHolder();
            statHolder.a(this.eo.bh(), cnVar.getDuration());
            if (!TextUtils.isEmpty(this.ctaText)) {
                cnVar.setCtaText(this.ctaText);
            } else if (!TextUtils.isEmpty(this.eo.getCtaText())) {
                cnVar.setCtaText(this.eo.getCtaText());
            }
            Iterator it2 = this.ew.iterator();
            while (it2.hasNext()) {
                df dfVar = (df) it2.next();
                a(dfVar.cg(), dfVar.getUrl(), (cg) cnVar);
            }
            Iterator it3 = this.ey.iterator();
            while (it3.hasNext()) {
                statHolder.b((dg) it3.next());
            }
            Iterator it4 = this.ex.iterator();
            while (it4.hasNext()) {
                cnVar.addCompanion((ch) it4.next());
            }
        }
    }

    private void f(@NonNull XmlPullParser xmlPullParser) {
        while (d(xmlPullParser) == 2) {
            if (e(xmlPullParser) == 2 && VastTagName.AD.equals(xmlPullParser.getName())) {
                g(xmlPullParser);
            }
        }
    }

    private void g(@NonNull XmlPullParser xmlPullParser) {
        while (d(xmlPullParser) == 2) {
            if (e(xmlPullParser) == 2) {
                String name = xmlPullParser.getName();
                if (VastTagName.WRAPPER.equals(name)) {
                    this.eA = true;
                    ah.a("VAST file contains wrapped ad information.");
                    int ba = this.eo.ba();
                    if (ba < 5) {
                        a(xmlPullParser, ba);
                    } else {
                        ah.a("got VAST wrapper, but max redirects limit exceeded");
                        a(xmlPullParser);
                    }
                } else if (VastTagName.IN_LINE.equals(name)) {
                    this.eA = false;
                    ah.a("VAST file contains inline ad information.");
                    h(xmlPullParser);
                } else {
                    a(xmlPullParser);
                }
            }
        }
    }

    private void a(@NonNull XmlPullParser xmlPullParser, int i) {
        String str = null;
        while (d(xmlPullParser) == 2) {
            if (e(xmlPullParser) == 2) {
                String name = xmlPullParser.getName();
                if (VastTagName.IMPRESSION.equals(name)) {
                    j(xmlPullParser);
                } else if (VastTagName.CREATIVES.equals(name)) {
                    k(xmlPullParser);
                } else if (VastTagName.EXTENSIONS.equals(name)) {
                    i(xmlPullParser);
                } else if (VastTagName.VAST_AD_TAG_URI.equals(name)) {
                    str = c(xmlPullParser);
                } else {
                    a(xmlPullParser);
                }
            }
        }
        if (str != null) {
            this.eB = bz.q(str);
            this.eB.f(i + 1);
            this.eB.c(this.cK);
            this.eB.setCtaText(this.ctaText != null ? this.ctaText : this.eo.getCtaText());
            this.eB.b(this.ex);
            this.eB.a(this.eo.bc());
            this.eB.b(this.eo.bd());
            this.eB.c(this.eo.be());
            this.eB.d(this.eo.bf());
            this.eB.e(this.eo.bg());
            this.eB.f(this.eo.bi());
            this.eB.g(this.eo.bj());
            this.eB.setAllowCloseDelay(this.eo.getAllowCloseDelay());
            dh bh = this.eB.bh();
            bh.e(this.ey);
            bh.f(this.ew);
            bh.a(this.eo.bh(), -1.0f);
            this.eo.b(this.eB);
            return;
        }
        ah.a("got VAST wrapper, but no vastAdTagUri");
    }

    private void h(@NonNull XmlPullParser xmlPullParser) {
        while (d(xmlPullParser) == 2) {
            if (e(xmlPullParser) == 2) {
                String name = xmlPullParser.getName();
                if (VastTagName.IMPRESSION.equals(name)) {
                    j(xmlPullParser);
                } else if (name != null && name.equals(VastTagName.CREATIVES)) {
                    k(xmlPullParser);
                } else if (name == null || !name.equals(VastTagName.EXTENSIONS)) {
                    a(xmlPullParser);
                } else {
                    i(xmlPullParser);
                }
            }
        }
        cG();
    }

    private void i(@NonNull XmlPullParser xmlPullParser) {
        while (d(xmlPullParser) == 2) {
            if (e(xmlPullParser) == 2) {
                if (VastTagName.EXTENSION.equals(xmlPullParser.getName())) {
                    String a2 = a("type", xmlPullParser);
                    for (String equals : ev) {
                        if (equals.equals(a2)) {
                            a(xmlPullParser, a2);
                        } else {
                            a(xmlPullParser);
                        }
                    }
                } else {
                    a(xmlPullParser);
                }
            }
        }
    }

    private void a(@NonNull XmlPullParser xmlPullParser, String str) {
        if ("linkTxt".equals(str)) {
            String c = c(xmlPullParser);
            this.ctaText = hn.decode(c);
            StringBuilder sb = new StringBuilder();
            sb.append("VAST linkTxt raw text: ");
            sb.append(c);
            ah.a(sb.toString());
        }
    }

    private void j(@NonNull XmlPullParser xmlPullParser) {
        String c = c(xmlPullParser);
        if (!TextUtils.isEmpty(c)) {
            this.cK.add(dg.c("impression", c));
            StringBuilder sb = new StringBuilder();
            sb.append("Impression tracker url for wrapper: ");
            sb.append(c);
            ah.a(sb.toString());
        }
    }

    private void k(@NonNull XmlPullParser xmlPullParser) {
        while (d(xmlPullParser) == 2) {
            if (e(xmlPullParser) == 2) {
                if (VastTagName.CREATIVE.equals(xmlPullParser.getName())) {
                    b(xmlPullParser, a("id", xmlPullParser));
                } else {
                    a(xmlPullParser);
                }
            }
        }
    }

    private void b(@NonNull XmlPullParser xmlPullParser, @Nullable String str) {
        while (d(xmlPullParser) == 2) {
            if (e(xmlPullParser) == 2) {
                String name = xmlPullParser.getName();
                cn cnVar = null;
                if (VastTagName.LINEAR.equals(name)) {
                    if (!this.eA) {
                        cnVar = cn.newBanner();
                        cnVar.setId(str != null ? str : "");
                    }
                    a(xmlPullParser, cnVar, a(VastAttributes.SKIP_OFFSET, xmlPullParser));
                    if (cnVar != null) {
                        if (cnVar.getDuration() <= 0.0f) {
                            b(cnVar.getId(), "Required field", "VAST has no valid Duration");
                        } else if (cnVar.getMediaData() != null) {
                            this.ez.add(cnVar);
                        } else {
                            b(cnVar.getId(), "Required field", "VAST has no valid mediaData");
                        }
                    }
                } else if (name == null || !name.equals(VastTagName.COMPANION_ADS)) {
                    a(xmlPullParser);
                } else {
                    String a2 = a(VastAttributes.REQUIRED, xmlPullParser);
                    if (a2 != null && !"all".equals(a2) && !"any".equals(a2) && !"none".equals(a2)) {
                        StringBuilder sb = new StringBuilder();
                        sb.append("Wrong companion required attribute:");
                        sb.append(a2);
                        b(str, "Bad value", sb.toString());
                        a2 = null;
                    }
                    a(xmlPullParser, str, a2);
                }
            }
        }
    }

    private void a(@NonNull XmlPullParser xmlPullParser, @Nullable String str, @Nullable String str2) {
        while (d(xmlPullParser) == 2) {
            b(xmlPullParser, str, str2);
        }
    }

    private void b(@NonNull XmlPullParser xmlPullParser, @Nullable String str, @Nullable String str2) {
        if (e(xmlPullParser) == 2) {
            String name = xmlPullParser.getName();
            if (name == null || !name.equals(VastTagName.COMPANION)) {
                a(xmlPullParser);
            } else {
                String a2 = a("width", xmlPullParser);
                String a3 = a("height", xmlPullParser);
                String a4 = a("id", xmlPullParser);
                ch newBanner = ch.newBanner();
                if (a4 == null) {
                    a4 = "";
                }
                newBanner.setId(a4);
                try {
                    newBanner.setWidth(Integer.parseInt(a2));
                    newBanner.setHeight(Integer.parseInt(a3));
                } catch (NumberFormatException unused) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("Unable  to convert required companion attributes, width = ");
                    sb.append(a2);
                    sb.append(" height = ");
                    sb.append(a3);
                    b(str, "Bad value", sb.toString());
                }
                newBanner.setRequired(str2);
                String a5 = a(VastAttributes.ASSET_WIDTH, xmlPullParser);
                String a6 = a(VastAttributes.ASSET_HEIGHT, xmlPullParser);
                try {
                    if (!TextUtils.isEmpty(a5)) {
                        newBanner.setAssetWidth(Integer.parseInt(a5));
                    }
                    if (!TextUtils.isEmpty(a6)) {
                        newBanner.setAssetHeight(Integer.parseInt(a6));
                    }
                } catch (NumberFormatException e) {
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("wrong VAST asset dimensions: ");
                    sb2.append(e.getMessage());
                    ah.a(sb2.toString());
                }
                String a7 = a(VastAttributes.EXPANDED_WIDTH, xmlPullParser);
                String a8 = a(VastAttributes.EXPANDED_HEIGHT, xmlPullParser);
                try {
                    if (!TextUtils.isEmpty(a7)) {
                        newBanner.setExpandedWidth(Integer.parseInt(a7));
                    }
                    if (!TextUtils.isEmpty(a8)) {
                        newBanner.setExpandedHeight(Integer.parseInt(a8));
                    }
                } catch (NumberFormatException e2) {
                    StringBuilder sb3 = new StringBuilder();
                    sb3.append("wrong VAST expanded dimensions ");
                    sb3.append(e2.getMessage());
                    ah.a(sb3.toString());
                }
                newBanner.setAdSlotID(a(VastAttributes.AD_SLOT_ID, xmlPullParser));
                newBanner.setApiFramework(a(VastAttributes.API_FRAMEWORK, xmlPullParser));
                this.ex.add(newBanner);
                while (d(xmlPullParser) == 2) {
                    String name2 = xmlPullParser.getName();
                    if (VastTagName.STATIC_RESOURCE.equals(name2)) {
                        newBanner.setStaticResource(hn.decode(c(xmlPullParser)));
                    } else if (VastTagName.HTML_RESOURCE.equals(name2)) {
                        newBanner.setHtmlResource(hn.decode(c(xmlPullParser)));
                    } else if (VastTagName.I_FRAME_RESOURCE.equals(name2)) {
                        newBanner.setIframeResource(hn.decode(c(xmlPullParser)));
                    } else if (VastTagName.COMPANION_CLICK_THROUGH.equals(name2)) {
                        String c = c(xmlPullParser);
                        if (!TextUtils.isEmpty(c)) {
                            newBanner.setTrackingLink(U(c));
                        }
                    } else if (VastTagName.COMPANION_CLICK_TRACKING.equals(name2)) {
                        String c2 = c(xmlPullParser);
                        if (!TextUtils.isEmpty(c2)) {
                            newBanner.getStatHolder().b(dg.c("click", c2));
                        }
                    } else if (VastTagName.TRACKING_EVENTS.equals(name2)) {
                        a(xmlPullParser, (cg) newBanner);
                    } else {
                        a(xmlPullParser);
                    }
                }
            }
        }
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public float W(@NonNull String str) {
        long j = 0;
        float f = -1.0f;
        try {
            if (str.contains(".")) {
                int indexOf = str.indexOf(".");
                long parseLong = Long.parseLong(str.substring(indexOf + 1));
                if (parseLong > 1000) {
                    return -1.0f;
                }
                str = str.substring(0, indexOf);
                j = parseLong;
            }
            String[] split = str.split(":", 3);
            long parseInt = (long) Integer.parseInt(split[0]);
            long parseInt2 = (long) Integer.parseInt(split[1]);
            long parseInt3 = (long) Integer.parseInt(split[2]);
            if (parseInt >= 24 || parseInt2 >= 60 || parseInt3 >= 60) {
                return -1.0f;
            }
            f = ((float) (((j + (parseInt3 * 1000)) + (parseInt2 * ChunkedTrackBlacklistUtil.DEFAULT_TRACK_BLACKLIST_MS)) + (parseInt * 3600000))) / 1000.0f;
            return f;
        } catch (Exception unused) {
        }
    }

    private boolean a(@NonNull XmlPullParser xmlPullParser, @NonNull cn cnVar) {
        float f;
        try {
            f = W(c(xmlPullParser));
        } catch (Exception unused) {
            f = 0.0f;
        }
        if (f <= 0.0f) {
            return false;
        }
        cnVar.setDuration(f);
        return true;
    }

    private void b(@NonNull XmlPullParser xmlPullParser, @Nullable cn cnVar) {
        while (d(xmlPullParser) == 2) {
            if (e(xmlPullParser) == 2) {
                String name = xmlPullParser.getName();
                if (VastTagName.CLICK_THROUGH.equals(name)) {
                    if (cnVar != null) {
                        String c = c(xmlPullParser);
                        if (!TextUtils.isEmpty(c)) {
                            cnVar.setTrackingLink(U(c));
                        }
                    }
                } else if (VastTagName.CLICK_TRACKING.equals(name)) {
                    String c2 = c(xmlPullParser);
                    if (!TextUtils.isEmpty(c2)) {
                        this.ey.add(dg.c("click", c2));
                    }
                } else {
                    a(xmlPullParser);
                }
            }
        }
    }

    private void c(@NonNull XmlPullParser xmlPullParser, @NonNull cn cnVar) {
        if ("instreamads".equals(this.adConfig.getFormat()) || Events.CREATIVE_FULLSCREEN.equals(this.adConfig.getFormat())) {
            e(xmlPullParser, cnVar);
        } else if ("instreamaudioads".equals(this.adConfig.getFormat())) {
            d(xmlPullParser, cnVar);
        }
    }

    private void d(@NonNull XmlPullParser xmlPullParser, @NonNull cn<AudioData> cnVar) {
        int i;
        while (d(xmlPullParser) == 2) {
            if (e(xmlPullParser) == 2) {
                if (VastTagName.MEDIA_FILE.equals(xmlPullParser.getName())) {
                    String a2 = a("type", xmlPullParser);
                    String a3 = a(VastAttributes.BITRATE, xmlPullParser);
                    String U = U(c(xmlPullParser));
                    AudioData audioData = null;
                    if (!TextUtils.isEmpty(a2) && !TextUtils.isEmpty(U) && a2.toLowerCase(Locale.ROOT).trim().startsWith(MimeTypes.BASE_TYPE_AUDIO)) {
                        if (a3 != null) {
                            try {
                                i = Integer.parseInt(a3);
                            } catch (NumberFormatException unused) {
                            }
                            audioData = AudioData.newAudioData(U);
                            audioData.setBitrate(i);
                        }
                        i = 0;
                        audioData = AudioData.newAudioData(U);
                        audioData.setBitrate(i);
                    }
                    if (audioData == null) {
                        StringBuilder sb = new StringBuilder();
                        sb.append("Skipping unsupported VAST file (mimetype=");
                        sb.append(a2);
                        sb.append(",url=");
                        sb.append(U);
                        ah.a(sb.toString());
                    } else {
                        cnVar.setMediaData(audioData);
                    }
                } else {
                    a(xmlPullParser);
                }
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:37:0x0086  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00b4  */
    private void e(@NonNull XmlPullParser xmlPullParser, @NonNull cn<VideoData> cnVar) {
        int i;
        int i2;
        ArrayList arrayList = new ArrayList();
        while (d(xmlPullParser) == 2) {
            if (e(xmlPullParser) == 2) {
                if (VastTagName.MEDIA_FILE.equals(xmlPullParser.getName())) {
                    String a2 = a("type", xmlPullParser);
                    String a3 = a(VastAttributes.BITRATE, xmlPullParser);
                    String a4 = a("width", xmlPullParser);
                    String a5 = a("height", xmlPullParser);
                    String U = U(c(xmlPullParser));
                    VideoData videoData = null;
                    if (!TextUtils.isEmpty(a2) && !TextUtils.isEmpty(U)) {
                        String[] strArr = eu;
                        int length = strArr.length;
                        int i3 = 0;
                        int i4 = 0;
                        while (true) {
                            if (i4 >= length) {
                                break;
                            } else if (strArr[i4].equals(a2)) {
                                if (a4 != null) {
                                    try {
                                        i2 = Integer.parseInt(a4);
                                    } catch (NumberFormatException unused) {
                                        i2 = 0;
                                        i = 0;
                                        videoData = VideoData.newVideoData(U, i2, i);
                                        videoData.setBitrate(i3);
                                        if (videoData == null) {
                                        }
                                    }
                                } else {
                                    i2 = 0;
                                }
                                if (a5 != null) {
                                    try {
                                        i = Integer.parseInt(a5);
                                    } catch (NumberFormatException unused2) {
                                        i = 0;
                                        videoData = VideoData.newVideoData(U, i2, i);
                                        videoData.setBitrate(i3);
                                        if (videoData == null) {
                                        }
                                    }
                                } else {
                                    i = 0;
                                }
                                if (a3 != null) {
                                    try {
                                        i3 = Integer.parseInt(a3);
                                    } catch (NumberFormatException unused3) {
                                    }
                                }
                                if (i2 > 0 && i > 0) {
                                    videoData = VideoData.newVideoData(U, i2, i);
                                    videoData.setBitrate(i3);
                                }
                            } else {
                                i4++;
                            }
                        }
                    }
                    if (videoData == null) {
                        StringBuilder sb = new StringBuilder();
                        sb.append("Skipping unsupported VAST file (mimeType=");
                        sb.append(a2);
                        sb.append(",width=");
                        sb.append(a4);
                        sb.append(",height=");
                        sb.append(a5);
                        sb.append(",url=");
                        sb.append(U);
                        ah.a(sb.toString());
                    } else {
                        arrayList.add(videoData);
                    }
                } else {
                    a(xmlPullParser);
                }
            }
        }
        cnVar.setMediaData(VideoData.chooseBest(arrayList, this.adConfig.getVideoQuality()));
    }

    private void a(@NonNull XmlPullParser xmlPullParser, @Nullable cg cgVar) {
        while (d(xmlPullParser) == 2) {
            if (e(xmlPullParser) == 2) {
                if (VastTagName.TRACKING.equals(xmlPullParser.getName())) {
                    String a2 = a("event", xmlPullParser);
                    String a3 = a("offset", xmlPullParser);
                    if (a2 != null) {
                        if (!NotificationCompat.CATEGORY_PROGRESS.equals(a2) || TextUtils.isEmpty(a3)) {
                            c(a2, c(xmlPullParser), cgVar);
                        } else if (a3.endsWith("%")) {
                            try {
                                a((float) Integer.parseInt(a3.replace("%", "")), c(xmlPullParser), cgVar);
                            } catch (Exception unused) {
                                StringBuilder sb = new StringBuilder();
                                sb.append("Unable to parse progress stat with value ");
                                sb.append(a3);
                                ah.a(sb.toString());
                            }
                        } else {
                            b(a3, c(xmlPullParser), cgVar);
                        }
                    }
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("Added VAST tracking \"");
                    sb2.append(a2);
                    sb2.append("\"");
                    ah.a(sb2.toString());
                } else {
                    a(xmlPullParser);
                }
            }
        }
    }

    private void a(@NonNull String str, @NonNull String str2, @Nullable cg cgVar) {
        if (cgVar != null) {
            cgVar.getStatHolder().b(dg.c(str, str2));
            return;
        }
        this.ey.add(dg.c(str, str2));
    }

    private void b(@NonNull String str, @NonNull String str2, @Nullable cg cgVar) {
        float f;
        try {
            f = W(str);
        } catch (Exception unused) {
            f = -1.0f;
        }
        if (f >= 0.0f) {
            df M = df.M(str2);
            M.h(f);
            if (cgVar != null) {
                cgVar.getStatHolder().b(M);
            } else {
                this.ey.add(M);
            }
        } else {
            StringBuilder sb = new StringBuilder();
            sb.append("Unable to parse progress stat with value ");
            sb.append(str);
            ah.a(sb.toString());
        }
    }

    private void a(float f, @NonNull String str, @Nullable cg cgVar) {
        df M = df.M(str);
        if (cgVar == null || cgVar.getDuration() <= 0.0f) {
            M.i(f);
            this.ew.add(M);
            return;
        }
        M.h(cgVar.getDuration() * (f / 100.0f));
        cgVar.getStatHolder().b(M);
    }

    private void c(@NonNull String str, @NonNull String str2, @Nullable cg cgVar) {
        if ("start".equalsIgnoreCase(str)) {
            a("playbackStarted", str2, cgVar);
        } else if ("firstQuartile".equalsIgnoreCase(str)) {
            a(25.0f, str2, cgVar);
        } else if ("midpoint".equalsIgnoreCase(str)) {
            a(50.0f, str2, cgVar);
        } else if ("thirdQuartile".equalsIgnoreCase(str)) {
            a(75.0f, str2, cgVar);
        } else if ("complete".equalsIgnoreCase(str)) {
            a(100.0f, str2, cgVar);
        } else if ("creativeView".equalsIgnoreCase(str)) {
            a("playbackStarted", str2, cgVar);
        } else if ("mute".equalsIgnoreCase(str)) {
            a("volumeOff", str2, cgVar);
        } else if ("unmute".equalsIgnoreCase(str)) {
            a("volumeOn", str2, cgVar);
        } else if (CampaignEx.JSON_NATIVE_VIDEO_PAUSE.equalsIgnoreCase(str)) {
            a("playbackPaused", str2, cgVar);
        } else if (CampaignEx.JSON_NATIVE_VIDEO_RESUME.equalsIgnoreCase(str)) {
            a("playbackResumed", str2, cgVar);
        } else if (Events.CREATIVE_FULLSCREEN.equalsIgnoreCase(str)) {
            a("fullscreenOn", str2, cgVar);
        } else if ("exitFullscreen".equalsIgnoreCase(str)) {
            a("fullscreenOff", str2, cgVar);
        } else if ("skip".equalsIgnoreCase(str)) {
            a("closedByUser", str2, cgVar);
        } else if ("error".equalsIgnoreCase(str)) {
            a("error", str2, cgVar);
        } else if (VastTagName.CLICK_TRACKING.equalsIgnoreCase(str)) {
            a("click", str2, cgVar);
        } else if ("close".equalsIgnoreCase(str)) {
            a("closedByUser", str2, cgVar);
        } else if ("closeLinear".equalsIgnoreCase(str)) {
            a("closedByUser", str2, cgVar);
        }
    }

    /* access modifiers changed from: 0000 */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x006c  */
    /* JADX WARNING: Removed duplicated region for block: B:16:? A[RETURN, SYNTHETIC] */
    @VisibleForTesting
    public void b(@NonNull cn cnVar, @Nullable String str) {
        float f;
        if (str != null) {
            if (str.contains("%")) {
                int parseInt = Integer.parseInt(str.substring(0, str.length() - 1));
                StringBuilder sb = new StringBuilder();
                sb.append("Linear skipoffset is ");
                sb.append(str);
                sb.append(" [%]");
                ah.a(sb.toString());
                f = (cnVar.getDuration() / 100.0f) * ((float) parseInt);
            } else if (str.contains(":")) {
                try {
                    f = W(str);
                } catch (Exception unused) {
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("Failed to convert ISO time skipoffset string ");
                    sb2.append(str);
                    b(cnVar.getId(), "Bad value", sb2.toString());
                }
            }
            if (f <= 0.0f) {
                cnVar.setAllowCloseDelay(f);
                return;
            }
            return;
        }
        f = -1.0f;
        if (f <= 0.0f) {
        }
    }

    private void a(@NonNull XmlPullParser xmlPullParser, @Nullable cn cnVar, @Nullable String str) {
        while (d(xmlPullParser) == 2) {
            String name = xmlPullParser.getName();
            if (e(xmlPullParser) == 2) {
                if (VastTagName.DURATION.equals(name)) {
                    if (cnVar == null) {
                        continue;
                    } else if (a(xmlPullParser, cnVar)) {
                        b(cnVar, str);
                    } else {
                        return;
                    }
                } else if (VastTagName.TRACKING_EVENTS.equals(name)) {
                    a(xmlPullParser, (cg) cnVar);
                } else if (VastTagName.MEDIA_FILES.equals(name)) {
                    if (cnVar == null) {
                        continue;
                    } else {
                        c(xmlPullParser, cnVar);
                        if (cnVar.getMediaData() == null) {
                            ah.a("Unable to find valid mediafile!");
                            return;
                        }
                    }
                } else if (VastTagName.VIDEO_CLICKS.equals(name)) {
                    b(xmlPullParser, cnVar);
                } else {
                    a(xmlPullParser);
                }
            }
        }
    }
}
