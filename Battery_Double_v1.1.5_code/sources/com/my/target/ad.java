package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import com.github.mikephil.charting.utils.Utils;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* compiled from: ResearchViewabilityTracker */
public class ad {
    @NonNull
    private final ArrayList<cf> r = new ArrayList<>();
    private float s = -1.0f;
    private long u;
    @Nullable
    private WeakReference<View> viewWeakReference;

    @NonNull
    public static ad b(@NonNull dh dhVar) {
        return new ad(dhVar);
    }

    public void setView(@Nullable View view) {
        if (view != null || this.viewWeakReference == null) {
            this.viewWeakReference = new WeakReference<>(view);
        } else {
            this.viewWeakReference.clear();
        }
    }

    private ad(@NonNull dh dhVar) {
        Iterator it = dhVar.cj().iterator();
        while (it.hasNext()) {
            dd ddVar = (dd) it.next();
            if (ddVar instanceof cf) {
                this.r.add((cf) ddVar);
            }
        }
    }

    public void c(int i) {
        float f = (float) i;
        if (f != this.s) {
            if (!d(i)) {
                rewind();
            }
            Context context = null;
            double d = Utils.DOUBLE_EPSILON;
            if (this.viewWeakReference != null) {
                View view = (View) this.viewWeakReference.get();
                if (view != null) {
                    d = hp.j(view);
                    context = view.getContext();
                }
            }
            a(d, i, context);
            this.s = f;
            this.u = System.currentTimeMillis();
        }
    }

    private void rewind() {
        Iterator it = this.r.iterator();
        while (it.hasNext()) {
            ((cf) it.next()).g(-1.0f);
        }
    }

    private boolean d(int i) {
        float f = (float) i;
        boolean z = false;
        if (f < this.s) {
            return false;
        }
        if (this.u <= 0) {
            return true;
        }
        if ((((long) (f - this.s)) * 1000) - (System.currentTimeMillis() - this.u) <= 1000) {
            z = true;
        }
        return z;
    }

    private void a(double d, int i, @Nullable Context context) {
        if (!this.r.isEmpty()) {
            if (context == null) {
                Iterator it = this.r.iterator();
                while (it.hasNext()) {
                    ((dd) it.next()).g(-1.0f);
                }
                return;
            }
            b(d, i, context);
        }
    }

    private void b(double d, int i, @NonNull Context context) {
        ArrayList arrayList = new ArrayList();
        Iterator it = this.r.iterator();
        while (it.hasNext()) {
            cf cfVar = (cf) it.next();
            int bn = cfVar.bn();
            int bo = cfVar.bo();
            if (!(bn <= i && (bo == 0 || bo >= i))) {
                cfVar.g(-1.0f);
            } else if (((double) cfVar.cn()) > d) {
                cfVar.g(-1.0f);
            } else {
                if (cfVar.cd() >= 0.0f) {
                    float f = (float) i;
                    if (f > cfVar.cd()) {
                        if (f - cfVar.cd() >= cfVar.getDuration()) {
                            arrayList.add(cfVar);
                            it.remove();
                        }
                    }
                }
                cfVar.g((float) i);
            }
        }
        if (!arrayList.isEmpty()) {
            hl.a((List<dg>) arrayList, context);
        }
    }
}
