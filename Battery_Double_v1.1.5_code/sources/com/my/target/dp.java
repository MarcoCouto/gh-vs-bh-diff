package com.my.target;

import android.content.Context;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.util.Base64;
import com.ironsource.sdk.constants.Constants;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.my.target.common.MyTargetVersion;
import java.nio.charset.Charset;
import org.json.JSONObject;

/* compiled from: LogMessage */
public class dp {
    /* access modifiers changed from: private */
    @NonNull
    public static String ee = "https://ad.mail.ru/sdk/log/";
    @VisibleForTesting
    public static boolean ef = true;
    @Nullable
    private String cj;
    @Nullable
    private String eg;
    @Nullable
    private String eh;
    @Nullable
    private String ei;
    @NonNull
    private final String name;
    private int slotId;
    @NonNull
    private final String type;

    @NonNull
    public static dp P(@NonNull String str) {
        return new dp(str, "error");
    }

    private dp(@NonNull String str, @NonNull String str2) {
        this.name = str;
        this.type = str2;
    }

    @NonNull
    public dp Q(@Nullable String str) {
        this.eg = str;
        return this;
    }

    @NonNull
    public dp r(int i) {
        this.slotId = i;
        return this;
    }

    @NonNull
    public dp R(@Nullable String str) {
        this.eh = str;
        return this;
    }

    @NonNull
    public dp S(@Nullable String str) {
        this.cj = str;
        return this;
    }

    public void q(@NonNull final Context context) {
        ai.b(new Runnable() {
            public void run() {
                String cw = dp.this.cw();
                StringBuilder sb = new StringBuilder();
                sb.append("send message to log:\n ");
                sb.append(cw);
                ah.a(sb.toString());
                if (dp.ef) {
                    dl.cr().O(Base64.encodeToString(cw.getBytes(Charset.forName("UTF-8")), 0)).f(dp.ee, context);
                }
            }
        });
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    @NonNull
    public String cw() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("sdk", "myTarget");
            jSONObject.put("sdkver", MyTargetVersion.VERSION);
            jSONObject.put("os", Constants.JAVASCRIPT_INTERFACE_NAME);
            jSONObject.put("osver", VERSION.RELEASE);
            jSONObject.put("type", this.type);
            jSONObject.put("name", this.name);
            if (this.eg != null) {
                jSONObject.put("message", this.eg);
            }
            if (this.slotId > 0) {
                jSONObject.put("slot", this.slotId);
            }
            if (this.eh != null) {
                jSONObject.put("url", this.eh);
            }
            if (this.cj != null) {
                jSONObject.put(RequestParameters.BANNER_ID, this.cj);
            }
            if (this.ei != null) {
                jSONObject.put("data", this.ei);
            }
        } catch (Throwable unused) {
        }
        return jSONObject.toString();
    }
}
