package com.my.target;

import android.annotation.SuppressLint;
import android.graphics.SurfaceTexture;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.support.v7.widget.helper.ItemTouchHelper.Callback;
import android.util.Log;
import android.view.Surface;
import android.view.TextureView;
import android.view.TextureView.SurfaceTextureListener;
import com.my.target.common.models.VideoData;

/* compiled from: DefaultVideoPlayer */
public class hr implements OnCompletionListener, OnErrorListener, OnPreparedListener, SurfaceTextureListener, hq {
    @NonNull
    private final hk A = hk.C(Callback.DEFAULT_DRAG_ANIMATION_DURATION);
    @Nullable
    private TextureView hl;
    @Nullable
    private VideoData kk;
    @NonNull
    private final a mF = new a();
    @NonNull
    private final MediaPlayer mG;
    /* access modifiers changed from: private */
    @Nullable
    public com.my.target.hq.a mH;
    @Nullable
    private Surface mI;
    private int mJ;
    private int state = 0;
    private float volume = 1.0f;

    @VisibleForTesting
    /* compiled from: DefaultVideoPlayer */
    class a implements Runnable {
        a() {
        }

        public void run() {
            if (hr.this.mH != null) {
                float position = ((float) hr.this.getPosition()) / 1000.0f;
                float duration = hr.this.getDuration();
                if (duration > 0.0f) {
                    hr.this.mH.a(position, duration);
                }
            }
        }
    }

    public void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int i, int i2) {
    }

    public void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {
    }

    @NonNull
    public static hq dW() {
        return new hr(new MediaPlayer());
    }

    public boolean isStarted() {
        return this.state >= 1 && this.state < 3;
    }

    public void a(@Nullable com.my.target.hq.a aVar) {
        this.mH = aVar;
    }

    public boolean isPaused() {
        return this.state == 2;
    }

    hr(@NonNull MediaPlayer mediaPlayer) {
        this.mG = mediaPlayer;
    }

    public long getPosition() {
        if (dY()) {
            return (long) this.mG.getCurrentPosition();
        }
        return 0;
    }

    public float getDuration() {
        if (dY()) {
            return ((float) this.mG.getDuration()) / 1000.0f;
        }
        return 0.0f;
    }

    public void a(@NonNull VideoData videoData, @NonNull fn fnVar) {
        Uri uri;
        String str = (String) videoData.getData();
        if (str != null) {
            uri = Uri.parse(str);
        } else {
            uri = Uri.parse(videoData.getUrl());
        }
        this.kk = videoData;
        a(uri, fnVar);
    }

    public void stop() {
        this.A.e(this.mF);
        try {
            this.mG.stop();
        } catch (IllegalStateException unused) {
            ah.a("stop called in wrong state");
        }
        if (this.mH != null) {
            this.mH.x();
        }
        this.state = 3;
    }

    @SuppressLint({"Recycle"})
    public void a(@NonNull Uri uri, @NonNull fn fnVar) {
        ah.a("Play video in Android MediaPlayer");
        if (this.state != 0) {
            this.mG.reset();
            this.state = 0;
        }
        this.mG.setOnCompletionListener(this);
        this.mG.setOnErrorListener(this);
        this.mG.setOnPreparedListener(this);
        try {
            this.mG.setDataSource(fnVar.getContext(), uri);
            if (this.mH != null) {
                this.mH.B();
            }
            dX();
            this.hl = fnVar.getTextureView();
            if (this.hl.getSurfaceTextureListener() != null) {
                Log.w("DefaultVideoPlayer", "Replacing existing SurfaceTextureListener.");
            }
            this.hl.setSurfaceTextureListener(this);
            Surface surface = null;
            SurfaceTexture surfaceTexture = this.hl.isAvailable() ? this.hl.getSurfaceTexture() : null;
            if (surfaceTexture != null) {
                surface = new Surface(surfaceTexture);
            }
            a(surface);
            try {
                this.mG.prepareAsync();
            } catch (IllegalStateException unused) {
                ah.a("prepareAsync called in wrong state");
            }
        } catch (Exception e) {
            if (this.mH != null) {
                this.mH.d(e.toString());
            }
            StringBuilder sb = new StringBuilder();
            sb.append("DefaultVideoPlayerUnable to parse video source ");
            sb.append(e.getMessage());
            ah.a(sb.toString());
            this.state = 5;
            e.printStackTrace();
        }
    }

    public void destroy() {
        this.state = 5;
        this.A.e(this.mF);
        dX();
        a((Surface) null);
        if (dY()) {
            try {
                this.mG.stop();
            } catch (IllegalStateException unused) {
                ah.a("stop called in wrong state");
            }
        }
        this.mG.release();
    }

    public void K() {
        setVolume(0.2f);
    }

    public void L() {
        setVolume(0.0f);
    }

    public void setVolume(float f) {
        this.volume = f;
        if (dY()) {
            this.mG.setVolume(f, f);
        }
        if (this.mH != null) {
            this.mH.e(f);
        }
    }

    public void cJ() {
        setVolume(1.0f);
    }

    public void pause() {
        if (this.state == 1) {
            this.mJ = this.mG.getCurrentPosition();
            this.A.e(this.mF);
            try {
                this.mG.pause();
            } catch (IllegalStateException unused) {
                ah.a("pause called in wrong state");
            }
            this.state = 2;
            if (this.mH != null) {
                this.mH.z();
            }
        }
    }

    public void resume() {
        if (this.state == 2) {
            this.A.d(this.mF);
            try {
                this.mG.start();
            } catch (IllegalStateException unused) {
                ah.a("start called in wrong state");
            }
            if (this.mJ > 0) {
                try {
                    this.mG.seekTo(this.mJ);
                } catch (IllegalStateException unused2) {
                    ah.a("seekTo called in wrong state");
                }
                this.mJ = 0;
            }
            this.state = 1;
            if (this.mH != null) {
                this.mH.A();
            }
        }
    }

    public boolean isPlaying() {
        return this.state == 1;
    }

    public void onCompletion(MediaPlayer mediaPlayer) {
        this.state = 4;
        if (this.mH != null) {
            this.mH.onComplete();
        }
    }

    public boolean onError(MediaPlayer mediaPlayer, int i, int i2) {
        this.A.e(this.mF);
        dX();
        a((Surface) null);
        if (this.mH != null) {
            String str = "Unknown error";
            String str2 = "Unknown";
            if (i == 100) {
                str = "Server died";
            }
            if (i2 == -1004) {
                str2 = "IO error";
            } else if (i2 == -1007) {
                str2 = "Malformed error";
            } else if (i2 == -1010) {
                str2 = "Unsupported error";
            } else if (i2 == -110) {
                str2 = "Timed out error";
            } else if (i2 == Integer.MIN_VALUE) {
                str2 = "Low-level system error";
            }
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append(" (reason: ");
            sb.append(str2);
            sb.append(")");
            String sb2 = sb.toString();
            StringBuilder sb3 = new StringBuilder();
            sb3.append("DefaultVideoPlayerVideo error: ");
            sb3.append(sb2);
            ah.a(sb3.toString());
            this.mH.d(sb2);
        }
        if (this.state > 0) {
            this.mG.reset();
        }
        this.state = 0;
        return true;
    }

    public void onPrepared(MediaPlayer mediaPlayer) {
        mediaPlayer.setVolume(this.volume, this.volume);
        if (this.mH != null) {
            this.mH.y();
        }
        this.A.d(this.mF);
        this.state = 1;
        try {
            mediaPlayer.start();
        } catch (IllegalStateException unused) {
            ah.a("start called in wrong state");
        }
    }

    public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int i, int i2) {
        a(new Surface(surfaceTexture));
    }

    public boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {
        a((Surface) null);
        return true;
    }

    private void a(@Nullable Surface surface) {
        this.mG.setSurface(surface);
        if (!(this.mI == null || this.mI == surface)) {
            this.mI.release();
        }
        this.mI = surface;
    }

    private void dX() {
        if (this.hl != null) {
            if (this.hl.getSurfaceTextureListener() != this) {
                Log.w("DefaultVideoPlayer", "SurfaceTextureListener already unset or replaced.");
            } else {
                this.hl.setSurfaceTextureListener(null);
            }
            this.hl = null;
        }
    }

    private boolean dY() {
        return this.state >= 1 && this.state <= 4;
    }
}
