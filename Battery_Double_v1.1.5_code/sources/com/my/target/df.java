package com.my.target;

import android.support.annotation.NonNull;

/* compiled from: ProgressStat */
public class df extends dg {
    private float dT = -1.0f;
    private float value = -1.0f;

    @NonNull
    public static df M(@NonNull String str) {
        return new df(str);
    }

    private df(@NonNull String str) {
        super("playheadReachedValue", str);
    }

    public float cf() {
        return this.value;
    }

    public void h(float f) {
        this.value = f;
    }

    public float cg() {
        return this.dT;
    }

    public void i(float f) {
        this.dT = f;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("ProgressStat{");
        sb.append("value=");
        sb.append(this.value);
        sb.append(", pvalue=");
        sb.append(this.dT);
        sb.append('}');
        return sb.toString();
    }
}
