package com.my.target;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.my.target.ads.InterstitialAd;
import com.my.target.ads.InterstitialAd.InterstitialAdListener;
import com.my.target.common.MyTargetActivity;
import com.my.target.et.b;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* compiled from: InterstitialAdPromoEngine */
public class av extends as {
    @NonNull
    private final ArrayList<df> aV = new ArrayList<>();
    @NonNull
    private final cm ba;
    @Nullable
    private WeakReference<es> bb;
    @NonNull
    private final cx section;

    /* compiled from: InterstitialAdPromoEngine */
    static class a implements b, com.my.target.ev.a {
        @NonNull
        private final av bc;

        public void e(@NonNull String str) {
        }

        a(@NonNull av avVar) {
            this.bc = avVar;
        }

        public void b(@Nullable cg cgVar, @Nullable String str, @NonNull Context context) {
            if (cgVar != null) {
                this.bc.b(cgVar, str, context);
            }
        }

        public void ag() {
            this.bc.ag();
        }

        public void a(@NonNull cg cgVar, @NonNull Context context) {
            this.bc.a(cgVar, context);
        }

        public void a(@NonNull cg cgVar, @NonNull String str, @NonNull Context context) {
            this.bc.a(cgVar, str, context);
        }

        public void a(@NonNull cg cgVar, float f, float f2, @NonNull Context context) {
            this.bc.a(f, f2, context);
        }

        public void aj() {
            this.bc.aj();
        }

        public void ai() {
            this.bc.ai();
        }
    }

    @NonNull
    public static av a(@NonNull InterstitialAd interstitialAd, @NonNull cm cmVar, @NonNull cx cxVar) {
        return new av(interstitialAd, cmVar, cxVar);
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    @VisibleForTesting
    public es ah() {
        if (this.bb != null) {
            return (es) this.bb.get();
        }
        return null;
    }

    private av(@NonNull InterstitialAd interstitialAd, @NonNull cm cmVar, @NonNull cx cxVar) {
        super(interstitialAd);
        this.ba = cmVar;
        this.section = cxVar;
        this.aV.addAll(cmVar.getStatHolder().ck());
    }

    public void a(@NonNull fj fjVar, @NonNull FrameLayout frameLayout) {
        super.a(fjVar, frameLayout);
        a((ViewGroup) frameLayout, true);
    }

    public void a(boolean z) {
        super.a(z);
        es ah = ah();
        if (ah == null) {
            return;
        }
        if (z) {
            ah.resume();
        } else {
            ah.pause();
        }
    }

    public void C() {
        super.C();
        es ah = ah();
        if (ah != null) {
            ah.destroy();
        }
        if (this.bb != null) {
            this.bb.clear();
            this.bb = null;
        }
    }

    public void onActivityCreate(@NonNull MyTargetActivity myTargetActivity, @NonNull Intent intent, @NonNull FrameLayout frameLayout) {
        super.onActivityCreate(myTargetActivity, intent, frameLayout);
        a((ViewGroup) frameLayout, false);
    }

    public void onActivityPause() {
        super.onActivityPause();
        es ah = ah();
        if (ah != null) {
            ah.pause();
        }
    }

    public void onActivityResume() {
        super.onActivityResume();
        es ah = ah();
        if (ah != null) {
            ah.resume();
        }
    }

    public void onActivityStop() {
        super.onActivityStop();
        es ah = ah();
        if (ah != null) {
            ah.stop();
        }
    }

    public void onActivityDestroy() {
        super.onActivityDestroy();
        if (this.bb != null) {
            es esVar = (es) this.bb.get();
            if (esVar != null) {
                View cI = esVar.cI();
                ViewParent parent = cI.getParent();
                if (parent instanceof ViewGroup) {
                    ((ViewGroup) parent).removeView(cI);
                }
                esVar.destroy();
            }
            this.bb.clear();
            this.bb = null;
        }
    }

    /* access modifiers changed from: protected */
    public boolean af() {
        return this.ba.isAllowBackButton();
    }

    /* access modifiers changed from: 0000 */
    public void b(@NonNull cg cgVar, @Nullable String str, @NonNull Context context) {
        if (ah() != null) {
            hd dM = hd.dM();
            if (TextUtils.isEmpty(str)) {
                dM.b(cgVar, context);
            } else {
                dM.c(cgVar, str, context);
            }
            if (cgVar instanceof cj) {
                hl.a((List<dg>) this.ba.getStatHolder().N("click"), context);
            }
            InterstitialAdListener listener = this.ad.getListener();
            if (listener != null) {
                listener.onClick(this.ad);
            }
            if (this.ba.getVideoBanner() == null && this.ba.isCloseOnClick()) {
                dismiss();
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void ai() {
        InterstitialAdListener listener = this.ad.getListener();
        if (listener != null) {
            listener.onVideoCompleted(this.ad);
        }
        ci endCard = this.ba.getEndCard();
        ViewParent viewParent = null;
        es ah = ah();
        if (ah != null) {
            viewParent = ah.cI().getParent();
        }
        if (endCard != null && (viewParent instanceof ViewGroup)) {
            a(endCard, (ViewGroup) viewParent);
        }
    }

    /* access modifiers changed from: 0000 */
    public void ag() {
        dismiss();
    }

    /* access modifiers changed from: 0000 */
    public void aj() {
        es ah = ah();
        if (ah instanceof et) {
            ((et) ah).cZ();
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(float f, float f2, @NonNull Context context) {
        if (!this.aV.isEmpty()) {
            float f3 = f2 - f;
            ArrayList arrayList = new ArrayList();
            Iterator it = this.aV.iterator();
            while (it.hasNext()) {
                df dfVar = (df) it.next();
                float cf = dfVar.cf();
                if (cf < 0.0f && dfVar.cg() >= 0.0f) {
                    cf = (f2 / 100.0f) * dfVar.cg();
                }
                if (cf >= 0.0f && cf <= f3) {
                    arrayList.add(dfVar);
                    it.remove();
                }
            }
            hl.a((List<dg>) arrayList, context);
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(cg cgVar, @NonNull String str, @NonNull Context context) {
        hl.a((List<dg>) cgVar.getStatHolder().N(str), context);
    }

    /* access modifiers changed from: 0000 */
    public void a(cg cgVar, @NonNull Context context) {
        hl.a((List<dg>) cgVar.getStatHolder().N("playbackStarted"), context);
    }

    private void a(@NonNull ci ciVar, @NonNull ViewGroup viewGroup) {
        ev evVar;
        es ah = ah();
        if (ah != null) {
            ah.destroy();
        }
        if (ciVar instanceof ck) {
            viewGroup.removeAllViews();
            if (CampaignEx.JSON_KEY_MRAID.equals(ciVar.getType())) {
                evVar = er.u(viewGroup.getContext());
            } else {
                evVar = eo.s(viewGroup.getContext());
            }
            this.bb = new WeakReference<>(evVar);
            evVar.a(new a(this));
            evVar.a(this.section, (ck) ciVar);
            viewGroup.addView(evVar.cI(), new LayoutParams(-1, -1));
        } else if (ciVar instanceof cl) {
            viewGroup.removeAllViews();
            ep t = ep.t(viewGroup.getContext());
            this.bb = new WeakReference<>(t);
            t.a(new a(this));
            t.e((cl) ciVar);
            viewGroup.addView(t.cI(), new LayoutParams(-1, -1));
        }
    }

    private void a(@NonNull ViewGroup viewGroup, boolean z) {
        et a2 = et.a(this.ba, this.ad.isUseExoPlayer(), viewGroup.getContext());
        this.bb = new WeakReference<>(a2);
        a2.r(z);
        a2.b(new a(this));
        viewGroup.addView(a2.cI(), new LayoutParams(-1, -1));
        hl.a((List<dg>) this.ba.getStatHolder().N("playbackStarted"), viewGroup.getContext());
        hl.a((List<dg>) this.section.w("impression"), viewGroup.getContext());
    }
}
