package com.my.target;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.my.target.cd;
import com.my.target.common.models.AudioData;
import com.my.target.common.models.VideoData;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* compiled from: MediaSection */
public class cz<T extends cd> extends cu {
    @NonNull
    private final ArrayList<cn<T>> banners = new ArrayList<>();
    @NonNull
    private final ArrayList<bz> dC = new ArrayList<>();
    @NonNull
    private final ArrayList<bz> dD = new ArrayList<>();
    @NonNull
    private final ArrayList<bz> dE = new ArrayList<>();
    private int dF = 10;
    private int dG = -1;
    @NonNull
    private final String name;

    @NonNull
    public static cz<VideoData> z(@NonNull String str) {
        return B(str);
    }

    @NonNull
    public static cz<AudioData> A(@NonNull String str) {
        return B(str);
    }

    @NonNull
    private static <T extends cd> cz<T> B(@NonNull String str) {
        return new cz<>(str);
    }

    private cz(@NonNull String str) {
        this.name = str;
    }

    @NonNull
    public String getName() {
        return this.name;
    }

    public void o(int i) {
        this.dF = i;
    }

    public int bJ() {
        return this.dF;
    }

    public int bK() {
        return this.dG;
    }

    public void p(int i) {
        this.dG = i;
    }

    public void g(@NonNull cn<T> cnVar) {
        this.banners.add(cnVar);
    }

    public void a(@NonNull cn<T> cnVar, int i) {
        int size = this.banners.size();
        if (i >= 0 && i <= size) {
            this.banners.add(i, cnVar);
            Iterator it = this.dE.iterator();
            while (it.hasNext()) {
                bz bzVar = (bz) it.next();
                int position = bzVar.getPosition();
                if (position >= i) {
                    bzVar.setPosition(position + 1);
                }
            }
        }
    }

    @NonNull
    public List<cn<T>> bI() {
        return new ArrayList(this.banners);
    }

    @NonNull
    public ArrayList<bz> bL() {
        return new ArrayList<>(this.dD);
    }

    public int getBannersCount() {
        return this.banners.size();
    }

    @Nullable
    public bz bM() {
        if (this.dC.size() > 0) {
            return (bz) this.dC.remove(0);
        }
        return null;
    }

    @NonNull
    public ArrayList<bz> f(float f) {
        ArrayList<bz> arrayList = new ArrayList<>();
        Iterator it = this.dD.iterator();
        while (it.hasNext()) {
            bz bzVar = (bz) it.next();
            if (bzVar.getPoint() == f) {
                arrayList.add(bzVar);
            }
        }
        if (arrayList.size() > 0) {
            this.dD.removeAll(arrayList);
        }
        return arrayList;
    }

    public void c(@NonNull bz bzVar) {
        if (bzVar.aV()) {
            this.dD.add(bzVar);
        } else if (bzVar.aT()) {
            this.dC.add(bzVar);
        } else {
            this.dE.add(bzVar);
        }
    }

    public void bN() {
        this.dE.clear();
    }

    public void b(@NonNull cz<T> czVar) {
        this.banners.addAll(czVar.banners);
        this.dC.addAll(czVar.dC);
        this.dD.addAll(czVar.dD);
        d(czVar.aZ());
    }

    public boolean bO() {
        return !this.dD.isEmpty() || !this.dC.isEmpty();
    }
}
