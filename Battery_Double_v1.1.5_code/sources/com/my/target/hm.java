package com.my.target;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.GradientDrawable.Orientation;
import android.graphics.drawable.RippleDrawable;
import android.graphics.drawable.StateListDrawable;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.util.StateSet;
import android.util.TypedValue;
import android.view.View;
import android.view.View.AccessibilityDelegate;
import android.view.Window;
import android.view.accessibility.AccessibilityNodeInfo;
import java.util.concurrent.atomic.AtomicInteger;

/* compiled from: UiUtils */
public class hm {
    @NonNull
    private static final AtomicInteger my = new AtomicInteger(1);
    @NonNull
    private final Context context;

    /* compiled from: UiUtils */
    static class a extends AccessibilityDelegate {
        @NonNull
        private final String mz;

        a(@NonNull String str) {
            this.mz = str;
        }

        @TargetApi(18)
        public void onInitializeAccessibilityNodeInfo(View view, AccessibilityNodeInfo accessibilityNodeInfo) {
            super.onInitializeAccessibilityNodeInfo(view, accessibilityNodeInfo);
            accessibilityNodeInfo.setViewIdResourceName(this.mz);
        }
    }

    public static int a(int i, @NonNull Context context2) {
        return (int) TypedValue.applyDimension(1, (float) i, context2.getResources().getDisplayMetrics());
    }

    public static int D(int i) {
        float[] fArr = new float[3];
        Color.colorToHSV(i, fArr);
        fArr[2] = fArr[2] * 0.7f;
        return Color.HSVToColor(fArr);
    }

    public static void a(@NonNull View view, int i, int i2) {
        ColorDrawable colorDrawable = new ColorDrawable(i);
        ColorDrawable colorDrawable2 = new ColorDrawable(i2);
        StateListDrawable stateListDrawable = new StateListDrawable();
        stateListDrawable.addState(new int[]{16842919}, colorDrawable2);
        stateListDrawable.addState(StateSet.WILD_CARD, colorDrawable);
        if (VERSION.SDK_INT >= 21) {
            view.setBackground(new RippleDrawable(new ColorStateList(new int[][]{new int[]{16842919}, StateSet.WILD_CARD}, new int[]{D(i2), D(i)}), stateListDrawable, null));
        } else if (VERSION.SDK_INT >= 18) {
            view.setBackground(stateListDrawable);
        } else {
            view.setBackgroundDrawable(stateListDrawable);
        }
    }

    public static void a(@NonNull View view, int i, int i2, int i3) {
        a(view, i, i2, 0, 0, i3);
    }

    public static void a(@NonNull View view, int i, int i2, int i3, int i4, int i5) {
        GradientDrawable gradientDrawable = new GradientDrawable(Orientation.TOP_BOTTOM, new int[]{i, i});
        float f = (float) i5;
        gradientDrawable.setCornerRadius(f);
        GradientDrawable gradientDrawable2 = new GradientDrawable(Orientation.TOP_BOTTOM, new int[]{i2, i2});
        gradientDrawable2.setCornerRadius(f);
        if (i3 != 0) {
            gradientDrawable.setStroke(i4, i3);
            gradientDrawable2.setStroke(i4, i3);
        }
        StateListDrawable stateListDrawable = new StateListDrawable();
        stateListDrawable.addState(new int[]{16842919}, gradientDrawable2);
        stateListDrawable.addState(StateSet.WILD_CARD, gradientDrawable);
        if (VERSION.SDK_INT >= 21) {
            view.setBackground(new RippleDrawable(new ColorStateList(new int[][]{new int[]{16842919}, StateSet.WILD_CARD}, new int[]{D(i2), D(i)}), stateListDrawable, null));
        } else if (VERSION.SDK_INT >= 16) {
            view.setBackground(stateListDrawable);
        } else {
            view.setBackgroundDrawable(stateListDrawable);
        }
    }

    public static int dV() {
        int i;
        int i2;
        if (VERSION.SDK_INT >= 17) {
            return View.generateViewId();
        }
        do {
            i = my.get();
            i2 = i + 1;
            if (i2 > 16777215) {
                i2 = 1;
            }
        } while (!my.compareAndSet(i, i2));
        return i;
    }

    @NonNull
    public static hm R(@NonNull Context context2) {
        return new hm(context2);
    }

    public static void a(@NonNull View view, @NonNull String str) {
        if (VERSION.SDK_INT >= 18) {
            view.setAccessibilityDelegate(new a(str));
        }
    }

    public static boolean a(@NonNull Activity activity, @NonNull View view) {
        while (true) {
            boolean z = false;
            if (!view.isHardwareAccelerated() || (view.getLayerType() & 1) != 0) {
                return false;
            }
            if (!(view.getParent() instanceof View)) {
                Window window = activity.getWindow();
                if (window == null) {
                    return false;
                }
                if ((window.getAttributes().flags & 16777216) != 0) {
                    z = true;
                }
                return z;
            }
            view = (View) view.getParent();
        }
        return false;
    }

    private hm(@NonNull Context context2) {
        this.context = context2;
    }

    public int E(int i) {
        return (int) TypedValue.applyDimension(1, (float) i, this.context.getResources().getDisplayMetrics());
    }

    public int F(int i) {
        return (int) TypedValue.applyDimension(2, (float) i, this.context.getResources().getDisplayMetrics());
    }

    public static int a(@NonNull Activity activity) {
        int rotation = activity.getWindowManager().getDefaultDisplay().getRotation();
        int i = activity.getResources().getConfiguration().orientation;
        if (1 == i) {
            switch (rotation) {
                case 1:
                case 2:
                    return 9;
                default:
                    return 1;
            }
        } else if (2 == i) {
            switch (rotation) {
                case 2:
                case 3:
                    return 8;
                default:
                    return 0;
            }
        } else {
            ah.a("Unknown screen orientation. Defaulting to portrait.");
            return 9;
        }
    }

    public int G(int i) {
        return Math.round(((float) i) / (((float) this.context.getResources().getDisplayMetrics().densityDpi) / 160.0f));
    }
}
