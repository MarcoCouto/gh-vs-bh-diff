package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import com.my.target.common.models.ImageData;
import org.json.JSONObject;

/* compiled from: InterstitialSliderAdBannerParser */
public class eb {
    @NonNull
    private final cy be;
    @NonNull
    private final dv er;
    @NonNull
    private final ea es;

    @NonNull
    public static eb a(@NonNull cy cyVar, @NonNull bz bzVar, @NonNull a aVar, @NonNull Context context) {
        return new eb(cyVar, bzVar, aVar, context);
    }

    private eb(@NonNull cy cyVar, @NonNull bz bzVar, @NonNull a aVar, @NonNull Context context) {
        this.be = cyVar;
        this.er = dv.b(bzVar, aVar, context);
        this.es = ea.e(bzVar, aVar, context);
    }

    public boolean a(@NonNull JSONObject jSONObject, @NonNull cl clVar) {
        b(jSONObject, clVar);
        return this.es.b(jSONObject, clVar);
    }

    private void b(@NonNull JSONObject jSONObject, @NonNull ci ciVar) {
        this.er.a(jSONObject, (cg) ciVar);
        ciVar.setAllowClose(true);
        String optString = jSONObject.optString("close_icon_hd");
        if (!TextUtils.isEmpty(optString)) {
            ciVar.setCloseIcon(ImageData.newImageData(optString));
        } else {
            ciVar.setCloseIcon(this.be.getCloseIcon());
        }
    }
}
