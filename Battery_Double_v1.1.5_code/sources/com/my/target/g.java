package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.my.target.common.models.VideoData;
import com.yandex.mobile.ads.video.models.vmap.AdBreak.BreakId;
import java.util.ArrayList;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONObject;

/* compiled from: InstreamAdResponseParser */
public class g extends c<cv> {
    @NonNull
    public static c<cv> f() {
        return new g();
    }

    private g() {
    }

    @Nullable
    public cv a(@NonNull String str, @NonNull bz bzVar, @Nullable cv cvVar, @NonNull a aVar, @NonNull Context context) {
        if (isVast(str)) {
            return b(str, bzVar, cvVar, aVar, context);
        }
        return c(str, bzVar, cvVar, aVar, context);
    }

    @NonNull
    private cv b(@NonNull String str, @NonNull bz bzVar, @Nullable cv cvVar, @NonNull a aVar, @NonNull Context context) {
        em a2 = em.a(aVar, bzVar, context);
        a2.V(str);
        String bb = bzVar.bb();
        if (bb == null) {
            bb = BreakId.PREROLL;
        }
        if (cvVar == null) {
            cvVar = cv.by();
        }
        cz x = cvVar.x(bb);
        if (x == null) {
            return cvVar;
        }
        if (!a2.cD().isEmpty()) {
            a(a2, x, bzVar);
        } else {
            bz cE = a2.cE();
            if (cE != null) {
                cE.s(x.getName());
                int position = bzVar.getPosition();
                if (position >= 0) {
                    cE.setPosition(position);
                } else {
                    cE.setPosition(x.getBannersCount());
                }
                x.c(cE);
            }
        }
        return cvVar;
    }

    private void a(@NonNull em<VideoData> emVar, @NonNull cz<VideoData> czVar, @NonNull bz bzVar) {
        czVar.d(emVar.aZ());
        int position = bzVar.getPosition();
        Iterator it = emVar.cD().iterator();
        while (it.hasNext()) {
            cn cnVar = (cn) it.next();
            Boolean bc = bzVar.bc();
            if (bc != null) {
                cnVar.setAllowClose(bc.booleanValue());
            }
            float allowCloseDelay = bzVar.getAllowCloseDelay();
            if (allowCloseDelay > 0.0f) {
                cnVar.setAllowCloseDelay(allowCloseDelay);
            }
            Boolean bd = bzVar.bd();
            if (bd != null) {
                cnVar.setAllowPause(bd.booleanValue());
            }
            Boolean bi = bzVar.bi();
            if (bi != null) {
                cnVar.setDirectLink(bi.booleanValue());
            }
            Boolean bj = bzVar.bj();
            if (bj != null) {
                cnVar.setOpenInBrowser(bj.booleanValue());
            }
            cnVar.setCloseActionText("Close");
            cnVar.setPoint(bzVar.getPoint());
            cnVar.setPointP(bzVar.getPointP());
            if (position >= 0) {
                int i = position + 1;
                czVar.a(cnVar, position);
                position = i;
            } else {
                czVar.g(cnVar);
            }
        }
    }

    @Nullable
    private cv c(@NonNull String str, @NonNull bz bzVar, @Nullable cv cvVar, @NonNull a aVar, @NonNull Context context) {
        JSONObject a2 = a(str, context);
        if (a2 == null) {
            return cvVar;
        }
        JSONObject optJSONObject = a2.optJSONObject(aVar.getFormat());
        if (optJSONObject == null) {
            return cvVar;
        }
        if (cvVar == null) {
            cvVar = cv.by();
        }
        dx.cA().a(optJSONObject, cvVar);
        dt a3 = dt.a(bzVar, aVar, context);
        JSONObject optJSONObject2 = optJSONObject.optJSONObject("sections");
        if (optJSONObject2 != null) {
            String bb = bzVar.bb();
            if (bb != null) {
                cz x = cvVar.x(bb);
                if (x != null) {
                    a(optJSONObject2, a3, x, dw.c(bzVar, aVar, context), bzVar);
                }
            } else {
                Iterator it = cvVar.bz().iterator();
                while (it.hasNext()) {
                    a(optJSONObject2, a3, (cz) it.next(), dw.c(bzVar, aVar, context), bzVar);
                }
            }
        }
        return cvVar;
    }

    private void a(@NonNull JSONObject jSONObject, @NonNull dt dtVar, @NonNull cz<VideoData> czVar, @NonNull dw dwVar, @NonNull bz bzVar) {
        cz<VideoData> czVar2 = czVar;
        JSONObject jSONObject2 = jSONObject;
        JSONArray optJSONArray = jSONObject.optJSONArray(czVar.getName());
        if (optJSONArray != null) {
            int position = bzVar.getPosition();
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            int i = position;
            for (int i2 = 0; i2 < optJSONArray.length(); i2++) {
                JSONObject optJSONObject = optJSONArray.optJSONObject(i2);
                if (optJSONObject != null) {
                    if ("additionalData".equals(optJSONObject.optString("type"))) {
                        a(bzVar, dtVar, optJSONObject, czVar, arrayList2, arrayList);
                    } else {
                        cn newVideoBanner = cn.newVideoBanner();
                        if (dwVar.a(optJSONObject, newVideoBanner)) {
                            float point = bzVar.getPoint();
                            if (point >= 0.0f) {
                                newVideoBanner.setPoint(point);
                            }
                            float pointP = bzVar.getPointP();
                            if (pointP >= 0.0f) {
                                newVideoBanner.setPointP(pointP);
                            }
                            if (i >= 0) {
                                int i3 = i + 1;
                                czVar2.a(newVideoBanner, i);
                                i = i3;
                            } else {
                                czVar2.g(newVideoBanner);
                            }
                        }
                    }
                }
                dw dwVar2 = dwVar;
            }
            a(arrayList2, arrayList);
        }
    }

    private void a(@NonNull bz bzVar, @NonNull dt dtVar, @NonNull JSONObject jSONObject, @NonNull cz czVar, @NonNull ArrayList<bz> arrayList, @NonNull ArrayList<bz> arrayList2) {
        bz d = dtVar.d(jSONObject);
        if (d != null) {
            d.s(czVar.getName());
            if (d.aU() != -1) {
                arrayList2.add(d);
                return;
            }
            arrayList.add(d);
            if (!d.aV() && !d.aT()) {
                bzVar.b(d);
                int position = bzVar.getPosition();
                if (position >= 0) {
                    d.setPosition(position);
                } else {
                    d.setPosition(czVar.getBannersCount());
                }
            }
            czVar.c(d);
        }
    }

    private void a(@NonNull ArrayList<bz> arrayList, @NonNull ArrayList<bz> arrayList2) {
        Iterator it = arrayList2.iterator();
        while (it.hasNext()) {
            bz bzVar = (bz) it.next();
            Iterator it2 = arrayList.iterator();
            while (true) {
                if (!it2.hasNext()) {
                    break;
                }
                bz bzVar2 = (bz) it2.next();
                if (bzVar.aU() == bzVar2.getId()) {
                    bzVar2.a(bzVar);
                    break;
                }
            }
        }
    }
}
