package com.my.target;

import android.content.Context;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.LayoutParams;
import android.support.v7.widget.RecyclerView.State;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import java.util.List;

/* compiled from: PromoCardImageRecyclerView */
public class gg extends RecyclerView implements gh {
    @NonNull
    private final OnClickListener cardClickListener;
    /* access modifiers changed from: private */
    @Nullable
    public List<cp> cards;
    /* access modifiers changed from: private */
    @NonNull
    public final b ju;
    @NonNull
    private final gf jv;
    /* access modifiers changed from: private */
    @Nullable
    public com.my.target.gh.a jw;
    /* access modifiers changed from: private */
    public boolean moving;

    /* compiled from: PromoCardImageRecyclerView */
    class a implements OnClickListener {
        private a() {
        }

        public void onClick(View view) {
            if (!gg.this.moving && gg.this.isClickable()) {
                View findContainingItemView = gg.this.ju.findContainingItemView(view);
                if (!(findContainingItemView == null || gg.this.jw == null || gg.this.cards == null)) {
                    gg.this.jw.b(findContainingItemView, gg.this.ju.getPosition(findContainingItemView));
                }
            }
        }
    }

    /* compiled from: PromoCardImageRecyclerView */
    static class b extends LinearLayoutManager {
        private int dividerPadding;
        @Nullable
        private com.my.target.fq.a jy;

        public b(Context context) {
            super(context, 0, false);
        }

        public void measureChildWithMargins(View view, int i, int i2) {
            LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
            int width = getWidth();
            if (getHeight() > 0 && width > 0) {
                if (getItemViewType(view) == 1) {
                    layoutParams.rightMargin = this.dividerPadding;
                } else if (getItemViewType(view) == 2) {
                    layoutParams.leftMargin = this.dividerPadding;
                } else {
                    layoutParams.leftMargin = this.dividerPadding;
                    layoutParams.rightMargin = this.dividerPadding;
                }
                super.measureChildWithMargins(view, i, i2);
            }
        }

        public void a(@Nullable com.my.target.fq.a aVar) {
            this.jy = aVar;
        }

        public void onLayoutCompleted(State state) {
            super.onLayoutCompleted(state);
            if (this.jy != null) {
                this.jy.m612do();
            }
        }

        public void setDividerPadding(int i) {
            this.dividerPadding = i;
        }
    }

    public gg(@NonNull Context context) {
        this(context, null);
    }

    public gg(@NonNull Context context, @Nullable AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public gg(@NonNull Context context, @Nullable AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.cardClickListener = new a();
        this.ju = new b(context);
        this.ju.setDividerPadding(hm.a(4, context));
        this.jv = new gf(getContext());
        setHasFixedSize(true);
    }

    public void setupCards(@NonNull List<cp> list) {
        this.cards = list;
        this.jv.setCards(list);
        if (isClickable()) {
            this.jv.b(this.cardClickListener);
        }
        setCardLayoutManager(this.ju);
        swapAdapter(this.jv, true);
    }

    public void onScrollStateChanged(int i) {
        super.onScrollStateChanged(i);
        this.moving = i != 0;
        if (!this.moving) {
            checkCardChanged();
        }
    }

    @NonNull
    public int[] getVisibleCardNumbers() {
        int findFirstCompletelyVisibleItemPosition = this.ju.findFirstCompletelyVisibleItemPosition();
        int findLastCompletelyVisibleItemPosition = this.ju.findLastCompletelyVisibleItemPosition();
        if (this.cards == null || findFirstCompletelyVisibleItemPosition > findLastCompletelyVisibleItemPosition || findFirstCompletelyVisibleItemPosition < 0 || findLastCompletelyVisibleItemPosition >= this.cards.size()) {
            return new int[0];
        }
        int[] iArr = new int[((findLastCompletelyVisibleItemPosition - findFirstCompletelyVisibleItemPosition) + 1)];
        for (int i = 0; i < iArr.length; i++) {
            iArr[i] = findFirstCompletelyVisibleItemPosition;
            findFirstCompletelyVisibleItemPosition++;
        }
        return iArr;
    }

    public Parcelable getState() {
        return this.ju.onSaveInstanceState();
    }

    public void restoreState(@NonNull Parcelable parcelable) {
        this.ju.onRestoreInstanceState(parcelable);
    }

    public void setPromoCardSliderListener(@Nullable com.my.target.gh.a aVar) {
        this.jw = aVar;
    }

    public void dispose() {
        this.jv.dispose();
    }

    /* access modifiers changed from: private */
    public void checkCardChanged() {
        if (this.jw != null) {
            this.jw.b((View) this, getVisibleCardNumbers());
        }
    }

    private void setCardLayoutManager(b bVar) {
        bVar.a(new com.my.target.fq.a() {
            /* renamed from: do reason: not valid java name */
            public void m613do() {
                gg.this.checkCardChanged();
            }
        });
        super.setLayoutManager(bVar);
    }
}
