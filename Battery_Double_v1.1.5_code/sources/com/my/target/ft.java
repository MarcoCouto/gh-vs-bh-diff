package com.my.target;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;

/* compiled from: FramedImageView */
public class ft extends FrameLayout {
    @NonNull
    private final ImageView hM;

    public ft(@NonNull Context context) {
        super(context);
        this.hM = new ImageView(context);
        LayoutParams layoutParams = new LayoutParams(-2, -2);
        layoutParams.gravity = 17;
        addView(this.hM, layoutParams);
    }

    public void setImageBitmap(@Nullable Bitmap bitmap) {
        this.hM.setImageBitmap(bitmap);
    }
}
