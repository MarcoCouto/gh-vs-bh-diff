package com.my.target;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.telephony.CellIdentityCdma;
import android.telephony.CellIdentityGsm;
import android.telephony.CellIdentityLte;
import android.telephony.CellIdentityWcdma;
import android.telephony.CellInfo;
import android.telephony.CellInfoCdma;
import android.telephony.CellInfoGsm;
import android.telephony.CellInfoLte;
import android.telephony.CellInfoWcdma;
import android.telephony.CellLocation;
import android.telephony.CellSignalStrengthCdma;
import android.telephony.CellSignalStrengthGsm;
import android.telephony.CellSignalStrengthLte;
import android.telephony.CellSignalStrengthWcdma;
import android.telephony.TelephonyManager;
import android.telephony.gsm.GsmCellLocation;
import android.text.TextUtils;
import com.facebook.places.model.PlaceFields;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/* compiled from: EnvironmentParamsDataProvider */
public final class fa extends fb {
    private boolean gu = true;
    private boolean gv = true;

    /* compiled from: EnvironmentParamsDataProvider */
    static class a {
        @Nullable
        private String gw;
        /* access modifiers changed from: private */
        @Nullable
        public ArrayList<b> gx = null;

        a(@NonNull Context context) {
            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(PlaceFields.PHONE);
            if (telephonyManager != null) {
                try {
                    if (VERSION.SDK_INT >= 17 && fa.h("android.permission.ACCESS_COARSE_LOCATION", context)) {
                        this.gx = b(telephonyManager);
                    }
                    if ((this.gx == null || this.gx.isEmpty()) && (fa.h("android.permission.ACCESS_FINE_LOCATION", context) || fa.h("android.permission.ACCESS_COARSE_LOCATION", context))) {
                        this.gx = a(telephonyManager);
                    }
                } catch (Exception e) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("Environment provider exception ");
                    sb.append(e.getMessage());
                    ah.a(sb.toString());
                }
            }
        }

        @Nullable
        @SuppressLint({"MissingPermission"})
        private ArrayList<b> a(@NonNull TelephonyManager telephonyManager) {
            CellLocation cellLocation = telephonyManager.getCellLocation();
            if (cellLocation == null || !(cellLocation instanceof GsmCellLocation)) {
                return null;
            }
            ArrayList<b> arrayList = new ArrayList<>();
            GsmCellLocation gsmCellLocation = (GsmCellLocation) cellLocation;
            b bVar = new b("gsm");
            arrayList.add(bVar);
            bVar.gy = gsmCellLocation.getCid();
            bVar.gz = gsmCellLocation.getLac();
            this.gw = telephonyManager.getNetworkOperator();
            if (!TextUtils.isEmpty(this.gw)) {
                try {
                    bVar.mcc = Integer.parseInt(this.gw.substring(0, 3));
                    bVar.mnc = Integer.parseInt(this.gw.substring(3));
                } catch (Exception unused) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("unable to substring network operator ");
                    sb.append(this.gw);
                    ah.a(sb.toString());
                }
            }
            StringBuilder sb2 = new StringBuilder();
            sb2.append("current cell: ");
            sb2.append(bVar.gy);
            sb2.append(",");
            sb2.append(bVar.gz);
            sb2.append(",");
            sb2.append(bVar.mcc);
            sb2.append(",");
            sb2.append(bVar.mnc);
            ah.a(sb2.toString());
            return arrayList;
        }

        @SuppressLint({"MissingPermission"})
        @Nullable
        @TargetApi(17)
        private ArrayList<b> b(@NonNull TelephonyManager telephonyManager) {
            b bVar;
            List<CellInfo> allCellInfo = telephonyManager.getAllCellInfo();
            if (allCellInfo == null) {
                return null;
            }
            ArrayList<b> arrayList = new ArrayList<>();
            for (CellInfo cellInfo : allCellInfo) {
                if (cellInfo.isRegistered()) {
                    if (cellInfo instanceof CellInfoLte) {
                        bVar = new b("lte");
                        CellInfoLte cellInfoLte = (CellInfoLte) cellInfo;
                        CellIdentityLte cellIdentity = cellInfoLte.getCellIdentity();
                        CellSignalStrengthLte cellSignalStrength = cellInfoLte.getCellSignalStrength();
                        bVar.gy = cellIdentity.getCi();
                        bVar.gz = Integer.MAX_VALUE;
                        bVar.mcc = cellIdentity.getMcc();
                        bVar.mnc = cellIdentity.getMnc();
                        bVar.level = cellSignalStrength.getLevel();
                        bVar.gA = cellSignalStrength.getDbm();
                        bVar.gB = cellSignalStrength.getAsuLevel();
                        bVar.gC = cellSignalStrength.getTimingAdvance();
                        if (VERSION.SDK_INT >= 24) {
                            bVar.gD = cellIdentity.getEarfcn();
                        }
                        bVar.gE = Integer.MAX_VALUE;
                        bVar.gF = Integer.MAX_VALUE;
                        bVar.gG = cellIdentity.getTac();
                    } else if (cellInfo instanceof CellInfoGsm) {
                        bVar = new b("gsm");
                        CellInfoGsm cellInfoGsm = (CellInfoGsm) cellInfo;
                        CellIdentityGsm cellIdentity2 = cellInfoGsm.getCellIdentity();
                        CellSignalStrengthGsm cellSignalStrength2 = cellInfoGsm.getCellSignalStrength();
                        bVar.gy = cellIdentity2.getCid();
                        bVar.gz = cellIdentity2.getLac();
                        bVar.mcc = cellIdentity2.getMcc();
                        bVar.mnc = cellIdentity2.getMnc();
                        bVar.level = cellSignalStrength2.getLevel();
                        bVar.gA = cellSignalStrength2.getDbm();
                        bVar.gB = cellSignalStrength2.getAsuLevel();
                        if (VERSION.SDK_INT >= 26) {
                            bVar.gC = cellSignalStrength2.getTimingAdvance();
                        } else {
                            bVar.gC = Integer.MAX_VALUE;
                        }
                        bVar.gD = Integer.MAX_VALUE;
                        if (VERSION.SDK_INT >= 24) {
                            bVar.gE = cellIdentity2.getBsic();
                        }
                        bVar.gF = cellIdentity2.getPsc();
                        bVar.gG = Integer.MAX_VALUE;
                    } else if (VERSION.SDK_INT >= 18 && (cellInfo instanceof CellInfoWcdma)) {
                        bVar = new b("wcdma");
                        CellInfoWcdma cellInfoWcdma = (CellInfoWcdma) cellInfo;
                        CellIdentityWcdma cellIdentity3 = cellInfoWcdma.getCellIdentity();
                        CellSignalStrengthWcdma cellSignalStrength3 = cellInfoWcdma.getCellSignalStrength();
                        bVar.gy = cellIdentity3.getCid();
                        bVar.gz = cellIdentity3.getLac();
                        bVar.mcc = cellIdentity3.getMcc();
                        bVar.mnc = cellIdentity3.getMnc();
                        bVar.level = cellSignalStrength3.getLevel();
                        bVar.gA = cellSignalStrength3.getDbm();
                        bVar.gB = cellSignalStrength3.getAsuLevel();
                        bVar.gC = Integer.MAX_VALUE;
                        if (VERSION.SDK_INT >= 24) {
                            bVar.gD = cellIdentity3.getUarfcn();
                        }
                        bVar.gE = Integer.MAX_VALUE;
                        bVar.gF = cellIdentity3.getPsc();
                        bVar.gG = Integer.MAX_VALUE;
                    } else if (cellInfo instanceof CellInfoCdma) {
                        bVar = new b("cdma");
                        CellInfoCdma cellInfoCdma = (CellInfoCdma) cellInfo;
                        CellIdentityCdma cellIdentity4 = cellInfoCdma.getCellIdentity();
                        CellSignalStrengthCdma cellSignalStrength4 = cellInfoCdma.getCellSignalStrength();
                        bVar.gH = cellIdentity4.getNetworkId();
                        bVar.gI = cellIdentity4.getSystemId();
                        bVar.gJ = cellIdentity4.getBasestationId();
                        bVar.gK = cellIdentity4.getLatitude();
                        bVar.gL = cellIdentity4.getLongitude();
                        bVar.gM = cellSignalStrength4.getCdmaLevel();
                        bVar.level = cellSignalStrength4.getLevel();
                        bVar.gN = cellSignalStrength4.getEvdoLevel();
                        bVar.gB = cellSignalStrength4.getAsuLevel();
                        bVar.gO = cellSignalStrength4.getCdmaDbm();
                        bVar.gA = cellSignalStrength4.getDbm();
                        bVar.gP = cellSignalStrength4.getEvdoDbm();
                        bVar.gQ = cellSignalStrength4.getEvdoEcio();
                        bVar.gR = cellSignalStrength4.getCdmaEcio();
                        bVar.gS = cellSignalStrength4.getEvdoSnr();
                    }
                    arrayList.add(bVar);
                }
            }
            return arrayList;
        }
    }

    /* compiled from: EnvironmentParamsDataProvider */
    static class b {
        int gA = Integer.MAX_VALUE;
        int gB = Integer.MAX_VALUE;
        int gC = Integer.MAX_VALUE;
        int gD = Integer.MAX_VALUE;
        int gE = Integer.MAX_VALUE;
        int gF = Integer.MAX_VALUE;
        int gG = Integer.MAX_VALUE;
        int gH = Integer.MAX_VALUE;
        int gI = Integer.MAX_VALUE;
        int gJ = Integer.MAX_VALUE;
        int gK = Integer.MAX_VALUE;
        int gL = Integer.MAX_VALUE;
        int gM = Integer.MAX_VALUE;
        int gN = Integer.MAX_VALUE;
        int gO = Integer.MAX_VALUE;
        int gP = Integer.MAX_VALUE;
        int gQ = Integer.MAX_VALUE;
        int gR = Integer.MAX_VALUE;
        int gS = Integer.MAX_VALUE;
        int gy = Integer.MAX_VALUE;
        int gz = Integer.MAX_VALUE;
        int level = Integer.MAX_VALUE;
        int mcc = Integer.MAX_VALUE;
        int mnc = Integer.MAX_VALUE;
        public final String type;

        b(String str) {
            this.type = str;
        }
    }

    /* compiled from: EnvironmentParamsDataProvider */
    static class c {
        WifiInfo gT;
        List<ScanResult> gU;

        @SuppressLint({"MissingPermission"})
        c(@NonNull Context context) {
            try {
                WifiManager wifiManager = (WifiManager) context.getApplicationContext().getSystemService("wifi");
                if (wifiManager != null && wifiManager.isWifiEnabled()) {
                    this.gT = wifiManager.getConnectionInfo();
                    if (VERSION.SDK_INT < 24 || fa.A(context)) {
                        this.gU = wifiManager.getScanResults();
                    }
                    if (this.gU != null) {
                        Collections.sort(this.gU, new Comparator<ScanResult>() {
                            /* renamed from: a */
                            public int compare(ScanResult scanResult, ScanResult scanResult2) {
                                if (scanResult.level < scanResult2.level) {
                                    return 1;
                                }
                                return scanResult.level > scanResult2.level ? -1 : 0;
                            }
                        });
                    }
                }
            } catch (SecurityException unused) {
                ah.a("No permissions for access to wifi state");
            }
        }
    }

    /* access modifiers changed from: private */
    public static boolean A(@NonNull Context context) {
        return h("android.permission.ACCESS_FINE_LOCATION", context) || h("android.permission.ACCESS_COARSE_LOCATION", context);
    }

    /* access modifiers changed from: private */
    public static boolean h(@NonNull String str, @NonNull Context context) {
        int i;
        try {
            i = context.checkCallingOrSelfPermission(str);
        } catch (Throwable th) {
            StringBuilder sb = new StringBuilder();
            sb.append("unable to check ");
            sb.append(str);
            sb.append(" permission! Unexpected throwable in Context.checkCallingOrSelfPermission() method: ");
            sb.append(th.getMessage());
            ah.a(sb.toString());
            i = -1;
        }
        return i == 0;
    }

    public void u(boolean z) {
        this.gv = z;
    }

    public void v(boolean z) {
        this.gu = z;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:26:0x003a, code lost:
        return;
     */
    public synchronized void collectData(@NonNull Context context) {
        removeAll();
        if (this.gu) {
            if (h("android.permission.ACCESS_FINE_LOCATION", context) || h("android.permission.ACCESS_COARSE_LOCATION", context)) {
                B(context);
            }
            if (this.gv) {
                if (h("android.permission.ACCESS_WIFI_STATE", context)) {
                    C(context);
                }
                if (h("android.permission.ACCESS_COARSE_LOCATION", context)) {
                    D(context);
                }
            }
        }
    }

    @SuppressLint({"MissingPermission"})
    private void B(@NonNull Context context) {
        LocationManager locationManager = (LocationManager) context.getSystemService("location");
        if (locationManager != null) {
            Location location = null;
            long j = 0;
            String str = null;
            float f = Float.MAX_VALUE;
            for (String str2 : locationManager.getAllProviders()) {
                try {
                    Location lastKnownLocation = locationManager.getLastKnownLocation(str2);
                    if (lastKnownLocation != null) {
                        StringBuilder sb = new StringBuilder();
                        sb.append("locationProvider: ");
                        sb.append(str2);
                        ah.a(sb.toString());
                        float accuracy = lastKnownLocation.getAccuracy();
                        long time = lastKnownLocation.getTime();
                        if (location == null || (time > j && accuracy < f)) {
                            str = str2;
                            location = lastKnownLocation;
                            f = accuracy;
                            j = time;
                        }
                    }
                } catch (SecurityException unused) {
                    ah.a("No permissions for get geo data");
                }
            }
            if (location != null) {
                StringBuilder sb2 = new StringBuilder();
                sb2.append(location.getLatitude());
                sb2.append(",");
                sb2.append(location.getLongitude());
                sb2.append(",");
                sb2.append(location.getAccuracy());
                sb2.append(",");
                sb2.append(location.getSpeed());
                sb2.append(",");
                long j2 = j / 1000;
                sb2.append(j2);
                addParam("location", sb2.toString());
                addParam("location_provider", str);
                StringBuilder sb3 = new StringBuilder();
                sb3.append("location: ");
                sb3.append(location.getLatitude());
                sb3.append(", ");
                sb3.append(location.getLongitude());
                sb3.append(" accuracy = ");
                sb3.append(location.getAccuracy());
                sb3.append(" speed = ");
                sb3.append(location.getSpeed());
                sb3.append(" time = ");
                sb3.append(j2);
                sb3.append("  provider: ");
                sb3.append(str);
                ah.a(sb3.toString());
            }
        }
    }

    @SuppressLint({"HardwareIds"})
    private void C(@NonNull Context context) {
        c cVar = new c(context);
        if (cVar.gT != null) {
            WifiInfo wifiInfo = cVar.gT;
            String bssid = wifiInfo.getBSSID();
            if (bssid == null) {
                bssid = "";
            }
            int linkSpeed = wifiInfo.getLinkSpeed();
            int networkId = wifiInfo.getNetworkId();
            int rssi = wifiInfo.getRssi();
            String ssid = wifiInfo.getSSID();
            if (ssid == null) {
                ssid = "";
            }
            StringBuilder sb = new StringBuilder();
            sb.append(bssid);
            sb.append(",");
            sb.append(ssid);
            sb.append(",");
            sb.append(rssi);
            sb.append(",");
            sb.append(networkId);
            sb.append(",");
            sb.append(linkSpeed);
            addParam("wifi", sb.toString());
            StringBuilder sb2 = new StringBuilder();
            sb2.append("mac: ");
            sb2.append(wifiInfo.getMacAddress());
            ah.a(sb2.toString());
            StringBuilder sb3 = new StringBuilder();
            sb3.append("ip: ");
            sb3.append(wifiInfo.getIpAddress());
            ah.a(sb3.toString());
            StringBuilder sb4 = new StringBuilder();
            sb4.append("wifi: ");
            sb4.append(bssid);
            sb4.append(",");
            sb4.append(ssid);
            sb4.append(",");
            sb4.append(rssi);
            sb4.append(",");
            sb4.append(networkId);
            sb4.append(",");
            sb4.append(linkSpeed);
            ah.a(sb4.toString());
        }
        if (cVar.gU != null) {
            int i = 1;
            for (ScanResult scanResult : cVar.gU) {
                if (i < 6) {
                    StringBuilder sb5 = new StringBuilder();
                    sb5.append(scanResult.level);
                    sb5.append("");
                    ah.a(sb5.toString());
                    String str = scanResult.BSSID;
                    if (str == null) {
                        str = "";
                    }
                    String str2 = scanResult.SSID;
                    if (str2 == null) {
                        str2 = "";
                    }
                    StringBuilder sb6 = new StringBuilder();
                    sb6.append("wifi");
                    sb6.append(i);
                    String sb7 = sb6.toString();
                    StringBuilder sb8 = new StringBuilder();
                    sb8.append(str);
                    sb8.append(",");
                    sb8.append(str2);
                    sb8.append(",");
                    sb8.append(scanResult.level);
                    addParam(sb7, sb8.toString());
                    StringBuilder sb9 = new StringBuilder();
                    sb9.append("wifi");
                    sb9.append(i);
                    sb9.append(": ");
                    sb9.append(str);
                    sb9.append(",");
                    sb9.append(str2);
                    sb9.append(",");
                    sb9.append(scanResult.level);
                    ah.a(sb9.toString());
                    i++;
                }
            }
        }
    }

    private void D(@NonNull Context context) {
        a aVar = new a(context);
        String str = ",";
        if (aVar.gx != null) {
            int i = 0;
            while (i < aVar.gx.size()) {
                StringBuilder sb = new StringBuilder();
                b bVar = (b) aVar.gx.get(i);
                if (!"cdma".equals(bVar.type)) {
                    sb.append(bVar.type);
                    sb.append(str);
                    sb.append(bVar.gy);
                    sb.append(str);
                    sb.append(bVar.gz);
                    sb.append(str);
                    sb.append(bVar.mcc);
                    sb.append(str);
                    sb.append(bVar.mnc);
                    sb.append(str);
                    sb.append(bVar.level);
                    sb.append(str);
                    sb.append(bVar.gA);
                    sb.append(str);
                    sb.append(bVar.gB);
                    sb.append(str);
                    sb.append(bVar.gC);
                    sb.append(str);
                    sb.append(bVar.gD);
                    sb.append(str);
                    sb.append(bVar.gE);
                    sb.append(str);
                    sb.append(bVar.gF);
                    sb.append(str);
                    sb.append(bVar.gG);
                } else {
                    sb.append(bVar.gH);
                    sb.append(str);
                    sb.append(bVar.gI);
                    sb.append(str);
                    sb.append(bVar.gJ);
                    sb.append(str);
                    sb.append(bVar.gK);
                    sb.append(str);
                    sb.append(bVar.gL);
                    sb.append(str);
                    sb.append(bVar.gM);
                    sb.append(str);
                    sb.append(bVar.level);
                    sb.append(str);
                    sb.append(bVar.gN);
                    sb.append(str);
                    sb.append(bVar.gB);
                    sb.append(str);
                    sb.append(bVar.gO);
                    sb.append(str);
                    sb.append(bVar.gA);
                    sb.append(str);
                    sb.append(bVar.gP);
                    sb.append(str);
                    sb.append(bVar.gQ);
                    sb.append(str);
                    sb.append(bVar.gR);
                    sb.append(str);
                    sb.append(bVar.gS);
                }
                StringBuilder sb2 = new StringBuilder();
                sb2.append("cell");
                sb2.append(i != 0 ? Integer.valueOf(i) : "");
                addParam(sb2.toString(), sb.toString());
                i++;
            }
        }
    }
}
