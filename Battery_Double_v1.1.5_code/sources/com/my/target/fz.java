package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils.TruncateAt;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import android.widget.TextView;

/* compiled from: TextViewWithAgeView */
public class fz extends ViewGroup {
    private static final int ij = hm.dV();
    private static final int ik = hm.dV();
    @NonNull
    private final TextView il;
    @NonNull
    private final fp im;
    private final int in;

    /* renamed from: io reason: collision with root package name */
    private final int f3192io;

    public fz(@NonNull Context context) {
        super(context);
        hm R = hm.R(context);
        this.il = new TextView(context);
        this.im = new fp(context);
        this.il.setId(ij);
        this.im.setId(ik);
        this.im.setLines(1);
        this.il.setTextSize(2, 18.0f);
        this.il.setEllipsize(TruncateAt.END);
        this.il.setMaxLines(1);
        this.il.setTextColor(-1);
        this.in = R.E(4);
        this.f3192io = R.E(2);
        hm.a((View) this.il, "title_text");
        hm.a((View) this.im, "age_bordering");
        addView(this.il);
        addView(this.im);
    }

    @NonNull
    public TextView getLeftText() {
        return this.il;
    }

    @NonNull
    public fp getRightBorderedView() {
        return this.im;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int size = MeasureSpec.getSize(i);
        int size2 = MeasureSpec.getSize(i2);
        this.im.measure(MeasureSpec.makeMeasureSpec(size, Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(size2 - (this.f3192io * 2), Integer.MIN_VALUE));
        int i3 = size / 2;
        if (this.im.getMeasuredWidth() > i3) {
            this.im.measure(MeasureSpec.makeMeasureSpec(i3, Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(size2 - (this.f3192io * 2), Integer.MIN_VALUE));
        }
        this.il.measure(MeasureSpec.makeMeasureSpec((size - this.im.getMeasuredWidth()) - this.in, Integer.MIN_VALUE), MeasureSpec.makeMeasureSpec(size2 - (this.f3192io * 2), Integer.MIN_VALUE));
        setMeasuredDimension(this.il.getMeasuredWidth() + this.im.getMeasuredWidth() + this.in, Math.max(this.il.getMeasuredHeight(), this.im.getMeasuredHeight()));
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        int measuredWidth = this.il.getMeasuredWidth();
        int measuredHeight = this.il.getMeasuredHeight();
        int measuredWidth2 = this.im.getMeasuredWidth();
        int measuredHeight2 = this.im.getMeasuredHeight();
        int measuredHeight3 = getMeasuredHeight();
        int i5 = (measuredHeight3 - measuredHeight) / 2;
        int i6 = (measuredHeight3 - measuredHeight2) / 2;
        int i7 = this.in + measuredWidth;
        this.il.layout(0, i5, measuredWidth, measuredHeight + i5);
        this.im.layout(i7, i6, measuredWidth2 + i7, measuredHeight2 + i6);
    }
}
