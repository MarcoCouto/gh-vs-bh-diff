package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.webkit.URLUtil;
import java.util.List;

/* compiled from: StatResolver */
public class hl {
    private static final hl ms = new hl();

    public static void a(@Nullable dg dgVar, @NonNull Context context) {
        ms.b(dgVar, context);
    }

    public static void a(@Nullable List<dg> list, @NonNull Context context) {
        ms.c(list, context);
    }

    public static void o(@Nullable String str, @NonNull Context context) {
        ms.p(str, context);
    }

    public static void b(@Nullable List<String> list, @NonNull Context context) {
        ms.d(list, context);
    }

    hl() {
    }

    /* access modifiers changed from: 0000 */
    public void b(@Nullable final dg dgVar, @NonNull Context context) {
        if (dgVar != null) {
            final Context applicationContext = context.getApplicationContext();
            ai.b(new Runnable() {
                public void run() {
                    hl.this.c(dgVar);
                    String a2 = hl.this.aj(dgVar.getUrl());
                    if (a2 != null) {
                        dn.cu().f(a2, applicationContext);
                    }
                }
            });
        }
    }

    /* access modifiers changed from: 0000 */
    public void c(@Nullable final List<dg> list, @NonNull Context context) {
        if (list != null && list.size() > 0) {
            final Context applicationContext = context.getApplicationContext();
            ai.b(new Runnable() {
                public void run() {
                    dn cu = dn.cu();
                    for (dg dgVar : list) {
                        hl.this.c(dgVar);
                        String a2 = hl.this.aj(dgVar.getUrl());
                        if (a2 != null) {
                            cu.f(a2, applicationContext);
                        }
                    }
                }
            });
        }
    }

    /* access modifiers changed from: 0000 */
    public void p(@Nullable final String str, @NonNull Context context) {
        if (!TextUtils.isEmpty(str)) {
            final Context applicationContext = context.getApplicationContext();
            ai.b(new Runnable() {
                public void run() {
                    String a2 = hl.this.aj(str);
                    if (a2 != null) {
                        dn.cu().f(a2, applicationContext);
                    }
                }
            });
        }
    }

    /* access modifiers changed from: 0000 */
    public void d(@Nullable final List<String> list, @NonNull Context context) {
        if (list != null && list.size() > 0) {
            final Context applicationContext = context.getApplicationContext();
            ai.b(new Runnable() {
                public void run() {
                    dn cu = dn.cu();
                    for (String a2 : list) {
                        String a3 = hl.this.aj(a2);
                        if (a3 != null) {
                            cu.f(a3, applicationContext);
                        }
                    }
                }
            });
        }
    }

    /* access modifiers changed from: private */
    @Nullable
    public String aj(@NonNull String str) {
        String decode = hn.decode(str);
        if (URLUtil.isNetworkUrl(decode)) {
            return decode;
        }
        StringBuilder sb = new StringBuilder();
        sb.append("invalid stat url: ");
        sb.append(decode);
        ah.a(sb.toString());
        return null;
    }

    /* access modifiers changed from: private */
    public void c(@NonNull dg dgVar) {
        if (dgVar instanceof df) {
            float cf = ((df) dgVar).cf();
            StringBuilder sb = new StringBuilder();
            sb.append("tracking progress stat value:");
            sb.append(cf);
            sb.append(" url:");
            sb.append(dgVar.getUrl());
            ah.a(sb.toString());
        } else if (dgVar instanceof de) {
            de deVar = (de) dgVar;
            int cn = deVar.cn();
            float cf2 = deVar.cf();
            boolean ce = deVar.ce();
            StringBuilder sb2 = new StringBuilder();
            sb2.append("tracking ovv stat percent:");
            sb2.append(cn);
            sb2.append(" value:");
            sb2.append(cf2);
            sb2.append(" ovv:");
            sb2.append(ce);
            sb2.append(" url:");
            sb2.append(dgVar.getUrl());
            ah.a(sb2.toString());
        } else if (dgVar instanceof dd) {
            dd ddVar = (dd) dgVar;
            int cn2 = ddVar.cn();
            float cf3 = ddVar.cf();
            float duration = ddVar.getDuration();
            StringBuilder sb3 = new StringBuilder();
            sb3.append("tracking mrc stat percent: value:");
            sb3.append(cf3);
            sb3.append(" percent ");
            sb3.append(cn2);
            sb3.append(" duration:");
            sb3.append(duration);
            sb3.append(" url:");
            sb3.append(dgVar.getUrl());
            ah.a(sb3.toString());
        } else {
            StringBuilder sb4 = new StringBuilder();
            sb4.append("tracking stat type:");
            sb4.append(dgVar.getType());
            sb4.append(" url:");
            sb4.append(dgVar.getUrl());
            ah.a(sb4.toString());
        }
    }
}
