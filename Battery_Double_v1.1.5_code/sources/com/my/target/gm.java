package com.my.target;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.my.target.eq.a;

/* compiled from: InterstitialPromoVideoView */
public interface gm extends gn {
    void a(@NonNull cm cmVar);

    void dG();

    void destroy();

    void finish();

    @NonNull
    gp getPromoMediaView();

    boolean isPaused();

    boolean isPlaying();

    void pause();

    void play();

    void resume();

    void setMediaListener(@Nullable a aVar);

    void setTimeChanged(float f);

    void stop(boolean z);

    void z(int i);

    void z(boolean z);
}
