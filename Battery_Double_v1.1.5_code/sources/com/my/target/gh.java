package com.my.target;

import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

/* compiled from: PromoCardSlider */
public interface gh {

    /* compiled from: PromoCardSlider */
    public interface a {
        void b(@NonNull View view, int i);

        void b(@NonNull View view, @NonNull int[] iArr);
    }

    void dispose();

    @Nullable
    Parcelable getState();

    @NonNull
    int[] getVisibleCardNumbers();

    void restoreState(@NonNull Parcelable parcelable);

    void setPromoCardSliderListener(@Nullable a aVar);
}
