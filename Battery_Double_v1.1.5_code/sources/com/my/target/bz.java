package com.my.target;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.util.ArrayList;
import java.util.Iterator;

/* compiled from: AdService */
public class bz {
    private float allowCloseDelay = -1.0f;
    @NonNull
    private final ArrayList<bz> cH = new ArrayList<>();
    @NonNull
    private final ArrayList<dg> cI = new ArrayList<>();
    @NonNull
    private final dh cJ = dh.ch();
    @Nullable
    private ArrayList<dg> cK;
    @Nullable
    private bz cL;
    @Nullable
    private String cM;
    private int cN;
    private int cO = -1;
    private boolean cP;
    private boolean cQ;
    private boolean cR;
    @Nullable
    private Boolean cS;
    @Nullable
    private Boolean cT;
    @Nullable
    private Boolean cU;
    @Nullable
    private Boolean cV;
    @Nullable
    private Boolean cW;
    @Nullable
    private Boolean cX;
    @Nullable
    private Boolean cY;
    @Nullable
    private ArrayList<ch> companionBanners;
    @Nullable
    private String ctaText;
    private int id = -1;
    private float point = -1.0f;
    private float pointP = -1.0f;
    private int position = -1;
    @NonNull
    private final String url;

    @NonNull
    public static bz q(@NonNull String str) {
        return new bz(str);
    }

    private bz(@NonNull String str) {
        this.url = str;
    }

    public boolean aT() {
        return this.cQ;
    }

    public void m(boolean z) {
        this.cQ = z;
    }

    public int aU() {
        return this.cO;
    }

    public boolean aV() {
        return this.cR;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int i) {
        this.id = i;
    }

    public void a(@Nullable bz bzVar) {
        this.cL = bzVar;
        if (bzVar != null) {
            bzVar.setPosition(this.position);
        }
    }

    @Nullable
    public bz aW() {
        return this.cL;
    }

    public boolean aX() {
        return this.cP;
    }

    public void n(boolean z) {
        this.cP = z;
    }

    @NonNull
    public String getUrl() {
        return this.url;
    }

    @NonNull
    public ArrayList<bz> aY() {
        return this.cH;
    }

    public void a(dg dgVar) {
        this.cI.add(dgVar);
    }

    @NonNull
    public ArrayList<dg> r(@NonNull String str) {
        ArrayList<dg> arrayList = new ArrayList<>();
        Iterator it = this.cI.iterator();
        while (it.hasNext()) {
            dg dgVar = (dg) it.next();
            if (str.equals(dgVar.getType())) {
                arrayList.add(dgVar);
            }
        }
        return arrayList;
    }

    @Nullable
    public ArrayList<dg> aZ() {
        if (this.cK != null) {
            return new ArrayList<>(this.cK);
        }
        return null;
    }

    @Nullable
    public ArrayList<ch> getCompanionBanners() {
        return this.companionBanners;
    }

    public void b(@Nullable ArrayList<ch> arrayList) {
        this.companionBanners = arrayList;
    }

    public void c(@Nullable ArrayList<dg> arrayList) {
        this.cK = arrayList;
    }

    public int ba() {
        return this.cN;
    }

    public void f(int i) {
        this.cN = i;
    }

    public float getPoint() {
        return this.point;
    }

    public void setPoint(float f) {
        this.point = f;
    }

    public float getPointP() {
        return this.pointP;
    }

    public void setPointP(float f) {
        this.pointP = f;
    }

    public int getPosition() {
        return this.position;
    }

    public void setPosition(int i) {
        this.position = i;
        if (this.cL != null) {
            this.cL.setPosition(i);
        }
    }

    @Nullable
    public String bb() {
        return this.cM;
    }

    public void s(@Nullable String str) {
        this.cM = str;
    }

    public void g(int i) {
        this.cO = i;
    }

    public void o(boolean z) {
        this.cR = z;
    }

    public void b(@NonNull bz bzVar) {
        this.cH.add(bzVar);
    }

    public void d(@Nullable ArrayList<dg> arrayList) {
        if (this.cK == null) {
            this.cK = arrayList;
        } else if (arrayList != null) {
            this.cK.addAll(arrayList);
        }
    }

    @Nullable
    public String getCtaText() {
        return this.ctaText;
    }

    public void setCtaText(@Nullable String str) {
        this.ctaText = str;
    }

    public float getAllowCloseDelay() {
        return this.allowCloseDelay;
    }

    public void setAllowCloseDelay(float f) {
        this.allowCloseDelay = f;
    }

    @Nullable
    public Boolean bc() {
        return this.cS;
    }

    public void a(@Nullable Boolean bool) {
        this.cS = bool;
    }

    @Nullable
    public Boolean bd() {
        return this.cT;
    }

    public void b(@Nullable Boolean bool) {
        this.cT = bool;
    }

    @Nullable
    public Boolean be() {
        return this.cU;
    }

    public void c(@Nullable Boolean bool) {
        this.cU = bool;
    }

    @Nullable
    public Boolean bf() {
        return this.cV;
    }

    public void d(@Nullable Boolean bool) {
        this.cV = bool;
    }

    @Nullable
    public Boolean bg() {
        return this.cW;
    }

    public void e(@Nullable Boolean bool) {
        this.cW = bool;
    }

    @NonNull
    public dh bh() {
        return this.cJ;
    }

    @Nullable
    public Boolean bi() {
        return this.cX;
    }

    public void f(@Nullable Boolean bool) {
        this.cX = bool;
    }

    @Nullable
    public Boolean bj() {
        return this.cY;
    }

    public void g(@Nullable Boolean bool) {
        this.cY = bool;
    }
}
