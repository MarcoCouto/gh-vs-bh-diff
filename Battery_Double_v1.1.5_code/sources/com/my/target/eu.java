package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.View.OnClickListener;
import com.my.target.ha.c;

/* compiled from: InterstitialSliderPresenter */
public class eu {
    /* access modifiers changed from: private */
    @NonNull
    public final gx fu;
    /* access modifiers changed from: private */
    @Nullable
    public a fv;

    /* compiled from: InterstitialSliderPresenter */
    public interface a {
        void a(@NonNull cl clVar);

        void ag();

        void b(@NonNull cl clVar);
    }

    @NonNull
    public static eu v(@NonNull Context context) {
        return new eu(context);
    }

    private eu(@NonNull Context context) {
        this.fu = new gx(context);
        this.fu.setFSSliderCardListener(new c() {
            public void f(@NonNull cl clVar) {
                if (eu.this.fv != null) {
                    eu.this.fv.a(clVar);
                }
            }

            public void a(int i, @NonNull cl clVar) {
                if (eu.this.fv != null) {
                    eu.this.fv.b(clVar);
                }
                eu.this.fu.B(i);
            }
        });
        this.fu.setCloseClickListener(new OnClickListener() {
            public void onClick(View view) {
                if (eu.this.fv != null) {
                    eu.this.fv.ag();
                }
            }
        });
    }

    public void a(@NonNull cy cyVar) {
        this.fu.a(cyVar, cyVar.bI());
    }

    @NonNull
    public View getView() {
        return this.fu;
    }

    public void a(@Nullable a aVar) {
        this.fv = aVar;
    }
}
