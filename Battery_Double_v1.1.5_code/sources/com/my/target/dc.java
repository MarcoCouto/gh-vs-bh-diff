package com.my.target;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import org.json.JSONObject;

/* compiled from: StandardAdSection */
public class dc extends cu {
    @Nullable
    private cr bK;
    private JSONObject dJ;
    private boolean dP = true;
    @Nullable
    private String dQ;

    @NonNull
    public static dc bZ() {
        return new dc();
    }

    private dc() {
    }

    public void p(boolean z) {
        this.dP = z;
    }

    public boolean ca() {
        return this.dP;
    }

    public void J(@Nullable String str) {
        this.dQ = str;
    }

    @Nullable
    public String cb() {
        return this.dQ;
    }

    public void c(JSONObject jSONObject) {
        this.dJ = jSONObject;
    }

    public JSONObject bS() {
        return this.dJ;
    }

    @Nullable
    public cr cc() {
        return this.bK;
    }

    public void a(@Nullable cr crVar) {
        this.bK = crVar;
    }

    public int getBannersCount() {
        return this.bK == null ? 0 : 1;
    }
}
