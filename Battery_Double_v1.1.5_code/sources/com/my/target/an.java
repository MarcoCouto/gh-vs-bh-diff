package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;

/* compiled from: IInterstitialAdEngine */
public interface an {
    void destroy();

    void dismiss();

    void k(@NonNull Context context);

    void l(@NonNull Context context);
}
