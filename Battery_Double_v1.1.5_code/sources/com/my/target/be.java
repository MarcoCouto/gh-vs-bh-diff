package com.my.target;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.my.target.ads.MyTargetView;
import com.my.target.ads.MyTargetView.MyTargetViewListener;
import com.my.target.b.C0072b;
import java.lang.ref.WeakReference;

/* compiled from: StandardAdMasterEngine */
public class be {
    @NonNull
    final a adConfig;
    @NonNull
    final MyTargetView bP;
    @NonNull
    final b bQ = new b();
    @NonNull
    final c bR;
    @Nullable
    private ap bS;
    private boolean bT = true;
    private boolean bU;
    private int bV = -1;
    private long bW;
    private long bX;

    /* compiled from: StandardAdMasterEngine */
    static class a implements com.my.target.ap.a {
        @NonNull
        private final be engine;

        public a(@NonNull be beVar) {
            this.engine = beVar;
        }

        public void aa() {
            this.engine.aa();
        }

        public void e(@NonNull String str) {
            this.engine.e(str);
        }

        public void ab() {
            this.engine.ab();
        }

        public void onClick() {
            this.engine.onClick();
        }

        public void ac() {
            this.engine.ac();
        }

        public void ad() {
            this.engine.ad();
        }
    }

    /* compiled from: StandardAdMasterEngine */
    static class b {
        private boolean bZ;
        private boolean ca;
        private boolean cb;
        private boolean cc;
        private boolean cd;
        private boolean ce;
        private boolean cf;

        b() {
        }

        public void e(boolean z) {
            this.ca = z;
        }

        public void f(boolean z) {
            this.cc = z;
        }

        public void setFocused(boolean z) {
            this.cd = z;
        }

        public boolean aA() {
            return this.cc && this.cb && (this.cf || this.cd) && !this.bZ;
        }

        public boolean aB() {
            return this.cb && this.bZ && (this.cf || this.cd) && !this.ce && this.ca;
        }

        public void g(boolean z) {
            this.bZ = z;
            this.ca = false;
        }

        public boolean canPause() {
            return !this.ca && this.bZ && (this.cf || !this.cd);
        }

        public void aC() {
            this.ce = false;
            this.cb = false;
        }

        public void h(boolean z) {
            this.cb = z;
        }

        public void i(boolean z) {
            this.ce = z;
        }

        public void j(boolean z) {
            this.cf = z;
        }

        public boolean isPaused() {
            return this.ca;
        }

        public boolean aD() {
            return this.bZ;
        }
    }

    /* compiled from: StandardAdMasterEngine */
    static class c implements Runnable {
        @NonNull
        private final WeakReference<be> cg;

        c(@NonNull be beVar) {
            this.cg = new WeakReference<>(beVar);
        }

        public void run() {
            be beVar = (be) this.cg.get();
            if (beVar != null) {
                beVar.ax();
            }
        }
    }

    @NonNull
    public static be a(@NonNull MyTargetView myTargetView, @NonNull a aVar) {
        return new be(myTargetView, aVar);
    }

    private be(@NonNull MyTargetView myTargetView, @NonNull a aVar) {
        this.bP = myTargetView;
        this.adConfig = aVar;
        this.bR = new c(this);
        if (!(myTargetView.getContext() instanceof Activity)) {
            ah.a("MyTargetView was created with non-activity focus, so system cannot automatically handle lifecycle");
            this.bQ.j(true);
            return;
        }
        this.bQ.j(false);
    }

    public void a(@NonNull dc dcVar) {
        if (this.bQ.aD()) {
            stop();
        }
        ay();
        b(dcVar);
        if (this.bS != null) {
            this.bS.a(new a(this));
            this.bW = System.currentTimeMillis() + ((long) this.bV);
            this.bX = 0;
            if (this.bU && this.bQ.isPaused()) {
                this.bX = (long) this.bV;
            }
            this.bS.prepare();
        }
    }

    public void d(boolean z) {
        this.bQ.f(z);
        this.bQ.setFocused(this.bP.hasWindowFocus());
        if (this.bQ.aA()) {
            start();
        } else if (!z && this.bQ.aD()) {
            stop();
        }
    }

    public void onWindowFocusChanged(boolean z) {
        this.bQ.setFocused(z);
        if (this.bQ.aA()) {
            start();
        } else if (this.bQ.aB()) {
            resume();
        } else if (this.bQ.canPause()) {
            pause();
        }
    }

    public void destroy() {
        if (this.bQ.aD()) {
            stop();
        }
        this.bQ.aC();
        ay();
    }

    /* access modifiers changed from: 0000 */
    public void ax() {
        ah.a("load new standard ad");
        ae.a(this.adConfig).a((C0072b<T>) new com.my.target.ae.a() {
            public void onResult(@Nullable dc dcVar, @Nullable String str) {
                if (dcVar != null) {
                    be.this.a(dcVar);
                    return;
                }
                ah.a("No new ad");
                be.this.az();
            }
        }).a(this.bP.getContext());
    }

    private void b(@NonNull dc dcVar) {
        boolean z = true;
        this.bU = dcVar.ca() && this.adConfig.isRefreshAd() && !this.adConfig.getFormat().equals("standard_300x250");
        cr cc = dcVar.cc();
        if (cc == null) {
            cs bx = dcVar.bx();
            if (bx == null) {
                MyTargetViewListener listener = this.bP.getListener();
                if (listener != null) {
                    listener.onNoAd("no ad", this.bP);
                }
                return;
            }
            this.bS = ba.a(this.bP, bx, this.adConfig);
            if (this.bU) {
                this.bV = bx.br() * 1000;
                if (this.bV <= 0) {
                    z = false;
                }
                this.bU = z;
            }
        } else {
            this.bS = bd.a(this.bP, cc, dcVar, this.adConfig);
            this.bV = cc.getTimeout() * 1000;
        }
    }

    /* access modifiers changed from: 0000 */
    public void ay() {
        if (this.bS != null) {
            this.bS.destroy();
            this.bS.a(null);
            this.bS = null;
        }
        this.bP.removeAllViews();
    }

    /* access modifiers changed from: 0000 */
    public void start() {
        if (this.bV > 0 && this.bU) {
            this.bP.postDelayed(this.bR, (long) this.bV);
        }
        if (this.bS != null) {
            this.bS.start();
        }
        this.bQ.g(true);
    }

    /* access modifiers changed from: 0000 */
    public void resume() {
        if (this.bX > 0 && this.bU) {
            this.bW = System.currentTimeMillis() + this.bX;
            this.bP.postDelayed(this.bR, this.bX);
            this.bX = 0;
        }
        if (this.bS != null) {
            this.bS.resume();
        }
        this.bQ.e(false);
    }

    /* access modifiers changed from: 0000 */
    public void pause() {
        this.bP.removeCallbacks(this.bR);
        if (this.bU) {
            this.bX = this.bW - System.currentTimeMillis();
        }
        if (this.bS != null) {
            this.bS.pause();
        }
        this.bQ.e(true);
    }

    /* access modifiers changed from: 0000 */
    public void stop() {
        this.bQ.g(false);
        this.bP.removeCallbacks(this.bR);
        if (this.bS != null) {
            this.bS.stop();
        }
    }

    /* access modifiers changed from: 0000 */
    public void az() {
        if (this.bU && this.bV > 0) {
            this.bP.removeCallbacks(this.bR);
            this.bP.postDelayed(this.bR, (long) this.bV);
        }
    }

    /* access modifiers changed from: 0000 */
    public void aa() {
        if (this.bT) {
            this.bQ.h(true);
            MyTargetViewListener listener = this.bP.getListener();
            if (listener != null) {
                listener.onLoad(this.bP);
            }
            this.bT = false;
        }
        if (this.bQ.aA()) {
            start();
        }
    }

    /* access modifiers changed from: 0000 */
    public void e(@NonNull String str) {
        if (this.bT) {
            this.bQ.h(false);
            MyTargetViewListener listener = this.bP.getListener();
            if (listener != null) {
                listener.onNoAd(str, this.bP);
            }
            this.bT = false;
            return;
        }
        ay();
        az();
    }

    /* access modifiers changed from: 0000 */
    public void ad() {
        this.bQ.i(false);
        if (this.bQ.aB()) {
            resume();
        }
    }

    /* access modifiers changed from: 0000 */
    public void ac() {
        if (this.bQ.canPause()) {
            pause();
        }
        this.bQ.i(true);
    }

    /* access modifiers changed from: private */
    public void onClick() {
        MyTargetViewListener listener = this.bP.getListener();
        if (listener != null) {
            listener.onClick(this.bP);
        }
    }

    /* access modifiers changed from: private */
    public void ab() {
        MyTargetViewListener listener = this.bP.getListener();
        if (listener != null) {
            listener.onShow(this.bP);
        }
    }
}
