package com.my.target;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewCompat;
import android.text.TextUtils;
import android.text.TextUtils.TruncateAt;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.util.HashMap;

@SuppressLint({"ViewConstructor"})
/* compiled from: BodyView */
public class gi extends FrameLayout implements OnTouchListener {
    @NonNull
    private final TextView descriptionLabel;
    @NonNull
    private final TextView disclaimerLabel;
    @NonNull
    private final LinearLayout jA;
    private final boolean jB;
    @NonNull
    private final HashMap<View, Boolean> jC = new HashMap<>();
    @Nullable
    private OnClickListener jD;
    @NonNull
    private final TextView jz;
    @Nullable
    private String navigationType;
    @NonNull
    private final LinearLayout ratingLayout;
    @NonNull
    private final fy starsView;
    @NonNull
    private final TextView titleLabel;
    @NonNull
    private final hm uiUtils;
    @NonNull
    private final TextView votesLabel;

    public gi(@NonNull Context context, @NonNull hm hmVar, boolean z) {
        super(context);
        this.titleLabel = new TextView(context);
        this.jz = new TextView(context);
        this.descriptionLabel = new TextView(context);
        this.ratingLayout = new LinearLayout(context);
        this.disclaimerLabel = new TextView(context);
        this.starsView = new fy(context);
        this.votesLabel = new TextView(context);
        this.jA = new LinearLayout(context);
        hm.a((View) this.titleLabel, "title_text");
        hm.a((View) this.descriptionLabel, "description_text");
        hm.a((View) this.disclaimerLabel, "disclaimer_text");
        hm.a((View) this.starsView, "stars_view");
        hm.a((View) this.votesLabel, "votes_text");
        this.uiUtils = hmVar;
        this.jB = z;
    }

    public void setBanner(@NonNull cm cmVar) {
        this.navigationType = cmVar.getNavigationType();
        this.titleLabel.setText(cmVar.getTitle());
        this.descriptionLabel.setText(cmVar.getDescription());
        this.starsView.setRating(cmVar.getRating());
        this.votesLabel.setText(String.valueOf(cmVar.getVotes()));
        if ("store".equals(cmVar.getNavigationType())) {
            hm.a((View) this.jz, "category_text");
            String category = cmVar.getCategory();
            String subCategory = cmVar.getSubCategory();
            String str = "";
            if (!TextUtils.isEmpty(category)) {
                StringBuilder sb = new StringBuilder();
                sb.append(str);
                sb.append(category);
                str = sb.toString();
            }
            if (!TextUtils.isEmpty(str) && !TextUtils.isEmpty(subCategory)) {
                StringBuilder sb2 = new StringBuilder();
                sb2.append(str);
                sb2.append(", ");
                str = sb2.toString();
            }
            if (!TextUtils.isEmpty(subCategory)) {
                StringBuilder sb3 = new StringBuilder();
                sb3.append(str);
                sb3.append(subCategory);
                str = sb3.toString();
            }
            if (!TextUtils.isEmpty(str)) {
                this.jz.setText(str);
                this.jz.setVisibility(0);
            } else {
                this.jz.setVisibility(8);
            }
            this.ratingLayout.setVisibility(0);
            this.ratingLayout.setGravity(16);
            if (cmVar.getRating() > 0.0f) {
                this.starsView.setVisibility(0);
                if (cmVar.getVotes() > 0) {
                    this.votesLabel.setVisibility(0);
                } else {
                    this.votesLabel.setVisibility(8);
                }
            } else {
                this.starsView.setVisibility(8);
                this.votesLabel.setVisibility(8);
            }
            this.jz.setTextColor(-3355444);
        } else {
            hm.a((View) this.jz, "domain_text");
            this.ratingLayout.setVisibility(8);
            this.jz.setText(cmVar.getDomain());
            this.ratingLayout.setVisibility(8);
            this.jz.setTextColor(-16733198);
        }
        if (TextUtils.isEmpty(cmVar.getDisclaimer())) {
            this.disclaimerLabel.setVisibility(8);
        } else {
            this.disclaimerLabel.setVisibility(0);
            this.disclaimerLabel.setText(cmVar.getDisclaimer());
        }
        if (this.jB) {
            this.titleLabel.setTextSize(2, 32.0f);
            this.descriptionLabel.setTextSize(2, 24.0f);
            this.disclaimerLabel.setTextSize(2, 18.0f);
            this.jz.setTextSize(2, 18.0f);
            return;
        }
        this.titleLabel.setTextSize(2, 20.0f);
        this.descriptionLabel.setTextSize(2, 16.0f);
        this.disclaimerLabel.setTextSize(2, 14.0f);
        this.jz.setTextSize(2, 16.0f);
    }

    @SuppressLint({"ClickableViewAccessibility"})
    public void a(@NonNull ca caVar, @NonNull OnClickListener onClickListener) {
        if (caVar.dn) {
            setOnClickListener(onClickListener);
            hm.a(this, -1, -3806472);
            return;
        }
        this.jD = onClickListener;
        this.titleLabel.setOnTouchListener(this);
        this.jz.setOnTouchListener(this);
        this.descriptionLabel.setOnTouchListener(this);
        this.starsView.setOnTouchListener(this);
        this.votesLabel.setOnTouchListener(this);
        setOnTouchListener(this);
        this.jC.put(this.titleLabel, Boolean.valueOf(caVar.db));
        if ("store".equals(this.navigationType)) {
            this.jC.put(this.jz, Boolean.valueOf(caVar.dl));
        } else {
            this.jC.put(this.jz, Boolean.valueOf(caVar.dk));
        }
        this.jC.put(this.descriptionLabel, Boolean.valueOf(caVar.dc));
        this.jC.put(this.starsView, Boolean.valueOf(caVar.df));
        this.jC.put(this.votesLabel, Boolean.valueOf(caVar.dg));
        this.jC.put(this, Boolean.valueOf(caVar.dm));
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (!this.jC.containsKey(view)) {
            return false;
        }
        if (!((Boolean) this.jC.get(view)).booleanValue()) {
            return true;
        }
        int action = motionEvent.getAction();
        if (action != 3) {
            switch (action) {
                case 0:
                    setBackgroundColor(-3806472);
                    break;
                case 1:
                    setBackgroundColor(-1);
                    if (this.jD != null) {
                        this.jD.onClick(view);
                        break;
                    }
                    break;
            }
        } else {
            setBackgroundColor(-1);
        }
        return true;
    }

    /* access modifiers changed from: 0000 */
    public void y(boolean z) {
        this.jA.setOrientation(1);
        this.jA.setGravity(1);
        this.titleLabel.setGravity(1);
        this.titleLabel.setTextColor(ViewCompat.MEASURED_STATE_MASK);
        LayoutParams layoutParams = new LayoutParams(-2, -2);
        layoutParams.gravity = 1;
        layoutParams.leftMargin = this.uiUtils.E(8);
        layoutParams.rightMargin = this.uiUtils.E(8);
        this.titleLabel.setLayoutParams(layoutParams);
        LayoutParams layoutParams2 = new LayoutParams(-2, -2);
        layoutParams2.gravity = 1;
        this.jz.setLayoutParams(layoutParams2);
        this.jz.setLines(1);
        this.jz.setEllipsize(TruncateAt.MIDDLE);
        this.descriptionLabel.setGravity(1);
        this.descriptionLabel.setTextColor(ViewCompat.MEASURED_STATE_MASK);
        LayoutParams layoutParams3 = new LayoutParams(-2, -2);
        if (z) {
            this.descriptionLabel.setTextSize(2, 12.0f);
            this.descriptionLabel.setLines(2);
            this.descriptionLabel.setEllipsize(TruncateAt.END);
            layoutParams3.topMargin = 0;
            layoutParams3.leftMargin = this.uiUtils.E(4);
            layoutParams3.rightMargin = this.uiUtils.E(4);
        } else {
            this.descriptionLabel.setTextSize(2, 16.0f);
            layoutParams3.topMargin = this.uiUtils.E(8);
            layoutParams3.leftMargin = this.uiUtils.E(16);
            layoutParams3.rightMargin = this.uiUtils.E(16);
        }
        layoutParams3.gravity = 1;
        this.descriptionLabel.setLayoutParams(layoutParams3);
        this.ratingLayout.setOrientation(0);
        LayoutParams layoutParams4 = new LayoutParams(-2, -2);
        layoutParams4.gravity = 1;
        this.ratingLayout.setLayoutParams(layoutParams4);
        LayoutParams layoutParams5 = new LayoutParams(this.uiUtils.E(73), this.uiUtils.E(12));
        layoutParams5.topMargin = this.uiUtils.E(4);
        layoutParams5.rightMargin = this.uiUtils.E(4);
        this.starsView.setLayoutParams(layoutParams5);
        this.votesLabel.setTextColor(-6710887);
        this.votesLabel.setTextSize(2, 14.0f);
        this.disclaimerLabel.setTextColor(-6710887);
        this.disclaimerLabel.setGravity(1);
        LayoutParams layoutParams6 = new LayoutParams(-2, -2);
        layoutParams6.gravity = 1;
        if (z) {
            layoutParams6.leftMargin = this.uiUtils.E(4);
            layoutParams6.rightMargin = this.uiUtils.E(4);
        } else {
            layoutParams6.leftMargin = this.uiUtils.E(16);
            layoutParams6.rightMargin = this.uiUtils.E(16);
        }
        layoutParams6.gravity = 1;
        this.disclaimerLabel.setLayoutParams(layoutParams6);
        LayoutParams layoutParams7 = new LayoutParams(-2, -2);
        layoutParams7.gravity = 17;
        addView(this.jA, layoutParams7);
        this.jA.addView(this.titleLabel);
        this.jA.addView(this.jz);
        this.jA.addView(this.ratingLayout);
        this.jA.addView(this.descriptionLabel);
        this.jA.addView(this.disclaimerLabel);
        this.ratingLayout.addView(this.starsView);
        this.ratingLayout.addView(this.votesLabel);
    }
}
