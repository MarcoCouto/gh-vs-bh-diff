package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import com.ironsource.sdk.precache.DownloadManager;
import com.my.target.common.models.ImageData;
import org.json.JSONObject;

/* compiled from: InterstitialSliderAdSectionParser */
public class ec {
    @NonNull
    public static ec f(@NonNull bz bzVar, @NonNull a aVar, @NonNull Context context) {
        return new ec(bzVar, aVar, context);
    }

    private ec(@NonNull bz bzVar, @NonNull a aVar, @NonNull Context context) {
    }

    public void a(@NonNull JSONObject jSONObject, @NonNull cy cyVar) {
        JSONObject optJSONObject = jSONObject.optJSONObject(DownloadManager.SETTINGS);
        if (optJSONObject != null) {
            b(optJSONObject, cyVar);
        }
    }

    private void b(@NonNull JSONObject jSONObject, @NonNull cy cyVar) {
        String optString = jSONObject.optString("close_icon_hd", "");
        if (!TextUtils.isEmpty(optString)) {
            cyVar.setCloseIcon(ImageData.newImageData(optString));
        }
        cyVar.setBackgroundColor(ed.a(jSONObject, "backgroundColor", cyVar.getBackgroundColor()));
        cyVar.n(ed.a(jSONObject, "markerColor", cyVar.bH()));
        cyVar.m(ed.a(jSONObject, "activeMarkerColor", cyVar.bG()));
    }
}
