package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.view.View;
import android.view.View.OnClickListener;
import java.util.List;

/* compiled from: InterstitialPromoPresenter */
public class et implements es {
    @NonNull
    private final cm ba;
    private long eL;
    private long eM;
    @NonNull
    private final d fm;
    @NonNull
    private final gn fn;
    @Nullable
    private gm fo;
    @Nullable
    private gv fp;
    @Nullable
    private en fq;
    @Nullable
    private eq fr;
    @Nullable
    private b fs;

    /* compiled from: InterstitialPromoPresenter */
    static class a implements OnClickListener {
        private et ft;

        a(et etVar) {
            this.ft = etVar;
        }

        public void onClick(View view) {
            eq cX = this.ft.cX();
            if (cX != null) {
                cX.cL();
            }
            b cW = this.ft.cW();
            if (cW != null) {
                cW.ag();
            }
        }
    }

    /* compiled from: InterstitialPromoPresenter */
    public interface b extends com.my.target.es.a {
        void ai();

        void aj();
    }

    /* compiled from: InterstitialPromoPresenter */
    static class c implements com.my.target.gn.a {
        @NonNull
        private final et ft;

        c(@NonNull et etVar) {
            this.ft = etVar;
        }

        public void s(boolean z) {
            if (!z) {
                eq cX = this.ft.cX();
                if (cX != null) {
                    cX.a(this.ft.cY());
                    cX.destroy();
                }
                this.ft.o(null);
            }
        }

        public void da() {
            b cW = this.ft.cW();
            if (cW != null) {
                cW.b(this.ft.cY(), null, this.ft.cI().getContext());
            }
        }
    }

    /* compiled from: InterstitialPromoPresenter */
    static class d implements Runnable {
        @NonNull
        private final gn eC;

        d(@NonNull gn gnVar) {
            this.eC = gnVar;
        }

        public void run() {
            ah.a("banner became just closeable");
            this.eC.dF();
        }
    }

    @NonNull
    public static et a(@NonNull cm cmVar, boolean z, @NonNull Context context) {
        return new et(cmVar, z, context);
    }

    @Nullable
    public b cW() {
        return this.fs;
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    @VisibleForTesting
    public eq cX() {
        return this.fr;
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public void o(@Nullable eq eqVar) {
        this.fr = eqVar;
    }

    @NonNull
    public cm cY() {
        return this.ba;
    }

    public void b(@Nullable b bVar) {
        this.fs = bVar;
        if (this.fr != null) {
            this.fr.a(bVar);
        }
        if (this.fq != null) {
            this.fq.a(bVar);
        }
    }

    public void r(boolean z) {
        if (this.fr != null) {
            this.fr.r(z);
        }
    }

    private et(@NonNull cm cmVar, boolean z, @NonNull Context context) {
        this.ba = cmVar;
        c cVar = new c(this);
        cn videoBanner = cmVar.getVideoBanner();
        if (!cmVar.getInterstitialAdCards().isEmpty()) {
            gv gvVar = new gv(context);
            this.fp = gvVar;
            this.fn = gvVar;
        } else if (videoBanner == null || cmVar.getStyle() != 1) {
            gj gjVar = new gj(context, z);
            this.fo = gjVar;
            this.fn = gjVar;
        } else {
            gr grVar = new gr(context, z);
            this.fo = grVar;
            this.fn = grVar;
        }
        this.fm = new d(this.fn);
        this.fn.setInterstitialPromoViewListener(cVar);
        this.fn.setBanner(cmVar);
        this.fn.getCloseButton().setOnClickListener(new a(this));
        this.fn.setClickArea(cmVar.getClickArea());
        if (!(this.fo == null || videoBanner == null)) {
            this.fr = eq.a(videoBanner, this.fo);
            this.fr.a(videoBanner, context);
            if (videoBanner.isAutoPlay()) {
                this.eL = 0;
            }
        }
        if (videoBanner == null || !videoBanner.isAutoPlay()) {
            this.eM = (long) (cmVar.getAllowCloseDelay() * 1000.0f);
            if (this.eM > 0) {
                StringBuilder sb = new StringBuilder();
                sb.append("banner will be allowed to close in ");
                sb.append(this.eM);
                sb.append(" millis");
                ah.a(sb.toString());
                a(this.eM);
            } else {
                ah.a("banner is allowed to close");
                this.fn.dF();
            }
        }
        List interstitialAdCards = cmVar.getInterstitialAdCards();
        if (!interstitialAdCards.isEmpty() && this.fp != null) {
            this.fq = en.a(interstitialAdCards, this.fp);
        }
    }

    public void pause() {
        if (this.fr != null) {
            this.fr.pause();
        }
        this.fn.getView().removeCallbacks(this.fm);
        if (this.eL > 0) {
            long currentTimeMillis = System.currentTimeMillis() - this.eL;
            if (currentTimeMillis <= 0 || currentTimeMillis >= this.eM) {
                this.eM = 0;
            } else {
                this.eM -= currentTimeMillis;
            }
        }
    }

    public void resume() {
        if (this.fr == null && this.eM > 0) {
            a(this.eM);
        }
    }

    @NonNull
    public View cI() {
        return this.fn.getView();
    }

    public void stop() {
        if (this.fr != null) {
            this.fr.stop();
        }
    }

    public void destroy() {
        if (this.fr != null) {
            this.fr.destroy();
        }
    }

    public void cZ() {
        if (this.fr != null) {
            this.fr.a(this.ba);
        }
    }

    private void a(long j) {
        if (this.fo != null) {
            this.fo.getView().removeCallbacks(this.fm);
            this.eL = System.currentTimeMillis();
            this.fo.getView().postDelayed(this.fm, j);
        }
    }
}
