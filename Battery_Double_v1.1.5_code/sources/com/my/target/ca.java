package com.my.target;

import android.support.annotation.NonNull;

/* compiled from: ClickArea */
public class ca {
    @NonNull
    public static final ca cZ = new ca(4096);
    @NonNull
    public static final ca da = new ca(64);
    public final boolean db;
    public final boolean dc;
    public final boolean dd;
    public final boolean de;
    public final boolean df;
    public final boolean dg;
    public final boolean dh;
    public final boolean di;
    public final boolean dj;
    public final boolean dk;
    public final boolean dl;
    public final boolean dm;
    public final boolean dn;

    /* renamed from: do reason: not valid java name */
    private final int f675do;

    @NonNull
    public static ca h(int i) {
        return new ca(i);
    }

    private ca(int i) {
        this.f675do = i;
        boolean z = false;
        this.db = (i & 1) == 1;
        this.dc = (i & 2) == 2;
        this.dd = (i & 4) == 4;
        this.de = (i & 8) == 8;
        this.df = (i & 16) == 16;
        this.dg = (i & 32) == 32;
        this.dh = (i & 64) == 64;
        this.di = (i & 128) == 128;
        this.dj = (i & 256) == 256;
        this.dk = (i & 512) == 512;
        this.dl = (i & 1024) == 1024;
        this.dm = (i & 2048) == 2048;
        if ((i & 4096) == 4096) {
            z = true;
        }
        this.dn = z;
    }

    public int bk() {
        return this.f675do;
    }
}
