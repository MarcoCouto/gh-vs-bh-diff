package com.my.target;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.my.target.b.C0072b;

/* compiled from: NativeAdFactory */
public final class v extends b<da> {
    @Nullable
    private final da section;

    /* compiled from: NativeAdFactory */
    static class a implements com.my.target.b.a<da> {
        public boolean a() {
            return false;
        }

        private a() {
        }

        @NonNull
        public c<da> b() {
            return w.f();
        }

        @Nullable
        public d<da> c() {
            return x.k();
        }

        @NonNull
        public e d() {
            return y.l();
        }
    }

    public interface b extends C0072b {
    }

    @NonNull
    public static b<da> a(@NonNull a aVar) {
        return new v(aVar, null);
    }

    @NonNull
    public static b<da> a(@NonNull da daVar, @NonNull a aVar) {
        return new v(aVar, daVar);
    }

    private v(@NonNull a aVar, @Nullable da daVar) {
        super(new a(), aVar);
        this.section = daVar;
    }

    /* access modifiers changed from: protected */
    @Nullable
    /* renamed from: g */
    public da b(@NonNull Context context) {
        if (this.section != null) {
            return (da) a(this.section, context);
        }
        return (da) super.b(context);
    }
}
