package com.my.target;

import android.support.annotation.NonNull;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: AbstractJsCall */
public abstract class bg implements bi {
    @NonNull
    private final JSONObject ch = new JSONObject();
    @NonNull
    JSONObject ci = new JSONObject();
    @NonNull
    private final String type;

    public bg(@NonNull String str) throws JSONException {
        this.type = str;
        this.ch.put("method", str);
        this.ch.put("data", this.ci);
    }

    @NonNull
    public JSONObject aE() {
        return this.ch;
    }
}
