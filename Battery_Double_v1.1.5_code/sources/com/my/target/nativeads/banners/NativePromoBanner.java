package com.my.target.nativeads.banners;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.my.target.co;
import com.my.target.common.models.ImageData;
import com.my.target.cp;
import java.util.ArrayList;
import java.util.List;

public class NativePromoBanner {
    /* access modifiers changed from: private */
    @Nullable
    public String advertisingLabel;
    /* access modifiers changed from: private */
    @Nullable
    public String ageRestrictions;
    @Nullable
    private String category;
    /* access modifiers changed from: private */
    @Nullable
    public String ctaText;
    /* access modifiers changed from: private */
    @Nullable
    public String description;
    /* access modifiers changed from: private */
    @Nullable
    public String disclaimer;
    /* access modifiers changed from: private */
    @Nullable
    public String domain;
    /* access modifiers changed from: private */
    public boolean hasVideo;
    /* access modifiers changed from: private */
    @Nullable
    public ImageData icon;
    /* access modifiers changed from: private */
    @Nullable
    public ImageData image;
    @NonNull
    private ArrayList<NativePromoCard> nativePromoCards;
    /* access modifiers changed from: private */
    @NonNull
    public String navigationType;
    /* access modifiers changed from: private */
    public float rating;
    @Nullable
    private String subCategory;
    /* access modifiers changed from: private */
    @Nullable
    public String title;
    /* access modifiers changed from: private */
    public int votes;

    public static class Builder {
        private final NativePromoBanner nativePromoBanner = new NativePromoBanner();

        @NonNull
        public static Builder createBuilder() {
            return new Builder();
        }

        private Builder() {
        }

        @NonNull
        public Builder setHasVideo(boolean z) {
            this.nativePromoBanner.hasVideo = z;
            return this;
        }

        @NonNull
        public Builder setTitle(@Nullable String str) {
            this.nativePromoBanner.title = str;
            return this;
        }

        @NonNull
        public Builder setDescription(@Nullable String str) {
            this.nativePromoBanner.description = str;
            return this;
        }

        @NonNull
        public Builder setCtaText(@Nullable String str) {
            this.nativePromoBanner.ctaText = str;
            return this;
        }

        @NonNull
        public Builder setDomain(@Nullable String str) {
            this.nativePromoBanner.domain = str;
            return this;
        }

        @NonNull
        public Builder setAdvertisingLabel(@Nullable String str) {
            this.nativePromoBanner.advertisingLabel = str;
            return this;
        }

        @NonNull
        public Builder setIcon(@Nullable ImageData imageData) {
            this.nativePromoBanner.icon = imageData;
            return this;
        }

        @NonNull
        public Builder setImage(@Nullable ImageData imageData) {
            this.nativePromoBanner.image = imageData;
            return this;
        }

        @NonNull
        public Builder setRating(float f) {
            this.nativePromoBanner.rating = f;
            return this;
        }

        @NonNull
        public Builder setAgeRestrictions(@Nullable String str) {
            this.nativePromoBanner.ageRestrictions = str;
            return this;
        }

        @NonNull
        public Builder setDisclaimer(@Nullable String str) {
            this.nativePromoBanner.disclaimer = str;
            return this;
        }

        @NonNull
        public Builder setVotes(int i) {
            this.nativePromoBanner.votes = i;
            return this;
        }

        @NonNull
        public Builder setNavigationType(@NonNull String str) {
            if ("web".equals(str) || "store".equals(str)) {
                this.nativePromoBanner.navigationType = str;
            }
            return this;
        }

        @NonNull
        public NativePromoBanner build() {
            return this.nativePromoBanner;
        }
    }

    @NonNull
    public static NativePromoBanner newBanner(@NonNull co coVar) {
        return new NativePromoBanner(coVar);
    }

    private NativePromoBanner(@NonNull co coVar) {
        this.navigationType = "web";
        this.nativePromoCards = new ArrayList<>();
        this.navigationType = coVar.getNavigationType();
        this.rating = coVar.getRating();
        this.votes = coVar.getVotes();
        this.hasVideo = coVar.getVideoBanner() != null;
        String title2 = coVar.getTitle();
        if (TextUtils.isEmpty(title2)) {
            title2 = null;
        }
        this.title = title2;
        String description2 = coVar.getDescription();
        if (TextUtils.isEmpty(description2)) {
            description2 = null;
        }
        this.description = description2;
        String ctaText2 = coVar.getCtaText();
        if (TextUtils.isEmpty(ctaText2)) {
            ctaText2 = null;
        }
        this.ctaText = ctaText2;
        String disclaimer2 = coVar.getDisclaimer();
        if (TextUtils.isEmpty(disclaimer2)) {
            disclaimer2 = null;
        }
        this.disclaimer = disclaimer2;
        String ageRestrictions2 = coVar.getAgeRestrictions();
        if (TextUtils.isEmpty(ageRestrictions2)) {
            ageRestrictions2 = null;
        }
        this.ageRestrictions = ageRestrictions2;
        String category2 = coVar.getCategory();
        if (TextUtils.isEmpty(category2)) {
            category2 = null;
        }
        this.category = category2;
        String subCategory2 = coVar.getSubCategory();
        if (TextUtils.isEmpty(subCategory2)) {
            subCategory2 = null;
        }
        this.subCategory = subCategory2;
        String domain2 = coVar.getDomain();
        if (TextUtils.isEmpty(domain2)) {
            domain2 = null;
        }
        this.domain = domain2;
        String advertisingLabel2 = coVar.getAdvertisingLabel();
        if (TextUtils.isEmpty(advertisingLabel2)) {
            advertisingLabel2 = null;
        }
        this.advertisingLabel = advertisingLabel2;
        this.image = coVar.getImage();
        this.icon = coVar.getIcon();
        processCards(coVar);
    }

    private NativePromoBanner() {
        this.navigationType = "web";
        this.nativePromoCards = new ArrayList<>();
    }

    private void processCards(@NonNull co coVar) {
        if (!this.hasVideo) {
            List<cp> nativeAdCards = coVar.getNativeAdCards();
            if (!nativeAdCards.isEmpty()) {
                for (cp newCard : nativeAdCards) {
                    this.nativePromoCards.add(NativePromoCard.newCard(newCard));
                }
            }
        }
    }

    @Nullable
    public ImageData getIcon() {
        return this.icon;
    }

    @Nullable
    public String getTitle() {
        return this.title;
    }

    @Nullable
    public String getDescription() {
        return this.description;
    }

    @Nullable
    public String getCtaText() {
        return this.ctaText;
    }

    @Nullable
    public String getDisclaimer() {
        return this.disclaimer;
    }

    @Nullable
    public String getAgeRestrictions() {
        return this.ageRestrictions;
    }

    public float getRating() {
        return this.rating;
    }

    public int getVotes() {
        return this.votes;
    }

    @Nullable
    public String getCategory() {
        return this.category;
    }

    @Nullable
    public String getSubCategory() {
        return this.subCategory;
    }

    @Nullable
    public String getDomain() {
        return this.domain;
    }

    @NonNull
    public String getNavigationType() {
        return this.navigationType;
    }

    @Nullable
    public ImageData getImage() {
        return this.image;
    }

    @Nullable
    public String getAdvertisingLabel() {
        return this.advertisingLabel;
    }

    public boolean hasVideo() {
        return this.hasVideo;
    }

    @NonNull
    public ArrayList<NativePromoCard> getCards() {
        return this.nativePromoCards;
    }
}
