package com.my.target.nativeads.banners;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.my.target.common.models.ImageData;
import com.my.target.cp;

public class NativePromoCard {
    @Nullable
    private final String ctaText;
    @Nullable
    private final String description;
    @Nullable
    private final ImageData image;
    @Nullable
    private final String title;

    @NonNull
    static NativePromoCard newCard(@NonNull cp cpVar) {
        return new NativePromoCard(cpVar);
    }

    private NativePromoCard(@NonNull cp cpVar) {
        if (!TextUtils.isEmpty(cpVar.getTitle())) {
            this.title = cpVar.getTitle();
        } else {
            this.title = null;
        }
        if (!TextUtils.isEmpty(cpVar.getDescription())) {
            this.description = cpVar.getDescription();
        } else {
            this.description = null;
        }
        if (!TextUtils.isEmpty(cpVar.getCtaText())) {
            this.ctaText = cpVar.getCtaText();
        } else {
            this.ctaText = null;
        }
        this.image = cpVar.getImage();
    }

    @Nullable
    public String getTitle() {
        return this.title;
    }

    @Nullable
    public String getDescription() {
        return this.description;
    }

    @Nullable
    public String getCtaText() {
        return this.ctaText;
    }

    @Nullable
    public ImageData getImage() {
        return this.image;
    }
}
