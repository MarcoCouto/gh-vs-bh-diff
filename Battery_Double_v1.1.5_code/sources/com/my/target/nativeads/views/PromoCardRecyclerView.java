package com.my.target.nativeads.views;

import android.content.Context;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.PagerSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout.LayoutParams;
import com.my.target.ah;
import com.my.target.common.models.ImageData;
import com.my.target.fq;
import com.my.target.fx;
import com.my.target.gh;
import com.my.target.gh.a;
import com.my.target.hg;
import com.my.target.nativeads.banners.NativePromoCard;
import java.util.ArrayList;
import java.util.List;

public class PromoCardRecyclerView extends RecyclerView implements gh {
    @NonNull
    private final OnClickListener cardClickListener = new OnClickListener() {
        public void onClick(View view) {
            if (!PromoCardRecyclerView.this.moving) {
                View findContainingItemView = PromoCardRecyclerView.this.layoutManager.findContainingItemView(view);
                if (findContainingItemView != null) {
                    if (!PromoCardRecyclerView.this.layoutManager.g(findContainingItemView)) {
                        int[] calculateDistanceToFinalSnap = PromoCardRecyclerView.this.snapHelper.calculateDistanceToFinalSnap(PromoCardRecyclerView.this.layoutManager, findContainingItemView);
                        if (calculateDistanceToFinalSnap != null) {
                            PromoCardRecyclerView.this.smoothScrollBy(calculateDistanceToFinalSnap[0], 0);
                        }
                    } else if (!(PromoCardRecyclerView.this.onPromoCardSliderListener == null || PromoCardRecyclerView.this.cards == null)) {
                        PromoCardRecyclerView.this.onPromoCardSliderListener.b(findContainingItemView, PromoCardRecyclerView.this.layoutManager.getPosition(findContainingItemView));
                    }
                }
            }
        }
    };
    /* access modifiers changed from: private */
    @Nullable
    public List<NativePromoCard> cards;
    private int displayedCardNum = -1;
    /* access modifiers changed from: private */
    @NonNull
    public final fq layoutManager = new fq(getContext());
    /* access modifiers changed from: private */
    public boolean moving;
    /* access modifiers changed from: private */
    @Nullable
    public a onPromoCardSliderListener;
    @Nullable
    private PromoCardAdapter promoCardAdapter;
    /* access modifiers changed from: private */
    @NonNull
    public final PagerSnapHelper snapHelper;

    public static abstract class PromoCardAdapter extends Adapter<PromoCardViewHolder> {
        @Nullable
        private OnClickListener cardClickListener;
        @NonNull
        private final List<NativePromoCard> nativePromoCards = new ArrayList();

        @NonNull
        public abstract PromoCardView getPromoCardView();

        public void setCards(@NonNull List<NativePromoCard> list) {
            this.nativePromoCards.clear();
            this.nativePromoCards.addAll(list);
            notifyDataSetChanged();
        }

        public void setClickListener(OnClickListener onClickListener) {
            this.cardClickListener = onClickListener;
        }

        @NonNull
        public List<NativePromoCard> getNativePromoCards() {
            return this.nativePromoCards;
        }

        @NonNull
        public PromoCardViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            return new PromoCardViewHolder(getPromoCardView());
        }

        public void onBindViewHolder(@NonNull PromoCardViewHolder promoCardViewHolder, int i) {
            if (i < this.nativePromoCards.size()) {
                NativePromoCard nativePromoCard = (NativePromoCard) this.nativePromoCards.get(i);
                if (nativePromoCard != null) {
                    setBannerToView(nativePromoCard, promoCardViewHolder.getPromoCardView());
                }
            }
            View view = promoCardViewHolder.getPromoCardView().getView();
            StringBuilder sb = new StringBuilder();
            sb.append("card_");
            sb.append(i);
            view.setContentDescription(sb.toString());
            promoCardViewHolder.getPromoCardView().getView().setOnClickListener(this.cardClickListener);
            promoCardViewHolder.getPromoCardView().getCtaButtonView().setOnClickListener(this.cardClickListener);
        }

        public void onViewRecycled(@NonNull PromoCardViewHolder promoCardViewHolder) {
            int layoutPosition = promoCardViewHolder.getLayoutPosition();
            fx fxVar = (fx) promoCardViewHolder.getPromoCardView().getMediaAdView().getImageView();
            fxVar.setImageData(null);
            if (layoutPosition > 0 && layoutPosition < this.nativePromoCards.size()) {
                NativePromoCard nativePromoCard = (NativePromoCard) this.nativePromoCards.get(layoutPosition);
                if (nativePromoCard != null) {
                    ImageData image = nativePromoCard.getImage();
                    if (image != null) {
                        hg.b(image, (ImageView) fxVar);
                    }
                }
            }
            promoCardViewHolder.getPromoCardView().getView().setOnClickListener(null);
            promoCardViewHolder.getPromoCardView().getCtaButtonView().setOnClickListener(null);
            super.onViewRecycled(promoCardViewHolder);
        }

        public int getItemCount() {
            return this.nativePromoCards.size();
        }

        public void dispose() {
            this.cardClickListener = null;
        }

        private void setBannerToView(@NonNull NativePromoCard nativePromoCard, @NonNull PromoCardView promoCardView) {
            if (nativePromoCard.getImage() != null) {
                promoCardView.getMediaAdView().setPlaceHolderDimension(nativePromoCard.getImage().getWidth(), nativePromoCard.getImage().getHeight());
                if (nativePromoCard.getImage().getData() != null) {
                    promoCardView.getMediaAdView().getImageView().setImageBitmap(nativePromoCard.getImage().getData());
                } else {
                    hg.a(nativePromoCard.getImage(), promoCardView.getMediaAdView().getImageView());
                }
            }
            promoCardView.getTitleTextView().setText(nativePromoCard.getTitle());
            promoCardView.getDescriptionTextView().setText(nativePromoCard.getDescription());
            String ctaText = nativePromoCard.getCtaText();
            promoCardView.getCtaButtonView().setText(ctaText);
            promoCardView.getCtaButtonView().setContentDescription(ctaText);
        }
    }

    static class PromoCardViewHolder extends ViewHolder {
        @NonNull
        private final PromoCardView promoCardView;

        PromoCardViewHolder(@NonNull PromoCardView promoCardView2) {
            super(promoCardView2.getView());
            promoCardView2.getView().setLayoutParams(new LayoutParams(-1, -2));
            this.promoCardView = promoCardView2;
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        public PromoCardView getPromoCardView() {
            return this.promoCardView;
        }
    }

    public PromoCardRecyclerView(Context context) {
        super(context);
        setHasFixedSize(true);
        this.snapHelper = new PagerSnapHelper();
        this.snapHelper.attachToRecyclerView(this);
    }

    public PromoCardRecyclerView(Context context, @Nullable AttributeSet attributeSet) {
        super(context, attributeSet);
        setHasFixedSize(true);
        this.snapHelper = new PagerSnapHelper();
        this.snapHelper.attachToRecyclerView(this);
    }

    public PromoCardRecyclerView(Context context, @Nullable AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        setHasFixedSize(true);
        this.snapHelper = new PagerSnapHelper();
        this.snapHelper.attachToRecyclerView(this);
    }

    @NonNull
    public int[] getVisibleCardNumbers() {
        int findFirstCompletelyVisibleItemPosition = this.layoutManager.findFirstCompletelyVisibleItemPosition();
        int findLastCompletelyVisibleItemPosition = this.layoutManager.findLastCompletelyVisibleItemPosition();
        if (this.cards == null || findFirstCompletelyVisibleItemPosition > findLastCompletelyVisibleItemPosition || findFirstCompletelyVisibleItemPosition < 0 || findLastCompletelyVisibleItemPosition >= this.cards.size()) {
            return new int[0];
        }
        int[] iArr = new int[((findLastCompletelyVisibleItemPosition - findFirstCompletelyVisibleItemPosition) + 1)];
        for (int i = 0; i < iArr.length; i++) {
            iArr[i] = findFirstCompletelyVisibleItemPosition;
            findFirstCompletelyVisibleItemPosition++;
        }
        return iArr;
    }

    public void setAdapter(Adapter adapter) {
        if (adapter instanceof PromoCardAdapter) {
            setPromoCardAdapter((PromoCardAdapter) adapter);
        } else {
            ah.a("You must use setPromoCardAdapter(PromoCardAdapter) method with custom CardRecyclerView");
        }
    }

    public void setPromoCardAdapter(@Nullable PromoCardAdapter promoCardAdapter2) {
        if (promoCardAdapter2 != null) {
            this.cards = promoCardAdapter2.getNativePromoCards();
            this.promoCardAdapter = promoCardAdapter2;
            this.promoCardAdapter.setClickListener(this.cardClickListener);
            setLayoutManager(this.layoutManager);
            super.swapAdapter(this.promoCardAdapter, true);
        }
    }

    public void setPromoCardSliderListener(@Nullable a aVar) {
        this.onPromoCardSliderListener = aVar;
    }

    public void dispose() {
        if (this.promoCardAdapter != null) {
            this.promoCardAdapter.dispose();
        }
    }

    @Nullable
    public Parcelable getState() {
        return this.layoutManager.onSaveInstanceState();
    }

    public void restoreState(@NonNull Parcelable parcelable) {
        this.layoutManager.onRestoreInstanceState(parcelable);
    }

    public void onScrollStateChanged(int i) {
        super.onScrollStateChanged(i);
        this.moving = i != 0;
        if (!this.moving) {
            checkCardChanged();
        }
    }

    private void checkCardChanged() {
        int findFirstCompletelyVisibleItemPosition = this.layoutManager.findFirstCompletelyVisibleItemPosition();
        if (findFirstCompletelyVisibleItemPosition >= 0 && this.displayedCardNum != findFirstCompletelyVisibleItemPosition) {
            this.displayedCardNum = findFirstCompletelyVisibleItemPosition;
            if (!(this.onPromoCardSliderListener == null || this.cards == null)) {
                View findViewByPosition = this.layoutManager.findViewByPosition(this.displayedCardNum);
                if (findViewByPosition != null) {
                    this.onPromoCardSliderListener.b(findViewByPosition, new int[]{this.displayedCardNum});
                }
            }
        }
    }
}
