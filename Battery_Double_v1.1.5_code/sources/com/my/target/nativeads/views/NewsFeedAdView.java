package com.my.target.nativeads.views;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.GradientDrawable.Orientation;
import android.graphics.drawable.StateListDrawable;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewCompat;
import android.text.TextUtils;
import android.text.TextUtils.TruncateAt;
import android.util.AttributeSet;
import android.util.StateSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import com.my.target.R;
import com.my.target.ah;
import com.my.target.common.models.ImageData;
import com.my.target.fp;
import com.my.target.fx;
import com.my.target.fy;
import com.my.target.hg;
import com.my.target.hm;
import com.my.target.nativeads.banners.NativePromoBanner;

public class NewsFeedAdView extends RelativeLayout {
    private static final int COLOR_PLACEHOLDER_GRAY = -1118482;
    private static final int LABELS_ID = hm.dV();
    private static final int RATING_ID = hm.dV();
    private static final int STANDARD_BLUE = -16748844;
    private static final int STANDARD_GREY = -6710887;
    private static final int TITLE_2_ID = hm.dV();
    @NonNull
    private final TextView advertisingLabel;
    @NonNull
    private final fp ageRestrictionLabel;
    @Nullable
    private NativePromoBanner banner;
    @NonNull
    private final Button ctaButton;
    @Nullable
    private LayoutParams ctaParams;
    @NonNull
    private final TextView disclaimerLabel;
    @Nullable
    private LayoutParams disclaimerParams;
    @NonNull
    private final fx iconImageView;
    @NonNull
    private final LinearLayout labelsLayout;
    @NonNull
    private final LinearLayout ratingLayout;
    @NonNull
    private final fy starsView;
    @NonNull
    private final TextView titleLabel;
    @NonNull
    private final hm uiUtils;
    @NonNull
    private final TextView urlLabel;
    @Nullable
    private LayoutParams urlLabelParams;
    @NonNull
    private final TextView votesLabel;

    public NewsFeedAdView(@NonNull Context context) {
        this(context, null);
    }

    public NewsFeedAdView(@NonNull Context context, @Nullable AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public NewsFeedAdView(@NonNull Context context, @Nullable AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.ageRestrictionLabel = new fp(context);
        this.advertisingLabel = new TextView(context);
        this.iconImageView = new fx(context);
        this.labelsLayout = new LinearLayout(context);
        this.titleLabel = new TextView(context);
        this.urlLabel = new TextView(context);
        this.ratingLayout = new LinearLayout(context);
        this.starsView = new fy(context);
        this.votesLabel = new TextView(context);
        this.disclaimerLabel = new TextView(context);
        this.ctaButton = new Button(context);
        this.uiUtils = hm.R(context);
        setId(R.id.nativeads_ad_view);
        this.ageRestrictionLabel.setId(R.id.nativeads_age_restrictions);
        this.advertisingLabel.setId(R.id.nativeads_advertising);
        this.iconImageView.setId(R.id.nativeads_icon);
        this.labelsLayout.setId(LABELS_ID);
        this.titleLabel.setId(R.id.nativeads_title);
        this.urlLabel.setId(R.id.nativeads_domain);
        this.ratingLayout.setId(RATING_ID);
        this.starsView.setId(R.id.nativeads_rating);
        this.votesLabel.setId(R.id.nativeads_votes);
        this.disclaimerLabel.setId(R.id.nativeads_disclaimer);
        this.ctaButton.setId(R.id.nativeads_call_to_action);
        initView();
    }

    @NonNull
    public TextView getAgeRestrictionTextView() {
        return this.ageRestrictionLabel;
    }

    @NonNull
    public TextView getAdvertisingTextView() {
        return this.advertisingLabel;
    }

    @NonNull
    public ImageView getIconImageView() {
        return this.iconImageView;
    }

    @NonNull
    public TextView getTitleTextView() {
        return this.titleLabel;
    }

    @NonNull
    public TextView getDomainOrCategoryTextView() {
        return this.urlLabel;
    }

    @NonNull
    public Button getCtaButtonView() {
        return this.ctaButton;
    }

    @NonNull
    public TextView getVotesTextView() {
        return this.votesLabel;
    }

    @NonNull
    public fy getStarsRatingView() {
        return this.starsView;
    }

    @NonNull
    public TextView getDisclaimerTextView() {
        return this.disclaimerLabel;
    }

    /* JADX WARNING: Removed duplicated region for block: B:39:0x0142  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x0174  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x017e  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x019c  */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x021d  */
    public void setupView(@Nullable NativePromoBanner nativePromoBanner) {
        boolean z;
        String disclaimer;
        if (nativePromoBanner != null) {
            this.banner = nativePromoBanner;
            ah.a("Setup banner");
            if ("web".equals(nativePromoBanner.getNavigationType())) {
                this.urlLabel.setVisibility(0);
                this.ratingLayout.setVisibility(8);
                this.urlLabel.setText(nativePromoBanner.getDomain());
            } else if ("store".equals(nativePromoBanner.getNavigationType())) {
                String category = nativePromoBanner.getCategory();
                String subCategory = nativePromoBanner.getSubCategory();
                String str = "";
                if (!TextUtils.isEmpty(category)) {
                    StringBuilder sb = new StringBuilder();
                    sb.append(str);
                    sb.append(category);
                    str = sb.toString();
                }
                if (!TextUtils.isEmpty(str) && !TextUtils.isEmpty(subCategory)) {
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append(str);
                    sb2.append(", ");
                    str = sb2.toString();
                }
                if (!TextUtils.isEmpty(subCategory)) {
                    StringBuilder sb3 = new StringBuilder();
                    sb3.append(str);
                    sb3.append(subCategory);
                    str = sb3.toString();
                }
                if (nativePromoBanner.getRating() <= 0.0f || nativePromoBanner.getRating() > 5.0f) {
                    this.urlLabel.setVisibility(0);
                    this.urlLabel.setText(str);
                    hm.a((View) this.urlLabel, "category_text");
                    this.ratingLayout.setVisibility(8);
                } else {
                    this.starsView.setRating(nativePromoBanner.getRating());
                    this.votesLabel.setText(String.valueOf(nativePromoBanner.getVotes()));
                    this.urlLabel.setVisibility(8);
                    ViewParent parent = this.ratingLayout.getParent();
                    if (parent != null) {
                        ((ViewGroup) parent).removeView(this.ratingLayout);
                    }
                    this.labelsLayout.addView(this.ratingLayout, 1);
                    this.ratingLayout.setVisibility(0);
                    this.starsView.setVisibility(0);
                    if (nativePromoBanner.getVotes() > 0) {
                        this.votesLabel.setVisibility(0);
                    } else {
                        this.votesLabel.setVisibility(8);
                    }
                    this.urlLabel.setVisibility(8);
                    hm.a((View) this.votesLabel, "votes_text");
                }
            }
            ImageData icon = nativePromoBanner.getIcon();
            if (icon != null) {
                if (icon.getData() != null) {
                    this.iconImageView.setImageBitmap(icon.getData());
                    z = true;
                    if (!z) {
                        this.iconImageView.setImageData(null);
                        this.iconImageView.setContentDescription("");
                    }
                    this.titleLabel.setText(nativePromoBanner.getTitle());
                    this.advertisingLabel.setText(nativePromoBanner.getAdvertisingLabel());
                    this.ctaButton.setText(nativePromoBanner.getCtaText());
                    if (TextUtils.isEmpty(nativePromoBanner.getAgeRestrictions())) {
                        this.ageRestrictionLabel.setText(nativePromoBanner.getAgeRestrictions());
                    } else {
                        this.ageRestrictionLabel.setVisibility(8);
                    }
                    disclaimer = nativePromoBanner.getDisclaimer();
                    ViewParent parent2 = this.ctaButton.getParent();
                    if (TextUtils.isEmpty(disclaimer)) {
                        ViewParent parent3 = this.disclaimerLabel.getParent();
                        if (parent3 != null) {
                            ((ViewGroup) parent3).removeView(this.disclaimerLabel);
                        }
                        if (parent2 != null) {
                            ((ViewGroup) parent2).removeView(this.ctaButton);
                        }
                        RelativeLayout relativeLayout = new RelativeLayout(getContext());
                        relativeLayout.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
                        this.disclaimerLabel.setText(disclaimer);
                        this.disclaimerParams = new LayoutParams(-2, -2);
                        this.disclaimerParams.addRule(9);
                        this.disclaimerParams.addRule(0, this.ctaButton.getId());
                        this.disclaimerParams.rightMargin = this.uiUtils.E(3);
                        this.disclaimerLabel.setLayoutParams(this.disclaimerParams);
                        this.ctaParams = new LayoutParams(-2, this.uiUtils.E(30));
                        this.ctaParams.addRule(11);
                        this.ctaButton.setLayoutParams(this.ctaParams);
                        relativeLayout.addView(this.disclaimerLabel);
                        relativeLayout.addView(this.ctaButton);
                        this.labelsLayout.addView(relativeLayout, 2);
                    } else {
                        this.disclaimerLabel.setVisibility(8);
                        if ("web".equals(nativePromoBanner.getNavigationType()) && !TextUtils.isEmpty(nativePromoBanner.getDomain())) {
                            ViewParent parent4 = this.urlLabel.getParent();
                            if (parent4 != null) {
                                ((ViewGroup) parent4).removeView(this.urlLabel);
                            }
                            if (parent2 != null) {
                                ((ViewGroup) parent2).removeView(this.ctaButton);
                            }
                            RelativeLayout relativeLayout2 = new RelativeLayout(getContext());
                            relativeLayout2.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
                            this.urlLabelParams = new LayoutParams(-2, -2);
                            this.urlLabelParams.addRule(9);
                            this.urlLabelParams.addRule(0, this.ctaButton.getId());
                            this.urlLabelParams.rightMargin = this.uiUtils.E(3);
                            this.urlLabel.setLayoutParams(this.urlLabelParams);
                            this.ctaParams = new LayoutParams(-2, this.uiUtils.E(30));
                            this.ctaParams.addRule(11);
                            this.ctaParams.topMargin = this.uiUtils.E(9);
                            this.ctaButton.setLayoutParams(this.ctaParams);
                            relativeLayout2.addView(this.urlLabel);
                            relativeLayout2.addView(this.ctaButton);
                            this.labelsLayout.addView(relativeLayout2, 1);
                        }
                    }
                }
                this.iconImageView.setBackgroundColor(-1118482);
                this.iconImageView.setPlaceholderWidth(icon.getWidth());
                this.iconImageView.setPlaceholderHeight(icon.getHeight());
            }
            z = false;
            if (!z) {
            }
            this.titleLabel.setText(nativePromoBanner.getTitle());
            this.advertisingLabel.setText(nativePromoBanner.getAdvertisingLabel());
            this.ctaButton.setText(nativePromoBanner.getCtaText());
            if (TextUtils.isEmpty(nativePromoBanner.getAgeRestrictions())) {
            }
            disclaimer = nativePromoBanner.getDisclaimer();
            ViewParent parent22 = this.ctaButton.getParent();
            if (TextUtils.isEmpty(disclaimer)) {
            }
        }
    }

    public void loadImages() {
        if (this.banner != null) {
            ImageData icon = this.banner.getIcon();
            if (icon != null) {
                if (icon.getData() == null) {
                    hg.a(icon, (ImageView) this.iconImageView);
                } else {
                    this.iconImageView.setImageBitmap(icon.getData());
                }
            }
        }
    }

    private void initView() {
        setPadding(this.uiUtils.E(12), this.uiUtils.E(12), this.uiUtils.E(12), this.uiUtils.E(12));
        this.ageRestrictionLabel.f(1, -7829368);
        this.ageRestrictionLabel.setPadding(this.uiUtils.E(2), 0, 0, 0);
        LayoutParams layoutParams = new LayoutParams(-2, -2);
        layoutParams.rightMargin = this.uiUtils.E(9);
        this.ageRestrictionLabel.setLayoutParams(layoutParams);
        LayoutParams layoutParams2 = new LayoutParams(-2, -2);
        layoutParams2.addRule(1, R.id.nativeads_age_restrictions);
        this.advertisingLabel.setLayoutParams(layoutParams2);
        LayoutParams layoutParams3 = new LayoutParams(this.uiUtils.E(54), this.uiUtils.E(54));
        layoutParams3.addRule(3, R.id.nativeads_advertising);
        layoutParams3.topMargin = this.uiUtils.E(9);
        this.iconImageView.setLayoutParams(layoutParams3);
        this.labelsLayout.setOrientation(1);
        this.labelsLayout.setMinimumHeight(this.uiUtils.E(54));
        LayoutParams layoutParams4 = new LayoutParams(-1, -2);
        layoutParams4.addRule(3, R.id.nativeads_advertising);
        layoutParams4.addRule(1, R.id.nativeads_icon);
        layoutParams4.leftMargin = this.uiUtils.E(9);
        layoutParams4.topMargin = this.uiUtils.E(3);
        this.labelsLayout.setLayoutParams(layoutParams4);
        this.titleLabel.setLayoutParams(new LayoutParams(-2, -2));
        this.urlLabelParams = new LayoutParams(-2, -2);
        this.urlLabelParams.topMargin = this.uiUtils.E(2);
        this.urlLabel.setLayoutParams(this.urlLabelParams);
        this.ratingLayout.setOrientation(0);
        LayoutParams layoutParams5 = new LayoutParams(-2, -2);
        layoutParams5.addRule(3, TITLE_2_ID);
        this.ratingLayout.setLayoutParams(layoutParams5);
        LinearLayout.LayoutParams layoutParams6 = new LinearLayout.LayoutParams(this.uiUtils.E(73), this.uiUtils.E(12));
        layoutParams6.topMargin = this.uiUtils.E(4);
        layoutParams6.rightMargin = this.uiUtils.E(4);
        this.starsView.setLayoutParams(layoutParams6);
        this.disclaimerParams = new LayoutParams(-2, -2);
        this.disclaimerParams.addRule(3, RATING_ID);
        this.disclaimerLabel.setLayoutParams(this.disclaimerParams);
        this.ctaButton.setPadding(this.uiUtils.E(10), 0, this.uiUtils.E(10), 0);
        this.ctaButton.setTransformationMethod(null);
        this.ctaButton.setMaxEms(8);
        this.ctaButton.setLines(1);
        this.ctaButton.setEllipsize(TruncateAt.END);
        this.ctaParams = new LayoutParams(-2, this.uiUtils.E(30));
        this.ctaParams.addRule(3, LABELS_ID);
        this.ctaParams.addRule(11);
        this.ctaParams.topMargin = -this.uiUtils.E(23);
        this.ctaButton.setLayoutParams(this.ctaParams);
        hm.a(this, 0, -3806472);
        GradientDrawable gradientDrawable = new GradientDrawable(Orientation.TOP_BOTTOM, new int[]{0, 0});
        gradientDrawable.setStroke(this.uiUtils.E(1), STANDARD_BLUE);
        gradientDrawable.setCornerRadius((float) this.uiUtils.E(1));
        GradientDrawable gradientDrawable2 = new GradientDrawable(Orientation.TOP_BOTTOM, new int[]{-3806472, -3806472});
        gradientDrawable2.setStroke(this.uiUtils.E(1), STANDARD_BLUE);
        gradientDrawable2.setCornerRadius((float) this.uiUtils.E(1));
        StateListDrawable stateListDrawable = new StateListDrawable();
        stateListDrawable.addState(new int[]{16842919}, gradientDrawable2);
        stateListDrawable.addState(StateSet.WILD_CARD, gradientDrawable);
        if (VERSION.SDK_INT >= 16) {
            this.ctaButton.setBackground(stateListDrawable);
        } else {
            this.ctaButton.setBackgroundDrawable(stateListDrawable);
        }
        setClickable(true);
        addView(this.ageRestrictionLabel);
        addView(this.advertisingLabel);
        addView(this.iconImageView);
        addView(this.labelsLayout);
        this.labelsLayout.addView(this.titleLabel);
        this.labelsLayout.addView(this.urlLabel);
        addView(this.ctaButton);
        addView(this.ratingLayout);
        addView(this.disclaimerLabel);
        this.ratingLayout.addView(this.starsView);
        this.ratingLayout.addView(this.votesLabel);
        updateDefaultParams();
    }

    private void updateDefaultParams() {
        this.ageRestrictionLabel.setTextColor(STANDARD_GREY);
        this.ageRestrictionLabel.f(1, STANDARD_GREY);
        this.ageRestrictionLabel.setBackgroundColor(0);
        this.advertisingLabel.setTextSize(2, 14.0f);
        this.advertisingLabel.setTextColor(STANDARD_GREY);
        this.titleLabel.setTextColor(ViewCompat.MEASURED_STATE_MASK);
        this.titleLabel.setTextSize(2, 16.0f);
        this.titleLabel.setTypeface(null, 1);
        this.urlLabel.setTextColor(STANDARD_GREY);
        this.urlLabel.setTextSize(2, 14.0f);
        this.votesLabel.setTextColor(STANDARD_GREY);
        this.votesLabel.setTextSize(2, 14.0f);
        this.disclaimerLabel.setTextColor(STANDARD_GREY);
        this.disclaimerLabel.setTextSize(2, 12.0f);
        this.ctaButton.setTextColor(STANDARD_BLUE);
    }
}
