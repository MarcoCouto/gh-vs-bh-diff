package com.my.target.nativeads.views;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.GradientDrawable.Orientation;
import android.graphics.drawable.StateListDrawable;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.StateSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import com.my.target.hm;
import com.my.target.nativeads.NativeAppwallAd;
import com.my.target.nativeads.banners.NativeAppwallBanner;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class AppwallAdView extends FrameLayout implements OnGlobalLayoutListener, OnScrollListener, OnItemClickListener {
    private static final int BACKGROUND_COLOR = -1118482;
    @Nullable
    private AppwallAdViewListener appwallAdViewListener;
    @NonNull
    private final ListView listView;
    @NonNull
    private final hm uiUtils;
    @NonNull
    private final HashMap<NativeAppwallBanner, Boolean> viewMap = new HashMap<>();

    public interface AppwallAdViewListener {
        void onBannerClick(@NonNull NativeAppwallBanner nativeAppwallBanner);

        void onBannersShow(@NonNull List<NativeAppwallBanner> list);
    }

    public static class AppwallAdapter extends ArrayAdapter<NativeAppwallBanner> {
        AppwallAdapter(@NonNull Context context, @NonNull List<NativeAppwallBanner> list) {
            super(context, 0, list);
        }

        @NonNull
        public View getView(int i, View view, @NonNull ViewGroup viewGroup) {
            NativeAppwallBanner nativeAppwallBanner = (NativeAppwallBanner) getItem(i);
            if (view == null) {
                view = new AppwallCardPlaceholder(new AppwallAdTeaserView(getContext()), getContext());
            }
            if (nativeAppwallBanner != null) {
                ((AppwallCardPlaceholder) view).getView().setNativeAppwallBanner(nativeAppwallBanner);
            }
            return view;
        }
    }

    @SuppressLint({"ViewConstructor"})
    public static class AppwallCardPlaceholder extends FrameLayout {
        @NonNull
        private final LinearLayout rootLayout;
        @NonNull
        private final AppwallAdTeaserView view;

        public AppwallCardPlaceholder(@NonNull AppwallAdTeaserView appwallAdTeaserView, Context context) {
            super(context);
            hm R = hm.R(context);
            this.view = appwallAdTeaserView;
            int E = R.E(9);
            int E2 = R.E(4);
            int E3 = R.E(2);
            this.rootLayout = new LinearLayout(context);
            this.rootLayout.setOrientation(1);
            this.rootLayout.setBackgroundColor(-1118482);
            LayoutParams layoutParams = new LayoutParams(-1, -2);
            layoutParams.setMargins(E, E2, E, E2);
            appwallAdTeaserView.setLayoutParams(layoutParams);
            this.rootLayout.addView(appwallAdTeaserView);
            if (VERSION.SDK_INT >= 21) {
                appwallAdTeaserView.setElevation((float) E3);
                GradientDrawable gradientDrawable = new GradientDrawable(Orientation.TOP_BOTTOM, new int[]{-1, -1});
                GradientDrawable gradientDrawable2 = new GradientDrawable(Orientation.TOP_BOTTOM, new int[]{-1118482, -1118482});
                StateListDrawable stateListDrawable = new StateListDrawable();
                stateListDrawable.addState(new int[]{16842919}, gradientDrawable2);
                stateListDrawable.addState(StateSet.WILD_CARD, gradientDrawable);
                appwallAdTeaserView.setBackground(stateListDrawable);
            } else {
                View view2 = new View(context);
                LayoutParams layoutParams2 = new LayoutParams(-1, E2);
                view2.setBackgroundDrawable(new GradientDrawable(Orientation.TOP_BOTTOM, new int[]{-3355444, -1118482}));
                view2.setLayoutParams(layoutParams2);
                this.rootLayout.addView(view2);
                GradientDrawable gradientDrawable3 = new GradientDrawable(Orientation.TOP_BOTTOM, new int[]{-1, -1});
                gradientDrawable3.setStroke(1, -3355444);
                GradientDrawable gradientDrawable4 = new GradientDrawable(Orientation.TOP_BOTTOM, new int[]{-1118482, -1118482});
                gradientDrawable4.setStroke(1, -3355444);
                StateListDrawable stateListDrawable2 = new StateListDrawable();
                stateListDrawable2.addState(new int[]{16842919}, gradientDrawable4);
                stateListDrawable2.addState(StateSet.WILD_CARD, gradientDrawable3);
                appwallAdTeaserView.setBackgroundDrawable(stateListDrawable2);
                layoutParams.setMargins(0, E2, 0, 0);
                this.rootLayout.setPadding(E, 0, E, 0);
            }
            addView(this.rootLayout, -2, -2);
        }

        @NonNull
        public AppwallAdTeaserView getView() {
            return this.view;
        }
    }

    public void onScrollStateChanged(AbsListView absListView, int i) {
    }

    public AppwallAdView(@NonNull Context context) {
        super(context);
        setVerticalFadingEdgeEnabled(false);
        setBackgroundColor(-1);
        this.uiUtils = hm.R(context);
        this.listView = new ListView(context);
        initLayout();
    }

    public void setAppwallAdViewListener(@Nullable AppwallAdViewListener appwallAdViewListener2) {
        this.appwallAdViewListener = appwallAdViewListener2;
    }

    public void onScroll(AbsListView absListView, int i, int i2, int i3) {
        countVisibleBanners();
    }

    public void setupView(@NonNull NativeAppwallAd nativeAppwallAd) {
        this.listView.setAdapter(new AppwallAdapter(getContext(), nativeAppwallAd.getBanners()));
    }

    public void notifyDataSetChanged() {
        ((AppwallAdapter) this.listView.getAdapter()).notifyDataSetChanged();
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
        NativeAppwallBanner nativeAppwallBanner = (NativeAppwallBanner) this.listView.getAdapter().getItem(i);
        if (this.appwallAdViewListener != null) {
            this.appwallAdViewListener.onBannerClick(nativeAppwallBanner);
        }
    }

    public void onGlobalLayout() {
        countVisibleBanners();
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        countVisibleBanners();
        ViewTreeObserver viewTreeObserver = getViewTreeObserver();
        if (viewTreeObserver.isAlive()) {
            viewTreeObserver.addOnGlobalLayoutListener(this);
        }
    }

    private void initLayout() {
        int E = this.uiUtils.E(4);
        int E2 = this.uiUtils.E(4);
        this.listView.setDividerHeight(0);
        this.listView.setVerticalFadingEdgeEnabled(false);
        this.listView.setOnItemClickListener(this);
        this.listView.setOnScrollListener(this);
        this.listView.setPadding(0, E, 0, E2);
        this.listView.setClipToPadding(false);
        addView(this.listView, -1, -1);
        this.listView.setBackgroundColor(-1118482);
    }

    private void countVisibleBanners() {
        if (this.listView.getAdapter() != null) {
            int lastVisiblePosition = this.listView.getLastVisiblePosition();
            ArrayList arrayList = new ArrayList();
            for (int firstVisiblePosition = this.listView.getFirstVisiblePosition(); firstVisiblePosition <= lastVisiblePosition; firstVisiblePosition++) {
                NativeAppwallBanner nativeAppwallBanner = (NativeAppwallBanner) this.listView.getAdapter().getItem(firstVisiblePosition);
                if (this.viewMap.get(nativeAppwallBanner) == null) {
                    arrayList.add(nativeAppwallBanner);
                    this.viewMap.put(nativeAppwallBanner, Boolean.valueOf(true));
                }
            }
            if (arrayList.size() > 0 && this.appwallAdViewListener != null) {
                this.appwallAdViewListener.onBannersShow(arrayList);
            }
        }
    }
}
