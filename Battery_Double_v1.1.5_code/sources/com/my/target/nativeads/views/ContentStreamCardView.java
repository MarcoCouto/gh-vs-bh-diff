package com.my.target.nativeads.views;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.GradientDrawable.Orientation;
import android.graphics.drawable.StateListDrawable;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewCompat;
import android.text.TextUtils.TruncateAt;
import android.util.StateSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import com.my.target.hm;

public class ContentStreamCardView extends LinearLayout implements PromoCardView {
    private static final int CTA_ID = hm.dV();
    private static final int MARGIN_DP = 12;
    private static final int MEDIA_ID = hm.dV();
    private static final int STANDARD_BLUE = -16748844;
    /* access modifiers changed from: private */
    @Nullable
    public OnClickListener cardClickListener;
    @NonNull
    private final RelativeLayout containerLayout;
    @NonNull
    private final Button ctaButton;
    @NonNull
    private final TextView descriptionView;
    @NonNull
    private final OnClickListener elementClickListener = new OnClickListener() {
        public void onClick(View view) {
            if (ContentStreamCardView.this.cardClickListener != null) {
                ContentStreamCardView.this.cardClickListener.onClick(ContentStreamCardView.this);
            }
        }
    };
    @NonNull
    private final MediaAdView mediaAdView;
    @NonNull
    private final LinearLayout textContainerLayout;
    @NonNull
    private final TextView titleView;
    @NonNull
    private final hm uiUtils;

    @NonNull
    public View getView() {
        return this;
    }

    public ContentStreamCardView(@NonNull Context context) {
        super(context);
        this.mediaAdView = new MediaAdView(context);
        this.titleView = new TextView(context);
        this.descriptionView = new TextView(context);
        this.containerLayout = new RelativeLayout(context);
        this.ctaButton = new Button(context);
        this.uiUtils = hm.R(context);
        this.textContainerLayout = new LinearLayout(context);
        hm.a((View) this, "card_view");
        hm.a((View) this.mediaAdView, "card_media_view");
        hm.a((View) this.titleView, "card_title_text");
        hm.a((View) this.descriptionView, "card_description_text");
        hm.a((View) this.ctaButton, "card_cta_text");
        initView();
    }

    @NonNull
    public MediaAdView getMediaAdView() {
        return this.mediaAdView;
    }

    @NonNull
    public TextView getTitleTextView() {
        return this.titleView;
    }

    public void setOnClickListener(@Nullable OnClickListener onClickListener) {
        this.cardClickListener = onClickListener;
        super.setOnClickListener(onClickListener);
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            getChildAt(i).setOnClickListener(this.elementClickListener);
        }
    }

    @NonNull
    public TextView getDescriptionTextView() {
        return this.descriptionView;
    }

    @NonNull
    public Button getCtaButtonView() {
        return this.ctaButton;
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        float x = motionEvent.getX();
        float y = motionEvent.getY();
        return x > ((float) this.mediaAdView.getLeft()) && x < ((float) this.mediaAdView.getRight()) && y > ((float) this.mediaAdView.getTop()) && y < ((float) this.mediaAdView.getBottom());
    }

    private void initView() {
        this.mediaAdView.setId(MEDIA_ID);
        setOrientation(1);
        setPadding(this.uiUtils.E(8), this.uiUtils.E(8), this.uiUtils.E(8), this.uiUtils.E(8));
        setClickable(true);
        hm.a(this, 0, -3806472);
        hm.a(this.containerLayout, 0, -3806472, -3355444, this.uiUtils.E(1), 0);
        this.ctaButton.setId(CTA_ID);
        this.ctaButton.setMaxEms(10);
        this.ctaButton.setLines(1);
        this.ctaButton.setEllipsize(TruncateAt.END);
        this.ctaButton.setPadding(this.uiUtils.E(10), 0, this.uiUtils.E(10), 0);
        this.ctaButton.setTextSize(2, 12.0f);
        LayoutParams layoutParams = new LayoutParams(-2, this.uiUtils.E(30));
        layoutParams.addRule(11, -1);
        layoutParams.addRule(15, -1);
        layoutParams.setMargins(this.uiUtils.E(12), this.uiUtils.E(12), this.uiUtils.E(12), this.uiUtils.E(12));
        this.ctaButton.setLayoutParams(layoutParams);
        this.ctaButton.setTransformationMethod(null);
        if (VERSION.SDK_INT >= 21) {
            this.ctaButton.setStateListAnimator(null);
        }
        this.ctaButton.setTextColor(STANDARD_BLUE);
        GradientDrawable gradientDrawable = new GradientDrawable(Orientation.TOP_BOTTOM, new int[]{0, 0});
        gradientDrawable.setStroke(this.uiUtils.E(1), STANDARD_BLUE);
        gradientDrawable.setCornerRadius((float) this.uiUtils.E(1));
        GradientDrawable gradientDrawable2 = new GradientDrawable(Orientation.TOP_BOTTOM, new int[]{-3806472, -3806472});
        gradientDrawable2.setStroke(this.uiUtils.E(1), STANDARD_BLUE);
        gradientDrawable2.setCornerRadius((float) this.uiUtils.E(1));
        StateListDrawable stateListDrawable = new StateListDrawable();
        stateListDrawable.addState(new int[]{16842919}, gradientDrawable2);
        stateListDrawable.addState(StateSet.WILD_CARD, gradientDrawable);
        this.ctaButton.setBackgroundDrawable(stateListDrawable);
        LayoutParams layoutParams2 = new LayoutParams(-1, -2);
        layoutParams2.addRule(0, CTA_ID);
        this.textContainerLayout.setLayoutParams(layoutParams2);
        this.textContainerLayout.setGravity(16);
        this.textContainerLayout.setOrientation(1);
        this.titleView.setTextColor(ViewCompat.MEASURED_STATE_MASK);
        this.titleView.setTextSize(2, 14.0f);
        this.titleView.setTypeface(null, 1);
        this.titleView.setLines(2);
        this.titleView.setEllipsize(TruncateAt.END);
        this.titleView.setPadding(this.uiUtils.E(12), this.uiUtils.E(6), this.uiUtils.E(1), this.uiUtils.E(1));
        this.descriptionView.setTextColor(ViewCompat.MEASURED_STATE_MASK);
        this.descriptionView.setTextSize(2, 12.0f);
        this.descriptionView.setLines(1);
        this.descriptionView.setEllipsize(TruncateAt.END);
        this.descriptionView.setPadding(this.uiUtils.E(12), this.uiUtils.E(1), this.uiUtils.E(1), this.uiUtils.E(12));
        addView(this.mediaAdView);
        addView(this.containerLayout);
        this.containerLayout.addView(this.ctaButton);
        this.containerLayout.addView(this.textContainerLayout);
        this.textContainerLayout.addView(this.titleView);
        this.textContainerLayout.addView(this.descriptionView);
    }
}
