package com.my.target.nativeads.views;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import com.my.target.R;
import com.my.target.ah;
import com.my.target.fp;
import com.my.target.fy;
import com.my.target.hm;
import com.my.target.nativeads.banners.NativePromoBanner;
import com.my.target.nativeads.factories.NativeViewsFactory;

public class ContentWallAdView extends RelativeLayout {
    @NonNull
    private final TextView advertisingLabel;
    @NonNull
    private final fp ageRestrictionLabel;
    @NonNull
    private final MediaAdView mediaAdView;
    @NonNull
    private final fy starsView;
    @NonNull
    private final hm uiUtils;
    @NonNull
    private final TextView votesLabel;

    public ContentWallAdView(@NonNull Context context) {
        this(context, null);
    }

    public ContentWallAdView(@NonNull Context context, @Nullable AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public ContentWallAdView(@NonNull Context context, @Nullable AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.ageRestrictionLabel = new fp(context);
        this.advertisingLabel = new TextView(context);
        this.mediaAdView = NativeViewsFactory.getMediaAdView(context);
        this.starsView = new fy(context);
        this.votesLabel = new TextView(context);
        this.uiUtils = hm.R(context);
        hm.a((View) this.votesLabel, "votes_text");
        initView();
    }

    @NonNull
    public TextView getAdvertisingTextView() {
        return this.advertisingLabel;
    }

    @NonNull
    public TextView getAgeRestrictionTextView() {
        return this.ageRestrictionLabel;
    }

    @NonNull
    public MediaAdView getMediaAdView() {
        return this.mediaAdView;
    }

    public void setupView(@Nullable NativePromoBanner nativePromoBanner) {
        if (nativePromoBanner != null) {
            ah.a("Setup banner");
            String ageRestrictions = nativePromoBanner.getAgeRestrictions();
            if (!TextUtils.isEmpty(ageRestrictions)) {
                this.ageRestrictionLabel.setText(ageRestrictions);
            } else {
                this.ageRestrictionLabel.setVisibility(8);
                this.advertisingLabel.setPadding(0, 0, 0, 0);
            }
            this.ageRestrictionLabel.setText(ageRestrictions);
            this.advertisingLabel.setText(nativePromoBanner.getAdvertisingLabel());
        }
    }

    private void initView() {
        setPadding(this.uiUtils.E(12), this.uiUtils.E(12), this.uiUtils.E(12), this.uiUtils.E(12));
        setId(R.id.nativeads_ad_view);
        this.ageRestrictionLabel.setId(R.id.nativeads_age_restrictions);
        this.ageRestrictionLabel.setPadding(this.uiUtils.E(2), 0, 0, 0);
        LayoutParams layoutParams = new LayoutParams(-2, -2);
        layoutParams.rightMargin = this.uiUtils.E(9);
        this.ageRestrictionLabel.setLayoutParams(layoutParams);
        this.ageRestrictionLabel.setTextColor(-1);
        this.ageRestrictionLabel.f(1, -1);
        this.advertisingLabel.setId(R.id.nativeads_advertising);
        LayoutParams layoutParams2 = new LayoutParams(-2, -2);
        layoutParams2.addRule(1, R.id.nativeads_age_restrictions);
        this.advertisingLabel.setLayoutParams(layoutParams2);
        this.advertisingLabel.setTextColor(-1);
        this.advertisingLabel.setPadding(this.uiUtils.E(3), 0, 0, 0);
        this.mediaAdView.setId(R.id.nativeads_media_view);
        this.mediaAdView.setLayoutParams(new LayoutParams(-1, -2));
        this.starsView.setId(R.id.nativeads_rating);
        LinearLayout.LayoutParams layoutParams3 = new LinearLayout.LayoutParams(this.uiUtils.E(73), this.uiUtils.E(12));
        layoutParams3.topMargin = this.uiUtils.E(4);
        layoutParams3.rightMargin = this.uiUtils.E(4);
        this.starsView.setLayoutParams(layoutParams3);
        this.votesLabel.setId(R.id.nativeads_votes);
        LinearLayout linearLayout = new LinearLayout(getContext());
        linearLayout.setOrientation(0);
        linearLayout.setPadding(this.uiUtils.E(3), this.uiUtils.E(3), this.uiUtils.E(3), this.uiUtils.E(3));
        linearLayout.setBackgroundColor(Color.parseColor("#55000000"));
        linearLayout.setLayoutParams(new LayoutParams(-1, -2));
        hm.a(this, 0, -3806472);
        setClickable(true);
        addView(this.mediaAdView);
        linearLayout.addView(this.ageRestrictionLabel);
        linearLayout.addView(this.advertisingLabel);
        addView(linearLayout);
    }
}
