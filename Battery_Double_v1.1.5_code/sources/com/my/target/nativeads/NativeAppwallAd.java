package com.my.target.nativeads;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.widget.ImageView;
import com.my.target.ah;
import com.my.target.b.C0072b;
import com.my.target.bc;
import com.my.target.common.BaseAd;
import com.my.target.common.models.ImageData;
import com.my.target.cq;
import com.my.target.db;
import com.my.target.dg;
import com.my.target.hd;
import com.my.target.hg;
import com.my.target.hi;
import com.my.target.hl;
import com.my.target.nativeads.banners.NativeAppwallBanner;
import com.my.target.nativeads.views.AppwallAdView;
import com.my.target.nativeads.views.AppwallAdView.AppwallAdViewListener;
import com.my.target.z;
import com.my.target.z.b;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public final class NativeAppwallAd extends BaseAd {
    private static final String DEFAULT_TITLE = "Apps";
    private static final int DEFAULT_TITLE_BACKGROUND_COLOR = -12232093;
    private static final int DEFAULT_TITLE_SUPPLEMENTARY_COLOR = -13220531;
    private static final int DEFAULT_TITLE_TEXT_COLOR = -1;
    @NonNull
    private final ArrayList<NativeAppwallBanner> banners = new ArrayList<>();
    @NonNull
    private final HashMap<NativeAppwallBanner, cq> bannersMap = new HashMap<>();
    @NonNull
    private final hd clickHandler = hd.dM();
    @NonNull
    private final Context context;
    @Nullable
    private bc engine;
    private boolean hideStatusBarInDialog = false;
    @Nullable
    private AppwallAdListener listener;
    @Nullable
    private db section;
    @NonNull
    private String title = DEFAULT_TITLE;
    private int titleBackgroundColor = DEFAULT_TITLE_BACKGROUND_COLOR;
    private int titleSupplementaryColor = DEFAULT_TITLE_SUPPLEMENTARY_COLOR;
    private int titleTextColor = -1;
    /* access modifiers changed from: private */
    @Nullable
    public WeakReference<AppwallAdView> viewWeakReference;

    public interface AppwallAdListener {
        void onClick(@NonNull NativeAppwallBanner nativeAppwallBanner, @NonNull NativeAppwallAd nativeAppwallAd);

        void onDismiss(@NonNull NativeAppwallAd nativeAppwallAd);

        void onDisplay(@NonNull NativeAppwallAd nativeAppwallAd);

        void onLoad(@NonNull NativeAppwallAd nativeAppwallAd);

        void onNoAd(@NonNull String str, @NonNull NativeAppwallAd nativeAppwallAd);
    }

    public static void loadImageToView(@NonNull ImageData imageData, @NonNull ImageView imageView) {
        hg.a(imageData, imageView);
    }

    public NativeAppwallAd(int i, @NonNull Context context2) {
        super(i, "appwall");
        this.context = context2;
        this.adConfig.setAutoLoadImages(true);
        ah.c("NativeAppwallAd created. Version: 5.4.7");
    }

    @Nullable
    public AppwallAdListener getListener() {
        return this.listener;
    }

    public void setListener(@Nullable AppwallAdListener appwallAdListener) {
        this.listener = appwallAdListener;
    }

    public boolean isHideStatusBarInDialog() {
        return this.hideStatusBarInDialog;
    }

    public void setHideStatusBarInDialog(boolean z) {
        this.hideStatusBarInDialog = z;
    }

    public void setAutoLoadImages(boolean z) {
        this.adConfig.setAutoLoadImages(z);
    }

    public boolean isAutoLoadImages() {
        return this.adConfig.isAutoLoadImages();
    }

    public long getCachePeriod() {
        return this.adConfig.getCachePeriod();
    }

    public void setCachePeriod(long j) {
        this.adConfig.setCachePeriod(j);
    }

    @NonNull
    public ArrayList<NativeAppwallBanner> getBanners() {
        return this.banners;
    }

    @NonNull
    public String getTitle() {
        return this.title;
    }

    public void setTitle(@NonNull String str) {
        this.title = str;
    }

    public int getTitleBackgroundColor() {
        return this.titleBackgroundColor;
    }

    public void setTitleBackgroundColor(int i) {
        this.titleBackgroundColor = i;
    }

    public int getTitleSupplementaryColor() {
        return this.titleSupplementaryColor;
    }

    public void setTitleSupplementaryColor(int i) {
        this.titleSupplementaryColor = i;
    }

    public int getTitleTextColor() {
        return this.titleTextColor;
    }

    public void setTitleTextColor(int i) {
        this.titleTextColor = i;
    }

    public void load() {
        z.a(this.adConfig).a((C0072b<T>) new b() {
            public void onResult(@Nullable db dbVar, @Nullable String str) {
                NativeAppwallAd.this.handleResult(dbVar, str);
            }
        }).a(this.context);
    }

    public boolean hasNotifications() {
        for (NativeAppwallBanner isHasNotification : this.bannersMap.keySet()) {
            if (isHasNotification.isHasNotification()) {
                return true;
            }
        }
        return false;
    }

    public void registerAppwallAdView(@NonNull AppwallAdView appwallAdView) {
        unregisterAppwallAdView();
        this.viewWeakReference = new WeakReference<>(appwallAdView);
        appwallAdView.setAppwallAdViewListener(new AppwallAdViewListener() {
            public void onBannersShow(@NonNull List<NativeAppwallBanner> list) {
                NativeAppwallAd.this.handleBannersShow(list);
            }

            public void onBannerClick(@NonNull NativeAppwallBanner nativeAppwallBanner) {
                NativeAppwallAd.this.handleBannerClick(nativeAppwallBanner);
                if (NativeAppwallAd.this.viewWeakReference != null) {
                    AppwallAdView appwallAdView = (AppwallAdView) NativeAppwallAd.this.viewWeakReference.get();
                    if (appwallAdView != null) {
                        appwallAdView.notifyDataSetChanged();
                    }
                }
            }
        });
    }

    public void unregisterAppwallAdView() {
        if (this.viewWeakReference != null) {
            AppwallAdView appwallAdView = (AppwallAdView) this.viewWeakReference.get();
            if (appwallAdView != null) {
                appwallAdView.setAppwallAdViewListener(null);
            }
            this.viewWeakReference.clear();
            this.viewWeakReference = null;
        }
    }

    public void show() {
        if (this.section == null || this.banners.size() <= 0) {
            ah.c("NativeAppwallAd.show: No ad");
            return;
        }
        if (this.engine == null) {
            this.engine = bc.a(this);
        }
        this.engine.k(this.context);
    }

    public void showDialog() {
        if (this.section == null || this.banners.size() <= 0) {
            ah.c("NativeAppwallAd.show: No ad");
            return;
        }
        if (this.engine == null) {
            this.engine = bc.a(this);
        }
        this.engine.l(this.context);
    }

    public void dismiss() {
        if (this.engine != null) {
            this.engine.dismiss();
        }
    }

    public void destroy() {
        unregisterAppwallAdView();
        if (this.engine != null) {
            this.engine.destroy();
            this.engine = null;
        }
        this.listener = null;
    }

    public void handleBannerClick(NativeAppwallBanner nativeAppwallBanner) {
        cq cqVar = (cq) this.bannersMap.get(nativeAppwallBanner);
        if (cqVar != null) {
            this.clickHandler.b(cqVar, this.context);
            if (this.section != null) {
                nativeAppwallBanner.setHasNotification(false);
                hi.a(this.section, this.adConfig).a(cqVar, false, this.context);
            }
            if (this.listener != null) {
                this.listener.onClick(nativeAppwallBanner, this);
                return;
            }
            return;
        }
        StringBuilder sb = new StringBuilder();
        sb.append("unable to handle banner click: no internal banner for id ");
        sb.append(nativeAppwallBanner.getId());
        ah.a(sb.toString());
    }

    @Nullable
    public String prepareBannerClickLink(NativeAppwallBanner nativeAppwallBanner) {
        cq cqVar = (cq) this.bannersMap.get(nativeAppwallBanner);
        if (cqVar != null) {
            hl.a((List<dg>) cqVar.getStatHolder().N("click"), this.context);
            if (this.section != null) {
                hi.a(this.section, this.adConfig).a(cqVar, false, this.context);
            }
            return cqVar.getTrackingLink();
        }
        StringBuilder sb = new StringBuilder();
        sb.append("unable to handle banner click: no internal banner for id ");
        sb.append(nativeAppwallBanner.getId());
        ah.a(sb.toString());
        return null;
    }

    public void handleBannersShow(@NonNull List<NativeAppwallBanner> list) {
        ArrayList arrayList = new ArrayList();
        for (NativeAppwallBanner nativeAppwallBanner : list) {
            cq cqVar = (cq) this.bannersMap.get(nativeAppwallBanner);
            if (cqVar != null) {
                arrayList.addAll(cqVar.getStatHolder().N("playbackStarted"));
            } else {
                StringBuilder sb = new StringBuilder();
                sb.append("unable to handle banner show: no internal banner for id ");
                sb.append(nativeAppwallBanner.getId());
                ah.a(sb.toString());
            }
        }
        if (arrayList.size() > 0) {
            hl.a((List<dg>) arrayList, this.context);
        }
    }

    public void handleBannerShow(@NonNull NativeAppwallBanner nativeAppwallBanner) {
        cq cqVar = (cq) this.bannersMap.get(nativeAppwallBanner);
        if (cqVar != null) {
            hl.a((List<dg>) cqVar.getStatHolder().N("playbackStarted"), this.context);
            return;
        }
        StringBuilder sb = new StringBuilder();
        sb.append("unable to handle banner show: no internal banner for id ");
        sb.append(nativeAppwallBanner.getId());
        ah.a(sb.toString());
    }

    /* access modifiers changed from: private */
    public void handleResult(@Nullable db dbVar, @Nullable String str) {
        if (this.listener == null) {
            return;
        }
        if (dbVar == null) {
            AppwallAdListener appwallAdListener = this.listener;
            if (str == null) {
                str = "no ad";
            }
            appwallAdListener.onNoAd(str, this);
            return;
        }
        this.section = dbVar;
        for (cq cqVar : dbVar.bI()) {
            NativeAppwallBanner newBanner = NativeAppwallBanner.newBanner(cqVar);
            this.banners.add(newBanner);
            this.bannersMap.put(newBanner, cqVar);
        }
        this.listener.onLoad(this);
    }
}
