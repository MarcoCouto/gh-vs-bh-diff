package com.my.target.nativeads;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import com.my.target.ah;
import com.my.target.ao;
import com.my.target.az;
import com.my.target.b.C0072b;
import com.my.target.bb;
import com.my.target.co;
import com.my.target.common.BaseAd;
import com.my.target.common.models.ImageData;
import com.my.target.cs;
import com.my.target.da;
import com.my.target.hg;
import com.my.target.nativeads.banners.NativePromoBanner;
import com.my.target.v;
import com.my.target.v.b;
import java.util.List;

public final class NativeAd extends BaseAd {
    private int adChoicesPlacement = 0;
    @NonNull
    private final Context appContext;
    @Nullable
    private ao engine;
    @Nullable
    private NativeAdListener listener;

    public interface AdChoicesPlacement {
        public static final int BOTTOM_LEFT = 3;
        public static final int BOTTOM_RIGHT = 2;
        public static final int TOP_LEFT = 1;
        public static final int TOP_RIGHT = 0;
    }

    public interface NativeAdListener {
        void onClick(@NonNull NativeAd nativeAd);

        void onLoad(@NonNull NativePromoBanner nativePromoBanner, @NonNull NativeAd nativeAd);

        void onNoAd(@NonNull String str, @NonNull NativeAd nativeAd);

        void onShow(@NonNull NativeAd nativeAd);

        void onVideoComplete(@NonNull NativeAd nativeAd);

        void onVideoPause(@NonNull NativeAd nativeAd);

        void onVideoPlay(@NonNull NativeAd nativeAd);
    }

    public static void loadImageToView(@NonNull ImageData imageData, @NonNull ImageView imageView) {
        hg.a(imageData, imageView);
    }

    public NativeAd(int i, @NonNull Context context) {
        super(i, "nativeads");
        this.appContext = context.getApplicationContext();
        ah.c("NativeAd created. Version: 5.4.7");
    }

    public void setAutoLoadVideo(boolean z) {
        this.adConfig.setAutoLoadVideo(z);
    }

    public void setAutoLoadImages(boolean z) {
        this.adConfig.setAutoLoadImages(z);
    }

    public boolean isAutoLoadImages() {
        return this.adConfig.isAutoLoadImages();
    }

    public boolean isAutoLoadVideo() {
        return this.adConfig.isAutoLoadVideo();
    }

    public void setMediationEnabled(boolean z) {
        this.adConfig.setMediationEnabled(z);
    }

    public boolean isMediationEnabled() {
        return this.adConfig.isMediationEnabled();
    }

    @Nullable
    public NativeAdListener getListener() {
        return this.listener;
    }

    public void setListener(@Nullable NativeAdListener nativeAdListener) {
        this.listener = nativeAdListener;
    }

    @Nullable
    public NativePromoBanner getBanner() {
        if (this.engine == null) {
            return null;
        }
        return this.engine.Z();
    }

    public int getAdChoicesPlacement() {
        return this.adChoicesPlacement;
    }

    public void setAdChoicesPlacement(int i) {
        this.adChoicesPlacement = i;
    }

    public final void load() {
        v.a(this.adConfig).a((C0072b<T>) new b() {
            public void onResult(@Nullable da daVar, @Nullable String str) {
                NativeAd.this.handleResult(daVar, str);
            }
        }).a(this.appContext);
    }

    public void loadFromBid(@NonNull String str) {
        this.adConfig.setBidId(str);
        load();
    }

    public final void handleSection(@NonNull da daVar) {
        v.a(daVar, this.adConfig).a((C0072b<T>) new b() {
            public void onResult(@Nullable da daVar, @Nullable String str) {
                NativeAd.this.handleResult(daVar, str);
            }
        }).a(this.appContext);
    }

    public final void registerView(@NonNull View view, @Nullable List<View> list) {
        if (this.engine != null) {
            this.engine.registerView(view, list, this.adChoicesPlacement);
        }
    }

    public final void registerView(@NonNull View view) {
        registerView(view, null);
    }

    public final void unregisterView() {
        if (this.engine != null) {
            this.engine.unregisterView();
        }
    }

    /* access modifiers changed from: 0000 */
    public void setBanner(@NonNull co coVar) {
        this.engine = bb.a(this, coVar);
    }

    /* access modifiers changed from: private */
    public void handleResult(@Nullable da daVar, @Nullable String str) {
        cs csVar;
        if (this.listener != null) {
            co coVar = null;
            if (daVar != null) {
                coVar = daVar.bR();
                csVar = daVar.bx();
            } else {
                csVar = null;
            }
            if (coVar != null) {
                this.engine = bb.a(this, coVar);
                if (this.engine.Z() != null) {
                    this.listener.onLoad(this.engine.Z(), this);
                }
            } else if (csVar != null) {
                az a2 = az.a(this, csVar, this.adConfig);
                this.engine = a2;
                a2.n(this.appContext);
            } else {
                NativeAdListener nativeAdListener = this.listener;
                if (str == null) {
                    str = "no ad";
                }
                nativeAdListener.onNoAd(str, this);
            }
        }
    }
}
