package com.my.target;

import android.support.annotation.NonNull;
import com.ironsource.sdk.precache.DownloadManager;
import org.json.JSONObject;

/* compiled from: StandardAdSectionParser */
public class ek {
    @NonNull
    public static ek cC() {
        return new ek();
    }

    private ek() {
    }

    public void a(@NonNull JSONObject jSONObject, @NonNull dc dcVar) {
        JSONObject optJSONObject = jSONObject.optJSONObject(DownloadManager.SETTINGS);
        if (optJSONObject != null) {
            b(optJSONObject, dcVar);
        }
    }

    private void b(@NonNull JSONObject jSONObject, @NonNull dc dcVar) {
        dcVar.p(jSONObject.optBoolean("hasAdditionalAds", dcVar.ca()));
    }
}
