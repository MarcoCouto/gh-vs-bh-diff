package com.my.target;

import android.support.annotation.NonNull;
import com.my.target.common.models.ImageData;
import java.util.ArrayList;
import java.util.List;

/* compiled from: InterstitialSliderAdSection */
public class cy extends cu {
    private int backgroundColor = -16368537;
    @NonNull
    private final ArrayList<cl> banners = new ArrayList<>();
    private ImageData closeIcon;
    private int dA = -1;
    private int dB = -14696781;

    @NonNull
    public static cy bF() {
        return new cy();
    }

    public int bG() {
        return this.dB;
    }

    public int getBackgroundColor() {
        return this.backgroundColor;
    }

    public ImageData getCloseIcon() {
        return this.closeIcon;
    }

    public int bH() {
        return this.dA;
    }

    public void m(int i) {
        this.dB = i;
    }

    public void setBackgroundColor(int i) {
        this.backgroundColor = i;
    }

    public void setCloseIcon(ImageData imageData) {
        this.closeIcon = imageData;
    }

    public void n(int i) {
        this.dA = i;
    }

    private cy() {
    }

    public void c(@NonNull cl clVar) {
        this.banners.add(clVar);
    }

    public void d(@NonNull cl clVar) {
        this.banners.remove(clVar);
    }

    @NonNull
    public List<cl> bI() {
        return new ArrayList(this.banners);
    }

    public int getBannersCount() {
        return this.banners.size();
    }
}
