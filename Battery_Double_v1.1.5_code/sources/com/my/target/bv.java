package com.my.target;

import android.support.annotation.NonNull;
import com.explorestack.iab.mraid.MRAIDNativeFeature;
import com.tapjoy.TJAdUnitConstants.String;
import com.yandex.mobile.ads.video.tracking.Tracker.Events;

/* compiled from: MraidJsCommand */
public class bv {
    public final boolean cu;
    @NonNull
    private final String cv;

    public bv(@NonNull String str, @NonNull String str2) {
        char c;
        switch (str.hashCode()) {
            case -1910759310:
                if (str.equals("vpaidInit")) {
                    c = 5;
                    break;
                }
            case -1886160473:
                if (str.equals("playVideo")) {
                    c = 13;
                    break;
                }
            case -1289167206:
                if (str.equals(Events.CREATIVE_EXPAND)) {
                    c = 12;
                    break;
                }
            case -934437708:
                if (str.equals("resize")) {
                    c = 9;
                    break;
                }
            case -733616544:
                if (str.equals("createCalendarEvent")) {
                    c = 11;
                    break;
                }
            case 3417674:
                if (str.equals("open")) {
                    c = 8;
                    break;
                }
            case 94756344:
                if (str.equals("close")) {
                    c = 0;
                    break;
                }
            case 133423073:
                if (str.equals("setOrientationProperties")) {
                    c = 2;
                    break;
                }
            case 459238621:
                if (str.equals(MRAIDNativeFeature.STORE_PICTURE)) {
                    c = 10;
                    break;
                }
            case 624734601:
                if (str.equals("setResizeProperties")) {
                    c = 3;
                    break;
                }
            case 892543864:
                if (str.equals("vpaidEvent")) {
                    c = 6;
                    break;
                }
            case 1362316271:
                if (str.equals("setExpandProperties")) {
                    c = 4;
                    break;
                }
            case 1614272768:
                if (str.equals("useCustomClose")) {
                    c = 1;
                    break;
                }
            case 1797992422:
                if (str.equals("playheadEvent")) {
                    c = 7;
                    break;
                }
            default:
                c = 65535;
                break;
        }
        switch (c) {
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
                this.cu = false;
                this.cv = str;
                return;
            case 8:
            case 9:
            case 10:
            case 11:
                this.cu = true;
                this.cv = str;
                return;
            case 12:
            case 13:
                this.cu = String.INLINE.equals(str2);
                this.cv = str;
                return;
            default:
                this.cu = false;
                this.cv = "";
                return;
        }
    }

    public String toString() {
        return this.cv;
    }
}
