package com.my.target;

import android.support.annotation.NonNull;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/* compiled from: StatHolder */
public class dh {
    private final Set<dg> dU = new HashSet();
    private final Set<df> dV = new HashSet();
    private final ArrayList<de> dW = new ArrayList<>();
    private final ArrayList<dd> dX = new ArrayList<>();

    @NonNull
    public static dh ch() {
        return new dh();
    }

    private dh() {
    }

    public void b(@NonNull dg dgVar) {
        if (dgVar instanceof df) {
            this.dV.add((df) dgVar);
        } else if (dgVar instanceof de) {
            de deVar = (de) dgVar;
            if (this.dW.isEmpty()) {
                this.dW.add(deVar);
                return;
            }
            int size = this.dW.size();
            while (size > 0 && ((de) this.dW.get(size - 1)).cf() < deVar.cf()) {
                size--;
            }
            this.dW.add(size, deVar);
        } else if (dgVar instanceof dd) {
            this.dX.add((dd) dgVar);
        } else {
            this.dU.add(dgVar);
        }
    }

    @NonNull
    public ArrayList<de> ci() {
        return new ArrayList<>(this.dW);
    }

    @NonNull
    public ArrayList<dd> cj() {
        return new ArrayList<>(this.dX);
    }

    @NonNull
    public Set<df> ck() {
        return new HashSet(this.dV);
    }

    @NonNull
    public Set<dg> cl() {
        return new HashSet(this.dU);
    }

    @NonNull
    public ArrayList<dg> N(@NonNull String str) {
        ArrayList<dg> arrayList = new ArrayList<>();
        for (dg dgVar : this.dU) {
            if (str.equals(dgVar.getType())) {
                arrayList.add(dgVar);
            }
        }
        return arrayList;
    }

    public void a(@NonNull List<df> list) {
        list.addAll(this.dV);
        Collections.sort(list, new Comparator<df>() {
            /* renamed from: a */
            public int compare(df dfVar, df dfVar2) {
                return (int) (dfVar2.cf() - dfVar.cf());
            }
        });
    }

    public void a(@NonNull dh dhVar, float f) {
        this.dU.addAll(dhVar.cl());
        this.dX.addAll(dhVar.cj());
        if (f <= 0.0f) {
            this.dV.addAll(dhVar.ck());
            this.dW.addAll(dhVar.ci());
            return;
        }
        for (df dfVar : dhVar.ck()) {
            float cg = dfVar.cg();
            if (cg >= 0.0f) {
                dfVar.h((cg * f) / 100.0f);
                dfVar.i(-1.0f);
            }
            b(dfVar);
        }
        Iterator it = dhVar.ci().iterator();
        while (it.hasNext()) {
            de deVar = (de) it.next();
            float cg2 = deVar.cg();
            if (cg2 >= 0.0f) {
                deVar.h((cg2 * f) / 100.0f);
                deVar.i(-1.0f);
            }
            b(deVar);
        }
    }

    public void e(@NonNull ArrayList<dg> arrayList) {
        Iterator it = arrayList.iterator();
        while (it.hasNext()) {
            b((dg) it.next());
        }
    }

    public void f(@NonNull ArrayList<df> arrayList) {
        this.dV.addAll(arrayList);
    }

    public boolean cm() {
        return !this.dU.isEmpty() || !this.dV.isEmpty() || !this.dW.isEmpty() || !this.dX.isEmpty();
    }
}
