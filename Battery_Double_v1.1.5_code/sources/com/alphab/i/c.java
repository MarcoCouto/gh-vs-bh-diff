package com.alphab.i;

import android.content.Context;
import java.lang.ref.WeakReference;

/* compiled from: ThreadPoolManager */
public class c {

    /* renamed from: a reason: collision with root package name */
    public static final String f1009a = "com.alphab.i.c";
    private static volatile c c;
    /* access modifiers changed from: private */
    public WeakReference<Context> b;
    private b d;
    private a e;

    /* compiled from: ThreadPoolManager */
    public class a {
        private com.mintegral.msdk.base.common.e.b b;

        public a() {
        }

        public final com.mintegral.msdk.base.common.e.b a() {
            if (this.b == null) {
                synchronized (a.class) {
                    if (this.b == null) {
                        Context context = (Context) c.this.b.get();
                        if (context != null) {
                            this.b = new com.mintegral.msdk.base.common.e.b(context, 5);
                        }
                    }
                }
            }
            return this.b;
        }
    }

    /* compiled from: ThreadPoolManager */
    public class b {
        private com.mintegral.msdk.base.common.e.b b;

        public b() {
        }

        public final synchronized void a(com.mintegral.msdk.base.common.e.a aVar) {
            try {
                if (this.b == null) {
                    Context context = (Context) c.this.b.get();
                    if (context != null) {
                        String str = c.f1009a;
                        this.b = new com.mintegral.msdk.base.common.e.b(context, 0);
                    }
                }
                this.b.b(aVar);
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        }

        public final synchronized void a(com.mintegral.msdk.base.common.e.a aVar, com.mintegral.msdk.base.common.e.a.b bVar) {
            try {
                if (this.b == null) {
                    Context context = (Context) c.this.b.get();
                    if (context != null) {
                        String str = c.f1009a;
                        this.b = new com.mintegral.msdk.base.common.e.b(context, 0);
                    }
                }
                this.b.a(aVar, bVar);
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        }
    }

    private c(Context context) {
        this.b = new WeakReference<>(context);
    }

    public static c a(Context context) {
        if (c == null) {
            synchronized (c.class) {
                if (c == null) {
                    c = new c(context);
                }
            }
        }
        return c;
    }

    public final synchronized b a() {
        if (this.d == null) {
            this.d = new b();
        }
        return this.d;
    }

    public final synchronized a b() {
        if (this.e == null) {
            this.e = new a();
        }
        return this.e;
    }
}
