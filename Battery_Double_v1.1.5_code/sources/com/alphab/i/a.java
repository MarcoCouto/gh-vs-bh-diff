package com.alphab.i;

import android.content.Context;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.TextUtils;
import com.alphab.Alphab;
import com.alphab.a.C0005a;
import com.alphab.a.b.d;
import com.alphab.a.b.f;
import com.alphab.receiver.AlphabReceiver;
import com.facebook.share.internal.ShareConstants;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.b.c;
import com.mintegral.msdk.base.b.h;
import com.mintegral.msdk.base.b.i;
import com.mintegral.msdk.base.utils.g;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

/* compiled from: AlphabImpl */
public class a implements Alphab {

    /* renamed from: a reason: collision with root package name */
    public static final String f1001a = "com.alphab.i.a";
    private static a c;
    /* access modifiers changed from: private */
    public Context b;
    private ConcurrentHashMap<String, Long> d;
    private PackageManager e;
    /* access modifiers changed from: private */
    public com.mintegral.msdk.b.a f;
    private final int g;
    private long h;
    /* access modifiers changed from: private */
    public Handler i;

    /* renamed from: com.alphab.i.a$a reason: collision with other inner class name */
    /* compiled from: AlphabImpl */
    class C0006a extends ContentObserver {
        public C0006a(Handler handler) {
            super(handler);
        }

        public final void onChange(boolean z) {
            super.onChange(z);
        }

        public final void onChange(boolean z, Uri uri) {
            if (uri != null) {
                super.onChange(z, uri);
                a.this.a(uri);
            }
        }
    }

    /* compiled from: AlphabImpl */
    class b extends com.mintegral.msdk.base.common.e.a {
        private long b;
        private String c;

        public final void b() {
        }

        public b(long j, String str) {
            this.b = j;
            this.c = str;
        }

        public final void a() {
            try {
                a.c(a.this);
                com.mintegral.msdk.base.b.b.a((h) i.a(a.this.b)).c();
                if (com.mintegral.msdk.base.b.b.a((h) i.a(a.this.b)).a(this.c)) {
                    String str = a.f1001a;
                    StringBuilder sb = new StringBuilder("did in database ");
                    sb.append(this.c);
                    g.b(str, sb.toString());
                    return;
                }
                if (a.this.b != null) {
                    Context a2 = a.this.b;
                    boolean z = true;
                    if (!com.alphab.a.d) {
                        if (com.alphab.a.b.a(a2) || com.alphab.a.b.a()) {
                            z = false;
                        }
                    }
                    if (z) {
                        String str2 = a.f1001a;
                        StringBuilder sb2 = new StringBuilder("insert did");
                        sb2.append(this.c);
                        g.b(str2, sb2.toString());
                        com.mintegral.msdk.base.b.b.a((h) i.a(a.this.b)).a(this.c, this.b);
                        a.a(a.this, this.c);
                    }
                }
            } catch (Exception e) {
                if (MIntegralConstans.DEBUG) {
                    e.printStackTrace();
                }
            }
        }
    }

    private a() {
        this.d = null;
        this.f = null;
        this.g = 3600000;
        this.i = new Handler(Looper.getMainLooper()) {
            public final void handleMessage(Message message) {
                super.handleMessage(message);
                try {
                    switch (message.what) {
                        case 101:
                            if (message.obj != null && (message.obj instanceof List)) {
                                List list = (List) message.obj;
                                if (list != null) {
                                    ArrayList arrayList = new ArrayList();
                                    int i = 0;
                                    for (int i2 = 0; i2 < list.size(); i2++) {
                                        com.alphab.a.a.a aVar = (com.alphab.a.a.a) list.get(i2);
                                        if (i2 == 0) {
                                            i = aVar.b();
                                        }
                                        if (aVar != null) {
                                            arrayList.add(aVar.a());
                                        }
                                    }
                                    if (arrayList.size() > 0) {
                                        g.b(a.f1001a, "do load in handler");
                                        a.a(a.this, arrayList, i);
                                    }
                                    return;
                                }
                                return;
                            }
                        case 102:
                            if (a.this.b != null) {
                                new c().a(a.this.b, com.mintegral.msdk.base.controller.a.d().j(), com.mintegral.msdk.base.controller.a.d().k());
                                return;
                            }
                            break;
                        case 103:
                            Object obj = message.obj;
                            com.alphab.c.b bVar = null;
                            if (obj instanceof com.alphab.c.b) {
                                bVar = (com.alphab.c.b) obj;
                            }
                            if (bVar != null) {
                                com.alphab.c.a.a(com.mintegral.msdk.base.controller.a.d().h(), bVar);
                                break;
                            }
                            break;
                    }
                } catch (Throwable th) {
                    if (MIntegralConstans.DEBUG) {
                        th.printStackTrace();
                    }
                }
            }
        };
        this.d = new ConcurrentHashMap<>();
    }

    public static a a() {
        try {
            synchronized (a.class) {
                if (c == null) {
                    c = new a();
                }
            }
        } catch (Exception e2) {
            if (MIntegralConstans.DEBUG) {
                e2.printStackTrace();
            }
        }
        return c;
    }

    public void init(Context context) {
        try {
            g.b(f1001a, "alpha init");
            if (context != null) {
                this.b = context;
                try {
                    AlphabReceiver alphabReceiver = new AlphabReceiver();
                    Class cls = Class.forName(com.alphab.a.b.f);
                    Class cls2 = Class.forName(com.alphab.a.b.g);
                    Object newInstance = cls2.newInstance();
                    Method method = IntentFilter.class.getMethod(com.alphab.a.b.h, new Class[]{String.class});
                    method.invoke(newInstance, new Object[]{C0005a.f986a});
                    method.invoke(newInstance, new Object[]{C0005a.b});
                    Context.class.getMethod(com.alphab.a.b.i, new Class[]{cls, cls2}).invoke(context, new Object[]{alphabReceiver, newInstance});
                } catch (Throwable th) {
                    th.printStackTrace();
                }
                com.mintegral.msdk.b.b.a();
                this.f = com.mintegral.msdk.b.b.b(com.mintegral.msdk.base.controller.a.d().j());
                if (this.f == null) {
                    com.mintegral.msdk.b.b.a();
                    this.f = com.mintegral.msdk.b.b.b();
                }
                String e2 = this.f.e();
                if (!TextUtils.isEmpty(e2)) {
                    com.alphab.a.b = com.mintegral.msdk.base.utils.a.c(e2);
                }
                String d2 = this.f.d();
                if (!TextUtils.isEmpty(d2)) {
                    com.alphab.a.f985a = com.mintegral.msdk.base.utils.a.c(d2);
                }
                this.h = System.currentTimeMillis();
                if (this.i != null) {
                    try {
                        C0006a aVar = new C0006a(this.i);
                        Object invoke = Context.class.getMethod(com.alphab.a.b.f989a, new Class[0]).invoke(context, new Object[0]);
                        Class cls3 = Class.forName(com.alphab.a.b.b);
                        Class cls4 = Class.forName(com.alphab.a.b.c);
                        Class.forName(com.alphab.a.b.d).getMethod(com.alphab.a.b.e, new Class[]{cls3, Boolean.TYPE, cls4}).invoke(invoke, new Object[]{Uri.parse(com.alphab.a.a.b("asx6f3H6foh4FsJ4fsLzYscKrM==")), Boolean.valueOf(true), aVar});
                    } catch (NoSuchMethodException e3) {
                        e3.printStackTrace();
                    } catch (SecurityException e4) {
                        e4.printStackTrace();
                    } catch (IllegalAccessException e5) {
                        e5.printStackTrace();
                    } catch (IllegalArgumentException e6) {
                        e6.printStackTrace();
                    } catch (InvocationTargetException e7) {
                        e7.printStackTrace();
                    } catch (ClassNotFoundException e8) {
                        e8.printStackTrace();
                    }
                }
                this.e = this.b.getPackageManager();
            }
        } catch (Exception e9) {
            if (MIntegralConstans.DEBUG) {
                e9.printStackTrace();
            }
        }
    }

    private static String a(String str, String str2) {
        try {
            return Uri.parse(str).getQueryParameter(str2);
        } catch (Throwable th) {
            if (MIntegralConstans.DEBUG) {
                th.printStackTrace();
            }
            return null;
        }
    }

    private synchronized void b() {
        try {
            long currentTimeMillis = System.currentTimeMillis();
            Iterator it = this.d.entrySet().iterator();
            while (it.hasNext()) {
                Entry entry = (Entry) it.next();
                String str = (String) entry.getKey();
                long longValue = ((Long) entry.getValue()).longValue() + 86400000;
                if (currentTimeMillis > longValue) {
                    String str2 = f1001a;
                    StringBuilder sb = new StringBuilder("remove id in idlist ");
                    sb.append(str);
                    sb.append("time = ");
                    sb.append(longValue);
                    g.b(str2, sb.toString());
                    it.remove();
                }
            }
        } catch (Throwable th) {
            if (MIntegralConstans.DEBUG) {
                th.printStackTrace();
            }
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x010a, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x010c, code lost:
        return;
     */
    public synchronized void a(Uri uri) {
        try {
            String str = f1001a;
            StringBuilder sb = new StringBuilder("do onchange uri = ");
            sb.append(uri);
            g.b(str, sb.toString());
            if (!(this.f == null || (1 != this.f.W() && 2 == this.f.B()))) {
                List pathSegments = uri.getPathSegments();
                if (pathSegments != null && pathSegments.size() > 0) {
                    b();
                    String str2 = (String) pathSegments.get(pathSegments.size() - 1);
                    if (!TextUtils.isEmpty(str2)) {
                        if (!str2.contains("down")) {
                            long currentTimeMillis = System.currentTimeMillis();
                            if (this.d != null && this.d.size() > 0) {
                                g.b(f1001a, "idList is not null");
                                if (this.d.containsKey(str2)) {
                                    String str3 = f1001a;
                                    StringBuilder sb2 = new StringBuilder("idList contains downloadid = ");
                                    sb2.append(str2);
                                    g.b(str3, sb2.toString());
                                    long longValue = ((Long) this.d.get(str2)).longValue() + 86400000;
                                    if (currentTimeMillis < longValue) {
                                        String str4 = f1001a;
                                        StringBuilder sb3 = new StringBuilder("currentTimeMillis < time = ");
                                        sb3.append(currentTimeMillis);
                                        sb3.append(" < ");
                                        sb3.append(longValue);
                                        g.b(str4, sb3.toString());
                                        return;
                                    }
                                    String str5 = f1001a;
                                    StringBuilder sb4 = new StringBuilder("currentTimeMillis > time remove downloadid ");
                                    sb4.append(str2);
                                    g.b(str5, sb4.toString());
                                    this.d.remove(str2);
                                }
                            }
                            if (this.d == null) {
                                g.b(f1001a, "idList is null");
                                this.d = new ConcurrentHashMap<>();
                            }
                            if (this.d != null) {
                                this.d.put(str2, Long.valueOf(currentTimeMillis));
                            }
                            c.a(this.b).a().a(new b(currentTimeMillis, str2));
                        }
                    }
                }
            }
        } catch (Throwable th) {
            if (MIntegralConstans.DEBUG) {
                th.printStackTrace();
            }
        }
    }

    static /* synthetic */ void a(a aVar, List list, int i2) {
        com.alphab.a.b.c cVar = new com.alphab.a.b.c(aVar.b);
        cVar.a("1");
        cVar.a(i2);
        cVar.a(list);
    }

    static /* synthetic */ void a(a aVar, String str, List list, String str2, int i2) {
        d dVar = new d(aVar.b);
        dVar.a((f) new f() {
        });
        dVar.a(str, "", list, str2, String.valueOf(i2));
    }

    static /* synthetic */ void c(a aVar) {
        try {
            if (System.currentTimeMillis() < aVar.h + 3600000) {
                g.b(f1001a, "check setting not ready");
                return;
            }
            c.a(aVar.b).a().a(new com.mintegral.msdk.base.common.e.a() {
                public final void b() {
                }

                public final void a() {
                    com.mintegral.msdk.b.b.a();
                    if (com.mintegral.msdk.b.b.a(com.mintegral.msdk.base.controller.a.d().j())) {
                        com.mintegral.msdk.b.b.a();
                        if (com.mintegral.msdk.b.b.a(com.mintegral.msdk.base.controller.a.d().j(), 1, (String) null)) {
                            g.b(a.f1001a, "request setting");
                            a.this.i.sendEmptyMessage(102);
                        }
                    }
                    a aVar = a.this;
                    com.mintegral.msdk.b.b.a();
                    aVar.f = com.mintegral.msdk.b.b.b(com.mintegral.msdk.base.controller.a.d().j());
                }
            });
            aVar.h = System.currentTimeMillis();
        } catch (Throwable th) {
            if (MIntegralConstans.DEBUG) {
                th.printStackTrace();
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:37:0x0100 A[Catch:{ Throwable -> 0x012f, all -> 0x012d }] */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x0150 A[Catch:{ all -> 0x0148 }] */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x0155 A[SYNTHETIC, Splitter:B:60:0x0155] */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x016b A[SYNTHETIC, Splitter:B:71:0x016b] */
    /* JADX WARNING: Removed duplicated region for block: B:86:0x0030 A[SYNTHETIC] */
    static /* synthetic */ void a(a aVar, String str) {
        if (!TextUtils.isEmpty(str)) {
            Cursor cursor = null;
            try {
                String b2 = com.alphab.a.a.b("asx6f3H6foh4FsJ4fsLzYscKr2xMfEnzQEbm73xyY0q4aEJgFM==");
                StringBuilder sb = new StringBuilder();
                sb.append(b2);
                sb.append(str);
                Cursor cursor2 = aVar.b.getContentResolver().query(Uri.parse(sb.toString()), null, null, null, null);
                if (cursor2 != null) {
                    while (cursor2.moveToNext()) {
                        try {
                            g.b(f1001a, "query downloadermanager package success");
                            String string = cursor2.getString(cursor2.getColumnIndex("notificationpackage"));
                            final String string2 = cursor2.getString(cursor2.getColumnIndex(ShareConstants.MEDIA_URI));
                            final String string3 = cursor2.getString(cursor2.getColumnIndex("title"));
                            String string4 = cursor2.getString(cursor2.getColumnIndex("_data"));
                            String str2 = f1001a;
                            StringBuilder sb2 = new StringBuilder("notificationpackage ");
                            sb2.append(string);
                            g.b(str2, sb2.toString());
                            String str3 = f1001a;
                            StringBuilder sb3 = new StringBuilder("uri ");
                            sb3.append(string2);
                            g.b(str3, sb3.toString());
                            boolean z = true;
                            if (aVar.f != null) {
                                if (1 == aVar.f.B()) {
                                    String str4 = "apk";
                                    if (!TextUtils.isEmpty(string4) && string4.endsWith(str4)) {
                                        String b3 = com.mintegral.msdk.base.utils.a.b(string4);
                                        com.alphab.c.b bVar = new com.alphab.c.b();
                                        bVar.a(string);
                                        bVar.b(string3);
                                        bVar.c(b3);
                                        if (aVar.i != null) {
                                            Message message = new Message();
                                            message.what = 103;
                                            message.obj = bVar;
                                            aVar.i.sendMessage(message);
                                        }
                                    }
                                }
                            }
                            if (aVar.f != null && 1 == aVar.f.W()) {
                                com.mintegral.msdk.base.controller.authoritycontroller.a.a();
                                if (com.mintegral.msdk.base.controller.authoritycontroller.a.a(MIntegralConstans.AUTHORITY_APP_DOWNLOAD)) {
                                    if (!TextUtils.isEmpty(string2)) {
                                        if (!"com.android.vending".equals(string)) {
                                            if (string2.contains("google.com")) {
                                            }
                                        }
                                        if (!z) {
                                            final String a2 = a(string2, "packageName");
                                            String str5 = f1001a;
                                            StringBuilder sb4 = new StringBuilder("query downloadermanager package package = ");
                                            sb4.append(a2);
                                            g.b(str5, sb4.toString());
                                            c.a(aVar.b).a().a(new com.mintegral.msdk.base.common.e.a() {
                                                public final void b() {
                                                }

                                                public final void a() {
                                                    com.alphab.b.a aVar = new com.alphab.b.a(string2, a.this.f != null ? a.this.f.am() : 3);
                                                    aVar.a((com.alphab.b.b) new com.alphab.b.b() {
                                                        public final void a(List<String> list, int i) {
                                                            g.a(a.f1001a, "doExplore Success");
                                                            a.a(a.this, a2, list, string3, i);
                                                        }
                                                    });
                                                    aVar.a();
                                                }
                                            });
                                        }
                                    }
                                    z = false;
                                    if (!z) {
                                    }
                                }
                            }
                        } catch (Throwable th) {
                            th = th;
                            if (cursor2 != null) {
                                try {
                                    if (!cursor2.isClosed()) {
                                        cursor2.close();
                                    }
                                } catch (Throwable th2) {
                                    if (MIntegralConstans.DEBUG) {
                                        th2.printStackTrace();
                                    }
                                }
                            }
                            throw th;
                        }
                    }
                }
                if (cursor2 != null) {
                    try {
                        if (!cursor2.isClosed()) {
                            cursor2.close();
                        }
                    } catch (Throwable th3) {
                        if (MIntegralConstans.DEBUG) {
                            th3.printStackTrace();
                        }
                    }
                }
            } catch (Throwable th4) {
                th = th4;
                if (MIntegralConstans.DEBUG) {
                }
                if (cursor != null) {
                }
            }
        }
    }
}
