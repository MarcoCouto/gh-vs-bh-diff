package com.alphab.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.mintegral.msdk.MIntegralConstans;

public class AlphabReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        try {
            new a(intent);
        } catch (Throwable th) {
            if (MIntegralConstans.DEBUG) {
                th.printStackTrace();
            }
        }
    }
}
