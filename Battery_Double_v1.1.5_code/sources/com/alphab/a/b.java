package com.alphab.a;

import android.content.Context;
import android.net.Proxy;
import android.os.Build.VERSION;
import android.text.TextUtils;
import com.mintegral.msdk.base.utils.g;
import java.net.NetworkInterface;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Iterator;

/* compiled from: NetUtils */
public final class b {
    public static boolean a(Context context) {
        String str;
        int i;
        boolean z = false;
        if (context == null) {
            return false;
        }
        try {
            if (VERSION.SDK_INT >= 14) {
                str = System.getProperty("http.proxyHost");
                String property = System.getProperty("http.proxyPort");
                if (property == null) {
                    property = "-1";
                }
                i = Integer.parseInt(property);
            } else {
                String host = Proxy.getHost(context);
                i = Proxy.getPort(context);
                str = host;
            }
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append("~");
            g.a("address = ", sb.toString());
            StringBuilder sb2 = new StringBuilder();
            sb2.append(i);
            sb2.append("~");
            g.a("port = ", sb2.toString());
            if (!TextUtils.isEmpty(str) && i != -1) {
                z = true;
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
        return z;
    }

    public static boolean a() {
        try {
            Enumeration networkInterfaces = NetworkInterface.getNetworkInterfaces();
            if (networkInterfaces != null) {
                Iterator it = Collections.list(networkInterfaces).iterator();
                while (it.hasNext()) {
                    Object next = it.next();
                    if (next instanceof NetworkInterface) {
                        NetworkInterface networkInterface = (NetworkInterface) next;
                        if (networkInterface.isUp() && networkInterface.getInterfaceAddresses().size() != 0) {
                            if ("tun0".equals(networkInterface.getName()) || "ppp0".equals(networkInterface.getName())) {
                                return true;
                            }
                        }
                    }
                }
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
        return false;
    }
}
