package com.alphab.a.b;

import android.text.TextUtils;
import com.mintegral.msdk.base.common.net.d;
import com.mintegral.msdk.base.utils.g;
import java.io.IOException;
import org.apache.http.HttpEntity;
import org.apache.http.ParseException;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: AlphabJSONObjectHandler */
public abstract class a extends d<JSONObject> {
    /* access modifiers changed from: private */
    /* renamed from: e */
    public JSONObject b(HttpEntity httpEntity) throws Exception {
        try {
            return a(httpEntity);
        } catch (JSONException e) {
            g.a("JSONObjectResponseHandler", (Throwable) e);
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public final JSONObject a(HttpEntity httpEntity) throws ParseException, JSONException, IOException {
        String str = "";
        try {
            String c = c(httpEntity);
            if (!TextUtils.isEmpty(c)) {
                if (c.length() > 1) {
                    String b = com.alphab.a.a.b(c);
                    try {
                        return new JSONObject(b);
                    } catch (Exception e) {
                        Exception exc = e;
                        str = b;
                        e = exc;
                        e.printStackTrace();
                        StringBuilder sb = new StringBuilder("wrong json  : ");
                        sb.append(str);
                        g.d("JSONObjectResponseHandler", sb.toString());
                        return null;
                    } catch (Throwable th) {
                        Throwable th2 = th;
                        str = b;
                        th = th2;
                        th.printStackTrace();
                        StringBuilder sb2 = new StringBuilder("wrong json : ");
                        sb2.append(str);
                        g.d("JSONObjectResponseHandler", sb2.toString());
                        return null;
                    }
                }
            }
            return null;
        } catch (Exception e2) {
            e = e2;
            e.printStackTrace();
            StringBuilder sb3 = new StringBuilder("wrong json  : ");
            sb3.append(str);
            g.d("JSONObjectResponseHandler", sb3.toString());
            return null;
        } catch (Throwable th3) {
            th = th3;
            th.printStackTrace();
            StringBuilder sb22 = new StringBuilder("wrong json : ");
            sb22.append(str);
            g.d("JSONObjectResponseHandler", sb22.toString());
            return null;
        }
    }
}
