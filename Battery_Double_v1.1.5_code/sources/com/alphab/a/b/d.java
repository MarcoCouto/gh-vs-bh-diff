package com.alphab.a.b;

import android.content.Context;
import android.text.TextUtils;
import com.alphab.i.c;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.base.common.e.a;
import com.mintegral.msdk.base.common.e.a.C0049a;
import com.mintegral.msdk.base.common.e.a.b;
import com.mintegral.msdk.base.common.net.l;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/* compiled from: ReqPKGAndReportManager */
public final class d extends b {
    /* access modifiers changed from: private */
    public l i = new l();
    /* access modifiers changed from: private */
    public String j;
    /* access modifiers changed from: private */
    public String k;
    /* access modifiers changed from: private */
    public List<String> l;
    /* access modifiers changed from: private */
    public String m;
    /* access modifiers changed from: private */
    public String n;
    private a o = new a() {
        public final void b() {
        }

        public final void a() {
            String str = "";
            d.a(d.this, d.this.j, d.this.k, d.this.l, d.this.m, d.this.n);
            if (d.this.i != null) {
                str = com.alphab.a.a.a(d.this.i.b());
            }
            d.this.f = new l();
            d.this.f.a(TtmlNode.TAG_P, str);
        }
    };

    public d(Context context) {
        super(context);
    }

    public final void a(f fVar) {
        this.g = fVar;
    }

    public final void a(String str, String str2, List<String> list, String str3, String str4) {
        this.j = str;
        this.k = str2;
        this.l = list;
        this.m = str3;
        this.n = str4;
        c.a(this.f990a).a().a(this.o, new b() {
            public final void a(int i) {
                if (i == C0049a.e && d.this.f990a != null && d.this.h != null) {
                    d.this.h.sendEmptyMessage(d.this.d);
                }
            }
        });
    }

    /* access modifiers changed from: protected */
    public final void a(l lVar) {
        super.a(lVar);
        lVar.a("is_clever", com.mintegral.msdk.base.common.a.v);
    }

    static /* synthetic */ void a(d dVar, String str, String str2, List list, String str3, String str4) {
        JSONArray jSONArray;
        JSONObject jSONObject;
        try {
            if (TextUtils.isEmpty(str2)) {
                str2 = "";
            }
            if (TextUtils.isEmpty(str)) {
                str = "";
            }
            jSONArray = new JSONArray();
            jSONObject = new JSONObject();
            jSONObject.put(TtmlNode.TAG_P, str);
            jSONObject.put("v", str2);
            JSONArray jSONArray2 = new JSONArray();
            if (list != null && list.size() >= 0) {
                for (int i2 = 0; i2 < list.size(); i2++) {
                    jSONArray2.put(list.get(i2));
                }
            }
            jSONObject.put("ul", jSONArray2);
            jSONObject.put("kw", str3);
            jSONObject.put("fl", str4);
        } catch (Throwable th) {
            if (MIntegralConstans.DEBUG) {
                th.printStackTrace();
            }
            return;
        }
        jSONArray.put(jSONObject);
        String b = com.mintegral.msdk.base.utils.a.b(jSONArray.toString());
        dVar.i = new l();
        if (!(dVar.i == null || dVar.f990a == null)) {
            dVar.i.a("clever", b);
        }
        dVar.a(dVar.i);
    }
}
