package com.alphab.a.b;

import android.content.Context;
import android.text.TextUtils;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.base.common.e.a;
import com.mintegral.msdk.base.common.e.a.C0049a;
import com.mintegral.msdk.base.common.e.a.b;
import com.mintegral.msdk.base.common.net.a.d;
import com.mintegral.msdk.base.common.net.l;
import com.mintegral.msdk.base.utils.g;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/* compiled from: ReqCampaignManager */
public final class c extends b {
    /* access modifiers changed from: private */
    public String i;
    /* access modifiers changed from: private */
    public List<String> j;
    /* access modifiers changed from: private */
    public String k;
    private String l;
    private int m;
    private String n;
    private a o;

    public final void a(String str) {
        this.l = str;
    }

    public final void a(int i2) {
        this.m = i2;
    }

    public c(Context context) {
        super(context);
        this.i = "ReqCampaignManager";
        this.l = "1";
        this.m = 0;
        this.o = new a() {
            public final void b() {
            }

            public final void a() {
                c.a(c.this, c.this.j, c.this.k);
                String str = "";
                try {
                    d.d(c.this.e);
                } catch (Exception e) {
                    g.a(c.this.i, e.getMessage());
                }
                if (c.this.e != null) {
                    str = com.alphab.a.a.a(c.this.e.b());
                }
                if (!TextUtils.isEmpty(str)) {
                    c cVar = c.this;
                    StringBuilder sb = new StringBuilder();
                    sb.append(com.alphab.a.b);
                    sb.append("?p=");
                    sb.append(str);
                    cVar.b = sb.toString();
                }
            }
        };
        this.e = new l();
    }

    public final void a(List<String> list) {
        if (list != null) {
            this.j = list;
            this.k = null;
            if (this.o != null) {
                com.alphab.i.c.a(this.f990a).a().a(this.o, new b() {
                    public final void a(int i) {
                        if (i == C0049a.e && c.this.f990a != null && c.this.h != null) {
                            c.this.h.sendEmptyMessage(c.this.c);
                        }
                    }
                });
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void a(l lVar) {
        super.a(lVar);
        lVar.a("clever_type", this.l);
        lVar.a("ad_num", "1");
        lVar.a("tnum", "0");
        lVar.a("only_impression", "1");
        lVar.a("ping_mode", "1");
        lVar.a("offset", "0");
        String str = this.i;
        StringBuilder sb = new StringBuilder("cleverTypeUpValue:");
        sb.append(this.m);
        g.a(str, sb.toString());
        StringBuilder sb2 = new StringBuilder();
        sb2.append(this.m);
        lVar.a("clever_update", sb2.toString());
        if (this.l.equals("2")) {
            lVar.a("pkg_source", com.mintegral.msdk.base.utils.c.a(this.n, this.f990a));
        }
    }

    static /* synthetic */ void a(c cVar, List list, String str) {
        if (list != null) {
            try {
                if (list.size() != 0) {
                    TextUtils.isEmpty(str);
                    JSONArray jSONArray = new JSONArray();
                    int size = list.size();
                    if (cVar.l.equals("2")) {
                        cVar.n = (String) list.get(0);
                    }
                    for (int i2 = 0; i2 < size; i2++) {
                        JSONObject jSONObject = new JSONObject();
                        jSONObject.put(TtmlNode.TAG_P, list.get(i2));
                        jSONObject.put("v", "");
                        jSONArray.put(jSONObject);
                    }
                    String b = com.mintegral.msdk.base.utils.a.b(jSONArray.toString());
                    if (!(cVar.e == null || cVar.f990a == null)) {
                        cVar.e.a("clever", b);
                        cVar.a(cVar.e);
                    }
                }
            } catch (Throwable th) {
                if (MIntegralConstans.DEBUG) {
                    th.printStackTrace();
                }
            }
        }
    }
}
