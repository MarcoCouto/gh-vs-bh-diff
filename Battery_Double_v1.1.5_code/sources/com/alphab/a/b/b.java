package com.alphab.a.b;

import android.content.Context;
import android.os.Build.VERSION;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.TextUtils;
import com.alphab.a;
import com.alphab.i.c;
import com.integralads.avid.library.inmobi.utils.AvidJSONUtil;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.mintegral.msdk.base.common.net.d;
import com.mintegral.msdk.base.common.net.l;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.utils.CommonMD5;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.out.MTGConfiguration;
import com.tapjoy.TapjoyConstants;

/* compiled from: AlphabReqImpl */
public abstract class b {

    /* renamed from: a reason: collision with root package name */
    protected Context f990a;
    protected String b;
    protected int c = 0;
    protected int d = 1;
    protected l e;
    protected l f;
    protected f g;
    protected Handler h = new Handler(Looper.getMainLooper()) {
        public final void handleMessage(Message message) {
            if (message.what != b.this.c) {
                if (message.what == b.this.d) {
                    e eVar = new e(b.this.f990a, c.a(b.this.f990a).b().a());
                    g.a("AlphabReqImpl", "setting  is request");
                    eVar.b(a.f985a, b.this.f, b.this.g);
                }
            } else if (b.this.f990a != null) {
                e eVar2 = new e(b.this.f990a, c.a(b.this.f990a).b().a());
                if (!TextUtils.isEmpty(b.this.b)) {
                    eVar2.a(b.this.b, new l(), (d<?>) new f() {
                    });
                }
            }
        }
    };

    public b(Context context) {
        this.f990a = context;
    }

    /* access modifiers changed from: protected */
    public void a(l lVar) {
        lVar.a("app_id", com.mintegral.msdk.base.controller.a.d().j());
        StringBuilder sb = new StringBuilder();
        sb.append(com.mintegral.msdk.base.controller.a.d().j());
        sb.append(com.mintegral.msdk.base.controller.a.d().k());
        lVar.a("sign", CommonMD5.getMD5(sb.toString()));
        lVar.a(TapjoyConstants.TJC_PLATFORM, "1");
        lVar.a(TapjoyConstants.TJC_DEVICE_OS_VERSION_NAME, VERSION.RELEASE);
        lVar.a(CampaignEx.JSON_KEY_PACKAGE_NAME, com.mintegral.msdk.base.utils.c.n(this.f990a));
        lVar.a("app_version_name", com.mintegral.msdk.base.utils.c.j(this.f990a));
        StringBuilder sb2 = new StringBuilder();
        sb2.append(com.mintegral.msdk.base.utils.c.i(this.f990a));
        lVar.a("app_version_code", sb2.toString());
        StringBuilder sb3 = new StringBuilder();
        sb3.append(com.mintegral.msdk.base.utils.c.g(this.f990a));
        lVar.a("orientation", sb3.toString());
        lVar.a("model", com.mintegral.msdk.base.utils.c.c());
        lVar.a("brand", com.mintegral.msdk.base.utils.c.e());
        lVar.a("gaid", com.mintegral.msdk.base.utils.c.k());
        lVar.a(RequestParameters.NETWORK_MNC, com.mintegral.msdk.base.utils.c.b());
        lVar.a(RequestParameters.NETWORK_MCC, com.mintegral.msdk.base.utils.c.a());
        StringBuilder sb4 = new StringBuilder();
        sb4.append(com.mintegral.msdk.base.utils.c.p(this.f990a));
        lVar.a("network_type", sb4.toString());
        lVar.a("language", com.mintegral.msdk.base.utils.c.f(this.f990a));
        lVar.a(TapjoyConstants.TJC_DEVICE_TIMEZONE, com.mintegral.msdk.base.utils.c.h());
        lVar.a("useragent", com.mintegral.msdk.base.utils.c.f());
        lVar.a("sdk_version", MTGConfiguration.SDK_VERSION);
        lVar.a("gp_version", com.mintegral.msdk.base.utils.c.q(this.f990a));
        StringBuilder sb5 = new StringBuilder();
        sb5.append(com.mintegral.msdk.base.utils.c.l(this.f990a));
        sb5.append(AvidJSONUtil.KEY_X);
        sb5.append(com.mintegral.msdk.base.utils.c.m(this.f990a));
        lVar.a("screen_size", sb5.toString());
        com.mintegral.msdk.base.common.net.a.d.a(lVar, this.f990a);
    }
}
