package com.alphab.a;

import android.text.TextUtils;
import java.util.HashMap;
import java.util.Map;

/* compiled from: AlphabBase64Util */
public final class a {

    /* renamed from: a reason: collision with root package name */
    private static Map<Character, Character> f987a;
    private static Map<Character, Character> b;

    public static String a(String str) {
        Character ch;
        String str2 = "";
        try {
            if (TextUtils.isEmpty(str)) {
                return str2;
            }
            char[] charArray = com.mintegral.msdk.base.utils.a.a(str.getBytes()).toCharArray();
            if (charArray == null || charArray.length <= 0) {
                return str2;
            }
            char[] cArr = new char[charArray.length];
            for (int i = 0; i < charArray.length; i++) {
                char c = charArray[i];
                if (f987a == null) {
                    HashMap hashMap = new HashMap();
                    f987a = hashMap;
                    hashMap.put(Character.valueOf('A'), Character.valueOf('u'));
                    f987a.put(Character.valueOf('B'), Character.valueOf('V'));
                    f987a.put(Character.valueOf('C'), Character.valueOf('U'));
                    f987a.put(Character.valueOf('D'), Character.valueOf('o'));
                    f987a.put(Character.valueOf('E'), Character.valueOf('X'));
                    f987a.put(Character.valueOf('F'), Character.valueOf('c'));
                    f987a.put(Character.valueOf('G'), Character.valueOf('3'));
                    f987a.put(Character.valueOf('H'), Character.valueOf('p'));
                    f987a.put(Character.valueOf('I'), Character.valueOf('C'));
                    f987a.put(Character.valueOf('J'), Character.valueOf('n'));
                    f987a.put(Character.valueOf('K'), Character.valueOf('D'));
                    f987a.put(Character.valueOf('L'), Character.valueOf('F'));
                    f987a.put(Character.valueOf('M'), Character.valueOf('v'));
                    f987a.put(Character.valueOf('N'), Character.valueOf('b'));
                    f987a.put(Character.valueOf('O'), Character.valueOf('8'));
                    f987a.put(Character.valueOf('P'), Character.valueOf('l'));
                    f987a.put(Character.valueOf('Q'), Character.valueOf('N'));
                    f987a.put(Character.valueOf('R'), Character.valueOf('J'));
                    f987a.put(Character.valueOf('S'), Character.valueOf('j'));
                    f987a.put(Character.valueOf('T'), Character.valueOf('9'));
                    f987a.put(Character.valueOf('U'), Character.valueOf('Z'));
                    f987a.put(Character.valueOf('V'), Character.valueOf('H'));
                    f987a.put(Character.valueOf('W'), Character.valueOf('E'));
                    f987a.put(Character.valueOf('X'), Character.valueOf('i'));
                    f987a.put(Character.valueOf('Y'), Character.valueOf('a'));
                    f987a.put(Character.valueOf('Z'), Character.valueOf('7'));
                    f987a.put(Character.valueOf('a'), Character.valueOf('Q'));
                    f987a.put(Character.valueOf('b'), Character.valueOf('Y'));
                    f987a.put(Character.valueOf('c'), Character.valueOf('r'));
                    f987a.put(Character.valueOf('d'), Character.valueOf('f'));
                    f987a.put(Character.valueOf('e'), Character.valueOf('S'));
                    f987a.put(Character.valueOf('f'), Character.valueOf('m'));
                    f987a.put(Character.valueOf('g'), Character.valueOf('R'));
                    f987a.put(Character.valueOf('h'), Character.valueOf('O'));
                    f987a.put(Character.valueOf('i'), Character.valueOf('k'));
                    f987a.put(Character.valueOf('j'), Character.valueOf('G'));
                    f987a.put(Character.valueOf('k'), Character.valueOf('K'));
                    f987a.put(Character.valueOf('l'), Character.valueOf('A'));
                    f987a.put(Character.valueOf('m'), Character.valueOf('0'));
                    f987a.put(Character.valueOf('n'), Character.valueOf('e'));
                    f987a.put(Character.valueOf('o'), Character.valueOf('h'));
                    f987a.put(Character.valueOf('p'), Character.valueOf('I'));
                    f987a.put(Character.valueOf('q'), Character.valueOf('d'));
                    f987a.put(Character.valueOf('r'), Character.valueOf('t'));
                    f987a.put(Character.valueOf('s'), Character.valueOf('z'));
                    f987a.put(Character.valueOf('t'), Character.valueOf('B'));
                    f987a.put(Character.valueOf('u'), Character.valueOf('6'));
                    f987a.put(Character.valueOf('v'), Character.valueOf('4'));
                    f987a.put(Character.valueOf('w'), Character.valueOf('M'));
                    f987a.put(Character.valueOf('x'), Character.valueOf('q'));
                    f987a.put(Character.valueOf('y'), Character.valueOf('2'));
                    f987a.put(Character.valueOf('z'), Character.valueOf('g'));
                    f987a.put(Character.valueOf('0'), Character.valueOf('P'));
                    f987a.put(Character.valueOf('1'), Character.valueOf('5'));
                    f987a.put(Character.valueOf('2'), Character.valueOf('s'));
                    f987a.put(Character.valueOf('3'), Character.valueOf('y'));
                    f987a.put(Character.valueOf('4'), Character.valueOf('T'));
                    f987a.put(Character.valueOf('5'), Character.valueOf('L'));
                    f987a.put(Character.valueOf('6'), Character.valueOf('1'));
                    f987a.put(Character.valueOf('7'), Character.valueOf('w'));
                    f987a.put(Character.valueOf('8'), Character.valueOf('W'));
                    f987a.put(Character.valueOf('9'), Character.valueOf('x'));
                    f987a.put(Character.valueOf('+'), Character.valueOf('+'));
                    f987a.put(Character.valueOf('/'), Character.valueOf('/'));
                }
                if (f987a.containsKey(Character.valueOf(c))) {
                    ch = (Character) f987a.get(Character.valueOf(c));
                } else {
                    ch = Character.valueOf(c);
                }
                cArr[i] = ch.charValue();
            }
            return new String(cArr);
        } catch (Exception e) {
            e.printStackTrace();
            return str2;
        }
    }

    public static String b(String str) {
        Character ch;
        String str2 = "";
        try {
            if (!TextUtils.isEmpty(str)) {
                char[] charArray = str.toCharArray();
                if (charArray != null && charArray.length > 0) {
                    char[] cArr = new char[charArray.length];
                    for (int i = 0; i < charArray.length; i++) {
                        char c = charArray[i];
                        if (b == null) {
                            HashMap hashMap = new HashMap();
                            b = hashMap;
                            hashMap.put(Character.valueOf('u'), Character.valueOf('A'));
                            b.put(Character.valueOf('V'), Character.valueOf('B'));
                            b.put(Character.valueOf('U'), Character.valueOf('C'));
                            b.put(Character.valueOf('o'), Character.valueOf('D'));
                            b.put(Character.valueOf('X'), Character.valueOf('E'));
                            b.put(Character.valueOf('c'), Character.valueOf('F'));
                            b.put(Character.valueOf('3'), Character.valueOf('G'));
                            b.put(Character.valueOf('p'), Character.valueOf('H'));
                            b.put(Character.valueOf('C'), Character.valueOf('I'));
                            b.put(Character.valueOf('n'), Character.valueOf('J'));
                            b.put(Character.valueOf('D'), Character.valueOf('K'));
                            b.put(Character.valueOf('F'), Character.valueOf('L'));
                            b.put(Character.valueOf('v'), Character.valueOf('M'));
                            b.put(Character.valueOf('b'), Character.valueOf('N'));
                            b.put(Character.valueOf('8'), Character.valueOf('O'));
                            b.put(Character.valueOf('l'), Character.valueOf('P'));
                            b.put(Character.valueOf('N'), Character.valueOf('Q'));
                            b.put(Character.valueOf('J'), Character.valueOf('R'));
                            b.put(Character.valueOf('j'), Character.valueOf('S'));
                            b.put(Character.valueOf('9'), Character.valueOf('T'));
                            b.put(Character.valueOf('Z'), Character.valueOf('U'));
                            b.put(Character.valueOf('H'), Character.valueOf('V'));
                            b.put(Character.valueOf('E'), Character.valueOf('W'));
                            b.put(Character.valueOf('i'), Character.valueOf('X'));
                            b.put(Character.valueOf('a'), Character.valueOf('Y'));
                            b.put(Character.valueOf('7'), Character.valueOf('Z'));
                            b.put(Character.valueOf('Q'), Character.valueOf('a'));
                            b.put(Character.valueOf('Y'), Character.valueOf('b'));
                            b.put(Character.valueOf('r'), Character.valueOf('c'));
                            b.put(Character.valueOf('f'), Character.valueOf('d'));
                            b.put(Character.valueOf('S'), Character.valueOf('e'));
                            b.put(Character.valueOf('m'), Character.valueOf('f'));
                            b.put(Character.valueOf('R'), Character.valueOf('g'));
                            b.put(Character.valueOf('O'), Character.valueOf('h'));
                            b.put(Character.valueOf('k'), Character.valueOf('i'));
                            b.put(Character.valueOf('G'), Character.valueOf('j'));
                            b.put(Character.valueOf('K'), Character.valueOf('k'));
                            b.put(Character.valueOf('A'), Character.valueOf('l'));
                            b.put(Character.valueOf('0'), Character.valueOf('m'));
                            b.put(Character.valueOf('e'), Character.valueOf('n'));
                            b.put(Character.valueOf('h'), Character.valueOf('o'));
                            b.put(Character.valueOf('I'), Character.valueOf('p'));
                            b.put(Character.valueOf('d'), Character.valueOf('q'));
                            b.put(Character.valueOf('t'), Character.valueOf('r'));
                            b.put(Character.valueOf('z'), Character.valueOf('s'));
                            b.put(Character.valueOf('B'), Character.valueOf('t'));
                            b.put(Character.valueOf('6'), Character.valueOf('u'));
                            b.put(Character.valueOf('4'), Character.valueOf('v'));
                            b.put(Character.valueOf('M'), Character.valueOf('w'));
                            b.put(Character.valueOf('q'), Character.valueOf('x'));
                            b.put(Character.valueOf('2'), Character.valueOf('y'));
                            b.put(Character.valueOf('g'), Character.valueOf('z'));
                            b.put(Character.valueOf('P'), Character.valueOf('0'));
                            b.put(Character.valueOf('5'), Character.valueOf('1'));
                            b.put(Character.valueOf('s'), Character.valueOf('2'));
                            b.put(Character.valueOf('y'), Character.valueOf('3'));
                            b.put(Character.valueOf('T'), Character.valueOf('4'));
                            b.put(Character.valueOf('L'), Character.valueOf('5'));
                            b.put(Character.valueOf('1'), Character.valueOf('6'));
                            b.put(Character.valueOf('w'), Character.valueOf('7'));
                            b.put(Character.valueOf('W'), Character.valueOf('8'));
                            b.put(Character.valueOf('x'), Character.valueOf('9'));
                            b.put(Character.valueOf('+'), Character.valueOf('+'));
                            b.put(Character.valueOf('/'), Character.valueOf('/'));
                        }
                        if (b.containsKey(Character.valueOf(c))) {
                            ch = (Character) b.get(Character.valueOf(c));
                        } else {
                            ch = Character.valueOf(c);
                        }
                        cArr[i] = ch.charValue();
                    }
                    str2 = new String(cArr);
                }
                return new String(com.mintegral.msdk.base.utils.a.a(str2));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return str2;
    }
}
