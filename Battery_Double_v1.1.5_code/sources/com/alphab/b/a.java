package com.alphab.b;

import android.text.TextUtils;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.click.e;
import java.util.ArrayList;

/* compiled from: ExploreGPUrl */
public final class a {

    /* renamed from: a reason: collision with root package name */
    private String f998a;
    private b b;
    private e c = new e();
    private int d;
    private int e;

    public a(String str, int i) {
        this.f998a = str;
        this.d = i;
    }

    public final void a(b bVar) {
        this.b = bVar;
    }

    public final void a() {
        ArrayList arrayList = new ArrayList();
        try {
            arrayList.add(this.f998a);
            String str = this.f998a;
            int i = 1;
            while (true) {
                if (i > this.d) {
                    break;
                }
                com.mintegral.msdk.click.e.a a2 = a(str);
                if (a2 != null) {
                    if (a2.f != 200) {
                        str = a2.f2657a;
                        if (TextUtils.isEmpty(str)) {
                            break;
                        }
                        arrayList.add(str);
                        i++;
                    } else {
                        this.e = a2.e;
                        break;
                    }
                } else {
                    break;
                }
            }
        } catch (Throwable th) {
            if (MIntegralConstans.DEBUG) {
                th.printStackTrace();
            }
        }
        if (this.b != null) {
            this.b.a(arrayList, this.e);
        }
    }

    private com.mintegral.msdk.click.e.a a(String str) {
        try {
            if (this.c == null) {
                return null;
            }
            this.c.a();
            return this.c.a(str, false, false, null);
        } catch (Throwable th) {
            if (!MIntegralConstans.DEBUG) {
                return null;
            }
            th.printStackTrace();
            return null;
        }
    }
}
