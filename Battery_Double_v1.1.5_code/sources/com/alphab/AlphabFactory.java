package com.alphab;

public interface AlphabFactory {
    Operation createAlphab();
}
