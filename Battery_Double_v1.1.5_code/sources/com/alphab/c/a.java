package com.alphab.c;

import android.content.Context;
import android.text.TextUtils;
import com.mintegral.msdk.base.common.d.c;
import com.mintegral.msdk.base.common.d.c.b;
import com.mintegral.msdk.base.utils.g;

/* compiled from: AlphabReportTool */
public final class a {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public static String f999a = "AlphabReportTool";

    public static void a(Context context, b bVar) {
        try {
            String bVar2 = bVar.toString();
            String str = f999a;
            StringBuilder sb = new StringBuilder("reportDLAPK===");
            sb.append(bVar2);
            g.a(str, sb.toString());
            if (context != null && !TextUtils.isEmpty(bVar2)) {
                com.mintegral.msdk.base.common.d.c.a aVar = new com.mintegral.msdk.base.common.d.c.a(context);
                aVar.c();
                aVar.b(com.mintegral.msdk.base.common.a.f, c.a(bVar2, context), new b() {
                    public final void a(String str) {
                        g.d(a.f999a, str);
                    }

                    public final void b(String str) {
                        g.d(a.f999a, str);
                    }
                });
            }
        } catch (Throwable th) {
            g.c(f999a, th.getMessage(), th);
        }
    }
}
