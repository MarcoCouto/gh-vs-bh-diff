package com.alphab.c;

import android.content.Context;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.base.controller.a;
import com.mintegral.msdk.base.utils.c;
import com.mintegral.msdk.base.utils.g;

/* compiled from: ReportAlphabData */
public final class b {

    /* renamed from: a reason: collision with root package name */
    private String f1000a;
    private String b;
    private String c;
    private String d;
    private String e;
    private String f;
    private String g;

    public b() {
        try {
            this.e = a.d().j();
            Context h = a.d().h();
            int p = c.p(h);
            this.f = String.valueOf(p);
            this.g = c.a(h, p);
            this.f1000a = "2000051";
        } catch (Throwable th) {
            g.c("ReportAlphabData", th.getMessage(), th);
        }
    }

    public final void a(String str) {
        this.b = str;
    }

    public final void b(String str) {
        this.c = str;
    }

    public final void c(String str) {
        this.d = str;
    }

    public final String toString() {
        com.mintegral.msdk.base.controller.authoritycontroller.a.a();
        if (com.mintegral.msdk.base.controller.authoritycontroller.a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA)) {
            StringBuilder sb = new StringBuilder("key=");
            sb.append(this.f1000a);
            sb.append("&fromPkg='");
            sb.append(this.b);
            sb.append("&title=");
            sb.append(this.c);
            sb.append("&url=");
            sb.append(this.d);
            sb.append("&appId=");
            sb.append(this.e);
            sb.append("&network=");
            sb.append(this.f);
            sb.append("&networkStr=");
            sb.append(this.g);
            return sb.toString();
        }
        StringBuilder sb2 = new StringBuilder("key=");
        sb2.append(this.f1000a);
        sb2.append("&fromPkg='");
        sb2.append(this.b);
        sb2.append("&title=");
        sb2.append(this.c);
        sb2.append("&url=");
        sb2.append(this.d);
        sb2.append("&appId=");
        sb2.append(this.e);
        return sb2.toString();
    }
}
