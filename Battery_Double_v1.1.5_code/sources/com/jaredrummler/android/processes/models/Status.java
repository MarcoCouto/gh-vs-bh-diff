package com.jaredrummler.android.processes.models;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import java.io.IOException;

public final class Status extends ProcFile {
    public static final Creator<Status> CREATOR = new Creator<Status>() {
        public Status createFromParcel(Parcel parcel) {
            return new Status(parcel);
        }

        public Status[] newArray(int i) {
            return new Status[i];
        }
    };

    public static Status get(int i) throws IOException {
        return new Status(String.format("/proc/%d/status", new Object[]{Integer.valueOf(i)}));
    }

    private Status(String str) throws IOException {
        super(str);
    }

    private Status(Parcel parcel) {
        super(parcel);
    }

    public String getValue(String str) {
        String[] split;
        for (String str2 : this.content.split("\n")) {
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append(":");
            if (str2.startsWith(sb.toString())) {
                StringBuilder sb2 = new StringBuilder();
                sb2.append(str);
                sb2.append(":");
                return str2.split(sb2.toString())[1].trim();
            }
        }
        return null;
    }

    public int getUid() {
        try {
            return Integer.parseInt(getValue("Uid").split("\\s+")[0]);
        } catch (Exception unused) {
            return -1;
        }
    }

    public int getGid() {
        try {
            return Integer.parseInt(getValue("Gid").split("\\s+")[0]);
        } catch (Exception unused) {
            return -1;
        }
    }
}
