package com.jaredrummler.android.processes.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class ControlGroup implements Parcelable {
    public static final Creator<ControlGroup> CREATOR = new Creator<ControlGroup>() {
        public ControlGroup createFromParcel(Parcel parcel) {
            return new ControlGroup(parcel);
        }

        public ControlGroup[] newArray(int i) {
            return new ControlGroup[i];
        }
    };
    public final String group;
    public final int id;
    public final String subsystems;

    public int describeContents() {
        return 0;
    }

    protected ControlGroup(String str) throws NumberFormatException, IndexOutOfBoundsException {
        String[] split = str.split(":");
        this.id = Integer.parseInt(split[0]);
        this.subsystems = split[1];
        this.group = split[2];
    }

    protected ControlGroup(Parcel parcel) {
        this.id = parcel.readInt();
        this.subsystems = parcel.readString();
        this.group = parcel.readString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.id);
        parcel.writeString(this.subsystems);
        parcel.writeString(this.group);
    }

    public String toString() {
        return String.format("%d:%s:%s", new Object[]{Integer.valueOf(this.id), this.subsystems, this.group});
    }
}
