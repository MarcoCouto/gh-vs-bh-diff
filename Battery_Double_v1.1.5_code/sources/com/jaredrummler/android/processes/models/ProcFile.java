package com.jaredrummler.android.processes.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class ProcFile extends File implements Parcelable {
    public static final Creator<ProcFile> CREATOR = new Creator<ProcFile>() {
        public ProcFile createFromParcel(Parcel parcel) {
            return new ProcFile(parcel);
        }

        public ProcFile[] newArray(int i) {
            return new ProcFile[i];
        }
    };
    public final String content;

    public int describeContents() {
        return 0;
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x0033 A[SYNTHETIC, Splitter:B:16:0x0033] */
    protected static String readFile(String str) throws IOException {
        BufferedReader bufferedReader;
        try {
            StringBuilder sb = new StringBuilder();
            bufferedReader = new BufferedReader(new FileReader(str));
            try {
                String str2 = "";
                for (String readLine = bufferedReader.readLine(); readLine != null; readLine = bufferedReader.readLine()) {
                    sb.append(str2);
                    sb.append(readLine);
                    str2 = "\n";
                }
                String sb2 = sb.toString();
                try {
                    bufferedReader.close();
                } catch (IOException unused) {
                }
                return sb2;
            } catch (Throwable th) {
                th = th;
                if (bufferedReader != null) {
                    try {
                        bufferedReader.close();
                    } catch (IOException unused2) {
                    }
                }
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            bufferedReader = null;
            if (bufferedReader != null) {
            }
            throw th;
        }
    }

    protected ProcFile(String str) throws IOException {
        super(str);
        this.content = readFile(str);
    }

    protected ProcFile(Parcel parcel) {
        super(parcel.readString());
        this.content = parcel.readString();
    }

    public long length() {
        return (long) this.content.length();
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(getAbsolutePath());
        parcel.writeString(this.content);
    }
}
