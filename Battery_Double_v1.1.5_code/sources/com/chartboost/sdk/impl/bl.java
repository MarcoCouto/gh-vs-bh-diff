package com.chartboost.sdk.impl;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

public class bl extends OutputStream {

    /* renamed from: a reason: collision with root package name */
    private static final byte[] f1901a = new byte[0];
    private final List<byte[]> b;
    private int c;
    private int d;
    private byte[] e;
    private int f;

    public void close() throws IOException {
    }

    public bl() {
        this(1024);
    }

    public bl(int i) {
        this.b = new ArrayList();
        if (i >= 0) {
            synchronized (this) {
                a(i);
            }
            return;
        }
        StringBuilder sb = new StringBuilder();
        sb.append("Negative initial size: ");
        sb.append(i);
        throw new IllegalArgumentException(sb.toString());
    }

    private void a(int i) {
        if (this.c < this.b.size() - 1) {
            this.d += this.e.length;
            this.c++;
            this.e = (byte[]) this.b.get(this.c);
            return;
        }
        if (this.e == null) {
            this.d = 0;
        } else {
            i = Math.max(this.e.length << 1, i - this.d);
            this.d += this.e.length;
        }
        this.c++;
        this.e = new byte[i];
        this.b.add(this.e);
    }

    public void write(byte[] bArr, int i, int i2) {
        if (i >= 0 && i <= bArr.length && i2 >= 0) {
            int i3 = i + i2;
            if (i3 <= bArr.length && i3 >= 0) {
                if (i2 != 0) {
                    synchronized (this) {
                        int i4 = this.f + i2;
                        int i5 = this.f - this.d;
                        while (i2 > 0) {
                            int min = Math.min(i2, this.e.length - i5);
                            System.arraycopy(bArr, i3 - i2, this.e, i5, min);
                            i2 -= min;
                            if (i2 > 0) {
                                a(i4);
                                i5 = 0;
                            }
                        }
                        this.f = i4;
                    }
                    return;
                }
                return;
            }
        }
        throw new IndexOutOfBoundsException();
    }

    public synchronized void write(int i) {
        int i2 = this.f - this.d;
        if (i2 == this.e.length) {
            a(this.f + 1);
            i2 = 0;
        }
        this.e[i2] = (byte) i;
        this.f++;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x002c, code lost:
        return r1;
     */
    public synchronized byte[] a() {
        int i = this.f;
        if (i != 0) {
            byte[] bArr = new byte[i];
            int i2 = 0;
            for (byte[] bArr2 : this.b) {
                int min = Math.min(bArr2.length, i);
                System.arraycopy(bArr2, 0, bArr, i2, min);
                i2 += min;
                i -= min;
                if (i == 0) {
                    break;
                }
            }
        } else {
            return f1901a;
        }
    }

    public String toString() {
        return new String(a());
    }
}
