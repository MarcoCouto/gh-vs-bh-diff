package com.chartboost.sdk.impl;

import android.os.Handler;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.animation.AlphaAnimation;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import com.chartboost.sdk.Libraries.CBLogging;
import com.chartboost.sdk.Model.a;
import com.chartboost.sdk.Model.c;
import com.chartboost.sdk.d;

public class ay {

    /* renamed from: a reason: collision with root package name */
    private final Handler f1882a;

    public static Integer a(int i) {
        if (i < 1 || i > 9) {
            return null;
        }
        return Integer.valueOf(i);
    }

    public ay(Handler handler) {
        this.f1882a = handler;
    }

    public void a(int i, c cVar, Runnable runnable, d dVar) {
        a(i, cVar, runnable, true, dVar);
    }

    public void a(int i, c cVar, Runnable runnable) {
        a(i, cVar, runnable, false);
    }

    private void a(int i, c cVar, Runnable runnable, boolean z, d dVar) {
        if (i == 7) {
            if (runnable != null) {
                runnable.run();
            }
        } else if (cVar == null || cVar.s == null) {
            CBLogging.a("AnimationManager", "Transition of impression canceled due to lack of container");
        } else {
            final View d = cVar.s.d();
            if (d == null) {
                dVar.d(cVar);
                CBLogging.a("AnimationManager", "Transition of impression canceled due to lack of view");
                return;
            }
            ViewTreeObserver viewTreeObserver = d.getViewTreeObserver();
            if (viewTreeObserver.isAlive()) {
                final int i2 = i;
                final c cVar2 = cVar;
                final Runnable runnable2 = runnable;
                final boolean z2 = z;
                AnonymousClass1 r1 = new OnGlobalLayoutListener() {
                    public void onGlobalLayout() {
                        d.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                        ay.this.a(i2, cVar2, runnable2, z2);
                    }
                };
                viewTreeObserver.addOnGlobalLayoutListener(r1);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(int i, c cVar, Runnable runnable, boolean z) {
        bc bcVar;
        ScaleAnimation scaleAnimation;
        TranslateAnimation translateAnimation;
        bc bcVar2;
        ScaleAnimation scaleAnimation2;
        TranslateAnimation translateAnimation2;
        AlphaAnimation alphaAnimation;
        int i2 = i;
        c cVar2 = cVar;
        Runnable runnable2 = runnable;
        AnimationSet animationSet = new AnimationSet(true);
        animationSet.addAnimation(new AlphaAnimation(1.0f, 1.0f));
        if (cVar2 == null || cVar2.s == null) {
            CBLogging.a("AnimationManager", "Transition of impression canceled due to lack of container");
            if (runnable2 != null) {
                runnable.run();
            }
            return;
        }
        View d = cVar2.s.d();
        if (d == null) {
            if (runnable2 != null) {
                runnable.run();
            }
            CBLogging.a("AnimationManager", "Transition of impression canceled due to lack of view");
            return;
        }
        if (cVar2.n == 2 || cVar2.n == 1) {
            d = cVar2.s;
        }
        float width = (float) d.getWidth();
        float height = (float) d.getHeight();
        int i3 = cVar2.p.m;
        switch (i2) {
            case 1:
                if (z) {
                    bcVar = new bc(-60.0f, 0.0f, width / 2.0f, height / 2.0f, true);
                } else {
                    bcVar = new bc(0.0f, 60.0f, width / 2.0f, height / 2.0f, true);
                }
                bcVar.setDuration(500);
                bcVar.setFillAfter(true);
                animationSet.addAnimation(bcVar);
                if (z) {
                    scaleAnimation = new ScaleAnimation(0.4f, 1.0f, 0.4f, 1.0f);
                } else {
                    scaleAnimation = new ScaleAnimation(1.0f, 0.4f, 1.0f, 0.4f);
                }
                scaleAnimation.setDuration(500);
                scaleAnimation.setFillAfter(true);
                animationSet.addAnimation(scaleAnimation);
                if (z) {
                    translateAnimation = new TranslateAnimation((-width) * 0.4f, 0.0f, height * 0.3f, 0.0f);
                } else {
                    translateAnimation = new TranslateAnimation(0.0f, width, 0.0f, height * 0.3f);
                }
                translateAnimation.setDuration(500);
                translateAnimation.setFillAfter(true);
                animationSet.addAnimation(translateAnimation);
                break;
            case 2:
                if (!z) {
                    ScaleAnimation scaleAnimation3 = new ScaleAnimation(1.0f, 0.0f, 1.0f, 0.0f, 1, 0.5f, 1, 0.5f);
                    scaleAnimation3.setDuration(500);
                    scaleAnimation3.setStartOffset(0);
                    scaleAnimation3.setFillAfter(true);
                    animationSet.addAnimation(scaleAnimation3);
                    break;
                } else {
                    ScaleAnimation scaleAnimation4 = new ScaleAnimation(0.6f, 1.1f, 0.6f, 1.1f, 1, 0.5f, 1, 0.5f);
                    float f = (float) 500;
                    float f2 = 0.6f * f;
                    scaleAnimation4.setDuration((long) Math.round(f2));
                    scaleAnimation4.setStartOffset(0);
                    scaleAnimation4.setFillAfter(true);
                    animationSet.addAnimation(scaleAnimation4);
                    ScaleAnimation scaleAnimation5 = new ScaleAnimation(1.0f, 0.81818175f, 1.0f, 0.81818175f, 1, 0.5f, 1, 0.5f);
                    scaleAnimation5.setDuration((long) Math.round(0.19999999f * f));
                    scaleAnimation5.setStartOffset((long) Math.round(f2));
                    scaleAnimation5.setFillAfter(true);
                    animationSet.addAnimation(scaleAnimation5);
                    ScaleAnimation scaleAnimation6 = new ScaleAnimation(1.0f, 1.1111112f, 1.0f, 1.1111112f, 1, 0.5f, 1, 0.5f);
                    scaleAnimation6.setDuration((long) Math.round(0.099999964f * f));
                    scaleAnimation6.setStartOffset((long) Math.round(f * 0.8f));
                    scaleAnimation6.setFillAfter(true);
                    animationSet.addAnimation(scaleAnimation6);
                    break;
                }
            case 3:
                if (z) {
                    bcVar2 = new bc(-60.0f, 0.0f, width / 2.0f, height / 2.0f, false);
                } else {
                    bcVar2 = new bc(0.0f, 60.0f, width / 2.0f, height / 2.0f, false);
                }
                bcVar2.setDuration(500);
                bcVar2.setFillAfter(true);
                animationSet.addAnimation(bcVar2);
                if (z) {
                    scaleAnimation2 = new ScaleAnimation(0.4f, 1.0f, 0.4f, 1.0f);
                } else {
                    scaleAnimation2 = new ScaleAnimation(1.0f, 0.4f, 1.0f, 0.4f);
                }
                scaleAnimation2.setDuration(500);
                scaleAnimation2.setFillAfter(true);
                animationSet.addAnimation(scaleAnimation2);
                if (z) {
                    translateAnimation2 = new TranslateAnimation(width * 0.3f, 0.0f, (-height) * 0.4f, 0.0f);
                } else {
                    translateAnimation2 = new TranslateAnimation(0.0f, width * 0.3f, 0.0f, height);
                }
                translateAnimation2.setDuration(500);
                translateAnimation2.setFillAfter(true);
                animationSet.addAnimation(translateAnimation2);
                break;
            case 4:
                TranslateAnimation translateAnimation3 = new TranslateAnimation(0.0f, 0.0f, z ? -height : 0.0f, z ? 0.0f : -height);
                translateAnimation3.setDuration(500);
                translateAnimation3.setFillAfter(true);
                animationSet.addAnimation(translateAnimation3);
                break;
            case 5:
                float f3 = z ? height : 0.0f;
                if (z) {
                    height = 0.0f;
                }
                TranslateAnimation translateAnimation4 = new TranslateAnimation(0.0f, 0.0f, f3, height);
                translateAnimation4.setDuration(500);
                translateAnimation4.setFillAfter(true);
                animationSet.addAnimation(translateAnimation4);
                break;
            case 6:
                if (z) {
                    alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
                } else {
                    alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
                }
                alphaAnimation.setDuration(500);
                alphaAnimation.setFillAfter(true);
                animationSet = new AnimationSet(true);
                animationSet.addAnimation(alphaAnimation);
                break;
            case 8:
                float f4 = z ? width : 0.0f;
                if (z) {
                    width = 0.0f;
                }
                TranslateAnimation translateAnimation5 = new TranslateAnimation(f4, width, 0.0f, 0.0f);
                translateAnimation5.setDuration(500);
                translateAnimation5.setFillAfter(true);
                animationSet.addAnimation(translateAnimation5);
                break;
            case 9:
                TranslateAnimation translateAnimation6 = new TranslateAnimation(z ? -width : 0.0f, z ? 0.0f : -width, 0.0f, 0.0f);
                translateAnimation6.setDuration(500);
                translateAnimation6.setFillAfter(true);
                animationSet.addAnimation(translateAnimation6);
                break;
        }
        if (i2 == 7) {
            if (runnable2 != null) {
                runnable.run();
            }
        } else {
            if (runnable2 != null) {
                this.f1882a.postDelayed(runnable2, 500);
            }
            d.startAnimation(animationSet);
        }
    }

    public void a(boolean z, View view, a aVar) {
        int i = aVar.m;
        a(z, view, 500);
    }

    public void a(boolean z, View view, long j) {
        view.clearAnimation();
        if (z) {
            view.setVisibility(0);
        }
        float f = 1.0f;
        float f2 = z ? 0.0f : 1.0f;
        if (!z) {
            f = 0.0f;
        }
        AlphaAnimation alphaAnimation = new AlphaAnimation(f2, f);
        alphaAnimation.setDuration(j);
        alphaAnimation.setFillBefore(true);
        view.startAnimation(alphaAnimation);
    }
}
