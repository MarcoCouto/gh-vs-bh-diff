package com.chartboost.sdk.impl;

import com.chartboost.sdk.Libraries.CBLogging;
import com.chartboost.sdk.Libraries.f;
import com.chartboost.sdk.Model.CBError;
import com.chartboost.sdk.Model.b;
import com.chartboost.sdk.Model.e;
import com.chartboost.sdk.i;
import com.chartboost.sdk.impl.al.a;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import org.json.JSONObject;

public class m implements a {

    /* renamed from: a reason: collision with root package name */
    public final l f1917a;
    private final f b;
    private final aj c;
    private final ar d;
    private final com.chartboost.sdk.Tracking.a e;
    private final AtomicReference<e> f;
    private int g = 1;
    private int h = 0;
    private long i = 0;
    private al j = null;
    private AtomicInteger k = null;

    public m(l lVar, f fVar, aj ajVar, ar arVar, com.chartboost.sdk.Tracking.a aVar, AtomicReference<e> atomicReference) {
        this.f1917a = lVar;
        this.b = fVar;
        this.c = ajVar;
        this.d = arVar;
        this.e = aVar;
        this.f = atomicReference;
    }

    public synchronized void a() {
        try {
            CBLogging.b("Chartboost SDK", "Sdk Version = 7.5.0, Commit: 7fc7bc32841a43689553f0e08928c7ad6ed7e23b");
            e eVar = (e) this.f.get();
            a(eVar);
            if (!eVar.c && !eVar.b) {
                if (i.v) {
                    if (this.g == 3) {
                        if (this.k.get() <= 0) {
                            CBLogging.a("Prefetcher", "Change state to COOLDOWN");
                            this.g = 4;
                            this.k = null;
                        } else {
                            return;
                        }
                    }
                    if (this.g == 4) {
                        if (this.i - System.nanoTime() > 0) {
                            CBLogging.a("Prefetcher", "Prefetch session is still active. Won't be making any new prefetch until the prefetch session expires");
                            return;
                        }
                        CBLogging.a("Prefetcher", "Change state to IDLE");
                        this.g = 1;
                        this.h = 0;
                        this.i = 0;
                    }
                    if (this.g == 1) {
                        if (eVar.y) {
                            ao aoVar = new ao(eVar.H, this.d, this.e, 2, this);
                            aoVar.a("cache_assets", this.b.c(), 0);
                            aoVar.l = true;
                            CBLogging.a("Prefetcher", "Change state to AWAIT_PREFETCH_RESPONSE");
                            this.g = 2;
                            this.h = 2;
                            this.i = System.nanoTime() + TimeUnit.MINUTES.toNanos((long) eVar.D);
                            this.j = aoVar;
                        } else if (eVar.e) {
                            al alVar = new al("/api/video-prefetch", this.d, this.e, 2, this);
                            alVar.a("local-videos", (Object) this.b.b());
                            alVar.l = true;
                            CBLogging.a("Prefetcher", "Change state to AWAIT_PREFETCH_RESPONSE");
                            this.g = 2;
                            this.h = 1;
                            this.i = System.nanoTime() + TimeUnit.MINUTES.toNanos((long) eVar.i);
                            this.j = alVar;
                        } else {
                            CBLogging.b("Prefetcher", "Did not prefetch because neither native nor webview are enabled.");
                            return;
                        }
                        this.c.a(this.j);
                    } else {
                        return;
                    }
                }
            }
            b();
        } catch (Exception e2) {
            if (this.g == 2) {
                CBLogging.a("Prefetcher", "Change state to COOLDOWN");
                this.g = 4;
                this.j = null;
            }
            com.chartboost.sdk.Tracking.a.a(getClass(), "prefetch", e2);
        }
    }

    private void a(e eVar) {
        boolean z = eVar.y;
        if ((this.h == 1 && !(!z && eVar.e)) || (this.h == 2 && !z)) {
            CBLogging.a("Prefetcher", "Change state to IDLE");
            this.g = 1;
            this.h = 0;
            this.i = 0;
            this.j = null;
            AtomicInteger atomicInteger = this.k;
            this.k = null;
            if (atomicInteger != null) {
                this.f1917a.a(atomicInteger);
            }
        }
    }

    public synchronized void b() {
        if (this.g == 2) {
            CBLogging.a("Prefetcher", "Change state to COOLDOWN");
            this.g = 4;
            this.j = null;
        } else if (this.g == 3) {
            CBLogging.a("Prefetcher", "Change state to COOLDOWN");
            this.g = 4;
            AtomicInteger atomicInteger = this.k;
            this.k = null;
            if (atomicInteger != null) {
                this.f1917a.a(atomicInteger);
            }
        }
    }

    public synchronized void a(al alVar, JSONObject jSONObject) {
        try {
            if (this.g == 2) {
                if (alVar == this.j) {
                    CBLogging.a("Prefetcher", "Change state to DOWNLOAD_ASSETS");
                    this.g = 3;
                    this.j = null;
                    this.k = new AtomicInteger();
                    if (jSONObject != null) {
                        StringBuilder sb = new StringBuilder();
                        sb.append("Got Asset list for Prefetch from server :)");
                        sb.append(jSONObject);
                        CBLogging.a("Prefetcher", sb.toString());
                        if (this.h == 1) {
                            this.f1917a.a(3, b.a(jSONObject), this.k, null);
                        } else if (this.h == 2) {
                            this.f1917a.a(3, b.a(jSONObject, ((e) this.f.get()).v), this.k, null);
                        }
                    }
                } else {
                    return;
                }
            } else {
                return;
            }
        } catch (Exception e2) {
            com.chartboost.sdk.Tracking.a.a(getClass(), "onSuccess", e2);
        }
        return;
    }

    public synchronized void a(al alVar, CBError cBError) {
        if (this.g == 2) {
            if (alVar == this.j) {
                this.j = null;
                CBLogging.a("Prefetcher", "Change state to COOLDOWN");
                this.g = 4;
            }
        }
    }
}
