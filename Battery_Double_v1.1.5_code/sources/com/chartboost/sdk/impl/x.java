package com.chartboost.sdk.impl;

import android.content.Context;
import android.graphics.Point;
import android.os.Handler;
import android.support.v4.app.NotificationCompat;
import android.support.v4.view.ViewCompat;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RelativeLayout.LayoutParams;
import com.chartboost.sdk.Libraries.CBLogging;
import com.chartboost.sdk.Libraries.CBUtility;
import com.chartboost.sdk.Libraries.f;
import com.chartboost.sdk.Libraries.h;
import com.chartboost.sdk.Model.CBError.CBImpressionError;
import com.chartboost.sdk.Model.c;
import com.chartboost.sdk.e;
import com.chartboost.sdk.g;
import com.github.mikephil.charting.utils.Utils;
import com.google.android.exoplayer2.util.MimeTypes;
import com.integralads.avid.library.inmobi.utils.AvidJSONUtil;
import com.ironsource.sdk.constants.Constants.ParametersKeys;
import com.tapjoy.TJAdUnitConstants.String;
import com.vungle.warren.model.AdvertisementDBAdapter.AdvertisementColumns;
import com.yandex.mobile.ads.video.tracking.Tracker.Events;
import java.io.File;
import org.json.JSONObject;

public class x extends w {
    protected boolean A;
    protected boolean B;
    protected boolean C = false;
    protected int D = 0;
    protected h E;
    protected h F;
    protected h G;
    protected h H;
    protected h I;
    protected h J;
    protected h K;
    protected h L;
    protected boolean M = false;
    protected boolean N = false;
    protected boolean O = false;
    private boolean P = false;
    private boolean Q = false;
    private boolean R = false;
    final f q;
    protected int r = 0;
    protected int s;
    protected String t;
    protected String u;
    protected int v = 0;
    protected int w = 0;
    JSONObject x;
    protected boolean y;
    protected boolean z;

    public class a extends com.chartboost.sdk.impl.w.a {
        final ad h;
        aa i;
        final v j;
        final y k;
        private final bb m;
        private View n;
        private final bb o;

        private a(Context context) {
            super(context);
            g a2 = g.a();
            if (x.this.N) {
                this.n = new View(context);
                this.n.setBackgroundColor(ViewCompat.MEASURED_STATE_MASK);
                this.n.setVisibility(8);
                addView(this.n);
            }
            if (x.this.e.n == 2) {
                this.i = (aa) a2.a(new aa(context, x.this));
                this.i.setVisibility(8);
                addView(this.i);
            }
            this.h = (ad) a2.a(new ad(context, x.this));
            a((View) this.h.g);
            this.h.setVisibility(8);
            addView(this.h);
            this.j = (v) a2.a(new v(context, x.this));
            this.j.setVisibility(8);
            addView(this.j);
            if (x.this.e.n == 2) {
                this.k = (y) a2.a(new y(context, x.this));
                this.k.setVisibility(8);
                addView(this.k);
            } else {
                this.k = null;
            }
            this.m = new bb(getContext(), x.this) {
                /* access modifiers changed from: protected */
                public void a(MotionEvent motionEvent) {
                    if (x.this.e.n == 2) {
                        a.this.k.a(false);
                    }
                    if (x.this.r == 1) {
                        a.this.c(false);
                    }
                    a.this.b(true);
                }
            };
            this.m.setVisibility(8);
            addView(this.m);
            this.o = new bb(getContext(), x.this) {
                /* access modifiers changed from: protected */
                public void a(MotionEvent motionEvent) {
                    a.this.d();
                }
            };
            this.o.setVisibility(8);
            this.o.setContentDescription("CBClose");
            addView(this.o);
            JSONObject optJSONObject = x.this.x.optJSONObject(NotificationCompat.CATEGORY_PROGRESS);
            JSONObject optJSONObject2 = x.this.x.optJSONObject("video-controls-background");
            if (optJSONObject != null && !optJSONObject.isNull("background-color") && !optJSONObject.isNull("border-color") && !optJSONObject.isNull("progress-color") && !optJSONObject.isNull("radius")) {
                x.this.M = true;
                z c = this.h.c();
                c.a(e.a(optJSONObject.optString("background-color")));
                c.b(e.a(optJSONObject.optString("border-color")));
                c.c(e.a(optJSONObject.optString("progress-color")));
                c.b((float) optJSONObject.optDouble("radius", Utils.DOUBLE_EPSILON));
            }
            if (optJSONObject2 != null && !optJSONObject2.isNull("color")) {
                this.h.a(e.a(optJSONObject2.optString("color")));
            }
            if (x.this.e.n == 2 && x.this.A) {
                JSONObject optJSONObject3 = x.this.x.optJSONObject("post-video-toaster");
                if (optJSONObject3 != null) {
                    this.j.a(optJSONObject3.optString("title"), optJSONObject3.optString("tagline"));
                }
            }
            if (x.this.e.n == 2 && x.this.z) {
                JSONObject optJSONObject4 = x.this.x.optJSONObject("confirmation");
                if (optJSONObject4 != null) {
                    this.i.a(optJSONObject4.optString(MimeTypes.BASE_TYPE_TEXT), e.a(optJSONObject4.optString("color")));
                }
            }
            if (x.this.e.n == 2 && x.this.B) {
                JSONObject a3 = com.chartboost.sdk.Libraries.e.a(x.this.x, "post-video-reward-toaster");
                this.k.a((a3 == null || !a3.optString(ParametersKeys.POSITION).equals("inside-top")) ? 1 : 0);
                this.k.a(a3 != null ? a3.optString(MimeTypes.BASE_TYPE_TEXT) : "");
                if (x.this.J.c()) {
                    this.k.a(x.this.L);
                }
            }
            JSONObject g = x.this.g();
            if (g == null || g.isNull("video-click-button")) {
                this.h.d();
            }
            this.h.d(x.this.x.optBoolean("video-progress-timer-enabled"));
            if (x.this.O || x.this.N) {
                this.f.setVisibility(4);
            }
            String[] strArr = new String[1];
            strArr[0] = CBUtility.a(x.this.a()) ? "video-portrait" : "video-landscape";
            JSONObject a4 = com.chartboost.sdk.Libraries.e.a(g, strArr);
            x.this.u = a4 != null ? a4.optString("id") : "";
            if (x.this.u.isEmpty()) {
                x.this.a(CBImpressionError.VIDEO_ID_MISSING);
                return;
            }
            if (x.this.t == null) {
                x.this.t = x.this.q.a(x.this.u);
            }
            if (x.this.t == null) {
                x.this.a(CBImpressionError.VIDEO_UNAVAILABLE);
            } else {
                this.h.a(x.this.t);
            }
        }

        /* access modifiers changed from: protected */
        public void c() {
            super.c();
            if (x.this.r != 0 || (x.this.z && !x.this.o())) {
                a(x.this.r, false);
            } else {
                b(false);
            }
        }

        public void e() {
            c(true);
            this.h.h();
            x.this.s++;
            if (x.this.s <= 1 && !x.this.u() && x.this.v >= 1) {
                x.this.e.e();
            }
        }

        /* access modifiers changed from: protected */
        public void a(int i2, int i3) {
            super.a(i2, i3);
            a(x.this.r, false);
            boolean a2 = CBUtility.a(x.this.a());
            LayoutParams layoutParams = new LayoutParams(-1, -1);
            LayoutParams layoutParams2 = new LayoutParams(-2, -2);
            LayoutParams layoutParams3 = new LayoutParams(-1, -1);
            LayoutParams layoutParams4 = new LayoutParams(-1, -1);
            LayoutParams layoutParams5 = new LayoutParams(-1, -1);
            LayoutParams layoutParams6 = (LayoutParams) this.c.getLayoutParams();
            x.this.a(layoutParams2, a2 ? x.this.F : x.this.E, 1.0f);
            Point b = x.this.b(a2 ? "replay-portrait" : "replay-landscape");
            int round = Math.round(((((float) layoutParams6.leftMargin) + (((float) layoutParams6.width) / 2.0f)) + ((float) b.x)) - (((float) layoutParams2.width) / 2.0f));
            int round2 = Math.round(((((float) layoutParams6.topMargin) + (((float) layoutParams6.height) / 2.0f)) + ((float) b.y)) - (((float) layoutParams2.height) / 2.0f));
            layoutParams2.leftMargin = Math.min(Math.max(0, round), i2 - layoutParams2.width);
            layoutParams2.topMargin = Math.min(Math.max(0, round2), i3 - layoutParams2.height);
            this.m.bringToFront();
            if (a2) {
                this.m.a(x.this.F);
            } else {
                this.m.a(x.this.E);
            }
            LayoutParams layoutParams7 = (LayoutParams) this.e.getLayoutParams();
            if (!x.this.s()) {
                layoutParams3.width = layoutParams7.width;
                layoutParams3.height = layoutParams7.height;
                layoutParams3.leftMargin = layoutParams7.leftMargin;
                layoutParams3.topMargin = layoutParams7.topMargin;
                layoutParams4.width = layoutParams7.width;
                layoutParams4.height = layoutParams7.height;
                layoutParams4.leftMargin = layoutParams7.leftMargin;
                layoutParams4.topMargin = layoutParams7.topMargin;
            } else {
                LayoutParams layoutParams8 = new LayoutParams(-2, -2);
                h hVar = a2 ? x.this.l : x.this.m;
                x.this.a(layoutParams8, hVar, 1.0f);
                layoutParams8.leftMargin = 0;
                layoutParams8.topMargin = 0;
                layoutParams8.addRule(11);
                this.o.setLayoutParams(layoutParams8);
                this.o.a(hVar);
            }
            layoutParams5.width = layoutParams7.width;
            layoutParams5.height = 72;
            layoutParams5.leftMargin = layoutParams7.leftMargin;
            layoutParams5.topMargin = (layoutParams7.topMargin + layoutParams7.height) - 72;
            if (x.this.N) {
                this.n.setLayoutParams(layoutParams);
            }
            if (x.this.e.n == 2) {
                this.i.setLayoutParams(layoutParams3);
            }
            this.h.setLayoutParams(layoutParams4);
            this.j.setLayoutParams(layoutParams5);
            this.m.setLayoutParams(layoutParams2);
            if (x.this.e.n == 2) {
                this.i.a();
            }
            this.h.a();
        }

        /* access modifiers changed from: 0000 */
        public void b(boolean z) {
            if (x.this.r != 1) {
                if (x.this.z) {
                    a(0, z);
                    return;
                }
                a(1, z);
                JSONObject a2 = com.chartboost.sdk.Libraries.e.a(x.this.x, "timer");
                if (x.this.s >= 1 || a2 == null || a2.isNull(AdvertisementColumns.COLUMN_DELAY)) {
                    this.h.b(!x.this.y);
                } else {
                    String str = "InterstitialVideoViewProtocol";
                    String str2 = "controls starting %s, setting timer";
                    Object[] objArr = new Object[1];
                    objArr[0] = x.this.y ? String.VISIBLE : "hidden";
                    CBLogging.c(str, String.format(str2, objArr));
                    this.h.b(x.this.y);
                    x.this.a((View) this.h, (Runnable) new Runnable() {
                        public void run() {
                            String str = "InterstitialVideoViewProtocol";
                            String str2 = "controls %s automatically from timer";
                            Object[] objArr = new Object[1];
                            objArr[0] = x.this.y ? "hidden" : "shown";
                            CBLogging.c(str, String.format(str2, objArr));
                            a.this.h.a(!x.this.y, true);
                            synchronized (x.this.g) {
                                x.this.g.remove(a.this.h);
                            }
                        }
                    }, Math.round(a2.optDouble(AdvertisementColumns.COLUMN_DELAY, Utils.DOUBLE_EPSILON) * 1000.0d));
                }
                this.h.e();
                if (x.this.s <= 1) {
                    x.this.e.f();
                }
            }
        }

        /* access modifiers changed from: 0000 */
        public void c(boolean z) {
            this.h.f();
            if (x.this.r == 1 && z) {
                if (x.this.s < 1 && x.this.x != null && !x.this.x.isNull("post-video-reward-toaster") && x.this.B && x.this.J.c() && x.this.K.c()) {
                    e(true);
                }
                a(2, true);
                if (CBUtility.a(CBUtility.a())) {
                    requestLayout();
                }
            }
        }

        private void e(boolean z) {
            if (z) {
                this.k.a(true);
            } else {
                this.k.setVisibility(0);
            }
            x.this.f1841a.postDelayed(new Runnable() {
                public void run() {
                    a.this.k.a(false);
                }
            }, 2500);
        }

        private void a(int i2, boolean z) {
            x.this.r = i2;
            boolean z2 = true;
            switch (i2) {
                case 0:
                    x.this.a(!x.this.s(), (View) this.e, z);
                    if (x.this.e.n == 2) {
                        x.this.a(true, (View) this.i, z);
                    }
                    if (x.this.N) {
                        x.this.a(false, this.n, z);
                    }
                    x.this.a(false, (View) this.h, z);
                    x.this.a(false, (View) this.m, z);
                    x.this.a(false, (View) this.j, z);
                    this.e.setEnabled(false);
                    this.m.setEnabled(false);
                    this.h.setEnabled(false);
                    break;
                case 1:
                    x.this.a(false, (View) this.e, z);
                    if (x.this.e.n == 2) {
                        x.this.a(false, (View) this.i, z);
                    }
                    if (x.this.N) {
                        x.this.a(true, this.n, z);
                    }
                    x.this.a(true, (View) this.h, z);
                    x.this.a(false, (View) this.m, z);
                    x.this.a(false, (View) this.j, z);
                    this.e.setEnabled(true);
                    this.m.setEnabled(false);
                    this.h.setEnabled(true);
                    break;
                case 2:
                    x.this.a(true, (View) this.e, z);
                    if (x.this.e.n == 2) {
                        x.this.a(false, (View) this.i, z);
                    }
                    if (x.this.N) {
                        x.this.a(false, this.n, z);
                    }
                    x.this.a(false, (View) this.h, z);
                    x.this.a(true, (View) this.m, z);
                    x.this.a(x.this.K.c() && x.this.J.c() && x.this.A, (View) this.j, z);
                    this.m.setEnabled(true);
                    this.e.setEnabled(true);
                    this.h.setEnabled(false);
                    if (x.this.C) {
                        e(false);
                        break;
                    }
                    break;
            }
            boolean f = f();
            bb d = d(true);
            d.setEnabled(f);
            x.this.a(f, (View) d, z);
            bb d2 = d(false);
            d2.setEnabled(false);
            x.this.a(false, (View) d2, z);
            if (x.this.O || x.this.N) {
                x.this.a(!x.this.s(), (View) this.f, z);
            }
            x.this.a(!x.this.s(), (View) this.c, z);
            if (i2 == 0) {
                z2 = false;
            }
            a(z2);
        }

        /* access modifiers changed from: protected */
        public boolean f() {
            if (x.this.r == 1 && x.this.s < 1) {
                StringBuilder sb = new StringBuilder();
                sb.append("close-");
                sb.append(CBUtility.a(x.this.a()) ? "portrait" : "landscape");
                JSONObject a2 = com.chartboost.sdk.Libraries.e.a(x.this.g(), sb.toString());
                float optDouble = a2 != null ? (float) a2.optDouble(AdvertisementColumns.COLUMN_DELAY, -1.0d) : -1.0f;
                int round = optDouble >= 0.0f ? Math.round(optDouble * 1000.0f) : -1;
                x.this.D = round;
                if (round < 0 || round > this.h.b().d()) {
                    return false;
                }
            }
            return true;
        }

        public void b() {
            x.this.n();
            super.b();
        }

        /* access modifiers changed from: protected */
        public void d() {
            if (x.this.r != 1 || x.this.e.f1830a.f1903a != 1) {
                if (x.this.r == 1) {
                    c(false);
                    this.h.h();
                    if (x.this.s < 1) {
                        x.this.s++;
                        x.this.e.e();
                    }
                }
                x.this.f1841a.post(new Runnable() {
                    public void run() {
                        try {
                            x.this.h();
                        } catch (Exception e) {
                            com.chartboost.sdk.Tracking.a.a(a.class, "onCloseButton Runnable.run", e);
                        }
                    }
                });
            }
        }

        /* access modifiers changed from: protected */
        public void a(float f, float f2, float f3, float f4) {
            if ((!x.this.y || x.this.r != 1) && x.this.r != 0) {
                b(f, f2, f3, f4);
            }
        }

        /* access modifiers changed from: protected */
        public void b(float f, float f2, float f3, float f4) {
            if (x.this.r == 1) {
                c(false);
            }
            x.this.b(com.chartboost.sdk.Libraries.e.a(com.chartboost.sdk.Libraries.e.a(AvidJSONUtil.KEY_X, (Object) Float.valueOf(f)), com.chartboost.sdk.Libraries.e.a(AvidJSONUtil.KEY_Y, (Object) Float.valueOf(f2)), com.chartboost.sdk.Libraries.e.a("w", (Object) Float.valueOf(f3)), com.chartboost.sdk.Libraries.e.a("h", (Object) Float.valueOf(f4))));
        }

        /* access modifiers changed from: protected */
        public void g() {
            x.this.z = false;
            b(true);
        }

        public bb d(boolean z) {
            return ((!x.this.s() || !z) && (x.this.s() || z)) ? this.d : this.o;
        }
    }

    public x(c cVar, f fVar, Handler handler, com.chartboost.sdk.c cVar2) {
        super(cVar, handler, cVar2);
        this.q = fVar;
        this.r = 0;
        this.E = new h(this);
        this.F = new h(this);
        this.G = new h(this);
        this.H = new h(this);
        this.I = new h(this);
        this.J = new h(this);
        this.K = new h(this);
        this.L = new h(this);
        this.s = 0;
    }

    public boolean o() {
        return this.e.n == 1;
    }

    /* access modifiers changed from: protected */
    public com.chartboost.sdk.e.a b(Context context) {
        return new a(context);
    }

    public boolean l() {
        e().d();
        return true;
    }

    public void m() {
        super.m();
        if (this.r == 1 && this.P) {
            e().h.b().a(this.v);
            e().h.e();
        }
        this.P = false;
    }

    public void n() {
        super.n();
        if (this.r == 1 && !this.P) {
            this.P = true;
            e().h.g();
        }
    }

    public boolean a(JSONObject jSONObject) {
        if (!super.a(jSONObject)) {
            return false;
        }
        this.x = jSONObject.optJSONObject("ux");
        if (this.x == null) {
            this.x = com.chartboost.sdk.Libraries.e.a(new com.chartboost.sdk.Libraries.e.a[0]);
        }
        if (this.d.isNull("video-landscape") || this.d.isNull("replay-landscape")) {
            this.i = false;
        }
        if (!this.E.a("replay-landscape") || !this.F.a("replay-portrait") || !this.I.a("video-click-button") || !this.J.a("post-video-reward-icon") || !this.K.a("post-video-button") || !this.G.a("video-confirmation-button") || !this.H.a("video-confirmation-icon") || !this.L.a("post-video-reward-icon")) {
            CBLogging.b("InterstitialVideoViewProtocol", "Error while downloading the assets");
            a(CBImpressionError.ASSETS_DOWNLOAD_FAILURE);
            return false;
        }
        this.y = this.x.optBoolean("video-controls-togglable");
        this.N = jSONObject.optBoolean(Events.CREATIVE_FULLSCREEN);
        this.O = jSONObject.optBoolean("preroll_popup_fullscreen");
        if (this.e.n == 2) {
            JSONObject optJSONObject = this.x.optJSONObject("confirmation");
            JSONObject optJSONObject2 = this.x.optJSONObject("post-video-toaster");
            if (optJSONObject2 != null && !optJSONObject2.isNull("title") && !optJSONObject2.isNull("tagline")) {
                this.A = true;
            }
            if (optJSONObject != null && !optJSONObject.isNull(MimeTypes.BASE_TYPE_TEXT) && !optJSONObject.isNull("color")) {
                this.z = true;
            }
            if (!this.x.isNull("post-video-reward-toaster")) {
                this.B = true;
            }
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void i() {
        if (this.z && (!this.G.c() || !this.H.c())) {
            this.z = false;
        }
        super.i();
    }

    public void d() {
        super.d();
        this.E = null;
        this.F = null;
        this.I = null;
        this.J = null;
        this.K = null;
        this.G = null;
        this.H = null;
        this.L = null;
    }

    public boolean p() {
        return this.r == 1;
    }

    /* renamed from: q */
    public a e() {
        return (a) super.e();
    }

    /* access modifiers changed from: protected */
    public void r() {
        this.e.p();
    }

    /* access modifiers changed from: protected */
    public boolean s() {
        switch (this.r) {
            case 0:
                if (!this.O && !CBUtility.a(CBUtility.a())) {
                    return false;
                }
            case 1:
                if (!this.N && !CBUtility.a(CBUtility.a())) {
                    return false;
                }
            case 2:
                return false;
        }
        return true;
    }

    public boolean t() {
        return this.Q;
    }

    public void a(boolean z2) {
        this.Q = z2;
    }

    public boolean u() {
        return this.R;
    }

    public void v() {
        if (this.t != null) {
            new File(this.t).delete();
        }
        this.R = true;
        a(CBImpressionError.ERROR_PLAYING_VIDEO);
    }

    public float j() {
        return (float) this.w;
    }

    public float k() {
        return (float) this.v;
    }
}
