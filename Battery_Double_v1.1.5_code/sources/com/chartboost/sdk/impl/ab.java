package com.chartboost.sdk.impl;

import android.content.Context;
import android.view.View;
import android.view.animation.TranslateAnimation;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import com.chartboost.sdk.Libraries.CBUtility;

public abstract class ab extends RelativeLayout {

    /* renamed from: a reason: collision with root package name */
    protected x f1854a;
    private ac b;
    private int c = 1;

    /* access modifiers changed from: protected */
    public abstract View a();

    /* access modifiers changed from: protected */
    public abstract int b();

    public ab(Context context, x xVar) {
        super(context);
        this.f1854a = xVar;
        a(context);
    }

    public void a(int i) {
        LayoutParams layoutParams;
        this.c = i;
        setClickable(false);
        int b2 = b();
        switch (this.c) {
            case 0:
                layoutParams = new LayoutParams(-1, CBUtility.a(b2, getContext()));
                layoutParams.addRule(10);
                this.b.b(1);
                break;
            case 1:
                layoutParams = new LayoutParams(-1, CBUtility.a(b2, getContext()));
                layoutParams.addRule(12);
                this.b.b(4);
                break;
            case 2:
                layoutParams = new LayoutParams(CBUtility.a(b2, getContext()), -1);
                layoutParams.addRule(9);
                this.b.b(8);
                break;
            case 3:
                layoutParams = new LayoutParams(CBUtility.a(b2, getContext()), -1);
                layoutParams.addRule(11);
                this.b.b(2);
                break;
            default:
                layoutParams = null;
                break;
        }
        setLayoutParams(layoutParams);
    }

    private void a(Context context) {
        Context context2 = getContext();
        setGravity(17);
        this.b = new ac(context2);
        this.b.a(-1);
        this.b.setBackgroundColor(-855638017);
        addView(this.b, new LayoutParams(-1, -1));
        addView(a(), new LayoutParams(-1, -1));
    }

    public void a(boolean z) {
        a(z, 500);
    }

    private void a(final boolean z, long j) {
        this.f1854a.C = z;
        if ((!z || getVisibility() != 0) && (z || getVisibility() != 8)) {
            AnonymousClass1 r0 = new Runnable() {
                public void run() {
                    if (!z) {
                        ab.this.setVisibility(8);
                        ab.this.clearAnimation();
                    }
                    synchronized (ab.this.f1854a.g) {
                        ab.this.f1854a.g.remove(ab.this);
                    }
                }
            };
            if (z) {
                setVisibility(0);
            }
            float a2 = CBUtility.a((float) b(), getContext());
            TranslateAnimation translateAnimation = null;
            switch (this.c) {
                case 0:
                    translateAnimation = new TranslateAnimation(0.0f, 0.0f, z ? -a2 : 0.0f, z ? 0.0f : -a2);
                    break;
                case 1:
                    float f = z ? a2 : 0.0f;
                    if (z) {
                        a2 = 0.0f;
                    }
                    translateAnimation = new TranslateAnimation(0.0f, 0.0f, f, a2);
                    break;
                case 2:
                    translateAnimation = new TranslateAnimation(z ? -a2 : 0.0f, z ? 0.0f : -a2, 0.0f, 0.0f);
                    break;
                case 3:
                    float f2 = z ? a2 : 0.0f;
                    if (z) {
                        a2 = 0.0f;
                    }
                    translateAnimation = new TranslateAnimation(f2, a2, 0.0f, 0.0f);
                    break;
            }
            translateAnimation.setDuration(j);
            translateAnimation.setFillAfter(!z);
            startAnimation(translateAnimation);
            synchronized (this.f1854a.g) {
                this.f1854a.g.put(this, r0);
            }
            this.f1854a.f1841a.postDelayed(r0, j);
        }
    }
}
