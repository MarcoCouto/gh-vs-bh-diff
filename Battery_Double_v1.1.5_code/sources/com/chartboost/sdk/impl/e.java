package com.chartboost.sdk.impl;

import android.content.SharedPreferences;
import android.os.Handler;
import com.chartboost.sdk.Libraries.CBLogging;
import com.chartboost.sdk.Libraries.CBUtility;
import com.chartboost.sdk.Libraries.f;
import com.chartboost.sdk.Libraries.i;
import com.chartboost.sdk.Model.CBError;
import com.chartboost.sdk.Model.CBError.CBImpressionError;
import com.chartboost.sdk.Model.b;
import com.chartboost.sdk.c;
import com.chartboost.sdk.c.C0019c;
import com.chartboost.sdk.d;
import com.chartboost.sdk.g;
import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import org.json.JSONException;
import org.json.JSONObject;

public class e {
    private final long A = TimeUnit.SECONDS.toNanos(1);
    private final String[] B = {"ASKED_TO_CACHE", "ASKED_TO_SHOW", "REQUESTING_TO_CACHE", "REQUESTING_TO_SHOW", "DOWNLOADING_TO_CACHE", "DOWNLOADING_TO_SHOW", "READY", "ASKING_UI_TO_SHOW_AD", "DONE"};

    /* renamed from: a reason: collision with root package name */
    final ScheduledExecutorService f1906a;
    public final f b;
    final i c;
    final Handler d;
    final c e;
    final c f;
    int g = 0;
    final Map<String, f> h;
    final SortedSet<f> i;
    final SortedSet<f> j;
    ScheduledFuture<?> k;
    private final l l;
    private final aj m;
    private final ak n;
    private final ar o;
    private final AtomicReference<com.chartboost.sdk.Model.e> p;
    private final SharedPreferences q;
    private final com.chartboost.sdk.Tracking.a r;
    private final am s;
    private final d t;
    private final an u;
    private int v;
    private boolean w;
    private final Map<String, Long> x;
    private final Map<String, Integer> y;
    private final long z = TimeUnit.SECONDS.toNanos(5);

    public class a implements Runnable {

        /* renamed from: a reason: collision with root package name */
        final int f1909a;
        final String b;
        final f c;
        final CBImpressionError d;

        public a(int i, String str, f fVar, CBImpressionError cBImpressionError) {
            this.f1909a = i;
            this.b = str;
            this.c = fVar;
            this.d = cBImpressionError;
        }

        public void run() {
            try {
                synchronized (e.this) {
                    int i = this.f1909a;
                    if (i != 0) {
                        switch (i) {
                            case 2:
                                e.this.k = null;
                                e.this.b();
                                break;
                            case 3:
                                e.this.b(this.b);
                                break;
                            case 4:
                                e.this.c(this.b);
                                break;
                            case 5:
                                e.this.b(this.c);
                                break;
                            case 6:
                                e.this.a(this.c, this.d);
                                break;
                            case 7:
                                e.this.a(this.c);
                                break;
                            case 8:
                                e.this.d(this.b);
                                break;
                        }
                    } else {
                        e.this.a();
                    }
                }
            } catch (Exception e2) {
                com.chartboost.sdk.Tracking.a.a(getClass(), "run", e2);
            }
        }
    }

    public e(c cVar, ScheduledExecutorService scheduledExecutorService, l lVar, f fVar, aj ajVar, ak akVar, ar arVar, AtomicReference<com.chartboost.sdk.Model.e> atomicReference, SharedPreferences sharedPreferences, i iVar, com.chartboost.sdk.Tracking.a aVar, Handler handler, c cVar2, am amVar, d dVar, an anVar) {
        this.f1906a = scheduledExecutorService;
        this.l = lVar;
        this.b = fVar;
        this.m = ajVar;
        this.n = akVar;
        this.o = arVar;
        this.p = atomicReference;
        this.q = sharedPreferences;
        this.c = iVar;
        this.r = aVar;
        this.d = handler;
        this.e = cVar2;
        this.s = amVar;
        this.t = dVar;
        this.u = anVar;
        this.f = cVar;
        this.v = 1;
        this.h = new HashMap();
        this.j = new TreeSet();
        this.i = new TreeSet();
        this.x = new HashMap();
        this.y = new HashMap();
        this.w = false;
    }

    /* access modifiers changed from: 0000 */
    public void a() {
        if (this.g == 0) {
            this.g = 1;
            b();
        }
    }

    /* access modifiers changed from: 0000 */
    public void b() {
        if (!this.w) {
            try {
                this.w = true;
                d();
                if (this.g == 1) {
                    if (!a(this.j, 1, 3, 1, "show")) {
                        a(this.i, 0, 2, 2, "cache");
                    }
                }
                c();
            } finally {
                this.w = false;
            }
        }
    }

    private void c() {
        Long l2;
        boolean z2 = true;
        if (this.g == 1) {
            long b2 = this.c.b();
            l2 = null;
            for (Entry entry : this.x.entrySet()) {
                if (((f) this.h.get((String) entry.getKey())) != null) {
                    long max = Math.max(this.z, ((Long) entry.getValue()).longValue() - b2);
                    if (l2 == null || max < l2.longValue()) {
                        l2 = Long.valueOf(max);
                    }
                }
            }
        } else {
            l2 = null;
        }
        if (!(l2 == null || this.k == null)) {
            if (Math.abs(l2.longValue() - this.k.getDelay(TimeUnit.NANOSECONDS)) > TimeUnit.SECONDS.toNanos(5)) {
                z2 = false;
            }
            if (z2) {
                return;
            }
        }
        if (this.k != null) {
            this.k.cancel(false);
            this.k = null;
        }
        if (l2 != null) {
            ScheduledExecutorService scheduledExecutorService = this.f1906a;
            a aVar = new a(2, null, null, null);
            this.k = scheduledExecutorService.schedule(aVar, l2.longValue(), TimeUnit.NANOSECONDS);
        }
    }

    private boolean a(SortedSet<f> sortedSet, int i2, int i3, int i4, String str) {
        Iterator it = sortedSet.iterator();
        while (it.hasNext()) {
            f fVar = (f) it.next();
            if (fVar.c != i2 || fVar.d != null) {
                it.remove();
            } else if (e(fVar.b)) {
                continue;
            } else if (!this.f.g(fVar.b)) {
                fVar.c = 8;
                this.h.remove(fVar.b);
                it.remove();
            } else {
                fVar.c = i3;
                it.remove();
                a(fVar, i4, str);
                return true;
            }
        }
        return false;
    }

    public synchronized com.chartboost.sdk.Model.a a(String str) {
        f fVar = (f) this.h.get(str);
        if (fVar == null || (fVar.c != 6 && fVar.c != 7)) {
            return null;
        }
        return fVar.d;
    }

    public synchronized boolean a(String str, String str2, Map<String, b> map) {
        int i2 = this.v;
        this.v = i2 + 1;
        f fVar = new f(i2, str, 6);
        fVar.e = Integer.valueOf(1);
        try {
            fVar.d = new t(new JSONObject(str2));
            fVar.d.C = (b) map.values().iterator().next();
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
        this.h.put(str, fVar);
        this.i.add(fVar);
        return true;
    }

    /* access modifiers changed from: 0000 */
    public void b(String str) {
        if (e()) {
            c cVar = this.f;
            cVar.getClass();
            this.d.postDelayed(new com.chartboost.sdk.impl.c.a(4, str, CBImpressionError.FIRST_SESSION_INTERSTITIALS_DISABLED), this.A);
            return;
        }
        f fVar = (f) this.h.get(str);
        if (fVar != null && fVar.c == 6 && !a(fVar.d)) {
            this.h.remove(str);
            fVar = null;
        }
        if (fVar == null) {
            int i2 = this.v;
            this.v = i2 + 1;
            fVar = new f(i2, str, 0);
            this.h.put(str, fVar);
            this.i.add(fVar);
        }
        fVar.f = true;
        if (fVar.h == null) {
            fVar.h = Long.valueOf(this.c.b());
        }
        switch (fVar.c) {
            case 6:
            case 7:
                Handler handler = this.d;
                c cVar2 = this.f;
                cVar2.getClass();
                handler.post(new com.chartboost.sdk.impl.c.a(0, str, null));
                break;
        }
        b();
    }

    private boolean a(com.chartboost.sdk.Model.a aVar) {
        File file = this.b.d().f1820a;
        for (b bVar : aVar.n.values()) {
            if (!bVar.a(file).exists()) {
                StringBuilder sb = new StringBuilder();
                sb.append("Asset does not exist: ");
                sb.append(bVar.b);
                CBLogging.b("AdUnitManager", sb.toString());
                return false;
            }
        }
        return true;
    }

    private void d() {
        long b2 = this.c.b();
        Iterator it = this.x.values().iterator();
        while (it.hasNext()) {
            if (b2 - ((Long) it.next()).longValue() >= 0) {
                it.remove();
            }
        }
    }

    private boolean e(String str) {
        return this.x.containsKey(str);
    }

    /* JADX WARNING: type inference failed for: r0v5, types: [com.chartboost.sdk.impl.af, com.chartboost.sdk.impl.al] */
    /* JADX WARNING: type inference failed for: r15v0, types: [com.chartboost.sdk.impl.al] */
    /* JADX WARNING: type inference failed for: r15v1, types: [com.chartboost.sdk.impl.ao] */
    /* JADX WARNING: type inference failed for: r15v2, types: [com.chartboost.sdk.impl.al] */
    /* JADX WARNING: type inference failed for: r15v3, types: [com.chartboost.sdk.impl.al] */
    /* JADX WARNING: type inference failed for: r15v4, types: [com.chartboost.sdk.impl.ao] */
    /* JADX WARNING: type inference failed for: r15v5, types: [com.chartboost.sdk.impl.al] */
    /* JADX WARNING: Multi-variable type inference failed. Error: jadx.core.utils.exceptions.JadxRuntimeException: No candidate types for var: r15v3, types: [com.chartboost.sdk.impl.al]
  assigns: [com.chartboost.sdk.impl.al, com.chartboost.sdk.impl.ao]
  uses: [com.chartboost.sdk.impl.al, com.chartboost.sdk.impl.af, com.chartboost.sdk.impl.ao]
  mth insns count: 130
    	at jadx.core.dex.visitors.typeinference.TypeSearch.fillTypeCandidates(TypeSearch.java:237)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.typeinference.TypeSearch.run(TypeSearch.java:53)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.runMultiVariableSearch(TypeInferenceVisitor.java:99)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.visit(TypeInferenceVisitor.java:92)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:27)
    	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$1(DepthTraversal.java:14)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
    	at jadx.core.ProcessClass.process(ProcessClass.java:30)
    	at jadx.core.ProcessClass.lambda$processDependencies$0(ProcessClass.java:49)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:49)
    	at jadx.core.ProcessClass.process(ProcessClass.java:35)
    	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
    	at jadx.api.JavaClass.decompile(JavaClass.java:62)
    	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
     */
    /* JADX WARNING: Unknown variable types count: 4 */
    private void a(f fVar, int i2, String str) {
        ? r0;
        f fVar2 = fVar;
        try {
            com.chartboost.sdk.Model.e eVar = (com.chartboost.sdk.Model.e) this.p.get();
            boolean z2 = this.f.f1903a == 2;
            boolean z3 = eVar.y && !z2;
            final long b2 = this.c.b();
            final f fVar3 = fVar;
            final boolean z4 = z2;
            final boolean z5 = z3;
            AnonymousClass1 r1 = new com.chartboost.sdk.impl.al.a() {
                public void a(al alVar, JSONObject jSONObject) {
                    com.chartboost.sdk.Model.a aVar;
                    try {
                        fVar3.p = Integer.valueOf((int) TimeUnit.NANOSECONDS.toMillis(e.this.c.b() - b2));
                        fVar3.q = Integer.valueOf((int) TimeUnit.NANOSECONDS.toMillis(alVar.h));
                        fVar3.r = Integer.valueOf((int) TimeUnit.NANOSECONDS.toMillis(alVar.i));
                        if (z4) {
                            aVar = new com.chartboost.sdk.Model.a(0, jSONObject, true);
                        } else if (z5) {
                            aVar = new com.chartboost.sdk.Model.a(1, jSONObject, false);
                        } else {
                            aVar = new com.chartboost.sdk.Model.a(0, jSONObject, false);
                        }
                        e.this.a(fVar3, aVar);
                    } catch (JSONException e2) {
                        com.chartboost.sdk.Tracking.a.a(e.class, "sendAdGetRequest.onSuccess", (Exception) e2);
                        e.this.a(fVar3, new CBError(com.chartboost.sdk.Model.CBError.a.UNEXPECTED_RESPONSE, "Response conversion failure"));
                    }
                }

                public void a(al alVar, CBError cBError) {
                    e.this.a(fVar3, cBError);
                }
            };
            boolean z6 = fVar2.c == 2;
            if (z2) {
                ? alVar = new al(this.f.d, this.o, this.r, i2, r1);
                alVar.l = true;
                alVar.a("location", (Object) fVar2.b);
                alVar.a("cache", (Object) Boolean.valueOf(z6));
                alVar.a("raw", (Object) Boolean.valueOf(true));
                fVar2.e = Integer.valueOf(0);
                r0 = alVar;
            } else if (z3) {
                ? aoVar = new ao(String.format(this.f.e, new Object[]{eVar.F}), this.o, this.r, i2, r1);
                aoVar.a("cache_assets", this.b.c(), 0);
                aoVar.a("location", fVar2.b, 0);
                aoVar.a("cache", Boolean.valueOf(z6), 0);
                aoVar.l = true;
                fVar2.e = Integer.valueOf(1);
                r0 = aoVar;
            } else {
                ? alVar2 = new al(this.f.d, this.o, this.r, i2, r1);
                alVar2.a("local-videos", (Object) this.b.b());
                alVar2.l = true;
                alVar2.a("location", (Object) fVar2.b);
                alVar2.a("cache", (Object) Boolean.valueOf(z6));
                fVar2.e = Integer.valueOf(0);
                r0 = alVar2;
            }
            r0.j = 1;
            this.g = 2;
            this.m.a(r0);
            this.r.a(this.f.a(fVar2.e.intValue()), str, fVar2.b);
        } catch (Exception e2) {
            com.chartboost.sdk.Tracking.a.a(getClass(), "sendAdGetRequest", e2);
            a(fVar2, new CBError(com.chartboost.sdk.Model.CBError.a.MISCELLANEOUS, "error sending ad-get request"));
        }
    }

    /* access modifiers changed from: 0000 */
    public synchronized void a(f fVar, com.chartboost.sdk.Model.a aVar) {
        this.g = 1;
        fVar.c = fVar.c == 2 ? 4 : 5;
        fVar.d = aVar;
        c(fVar);
        b();
    }

    private void c(final f fVar) {
        if (fVar.d != null && (fVar.c == 5 || fVar.c == 4)) {
            int i2 = fVar.c == 5 ? 1 : 2;
            if (fVar.g > i2) {
                AnonymousClass2 r1 = new h() {
                    public void a(boolean z, int i, int i2) {
                        e.this.a(fVar, z, i, i2);
                    }
                };
                fVar.g = i2;
                this.l.a(i2, fVar.d.n, new AtomicInteger(), (h) g.a().a(r1));
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public synchronized void a(f fVar, boolean z2, int i2, int i3) {
        if (fVar.c == 4 || fVar.c == 5) {
            fVar.n = Integer.valueOf(i2);
            fVar.o = Integer.valueOf(i3);
            if (z2) {
                d(fVar);
            } else {
                e(fVar);
            }
        }
        b();
    }

    private void d(f fVar) {
        int i2 = fVar.c;
        long b2 = this.c.b();
        if (fVar.h != null) {
            fVar.k = Integer.valueOf((int) TimeUnit.NANOSECONDS.toMillis(b2 - fVar.h.longValue()));
        }
        if (fVar.i != null) {
            fVar.l = Integer.valueOf((int) TimeUnit.NANOSECONDS.toMillis(b2 - fVar.i.longValue()));
        }
        b(fVar, "ad-unit-cached");
        fVar.c = 6;
        if (fVar.f) {
            Handler handler = this.d;
            c cVar = this.f;
            cVar.getClass();
            handler.post(new com.chartboost.sdk.impl.c.a(0, fVar.b, null));
        }
        if (i2 == 5) {
            h(fVar);
        }
    }

    private void e(f fVar) {
        b(fVar, CBImpressionError.ASSETS_DOWNLOAD_FAILURE);
        f(fVar);
        g(fVar);
    }

    private void f(f fVar) {
        this.h.remove(fVar.b);
        fVar.c = 8;
        fVar.d = null;
    }

    /* access modifiers changed from: 0000 */
    public synchronized void a(f fVar, CBError cBError) {
        if (this.g != 0) {
            this.g = 1;
            b(fVar, cBError.c());
            f(fVar);
            g(fVar);
            b();
        }
    }

    private void g(f fVar) {
        com.chartboost.sdk.Model.e eVar = (com.chartboost.sdk.Model.e) this.p.get();
        long j2 = eVar.s;
        int i2 = eVar.t;
        Integer num = (Integer) this.y.get(fVar.b);
        if (num == null) {
            num = Integer.valueOf(0);
        }
        Integer valueOf = Integer.valueOf(Math.min(num.intValue(), i2));
        this.y.put(fVar.b, Integer.valueOf(valueOf.intValue() + 1));
        this.x.put(fVar.b, Long.valueOf(this.c.b() + TimeUnit.MILLISECONDS.toNanos(j2 << valueOf.intValue())));
    }

    /* access modifiers changed from: 0000 */
    public void c(String str) {
        if (e()) {
            c cVar = this.f;
            cVar.getClass();
            this.d.postDelayed(new com.chartboost.sdk.impl.c.a(4, str, CBImpressionError.FIRST_SESSION_INTERSTITIALS_DISABLED), this.A);
            return;
        }
        f fVar = (f) this.h.get(str);
        if (fVar == null) {
            int i2 = this.v;
            this.v = i2 + 1;
            fVar = new f(i2, str, 1);
            this.h.put(str, fVar);
            this.j.add(fVar);
        }
        if (fVar.i == null) {
            fVar.i = Long.valueOf(this.c.b());
        }
        switch (fVar.c) {
            case 0:
                this.i.remove(fVar);
                this.j.add(fVar);
                fVar.c = 1;
                break;
            case 2:
                fVar.c = 3;
                break;
            case 4:
                fVar.c = 5;
                c(fVar);
                break;
            case 6:
                h(fVar);
                break;
        }
        b();
    }

    private void h(f fVar) {
        CBImpressionError cBImpressionError;
        String str;
        if (!this.n.c()) {
            Handler handler = this.d;
            c cVar = this.f;
            cVar.getClass();
            handler.post(new com.chartboost.sdk.impl.c.a(4, fVar.b, CBImpressionError.INTERNET_UNAVAILABLE_AT_SHOW));
            return;
        }
        com.chartboost.sdk.Model.c cVar2 = null;
        try {
            com.chartboost.sdk.Model.a aVar = fVar.d;
            File file = this.b.d().f1820a;
            if (aVar.m != 0 || (!this.f.g && !aVar.A.equals("video"))) {
                cBImpressionError = null;
            } else {
                cBImpressionError = a(aVar.l);
                if (cBImpressionError != null) {
                    CBLogging.b("AdUnitManager", "Video media unavailable for the impression");
                }
            }
            if (cBImpressionError == null) {
                for (b bVar : aVar.n.values()) {
                    if (!bVar.a(file).exists()) {
                        StringBuilder sb = new StringBuilder();
                        sb.append("Asset does not exist: ");
                        sb.append(bVar.b);
                        CBLogging.b("AdUnitManager", sb.toString());
                        cBImpressionError = CBImpressionError.ASSET_MISSING;
                    }
                }
            }
            if (cBImpressionError == null) {
                if (aVar.m == 1) {
                    str = a(aVar, file);
                    if (str == null) {
                        cBImpressionError = CBImpressionError.ERROR_LOADING_WEB_VIEW;
                    }
                } else {
                    str = null;
                }
                if (cBImpressionError == null) {
                    cVar2 = a(fVar, str);
                }
            }
        } catch (Exception e2) {
            com.chartboost.sdk.Tracking.a.a(getClass(), "showReady", e2);
            cBImpressionError = CBImpressionError.INTERNAL;
        }
        if (cBImpressionError == null) {
            fVar.c = 7;
            c cVar3 = this.e;
            cVar3.getClass();
            C0019c cVar4 = new C0019c(10);
            cVar4.d = cVar2;
            fVar.j = Long.valueOf(this.c.b());
            this.d.post(cVar4);
        } else {
            b(fVar, cBImpressionError);
            f(fVar);
        }
    }

    private String a(com.chartboost.sdk.Model.a aVar, File file) {
        if (aVar.C == null) {
            CBLogging.b("AdUnitManager", "AdUnit does not have a template body");
            return null;
        }
        File a2 = aVar.C.a(file);
        HashMap hashMap = new HashMap();
        if (aVar instanceof t) {
            hashMap.put("{% encoding %}", "base64");
            hashMap.put("{% adm %}", ((t) aVar).d);
            String str = this.f.f1903a == 0 ? "8" : "9";
            String str2 = this.f.f1903a == 0 ? "true" : "false";
            hashMap.put("{{ ad_type }}", str);
            hashMap.put("{{ show_close_button }}", str2);
            hashMap.put("{{ preroll_popup }}", "false");
            hashMap.put("{{ post_video_reward_toaster_enabled }}", "false");
            if (str.equals("9")) {
                hashMap.put("{{ post_video_reward_toaster_enabled }}", "false");
            }
        }
        hashMap.putAll(aVar.o);
        hashMap.put("{% certification_providers %}", o.a(aVar.D));
        for (Entry entry : aVar.n.entrySet()) {
            hashMap.put(entry.getKey(), ((b) entry.getValue()).b);
        }
        try {
            return n.a(a2, hashMap);
        } catch (Exception e2) {
            com.chartboost.sdk.Tracking.a.a(getClass(), "loadTemplateHtml", e2);
            return null;
        }
    }

    private com.chartboost.sdk.Model.c a(f fVar, String str) {
        f fVar2 = fVar;
        com.chartboost.sdk.Model.c cVar = new com.chartboost.sdk.Model.c(fVar2.d, new d(this, fVar2), this.b, this.m, this.o, this.q, this.r, this.d, this.e, this.s, this.t, this.u, this.f, fVar2.b, str);
        return cVar;
    }

    /* access modifiers changed from: 0000 */
    public void a(f fVar, CBImpressionError cBImpressionError) {
        b(fVar, cBImpressionError);
        if (fVar.c != 7) {
            return;
        }
        if (cBImpressionError == CBImpressionError.IMPRESSION_ALREADY_VISIBLE) {
            fVar.c = 6;
            fVar.j = null;
            fVar.i = null;
            fVar.m = null;
            return;
        }
        g(fVar);
        f(fVar);
        b();
    }

    private void b(f fVar, CBImpressionError cBImpressionError) {
        String str;
        Handler handler = this.d;
        c cVar = this.f;
        cVar.getClass();
        handler.post(new com.chartboost.sdk.impl.c.a(4, fVar.b, cBImpressionError));
        if (cBImpressionError != CBImpressionError.NO_AD_FOUND) {
            String str2 = null;
            String str3 = fVar.d != null ? fVar.d.q : null;
            String str4 = (fVar.c == 0 || fVar.c == 2 || fVar.c == 4) ? "cache" : "show";
            Integer valueOf = Integer.valueOf(fVar.d != null ? fVar.d.m : fVar.e.intValue());
            if (valueOf != null) {
                str2 = valueOf.intValue() == 0 ? "native" : "web";
            }
            String str5 = str2;
            if (fVar.c < 0 || fVar.c >= this.B.length) {
                StringBuilder sb = new StringBuilder();
                sb.append("Unknown state: ");
                sb.append(fVar.c);
                str = sb.toString();
            } else {
                str = this.B[fVar.c];
            }
            this.r.a(this.f.b, str4, str5, cBImpressionError.toString(), str3, fVar.b, str);
        }
    }

    private void b(f fVar, String str) {
        Integer num;
        String str2;
        f fVar2 = fVar;
        if (((com.chartboost.sdk.Model.e) this.p.get()).p) {
            String str3 = null;
            Object obj = fVar2.d != null ? fVar2.d.q : null;
            String str4 = (fVar2.c == 0 || fVar2.c == 2 || fVar2.c == 4) ? "cache" : "show";
            if (fVar2.d != null) {
                num = Integer.valueOf(fVar2.d.m);
            } else {
                num = fVar2.e;
            }
            if (num != null) {
                str3 = num.intValue() == 0 ? "native" : "web";
            }
            String str5 = str3;
            if (fVar2.c < 0 || fVar2.c >= this.B.length) {
                StringBuilder sb = new StringBuilder();
                sb.append("Unknown state: ");
                sb.append(fVar2.c);
                str2 = sb.toString();
            } else {
                str2 = this.B[fVar2.c];
            }
            this.r.a(str, this.f.b, str4, str5, null, null, com.chartboost.sdk.Libraries.e.a(com.chartboost.sdk.Libraries.e.a("adGetRequestSubmitToCallbackMs", (Object) fVar2.p), com.chartboost.sdk.Libraries.e.a("downloadRequestToCompletionMs", (Object) fVar2.n), com.chartboost.sdk.Libraries.e.a("downloadAccumulatedProcessingMs", (Object) fVar2.o), com.chartboost.sdk.Libraries.e.a("adGetRequestGetResponseCodeMs", (Object) fVar2.q), com.chartboost.sdk.Libraries.e.a("adGetRequestReadDataMs", (Object) fVar2.r), com.chartboost.sdk.Libraries.e.a("cacheRequestToReadyMs", (Object) fVar2.k), com.chartboost.sdk.Libraries.e.a("showRequestToReadyMs", (Object) fVar2.l), com.chartboost.sdk.Libraries.e.a("showRequestToShownMs", (Object) fVar2.m), com.chartboost.sdk.Libraries.e.a("adId", obj), com.chartboost.sdk.Libraries.e.a("location", (Object) fVar2.b), com.chartboost.sdk.Libraries.e.a("state", (Object) str2)), false);
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(f fVar) {
        if (fVar.c == 7) {
            fVar.c = 6;
            fVar.j = null;
            fVar.i = null;
            fVar.m = null;
        }
    }

    /* access modifiers changed from: 0000 */
    public void b(f fVar) {
        if (fVar.c == 7) {
            if (fVar.i != null && fVar.m == null) {
                fVar.m = Integer.valueOf((int) TimeUnit.NANOSECONDS.toMillis(this.c.b() - fVar.i.longValue()));
            }
            b(fVar, "ad-unit-shown");
            this.y.remove(fVar.b);
            Handler handler = this.d;
            c cVar = this.f;
            cVar.getClass();
            handler.post(new com.chartboost.sdk.impl.c.a(5, fVar.b, null));
            i(fVar);
            f(fVar);
            b();
        }
    }

    /* access modifiers changed from: 0000 */
    public void d(String str) {
        f fVar = (f) this.h.get(str);
        if (fVar != null && fVar.c == 6) {
            f(fVar);
            b();
        }
    }

    private void i(f fVar) {
        al alVar = new al(this.f.f, this.o, this.r, 2, new g(this, fVar.b));
        alVar.j = 1;
        alVar.a("cached", (Object) "0");
        String str = fVar.d.q;
        if (!str.isEmpty()) {
            alVar.a("ad_id", (Object) str);
        }
        alVar.a("location", (Object) fVar.b);
        this.m.a(alVar);
        this.r.b(this.f.a(fVar.d.m), fVar.b, str);
    }

    /* access modifiers changed from: 0000 */
    public CBImpressionError a(JSONObject jSONObject) {
        if (jSONObject == null) {
            return CBImpressionError.INVALID_RESPONSE;
        }
        JSONObject optJSONObject = jSONObject.optJSONObject("assets");
        if (optJSONObject == null) {
            return CBImpressionError.INVALID_RESPONSE;
        }
        JSONObject optJSONObject2 = optJSONObject.optJSONObject(CBUtility.a(CBUtility.a()) ? "video-portrait" : "video-landscape");
        if (optJSONObject2 == null) {
            return CBImpressionError.VIDEO_UNAVAILABLE_FOR_CURRENT_ORIENTATION;
        }
        String optString = optJSONObject2.optString("id");
        if (optString.isEmpty()) {
            return CBImpressionError.VIDEO_ID_MISSING;
        }
        return new File(this.b.d().g, optString).exists() ? null : CBImpressionError.VIDEO_UNAVAILABLE;
    }

    private boolean e() {
        boolean z2 = false;
        if (this.f.f1903a != 0 || com.chartboost.sdk.i.u) {
            return false;
        }
        if (this.q.getInt("cbPrefSessionCount", 0) == 1) {
            z2 = true;
        }
        return z2;
    }
}
