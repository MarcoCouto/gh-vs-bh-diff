package com.chartboost.sdk.impl;

import android.app.Application;
import android.webkit.WebView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;

public class o {

    /* renamed from: a reason: collision with root package name */
    static List<String> f1918a = new ArrayList();
    static HashMap<String, p> b = new HashMap<>();

    static boolean b() {
        return true;
    }

    public static void a() {
        f1918a.clear();
        if (b()) {
            f1918a.add("moat");
        }
    }

    public static void a(List<String> list) {
        for (String str : list) {
            if (f1918a.contains(str) && !b.containsKey(str)) {
                b.put(str, null);
            }
        }
    }

    public static void a(Application application, boolean z, boolean z2, boolean z3) {
        for (String str : b.keySet()) {
            if (str.contains("moat")) {
                if (b.get(str) != null) {
                    ((p) b.get(str)).b();
                }
                r rVar = new r();
                rVar.a(application, z, z2, z3);
                b.put("moat", rVar);
            }
        }
    }

    public static void a(WebView webView, HashSet<String> hashSet) {
        Iterator it = hashSet.iterator();
        while (it.hasNext()) {
            p pVar = (p) b.get((String) it.next());
            if (pVar != null) {
                pVar.a(webView);
            }
        }
    }

    public static void c() {
        for (p pVar : b.values()) {
            if (pVar != null) {
                pVar.a();
            }
        }
    }

    public static void d() {
        for (String str : b.keySet()) {
            p pVar = (p) b.get(str);
            if (pVar != null) {
                pVar.b();
            }
        }
    }

    public static JSONArray e() {
        JSONArray jSONArray = new JSONArray();
        if (f1918a != null) {
            for (String put : f1918a) {
                jSONArray.put(put);
            }
        }
        return jSONArray;
    }

    public static JSONArray f() {
        JSONArray jSONArray = new JSONArray();
        if (b != null) {
            for (String put : b.keySet()) {
                jSONArray.put(put);
            }
        }
        return jSONArray;
    }

    public static String a(HashSet<String> hashSet) {
        JSONArray jSONArray = new JSONArray();
        Iterator it = hashSet.iterator();
        while (it.hasNext()) {
            String str = (String) it.next();
            if (((p) b.get(str)) != null) {
                jSONArray.put(str);
            }
        }
        return jSONArray.toString();
    }
}
