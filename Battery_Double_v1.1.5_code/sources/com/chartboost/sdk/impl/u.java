package com.chartboost.sdk.impl;

import android.os.Handler;
import android.text.TextUtils;
import com.chartboost.sdk.Chartboost;
import com.chartboost.sdk.Model.CBError.CBImpressionError;
import com.chartboost.sdk.Model.b;
import com.chartboost.sdk.h;
import com.chartboost.sdk.i;
import com.chartboost.sdk.impl.c.a;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import org.json.JSONException;
import org.json.JSONObject;

public class u {
    public static void a(final String str, final String str2) {
        final h a2 = h.a();
        if (!c(str, str2)) {
            Handler handler = a2.p;
            c cVar = a2.g;
            cVar.getClass();
            handler.post(new a(4, str, CBImpressionError.INTERNAL));
            return;
        }
        i.t = false;
        try {
            final Map a3 = b.a(str2);
            a2.i.f1917a.a(1, a3, new AtomicInteger(), new h() {
                public void a(boolean z, int i, int i2) {
                    if (!z) {
                        Handler handler = a2.p;
                        c cVar = a2.g;
                        cVar.getClass();
                        handler.post(new a(4, str, CBImpressionError.ASSETS_DOWNLOAD_FAILURE));
                        return;
                    }
                    if (a2.f.a(str) == null) {
                        a2.f.a(str, str2, a3);
                    }
                    Chartboost.cacheInterstitial(str);
                }
            });
        } catch (Exception unused) {
            Handler handler2 = a2.p;
            c cVar2 = a2.g;
            cVar2.getClass();
            handler2.post(new a(4, str, CBImpressionError.ASSETS_DOWNLOAD_FAILURE));
        }
    }

    public static void b(final String str, final String str2) {
        final h a2 = h.a();
        if (!c(str, str2)) {
            Handler handler = a2.p;
            c cVar = a2.g;
            cVar.getClass();
            handler.post(new a(4, str, CBImpressionError.INTERNAL));
            return;
        }
        i.t = false;
        try {
            final Map a3 = b.a(str2);
            a2.i.f1917a.a(1, a3, new AtomicInteger(), new h() {
                public void a(boolean z, int i, int i2) {
                    if (!z) {
                        Handler handler = a2.p;
                        c cVar = a2.l;
                        cVar.getClass();
                        handler.post(new a(4, str, CBImpressionError.ASSETS_DOWNLOAD_FAILURE));
                        return;
                    }
                    if (a2.k.a(str) == null) {
                        a2.k.a(str, str2, a3);
                    }
                    Chartboost.cacheRewardedVideo(str);
                }
            });
        } catch (Exception unused) {
            Handler handler2 = a2.p;
            c cVar2 = a2.l;
            cVar2.getClass();
            handler2.post(new a(4, str, CBImpressionError.ASSETS_DOWNLOAD_FAILURE));
        }
    }

    private static boolean c(String str, String str2) {
        if (h.a() == null || TextUtils.isEmpty(str) || TextUtils.isEmpty(str2)) {
            return false;
        }
        try {
            t.a(new JSONObject(str2));
            return true;
        } catch (JSONException unused) {
            return false;
        }
    }
}
