package com.chartboost.sdk.impl;

import android.content.Context;
import android.graphics.Point;
import android.os.Handler;
import android.support.v4.view.ViewCompat;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.RelativeLayout.LayoutParams;
import com.chartboost.sdk.Libraries.CBLogging;
import com.chartboost.sdk.Libraries.CBUtility;
import com.chartboost.sdk.Libraries.h;
import com.chartboost.sdk.Model.CBError.CBImpressionError;
import com.chartboost.sdk.Model.c;
import com.chartboost.sdk.e;
import com.chartboost.sdk.g;
import com.integralads.avid.library.inmobi.utils.AvidJSONUtil;
import org.json.JSONObject;

public class w extends e {
    h j = new h(this);
    h k = new h(this);
    h l = new h(this);
    h m = new h(this);
    h n = new h(this);
    h o = new h(this);
    protected float p = 1.0f;
    private final String q = "ImageViewProtocol";

    public class a extends com.chartboost.sdk.e.a {
        protected ba c;
        protected bb d;
        protected bb e;
        protected ImageView f;
        private boolean h = false;

        protected a(Context context) {
            super(context);
            setBackgroundColor(0);
            setLayoutParams(new LayoutParams(-1, -1));
            g a2 = g.a();
            this.c = (ba) a2.a(new ba(context));
            addView(this.c, new LayoutParams(-1, -1));
            this.e = (bb) a2.a(new bb(context, w.this) {
                /* access modifiers changed from: protected */
                public void a(MotionEvent motionEvent) {
                    a.this.a(motionEvent.getX(), motionEvent.getY(), (float) a.this.e.getWidth(), (float) a.this.e.getHeight());
                }
            });
            a((View) this.e);
            this.e.setContentDescription("CBAd");
            this.f = (ImageView) a2.a(new ImageView(context));
            this.f.setBackgroundColor(ViewCompat.MEASURED_STATE_MASK);
            addView(this.f);
            addView(this.e);
        }

        /* access modifiers changed from: protected */
        public void c() {
            this.d = new bb(getContext()) {
                /* access modifiers changed from: protected */
                public void a(MotionEvent motionEvent) {
                    a.this.d();
                }
            };
            this.d.setContentDescription("CBClose");
            addView(this.d);
        }

        /* access modifiers changed from: protected */
        public void a(float f2, float f3, float f4, float f5) {
            w.this.b(com.chartboost.sdk.Libraries.e.a(com.chartboost.sdk.Libraries.e.a(AvidJSONUtil.KEY_X, (Object) Float.valueOf(f2)), com.chartboost.sdk.Libraries.e.a(AvidJSONUtil.KEY_Y, (Object) Float.valueOf(f3)), com.chartboost.sdk.Libraries.e.a("w", (Object) Float.valueOf(f4)), com.chartboost.sdk.Libraries.e.a("h", (Object) Float.valueOf(f5))));
        }

        /* access modifiers changed from: protected */
        public void a(int i, int i2) {
            int i3;
            int i4;
            if (!this.h) {
                c();
                this.h = true;
            }
            boolean a2 = CBUtility.a(w.this.a());
            h hVar = a2 ? w.this.j : w.this.k;
            h hVar2 = a2 ? w.this.l : w.this.m;
            if (!hVar.c()) {
                if (hVar == w.this.j) {
                    hVar = w.this.k;
                } else {
                    hVar = w.this.j;
                }
            }
            if (!hVar2.c()) {
                if (hVar2 == w.this.l) {
                    hVar2 = w.this.m;
                } else {
                    hVar2 = w.this.l;
                }
            }
            LayoutParams layoutParams = new LayoutParams(-2, -2);
            LayoutParams layoutParams2 = new LayoutParams(-2, -2);
            w.this.a(layoutParams, hVar, 1.0f);
            w.this.p = Math.min(Math.min(((float) i) / ((float) layoutParams.width), ((float) i2) / ((float) layoutParams.height)), 1.0f);
            layoutParams.width = (int) (((float) layoutParams.width) * w.this.p);
            layoutParams.height = (int) (((float) layoutParams.height) * w.this.p);
            Point b = w.this.b(a2 ? "frame-portrait" : "frame-landscape");
            layoutParams.leftMargin = Math.round((((float) (i - layoutParams.width)) / 2.0f) + ((((float) b.x) / hVar.e()) * w.this.p));
            layoutParams.topMargin = Math.round((((float) (i2 - layoutParams.height)) / 2.0f) + ((((float) b.y) / hVar.e()) * w.this.p));
            w.this.a(layoutParams2, hVar2, 1.0f);
            Point b2 = w.this.b(a2 ? "close-portrait" : "close-landscape");
            if (b2.x == 0 && b2.y == 0) {
                i4 = layoutParams.leftMargin + layoutParams.width + Math.round(((float) (-layoutParams2.width)) / 2.0f);
                i3 = layoutParams.topMargin + Math.round(((float) (-layoutParams2.height)) / 2.0f);
            } else {
                int round = Math.round(((((float) layoutParams.leftMargin) + (((float) layoutParams.width) / 2.0f)) + ((float) b2.x)) - (((float) layoutParams2.width) / 2.0f));
                i3 = Math.round(((((float) layoutParams.topMargin) + (((float) layoutParams.height) / 2.0f)) + ((float) b2.y)) - (((float) layoutParams2.height) / 2.0f));
                i4 = round;
            }
            layoutParams2.leftMargin = Math.min(Math.max(0, i4), i - layoutParams2.width);
            layoutParams2.topMargin = Math.min(Math.max(0, i3), i2 - layoutParams2.height);
            this.c.setLayoutParams(layoutParams);
            this.d.setLayoutParams(layoutParams2);
            this.c.setScaleType(ScaleType.FIT_CENTER);
            this.c.a(hVar);
            this.d.a(hVar2);
            h hVar3 = a2 ? w.this.n : w.this.o;
            if (!hVar3.c()) {
                if (hVar3 == w.this.n) {
                    hVar3 = w.this.o;
                } else {
                    hVar3 = w.this.n;
                }
            }
            LayoutParams layoutParams3 = new LayoutParams(-2, -2);
            w.this.a(layoutParams3, hVar3, w.this.p);
            Point b3 = w.this.b(a2 ? "ad-portrait" : "ad-landscape");
            layoutParams3.leftMargin = Math.round((((float) (i - layoutParams3.width)) / 2.0f) + ((((float) b3.x) / hVar3.e()) * w.this.p));
            layoutParams3.topMargin = Math.round((((float) (i2 - layoutParams3.height)) / 2.0f) + ((((float) b3.y) / hVar3.e()) * w.this.p));
            this.f.setLayoutParams(layoutParams3);
            this.e.setLayoutParams(layoutParams3);
            this.e.a(ScaleType.FIT_CENTER);
            this.e.a(hVar3);
        }

        /* access modifiers changed from: protected */
        public void d() {
            w.this.h();
        }

        public void b() {
            super.b();
            this.c = null;
            this.d = null;
            this.e = null;
            this.f = null;
        }
    }

    public w(c cVar, Handler handler, com.chartboost.sdk.c cVar2) {
        super(cVar, handler, cVar2);
    }

    /* access modifiers changed from: protected */
    public com.chartboost.sdk.e.a b(Context context) {
        return new a(context);
    }

    public boolean a(JSONObject jSONObject) {
        if (!super.a(jSONObject)) {
            return false;
        }
        if (this.d.isNull("frame-portrait") || this.d.isNull("close-portrait")) {
            this.h = false;
        }
        if (this.d.isNull("frame-landscape") || this.d.isNull("close-landscape")) {
            this.i = false;
        }
        if (this.d.isNull("ad-portrait")) {
            this.h = false;
        }
        if (this.d.isNull("ad-landscape")) {
            this.i = false;
        }
        if (this.k.a("frame-landscape") && this.j.a("frame-portrait") && this.m.a("close-landscape") && this.l.a("close-portrait") && this.o.a("ad-landscape") && this.n.a("ad-portrait")) {
            return true;
        }
        CBLogging.b("ImageViewProtocol", "Error while downloading the assets");
        a(CBImpressionError.ASSETS_DOWNLOAD_FAILURE);
        return false;
    }

    /* access modifiers changed from: protected */
    public Point b(String str) {
        JSONObject a2 = com.chartboost.sdk.Libraries.e.a(this.d, str, "offset");
        if (a2 != null) {
            return new Point(a2.optInt(AvidJSONUtil.KEY_X), a2.optInt(AvidJSONUtil.KEY_Y));
        }
        return new Point(0, 0);
    }

    public void a(ViewGroup.LayoutParams layoutParams, h hVar, float f) {
        if (hVar != null && hVar.c()) {
            layoutParams.width = (int) ((((float) hVar.a()) / hVar.e()) * f);
            layoutParams.height = (int) ((((float) hVar.b()) / hVar.e()) * f);
        }
    }

    public void d() {
        super.d();
        this.k = null;
        this.j = null;
        this.m = null;
        this.l = null;
        this.o = null;
        this.n = null;
    }
}
