package com.chartboost.sdk.impl;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.View;

public abstract class bd extends View {

    /* renamed from: a reason: collision with root package name */
    private Bitmap f1890a = null;
    private Canvas b = null;

    /* access modifiers changed from: protected */
    public abstract void a(Canvas canvas);

    public bd(Context context) {
        super(context);
        a(context);
    }

    private void a(Context context) {
        try {
            getClass().getMethod("setLayerType", new Class[]{Integer.TYPE, Paint.class}).invoke(this, new Object[]{Integer.valueOf(1), null});
        } catch (Exception unused) {
        }
    }

    private boolean b(Canvas canvas) {
        try {
            return ((Boolean) Canvas.class.getMethod("isHardwareAccelerated", new Class[0]).invoke(canvas, new Object[0])).booleanValue();
        } catch (Exception unused) {
            return false;
        }
    }

    /* access modifiers changed from: protected */
    public final void onDraw(Canvas canvas) {
        Canvas canvas2;
        boolean b2 = b(canvas);
        if (b2) {
            if (!(this.f1890a != null && this.f1890a.getWidth() == canvas.getWidth() && this.f1890a.getHeight() == canvas.getHeight())) {
                if (this.f1890a != null && !this.f1890a.isRecycled()) {
                    this.f1890a.recycle();
                }
                try {
                    this.f1890a = Bitmap.createBitmap(canvas.getWidth(), canvas.getHeight(), Config.ARGB_8888);
                    this.b = new Canvas(this.f1890a);
                } catch (Throwable unused) {
                    return;
                }
            }
            this.f1890a.eraseColor(0);
            canvas2 = canvas;
            canvas = this.b;
        } else {
            canvas2 = null;
        }
        a(canvas);
        if (b2) {
            canvas2.drawBitmap(this.f1890a, 0.0f, 0.0f, null);
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.f1890a != null && !this.f1890a.isRecycled()) {
            this.f1890a.recycle();
        }
        this.f1890a = null;
    }
}
