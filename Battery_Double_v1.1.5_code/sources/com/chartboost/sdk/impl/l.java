package com.chartboost.sdk.impl;

import com.chartboost.sdk.Libraries.CBLogging;
import com.chartboost.sdk.Libraries.CBUtility;
import com.chartboost.sdk.Libraries.f;
import com.chartboost.sdk.Libraries.i;
import com.chartboost.sdk.Model.CBError;
import com.chartboost.sdk.Model.b;
import com.chartboost.sdk.Model.e;
import com.chartboost.sdk.Tracking.a;
import io.fabric.sdk.android.services.settings.SettingsJsonConstants;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

public class l {

    /* renamed from: a reason: collision with root package name */
    int f1915a = 1;
    private final Executor b;
    private final aj c;
    private final ak d;
    private final AtomicReference<e> e;
    private final i f;
    private final a g;
    private final f h;
    private k i = null;
    private final PriorityQueue<j> j;

    public l(Executor executor, f fVar, aj ajVar, ak akVar, AtomicReference<e> atomicReference, i iVar, a aVar) {
        this.b = executor;
        this.h = fVar;
        this.c = ajVar;
        this.d = akVar;
        this.e = atomicReference;
        this.f = iVar;
        this.g = aVar;
        this.j = new PriorityQueue<>();
    }

    public synchronized void a(int i2, Map<String, b> map, AtomicInteger atomicInteger, h hVar) {
        synchronized (this) {
            long b2 = this.f.b();
            AtomicInteger atomicInteger2 = new AtomicInteger();
            AtomicReference atomicReference = new AtomicReference(hVar);
            for (b bVar : map.values()) {
                long j2 = b2;
                long j3 = b2;
                j jVar = r2;
                j jVar2 = new j(this.f, i2, bVar.b, bVar.c, bVar.f1829a, atomicInteger, atomicReference, j2, atomicInteger2);
                this.j.add(jVar);
                b2 = j3;
            }
            if (this.f1915a == 1 || this.f1915a == 2) {
                d();
            }
        }
    }

    public synchronized void a(AtomicInteger atomicInteger) {
        atomicInteger.set(-10000);
        switch (this.f1915a) {
            case 1:
            case 3:
                break;
            case 2:
                if ((this.i.f1914a.e == atomicInteger) && this.i.b()) {
                    this.i = null;
                    d();
                    break;
                }
        }
    }

    public synchronized void a() {
        switch (this.f1915a) {
            case 1:
                CBLogging.a("Downloader", "Change state to PAUSED");
                this.f1915a = 4;
                break;
            case 2:
                if (!this.i.b()) {
                    CBLogging.a("Downloader", "Change state to PAUSING");
                    this.f1915a = 3;
                    break;
                } else {
                    this.j.add(this.i.f1914a);
                    this.i = null;
                    CBLogging.a("Downloader", "Change state to PAUSED");
                    this.f1915a = 4;
                    break;
                }
            case 3:
                break;
        }
    }

    public synchronized void b() {
        switch (this.f1915a) {
            case 1:
            case 2:
                break;
            case 3:
                CBLogging.a("Downloader", "Change state to DOWNLOADING");
                this.f1915a = 2;
                break;
            case 4:
                CBLogging.a("Downloader", "Change state to IDLE");
                this.f1915a = 1;
                d();
                break;
        }
    }

    /* access modifiers changed from: 0000 */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x00d2, code lost:
        return;
     */
    public synchronized void a(k kVar, CBError cBError, ai aiVar) {
        String str;
        String str2;
        k kVar2 = kVar;
        ai aiVar2 = aiVar;
        synchronized (this) {
            switch (this.f1915a) {
                case 1:
                    break;
                case 2:
                case 3:
                    if (kVar2 == this.i) {
                        j jVar = kVar2.f1914a;
                        this.i = null;
                        long millis = TimeUnit.NANOSECONDS.toMillis(kVar2.g);
                        jVar.f.addAndGet((int) millis);
                        jVar.a(this.b, cBError == null);
                        long millis2 = TimeUnit.NANOSECONDS.toMillis(kVar2.h);
                        long millis3 = TimeUnit.NANOSECONDS.toMillis(kVar2.i);
                        if (cBError == null) {
                            this.g.a(jVar.c, millis, millis2, millis3);
                            StringBuilder sb = new StringBuilder();
                            sb.append("Downloaded ");
                            sb.append(jVar.c);
                            CBLogging.a("Downloader", sb.toString());
                        } else {
                            String b2 = cBError.b();
                            this.g.a(jVar.c, b2, millis, millis2, millis3);
                            String str3 = "Downloader";
                            StringBuilder sb2 = new StringBuilder();
                            sb2.append("Failed to download ");
                            sb2.append(jVar.c);
                            if (aiVar2 != null) {
                                StringBuilder sb3 = new StringBuilder();
                                sb3.append(" Status code=");
                                sb3.append(aiVar2.f1866a);
                                str = sb3.toString();
                            } else {
                                str = "";
                            }
                            sb2.append(str);
                            if (b2 != null) {
                                StringBuilder sb4 = new StringBuilder();
                                sb4.append(" Error message=");
                                sb4.append(b2);
                                str2 = sb4.toString();
                            } else {
                                str2 = "";
                            }
                            sb2.append(str2);
                            CBLogging.a(str3, sb2.toString());
                        }
                        if (this.f1915a != 3) {
                            d();
                            break;
                        } else {
                            CBLogging.a("Downloader", "Change state to PAUSED");
                            this.f1915a = 4;
                            break;
                        }
                    } else {
                        return;
                    }
            }
        }
    }

    private void d() {
        if (this.i != null) {
            j jVar = (j) this.j.peek();
            if (jVar != null && this.i.f1914a.f1913a > jVar.f1913a && this.i.b()) {
                this.j.add(this.i.f1914a);
                this.i = null;
            }
        }
        while (this.i == null) {
            j jVar2 = (j) this.j.poll();
            if (jVar2 == null) {
                break;
            } else if (jVar2.e.get() > 0) {
                File file = new File(this.h.d().f1820a, jVar2.d);
                if (file.exists() || file.mkdirs() || file.isDirectory()) {
                    File file2 = new File(file, jVar2.b);
                    if (file2.exists()) {
                        this.h.c(file2);
                        jVar2.a(this.b, true);
                    } else {
                        this.i = new k(this, this.d, jVar2, file2);
                        this.c.a(this.i);
                        this.g.a(jVar2.c, jVar2.b);
                    }
                } else {
                    StringBuilder sb = new StringBuilder();
                    sb.append("Unable to create directory ");
                    sb.append(file.getPath());
                    CBLogging.b("Downloader", sb.toString());
                    jVar2.a(this.b, false);
                }
            }
        }
        if (this.i != null) {
            if (this.f1915a != 2) {
                CBLogging.a("Downloader", "Change state to DOWNLOADING");
                this.f1915a = 2;
            }
        } else if (this.f1915a != 1) {
            CBLogging.a("Downloader", "Change state to IDLE");
            this.f1915a = 1;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:56:0x0144 A[Catch:{ Exception -> 0x019b }] */
    /* JADX WARNING: Removed duplicated region for block: B:80:0x0186 A[SYNTHETIC] */
    public synchronized void c() {
        boolean z;
        synchronized (this) {
            if (this.f1915a == 1) {
                try {
                    CBLogging.a("Downloader", "########### Trimming the disk cache");
                    File file = this.h.d().f1820a;
                    ArrayList arrayList = new ArrayList();
                    String[] list = file.list();
                    if (list != null && list.length > 0) {
                        for (String str : list) {
                            if (!str.equalsIgnoreCase("requests") && !str.equalsIgnoreCase("track") && !str.equalsIgnoreCase(SettingsJsonConstants.SESSION_KEY) && !str.equalsIgnoreCase("videoCompletionEvents")) {
                                if (!str.contains(".")) {
                                    arrayList.addAll(CBUtility.a(new File(file, str), true));
                                }
                            }
                        }
                    }
                    File[] fileArr = new File[arrayList.size()];
                    arrayList.toArray(fileArr);
                    if (fileArr.length > 1) {
                        Arrays.sort(fileArr, new Comparator<File>() {
                            /* renamed from: a */
                            public int compare(File file, File file2) {
                                return Long.valueOf(file.lastModified()).compareTo(Long.valueOf(file2.lastModified()));
                            }
                        });
                    }
                    if (fileArr.length > 0) {
                        e eVar = (e) this.e.get();
                        long j2 = (long) eVar.u;
                        long b2 = this.h.b(this.h.d().g);
                        long a2 = this.f.a();
                        List<String> list2 = eVar.d;
                        StringBuilder sb = new StringBuilder();
                        sb.append("Total local file count:");
                        sb.append(fileArr.length);
                        CBLogging.a("Downloader", sb.toString());
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append("Video Folder Size in bytes :");
                        sb2.append(b2);
                        CBLogging.a("Downloader", sb2.toString());
                        StringBuilder sb3 = new StringBuilder();
                        sb3.append("Max Bytes allowed:");
                        sb3.append(j2);
                        CBLogging.a("Downloader", sb3.toString());
                        int length = fileArr.length;
                        long j3 = b2;
                        int i2 = 0;
                        while (i2 < length) {
                            File file2 = fileArr[i2];
                            long j4 = j2;
                            e eVar2 = eVar;
                            boolean z2 = TimeUnit.MILLISECONDS.toDays(a2 - file2.lastModified()) >= ((long) eVar.w);
                            boolean endsWith = file2.getName().endsWith(".tmp");
                            File parentFile = file2.getParentFile();
                            boolean contains = parentFile.getAbsolutePath().contains("/videos");
                            boolean z3 = j3 > j4 && contains;
                            if (file2.length() != 0 && !endsWith && !z2 && !list2.contains(parentFile.getName())) {
                                if (!z3) {
                                    z = false;
                                    if (z) {
                                        if (contains) {
                                            j3 -= file2.length();
                                        }
                                        StringBuilder sb4 = new StringBuilder();
                                        sb4.append("Deleting file at path:");
                                        sb4.append(file2.getPath());
                                        CBLogging.a("Downloader", sb4.toString());
                                        if (!file2.delete()) {
                                            StringBuilder sb5 = new StringBuilder();
                                            sb5.append("Unable to delete ");
                                            sb5.append(file2.getPath());
                                            CBLogging.b("Downloader", sb5.toString());
                                        }
                                    }
                                    i2++;
                                    j2 = j4;
                                    eVar = eVar2;
                                }
                            }
                            z = true;
                            if (z) {
                            }
                            i2++;
                            j2 = j4;
                            eVar = eVar2;
                        }
                    }
                    this.g.a(this.h.e());
                } catch (Exception e2) {
                    a.a(getClass(), "reduceCacheSize", e2);
                }
            } else {
                return;
            }
        }
        return;
    }
}
