package com.chartboost.sdk.impl;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;

public class bk {

    /* renamed from: a reason: collision with root package name */
    public static final char f1900a = File.separatorChar;
    public static final String b;

    static {
        bm bmVar = new bm(4);
        PrintWriter printWriter = new PrintWriter(bmVar);
        printWriter.println();
        b = bmVar.toString();
        printWriter.close();
    }

    public static void a(InputStream inputStream) {
        a((Closeable) inputStream);
    }

    public static void a(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException unused) {
            }
        }
    }

    public static byte[] b(InputStream inputStream) throws IOException {
        bl blVar = new bl();
        a(inputStream, (OutputStream) blVar);
        return blVar.a();
    }

    public static byte[] a(InputStream inputStream, long j) throws IOException {
        if (j <= 2147483647L) {
            return a(inputStream, (int) j);
        }
        StringBuilder sb = new StringBuilder();
        sb.append("Size cannot be greater than Integer max value: ");
        sb.append(j);
        throw new IllegalArgumentException(sb.toString());
    }

    public static byte[] a(InputStream inputStream, int i) throws IOException {
        if (i >= 0) {
            int i2 = 0;
            if (i == 0) {
                return new byte[0];
            }
            byte[] bArr = new byte[i];
            while (i2 < i) {
                int read = inputStream.read(bArr, i2, i - i2);
                if (read == -1) {
                    break;
                }
                i2 += read;
            }
            if (i2 == i) {
                return bArr;
            }
            StringBuilder sb = new StringBuilder();
            sb.append("Unexpected readed size. current: ");
            sb.append(i2);
            sb.append(", excepted: ");
            sb.append(i);
            throw new IOException(sb.toString());
        }
        StringBuilder sb2 = new StringBuilder();
        sb2.append("Size must be equal or greater than zero: ");
        sb2.append(i);
        throw new IllegalArgumentException(sb2.toString());
    }

    public static int a(InputStream inputStream, OutputStream outputStream) throws IOException {
        long b2 = b(inputStream, outputStream);
        if (b2 > 2147483647L) {
            return -1;
        }
        return (int) b2;
    }

    public static long b(InputStream inputStream, OutputStream outputStream) throws IOException {
        return a(inputStream, outputStream, new byte[4096]);
    }

    public static long a(InputStream inputStream, OutputStream outputStream, byte[] bArr) throws IOException {
        long j = 0;
        while (true) {
            int read = inputStream.read(bArr);
            if (-1 == read) {
                return j;
            }
            outputStream.write(bArr, 0, read);
            j += (long) read;
        }
    }
}
