package com.chartboost.sdk.impl;

import android.app.Activity;
import android.content.Context;
import com.google.android.gms.common.GoogleApiAvailability;

public class au {
    public static boolean a(Context context) {
        GoogleApiAvailability instance = GoogleApiAvailability.getInstance();
        int isGooglePlayServicesAvailable = instance.isGooglePlayServicesAvailable(context);
        if (isGooglePlayServicesAvailable == 0) {
            return true;
        }
        if (!instance.isUserResolvableError(isGooglePlayServicesAvailable) || !(context instanceof Activity)) {
            return false;
        }
        instance.getErrorDialog((Activity) context, isGooglePlayServicesAvailable, 9000).show();
        return true;
    }
}
