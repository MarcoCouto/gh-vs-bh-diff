package com.chartboost.sdk.impl;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.PorterDuff.Mode;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.ImageView.ScaleType;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import com.chartboost.sdk.Libraries.h;

public abstract class bb extends RelativeLayout {

    /* renamed from: a reason: collision with root package name */
    private final Rect f1886a;
    final a c;
    protected Button d;
    boolean e;

    private class a extends ba {
        private boolean c;

        public a(Context context) {
            super(context);
            this.c = false;
            this.c = false;
        }

        public boolean performClick() {
            super.performClick();
            return true;
        }

        /* access modifiers changed from: protected */
        public void a(boolean z) {
            if (!bb.this.e || !z) {
                if (this.c) {
                    if (getDrawable() != null) {
                        getDrawable().clearColorFilter();
                    } else if (getBackground() != null) {
                        getBackground().clearColorFilter();
                    }
                    invalidate();
                    this.c = false;
                }
            } else if (!this.c) {
                if (getDrawable() != null) {
                    getDrawable().setColorFilter(1996488704, Mode.SRC_ATOP);
                } else if (getBackground() != null) {
                    getBackground().setColorFilter(1996488704, Mode.SRC_ATOP);
                }
                invalidate();
                this.c = true;
            }
        }
    }

    /* access modifiers changed from: protected */
    public abstract void a(MotionEvent motionEvent);

    public bb(Context context) {
        this(context, null);
    }

    @SuppressLint({"ClickableViewAccessibility"})
    public bb(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f1886a = new Rect();
        this.d = null;
        this.e = true;
        this.c = new a(getContext());
        this.c.setOnTouchListener(new OnTouchListener() {
            public boolean onTouch(View view, MotionEvent motionEvent) {
                boolean a2 = bb.this.a(view, motionEvent);
                switch (motionEvent.getActionMasked()) {
                    case 0:
                        bb.this.c.a(a2);
                        return a2;
                    case 1:
                        if (bb.this.getVisibility() == 0 && bb.this.isEnabled() && a2) {
                            bb.this.a(motionEvent);
                        }
                        bb.this.c.a(false);
                        break;
                    case 2:
                        bb.this.c.a(a2);
                        break;
                    case 3:
                    case 4:
                        bb.this.c.a(false);
                        break;
                }
                return true;
            }
        });
        addView(this.c, new LayoutParams(-1, -1));
    }

    /* access modifiers changed from: 0000 */
    public boolean a(View view, MotionEvent motionEvent) {
        view.getLocalVisibleRect(this.f1886a);
        this.f1886a.left += view.getPaddingLeft();
        this.f1886a.top += view.getPaddingTop();
        this.f1886a.right -= view.getPaddingRight();
        this.f1886a.bottom -= view.getPaddingBottom();
        return this.f1886a.contains(Math.round(motionEvent.getX()), Math.round(motionEvent.getY()));
    }

    public void a(String str) {
        if (str != null) {
            a().setText(str);
            addView(a(), new LayoutParams(-1, -1));
            this.c.setVisibility(8);
            a(false);
            this.d.setOnClickListener(new OnClickListener() {
                public void onClick(View view) {
                    bb.this.a((MotionEvent) null);
                }
            });
        } else if (this.d != null) {
            removeView(a());
            this.d = null;
            this.c.setVisibility(0);
            a(true);
        }
    }

    public TextView a() {
        if (this.d == null) {
            this.d = new Button(getContext());
            this.d.setGravity(17);
        }
        this.d.postInvalidate();
        return this.d;
    }

    public void a(h hVar) {
        if (hVar != null && hVar.c()) {
            this.c.a(hVar);
            a((String) null);
        }
    }

    public void a(ScaleType scaleType) {
        this.c.setScaleType(scaleType);
    }

    public void a(boolean z) {
        this.e = z;
    }
}
