package com.chartboost.sdk.impl;

import com.appodealx.sdk.utils.RequestInfoKeys;
import com.chartboost.sdk.Model.a;
import com.github.mikephil.charting.utils.Utils;
import com.ironsource.sdk.constants.Constants.ParametersKeys;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class t extends a {

    /* renamed from: a reason: collision with root package name */
    public final String f1921a;
    public final String b;
    public final double c;
    public final String d;
    public final String e;
    public final String f;
    public final String g;
    public final String h;
    public final String i;
    public final String j;
    public final List<String> k = new ArrayList();

    public t(JSONObject jSONObject) throws JSONException {
        String str;
        String str2 = "";
        String str3 = "";
        String str4 = "";
        String str5 = "";
        String str6 = "";
        this.m = 1;
        String str7 = "";
        String str8 = "";
        String str9 = "";
        String str10 = "";
        this.f1921a = jSONObject.getString("id");
        this.b = jSONObject.optString("nbr", "");
        this.h = jSONObject.optString("cur", "USD");
        this.i = jSONObject.optString("bidid", "");
        JSONArray optJSONArray = jSONObject.optJSONArray("seatbid");
        double d2 = Utils.DOUBLE_EPSILON;
        if (optJSONArray == null || optJSONArray.length() <= 0) {
            str = str5;
        } else {
            JSONObject jSONObject2 = optJSONArray.getJSONObject(0);
            str = jSONObject2.getString("seat");
            JSONArray optJSONArray2 = jSONObject2.optJSONArray("bid");
            if (optJSONArray2 != null && optJSONArray2.length() > 0) {
                JSONObject jSONObject3 = optJSONArray2.getJSONObject(0);
                str4 = jSONObject3.getString("id");
                str3 = jSONObject3.getString("impid");
                d2 = jSONObject3.getDouble("price");
                str6 = jSONObject3.optString("burl", "");
                str9 = jSONObject3.optString("crid", "");
                str2 = jSONObject3.optString(ParametersKeys.ADM, "");
                JSONObject jSONObject4 = jSONObject3.optJSONObject(RequestInfoKeys.EXT).getJSONObject("bidder");
                if (jSONObject4 != null) {
                    str8 = jSONObject4.optString("crtype", "");
                    str7 = jSONObject4.optString("adId", "");
                    str10 = jSONObject4.optString("cgn", "");
                    JSONArray optJSONArray3 = jSONObject4.optJSONArray("imptrackers");
                    if (optJSONArray3 != null) {
                        for (int i2 = 0; i2 < optJSONArray3.length(); i2++) {
                            this.k.add(optJSONArray3.optString(i2, ""));
                        }
                    }
                }
            }
        }
        this.c = d2;
        this.d = str2;
        this.e = str3;
        this.f = str4;
        this.g = str;
        this.j = str6;
        this.q = str7;
        this.A = str8;
        this.s = str9;
        this.r = str10;
        this.y.put("imptrackers", this.k);
    }

    public static void a(JSONObject jSONObject) throws JSONException {
        jSONObject.getString("id");
        jSONObject.optString("nbr", "");
        jSONObject.optString("cur", "USD");
        jSONObject.optString("bidid", "");
        JSONArray optJSONArray = jSONObject.optJSONArray("seatbid");
        if (optJSONArray != null && optJSONArray.length() > 0) {
            JSONObject jSONObject2 = optJSONArray.getJSONObject(0);
            jSONObject2.getString("seat");
            JSONArray optJSONArray2 = jSONObject2.optJSONArray("bid");
            if (optJSONArray2 != null && optJSONArray2.length() > 0) {
                JSONObject jSONObject3 = optJSONArray2.getJSONObject(0);
                jSONObject3.getString("id");
                jSONObject3.getString("impid");
                jSONObject3.getDouble("price");
                jSONObject3.optString("burl", "");
                jSONObject3.optString("crid", "");
                jSONObject3.optString(ParametersKeys.ADM, "");
                JSONObject optJSONObject = jSONObject3.optJSONObject(RequestInfoKeys.EXT);
                if (optJSONObject != null) {
                    optJSONObject.optString("crtype", "");
                    optJSONObject.optString("adId", "");
                    optJSONObject.optString("cgn", "");
                }
            }
        }
    }
}
