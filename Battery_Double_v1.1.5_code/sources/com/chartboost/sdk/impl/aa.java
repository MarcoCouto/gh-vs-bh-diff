package com.chartboost.sdk.impl;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v4.view.ViewCompat;
import android.view.MotionEvent;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import com.chartboost.sdk.e;

@SuppressLint({"ViewConstructor"})
public class aa extends LinearLayout {

    /* renamed from: a reason: collision with root package name */
    final x f1852a;
    final bb b;
    private LinearLayout c;
    private ba d;
    private TextView e;
    private int f = Integer.MIN_VALUE;

    public aa(Context context, x xVar) {
        super(context);
        this.f1852a = xVar;
        int round = Math.round(context.getResources().getDisplayMetrics().density * 8.0f);
        setOrientation(1);
        setGravity(17);
        this.c = new LinearLayout(context);
        this.c.setGravity(17);
        this.c.setOrientation(0);
        this.c.setPadding(round, round, round, round);
        this.d = new ba(context);
        this.d.setScaleType(ScaleType.FIT_CENTER);
        this.d.setPadding(0, 0, round, 0);
        LayoutParams layoutParams = new LayoutParams(-2, -2);
        xVar.a(layoutParams, xVar.H, 1.0f);
        this.e = new TextView(getContext());
        this.e.setTextColor(-1);
        this.e.setTypeface(null, 1);
        this.e.setGravity(17);
        this.e.setTextSize(2, e.a(context) ? 26.0f : 16.0f);
        this.c.addView(this.d, layoutParams);
        this.c.addView(this.e, new LayoutParams(-2, -2));
        this.b = new bb(getContext()) {
            /* access modifiers changed from: protected */
            public void a(MotionEvent motionEvent) {
                aa.this.b.setEnabled(false);
                aa.this.f1852a.e().g();
            }
        };
        this.b.setContentDescription("CBWatch");
        this.b.setPadding(0, 0, 0, round);
        this.b.a(ScaleType.FIT_CENTER);
        this.b.setPadding(round, round, round, round);
        LayoutParams layoutParams2 = new LayoutParams(-2, -2);
        xVar.a(layoutParams2, xVar.G, 1.0f);
        this.d.a(xVar.H);
        this.b.a(xVar.G);
        addView(this.c, new LayoutParams(-2, -2));
        addView(this.b, layoutParams2);
        a();
    }

    public void a(boolean z) {
        setBackgroundColor(z ? ViewCompat.MEASURED_STATE_MASK : this.f);
    }

    public void a(String str, int i) {
        this.e.setText(str);
        this.f = i;
        a(this.f1852a.s());
    }

    public void a() {
        a(this.f1852a.s());
    }
}
