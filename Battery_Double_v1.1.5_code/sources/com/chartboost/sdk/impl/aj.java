package com.chartboost.sdk.impl;

import android.os.Handler;
import com.chartboost.sdk.Libraries.i;
import java.util.concurrent.Executor;

public class aj {

    /* renamed from: a reason: collision with root package name */
    private final Executor f1867a;
    private final Executor b;
    private final aq c;
    private final ak d;
    private final i e;
    private final Handler f;

    public aj(Executor executor, aq aqVar, ak akVar, i iVar, Handler handler, Executor executor2) {
        this.f1867a = executor2;
        this.b = executor;
        this.c = aqVar;
        this.d = akVar;
        this.e = iVar;
        this.f = handler;
    }

    public <T> void a(af<T> afVar) {
        Executor executor = this.f1867a;
        ap apVar = new ap(this.b, this.c, this.d, this.e, this.f, afVar);
        executor.execute(apVar);
    }
}
