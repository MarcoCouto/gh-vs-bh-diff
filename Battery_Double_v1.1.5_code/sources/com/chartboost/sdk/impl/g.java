package com.chartboost.sdk.impl;

import com.chartboost.sdk.Model.CBError;
import com.chartboost.sdk.i;
import com.chartboost.sdk.impl.al.a;
import org.json.JSONObject;

public class g implements a {

    /* renamed from: a reason: collision with root package name */
    private final e f1911a;
    private final String b;

    public g(e eVar, String str) {
        this.f1911a = eVar;
        this.b = str;
    }

    public void a(al alVar, JSONObject jSONObject) {
        if (this.f1911a.f.h || i.t) {
            synchronized (this.f1911a) {
                this.f1911a.b(this.b);
            }
        }
    }

    public void a(al alVar, CBError cBError) {
        if (this.f1911a.f.h) {
            synchronized (this.f1911a) {
                this.f1911a.b(this.b);
            }
        }
    }
}
