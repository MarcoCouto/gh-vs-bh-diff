package com.chartboost.sdk.impl;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Rect;
import android.os.Build;
import android.os.Build.VERSION;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import com.chartboost.sdk.Chartboost;
import com.chartboost.sdk.Libraries.CBLogging;
import com.chartboost.sdk.Libraries.CBUtility;
import com.chartboost.sdk.Libraries.d;
import com.chartboost.sdk.Libraries.i;
import com.chartboost.sdk.Model.e;
import com.chartboost.sdk.Tracking.a;
import com.chartboost.sdk.g;
import com.facebook.places.model.PlaceFields;
import io.fabric.sdk.android.services.common.CommonUtils;
import java.util.Locale;
import java.util.concurrent.atomic.AtomicReference;
import org.json.JSONObject;

public class ar {

    /* renamed from: a reason: collision with root package name */
    final d f1877a;
    final ak b;
    final AtomicReference<e> c;
    final SharedPreferences d;
    final i e;
    final String f;
    final String g;
    final String h;
    final String i;
    String j;
    String k;
    final String l;
    final Integer m;
    final Integer n;
    final Integer o;
    final Integer p;
    final String q;
    final Float r;
    final String s;
    final String t;
    final String u;
    final JSONObject v;
    final boolean w;
    final String x;
    final Integer y;

    /* JADX WARNING: Removed duplicated region for block: B:49:0x019c  */
    public ar(Context context, String str, d dVar, ak akVar, AtomicReference<e> atomicReference, SharedPreferences sharedPreferences, i iVar) {
        JSONObject jSONObject;
        int i2;
        int i3;
        int i4;
        String str2;
        Object obj;
        this.f1877a = dVar;
        this.b = akVar;
        this.c = atomicReference;
        this.d = sharedPreferences;
        this.e = iVar;
        this.s = str;
        if ("sdk".equals(Build.PRODUCT) || CommonUtils.GOOGLE_SDK.equals(Build.PRODUCT) || (Build.MANUFACTURER != null && Build.MANUFACTURER.contains("Genymotion"))) {
            this.f = "Android Simulator";
        } else {
            this.f = Build.MODEL;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(Build.MANUFACTURER);
        sb.append(" ");
        sb.append(Build.MODEL);
        this.t = sb.toString();
        this.u = at.a(context);
        StringBuilder sb2 = new StringBuilder();
        sb2.append("Android ");
        sb2.append(VERSION.RELEASE);
        this.g = sb2.toString();
        this.h = Locale.getDefault().getCountry();
        this.i = Locale.getDefault().getLanguage();
        this.l = "7.5.0";
        this.r = Float.valueOf(context.getResources().getDisplayMetrics().density);
        try {
            String packageName = context.getPackageName();
            this.j = context.getPackageManager().getPackageInfo(packageName, 128).versionName;
            this.k = packageName;
        } catch (Exception e2) {
            CBLogging.a("RequestBody", "Exception raised getting package mager object", e2);
        }
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(PlaceFields.PHONE);
        int i5 = 0;
        if (telephonyManager == null || telephonyManager.getPhoneType() == 0 || telephonyManager.getSimState() != 5) {
            jSONObject = new JSONObject();
        } else {
            String str3 = null;
            try {
                str2 = telephonyManager.getSimOperator();
            } catch (Exception e3) {
                a.a(Chartboost.class, "Unable to retrieve sim operator information", e3);
                str2 = null;
            }
            if (str2 == null || TextUtils.isEmpty(str2)) {
                obj = null;
            } else {
                str3 = str2.substring(0, 3);
                obj = str2.substring(3);
            }
            jSONObject = com.chartboost.sdk.Libraries.e.a(com.chartboost.sdk.Libraries.e.a("carrier-name", (Object) telephonyManager.getNetworkOperatorName()), com.chartboost.sdk.Libraries.e.a("mobile-country-code", (Object) str3), com.chartboost.sdk.Libraries.e.a("mobile-network-code", obj), com.chartboost.sdk.Libraries.e.a("iso-country-code", (Object) telephonyManager.getNetworkCountryIso()), com.chartboost.sdk.Libraries.e.a("phone-type", (Object) Integer.valueOf(telephonyManager.getPhoneType())));
        }
        this.v = jSONObject;
        this.w = CBUtility.c();
        this.x = CBUtility.d();
        this.y = Integer.valueOf(at.b(context));
        try {
            if (context instanceof Activity) {
                Activity activity = (Activity) context;
                Rect rect = new Rect();
                activity.getWindow().getDecorView().getWindowVisibleDisplayFrame(rect);
                i2 = rect.width();
                try {
                    i5 = rect.height();
                } catch (Exception e4) {
                    e = e4;
                    CBLogging.b("RequestBody", "Exception getting activity size", e);
                    DisplayMetrics displayMetrics = (DisplayMetrics) g.a().a(new DisplayMetrics());
                    displayMetrics.setTo(context.getResources().getDisplayMetrics());
                    if (VERSION.SDK_INT >= 17) {
                    }
                    i3 = displayMetrics.widthPixels;
                    i4 = displayMetrics.heightPixels;
                    this.o = Integer.valueOf(i3);
                    this.p = Integer.valueOf(i4);
                    StringBuilder sb3 = new StringBuilder();
                    sb3.append("");
                    sb3.append(displayMetrics.densityDpi);
                    this.q = sb3.toString();
                    i3 = i2;
                    i5 = i4;
                    this.m = Integer.valueOf(i3);
                    this.n = Integer.valueOf(i5);
                }
            } else {
                i2 = 0;
            }
        } catch (Exception e5) {
            e = e5;
            i2 = 0;
            CBLogging.b("RequestBody", "Exception getting activity size", e);
            DisplayMetrics displayMetrics2 = (DisplayMetrics) g.a().a(new DisplayMetrics());
            displayMetrics2.setTo(context.getResources().getDisplayMetrics());
            if (VERSION.SDK_INT >= 17) {
            }
            i3 = displayMetrics2.widthPixels;
            i4 = displayMetrics2.heightPixels;
            this.o = Integer.valueOf(i3);
            this.p = Integer.valueOf(i4);
            StringBuilder sb32 = new StringBuilder();
            sb32.append("");
            sb32.append(displayMetrics2.densityDpi);
            this.q = sb32.toString();
            i3 = i2;
            i5 = i4;
            this.m = Integer.valueOf(i3);
            this.n = Integer.valueOf(i5);
        }
        DisplayMetrics displayMetrics22 = (DisplayMetrics) g.a().a(new DisplayMetrics());
        displayMetrics22.setTo(context.getResources().getDisplayMetrics());
        if (VERSION.SDK_INT >= 17) {
            WindowManager windowManager = (WindowManager) context.getSystemService("window");
            if (windowManager != null) {
                windowManager.getDefaultDisplay().getRealMetrics(displayMetrics22);
            }
        }
        i3 = displayMetrics22.widthPixels;
        i4 = displayMetrics22.heightPixels;
        this.o = Integer.valueOf(i3);
        this.p = Integer.valueOf(i4);
        StringBuilder sb322 = new StringBuilder();
        sb322.append("");
        sb322.append(displayMetrics22.densityDpi);
        this.q = sb322.toString();
        if (i2 > 0 && i2 <= i3) {
            i3 = i2;
        }
        if (i5 <= 0 || i5 > i4) {
            i5 = i4;
        }
        this.m = Integer.valueOf(i3);
        this.n = Integer.valueOf(i5);
    }
}
