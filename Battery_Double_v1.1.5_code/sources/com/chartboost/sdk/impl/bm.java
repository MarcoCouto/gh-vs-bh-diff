package com.chartboost.sdk.impl;

import java.io.Serializable;
import java.io.Writer;

public class bm extends Writer implements Serializable {

    /* renamed from: a reason: collision with root package name */
    private final StringBuilder f1902a;

    public void close() {
    }

    public void flush() {
    }

    public bm() {
        this.f1902a = new StringBuilder();
    }

    public bm(int i) {
        this.f1902a = new StringBuilder(i);
    }

    public Writer append(char c) {
        this.f1902a.append(c);
        return this;
    }

    public Writer append(CharSequence charSequence) {
        this.f1902a.append(charSequence);
        return this;
    }

    public Writer append(CharSequence charSequence, int i, int i2) {
        this.f1902a.append(charSequence, i, i2);
        return this;
    }

    public void write(String str) {
        if (str != null) {
            this.f1902a.append(str);
        }
    }

    public void write(char[] cArr, int i, int i2) {
        if (cArr != null) {
            this.f1902a.append(cArr, i, i2);
        }
    }

    public String toString() {
        return this.f1902a.toString();
    }
}
