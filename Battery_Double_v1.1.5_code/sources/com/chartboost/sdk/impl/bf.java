package com.chartboost.sdk.impl;

import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.webkit.ConsoleMessage;
import android.webkit.JsPromptResult;
import android.webkit.WebChromeClient;
import android.webkit.WebChromeClient.CustomViewCallback;
import android.webkit.WebView;
import android.widget.FrameLayout;
import com.chartboost.sdk.Libraries.CBLogging;
import com.chartboost.sdk.Libraries.e;
import com.chartboost.sdk.Model.b;
import java.util.Map.Entry;
import org.json.JSONException;
import org.json.JSONObject;

public class bf extends WebChromeClient {

    /* renamed from: a reason: collision with root package name */
    private View f1892a;
    private ViewGroup b;
    private boolean c = false;
    private FrameLayout d;
    private CustomViewCallback e;
    private a f;
    private final bh g;
    private final Handler h;

    public interface a {
        void a(boolean z);
    }

    public bf(View view, ViewGroup viewGroup, View view2, bg bgVar, bh bhVar, Handler handler) {
        this.f1892a = view;
        this.b = viewGroup;
        this.g = bhVar;
        this.h = handler;
    }

    public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
        String simpleName = bf.class.getSimpleName();
        StringBuilder sb = new StringBuilder();
        sb.append("Chartboost Webview:");
        sb.append(consoleMessage.message());
        sb.append(" -- From line ");
        sb.append(consoleMessage.lineNumber());
        sb.append(" of ");
        sb.append(consoleMessage.sourceId());
        Log.d(simpleName, sb.toString());
        return true;
    }

    public boolean onJsPrompt(WebView webView, String str, String str2, String str3, JsPromptResult jsPromptResult) {
        try {
            JSONObject jSONObject = new JSONObject(str2);
            jsPromptResult.confirm(a(jSONObject.getJSONObject("eventArgs"), jSONObject.getString("eventType")));
            return true;
        } catch (JSONException unused) {
            CBLogging.b("CBWebChromeClient", "Exception caught parsing the function name from js to native");
            return true;
        }
    }

    public String a(JSONObject jSONObject, String str) {
        char c2;
        String str2 = str;
        int i = 8;
        switch (str.hashCode()) {
            case -2012425132:
                if (str2.equals("getDefaultPosition")) {
                    c2 = 18;
                    break;
                }
            case -1757019252:
                if (str2.equals("getCurrentPosition")) {
                    c2 = 17;
                    break;
                }
            case -1554056650:
                if (str2.equals("currentVideoDuration")) {
                    c2 = 7;
                    break;
                }
            case -1263203643:
                if (str2.equals("openUrl")) {
                    c2 = 14;
                    break;
                }
            case -1086137328:
                if (str2.equals("videoCompleted")) {
                    c2 = 3;
                    break;
                }
            case -715147645:
                if (str2.equals("getScreenSize")) {
                    c2 = 16;
                    break;
                }
            case -640720077:
                if (str2.equals("videoPlaying")) {
                    c2 = 4;
                    break;
                }
            case 3529469:
                if (str2.equals("show")) {
                    c2 = 9;
                    break;
                }
            case 94750088:
                if (str2.equals("click")) {
                    c2 = 1;
                    break;
                }
            case 94756344:
                if (str2.equals("close")) {
                    c2 = 2;
                    break;
                }
            case 95458899:
                if (str2.equals("debug")) {
                    c2 = 12;
                    break;
                }
            case 96784904:
                if (str2.equals("error")) {
                    c2 = 10;
                    break;
                }
            case 133423073:
                if (str2.equals("setOrientationProperties")) {
                    c2 = 20;
                    break;
                }
            case 160987616:
                if (str2.equals("getParameters")) {
                    c2 = 0;
                    break;
                }
            case 937504109:
                if (str2.equals("getOrientationProperties")) {
                    c2 = 19;
                    break;
                }
            case 939594121:
                if (str2.equals("videoPaused")) {
                    c2 = 5;
                    break;
                }
            case 1000390722:
                if (str2.equals("videoReplay")) {
                    c2 = 6;
                    break;
                }
            case 1082777163:
                if (str2.equals("totalVideoDuration")) {
                    c2 = 8;
                    break;
                }
            case 1124446108:
                if (str2.equals("warning")) {
                    c2 = 11;
                    break;
                }
            case 1270488759:
                if (str2.equals("tracking")) {
                    c2 = 13;
                    break;
                }
            case 1880941391:
                if (str2.equals("getMaxSize")) {
                    c2 = 15;
                    break;
                }
            default:
                c2 = 65535;
                break;
        }
        switch (c2) {
            case 0:
                StringBuilder sb = new StringBuilder();
                sb.append("JavaScript to native ");
                sb.append(str2);
                sb.append(" callback triggered.");
                Log.d("CBWebChromeClient", sb.toString());
                if (this.g.e != null) {
                    com.chartboost.sdk.Model.a aVar = this.g.e.p;
                    if (aVar != null) {
                        JSONObject a2 = e.a(new com.chartboost.sdk.Libraries.e.a[0]);
                        for (Entry entry : aVar.o.entrySet()) {
                            e.a(a2, (String) entry.getKey(), entry.getValue());
                        }
                        for (Entry entry2 : aVar.n.entrySet()) {
                            b bVar = (b) entry2.getValue();
                            String str3 = (String) entry2.getKey();
                            StringBuilder sb2 = new StringBuilder();
                            sb2.append(bVar.f1829a);
                            sb2.append("/");
                            sb2.append(bVar.b);
                            e.a(a2, str3, sb2.toString());
                        }
                        return a2.toString();
                    }
                }
                return "{}";
            case 1:
                i = 0;
                break;
            case 2:
                i = 1;
                break;
            case 3:
                i = 9;
                break;
            case 4:
                i = 11;
                break;
            case 5:
                i = 10;
                break;
            case 6:
                i = 12;
                break;
            case 7:
                i = 2;
                break;
            case 8:
                i = 7;
                break;
            case 9:
                i = 6;
                break;
            case 10:
                Log.d(bg.class.getName(), "Javascript Error occured");
                i = 4;
                break;
            case 11:
                Log.d(bg.class.getName(), "Javascript warning occurred");
                i = 13;
                break;
            case 12:
                i = 3;
                break;
            case 13:
                break;
            case 14:
                i = 5;
                break;
            case 15:
                StringBuilder sb3 = new StringBuilder();
                sb3.append("JavaScript to native ");
                sb3.append(str2);
                sb3.append(" callback triggered.");
                Log.d("CBWebChromeClient", sb3.toString());
                return this.g.s();
            case 16:
                StringBuilder sb4 = new StringBuilder();
                sb4.append("JavaScript to native ");
                sb4.append(str2);
                sb4.append(" callback triggered.");
                Log.d("CBWebChromeClient", sb4.toString());
                return this.g.t();
            case 17:
                StringBuilder sb5 = new StringBuilder();
                sb5.append("JavaScript to native ");
                sb5.append(str2);
                sb5.append(" callback triggered.");
                Log.d("CBWebChromeClient", sb5.toString());
                return this.g.v();
            case 18:
                StringBuilder sb6 = new StringBuilder();
                sb6.append("JavaScript to native ");
                sb6.append(str2);
                sb6.append(" callback triggered.");
                Log.d("CBWebChromeClient", sb6.toString());
                return this.g.u();
            case 19:
                StringBuilder sb7 = new StringBuilder();
                sb7.append("JavaScript to native ");
                sb7.append(str2);
                sb7.append(" callback triggered.");
                Log.d("CBWebChromeClient", sb7.toString());
                return this.g.p();
            case 20:
                StringBuilder sb8 = new StringBuilder();
                sb8.append("JavaScript to native ");
                sb8.append(str2);
                sb8.append(" callback triggered.");
                Log.d("CBWebChromeClient", sb8.toString());
                i = 14;
                break;
            default:
                StringBuilder sb9 = new StringBuilder();
                sb9.append("JavaScript to native ");
                sb9.append(str2);
                sb9.append(" callback not recognized.");
                Log.e("CBWebChromeClient", sb9.toString());
                return "Function name not recognized.";
        }
        StringBuilder sb10 = new StringBuilder();
        sb10.append("JavaScript to native ");
        sb10.append(str2);
        sb10.append(" callback triggered.");
        Log.d("CBWebChromeClient", sb10.toString());
        bi biVar = new bi(this, this.g, i, str, jSONObject);
        this.h.post(biVar);
        return "Native function successfully called.";
    }

    public void onShowCustomView(View view, CustomViewCallback customViewCallback) {
        if (view instanceof FrameLayout) {
            FrameLayout frameLayout = (FrameLayout) view;
            this.c = true;
            this.d = frameLayout;
            this.e = customViewCallback;
            this.f1892a.setVisibility(4);
            this.b.addView(this.d, new LayoutParams(-1, -1));
            this.b.setVisibility(0);
            if (this.f != null) {
                this.f.a(true);
            }
        }
    }

    public void onShowCustomView(View view, int i, CustomViewCallback customViewCallback) {
        onShowCustomView(view, customViewCallback);
    }

    public void onHideCustomView() {
        if (this.c) {
            this.b.setVisibility(4);
            this.b.removeView(this.d);
            this.f1892a.setVisibility(0);
            if (this.e != null && !this.e.getClass().getName().contains(".chromium.")) {
                this.e.onCustomViewHidden();
            }
            this.c = false;
            this.d = null;
            this.e = null;
            if (this.f != null) {
                this.f.a(false);
            }
        }
    }
}
