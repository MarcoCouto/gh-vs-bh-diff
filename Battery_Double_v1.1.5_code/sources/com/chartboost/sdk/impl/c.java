package com.chartboost.sdk.impl;

import com.chartboost.sdk.Model.CBError.CBImpressionError;
import com.chartboost.sdk.i;
import com.smaato.sdk.core.api.VideoType;

public class c {

    /* renamed from: a reason: collision with root package name */
    public final int f1903a;
    public final String b;
    public final String c;
    public final String d;
    public final String e;
    public final String f;
    public final boolean g;
    public final boolean h;

    public class a implements Runnable {
        private final int b;
        private final String c;
        private final CBImpressionError d;

        public a(int i, String str, CBImpressionError cBImpressionError) {
            this.b = i;
            this.c = str;
            this.d = cBImpressionError;
        }

        public void run() {
            switch (this.b) {
                case 0:
                    c.this.d(this.c);
                    return;
                case 1:
                    c.this.a(this.c);
                    return;
                case 2:
                    c.this.b(this.c);
                    return;
                case 3:
                    c.this.c(this.c);
                    return;
                case 4:
                    c.this.a(this.c, this.d);
                    return;
                case 5:
                    c.this.e(this.c);
                    return;
                default:
                    return;
            }
        }
    }

    private c(int i, String str, String str2, String str3, String str4, String str5, boolean z, boolean z2) {
        this.f1903a = i;
        this.b = str;
        this.c = str2;
        this.d = str3;
        this.e = str4;
        this.f = str5;
        this.g = z;
        this.h = z2;
    }

    public static c a() {
        c cVar = new c(0, VideoType.INTERSTITIAL, VideoType.INTERSTITIAL, "/interstitial/get", "webview/%s/interstitial/get", "/interstitial/show", false, false);
        return cVar;
    }

    public static c b() {
        c cVar = new c(1, "rewarded", "rewarded-video", "/reward/get", "webview/%s/reward/get", "/reward/show", true, false);
        return cVar;
    }

    public static c c() {
        c cVar = new c(2, "inplay", null, "/inplay/get", "no webview endpoint", "/inplay/show", false, true);
        return cVar;
    }

    public String a(int i) {
        String str = "%s-%s";
        Object[] objArr = new Object[2];
        objArr[0] = this.c;
        objArr[1] = i == 1 ? "web" : "native";
        return String.format(str, objArr);
    }

    public void a(String str) {
        if (i.d != null) {
            switch (this.f1903a) {
                case 0:
                    i.d.didClickInterstitial(str);
                    return;
                case 1:
                    i.d.didClickRewardedVideo(str);
                    return;
                default:
                    return;
            }
        }
    }

    public void b(String str) {
        if (i.d != null) {
            switch (this.f1903a) {
                case 0:
                    i.d.didCloseInterstitial(str);
                    return;
                case 1:
                    i.d.didCloseRewardedVideo(str);
                    return;
                default:
                    return;
            }
        }
    }

    public void c(String str) {
        if (i.d != null) {
            switch (this.f1903a) {
                case 0:
                    i.d.didDismissInterstitial(str);
                    return;
                case 1:
                    i.d.didDismissRewardedVideo(str);
                    return;
                default:
                    return;
            }
        }
    }

    public void d(String str) {
        if (i.d != null) {
            switch (this.f1903a) {
                case 0:
                    i.d.didCacheInterstitial(str);
                    return;
                case 1:
                    i.d.didCacheRewardedVideo(str);
                    return;
                case 2:
                    i.d.didCacheInPlay(str);
                    return;
                default:
                    return;
            }
        }
    }

    public void a(String str, CBImpressionError cBImpressionError) {
        if (i.d != null) {
            switch (this.f1903a) {
                case 0:
                    i.d.didFailToLoadInterstitial(str, cBImpressionError);
                    return;
                case 1:
                    i.d.didFailToLoadRewardedVideo(str, cBImpressionError);
                    return;
                case 2:
                    i.d.didFailToLoadInPlay(str, cBImpressionError);
                    return;
                default:
                    return;
            }
        }
    }

    public void e(String str) {
        if (i.d != null) {
            switch (this.f1903a) {
                case 0:
                    i.d.didDisplayInterstitial(str);
                    return;
                case 1:
                    i.d.didDisplayRewardedVideo(str);
                    return;
                default:
                    return;
            }
        }
    }

    public boolean f(String str) {
        if (i.d != null) {
            switch (this.f1903a) {
                case 0:
                    return i.d.shouldDisplayInterstitial(str);
                case 1:
                    return i.d.shouldDisplayRewardedVideo(str);
            }
        }
        return true;
    }

    public boolean g(String str) {
        if (i.d != null) {
            switch (this.f1903a) {
                case 0:
                    return i.d.shouldRequestInterstitial(str);
            }
        }
        return true;
    }
}
