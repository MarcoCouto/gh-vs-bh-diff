package com.chartboost.sdk.impl;

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnBufferingUpdateListener;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.media.MediaPlayer.OnVideoSizeChangedListener;
import android.net.Uri;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;
import com.chartboost.sdk.Libraries.CBLogging;
import com.chartboost.sdk.impl.ax.a;
import com.mintegral.msdk.base.entity.CampaignEx;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Map;

public class av extends SurfaceView implements OnBufferingUpdateListener, OnCompletionListener, OnErrorListener, OnPreparedListener, OnVideoSizeChangedListener, Callback, a {

    /* renamed from: a reason: collision with root package name */
    private final String f1879a = "VideoSurfaceView";
    private Uri b;
    private Map<String, String> c;
    private int d;
    private int e = 0;
    private int f = 0;
    private SurfaceHolder g = null;
    private MediaPlayer h = null;
    private int i;
    private int j;
    private int k;
    private int l;
    private OnCompletionListener m;
    private OnPreparedListener n;
    private int o;
    private OnErrorListener p;
    private int q;

    public void a(int i2, int i3) {
    }

    public av(Context context) {
        super(context);
        f();
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        int defaultSize = getDefaultSize(0, i2);
        int defaultSize2 = getDefaultSize(0, i3);
        if (this.i > 0 && this.j > 0) {
            int min = Math.min(defaultSize2, Math.round((((float) this.j) / ((float) this.i)) * ((float) defaultSize)));
            defaultSize = Math.min(defaultSize, Math.round((((float) this.i) / ((float) this.j)) * ((float) defaultSize2)));
            defaultSize2 = min;
        }
        setMeasuredDimension(defaultSize, defaultSize2);
    }

    private void f() {
        this.i = 0;
        this.j = 0;
        getHolder().addCallback(this);
        getHolder().setType(3);
        setFocusable(true);
        setFocusableInTouchMode(true);
        requestFocus();
        this.e = 0;
        this.f = 0;
    }

    public void a(Uri uri) {
        a(uri, null);
    }

    public void a(Uri uri, Map<String, String> map) {
        this.b = uri;
        this.c = map;
        this.q = 0;
        g();
        requestLayout();
        invalidate();
    }

    private void g() {
        if (this.b != null && this.g != null) {
            Intent intent = new Intent("com.android.music.musicservicecommand");
            intent.putExtra("command", CampaignEx.JSON_NATIVE_VIDEO_PAUSE);
            getContext().sendBroadcast(intent);
            a(false);
            try {
                this.h = new MediaPlayer();
                this.h.setOnPreparedListener(this);
                this.h.setOnVideoSizeChangedListener(this);
                this.d = -1;
                this.h.setOnCompletionListener(this);
                this.h.setOnErrorListener(this);
                this.h.setOnBufferingUpdateListener(this);
                this.o = 0;
                this.h.setDisplay(this.g);
                this.h.setAudioStreamType(3);
                this.h.setScreenOnWhilePlaying(true);
                FileInputStream fileInputStream = new FileInputStream(new File(this.b.toString()));
                this.h.setDataSource(fileInputStream.getFD());
                fileInputStream.close();
                this.h.prepareAsync();
                this.e = 1;
            } catch (IOException e2) {
                StringBuilder sb = new StringBuilder();
                sb.append("Unable to open content: ");
                sb.append(this.b);
                CBLogging.c("VideoSurfaceView", sb.toString(), e2);
                this.e = -1;
                this.f = -1;
                onError(this.h, 1, 0);
            } catch (IllegalArgumentException e3) {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("Unable to open content: ");
                sb2.append(this.b);
                CBLogging.c("VideoSurfaceView", sb2.toString(), e3);
                this.e = -1;
                this.f = -1;
                onError(this.h, 1, 0);
            }
        }
    }

    public void onVideoSizeChanged(MediaPlayer mediaPlayer, int i2, int i3) {
        this.i = mediaPlayer.getVideoWidth();
        this.j = mediaPlayer.getVideoHeight();
        if (this.i != 0 && this.j != 0) {
            getHolder().setFixedSize(this.i, this.j);
        }
    }

    public void onPrepared(MediaPlayer mediaPlayer) {
        this.e = 2;
        this.i = mediaPlayer.getVideoWidth();
        this.j = mediaPlayer.getVideoHeight();
        if (this.n != null) {
            this.n.onPrepared(this.h);
        }
        int i2 = this.q;
        if (i2 != 0) {
            a(i2);
        }
        if (this.i != 0 && this.j != 0) {
            getHolder().setFixedSize(this.i, this.j);
            if (this.k == this.i && this.l == this.j && this.f == 3) {
                a();
            }
        } else if (this.f == 3) {
            a();
        }
    }

    public void onCompletion(MediaPlayer mediaPlayer) {
        this.f = 5;
        if (this.e != 5) {
            this.e = 5;
            if (this.m != null) {
                this.m.onCompletion(this.h);
            }
        }
    }

    public boolean onError(MediaPlayer mediaPlayer, int i2, int i3) {
        StringBuilder sb = new StringBuilder();
        sb.append("Error: ");
        sb.append(i2);
        sb.append(",");
        sb.append(i3);
        CBLogging.a("VideoSurfaceView", sb.toString());
        this.e = -1;
        this.f = -1;
        return (this.p == null || this.p.onError(this.h, i2, i3)) ? true : true;
    }

    public void onBufferingUpdate(MediaPlayer mediaPlayer, int i2) {
        this.o = i2;
    }

    public void a(OnPreparedListener onPreparedListener) {
        this.n = onPreparedListener;
    }

    public void a(OnCompletionListener onCompletionListener) {
        this.m = onCompletionListener;
    }

    public void a(OnErrorListener onErrorListener) {
        this.p = onErrorListener;
    }

    public void surfaceChanged(SurfaceHolder surfaceHolder, int i2, int i3, int i4) {
        this.k = i3;
        this.l = i4;
        boolean z = false;
        boolean z2 = this.f == 3;
        if (this.i == i3 && this.j == i4) {
            z = true;
        }
        if (this.h != null && z2 && z) {
            if (this.q != 0) {
                a(this.q);
            }
            a();
        }
    }

    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        this.g = surfaceHolder;
        g();
    }

    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        this.g = null;
        a(true);
    }

    private void a(boolean z) {
        if (this.h != null) {
            this.h.reset();
            this.h.release();
            this.h = null;
            this.e = 0;
            if (z) {
                this.f = 0;
            }
        }
    }

    public void a() {
        if (h()) {
            this.h.start();
            this.e = 3;
        }
        this.f = 3;
    }

    public void b() {
        if (h() && this.h.isPlaying()) {
            this.h.pause();
            this.e = 4;
        }
        this.f = 4;
    }

    public int c() {
        if (!h()) {
            this.d = -1;
            return this.d;
        } else if (this.d > 0) {
            return this.d;
        } else {
            this.d = this.h.getDuration();
            return this.d;
        }
    }

    public int d() {
        if (h()) {
            return this.h.getCurrentPosition();
        }
        return 0;
    }

    public void a(int i2) {
        if (h()) {
            this.h.seekTo(i2);
            this.q = 0;
            return;
        }
        this.q = i2;
    }

    public boolean e() {
        return h() && this.h.isPlaying();
    }

    private boolean h() {
        return (this.h == null || this.e == -1 || this.e == 0 || this.e == 1) ? false : true;
    }
}
