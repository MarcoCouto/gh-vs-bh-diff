package com.chartboost.sdk.impl;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Point;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import android.os.Handler;
import android.support.v4.view.ViewCompat;
import android.view.MotionEvent;
import android.view.animation.AlphaAnimation;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import com.chartboost.sdk.Libraries.CBUtility;
import com.chartboost.sdk.Libraries.e;
import com.chartboost.sdk.Libraries.h;
import com.chartboost.sdk.g;
import com.chartboost.sdk.impl.x.a;
import com.integralads.avid.library.inmobi.utils.AvidJSONUtil;
import java.util.Locale;
import org.json.JSONObject;

@SuppressLint({"ViewConstructor"})
public class ad extends RelativeLayout implements OnCompletionListener, OnErrorListener, OnPreparedListener {
    private static final CharSequence k = "00:00";

    /* renamed from: a reason: collision with root package name */
    final RelativeLayout f1857a;
    final ac b;
    final ac c;
    final bb d;
    final TextView e;
    final z f;
    final ax g;
    final x h;
    final Handler i;
    final Runnable j = new Runnable() {
        private int b = 0;

        public void run() {
            a q = ad.this.h.e();
            if (q != null) {
                if (ad.this.g.a().e()) {
                    int d = ad.this.g.a().d();
                    if (d > 0) {
                        ad.this.h.v = d;
                        if (((float) ad.this.h.v) / 1000.0f > 0.0f && !ad.this.h.t()) {
                            ad.this.h.r();
                            ad.this.h.a(true);
                        }
                    }
                    float c = ((float) d) / ((float) ad.this.g.a().c());
                    if (ad.this.h.M) {
                        ad.this.f.a(c);
                    }
                    int i = d / 1000;
                    if (this.b != i) {
                        this.b = i;
                        int i2 = i / 60;
                        int i3 = i % 60;
                        ad.this.e.setText(String.format(Locale.US, "%02d:%02d", new Object[]{Integer.valueOf(i2), Integer.valueOf(i3)}));
                    }
                }
                if (q.f()) {
                    bb d2 = q.d(true);
                    if (d2.getVisibility() == 8) {
                        ad.this.h.a(true, d2);
                        d2.setEnabled(true);
                    }
                }
                ad.this.i.removeCallbacks(ad.this.j);
                ad.this.i.postDelayed(ad.this.j, 16);
            }
        }
    };
    private boolean l = false;
    private boolean m = false;
    private final Runnable n = new Runnable() {
        public void run() {
            ad.this.a(false);
        }
    };
    private final Runnable o = new Runnable() {
        public void run() {
            if (ad.this.b != null) {
                ad.this.b.setVisibility(8);
            }
            if (ad.this.h.M) {
                ad.this.f.setVisibility(8);
            }
            ad.this.c.setVisibility(8);
            if (ad.this.d != null) {
                ad.this.d.setEnabled(false);
            }
        }
    };

    public ad(Context context, x xVar) {
        super(context);
        this.h = xVar;
        this.i = xVar.f1841a;
        JSONObject g2 = xVar.g();
        float f2 = context.getResources().getDisplayMetrics().density;
        float f3 = 10.0f * f2;
        int round = Math.round(f3);
        g a2 = g.a();
        this.g = (ax) a2.a(new ax(context));
        LayoutParams layoutParams = new LayoutParams(-1, -2);
        layoutParams.addRule(13);
        addView(this.g, layoutParams);
        this.f1857a = (RelativeLayout) a2.a(new RelativeLayout(context));
        if (g2 == null || g2.isNull("video-click-button")) {
            this.b = null;
            this.d = null;
        } else {
            this.b = (ac) a2.a(new ac(context));
            this.b.setVisibility(8);
            this.d = new bb(context) {
                /* access modifiers changed from: protected */
                public void a(MotionEvent motionEvent) {
                    ad.this.h.b(e.a(e.a(AvidJSONUtil.KEY_X, (Object) Float.valueOf(motionEvent.getX())), e.a(AvidJSONUtil.KEY_Y, (Object) Float.valueOf(motionEvent.getY())), e.a("w", (Object) Integer.valueOf(ad.this.d.getWidth())), e.a("h", (Object) Integer.valueOf(ad.this.d.getHeight()))));
                }
            };
            this.d.a(ScaleType.FIT_CENTER);
            h hVar = xVar.I;
            Point b2 = xVar.b("video-click-button");
            LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-2, -2);
            layoutParams2.leftMargin = Math.round(((float) b2.x) / hVar.e());
            layoutParams2.topMargin = Math.round(((float) b2.y) / hVar.e());
            xVar.a(layoutParams2, hVar, 1.0f);
            this.d.a(hVar);
            this.b.addView(this.d, layoutParams2);
            LayoutParams layoutParams3 = new LayoutParams(-1, Math.round(((float) layoutParams2.height) + f3));
            layoutParams3.addRule(10);
            this.f1857a.addView(this.b, layoutParams3);
        }
        this.c = (ac) a2.a(new ac(context));
        this.c.setVisibility(8);
        LayoutParams layoutParams4 = new LayoutParams(-1, Math.round(f2 * 32.5f));
        layoutParams4.addRule(12);
        this.f1857a.addView(this.c, layoutParams4);
        this.c.setGravity(16);
        this.c.setPadding(round, round, round, round);
        this.e = (TextView) a2.a(new TextView(context));
        this.e.setTextColor(-1);
        this.e.setTextSize(2, 11.0f);
        this.e.setText(k);
        this.e.setPadding(0, 0, round, 0);
        this.e.setSingleLine();
        this.e.measure(0, 0);
        int measuredWidth = this.e.getMeasuredWidth();
        this.e.setGravity(17);
        this.c.addView(this.e, new LinearLayout.LayoutParams(measuredWidth, -1));
        this.f = (z) a2.a(new z(context));
        this.f.setVisibility(8);
        LinearLayout.LayoutParams layoutParams5 = new LinearLayout.LayoutParams(-1, Math.round(f3));
        layoutParams5.setMargins(0, CBUtility.a(1, context), 0, 0);
        this.c.addView(this.f, layoutParams5);
        LayoutParams layoutParams6 = new LayoutParams(-1, -1);
        layoutParams6.addRule(6, this.g.getId());
        layoutParams6.addRule(8, this.g.getId());
        layoutParams6.addRule(5, this.g.getId());
        layoutParams6.addRule(7, this.g.getId());
        addView(this.f1857a, layoutParams6);
        a();
    }

    public void setEnabled(boolean z) {
        super.setEnabled(z);
        if (this.d != null) {
            this.d.setEnabled(z);
        }
        if (z) {
            b(false);
        }
    }

    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.i.removeCallbacks(this.j);
    }

    @SuppressLint({"ClickableViewAccessibility"})
    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (!this.g.a().e() || motionEvent.getActionMasked() != 0) {
            return false;
        }
        if (this.h != null) {
            a(true);
        }
        return true;
    }

    public void onCompletion(MediaPlayer mediaPlayer) {
        this.h.v = this.g.a().c();
        if (this.h.e() != null) {
            this.h.e().e();
        }
    }

    public void onPrepared(MediaPlayer mediaPlayer) {
        this.h.w = this.g.a().c();
        this.h.e().a(true);
    }

    public boolean onError(MediaPlayer mediaPlayer, int i2, int i3) {
        this.h.v();
        return false;
    }

    /* access modifiers changed from: 0000 */
    public void a(boolean z) {
        a(!this.l, z);
    }

    /* access modifiers changed from: protected */
    public void a(boolean z, boolean z2) {
        this.i.removeCallbacks(this.n);
        this.i.removeCallbacks(this.o);
        if (this.h.y && this.h.p() && z != this.l) {
            this.l = z;
            AlphaAnimation alphaAnimation = this.l ? new AlphaAnimation(0.0f, 1.0f) : new AlphaAnimation(1.0f, 0.0f);
            alphaAnimation.setDuration(z2 ? 100 : 200);
            alphaAnimation.setFillAfter(true);
            if (!this.m && this.b != null) {
                this.b.setVisibility(0);
                this.b.startAnimation(alphaAnimation);
                if (this.d != null) {
                    this.d.setEnabled(true);
                }
            }
            if (this.h.M) {
                this.f.setVisibility(0);
            }
            this.c.setVisibility(0);
            this.c.startAnimation(alphaAnimation);
            if (this.l) {
                this.i.postDelayed(this.n, 3000);
            } else {
                this.i.postDelayed(this.o, alphaAnimation.getDuration());
            }
        }
    }

    public void b(boolean z) {
        this.i.removeCallbacks(this.n);
        this.i.removeCallbacks(this.o);
        if (z) {
            if (!this.m && this.b != null) {
                this.b.setVisibility(0);
            }
            if (this.h.M) {
                this.f.setVisibility(0);
            }
            this.c.setVisibility(0);
            if (this.d != null) {
                this.d.setEnabled(true);
            }
        } else {
            if (this.b != null) {
                this.b.clearAnimation();
                this.b.setVisibility(8);
            }
            this.c.clearAnimation();
            if (this.h.M) {
                this.f.setVisibility(8);
            }
            this.c.setVisibility(8);
            if (this.d != null) {
                this.d.setEnabled(false);
            }
        }
        this.l = z;
    }

    public void c(boolean z) {
        setBackgroundColor(z ? ViewCompat.MEASURED_STATE_MASK : 0);
        LayoutParams layoutParams = new LayoutParams(-1, -1);
        if (!z) {
            layoutParams.addRule(6, this.g.getId());
            layoutParams.addRule(8, this.g.getId());
            layoutParams.addRule(5, this.g.getId());
            layoutParams.addRule(7, this.g.getId());
        }
        this.f1857a.setLayoutParams(layoutParams);
        if (this.b != null) {
            this.b.setGravity(8388627);
            this.b.requestLayout();
        }
    }

    public void a() {
        c(CBUtility.a(CBUtility.a()));
    }

    public ax.a b() {
        return this.g.a();
    }

    public z c() {
        return this.f;
    }

    public void a(int i2) {
        if (this.b != null) {
            this.b.setBackgroundColor(i2);
        }
        this.c.setBackgroundColor(i2);
    }

    public void d() {
        if (this.b != null) {
            this.b.setVisibility(8);
        }
        this.m = true;
        if (this.d != null) {
            this.d.setEnabled(false);
        }
    }

    public void d(boolean z) {
        this.e.setVisibility(z ? 0 : 8);
    }

    public void a(String str) {
        this.g.a().a((OnCompletionListener) this);
        this.g.a().a((OnErrorListener) this);
        this.g.a().a((OnPreparedListener) this);
        this.g.a().a(Uri.parse(str));
    }

    public void e() {
        this.i.postDelayed(new Runnable() {
            public void run() {
                ad.this.g.setVisibility(0);
            }
        }, 500);
        this.g.a().a();
        this.i.removeCallbacks(this.j);
        this.i.postDelayed(this.j, 16);
    }

    public void f() {
        if (this.g.a().e()) {
            this.h.v = this.g.a().d();
            this.g.a().b();
        }
        if (this.h.e().e.getVisibility() == 0) {
            this.h.e().e.postInvalidate();
        }
        this.i.removeCallbacks(this.j);
    }

    public void g() {
        if (this.g.a().e()) {
            this.h.v = this.g.a().d();
        }
        this.g.a().b();
        this.i.removeCallbacks(this.j);
    }

    public void h() {
        this.g.setVisibility(8);
        invalidate();
    }
}
