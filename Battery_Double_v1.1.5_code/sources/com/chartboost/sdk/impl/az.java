package com.chartboost.sdk.impl;

import android.content.Context;
import android.view.View;
import com.chartboost.sdk.Model.a;

public final class az extends View {

    /* renamed from: a reason: collision with root package name */
    private boolean f1884a = false;

    public az(Context context) {
        super(context);
        setFocusable(false);
        setBackgroundColor(-1442840576);
    }

    public void a(ay ayVar, a aVar) {
        if (!this.f1884a) {
            ayVar.a(true, (View) this, aVar);
            this.f1884a = true;
        }
    }
}
