package com.chartboost.sdk.impl;

import android.content.Context;
import android.content.Intent;
import android.graphics.Matrix;
import android.graphics.SurfaceTexture;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnBufferingUpdateListener;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.media.MediaPlayer.OnVideoSizeChangedListener;
import android.net.Uri;
import android.view.Surface;
import android.view.TextureView;
import android.view.TextureView.SurfaceTextureListener;
import com.chartboost.sdk.Libraries.CBLogging;
import com.chartboost.sdk.impl.ax.a;
import com.mintegral.msdk.base.entity.CampaignEx;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Map;

public class aw extends TextureView implements OnBufferingUpdateListener, OnCompletionListener, OnErrorListener, OnPreparedListener, OnVideoSizeChangedListener, SurfaceTextureListener, a {

    /* renamed from: a reason: collision with root package name */
    private final String f1880a = "VideoTextureView";
    private Uri b;
    private Map<String, String> c;
    private int d;
    private int e = 0;
    private int f = 0;
    private Surface g = null;
    private MediaPlayer h = null;
    private int i;
    private int j;
    private OnCompletionListener k;
    private OnPreparedListener l;
    private int m;
    private OnErrorListener n;
    private int o;

    public void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {
    }

    public aw(Context context) {
        super(context);
        f();
    }

    private void f() {
        this.i = 0;
        this.j = 0;
        setSurfaceTextureListener(this);
        setFocusable(true);
        setFocusableInTouchMode(true);
        requestFocus();
        this.e = 0;
        this.f = 0;
    }

    public void a(Uri uri) {
        a(uri, null);
    }

    public void a(int i2, int i3) {
        if (this.i != 0 && this.j != 0 && i2 != 0 && i3 != 0) {
            float f2 = (float) i2;
            float f3 = (float) i3;
            float min = Math.min(f2 / ((float) this.i), f3 / ((float) this.j));
            float f4 = ((float) this.i) * min;
            float f5 = min * ((float) this.j);
            Matrix matrix = new Matrix();
            matrix.setScale(f4 / f2, f5 / f3, f2 / 2.0f, f3 / 2.0f);
            setTransform(matrix);
        }
    }

    public void a(Uri uri, Map<String, String> map) {
        this.b = uri;
        this.c = map;
        this.o = 0;
        g();
        requestLayout();
        invalidate();
    }

    private void g() {
        if (this.b != null && this.g != null) {
            Intent intent = new Intent("com.android.music.musicservicecommand");
            intent.putExtra("command", CampaignEx.JSON_NATIVE_VIDEO_PAUSE);
            getContext().sendBroadcast(intent);
            a(false);
            h();
            try {
                this.h = new MediaPlayer();
                this.h.setOnPreparedListener(this);
                this.h.setOnVideoSizeChangedListener(this);
                this.d = -1;
                this.h.setOnCompletionListener(this);
                this.h.setOnErrorListener(this);
                this.h.setOnBufferingUpdateListener(this);
                this.m = 0;
                FileInputStream fileInputStream = new FileInputStream(new File(this.b.toString()));
                this.h.setDataSource(fileInputStream.getFD());
                fileInputStream.close();
                this.h.setSurface(this.g);
                this.h.setAudioStreamType(3);
                this.h.setScreenOnWhilePlaying(true);
                this.h.prepareAsync();
                this.e = 1;
            } catch (IOException e2) {
                StringBuilder sb = new StringBuilder();
                sb.append("Unable to open content: ");
                sb.append(this.b);
                CBLogging.c("VideoTextureView", sb.toString(), e2);
                this.e = -1;
                this.f = -1;
                onError(this.h, 1, 0);
            } catch (IllegalArgumentException e3) {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("Unable to open content: ");
                sb2.append(this.b);
                CBLogging.c("VideoTextureView", sb2.toString(), e3);
                this.e = -1;
                this.f = -1;
                onError(this.h, 1, 0);
            }
        }
    }

    private void h() {
        try {
            MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();
            if (s.a().a(16)) {
                mediaMetadataRetriever.setDataSource(this.b.toString());
            } else {
                FileInputStream fileInputStream = new FileInputStream(this.b.toString());
                mediaMetadataRetriever.setDataSource(fileInputStream.getFD());
                fileInputStream.close();
            }
            String extractMetadata = mediaMetadataRetriever.extractMetadata(19);
            String extractMetadata2 = mediaMetadataRetriever.extractMetadata(18);
            this.j = Integer.parseInt(extractMetadata);
            this.i = Integer.parseInt(extractMetadata2);
        } catch (Exception e2) {
            CBLogging.c("play video", "read size error", e2);
        }
    }

    public void onVideoSizeChanged(MediaPlayer mediaPlayer, int i2, int i3) {
        this.i = mediaPlayer.getVideoWidth();
        this.j = mediaPlayer.getVideoHeight();
        if (this.i != 0 && this.j != 0) {
            a(getWidth(), getHeight());
        }
    }

    public void onPrepared(MediaPlayer mediaPlayer) {
        this.e = 2;
        this.i = mediaPlayer.getVideoWidth();
        this.j = mediaPlayer.getVideoHeight();
        if (this.l != null) {
            this.l.onPrepared(this.h);
        }
        int i2 = this.o;
        if (i2 != 0) {
            a(i2);
        }
        if (this.f == 3) {
            a();
        }
    }

    public void onCompletion(MediaPlayer mediaPlayer) {
        this.f = 5;
        if (this.e != 5) {
            this.e = 5;
            if (this.k != null) {
                this.k.onCompletion(this.h);
            }
        }
    }

    public boolean onError(MediaPlayer mediaPlayer, int i2, int i3) {
        StringBuilder sb = new StringBuilder();
        sb.append("Error: ");
        sb.append(i2);
        sb.append(",");
        sb.append(i3);
        CBLogging.a("VideoTextureView", sb.toString());
        if (i2 == 100) {
            g();
            return true;
        }
        this.e = -1;
        this.f = -1;
        return (this.n == null || this.n.onError(this.h, i2, i3)) ? true : true;
    }

    public void onBufferingUpdate(MediaPlayer mediaPlayer, int i2) {
        this.m = i2;
    }

    public void a(OnPreparedListener onPreparedListener) {
        this.l = onPreparedListener;
    }

    public void a(OnCompletionListener onCompletionListener) {
        this.k = onCompletionListener;
    }

    public void a(OnErrorListener onErrorListener) {
        this.n = onErrorListener;
    }

    private void a(boolean z) {
        if (this.h != null) {
            this.h.reset();
            this.h.release();
            this.h = null;
            this.e = 0;
            if (z) {
                this.f = 0;
            }
        }
    }

    public void a() {
        if (i()) {
            this.h.start();
            this.e = 3;
        }
        this.f = 3;
    }

    public void b() {
        if (i() && this.h.isPlaying()) {
            this.h.pause();
            this.e = 4;
        }
        this.f = 4;
    }

    public int c() {
        if (!i()) {
            this.d = -1;
            return this.d;
        } else if (this.d > 0) {
            return this.d;
        } else {
            this.d = this.h.getDuration();
            return this.d;
        }
    }

    public int d() {
        if (i()) {
            return this.h.getCurrentPosition();
        }
        return 0;
    }

    public void a(int i2) {
        if (i()) {
            this.h.seekTo(i2);
            this.o = 0;
            return;
        }
        this.o = i2;
    }

    public boolean e() {
        return i() && this.h.isPlaying();
    }

    private boolean i() {
        return (this.h == null || this.e == -1 || this.e == 0 || this.e == 1) ? false : true;
    }

    public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int i2, int i3) {
        this.g = new Surface(surfaceTexture);
        g();
    }

    public boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {
        this.g = null;
        a(true);
        return true;
    }

    public void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int i2, int i3) {
        boolean z = this.f == 3;
        if (this.h != null && z) {
            if (this.o != 0) {
                a(this.o);
            }
            a();
        }
    }
}
