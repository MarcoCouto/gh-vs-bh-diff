package com.chartboost.sdk.impl;

import android.os.Handler;
import com.chartboost.sdk.Libraries.i;
import com.chartboost.sdk.Model.CBError;
import com.chartboost.sdk.Tracking.a;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.io.BufferedInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.util.Map;
import java.util.concurrent.Executor;

public class ap<T> implements Comparable<ap>, Runnable {

    /* renamed from: a reason: collision with root package name */
    public final af<T> f1876a;
    private final Executor b;
    private final aq c;
    private final ak d;
    private final i e;
    private final Handler f;
    private ah<T> g;
    private ai h;

    private static boolean a(int i) {
        return ((100 <= i && i < 200) || i == 204 || i == 304) ? false : true;
    }

    ap(Executor executor, aq aqVar, ak akVar, i iVar, Handler handler, af<T> afVar) {
        this.b = executor;
        this.c = aqVar;
        this.d = akVar;
        this.e = iVar;
        this.f = handler;
        this.f1876a = afVar;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:26:0x00a8, code lost:
        r7.b.execute(r7);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x00ae, code lost:
        r7.f.post(r7);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x00da, code lost:
        return;
     */
    public void run() {
        if (this.g != null) {
            try {
                if (this.g.b == null) {
                    this.f1876a.a(this.g.f1865a, this.h);
                } else {
                    this.f1876a.a(this.g.b, this.h);
                }
            } catch (Exception e2) {
                a.a(getClass(), "deliver result", e2);
            }
        } else if (this.f1876a.e.compareAndSet(0, 1)) {
            long b2 = this.e.b();
            try {
                if (this.d.c()) {
                    this.h = a(this.f1876a);
                    int i = this.h.f1866a;
                    if (i < 200 || i >= 300) {
                        CBError.a aVar = CBError.a.NETWORK_FAILURE;
                        StringBuilder sb = new StringBuilder();
                        sb.append("Failure due to HTTP status code ");
                        sb.append(i);
                        this.g = ah.a(new CBError(aVar, sb.toString()));
                    } else {
                        this.g = this.f1876a.a(this.h);
                    }
                } else {
                    this.g = ah.a(new CBError(CBError.a.INTERNET_UNAVAILABLE, "Internet Unavailable"));
                }
                this.f1876a.g = this.e.b() - b2;
                switch (this.f1876a.j) {
                    case 0:
                        break;
                    case 1:
                        break;
                }
            } catch (Throwable th) {
                this.f1876a.g = this.e.b() - b2;
                switch (this.f1876a.j) {
                    case 0:
                        this.f.post(this);
                        break;
                    case 1:
                        this.b.execute(this);
                        break;
                }
                throw th;
            }
        }
    }

    private ai a(af<T> afVar) throws IOException {
        int i = 10000;
        int i2 = 0;
        while (true) {
            try {
                return a(afVar, i);
            } catch (SocketTimeoutException e2) {
                if (i2 < 1) {
                    i *= 2;
                    i2++;
                } else {
                    throw e2;
                }
            }
        }
    }

    /* JADX WARNING: type inference failed for: r4v1, types: [java.io.InputStream] */
    /* JADX WARNING: type inference failed for: r4v4, types: [java.io.FileOutputStream] */
    /* JADX WARNING: type inference failed for: r1v6, types: [java.io.InputStream] */
    /* JADX WARNING: type inference failed for: r1v7 */
    /* JADX WARNING: type inference failed for: r4v5 */
    /* JADX WARNING: type inference failed for: r4v10 */
    /* JADX WARNING: type inference failed for: r4v11 */
    /* JADX WARNING: type inference failed for: r1v19 */
    /* JADX WARNING: type inference failed for: r1v20 */
    /* JADX WARNING: Can't wrap try/catch for region: R(2:82|83) */
    /* JADX WARNING: Can't wrap try/catch for region: R(4:81|(2:91|92)|93|94) */
    /* JADX WARNING: Can't wrap try/catch for region: R(5:22|23|(2:26|27)|28|29) */
    /* JADX WARNING: Can't wrap try/catch for region: R(6:64|65|(0)|(0)|76|77) */
    /* JADX WARNING: Can't wrap try/catch for region: R(7:38|39|(2:41|(13:43|44|45|46|47|48|49|(2:51|52)|53|54|55|56|(2:58|(2:60|61)(2:62|63)))(5:78|79|80|(1:85)(1:86)|(2:88|89)))(1:95)|96|97|98|99) */
    /* JADX WARNING: Can't wrap try/catch for region: R(8:48|49|(2:51|52)|53|54|55|56|(2:58|(2:60|61)(2:62|63))) */
    /* JADX WARNING: Code restructure failed: missing block: B:83:?, code lost:
        r0 = r2.getErrorStream();
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:28:0x0080 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:53:0x00d8 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:55:0x00db */
    /* JADX WARNING: Missing exception handler attribute for start block: B:76:0x014c */
    /* JADX WARNING: Missing exception handler attribute for start block: B:82:0x0155 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:93:0x0173 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:96:0x0176 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x007d A[SYNTHETIC, Splitter:B:26:0x007d] */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x00e3 A[Catch:{ all -> 0x0153, all -> 0x0189 }] */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x0144 A[SYNTHETIC, Splitter:B:70:0x0144] */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x0149 A[SYNTHETIC, Splitter:B:74:0x0149] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:28:0x0080=Splitter:B:28:0x0080, B:96:0x0176=Splitter:B:96:0x0176} */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:93:0x0173=Splitter:B:93:0x0173, B:55:0x00db=Splitter:B:55:0x00db, B:76:0x014c=Splitter:B:76:0x014c} */
    /* JADX WARNING: Unknown variable types count: 3 */
    private ai a(af<T> afVar, int i) throws IOException {
        long b2;
        byte[] bArr;
        ? r4;
        ? r1;
        DataOutputStream dataOutputStream;
        ag a2 = afVar.a();
        Map<String, String> map = a2.f1864a;
        HttpURLConnection a3 = this.c.a(afVar);
        a3.setConnectTimeout(i);
        a3.setReadTimeout(i);
        a3.setUseCaches(false);
        a3.setDoInput(true);
        if (map != null) {
            try {
                for (String str : map.keySet()) {
                    a3.addRequestProperty(str, (String) map.get(str));
                }
            } catch (Throwable th) {
                a3.disconnect();
                throw th;
            }
        }
        a3.setRequestMethod(afVar.b);
        ? r42 = 0;
        if (afVar.b.equals(HttpRequest.METHOD_POST) && a2.b != null) {
            a3.setDoOutput(true);
            a3.setFixedLengthStreamingMode(a2.b.length);
            if (a2.c != null) {
                a3.addRequestProperty("Content-Type", a2.c);
            }
            try {
                DataOutputStream dataOutputStream2 = new DataOutputStream(a3.getOutputStream());
                try {
                    dataOutputStream2.write(a2.b);
                    try {
                        dataOutputStream2.close();
                    } catch (IOException unused) {
                    }
                } catch (Throwable th2) {
                    th = th2;
                    dataOutputStream = dataOutputStream2;
                    if (dataOutputStream != 0) {
                        dataOutputStream.close();
                    }
                    throw th;
                }
            } catch (Throwable th3) {
                th = th3;
                dataOutputStream = r42;
                if (dataOutputStream != 0) {
                }
                throw th;
            }
        }
        b2 = this.e.b();
        int responseCode = a3.getResponseCode();
        long b3 = this.e.b();
        afVar.h = b3 - b2;
        if (responseCode != -1) {
            try {
                if (!a(responseCode)) {
                    bArr = new byte[0];
                } else if (afVar.f != null) {
                    File parentFile = afVar.f.getParentFile();
                    StringBuilder sb = new StringBuilder();
                    sb.append(afVar.f.getName());
                    sb.append(".tmp");
                    File file = new File(parentFile, sb.toString());
                    bArr = new byte[0];
                    try {
                        InputStream inputStream = a3.getInputStream();
                        try {
                            FileOutputStream fileOutputStream = new FileOutputStream(file);
                            try {
                                bk.a(inputStream, (OutputStream) fileOutputStream);
                                if (inputStream != 0) {
                                    inputStream.close();
                                }
                                fileOutputStream.close();
                                if (!file.renameTo(afVar.f)) {
                                    if (!file.delete()) {
                                        StringBuilder sb2 = new StringBuilder();
                                        sb2.append("Unable to delete ");
                                        sb2.append(file.getAbsolutePath());
                                        sb2.append(" after failing to rename to ");
                                        sb2.append(afVar.f.getAbsolutePath());
                                        throw new IOException(sb2.toString());
                                    }
                                    StringBuilder sb3 = new StringBuilder();
                                    sb3.append("Unable to move ");
                                    sb3.append(file.getAbsolutePath());
                                    sb3.append(" to ");
                                    sb3.append(afVar.f.getAbsolutePath());
                                    throw new IOException(sb3.toString());
                                }
                            } catch (Throwable th4) {
                                th = th4;
                                r4 = fileOutputStream;
                                r1 = inputStream;
                                if (r1 != 0) {
                                }
                                if (r4 != 0) {
                                }
                                throw th;
                            }
                        } catch (Throwable th5) {
                            th = th5;
                            r4 = r42;
                            r1 = inputStream;
                            if (r1 != 0) {
                            }
                            if (r4 != 0) {
                            }
                            throw th;
                        }
                    } catch (Throwable th6) {
                        th = th6;
                        r1 = r42;
                        r4 = r42;
                        if (r1 != 0) {
                            try {
                                r1.close();
                            } catch (IOException unused2) {
                            }
                        }
                        if (r4 != 0) {
                            r4.close();
                        }
                        throw th;
                    }
                } else {
                    InputStream errorStream = a3.getInputStream();
                    InputStream inputStream2 = errorStream;
                    if (inputStream2 != null) {
                        bArr = bk.b(new BufferedInputStream(inputStream2));
                    } else {
                        bArr = new byte[0];
                    }
                    if (inputStream2 != null) {
                        inputStream2.close();
                    }
                }
                afVar.i = this.e.b() - b3;
                ai aiVar = new ai(responseCode, bArr);
                a3.disconnect();
                return aiVar;
            } catch (Throwable th7) {
                afVar.i = this.e.b() - b3;
                throw th7;
            }
        } else {
            throw new IOException("Could not retrieve response code from HttpUrlConnection.");
        }
    }

    /* renamed from: a */
    public int compareTo(ap apVar) {
        return this.f1876a.d - apVar.f1876a.d;
    }
}
