package com.chartboost.sdk.impl;

import com.chartboost.sdk.Model.CBError;

public class ah<T> {

    /* renamed from: a reason: collision with root package name */
    public final T f1865a;
    public final CBError b;

    public static <T> ah<T> a(T t) {
        return new ah<>(t, null);
    }

    public static <T> ah<T> a(CBError cBError) {
        return new ah<>(null, cBError);
    }

    private ah(T t, CBError cBError) {
        this.f1865a = t;
        this.b = cBError;
    }
}
