package com.chartboost.sdk.impl;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.widget.ImageView;
import android.widget.TextView;
import com.chartboost.sdk.Libraries.h;

public class ba extends ImageView {

    /* renamed from: a reason: collision with root package name */
    protected TextView f1885a = null;
    private h b = null;

    public ba(Context context) {
        super(context);
    }

    public void a(h hVar) {
        if (hVar != null && hVar.c() && this.b != hVar) {
            this.b = hVar;
            setImageDrawable(new BitmapDrawable(hVar.d()));
        }
    }

    public void setImageBitmap(Bitmap bitmap) {
        this.b = null;
        setImageDrawable(new BitmapDrawable(bitmap));
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        a(canvas);
    }

    /* access modifiers changed from: protected */
    public void a(Canvas canvas) {
        if (this.f1885a != null) {
            this.f1885a.layout(0, 0, canvas.getWidth(), canvas.getHeight());
            this.f1885a.setEnabled(isEnabled());
            this.f1885a.setSelected(isSelected());
            if (isFocused()) {
                this.f1885a.requestFocus();
            } else {
                this.f1885a.clearFocus();
            }
            this.f1885a.setPressed(isPressed());
            this.f1885a.draw(canvas);
        }
    }
}
