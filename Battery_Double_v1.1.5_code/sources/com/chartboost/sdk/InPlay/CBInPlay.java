package com.chartboost.sdk.InPlay;

import android.graphics.Bitmap;
import android.os.Handler;
import com.chartboost.sdk.Libraries.CBLogging;
import com.chartboost.sdk.Model.CBError.CBClickError;
import com.chartboost.sdk.Model.CBError.CBImpressionError;
import com.chartboost.sdk.Tracking.a;
import com.chartboost.sdk.b;
import com.chartboost.sdk.h;
import com.chartboost.sdk.impl.aj;
import com.chartboost.sdk.impl.al;
import com.chartboost.sdk.impl.am;
import com.chartboost.sdk.impl.ar;
import com.chartboost.sdk.impl.bj;
import com.chartboost.sdk.impl.c;
import com.chartboost.sdk.impl.e;
import com.chartboost.sdk.impl.g;
import com.chartboost.sdk.impl.s;
import java.io.File;
import java.util.concurrent.Executor;

public final class CBInPlay {

    /* renamed from: a reason: collision with root package name */
    private final e f1811a;
    public final String appName;
    private final aj b;
    private final ar c;
    private final a d;
    private final am e;
    private final com.chartboost.sdk.Model.a f;
    private Bitmap g;
    public final File largeAppIconFile;
    public final String largeAppIconUrl;
    public final String location;

    private CBInPlay(e eVar, aj ajVar, ar arVar, a aVar, am amVar, com.chartboost.sdk.Model.a aVar2, String str, File file, String str2) {
        this.f1811a = eVar;
        this.b = ajVar;
        this.c = arVar;
        this.d = aVar;
        this.e = amVar;
        this.f = aVar2;
        this.appName = aVar2.B;
        this.largeAppIconUrl = str;
        this.largeAppIconFile = file;
        this.location = str2;
    }

    public void show() {
        al alVar = new al("/inplay/show", this.c, this.d, 2, new g(this.f1811a, this.location));
        alVar.j = 1;
        alVar.a("inplay-dictionary", (Object) this.f.l);
        alVar.a("location", (Object) this.location);
        this.b.a(alVar);
    }

    public void click() {
        String str = this.f.u;
        String str2 = this.f.t;
        String str3 = (str2.isEmpty() || !this.e.a(str2)) ? str : str2;
        al alVar = new al("/api/click", this.c, this.d, 2, null);
        alVar.a("location", (Object) this.location);
        alVar.a("to", (Object) this.f.x);
        alVar.a("cgn", (Object) this.f.r);
        alVar.a("creative", (Object) this.f.s);
        alVar.a("ad_id", (Object) this.f.q);
        alVar.a("type", (Object) "native");
        if (str3 == null || str3.isEmpty()) {
            this.e.a(null, false, str3, CBClickError.URI_INVALID, alVar);
        } else {
            this.e.a(null, str3, alVar);
        }
    }

    public String getLocation() {
        return this.location;
    }

    public Bitmap getAppIcon() throws Exception {
        if (this.g == null) {
            try {
                byte[] b2 = bj.b(this.largeAppIconFile);
                if (b2 != null) {
                    this.g = s.a().a(b2);
                }
                if (this.g == null) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("Error decoding inplay bitmap ");
                    sb.append(this.largeAppIconFile.getAbsolutePath());
                    CBLogging.b("CBInPlay", sb.toString());
                    if (!this.largeAppIconFile.delete()) {
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append("Unable to delete corrupt inplay bitmap ");
                        sb2.append(this.largeAppIconFile.getAbsolutePath());
                        CBLogging.b("CBInPlay", sb2.toString());
                    }
                    throw new Exception("decodeByteArrayToBitmap returned null");
                }
            } catch (OutOfMemoryError e2) {
                StringBuilder sb3 = new StringBuilder();
                sb3.append("Out of memory decoding inplay bitmap ");
                sb3.append(this.largeAppIconFile.getAbsolutePath());
                CBLogging.b("CBInPlay", sb3.toString());
                throw new Exception(e2);
            }
        }
        return this.g;
    }

    public String getAppName() {
        return this.appName;
    }

    public static void cacheInPlay(String str) {
        h a2 = h.a();
        if (a2 != null && b.a()) {
            if (s.a().a((CharSequence) str)) {
                CBLogging.b("CBInPlay", "cacheInPlay location cannot be empty");
                Handler handler = a2.p;
                c cVar = a2.e;
                cVar.getClass();
                handler.post(new c.a(4, str, CBImpressionError.INVALID_LOCATION));
                return;
            }
            com.chartboost.sdk.Model.e eVar = (com.chartboost.sdk.Model.e) a2.m.get();
            if ((!eVar.y || !eVar.z) && (!eVar.e || !eVar.f)) {
                Handler handler2 = a2.p;
                c cVar2 = a2.e;
                cVar2.getClass();
                handler2.post(new c.a(4, str, CBImpressionError.END_POINT_DISABLED));
            } else {
                e eVar2 = a2.d;
                eVar2.getClass();
                e.a aVar = new e.a(3, str, null, null);
                a2.f1847a.execute(aVar);
            }
        }
    }

    public static boolean hasInPlay(String str) {
        h a2 = h.a();
        boolean z = false;
        if (a2 == null || !b.a()) {
            return false;
        }
        if (s.a().a((CharSequence) str)) {
            CBLogging.b("CBInPlay", "hasInPlay location cannot be empty");
            Handler handler = a2.p;
            c cVar = a2.e;
            cVar.getClass();
            handler.post(new c.a(4, str, CBImpressionError.INVALID_LOCATION));
            return false;
        }
        if (a2.d.a(str) != null) {
            z = true;
        }
        return z;
    }

    public static CBInPlay getInPlay(String str) {
        h a2 = h.a();
        CBInPlay cBInPlay = null;
        if (a2 == null || !b.a()) {
            return null;
        }
        if (s.a().a((CharSequence) str)) {
            CBLogging.b("CBInPlay", "Inplay location cannot be empty");
            Handler handler = a2.p;
            c cVar = a2.e;
            cVar.getClass();
            handler.post(new c.a(4, str, CBImpressionError.INVALID_LOCATION));
            return null;
        }
        com.chartboost.sdk.Model.e eVar = (com.chartboost.sdk.Model.e) a2.m.get();
        CBImpressionError cBImpressionError = CBImpressionError.NO_AD_FOUND;
        if ((eVar.y && eVar.z) || (eVar.e && eVar.f)) {
            com.chartboost.sdk.Model.a a3 = a2.d.a(str);
            if (a3 != null) {
                com.chartboost.sdk.Model.b bVar = (com.chartboost.sdk.Model.b) a3.n.get("lg");
                if (bVar != null) {
                    File a4 = bVar.a(a2.d.b.d().f1820a);
                    if (!a4.exists()) {
                        cBImpressionError = CBImpressionError.ASSET_MISSING;
                    } else {
                        cBInPlay = new CBInPlay(a2.d, a2.h, a2.j, a2.o, a2.r, a3, bVar.c, a4, str);
                    }
                }
                Executor executor = a2.f1847a;
                e eVar2 = a2.d;
                eVar2.getClass();
                e.a aVar = new e.a(8, str, null, null);
                executor.execute(aVar);
            }
            if (cBInPlay == null) {
                Executor executor2 = a2.f1847a;
                e eVar3 = a2.d;
                eVar3.getClass();
                e.a aVar2 = new e.a(3, str, null, null);
                executor2.execute(aVar2);
            }
        }
        if (cBInPlay == null) {
            Handler handler2 = a2.p;
            c cVar2 = a2.e;
            cVar2.getClass();
            handler2.post(new c.a(4, str, cBImpressionError));
        }
        return cBInPlay;
    }
}
