package com.chartboost.sdk;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Application.ActivityLifecycleCallbacks;
import android.content.Context;
import android.os.Build.VERSION;
import android.os.Handler;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import com.adcolony.sdk.AdColonyAppOptions;
import com.chartboost.sdk.Libraries.CBLogging;
import com.chartboost.sdk.Libraries.CBLogging.Level;
import com.chartboost.sdk.Libraries.b;
import com.chartboost.sdk.Model.CBError.CBImpressionError;
import com.chartboost.sdk.Model.e;
import com.chartboost.sdk.c.C0019c;
import com.chartboost.sdk.impl.as;
import com.chartboost.sdk.impl.c;
import com.chartboost.sdk.impl.c.a;
import com.chartboost.sdk.impl.s;
import com.chartboost.sdk.impl.u;
import java.util.HashMap;
import java.util.Map;

public class Chartboost {

    public enum CBFramework {
        CBFrameworkUnity(AdColonyAppOptions.UNITY),
        CBFrameworkCorona(AdColonyAppOptions.CORONA),
        CBFrameworkAir("AIR"),
        CBFrameworkGameSalad("GameSalad"),
        CBFrameworkCordova("Cordova"),
        CBFrameworkCocoonJS("CocoonJS"),
        CBFrameworkCocos2dx("Cocos2dx"),
        CBFrameworkPrime31Unreal("Prime31Unreal"),
        CBFrameworkWeeby("Weeby"),
        CBFrameworkOther("Other");
        

        /* renamed from: a reason: collision with root package name */
        private final String f1808a;

        private CBFramework(String str) {
            this.f1808a = str;
        }

        public String toString() {
            return this.f1808a;
        }

        public boolean doesWrapperUseCustomShouldDisplayBehavior() {
            return this == CBFrameworkAir || this == CBFrameworkCocos2dx;
        }

        public boolean doesWrapperUseCustomBackgroundingBehavior() {
            return this == CBFrameworkAir;
        }
    }

    public enum CBMediation {
        CBMediationAdMarvel(AdColonyAppOptions.ADMARVEL),
        CBMediationAdMob(AdColonyAppOptions.ADMOB),
        CBMediationFuse("Fuse"),
        CBMediationFyber(AdColonyAppOptions.FYBER),
        CBMediationHeyZap("HeyZap"),
        CBMediationMoPub(AdColonyAppOptions.MOPUB),
        CBMediationironSource(AdColonyAppOptions.IRONSOURCE),
        CBMediationHyprMX("HyprMX"),
        CBMediationAerServ("AerServ"),
        CBMediationHelium("Helium"),
        CBMediationOther("Other");
        

        /* renamed from: a reason: collision with root package name */
        private final String f1809a;

        private CBMediation(String str) {
            this.f1809a = str;
        }

        public String toString() {
            return this.f1809a;
        }
    }

    public enum CBPIDataUseConsent {
        UNKNOWN(-1),
        NO_BEHAVIORAL(0),
        YES_BEHAVIORAL(1);
        
        private static Map<Integer, CBPIDataUseConsent> b;

        /* renamed from: a reason: collision with root package name */
        private int f1810a;

        static {
            int i;
            CBPIDataUseConsent[] values;
            b = new HashMap();
            for (CBPIDataUseConsent cBPIDataUseConsent : values()) {
                b.put(Integer.valueOf(cBPIDataUseConsent.f1810a), cBPIDataUseConsent);
            }
        }

        private CBPIDataUseConsent(int i) {
            this.f1810a = i;
        }

        public static CBPIDataUseConsent valueOf(int i) {
            CBPIDataUseConsent cBPIDataUseConsent = (CBPIDataUseConsent) b.get(Integer.valueOf(i));
            return cBPIDataUseConsent == null ? UNKNOWN : cBPIDataUseConsent;
        }

        public int getValue() {
            return this.f1810a;
        }
    }

    @Deprecated
    public static void closeImpression() {
    }

    public static String getSDKVersion() {
        return "7.5.0";
    }

    public static boolean hasMoreApps(String str) {
        return false;
    }

    public static void setShouldDisplayLoadingViewForMoreApps(boolean z) {
    }

    private Chartboost() {
    }

    public static void startWithAppId(Activity activity, String str, String str2) {
        i.f1850a = "​!SDK-VERSION-STRING!:​com.chartboost.sdk:android-sdk:7.5.0";
        as.a("Chartboost.startWithAppId", (Object) activity);
        f fVar = new f(0);
        fVar.h = activity;
        fVar.i = str;
        fVar.j = str2;
        h.b(fVar);
    }

    public static void setPIDataUseConsent(Context context, CBPIDataUseConsent cBPIDataUseConsent) {
        h.a(context, cBPIDataUseConsent);
    }

    public static CBPIDataUseConsent getPIDataUseConsent() {
        return i.x;
    }

    @Deprecated
    public static void restrictDataCollection(Context context, boolean z) {
        setPIDataUseConsent(context, z ? CBPIDataUseConsent.NO_BEHAVIORAL : CBPIDataUseConsent.UNKNOWN);
    }

    public static void onCreate(Activity activity) {
        as.a("Chartboost.onCreate", (Object) activity);
        h a2 = h.a();
        if (a2 != null && !i.s) {
            a2.q.b(activity);
        }
    }

    public static void onStart(Activity activity) {
        as.a("Chartboost.onStart", (Object) activity);
        h a2 = h.a();
        if (a2 != null && !i.s) {
            a2.q.d(activity);
        }
    }

    public static void onResume(Activity activity) {
        as.a("Chartboost.onResume", (Object) activity);
        h a2 = h.a();
        if (a2 != null && !i.s) {
            a2.q.f(activity);
        }
    }

    public static void onPause(Activity activity) {
        as.a("Chartboost.onPause", (Object) activity);
        h a2 = h.a();
        if (a2 != null && !i.s) {
            a2.q.g(activity);
        }
    }

    public static void onStop(Activity activity) {
        as.a("Chartboost.onStop", (Object) activity);
        h a2 = h.a();
        if (a2 != null && !i.s) {
            a2.q.h(activity);
        }
    }

    public static boolean onBackPressed() {
        as.a("Chartboost.onBackPressed");
        h a2 = h.a();
        if (a2 == null) {
            return false;
        }
        return a2.q.j();
    }

    public static void onDestroy(Activity activity) {
        as.a("Chartboost.onDestroy", (Object) activity);
        h a2 = h.a();
        if (a2 != null && !i.s) {
            a2.q.j(activity);
        }
    }

    public static boolean hasRewardedVideo(String str) {
        as.a("Chartboost.hasRewardedVideo", str);
        h a2 = h.a();
        boolean z = false;
        if (a2 == null || !b.a()) {
            return false;
        }
        if (a2.k.a(str) != null) {
            z = true;
        }
        return z;
    }

    public static void cacheRewardedVideo(String str) {
        as.a("Chartboost.cacheRewardedVideo", str);
        h a2 = h.a();
        if (a2 != null && b.a() && h.f()) {
            if (s.a().a((CharSequence) str)) {
                CBLogging.b("Chartboost", "cacheRewardedVideo location cannot be empty");
                Handler handler = a2.p;
                c cVar = a2.l;
                cVar.getClass();
                handler.post(new a(4, str, CBImpressionError.INVALID_LOCATION));
                return;
            }
            e eVar = (e) a2.m.get();
            if ((!eVar.y || !eVar.E) && (!eVar.e || !eVar.j)) {
                Handler handler2 = a2.p;
                c cVar2 = a2.l;
                cVar2.getClass();
                handler2.post(new a(4, str, CBImpressionError.END_POINT_DISABLED));
            } else {
                com.chartboost.sdk.impl.e eVar2 = a2.k;
                eVar2.getClass();
                com.chartboost.sdk.impl.e.a aVar = new com.chartboost.sdk.impl.e.a(3, str, null, null);
                a2.f1847a.execute(aVar);
            }
        }
    }

    public static void cacheRewardedVideo(String str, String str2) {
        as.a("Chartboost.cacheHeliumRewardedVideo", str);
        u.b(str, str2);
    }

    public static void showRewardedVideo(String str) {
        as.a("Chartboost.showRewardedVideo", str);
        h a2 = h.a();
        if (a2 != null && b.a() && h.f()) {
            if (s.a().a((CharSequence) str)) {
                CBLogging.b("Chartboost", "showRewardedVideo location cannot be empty");
                Handler handler = a2.p;
                c cVar = a2.l;
                cVar.getClass();
                handler.post(new a(4, str, CBImpressionError.INVALID_LOCATION));
                return;
            }
            e eVar = (e) a2.m.get();
            if ((!eVar.y || !eVar.E) && (!eVar.e || !eVar.j)) {
                Handler handler2 = a2.p;
                c cVar2 = a2.l;
                cVar2.getClass();
                handler2.post(new a(4, str, CBImpressionError.END_POINT_DISABLED));
            } else {
                com.chartboost.sdk.impl.e eVar2 = a2.k;
                eVar2.getClass();
                com.chartboost.sdk.impl.e.a aVar = new com.chartboost.sdk.impl.e.a(4, str, null, null);
                a2.f1847a.execute(aVar);
            }
        }
    }

    public static boolean hasInterstitial(String str) {
        as.a("Chartboost.hasInterstitial", str);
        h a2 = h.a();
        boolean z = false;
        if (a2 == null || !b.a()) {
            return false;
        }
        if (a2.f.a(str) != null) {
            z = true;
        }
        return z;
    }

    public static void cacheInterstitial(String str) {
        as.a("Chartboost.cacheInterstitial", str);
        h a2 = h.a();
        if (a2 != null && b.a() && h.f()) {
            if (s.a().a((CharSequence) str)) {
                CBLogging.b("Chartboost", "cacheInterstitial location cannot be empty");
                Handler handler = a2.p;
                c cVar = a2.g;
                cVar.getClass();
                handler.post(new a(4, str, CBImpressionError.INVALID_LOCATION));
                return;
            }
            e eVar = (e) a2.m.get();
            if ((!eVar.y || !eVar.A) && (!eVar.e || !eVar.g)) {
                Handler handler2 = a2.p;
                c cVar2 = a2.g;
                cVar2.getClass();
                handler2.post(new a(4, str, CBImpressionError.END_POINT_DISABLED));
            } else {
                com.chartboost.sdk.impl.e eVar2 = a2.f;
                eVar2.getClass();
                com.chartboost.sdk.impl.e.a aVar = new com.chartboost.sdk.impl.e.a(3, str, null, null);
                a2.f1847a.execute(aVar);
            }
        }
    }

    public static void cacheInterstitial(String str, String str2) {
        as.a("Chartboost.cacheHeliumInterstitial", str);
        u.a(str, str2);
    }

    public static void showInterstitial(String str) {
        as.a("Chartboost.showInterstitial", str);
        h a2 = h.a();
        if (a2 != null && b.a() && h.f()) {
            if (s.a().a((CharSequence) str)) {
                CBLogging.b("Chartboost", "showInterstitial location cannot be empty");
                Handler handler = a2.p;
                c cVar = a2.g;
                cVar.getClass();
                handler.post(new a(4, str, CBImpressionError.INVALID_LOCATION));
                return;
            }
            e eVar = (e) a2.m.get();
            if ((!eVar.y || !eVar.A) && (!eVar.e || !eVar.g)) {
                Handler handler2 = a2.p;
                c cVar2 = a2.g;
                cVar2.getClass();
                handler2.post(new a(4, str, CBImpressionError.END_POINT_DISABLED));
            } else {
                com.chartboost.sdk.impl.e eVar2 = a2.f;
                eVar2.getClass();
                com.chartboost.sdk.impl.e.a aVar = new com.chartboost.sdk.impl.e.a(4, str, null, null);
                a2.f1847a.execute(aVar);
            }
        }
    }

    public static void cacheMoreApps(String str) {
        h a2 = h.a();
        if (a2 != null && b.a() && h.f()) {
            a2.getClass();
            h.a aVar = new h.a(5);
            aVar.b = str;
            a2.p.postDelayed(aVar, b.c);
        }
    }

    public static void showMoreApps(String str) {
        cacheMoreApps(str);
    }

    public static boolean isAnyViewVisible() {
        as.a("Chartboost.isAnyViewVisible");
        h a2 = h.a();
        return a2 != null && a2.q.e();
    }

    public static void setMediation(CBMediation cBMediation, String str) {
        as.a("Chartboost.setMediation");
        f fVar = new f(3);
        fVar.c = cBMediation;
        fVar.d = str;
        h.b(fVar);
    }

    public static void setFramework(CBFramework cBFramework, String str) {
        as.a("Chartboost.setFramework");
        f fVar = new f(4);
        fVar.b = cBFramework;
        fVar.d = str;
        h.b(fVar);
    }

    @Deprecated
    public static void setFrameworkVersion(String str) {
        as.a("Chartboost.setFrameworkVersion", str);
        f fVar = new f(5);
        fVar.d = str;
        h.b(fVar);
    }

    public static void setChartboostWrapperVersion(String str) {
        as.a("Chartboost.setChartboostWrapperVersion", str);
        f fVar = new f(5);
        fVar.d = str;
        h.b(fVar);
    }

    public static String getCustomId() {
        if (!b.b()) {
            return "";
        }
        return i.b;
    }

    public static void setCustomId(String str) {
        as.a("Chartboost.setCustomId", str);
        f fVar = new f(6);
        fVar.e = str;
        h.b(fVar);
    }

    public static void setLoggingLevel(Level level) {
        as.a("Chartboost.setLoggingLevel", level.toString());
        f fVar = new f(7);
        fVar.f = level;
        h.b(fVar);
    }

    public static Level getLoggingLevel() {
        b.b();
        return CBLogging.f1812a;
    }

    public static a getDelegate() {
        return i.d;
    }

    public static void setDelegate(ChartboostDelegate chartboostDelegate) {
        as.a("Chartboost.setDelegate", (Object) chartboostDelegate);
        f fVar = new f(8);
        fVar.g = chartboostDelegate;
        h.b(fVar);
    }

    public static boolean getAutoCacheAds() {
        return i.t;
    }

    public static void setAutoCacheAds(boolean z) {
        as.a("Chartboost.setAutoCacheAds", z);
        h a2 = h.a();
        if (a2 != null) {
            a2.getClass();
            h.a aVar = new h.a(1);
            aVar.c = z;
            h.b(aVar);
        }
    }

    public static void setShouldRequestInterstitialsInFirstSession(boolean z) {
        as.a("Chartboost.setShouldRequestInterstitialsInFirstSession", z);
        if (b.b()) {
            f fVar = new f(1);
            fVar.f1845a = z;
            h.b(fVar);
        }
    }

    public static void setShouldPrefetchVideoContent(boolean z) {
        as.a("Chartboost.setShouldPrefetchVideoContent", z);
        h a2 = h.a();
        if (a2 != null && b.a()) {
            a2.getClass();
            h.a aVar = new h.a(2);
            aVar.d = z;
            h.b(aVar);
        }
    }

    public static void setShouldHideSystemUI(Boolean bool) {
        as.a("Chartboost.setHideSystemUI", (Object) bool);
        i.h = bool.booleanValue();
    }

    public static boolean isWebViewEnabled() {
        h a2 = h.a();
        return a2 == null || ((e) a2.m.get()).y;
    }

    @TargetApi(14)
    public static void setActivityCallbacks(boolean z) {
        as.a("Chartboost.setActivityCallbacks", z);
        h a2 = h.a();
        if (a2 != null) {
            Activity a3 = a2.q.a();
            if (a3 != null) {
                ActivityLifecycleCallbacks activityLifecycleCallbacks = a2.q.h;
                if (activityLifecycleCallbacks != null) {
                    if (!i.s && z) {
                        a3.getApplication().registerActivityLifecycleCallbacks(activityLifecycleCallbacks);
                        i.s = true;
                    } else if (i.s && !z) {
                        a3.getApplication().unregisterActivityLifecycleCallbacks(activityLifecycleCallbacks);
                        i.s = false;
                    }
                }
            }
        }
    }

    private static void showInterstitialAIR(String str, boolean z) {
        h a2 = h.a();
        if (a2 != null && b.a() && h.f()) {
            e eVar = (e) a2.m.get();
            if ((!eVar.y || !eVar.A) && (!eVar.e || !eVar.g)) {
                i.d.didFailToLoadInterstitial(str, CBImpressionError.END_POINT_DISABLED);
            } else {
                Handler handler = a2.p;
                c cVar = a2.g;
                cVar.getClass();
                handler.post(new a(4, str, CBImpressionError.INTERNAL));
            }
        }
    }

    private static void showMoreAppsAIR(String str, boolean z) {
        cacheMoreApps(str);
    }

    private static void showRewardedVideoAIR(String str, boolean z) {
        h a2 = h.a();
        if (a2 != null && b.a() && h.f()) {
            e eVar = (e) a2.m.get();
            if ((!eVar.y || !eVar.E) && (!eVar.e || !eVar.j)) {
                i.d.didFailToLoadRewardedVideo(str, CBImpressionError.END_POINT_DISABLED);
            } else {
                Handler handler = a2.p;
                c cVar = a2.g;
                cVar.getClass();
                handler.post(new a(4, str, CBImpressionError.INTERNAL));
            }
        }
    }

    private static void forwardTouchEventsAIR(boolean z) {
        h a2 = h.a();
        if (a2 != null) {
            c cVar = a2.q;
            cVar.getClass();
            C0019c cVar2 = new C0019c(6);
            cVar2.c = z;
            h.b(cVar2);
        }
    }

    @TargetApi(28)
    public static void setActivityAttrs(Activity activity) {
        if (activity == null || !i.h) {
            if ((activity.getWindow().getAttributes().flags & 1024) != 0) {
                CBLogging.d("Chartboost", "Attempting to show Status and Navigation bars on a fullscreen activity. Please change your Chartboost activity theme to: \"@android:style/Theme.Translucent\"` in your Manifest file");
            }
            return;
        }
        Window window = activity.getWindow();
        int i = 2;
        if (s.a().a(16)) {
            i = 1798;
            if (VERSION.SDK_INT >= 19) {
                i = 5894;
            }
            if (VERSION.SDK_INT >= 28) {
                LayoutParams attributes = window.getAttributes();
                attributes.layoutInDisplayCutoutMode = 1;
                window.setAttributes(attributes);
            }
        }
        window.getDecorView().setSystemUiVisibility(i);
    }
}
