package com.chartboost.sdk.Libraries;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import com.chartboost.sdk.e;
import java.io.File;
import org.json.JSONObject;

public class h {

    /* renamed from: a reason: collision with root package name */
    private a f1821a;
    private final e b;
    private String c;
    private float d = 1.0f;

    public static class a {

        /* renamed from: a reason: collision with root package name */
        private int f1822a;
        private final String b;
        private final File c;
        private Bitmap d;
        private final f e;
        private int f = -1;
        private int g = -1;

        public a(String str, File file, f fVar) {
            this.c = file;
            this.b = str;
            this.d = null;
            this.f1822a = 1;
            this.e = fVar;
        }

        public Bitmap a() {
            if (this.d == null) {
                b();
            }
            return this.d;
        }

        public void b() {
            if (this.d == null) {
                StringBuilder sb = new StringBuilder();
                sb.append("Loading image '");
                sb.append(this.b);
                sb.append("' from cache");
                CBLogging.a("MemoryBitmap", sb.toString());
                byte[] a2 = this.e.a(this.c);
                if (a2 == null) {
                    CBLogging.b("MemoryBitmap", "decode() - bitmap not found");
                    return;
                }
                Options options = new Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeByteArray(a2, 0, a2.length, options);
                Options options2 = new Options();
                options2.inJustDecodeBounds = false;
                options2.inDither = false;
                options2.inPurgeable = true;
                options2.inInputShareable = true;
                options2.inTempStorage = new byte[32768];
                options2.inSampleSize = 1;
                while (true) {
                    if (options2.inSampleSize >= 32) {
                        break;
                    }
                    try {
                        this.d = BitmapFactory.decodeByteArray(a2, 0, a2.length, options2);
                        break;
                    } catch (OutOfMemoryError e2) {
                        CBLogging.a("MemoryBitmap", "OutOfMemoryError suppressed - trying larger sample size", e2);
                        options2.inSampleSize *= 2;
                    } catch (Exception e3) {
                        CBLogging.a("MemoryBitmap", "Exception raised decoding bitmap", e3);
                        com.chartboost.sdk.Tracking.a.a(getClass(), "decodeByteArray", e3);
                    }
                }
                this.f1822a = options2.inSampleSize;
            }
            return;
            if (this.d == null) {
                this.c.delete();
                StringBuilder sb2 = new StringBuilder();
                sb2.append("Unable to decode ");
                sb2.append(this.b);
                throw new RuntimeException(sb2.toString());
            }
            this.f1822a = options2.inSampleSize;
        }

        public int c() {
            return this.f1822a;
        }

        public int d() {
            if (this.d != null) {
                return this.d.getWidth();
            }
            if (this.f >= 0) {
                return this.f;
            }
            f();
            return this.f;
        }

        public int e() {
            if (this.d != null) {
                return this.d.getHeight();
            }
            if (this.g >= 0) {
                return this.g;
            }
            f();
            return this.g;
        }

        private void f() {
            try {
                Options options = new Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(this.c.getAbsolutePath(), options);
                this.f = options.outWidth;
                this.g = options.outHeight;
            } catch (Exception e2) {
                CBLogging.a("MemoryBitmap", "Error decoding file size", e2);
                com.chartboost.sdk.Tracking.a.a(getClass(), "decodeSize", e2);
            }
        }
    }

    public h(e eVar) {
        this.b = eVar;
    }

    public int a() {
        return this.f1821a.d() * this.f1821a.c();
    }

    public int b() {
        return this.f1821a.e() * this.f1821a.c();
    }

    public boolean a(String str) {
        return a(this.b.g(), str);
    }

    public boolean a(JSONObject jSONObject, String str) {
        boolean z = true;
        JSONObject a2 = e.a(jSONObject, str);
        this.c = str;
        if (a2 == null) {
            return true;
        }
        String optString = a2.optString("url");
        this.d = (float) a2.optDouble("scale", 1.0d);
        if (optString.isEmpty()) {
            return true;
        }
        String optString2 = a2.optString("checksum");
        if (optString2.isEmpty()) {
            return false;
        }
        this.f1821a = this.b.e.j.a(optString2);
        if (this.f1821a == null) {
            z = false;
        }
        return z;
    }

    public boolean c() {
        return this.f1821a != null;
    }

    public Bitmap d() {
        if (this.f1821a != null) {
            return this.f1821a.a();
        }
        return null;
    }

    public float e() {
        return this.d;
    }
}
