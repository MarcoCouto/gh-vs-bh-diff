package com.chartboost.sdk.Libraries;

import android.app.Activity;
import com.chartboost.sdk.impl.as;
import java.lang.ref.WeakReference;

public final class j extends WeakReference<Activity> {

    /* renamed from: a reason: collision with root package name */
    public final int f1823a;

    public j(Activity activity) {
        super(activity);
        as.a("WeakActivity.WeakActivity", (Object) activity);
        this.f1823a = activity.hashCode();
    }

    public boolean a(Activity activity) {
        return activity != null && activity.hashCode() == this.f1823a;
    }

    public int hashCode() {
        return this.f1823a;
    }
}
