package com.chartboost.sdk.Libraries;

import org.json.JSONException;
import org.json.JSONObject;

public class e {

    public static class a {

        /* renamed from: a reason: collision with root package name */
        final String f1818a;
        final Object b;

        public a(String str, Object obj) {
            this.f1818a = str;
            this.b = obj;
        }
    }

    public static JSONObject a(JSONObject jSONObject, String... strArr) {
        for (String str : strArr) {
            if (jSONObject == null) {
                break;
            }
            jSONObject = jSONObject.optJSONObject(str);
        }
        return jSONObject;
    }

    public static void a(JSONObject jSONObject, String str, Object obj) {
        try {
            jSONObject.put(str, obj);
        } catch (JSONException e) {
            StringBuilder sb = new StringBuilder();
            sb.append("put (");
            sb.append(str);
            sb.append(")");
            com.chartboost.sdk.Tracking.a.a(e.class, sb.toString(), (Exception) e);
        }
    }

    public static JSONObject a(a... aVarArr) {
        JSONObject jSONObject = new JSONObject();
        for (a aVar : aVarArr) {
            a(jSONObject, aVar.f1818a, aVar.b);
        }
        return jSONObject;
    }

    public static a a(String str, Object obj) {
        return new a(str, obj);
    }
}
