package com.chartboost.sdk.Libraries;

import com.tapjoy.TJAdUnitConstants.String;
import java.io.File;

public class g {

    /* renamed from: a reason: collision with root package name */
    public final File f1820a;
    public final File b;
    public final File c;
    public final File d;
    public final File e;
    public final File f;
    public final File g;

    g(File file) {
        this.f1820a = new File(file, ".chartboost");
        if (!this.f1820a.exists()) {
            this.f1820a.mkdirs();
        }
        this.b = a(this.f1820a, "css");
        this.c = a(this.f1820a, String.HTML);
        this.d = a(this.f1820a, "images");
        this.e = a(this.f1820a, "js");
        this.f = a(this.f1820a, "templates");
        this.g = a(this.f1820a, "videos");
    }

    private static File a(File file, String str) {
        File file2 = new File(file, str);
        if (!file2.exists()) {
            file2.mkdir();
        }
        return file2;
    }
}
