package com.chartboost.sdk.Libraries;

import android.content.ContentResolver;
import android.content.Context;
import android.os.Build;
import android.os.Looper;
import android.provider.Settings.Secure;
import android.provider.Settings.SettingNotFoundException;
import android.util.Base64;
import com.chartboost.sdk.i;
import com.chartboost.sdk.impl.at;
import com.chartboost.sdk.impl.au;
import com.tapjoy.TapjoyConstants;
import org.json.JSONObject;

public class d {

    /* renamed from: a reason: collision with root package name */
    private int f1816a = -1;
    private String b = null;
    private final String c;

    public static class a {

        /* renamed from: a reason: collision with root package name */
        public final int f1817a;
        public final String b;
        public final String c;
        public final String d;

        public a(int i, String str, String str2, String str3) {
            this.f1817a = i;
            this.b = str;
            this.c = str2;
            this.d = str3;
        }
    }

    public d(Context context) {
        this.c = at.c(context);
    }

    public synchronized a a() {
        if (Looper.myLooper() != Looper.getMainLooper() || "robolectric".equals(Build.FINGERPRINT)) {
            if (b()) {
                c();
            } else {
                a(i.n);
            }
            String str = this.b;
            JSONObject jSONObject = new JSONObject();
            if (this.c != null && str == null) {
                e.a(jSONObject, "uuid", this.c);
            }
            if (str != null) {
                e.a(jSONObject, "gaid", str);
            }
            return new a(this.f1816a, Base64.encodeToString(jSONObject.toString().getBytes(), 0), str != null ? "000000000" : this.c, str);
        }
        CBLogging.b("CBIdentity", "I must be called from a background thread");
        return null;
    }

    private static boolean b() {
        return !"Amazon".equalsIgnoreCase(Build.MANUFACTURER);
    }

    private void c() {
        if (au.a(i.n)) {
            a aVar = new a(i.n);
            this.f1816a = aVar.f1814a;
            this.b = aVar.b;
        }
    }

    private void a(Context context) {
        try {
            ContentResolver contentResolver = context.getContentResolver();
            if (!(Secure.getInt(contentResolver, "limit_ad_tracking") != 0)) {
                String string = Secure.getString(contentResolver, TapjoyConstants.TJC_ADVERTISING_ID);
                if ("00000000-0000-0000-0000-000000000000".equals(string)) {
                    this.f1816a = 1;
                    this.b = null;
                    return;
                }
                this.f1816a = 0;
                this.b = string;
                return;
            }
            this.f1816a = 1;
            this.b = null;
        } catch (SettingNotFoundException unused) {
            this.f1816a = -1;
            this.b = null;
        }
    }
}
