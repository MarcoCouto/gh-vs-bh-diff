package com.chartboost.sdk;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.SurfaceView;
import android.view.ViewGroup.LayoutParams;
import android.widget.RelativeLayout;
import com.chartboost.sdk.Libraries.CBLogging;
import com.chartboost.sdk.Model.CBError.CBImpressionError;
import com.chartboost.sdk.Model.c;
import com.chartboost.sdk.Tracking.a;
import com.chartboost.sdk.impl.s;

@SuppressLint({"Registered"})
public class CBImpressionActivity extends Activity {

    /* renamed from: a reason: collision with root package name */
    final a f1806a;
    final Handler b;
    final c c;
    private Activity d;

    public CBImpressionActivity() {
        this.f1806a = h.a() != null ? h.a().o : null;
        this.b = h.a() != null ? h.a().p : null;
        this.c = h.a() != null ? h.a().q : null;
        this.d = null;
    }

    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        if (this.d != null) {
            return this.d.dispatchTouchEvent(motionEvent);
        }
        return super.dispatchTouchEvent(motionEvent);
    }

    public void forwardTouchEvents(Activity activity) {
        this.d = activity;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if ((getIntent() != null && !getIntent().getBooleanExtra("isChartboost", false)) || this.f1806a == null || this.b == null || this.c == null) {
            CBLogging.b("CBImpressionActivity", "This activity cannot be called from outside chartboost SDK");
            finish();
            return;
        }
        a();
        requestWindowFeature(1);
        getWindow().setWindowAnimations(0);
        this.c.a(this);
        setContentView(new RelativeLayout(this));
        b();
        CBLogging.a("CBImpressionActivity", "Impression Activity onCreate() called");
    }

    @TargetApi(11)
    private void a() {
        if (s.a().a(11)) {
            getWindow().setFlags(16777216, 16777216);
        }
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        try {
            super.onStart();
            if (this.c != null && !i.s) {
                this.c.e(this);
            }
        } catch (Exception e) {
            a.a(getClass(), "onStart", e);
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        try {
            super.onResume();
            if (this.c != null && !i.s) {
                this.c.a((Activity) this);
                this.c.h();
            }
        } catch (Exception e) {
            a.a(getClass(), "onResume", e);
        }
        Chartboost.setActivityAttrs(this);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        try {
            super.onPause();
            if (this.c != null && !i.s) {
                this.c.a((Activity) this);
                this.c.i();
            }
        } catch (Exception e) {
            a.a(getClass(), "onPause", e);
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        try {
            super.onStop();
            if (this.c != null && !i.s) {
                this.c.i(this);
            }
        } catch (Exception e) {
            a.a(getClass(), "onStop", e);
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        try {
            if (this.c != null && !i.s) {
                this.c.k(this);
            }
            super.onDestroy();
        } catch (Exception e) {
            a.a(getClass(), "onDestroy", e);
        } catch (Throwable th) {
            super.onDestroy();
            throw th;
        }
    }

    public void onBackPressed() {
        try {
            if (this.c == null || !this.c.k()) {
                super.onBackPressed();
            }
        } catch (Exception e) {
            a.a(getClass(), "onBackPressed", e);
        }
    }

    public void onAttachedToWindow() {
        try {
            super.onAttachedToWindow();
            if (s.a().a(14) && getWindow() != null && getWindow().getDecorView() != null && !getWindow().getDecorView().isHardwareAccelerated() && this.c != null) {
                CBLogging.b("CBImpressionActivity", "The activity passed down is not hardware accelerated, so Chartboost cannot show ads");
                c d2 = this.c.d();
                if (d2 != null) {
                    d2.a(CBImpressionError.HARDWARE_ACCELERATION_DISABLED);
                }
                finish();
            }
        } catch (Exception e) {
            a.a(getClass(), "onAttachedToWindow", e);
        }
    }

    private void b() {
        if (!s.a().a(14)) {
            this.b.post(new Runnable() {
                public void run() {
                    try {
                        CBLogging.e("VideoInit", "preparing activity for video surface");
                        CBImpressionActivity.this.addContentView(new SurfaceView(CBImpressionActivity.this), new LayoutParams(0, 0));
                    } catch (Exception e) {
                        a.a(CBImpressionActivity.class, "postCreateSurfaceView Runnable.run", e);
                    }
                }
            });
        }
    }
}
