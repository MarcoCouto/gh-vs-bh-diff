package com.chartboost.sdk;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.text.TextUtils;
import com.chartboost.sdk.Chartboost.CBFramework;
import com.chartboost.sdk.Chartboost.CBMediation;
import com.chartboost.sdk.Libraries.CBLogging;
import com.chartboost.sdk.Libraries.CBLogging.Level;
import com.chartboost.sdk.h.a;
import com.chartboost.sdk.impl.ae;
import com.chartboost.sdk.impl.as;
import com.chartboost.sdk.impl.o;
import com.chartboost.sdk.impl.s;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ScheduledExecutorService;

class f implements Runnable {

    /* renamed from: a reason: collision with root package name */
    boolean f1845a = false;
    CBFramework b = null;
    CBMediation c = null;
    String d = null;
    String e = null;
    Level f = null;
    ChartboostDelegate g = null;
    Activity h = null;
    String i = null;
    String j = null;
    private final int k;

    f(int i2) {
        this.k = i2;
    }

    /* JADX WARNING: Removed duplicated region for block: B:56:0x011c A[Catch:{ Throwable -> 0x0116 }] */
    public void run() {
        try {
            switch (this.k) {
                case 0:
                    if (h.a() == null) {
                        synchronized (h.class) {
                            if (h.a() == null) {
                                if (this.h == null) {
                                    CBLogging.b("ChartboostCommand", "Activity object is null. Please pass a valid activity object");
                                    return;
                                } else if (!b.a((Context) this.h)) {
                                    CBLogging.b("ChartboostCommand", "Permissions not set correctly");
                                    return;
                                } else {
                                    if (!b.b(this.h)) {
                                        CBLogging.b("ChartboostCommand", "Please add CBImpressionActivity in AndroidManifest.xml following README.md instructions.");
                                    }
                                    if (!TextUtils.isEmpty(this.i)) {
                                        if (!TextUtils.isEmpty(this.j)) {
                                            s a2 = s.a();
                                            g a3 = g.a();
                                            Handler handler = a2.f1920a;
                                            o.a();
                                            ScheduledExecutorService scheduledExecutorService = null;
                                            try {
                                                ScheduledExecutorService scheduledExecutorService2 = (ScheduledExecutorService) a3.a(ae.a());
                                                try {
                                                    h hVar = new h(this.h, this.i, this.j, a2, scheduledExecutorService2, handler, (ExecutorService) a3.a(ae.a(4)));
                                                    h.a(hVar);
                                                    hVar.b.c();
                                                    hVar.getClass();
                                                    hVar.a((Runnable) new a(3));
                                                    break;
                                                } catch (Throwable th) {
                                                    th = th;
                                                    scheduledExecutorService = scheduledExecutorService2;
                                                    if (scheduledExecutorService != null) {
                                                        scheduledExecutorService.shutdown();
                                                    }
                                                    CBLogging.a("ChartboostCommand", "Unable to start threads", th);
                                                    return;
                                                }
                                            } catch (Throwable th2) {
                                                th = th2;
                                                if (scheduledExecutorService != null) {
                                                }
                                                CBLogging.a("ChartboostCommand", "Unable to start threads", th);
                                                return;
                                            }
                                        }
                                    }
                                    CBLogging.b("ChartboostCommand", "AppId or AppSignature is null. Please pass a valid id's");
                                    return;
                                }
                            }
                        }
                    }
                    break;
                case 1:
                    i.u = this.f1845a;
                    break;
                case 3:
                    i.j = this.c;
                    i.k = this.d;
                    StringBuilder sb = new StringBuilder();
                    sb.append(i.j);
                    sb.append(" ");
                    sb.append(i.k);
                    i.i = sb.toString();
                    break;
                case 4:
                    if (this.b != null) {
                        i.e = this.b;
                        i.f = this.d;
                        i.g = String.format("%s %s", new Object[]{this.b, this.d});
                        break;
                    } else {
                        CBLogging.b("ChartboostCommand", "Pass a valid CBFramework enum value");
                        return;
                    }
                case 5:
                    b.a(this.d);
                    break;
                case 6:
                    i.b = this.e;
                    break;
                case 7:
                    if (b.b()) {
                        CBLogging.f1812a = this.f;
                        break;
                    } else {
                        return;
                    }
                case 8:
                    i.d = this.g;
                    as.a("SdkSettings.assignDelegate", (Object) this.g);
                    break;
            }
        } catch (Exception e2) {
            StringBuilder sb2 = new StringBuilder();
            sb2.append("run (");
            sb2.append(this.k);
            sb2.append(")");
            com.chartboost.sdk.Tracking.a.a(f.class, sb2.toString(), e2);
        }
    }
}
