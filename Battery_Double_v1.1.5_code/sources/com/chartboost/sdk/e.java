package com.chartboost.sdk;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Handler;
import android.support.v7.widget.helper.ItemTouchHelper.Callback;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.RelativeLayout;
import com.chartboost.sdk.Libraries.CBLogging;
import com.chartboost.sdk.Libraries.CBUtility;
import com.chartboost.sdk.Model.CBError.CBImpressionError;
import com.chartboost.sdk.Model.c;
import java.util.IdentityHashMap;
import java.util.Map;
import org.json.JSONObject;

public abstract class e {

    /* renamed from: a reason: collision with root package name */
    public final Handler f1841a;
    public final c b;
    public boolean c = false;
    protected JSONObject d;
    public final c e;
    protected int f;
    public final Map<View, Runnable> g = new IdentityHashMap();
    protected boolean h = true;
    protected boolean i = true;
    private boolean j;
    private a k;

    public abstract class a extends RelativeLayout {

        /* renamed from: a reason: collision with root package name */
        Integer f1843a = null;
        private boolean c = false;
        private int d = -1;
        private int e = -1;
        private int f = -1;
        private int g = -1;

        /* access modifiers changed from: protected */
        public abstract void a(int i, int i2);

        public void b() {
        }

        public a(Context context) {
            super(context);
            setFocusableInTouchMode(true);
            requestFocus();
        }

        /* access modifiers changed from: protected */
        public void onSizeChanged(int i, int i2, int i3, int i4) {
            super.onSizeChanged(i, i2, i3, i4);
            this.f = i;
            this.g = i2;
            if (this.d != -1 && this.e != -1 && e.this.e != null && e.this.e.p.m == 0) {
                a();
            }
        }

        private boolean b(int i, int i2) {
            boolean z = true;
            if (e.this.e != null && e.this.e.p.m == 1) {
                return true;
            }
            if (this.c) {
                return false;
            }
            int a2 = CBUtility.a();
            if (this.d == i && this.e == i2 && this.f1843a != null && this.f1843a.intValue() == a2) {
                return true;
            }
            this.c = true;
            try {
                if (e.this.h && CBUtility.a(a2)) {
                    e.this.f = a2;
                } else if (e.this.i && CBUtility.b(a2)) {
                    e.this.f = a2;
                }
                a(i, i2);
                post(new Runnable() {
                    public void run() {
                        a.this.requestLayout();
                    }
                });
                this.d = i;
                this.e = i2;
                this.f1843a = Integer.valueOf(a2);
            } catch (Exception e2) {
                CBLogging.a("CBViewProtocol", "Exception raised while layouting Subviews", e2);
                com.chartboost.sdk.Tracking.a.a(getClass(), "tryLayout", e2);
                z = false;
            }
            this.c = false;
            return z;
        }

        public final void a() {
            a(false);
        }

        public final void a(boolean z) {
            if (z) {
                this.f1843a = null;
            }
            a((Activity) getContext());
        }

        public boolean a(Activity activity) {
            int i;
            int i2;
            int i3;
            if (this.f == -1 || this.g == -1) {
                try {
                    i2 = getWidth();
                    i = getHeight();
                    if (i2 == 0 || i == 0) {
                        View findViewById = activity.getWindow().findViewById(16908290);
                        if (findViewById == null) {
                            findViewById = activity.getWindow().getDecorView();
                        }
                        int width = findViewById.getWidth();
                        i = findViewById.getHeight();
                        i2 = width;
                    }
                } catch (Exception unused) {
                    i2 = 0;
                    i = 0;
                }
                if (i2 == 0 || i == 0) {
                    DisplayMetrics displayMetrics = new DisplayMetrics();
                    activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                    i3 = displayMetrics.widthPixels;
                    i = displayMetrics.heightPixels;
                } else {
                    i3 = i2;
                }
                this.f = i3;
                this.g = i;
            }
            return b(this.f, this.g);
        }

        public void onDetachedFromWindow() {
            super.onDetachedFromWindow();
            synchronized (e.this.g) {
                for (Runnable removeCallbacks : e.this.g.values()) {
                    e.this.f1841a.removeCallbacks(removeCallbacks);
                }
                e.this.g.clear();
            }
        }

        public final void a(View view) {
            int id = getId();
            int i = Callback.DEFAULT_DRAG_ANIMATION_DURATION;
            if (200 == id) {
                i = 201;
            }
            View findViewById = findViewById(i);
            while (findViewById != null) {
                i++;
                findViewById = findViewById(i);
            }
            view.setId(i);
            view.setSaveEnabled(false);
        }
    }

    /* access modifiers changed from: protected */
    public abstract a b(Context context);

    public float j() {
        return 0.0f;
    }

    public float k() {
        return 0.0f;
    }

    public boolean l() {
        return false;
    }

    public static boolean a(Context context) {
        return (context.getResources().getConfiguration().screenLayout & 15) >= 4;
    }

    public e(c cVar, Handler handler, c cVar2) {
        this.f1841a = handler;
        this.b = cVar2;
        this.e = cVar;
        this.k = null;
        this.f = CBUtility.a();
        this.j = false;
    }

    public int a() {
        return this.f;
    }

    public boolean a(JSONObject jSONObject) {
        this.d = com.chartboost.sdk.Libraries.e.a(jSONObject, "assets");
        if (this.d != null) {
            return true;
        }
        this.d = new JSONObject();
        CBLogging.b("CBViewProtocol", "Media got from the response is null or empty");
        a(CBImpressionError.INVALID_RESPONSE);
        return false;
    }

    public void b() {
        i();
    }

    public CBImpressionError c() {
        Activity b2 = this.b.b();
        if (b2 == null) {
            this.k = null;
            return CBImpressionError.NO_HOST_ACTIVITY;
        } else if (!this.i && !this.h) {
            return CBImpressionError.WRONG_ORIENTATION;
        } else {
            if (this.k == null) {
                this.k = b((Context) b2);
            }
            if (this.e.p.m != 0 || this.k.a(b2)) {
                return null;
            }
            this.k = null;
            return CBImpressionError.ERROR_CREATING_VIEW;
        }
    }

    public void d() {
        f();
        synchronized (this.g) {
            for (Runnable removeCallbacks : this.g.values()) {
                this.f1841a.removeCallbacks(removeCallbacks);
            }
            this.g.clear();
        }
    }

    public a e() {
        return this.k;
    }

    public void f() {
        if (this.k != null) {
            this.k.b();
        }
        this.k = null;
    }

    public JSONObject g() {
        return this.d;
    }

    public void a(CBImpressionError cBImpressionError) {
        this.e.a(cBImpressionError);
    }

    public void h() {
        if (!this.j) {
            this.j = true;
            this.e.c();
        }
    }

    /* access modifiers changed from: protected */
    public void i() {
        this.e.d();
    }

    public boolean b(JSONObject jSONObject) {
        return this.e.a(jSONObject);
    }

    public void a(boolean z, View view) {
        a(z, view, true);
    }

    public void a(final boolean z, final View view, boolean z2) {
        int i2 = 8;
        if ((z && view.getVisibility() == 0) || (!z && view.getVisibility() == 8)) {
            synchronized (this.g) {
                if (!this.g.containsKey(view)) {
                    return;
                }
            }
        }
        if (!z2) {
            if (z) {
                i2 = 0;
            }
            view.setVisibility(i2);
            view.setClickable(z);
            return;
        }
        AnonymousClass1 r6 = new Runnable() {
            public void run() {
                if (!z) {
                    view.setVisibility(8);
                    view.setClickable(false);
                }
                synchronized (e.this.g) {
                    e.this.g.remove(view);
                }
            }
        };
        int i3 = this.e.p.m;
        this.e.i.f1839a.a(z, view, 500);
        a(view, (Runnable) r6, 500);
    }

    public void a(View view, Runnable runnable, long j2) {
        synchronized (this.g) {
            Runnable runnable2 = (Runnable) this.g.get(view);
            if (runnable2 != null) {
                this.f1841a.removeCallbacks(runnable2);
            }
            this.g.put(view, runnable);
        }
        this.f1841a.postDelayed(runnable, j2);
    }

    public static int a(String str) {
        if (str != null) {
            if (!str.startsWith("#")) {
                try {
                    return Color.parseColor(str);
                } catch (IllegalArgumentException unused) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("#");
                    sb.append(str);
                    str = sb.toString();
                }
            }
            if (str.length() == 4 || str.length() == 5) {
                StringBuilder sb2 = new StringBuilder((str.length() * 2) + 1);
                sb2.append("#");
                int i2 = 0;
                while (i2 < str.length() - 1) {
                    i2++;
                    sb2.append(str.charAt(i2));
                    sb2.append(str.charAt(i2));
                }
                str = sb2.toString();
            }
            try {
                return Color.parseColor(str);
            } catch (IllegalArgumentException e2) {
                StringBuilder sb3 = new StringBuilder();
                sb3.append("error parsing color ");
                sb3.append(str);
                CBLogging.c("CBViewProtocol", sb3.toString(), e2);
            }
        }
        return 0;
    }

    public void m() {
        if (this.c) {
            this.c = false;
        }
        a e2 = e();
        if (e2 == null) {
            return;
        }
        if (e2.f1843a == null || CBUtility.a() != e2.f1843a.intValue()) {
            e2.a(false);
        }
    }

    public void n() {
        this.c = true;
    }
}
