package com.chartboost.sdk.Tracking;

import android.text.TextUtils;
import android.util.Log;
import com.chartboost.sdk.Libraries.CBLogging;
import com.chartboost.sdk.Libraries.b;
import com.chartboost.sdk.Model.e;
import io.fabric.sdk.android.services.settings.SettingsJsonConstants;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import org.json.JSONObject;

public class a implements b {
    private static a d;
    private static final Long g = Long.valueOf(TimeUnit.MINUTES.toMillis(5));
    private final AtomicReference<e> e;
    private boolean f = false;
    private long h = (System.currentTimeMillis() - g.longValue());

    public a(AtomicReference<e> atomicReference) {
        d = this;
        this.e = atomicReference;
    }

    public void a() {
        a("start");
        a("did-become-active");
    }

    private void a(String str) {
        if (((e) this.e.get()).n) {
            a(SettingsJsonConstants.SESSION_KEY, str, null, null, null, false);
        }
    }

    public void b() {
        c();
    }

    private void c() {
        if (((e) this.e.get()).n) {
            a(SettingsJsonConstants.SESSION_KEY, TtmlNode.END, null, null, null, null, null, false);
            a("did-become-active");
        }
    }

    public void a(String str, String str2, String str3, String str4) {
        if (((e) this.e.get()).o) {
            a("webview-track", str, str2, str3, str4, null, null, false);
        }
    }

    public void a(JSONObject jSONObject) {
        e eVar = (e) this.e.get();
        if (eVar.o) {
            a("folder", com.chartboost.sdk.b.a(eVar), null, null, null, null, jSONObject, false);
        }
    }

    public void a(String str, String str2, String str3) {
        if (((e) this.e.get()).o) {
            a("load", str, str2, str3, null, false);
        }
    }

    public void b(String str, String str2, String str3) {
        if (((e) this.e.get()).o) {
            a("ad-show", str, str2, str3, null, false);
        }
    }

    public void c(String str, String str2, String str3) {
        if (((e) this.e.get()).o) {
            a("ad-click", str, str2, str3, null, false);
        }
    }

    public void d(String str, String str2, String str3) {
        if (((e) this.e.get()).o) {
            a("ad-close", str, str2, str3, null, false);
        }
    }

    public void e(String str, String str2, String str3) {
        if (((e) this.e.get()).o) {
            a("ad-dismiss", str, str2, str3, null, false);
        }
    }

    public void a(String str, String str2, String str3, String str4, boolean z) {
        if (((e) this.e.get()).o) {
            String str5 = "ad-error";
            if (TextUtils.isEmpty(str3)) {
                str3 = "empty-adid";
            }
            a(str5, str, str2, str3, str4, z);
        }
    }

    public void a(String str, String str2, String str3, String str4, String str5, String str6, String str7) {
        if (((e) this.e.get()).l) {
            String str8 = str5;
            JSONObject a2 = com.chartboost.sdk.Libraries.e.a(com.chartboost.sdk.Libraries.e.a("adId", (Object) str5), com.chartboost.sdk.Libraries.e.a("location", (Object) str6), com.chartboost.sdk.Libraries.e.a("state", (Object) str7));
            a("ad-unit-error", str, str2, str3, str4, null, a2, true);
        }
    }

    public void b(String str, String str2, String str3, String str4) {
        if (((e) this.e.get()).o) {
            String str5 = "ad-warning";
            if (TextUtils.isEmpty(str3)) {
                str3 = "empty-adid";
            }
            a(str5, str, str2, str3, str4, false);
        }
    }

    public void a(String str, String str2) {
        e eVar = (e) this.e.get();
        if (eVar.o) {
            a("download-asset-start", com.chartboost.sdk.b.a(eVar), str, str2, null, null, null, false);
        }
    }

    public void a(String str, String str2, long j, long j2, long j3) {
        if (((e) this.e.get()).p) {
            JSONObject a2 = com.chartboost.sdk.Libraries.e.a(com.chartboost.sdk.Libraries.e.a("processingMs", (Object) Long.valueOf(j)), com.chartboost.sdk.Libraries.e.a("getResponseCodeMs", (Object) Long.valueOf(j2)), com.chartboost.sdk.Libraries.e.a("readDataMs", (Object) Long.valueOf(j3)));
            a("download-asset-failure", str, str2, null, null, null, a2, false);
        }
    }

    public void a(String str, long j, long j2, long j3) {
        if (((e) this.e.get()).p) {
            JSONObject a2 = com.chartboost.sdk.Libraries.e.a(com.chartboost.sdk.Libraries.e.a("processingMs", (Object) Long.valueOf(j)), com.chartboost.sdk.Libraries.e.a("getResposeCodeMs", (Object) Long.valueOf(j2)), com.chartboost.sdk.Libraries.e.a("readDataMs", (Object) Long.valueOf(j3)));
            a("download-asset-success", str, null, null, null, null, a2, false);
        }
    }

    public void b(String str, String str2) {
        if (((e) this.e.get()).o) {
            a("playback-complete", str, str2, null, null, false);
        }
    }

    public void c(String str, String str2) {
        if (((e) this.e.get()).o) {
            a("replay", str, str2, null, null, false);
        }
    }

    public void d(String str, String str2) {
        if (((e) this.e.get()).o) {
            a("playback-start", str, str2, null, null, false);
        }
    }

    public void e(String str, String str2) {
        if (((e) this.e.get()).o) {
            a("playback-stop", str, str2, null, null, false);
        }
    }

    public static void a(Class cls, String str, Exception exc) {
        exc.printStackTrace();
        a aVar = d;
        if (aVar != null) {
            aVar.b(cls, str, exc);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0066, code lost:
        return;
     */
    private synchronized void b(Class cls, String str, Exception exc) {
        synchronized (this) {
            if (this.e != null) {
                e eVar = (e) this.e.get();
                if (eVar != null && eVar.k && !this.f) {
                    this.f = true;
                    try {
                        long currentTimeMillis = System.currentTimeMillis();
                        if (currentTimeMillis - this.h >= g.longValue()) {
                            a("exception", cls.getName(), str, exc.getClass().getName(), exc.getMessage(), eVar.r ? Log.getStackTraceString(exc) : null, null, true);
                            this.h = currentTimeMillis;
                        }
                    } catch (Exception e2) {
                        try {
                            e2.printStackTrace();
                        } catch (Throwable th) {
                            this.f = false;
                            throw th;
                        }
                    }
                    this.f = false;
                }
            }
        }
    }

    public void a(String str, String str2, String str3, String str4, String str5, String str6, JSONObject jSONObject) {
        if (((e) this.e.get()).o) {
            a(str, str2, str3, str4, str5, str6, jSONObject, false);
        }
    }

    private void a(String str, String str2, String str3, String str4, String str5, boolean z) {
        a(str, str2, str3, str4, str5, null, new JSONObject(), z);
    }

    public void a(String str, String str2, String str3, String str4, String str5, String str6, JSONObject jSONObject, boolean z) {
        if (((e) this.e.get()).o) {
            String str7 = "CBTrack";
            if (str == null) {
                str = "unknown event";
            }
            CBLogging.a(str7, str);
        }
    }
}
