package com.chartboost.sdk.Model;

import android.content.SharedPreferences;
import android.os.Handler;
import com.chartboost.sdk.Libraries.CBLogging;
import com.chartboost.sdk.Libraries.f;
import com.chartboost.sdk.Model.CBError.CBClickError;
import com.chartboost.sdk.Model.CBError.CBImpressionError;
import com.chartboost.sdk.Tracking.a;
import com.chartboost.sdk.d;
import com.chartboost.sdk.e;
import com.chartboost.sdk.i;
import com.chartboost.sdk.impl.aj;
import com.chartboost.sdk.impl.al;
import com.chartboost.sdk.impl.am;
import com.chartboost.sdk.impl.an;
import com.chartboost.sdk.impl.ar;
import com.chartboost.sdk.impl.be;
import com.chartboost.sdk.impl.bh;
import com.chartboost.sdk.impl.s;
import com.chartboost.sdk.impl.w;
import com.chartboost.sdk.impl.x;
import com.mintegral.msdk.reward.player.MTGRewardVideoActivity;
import java.util.Locale;
import org.json.JSONObject;

public class c {
    private boolean A;
    private Boolean B = null;
    private e C;
    private Runnable D;

    /* renamed from: a reason: collision with root package name */
    public final com.chartboost.sdk.impl.c f1830a;
    public final f b;
    public final aj c;
    public final ar d;
    public final a e;
    public final Handler f;
    public final com.chartboost.sdk.c g;
    public final am h;
    public final d i;
    public final an j;
    public final d k;
    public int l;
    public final String m;
    public int n;
    public final String o;
    public final a p;
    public final SharedPreferences q;
    public boolean r;
    public be s;
    public boolean t = false;
    public boolean u = false;
    public boolean v = false;
    public al w;
    public boolean x;
    public boolean y = false;
    public boolean z = false;

    public c(a aVar, d dVar, f fVar, aj ajVar, ar arVar, SharedPreferences sharedPreferences, a aVar2, Handler handler, com.chartboost.sdk.c cVar, am amVar, d dVar2, an anVar, com.chartboost.sdk.impl.c cVar2, String str, String str2) {
        this.p = aVar;
        this.f1830a = cVar2;
        this.b = fVar;
        this.c = ajVar;
        this.d = arVar;
        this.e = aVar2;
        this.f = handler;
        this.g = cVar;
        this.h = amVar;
        this.i = dVar2;
        this.j = anVar;
        this.k = dVar;
        this.l = 0;
        this.r = false;
        this.x = false;
        this.z = true;
        this.n = 3;
        this.m = str;
        this.o = str2;
        this.A = true;
        this.q = sharedPreferences;
    }

    public boolean a() {
        this.l = 0;
        if (this.p.m == 0) {
            switch (this.f1830a.f1903a) {
                case 0:
                    if (!this.p.A.equals("video")) {
                        this.n = 0;
                        this.C = new w(this, this.f, this.g);
                        break;
                    } else {
                        this.n = 1;
                        this.C = new x(this, this.b, this.f, this.g);
                        this.A = false;
                        break;
                    }
                case 1:
                    this.n = 2;
                    this.C = new x(this, this.b, this.f, this.g);
                    this.A = false;
                    break;
            }
        } else {
            switch (this.f1830a.f1903a) {
                case 0:
                    if (!this.p.A.equals("video")) {
                        this.n = 0;
                        break;
                    } else {
                        this.n = 1;
                        this.A = false;
                        break;
                    }
                case 1:
                    this.n = 2;
                    this.A = false;
                    break;
            }
            bh bhVar = new bh(this, this.b, this.c, this.q, this.e, this.f, this.g, this.i);
            this.C = bhVar;
        }
        return this.C.a(this.p.l);
    }

    public boolean b() {
        return this.A;
    }

    public void c() {
        this.z = true;
        this.g.b(this);
        this.k.b(this);
    }

    public void d() {
        this.k.a(this);
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x003f A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0040  */
    public boolean a(JSONObject jSONObject) {
        if (this.l != 2 || this.t) {
            return false;
        }
        String str = this.p.u;
        String str2 = this.p.t;
        if (!str2.isEmpty()) {
            try {
                if (this.h.a(str2)) {
                    try {
                        this.B = Boolean.TRUE;
                        str = str2;
                    } catch (Exception e2) {
                        String str3 = str2;
                        e = e2;
                        str = str3;
                        a.a(getClass(), "onClick", e);
                        if (!this.x) {
                        }
                    }
                } else {
                    this.B = Boolean.FALSE;
                }
            } catch (Exception e3) {
                e = e3;
            }
        }
        if (!this.x) {
            return false;
        }
        this.x = true;
        this.z = false;
        a(str, jSONObject);
        return true;
    }

    private boolean x() {
        return this.B != null;
    }

    private boolean y() {
        return this.B.booleanValue();
    }

    public void a(CBImpressionError cBImpressionError) {
        this.k.a(this, cBImpressionError);
    }

    public void e() {
        this.u = true;
        this.A = true;
        if (this.f1830a.f1903a == 1 && i.d != null) {
            i.d.didCompleteRewardedVideo(this.m, this.p.v);
        } else if (this.f1830a.f1903a == 0 && i.d != null) {
            i.d.didCompleteInterstitial(this.m);
        }
        w();
    }

    public void f() {
        this.v = true;
    }

    public boolean g() {
        if (this.C != null) {
            this.C.b();
            if (this.C.e() != null) {
                return true;
            }
        } else {
            CBLogging.b("CBImpression", "reinitializing -- no view protocol exists!!");
        }
        CBLogging.e("CBImpression", "reinitializing -- view not yet created");
        return false;
    }

    public void h() {
        i();
        if (this.r) {
            if (this.C != null) {
                this.C.d();
            }
            this.C = null;
            CBLogging.e("CBImpression", "Destroying the view and view data");
        }
    }

    public void i() {
        if (this.s != null) {
            this.s.b();
            try {
                if (!(this.C == null || this.C.e() == null || this.C.e().getParent() == null)) {
                    this.s.removeView(this.C.e());
                }
            } catch (Exception e2) {
                CBLogging.a("CBImpression", "Exception raised while cleaning up views", e2);
                a.a(getClass(), "cleanUpViews", e2);
            }
            this.s = null;
        }
        if (this.C != null) {
            this.C.f();
        }
        CBLogging.e("CBImpression", "Destroying the view");
    }

    public CBImpressionError j() {
        try {
            if (this.C != null) {
                return this.C.c();
            }
        } catch (Exception e2) {
            a.a(getClass(), "tryCreatingView", e2);
        }
        return CBImpressionError.ERROR_CREATING_VIEW;
    }

    public e.a k() {
        if (this.C != null) {
            return this.C.e();
        }
        return null;
    }

    public void l() {
        if (this.C != null && this.C.e() != null) {
            this.C.e().setVisibility(8);
        }
    }

    public void a(Runnable runnable) {
        this.D = runnable;
    }

    public void m() {
        this.t = true;
    }

    public void n() {
        if (this.D != null) {
            this.D.run();
            this.D = null;
        }
        this.t = false;
    }

    public String o() {
        return this.p.q;
    }

    public void p() {
        this.k.c(this);
    }

    public boolean q() {
        if (this.C != null) {
            return this.C.l();
        }
        return false;
    }

    public void r() {
        this.x = false;
        if (this.C != null && this.y) {
            this.y = false;
            this.C.m();
        }
    }

    public void s() {
        this.x = false;
    }

    public void t() {
        if (this.C != null && !this.y) {
            this.y = true;
            this.C.n();
        }
    }

    public e u() {
        return this.C;
    }

    public boolean v() {
        return this.z;
    }

    public void w() {
        al alVar = new al("/api/video-complete", this.d, this.e, 2, null);
        alVar.a("location", (Object) this.m);
        alVar.a(MTGRewardVideoActivity.INTENT_REWARD, (Object) Integer.valueOf(this.p.v));
        alVar.a("currency-name", (Object) this.p.w);
        alVar.a("ad_id", (Object) o());
        alVar.a("force_close", (Object) Boolean.valueOf(false));
        if (!this.p.r.isEmpty()) {
            alVar.a("cgn", (Object) this.p.r);
        }
        e u2 = k() != null ? u() : null;
        if (u2 != null) {
            float k2 = u2.k();
            float j2 = u2.j();
            CBLogging.a(getClass().getSimpleName(), String.format(Locale.US, "TotalDuration: %f PlaybackTime: %f", new Object[]{Float.valueOf(j2), Float.valueOf(k2)}));
            float f2 = j2 / 1000.0f;
            alVar.a("total_time", (Object) Float.valueOf(f2));
            if (k2 <= 0.0f) {
                alVar.a("playback_time", (Object) Float.valueOf(f2));
            } else {
                alVar.a("playback_time", (Object) Float.valueOf(k2 / 1000.0f));
            }
        }
        this.c.a(alVar);
        this.e.b(this.f1830a.a(this.p.m), o());
    }

    /* access modifiers changed from: 0000 */
    public void a(String str, JSONObject jSONObject) {
        Handler handler = this.f;
        com.chartboost.sdk.impl.c cVar = this.f1830a;
        cVar.getClass();
        handler.post(new com.chartboost.sdk.impl.c.a(1, this.m, null));
        if (b() && this.l == 2) {
            d c2 = this.g.c();
            if (c2 != null) {
                c2.b(this);
            }
        }
        if (!s.a().a((CharSequence) str)) {
            al alVar = new al("/api/click", this.d, this.e, 2, null);
            if (!this.p.q.isEmpty()) {
                alVar.a("ad_id", (Object) this.p.q);
            }
            if (!this.p.x.isEmpty()) {
                alVar.a("to", (Object) this.p.x);
            }
            if (!this.p.r.isEmpty()) {
                alVar.a("cgn", (Object) this.p.r);
            }
            if (!this.p.s.isEmpty()) {
                alVar.a("creative", (Object) this.p.s);
            }
            if (this.n == 1 || this.n == 2) {
                e eVar = (this.p.m != 0 || k() == null) ? (this.p.m != 1 || k() == null) ? null : (bh) u() : (x) u();
                if (eVar != null) {
                    float k2 = eVar.k();
                    float j2 = eVar.j();
                    CBLogging.a(getClass().getSimpleName(), String.format(Locale.US, "TotalDuration: %f PlaybackTime: %f", new Object[]{Float.valueOf(j2), Float.valueOf(k2)}));
                    float f2 = j2 / 1000.0f;
                    alVar.a("total_time", (Object) Float.valueOf(f2));
                    if (k2 <= 0.0f) {
                        alVar.a("playback_time", (Object) Float.valueOf(f2));
                    } else {
                        alVar.a("playback_time", (Object) Float.valueOf(k2 / 1000.0f));
                    }
                }
            }
            if (jSONObject != null) {
                alVar.a("click_coordinates", (Object) jSONObject);
            }
            alVar.a("location", (Object) this.m);
            if (x()) {
                alVar.a("retarget_reinstall", (Object) Boolean.valueOf(y()));
            }
            this.w = alVar;
            this.h.a(this, str, null);
        } else {
            this.h.a(this, false, str, CBClickError.URI_INVALID, null);
        }
        this.e.c(this.f1830a.a(this.p.m), this.m, o());
    }
}
