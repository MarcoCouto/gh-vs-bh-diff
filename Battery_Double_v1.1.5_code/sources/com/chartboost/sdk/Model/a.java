package com.chartboost.sdk.Model;

import com.facebook.share.internal.MessengerShareContentUtility;
import com.ironsource.sdk.constants.Constants.ParametersKeys;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.reward.player.MTGRewardVideoActivity;
import com.tapjoy.TJAdUnitConstants.String;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class a {
    public String A;
    public final String B;
    public b C;
    public final HashSet<String> D;
    public final JSONObject l;
    public int m;
    public final Map<String, b> n;
    public final Map<String, String> o;
    public final String p;
    public String q;
    public String r;
    public String s;
    public final String t;
    public final String u;
    public final int v;
    public final String w;
    public final String x;
    public final Map<String, List<String>> y;
    public final int z;

    public a() {
        this.l = null;
        this.m = 1;
        this.n = new HashMap();
        this.o = new HashMap();
        this.p = "dummy_template";
        this.q = "";
        this.r = "";
        this.s = "";
        this.t = "";
        this.u = "";
        this.v = 0;
        this.w = "";
        this.x = "";
        this.y = new HashMap();
        this.z = 0;
        this.A = "";
        this.B = "";
        this.C = new b("", "", "");
        this.D = new HashSet<>();
    }

    public a(int i, JSONObject jSONObject, boolean z2) throws JSONException {
        this.m = i;
        this.l = jSONObject;
        this.q = jSONObject.getString("ad_id");
        this.r = jSONObject.getString("cgn");
        this.s = jSONObject.getString("creative");
        this.t = jSONObject.optString("deep-link");
        this.u = jSONObject.getString("link");
        this.x = jSONObject.getString("to");
        this.z = jSONObject.optInt(String.SPLIT_VIEW_ANIMATION);
        this.A = jSONObject.optString("media-type");
        this.B = jSONObject.optString("name");
        this.n = new HashMap();
        this.o = new HashMap();
        this.y = new HashMap();
        this.D = new HashSet<>();
        if (i == 1) {
            JSONObject jSONObject2 = jSONObject.getJSONObject(ParametersKeys.WEB_VIEW);
            JSONArray jSONArray = jSONObject2.getJSONArray(MessengerShareContentUtility.ELEMENTS);
            String str = "";
            int i2 = 0;
            for (int i3 = 0; i3 < jSONArray.length(); i3++) {
                JSONObject jSONObject3 = jSONArray.getJSONObject(i3);
                String string = jSONObject3.getString("name");
                String optString = jSONObject3.optString("param");
                String string2 = jSONObject3.getString("type");
                String string3 = jSONObject3.getString("value");
                if (string2.equals("param")) {
                    this.o.put(optString, string3);
                    if (string.equals(CampaignEx.JSON_KEY_REWARD_AMOUNT)) {
                        i2 = Integer.valueOf(string3).intValue();
                    } else if (string.equals("reward_currency")) {
                        str = string3;
                    }
                } else {
                    if (string2.equals(String.HTML) && optString.isEmpty()) {
                        optString = TtmlNode.TAG_BODY;
                    } else if (optString.isEmpty()) {
                        optString = string;
                    }
                    this.n.put(optString, new b(string2, string, string3));
                }
            }
            this.v = i2;
            this.w = str;
            this.C = (b) this.n.get(TtmlNode.TAG_BODY);
            if (this.C != null) {
                this.p = jSONObject2.getString("template");
                JSONObject optJSONObject = jSONObject.optJSONObject(EventEntry.TABLE_NAME);
                if (optJSONObject != null) {
                    Iterator keys = optJSONObject.keys();
                    while (keys.hasNext()) {
                        String str2 = (String) keys.next();
                        JSONArray jSONArray2 = optJSONObject.getJSONArray(str2);
                        ArrayList arrayList = new ArrayList();
                        for (int i4 = 0; i4 < jSONArray2.length(); i4++) {
                            arrayList.add(jSONArray2.getString(i4));
                        }
                        this.y.put(str2, arrayList);
                    }
                }
                JSONArray optJSONArray = jSONObject.optJSONArray("certification_providers");
                if (optJSONArray != null) {
                    for (int i5 = 0; i5 < optJSONArray.length(); i5++) {
                        this.D.add(optJSONArray.getString(i5));
                    }
                    return;
                }
                return;
            }
            throw new RuntimeException("WebView AdUnit does not have a template html body asset");
        }
        if (z2) {
            String string4 = jSONObject.getJSONObject("icons").getString("lg");
            this.n.put("lg", new b("inPlayIcons", string4.substring(string4.lastIndexOf("/") + 1), string4));
            this.v = 0;
            this.w = "";
        } else {
            JSONObject jSONObject4 = jSONObject.getJSONObject("assets");
            Iterator keys2 = jSONObject4.keys();
            while (keys2.hasNext()) {
                String str3 = (String) keys2.next();
                JSONObject jSONObject5 = jSONObject4.getJSONObject(str3);
                String str4 = (str3.equals("video-portrait") || str3.equals("video-landscape")) ? "videos" : "images";
                String optString2 = jSONObject5.optString("id", null);
                if (optString2 == null) {
                    StringBuilder sb = new StringBuilder();
                    sb.append(jSONObject5.getString("checksum"));
                    sb.append(".png");
                    optString2 = sb.toString();
                }
                this.n.put(str3, new b(str4, optString2, jSONObject5.getString("url")));
            }
            this.v = jSONObject.optInt(MTGRewardVideoActivity.INTENT_REWARD);
            this.w = jSONObject.optString("currency-name");
        }
        this.C = null;
        this.p = "";
    }
}
