package com.chartboost.sdk.Model;

import android.text.TextUtils;
import com.appodealx.sdk.utils.RequestInfoKeys;
import com.chartboost.sdk.Libraries.e;
import com.chartboost.sdk.Tracking.a;
import com.facebook.share.internal.MessengerShareContentUtility;
import com.tapjoy.TJAdUnitConstants.String;
import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class b {

    /* renamed from: a reason: collision with root package name */
    public final String f1829a;
    public final String b;
    public final String c;

    public b(String str, String str2, String str3) {
        this.f1829a = str;
        this.b = str2;
        this.c = str3;
    }

    private static Map<String, b> b(JSONObject jSONObject) throws JSONException {
        HashMap hashMap = new HashMap();
        Iterator keys = jSONObject.keys();
        while (keys.hasNext()) {
            String str = (String) keys.next();
            JSONObject jSONObject2 = jSONObject.getJSONObject(str);
            Iterator keys2 = jSONObject2.keys();
            while (keys2.hasNext()) {
                String str2 = (String) keys2.next();
                JSONObject jSONObject3 = jSONObject2.getJSONObject(str2);
                hashMap.put(str2, new b(str, jSONObject3.getString("filename"), jSONObject3.getString("url")));
            }
        }
        return hashMap;
    }

    public static Map<String, b> a(JSONObject jSONObject) {
        HashMap hashMap = new HashMap();
        try {
            JSONArray jSONArray = jSONObject.getJSONArray("videos");
            int length = jSONArray.length();
            for (int i = 0; i < length; i++) {
                try {
                    JSONObject jSONObject2 = jSONArray.getJSONObject(i);
                    String string = jSONObject2.getString("id");
                    hashMap.put(string, new b("videos", string, jSONObject2.getString("video")));
                } catch (JSONException e) {
                    a.a(b.class, "deserializeNativeVideos (file)", (Exception) e);
                }
            }
        } catch (JSONException e2) {
            a.a(b.class, "deserializeNativeVideos (videos array)", (Exception) e2);
        }
        return hashMap;
    }

    private static JSONObject a(JSONArray jSONArray) throws JSONException {
        JSONObject a2 = e.a(new e.a[0]);
        for (int i = 0; i < jSONArray.length(); i++) {
            JSONObject jSONObject = jSONArray.getJSONObject(i);
            String optString = jSONObject.optString("name");
            String optString2 = jSONObject.optString("type");
            String optString3 = jSONObject.optString("value");
            String optString4 = jSONObject.optString("param");
            if (!optString2.equals("param") && optString4.isEmpty()) {
                JSONObject optJSONObject = a2.optJSONObject(optString2);
                if (optJSONObject == null) {
                    optJSONObject = e.a(new e.a[0]);
                    a2.put(optString2, optJSONObject);
                }
                optJSONObject.put(optString2.equals(String.HTML) ? TtmlNode.TAG_BODY : optString, e.a(e.a("filename", (Object) optString), e.a("url", (Object) optString3)));
            }
        }
        return a2;
    }

    public static Map<String, b> a(JSONObject jSONObject, int i) {
        HashMap hashMap = new HashMap();
        try {
            JSONObject jSONObject2 = jSONObject.getJSONObject("cache_assets");
            Iterator keys = jSONObject2.keys();
            while (keys.hasNext()) {
                String str = (String) keys.next();
                int i2 = 0;
                if (str.equals("templates")) {
                    JSONArray optJSONArray = jSONObject2.optJSONArray("templates");
                    if (optJSONArray != null) {
                        int min = Math.min(i, optJSONArray.length());
                        while (i2 < min) {
                            for (Entry value : b(a(optJSONArray.getJSONObject(i2).getJSONArray(MessengerShareContentUtility.ELEMENTS))).entrySet()) {
                                b bVar = (b) value.getValue();
                                hashMap.put(bVar.b, bVar);
                            }
                            i2++;
                        }
                    }
                } else {
                    JSONArray jSONArray = jSONObject2.getJSONArray(str);
                    while (i2 < jSONArray.length()) {
                        JSONObject jSONObject3 = jSONArray.getJSONObject(i2);
                        String string = jSONObject3.getString("name");
                        hashMap.put(string, new b(str, string, jSONObject3.getString("value")));
                        i2++;
                    }
                }
            }
        } catch (JSONException e) {
            a.a(b.class, "v2PrefetchToAssets", (Exception) e);
        }
        return hashMap;
    }

    public static Map<String, b> a(String str) {
        HashMap hashMap = new HashMap();
        try {
            JSONArray optJSONArray = new JSONObject(str).optJSONArray("seatbid");
            if (optJSONArray != null) {
                for (int i = 0; i < optJSONArray.length(); i++) {
                    JSONArray optJSONArray2 = optJSONArray.optJSONObject(i).optJSONArray("bid");
                    if (optJSONArray2 != null) {
                        for (int i2 = 0; i2 < optJSONArray2.length(); i2++) {
                            JSONObject jSONObject = optJSONArray2.getJSONObject(i2).getJSONObject(RequestInfoKeys.EXT);
                            if (jSONObject != null) {
                                JSONObject optJSONObject = jSONObject.optJSONObject("bidder");
                                if (optJSONObject != null) {
                                    String string = optJSONObject.getString("template");
                                    if (TextUtils.isEmpty(string)) {
                                        return null;
                                    }
                                    String substring = string.substring(string.lastIndexOf(47) + 1);
                                    hashMap.put(substring, new b(String.HTML, substring, string));
                                } else {
                                    continue;
                                }
                            }
                        }
                        continue;
                    }
                }
            }
        } catch (JSONException e) {
            a.a(b.class, "bidResponseToAssets", (Exception) e);
        }
        return hashMap;
    }

    public File a(File file) {
        StringBuilder sb = new StringBuilder();
        sb.append(this.f1829a);
        sb.append("/");
        sb.append(this.b);
        return new File(file, sb.toString());
    }
}
