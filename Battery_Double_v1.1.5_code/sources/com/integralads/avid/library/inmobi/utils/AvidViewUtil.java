package com.integralads.avid.library.inmobi.utils;

import android.os.Build.VERSION;
import android.view.View;
import com.github.mikephil.charting.utils.Utils;

public class AvidViewUtil {
    public static boolean isViewVisible(View view) {
        if (view.getVisibility() != 0) {
            return false;
        }
        if (VERSION.SDK_INT < 11 || ((double) view.getAlpha()) > Utils.DOUBLE_EPSILON) {
            return true;
        }
        return false;
    }
}
