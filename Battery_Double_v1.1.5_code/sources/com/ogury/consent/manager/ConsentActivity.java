package com.ogury.consent.manager;

import android.app.Activity;
import android.content.Context;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import com.ogury.consent.manager.util.consent.ConsentException;
import com.ogury.consent.manager.util.consent.ansi;
import com.ogury.consent.manager.util.consent.cocoartf1671;
import java.util.HashMap;

public class ConsentActivity extends Activity {

    /* renamed from: a reason: collision with root package name */
    public static final rtf1 f3194a = new rtf1(null);
    private final LayoutParams b = new LayoutParams(-1, -1);
    private HashMap c;

    public static final class rtf1 {
        private rtf1() {
        }

        public /* synthetic */ rtf1(tx6480 tx6480) {
            this();
        }
    }

    public void _$_clearFindViewByIdCache() {
        if (this.c != null) {
            this.c.clear();
        }
    }

    public View _$_findCachedViewById(int i) {
        if (this.c == null) {
            this.c = new HashMap();
        }
        View view = (View) this.c.get(Integer.valueOf(i));
        if (view != null) {
            return view;
        }
        View findViewById = findViewById(i);
        this.c.put(Integer.valueOf(i), findViewById);
        return findViewById;
    }

    public void onBackPressed() {
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (bundle != null) {
            finish();
            return;
        }
        ansi ansi = ansi.f3236a;
        if (ansi.e() != null) {
            Context context = this;
            FrameLayout frameLayout = new FrameLayout(context, null);
            frameLayout.setLayoutParams(this.b);
            ansi ansi2 = ansi.f3236a;
            WebView e = ansi.e();
            if (e != null) {
                ansicpg1252 ansicpg1252 = ansicpg1252.b;
                e.setWebViewClient(new ansi(context, ansicpg1252.a()));
            }
            ansi ansi3 = ansi.f3236a;
            WebView e2 = ansi.e();
            if (VERSION.SDK_INT < 16 && e2 != null) {
                e2.setLayerType(1, null);
            }
            ansi ansi4 = ansi.f3236a;
            frameLayout.addView(ansi.e(), this.b);
            ansi ansi5 = ansi.f3236a;
            ansi.a((WebView) null);
            setContentView(frameLayout);
            return;
        }
        ansicpg1252 ansicpg12522 = ansicpg1252.b;
        ansicpg1252.a().a(new ConsentException("system-error", ""));
        cocoartf1671 cocoartf1671 = cocoartf1671.f3237a;
        cocoartf1671.a("cached webview has been destroyed");
        finish();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        ansi ansi = ansi.f3236a;
        ansi.a((WebView) null);
        super.onDestroy();
    }
}
