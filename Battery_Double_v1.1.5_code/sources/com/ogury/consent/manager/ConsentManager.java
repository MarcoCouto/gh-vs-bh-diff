package com.ogury.consent.manager;

import android.content.Context;

public final class ConsentManager {
    public static final ConsentManager INSTANCE = new ConsentManager();

    /* renamed from: a reason: collision with root package name */
    private static rtf1 f3195a = new rtf1();
    private static String b = "";
    private static String c = "";

    public enum Answer {
        FULL_APPROVAL,
        PARTIAL_APPROVAL,
        REFUSAL,
        NO_ANSWER
    }

    public enum Purpose {
        INFORMATION,
        PERSONALISATION,
        AD,
        CONTENT,
        MEASUREMENT
    }

    private ConsentManager() {
    }

    public static final void ask(Context context, String str, ConsentListener consentListener) {
        tx7200.b(context, "context");
        tx7200.b(str, "assetKey");
        tx7200.b(consentListener, "consentListener");
        if (!f3195a.a()) {
            rtf1.a(context, b);
            rtf1.a(c);
            f3195a.a(consentListener);
            f3195a.a(context, str, "ask");
        }
    }

    public static final void edit(Context context, String str, ConsentListener consentListener) {
        tx7200.b(context, "context");
        tx7200.b(str, "assetKey");
        tx7200.b(consentListener, "consentListener");
        if (!f3195a.a()) {
            rtf1.a(context, b);
            rtf1.a(c);
            f3195a.a(consentListener);
            f3195a.a(context, str, "edit");
        }
    }

    public static final String getIabString() {
        return rtf1.b();
    }

    public static final boolean isAccepted(String str) {
        tx7200.b(str, "vendorSlug");
        tx7200.b(str, "receiver$0");
        StringBuilder sb = new StringBuilder("\"");
        sb.append(str);
        sb.append("\"");
        return rtf1.b(sb.toString());
    }

    public static final boolean isPurposeAccepted(int i) {
        if (i < 0 || 4 < i) {
            return false;
        }
        Purpose purpose = i == Purpose.INFORMATION.ordinal() ? Purpose.INFORMATION : i == Purpose.PERSONALISATION.ordinal() ? Purpose.PERSONALISATION : i == Purpose.AD.ordinal() ? Purpose.AD : i == Purpose.CONTENT.ordinal() ? Purpose.CONTENT : Purpose.MEASUREMENT;
        return rtf1.a(purpose);
    }

    public static final boolean isPurposeAccepted(Purpose purpose) {
        tx7200.b(purpose, "purpose");
        return rtf1.a(purpose);
    }

    public final String getFakeBundleID() {
        return b;
    }

    public final String getFakeShowFormat() {
        return c;
    }

    public final void setClientConsentImpl(rtf1 rtf1) {
        tx7200.b(rtf1, "consentImpl");
        f3195a = rtf1;
    }
}
