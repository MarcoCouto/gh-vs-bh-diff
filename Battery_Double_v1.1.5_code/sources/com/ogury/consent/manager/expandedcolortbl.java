package com.ogury.consent.manager;

import com.facebook.share.internal.ShareConstants;
import com.ogury.consent.manager.rtf1.C0074rtf1;
import com.ogury.consent.manager.util.consent.ConsentException;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.Closeable;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.net.URL;
import java.net.URLConnection;
import javax.net.ssl.HttpsURLConnection;

public final class expandedcolortbl {

    /* renamed from: a reason: collision with root package name */
    public static final rtf1 f3207a = new rtf1(null);

    public static final class rtf1 {
        private rtf1() {
        }

        public /* synthetic */ rtf1(tx6480 tx6480) {
            this();
        }
    }

    private static String a(InputStream inputStream) {
        Throwable th;
        Closeable bufferedReader = new BufferedReader(new InputStreamReader(inputStream, bbb.f3201a), 8192);
        try {
            Reader reader = (BufferedReader) bufferedReader;
            tx7200.b(reader, "receiver$0");
            StringWriter stringWriter = new StringWriter();
            C0074rtf1.a(reader, (Writer) stringWriter, 8192);
            String stringWriter2 = stringWriter.toString();
            tx7200.a((Object) stringWriter2, "buffer.toString()");
            tx5040.a(bufferedReader, null);
            return stringWriter2;
        } catch (Throwable th2) {
            tx5040.a(bufferedReader, th);
            throw th2;
        }
    }

    public static void a(margl1440 margl1440) {
        tx7200.b(margl1440, ShareConstants.WEB_DIALOG_RESULT_PARAM_REQUEST_ID);
        try {
            URLConnection openConnection = new URL(margl1440.c()).openConnection();
            if (openConnection != null) {
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) openConnection;
                httpsURLConnection.setReadTimeout(10000);
                httpsURLConnection.setConnectTimeout(150000);
                httpsURLConnection.setRequestMethod(margl1440.a());
                httpsURLConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                httpsURLConnection.setRequestProperty("Accept", "application/json");
                httpsURLConnection.setDoInput(true);
                httpsURLConnection.setDoOutput(true);
                OutputStream outputStream = httpsURLConnection.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
                bufferedWriter.write(margl1440.b());
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();
                httpsURLConnection.connect();
                int responseCode = httpsURLConnection.getResponseCode();
                com.ogury.consent.manager.util.consent.rtf1 d = margl1440.d();
                if (200 <= responseCode) {
                    if (299 >= responseCode) {
                        if (d != null) {
                            InputStream inputStream = httpsURLConnection.getInputStream();
                            tx7200.a((Object) inputStream, "conn.inputStream");
                            d.a(a(inputStream));
                        }
                        httpsURLConnection.disconnect();
                        return;
                    }
                }
                if (d != null) {
                    InputStream errorStream = httpsURLConnection.getErrorStream();
                    tx7200.a((Object) errorStream, "conn.errorStream");
                    d.a(new ConsentException(a(errorStream), "system-error"));
                }
                try {
                    httpsURLConnection.disconnect();
                } catch (Exception unused) {
                }
                return;
            }
            throw new viewkind0("null cannot be cast to non-null type javax.net.ssl.HttpsURLConnection");
        } catch (Exception unused2) {
            com.ogury.consent.manager.util.consent.rtf1 d2 = margl1440.d();
            if (d2 != null) {
                d2.a(new ConsentException("server-not-responding", ""));
            }
        }
    }
}
