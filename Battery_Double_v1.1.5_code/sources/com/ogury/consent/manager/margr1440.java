package com.ogury.consent.manager;

import android.content.Context;
import android.content.res.Resources;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Build.VERSION;
import android.util.DisplayMetrics;
import com.ironsource.mediationsdk.utils.GeneralPropertiesWorker;
import com.ironsource.mediationsdk.utils.ServerResponseWrapper;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.ogury.consent.manager.rtf1.C0074rtf1;
import com.ogury.consent.manager.util.consent.ansi;
import com.tapjoy.TJAdUnitConstants.String;
import java.util.Locale;
import org.json.JSONObject;

public final class margr1440 {

    /* renamed from: a reason: collision with root package name */
    public static final rtf1 f3218a = new rtf1(null);

    public static final class rtf1 {
        private rtf1() {
        }

        public /* synthetic */ rtf1(tx6480 tx6480) {
            this();
        }
    }

    public static String a(Context context, String str) {
        tx7200.b(context, "context");
        tx7200.b(str, ServerResponseWrapper.APP_KEY_FIELD);
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("assetType", "android");
        jSONObject.put("assetKey", str);
        ansi ansi = ansi.f3236a;
        jSONObject.put("deviceId", ansi.c().b());
        jSONObject.put(GeneralPropertiesWorker.SDK_VERSION, "1.1.7");
        String str2 = "connectivity";
        NetworkInfo a2 = C0074rtf1.a(context);
        jSONObject.put(str2, a2 != null ? a2.getTypeName() : null);
        String str3 = RequestParameters.DEVICE_MODEL;
        StringBuilder sb = new StringBuilder();
        sb.append(Build.MANUFACTURER);
        sb.append(" ");
        sb.append(Build.MODEL);
        String sb2 = sb.toString();
        if (sb2.length() > 32) {
            if (sb2 != null) {
                sb2 = sb2.substring(0, 31);
                tx7200.a((Object) sb2, "(this as java.lang.Strin…ing(startIndex, endIndex)");
            } else {
                throw new viewkind0("null cannot be cast to non-null type java.lang.String");
            }
        }
        jSONObject.put(str3, sb2);
        jSONObject.put("deviceOsVersion", String.valueOf(VERSION.SDK_INT));
        Context applicationContext = context.getApplicationContext();
        tx7200.a((Object) applicationContext, "context.applicationContext");
        Resources resources = applicationContext.getResources();
        tx7200.a((Object) resources, "context.applicationContext.resources");
        DisplayMetrics displayMetrics = resources.getDisplayMetrics();
        tx7200.a((Object) displayMetrics, "context.applicationConte….resources.displayMetrics");
        jSONObject.put("deviceScreenWidth", displayMetrics.widthPixels);
        jSONObject.put("deviceScreenHeight", displayMetrics.heightPixels);
        String str4 = String.BUNDLE;
        ansi ansi2 = ansi.f3236a;
        jSONObject.put(str4, ansi.c().a());
        StringBuilder sb3 = new StringBuilder();
        Locale locale = Locale.getDefault();
        tx7200.a((Object) locale, "Locale.getDefault()");
        sb3.append(locale.getLanguage());
        sb3.append("-");
        Locale locale2 = Locale.getDefault();
        tx7200.a((Object) locale2, "Locale.getDefault()");
        sb3.append(locale2.getCountry());
        jSONObject.put("locale", sb3.toString());
        jSONObject.put("deviceScreenDensity", displayMetrics.densityDpi);
        String jSONObject2 = jSONObject.toString();
        tx7200.a((Object) jSONObject2, "json.toString()");
        return jSONObject2;
    }
}
