package com.ogury.consent.manager;

import com.ogury.consent.manager.ConsentManager.Answer;

public final class colortbl {

    /* renamed from: a reason: collision with root package name */
    private String f3206a = "";
    private String b = "";
    private Answer c = Answer.NO_ANSWER;
    private String d = "";
    private String e = "";

    public final String a() {
        return this.f3206a;
    }

    public final void a(Answer answer) {
        tx7200.b(answer, "<set-?>");
        this.c = answer;
    }

    public final void a(String str) {
        tx7200.b(str, "<set-?>");
        this.f3206a = str;
    }

    public final String b() {
        return this.b;
    }

    public final void b(String str) {
        tx7200.b(str, "<set-?>");
        this.b = str;
    }

    public final Answer c() {
        return this.c;
    }

    public final void c(String str) {
        tx7200.b(str, "<set-?>");
        this.d = str;
    }

    public final String d() {
        return this.d;
    }

    public final void d(String str) {
        tx7200.b(str, "<set-?>");
        this.e = str;
    }

    public final String e() {
        return this.e;
    }
}
