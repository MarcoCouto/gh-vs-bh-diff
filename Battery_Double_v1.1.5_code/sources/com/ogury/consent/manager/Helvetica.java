package com.ogury.consent.manager;

import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Looper;
import java.io.IOException;

public final class Helvetica {

    /* renamed from: a reason: collision with root package name */
    public static final Helvetica f3196a = new Helvetica();

    private Helvetica() {
    }

    public static tx2160 a(Context context) throws Exception {
        tx7200.b(context, "context");
        if (!tx7200.a((Object) Looper.myLooper(), (Object) Looper.getMainLooper())) {
            try {
                context.getPackageManager().getPackageInfo("com.android.vending", 0);
                return b(context);
            } catch (Exception e) {
                throw e;
            }
        } else {
            throw new IllegalStateException("Cannot be called from the main thread");
        }
    }

    private static tx2160 b(Context context) throws Exception {
        cocoartf1671 cocoartf1671 = new cocoartf1671();
        Intent intent = new Intent(AdvertisingInfoServiceStrategy.GOOGLE_PLAY_SERVICES_INTENT);
        intent.setPackage("com.google.android.gms");
        ServiceConnection serviceConnection = cocoartf1671;
        if (context.bindService(intent, serviceConnection, 1)) {
            try {
                cocoasubrtf100 cocoasubrtf100 = new cocoasubrtf100(cocoartf1671.a());
                tx2160 tx2160 = new tx2160(cocoasubrtf100.a(), cocoasubrtf100.a(true));
                context.unbindService(serviceConnection);
                return tx2160;
            } catch (Exception e) {
                throw e;
            } catch (Throwable th) {
                context.unbindService(serviceConnection);
                throw th;
            }
        } else {
            throw new IOException("Google Play connection failed");
        }
    }
}
