package com.ogury.consent.manager;

import android.content.Context;
import com.facebook.internal.FacebookRequestErrorClassification;
import com.ogury.consent.manager.rtf1.C0074rtf1;

public final class aaa {
    public static /* synthetic */ int a(CharSequence charSequence, char c, int i, boolean z, int i2, Object obj) {
        boolean z2;
        if ((i2 & 2) != 0) {
            i = 0;
        }
        tx7200.b(charSequence, "receiver$0");
        boolean z3 = charSequence instanceof String;
        if (z3) {
            return ((String) charSequence).indexOf(46, i);
        }
        char[] cArr = {'.'};
        tx7200.b(charSequence, "receiver$0");
        tx7200.b(cArr, "chars");
        if (z3) {
            tx7200.b(cArr, "receiver$0");
            return ((String) charSequence).indexOf(cArr[0], i);
        }
        if (i < 0) {
            i = 0;
        }
        tx7200.b(charSequence, "receiver$0");
        int length = charSequence.length() - 1;
        if (i <= length) {
            while (true) {
                char charAt = charSequence.charAt(i);
                int i3 = 0;
                while (true) {
                    if (i3 > 0) {
                        z2 = false;
                        break;
                    } else if (a(cArr[i3], charAt, false)) {
                        z2 = true;
                        break;
                    } else {
                        i3++;
                    }
                }
                if (!z2) {
                    if (i == length) {
                        break;
                    }
                    i++;
                } else {
                    return i;
                }
            }
        }
        return -1;
    }

    private static /* synthetic */ int a(CharSequence charSequence, CharSequence charSequence2, int i, int i2, boolean z, boolean z2, int i3, Object obj) {
        boolean z3;
        int length = charSequence.length();
        if (i2 > length) {
            i2 = length;
        }
        partightenfactor0 cf0 = new cf0(0, i2);
        if (!(charSequence instanceof String) || !(charSequence2 instanceof String)) {
            int a2 = cf0.a();
            int b = cf0.b();
            int c = cf0.c();
            if (c <= 0 ? a2 >= b : a2 <= b) {
                while (true) {
                    int length2 = charSequence2.length();
                    tx7200.b(charSequence2, "receiver$0");
                    tx7200.b(charSequence, FacebookRequestErrorClassification.KEY_OTHER);
                    if (a2 >= 0 && charSequence2.length() - length2 >= 0 && a2 <= charSequence.length() - length2) {
                        int i4 = 0;
                        while (true) {
                            if (i4 >= length2) {
                                z3 = true;
                                break;
                            } else if (!a(charSequence2.charAt(i4 + 0), charSequence.charAt(a2 + i4), false)) {
                                break;
                            } else {
                                i4++;
                            }
                        }
                    }
                    z3 = false;
                    if (!z3) {
                        if (a2 == b) {
                            break;
                        }
                        a2 += c;
                    } else {
                        return a2;
                    }
                }
            }
        } else {
            int a3 = cf0.a();
            int b2 = cf0.b();
            int c2 = cf0.c();
            if (c2 <= 0 ? a3 >= b2 : a3 <= b2) {
                while (true) {
                    String str = (String) charSequence2;
                    String str2 = (String) charSequence;
                    int length3 = charSequence2.length();
                    tx7200.b(str, "receiver$0");
                    tx7200.b(str2, FacebookRequestErrorClassification.KEY_OTHER);
                    if (!str.regionMatches(0, str2, a3, length3)) {
                        if (a3 == b2) {
                            break;
                        }
                        a3 += c2;
                    } else {
                        return a3;
                    }
                }
            }
        }
        return -1;
    }

    private static int a(CharSequence charSequence, String str, int i, boolean z) {
        tx7200.b(charSequence, "receiver$0");
        tx7200.b(str, "string");
        if (charSequence instanceof String) {
            return ((String) charSequence).indexOf(str, 0);
        }
        return a(charSequence, str, 0, charSequence.length(), false, false, 16, null);
    }

    public static /* synthetic */ String a(String str, String str2, String str3, int i, Object obj) {
        tx7200.b(str, "receiver$0");
        tx7200.b(str2, "delimiter");
        tx7200.b(str, "missingDelimiterValue");
        int a2 = a(str, str2, 0, false);
        if (a2 == -1) {
            return str;
        }
        String substring = str.substring(a2 + str2.length(), str.length());
        tx7200.a((Object) substring, "(this as java.lang.Strin…ing(startIndex, endIndex)");
        return substring;
    }

    private static boolean a(char c, char c2, boolean z) {
        return c == c2;
    }

    public static boolean a(Context context) {
        tx7200.b(context, "context");
        return C0074rtf1.b(context);
    }

    public static /* synthetic */ boolean a(CharSequence charSequence, CharSequence charSequence2, boolean z, int i, Object obj) {
        tx7200.b(charSequence, "receiver$0");
        tx7200.b(charSequence2, FacebookRequestErrorClassification.KEY_OTHER);
        if (charSequence2 instanceof String) {
            return a(charSequence, (String) charSequence2, 0, false) >= 0;
        }
        return a(charSequence, charSequence2, 0, charSequence.length(), false, false, 16, null) >= 0;
    }

    public static /* synthetic */ boolean a(String str, String str2, boolean z, int i, Object obj) {
        tx7200.b(str, "receiver$0");
        tx7200.b(str2, "prefix");
        return str.startsWith(str2);
    }
}
