package com.ogury.consent.manager;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.webkit.WebSettings;
import android.webkit.WebView;
import com.ironsource.mediationsdk.utils.ServerResponseWrapper;
import com.ironsource.sdk.constants.Constants.ParametersKeys;
import com.ogury.consent.manager.util.consent.ConsentException;
import com.ogury.consent.manager.util.consent.cocoartf1671;
import java.util.List;

public final class ansicpg1252 {

    /* renamed from: a reason: collision with root package name */
    public static com.ogury.consent.manager.util.consent.rtf1 f3198a;
    public static final ansicpg1252 b = new ansicpg1252();
    private static final Handler c = new Handler();

    static final class ansi implements Runnable {

        /* renamed from: a reason: collision with root package name */
        final /* synthetic */ boolean f3199a;
        final /* synthetic */ com.ogury.consent.manager.util.consent.rtf1 b;

        ansi(boolean z, com.ogury.consent.manager.util.consent.rtf1 rtf1) {
            this.f3199a = z;
            this.b = rtf1;
        }

        public final void run() {
            if (!this.f3199a) {
                ansicpg1252.a(this.b, "timeout-error");
            }
        }
    }

    public static final class rtf1 implements com.ogury.consent.manager.util.consent.rtf1 {

        /* renamed from: a reason: collision with root package name */
        final /* synthetic */ com.ogury.consent.manager.tx8640.rtf1 f3200a;
        final /* synthetic */ com.ogury.consent.manager.tx8640.rtf1 b;
        final /* synthetic */ Context c;

        rtf1(com.ogury.consent.manager.tx8640.rtf1 rtf1, com.ogury.consent.manager.tx8640.rtf1 rtf12, Context context) {
            this.f3200a = rtf1;
            this.b = rtf12;
            this.c = context;
        }

        public final void a(ConsentException consentException) {
            tx7200.b(consentException, "exception");
            ansicpg1252 ansicpg1252 = ansicpg1252.b;
            ansicpg1252.a().a(consentException);
        }

        public final void a(String str) {
            tx7200.b(str, ServerResponseWrapper.RESPONSE_FIELD);
            if (tx7200.a((Object) str, (Object) ParametersKeys.READY)) {
                this.f3200a.f3235a = true;
                ansicpg1252 ansicpg1252 = ansicpg1252.b;
                boolean z = this.b.f3235a;
                ansicpg1252 ansicpg12522 = ansicpg1252.b;
                ansicpg1252.a(z, ansicpg1252.a(), 20000);
                return;
            }
            if (tx7200.a((Object) str, (Object) "success")) {
                this.b.f3235a = true;
                if (!ansicpg1252.a(ansicpg1252.b, this.c)) {
                    com.ogury.consent.manager.ConsentActivity.rtf1 rtf1 = ConsentActivity.f3194a;
                    Context context = this.c;
                    tx7200.b(context, "context");
                    context.startActivity(new Intent(context, ConsentActivity.class));
                } else {
                    ansicpg1252.a(ansicpg1252.a(), "app-in-background");
                }
                ansicpg1252 ansicpg12523 = ansicpg1252.b;
                ansicpg1252.b().removeCallbacksAndMessages(null);
            }
        }
    }

    private ansicpg1252() {
    }

    public static com.ogury.consent.manager.util.consent.rtf1 a() {
        com.ogury.consent.manager.util.consent.rtf1 rtf12 = f3198a;
        if (rtf12 == null) {
            tx7200.a("gatewayCallback");
        }
        return rtf12;
    }

    public static void a(Context context, com.ogury.consent.manager.util.consent.rtf1 rtf12) {
        tx7200.b(context, "context");
        tx7200.b(rtf12, "callback");
        f3198a = rtf12;
        com.ogury.consent.manager.tx8640.rtf1 rtf13 = new com.ogury.consent.manager.tx8640.rtf1();
        rtf13.f3235a = false;
        com.ogury.consent.manager.tx8640.rtf1 rtf14 = new com.ogury.consent.manager.tx8640.rtf1();
        rtf14.f3235a = false;
        try {
            WebView webView = new WebView(context.getApplicationContext(), null);
            WebSettings settings = webView.getSettings();
            tx7200.a((Object) settings, "webview.settings");
            settings.setJavaScriptEnabled(true);
            com.ogury.consent.manager.util.consent.ansi ansi2 = com.ogury.consent.manager.util.consent.ansi.f3236a;
            com.ogury.consent.manager.util.consent.ansi.a(webView);
            webView.setWebViewClient(new ansi(context, new rtf1(rtf13, rtf14, context)));
            a(rtf13.f3235a, rtf12, 30000);
            webView.loadUrl("https://consent-form.ogury.co/?assetType=android");
        } catch (Exception unused) {
            cocoartf1671 cocoartf1671 = cocoartf1671.f3237a;
            cocoartf1671.a("cannot create webview");
            com.ogury.consent.manager.util.consent.rtf1 rtf15 = f3198a;
            if (rtf15 == null) {
                tx7200.a("gatewayCallback");
            }
            a(rtf15, "system-error");
        }
    }

    /* access modifiers changed from: private */
    public static void a(com.ogury.consent.manager.util.consent.rtf1 rtf12, String str) {
        com.ogury.consent.manager.util.consent.ansi ansi2 = com.ogury.consent.manager.util.consent.ansi.f3236a;
        com.ogury.consent.manager.util.consent.ansi.a((WebView) null);
        rtf12.a(new ConsentException(str, ""));
    }

    /* access modifiers changed from: private */
    public static void a(boolean z, com.ogury.consent.manager.util.consent.rtf1 rtf12, long j) {
        c.postDelayed(new ansi(z, rtf12), j);
    }

    public static final /* synthetic */ boolean a(ansicpg1252 ansicpg1252, Context context) {
        Object systemService = context.getSystemService("activity");
        if (systemService != null) {
            List<RunningAppProcessInfo> runningAppProcesses = ((ActivityManager) systemService).getRunningAppProcesses();
            if (runningAppProcesses == null) {
                return false;
            }
            String packageName = context.getPackageName();
            for (RunningAppProcessInfo runningAppProcessInfo : runningAppProcesses) {
                if (runningAppProcessInfo.importance == 100 && tx7200.a((Object) runningAppProcessInfo.processName, (Object) packageName)) {
                    return false;
                }
            }
            return true;
        }
        throw new viewkind0("null cannot be cast to non-null type android.app.ActivityManager");
    }

    public static Handler b() {
        return c;
    }
}
