package com.ogury.consent.manager;

public final class tx2160 {

    /* renamed from: a reason: collision with root package name */
    private final String f3229a;
    private final boolean b;

    public static final class rtf1 extends Thread {

        /* renamed from: a reason: collision with root package name */
        final /* synthetic */ tx5760 f3230a;

        rtf1(tx5760 tx5760) {
            this.f3230a = tx5760;
        }

        public final void run() {
            this.f3230a.a();
        }
    }

    public tx2160(String str, boolean z) {
        tx7200.b(str, "id");
        this.f3229a = str;
        this.b = z;
    }

    public static final Thread a(boolean z, boolean z2, ClassLoader classLoader, String str, int i, tx5760<tx720> tx5760) {
        tx7200.b(tx5760, "block");
        rtf1 rtf12 = new rtf1(tx5760);
        rtf12.start();
        return rtf12;
    }

    public final String a() {
        return this.f3229a;
    }
}
