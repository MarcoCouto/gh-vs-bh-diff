package com.ogury.consent.manager;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences.Editor;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.util.Base64;
import android.webkit.WebView;
import com.amazon.device.ads.AdConstants;
import com.ironsource.mediationsdk.utils.ServerResponseWrapper;
import com.ironsource.sdk.constants.Constants.ParametersKeys;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.ogury.consent.manager.rtf1.C0074rtf1;
import com.ogury.consent.manager.util.consent.ConsentException;
import com.ogury.consent.manager.util.consent.ansicpg1252;
import com.ogury.consent.manager.util.consent.cocoartf1671;
import java.net.URLDecoder;
import java.util.Date;
import org.json.JSONObject;

public final class f0 {

    /* renamed from: a reason: collision with root package name */
    public static final rtf1 f3208a = new rtf1(null);

    public static final class ansi implements fswiss {

        /* renamed from: a reason: collision with root package name */
        final /* synthetic */ com.ogury.consent.manager.util.consent.rtf1 f3209a;
        final /* synthetic */ String b;
        final /* synthetic */ Context c;

        static final class rtf1 implements Runnable {

            /* renamed from: a reason: collision with root package name */
            final /* synthetic */ ansi f3210a;
            final /* synthetic */ String b;

            rtf1(ansi ansi, String str) {
                this.f3210a = ansi;
                this.b = str;
            }

            public final void run() {
                com.ogury.consent.manager.util.consent.ansi ansi = com.ogury.consent.manager.util.consent.ansi.f3236a;
                com.ogury.consent.manager.util.consent.ansi.c().b(this.b);
                this.f3210a.f3209a.a(this.f3210a.b);
                new tx8640();
                com.ogury.consent.manager.util.consent.ansi ansi2 = com.ogury.consent.manager.util.consent.ansi.f3236a;
                String d = com.ogury.consent.manager.util.consent.ansi.a().d();
                Context context = this.f3210a.c;
                tx7200.b(d, "iabString");
                tx7200.b(context, "context");
                Editor edit = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext()).edit();
                edit.putString(AdConstants.IABCONSENT_CONSENT_STRING, d);
                edit.putBoolean("IABConsent_CMPPresent", true);
                edit.apply();
                new fonttbl();
                fonttbl.a("", this.f3210a.c);
            }
        }

        ansi(com.ogury.consent.manager.util.consent.rtf1 rtf12, String str, Context context) {
            this.f3209a = rtf12;
            this.b = str;
            this.c = context;
        }

        public final void a(String str) {
            tx7200.b(str, "aaid");
            new Handler(Looper.getMainLooper()).post(new rtf1(this, str));
        }
    }

    public static final class rtf1 {
        private rtf1() {
        }

        public /* synthetic */ rtf1(tx6480 tx6480) {
            this();
        }
    }

    private static void a(Context context) {
        if (context instanceof ConsentActivity) {
            ((ConsentActivity) context).finish();
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x012d  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x013f  */
    public static void a(String str, Context context, com.ogury.consent.manager.util.consent.rtf1 rtf12, WebView webView) {
        String str2;
        tx7200.b(str, "url");
        tx7200.b(context, "context");
        tx7200.b(rtf12, "callback");
        tx7200.b(webView, "webView");
        String lowerCase = str.toLowerCase();
        tx7200.a((Object) lowerCase, "(this as java.lang.String).toLowerCase()");
        boolean z = false;
        if (aaa.a(lowerCase, "https://ogyconsent", false, 2, (Object) null)) {
            String substring = str.substring(20);
            tx7200.a((Object) substring, "(this as java.lang.String).substring(startIndex)");
            if (aaa.a(substring, "consent=", false, 2, (Object) null)) {
                new ansicpg1252();
                tx7200.b(substring, RequestParameters.CONSENT);
                String decode = URLDecoder.decode(substring, "UTF-8");
                if (decode != null) {
                    JSONObject a2 = C0074rtf1.a(aaa.a(decode, "consent=", (String) null, 2, (Object) null));
                    if (a2 != null) {
                        JSONObject optJSONObject = a2.optJSONObject(ServerResponseWrapper.RESPONSE_FIELD);
                        if (optJSONObject != null) {
                            JSONObject optJSONObject2 = optJSONObject.optJSONObject(ServerResponseWrapper.RESPONSE_FIELD);
                            if (optJSONObject2 != null) {
                                com.ogury.consent.manager.util.consent.ansi ansi2 = com.ogury.consent.manager.util.consent.ansi.f3236a;
                                colortbl a3 = com.ogury.consent.manager.util.consent.ansi.a();
                                String optString = optJSONObject.optString("lastOpt");
                                tx7200.a((Object) optString, "responseObject.optString(\"lastOpt\")");
                                a3.a(C0074rtf1.b(optString));
                                com.ogury.consent.manager.util.consent.ansi ansi3 = com.ogury.consent.manager.util.consent.ansi.f3236a;
                                colortbl a4 = com.ogury.consent.manager.util.consent.ansi.a();
                                String optString2 = optJSONObject2.optString("iabString");
                                tx7200.a((Object) optString2, "consentResponseObject.optString(\"iabString\")");
                                a4.c(optString2);
                                com.ogury.consent.manager.util.consent.ansi ansi4 = com.ogury.consent.manager.util.consent.ansi.f3236a;
                                colortbl a5 = com.ogury.consent.manager.util.consent.ansi.a();
                                String optString3 = optJSONObject2.optString("purposes");
                                tx7200.a((Object) optString3, "consentResponseObject.optString(\"purposes\")");
                                a5.d(optString3);
                                com.ogury.consent.manager.util.consent.ansi ansi5 = com.ogury.consent.manager.util.consent.ansi.f3236a;
                                colortbl a6 = com.ogury.consent.manager.util.consent.ansi.a();
                                String optString4 = optJSONObject2.optString("refusedVendors");
                                tx7200.a((Object) optString4, "consentResponseObject.optString(\"refusedVendors\")");
                                a6.b(optString4);
                                com.ogury.consent.manager.util.consent.ansi ansi6 = com.ogury.consent.manager.util.consent.ansi.f3236a;
                                colortbl a7 = com.ogury.consent.manager.util.consent.ansi.a();
                                String optString5 = optJSONObject2.optString("acceptedVendors");
                                tx7200.a((Object) optString5, "consentResponseObject.optString(\"acceptedVendors\")");
                                a7.a(optString5);
                                JSONObject optJSONObject3 = a2.optJSONObject("sdk");
                                if (optJSONObject3 != null) {
                                    com.ogury.consent.manager.util.consent.ansi ansi7 = com.ogury.consent.manager.util.consent.ansi.f3236a;
                                    red255 c = com.ogury.consent.manager.util.consent.ansi.c();
                                    String optString6 = optJSONObject3.optString("crashReportUrl");
                                    tx7200.a((Object) optString6, "sdkObject.optString(\"crashReportUrl\")");
                                    c.c(optString6);
                                    if (optJSONObject3.has("cacheFor")) {
                                        com.ogury.consent.manager.util.consent.ansi ansi8 = com.ogury.consent.manager.util.consent.ansi.f3236a;
                                        com.ogury.consent.manager.util.consent.ansi.a(new Date().getTime() + (a2.optLong("cacheFor") * 1000));
                                    }
                                    com.ogury.consent.manager.util.consent.ansi ansi9 = com.ogury.consent.manager.util.consent.ansi.f3236a;
                                    str2 = com.ogury.consent.manager.util.consent.ansi.a().c().toString();
                                    if (!tx7200.a((Object) str2, (Object) "parsing-error")) {
                                        rtf12.a(new ConsentException(str2, ""));
                                        cocoartf1671 cocoartf1671 = cocoartf1671.f3237a;
                                        cocoartf1671.a("");
                                    } else {
                                        fcharset0 fcharset0 = fcharset0.f3211a;
                                        fcharset0.a(context, (fswiss) new ansi(rtf12, str2, context));
                                    }
                                    a(context);
                                }
                            }
                        }
                    }
                }
                str2 = "parsing-error";
                if (!tx7200.a((Object) str2, (Object) "parsing-error")) {
                }
                a(context);
            } else if (aaa.a(substring, "ogyRedirect=", false, 2, (Object) null)) {
                try {
                    Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(aaa.a(substring, "ogyRedirect=", (String) null, 2, (Object) null)));
                    if (context.getPackageManager().queryIntentActivities(intent, 65536).size() > 0) {
                        z = true;
                    }
                    if (z) {
                        context.startActivity(intent);
                    }
                } catch (Exception e) {
                    cocoartf1671 cocoartf16712 = cocoartf1671.f3237a;
                    cocoartf1671.a((Throwable) e);
                }
            } else {
                if (aaa.a(substring, "error=", false, 2, (Object) null)) {
                    rtf12.a(new ConsentException("form-error", ""));
                    a(context);
                }
            }
        } else {
            CharSequence charSequence = str;
            if (aaa.a(charSequence, (CharSequence) "?ready", false, 2, (Object) null)) {
                webView.setVisibility(0);
                StringBuilder sb = new StringBuilder("javascript:(function(){ogFormBridge.init(\"");
                com.ogury.consent.manager.util.consent.ansi ansi10 = com.ogury.consent.manager.util.consent.ansi.f3236a;
                String a8 = com.ogury.consent.manager.util.consent.ansi.b().a();
                tx7200.b(a8, "receiver$0");
                byte[] bytes = a8.getBytes(bbb.f3201a);
                tx7200.a((Object) bytes, "(this as java.lang.String).getBytes(charset)");
                String encodeToString = Base64.encodeToString(bytes, 11);
                tx7200.a((Object) encodeToString, "Base64.encodeToString(by…ADDING + Base64.URL_SAFE)");
                sb.append(encodeToString);
                sb.append("\")})()");
                webView.loadUrl(sb.toString());
                rtf12.a(ParametersKeys.READY);
                return;
            }
            if (aaa.a(charSequence, (CharSequence) "success", false, 2, (Object) null)) {
                rtf12.a("success");
            }
        }
    }
}
