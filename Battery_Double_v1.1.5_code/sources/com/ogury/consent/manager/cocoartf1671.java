package com.ogury.consent.manager;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import java.util.concurrent.LinkedBlockingQueue;

public final class cocoartf1671 implements ServiceConnection {

    /* renamed from: a reason: collision with root package name */
    private final LinkedBlockingQueue<IBinder> f3204a = new LinkedBlockingQueue<>(1);
    private boolean b;

    public final IBinder a() throws InterruptedException {
        if (!this.b) {
            this.b = true;
            Object take = this.f3204a.take();
            if (take != null) {
                return (IBinder) take;
            }
            throw new viewkind0("null cannot be cast to non-null type android.os.IBinder");
        }
        throw new IllegalStateException();
    }

    public final void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        tx7200.b(componentName, "name");
        tx7200.b(iBinder, NotificationCompat.CATEGORY_SERVICE);
        try {
            this.f3204a.put(iBinder);
        } catch (InterruptedException unused) {
            Log.d(NotificationCompat.CATEGORY_SERVICE, "intrerrupted");
        }
    }

    public final void onServiceDisconnected(ComponentName componentName) {
        tx7200.b(componentName, "name");
    }
}
