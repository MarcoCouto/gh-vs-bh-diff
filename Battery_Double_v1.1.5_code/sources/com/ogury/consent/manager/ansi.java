package com.ogury.consent.manager;

import android.content.Context;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.facebook.share.internal.ShareConstants;
import com.ironsource.sdk.constants.Constants.ParametersKeys;
import com.ogury.consent.manager.util.consent.ConsentException;
import com.ogury.consent.manager.util.consent.rtf1;

public final class ansi extends WebViewClient {

    /* renamed from: a reason: collision with root package name */
    private final Context f3197a;
    private final rtf1 b;

    public ansi(Context context, rtf1 rtf1) {
        tx7200.b(context, "context");
        tx7200.b(rtf1, "consentCallback");
        this.f3197a = context;
        this.b = rtf1;
    }

    private final void a(String str) {
        rtf1 rtf1 = this.b;
        String str2 = "form-error";
        if (str == null) {
            str = "";
        }
        rtf1.a(new ConsentException(str2, str));
    }

    private final boolean a(String str, WebView webView) {
        new f0();
        f0.a(str, this.f3197a, this.b, webView);
        return true;
    }

    public final void onReceivedError(WebView webView, int i, String str, String str2) {
        a(str);
    }

    public final void onReceivedError(WebView webView, WebResourceRequest webResourceRequest, WebResourceError webResourceError) {
        tx7200.b(webView, ParametersKeys.VIEW);
        tx7200.b(webResourceRequest, ShareConstants.WEB_DIALOG_RESULT_PARAM_REQUEST_ID);
        tx7200.b(webResourceError, "error");
        a(webResourceError.getDescription().toString());
    }

    public final WebResourceResponse shouldInterceptRequest(WebView webView, WebResourceRequest webResourceRequest) {
        tx7200.b(webView, ParametersKeys.VIEW);
        tx7200.b(webResourceRequest, ShareConstants.WEB_DIALOG_RESULT_PARAM_REQUEST_ID);
        return null;
    }

    public final WebResourceResponse shouldInterceptRequest(WebView webView, String str) {
        tx7200.b(webView, ParametersKeys.VIEW);
        tx7200.b(str, "url");
        return null;
    }

    public final boolean shouldOverrideUrlLoading(WebView webView, WebResourceRequest webResourceRequest) {
        tx7200.b(webView, ParametersKeys.VIEW);
        tx7200.b(webResourceRequest, ShareConstants.WEB_DIALOG_RESULT_PARAM_REQUEST_ID);
        String uri = webResourceRequest.getUrl().toString();
        tx7200.a((Object) uri, "request.url.toString()");
        return a(uri, webView);
    }

    public final boolean shouldOverrideUrlLoading(WebView webView, String str) {
        tx7200.b(webView, ParametersKeys.VIEW);
        tx7200.b(str, "url");
        return a(str, webView);
    }
}
