package com.ogury.consent.manager;

import com.ogury.consent.manager.ConsentManager.Answer;
import com.ogury.consent.manager.util.consent.ConsentException;

public interface ConsentListener {
    void onComplete(Answer answer);

    void onError(ConsentException consentException);
}
