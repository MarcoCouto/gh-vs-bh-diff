package com.ogury.consent.manager;

import com.google.android.exoplayer2.C;
import java.nio.charset.Charset;

public final class bbb {

    /* renamed from: a reason: collision with root package name */
    public static final Charset f3201a;
    public static final Charset b;
    public static final Charset c;
    public static final Charset d;
    public static final Charset e;
    public static final Charset f;
    public static final bbb g = new bbb();

    static {
        Charset forName = Charset.forName("UTF-8");
        tx7200.a((Object) forName, "Charset.forName(\"UTF-8\")");
        f3201a = forName;
        Charset forName2 = Charset.forName("UTF-16");
        tx7200.a((Object) forName2, "Charset.forName(\"UTF-16\")");
        b = forName2;
        Charset forName3 = Charset.forName("UTF-16BE");
        tx7200.a((Object) forName3, "Charset.forName(\"UTF-16BE\")");
        c = forName3;
        Charset forName4 = Charset.forName("UTF-16LE");
        tx7200.a((Object) forName4, "Charset.forName(\"UTF-16LE\")");
        d = forName4;
        Charset forName5 = Charset.forName(C.ASCII_NAME);
        tx7200.a((Object) forName5, "Charset.forName(\"US-ASCII\")");
        e = forName5;
        Charset forName6 = Charset.forName("ISO-8859-1");
        tx7200.a((Object) forName6, "Charset.forName(\"ISO-8859-1\")");
        f = forName6;
    }

    private bbb() {
    }
}
