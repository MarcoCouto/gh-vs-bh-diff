package com.ogury.consent.manager;

import com.ogury.consent.manager.util.consent.rtf1;

public final class vieww10800 {

    /* renamed from: a reason: collision with root package name */
    private String f3238a = "";
    private String b = "";
    private String c = "";
    private rtf1 d;

    public final vieww10800 a(rtf1 rtf1) {
        this.d = rtf1;
        return this;
    }

    public final vieww10800 a(String str) {
        tx7200.b(str, "requestMethod");
        this.f3238a = str;
        return this;
    }

    public final String a() {
        return this.f3238a;
    }

    public final vieww10800 b(String str) {
        tx7200.b(str, "requestBody");
        this.b = str;
        return this;
    }

    public final String b() {
        return this.b;
    }

    public final vieww10800 c(String str) {
        tx7200.b(str, "url");
        this.c = str;
        return this;
    }

    public final String c() {
        return this.c;
    }

    public final rtf1 d() {
        return this.d;
    }
}
