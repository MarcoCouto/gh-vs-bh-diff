package com.ogury.consent.manager;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.Looper;
import com.google.android.gms.common.internal.ServiceSpecificExtraArgs.CastExtraArgs;
import com.ironsource.mediationsdk.utils.ServerResponseWrapper;
import com.ironsource.sdk.constants.Constants.ParametersKeys;
import com.ogury.consent.manager.ConsentManager.Answer;
import com.ogury.consent.manager.ConsentManager.Purpose;
import com.ogury.consent.manager.util.consent.ConsentException;
import com.ogury.crashreport.CrashConfig;
import com.ogury.crashreport.CrashReport;
import com.ogury.crashreport.SdkInfo;
import com.tapjoy.TJAdUnitConstants.String;
import java.io.Reader;
import java.io.Writer;
import java.util.Date;
import org.json.JSONObject;

public final class rtf1 {

    /* renamed from: a reason: collision with root package name */
    public static final C0074rtf1 f3222a = new C0074rtf1(null);
    private aaa b = new aaa();
    private final expandedcolortbl c = new expandedcolortbl();
    /* access modifiers changed from: private */
    public String d = "";
    /* access modifiers changed from: private */
    public Handler e = new Handler(Looper.getMainLooper());
    private fcharset0 f = fcharset0.f3211a;
    private fonttbl g = new fonttbl();
    private com.ogury.consent.manager.util.consent.rtf1 h;
    private blue255 i = new blue255(this.c);
    private tx5040 j = new tx5040();
    private ansicpg1252 k = ansicpg1252.b;
    /* access modifiers changed from: private */
    public viewh8400 l = new viewh8400();
    private boolean m;

    public static final class ansi implements com.ogury.consent.manager.util.consent.rtf1 {

        /* renamed from: a reason: collision with root package name */
        final /* synthetic */ rtf1 f3223a;
        final /* synthetic */ String b;
        final /* synthetic */ Context c;

        ansi(rtf1 rtf1, String str, Context context) {
            this.f3223a = rtf1;
            this.b = str;
            this.c = context;
        }

        public final void a(ConsentException consentException) {
            tx7200.b(consentException, "exception");
            this.f3223a.a(consentException.getMessage(), "server-not-responding");
        }

        public final void a(String str) {
            tx7200.b(str, ServerResponseWrapper.RESPONSE_FIELD);
            rtf1 rtf1 = this.f3223a;
            this.f3223a.l;
            rtf1.a(rtf1, viewh8400.a(str), this.b, this.c);
            rtf1.a(this.f3223a, this.c, this.f3223a.d);
        }
    }

    static final class ansicpg1252 implements Runnable {

        /* renamed from: a reason: collision with root package name */
        final /* synthetic */ rtf1 f3224a;
        final /* synthetic */ String b;
        final /* synthetic */ String c;

        ansicpg1252(rtf1 rtf1, String str, String str2) {
            this.f3224a = rtf1;
            this.b = str;
            this.c = str2;
        }

        public final void run() {
            com.ogury.consent.manager.util.consent.rtf1 c2 = rtf1.c(this.f3224a);
            String str = this.b;
            String str2 = this.c;
            if (str2 == null) {
                str2 = "";
            }
            c2.a(new ConsentException(str, str2));
        }
    }

    static final class cocoartf1671 implements Runnable {

        /* renamed from: a reason: collision with root package name */
        final /* synthetic */ rtf1 f3225a;
        final /* synthetic */ String b;
        final /* synthetic */ Context c;

        cocoartf1671(rtf1 rtf1, String str, Context context) {
            this.f3225a = rtf1;
            this.b = str;
            this.c = context;
        }

        public final void run() {
            rtf1.a(this.f3225a, this.b, this.c);
        }
    }

    public static final class cocoasubrtf100 implements fswiss {

        /* renamed from: a reason: collision with root package name */
        final /* synthetic */ rtf1 f3226a;
        final /* synthetic */ Context b;
        final /* synthetic */ String c;
        final /* synthetic */ String d;

        /* renamed from: com.ogury.consent.manager.rtf1$cocoasubrtf100$rtf1 reason: collision with other inner class name */
        static final class C0073rtf1 implements Runnable {

            /* renamed from: a reason: collision with root package name */
            final /* synthetic */ cocoasubrtf100 f3227a;
            final /* synthetic */ String b;

            C0073rtf1(cocoasubrtf100 cocoasubrtf100, String str) {
                this.f3227a = cocoasubrtf100;
                this.b = str;
            }

            public final void run() {
                com.ogury.consent.manager.util.consent.ansi ansi = com.ogury.consent.manager.util.consent.ansi.f3236a;
                com.ogury.consent.manager.util.consent.ansi.c().b(this.b);
                rtf1.b(this.f3227a.f3226a, this.f3227a.b, this.f3227a.c);
                rtf1.b(this.f3227a.f3226a, this.f3227a.d, this.f3227a.b);
            }
        }

        cocoasubrtf100(rtf1 rtf1, Context context, String str, String str2) {
            this.f3226a = rtf1;
            this.b = context;
            this.c = str;
            this.d = str2;
        }

        public final void a(String str) {
            tx7200.b(str, "aaid");
            this.f3226a.e.post(new C0073rtf1(this, str));
        }
    }

    public static final class fonttbl implements com.ogury.consent.manager.util.consent.rtf1 {

        /* renamed from: a reason: collision with root package name */
        final /* synthetic */ rtf1 f3228a;
        final /* synthetic */ ConsentListener b;

        fonttbl(rtf1 rtf1, ConsentListener consentListener) {
            this.f3228a = rtf1;
            this.b = consentListener;
        }

        public final void a(ConsentException consentException) {
            tx7200.b(consentException, "exception");
            this.f3228a.a(false);
            this.b.onError(consentException);
        }

        public final void a(String str) {
            tx7200.b(str, ServerResponseWrapper.RESPONSE_FIELD);
            this.f3228a.a(false);
            ConsentListener consentListener = this.b;
            com.ogury.consent.manager.util.consent.ansi ansi = com.ogury.consent.manager.util.consent.ansi.f3236a;
            consentListener.onComplete(com.ogury.consent.manager.util.consent.ansi.a().c());
        }
    }

    /* renamed from: com.ogury.consent.manager.rtf1$rtf1 reason: collision with other inner class name */
    public static final class C0074rtf1 {
        private C0074rtf1() {
        }

        public /* synthetic */ C0074rtf1(tx6480 tx6480) {
            this();
        }

        private static int a(int i, int i2) {
            int i3 = i % 1;
            return i3 >= 0 ? i3 : i3 + 1;
        }

        public static int a(int i, int i2, int i3) {
            return i >= i2 ? i2 : i2 - a(a(i2, 1) - a(i, 1), 1);
        }

        public static long a(Reader reader, Writer writer, int i) {
            tx7200.b(reader, "receiver$0");
            tx7200.b(writer, "out");
            char[] cArr = new char[8192];
            int read = reader.read(cArr);
            long j = 0;
            while (read >= 0) {
                writer.write(cArr, 0, read);
                j += (long) read;
                read = reader.read(cArr);
            }
            return j;
        }

        public static NetworkInfo a(Context context) {
            tx7200.b(context, "receiver$0");
            String str = "android.permission.ACCESS_NETWORK_STATE";
            tx7200.b(context, "receiver$0");
            tx7200.b(str, ParametersKeys.PERMISSION);
            if (!(context.checkCallingOrSelfPermission(str) == 0)) {
                return null;
            }
            Object systemService = context.getSystemService("connectivity");
            if (systemService != null) {
                return ((ConnectivityManager) systemService).getActiveNetworkInfo();
            }
            throw new viewkind0("null cannot be cast to non-null type android.net.ConnectivityManager");
        }

        public static JSONObject a(String str) {
            tx7200.b(str, "receiver$0");
            try {
                return new JSONObject(str);
            } catch (Exception unused) {
                return null;
            }
        }

        public static Answer b(String str) {
            tx7200.b(str, "receiver$0");
            return tx7200.a((Object) str, (Object) Answer.FULL_APPROVAL.toString()) ? Answer.FULL_APPROVAL : tx7200.a((Object) str, (Object) Answer.PARTIAL_APPROVAL.toString()) ? Answer.PARTIAL_APPROVAL : tx7200.a((Object) str, (Object) Answer.REFUSAL.toString()) ? Answer.REFUSAL : Answer.NO_ANSWER;
        }

        public static boolean b(Context context) {
            tx7200.b(context, "receiver$0");
            NetworkInfo a2 = a(context);
            return a2 != null && a2.isConnected();
        }
    }

    private final void a(Context context) {
        boolean z = false;
        if (!aaa.a(context)) {
            com.ogury.consent.manager.util.consent.rtf1 rtf1 = this.h;
            if (rtf1 == null) {
                tx7200.a("consentCallback");
            }
            rtf1.a(new ConsentException("no-internet-connection", ""));
        } else {
            com.ogury.consent.manager.util.consent.ansi ansi2 = com.ogury.consent.manager.util.consent.ansi.f3236a;
            if (com.ogury.consent.manager.util.consent.ansi.b().a().length() == 0) {
                com.ogury.consent.manager.util.consent.rtf1 rtf12 = this.h;
                if (rtf12 == null) {
                    tx7200.a("consentCallback");
                }
                rtf12.a(new ConsentException("system-error", ""));
                com.ogury.consent.manager.util.consent.cocoartf1671 cocoartf16712 = com.ogury.consent.manager.util.consent.cocoartf1671.f3237a;
                com.ogury.consent.manager.util.consent.cocoartf1671.a("missing consent configuration");
            } else {
                z = true;
            }
        }
        if (z) {
            com.ogury.consent.manager.util.consent.rtf1 rtf13 = this.h;
            if (rtf13 == null) {
                tx7200.a("consentCallback");
            }
            ansicpg1252.a(context, rtf13);
        }
    }

    public static void a(Context context, String str) {
        tx7200.b(context, "context");
        tx7200.b(str, String.BUNDLE);
        com.ogury.consent.manager.util.consent.ansi ansi2 = com.ogury.consent.manager.util.consent.ansi.f3236a;
        red255 c2 = com.ogury.consent.manager.util.consent.ansi.c();
        Context applicationContext = context.getApplicationContext();
        tx7200.a((Object) applicationContext, "context.applicationContext");
        String packageName = applicationContext.getPackageName();
        tx7200.a((Object) packageName, "context.applicationContext.packageName");
        c2.a(packageName);
    }

    public static final /* synthetic */ void a(rtf1 rtf1, Context context, String str) {
        rtf1.j.a(context);
        tx7200.b(context, "context");
        tx7200.b(str, "assetKey");
        try {
            com.ogury.consent.manager.util.consent.ansi ansi2 = com.ogury.consent.manager.util.consent.ansi.f3236a;
            SdkInfo sdkInfo = new SdkInfo("1.1.7", str, com.ogury.consent.manager.util.consent.ansi.c().b());
            com.ogury.consent.manager.util.consent.ansi ansi3 = com.ogury.consent.manager.util.consent.ansi.f3236a;
            CrashReport.register(context, sdkInfo, new CrashConfig(com.ogury.consent.manager.util.consent.ansi.c().c(), context.getPackageName()));
        } catch (Throwable unused) {
            com.ogury.consent.manager.util.consent.cocoartf1671 cocoartf16712 = com.ogury.consent.manager.util.consent.cocoartf1671.f3237a;
            com.ogury.consent.manager.util.consent.cocoartf1671.a("crash report init failed");
        }
    }

    public static final /* synthetic */ void a(rtf1 rtf1, String str, Context context) {
        if (tx7200.a((Object) str, (Object) "edit")) {
            rtf1.a(context);
            return;
        }
        com.ogury.consent.manager.util.consent.ansi ansi2 = com.ogury.consent.manager.util.consent.ansi.f3236a;
        if (com.ogury.consent.manager.util.consent.ansi.b().b().length() > 0) {
            rtf1.a(context);
        } else {
            rtf1.c();
        }
    }

    public static final /* synthetic */ void a(rtf1 rtf1, String str, String str2, Context context) {
        if (str.length() == 0) {
            rtf1.e.post(new cocoartf1671(rtf1, str2, context));
            fonttbl.a(rtf1.d, context);
            return;
        }
        rtf1.a("", str);
    }

    public static void a(String str) {
        tx7200.b(str, "showFormat");
    }

    private final void a(String str, Context context) {
        com.ogury.consent.manager.util.consent.ansi ansi2 = com.ogury.consent.manager.util.consent.ansi.f3236a;
        com.ogury.consent.manager.util.consent.ansi.f();
        this.i.a(context, this.d, str, new ansi(this, str, context));
    }

    /* access modifiers changed from: private */
    public final void a(String str, String str2) {
        this.e.post(new ansicpg1252(this, str2, str));
    }

    public static boolean a(Purpose purpose) {
        tx7200.b(purpose, "purpose");
        com.ogury.consent.manager.util.consent.ansi ansi2 = com.ogury.consent.manager.util.consent.ansi.f3236a;
        if (com.ogury.consent.manager.util.consent.ansi.a().c() == Answer.FULL_APPROVAL) {
            return true;
        }
        com.ogury.consent.manager.util.consent.ansi ansi3 = com.ogury.consent.manager.util.consent.ansi.f3236a;
        if (com.ogury.consent.manager.util.consent.ansi.a().c() == Answer.REFUSAL) {
            return false;
        }
        com.ogury.consent.manager.util.consent.ansi ansi4 = com.ogury.consent.manager.util.consent.ansi.f3236a;
        return aaa.a((CharSequence) com.ogury.consent.manager.util.consent.ansi.a().e(), (CharSequence) String.valueOf(purpose.ordinal() + 1), false, 2, (Object) null);
    }

    public static String b() {
        com.ogury.consent.manager.util.consent.ansi ansi2 = com.ogury.consent.manager.util.consent.ansi.f3236a;
        if (!(com.ogury.consent.manager.util.consent.ansi.a().d().length() > 0)) {
            return "";
        }
        com.ogury.consent.manager.util.consent.ansi ansi3 = com.ogury.consent.manager.util.consent.ansi.f3236a;
        return com.ogury.consent.manager.util.consent.ansi.a().d();
    }

    public static final /* synthetic */ void b(rtf1 rtf1, Context context, String str) {
        fonttbl.b(str, context);
        rtf1.d = str;
    }

    public static final /* synthetic */ void b(rtf1 rtf1, String str, Context context) {
        String str2;
        if (tx7200.a((Object) str, (Object) "edit")) {
            str2 = "edit";
        } else {
            com.ogury.consent.manager.util.consent.ansi ansi2 = com.ogury.consent.manager.util.consent.ansi.f3236a;
            if (!com.ogury.consent.manager.util.consent.ansi.d().after(new Date())) {
                str2 = "ask";
            } else {
                rtf1.c();
                return;
            }
        }
        rtf1.a(str2, context);
    }

    public static boolean b(String str) {
        tx7200.b(str, "vendor");
        com.ogury.consent.manager.util.consent.ansi ansi2 = com.ogury.consent.manager.util.consent.ansi.f3236a;
        if (com.ogury.consent.manager.util.consent.ansi.a().c() == Answer.FULL_APPROVAL) {
            return true;
        }
        com.ogury.consent.manager.util.consent.ansi ansi3 = com.ogury.consent.manager.util.consent.ansi.f3236a;
        if (com.ogury.consent.manager.util.consent.ansi.a().c() != Answer.REFUSAL) {
            com.ogury.consent.manager.util.consent.ansi ansi4 = com.ogury.consent.manager.util.consent.ansi.f3236a;
            if (com.ogury.consent.manager.util.consent.ansi.a().a().length() > 0) {
                com.ogury.consent.manager.util.consent.ansi ansi5 = com.ogury.consent.manager.util.consent.ansi.f3236a;
                if (!tx7200.a((Object) com.ogury.consent.manager.util.consent.ansi.a().a(), (Object) "null")) {
                    com.ogury.consent.manager.util.consent.ansi ansi6 = com.ogury.consent.manager.util.consent.ansi.f3236a;
                    return aaa.a((CharSequence) com.ogury.consent.manager.util.consent.ansi.a().a(), (CharSequence) str, false, 2, (Object) null);
                }
            }
            com.ogury.consent.manager.util.consent.ansi ansi7 = com.ogury.consent.manager.util.consent.ansi.f3236a;
            if (!aaa.a((CharSequence) com.ogury.consent.manager.util.consent.ansi.a().b(), (CharSequence) str, false, 2, (Object) null)) {
                return true;
            }
        }
        return false;
    }

    public static final /* synthetic */ com.ogury.consent.manager.util.consent.rtf1 c(rtf1 rtf1) {
        com.ogury.consent.manager.util.consent.rtf1 rtf12 = rtf1.h;
        if (rtf12 == null) {
            tx7200.a("consentCallback");
        }
        return rtf12;
    }

    private final void c() {
        com.ogury.consent.manager.util.consent.ansi ansi2 = com.ogury.consent.manager.util.consent.ansi.f3236a;
        if (com.ogury.consent.manager.util.consent.ansi.a().c() != Answer.NO_ANSWER) {
            com.ogury.consent.manager.util.consent.rtf1 rtf1 = this.h;
            if (rtf1 == null) {
                tx7200.a("consentCallback");
            }
            com.ogury.consent.manager.util.consent.ansi ansi3 = com.ogury.consent.manager.util.consent.ansi.f3236a;
            rtf1.a(com.ogury.consent.manager.util.consent.ansi.a().c().toString());
            return;
        }
        com.ogury.consent.manager.util.consent.rtf1 rtf12 = this.h;
        if (rtf12 == null) {
            tx7200.a("consentCallback");
        }
        rtf12.a(new ConsentException("consent-not-received", ""));
    }

    public final void a(Context context, String str, String str2) {
        tx7200.b(context, "context");
        tx7200.b(str, "assetKey");
        tx7200.b(str2, "requestType");
        this.m = true;
        if (aaa.a(context)) {
            fcharset0.a(context, (fswiss) new cocoasubrtf100(this, context, str, str2));
            return;
        }
        com.ogury.consent.manager.util.consent.rtf1 rtf1 = this.h;
        if (rtf1 == null) {
            tx7200.a("consentCallback");
        }
        rtf1.a(new ConsentException("no-internet-connection", ""));
    }

    public final void a(ConsentListener consentListener) {
        tx7200.b(consentListener, CastExtraArgs.LISTENER);
        this.h = new fonttbl(this, consentListener);
    }

    public final void a(boolean z) {
        this.m = false;
    }

    public final boolean a() {
        return this.m;
    }
}
