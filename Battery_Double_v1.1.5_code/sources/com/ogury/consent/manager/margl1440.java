package com.ogury.consent.manager;

import com.ogury.consent.manager.util.consent.rtf1;

public final class margl1440 {

    /* renamed from: a reason: collision with root package name */
    private String f3217a = "";
    private String b = "";
    private String c = "";
    private rtf1 d;

    public margl1440(vieww10800 vieww10800) {
        tx7200.b(vieww10800, "builder");
        this.f3217a = vieww10800.a();
        this.b = vieww10800.b();
        this.c = vieww10800.c();
        this.d = vieww10800.d();
    }

    public final String a() {
        return this.f3217a;
    }

    public final String b() {
        return this.b;
    }

    public final String c() {
        return this.c;
    }

    public final rtf1 d() {
        return this.d;
    }
}
