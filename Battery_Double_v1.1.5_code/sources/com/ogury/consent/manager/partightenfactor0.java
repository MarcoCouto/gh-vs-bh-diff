package com.ogury.consent.manager;

import com.ogury.consent.manager.rtf1.C0074rtf1;
import java.util.Iterator;

public class partightenfactor0 implements Iterable<Integer> {

    /* renamed from: a reason: collision with root package name */
    public static final rtf1 f3220a = new rtf1(null);
    private final int b;
    private final int c;
    private final int d = 1;

    public static final class rtf1 {
        private rtf1() {
        }

        public /* synthetic */ rtf1(tx6480 tx6480) {
            this();
        }
    }

    public partightenfactor0(int i, int i2, int i3) {
        this.b = i;
        this.c = C0074rtf1.a(i, i2, 1);
    }

    public final int a() {
        return this.b;
    }

    public final int b() {
        return this.c;
    }

    public final int c() {
        return this.d;
    }

    public boolean d() {
        return this.d > 0 ? this.b > this.c : this.b < this.c;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0025, code lost:
        if (r2.d == r3.d) goto L_0x0027;
     */
    public boolean equals(Object obj) {
        if (obj instanceof partightenfactor0) {
            if (!d() || !((partightenfactor0) obj).d()) {
                partightenfactor0 partightenfactor0 = (partightenfactor0) obj;
                if (this.b == partightenfactor0.b) {
                    if (this.c == partightenfactor0.c) {
                    }
                }
            }
            return true;
        }
        return false;
    }

    public int hashCode() {
        if (d()) {
            return -1;
        }
        return (((this.b * 31) + this.c) * 31) + this.d;
    }

    public /* synthetic */ Iterator iterator() {
        return new fs24(this.b, this.c, this.d);
    }

    public String toString() {
        StringBuilder sb;
        int i;
        if (this.d > 0) {
            sb = new StringBuilder();
            sb.append(this.b);
            sb.append("..");
            sb.append(this.c);
            sb.append(" step ");
            i = this.d;
        } else {
            sb = new StringBuilder();
            sb.append(this.b);
            sb.append(" downTo ");
            sb.append(this.c);
            sb.append(" step ");
            i = -this.d;
        }
        sb.append(i);
        return sb.toString();
    }
}
