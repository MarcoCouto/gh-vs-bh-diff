package com.ogury.consent.manager;

import com.ironsource.mediationsdk.utils.ServerResponseWrapper;
import com.ogury.consent.manager.rtf1.C0074rtf1;
import com.ogury.consent.manager.util.consent.ansi;
import java.util.Date;
import org.json.JSONObject;

public final class viewh8400 {
    public static String a(String str) {
        tx7200.b(str, ServerResponseWrapper.RESPONSE_FIELD);
        JSONObject a2 = C0074rtf1.a(str);
        if (a2 == null) {
            return "parsing-error";
        }
        if (!(a(a2).length() == 0)) {
            return a(a2);
        }
        JSONObject optJSONObject = a2.optJSONObject(ServerResponseWrapper.RESPONSE_FIELD);
        if (optJSONObject == null) {
            return "parsing-error";
        }
        ansi ansi = ansi.f3236a;
        green255 b = ansi.b();
        String jSONObject = a2.toString();
        tx7200.a((Object) jSONObject, "json.toString()");
        b.a(jSONObject);
        ansi ansi2 = ansi.f3236a;
        green255 b2 = ansi.b();
        String jSONObject2 = optJSONObject.toString();
        tx7200.a((Object) jSONObject2, "responseObject.toString()");
        b2.b(jSONObject2);
        JSONObject optJSONObject2 = a2.optJSONObject("form");
        if (optJSONObject2 == null) {
            return "parsing-error";
        }
        ansi ansi3 = ansi.f3236a;
        green255 b3 = ansi.b();
        String optString = optJSONObject2.optString("secureToken");
        if (optString == null) {
            return "parsing-error";
        }
        b3.e(optString);
        ansi ansi4 = ansi.f3236a;
        green255 b4 = ansi.b();
        String jSONObject3 = optJSONObject2.toString();
        tx7200.a((Object) jSONObject3, "formObject.toString()");
        b4.d(jSONObject3);
        JSONObject optJSONObject3 = optJSONObject.optJSONObject(ServerResponseWrapper.RESPONSE_FIELD);
        if (optJSONObject3 == null) {
            return "parsing-error";
        }
        ansi ansi5 = ansi.f3236a;
        colortbl a3 = ansi.a();
        String optString2 = optJSONObject.optString("lastOpt");
        tx7200.a((Object) optString2, "responseObject.optString(\"lastOpt\")");
        a3.a(C0074rtf1.b(optString2));
        ansi ansi6 = ansi.f3236a;
        colortbl a4 = ansi.a();
        String optString3 = optJSONObject3.optString("iabString");
        if (optString3 == null) {
            optString3 = "";
        }
        a4.c(optString3);
        ansi ansi7 = ansi.f3236a;
        colortbl a5 = ansi.a();
        String optString4 = optJSONObject3.optString("acceptedVendors");
        if (optString4 == null) {
            optString4 = "";
        }
        a5.a(optString4);
        ansi ansi8 = ansi.f3236a;
        colortbl a6 = ansi.a();
        String optString5 = optJSONObject3.optString("purposes");
        if (optString5 == null) {
            optString5 = "";
        }
        a6.d(optString5);
        ansi ansi9 = ansi.f3236a;
        colortbl a7 = ansi.a();
        String optString6 = optJSONObject3.optString("refusedVendors");
        if (optString6 == null) {
            optString6 = "";
        }
        a7.b(optString6);
        JSONObject optJSONObject4 = a2.optJSONObject("sdk");
        if (optJSONObject4 == null) {
            return "parsing-error";
        }
        ansi ansi10 = ansi.f3236a;
        red255 c = ansi.c();
        String optString7 = optJSONObject4.optString("crashReportUrl");
        tx7200.a((Object) optString7, "sdkObject.optString(\"crashReportUrl\")");
        c.c(optString7);
        JSONObject optJSONObject5 = a2.optJSONObject("form");
        String optString8 = optJSONObject5 != null ? optJSONObject5.optString("showFormat") : null;
        if (optString8 != null && (true ^ tx7200.a((Object) optString8, (Object) "null"))) {
            ansi ansi11 = ansi.f3236a;
            ansi.b().c(optString8);
        }
        ansi ansi12 = ansi.f3236a;
        ansi.a(new Date().getTime() + (optJSONObject4.optLong("cacheFor") * 1000));
        return "";
    }

    private static String a(JSONObject jSONObject) {
        if (!jSONObject.has("error")) {
            return "";
        }
        String string = jSONObject.getString("error");
        tx7200.a((Object) string, "jsonObject.getString(\"error\")");
        return string;
    }
}
