package com.ogury.consent.manager;

public final class pardirnatural {

    /* renamed from: a reason: collision with root package name */
    private static final tx6480 f3219a;
    private static final fswiss[] b = new fswiss[0];

    static {
        tx6480 tx6480 = null;
        try {
            tx6480 = (tx6480) Class.forName("kotlin.reflect.jvm.internal.ReflectionFactoryImpl").newInstance();
        } catch (ClassCastException | ClassNotFoundException | IllegalAccessException | InstantiationException unused) {
        }
        if (tx6480 == null) {
            tx6480 = new tx6480();
        }
        f3219a = tx6480;
    }

    public static String a(tx7920 tx7920) {
        String obj = tx7920.getClass().getGenericInterfaces()[0].toString();
        return obj.startsWith("kotlin.jvm.functions.") ? obj.substring(21) : obj;
    }
}
