package com.ogury.consent.manager;

import android.content.Context;
import java.io.Closeable;

public final class tx5040 {

    /* renamed from: a reason: collision with root package name */
    private boolean f3232a;

    public static final void a(Closeable closeable, Throwable th) {
        if (th == null) {
            closeable.close();
            return;
        }
        try {
            closeable.close();
        } catch (Throwable th2) {
            tx7200.b(th, "receiver$0");
            tx7200.b(th2, "exception");
            tx3600.f3231a.a(th, th2);
        }
    }

    public final void a(Context context) {
        tx7200.b(context, "context");
        boolean z = false;
        if ((context.getPackageManager().getApplicationInfo(context.getPackageName(), 0).flags & 2) != 0) {
            z = true;
        }
        this.f3232a = z;
    }
}
