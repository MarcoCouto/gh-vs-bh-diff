package com.ogury.consent.manager;

import android.content.Context;
import io.fabric.sdk.android.services.network.HttpRequest;

public final class blue255 {

    /* renamed from: a reason: collision with root package name */
    private final expandedcolortbl f3202a;

    static final class rtf1 extends tx7920 implements tx5760<tx720> {

        /* renamed from: a reason: collision with root package name */
        final /* synthetic */ blue255 f3203a;
        final /* synthetic */ Context b;
        final /* synthetic */ String c;
        final /* synthetic */ String d;
        final /* synthetic */ com.ogury.consent.manager.util.consent.rtf1 e;

        rtf1(blue255 blue255, Context context, String str, String str2, com.ogury.consent.manager.util.consent.rtf1 rtf1) {
            this.f3203a = blue255;
            this.b = context;
            this.c = str;
            this.d = str2;
            this.e = rtf1;
            super(0);
        }

        public final /* synthetic */ Object a() {
            Context context = this.b;
            String str = this.c;
            String str2 = this.d;
            com.ogury.consent.manager.util.consent.rtf1 rtf1 = this.e;
            vieww10800 vieww10800 = new vieww10800();
            new margr1440();
            expandedcolortbl.a(new margl1440(vieww10800.b(margr1440.a(context, str)).a(HttpRequest.METHOD_POST).c("https://consent-manager-events.ogury.io/v1/".concat(String.valueOf(str2))).a(rtf1)));
            return tx720.f3233a;
        }
    }

    public blue255(expandedcolortbl expandedcolortbl) {
        tx7200.b(expandedcolortbl, "networkRequest");
        this.f3202a = expandedcolortbl;
    }

    public final void a(Context context, String str, String str2, com.ogury.consent.manager.util.consent.rtf1 rtf12) {
        tx7200.b(context, "context");
        tx7200.b(str, "assetKey");
        tx7200.b(str2, "requestType");
        tx7200.b(rtf12, "callback");
        rtf1 rtf13 = new rtf1(this, context, str, str2, rtf12);
        tx2160.a(true, false, null, null, -1, rtf13);
    }
}
