package com.ogury.consent.manager.util.consent;

import android.util.Log;
import com.ogury.consent.manager.tx7200;

public final class cocoartf1671 {

    /* renamed from: a reason: collision with root package name */
    public static final cocoartf1671 f3237a = new cocoartf1671();

    private cocoartf1671() {
    }

    public static void a(String str) {
        tx7200.b(str, "message");
        Log.d("consent_sdk", str);
    }

    public static void a(Throwable th) {
        tx7200.b(th, "error");
        Log.e("consent_sdk", "caught_error", th);
    }
}
