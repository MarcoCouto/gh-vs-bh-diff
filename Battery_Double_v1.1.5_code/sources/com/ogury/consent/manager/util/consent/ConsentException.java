package com.ogury.consent.manager.util.consent;

import com.ogury.consent.manager.tx7200;

public final class ConsentException extends Exception {
    private String type;

    public ConsentException(String str, String str2) {
        tx7200.b(str, "type");
        tx7200.b(str2, "message");
        super(str2);
        this.type = str;
    }

    public final String getType() {
        return this.type;
    }

    public final void setType(String str) {
        tx7200.b(str, "<set-?>");
        this.type = str;
    }
}
