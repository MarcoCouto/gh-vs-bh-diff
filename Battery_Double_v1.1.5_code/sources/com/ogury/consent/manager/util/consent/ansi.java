package com.ogury.consent.manager.util.consent;

import android.webkit.WebView;
import com.ogury.consent.manager.colortbl;
import com.ogury.consent.manager.green255;
import com.ogury.consent.manager.red255;
import com.ogury.consent.manager.tx7200;
import java.util.Date;

public final class ansi {

    /* renamed from: a reason: collision with root package name */
    public static final ansi f3236a = new ansi();
    private static colortbl b = new colortbl();
    private static green255 c = new green255();
    private static red255 d = new red255();
    private static Date e = new Date();
    private static WebView f;

    private ansi() {
    }

    public static colortbl a() {
        return b;
    }

    public static void a(long j) {
        e.setTime(j);
    }

    public static void a(WebView webView) {
        f = webView;
    }

    public static void a(Date date) {
        tx7200.b(date, "<set-?>");
        e = date;
    }

    public static green255 b() {
        return c;
    }

    public static red255 c() {
        return d;
    }

    public static Date d() {
        return e;
    }

    public static WebView e() {
        return f;
    }

    public static void f() {
        b = new colortbl();
        c = new green255();
    }
}
