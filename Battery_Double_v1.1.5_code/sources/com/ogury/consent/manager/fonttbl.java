package com.ogury.consent.manager;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.ogury.consent.manager.ConsentManager.Answer;
import com.ogury.consent.manager.util.consent.ansi;
import java.util.Date;

public final class fonttbl {

    /* renamed from: a reason: collision with root package name */
    public static final rtf1 f3214a = new rtf1(null);

    public static final class rtf1 {
        private rtf1() {
        }

        public /* synthetic */ rtf1(tx6480 tx6480) {
            this();
        }
    }

    public static void a(String str, Context context) {
        tx7200.b(str, "assetKey");
        tx7200.b(context, "context");
        boolean z = false;
        Editor edit = context.getSharedPreferences("cacheConsent", 0).edit();
        ansi ansi = ansi.f3236a;
        edit.putString("iabString", ansi.a().d());
        ansi ansi2 = ansi.f3236a;
        edit.putString("optin", ansi.a().c().toString());
        ansi ansi3 = ansi.f3236a;
        edit.putLong("cacheFor", ansi.d().getTime());
        ansi ansi4 = ansi.f3236a;
        edit.putString("aaid", ansi.c().b());
        ansi ansi5 = ansi.f3236a;
        edit.putString("acceptedVendors", ansi.a().a());
        ansi ansi6 = ansi.f3236a;
        edit.putString("refusedVendors", ansi.a().b());
        ansi ansi7 = ansi.f3236a;
        edit.putString("purposes", ansi.a().e());
        if (str.length() > 0) {
            z = true;
        }
        if (z) {
            edit.putString("assetKey", str);
        }
        String str2 = RequestParameters.PACKAGE_NAME;
        ansi ansi8 = ansi.f3236a;
        edit.putString(str2, ansi.c().a());
        edit.apply();
    }

    public static void b(String str, Context context) {
        tx7200.b(str, "assetKey");
        tx7200.b(context, "context");
        SharedPreferences sharedPreferences = context.getSharedPreferences("cacheConsent", 0);
        tx7200.a((Object) sharedPreferences, "prefs");
        boolean z = true;
        if (!(!tx7200.a((Object) sharedPreferences.getString("assetKey", ""), (Object) str))) {
            String string = sharedPreferences.getString(RequestParameters.PACKAGE_NAME, "");
            ansi ansi = ansi.f3236a;
            if (!(!tx7200.a((Object) string, (Object) ansi.c().a()))) {
                String string2 = sharedPreferences.getString("aaid", "");
                ansi ansi2 = ansi.f3236a;
                if (!(!tx7200.a((Object) string2, (Object) ansi.c().b()))) {
                    z = false;
                }
            }
        }
        if (z) {
            sharedPreferences.edit().clear().apply();
            ansi ansi3 = ansi.f3236a;
            ansi.a(new Date());
            return;
        }
        ansi ansi4 = ansi.f3236a;
        colortbl a2 = ansi.a();
        String string3 = sharedPreferences.getString("iabString", "");
        tx7200.a((Object) string3, "prefs.getString(IAB_STRING_KEY, \"\")");
        a2.c(string3);
        ansi ansi5 = ansi.f3236a;
        colortbl a3 = ansi.a();
        String string4 = sharedPreferences.getString("optin", "NO_ANSWER");
        tx7200.a((Object) string4, "prefs.getString(OPTIN_KEY, \"NO_ANSWER\")");
        a3.a(Answer.valueOf(string4));
        ansi ansi6 = ansi.f3236a;
        colortbl a4 = ansi.a();
        String string5 = sharedPreferences.getString("purposes", "");
        tx7200.a((Object) string5, "prefs.getString(PURPOSES, \"\")");
        a4.d(string5);
        ansi ansi7 = ansi.f3236a;
        colortbl a5 = ansi.a();
        String string6 = sharedPreferences.getString("acceptedVendors", "");
        tx7200.a((Object) string6, "prefs.getString(ACCEPTED_VENDORS, \"\")");
        a5.a(string6);
        ansi ansi8 = ansi.f3236a;
        colortbl a6 = ansi.a();
        String string7 = sharedPreferences.getString("refusedVendors", "");
        tx7200.a((Object) string7, "prefs.getString(REFUSED_VENDORS, \"\")");
        a6.b(string7);
        ansi ansi9 = ansi.f3236a;
        ansi.a(sharedPreferences.getLong("cacheFor", 0));
    }
}
