package com.ogury.crashreport;

import android.content.Context;
import android.content.SharedPreferences;

/* compiled from: CrashReportDao.kt */
public final class fonttbl {

    /* renamed from: a reason: collision with root package name */
    private final SharedPreferences f3249a;

    /* compiled from: CrashReportDao.kt */
    public static final class rtf1 {
        private rtf1() {
        }

        public /* synthetic */ rtf1(byte b) {
            this();
        }
    }

    static {
        new rtf1(0);
    }

    public fonttbl(Context context) {
        tx7920.b(context, "context");
        this.f3249a = context.getSharedPreferences("crashreport", 0);
    }

    public final void a(String str, String str2) {
        tx7920.b(str, "key");
        tx7920.b(str2, "uploadUrl");
        this.f3249a.edit().putString(str, str2).apply();
    }

    public final String a(String str) {
        tx7920.b(str, "key");
        String string = this.f3249a.getString(str, "");
        return string == null ? "" : string;
    }
}
