package com.ogury.crashreport;

import java.util.NoSuchElementException;

/* compiled from: ProgressionIterators.kt */
public final class fs24 extends tx720 {

    /* renamed from: a reason: collision with root package name */
    private final int f3250a;
    private boolean b;
    private int c;
    private final int d;

    public fs24(int i, int i2, int i3) {
        this.d = i3;
        this.f3250a = i2;
        boolean z = false;
        if (this.d <= 0 ? i >= i2 : i <= i2) {
            z = true;
        }
        this.b = z;
        if (!this.b) {
            i = this.f3250a;
        }
        this.c = i;
    }

    public final boolean hasNext() {
        return this.b;
    }

    public final int a() {
        int i = this.c;
        if (i != this.f3250a) {
            this.c += this.d;
        } else if (this.b) {
            this.b = false;
        } else {
            throw new NoSuchElementException();
        }
        return i;
    }
}
