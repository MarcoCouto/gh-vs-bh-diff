package com.ogury.crashreport;

/* compiled from: Ranges.kt */
public final class cf0 extends partightenfactor0 {

    /* compiled from: Ranges.kt */
    public static final class rtf1 {
        private rtf1() {
        }

        public /* synthetic */ rtf1(byte b) {
            this();
        }
    }

    public cf0(int i, int i2) {
        super(i, i2, 1);
    }

    public final boolean d() {
        return a() > b();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0027, code lost:
        if (b() == r3.b()) goto L_0x0029;
     */
    public final boolean equals(Object obj) {
        if (obj instanceof cf0) {
            if (!d() || !((cf0) obj).d()) {
                cf0 cf0 = (cf0) obj;
                if (a() == cf0.a()) {
                }
            }
            return true;
        }
        return false;
    }

    public final int hashCode() {
        if (d()) {
            return -1;
        }
        return (a() * 31) + b();
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(a());
        sb.append("..");
        sb.append(b());
        return sb.toString();
    }

    static {
        new rtf1(0);
        new cf0(1, 0);
    }
}
