package com.ogury.crashreport;

/* compiled from: PhoneMemory.kt */
public final class green255 {

    /* renamed from: a reason: collision with root package name */
    public static final rtf1 f3253a = new rtf1(0);
    private final long b;
    private final long c;
    private final long d;
    private final boolean e;

    /* compiled from: PhoneMemory.kt */
    public static final class rtf1 {
        private rtf1() {
        }

        public /* synthetic */ rtf1(byte b) {
            this();
        }
    }

    public green255() {
        this(0, 0, 0, false, 15);
    }

    public green255(long j, long j2, long j3, boolean z) {
        this.b = j;
        this.c = j2;
        this.d = j3;
        this.e = z;
    }

    public /* synthetic */ green255(long j, long j2, long j3, boolean z, int i) {
        this(0, 0, 0, false);
    }

    public final long a() {
        return this.b;
    }

    public final long b() {
        return this.c;
    }

    public final long c() {
        return this.d;
    }

    public final boolean d() {
        return this.e;
    }
}
