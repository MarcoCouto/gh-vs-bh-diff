package com.ogury.crashreport;

import android.content.Context;
import com.ironsource.sdk.constants.Constants.ParametersKeys;
import java.io.File;
import java.io.PrintWriter;

/* compiled from: FileStore.kt */
public final class Helvetica {

    /* renamed from: a reason: collision with root package name */
    private final File f3240a;

    /* compiled from: FileStore.kt */
    public static final class rtf1 {
        private rtf1() {
        }

        public /* synthetic */ rtf1(byte b) {
            this();
        }
    }

    static {
        new rtf1(0);
    }

    public Helvetica(Context context) {
        tx7920.b(context, "context");
        this.f3240a = new File(context.getFilesDir(), "presageDir");
        this.f3240a.mkdirs();
    }

    public final File[] a() {
        File[] listFiles = this.f3240a.listFiles();
        return listFiles == null ? new File[0] : listFiles;
    }

    public final boolean a(String str) {
        tx7920.b(str, "fileName");
        return new File(this.f3240a, str).createNewFile();
    }

    public static void a(File file) {
        tx7920.b(file, ParametersKeys.FILE);
        try {
            new PrintWriter(file).print("");
        } catch (Exception e) {
            f0 f0Var = f0.f3247a;
            f0.a(e);
        }
    }
}
