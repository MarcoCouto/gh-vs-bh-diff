package com.ogury.crashreport;

import android.content.Context;
import com.ogury.crashreport.fcharset0.rtf1;
import java.lang.Thread.UncaughtExceptionHandler;

/* compiled from: CrashReport.kt */
public final class CrashReport {

    /* renamed from: a reason: collision with root package name */
    private static boolean f3239a;
    private static cocoartf1671 b;

    static {
        new CrashReport();
    }

    private CrashReport() {
    }

    public static final void register(Context context, SdkInfo sdkInfo, CrashConfig crashConfig) {
        tx7920.b(context, "context");
        tx7920.b(sdkInfo, "sdkInfo");
        tx7920.b(crashConfig, "crashConfig");
        fonttbl fonttbl = new fonttbl(context);
        cocoasubrtf100 cocoasubrtf100 = new cocoasubrtf100(sdkInfo);
        Helvetica helvetica = new Helvetica(context);
        cocoartf1671 cocoartf1671 = new cocoartf1671(context, cocoasubrtf100, helvetica);
        b = cocoartf1671;
        helvetica.a(crashConfig.getPackageName());
        fonttbl.a(crashConfig.getPackageName(), crashConfig.getUrl());
        tx1440.a(true, false, null, null, -1, new ansi(new fswiss(cocoartf1671, fonttbl)));
        if (!f3239a) {
            rtf1 rtf1 = fcharset0.f3248a;
            tx7920.b(cocoartf1671, "crashFileStore");
            UncaughtExceptionHandler defaultUncaughtExceptionHandler = Thread.getDefaultUncaughtExceptionHandler();
            if (!(defaultUncaughtExceptionHandler instanceof fcharset0)) {
                Thread.setDefaultUncaughtExceptionHandler(new fcharset0(cocoartf1671, defaultUncaughtExceptionHandler));
            }
        }
        f3239a = true;
    }

    public static final void logException(Throwable th) {
        tx7920.b(th, "t");
        cocoartf1671 cocoartf1671 = b;
        if (cocoartf1671 != null) {
            cocoartf1671.a(th);
        }
    }
}
