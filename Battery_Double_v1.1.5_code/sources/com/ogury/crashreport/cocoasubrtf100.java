package com.ogury.crashreport;

import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.tapjoy.TapjoyConstants;
import java.io.File;
import org.json.JSONObject;

/* compiled from: CrashFormatter.kt */
public final class cocoasubrtf100 {

    /* renamed from: a reason: collision with root package name */
    private String f3244a;
    private String b;
    private final SdkInfo c;

    /* compiled from: CrashFormatter.kt */
    public static final class rtf1 {
        private rtf1() {
        }

        public /* synthetic */ rtf1(byte b) {
            this();
        }
    }

    static {
        new rtf1(0);
    }

    public cocoasubrtf100(SdkInfo sdkInfo) {
        tx7920.b(sdkInfo, "sdkInfo");
        this.c = sdkInfo;
    }

    public final ansi a(Throwable th, red255 red255, rtf1 rtf12, Helvetica helvetica) {
        boolean z;
        String str;
        tx7920.b(th, "throwable");
        tx7920.b(red255, "phoneInfo");
        tx7920.b(rtf12, "appInfo");
        tx7920.b(helvetica, "fileStore");
        File[] a2 = helvetica.a();
        StackTraceElement[] stackTrace = th.getStackTrace();
        tx7920.a((Object) stackTrace, "throwable.stackTrace");
        this.f3244a = viewh8400.a((Object[]) stackTrace, (CharSequence) "\n", (CharSequence) null, (CharSequence) null, 0, (CharSequence) null, (tx7200) null, 62);
        int length = a2.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                z = true;
                break;
            }
            File file = a2[i];
            String str2 = this.f3244a;
            if (str2 == null) {
                tx7920.a("stackTrace");
            }
            CharSequence charSequence = str2;
            String name = file.getName();
            tx7920.a((Object) name, "file.name");
            if (ii.a(charSequence, name, false, 2)) {
                String name2 = file.getName();
                tx7920.a((Object) name2, "file.name");
                this.b = name2;
                z = false;
                break;
            }
            i++;
        }
        if (z) {
            return colortbl.f3245a;
        }
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("created_at", System.currentTimeMillis());
        jSONObject.put("sdk_version", this.c.getSdkVersion());
        jSONObject.put(TapjoyConstants.TJC_API_KEY, this.c.getApiKey());
        jSONObject.put("aaid", this.c.getAaid());
        jSONObject.put(CampaignEx.JSON_KEY_PACKAGE_NAME, rtf12.b());
        jSONObject.put("package_version", rtf12.a());
        String str3 = "phone_model";
        String a3 = red255.a();
        if (a3.length() > 16) {
            if (a3 != null) {
                a3 = a3.substring(0, 16);
                tx7920.a((Object) a3, "(this as java.lang.Strin…ing(startIndex, endIndex)");
            } else {
                throw new margl1440("null cannot be cast to non-null type java.lang.String");
            }
        }
        jSONObject.put(str3, a3);
        jSONObject.put("android_version", red255.b());
        jSONObject.put("exception_type", th.getClass().getCanonicalName());
        String str4 = "message";
        green255 c2 = red255.c();
        StringBuilder sb = new StringBuilder();
        sb.append(th.getClass().getName());
        sb.append(" : ");
        sb.append(th.getMessage());
        if (c2.d()) {
            StringBuilder sb2 = new StringBuilder(" : Free[");
            sb2.append(c2.a());
            sb2.append("] Total[");
            sb2.append(c2.b());
            sb2.append("] Max[");
            sb2.append(c2.c());
            sb2.append(RequestParameters.RIGHT_BRACKETS);
            str = sb2.toString();
        } else {
            str = "";
        }
        sb.append(str);
        jSONObject.put(str4, sb.toString());
        String str5 = "stacktrace";
        String str6 = this.f3244a;
        if (str6 == null) {
            tx7920.a("stackTrace");
        }
        jSONObject.put(str5, str6);
        String jSONObject2 = jSONObject.toString();
        tx7920.a((Object) jSONObject2, "jsonObject.toString()");
        String str7 = this.b;
        if (str7 == null) {
            tx7920.a("packageName");
        }
        return new blue255(jSONObject2, str7);
    }
}
