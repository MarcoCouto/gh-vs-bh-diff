package com.mintegral.msdk.video.module;

import android.content.Context;
import android.content.res.Configuration;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.mintegral.msdk.base.common.c.b;
import com.mintegral.msdk.base.common.c.c;
import com.mintegral.msdk.video.module.a.a.e;
import com.mintegral.msdk.widget.a;
import org.json.JSONException;
import org.json.JSONObject;

public class MintegralClickCTAView extends MintegralBaseView {
    private ViewGroup i;
    private ImageView j;
    private TextView k;
    private TextView l;
    private String m;
    private float n;
    private float o;
    private int p;

    public void setUnitId(String str) {
        this.m = str;
    }

    public MintegralClickCTAView(Context context) {
        super(context);
    }

    public MintegralClickCTAView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public void init(Context context) {
        int findLayout = findLayout("mintegral_reward_clickable_cta");
        if (findLayout >= 0) {
            this.c.inflate(findLayout, this);
            this.i = (ViewGroup) findViewById(findID("mintegral_viewgroup_ctaroot"));
            this.j = (ImageView) findViewById(findID("mintegral_iv_appicon"));
            this.k = (TextView) findViewById(findID("mintegral_tv_desc"));
            this.l = (TextView) findViewById(findID("mintegral_tv_install"));
            this.f = isNotNULL(this.i, this.j, this.k, this.l);
            a();
            setWrapContent();
        }
    }

    /* access modifiers changed from: protected */
    public final void a() {
        super.a();
        if (this.f) {
            this.l.setOnClickListener(new a() {
                /* access modifiers changed from: protected */
                public final void a() {
                    JSONObject jSONObject;
                    JSONException e;
                    try {
                        jSONObject = new JSONObject();
                        try {
                            jSONObject.put(com.mintegral.msdk.base.common.a.z, MintegralClickCTAView.this.c());
                        } catch (JSONException e2) {
                            e = e2;
                        }
                    } catch (JSONException e3) {
                        JSONException jSONException = e3;
                        jSONObject = null;
                        e = jSONException;
                        e.printStackTrace();
                        MintegralClickCTAView.this.e.a(105, jSONObject);
                    }
                    MintegralClickCTAView.this.e.a(105, jSONObject);
                }
            });
        }
    }

    public void preLoadData() {
        if (this.f && this.b != null) {
            this.l.setText(this.b.getAdCall());
            if (!TextUtils.isEmpty(this.b.getIconUrl())) {
                this.k.setText(this.b.getAppName());
                b.a(this.f2997a.getApplicationContext()).a(this.b.getIconUrl(), (c) new e(this.j, this.b, this.m) {
                    public final void onFailedLoad(String str, String str2) {
                        super.onFailedLoad(str, str2);
                        MintegralClickCTAView.this.d();
                    }
                });
                return;
            }
            d();
        }
    }

    public void onSelfConfigurationChanged(Configuration configuration) {
        super.onSelfConfigurationChanged(configuration);
        this.p = configuration.orientation;
    }

    /* access modifiers changed from: private */
    public void d() {
        this.i.setBackgroundColor(0);
        this.j.setVisibility(8);
        this.k.setVisibility(8);
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        this.n = motionEvent.getRawX();
        this.o = motionEvent.getRawY();
        return super.onInterceptTouchEvent(motionEvent);
    }
}
