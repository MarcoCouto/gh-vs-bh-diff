package com.mintegral.msdk.video.module;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.drawable.GradientDrawable;
import android.os.Build.VERSION;
import android.os.Handler;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import com.github.mikephil.charting.utils.Utils;
import com.integralads.avid.library.inmobi.utils.AvidJSONUtil;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.ironsource.sdk.constants.LocationConst;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.base.utils.k;
import com.mintegral.msdk.base.utils.s;
import com.mintegral.msdk.playercommon.PlayerView;
import com.mintegral.msdk.video.js.h;
import com.mintegral.msdk.video.widget.SoundImageView;
import com.mintegral.msdk.videocommon.dialog.MTGAlertDialog;
import org.json.JSONObject;

public class MintegralVideoView extends MintegralBaseView implements h {
    /* access modifiers changed from: private */
    public static boolean M = false;
    private static int i;
    private static int j;
    private static int k;
    private static int l;
    private static int m;
    private double A;
    private boolean B = false;
    private boolean C = false;
    private boolean D = false;
    private boolean E = false;
    private boolean F = false;
    private boolean G = false;
    private boolean H = false;
    /* access modifiers changed from: private */
    public boolean I = false;
    private boolean J = false;
    private int K;
    private boolean L = false;
    private int N = 2;
    private b O = new b(this);
    private boolean P = false;
    /* access modifiers changed from: private */
    public PlayerView n;
    private SoundImageView o;
    /* access modifiers changed from: private */
    public TextView p;
    private View q;
    /* access modifiers changed from: private */
    public boolean r = false;
    private String s;
    private int t;
    private int u;
    private int v;
    private MTGAlertDialog w;
    private com.mintegral.msdk.videocommon.dialog.a x;
    private String y = "";
    private double z;

    public static class a {

        /* renamed from: a reason: collision with root package name */
        public int f3023a;
        public int b;

        public final String toString() {
            StringBuilder sb = new StringBuilder("ProgressData{curPlayPosition=");
            sb.append(this.f3023a);
            sb.append(", allDuration=");
            sb.append(this.b);
            sb.append('}');
            return sb.toString();
        }
    }

    private static final class b extends com.mintegral.msdk.playercommon.a {

        /* renamed from: a reason: collision with root package name */
        private MintegralVideoView f3024a;
        private int b;
        private int c;
        private boolean d;
        private a e = new a();

        public final int a() {
            return this.b;
        }

        public b(MintegralVideoView mintegralVideoView) {
            this.f3024a = mintegralVideoView;
        }

        public final void onPlayStarted(int i) {
            super.onPlayStarted(i);
            if (!this.d) {
                this.f3024a.e.a(10, this.e);
                this.d = true;
            }
            MintegralVideoView.M = false;
        }

        public final void onPlayCompleted() {
            super.onPlayCompleted();
            this.f3024a.p.setText(String.valueOf("0"));
            this.f3024a.n.setClickable(false);
            this.f3024a.e.a(121, "");
            this.f3024a.e.a(11, "");
            this.b = this.c;
            MintegralVideoView.M = true;
        }

        public final void onPlayError(String str) {
            super.onPlayError(str);
            this.f3024a.e.a(12, "");
        }

        public final void onPlayProgress(int i, int i2) {
            super.onPlayProgress(i, i2);
            if (this.f3024a.f) {
                int i3 = i2 - i;
                if (i3 <= 0) {
                    i3 = 0;
                }
                this.f3024a.p.setText(String.valueOf(i3));
            }
            this.c = i2;
            this.e.f3023a = i;
            this.e.b = i2;
            this.b = i;
            this.f3024a.e.a(15, this.e);
        }

        public final void OnBufferingStart(String str) {
            try {
                super.OnBufferingStart(str);
                this.f3024a.e.a(13, "");
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }

        public final void OnBufferingEnd() {
            try {
                super.OnBufferingEnd();
                this.f3024a.e.a(14, "");
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }

        public final void onPlaySetDataSourceError(String str) {
            super.onPlaySetDataSourceError(str);
        }
    }

    public boolean isShowingAlertView() {
        return this.r;
    }

    public void setUnitId(String str) {
        this.y = str;
    }

    public boolean isMiniCardShowing() {
        return this.E;
    }

    public boolean isShowingTransparent() {
        return this.J;
    }

    public void setShowingTransparent(boolean z2) {
        this.J = z2;
    }

    public MintegralVideoView(Context context) {
        super(context);
    }

    public MintegralVideoView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public void init(Context context) {
        int findLayout = findLayout("mintegral_reward_videoview_item");
        if (findLayout > 0) {
            this.c.inflate(findLayout, this);
            this.f = e();
            if (!this.f) {
                g.d(MintegralBaseView.TAG, "MintegralVideoView init fail");
            }
            a();
        }
        M = false;
    }

    public void setIsIV(boolean z2) {
        this.L = z2;
    }

    public void setSoundState(int i2) {
        this.N = i2;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        super.a();
        if (this.f) {
            this.n.setOnClickListener(new OnClickListener() {
                public final void onClick(View view) {
                    MintegralVideoView.this.e.a(1, "");
                }
            });
            this.o.setOnClickListener(new OnClickListener() {
                public final void onClick(View view) {
                    Integer valueOf = Integer.valueOf(2);
                    if (MintegralVideoView.this.n.isSilent()) {
                        valueOf = Integer.valueOf(1);
                    }
                    MintegralVideoView.this.e.a(5, valueOf);
                }
            });
            this.q.setOnClickListener(new OnClickListener() {
                public final void onClick(View view) {
                    MintegralVideoView.this.d();
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public void d() {
        if (this.t < 0 || this.v != 1 || this.J) {
            this.e.a(2, "");
            return;
        }
        f();
        if (this.x == null) {
            this.x = new com.mintegral.msdk.videocommon.dialog.a() {
                public final void a() {
                    MintegralVideoView.this.r = false;
                    MintegralVideoView.this.setShowingAlertViewCover(MintegralVideoView.this.r);
                    MintegralVideoView.this.g();
                }

                public final void b() {
                    MintegralVideoView.this.r = false;
                    MintegralVideoView.this.setShowingAlertViewCover(MintegralVideoView.this.r);
                    MintegralVideoView.this.e.a(2, "");
                }
            };
        }
        if (this.w == null) {
            this.w = new MTGAlertDialog(getContext(), this.x);
        }
        this.w.makeRVAlertView(this.y);
        if (this.n != null && !this.n.isComplete()) {
            this.w.show();
            this.r = true;
            setShowingAlertViewCover(this.r);
        }
    }

    public void preLoadData() {
        if (this.f && !TextUtils.isEmpty(this.s) && this.b != null) {
            if (this.b != null && s.b(this.b.getVideoResolution())) {
                String videoResolution = this.b.getVideoResolution();
                String str = MintegralBaseView.TAG;
                StringBuilder sb = new StringBuilder("MintegralBaseView videoResolution:");
                sb.append(videoResolution);
                g.b(str, sb.toString());
                String[] split = videoResolution.split(AvidJSONUtil.KEY_X);
                if (split.length == 2) {
                    if (k.c(split[0]) > Utils.DOUBLE_EPSILON) {
                        this.z = k.c(split[0]);
                    }
                    if (k.c(split[1]) > Utils.DOUBLE_EPSILON) {
                        this.A = k.c(split[1]);
                    }
                    String str2 = MintegralBaseView.TAG;
                    StringBuilder sb2 = new StringBuilder("MintegralBaseView mVideoW:");
                    sb2.append(this.z);
                    sb2.append("  mVideoH:");
                    sb2.append(this.A);
                    g.b(str2, sb2.toString());
                }
                if (this.z <= Utils.DOUBLE_EPSILON) {
                    this.z = 1280.0d;
                }
                if (this.A <= Utils.DOUBLE_EPSILON) {
                    this.A = 720.0d;
                }
            }
            this.n.initBufferIngParam(this.u);
            this.n.initVFPData(this.s, this.b.getVideoUrlEncode(), this.O);
            soundOperate(this.N, -1, null);
        }
        M = false;
    }

    public void defaultShow() {
        super.defaultShow();
        this.B = true;
        showVideoLocation(0, 0, k.i(this.f2997a), k.h(this.f2997a), 0, 0, 0, 0, 0);
        videoOperate(1);
        if (this.t == 0) {
            closeVideoOperate(-1, 2);
        }
    }

    public void showVideoLocation(int i2, int i3, int i4, int i5, int i6, int i7, int i8, int i9, int i10) {
        String str = MintegralBaseView.TAG;
        StringBuilder sb = new StringBuilder("showVideoLocation marginTop:");
        sb.append(i2);
        sb.append(" marginLeft:");
        sb.append(i3);
        sb.append(" width:");
        sb.append(i4);
        sb.append("  height:");
        sb.append(i5);
        sb.append(" radius:");
        sb.append(i6);
        sb.append(" borderTop:");
        sb.append(i7);
        sb.append(" borderLeft:");
        sb.append(i8);
        sb.append(" borderWidth:");
        sb.append(i9);
        sb.append(" borderHeight:");
        sb.append(i10);
        g.b(str, sb.toString());
        if (this.f) {
            setVisibility(0);
            if (!(i4 > 0 && i5 > 0 && k.i(this.f2997a) >= i4 && k.h(this.f2997a) >= i5) || this.B) {
                h();
            } else {
                j = i7;
                k = i8;
                l = i9 + 4;
                m = i10 + 4;
                float f = ((float) i4) / ((float) i5);
                float f2 = 0.0f;
                try {
                    f2 = (float) (this.z / this.A);
                } catch (Throwable th) {
                    g.c(MintegralBaseView.TAG, th.getMessage(), th);
                }
                if (i6 > 0) {
                    i = i6;
                    if (i6 > 0) {
                        GradientDrawable gradientDrawable = new GradientDrawable();
                        gradientDrawable.setCornerRadius((float) k.b(getContext(), (float) i6));
                        gradientDrawable.setColor(-1);
                        gradientDrawable.setStroke(1, 0);
                        if (VERSION.SDK_INT >= 16) {
                            setBackground(gradientDrawable);
                            this.n.setBackground(gradientDrawable);
                        } else {
                            setBackgroundDrawable(gradientDrawable);
                            this.n.setBackgroundDrawable(gradientDrawable);
                        }
                        if (VERSION.SDK_INT >= 21) {
                            setClipToOutline(true);
                            this.n.setClipToOutline(true);
                        }
                    }
                }
                if (Math.abs(f - f2) <= 0.1f || this.K == 1) {
                    g.b(MintegralBaseView.TAG, "showVideoLocation USE H5 SIZE.");
                    h();
                    if (this.J) {
                        setLayoutCenter(i4, i5);
                        if (M) {
                            this.e.a(114, "");
                        } else {
                            this.e.a(116, "");
                        }
                    } else {
                        setLayoutParam(i3, i2, i4, i5);
                    }
                } else {
                    h();
                    videoOperate(1);
                }
            }
        }
    }

    public void soundOperate(int i2, int i3) {
        soundOperate(i2, i3, "2");
    }

    public void soundOperate(int i2, int i3, String str) {
        if (this.f) {
            if (i2 == 1) {
                this.o.setSoundStatus(false);
                this.n.closeSound();
            } else if (i2 == 2) {
                this.o.setSoundStatus(true);
                this.n.openSound();
            }
            if (i3 == 1) {
                this.o.setVisibility(8);
            } else if (i3 == 2) {
                this.o.setVisibility(0);
            }
        }
        if (str != null && str.equals("2")) {
            this.e.a(7, Integer.valueOf(i2));
        }
    }

    public void videoOperate(int i2) {
        String str = MintegralBaseView.TAG;
        StringBuilder sb = new StringBuilder("VideoView videoOperate:");
        sb.append(i2);
        g.a(str, sb.toString());
        if (this.f) {
            if (i2 == 1) {
                if (getVisibility() == 0 && isfront()) {
                    g.a(MintegralBaseView.TAG, "VideoView videoOperate:play");
                    if (!this.r) {
                        g();
                    }
                }
            } else if (i2 == 2) {
                if (getVisibility() == 0 && isfront()) {
                    g.a(MintegralBaseView.TAG, "VideoView videoOperate:pause");
                    f();
                }
            } else if (i2 == 3 && !this.D) {
                this.n.release();
                this.D = true;
            }
        }
    }

    public void closeVideoOperate(int i2, int i3) {
        if (i2 == 1) {
            d();
        }
        if (i3 == 1) {
            if (this.f && this.q.getVisibility() != 8) {
                this.q.setVisibility(8);
                this.F = false;
            }
            if (!this.P && !this.I && !this.G) {
                this.P = true;
                if (this.t >= 0) {
                    if (this.t == 0) {
                        this.I = true;
                        return;
                    }
                    new Handler().postDelayed(new Runnable() {
                        public final void run() {
                            MintegralVideoView.this.I = true;
                        }
                    }, (long) (this.t * 1000));
                }
                return;
            }
            return;
        }
        if (i3 == 2 && this.f && this.q.getVisibility() != 0) {
            this.q.setVisibility(0);
            this.F = true;
        }
    }

    public void progressOperate(int i2, int i3) {
        if (this.f) {
            String str = MintegralBaseView.TAG;
            StringBuilder sb = new StringBuilder("progressOperate progress:");
            sb.append(i2);
            g.b(str, sb.toString());
            int videoLength = this.b != null ? this.b.getVideoLength() : 0;
            if (i2 > 0 && i2 <= videoLength && this.n != null) {
                String str2 = MintegralBaseView.TAG;
                StringBuilder sb2 = new StringBuilder("progressOperate progress:");
                sb2.append(i2);
                g.b(str2, sb2.toString());
                this.n.seekTo(i2 * 1000);
            }
            if (i3 == 1) {
                this.p.setVisibility(8);
            } else if (i3 == 2) {
                this.p.setVisibility(0);
            }
        }
    }

    public String getCurrentProgress() {
        try {
            int a2 = this.O.a();
            int i2 = 0;
            if (this.b != null) {
                i2 = this.b.getVideoLength();
            }
            JSONObject jSONObject = new JSONObject();
            jSONObject.put(NotificationCompat.CATEGORY_PROGRESS, a(a2, i2));
            jSONObject.put(LocationConst.TIME, a2);
            jSONObject.put(IronSourceConstants.EVENTS_DURATION, String.valueOf(i2));
            return jSONObject.toString();
        } catch (Throwable th) {
            g.c(MintegralBaseView.TAG, th.getMessage(), th);
            return "{}";
        }
    }

    public void setScaleFitXY(int i2) {
        this.K = i2;
    }

    public void setVisible(int i2) {
        setVisibility(i2);
    }

    public void setCover(boolean z2) {
        if (this.f) {
            this.E = z2;
            this.n.setIsCovered(z2);
        }
    }

    public void setShowingAlertViewCover(boolean z2) {
        this.n.setIsCovered(z2);
    }

    public boolean isH5Canvas() {
        return getLayoutParams().height < k.h(this.f2997a.getApplicationContext());
    }

    public int getBorderViewHeight() {
        return m;
    }

    public int getBorderViewWidth() {
        return l;
    }

    public int getBorderViewLeft() {
        return k;
    }

    public int getBorderViewTop() {
        return j;
    }

    public int getBorderViewRadius() {
        return i;
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        if (this.f && this.B) {
            h();
        }
    }

    private static String a(int i2, int i3) {
        if (i3 != 0) {
            double d = (double) (((float) i2) / ((float) i3));
            try {
                StringBuilder sb = new StringBuilder();
                sb.append(k.a(Double.valueOf(d)));
                return sb.toString();
            } catch (Throwable th) {
                th.printStackTrace();
            }
        }
        return String.valueOf(i3);
    }

    public int getCloseAlert() {
        return this.v;
    }

    public void setCloseAlert(int i2) {
        this.v = i2;
    }

    public int getVideoSkipTime() {
        return this.t;
    }

    public void setVideoSkipTime(int i2) {
        this.t = i2;
    }

    public void setPlayURL(String str) {
        this.s = str;
    }

    public void setBufferTimeout(int i2) {
        this.u = i2;
    }

    private boolean e() {
        try {
            this.n = (PlayerView) findViewById(findID("mintegral_vfpv"));
            this.o = (SoundImageView) findViewById(findID("mintegral_sound_switch"));
            this.p = (TextView) findViewById(findID("mintegral_tv_sound"));
            this.q = findViewById(findID("mintegral_rl_playing_close"));
            return isNotNULL(this.n, this.o, this.p, this.q);
        } catch (Throwable th) {
            g.c(MintegralBaseView.TAG, th.getMessage(), th);
            return false;
        }
    }

    public boolean isfront() {
        ViewGroup viewGroup = (ViewGroup) getParent();
        if (viewGroup == null) {
            return false;
        }
        int indexOfChild = viewGroup.indexOfChild(this);
        int childCount = viewGroup.getChildCount();
        int i2 = indexOfChild + 1;
        boolean z2 = false;
        while (i2 <= childCount - 1) {
            if (viewGroup.getChildAt(i2).getVisibility() == 0 && this.E) {
                return false;
            }
            i2++;
            z2 = true;
        }
        return z2;
    }

    private void f() {
        try {
            if (this.n != null) {
                this.n.onPause();
            }
        } catch (Throwable th) {
            g.c(MintegralBaseView.TAG, th.getMessage(), th);
        }
    }

    /* access modifiers changed from: private */
    public void g() {
        try {
            if (!this.C) {
                this.n.playVideo();
                this.C = true;
                return;
            }
            this.n.onResume();
        } catch (Exception e) {
            g.c(MintegralBaseView.TAG, e.getMessage(), e);
        }
    }

    private void h() {
        float i2 = (float) k.i(this.f2997a);
        float h = (float) k.h(this.f2997a);
        if (this.z <= Utils.DOUBLE_EPSILON || this.A <= Utils.DOUBLE_EPSILON || i2 <= 0.0f || h <= 0.0f) {
            try {
                setLayoutParam(0, 0, -1, -1);
                if (!isLandscape() && this.f) {
                    LayoutParams layoutParams = (LayoutParams) this.n.getLayoutParams();
                    int i3 = k.i(this.f2997a);
                    layoutParams.width = -1;
                    layoutParams.height = (i3 * 9) / 16;
                    layoutParams.addRule(13);
                }
            } catch (Throwable th) {
                th.printStackTrace();
            }
        } else {
            double d = this.z / this.A;
            double d2 = (double) (i2 / h);
            String str = MintegralBaseView.TAG;
            StringBuilder sb = new StringBuilder("videoWHDivide:");
            sb.append(d);
            sb.append("  screenWHDivide:");
            sb.append(d2);
            g.b(str, sb.toString());
            double a2 = k.a(Double.valueOf(d));
            double a3 = k.a(Double.valueOf(d2));
            String str2 = MintegralBaseView.TAG;
            StringBuilder sb2 = new StringBuilder("videoWHDivideFinal:");
            sb2.append(a2);
            sb2.append("  screenWHDivideFinal:");
            sb2.append(a3);
            g.b(str2, sb2.toString());
            LayoutParams layoutParams2 = (LayoutParams) this.n.getLayoutParams();
            if (a2 > a3) {
                double d3 = (double) i2;
                double d4 = this.A;
                Double.isNaN(d3);
                double d5 = (d3 * d4) / this.z;
                layoutParams2.width = -1;
                layoutParams2.height = (int) d5;
                layoutParams2.addRule(13, -1);
            } else if (a2 < a3) {
                double d6 = (double) h;
                Double.isNaN(d6);
                layoutParams2.width = (int) (d6 * d);
                layoutParams2.height = -1;
                layoutParams2.addRule(13, -1);
            } else {
                layoutParams2.width = -1;
                layoutParams2.height = -1;
            }
            this.n.setLayoutParams(layoutParams2);
            setMatchParent();
        }
    }

    public void onBackPress() {
        if (!this.E && !this.r) {
            if (this.F) {
                d();
            } else if (!this.G || !this.H) {
                if (!this.G && this.I) {
                    d();
                }
            } else {
                d();
            }
        }
    }

    public void notifyCloseBtn(int i2) {
        if (i2 == 0) {
            this.G = true;
            this.I = false;
            return;
        }
        if (i2 == 1) {
            this.H = true;
        }
    }

    public void notifyVideoClose() {
        this.e.a(2, "");
    }
}
