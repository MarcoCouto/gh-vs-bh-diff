package com.mintegral.msdk.video.module;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.RelativeLayout.LayoutParams;
import com.mintegral.msdk.widget.a;

public class MintegralVastEndCardView extends MintegralBaseView {
    private ViewGroup i;
    private View j;
    private View k;

    public void preLoadData() {
    }

    public MintegralVastEndCardView(Context context) {
        super(context);
    }

    public MintegralVastEndCardView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public void init(Context context) {
        int findLayout = findLayout("mintegral_reward_endcard_vast");
        if (findLayout >= 0) {
            this.c.inflate(findLayout, this);
            this.i = (ViewGroup) findViewById(findID("mintegral_rl_content"));
            this.j = findViewById(findID("mintegral_iv_vastclose"));
            this.k = findViewById(findID("mintegral_iv_vastok"));
            this.f = isNotNULL(this.i, this.j, this.k);
            a();
            if (this.f) {
                setMatchParent();
                setBackgroundResource(findColor("mintegral_reward_endcard_vast_bg"));
                setClickable(true);
                ((LayoutParams) this.i.getLayoutParams()).addRule(13, -1);
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void a() {
        super.a();
        if (this.f) {
            this.j.setOnClickListener(new OnClickListener() {
                public final void onClick(View view) {
                    MintegralVastEndCardView.this.e.a(104, "");
                }
            });
            this.k.setOnClickListener(new a() {
                /* access modifiers changed from: protected */
                public final void a() {
                    MintegralVastEndCardView.this.e.a(108, MintegralVastEndCardView.this.b());
                }
            });
        }
    }

    public void notifyShowListener() {
        this.e.a(111, "");
    }
}
