package com.mintegral.msdk.video.module;

import android.content.Context;
import android.util.AttributeSet;

public class MintegralPlayableView extends MintegralH5EndCardView {
    public MintegralPlayableView(Context context) {
        super(context);
    }

    public MintegralPlayableView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public void init(Context context) {
        super.init(context);
    }

    /* access modifiers changed from: protected */
    public final String f() {
        return super.f();
    }

    public void preLoadData() {
        super.preLoadData();
        super.setLoadPlayable(true);
    }

    public void onBackPress() {
        super.onBackPress();
    }
}
