package com.mintegral.msdk.video.module;

import android.content.Context;
import android.content.res.Configuration;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.base.utils.k;
import com.mintegral.msdk.base.utils.p;
import com.mintegral.msdk.video.module.a.a;
import com.mintegral.msdk.video.module.a.a.f;
import org.json.JSONException;
import org.json.JSONObject;

public abstract class MintegralBaseView extends RelativeLayout {
    public static final String TAG = "MintegralBaseView";

    /* renamed from: a reason: collision with root package name */
    protected Context f2997a;
    protected CampaignEx b;
    protected LayoutInflater c;
    protected int d;
    protected a e;
    protected boolean f;
    protected float g;
    protected float h;

    /* access modifiers changed from: protected */
    public void a() {
    }

    public abstract void init(Context context);

    public MintegralBaseView(Context context) {
        this(context, null);
    }

    public MintegralBaseView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.d = 1;
        this.e = new f();
        this.f = false;
        this.f2997a = context;
        this.c = LayoutInflater.from(context);
        init(context);
    }

    public void onConfigurationChanged(Configuration configuration) {
        this.d = configuration.orientation;
        super.onConfigurationChanged(configuration);
        String str = TAG;
        StringBuilder sb = new StringBuilder("onConfigurationChanged:");
        sb.append(configuration.orientation);
        Log.d(str, sb.toString());
    }

    public void onSelfConfigurationChanged(Configuration configuration) {
        this.d = configuration.orientation;
        String str = TAG;
        StringBuilder sb = new StringBuilder("onSelfConfigurationChanged:");
        sb.append(configuration.orientation);
        Log.d(str, sb.toString());
    }

    public void setNotifyListener(a aVar) {
        this.e = aVar;
    }

    public void setCampaign(CampaignEx campaignEx) {
        this.b = campaignEx;
    }

    public int findID(String str) {
        return p.a(this.f2997a.getApplicationContext(), str, "id");
    }

    public int findLayout(String str) {
        return p.a(this.f2997a.getApplicationContext(), str, TtmlNode.TAG_LAYOUT);
    }

    public int findColor(String str) {
        return p.a(this.f2997a.getApplicationContext(), str, "color");
    }

    public int findDrawable(String str) {
        return p.a(this.f2997a.getApplicationContext(), str, "drawable");
    }

    public boolean isNotNULL(View... viewArr) {
        if (viewArr == null) {
            return false;
        }
        int length = viewArr.length;
        int i = 0;
        boolean z = false;
        while (i < length) {
            if (viewArr[i] == null) {
                return false;
            }
            i++;
            z = true;
        }
        return z;
    }

    public void setMatchParent() {
        LayoutParams layoutParams = getLayoutParams();
        if (layoutParams == null) {
            setLayoutParams(new LayoutParams(-1, -1));
            return;
        }
        layoutParams.height = -1;
        layoutParams.width = -1;
    }

    public void setWrapContent() {
        LayoutParams layoutParams = getLayoutParams();
        if (layoutParams == null) {
            setLayoutParams(new LayoutParams(-2, -2));
            return;
        }
        layoutParams.height = -2;
        layoutParams.width = -2;
    }

    public void defaultShow() {
        g.a(TAG, "defaultShow");
    }

    public boolean isLandscape() {
        return this.f2997a.getResources().getConfiguration().orientation == 2;
    }

    public void setLayoutCenter(int i, int i2) {
        RelativeLayout.LayoutParams parentRelativeLayoutParams = getParentRelativeLayoutParams();
        LinearLayout.LayoutParams parentLinearLayoutParams = getParentLinearLayoutParams();
        if (parentRelativeLayoutParams != null) {
            parentRelativeLayoutParams.addRule(13);
            if (i != -999) {
                parentRelativeLayoutParams.width = i;
            }
            if (i2 != -999) {
                parentRelativeLayoutParams.height = i2;
            }
            setLayoutParams(parentRelativeLayoutParams);
            return;
        }
        if (parentLinearLayoutParams != null) {
            parentLinearLayoutParams.gravity = 17;
            if (i != -999) {
                parentLinearLayoutParams.width = i;
            }
            if (i2 != -999) {
                parentLinearLayoutParams.height = i2;
            }
            setLayoutParams(parentLinearLayoutParams);
        }
    }

    public void setLayoutParam(int i, int i2, int i3, int i4) {
        RelativeLayout.LayoutParams parentRelativeLayoutParams = getParentRelativeLayoutParams();
        LinearLayout.LayoutParams parentLinearLayoutParams = getParentLinearLayoutParams();
        if (parentRelativeLayoutParams != null) {
            parentRelativeLayoutParams.topMargin = i2;
            parentRelativeLayoutParams.leftMargin = i;
            if (i3 != -999) {
                parentRelativeLayoutParams.width = i3;
            }
            if (i4 != -999) {
                parentRelativeLayoutParams.height = i4;
            }
            setLayoutParams(parentRelativeLayoutParams);
            return;
        }
        if (parentLinearLayoutParams != null) {
            parentLinearLayoutParams.topMargin = i2;
            parentLinearLayoutParams.leftMargin = i;
            if (i3 != -999) {
                parentLinearLayoutParams.width = i3;
            }
            if (i4 != -999) {
                parentLinearLayoutParams.height = i4;
            }
            setLayoutParams(parentLinearLayoutParams);
        }
    }

    public RelativeLayout.LayoutParams getParentRelativeLayoutParams() {
        LayoutParams layoutParams = getLayoutParams();
        if (layoutParams instanceof RelativeLayout.LayoutParams) {
            return (RelativeLayout.LayoutParams) layoutParams;
        }
        return null;
    }

    public LinearLayout.LayoutParams getParentLinearLayoutParams() {
        LayoutParams layoutParams = getLayoutParams();
        if (layoutParams instanceof LinearLayout.LayoutParams) {
            return (LinearLayout.LayoutParams) layoutParams;
        }
        return null;
    }

    public CampaignEx getCampaign() {
        return this.b;
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        this.g = motionEvent.getRawX();
        this.h = motionEvent.getRawY();
        return super.onInterceptTouchEvent(motionEvent);
    }

    /* access modifiers changed from: protected */
    public final String b() {
        return c().toString();
    }

    /* access modifiers changed from: protected */
    public final JSONObject c() {
        JSONObject jSONObject;
        JSONException e2;
        JSONObject jSONObject2 = new JSONObject();
        try {
            jSONObject = new JSONObject();
            try {
                jSONObject.put(com.mintegral.msdk.base.common.a.x, k.a(com.mintegral.msdk.base.controller.a.d().h(), this.g));
                jSONObject.put(com.mintegral.msdk.base.common.a.y, k.a(com.mintegral.msdk.base.controller.a.d().h(), this.h));
                jSONObject.put(com.mintegral.msdk.base.common.a.A, 0);
                try {
                    this.d = getContext().getResources().getConfiguration().orientation;
                } catch (Exception e3) {
                    e3.printStackTrace();
                }
                jSONObject.put(com.mintegral.msdk.base.common.a.B, this.d);
                jSONObject.put(com.mintegral.msdk.base.common.a.C, (double) k.c(getContext()));
            } catch (JSONException e4) {
                e2 = e4;
                e2.printStackTrace();
                return jSONObject;
            }
        } catch (JSONException e5) {
            JSONException jSONException = e5;
            jSONObject = jSONObject2;
            e2 = jSONException;
            e2.printStackTrace();
            return jSONObject;
        }
        return jSONObject;
    }
}
