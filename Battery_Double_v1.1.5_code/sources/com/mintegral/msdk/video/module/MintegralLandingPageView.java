package com.mintegral.msdk.video.module;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.webkit.URLUtil;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.base.utils.j;
import com.mintegral.msdk.click.b;

public class MintegralLandingPageView extends MintegralH5EndCardView {

    private static final class a implements com.mintegral.msdk.mtgjscommon.base.a {
        private a() {
        }

        /* synthetic */ a(byte b) {
            this();
        }

        public final boolean a(String str) {
            if (TextUtils.isEmpty(str) || URLUtil.isNetworkUrl(str)) {
                return false;
            }
            j.b(com.mintegral.msdk.base.controller.a.d().h(), str);
            return true;
        }
    }

    public MintegralLandingPageView(Context context) {
        super(context);
    }

    public MintegralLandingPageView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public void init(Context context) {
        super.init(context);
    }

    /* access modifiers changed from: protected */
    public final String f() {
        if (this.b != null) {
            return b.a(this.b.getClickURL(), "-999", "-999");
        }
        return null;
    }

    public void preLoadData() {
        if (this.f) {
            this.k.setFilter(new a(0));
        }
        super.preLoadData();
        setVisibility(0);
        setCloseVisible(0);
    }

    public void webviewshow() {
        try {
            g.a(MintegralBaseView.TAG, "webviewshow");
            com.mintegral.msdk.mtgjscommon.windvane.g.a();
            com.mintegral.msdk.mtgjscommon.windvane.g.a(this.k, "webviewshow", "");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
