package com.mintegral.msdk.video.module;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Handler;
import android.os.Looper;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.RelativeLayout;
import com.mintegral.msdk.base.common.d.a;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.entity.p;
import com.mintegral.msdk.video.js.d;
import com.mintegral.msdk.video.js.f;
import com.mintegral.msdk.video.module.a.a.g;
import com.mintegral.msdk.video.module.a.a.i;
import com.mintegral.msdk.video.module.a.a.l;

public class MintegralContainerView extends MintegralBaseView implements d, f {
    private boolean A = false;
    private MintegralPlayableView i;
    private MintegralClickCTAView j;
    private MintegralClickMiniCardView k;
    private MintegralNativeEndCardView l;
    private MintegralH5EndCardView m;
    private MintegralVastEndCardView n;
    private MintegralLandingPageView o;
    /* access modifiers changed from: private */
    public String p;
    private int q;
    private int r = 1;
    private int s = 1;
    private int t = 1;
    private boolean u = false;
    private boolean v = false;
    private boolean w = false;
    private boolean x = true;
    private boolean y = false;
    private int z;

    public int getVideoSkipTime() {
        return this.z;
    }

    public void setVideoSkipTime(int i2) {
        this.z = i2;
    }

    public MintegralContainerView(Context context) {
        super(context);
    }

    public MintegralContainerView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public void init(Context context) {
        setVisibility(0);
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        MintegralBaseView[] mintegralBaseViewArr = {this.i, this.j, this.k, this.l, this.m, this.n, this.o};
        for (int i2 = 0; i2 < 7; i2++) {
            MintegralBaseView mintegralBaseView = mintegralBaseViewArr[i2];
            if (mintegralBaseView != null && (mintegralBaseView instanceof MintegralClickMiniCardView)) {
                mintegralBaseView.onSelfConfigurationChanged(configuration);
            } else if (mintegralBaseView != null && mintegralBaseView.getVisibility() == 0 && mintegralBaseView.getParent() != null && !isLast()) {
                mintegralBaseView.onSelfConfigurationChanged(configuration);
            }
        }
    }

    public void preLoadData() {
        if (this.b != null) {
            if (this.b.getPlayable_ads_without_video() == 2) {
                if (this.i == null) {
                    this.i = new MintegralPlayableView(this.f2997a);
                }
                this.i.setCloseDelayShowTime(this.s);
                this.i.setPlayCloseBtnTm(this.t);
                this.i.setCampaign(this.b);
                this.i.setNotifyListener(new i(this.e) {
                    public final void a(int i, Object obj) {
                        super.a(i, obj);
                        if (i == 100) {
                            MintegralContainerView.this.webviewshow();
                            MintegralContainerView.this.onConfigurationChanged(MintegralContainerView.this.getResources().getConfiguration());
                            p pVar = new p();
                            pVar.k(MintegralContainerView.this.b.getRequestIdNotice());
                            pVar.m(MintegralContainerView.this.b.getId());
                            pVar.a(MintegralContainerView.this.b.isMraid() ? p.f2605a : p.b);
                            a.b(pVar, MintegralContainerView.this.f2997a, MintegralContainerView.this.p);
                        }
                    }
                });
                this.i.preLoadData();
                return;
            }
            a(this.q);
            new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                public final void run() {
                    MintegralContainerView.this.a(Integer.valueOf(MintegralContainerView.this.b.getVideo_end_type()));
                }
            }, (long) getVideoSkipTime());
        }
    }

    public void showPlayableView() {
        if (this.b != null) {
            removeAllViews();
            setMatchParent();
            if (this.i == null) {
                preLoadData();
            }
            addView(this.i);
            if (this.i != null) {
                this.i.setUnitId(this.p);
            }
            bringToFront();
        }
    }

    public void showVideoClickView(int i2) {
        if (this.b != null) {
            if (i2 != -1) {
                switch (i2) {
                    case 1:
                        if (!this.u) {
                            if (!(this.m == null || this.m.getParent() == null)) {
                                removeView(this.m);
                            }
                            if (!(this.k == null || this.k.getParent() == null)) {
                                removeView(this.k);
                            }
                            if (this.j == null || this.j.getParent() == null) {
                                setWrapContent();
                                LayoutParams layoutParams = getLayoutParams();
                                if (layoutParams instanceof RelativeLayout.LayoutParams) {
                                    ((RelativeLayout.LayoutParams) layoutParams).addRule(12, -1);
                                }
                                try {
                                    if (this.b != null && this.b.getPlayable_ads_without_video() == 1) {
                                        if (this.j == null) {
                                            a(-1);
                                        }
                                        addView(this.j);
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                            if (isLast()) {
                                bringToFront();
                                return;
                            }
                        } else {
                            return;
                        }
                        break;
                    case 2:
                        if (!(this.j == null || this.j.getParent() == null)) {
                            removeView(this.j);
                        }
                        if (this.k == null || this.k.getParent() == null) {
                            try {
                                if (this.b != null && this.b.getPlayable_ads_without_video() == 1) {
                                    setMatchParent();
                                    f();
                                }
                            } catch (Exception e2) {
                                e2.printStackTrace();
                            }
                        }
                        if (!miniCardLoaded()) {
                            g();
                            break;
                        } else {
                            if (!(this.m == null || this.m.getParent() == null)) {
                                removeView(this.m);
                            }
                            this.e.a(112, "");
                            if (this.b != null && !this.b.isHasReportAdTrackPause()) {
                                this.b.setHasReportAdTrackPause(true);
                                com.mintegral.msdk.video.module.b.a.f(this.f2997a, this.b);
                            }
                            if (this.w) {
                                this.e.a(115, "");
                            } else {
                                bringToFront();
                                webviewshow();
                                onConfigurationChanged(getResources().getConfiguration());
                            }
                            this.v = true;
                            return;
                        }
                }
            } else if (!isLast() && !endCardShowing()) {
                g();
            }
        }
    }

    public void showEndcard(int i2) {
        if (this.b != null) {
            if (i2 != 1) {
                switch (i2) {
                    case 3:
                        removeAllViews();
                        setMatchParent();
                        if (this.n == null) {
                            a(Integer.valueOf(3));
                        }
                        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -1);
                        layoutParams.addRule(13, -1);
                        addView(this.n, layoutParams);
                        this.n.notifyShowListener();
                        bringToFront();
                        break;
                    case 4:
                        this.e.a(113, "");
                        removeAllViews();
                        setMatchParent();
                        if (this.o == null) {
                            a(Integer.valueOf(4));
                        }
                        this.o.preLoadData();
                        addView(this.o);
                        bringToFront();
                        break;
                    case 5:
                        this.e.a(106, "");
                        break;
                    default:
                        removeAllViews();
                        setMatchParent();
                        bringToFront();
                        d();
                        this.e.a(117, "");
                        break;
                }
            } else {
                this.e.a(104, "");
            }
        }
        this.u = true;
    }

    public boolean endCardShowing() {
        return this.u;
    }

    public boolean miniCardShowing() {
        return this.v;
    }

    public boolean miniCardLoaded() {
        return this.k != null && this.k.isLoadSuccess();
    }

    public void toggleCloseBtn(int i2) {
        if (this.i != null) {
            this.i.toggleCloseBtn(i2);
        }
        if (this.m != null) {
            this.m.toggleCloseBtn(i2);
        }
    }

    public void notifyCloseBtn(int i2) {
        if (this.i != null) {
            this.i.notifyCloseBtn(i2);
        }
        if (this.m != null) {
            this.m.notifyCloseBtn(i2);
        }
    }

    public void triggerCloseBtn(String str) {
        if (this.b != null) {
            this.e.a(122, "");
        }
    }

    public void readyStatus(int i2) {
        if (this.m != null) {
            this.m.readyStatus(i2);
        }
    }

    public void handlerPlayableException(String str) {
        if (this.m != null) {
            this.m.handlerPlayableException(str);
            if (!this.A) {
                return;
            }
        }
        d();
    }

    public void showMiniCard(int i2, int i3, int i4, int i5, int i6) {
        if (this.k != null) {
            this.k.setMiniCardLocation(i2, i3, i4, i5);
            this.k.setRadius(i6);
            this.k.setCloseVisible(8);
            this.k.setClickable(false);
            removeAllViews();
            setMatchParent();
            bringToFront();
            f();
            if (!this.y) {
                this.y = true;
                this.e.a(109, "");
                this.e.a(117, "");
            }
        }
    }

    public void resizeMiniCard(int i2, int i3, int i4) {
        if (this.k != null) {
            this.k.resizeMiniCard(i2, i3);
            this.k.setRadius(i4);
            removeAllViews();
            setMatchParent();
            bringToFront();
            f();
        }
    }

    public void configurationChanged(int i2, int i3, int i4) {
        if (this.k != null && this.k.getVisibility() == 0) {
            this.k.resizeMiniCard(i2, i3);
        }
    }

    public void webviewshow() {
        MintegralH5EndCardView[] mintegralH5EndCardViewArr = {this.i, this.k, this.m};
        for (int i2 = 0; i2 < 3; i2++) {
            MintegralH5EndCardView mintegralH5EndCardView = mintegralH5EndCardViewArr[i2];
            if (mintegralH5EndCardView != null && mintegralH5EndCardView.getVisibility() == 0 && mintegralH5EndCardView.getParent() != null && !isLast()) {
                mintegralH5EndCardView.webviewshow();
            }
        }
    }

    public void install(CampaignEx campaignEx) {
        this.e.a(105, campaignEx);
    }

    public void orientation(Configuration configuration) {
        MintegralH5EndCardView[] mintegralH5EndCardViewArr = {this.i, this.k, this.m};
        for (int i2 = 0; i2 < 3; i2++) {
            MintegralH5EndCardView mintegralH5EndCardView = mintegralH5EndCardViewArr[i2];
            if (mintegralH5EndCardView != null && mintegralH5EndCardView.getVisibility() == 0) {
                mintegralH5EndCardView.orientation(getResources().getConfiguration());
            }
        }
    }

    public void setNotifyListener(com.mintegral.msdk.video.module.a.a aVar) {
        super.setNotifyListener(aVar);
        MintegralBaseView[] mintegralBaseViewArr = {this.i, this.j, this.k, this.l, this.m, this.n, this.o};
        for (int i2 = 0; i2 < 7; i2++) {
            MintegralBaseView mintegralBaseView = mintegralBaseViewArr[i2];
            if (mintegralBaseView != null) {
                if (mintegralBaseView instanceof MintegralClickMiniCardView) {
                    mintegralBaseView.setNotifyListener(new g(this.k, aVar));
                } else {
                    mintegralBaseView.setNotifyListener(new i(aVar));
                }
            }
        }
    }

    public void defaultShow() {
        super.defaultShow();
    }

    public void addView(View view) {
        if (view != null) {
            a(view);
            super.addView(view);
            return;
        }
        com.mintegral.msdk.base.utils.g.d(MintegralBaseView.TAG, "view is null");
    }

    public void addView(View view, LayoutParams layoutParams) {
        if (view != null) {
            a(view);
            super.addView(view, layoutParams);
            return;
        }
        com.mintegral.msdk.base.utils.g.d(MintegralBaseView.TAG, "view is null");
    }

    public boolean canBackPress() {
        if (this.l != null) {
            return true;
        }
        if (this.m != null) {
            return this.m.canBackPress();
        }
        if (this.o != null) {
            return this.o.canBackPress();
        }
        if (this.i != null) {
            return this.i.canBackPress();
        }
        return false;
    }

    public boolean endcardIsPlayable() {
        return this.m != null && this.m.isPlayable();
    }

    public void setShowingTransparent(boolean z2) {
        this.w = z2;
    }

    public boolean getShowingTransparent() {
        return this.w;
    }

    public String getUnitID() {
        return this.p;
    }

    public void setUnitID(String str) {
        this.p = str;
    }

    public void setVideoInteractiveType(int i2) {
        this.q = i2;
    }

    public void setEndscreenType(int i2) {
        this.r = i2;
    }

    public int getVideoInteractiveType() {
        return this.q;
    }

    public void setCloseDelayTime(int i2) {
        this.s = i2;
    }

    public void setPlayCloseBtnTm(int i2) {
        this.t = i2;
    }

    private static void a(View view) {
        if (view != null) {
            try {
                ViewGroup viewGroup = (ViewGroup) view.getParent();
                if (viewGroup != null) {
                    viewGroup.removeView(view);
                }
            } catch (Throwable th) {
                com.mintegral.msdk.base.utils.g.c(MintegralBaseView.TAG, th.getMessage(), th);
            }
        }
    }

    private void d() {
        if (this.r != 2 || this.A) {
            e();
            return;
        }
        if (this.m == null) {
            a(Integer.valueOf(2));
        }
        if (this.m == null || !this.m.isLoadSuccess()) {
            e();
            if (this.m != null) {
                this.m.reportRenderFailed("timeout");
                this.m.setError(true);
            }
        } else {
            this.A = true;
            addView(this.m);
            webviewshow();
            onConfigurationChanged(getResources().getConfiguration());
            this.m.excuteTask();
            p pVar = new p();
            pVar.k(this.b.getRequestIdNotice());
            pVar.m(this.b.getId());
            pVar.a(this.b.isMraid() ? p.f2605a : p.b);
            a.b(pVar, this.f2997a, this.p);
        }
        if (this.m != null) {
            this.m.setUnitId(this.p);
        }
    }

    private void e() {
        this.r = 1;
        if (this.l == null) {
            a(Integer.valueOf(2));
        }
        addView(this.l);
        onConfigurationChanged(getResources().getConfiguration());
        this.l.notifyShowListener();
        bringToFront();
    }

    private void f() {
        if (this.k == null) {
            a(-2);
        }
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -1);
        layoutParams.addRule(13, -1);
        if (this.w && this.x) {
            this.x = false;
            layoutParams.width = 1;
            layoutParams.height = 1;
        }
        addView(this.k, layoutParams);
    }

    /* access modifiers changed from: private */
    public void a(Integer num) {
        if (this.b != null) {
            if (num == null) {
                num = Integer.valueOf(this.b.getVideo_end_type());
            }
            if (!isLast()) {
                g();
            }
            int intValue = num.intValue();
            if (intValue != 1) {
                switch (intValue) {
                    case 3:
                        if (this.n == null) {
                            this.n = new MintegralVastEndCardView(this.f2997a);
                        }
                        this.n.setCampaign(this.b);
                        this.n.setNotifyListener(new l(this.e));
                        this.n.preLoadData();
                        return;
                    case 4:
                        if (this.o == null) {
                            this.o = new MintegralLandingPageView(this.f2997a);
                        }
                        this.o.setCampaign(this.b);
                        this.o.setNotifyListener(new i(this.e));
                        break;
                    case 5:
                        break;
                    default:
                        if (this.r == 2) {
                            if (this.m == null) {
                                this.m = new MintegralH5EndCardView(this.f2997a);
                            }
                            this.m.setCampaign(this.b);
                            this.m.setCloseDelayShowTime(this.s);
                            this.m.setNotifyListener(new i(this.e));
                            this.m.setUnitId(this.p);
                            this.m.preLoadData();
                            com.mintegral.msdk.base.utils.g.a(MintegralBaseView.TAG, "preload H5Endcard");
                            if (!this.w) {
                                String str = MintegralBaseView.TAG;
                                StringBuilder sb = new StringBuilder("showTransparent = ");
                                sb.append(this.w);
                                sb.append(" addview");
                                com.mintegral.msdk.base.utils.g.a(str, sb.toString());
                                addView(this.m);
                                return;
                            }
                        } else {
                            if (this.l == null) {
                                this.l = new MintegralNativeEndCardView(this.f2997a);
                            }
                            this.l.setCampaign(this.b);
                            this.l.setUnitId(this.p);
                            this.l.setNotifyListener(new i(this.e));
                            this.l.preLoadData();
                            return;
                        }
                        break;
                }
            }
        }
    }

    public boolean isLast() {
        ViewGroup viewGroup = (ViewGroup) getParent();
        if (viewGroup == null || viewGroup.indexOfChild(this) != 0) {
            return false;
        }
        return true;
    }

    private void g() {
        this.v = false;
        ViewGroup viewGroup = (ViewGroup) getParent();
        if (viewGroup != null) {
            int i2 = 0;
            for (int i3 = 0; i3 < viewGroup.getChildCount(); i3++) {
                View childAt = viewGroup.getChildAt(i2);
                if (!(childAt instanceof MintegralContainerView)) {
                    viewGroup.bringChildToFront(childAt);
                } else {
                    i2++;
                }
            }
        }
    }

    private void a(int i2) {
        switch (i2) {
            case -3:
                return;
            case -2:
                if (this.b != null && this.b.getVideo_end_type() == 2) {
                    if (this.k == null) {
                        this.k = new MintegralClickMiniCardView(this.f2997a);
                    }
                    this.k.setCampaign(this.b);
                    this.k.setNotifyListener(new g(this.k, this.e));
                    this.k.preLoadData();
                    setMatchParent();
                    f();
                    g();
                    return;
                }
            default:
                if (this.j == null) {
                    this.j = new MintegralClickCTAView(this.f2997a);
                }
                this.j.setCampaign(this.b);
                this.j.setUnitId(this.p);
                this.j.setNotifyListener(new i(this.e));
                this.j.preLoadData();
                break;
        }
    }

    public void setMintegralClickMiniCardViewTransparent() {
        if (this.k != null) {
            this.k.setMintegralClickMiniCardViewTransparent();
            this.k.setMintegralClickMiniCardViewClickable(false);
        }
    }

    public void onPlayableBackPress() {
        if (this.i != null) {
            this.i.onBackPress();
        }
    }

    public void onMiniEndcardBackPress() {
        if (this.v) {
            this.e.a(107, "");
        }
    }

    public void onEndcardBackPress() {
        if (this.l != null || this.n != null) {
            this.e.a(104, "");
        } else if (this.o != null) {
            this.e.a(103, "");
        } else {
            if (this.m != null) {
                this.m.onBackPress();
            }
        }
    }

    public void release() {
        if (this.m != null) {
            this.m.release();
            this.m = null;
        }
        if (this.i != null) {
            this.i.release();
        }
        if (this.o != null) {
            this.o.release();
        }
    }

    public MintegralH5EndCardView getH5EndCardView() {
        if (this.m == null) {
            return this.i;
        }
        return this.m;
    }
}
