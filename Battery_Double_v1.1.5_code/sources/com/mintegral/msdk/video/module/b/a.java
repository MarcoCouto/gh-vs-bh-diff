package com.mintegral.msdk.video.module.b;

import android.content.Context;
import android.text.TextUtils;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.base.common.d.c;
import com.mintegral.msdk.base.common.d.c.b;
import com.mintegral.msdk.base.common.net.l;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.videocommon.b.d;
import java.net.URLEncoder;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import org.json.JSONObject;

/* compiled from: VideoViewReport */
public final class a {

    /* renamed from: a reason: collision with root package name */
    public static boolean f3033a = false;

    public static void a() {
        f3033a = false;
    }

    public static void a(String str, String str2) {
        try {
            if (com.mintegral.msdk.base.controller.a.d().h() != null && !TextUtils.isEmpty(str) && !TextUtils.isEmpty(str2)) {
                com.mintegral.msdk.base.common.d.c.a aVar = new com.mintegral.msdk.base.common.d.c.a(com.mintegral.msdk.base.controller.a.d().h());
                aVar.c();
                aVar.b(com.mintegral.msdk.base.common.a.f, c.a(str, com.mintegral.msdk.base.controller.a.d().h(), str2), new b() {
                    public final void a(String str) {
                        g.d("VideoViewReport", str);
                    }

                    public final void b(String str) {
                        g.d("VideoViewReport", str);
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
            g.d("VideoViewReport", e.getMessage());
        }
    }

    public static void a(Context context, CampaignEx campaignEx) {
        if (campaignEx != null && campaignEx.getNativeVideoTracking() != null && campaignEx.getNativeVideoTracking().n() != null) {
            com.mintegral.msdk.click.a.a(context, campaignEx, campaignEx.getCampaignUnitId(), campaignEx.getNativeVideoTracking().n(), false);
        }
    }

    public static void b(Context context, CampaignEx campaignEx) {
        if (campaignEx != null && campaignEx.getNativeVideoTracking() != null && campaignEx.getNativeVideoTracking().h() != null) {
            com.mintegral.msdk.click.a.a(context, campaignEx, campaignEx.getCampaignUnitId(), campaignEx.getNativeVideoTracking().h(), false);
        }
    }

    public static void c(Context context, CampaignEx campaignEx) {
        if (campaignEx != null && campaignEx.getNativeVideoTracking() != null && campaignEx.getNativeVideoTracking().i() != null) {
            com.mintegral.msdk.click.a.a(context, campaignEx, campaignEx.getCampaignUnitId(), campaignEx.getNativeVideoTracking().i(), false);
        }
    }

    public static void d(Context context, CampaignEx campaignEx) {
        if (!f3033a && campaignEx != null && campaignEx.getNativeVideoTracking() != null && campaignEx.getNativeVideoTracking().j() != null) {
            com.mintegral.msdk.click.a.a(context, campaignEx, campaignEx.getCampaignUnitId(), campaignEx.getNativeVideoTracking().j(), false);
            f3033a = true;
        }
    }

    public static void a(Context context, CampaignEx campaignEx, int i) {
        try {
            String[] o = campaignEx.getNativeVideoTracking().o();
            if (!(campaignEx == null || campaignEx.getNativeVideoTracking() == null || o == null)) {
                String[] strArr = new String[o.length];
                for (int i2 = 0; i2 < o.length; i2++) {
                    String str = o[i2];
                    JSONObject jSONObject = new JSONObject();
                    jSONObject.put("endscreen_type", i);
                    String jSONObject2 = jSONObject.toString();
                    StringBuilder sb = new StringBuilder();
                    sb.append(str);
                    sb.append("&value=");
                    sb.append(URLEncoder.encode(com.mintegral.msdk.base.utils.a.b(jSONObject2)));
                    strArr[i2] = sb.toString();
                }
                com.mintegral.msdk.click.a.a(context, campaignEx, campaignEx.getCampaignUnitId(), strArr, true);
            }
        } catch (Throwable unused) {
            g.d("", "reportEndcardshowData error");
        }
    }

    public static void e(Context context, CampaignEx campaignEx) {
        if (campaignEx != null && campaignEx.getNativeVideoTracking() != null && campaignEx.getNativeVideoTracking().p() != null) {
            com.mintegral.msdk.click.a.a(context, campaignEx, campaignEx.getCampaignUnitId(), campaignEx.getNativeVideoTracking().p(), false);
        }
    }

    public static void f(Context context, CampaignEx campaignEx) {
        if (campaignEx != null && campaignEx.getNativeVideoTracking() != null && campaignEx.getNativeVideoTracking().k() != null) {
            com.mintegral.msdk.click.a.a(context, campaignEx, campaignEx.getCampaignUnitId(), campaignEx.getNativeVideoTracking().k(), false);
        }
    }

    public static void a(Context context, CampaignEx campaignEx, int i, int i2) {
        if (i2 != 0 && context != null && campaignEx != null) {
            try {
                List g = campaignEx.getNativeVideoTracking().g();
                int i3 = ((i + 1) * 100) / i2;
                if (g != null) {
                    int i4 = 0;
                    while (i4 < g.size()) {
                        Map map = (Map) g.get(i4);
                        if (map != null && map.size() > 0) {
                            Iterator it = map.entrySet().iterator();
                            while (it.hasNext()) {
                                Entry entry = (Entry) it.next();
                                int intValue = ((Integer) entry.getKey()).intValue();
                                String str = (String) entry.getValue();
                                if (intValue <= i3 && !TextUtils.isEmpty(str)) {
                                    com.mintegral.msdk.click.a.a(context, campaignEx, campaignEx.getCampaignUnitId(), new String[]{str}, true);
                                    it.remove();
                                    g.remove(i4);
                                    i4--;
                                }
                            }
                        }
                        i4++;
                    }
                }
            } catch (Throwable unused) {
                g.d("", "reportPlayPercentageData error");
            }
        }
    }

    public static void a(CampaignEx campaignEx, Map<Integer, String> map, String str, int i) {
        if (!(campaignEx == null || map == null)) {
            try {
                if (map.size() > 0) {
                    Iterator it = map.entrySet().iterator();
                    while (it.hasNext()) {
                        Entry entry = (Entry) it.next();
                        String str2 = (String) entry.getValue();
                        if (i == ((Integer) entry.getKey()).intValue() && !TextUtils.isEmpty(str2)) {
                            com.mintegral.msdk.click.a.a(com.mintegral.msdk.base.controller.a.d().h(), campaignEx, str, str2, false, false);
                            it.remove();
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static void a(CampaignEx campaignEx, String str) {
        if (campaignEx != null) {
            try {
                if (campaignEx.getAdUrlList() != null && campaignEx.getAdUrlList().size() > 0) {
                    for (String str2 : campaignEx.getAdUrlList()) {
                        if (!TextUtils.isEmpty(str2)) {
                            com.mintegral.msdk.click.a.a(com.mintegral.msdk.base.controller.a.d().h(), campaignEx, str, str2, false, false);
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static void a(CampaignEx campaignEx, d dVar, String str, String str2) {
        if (campaignEx != null) {
            try {
                com.mintegral.msdk.video.module.c.a aVar = new com.mintegral.msdk.video.module.c.a(com.mintegral.msdk.base.controller.a.d().h());
                l lVar = new l();
                lVar.a("user_id", com.mintegral.msdk.base.utils.a.b(str2));
                lVar.a("cb_type", "1");
                lVar.a(CampaignEx.JSON_KEY_REWARD_NAME, dVar.a());
                String str3 = CampaignEx.JSON_KEY_REWARD_AMOUNT;
                StringBuilder sb = new StringBuilder();
                sb.append(dVar.b());
                lVar.a(str3, sb.toString());
                lVar.a(MIntegralConstans.PROPERTIES_UNIT_ID, str);
                lVar.a("click_id", campaignEx.getRequestIdNotice());
                aVar.b(lVar);
                StringBuilder sb2 = new StringBuilder();
                sb2.append(campaignEx.getHost());
                sb2.append("/addReward?");
                String sb3 = sb2.toString();
                String str4 = "";
                String trim = lVar.b().trim();
                if (!TextUtils.isEmpty(trim)) {
                    if (!sb3.endsWith("?") && !sb3.endsWith(RequestParameters.AMPERSAND)) {
                        StringBuilder sb4 = new StringBuilder();
                        sb4.append(sb3);
                        sb4.append(sb3.contains("?") ? RequestParameters.AMPERSAND : "?");
                        sb3 = sb4.toString();
                    }
                    StringBuilder sb5 = new StringBuilder();
                    sb5.append(sb3);
                    sb5.append(trim);
                    str4 = sb5.toString();
                }
                String str5 = str4;
                StringBuilder sb6 = new StringBuilder("rewardUrl:");
                sb6.append(str5);
                g.d("VideoViewReport", sb6.toString());
                com.mintegral.msdk.click.a.a(com.mintegral.msdk.base.controller.a.d().h(), campaignEx, campaignEx.getCampaignUnitId(), str5, false, false);
            } catch (Throwable th) {
                g.c("VideoViewReport", th.getMessage(), th);
            }
        }
    }
}
