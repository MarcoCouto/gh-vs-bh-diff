package com.mintegral.msdk.video.module;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.RelativeLayout.LayoutParams;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.entity.p;
import com.mintegral.msdk.base.utils.k;
import com.mintegral.msdk.base.utils.s;
import com.mintegral.msdk.base.webview.BrowserView.MTGDownloadListener;
import com.mintegral.msdk.mtgjscommon.mraid.d;
import com.mintegral.msdk.mtgjscommon.windvane.WindVaneWebView;
import com.mintegral.msdk.mtgjscommon.windvane.g;
import com.mintegral.msdk.video.js.f;
import com.mintegral.msdk.video.module.a.a;
import com.mintegral.msdk.videocommon.download.h;
import com.mintegral.msdk.videocommon.e.b;
import com.mintegral.msdk.videocommon.e.c;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONObject;

public class MintegralH5EndCardView extends MintegralBaseView implements f {
    /* access modifiers changed from: private */
    public boolean A = false;
    private boolean B = false;
    private boolean C = false;
    /* access modifiers changed from: private */
    public boolean D = false;
    /* access modifiers changed from: private */
    public boolean E = false;
    /* access modifiers changed from: private */
    public boolean F = false;
    private boolean G = false;
    private boolean H = false;
    protected View i;
    protected ImageView j;
    protected WindVaneWebView k;
    protected Handler l = new Handler();
    protected String m;
    Handler n = new Handler(Looper.getMainLooper()) {
        public final void handleMessage(Message message) {
            super.handleMessage(message);
            if (message.what == 100) {
                if (MintegralH5EndCardView.this.z) {
                    MintegralH5EndCardView.this.e.a(122, "");
                }
                MintegralH5EndCardView.this.e.a(103, "");
            }
        }
    };
    boolean o = false;
    private boolean p = false;
    /* access modifiers changed from: private */
    public boolean q = false;
    /* access modifiers changed from: private */
    public boolean r = false;
    private boolean s = false;
    private int t = 1;
    private int u = 1;
    private boolean v = false;
    private int w = 1;
    /* access modifiers changed from: private */
    public String x;
    /* access modifiers changed from: private */
    public long y = 0;
    /* access modifiers changed from: private */
    public boolean z = false;

    public void install(CampaignEx campaignEx) {
    }

    public MintegralH5EndCardView(Context context) {
        super(context);
    }

    public MintegralH5EndCardView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public void onSelfConfigurationChanged(Configuration configuration) {
        super.onSelfConfigurationChanged(configuration);
        orientation(configuration);
    }

    public void init(Context context) {
        int findLayout = findLayout("mintegral_reward_endcard_h5");
        if (findLayout >= 0) {
            this.i = this.c.inflate(findLayout, null);
            View view = this.i;
            this.j = (ImageView) view.findViewById(findID("mintegral_windwv_close"));
            this.k = (WindVaneWebView) view.findViewById(findID("mintegral_windwv_content"));
            this.f = isNotNULL(this.j, this.k);
            addView(this.i, d());
            a();
            e();
        }
    }

    /* access modifiers changed from: protected */
    public final void a() {
        super.a();
        if (this.f) {
            this.j.setOnClickListener(new OnClickListener() {
                public final void onClick(View view) {
                    MintegralH5EndCardView.this.onCloseViewClick();
                }
            });
        }
    }

    public void onCloseViewClick() {
        try {
            if (this.k != null) {
                g.a();
                g.a(this.k, "onSystemDestory", "");
                new Thread(new Runnable() {
                    public final void run() {
                        try {
                            Thread.sleep(300);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        MintegralH5EndCardView.this.n.sendEmptyMessage(100);
                    }
                }).start();
                return;
            }
            this.e.a(103, "");
            this.e.a(119, "webview is null when closing webview");
        } catch (Exception e) {
            this.e.a(103, "");
            a aVar = this.e;
            StringBuilder sb = new StringBuilder("close webview exception");
            sb.append(e.getMessage());
            aVar.a(119, sb.toString());
            com.mintegral.msdk.base.utils.g.a(MintegralBaseView.TAG, e.getMessage());
        }
    }

    public void setError(boolean z2) {
        this.r = z2;
    }

    /* JADX WARNING: Removed duplicated region for block: B:42:0x00fb A[Catch:{ Throwable -> 0x011d }] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x0112 A[Catch:{ Throwable -> 0x011d }] */
    public void preLoadData() {
        int i2;
        String f = f();
        if (!this.f || this.b == null || TextUtils.isEmpty(f)) {
            this.e.a(101, "");
        } else {
            MTGDownloadListener mTGDownloadListener = new MTGDownloadListener(this.b);
            mTGDownloadListener.setTitle(this.b.getAppName());
            this.k.setDownloadListener(mTGDownloadListener);
            this.k.setCampaignId(this.b.getId());
            setCloseVisible(8);
            this.k.setWebViewListener(new com.mintegral.msdk.mtgjscommon.b.a() {
                public final void a(WebView webView, String str) {
                    super.a(webView, str);
                    StringBuilder sb = new StringBuilder("===========finish+");
                    sb.append(str);
                    com.mintegral.msdk.base.utils.g.d("========", sb.toString());
                    if (!MintegralH5EndCardView.this.r) {
                        MintegralH5EndCardView.this.q = true;
                        String str2 = MintegralBaseView.TAG;
                        StringBuilder sb2 = new StringBuilder("onPageFinished,url:");
                        sb2.append(str);
                        com.mintegral.msdk.base.utils.g.a(str2, sb2.toString());
                        MintegralH5EndCardView.this.e.a(100, "");
                        if (MintegralH5EndCardView.this.b != null) {
                            p pVar = new p();
                            pVar.k(MintegralH5EndCardView.this.b.getRequestIdNotice());
                            pVar.m(MintegralH5EndCardView.this.b.getId());
                            pVar.c(1);
                            pVar.p(String.valueOf(System.currentTimeMillis() - MintegralH5EndCardView.this.y));
                            pVar.f(MintegralH5EndCardView.this.b.getendcard_url());
                            String str3 = "2";
                            if (s.b(MintegralH5EndCardView.this.b.getendcard_url()) && MintegralH5EndCardView.this.b.getendcard_url().contains(".zip")) {
                                str3 = "1";
                            }
                            pVar.g(str3);
                            pVar.o("");
                            if (MintegralH5EndCardView.this.b.getAdType() == 287) {
                                pVar.h("3");
                            } else if (MintegralH5EndCardView.this.b.getAdType() == 94) {
                                pVar.h("1");
                            } else if (MintegralH5EndCardView.this.b.getAdType() == 42) {
                                pVar.h("2");
                            }
                            pVar.a(MintegralH5EndCardView.this.b.isMraid() ? p.f2605a : p.b);
                            com.mintegral.msdk.base.common.d.a.c(pVar, MintegralH5EndCardView.this.f2997a, MintegralH5EndCardView.this.x);
                        }
                        MintegralH5EndCardView.this.e.a(120, "");
                    }
                }

                public final void a(WebView webView, int i, String str, String str2) {
                    super.a(webView, i, str, str2);
                    com.mintegral.msdk.base.utils.g.d("========", "===========onReceivedError");
                    if (!MintegralH5EndCardView.this.r) {
                        String str3 = MintegralBaseView.TAG;
                        StringBuilder sb = new StringBuilder("onReceivedError,url:");
                        sb.append(str2);
                        com.mintegral.msdk.base.utils.g.a(str3, sb.toString());
                        a aVar = MintegralH5EndCardView.this.e;
                        StringBuilder sb2 = new StringBuilder("onReceivedError ");
                        sb2.append(i);
                        sb2.append(str);
                        aVar.a(118, sb2.toString());
                        MintegralH5EndCardView.this.reportRenderFailed(str);
                        MintegralH5EndCardView.this.r = true;
                    }
                }
            });
            if (TextUtils.isEmpty(this.b.getMraid())) {
                try {
                    this.y = System.currentTimeMillis();
                    String str = this.b.getendcard_url();
                    b.a();
                    c a2 = b.a(com.mintegral.msdk.base.controller.a.d().j(), this.x);
                    if (this.s && s.b(str) && (str.contains("wfr=1") || (a2 != null && a2.m() > 0))) {
                        com.mintegral.msdk.base.utils.g.d(MintegralBaseView.TAG, "需要上报endcard加载时间");
                        if (str.contains("wfr=1")) {
                            String[] split = str.split(RequestParameters.AMPERSAND);
                            if (split != null && split.length > 0) {
                                int length = split.length;
                                int i3 = 0;
                                while (true) {
                                    if (i3 >= length) {
                                        break;
                                    }
                                    String str2 = split[i3];
                                    if (s.b(str2) && str2.contains("to") && str2.split(RequestParameters.EQUAL) != null && str2.split(RequestParameters.EQUAL).length > 0) {
                                        i2 = k.a((Object) str2.split(RequestParameters.EQUAL)[1]);
                                        String str3 = MintegralBaseView.TAG;
                                        StringBuilder sb = new StringBuilder("从url获取的waitingtime:");
                                        sb.append(i2);
                                        com.mintegral.msdk.base.utils.g.b(str3, sb.toString());
                                        break;
                                    }
                                    i3++;
                                }
                                if (i2 >= 0) {
                                    excuteEndCardShowTask(i2);
                                    String str4 = MintegralBaseView.TAG;
                                    StringBuilder sb2 = new StringBuilder("开启excuteEndCardShowTask:");
                                    sb2.append(i2);
                                    com.mintegral.msdk.base.utils.g.b(str4, sb2.toString());
                                } else {
                                    excuteEndCardShowTask(20);
                                    com.mintegral.msdk.base.utils.g.b(MintegralBaseView.TAG, "开启excuteEndCardShowTask: 20s def");
                                }
                            }
                        } else if (a2 != null && a2.m() > 0) {
                            i2 = a2.m();
                            if (i2 >= 0) {
                            }
                        }
                        i2 = 20;
                        if (i2 >= 0) {
                        }
                    }
                } catch (Throwable th) {
                    com.mintegral.msdk.base.utils.g.c(MintegralBaseView.TAG, th.getMessage(), th);
                }
            }
            setHtmlSource(h.a().a(f));
            if (TextUtils.isEmpty(this.m)) {
                String str5 = MintegralBaseView.TAG;
                StringBuilder sb3 = new StringBuilder("load url:");
                sb3.append(f);
                com.mintegral.msdk.base.utils.g.a(str5, sb3.toString());
                this.k.loadUrl(f);
            } else {
                com.mintegral.msdk.base.utils.g.a(MintegralBaseView.TAG, "load html...");
                this.k.loadDataWithBaseURL(f, this.m, WebRequest.CONTENT_TYPE_HTML, "UTF-8", null);
            }
        }
        this.o = false;
    }

    public void reportRenderFailed(String str) {
        if (this.b != null && !this.r) {
            p pVar = new p();
            pVar.k(this.b.getRequestIdNotice());
            pVar.m(this.b.getId());
            pVar.c(3);
            pVar.p(String.valueOf(System.currentTimeMillis() - this.y));
            pVar.f(this.b.getendcard_url());
            String str2 = "2";
            if (s.b(this.b.getendcard_url()) && this.b.getendcard_url().contains(".zip")) {
                str2 = "1";
            }
            pVar.g(str2);
            pVar.o(str);
            if (this.b.getAdType() == 287) {
                pVar.h("3");
            } else if (this.b.getAdType() == 94) {
                pVar.h("1");
            } else if (this.b.getAdType() == 42) {
                pVar.h("2");
            }
            pVar.a(this.b.isMraid() ? p.f2605a : p.b);
            com.mintegral.msdk.base.common.d.a.c(pVar, this.f2997a, this.x);
        }
    }

    public void defaultShow() {
        super.defaultShow();
    }

    public void notifyCloseBtn(int i2) {
        switch (i2) {
            case 0:
                this.B = true;
                return;
            case 1:
                this.C = true;
                break;
        }
    }

    public void toggleCloseBtn(int i2) {
        int visibility = this.j.getVisibility();
        switch (i2) {
            case 1:
                this.A = true;
                visibility = 0;
                break;
            case 2:
                this.A = false;
                visibility = 8;
                if (!this.o) {
                    if (!this.G && !this.B) {
                        this.G = true;
                        if (this.t != 0) {
                            this.D = false;
                            if (this.t >= 0) {
                                this.l.postDelayed(new Runnable() {
                                    public final void run() {
                                        MintegralH5EndCardView.this.D = true;
                                    }
                                }, (long) (this.t * 1000));
                                break;
                            }
                        } else {
                            this.D = true;
                            break;
                        }
                    }
                } else if (!this.H && !this.B) {
                    this.H = true;
                    if (this.u != 0) {
                        this.E = false;
                        if (this.u >= 0) {
                            this.l.postDelayed(new Runnable() {
                                public final void run() {
                                    MintegralH5EndCardView.this.E = true;
                                }
                            }, (long) (this.u * 1000));
                            break;
                        }
                    } else {
                        this.E = true;
                        break;
                    }
                }
                break;
        }
        setCloseVisible(visibility);
    }

    public void handlerPlayableException(String str) {
        com.mintegral.msdk.base.utils.g.d("========", "===========handlerPlayableException");
        if (!this.r) {
            this.r = true;
            this.q = false;
            if (this.b != null) {
                p pVar = new p();
                pVar.k(this.b.getRequestIdNotice());
                pVar.m(this.b.getId());
                pVar.o(str);
                com.mintegral.msdk.base.common.d.a.f(pVar, this.f2997a, this.x);
            }
        }
    }

    public void readyStatus(int i2) {
        String str = MintegralBaseView.TAG;
        StringBuilder sb = new StringBuilder("h5EncardView readyStatus:");
        sb.append(i2);
        sb.append("- isError");
        sb.append(this.r);
        com.mintegral.msdk.base.utils.g.b(str, sb.toString());
        this.w = i2;
        if (!this.r) {
            a(System.currentTimeMillis() - this.y, false);
        }
    }

    public void webviewshow() {
        if (this.k != null) {
            this.k.post(new Runnable() {
                public final void run() {
                    try {
                        com.mintegral.msdk.base.utils.g.a(MintegralBaseView.TAG, "webviewshow");
                        String str = "";
                        try {
                            int[] iArr = new int[2];
                            MintegralH5EndCardView.this.k.getLocationOnScreen(iArr);
                            String str2 = MintegralBaseView.TAG;
                            StringBuilder sb = new StringBuilder("coordinate:");
                            sb.append(iArr[0]);
                            sb.append("--");
                            sb.append(iArr[1]);
                            com.mintegral.msdk.base.utils.g.d(str2, sb.toString());
                            JSONObject jSONObject = new JSONObject();
                            Context h = com.mintegral.msdk.base.controller.a.d().h();
                            if (h != null) {
                                jSONObject.put("startX", k.a(h, (float) iArr[0]));
                                jSONObject.put("startY", k.a(h, (float) iArr[1]));
                                jSONObject.put(com.mintegral.msdk.base.common.a.C, (double) k.c(h));
                            }
                            str = jSONObject.toString();
                        } catch (Throwable th) {
                            com.mintegral.msdk.base.utils.g.c(MintegralBaseView.TAG, th.getMessage(), th);
                        }
                        String encodeToString = Base64.encodeToString(str.toString().getBytes(), 2);
                        g.a();
                        g.a(MintegralH5EndCardView.this.k, "webviewshow", encodeToString);
                        MintegralH5EndCardView.this.e.a(109, "");
                        MintegralH5EndCardView.i(MintegralH5EndCardView.this);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    public void orientation(Configuration configuration) {
        try {
            JSONObject jSONObject = new JSONObject();
            if (configuration.orientation == 2) {
                jSONObject.put("orientation", "landscape");
            } else {
                jSONObject.put("orientation", "portrait");
            }
            String encodeToString = Base64.encodeToString(jSONObject.toString().getBytes(), 2);
            g.a();
            g.a(this.k, "orientation", encodeToString);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean canBackPress() {
        return this.j != null && this.j.getVisibility() == 0;
    }

    public boolean isLoadSuccess() {
        return this.q;
    }

    public void setUnitId(String str) {
        this.x = str;
    }

    public void setCloseDelayShowTime(int i2) {
        this.t = i2;
    }

    public void setPlayCloseBtnTm(int i2) {
        this.u = i2;
    }

    public void setHtmlSource(String str) {
        this.m = str;
    }

    public void setCloseVisible(int i2) {
        if (this.f) {
            this.j.setVisibility(i2);
        }
    }

    public void setCloseVisibleForMraid(int i2) {
        if (this.f) {
            this.F = true;
            if (i2 == 4) {
                this.j.setImageDrawable(new ColorDrawable(16711680));
            } else {
                this.j.setImageResource(findDrawable("mintegral_reward_close"));
            }
            this.j.setVisibility(0);
        }
    }

    /* access modifiers changed from: protected */
    public LayoutParams d() {
        return new LayoutParams(-1, -1);
    }

    /* access modifiers changed from: protected */
    public void e() {
        if (this.f) {
            setMatchParent();
        }
    }

    /* access modifiers changed from: protected */
    public String f() {
        String str;
        if (this.b != null) {
            this.z = true;
            if (this.b.isMraid()) {
                this.s = false;
                String mraid = this.b.getMraid();
                if (!TextUtils.isEmpty(mraid)) {
                    File file = new File(mraid);
                    try {
                        if (!file.exists() || !file.isFile() || !file.canRead()) {
                            com.mintegral.msdk.base.utils.g.b(MintegralBaseView.TAG, "Mraid file not found. Will use endcard url.");
                            str = this.b.getEndScreenUrl();
                        } else {
                            String str2 = MintegralBaseView.TAG;
                            StringBuilder sb = new StringBuilder("Mraid file ");
                            sb.append(mraid);
                            com.mintegral.msdk.base.utils.g.b(str2, sb.toString());
                            StringBuilder sb2 = new StringBuilder("file:////");
                            sb2.append(mraid);
                            str = sb2.toString();
                        }
                        mraid = str;
                    } catch (Throwable th) {
                        if (MIntegralConstans.DEBUG) {
                            th.printStackTrace();
                        }
                    }
                } else {
                    mraid = this.b.getEndScreenUrl();
                    String str3 = MintegralBaseView.TAG;
                    StringBuilder sb3 = new StringBuilder("getURL playable=false endscreenurl兜底:");
                    sb3.append(mraid);
                    com.mintegral.msdk.base.utils.g.d(str3, sb3.toString());
                }
                return mraid;
            }
            String str4 = this.b.getendcard_url();
            if (!s.a(str4)) {
                this.s = true;
                String a2 = com.mintegral.msdk.videocommon.download.g.a().a(str4);
                if (TextUtils.isEmpty(a2)) {
                    String str5 = MintegralBaseView.TAG;
                    StringBuilder sb4 = new StringBuilder("getURL playable=true endcard本地资源地址为空拿服务端地址:");
                    sb4.append(str4);
                    com.mintegral.msdk.base.utils.g.b(str5, sb4.toString());
                    return str4;
                }
                String str6 = MintegralBaseView.TAG;
                StringBuilder sb5 = new StringBuilder("getURL playable=true 资源不为空endcard地址:");
                sb5.append(a2);
                com.mintegral.msdk.base.utils.g.b(str6, sb5.toString());
                return a2;
            }
            this.s = false;
            String endScreenUrl = this.b.getEndScreenUrl();
            String str7 = MintegralBaseView.TAG;
            StringBuilder sb6 = new StringBuilder("getURL playable=false endscreenurl兜底:");
            sb6.append(endScreenUrl);
            com.mintegral.msdk.base.utils.g.d(str7, sb6.toString());
            return endScreenUrl;
        }
        this.z = false;
        com.mintegral.msdk.base.utils.g.d(MintegralBaseView.TAG, "getURL playable=false url为空");
        return null;
    }

    public void excuteTask() {
        if (!this.s && this.t >= 0) {
            this.l.postDelayed(new Runnable() {
                public final void run() {
                    if (!MintegralH5EndCardView.this.F) {
                        MintegralH5EndCardView.this.setCloseVisible(0);
                    }
                    MintegralH5EndCardView.this.A = true;
                }
            }, (long) (this.t * 1000));
        }
    }

    public void excuteEndCardShowTask(final int i2) {
        this.l.postDelayed(new Runnable() {
            public final void run() {
                com.mintegral.msdk.base.utils.g.b(MintegralBaseView.TAG, "endcard 加载等待结束 开始插入数据库");
                MintegralH5EndCardView.this.a((long) (i2 * 1000), true);
            }
        }, (long) (i2 * 1000));
    }

    /* access modifiers changed from: private */
    public void a(long j2, boolean z2) {
        int i2;
        String str;
        int i3;
        try {
            if (this.v) {
                com.mintegral.msdk.base.utils.g.b(MintegralBaseView.TAG, "insertEndCardReadyState hasInsertLoadEndCardReport true return");
                return;
            }
            this.v = true;
            String str2 = "2";
            if (s.b(this.b.getendcard_url()) && this.b.getendcard_url().contains(".zip")) {
                str2 = "1";
            }
            String str3 = str2;
            String str4 = "ready yes";
            if (z2) {
                str = "ready timeout";
                i3 = 12;
                i2 = 2;
            } else if (this.w == 2) {
                str = "ready no";
                i3 = 11;
                i2 = 3;
            } else {
                str = str4;
                i3 = 10;
                i2 = 1;
            }
            p pVar = r3;
            p pVar2 = new p("2000043", i3, String.valueOf(j2), this.b.getendcard_url(), this.b.getId(), this.x, str, str3);
            if (this.b.getAdType() == 287) {
                pVar.h("3");
            } else if (this.b.getAdType() == 94) {
                pVar.h("1");
            } else if (this.b.getAdType() == 42) {
                pVar.h("2");
            }
            pVar.k(this.b.getRequestIdNotice());
            com.mintegral.msdk.base.common.d.a.a(pVar, com.mintegral.msdk.base.controller.a.d().h(), this.x);
            if (!isLoadSuccess() && i2 == 1) {
                pVar.c(i2);
                pVar.p(String.valueOf(j2));
                pVar.g(str3);
                pVar.f(this.b.getendcard_url());
                String str5 = "2";
                if (s.b(this.b.getendcard_url()) && this.b.getendcard_url().contains(".zip")) {
                    str5 = "1";
                }
                pVar.g(str5);
                pVar.m(this.b.getId());
                pVar.o(str);
                pVar.a(this.b.isMraid() ? p.f2605a : p.b);
                com.mintegral.msdk.base.common.d.a.c(pVar, this.f2997a, this.x);
            }
            String str6 = MintegralBaseView.TAG;
            StringBuilder sb = new StringBuilder("insertEndCardReadyState result:");
            sb.append(i3);
            sb.append(" endCardLoadTime:");
            sb.append(j2);
            sb.append(" endcardurl:");
            sb.append(this.b.getendcard_url());
            sb.append("  id:");
            sb.append(this.b.getId());
            sb.append("  unitid:");
            sb.append(this.x);
            sb.append("  reason:");
            sb.append(str);
            sb.append("  type:");
            sb.append(str3);
            com.mintegral.msdk.base.utils.g.b(str6, sb.toString());
        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Throwable th) {
            com.mintegral.msdk.base.utils.g.c(MintegralBaseView.TAG, th.getMessage(), th);
        }
    }

    public boolean isPlayable() {
        return this.s;
    }

    public void onBackPress() {
        if (this.A || ((this.B && this.C) || ((!this.B && this.D && !this.o) || (!this.B && this.E && this.o)))) {
            onCloseViewClick();
        }
    }

    public void setLoadPlayable(boolean z2) {
        this.o = z2;
    }

    public void release() {
        if (this.l != null) {
            this.l.removeCallbacksAndMessages(null);
        }
        if (this.n != null) {
            this.n.removeCallbacksAndMessages(null);
        }
    }

    public void volumeChange(double d) {
        com.mintegral.msdk.mtgjscommon.mraid.a.a();
        com.mintegral.msdk.mtgjscommon.mraid.a.a((WebView) this.k, d);
    }

    public void onWindowFocusChanged(boolean z2) {
        super.onWindowFocusChanged(z2);
        if (z2) {
            com.mintegral.msdk.mtgjscommon.mraid.a.a();
            com.mintegral.msdk.mtgjscommon.mraid.a.b(this.k, "true");
            return;
        }
        com.mintegral.msdk.mtgjscommon.mraid.a.a();
        com.mintegral.msdk.mtgjscommon.mraid.a.b(this.k, "false");
    }

    static /* synthetic */ void i(MintegralH5EndCardView mintegralH5EndCardView) {
        if (mintegralH5EndCardView.b != null && mintegralH5EndCardView.b.isMraid()) {
            String str = "undefined";
            switch (mintegralH5EndCardView.getResources().getConfiguration().orientation) {
                case 0:
                    str = "undefined";
                    break;
                case 1:
                    str = "portrait";
                    break;
                case 2:
                    str = "landscape";
                    break;
            }
            JSONObject jSONObject = new JSONObject();
            try {
                jSONObject.put("orientation", str);
                jSONObject.put("locked", "true");
            } catch (Exception e) {
                e.printStackTrace();
            }
            HashMap hashMap = new HashMap();
            hashMap.put("placementType", "Interstitial");
            hashMap.put("state", "default");
            hashMap.put("viewable", "true");
            hashMap.put("currentAppOrientation", jSONObject);
            if (mintegralH5EndCardView.getContext() instanceof Activity) {
                float l2 = (float) com.mintegral.msdk.base.utils.c.l(mintegralH5EndCardView.getContext());
                float m2 = (float) com.mintegral.msdk.base.utils.c.m(mintegralH5EndCardView.getContext());
                DisplayMetrics displayMetrics = new DisplayMetrics();
                ((Activity) mintegralH5EndCardView.getContext()).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                float f = (float) displayMetrics.widthPixels;
                float f2 = (float) displayMetrics.heightPixels;
                com.mintegral.msdk.mtgjscommon.mraid.a.a();
                com.mintegral.msdk.mtgjscommon.mraid.a.a((WebView) mintegralH5EndCardView.k, l2, m2);
                com.mintegral.msdk.mtgjscommon.mraid.a.a();
                com.mintegral.msdk.mtgjscommon.mraid.a.b(mintegralH5EndCardView.k, f, f2);
            }
            com.mintegral.msdk.mtgjscommon.mraid.a.a();
            com.mintegral.msdk.mtgjscommon.mraid.a.a(mintegralH5EndCardView.k, (float) mintegralH5EndCardView.k.getLeft(), (float) mintegralH5EndCardView.k.getTop(), (float) mintegralH5EndCardView.k.getWidth(), (float) mintegralH5EndCardView.k.getHeight());
            com.mintegral.msdk.mtgjscommon.mraid.a.a();
            com.mintegral.msdk.mtgjscommon.mraid.a.b(mintegralH5EndCardView.k, (float) mintegralH5EndCardView.k.getLeft(), (float) mintegralH5EndCardView.k.getTop(), (float) mintegralH5EndCardView.k.getWidth(), (float) mintegralH5EndCardView.k.getHeight());
            com.mintegral.msdk.mtgjscommon.mraid.a.a();
            com.mintegral.msdk.mtgjscommon.mraid.a.a((WebView) mintegralH5EndCardView.k, (Map<String, Object>) hashMap);
            com.mintegral.msdk.mtgjscommon.mraid.a.a();
            com.mintegral.msdk.mtgjscommon.mraid.a.a((WebView) mintegralH5EndCardView.k, d.f2741a);
            com.mintegral.msdk.mtgjscommon.mraid.a.a();
            com.mintegral.msdk.mtgjscommon.mraid.a.a(mintegralH5EndCardView.k);
        }
    }
}
