package com.mintegral.msdk.video.module.a.a;

import android.net.Uri;
import android.text.TextUtils;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.videocommon.download.a;

/* compiled from: ContainerViewStatisticsListener */
public class d extends k {
    public d(CampaignEx campaignEx, a aVar, com.mintegral.msdk.videocommon.b.d dVar, String str, com.mintegral.msdk.video.module.a.a aVar2) {
        super(campaignEx, aVar, dVar, str, aVar2);
    }

    public void a(int i, Object obj) {
        super.a(i, obj);
        if (this.f3029a) {
            switch (i) {
                case 100:
                    return;
                case 101:
                    return;
                case 103:
                    return;
                case 105:
                case 106:
                case 113:
                    com.mintegral.msdk.video.module.b.a.d(com.mintegral.msdk.base.controller.a.d().h(), this.b);
                    if (i != 105) {
                        String noticeUrl = this.b.getNoticeUrl();
                        if (!TextUtils.isEmpty(noticeUrl)) {
                            if (!noticeUrl.contains(com.mintegral.msdk.base.common.a.A)) {
                                StringBuilder sb = new StringBuilder();
                                sb.append(noticeUrl);
                                sb.append(RequestParameters.AMPERSAND);
                                sb.append(com.mintegral.msdk.base.common.a.A);
                                sb.append("=2");
                                noticeUrl = sb.toString();
                            } else {
                                String queryParameter = Uri.parse(noticeUrl).getQueryParameter(com.mintegral.msdk.base.common.a.A);
                                StringBuilder sb2 = new StringBuilder();
                                sb2.append(com.mintegral.msdk.base.common.a.A);
                                sb2.append(RequestParameters.EQUAL);
                                sb2.append(queryParameter);
                                String sb3 = sb2.toString();
                                StringBuilder sb4 = new StringBuilder();
                                sb4.append(com.mintegral.msdk.base.common.a.A);
                                sb4.append("=2");
                                noticeUrl = noticeUrl.replace(sb3, sb4.toString());
                            }
                        }
                        com.mintegral.msdk.click.a.a(com.mintegral.msdk.base.controller.a.d().h(), this.b, this.e, noticeUrl, true, false);
                        return;
                    }
                    break;
                case 107:
                    return;
                case 109:
                    b(2);
                    a(2);
                    return;
                case 110:
                    b(1);
                    a(1);
                    return;
                case 111:
                    a(1);
                    return;
                case 122:
                    a();
                    break;
            }
        }
    }
}
