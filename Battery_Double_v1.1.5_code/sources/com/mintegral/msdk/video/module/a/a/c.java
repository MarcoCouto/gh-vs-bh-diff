package com.mintegral.msdk.video.module.a.a;

import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.video.js.factory.IJSFactory;
import com.mintegral.msdk.video.js.h;
import com.mintegral.msdk.videocommon.b.d;
import com.mintegral.msdk.videocommon.download.a;

/* compiled from: ContainerViewJSListener */
public final class c extends d {
    private IJSFactory g;

    public c(IJSFactory iJSFactory, CampaignEx campaignEx, d dVar, a aVar, String str, com.mintegral.msdk.video.module.a.a aVar2) {
        super(campaignEx, aVar, dVar, str, aVar2);
        this.g = iJSFactory;
        if (iJSFactory == null) {
            this.f3029a = false;
        }
    }

    public final void a(int i, Object obj) {
        if (this.f3029a && i != 103) {
            if (i == 105) {
                String obj2 = obj.toString();
                StringBuilder sb = new StringBuilder("pt:");
                sb.append(obj2);
                g.d("=======", sb.toString());
                this.g.getJSNotifyProxy().a(3, obj2);
                i = -1;
            } else if (i == 107) {
                this.g.getJSContainerModule().showVideoClickView(-1);
                this.g.getJSVideoModule().setCover(false);
                this.g.getJSVideoModule().videoOperate(1);
            } else if (i == 112) {
                this.g.getJSVideoModule().setCover(true);
                this.g.getJSVideoModule().videoOperate(2);
            } else if (i != 115) {
                switch (i) {
                }
            } else {
                h jSVideoModule = this.g.getJSVideoModule();
                this.g.getJSContainerModule().resizeMiniCard(jSVideoModule.getBorderViewWidth(), jSVideoModule.getBorderViewHeight(), jSVideoModule.getBorderViewRadius());
            }
        }
        super.a(i, obj);
    }
}
