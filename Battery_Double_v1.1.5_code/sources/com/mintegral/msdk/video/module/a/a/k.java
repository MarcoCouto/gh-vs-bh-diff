package com.mintegral.msdk.video.module.a.a;

import android.text.TextUtils;
import com.mintegral.msdk.base.b.h;
import com.mintegral.msdk.base.b.i;
import com.mintegral.msdk.base.b.l;
import com.mintegral.msdk.base.b.m;
import com.mintegral.msdk.base.b.v;
import com.mintegral.msdk.base.b.w;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.entity.e;
import com.mintegral.msdk.base.entity.p;
import com.mintegral.msdk.base.utils.c;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.base.utils.s;
import com.mintegral.msdk.videocommon.b.d;
import com.mintegral.msdk.videocommon.download.a;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

/* compiled from: StatisticsOnNotifyListener */
public class k extends f {

    /* renamed from: a reason: collision with root package name */
    protected boolean f3029a;
    protected CampaignEx b;
    protected a c;
    protected d d;
    protected String e;
    protected com.mintegral.msdk.video.module.a.a f = new f();
    private boolean g = false;
    private boolean h = false;

    public k(CampaignEx campaignEx, a aVar, d dVar, String str, com.mintegral.msdk.video.module.a.a aVar2) {
        if (campaignEx != null && s.b(str) && aVar != null && aVar2 != null) {
            this.b = campaignEx;
            this.e = str;
            this.c = aVar;
            this.d = dVar;
            this.f = aVar2;
            this.f3029a = true;
        }
    }

    public void a(int i, Object obj) {
        super.a(i, obj);
        this.f.a(i, obj);
    }

    public final void a(int i) {
        if (this.b != null) {
            switch (i) {
                case 1:
                case 2:
                    com.mintegral.msdk.video.module.b.a.a(com.mintegral.msdk.base.controller.a.d().h(), this.b, i);
                    return;
                default:
                    return;
            }
        }
    }

    public final void a() {
        if (this.f3029a && this.b != null) {
            p pVar = new p("2000061", this.b.getId(), this.b.getRequestId(), this.e, c.p(com.mintegral.msdk.base.controller.a.d().h()));
            pVar.a(this.b.isMraid() ? p.f2605a : p.b);
            com.mintegral.msdk.base.common.d.a.d(pVar, com.mintegral.msdk.base.controller.a.d().h(), this.e);
        }
    }

    public final void a(int i, String str) {
        if (this.b != null) {
            p pVar = new p("2000062", this.b.getId(), this.b.getRequestId(), this.e, c.p(com.mintegral.msdk.base.controller.a.d().h()), i, str);
            com.mintegral.msdk.base.common.d.a.e(pVar, com.mintegral.msdk.base.controller.a.d().h(), this.e);
        }
    }

    public final void b(int i) {
        if (this.b != null) {
            String noticeUrl = this.b.getNoticeUrl();
            if (TextUtils.isEmpty(noticeUrl)) {
                return;
            }
            if (i == 1 || i == 2) {
                if (!noticeUrl.contains("endscreen_type")) {
                    StringBuilder sb = new StringBuilder(noticeUrl);
                    if (noticeUrl.contains("?")) {
                        sb.append("&endscreen_type=");
                        sb.append(i);
                    } else {
                        sb.append("?endscreen_type=");
                        sb.append(i);
                    }
                    noticeUrl = sb.toString();
                } else if (i == 2) {
                    if (noticeUrl.contains("endscreen_type=1")) {
                        noticeUrl = noticeUrl.replace("endscreen_type=1", "endscreen_type=2");
                    }
                } else if (noticeUrl.contains("endscreen_type=2")) {
                    noticeUrl = noticeUrl.replace("endscreen_type=2", "endscreen_type=1");
                }
                this.b.setNoticeUrl(noticeUrl);
            }
        }
    }

    public final void b() {
        try {
            if (this.f3029a && this.b != null && s.b(this.e) && com.mintegral.msdk.base.controller.a.d().h() != null) {
                l a2 = l.a((h) i.a(com.mintegral.msdk.base.controller.a.d().h()));
                e eVar = new e();
                eVar.a(System.currentTimeMillis());
                eVar.b(this.e);
                eVar.a(this.b.getId());
                a2.a(eVar);
            }
        } catch (Throwable th) {
            g.c("NotifyListener", th.getMessage(), th);
        }
    }

    public final void c() {
        try {
            if (this.f3029a && this.b != null && s.b(this.e)) {
                com.mintegral.msdk.videocommon.a.a.a().a(this.b, this.e);
            }
        } catch (Throwable th) {
            g.c("NotifyListener", th.getMessage(), th);
        }
    }

    /* access modifiers changed from: protected */
    public final void d() {
        if (this.c != null) {
            this.c.b(true);
        }
    }

    /* access modifiers changed from: protected */
    public final void e() {
        try {
            if (this.f3029a && !TextUtils.isEmpty(this.b.getOnlyImpressionURL()) && com.mintegral.msdk.base.common.a.c.f2510a != null && !com.mintegral.msdk.base.common.a.c.f2510a.containsKey(this.b.getOnlyImpressionURL()) && !this.h) {
                com.mintegral.msdk.base.common.a.c.f2510a.put(this.b.getOnlyImpressionURL(), Long.valueOf(System.currentTimeMillis()));
                com.mintegral.msdk.click.a.a(com.mintegral.msdk.base.controller.a.d().h(), this.b, this.e, this.b.getOnlyImpressionURL(), false, true);
                c();
                this.h = true;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public final void f() {
        try {
            if (this.f3029a && !this.g && !TextUtils.isEmpty(this.b.getImpressionURL())) {
                this.g = true;
                String impressionURL = this.b.getImpressionURL();
                if (this.b.getSpareOfferFlag() == 1) {
                    StringBuilder sb = new StringBuilder();
                    sb.append(impressionURL);
                    sb.append("&to=1");
                    impressionURL = sb.toString();
                }
                com.mintegral.msdk.click.a.a(com.mintegral.msdk.base.controller.a.d().h(), this.b, this.e, impressionURL, false, true);
                com.mintegral.msdk.video.module.b.a.a(com.mintegral.msdk.base.controller.a.d().h(), this.b);
                new Thread(new Runnable() {
                    public final void run() {
                        try {
                            m.a((h) i.a(com.mintegral.msdk.base.controller.a.d().h())).b(k.this.b.getId());
                        } catch (Throwable th) {
                            g.c("NotifyListener", th.getMessage(), th);
                        }
                    }
                }).start();
                if (this.f3029a && com.mintegral.msdk.base.common.a.c.b != null && !TextUtils.isEmpty(this.b.getId())) {
                    if (!com.mintegral.msdk.base.common.a.c.b.containsKey(this.e) || TextUtils.isEmpty(this.b.getId())) {
                        ArrayList arrayList = new ArrayList();
                        arrayList.add(this.b.getId());
                        com.mintegral.msdk.base.common.a.c.b.put(this.e, arrayList);
                    } else {
                        List list = (List) com.mintegral.msdk.base.common.a.c.b.get(this.e);
                        if (list.size() == 20) {
                            list.remove(0);
                        }
                        list.add(this.b.getId());
                    }
                }
            }
        } catch (Throwable th) {
            g.c("NotifyListener", th.getMessage(), th);
        }
    }

    /* access modifiers changed from: protected */
    public final void g() {
        try {
            if (this.f3029a && this.c != null) {
                if (this.c.k() != null && !TextUtils.isEmpty(this.c.k().getVideoUrlEncode())) {
                    v.a((h) i.a(com.mintegral.msdk.base.controller.a.d().h())).a(this.c.k().getVideoUrlEncode());
                }
                if (!TextUtils.isEmpty(this.c.c())) {
                    File file = new File(this.c.c());
                    if (file.exists() && file.isFile() && file.delete()) {
                        StringBuilder sb = new StringBuilder("DEL File :");
                        sb.append(file.getAbsolutePath());
                        g.a("NotifyListener", sb.toString());
                    }
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public final void a(String str) {
        try {
            if (this.f3029a) {
                w a2 = w.a((h) i.a(com.mintegral.msdk.base.controller.a.d().h()));
                p pVar = null;
                if (!TextUtils.isEmpty(this.b.getNoticeUrl())) {
                    int p = c.p(com.mintegral.msdk.base.controller.a.d().h());
                    pVar = new p("2000021", p, this.b.getNoticeUrl(), str, c.a(com.mintegral.msdk.base.controller.a.d().h(), p));
                } else if (!TextUtils.isEmpty(this.b.getClickURL())) {
                    int p2 = c.p(com.mintegral.msdk.base.controller.a.d().h());
                    pVar = new p("2000021", p2, this.b.getClickURL(), str, c.a(com.mintegral.msdk.base.controller.a.d().h(), p2));
                }
                if (pVar != null) {
                    pVar.m(this.b.getId());
                    pVar.e(this.b.getVideoUrlEncode());
                    pVar.o(str);
                    pVar.k(this.b.getRequestIdNotice());
                    pVar.l(this.e);
                    a2.a(pVar);
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }
}
