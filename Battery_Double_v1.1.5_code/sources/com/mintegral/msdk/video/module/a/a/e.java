package com.mintegral.msdk.video.module.a.a;

import android.graphics.Bitmap;
import android.widget.ImageView;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.base.b.h;
import com.mintegral.msdk.base.b.i;
import com.mintegral.msdk.base.b.w;
import com.mintegral.msdk.base.common.c.c;
import com.mintegral.msdk.base.controller.a;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.entity.p;
import com.mintegral.msdk.base.utils.g;
import java.lang.ref.WeakReference;

/* compiled from: DefaultImageLoaderListener */
public class e implements c {

    /* renamed from: a reason: collision with root package name */
    private CampaignEx f3026a;
    protected WeakReference<ImageView> b;
    private String c;

    public e(ImageView imageView) {
        this.b = new WeakReference<>(imageView);
    }

    public e(ImageView imageView, CampaignEx campaignEx, String str) {
        this.b = new WeakReference<>(imageView);
        this.f3026a = campaignEx;
        this.c = str;
    }

    public void onSuccessLoad(Bitmap bitmap, String str) {
        if (bitmap == null) {
            try {
                g.d("ImageLoaderListener", "bitmap=null");
            } catch (Throwable th) {
                if (MIntegralConstans.DEBUG) {
                    th.printStackTrace();
                }
            }
        } else {
            if (!(this.b == null || this.b.get() == null)) {
                ((ImageView) this.b.get()).setImageBitmap(bitmap);
                ((ImageView) this.b.get()).setVisibility(0);
            }
        }
    }

    public void onFailedLoad(String str, String str2) {
        try {
            w a2 = w.a((h) i.a(a.d().h()));
            if (this.f3026a == null) {
                g.a("ImageLoaderListener", "campaign is null");
                return;
            }
            p pVar = new p();
            pVar.n("2000044");
            pVar.b(com.mintegral.msdk.base.utils.c.p(a.d().h()));
            pVar.m(this.f3026a.getId());
            pVar.d(this.f3026a.getImageUrl());
            pVar.k(this.f3026a.getRequestIdNotice());
            pVar.l(this.c);
            pVar.o(str);
            a2.a(pVar);
            StringBuilder sb = new StringBuilder("desc:");
            sb.append(str);
            g.d("ImageLoaderListener", sb.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
