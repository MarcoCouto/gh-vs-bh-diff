package com.mintegral.msdk.video.module.a.a;

import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.videocommon.b.d;
import com.mintegral.msdk.videocommon.download.a;

/* compiled from: PlayableStatisticsOnNotifyListener */
public final class h extends k {
    public h(CampaignEx campaignEx, a aVar, d dVar, String str, com.mintegral.msdk.video.module.a.a aVar2) {
        super(campaignEx, aVar, dVar, str, aVar2);
    }

    public final void a(int i, Object obj) {
        if (i == 100) {
            f();
            e();
            a(2);
        } else if (i == 109) {
            b(2);
        } else if (i != 122) {
            switch (i) {
                case 118:
                    String str = "";
                    if (obj != null && (obj instanceof String)) {
                        str = (String) obj;
                    }
                    a(3, str);
                    break;
                case 119:
                    String str2 = "";
                    if (obj != null && (obj instanceof String)) {
                        str2 = (String) obj;
                    }
                    a(4, str2);
                    break;
            }
        } else {
            a();
        }
        super.a(i, obj);
    }
}
