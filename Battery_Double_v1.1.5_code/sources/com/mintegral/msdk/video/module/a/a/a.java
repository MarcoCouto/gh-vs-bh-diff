package com.mintegral.msdk.video.module.a.a;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.click.b;

/* compiled from: CommonContainerNotifyListener */
public class a extends f {

    /* renamed from: a reason: collision with root package name */
    private Activity f3025a;
    private CampaignEx b;

    public a(Activity activity, CampaignEx campaignEx) {
        this.f3025a = activity;
        this.b = campaignEx;
    }

    public void a(int i, Object obj) {
        super.a(i, obj);
        switch (i) {
            case 103:
            case 104:
                if (this.f3025a != null) {
                    this.f3025a.finish();
                    return;
                }
                break;
            case 106:
                if (!(this.f3025a == null || this.b == null)) {
                    try {
                        Intent intent = new Intent();
                        intent.setAction("android.intent.action.VIEW");
                        String a2 = b.a(this.b.getClickURL(), "-999", "-999");
                        if (!TextUtils.isEmpty(a2)) {
                            intent.setData(Uri.parse(a2));
                            this.f3025a.startActivity(intent);
                        }
                    } catch (Throwable th) {
                        g.c("NotifyListener", th.getMessage(), th);
                    }
                    this.f3025a.finish();
                    break;
                }
        }
    }
}
