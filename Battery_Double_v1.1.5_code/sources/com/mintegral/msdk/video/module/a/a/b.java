package com.mintegral.msdk.video.module.a.a;

import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.video.module.MintegralContainerView;
import com.mintegral.msdk.video.module.MintegralVideoView;
import com.mintegral.msdk.videocommon.b.d;
import com.mintegral.msdk.videocommon.download.a;

/* compiled from: ContainerViewDefaultListener */
public final class b extends d {
    private MintegralVideoView g;
    private MintegralContainerView h;

    public b(MintegralVideoView mintegralVideoView, MintegralContainerView mintegralContainerView, CampaignEx campaignEx, d dVar, a aVar, String str, com.mintegral.msdk.video.module.a.a aVar2) {
        super(campaignEx, aVar, dVar, str, aVar2);
        this.g = mintegralVideoView;
        this.h = mintegralContainerView;
        if (mintegralVideoView == null || mintegralContainerView == null) {
            this.f3029a = false;
        }
    }

    public final void a(int i, Object obj) {
        if (!(!this.f3029a || i == 103 || i == 105)) {
            if (i == 107) {
                this.h.showVideoClickView(-1);
                this.g.setCover(false);
                this.g.videoOperate(1);
            } else if (i == 112) {
                this.g.setCover(true);
                this.g.videoOperate(2);
            } else if (i != 115) {
                switch (i) {
                }
            } else {
                this.h.resizeMiniCard(this.g.getBorderViewWidth(), this.g.getBorderViewHeight(), this.g.getBorderViewRadius());
            }
        }
        super.a(i, obj);
    }
}
