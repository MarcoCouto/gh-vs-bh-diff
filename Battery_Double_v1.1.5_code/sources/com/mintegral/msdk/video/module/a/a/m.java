package com.mintegral.msdk.video.module.a.a;

import android.os.Handler;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.video.module.MintegralContainerView;
import com.mintegral.msdk.video.module.MintegralVideoView;
import com.mintegral.msdk.videocommon.b.d;
import com.mintegral.msdk.videocommon.download.a;
import java.util.Timer;
import java.util.TimerTask;

/* compiled from: VideoViewDefaultListener */
public final class m extends o {
    /* access modifiers changed from: private */
    public MintegralVideoView h;
    /* access modifiers changed from: private */
    public MintegralContainerView i;
    private int j;
    private Timer k;
    /* access modifiers changed from: private */
    public Handler l = new Handler();
    private boolean m = false;
    private int n;

    public m(MintegralVideoView mintegralVideoView, MintegralContainerView mintegralContainerView, CampaignEx campaignEx, d dVar, a aVar, String str, int i2, int i3, com.mintegral.msdk.video.module.a.a aVar2, double d) {
        MintegralVideoView mintegralVideoView2 = mintegralVideoView;
        MintegralContainerView mintegralContainerView2 = mintegralContainerView;
        super(campaignEx, dVar, aVar, str, aVar2, d);
        this.h = mintegralVideoView2;
        this.i = mintegralContainerView2;
        this.n = i2;
        this.j = i3;
        if (mintegralVideoView2 != null) {
            this.m = mintegralVideoView.getVideoSkipTime() == 0;
        }
        if (mintegralVideoView2 == null || mintegralContainerView2 == null) {
            this.f3029a = false;
        }
    }

    public final void a(int i2, Object obj) {
        Integer num;
        if (this.f3029a) {
            switch (i2) {
                case 1:
                    if (!this.i.endCardShowing()) {
                        switch (this.i.getVideoInteractiveType()) {
                            case -2:
                                if (this.i.miniCardLoaded()) {
                                    this.i.showVideoClickView(2);
                                    break;
                                }
                                break;
                            case -1:
                                if (!this.i.isLast()) {
                                    this.i.showVideoClickView(-1);
                                    this.h.soundOperate(0, 2);
                                    h();
                                    break;
                                } else {
                                    this.i.showVideoClickView(1);
                                    this.h.soundOperate(0, 1);
                                    try {
                                        h();
                                        this.k = new Timer();
                                        this.k.schedule(new TimerTask() {
                                            public final void run() {
                                                try {
                                                    m.this.l.post(new Runnable() {
                                                        public final void run() {
                                                            m.this.i.showVideoClickView(-1);
                                                            m.this.h.soundOperate(0, 2);
                                                        }
                                                    });
                                                } catch (Throwable th) {
                                                    if (MIntegralConstans.DEBUG) {
                                                        th.printStackTrace();
                                                    }
                                                }
                                            }
                                        }, 3000);
                                        break;
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        break;
                                    }
                                }
                        }
                    }
                    break;
                case 2:
                case 6:
                    this.h.videoOperate(3);
                    if (this.n == 2 && !this.i.endCardShowing()) {
                        this.i.showEndcard(this.b.getVideo_end_type());
                        break;
                    } else {
                        i2 = 16;
                        break;
                    }
                case 5:
                    if (obj != null && (obj instanceof Integer)) {
                        if (((Integer) obj).intValue() == 1) {
                            num = Integer.valueOf(2);
                        } else {
                            num = Integer.valueOf(1);
                        }
                        this.h.soundOperate(num.intValue(), -1);
                        break;
                    }
                case 10:
                    this.h.soundOperate(0, 2);
                    this.h.progressOperate(0, 2);
                    break;
                case 11:
                case 12:
                    this.h.videoOperate(3);
                    if (this.b.getVideo_end_type() != 3) {
                        this.h.setVisibility(8);
                    } else {
                        this.h.setVisibility(0);
                    }
                    this.i.showEndcard(this.b.getVideo_end_type());
                    break;
                case 13:
                    this.h.closeVideoOperate(0, 2);
                    break;
                case 14:
                    if (!this.m) {
                        this.h.closeVideoOperate(0, 1);
                        break;
                    }
                    break;
                case 15:
                    if (obj != null && (obj instanceof MintegralVideoView.a)) {
                        MintegralVideoView.a aVar = (MintegralVideoView.a) obj;
                        int videoInteractiveType = this.i.getVideoInteractiveType();
                        if (videoInteractiveType >= 0 && aVar.f3023a >= videoInteractiveType) {
                            this.i.showVideoClickView(1);
                            this.h.soundOperate(0, 1);
                        }
                        if (this.j >= 0 && aVar.f3023a >= this.j) {
                            this.h.closeVideoOperate(0, 2);
                            this.m = true;
                            break;
                        }
                    }
            }
        }
        super.a(i2, obj);
    }

    private void h() {
        try {
            if (this.k != null) {
                this.k.cancel();
                this.k = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
