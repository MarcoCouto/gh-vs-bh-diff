package com.mintegral.msdk.video.module.a.a;

import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.video.module.MintegralVideoView;
import com.mintegral.msdk.videocommon.b.d;
import com.mintegral.msdk.videocommon.download.a;
import com.mintegral.msdk.videocommon.download.c;
import java.util.Map;

/* compiled from: VideoViewStatisticsListener */
public class o extends k {
    protected double g = 1.0d;
    private boolean h;
    private boolean i;
    private boolean j;
    private boolean k;
    private boolean l = false;
    private Map<Integer, String> m;

    public o(CampaignEx campaignEx, d dVar, a aVar, String str, com.mintegral.msdk.video.module.a.a aVar2, double d) {
        super(campaignEx, aVar, dVar, str, aVar2);
        if (this.f3029a) {
            this.m = campaignEx.getAdvImpList();
        }
        this.g = d;
    }

    public void a(int i2, Object obj) {
        int i3;
        int i4 = 0;
        switch (i2) {
            case 2:
            case 6:
            case 16:
                if (this.f3029a && !this.j) {
                    this.j = true;
                    d();
                    com.mintegral.msdk.video.module.b.a.e(com.mintegral.msdk.base.controller.a.d().h(), this.b);
                    break;
                }
            case 7:
                if (this.f3029a && obj != null && (obj instanceof Integer)) {
                    int intValue = ((Integer) obj).intValue();
                    if (intValue != 2) {
                        if (intValue == 1 && !this.h) {
                            this.h = true;
                            com.mintegral.msdk.video.module.b.a.c(com.mintegral.msdk.base.controller.a.d().h(), this.b);
                            break;
                        }
                    } else if (!this.i) {
                        this.i = true;
                        com.mintegral.msdk.video.module.b.a.b(com.mintegral.msdk.base.controller.a.d().h(), this.b);
                        break;
                    }
                }
                break;
            case 11:
                c.getInstance().b(false);
                d();
                break;
            case 12:
                if (obj != null && (obj instanceof String)) {
                    a((String) obj);
                }
                d();
                b();
                c();
                g();
                c.getInstance().b(false);
                break;
            case 13:
                b();
                c();
                break;
            case 15:
                try {
                    f();
                    e();
                    if (obj == null || !(obj instanceof MintegralVideoView.a)) {
                        i3 = 0;
                    } else {
                        int i5 = ((MintegralVideoView.a) obj).f3023a;
                        i3 = i5;
                        i4 = ((MintegralVideoView.a) obj).b;
                    }
                    if (i4 == 0 && this.b != null) {
                        i4 = this.b.getVideoLength();
                    }
                    com.mintegral.msdk.video.module.b.a.a(com.mintegral.msdk.base.controller.a.d().h(), this.b, i3, i4);
                    com.mintegral.msdk.video.module.b.a.a(this.b, this.m, this.e, i3);
                    if (!this.k) {
                        this.k = true;
                        com.mintegral.msdk.video.module.b.a.a(this.b, this.e);
                    }
                    if (!this.l) {
                        double d = (double) i3;
                        double d2 = (double) i4;
                        double d3 = this.g;
                        Double.isNaN(d2);
                        if (d >= d2 * d3) {
                            this.l = true;
                            i2 = 17;
                            break;
                        }
                    }
                } catch (Throwable th) {
                    g.c("NotifyListener", th.getMessage(), th);
                    return;
                }
                break;
        }
        this.f.a(i2, obj);
    }
}
