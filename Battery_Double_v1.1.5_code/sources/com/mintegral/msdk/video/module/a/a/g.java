package com.mintegral.msdk.video.module.a.a;

import com.mintegral.msdk.video.module.MintegralClickMiniCardView;
import com.mintegral.msdk.video.module.a.a;

/* compiled from: MiniCardProxyNotifyListener */
public final class g extends i {

    /* renamed from: a reason: collision with root package name */
    private MintegralClickMiniCardView f3027a;

    public g(MintegralClickMiniCardView mintegralClickMiniCardView, a aVar) {
        super(aVar);
        this.f3027a = mintegralClickMiniCardView;
    }

    public final void a(int i, Object obj) {
        boolean z = false;
        switch (i) {
            case 100:
                if (this.f3027a != null) {
                    this.f3027a.webviewshow();
                    this.f3027a.onSelfConfigurationChanged(this.f3027a.getResources().getConfiguration());
                    break;
                }
                break;
            case 101:
            case 102:
                z = true;
                break;
            case 103:
                i = 107;
                break;
        }
        if (!z) {
            super.a(i, obj);
        }
    }
}
