package com.mintegral.msdk.video.module.a.a;

import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.video.js.factory.IJSFactory;
import com.mintegral.msdk.video.js.h;
import com.mintegral.msdk.video.module.MintegralVideoView;
import com.mintegral.msdk.videocommon.b.d;
import com.mintegral.msdk.videocommon.download.a;

/* compiled from: VideoViewJSListener */
public final class n extends o {
    private IJSFactory h;
    private int i;
    private boolean j = false;

    public n(IJSFactory iJSFactory, CampaignEx campaignEx, d dVar, a aVar, String str, int i2, int i3, com.mintegral.msdk.video.module.a.a aVar2, double d) {
        IJSFactory iJSFactory2 = iJSFactory;
        super(campaignEx, dVar, aVar, str, aVar2, d);
        this.h = iJSFactory2;
        this.i = i2;
        this.j = i3 == 0;
        if (iJSFactory2 == null) {
            this.f3029a = false;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:43:0x0122  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x0142  */
    public final void a(int i2, Object obj) {
        Integer num;
        if (this.f3029a) {
            if (i2 != 114) {
                if (i2 != 116) {
                    int i3 = 1;
                    switch (i2) {
                        case 1:
                            if (!this.h.getJSContainerModule().endCardShowing()) {
                                this.h.getJSNotifyProxy().a(1, "");
                                break;
                            }
                            break;
                        case 2:
                            if (i2 == 2) {
                            }
                            this.h.getJSVideoModule().videoOperate(3);
                            if (this.h.getJSCommon().g() != 2) {
                            }
                            i2 = 16;
                            this.h.getJSNotifyProxy().a(1);
                            break;
                        default:
                            switch (i2) {
                                case 5:
                                    if (obj != null && (obj instanceof Integer)) {
                                        if (((Integer) obj).intValue() == 1) {
                                            num = Integer.valueOf(2);
                                        } else {
                                            num = Integer.valueOf(1);
                                        }
                                        this.h.getJSVideoModule().soundOperate(num.intValue(), -1);
                                        this.h.getJSNotifyProxy().a(5, String.valueOf(num));
                                        break;
                                    }
                                case 6:
                                    break;
                                default:
                                    switch (i2) {
                                        case 10:
                                            this.h.getJSNotifyProxy().a(0);
                                            break;
                                        case 11:
                                        case 12:
                                            this.h.getJSVideoModule().videoOperate(3);
                                            if (this.b.getVideo_end_type() != 3) {
                                                this.h.getJSVideoModule().setVisible(8);
                                            } else {
                                                this.h.getJSVideoModule().setVisible(0);
                                            }
                                            if (i2 == 12) {
                                                i3 = 2;
                                            }
                                            this.h.getJSNotifyProxy().a(i3);
                                            if (this.h.getJSCommon().g() != 2) {
                                                this.h.getJSContainerModule().showEndcard(this.b.getVideo_end_type());
                                                break;
                                            } else {
                                                this.h.getJSVideoModule().setVisible(0);
                                                h jSVideoModule = this.h.getJSVideoModule();
                                                this.h.getJSContainerModule().showMiniCard(jSVideoModule.getBorderViewTop(), jSVideoModule.getBorderViewLeft(), jSVideoModule.getBorderViewWidth(), jSVideoModule.getBorderViewHeight(), jSVideoModule.getBorderViewRadius());
                                                break;
                                            }
                                        case 13:
                                            if (!this.h.getJSVideoModule().isH5Canvas()) {
                                                this.h.getJSVideoModule().closeVideoOperate(0, 2);
                                            }
                                            this.h.getJSNotifyProxy().a(-1);
                                            break;
                                        case 14:
                                            if (!this.j) {
                                                this.h.getJSVideoModule().closeVideoOperate(0, 1);
                                                break;
                                            }
                                            break;
                                        case 15:
                                            if (obj != null && (obj instanceof MintegralVideoView.a)) {
                                                MintegralVideoView.a aVar = (MintegralVideoView.a) obj;
                                                this.j = true;
                                                this.h.getJSNotifyProxy().a(aVar);
                                                break;
                                            }
                                    }
                                    break;
                            }
                            if (i2 == 2) {
                                this.h.getJSNotifyProxy().a(2, "");
                            }
                            this.h.getJSVideoModule().videoOperate(3);
                            if (this.h.getJSCommon().g() != 2) {
                                if (this.b.getVideo_end_type() != 3) {
                                    this.h.getJSVideoModule().setVisible(8);
                                } else {
                                    this.h.getJSVideoModule().setVisible(0);
                                }
                                if (this.i == 2 && !this.h.getJSContainerModule().endCardShowing()) {
                                    this.h.getJSContainerModule().showEndcard(this.b.getVideo_end_type());
                                    this.h.getJSNotifyProxy().a(1);
                                    break;
                                }
                            }
                            i2 = 16;
                            this.h.getJSNotifyProxy().a(1);
                            break;
                    }
                } else {
                    h jSVideoModule2 = this.h.getJSVideoModule();
                    this.h.getJSContainerModule().configurationChanged(jSVideoModule2.getBorderViewWidth(), jSVideoModule2.getBorderViewHeight(), jSVideoModule2.getBorderViewRadius());
                }
            } else if (this.h.getJSCommon().g() == 2) {
                h jSVideoModule3 = this.h.getJSVideoModule();
                this.h.getJSContainerModule().showMiniCard(jSVideoModule3.getBorderViewTop(), jSVideoModule3.getBorderViewLeft(), jSVideoModule3.getBorderViewWidth(), jSVideoModule3.getBorderViewHeight(), jSVideoModule3.getBorderViewRadius());
            }
        }
        super.a(i2, obj);
    }
}
