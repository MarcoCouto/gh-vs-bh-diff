package com.mintegral.msdk.video.module.c;

import android.content.Context;
import android.os.Build.VERSION;
import android.text.TextUtils;
import com.integralads.avid.library.inmobi.utils.AvidJSONUtil;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.mintegral.msdk.b.b;
import com.mintegral.msdk.base.common.net.l;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.utils.c;
import com.mintegral.msdk.out.MTGConfiguration;
import com.tapjoy.TapjoyConstants;
import org.json.JSONObject;

/* compiled from: VideoViewRequest */
public final class a extends com.mintegral.msdk.base.common.net.a {
    public a(Context context) {
        super(context);
    }

    public final l b(l lVar) {
        a(lVar);
        return lVar;
    }

    /* access modifiers changed from: protected */
    public final void a(l lVar) {
        super.a(lVar);
        lVar.a(TapjoyConstants.TJC_PLATFORM, "1");
        lVar.a(TapjoyConstants.TJC_DEVICE_OS_VERSION_NAME, VERSION.RELEASE);
        lVar.a(CampaignEx.JSON_KEY_PACKAGE_NAME, c.n(this.b));
        lVar.a("app_version_name", c.j(this.b));
        StringBuilder sb = new StringBuilder();
        sb.append(c.i(this.b));
        lVar.a("app_version_code", sb.toString());
        StringBuilder sb2 = new StringBuilder();
        sb2.append(c.g(this.b));
        lVar.a("orientation", sb2.toString());
        lVar.a("model", c.c());
        lVar.a("brand", c.e());
        lVar.a("gaid", "");
        lVar.a("gaid2", c.l());
        lVar.a(RequestParameters.NETWORK_MNC, c.b());
        lVar.a(RequestParameters.NETWORK_MCC, c.a());
        int p = c.p(this.b);
        lVar.a("network_type", String.valueOf(p));
        StringBuilder sb3 = new StringBuilder();
        sb3.append(c.a(this.b, p));
        lVar.a("network_str", sb3.toString());
        lVar.a("language", c.f(this.b));
        lVar.a(TapjoyConstants.TJC_DEVICE_TIMEZONE, c.h());
        lVar.a("useragent", c.f());
        lVar.a("sdk_version", MTGConfiguration.SDK_VERSION);
        lVar.a("gp_version", c.q(this.b));
        StringBuilder sb4 = new StringBuilder();
        sb4.append(c.l(this.b));
        sb4.append(AvidJSONUtil.KEY_X);
        sb4.append(c.m(this.b));
        lVar.a("screen_size", sb4.toString());
        b.a();
        com.mintegral.msdk.b.a b = b.b(com.mintegral.msdk.base.controller.a.d().j());
        if (b != null) {
            JSONObject jSONObject = new JSONObject();
            try {
                if (b.as() == 1) {
                    if (c.b(this.b) != null) {
                        jSONObject.put("imei", c.b(this.b));
                    }
                    if (c.h(this.b) != null) {
                        jSONObject.put("mac", c.h(this.b));
                    }
                }
                if (b.au() == 1 && c.d(this.b) != null) {
                    jSONObject.put(TapjoyConstants.TJC_ANDROID_ID, c.d(this.b));
                }
                if (!TextUtils.isEmpty(jSONObject.toString())) {
                    String b2 = com.mintegral.msdk.base.utils.a.b(jSONObject.toString());
                    if (!TextUtils.isEmpty(b2)) {
                        lVar.a("dvi", b2);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
