package com.mintegral.msdk.video.js;

import com.mintegral.msdk.out.NativeListener.NativeTrackingListener;
import com.mintegral.msdk.videocommon.e.c;

/* compiled from: IJSCommon */
public interface b extends c {

    /* compiled from: IJSCommon */
    public interface a extends NativeTrackingListener {
        void a();

        void a(int i, String str);

        void b();

        void c();
    }

    void a(int i);

    void a(int i, String str);

    void a(a aVar);

    void a(c cVar);

    void a(String str);

    void a(boolean z);

    boolean a();

    void b();

    void b(int i);

    String c();

    void c(int i);

    void d();

    void d(int i);

    void e();

    void e(int i);

    void f();

    int g();
}
