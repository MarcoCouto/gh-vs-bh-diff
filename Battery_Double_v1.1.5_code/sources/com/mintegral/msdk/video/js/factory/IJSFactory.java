package com.mintegral.msdk.video.js.factory;

import com.mintegral.msdk.video.js.a;
import com.mintegral.msdk.video.js.b;
import com.mintegral.msdk.video.js.d;
import com.mintegral.msdk.video.js.e;
import com.mintegral.msdk.video.js.g;
import com.mintegral.msdk.video.js.h;

public interface IJSFactory {
    a getActivityProxy();

    g getIJSRewardVideoV1();

    b getJSCommon();

    d getJSContainerModule();

    e getJSNotifyProxy();

    h getJSVideoModule();
}
