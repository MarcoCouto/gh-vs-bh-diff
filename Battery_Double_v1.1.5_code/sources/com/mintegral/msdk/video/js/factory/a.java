package com.mintegral.msdk.video.js.factory;

import com.mintegral.msdk.video.js.a.c;
import com.mintegral.msdk.video.js.a.f;
import com.mintegral.msdk.video.js.b;
import com.mintegral.msdk.video.js.d;
import com.mintegral.msdk.video.js.e;
import com.mintegral.msdk.video.js.g;
import com.mintegral.msdk.video.js.h;

/* compiled from: DefaultJSFactory */
public class a implements IJSFactory {

    /* renamed from: a reason: collision with root package name */
    protected com.mintegral.msdk.video.js.a f2996a;
    protected b b;
    protected h c;
    protected e d;
    protected d e;
    protected g f;

    public com.mintegral.msdk.video.js.a getActivityProxy() {
        if (this.f2996a == null) {
            this.f2996a = new com.mintegral.msdk.video.js.a.a();
        }
        return this.f2996a;
    }

    public b getJSCommon() {
        if (this.b == null) {
            this.b = new com.mintegral.msdk.video.js.a.b();
        }
        return this.b;
    }

    public h getJSVideoModule() {
        if (this.c == null) {
            this.c = new f();
        }
        return this.c;
    }

    public e getJSNotifyProxy() {
        if (this.d == null) {
            this.d = new com.mintegral.msdk.video.js.a.d();
        }
        return this.d;
    }

    public d getJSContainerModule() {
        if (this.e == null) {
            this.e = new c();
        }
        return this.e;
    }

    public g getIJSRewardVideoV1() {
        if (this.f == null) {
            this.f = new com.mintegral.msdk.video.js.a.e();
        }
        return this.f;
    }
}
