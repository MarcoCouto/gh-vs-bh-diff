package com.mintegral.msdk.video.js.factory;

import android.app.Activity;
import android.webkit.WebView;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.video.js.a;
import com.mintegral.msdk.video.js.a.g;
import com.mintegral.msdk.video.js.a.h;
import com.mintegral.msdk.video.js.a.i;
import com.mintegral.msdk.video.js.a.j;
import com.mintegral.msdk.video.js.a.k;
import com.mintegral.msdk.video.js.a.l;
import com.mintegral.msdk.video.js.d;
import com.mintegral.msdk.video.js.e;
import com.mintegral.msdk.video.module.MintegralContainerView;
import com.mintegral.msdk.video.module.MintegralVideoView;

/* compiled from: JSFactory */
public final class b extends a {
    private Activity g;
    private WebView h;
    private MintegralVideoView i;
    private MintegralContainerView j;
    private CampaignEx k;

    public b(Activity activity, WebView webView, MintegralVideoView mintegralVideoView, MintegralContainerView mintegralContainerView, CampaignEx campaignEx) {
        this.g = activity;
        this.h = webView;
        this.i = mintegralVideoView;
        this.j = mintegralContainerView;
        this.k = campaignEx;
    }

    public final a getActivityProxy() {
        if (this.h == null) {
            return super.getActivityProxy();
        }
        if (this.f2996a == null) {
            this.f2996a = new g(this.h);
        }
        return this.f2996a;
    }

    public final e getJSNotifyProxy() {
        if (this.h == null) {
            return super.getJSNotifyProxy();
        }
        if (this.d == null) {
            this.d = new j(this.h);
        }
        return this.d;
    }

    public final com.mintegral.msdk.video.js.b getJSCommon() {
        if (this.g == null || this.k == null) {
            return super.getJSCommon();
        }
        if (this.b == null) {
            this.b = new h(this.g, this.k);
        }
        return this.b;
    }

    public final com.mintegral.msdk.video.js.h getJSVideoModule() {
        if (this.i == null) {
            return super.getJSVideoModule();
        }
        if (this.c == null) {
            this.c = new l(this.i);
        }
        return this.c;
    }

    public final d getJSContainerModule() {
        if (this.j == null) {
            return super.getJSContainerModule();
        }
        if (this.e == null) {
            this.e = new i(this.j);
        }
        return this.e;
    }

    public final com.mintegral.msdk.video.js.g getIJSRewardVideoV1() {
        if (this.j == null || this.g == null) {
            return super.getIJSRewardVideoV1();
        }
        if (this.f == null) {
            this.f = new k(this.g, this.j);
        }
        return this.f;
    }
}
