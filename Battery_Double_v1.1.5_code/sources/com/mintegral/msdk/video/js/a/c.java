package com.mintegral.msdk.video.js.a;

import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.video.js.d;
import com.mintegral.msdk.video.js.f;

/* compiled from: DefaultJSContainerModule */
public class c implements d, f {
    public void configurationChanged(int i, int i2, int i3) {
    }

    public void showVideoClickView(int i) {
        StringBuilder sb = new StringBuilder("showVideoClickView:");
        sb.append(i);
        g.a("js", sb.toString());
    }

    public void showEndcard(int i) {
        StringBuilder sb = new StringBuilder("showEndcard,type=");
        sb.append(i);
        g.a("js", sb.toString());
    }

    public boolean endCardShowing() {
        g.a("js", "endCardShowing");
        return true;
    }

    public boolean miniCardShowing() {
        g.a("js", "miniCardShowing");
        return false;
    }

    public void notifyCloseBtn(int i) {
        StringBuilder sb = new StringBuilder("notifyCloseBtn:state = ");
        sb.append(i);
        g.a("js", sb.toString());
    }

    public void toggleCloseBtn(int i) {
        StringBuilder sb = new StringBuilder("toggleCloseBtn:state=");
        sb.append(i);
        g.a("js", sb.toString());
    }

    public void readyStatus(int i) {
        StringBuilder sb = new StringBuilder("readyStatus:isReady=");
        sb.append(i);
        g.a("js", sb.toString());
    }

    public void showMiniCard(int i, int i2, int i3, int i4, int i5) {
        StringBuilder sb = new StringBuilder("showMiniCard top = ");
        sb.append(i);
        sb.append(" left = ");
        sb.append(i2);
        sb.append(" width = ");
        sb.append(i3);
        sb.append(" height = ");
        sb.append(i4);
        sb.append(" radius = ");
        sb.append(i5);
        g.a("js", sb.toString());
    }

    public void resizeMiniCard(int i, int i2, int i3) {
        StringBuilder sb = new StringBuilder("showMiniCard width = ");
        sb.append(i);
        sb.append(" height = ");
        sb.append(i2);
        sb.append(" radius = ");
        sb.append(i3);
        g.a("js", sb.toString());
    }
}
