package com.mintegral.msdk.video.js.a;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.b.b;
import com.mintegral.msdk.base.b.i;
import com.mintegral.msdk.base.b.w;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.entity.p;
import com.mintegral.msdk.base.utils.c;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.base.utils.k;
import com.mintegral.msdk.out.NativeListener.NativeTrackingListener;
import com.mintegral.msdk.video.js.a.b.C0066b;
import com.tapjoy.TapjoyConstants;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: JSCommon */
public final class h extends b {
    private Activity l;
    private CampaignEx m;
    private int n;

    /* compiled from: JSCommon */
    private static final class a {

        /* renamed from: a reason: collision with root package name */
        public String f2957a = c.d();
        public String b = c.i();
        public String c = "android";
        public String d;
        public String e;
        public String f;
        public String g;
        public String h;
        public String i;
        public String j;
        public String k;
        public String l;
        public String m;
        public String n;
        public String o;

        public a(Context context) {
            this.d = c.b(context);
            this.e = c.d(context);
            this.f = c.k();
            int p = c.p(context);
            this.g = String.valueOf(p);
            this.h = c.a(context, p);
            this.i = c.o(context);
            this.j = com.mintegral.msdk.base.controller.a.d().k();
            this.k = com.mintegral.msdk.base.controller.a.d().j();
            this.l = String.valueOf(k.i(context));
            this.m = String.valueOf(k.h(context));
            this.o = String.valueOf(k.c(context));
            if (context.getResources().getConfiguration().orientation == 2) {
                this.n = "landscape";
            } else {
                this.n = "portrait";
            }
        }

        public final JSONObject a() {
            JSONObject jSONObject = new JSONObject();
            try {
                com.mintegral.msdk.base.controller.authoritycontroller.a.a();
                if (com.mintegral.msdk.base.controller.authoritycontroller.a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA)) {
                    jSONObject.put("device", this.f2957a);
                    jSONObject.put("system_version", this.b);
                    jSONObject.put("network_type", this.g);
                    jSONObject.put("network_type_str", this.h);
                    jSONObject.put("device_ua", this.i);
                }
                jSONObject.put("plantform", this.c);
                com.mintegral.msdk.base.controller.authoritycontroller.a.a();
                if (com.mintegral.msdk.base.controller.authoritycontroller.a.a(MIntegralConstans.AUTHORITYIMEIMAC)) {
                    jSONObject.put("device_imei", this.d);
                }
                com.mintegral.msdk.base.controller.authoritycontroller.a.a();
                if (com.mintegral.msdk.base.controller.authoritycontroller.a.a(MIntegralConstans.AUTHORITY_ANDROID_ID)) {
                    jSONObject.put(TapjoyConstants.TJC_ANDROID_ID, this.e);
                }
                com.mintegral.msdk.base.controller.authoritycontroller.a.a();
                if (com.mintegral.msdk.base.controller.authoritycontroller.a.a(MIntegralConstans.AUTHORITY_DEVICE_ID)) {
                    jSONObject.put("google_ad_id", this.f);
                }
                jSONObject.put("appkey", this.j);
                jSONObject.put("appId", this.k);
                jSONObject.put("screen_width", this.l);
                jSONObject.put("screen_height", this.m);
                jSONObject.put("orientation", this.n);
                jSONObject.put("scale", this.o);
            } catch (JSONException e2) {
                e2.printStackTrace();
            }
            return jSONObject;
        }
    }

    public h(Activity activity, CampaignEx campaignEx) {
        this.l = activity;
        this.m = campaignEx;
    }

    public final int l() {
        return this.n;
    }

    public final void f(int i) {
        this.n = i;
    }

    public final String c() {
        JSONObject jSONObject = new JSONObject();
        a aVar = new a(com.mintegral.msdk.base.controller.a.d().h());
        try {
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put("playVideoMute", this.k);
            jSONObject.put("sdkSetting", jSONObject2);
            jSONObject.put("device", aVar.a());
            JSONArray jSONArray = new JSONArray();
            jSONArray.put(CampaignEx.campaignToJsonObject(this.m));
            jSONObject.put("campaignList", jSONArray);
            JSONObject jSONObject3 = new JSONObject();
            if (this.h != null) {
                jSONObject3 = this.h.P();
            }
            jSONObject.put("unitSetting", jSONObject3);
            String j = com.mintegral.msdk.base.controller.a.d().j();
            b.a();
            String c = b.c(j);
            if (!TextUtils.isEmpty(c)) {
                jSONObject.put("appSetting", new JSONObject(c));
            }
            this.j.a();
            this.f2954a = true;
        } catch (Throwable th) {
            th.printStackTrace();
        }
        return jSONObject.toString();
    }

    private CampaignEx a(String str, CampaignEx campaignEx) {
        if (TextUtils.isEmpty(str)) {
            return campaignEx;
        }
        if (TextUtils.isEmpty(str) && campaignEx == null) {
            campaignEx = null;
        } else if (!str.contains("notice")) {
            try {
                JSONObject campaignToJsonObject = CampaignEx.campaignToJsonObject(campaignEx);
                CampaignEx parseShortCutsCampaign = CampaignEx.parseShortCutsCampaign(campaignToJsonObject);
                if (parseShortCutsCampaign == null) {
                    parseShortCutsCampaign = campaignEx;
                }
                if (!TextUtils.isEmpty(str)) {
                    a(campaignToJsonObject, parseShortCutsCampaign);
                    JSONObject optJSONObject = new JSONObject(str).optJSONObject(com.mintegral.msdk.base.common.a.z);
                    String str2 = "-999";
                    String str3 = "-999";
                    if (optJSONObject != null) {
                        str2 = String.valueOf(k.b(this.l, (float) Integer.valueOf(optJSONObject.getString(com.mintegral.msdk.base.common.a.x)).intValue()));
                        str3 = String.valueOf(k.b(this.l, (float) Integer.valueOf(optJSONObject.getString(com.mintegral.msdk.base.common.a.y)).intValue()));
                    }
                    parseShortCutsCampaign.setClickURL(com.mintegral.msdk.click.b.a(parseShortCutsCampaign.getClickURL(), str2, str3));
                    String noticeUrl = parseShortCutsCampaign.getNoticeUrl();
                    if (optJSONObject != null) {
                        Iterator keys = optJSONObject.keys();
                        StringBuilder sb = new StringBuilder();
                        while (keys.hasNext()) {
                            sb.append(RequestParameters.AMPERSAND);
                            String str4 = (String) keys.next();
                            String optString = optJSONObject.optString(str4);
                            if (com.mintegral.msdk.base.common.a.x.equals(str4) || com.mintegral.msdk.base.common.a.y.equals(str4)) {
                                optString = String.valueOf(k.b(this.l, (float) Integer.valueOf(optString).intValue()));
                            }
                            sb.append(str4);
                            sb.append(RequestParameters.EQUAL);
                            sb.append(optString);
                        }
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append(noticeUrl);
                        sb2.append(sb);
                        parseShortCutsCampaign.setNoticeUrl(sb2.toString());
                    }
                }
                campaignEx = parseShortCutsCampaign;
            } catch (Throwable unused) {
            }
        } else {
            try {
                JSONObject campaignToJsonObject2 = CampaignEx.campaignToJsonObject(campaignEx);
                JSONObject jSONObject = new JSONObject(str);
                Iterator keys2 = jSONObject.keys();
                while (keys2.hasNext()) {
                    String str5 = (String) keys2.next();
                    campaignToJsonObject2.put(str5, jSONObject.getString(str5));
                }
                CampaignEx parseShortCutsCampaign2 = CampaignEx.parseShortCutsCampaign(campaignToJsonObject2);
                a(campaignToJsonObject2, parseShortCutsCampaign2);
                campaignEx = parseShortCutsCampaign2;
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return campaignEx;
    }

    private static void a(JSONObject jSONObject, CampaignEx campaignEx) {
        try {
            String optString = jSONObject.optString("unitId");
            if (!TextUtils.isEmpty(optString)) {
                campaignEx.setCampaignUnitId(optString);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public final void b(int i, String str) {
        super.b(i, str);
        switch (i) {
            case 1:
                if (this.m != null) {
                    CampaignEx a2 = a(str, this.m);
                    this.j.b();
                    m().a((NativeTrackingListener) this.j);
                    m().b(a2);
                    com.mintegral.msdk.video.module.b.a.d(com.mintegral.msdk.base.controller.a.d().h(), a2);
                    return;
                }
                return;
            case 2:
                return;
            case 3:
                try {
                    if (this.h.n() == -1) {
                        a((com.mintegral.msdk.video.js.b.a) new C0066b(this, this.j));
                    }
                    b(1, str);
                    break;
                } catch (Throwable th) {
                    g.c("js", th.getMessage(), th);
                    return;
                }
        }
    }

    public final void c(int i, String str) {
        super.c(i, str);
        try {
            this.j.a(i, str);
        } catch (Throwable th) {
            g.c("js", th.getMessage(), th);
        }
    }

    public final void a(int i, String str) {
        super.a(i, str);
        if (i == 2) {
            try {
                JSONObject jSONObject = new JSONObject(str);
                String optString = jSONObject.optString("event", "event");
                String optString2 = jSONObject.optString("template", "-1");
                String optString3 = jSONObject.optString(TtmlNode.TAG_LAYOUT, "-1");
                String optString4 = jSONObject.optString(MIntegralConstans.PROPERTIES_UNIT_ID, this.g);
                int p = c.p(this.l.getApplication());
                p pVar = new p("2000039", optString, optString2, optString3, optString4, this.m.getId(), p, c.a((Context) this.l.getApplication(), p));
                w.a((com.mintegral.msdk.base.b.h) i.a((Context) this.l.getApplication())).a(pVar);
            } catch (Throwable th) {
                g.c("js", th.getMessage(), th);
            }
        }
    }

    public final void f() {
        super.f();
        if (this.j != null) {
            this.j.c();
        }
    }

    public final void d() {
        super.d();
        try {
            this.l.finish();
        } catch (Throwable th) {
            g.c("js", th.getMessage(), th);
        }
    }

    private com.mintegral.msdk.click.a m() {
        if (this.i == null) {
            this.i = new com.mintegral.msdk.click.a(this.l.getApplicationContext(), this.g);
        }
        return this.i;
    }
}
