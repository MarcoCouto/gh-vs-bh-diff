package com.mintegral.msdk.video.js.a;

import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.video.js.e;
import com.mintegral.msdk.video.module.MintegralVideoView.a;

/* compiled from: DefaultJSNotifyProxy */
public class d implements e {
    public void a(int i) {
        StringBuilder sb = new StringBuilder("onVideoStatusNotify:");
        sb.append(i);
        g.a("js", sb.toString());
    }

    public void a(int i, String str) {
        StringBuilder sb = new StringBuilder("onClick:");
        sb.append(i);
        sb.append(",pt:");
        sb.append(str);
        g.a("js", sb.toString());
    }

    public void a(a aVar) {
        StringBuilder sb = new StringBuilder("onProgressNotify:");
        sb.append(aVar.toString());
        g.a("js", sb.toString());
    }

    public void a(Object obj) {
        StringBuilder sb = new StringBuilder("onWebviewShow:");
        sb.append(obj);
        g.a("js", sb.toString());
    }

    public void a(int i, int i2, int i3, int i4) {
        g.a("js", "showDataInfo");
    }
}
