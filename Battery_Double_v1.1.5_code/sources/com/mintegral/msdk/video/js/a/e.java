package com.mintegral.msdk.video.js.a;

import com.mintegral.msdk.video.js.g;

/* compiled from: DefaultJSRewardVideoV1 */
public class e implements g {
    public String a() {
        com.mintegral.msdk.base.utils.g.a("js", "getEndScreenInfo");
        return "{}";
    }

    public void notifyCloseBtn(int i) {
        StringBuilder sb = new StringBuilder("notifyCloseBtn,state=");
        sb.append(i);
        com.mintegral.msdk.base.utils.g.a("js", sb.toString());
    }

    public void toggleCloseBtn(int i) {
        StringBuilder sb = new StringBuilder("toggleCloseBtn,state=");
        sb.append(i);
        com.mintegral.msdk.base.utils.g.a("js", sb.toString());
    }

    public void a(String str) {
        StringBuilder sb = new StringBuilder("triggerCloseBtn,state=");
        sb.append(str);
        com.mintegral.msdk.base.utils.g.a("js", sb.toString());
    }

    public void b(String str) {
        StringBuilder sb = new StringBuilder("setOrientation,landscape=");
        sb.append(str);
        com.mintegral.msdk.base.utils.g.a("js", sb.toString());
    }

    public void c(String str) {
        StringBuilder sb = new StringBuilder("handlerPlayableException，msg=");
        sb.append(str);
        com.mintegral.msdk.base.utils.g.a("js", sb.toString());
    }
}
