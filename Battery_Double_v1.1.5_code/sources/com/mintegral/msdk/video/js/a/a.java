package com.mintegral.msdk.video.js.a;

import android.content.res.Configuration;
import com.mintegral.msdk.base.utils.g;

/* compiled from: DefaultJSActivity */
public class a implements com.mintegral.msdk.video.js.a {
    public void a() {
        g.a("js", "DefaultJSActivity-onPause");
    }

    public void b() {
        g.a("js", "DefaultJSActivity-onResume");
    }

    public void c() {
        g.a("js", "DefaultJSActivity-onDestory");
    }

    public void a(Configuration configuration) {
        StringBuilder sb = new StringBuilder("DefaultJSActivity-onConfigurationChanged:");
        sb.append(configuration.orientation);
        g.a("js", sb.toString());
    }

    public void d() {
        g.a("js", "DefaultJSActivity-onBackPressed");
    }

    public int e() {
        g.a("js", "isSystemResume");
        return 0;
    }

    public void a(int i) {
        StringBuilder sb = new StringBuilder("setSystemResume,isResume:");
        sb.append(i);
        g.a("js", sb.toString());
    }
}
