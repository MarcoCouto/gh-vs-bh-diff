package com.mintegral.msdk.video.js.a;

import com.mintegral.msdk.video.module.MintegralContainerView;

/* compiled from: JSContainerModule */
public final class i extends c {

    /* renamed from: a reason: collision with root package name */
    private MintegralContainerView f2958a;

    public i(MintegralContainerView mintegralContainerView) {
        this.f2958a = mintegralContainerView;
    }

    public final void showVideoClickView(int i) {
        super.showVideoClickView(i);
        if (this.f2958a != null) {
            this.f2958a.showVideoClickView(i);
        }
    }

    public final void showEndcard(int i) {
        super.showEndcard(i);
        try {
            if (this.f2958a != null) {
                this.f2958a.showEndcard(i);
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public final void toggleCloseBtn(int i) {
        super.toggleCloseBtn(i);
        try {
            if (this.f2958a != null) {
                this.f2958a.toggleCloseBtn(i);
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public final boolean endCardShowing() {
        try {
            if (this.f2958a != null) {
                return this.f2958a.endCardShowing();
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
        return super.endCardShowing();
    }

    public final boolean miniCardShowing() {
        try {
            if (this.f2958a != null) {
                return this.f2958a.miniCardShowing();
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
        return super.miniCardShowing();
    }

    public final void readyStatus(int i) {
        try {
            if (this.f2958a != null) {
                this.f2958a.readyStatus(i);
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
        super.readyStatus(i);
    }

    public final void showMiniCard(int i, int i2, int i3, int i4, int i5) {
        super.showMiniCard(i, i2, i3, i4, i5);
        try {
            if (this.f2958a != null) {
                this.f2958a.showMiniCard(i, i2, i3, i4, i5);
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public final void resizeMiniCard(int i, int i2, int i3) {
        super.resizeMiniCard(i, i2, i3);
        try {
            if (this.f2958a != null) {
                this.f2958a.resizeMiniCard(i, i2, i3);
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public final void configurationChanged(int i, int i2, int i3) {
        super.configurationChanged(i, i2, i3);
        try {
            if (this.f2958a != null) {
                this.f2958a.configurationChanged(i, i2, i3);
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }
}
