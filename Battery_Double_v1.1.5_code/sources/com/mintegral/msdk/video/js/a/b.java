package com.mintegral.msdk.video.js.a;

import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.out.Campaign;
import com.mintegral.msdk.out.NativeListener.NativeTrackingListener;
import com.mintegral.msdk.videocommon.e.c;

/* compiled from: DefaultJSCommon */
public class b implements com.mintegral.msdk.video.js.b {

    /* renamed from: a reason: collision with root package name */
    protected boolean f2954a = false;
    protected boolean b = false;
    protected int c = 0;
    protected int d = 0;
    protected int e = 0;
    protected int f = -1;
    protected String g;
    protected c h;
    protected com.mintegral.msdk.click.a i;
    public com.mintegral.msdk.video.js.b.a j = new a();
    protected int k = 2;

    /* compiled from: DefaultJSCommon */
    public static class a implements com.mintegral.msdk.video.js.b.a {
        public boolean onInterceptDefaultLoadingDialog() {
            g.a("js", "onInterceptDefaultLoadingDialog");
            return false;
        }

        public void onShowLoading(Campaign campaign) {
            StringBuilder sb = new StringBuilder("onShowLoading,campaign:");
            sb.append(campaign);
            g.a("js", sb.toString());
        }

        public void onDismissLoading(Campaign campaign) {
            StringBuilder sb = new StringBuilder("onDismissLoading,campaign:");
            sb.append(campaign);
            g.a("js", sb.toString());
        }

        public void onStartRedirection(Campaign campaign, String str) {
            StringBuilder sb = new StringBuilder("onStartRedirection,campaign:");
            sb.append(campaign);
            sb.append(",url:");
            sb.append(str);
            g.a("js", sb.toString());
        }

        public void onFinishRedirection(Campaign campaign, String str) {
            StringBuilder sb = new StringBuilder("onFinishRedirection,campaign:");
            sb.append(campaign);
            sb.append(",url:");
            sb.append(str);
            g.a("js", sb.toString());
        }

        public void onRedirectionFailed(Campaign campaign, String str) {
            StringBuilder sb = new StringBuilder("onFinishRedirection,campaign:");
            sb.append(campaign);
            sb.append(",url:");
            sb.append(str);
            g.a("js", sb.toString());
        }

        public void onDownloadStart(Campaign campaign) {
            StringBuilder sb = new StringBuilder("onDownloadStart,campaign:");
            sb.append(campaign);
            g.a("js", sb.toString());
        }

        public void onDownloadFinish(Campaign campaign) {
            StringBuilder sb = new StringBuilder("onDownloadFinish,campaign:");
            sb.append(campaign);
            g.a("js", sb.toString());
        }

        public void onDownloadProgress(int i) {
            StringBuilder sb = new StringBuilder("onDownloadProgress,progress:");
            sb.append(i);
            g.a("js", sb.toString());
        }

        public void a() {
            g.a("js", "onInitSuccess");
        }

        public void b() {
            g.a("js", "onStartInstall");
        }

        public void a(int i, String str) {
            StringBuilder sb = new StringBuilder("onH5Error,code:");
            sb.append(i);
            sb.append("，msg:");
            sb.append(str);
            g.a("js", sb.toString());
        }

        public void c() {
            g.a("js", "videoLocationReady");
        }
    }

    /* renamed from: com.mintegral.msdk.video.js.a.b$b reason: collision with other inner class name */
    /* compiled from: DefaultJSCommon */
    public static class C0066b implements com.mintegral.msdk.video.js.b.a {

        /* renamed from: a reason: collision with root package name */
        private com.mintegral.msdk.video.js.b f2955a;
        private com.mintegral.msdk.video.js.b.a b;

        public C0066b(com.mintegral.msdk.video.js.b bVar, com.mintegral.msdk.video.js.b.a aVar) {
            this.f2955a = bVar;
            this.b = aVar;
        }

        public final boolean onInterceptDefaultLoadingDialog() {
            return this.b != null && this.b.onInterceptDefaultLoadingDialog();
        }

        public final void onShowLoading(Campaign campaign) {
            if (this.b != null) {
                this.b.onShowLoading(campaign);
            }
        }

        public final void onDismissLoading(Campaign campaign) {
            if (this.b != null) {
                this.b.onDismissLoading(campaign);
            }
        }

        public final void onStartRedirection(Campaign campaign, String str) {
            if (this.b != null) {
                this.b.onStartRedirection(campaign, str);
            }
        }

        public final void onFinishRedirection(Campaign campaign, String str) {
            if (this.b != null) {
                this.b.onFinishRedirection(campaign, str);
            }
            if (this.f2955a != null) {
                this.f2955a.d();
            }
        }

        public final void onRedirectionFailed(Campaign campaign, String str) {
            if (this.b != null) {
                this.b.onRedirectionFailed(campaign, str);
            }
            if (this.f2955a != null) {
                this.f2955a.d();
            }
        }

        public final void onDownloadStart(Campaign campaign) {
            if (this.b != null) {
                this.b.onDownloadStart(campaign);
            }
        }

        public final void onDownloadFinish(Campaign campaign) {
            if (this.b != null) {
                this.b.onDownloadFinish(campaign);
            }
        }

        public final void onDownloadProgress(int i) {
            if (this.b != null) {
                this.b.onDownloadProgress(i);
            }
        }

        public final void a() {
            if (this.b != null) {
                this.b.a();
            }
        }

        public final void b() {
            if (this.b != null) {
                this.b.b();
            }
        }

        public final void a(int i, String str) {
            if (this.b != null) {
                this.b.a(i, str);
            }
        }

        public final void c() {
            if (this.b != null) {
                this.b.c();
            }
        }
    }

    public void f() {
    }

    public final void a(int i2) {
        this.k = i2;
    }

    public final void c(int i2) {
        this.c = i2;
    }

    public final void b(int i2) {
        this.d = i2;
    }

    public final void d(int i2) {
        this.e = i2;
    }

    public final int h() {
        if (this.c == 0 && this.b) {
            this.c = 1;
        }
        return this.c;
    }

    public final int i() {
        if (this.d == 0 && this.b) {
            this.d = 1;
        }
        return this.d;
    }

    public final int j() {
        if (this.e == 0 && this.b) {
            this.e = 1;
        }
        return this.e;
    }

    public final boolean k() {
        return this.b;
    }

    public final void a(boolean z) {
        StringBuilder sb = new StringBuilder("setIsShowingTransparent:");
        sb.append(z);
        g.a("js", sb.toString());
        this.b = z;
    }

    public final boolean a() {
        return this.f2954a;
    }

    public final void b() {
        this.f2954a = true;
    }

    public final void a(String str) {
        StringBuilder sb = new StringBuilder("setUnitId:");
        sb.append(str);
        g.a("js", sb.toString());
        this.g = str;
    }

    public final void a(com.mintegral.msdk.video.js.b.a aVar) {
        StringBuilder sb = new StringBuilder("setTrackingListener:");
        sb.append(aVar);
        g.a("js", sb.toString());
        this.j = aVar;
    }

    public final void a(c cVar) {
        StringBuilder sb = new StringBuilder("setSetting:");
        sb.append(cVar);
        g.a("js", sb.toString());
        this.h = cVar;
    }

    public final void e() {
        g.a("js", "release");
        if (this.i != null) {
            this.i.a();
            this.i.a((NativeTrackingListener) null);
            this.i.b();
        }
    }

    public void a(int i2, String str) {
        StringBuilder sb = new StringBuilder("statistics,type:");
        sb.append(i2);
        sb.append(",json:");
        sb.append(str);
        g.a("js", sb.toString());
    }

    public final void e(int i2) {
        this.f = i2;
    }

    public final int g() {
        return this.f;
    }

    public String c() {
        g.a("js", "init");
        return "{}";
    }

    public void b(int i2, String str) {
        StringBuilder sb = new StringBuilder("click:type");
        sb.append(i2);
        sb.append(",pt:");
        sb.append(str);
        g.a("js", sb.toString());
    }

    public void c(int i2, String str) {
        StringBuilder sb = new StringBuilder("handlerH5Exception,code=");
        sb.append(i2);
        sb.append(",msg:");
        sb.append(str);
        g.a("js", sb.toString());
    }

    public void d() {
        g.a("js", "finish");
    }
}
