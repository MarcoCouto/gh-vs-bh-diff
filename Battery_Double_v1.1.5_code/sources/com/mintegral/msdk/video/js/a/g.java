package com.mintegral.msdk.video.js.a;

import android.content.res.Configuration;
import android.util.Base64;
import android.webkit.WebView;
import org.json.JSONObject;

/* compiled from: JSActivityProxy */
public final class g extends a {

    /* renamed from: a reason: collision with root package name */
    private WebView f2956a;
    private int b = 0;

    public g(WebView webView) {
        this.f2956a = webView;
    }

    public final void a() {
        super.a();
        this.b = 1;
        com.mintegral.msdk.mtgjscommon.windvane.g.a();
        com.mintegral.msdk.mtgjscommon.windvane.g.a(this.f2956a, "onSystemPause", "");
    }

    public final void b() {
        super.b();
        this.b = 0;
        com.mintegral.msdk.mtgjscommon.windvane.g.a();
        com.mintegral.msdk.mtgjscommon.windvane.g.a(this.f2956a, "onSystemResume", "");
    }

    public final void c() {
        super.c();
        com.mintegral.msdk.mtgjscommon.windvane.g.a();
        com.mintegral.msdk.mtgjscommon.windvane.g.a(this.f2956a, "onSystemDestory", "");
    }

    public final void a(Configuration configuration) {
        super.a(configuration);
        try {
            JSONObject jSONObject = new JSONObject();
            if (configuration.orientation == 2) {
                jSONObject.put("orientation", "landscape");
            } else {
                jSONObject.put("orientation", "portrait");
            }
            String encodeToString = Base64.encodeToString(jSONObject.toString().getBytes(), 2);
            com.mintegral.msdk.mtgjscommon.windvane.g.a();
            com.mintegral.msdk.mtgjscommon.windvane.g.a(this.f2956a, "orientation", encodeToString);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public final void d() {
        super.d();
        com.mintegral.msdk.mtgjscommon.windvane.g.a();
        com.mintegral.msdk.mtgjscommon.windvane.g.a(this.f2956a, "onSystemBackPressed", "");
    }

    public final void a(int i) {
        super.a(i);
        this.b = i;
    }

    public final int e() {
        return this.b;
    }
}
