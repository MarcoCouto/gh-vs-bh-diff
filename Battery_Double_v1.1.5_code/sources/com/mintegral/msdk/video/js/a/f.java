package com.mintegral.msdk.video.js.a;

import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.video.js.h;

/* compiled from: DefaultJSVideoModule */
public class f implements h {
    public int getBorderViewHeight() {
        return 0;
    }

    public int getBorderViewLeft() {
        return 0;
    }

    public int getBorderViewRadius() {
        return 0;
    }

    public int getBorderViewTop() {
        return 0;
    }

    public int getBorderViewWidth() {
        return 0;
    }

    public boolean isH5Canvas() {
        return false;
    }

    public void showVideoLocation(int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8, int i9) {
        StringBuilder sb = new StringBuilder("showVideoLocation:marginTop=");
        sb.append(i);
        sb.append(",marginLeft=");
        sb.append(i2);
        sb.append(",width=");
        sb.append(i3);
        sb.append(",height=");
        sb.append(i4);
        sb.append(",radius=");
        sb.append(i5);
        sb.append(",borderTop=");
        sb.append(i6);
        sb.append(",borderTop=");
        sb.append(i6);
        sb.append(",borderLeft=");
        sb.append(i7);
        sb.append(",borderWidth=");
        sb.append(i8);
        sb.append(",borderHeight=");
        sb.append(i9);
        g.a("js", sb.toString());
    }

    public void soundOperate(int i, int i2) {
        StringBuilder sb = new StringBuilder("soundOperate:mute=");
        sb.append(i);
        sb.append(",soundViewVisible=");
        sb.append(i2);
        g.a("js", sb.toString());
    }

    public void soundOperate(int i, int i2, String str) {
        StringBuilder sb = new StringBuilder("soundOperate:mute=");
        sb.append(i);
        sb.append(",soundViewVisible=");
        sb.append(i2);
        sb.append(",pt=");
        sb.append(str);
        g.a("js", sb.toString());
    }

    public void videoOperate(int i) {
        StringBuilder sb = new StringBuilder("videoOperate:");
        sb.append(i);
        g.a("js", sb.toString());
    }

    public void closeVideoOperate(int i, int i2) {
        StringBuilder sb = new StringBuilder("closeOperte:close=");
        sb.append(i);
        sb.append("closeViewVisible=");
        sb.append(i2);
        g.a("js", sb.toString());
    }

    public void progressOperate(int i, int i2) {
        StringBuilder sb = new StringBuilder("progressOperate:progress=");
        sb.append(i);
        sb.append("progressViewVisible=");
        sb.append(i2);
        g.a("js", sb.toString());
    }

    public String getCurrentProgress() {
        g.a("js", "getCurrentProgress");
        return "{}";
    }

    public void setScaleFitXY(int i) {
        StringBuilder sb = new StringBuilder("setScaleFitXY:");
        sb.append(i);
        g.a("js", sb.toString());
    }

    public void setVisible(int i) {
        StringBuilder sb = new StringBuilder("setVisible:");
        sb.append(i);
        g.a("js", sb.toString());
    }

    public void setCover(boolean z) {
        StringBuilder sb = new StringBuilder("setCover:");
        sb.append(z);
        g.a("js", sb.toString());
    }

    public void notifyCloseBtn(int i) {
        StringBuilder sb = new StringBuilder("notifyCloseBtn:");
        sb.append(i);
        g.a("js", sb.toString());
    }
}
