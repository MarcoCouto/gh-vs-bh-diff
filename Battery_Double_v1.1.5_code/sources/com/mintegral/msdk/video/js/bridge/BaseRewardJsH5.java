package com.mintegral.msdk.video.js.bridge;

import android.content.Context;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import android.util.Base64;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.mtgjscommon.windvane.WindVaneWebView;
import com.mintegral.msdk.video.js.factory.IJSFactory;
import org.json.JSONObject;

public class BaseRewardJsH5 implements IRewardBridge {

    /* renamed from: a reason: collision with root package name */
    protected IJSFactory f2969a;

    public void initialize(Context context, WindVaneWebView windVaneWebView) {
        if (context instanceof IJSFactory) {
            this.f2969a = (IJSFactory) context;
        }
    }

    public void getEndScreenInfo(Object obj, String str) {
        try {
            if (this.f2969a != null) {
                g.a("JS-Reward-Brigde", "getEndScreenInfo");
                String a2 = this.f2969a.getIJSRewardVideoV1().a();
                String encodeToString = !TextUtils.isEmpty(a2) ? Base64.encodeToString(a2.getBytes(), 2) : "";
                com.mintegral.msdk.mtgjscommon.windvane.g.a();
                com.mintegral.msdk.mtgjscommon.windvane.g.a(obj, encodeToString);
            }
        } catch (Throwable th) {
            g.c("JS-Reward-Brigde", "getEndScreenInfo", th);
        }
    }

    public void install(Object obj, String str) {
        try {
            if (this.f2969a != null) {
                StringBuilder sb = new StringBuilder("install:");
                sb.append(str);
                g.a("JS-Reward-Brigde", sb.toString());
                if (this.f2969a.getJSContainerModule().endCardShowing()) {
                    this.f2969a.getJSCommon().b(3, str);
                    return;
                }
                this.f2969a.getJSCommon().b(1, str);
            }
        } catch (Throwable th) {
            g.c("JS-Reward-Brigde", "install", th);
        }
    }

    public void notifyCloseBtn(Object obj, String str) {
        try {
            if (this.f2969a != null && !TextUtils.isEmpty(str)) {
                int optInt = new JSONObject(str).optInt("state");
                StringBuilder sb = new StringBuilder("notifyCloseBtn,state:");
                sb.append(str);
                g.a("JS-Reward-Brigde", sb.toString());
                this.f2969a.getIJSRewardVideoV1().notifyCloseBtn(optInt);
            }
        } catch (Throwable th) {
            g.c("JS-Reward-Brigde", "notifyCloseBtn", th);
        }
    }

    public void toggleCloseBtn(Object obj, String str) {
        try {
            if (this.f2969a != null && !TextUtils.isEmpty(str)) {
                int optInt = new JSONObject(str).optInt("state");
                StringBuilder sb = new StringBuilder("toggleCloseBtn,state:");
                sb.append(str);
                g.a("JS-Reward-Brigde", sb.toString());
                this.f2969a.getIJSRewardVideoV1().toggleCloseBtn(optInt);
            }
        } catch (Throwable th) {
            g.c("JS-Reward-Brigde", "toggleCloseBtn", th);
        }
    }

    public void triggerCloseBtn(Object obj, String str) {
        try {
            if (this.f2969a != null && !TextUtils.isEmpty(str)) {
                this.f2969a.getIJSRewardVideoV1().a(new JSONObject(str).optString("state"));
                StringBuilder sb = new StringBuilder("triggerCloseBtn,state:");
                sb.append(str);
                g.a("JS-Reward-Brigde", sb.toString());
                com.mintegral.msdk.mtgjscommon.windvane.g.a();
                com.mintegral.msdk.mtgjscommon.windvane.g.a(obj, a(0));
            }
        } catch (Throwable th) {
            g.c("JS-Reward-Brigde", "triggerCloseBtn", th);
            com.mintegral.msdk.mtgjscommon.windvane.g.a();
            com.mintegral.msdk.mtgjscommon.windvane.g.a(obj, a(-1));
        }
    }

    public void setOrientation(Object obj, String str) {
        try {
            if (this.f2969a != null && !TextUtils.isEmpty(str)) {
                String optString = new JSONObject(str).optString("state");
                StringBuilder sb = new StringBuilder("setOrientation,state:");
                sb.append(str);
                g.a("JS-Reward-Brigde", sb.toString());
                this.f2969a.getIJSRewardVideoV1().b(optString);
            }
        } catch (Throwable th) {
            g.c("JS-Reward-Brigde", "setOrientation", th);
        }
    }

    public void handlerPlayableException(Object obj, String str) {
        try {
            if (this.f2969a != null && !TextUtils.isEmpty(str)) {
                String optString = new JSONObject(str).optString(NotificationCompat.CATEGORY_MESSAGE);
                StringBuilder sb = new StringBuilder("handlerPlayableException,msg:");
                sb.append(str);
                g.a("JS-Reward-Brigde", sb.toString());
                this.f2969a.getIJSRewardVideoV1().c(optString);
            }
        } catch (Throwable th) {
            g.c("JS-Reward-Brigde", "setOrientation", th);
        }
    }

    private static String a(int i) {
        String str = "";
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("code", i);
            String jSONObject2 = jSONObject.toString();
            if (!TextUtils.isEmpty(jSONObject2)) {
                return Base64.encodeToString(jSONObject2.getBytes(), 2);
            }
            return str;
        } catch (Throwable unused) {
            g.d("JS-Reward-Brigde", "code to string is error");
            return str;
        }
    }
}
