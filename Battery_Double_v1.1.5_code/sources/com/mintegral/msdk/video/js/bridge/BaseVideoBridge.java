package com.mintegral.msdk.video.js.bridge;

import android.content.Context;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import android.util.Base64;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.mtgjscommon.windvane.WindVaneWebView;
import com.mintegral.msdk.mtgjscommon.windvane.a;
import com.mintegral.msdk.mtgjscommon.windvane.i;
import com.mintegral.msdk.video.js.a.h;
import com.mintegral.msdk.video.js.b;
import com.mintegral.msdk.video.js.factory.IJSFactory;
import org.json.JSONException;
import org.json.JSONObject;

public class BaseVideoBridge extends i implements IVideoBridge {

    /* renamed from: a reason: collision with root package name */
    protected IJSFactory f2970a;

    public void initialize(Context context, WindVaneWebView windVaneWebView) {
        super.initialize(context, windVaneWebView);
        if (this.mContext instanceof IJSFactory) {
            this.f2970a = (IJSFactory) this.mContext;
        }
    }

    public void init(Object obj, String str) {
        g.b("JS-Video-Brigde", "init");
        try {
            boolean z = false;
            if (this.f2970a != null) {
                String c = this.f2970a.getJSCommon().c();
                if (!TextUtils.isEmpty(c)) {
                    c = Base64.encodeToString(c.getBytes(), 2);
                }
                com.mintegral.msdk.mtgjscommon.windvane.g.a();
                com.mintegral.msdk.mtgjscommon.windvane.g.a(obj, c);
                this.f2970a.getJSCommon().b();
                if (!TextUtils.isEmpty(str)) {
                    JSONObject jSONObject = new JSONObject(str);
                    int optInt = jSONObject.optInt("showTransparent");
                    int optInt2 = jSONObject.optInt("mute");
                    int optInt3 = jSONObject.optInt("closeType");
                    int optInt4 = jSONObject.optInt("orientationType");
                    b jSCommon = this.f2970a.getJSCommon();
                    if (optInt == 1) {
                        z = true;
                    }
                    jSCommon.a(z);
                    this.f2970a.getJSCommon().b(optInt2);
                    this.f2970a.getJSCommon().c(optInt3);
                    this.f2970a.getJSCommon().d(optInt4);
                }
                return;
            }
            if (obj != null) {
                a aVar = (a) obj;
                if (aVar.f2743a.getObject() instanceof h) {
                    h hVar = (h) aVar.f2743a.getObject();
                    String c2 = hVar.c();
                    if (!TextUtils.isEmpty(str)) {
                        JSONObject jSONObject2 = new JSONObject(str);
                        int optInt5 = jSONObject2.optInt("showTransparent");
                        int optInt6 = jSONObject2.optInt("mute");
                        int optInt7 = jSONObject2.optInt("closeType");
                        int optInt8 = jSONObject2.optInt("orientationType");
                        if (optInt5 == 1) {
                            z = true;
                        }
                        hVar.a(z);
                        hVar.b(optInt6);
                        hVar.c(optInt7);
                        hVar.d(optInt8);
                        StringBuilder sb = new StringBuilder("init jsCommon.setIsShowingTransparent = ");
                        sb.append(optInt5);
                        g.b("JS-Video-Brigde", sb.toString());
                    }
                    String encodeToString = Base64.encodeToString(c2.getBytes(), 2);
                    com.mintegral.msdk.mtgjscommon.windvane.g.a();
                    com.mintegral.msdk.mtgjscommon.windvane.g.a(obj, encodeToString);
                }
            }
        } catch (Throwable th) {
            g.c("JS-Video-Brigde", "init error", th);
        }
    }

    public void click(Object obj, String str) {
        int i;
        String str2;
        g.b("JS-Video-Brigde", "install");
        try {
            if (this.f2970a != null && !TextUtils.isEmpty(str)) {
                JSONObject jSONObject = new JSONObject(str);
                i = jSONObject.optInt("type");
                str2 = jSONObject.optString("pt");
                this.f2970a.getJSCommon().b(i, str2);
            }
        } catch (JSONException e) {
            i = 1;
            String str3 = "";
            e.printStackTrace();
            str2 = str3;
        } catch (Throwable th) {
            g.c("JS-Video-Brigde", "install error", th);
        }
    }

    public void statistics(Object obj, String str) {
        StringBuilder sb = new StringBuilder("statistics,params:");
        sb.append(str);
        g.b("JS-Video-Brigde", sb.toString());
        try {
            if (this.f2970a != null && !TextUtils.isEmpty(str)) {
                JSONObject jSONObject = new JSONObject(str);
                this.f2970a.getJSCommon().a(jSONObject.optInt("type"), jSONObject.optString("data"));
            }
        } catch (Throwable th) {
            g.c("JS-Video-Brigde", "statistics error", th);
        }
    }

    public void triggerCloseBtn(Object obj, String str) {
        g.b("JS-Video-Brigde", "triggerCloseBtn");
        try {
            if (this.f2970a != null && !TextUtils.isEmpty(str) && new JSONObject(str).optString("state").equals("click")) {
                this.f2970a.getJSVideoModule().closeVideoOperate(1, -1);
                com.mintegral.msdk.mtgjscommon.windvane.g.a();
                com.mintegral.msdk.mtgjscommon.windvane.g.a(obj, a(0));
            }
        } catch (Throwable th) {
            g.c("JS-Video-Brigde", "triggerCloseBtn error", th);
        }
    }

    public void showVideoLocation(Object obj, String str) {
        boolean z = this.f2970a != null;
        StringBuilder sb = new StringBuilder("showVideoLocation in basevideobridge ff = ");
        sb.append(z);
        g.b("Test", sb.toString());
        try {
            if (this.f2970a != null && !TextUtils.isEmpty(str)) {
                JSONObject jSONObject = new JSONObject(str);
                int optInt = jSONObject.optInt("margin_top", 0);
                int optInt2 = jSONObject.optInt("margin_left", 0);
                int optInt3 = jSONObject.optInt("view_width", 0);
                int optInt4 = jSONObject.optInt("view_height", 0);
                int optInt5 = jSONObject.optInt("radius", 0);
                int optInt6 = jSONObject.optInt("border_top", 0);
                int optInt7 = jSONObject.optInt("border_left", 0);
                int optInt8 = jSONObject.optInt("border_width", 0);
                int optInt9 = jSONObject.optInt("border_height", 0);
                StringBuilder sb2 = new StringBuilder("showVideoLocation,margin_top:");
                sb2.append(optInt);
                sb2.append(",marginLeft:");
                sb2.append(optInt2);
                sb2.append(",viewWidth:");
                sb2.append(optInt3);
                sb2.append(",viewHeight:");
                sb2.append(optInt4);
                sb2.append(",radius:");
                sb2.append(optInt5);
                sb2.append(",borderTop: ");
                sb2.append(optInt6);
                sb2.append(",borderLeft: ");
                sb2.append(optInt7);
                sb2.append(",borderWidth: ");
                sb2.append(optInt8);
                sb2.append(",borderHeight: ");
                sb2.append(optInt9);
                g.b("JS-Video-Brigde", sb2.toString());
                this.f2970a.getJSVideoModule().showVideoLocation(optInt, optInt2, optInt3, optInt4, optInt5, optInt6, optInt7, optInt8, optInt9);
                this.f2970a.getJSCommon().f();
            }
        } catch (Throwable th) {
            g.c("JS-Video-Brigde", "showVideoLocation error", th);
        }
    }

    public void soundOperate(Object obj, String str) {
        try {
            if (this.f2970a != null && !TextUtils.isEmpty(str)) {
                JSONObject jSONObject = new JSONObject(str);
                int optInt = jSONObject.optInt("mute");
                int optInt2 = jSONObject.optInt("view_visible");
                String optString = jSONObject.optString("pt", "");
                StringBuilder sb = new StringBuilder("soundOperate,mute:");
                sb.append(optInt);
                sb.append(",viewVisible:");
                sb.append(optInt2);
                sb.append(",pt:");
                sb.append(optString);
                g.b("JS-Video-Brigde", sb.toString());
                if (TextUtils.isEmpty(optString)) {
                    this.f2970a.getJSVideoModule().soundOperate(optInt, optInt2);
                } else {
                    this.f2970a.getJSVideoModule().soundOperate(optInt, optInt2, optString);
                }
                com.mintegral.msdk.mtgjscommon.windvane.g.a();
                com.mintegral.msdk.mtgjscommon.windvane.g.a(obj, a(0));
            }
        } catch (Throwable th) {
            g.c("JS-Video-Brigde", "soundOperate error", th);
        }
    }

    public void videoOperate(Object obj, String str) {
        try {
            if (this.f2970a != null && !TextUtils.isEmpty(str)) {
                int optInt = new JSONObject(str).optInt("pause_or_resume");
                StringBuilder sb = new StringBuilder("videoOperate,pauseOrResume:");
                sb.append(optInt);
                g.b("JS-Video-Brigde", sb.toString());
                this.f2970a.getJSVideoModule().videoOperate(optInt);
                com.mintegral.msdk.mtgjscommon.windvane.g.a();
                com.mintegral.msdk.mtgjscommon.windvane.g.a(obj, a(0));
            }
        } catch (Throwable th) {
            g.c("JS-Video-Brigde", "videoOperate error", th);
        }
    }

    public void closeVideoOperte(Object obj, String str) {
        try {
            if (this.f2970a != null && !TextUtils.isEmpty(str)) {
                JSONObject jSONObject = new JSONObject(str);
                int optInt = jSONObject.optInt("close");
                int optInt2 = jSONObject.optInt("view_visible");
                StringBuilder sb = new StringBuilder("closeVideoOperte,close:");
                sb.append(optInt);
                sb.append(",viewVisible:");
                sb.append(optInt2);
                g.b("JS-Video-Brigde", sb.toString());
                this.f2970a.getJSVideoModule().closeVideoOperate(optInt, optInt2);
                com.mintegral.msdk.mtgjscommon.windvane.g.a();
                com.mintegral.msdk.mtgjscommon.windvane.g.a(obj, a(0));
            }
        } catch (Throwable th) {
            g.c("JS-Video-Brigde", "closeOperte error", th);
        }
    }

    public void progressOperate(Object obj, String str) {
        try {
            if (this.f2970a != null && !TextUtils.isEmpty(str)) {
                JSONObject jSONObject = new JSONObject(str);
                int optInt = jSONObject.optInt(NotificationCompat.CATEGORY_PROGRESS);
                int optInt2 = jSONObject.optInt("view_visible");
                StringBuilder sb = new StringBuilder("progressOperate,progress:");
                sb.append(optInt);
                sb.append(",viewVisible:");
                sb.append(optInt2);
                g.b("JS-Video-Brigde", sb.toString());
                this.f2970a.getJSVideoModule().progressOperate(optInt, optInt2);
                com.mintegral.msdk.mtgjscommon.windvane.g.a();
                com.mintegral.msdk.mtgjscommon.windvane.g.a(obj, a(0));
            }
        } catch (Throwable th) {
            g.c("JS-Video-Brigde", "progressOperate error", th);
        }
    }

    public void getCurrentProgress(Object obj, String str) {
        try {
            if (this.f2970a != null) {
                String currentProgress = this.f2970a.getJSVideoModule().getCurrentProgress();
                StringBuilder sb = new StringBuilder("getCurrentProgress:");
                sb.append(currentProgress);
                g.b("JS-Video-Brigde", sb.toString());
                if (!TextUtils.isEmpty(currentProgress)) {
                    currentProgress = Base64.encodeToString(currentProgress.getBytes(), 2);
                }
                com.mintegral.msdk.mtgjscommon.windvane.g.a();
                com.mintegral.msdk.mtgjscommon.windvane.g.a(obj, currentProgress);
            }
        } catch (Throwable th) {
            g.c("JS-Video-Brigde", "getCurrentProgress error", th);
        }
    }

    public void showVideoClickView(Object obj, String str) {
        try {
            if (this.f2970a != null && !TextUtils.isEmpty(str)) {
                int optInt = new JSONObject(str).optInt("type");
                StringBuilder sb = new StringBuilder("showVideoClickView,type:");
                sb.append(optInt);
                g.b("JS-Video-Brigde", sb.toString());
                this.f2970a.getJSContainerModule().showVideoClickView(optInt);
            }
        } catch (Throwable th) {
            g.c("JS-Video-Brigde", "showVideoClickView error", th);
        }
    }

    public void setScaleFitXY(Object obj, String str) {
        try {
            if (this.f2970a != null && !TextUtils.isEmpty(str)) {
                int optInt = new JSONObject(str).optInt("fitxy");
                StringBuilder sb = new StringBuilder("setScaleFitXY,type:");
                sb.append(optInt);
                g.b("JS-Video-Brigde", sb.toString());
                this.f2970a.getJSVideoModule().setScaleFitXY(optInt);
            }
        } catch (Throwable th) {
            g.c("JS-Video-Brigde", "showVideoClickView error", th);
        }
    }

    public void notifyCloseBtn(Object obj, String str) {
        try {
            if (this.f2970a != null && !TextUtils.isEmpty(str)) {
                int optInt = new JSONObject(str).optInt("state");
                StringBuilder sb = new StringBuilder("notifyCloseBtn,result:");
                sb.append(optInt);
                g.b("JS-Video-Brigde", sb.toString());
                this.f2970a.getJSVideoModule().notifyCloseBtn(optInt);
            }
        } catch (Throwable th) {
            g.c("JS-Video-Brigde", "notifyCloseBtn", th);
        }
    }

    public void toggleCloseBtn(Object obj, String str) {
        try {
            if (this.f2970a != null && !TextUtils.isEmpty(str)) {
                int optInt = new JSONObject(str).optInt("state");
                StringBuilder sb = new StringBuilder("toggleCloseBtn,result:");
                sb.append(optInt);
                g.b("JS-Video-Brigde", sb.toString());
                int i = 2;
                if (optInt != 1) {
                    i = optInt == 2 ? 1 : 0;
                }
                this.f2970a.getJSVideoModule().closeVideoOperate(0, i);
            }
        } catch (Throwable th) {
            g.c("JS-Video-Brigde", "toggleCloseBtn", th);
        }
    }

    public void handlerH5Exception(Object obj, String str) {
        try {
            if (this.f2970a != null && !TextUtils.isEmpty(str)) {
                JSONObject jSONObject = new JSONObject(str);
                StringBuilder sb = new StringBuilder("handlerH5Exception,params:");
                sb.append(str);
                g.b("JS-Video-Brigde", sb.toString());
                this.f2970a.getJSCommon().c(jSONObject.optInt("code", -999), jSONObject.optString("message", "h5 error"));
            }
        } catch (Throwable th) {
            g.c("JS-Video-Brigde", "toggleCloseBtn", th);
        }
    }

    public void isSystemResume(Object obj, String str) {
        try {
            if (this.f2970a != null) {
                StringBuilder sb = new StringBuilder("isSystemResume,params:");
                sb.append(str);
                g.b("JS-Video-Brigde", sb.toString());
                int e = this.f2970a.getActivityProxy().e();
                com.mintegral.msdk.mtgjscommon.windvane.g.a();
                com.mintegral.msdk.mtgjscommon.windvane.g.a(obj, a(e));
            }
        } catch (Throwable th) {
            g.c("JS-Video-Brigde", "toggleCloseBtn", th);
        }
    }

    public void readyStatus(Object obj, String str) {
        try {
            if (this.f2970a != null) {
                StringBuilder sb = new StringBuilder("readyStatus,isReady:");
                sb.append(str);
                g.b("JS-Video-Brigde", sb.toString());
                this.f2970a.getJSContainerModule().readyStatus(new JSONObject(str).optInt("isReady", 1));
                return;
            }
            if (obj != null) {
                a aVar = (a) obj;
                if (aVar.f2743a.getObject() instanceof h) {
                    h hVar = (h) aVar.f2743a.getObject();
                    int optInt = new JSONObject(str).optInt("isReady", 1);
                    hVar.f(optInt);
                    if (aVar.f2743a instanceof WindVaneWebView) {
                        WindVaneWebView windVaneWebView = aVar.f2743a;
                        if (windVaneWebView.getWebViewListener() != null) {
                            windVaneWebView.getWebViewListener().a(optInt);
                        }
                    }
                }
            }
        } catch (Throwable th) {
            g.c("JS-Video-Brigde", "readyStatus", th);
        }
    }

    public void playVideoFinishOperate(Object obj, String str) {
        try {
            if (!TextUtils.isEmpty(str) && this.f2970a != null && !TextUtils.isEmpty(str)) {
                int optInt = new JSONObject(str).optInt("type");
                StringBuilder sb = new StringBuilder("playVideoFinishOperate,type: ");
                sb.append(optInt);
                g.b("JS-Video-Brigde", sb.toString());
                this.f2970a.getJSCommon().e(optInt);
            }
        } catch (Throwable th) {
            g.c("JS-Video-Brigde", "playVideoFinishOperate error", th);
        }
    }

    private static String a(int i) {
        String str = "";
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("code", i);
            String jSONObject2 = jSONObject.toString();
            if (!TextUtils.isEmpty(jSONObject2)) {
                return Base64.encodeToString(jSONObject2.getBytes(), 2);
            }
            return str;
        } catch (Throwable unused) {
            g.d("JS-Video-Brigde", "code to string is error");
            return str;
        }
    }
}
