package com.mintegral.msdk.video.js.bridge;

import android.os.Handler;
import android.os.Looper;
import com.mintegral.msdk.base.utils.k;

public class RewardJs extends BaseRewardJs implements IRewardBridge {
    private Handler b = new Handler(Looper.getMainLooper());

    public void getEndScreenInfo(final Object obj, final String str) {
        if (k.e()) {
            super.getEndScreenInfo(obj, str);
        } else {
            this.b.post(new Runnable() {
                public final void run() {
                    RewardJs.super.getEndScreenInfo(obj, str);
                }
            });
        }
    }

    public void install(final Object obj, final String str) {
        if (k.e()) {
            super.install(obj, str);
        } else {
            this.b.post(new Runnable() {
                public final void run() {
                    RewardJs.super.install(obj, str);
                }
            });
        }
    }

    public void notifyCloseBtn(final Object obj, final String str) {
        if (k.e()) {
            super.notifyCloseBtn(obj, str);
        } else {
            this.b.post(new Runnable() {
                public final void run() {
                    RewardJs.super.notifyCloseBtn(obj, str);
                }
            });
        }
    }

    public void toggleCloseBtn(final Object obj, final String str) {
        if (k.e()) {
            super.toggleCloseBtn(obj, str);
        } else {
            this.b.post(new Runnable() {
                public final void run() {
                    RewardJs.super.toggleCloseBtn(obj, str);
                }
            });
        }
    }

    public void triggerCloseBtn(final Object obj, final String str) {
        if (k.e()) {
            super.triggerCloseBtn(obj, str);
        } else {
            this.b.post(new Runnable() {
                public final void run() {
                    RewardJs.super.getEndScreenInfo(obj, str);
                }
            });
        }
    }

    public void setOrientation(final Object obj, final String str) {
        if (k.e()) {
            super.setOrientation(obj, str);
        } else {
            this.b.post(new Runnable() {
                public final void run() {
                    RewardJs.super.setOrientation(obj, str);
                }
            });
        }
    }

    public void handlerPlayableException(final Object obj, final String str) {
        if (k.e()) {
            super.handlerPlayableException(obj, str);
        } else {
            this.b.post(new Runnable() {
                public final void run() {
                    RewardJs.super.handlerPlayableException(obj, str);
                }
            });
        }
    }
}
