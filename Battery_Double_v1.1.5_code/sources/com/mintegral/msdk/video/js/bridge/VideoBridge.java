package com.mintegral.msdk.video.js.bridge;

import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.base.utils.k;
import com.mintegral.msdk.click.b;
import org.json.JSONException;
import org.json.JSONObject;

public class VideoBridge extends BaseVideoBridge {
    private Handler b = new Handler(Looper.getMainLooper());

    public void init(final Object obj, final String str) {
        if (k.e()) {
            super.init(obj, str);
        } else {
            this.b.post(new Runnable() {
                public final void run() {
                    VideoBridge.super.init(obj, str);
                }
            });
        }
    }

    public void click(final Object obj, final String str) {
        if (k.e()) {
            super.click(obj, str);
        } else {
            this.b.post(new Runnable() {
                public final void run() {
                    VideoBridge.super.click(obj, str);
                }
            });
        }
    }

    public void statistics(final Object obj, final String str) {
        if (k.e()) {
            super.statistics(obj, str);
        } else {
            this.b.post(new Runnable() {
                public final void run() {
                    VideoBridge.super.statistics(obj, str);
                }
            });
        }
    }

    public void triggerCloseBtn(final Object obj, final String str) {
        if (k.e()) {
            super.triggerCloseBtn(obj, str);
        } else {
            this.b.post(new Runnable() {
                public final void run() {
                    VideoBridge.super.triggerCloseBtn(obj, str);
                }
            });
        }
    }

    public void showVideoLocation(final Object obj, final String str) {
        if (k.e()) {
            super.showVideoLocation(obj, str);
        } else {
            this.b.post(new Runnable() {
                public final void run() {
                    VideoBridge.super.showVideoLocation(obj, str);
                }
            });
        }
    }

    public void soundOperate(final Object obj, final String str) {
        if (k.e()) {
            super.soundOperate(obj, str);
        } else {
            this.b.post(new Runnable() {
                public final void run() {
                    VideoBridge.super.soundOperate(obj, str);
                }
            });
        }
    }

    public void videoOperate(final Object obj, final String str) {
        if (k.e()) {
            super.videoOperate(obj, str);
        } else {
            this.b.post(new Runnable() {
                public final void run() {
                    VideoBridge.super.videoOperate(obj, str);
                }
            });
        }
    }

    public void closeVideoOperte(final Object obj, final String str) {
        if (k.e()) {
            super.closeVideoOperte(obj, str);
        } else {
            this.b.post(new Runnable() {
                public final void run() {
                    VideoBridge.super.closeVideoOperte(obj, str);
                }
            });
        }
    }

    public void progressOperate(final Object obj, final String str) {
        if (k.e()) {
            super.progressOperate(obj, str);
        } else {
            this.b.post(new Runnable() {
                public final void run() {
                    VideoBridge.super.progressOperate(obj, str);
                }
            });
        }
    }

    public void getCurrentProgress(final Object obj, final String str) {
        if (k.e()) {
            super.getCurrentProgress(obj, str);
        } else {
            this.b.post(new Runnable() {
                public final void run() {
                    VideoBridge.super.getCurrentProgress(obj, str);
                }
            });
        }
    }

    public void showVideoClickView(final Object obj, final String str) {
        if (k.e()) {
            super.showVideoClickView(obj, str);
        } else {
            this.b.post(new Runnable() {
                public final void run() {
                    VideoBridge.super.showVideoClickView(obj, str);
                }
            });
        }
    }

    public void setScaleFitXY(final Object obj, final String str) {
        if (k.e()) {
            super.setScaleFitXY(obj, str);
        } else {
            this.b.post(new Runnable() {
                public final void run() {
                    VideoBridge.super.setScaleFitXY(obj, str);
                }
            });
        }
    }

    public void notifyCloseBtn(final Object obj, final String str) {
        if (k.e()) {
            super.notifyCloseBtn(obj, str);
        } else {
            this.b.post(new Runnable() {
                public final void run() {
                    VideoBridge.super.notifyCloseBtn(obj, str);
                }
            });
        }
    }

    public void toggleCloseBtn(final Object obj, final String str) {
        if (k.e()) {
            super.toggleCloseBtn(obj, str);
        } else {
            this.b.post(new Runnable() {
                public final void run() {
                    VideoBridge.super.toggleCloseBtn(obj, str);
                }
            });
        }
    }

    public void handlerH5Exception(final Object obj, final String str) {
        if (k.e()) {
            super.handlerH5Exception(obj, str);
        } else {
            this.b.post(new Runnable() {
                public final void run() {
                    VideoBridge.super.handlerH5Exception(obj, str);
                }
            });
        }
    }

    public void isSystemResume(final Object obj, final String str) {
        if (k.e()) {
            super.isSystemResume(obj, str);
        } else {
            this.b.post(new Runnable() {
                public final void run() {
                    VideoBridge.super.isSystemResume(obj, str);
                }
            });
        }
    }

    public void readyStatus(final Object obj, final String str) {
        g.b("JS-Video-Brigde", "VIDEOBridge readyStatus");
        if (k.e()) {
            super.readyStatus(obj, str);
        } else {
            this.b.post(new Runnable() {
                public final void run() {
                    VideoBridge.super.readyStatus(obj, str);
                }
            });
        }
    }

    public void playVideoFinishOperate(final Object obj, final String str) {
        if (k.e()) {
            super.playVideoFinishOperate(obj, str);
        } else {
            this.b.post(new Runnable() {
                public final void run() {
                    VideoBridge.super.playVideoFinishOperate(obj, str);
                }
            });
        }
    }

    public void openURL(Object obj, String str) {
        StringBuilder sb = new StringBuilder("get H5 params:");
        sb.append(str);
        g.d("JS-Video-Brigde", sb.toString());
        if (!TextUtils.isEmpty(str)) {
            try {
                JSONObject jSONObject = new JSONObject(str);
                String optString = jSONObject.optString("url");
                if (jSONObject.optInt("type") == 1) {
                    b.a(this.mContext, optString);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
