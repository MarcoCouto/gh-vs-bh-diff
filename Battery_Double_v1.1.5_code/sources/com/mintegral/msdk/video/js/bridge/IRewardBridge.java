package com.mintegral.msdk.video.js.bridge;

public interface IRewardBridge {
    void getEndScreenInfo(Object obj, String str);

    void handlerPlayableException(Object obj, String str);

    void install(Object obj, String str);

    void notifyCloseBtn(Object obj, String str);

    void setOrientation(Object obj, String str);

    void toggleCloseBtn(Object obj, String str);

    void triggerCloseBtn(Object obj, String str);
}
