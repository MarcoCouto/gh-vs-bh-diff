package com.mintegral.msdk.video.js.bridge;

public interface IVideoBridge {
    void click(Object obj, String str);

    void closeVideoOperte(Object obj, String str);

    void getCurrentProgress(Object obj, String str);

    void handlerH5Exception(Object obj, String str);

    void init(Object obj, String str);

    void isSystemResume(Object obj, String str);

    void notifyCloseBtn(Object obj, String str);

    void playVideoFinishOperate(Object obj, String str);

    void progressOperate(Object obj, String str);

    void readyStatus(Object obj, String str);

    void setScaleFitXY(Object obj, String str);

    void showVideoClickView(Object obj, String str);

    void showVideoLocation(Object obj, String str);

    void soundOperate(Object obj, String str);

    void statistics(Object obj, String str);

    void toggleCloseBtn(Object obj, String str);

    void triggerCloseBtn(Object obj, String str);

    void videoOperate(Object obj, String str);
}
