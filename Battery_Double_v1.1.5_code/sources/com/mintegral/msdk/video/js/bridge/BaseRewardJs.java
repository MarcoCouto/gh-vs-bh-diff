package com.mintegral.msdk.video.js.bridge;

import android.content.Context;
import android.text.TextUtils;
import android.util.Base64;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.mtgjscommon.windvane.WindVaneWebView;
import com.mintegral.msdk.mtgjscommon.windvane.i;
import org.json.JSONObject;

public class BaseRewardJs extends i implements IRewardBridge {

    /* renamed from: a reason: collision with root package name */
    protected IRewardBridge f2968a;

    public void initialize(Context context, WindVaneWebView windVaneWebView) {
        boolean z;
        super.initialize(context, windVaneWebView);
        try {
            z = Class.forName("com.mintegral.msdk.video.js.factory.IJSFactory").isInstance(this.mContext);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            z = false;
        }
        if (z) {
            try {
                Class cls = Class.forName("com.mintegral.msdk.video.js.bridge.BaseRewardJsH5");
                this.f2968a = (IRewardBridge) cls.newInstance();
                cls.getMethod("initialize", new Class[]{Context.class, WindVaneWebView.class}).invoke(this.f2968a, new Object[]{context, windVaneWebView});
            } catch (Exception e2) {
                if (MIntegralConstans.DEBUG) {
                    e2.printStackTrace();
                }
            }
        } else {
            if (windVaneWebView.getObject() != null && (windVaneWebView.getObject() instanceof IRewardBridge)) {
                this.f2968a = (IRewardBridge) windVaneWebView.getObject();
            }
        }
    }

    public void getEndScreenInfo(Object obj, String str) {
        try {
            g.d("JS-Reward-Brigde", "getEndScreenInfo");
            if (this.f2968a != null) {
                this.f2968a.getEndScreenInfo(obj, str);
            }
        } catch (Throwable th) {
            g.c("JS-Reward-Brigde", "getEndScreenInfo", th);
        }
    }

    public void install(Object obj, String str) {
        try {
            if (this.f2968a != null) {
                this.f2968a.install(obj, str);
            }
        } catch (Throwable th) {
            g.c("JS-Reward-Brigde", "install", th);
        }
    }

    public void notifyCloseBtn(Object obj, String str) {
        try {
            if (this.f2968a != null && !TextUtils.isEmpty(str)) {
                this.f2968a.notifyCloseBtn(obj, str);
            }
        } catch (Throwable th) {
            g.c("JS-Reward-Brigde", "notifyCloseBtn", th);
        }
    }

    public void toggleCloseBtn(Object obj, String str) {
        try {
            if (this.f2968a != null && !TextUtils.isEmpty(str)) {
                this.f2968a.toggleCloseBtn(obj, str);
            }
        } catch (Throwable th) {
            g.c("JS-Reward-Brigde", "toggleCloseBtn", th);
        }
    }

    public void triggerCloseBtn(Object obj, String str) {
        try {
            if (this.f2968a != null && !TextUtils.isEmpty(str)) {
                this.f2968a.triggerCloseBtn(obj, str);
            }
        } catch (Throwable th) {
            g.c("JS-Reward-Brigde", "triggerCloseBtn", th);
            com.mintegral.msdk.mtgjscommon.windvane.g.a();
            com.mintegral.msdk.mtgjscommon.windvane.g.a(obj, a());
        }
    }

    public void setOrientation(Object obj, String str) {
        try {
            if (this.f2968a != null && !TextUtils.isEmpty(str)) {
                this.f2968a.setOrientation(obj, str);
            }
        } catch (Throwable th) {
            g.c("JS-Reward-Brigde", "setOrientation", th);
        }
    }

    public void handlerPlayableException(Object obj, String str) {
        try {
            if (this.f2968a != null && !TextUtils.isEmpty(str)) {
                this.f2968a.handlerPlayableException(obj, str);
            }
        } catch (Throwable th) {
            g.c("JS-Reward-Brigde", "handlerPlayableException", th);
        }
    }

    private static String a() {
        String str = "";
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("code", -1);
            String jSONObject2 = jSONObject.toString();
            if (!TextUtils.isEmpty(jSONObject2)) {
                return Base64.encodeToString(jSONObject2.getBytes(), 2);
            }
            return str;
        } catch (Throwable unused) {
            g.d("JS-Reward-Brigde", "code to string is error");
            return str;
        }
    }
}
