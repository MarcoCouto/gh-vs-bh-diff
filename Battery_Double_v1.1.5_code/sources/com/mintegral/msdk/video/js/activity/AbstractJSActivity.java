package com.mintegral.msdk.video.js.activity;

import android.app.Activity;
import android.content.res.Configuration;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.video.js.b;
import com.mintegral.msdk.video.js.d;
import com.mintegral.msdk.video.js.e;
import com.mintegral.msdk.video.js.factory.IJSFactory;
import com.mintegral.msdk.video.js.factory.a;
import com.mintegral.msdk.video.js.h;

public abstract class AbstractJSActivity extends Activity implements IJSFactory {
    protected static final String TAG = "AbstractJSActivity";
    protected IJSFactory jsFactory = new a();

    public boolean canBackPress() {
        return false;
    }

    public void registerJsFactory(IJSFactory iJSFactory) {
        this.jsFactory = iJSFactory;
    }

    public void onResume() {
        super.onResume();
        if (getJSCommon().a()) {
            getActivityProxy().b();
        }
        getActivityProxy().a(0);
    }

    public void onPause() {
        super.onPause();
        if (getJSCommon().a()) {
            getActivityProxy().a();
        }
        getActivityProxy().a(1);
    }

    public void onDestroy() {
        super.onDestroy();
        if (getJSCommon().a()) {
            getActivityProxy().c();
        }
        com.mintegral.msdk.video.module.b.a.a();
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        if (getJSCommon().a()) {
            getActivityProxy().a(configuration);
        }
    }

    public void onBackPressed() {
        if (getJSCommon().a()) {
            if (getJSContainerModule() == null || !getJSContainerModule().miniCardShowing()) {
                getActivityProxy().d();
            }
        } else if (canBackPress()) {
            super.onBackPressed();
        } else {
            g.a(TAG, "onBackPressed can't excute");
        }
    }

    public com.mintegral.msdk.video.js.a getActivityProxy() {
        return this.jsFactory.getActivityProxy();
    }

    public b getJSCommon() {
        return this.jsFactory.getJSCommon();
    }

    public h getJSVideoModule() {
        return this.jsFactory.getJSVideoModule();
    }

    public e getJSNotifyProxy() {
        return this.jsFactory.getJSNotifyProxy();
    }

    public d getJSContainerModule() {
        return this.jsFactory.getJSContainerModule();
    }

    public com.mintegral.msdk.video.js.g getIJSRewardVideoV1() {
        return this.jsFactory.getIJSRewardVideoV1();
    }
}
