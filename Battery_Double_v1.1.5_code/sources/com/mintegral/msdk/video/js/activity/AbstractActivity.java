package com.mintegral.msdk.video.js.activity;

import android.content.Intent;
import android.os.Bundle;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.base.utils.p;

public abstract class AbstractActivity extends AbstractJSActivity {
    protected static final int ERROR_UNKNOWN = 0;
    protected a errorListener = new C0067a();
    protected boolean loadSuccess = false;

    public interface a {

        /* renamed from: com.mintegral.msdk.video.js.activity.AbstractActivity$a$a reason: collision with other inner class name */
        public static class C0067a implements a {

            /* renamed from: a reason: collision with root package name */
            private boolean f2963a = false;

            public void a(String str) {
                g.d("ActivityErrorListener", str);
                this.f2963a = true;
            }

            public final void a() {
                this.f2963a = true;
            }
        }

        void a();

        void a(String str);
    }

    public abstract boolean checkEnv(Intent intent);

    public abstract int getLayoutID();

    /* access modifiers changed from: protected */
    public abstract boolean initVideoView();

    /* access modifiers changed from: protected */
    public abstract void loadVideoData();

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        try {
            g.a("AbstractJSActivity", "onCreate");
            super.onCreate(bundle);
            if (checkEnv(getIntent())) {
                int layoutID = getLayoutID();
                if (layoutID <= 0) {
                    g.d("AbstractJSActivity", "layoutID not found");
                    this.errorListener.a("not found resource");
                    finish();
                    return;
                }
                setContentView(layoutID);
                if (!initVideoView()) {
                    this.errorListener.a("not found View IDS");
                    finish();
                    return;
                }
                this.loadSuccess = true;
                loadVideoData();
                return;
            }
            g.d("AbstractJSActivity", "checkEnv error");
            this.errorListener.a("data error");
            finish();
        } catch (Throwable th) {
            g.c("AbstractJSActivity", "onCreate error", th);
            StringBuilder sb = new StringBuilder("onCreate error.");
            sb.append(th.getMessage());
            defaultLoad(0, sb.toString());
            finish();
        }
    }

    public boolean isLoadSuccess() {
        return this.loadSuccess;
    }

    public void defaultLoad(int i, String str) {
        StringBuilder sb = new StringBuilder("receiveError:");
        sb.append(i);
        sb.append(",descroption:");
        sb.append(str);
        g.d("AbstractJSActivity", sb.toString());
    }

    public void receiveSuccess() {
        g.d("AbstractJSActivity", "receiveSuccess");
    }

    public void registerErrorListener(a aVar) {
        this.errorListener = aVar;
    }

    public int findID(String str) {
        return p.a(getApplicationContext(), str, "id");
    }

    public int findLayout(String str) {
        return p.a(getApplicationContext(), str, TtmlNode.TAG_LAYOUT);
    }
}
