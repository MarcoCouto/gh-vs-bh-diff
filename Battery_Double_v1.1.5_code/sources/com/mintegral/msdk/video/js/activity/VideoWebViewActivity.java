package com.mintegral.msdk.video.js.activity;

import android.content.Context;
import android.os.Handler;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.utils.k;
import com.mintegral.msdk.mtgjscommon.windvane.WindVaneWebView;
import com.mintegral.msdk.video.js.a.h;
import com.mintegral.msdk.video.module.MintegralContainerView;
import com.mintegral.msdk.video.module.MintegralVideoView;
import com.mintegral.msdk.videocommon.a.C0068a;
import org.json.JSONObject;

public abstract class VideoWebViewActivity extends AbstractActivity {

    /* renamed from: a reason: collision with root package name */
    private boolean f2964a = false;
    /* access modifiers changed from: private */
    public int b = 0;
    public boolean isBidCampaign = false;
    protected Runnable jsbridgeConnectTimeout = new Runnable() {
        public final void run() {
            if (VideoWebViewActivity.this.getActivityProxy().e() == 0) {
                VideoWebViewActivity.this.defaultLoad(-3, "JS bridge connect timeout");
            } else {
                VideoWebViewActivity.this.b = -4;
            }
        }
    };
    /* access modifiers changed from: protected */
    public Handler mHandler = new Handler();
    public boolean mIsIV = false;
    /* access modifiers changed from: protected */
    public MintegralContainerView mintegralContainerView;
    protected MintegralVideoView mintegralVideoView;
    /* access modifiers changed from: protected */
    public Runnable receiveRunnable = new Runnable() {
        public final void run() {
            if (VideoWebViewActivity.this.getActivityProxy().e() == 0) {
                VideoWebViewActivity.this.defaultLoad(-1, "WebView load timeout");
            } else {
                VideoWebViewActivity.this.b = -3;
            }
        }
    };
    public String unitId = "";
    protected WindVaneWebView windVaneWebView;

    public static final class a {
    }

    public abstract MintegralContainerView findMintegralContainerView();

    public abstract MintegralVideoView findMintegralVideoView();

    public abstract WindVaneWebView findWindVaneWebView();

    public abstract CampaignEx getCampaignEx();

    public abstract a getH5Templete();

    public abstract boolean initViews();

    public abstract void loadModuleDatas();

    /* access modifiers changed from: protected */
    public int getH5Orientation() {
        h jSCommonByCampaign = getJSCommonByCampaign(getCampaignEx());
        if (jSCommonByCampaign != null) {
            return jSCommonByCampaign.j();
        }
        return 0;
    }

    /* access modifiers changed from: protected */
    public int getH5CloseType() {
        h jSCommonByCampaign = getJSCommonByCampaign(getCampaignEx());
        if (jSCommonByCampaign != null) {
            return jSCommonByCampaign.h();
        }
        return 0;
    }

    /* access modifiers changed from: protected */
    public int getH5MuteState() {
        h jSCommonByCampaign = getJSCommonByCampaign(getCampaignEx());
        if (jSCommonByCampaign != null) {
            return jSCommonByCampaign.i();
        }
        return 0;
    }

    /* access modifiers changed from: protected */
    public boolean getIsShowingTransparent() {
        h jSCommonByCampaign = getJSCommonByCampaign(getCampaignEx());
        if (jSCommonByCampaign != null) {
            return jSCommonByCampaign.k();
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public boolean isShowingAlertView() {
        if (this.mintegralVideoView != null) {
            return this.mintegralVideoView.isShowingAlertView();
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public h getJSCommonByCampaign(CampaignEx campaignEx) {
        if (campaignEx == null) {
            return null;
        }
        C0068a a2 = com.mintegral.msdk.videocommon.a.a(this.mIsIV ? 287 : 94, campaignEx);
        if (a2 != null && a2.b()) {
            WindVaneWebView a3 = a2.a();
            if (a3.getObject() instanceof h) {
                return (h) a3.getObject();
            }
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public void loadVideoData() {
        int i;
        int i2;
        try {
            if (this.windVaneWebView != null) {
                int i3 = getResources().getConfiguration().orientation;
                if (getIsShowingTransparent()) {
                    i2 = k.j(this);
                    i = k.k(this);
                    if (k.m(this)) {
                        int l = k.l(this);
                        if (i3 == 2) {
                            i2 += l;
                        } else {
                            i += l;
                        }
                    }
                } else {
                    i2 = k.i(this);
                    i = k.h(this);
                }
                int b2 = getCampaignEx().getRewardTemplateMode().b();
                if (getH5Orientation() == 1) {
                    b2 = i3;
                }
                getJSNotifyProxy().a(i3, b2, i2, i);
                JSONObject jSONObject = new JSONObject();
                jSONObject.put(com.mintegral.msdk.base.common.a.C, (double) k.c((Context) this));
                getJSNotifyProxy().a((Object) jSONObject.toString());
                getJSCommon().b();
                loadModuleDatas();
                this.mHandler.postDelayed(this.receiveRunnable, 2000);
            }
        } catch (Exception e) {
            if (MIntegralConstans.DEBUG) {
                e.printStackTrace();
            }
        }
    }

    public void onResume() {
        super.onResume();
        Runnable runnable = this.b == -3 ? this.receiveRunnable : this.b == -4 ? this.jsbridgeConnectTimeout : null;
        if (runnable != null) {
            runnable.run();
            this.b = 0;
        }
    }

    /* access modifiers changed from: protected */
    public boolean initVideoView() {
        this.windVaneWebView = findWindVaneWebView();
        this.mintegralVideoView = findMintegralVideoView();
        this.mintegralVideoView.setIsIV(this.mIsIV);
        this.mintegralVideoView.setUnitId(this.unitId);
        this.mintegralContainerView = findMintegralContainerView();
        return (this.mintegralVideoView == null || this.mintegralContainerView == null || !initViews()) ? false : true;
    }

    public void defaultLoad(int i, String str) {
        super.defaultLoad(i, str);
        this.mHandler.removeCallbacks(this.receiveRunnable);
        this.mHandler.removeCallbacks(this.jsbridgeConnectTimeout);
        this.errorListener.a();
        if (this.windVaneWebView != null) {
            this.windVaneWebView.setVisibility(8);
        }
    }

    public void finish() {
        this.mHandler.removeCallbacks(this.receiveRunnable);
        this.mHandler.removeCallbacks(this.jsbridgeConnectTimeout);
        this.f2964a = true;
        if (isLoadSuccess()) {
            if (this.windVaneWebView != null) {
                this.windVaneWebView.clearWebView();
            }
            this.mHandler.postDelayed(new Runnable() {
                public final void run() {
                    VideoWebViewActivity.super.finish();
                }
            }, 100);
            return;
        }
        super.finish();
    }

    public void onDestroy() {
        super.onDestroy();
        this.mHandler.removeCallbacks(this.receiveRunnable);
        this.mHandler.removeCallbacks(this.jsbridgeConnectTimeout);
        this.f2964a = true;
        if (this.windVaneWebView != null) {
            this.windVaneWebView.release();
        }
        getJSCommon().e();
    }
}
