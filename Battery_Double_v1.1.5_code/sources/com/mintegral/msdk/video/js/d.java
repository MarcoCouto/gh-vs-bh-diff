package com.mintegral.msdk.video.js;

/* compiled from: IJSContainerModule */
public interface d {
    void configurationChanged(int i, int i2, int i3);

    boolean endCardShowing();

    boolean miniCardShowing();

    void readyStatus(int i);

    void resizeMiniCard(int i, int i2, int i3);

    void showEndcard(int i);

    void showMiniCard(int i, int i2, int i3, int i4, int i5);

    void showVideoClickView(int i);
}
