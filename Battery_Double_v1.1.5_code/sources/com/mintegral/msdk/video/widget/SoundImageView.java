package com.mintegral.msdk.video.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;
import com.mintegral.msdk.base.utils.p;

public class SoundImageView extends ImageView {

    /* renamed from: a reason: collision with root package name */
    private boolean f3034a = true;

    public SoundImageView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public SoundImageView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public SoundImageView(Context context) {
        super(context);
    }

    public boolean getStatus() {
        return this.f3034a;
    }

    public void setSoundStatus(boolean z) {
        this.f3034a = z;
        if (this.f3034a) {
            setImageResource(p.a(getContext(), "mintegral_reward_sound_open", "drawable"));
        } else {
            setImageResource(p.a(getContext(), "mintegral_reward_sound_close", "drawable"));
        }
    }
}
