package com.mintegral.msdk.video;

public final class R {

    public static final class color {
        public static final int mintegral_reward_black = 2131099790;
        public static final int mintegral_reward_cta_bg = 2131099791;
        public static final int mintegral_reward_desc_textcolor = 2131099792;
        public static final int mintegral_reward_endcard_hor_bg = 2131099793;
        public static final int mintegral_reward_endcard_land_bg = 2131099794;
        public static final int mintegral_reward_endcard_line_bg = 2131099795;
        public static final int mintegral_reward_endcard_vast_bg = 2131099796;
        public static final int mintegral_reward_minicard_bg = 2131099798;
        public static final int mintegral_reward_six_black_transparent = 2131099799;
        public static final int mintegral_reward_title_textcolor = 2131099800;
        public static final int mintegral_reward_white = 2131099801;

        private color() {
        }
    }

    public static final class drawable {
        public static final int mintegral_reward_activity_ad_end_land_des_rl_hot = 2131231105;
        public static final int mintegral_reward_close = 2131231106;
        public static final int mintegral_reward_end_close_shape_oval = 2131231107;
        public static final int mintegral_reward_end_land_shape = 2131231108;
        public static final int mintegral_reward_end_pager_logo = 2131231109;
        public static final int mintegral_reward_end_shape_oval = 2131231110;
        public static final int mintegral_reward_shape_end_pager = 2131231111;
        public static final int mintegral_reward_shape_progress = 2131231112;
        public static final int mintegral_reward_sound_close = 2131231113;
        public static final int mintegral_reward_sound_open = 2131231114;
        public static final int mintegral_reward_vast_end_close = 2131231115;
        public static final int mintegral_reward_vast_end_ok = 2131231116;

        private drawable() {
        }
    }

    public static final class id {
        public static final int mintegral_iv_adbanner = 2131296489;
        public static final int mintegral_iv_appicon = 2131296490;
        public static final int mintegral_iv_close = 2131296491;
        public static final int mintegral_iv_hottag = 2131296492;
        public static final int mintegral_iv_icon = 2131296493;
        public static final int mintegral_iv_iconbg = 2131296494;
        public static final int mintegral_iv_vastclose = 2131296500;
        public static final int mintegral_iv_vastok = 2131296501;
        public static final int mintegral_ll_bottomlayout = 2131296505;
        public static final int mintegral_rl_bodycontainer = 2131296517;
        public static final int mintegral_rl_bottomcontainer = 2131296518;
        public static final int mintegral_rl_content = 2131296519;
        public static final int mintegral_rl_playing_close = 2131296521;
        public static final int mintegral_rl_topcontainer = 2131296522;
        public static final int mintegral_sound_switch = 2131296523;
        public static final int mintegral_sv_starlevel = 2131296524;
        public static final int mintegral_tv_adtag = 2131296526;
        public static final int mintegral_tv_appdesc = 2131296527;
        public static final int mintegral_tv_apptitle = 2131296528;
        public static final int mintegral_tv_cta = 2131296529;
        public static final int mintegral_tv_desc = 2131296530;
        public static final int mintegral_tv_install = 2131296531;
        public static final int mintegral_tv_sound = 2131296532;
        public static final int mintegral_tv_vasttag = 2131296533;
        public static final int mintegral_tv_vasttitle = 2131296534;
        public static final int mintegral_vfpv = 2131296535;
        public static final int mintegral_view_bottomline = 2131296544;
        public static final int mintegral_view_shadow = 2131296545;
        public static final int mintegral_viewgroup_ctaroot = 2131296546;
        public static final int mintegral_windwv_close = 2131296547;
        public static final int mintegral_windwv_content = 2131296548;

        private id() {
        }
    }

    public static final class layout {
        public static final int mintegral_reward_clickable_cta = 2131427419;
        public static final int mintegral_reward_endcard_h5 = 2131427420;
        public static final int mintegral_reward_endcard_native_hor = 2131427421;
        public static final int mintegral_reward_endcard_native_land = 2131427422;
        public static final int mintegral_reward_endcard_vast = 2131427423;
        public static final int mintegral_reward_videoview_item = 2131427424;

        private layout() {
        }
    }

    public static final class string {
        public static final int mintegral_reward_appdesc = 2131624116;
        public static final int mintegral_reward_apptitle = 2131624117;
        public static final int mintegral_reward_clickable_cta_btntext = 2131624118;
        public static final int mintegral_reward_endcard_ad = 2131624119;
        public static final int mintegral_reward_endcard_vast_notice = 2131624120;
        public static final int mintegral_reward_install = 2131624121;

        private string() {
        }
    }

    private R() {
    }
}
