package com.mintegral.msdk.mtgjscommon.b;

import android.net.http.SslError;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.mtgjscommon.windvane.c;

/* compiled from: DefaultWebViewListener */
public class a implements c {
    public void a(int i) {
    }

    public final void a() {
        g.a("WindVaneWebView", "onPageStarted");
    }

    public final boolean b() {
        g.a("WindVaneWebView", "shouldOverrideUrlLoading");
        return true;
    }

    public void a(WebView webView, int i, String str, String str2) {
        g.a("WindVaneWebView", "onReceivedError");
    }

    public void a(WebView webView, SslErrorHandler sslErrorHandler, SslError sslError) {
        g.a("WindVaneWebView", "onReceivedSslError");
    }

    public void a(WebView webView, String str) {
        g.a("WindVaneWebView", "onPageFinished");
    }

    public final void c() {
        g.a("WindVaneWebView", "onProgressChanged");
    }
}
