package com.mintegral.msdk.mtgjscommon.base;

import android.net.Uri;
import android.net.http.SslError;
import android.webkit.SslErrorHandler;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.mintegral.msdk.base.utils.o;
import com.mintegral.msdk.mtgjscommon.windvane.c;
import java.io.ByteArrayInputStream;
import java.util.Locale;

/* compiled from: BaseWebViewClient */
public class b extends WebViewClient {

    /* renamed from: a reason: collision with root package name */
    private a f2737a;
    private c b;

    public final void a(a aVar) {
        this.f2737a = aVar;
    }

    public boolean shouldOverrideUrlLoading(WebView webView, String str) {
        if (this.f2737a != null && this.f2737a.a(str)) {
            return true;
        }
        if (this.b != null) {
            this.b.b();
        }
        return super.shouldOverrideUrlLoading(webView, str);
    }

    public void onReceivedError(WebView webView, int i, String str, String str2) {
        super.onReceivedError(webView, i, str, str2);
        if (this.b != null) {
            this.b.a(webView, i, str, str2);
        }
    }

    public void onReceivedSslError(WebView webView, SslErrorHandler sslErrorHandler, SslError sslError) {
        super.onReceivedSslError(webView, sslErrorHandler, sslError);
        if (this.b != null) {
            this.b.a(webView, sslErrorHandler, sslError);
        }
    }

    public void onPageFinished(WebView webView, String str) {
        super.onPageFinished(webView, str);
        if (this.b != null) {
            this.b.a(webView, str);
        }
    }

    public final void a(c cVar) {
        this.b = cVar;
    }

    private static WebResourceResponse a() {
        try {
            StringBuilder sb = new StringBuilder("javascript:");
            sb.append(o.f2621a);
            return new WebResourceResponse("text/javascript", "UTF-8", new ByteArrayInputStream(sb.toString().getBytes()));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public WebResourceResponse shouldInterceptRequest(WebView webView, String str) {
        if (!"mraid.js".equals(Uri.parse(str.toLowerCase(Locale.US)).getLastPathSegment())) {
            return super.shouldInterceptRequest(webView, str);
        }
        WebResourceResponse a2 = a();
        if (a2 != null) {
            return a2;
        }
        return super.shouldInterceptRequest(webView, str);
    }
}
