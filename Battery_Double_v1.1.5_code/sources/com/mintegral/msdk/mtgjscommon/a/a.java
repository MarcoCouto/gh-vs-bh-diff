package com.mintegral.msdk.mtgjscommon.a;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/* compiled from: Hack */
public final class a {

    /* renamed from: a reason: collision with root package name */
    private static C0058a f2726a;

    /* renamed from: com.mintegral.msdk.mtgjscommon.a.a$a reason: collision with other inner class name */
    /* compiled from: Hack */
    public interface C0058a {
        boolean a();
    }

    /* compiled from: Hack */
    public static abstract class b {

        /* renamed from: com.mintegral.msdk.mtgjscommon.a.a$b$a reason: collision with other inner class name */
        /* compiled from: Hack */
        public static class C0059a extends Throwable {

            /* renamed from: a reason: collision with root package name */
            private Class<?> f2727a;
            private String b;

            public C0059a(Exception exc) {
                super(exc);
            }

            public final String toString() {
                if (getCause() == null) {
                    return super.toString();
                }
                StringBuilder sb = new StringBuilder();
                sb.append(getClass().getName());
                sb.append(": ");
                sb.append(getCause());
                return sb.toString();
            }

            public final void a(Class<?> cls) {
                this.f2727a = cls;
            }

            public final void a(String str) {
                this.b = str;
            }
        }
    }

    /* compiled from: Hack */
    public static class c<C> {

        /* renamed from: a reason: collision with root package name */
        protected Class<C> f2728a;

        public final d a(String str, Class<?>... clsArr) throws C0059a {
            return new d(this.f2728a, str, clsArr);
        }

        public c(Class<C> cls) {
            this.f2728a = cls;
        }
    }

    /* compiled from: Hack */
    public static class d {

        /* renamed from: a reason: collision with root package name */
        protected final Method f2729a;

        public final Object a(Object obj, Object... objArr) throws IllegalArgumentException, InvocationTargetException {
            try {
                return this.f2729a.invoke(obj, objArr);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
                return null;
            }
        }

        d(Class<?> cls, String str, Class<?>[] clsArr) throws C0059a {
            Method method = null;
            if (cls == null) {
                this.f2729a = null;
                return;
            }
            try {
                Method declaredMethod = cls.getDeclaredMethod(str, clsArr);
                try {
                    declaredMethod.setAccessible(true);
                    this.f2729a = declaredMethod;
                } catch (NoSuchMethodException e) {
                    Exception exc = e;
                    method = declaredMethod;
                    e = exc;
                    try {
                        C0059a aVar = new C0059a(e);
                        aVar.a(cls);
                        aVar.a(str);
                        a.b(aVar);
                        this.f2729a = method;
                    } catch (Throwable th) {
                        th = th;
                        this.f2729a = method;
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    method = declaredMethod;
                    this.f2729a = method;
                    throw th;
                }
            } catch (NoSuchMethodException e2) {
                e = e2;
                C0059a aVar2 = new C0059a(e);
                aVar2.a(cls);
                aVar2.a(str);
                a.b(aVar2);
                this.f2729a = method;
            }
        }
    }

    public static <T> c<T> a(ClassLoader classLoader, String str) throws C0059a {
        try {
            return new c<>(classLoader.loadClass(str));
        } catch (Exception e) {
            b(new C0059a(e));
            return new c<>(null);
        }
    }

    /* access modifiers changed from: private */
    public static void b(C0059a aVar) throws C0059a {
        if (f2726a == null || !f2726a.a()) {
            throw aVar;
        }
    }
}
