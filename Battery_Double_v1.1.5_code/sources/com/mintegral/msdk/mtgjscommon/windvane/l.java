package com.mintegral.msdk.mtgjscommon.windvane;

import android.graphics.Bitmap;
import android.text.TextUtils;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import com.mintegral.msdk.base.controller.a;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.mtgjscommon.base.b;

/* compiled from: WindVaneWebViewClient */
public final class l extends b {
    public static boolean b = true;

    /* renamed from: a reason: collision with root package name */
    protected String f2751a = null;
    private int c = 0;
    private c d;

    public final void onPageStarted(WebView webView, String str, Bitmap bitmap) {
        super.onPageStarted(webView, str, bitmap);
        this.f2751a = str;
        if (this.d != null) {
            this.d.a();
        }
    }

    public final WebResourceResponse shouldInterceptRequest(WebView webView, String str) {
        WebResourceResponse a2 = a(str);
        if (a2 == null) {
            return super.shouldInterceptRequest(webView, str);
        }
        StringBuilder sb = new StringBuilder("find WebResourceResponse url is ");
        sb.append(str);
        g.b("WindVaneWebViewClient", sb.toString());
        return a2;
    }

    private static WebResourceResponse a(String str) {
        try {
            if (!TextUtils.isEmpty(str) && j.d(str)) {
                StringBuilder sb = new StringBuilder("is image ");
                sb.append(str);
                g.b("WindVaneWebViewClient", sb.toString());
                Bitmap a2 = com.mintegral.msdk.base.common.c.b.a(a.d().h()).a(str);
                StringBuilder sb2 = new StringBuilder("find image from cache ");
                sb2.append(str);
                g.b("WindVaneWebViewClient", sb2.toString());
                if (a2 != null && !a2.isRecycled()) {
                    return new WebResourceResponse(j.e(str), "utf-8", com.mintegral.msdk.base.common.c.a.a(a2));
                }
            }
        } catch (Throwable unused) {
        }
        return null;
    }
}
