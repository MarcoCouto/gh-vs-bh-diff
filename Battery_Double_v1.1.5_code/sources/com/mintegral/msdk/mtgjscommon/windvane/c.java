package com.mintegral.msdk.mtgjscommon.windvane;

import android.net.http.SslError;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;

/* compiled from: IWebViewListener */
public interface c {
    void a();

    void a(int i);

    void a(WebView webView, int i, String str, String str2);

    void a(WebView webView, SslErrorHandler sslErrorHandler, SslError sslError);

    void a(WebView webView, String str);

    boolean b();

    void c();
}
