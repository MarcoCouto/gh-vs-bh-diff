package com.mintegral.msdk.mtgjscommon.windvane;

import android.content.Context;

/* compiled from: WindVanePlugin */
public abstract class i {
    /* access modifiers changed from: protected */
    public Context mContext;
    protected WindVaneWebView mWebView;

    public void initialize(Context context, WindVaneWebView windVaneWebView) {
        this.mContext = context;
        this.mWebView = windVaneWebView;
    }
}
