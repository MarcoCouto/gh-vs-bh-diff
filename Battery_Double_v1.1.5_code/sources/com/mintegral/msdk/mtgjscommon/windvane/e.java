package com.mintegral.msdk.mtgjscommon.windvane;

import com.tapjoy.TJAdUnitConstants.String;

/* compiled from: MimeTypeEnum */
public enum e {
    JS("js", "application/x-javascript"),
    CSS("css", WebRequest.CONTENT_TYPE_CSS),
    JPG("jpg", "image/jpeg"),
    JPEG("jpep", "image/jpeg"),
    PNG("png", "image/png"),
    WEBP("webp", "image/webp"),
    GIF("gif", "image/gif"),
    HTM("htm", WebRequest.CONTENT_TYPE_HTML),
    HTML(String.HTML, WebRequest.CONTENT_TYPE_HTML);
    
    private String j;
    private String k;

    public static e[] a() {
        return (e[]) l.clone();
    }

    private e(String str, String str2) {
        this.j = str;
        this.k = str2;
    }

    public final String b() {
        return this.j;
    }

    public final String c() {
        return this.k;
    }
}
