package com.mintegral.msdk.mtgjscommon.windvane;

import android.net.Uri;
import android.text.TextUtils;
import com.facebook.share.internal.MessengerShareContentUtility;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

/* compiled from: WindVaneUtil */
public final class j {

    /* renamed from: a reason: collision with root package name */
    private static final String[] f2749a = {"wv_hybrid:", "mraid:", "ssp:", "mvb_hybrid:"};
    private static final Pattern b = Pattern.compile("hybrid://(.+?):(.+?)/(.+?)(\\?(.*?))?");
    private static final Pattern c = Pattern.compile("mraid://(.+?):(.+?)/(.+?)(\\?(.*?))?");
    private static final Pattern d = Pattern.compile("ssp://(.+?):(.+?)/(.+?)(\\?(.*?))?");
    private static final Pattern e = Pattern.compile("mv://(.+?):(.+?)/(.+?)(\\?(.*?))?");
    private static Map<String, String> f = new HashMap();

    static {
        e[] a2;
        for (e eVar : e.a()) {
            f.put(eVar.b(), eVar.c());
        }
    }

    public static boolean a(String str) {
        for (String equals : f2749a) {
            if (equals.equals(str)) {
                return true;
            }
        }
        return false;
    }

    public static Pattern b(String str) {
        if ("wv_hybrid:".equals(str)) {
            return e;
        }
        if ("mraid:".equals(str)) {
            return c;
        }
        if ("ssp:".equals(str)) {
            return d;
        }
        if ("mvb_hybrid:".equals(str)) {
            return e;
        }
        return null;
    }

    public static String c(String str) {
        char[] cArr = {'\'', '\\'};
        StringBuffer stringBuffer = new StringBuffer(1000);
        stringBuffer.setLength(0);
        for (int i = 0; i < str.length(); i++) {
            char charAt = str.charAt(i);
            boolean z = true;
            if (charAt > 255) {
                stringBuffer.append("\\u");
                String upperCase = Integer.toHexString(charAt >>> 8).toUpperCase();
                if (upperCase.length() == 1) {
                    stringBuffer.append("0");
                }
                stringBuffer.append(upperCase);
                String upperCase2 = Integer.toHexString(charAt & 255).toUpperCase();
                if (upperCase2.length() == 1) {
                    stringBuffer.append("0");
                }
                stringBuffer.append(upperCase2);
            } else {
                int i2 = 0;
                while (true) {
                    if (i2 >= 2) {
                        z = false;
                        break;
                    } else if (cArr[i2] == charAt) {
                        StringBuilder sb = new StringBuilder("\\");
                        sb.append(charAt);
                        stringBuffer.append(sb.toString());
                        break;
                    } else {
                        i2++;
                    }
                }
                if (!z) {
                    stringBuffer.append(charAt);
                }
            }
        }
        return new String(stringBuffer);
    }

    public static boolean d(String str) {
        return e(str).startsWith(MessengerShareContentUtility.MEDIA_IMAGE);
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x002c  */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x002f A[RETURN] */
    public static String e(String str) {
        String str2;
        if (!TextUtils.isEmpty(str)) {
            String path = Uri.parse(str).getPath();
            if (path != null) {
                int lastIndexOf = path.lastIndexOf(".");
                if (lastIndexOf != -1) {
                    str2 = path.substring(lastIndexOf + 1);
                    String str3 = (String) f.get(str2);
                    return str3 != null ? "" : str3;
                }
            }
        }
        str2 = "";
        String str32 = (String) f.get(str2);
        if (str32 != null) {
        }
    }
}
