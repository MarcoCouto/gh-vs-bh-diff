package com.mintegral.msdk.mtgjscommon.windvane;

import android.webkit.JsPromptResult;
import android.webkit.WebChromeClient;
import android.webkit.WebView;

/* compiled from: WindVaneWebViewChromeClient */
public final class k extends WebChromeClient {

    /* renamed from: a reason: collision with root package name */
    WindVaneWebView f2750a;
    private c b;

    public k(WindVaneWebView windVaneWebView) {
        this.f2750a = windVaneWebView;
    }

    public final void a(c cVar) {
        this.b = cVar;
    }

    public final boolean onJsPrompt(WebView webView, String str, String str2, String str3, JsPromptResult jsPromptResult) {
        b jsBridge = this.f2750a.getJsBridge();
        if (jsBridge == null || str3 == null || !jsBridge.a(str3)) {
            return false;
        }
        jsBridge.b(str2);
        if (str2.contains("VideoBridge") || str2.contains("RewardJs")) {
            jsBridge.b(str2.replace("VideoBridge", "Interactive").replace("RewardJs", "Interactive"));
        }
        jsPromptResult.confirm("");
        return true;
    }

    public final void onProgressChanged(WebView webView, int i) {
        super.onProgressChanged(webView, i);
        if (this.b != null) {
            this.b.c();
        }
    }
}
