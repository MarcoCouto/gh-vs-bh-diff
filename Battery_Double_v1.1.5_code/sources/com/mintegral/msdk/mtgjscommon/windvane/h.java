package com.mintegral.msdk.mtgjscommon.windvane;

import android.content.Context;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Looper;
import android.os.Message;
import android.text.TextUtils;
import com.mintegral.msdk.mtgjscommon.a.a;
import com.mintegral.msdk.mtgjscommon.a.a.b.C0059a;
import com.mintegral.msdk.mtgjscommon.a.a.d;
import com.mintegral.msdk.mtgjscommon.mraid.c;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/* compiled from: WindVaneJsBridge */
public final class h implements Callback, b {

    /* renamed from: a reason: collision with root package name */
    protected Pattern f2748a;
    protected String b;
    protected final int c = 1;
    protected Context d;
    protected WindVaneWebView e;
    protected Handler f;

    public h(Context context) {
        this.d = context;
        this.f = new Handler(Looper.getMainLooper(), this);
    }

    public final void a(WindVaneWebView windVaneWebView) {
        this.e = windVaneWebView;
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x0054 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0055  */
    public final void b(String str) {
        a aVar;
        if (!TextUtils.isEmpty(str)) {
            Object obj = null;
            if (str != null) {
                aVar = c.a(this.e, str);
                if (aVar != null) {
                    aVar.f2743a = this.e;
                } else {
                    Matcher matcher = this.f2748a.matcher(str);
                    if (matcher.matches()) {
                        aVar = new a();
                        int groupCount = matcher.groupCount();
                        if (groupCount >= 5) {
                            aVar.f = matcher.group(5);
                        }
                        if (groupCount >= 3) {
                            aVar.d = matcher.group(1);
                            aVar.g = matcher.group(2);
                            aVar.e = matcher.group(3);
                            aVar.f2743a = this.e;
                        }
                    }
                }
                if (aVar == null) {
                    if (aVar.f2743a != null) {
                        obj = aVar.f2743a.getJsObject(aVar.d);
                    }
                    if (obj != null) {
                        try {
                            d a2 = a.a(this.d.getClassLoader(), obj.getClass().getName()).a(aVar.e, Object.class, String.class);
                            if (obj != null && (obj instanceof i)) {
                                aVar.b = obj;
                                aVar.c = a2;
                                aVar.b = obj;
                                Message obtain = Message.obtain();
                                obtain.what = 1;
                                obtain.obj = aVar;
                                this.f.sendMessage(obtain);
                            }
                            return;
                        } catch (C0059a e2) {
                            e2.printStackTrace();
                            return;
                        } catch (Exception e3) {
                            e3.printStackTrace();
                            return;
                        }
                    } else {
                        return;
                    }
                } else {
                    return;
                }
            }
            aVar = null;
            if (aVar == null) {
            }
        }
    }

    public final boolean handleMessage(Message message) {
        a aVar = (a) message.obj;
        if (aVar == null) {
            return false;
        }
        try {
            if (message.what != 1) {
                return false;
            }
            Object obj = aVar.b;
            d dVar = aVar.c;
            Object[] objArr = new Object[2];
            objArr[0] = aVar;
            objArr[1] = TextUtils.isEmpty(aVar.f) ? "{}" : aVar.f;
            dVar.a(obj, objArr);
            return true;
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public final boolean a(String str) {
        if (!j.a(str)) {
            return false;
        }
        this.f2748a = j.b(str);
        this.b = str;
        return true;
    }
}
