package com.mintegral.msdk.mtgjscommon.windvane;

import android.text.TextUtils;

/* compiled from: MVCallJs */
public final class d {

    /* renamed from: a reason: collision with root package name */
    private static d f2744a = new d();

    private d() {
    }

    public static d a() {
        return f2744a;
    }

    public static void a(Object obj, String str, String str2) {
        String str3;
        if (obj instanceof a) {
            a aVar = (a) obj;
            if (TextUtils.isEmpty(str2)) {
                str3 = String.format("javascript:window.MvBridge.fireEvent('%s', '');", new Object[]{str});
            } else {
                str3 = String.format("javascript:window.MvBridge.fireEvent('%s','%s');", new Object[]{str, j.c(str2)});
            }
            if (aVar.f2743a != null) {
                try {
                    aVar.f2743a.loadUrl(str3);
                } catch (Exception e) {
                    e.printStackTrace();
                } catch (Throwable th) {
                    th.printStackTrace();
                }
            }
        }
    }

    public static void a(Object obj, String str) {
        String str2;
        if (obj instanceof a) {
            a aVar = (a) obj;
            if (TextUtils.isEmpty(str)) {
                str2 = String.format("javascript:window.OfferWall.onSuccess(%s,'');", new Object[]{aVar.g});
            } else {
                str2 = String.format("javascript:window.OfferWall.onSuccess(%s,'%s');", new Object[]{aVar.g, j.c(str)});
            }
            if (aVar.f2743a != null) {
                try {
                    aVar.f2743a.loadUrl(str2);
                } catch (Exception e) {
                    e.printStackTrace();
                } catch (Throwable th) {
                    th.printStackTrace();
                }
            }
        }
    }

    public static void b(Object obj, String str) {
        if (obj instanceof a) {
            a aVar = (a) obj;
            if (TextUtils.isEmpty(str)) {
                String.format("javascript:window.MvBridge.onFailure(%s,'');", new Object[]{aVar.g});
            } else {
                str = j.c(str);
            }
            String format = String.format("javascript:window.MvBridge.onFailure(%s,'%s');", new Object[]{aVar.g, str});
            if (aVar.f2743a != null) {
                try {
                    aVar.f2743a.loadUrl(format);
                } catch (Exception e) {
                    e.printStackTrace();
                } catch (Throwable th) {
                    th.printStackTrace();
                }
            }
        }
    }
}
