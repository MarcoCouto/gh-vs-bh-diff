package com.mintegral.msdk.mtgjscommon.authority.jscommon;

import android.text.TextUtils;
import android.util.Base64;
import com.mintegral.msdk.base.controller.authoritycontroller.a;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.mtgjscommon.authority.activity.MTGAuthorityActivity;
import com.mintegral.msdk.mtgjscommon.windvane.i;

public class PrivateAuthorityJSBridge extends i {
    private static final String TAG = "com.mintegral.msdk.mtgjscommon.authority.jscommon.PrivateAuthorityJSBridge";

    public void getPrivateAuthorityStatus(Object obj, String str) {
        a.a();
        String c = a.c();
        String str2 = TAG;
        StringBuilder sb = new StringBuilder("GET authorityStatusString:");
        sb.append(c);
        g.a(str2, sb.toString());
        com.mintegral.msdk.mtgjscommon.windvane.g.a();
        com.mintegral.msdk.mtgjscommon.windvane.g.a(obj, TextUtils.isEmpty(c) ? "" : Base64.encodeToString(c.getBytes(), 2));
    }

    public void setPrivateAuthorityStatus(Object obj, String str) {
        String str2 = TAG;
        StringBuilder sb = new StringBuilder("SET authorityStatusString:");
        sb.append(str);
        g.a(str2, sb.toString());
        if (TextUtils.isEmpty(str)) {
            finishActivity(obj);
            return;
        }
        try {
            a.a().b(str);
        } catch (Throwable th) {
            th.printStackTrace();
        } finally {
            finishActivity(obj);
        }
    }

    private void finishActivity(Object obj) {
        String str = TAG;
        StringBuilder sb = new StringBuilder("close activity");
        sb.append(this.mContext);
        g.a(str, sb.toString());
        if (this.mContext instanceof MTGAuthorityActivity) {
            ((MTGAuthorityActivity) this.mContext).finish();
        }
    }
}
