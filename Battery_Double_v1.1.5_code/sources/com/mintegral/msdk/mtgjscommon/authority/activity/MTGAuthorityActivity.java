package com.mintegral.msdk.mtgjscommon.authority.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.DialogInterface.OnKeyListener;
import android.graphics.drawable.ColorDrawable;
import android.net.http.SslError;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.b.a;
import com.mintegral.msdk.b.b;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.base.utils.p;
import com.mintegral.msdk.mtgjscommon.windvane.WindVaneWebView;
import com.mintegral.msdk.mtgjscommon.windvane.c;
import java.util.regex.Pattern;

public class MTGAuthorityActivity extends Activity {
    public static final long TIMEOUT = 10000;
    /* access modifiers changed from: private */
    public static String f = "MTGAuthorityActivity";

    /* renamed from: a reason: collision with root package name */
    String f2730a = "";
    LinearLayout b;
    MTGAuthorityCustomView c;
    LayoutParams d;
    Handler e;
    /* access modifiers changed from: private */
    public WindVaneWebView g = null;
    private AlertDialog h;
    /* access modifiers changed from: private */
    public Runnable i = new Runnable() {
        public final void run() {
            MTGAuthorityActivity.this.dismissLoadingDialog();
            MTGAuthorityActivity.this.b.removeView(MTGAuthorityActivity.this.g);
            if (MTGAuthorityActivity.this.b.indexOfChild(MTGAuthorityActivity.this.c) == -1) {
                MTGAuthorityActivity.this.b.addView(MTGAuthorityActivity.this.c, MTGAuthorityActivity.this.d);
            }
        }
    };

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        return i2 == 4;
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        g.a(f, "MTGAuthorityActivity  onCreate");
        showLoadingDialog();
        try {
            b.a();
            a b2 = b.b(com.mintegral.msdk.base.controller.a.d().j());
            if (b2 == null) {
                b.a();
                b2 = b.b();
            }
            this.f2730a = b2.u();
            this.e = new Handler();
            if (TextUtils.isEmpty(this.f2730a) || !isHttpUrl(this.f2730a)) {
                dismissLoadingDialog();
            } else {
                String str = this.f2730a;
                this.g = new WindVaneWebView(this);
                this.e.postDelayed(this.i, 10000);
                this.g.setWebViewListener(new c() {
                    public final void a() {
                    }

                    public final void a(int i) {
                    }

                    public final boolean b() {
                        return false;
                    }

                    public final void c() {
                    }

                    public final void a(WebView webView, int i, String str, String str2) {
                        MTGAuthorityActivity.this.dismissLoadingDialog();
                        MTGAuthorityActivity.this.b.removeView(MTGAuthorityActivity.this.g);
                        if (MTGAuthorityActivity.this.b.indexOfChild(MTGAuthorityActivity.this.c) == -1) {
                            MTGAuthorityActivity.this.b.addView(MTGAuthorityActivity.this.c, MTGAuthorityActivity.this.d);
                        }
                        com.mintegral.msdk.base.controller.authoritycontroller.a.a().b.onShowPopWindowStatusFaile(str);
                    }

                    public final void a(WebView webView, SslErrorHandler sslErrorHandler, SslError sslError) {
                        MTGAuthorityActivity.this.dismissLoadingDialog();
                        MTGAuthorityActivity.this.b.removeView(MTGAuthorityActivity.this.g);
                        if (MTGAuthorityActivity.this.b.indexOfChild(MTGAuthorityActivity.this.c) == -1) {
                            MTGAuthorityActivity.this.b.addView(MTGAuthorityActivity.this.c, MTGAuthorityActivity.this.d);
                        }
                        com.mintegral.msdk.base.controller.authoritycontroller.a.a().b.onShowPopWindowStatusFaile(MIntegralConstans.AUTHORITY_APP_LOAD_FAILED);
                    }

                    public final void a(WebView webView, String str) {
                        g.a(MTGAuthorityActivity.f, "onPageFinished");
                        MTGAuthorityActivity.this.dismissLoadingDialog();
                        MTGAuthorityActivity.this.webviewshow(MTGAuthorityActivity.this.g);
                        MTGAuthorityActivity.this.e.removeCallbacks(MTGAuthorityActivity.this.i);
                        com.mintegral.msdk.base.controller.authoritycontroller.a.a().b.onShowPopWindowStatusSucessful();
                    }
                });
                this.g.loadUrl(str);
                this.g = this.g;
            }
            this.b = new LinearLayout(this);
            this.d = new LayoutParams(-1, -1);
            this.c = new MTGAuthorityCustomView(this);
            if (this.g == null) {
                this.b.addView(this.c, this.d);
            } else {
                this.b.addView(this.g, this.d);
            }
            setContentView(this.b);
        } catch (Throwable unused) {
            finish();
        }
    }

    public void webviewshow(WindVaneWebView windVaneWebView) {
        try {
            g.a(f, "webviewshow");
            com.mintegral.msdk.mtgjscommon.windvane.g.a();
            com.mintegral.msdk.mtgjscommon.windvane.g.a(windVaneWebView, "webviewshow", "");
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public static boolean isHttpUrl(String str) {
        boolean matches = Pattern.compile("(((https|http)?://)?([a-z0-9]+[.])|(www.))\\w+[.|\\/]([a-z0-9]{0,})?[[.]([a-z0-9]{0,})]+((/[\\S&&[^,;一-龥]]+)+)?([.][a-z0-9]{0,}+|/?)".trim()).matcher(str.trim()).matches();
        if (matches) {
            return true;
        }
        return matches;
    }

    public void showLoadingDialog() {
        try {
            if (this.h == null) {
                this.h = new Builder(this).create();
            }
            this.h.getWindow().setBackgroundDrawable(new ColorDrawable());
            this.h.setCancelable(false);
            this.h.setOnKeyListener(new OnKeyListener() {
                public final boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
                    return i == 84 || i == 4;
                }
            });
            this.h.show();
            View inflate = LayoutInflater.from(this).inflate(p.a(this, "loading_alert", TtmlNode.TAG_LAYOUT), null);
            if (inflate != null) {
                this.h.setContentView(inflate);
                this.h.setCanceledOnTouchOutside(false);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public void dismissLoadingDialog() {
        if (this.h != null && this.h.isShowing()) {
            this.h.dismiss();
        }
    }
}
