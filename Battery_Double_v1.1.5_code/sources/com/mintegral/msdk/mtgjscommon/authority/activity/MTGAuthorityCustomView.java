package com.mintegral.msdk.mtgjscommon.authority.activity;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.base.controller.authoritycontroller.AuthorityInfoBean;
import com.mintegral.msdk.base.controller.authoritycontroller.a;
import com.mintegral.msdk.base.utils.p;
import org.json.JSONException;
import org.json.JSONObject;

public class MTGAuthorityCustomView extends RelativeLayout {
    public CheckBox checkBox;
    public Button okButton;
    public WebView webViewContent;

    public MTGAuthorityCustomView(final Context context) {
        super(context);
        View inflate = LayoutInflater.from(getContext()).inflate(p.a(getContext(), "mintegral_jscommon_authoritylayout", TtmlNode.TAG_LAYOUT), this);
        if (inflate != null) {
            this.webViewContent = (WebView) inflate.findViewById(p.a(getContext(), "mintegral_jscommon_webcontent", "id"));
            this.checkBox = (CheckBox) inflate.findViewById(p.a(getContext(), "mintegral_jscommon_checkBox", "id"));
            this.okButton = (Button) inflate.findViewById(p.a(getContext(), "mintegral_jscommon_okbutton", "id"));
            this.webViewContent.getSettings().setJavaScriptEnabled(true);
            this.webViewContent.getSettings().setDefaultTextEncodingName("utf-8");
            this.webViewContent.loadUrl(MIntegralConstans.AUTHORITY_DEFAULTLOCAL_INFO_URL);
            inflate.setLayoutParams(new LayoutParams(-1, -1));
            this.okButton.setOnClickListener(new OnClickListener() {
                public final void onClick(View view) {
                    a.a().b(MTGAuthorityCustomView.this.authorityInfoToJsonString(MTGAuthorityCustomView.this.checkBox.isChecked() ? 1 : 0));
                    if (context instanceof Activity) {
                        ((Activity) context).finish();
                    }
                }
            });
            this.checkBox.setChecked(isSwitch(a.a().b()));
        }
    }

    public boolean isSwitch(AuthorityInfoBean authorityInfoBean) {
        int authDeviceIdStatus = authorityInfoBean.getAuthDeviceIdStatus();
        int authAndroidIdStatus = authorityInfoBean.getAuthAndroidIdStatus();
        int authGpsStatus = authorityInfoBean.getAuthGpsStatus();
        int authImeiAndMacStatus = authorityInfoBean.getAuthImeiAndMacStatus();
        int authGenDataStatus = authorityInfoBean.getAuthGenDataStatus();
        return authorityInfoBean.getAuthSerialIdStatus() == 1 || authAndroidIdStatus == 1 || authorityInfoBean.getAuthAppDownloadStatus() == 1 || authorityInfoBean.getAuthAppListStatus() == 1 || authorityInfoBean.getAuthAppProgressStatus() == 1 || authDeviceIdStatus == 1 || authGenDataStatus == 1 || authGpsStatus == 1 || authImeiAndMacStatus == 1;
    }

    public String authorityInfoToJsonString(int i) {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put(MIntegralConstans.AUTHORITY_GENERAL_DATA, String.valueOf(i));
            jSONObject.put(MIntegralConstans.AUTHORITY_DEVICE_ID, String.valueOf(i));
            jSONObject.put(MIntegralConstans.AUTHORITY_GPS, String.valueOf(i));
            jSONObject.put(MIntegralConstans.AUTHORITYIMEIMAC, String.valueOf(i));
            jSONObject.put(MIntegralConstans.AUTHORITY_ANDROID_ID, String.valueOf(i));
            jSONObject.put(MIntegralConstans.AUTHORITY_APPLIST, String.valueOf(i));
            jSONObject.put(MIntegralConstans.AUTHORITY_APP_DOWNLOAD, String.valueOf(i));
            jSONObject.put(MIntegralConstans.AUTHORITY_APP_PROGRESS, String.valueOf(i));
            jSONObject.put(MIntegralConstans.AUTHORITY_SERIAL_ID, String.valueOf(i));
            return jSONObject.toString();
        } catch (JSONException e) {
            e.printStackTrace();
            return "";
        }
    }
}
