package com.mintegral.msdk.mtgjscommon.mraid;

import android.net.Uri;
import android.text.TextUtils;
import android.webkit.WebView;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.mtgjscommon.windvane.WindVaneWebView;
import com.mintegral.msdk.mtgjscommon.windvane.a;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import org.json.JSONObject;

/* compiled from: MraidUriUtil */
public final class c {

    /* renamed from: a reason: collision with root package name */
    private static volatile ConcurrentHashMap<String, String> f2740a = new ConcurrentHashMap<>();
    private static ArrayList<String> b = new ArrayList<>();

    public static a a(WindVaneWebView windVaneWebView, String str) {
        Set<String> set;
        Uri parse = Uri.parse(str);
        String scheme = parse.getScheme();
        if (TextUtils.isEmpty(scheme) || !scheme.contains(CampaignEx.JSON_KEY_MRAID)) {
            return null;
        }
        a aVar = new a();
        aVar.d = "MraidJSBridge";
        aVar.e = parse.getHost();
        int i = 0;
        if (b.size() == 0) {
            for (Method name : b.class.getDeclaredMethods()) {
                b.add(name.getName());
            }
        }
        if (b.contains(aVar.e) || windVaneWebView == null) {
            String encodedQuery = parse.getEncodedQuery();
            if (encodedQuery == null) {
                set = Collections.emptySet();
            } else {
                LinkedHashSet linkedHashSet = new LinkedHashSet();
                do {
                    int indexOf = encodedQuery.indexOf(38, i);
                    if (indexOf == -1) {
                        indexOf = encodedQuery.length();
                    }
                    int indexOf2 = encodedQuery.indexOf(61, i);
                    if (indexOf2 > indexOf || indexOf2 == -1) {
                        indexOf2 = indexOf;
                    }
                    linkedHashSet.add(Uri.decode(encodedQuery.substring(i, indexOf2)));
                    i = indexOf + 1;
                } while (i < encodedQuery.length());
                set = Collections.unmodifiableSet(linkedHashSet);
            }
            try {
                JSONObject jSONObject = new JSONObject();
                for (String str2 : set) {
                    jSONObject.put(str2, parse.getQueryParameter(str2));
                }
                aVar.f = jSONObject.toString();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return aVar;
        }
        String campaignId = windVaneWebView.getCampaignId();
        String str3 = aVar.e;
        if (!TextUtils.isEmpty(campaignId)) {
            if (f2740a.containsKey(campaignId)) {
                String str4 = (String) f2740a.get(campaignId);
                if (!TextUtils.isEmpty(str3) && !str4.contains(str3)) {
                    if (str4.length() > 0) {
                        str4 = str4.concat(",");
                    }
                    f2740a.put(campaignId, str4.concat(str3));
                }
            } else {
                f2740a.put(campaignId, str3);
            }
        }
        C0060a.f2739a;
        a.a((WebView) windVaneWebView, aVar.e);
        C0060a.f2739a;
        a.a((WebView) windVaneWebView, aVar.e, "Specified command is not implemented");
        return null;
    }

    public static String a(String str) {
        if (f2740a.containsKey(str)) {
            return (String) f2740a.get(str);
        }
        return null;
    }

    public static void b(String str) {
        if (!TextUtils.isEmpty(str)) {
            f2740a.remove(str);
        }
    }
}
