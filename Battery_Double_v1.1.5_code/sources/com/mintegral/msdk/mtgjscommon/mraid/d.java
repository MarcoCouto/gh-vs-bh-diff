package com.mintegral.msdk.mtgjscommon.mraid;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import com.github.mikephil.charting.utils.Utils;
import com.google.android.exoplayer2.util.MimeTypes;
import java.lang.ref.WeakReference;

/* compiled from: MraidVolumeChangeReceiver */
public final class d {

    /* renamed from: a reason: collision with root package name */
    public static double f2741a = -1.0d;
    private Context b;
    private AudioManager c;
    private boolean d = false;
    private b e;
    private a f;

    /* compiled from: MraidVolumeChangeReceiver */
    private static class a extends BroadcastReceiver {

        /* renamed from: a reason: collision with root package name */
        private WeakReference<d> f2742a;

        public a(d dVar) {
            this.f2742a = new WeakReference<>(dVar);
        }

        public final void onReceive(Context context, Intent intent) {
            if ("android.media.VOLUME_CHANGED_ACTION".equals(intent.getAction()) && intent.getIntExtra("android.media.EXTRA_VOLUME_STREAM_TYPE", -1) == 3) {
                d dVar = (d) this.f2742a.get();
                if (dVar != null) {
                    b b = dVar.b();
                    if (b != null) {
                        double a2 = dVar.a();
                        if (a2 >= Utils.DOUBLE_EPSILON) {
                            b.a(a2);
                        }
                    }
                }
            }
        }
    }

    /* compiled from: MraidVolumeChangeReceiver */
    public interface b {
        void a(double d);
    }

    public d(Context context) {
        this.b = context;
        this.c = (AudioManager) context.getApplicationContext().getSystemService(MimeTypes.BASE_TYPE_AUDIO);
    }

    public final double a() {
        int i = -1;
        int streamMaxVolume = this.c != null ? this.c.getStreamMaxVolume(3) : -1;
        if (this.c != null) {
            i = this.c.getStreamVolume(3);
        }
        double d2 = (double) i;
        Double.isNaN(d2);
        double d3 = d2 * 100.0d;
        double d4 = (double) streamMaxVolume;
        Double.isNaN(d4);
        double d5 = d3 / d4;
        f2741a = d5;
        return d5;
    }

    public final b b() {
        return this.e;
    }

    public final void a(b bVar) {
        this.e = bVar;
    }

    public final void c() {
        if (this.b != null) {
            this.f = new a(this);
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction("android.media.VOLUME_CHANGED_ACTION");
            this.b.registerReceiver(this.f, intentFilter);
            this.d = true;
        }
    }

    public final void d() {
        if (this.d && this.b != null) {
            try {
                this.b.unregisterReceiver(this.f);
                this.e = null;
                this.d = false;
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }
}
