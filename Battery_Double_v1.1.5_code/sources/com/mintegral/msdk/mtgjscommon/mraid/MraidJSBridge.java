package com.mintegral.msdk.mtgjscommon.mraid;

import android.content.Context;
import android.text.TextUtils;
import android.webkit.WebView;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.mtgjscommon.windvane.WindVaneWebView;
import com.mintegral.msdk.mtgjscommon.windvane.a;
import com.mintegral.msdk.mtgjscommon.windvane.i;
import org.json.JSONObject;

public class MraidJSBridge extends i {

    /* renamed from: a reason: collision with root package name */
    private b f2738a;

    public void initialize(Context context, WindVaneWebView windVaneWebView) {
        super.initialize(context, windVaneWebView);
        try {
            if (context instanceof b) {
                this.f2738a = (b) context;
                return;
            }
            if (windVaneWebView.getObject() != null && (windVaneWebView.getObject() instanceof b)) {
                this.f2738a = (b) windVaneWebView.getObject();
            }
        } catch (Exception e) {
            if (MIntegralConstans.DEBUG) {
                e.printStackTrace();
            }
        }
    }

    public void open(Object obj, String str) {
        if (obj instanceof a) {
            a aVar = (a) obj;
            C0060a.f2739a;
            a.a((WebView) aVar.f2743a, "open");
        }
        try {
            String optString = new JSONObject(str).optString("url");
            StringBuilder sb = new StringBuilder("MRAID Open ");
            sb.append(optString);
            g.d("MraidJSBridge", sb.toString());
            if (this.f2738a != null && !TextUtils.isEmpty(optString)) {
                this.f2738a.open(optString);
            }
        } catch (Throwable th) {
            g.c("MraidJSBridge", "MRAID Open", th);
        }
    }

    public void close(Object obj, String str) {
        if (obj instanceof a) {
            a aVar = (a) obj;
            C0060a.f2739a;
            a.a((WebView) aVar.f2743a, "close");
        }
        try {
            g.d("MraidJSBridge", "MRAID close");
            if (this.f2738a != null) {
                this.f2738a.close();
            }
        } catch (Throwable th) {
            g.c("MraidJSBridge", "MRAID close", th);
        }
    }

    public void unload(Object obj, String str) {
        if (obj instanceof a) {
            a aVar = (a) obj;
            C0060a.f2739a;
            a.a((WebView) aVar.f2743a, "unload");
        }
        try {
            g.d("MraidJSBridge", "MRAID unload");
            if (this.f2738a != null) {
                this.f2738a.unload();
            }
        } catch (Throwable th) {
            g.c("MraidJSBridge", "MRAID unload", th);
        }
    }

    public void useCustomClose(Object obj, String str) {
        if (obj instanceof a) {
            a aVar = (a) obj;
            C0060a.f2739a;
            a.a((WebView) aVar.f2743a, "useCustomClose");
        }
        try {
            String optString = new JSONObject(str).optString("shouldUseCustomClose");
            StringBuilder sb = new StringBuilder("MRAID useCustomClose ");
            sb.append(optString);
            g.d("MraidJSBridge", sb.toString());
            if (!TextUtils.isEmpty(optString) && this.f2738a != null) {
                this.f2738a.useCustomClose(optString.toLowerCase().equals("true"));
            }
        } catch (Throwable th) {
            g.c("MraidJSBridge", "MRAID useCustomClose", th);
        }
    }

    public void setOrientationProperties(Object obj, String str) {
        if (obj instanceof a) {
            a aVar = (a) obj;
            C0060a.f2739a;
            a.a((WebView) aVar.f2743a, "setOrientationProperties");
        }
        try {
            JSONObject jSONObject = new JSONObject(str);
            String optString = jSONObject.optString("allowOrientationChange");
            String optString2 = jSONObject.optString("forceOrientation");
            g.d("MraidJSBridge", "MRAID setOrientationProperties");
            if (!TextUtils.isEmpty(optString) && !TextUtils.isEmpty(optString2) && this.f2738a != null) {
                optString.toLowerCase().equals("true");
                String lowerCase = optString2.toLowerCase();
                int hashCode = lowerCase.hashCode();
                if (hashCode != 729267099) {
                    if (hashCode == 1430647483) {
                        lowerCase.equals("landscape");
                    }
                } else if (lowerCase.equals("portrait")) {
                }
            }
        } catch (Throwable th) {
            g.c("MraidJSBridge", "MRAID setOrientationProperties", th);
        }
    }
}
