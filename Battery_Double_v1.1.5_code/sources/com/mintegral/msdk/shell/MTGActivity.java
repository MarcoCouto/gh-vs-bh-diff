package com.mintegral.msdk.shell;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.Fragment.InstantiationException;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.ViewGroup.LayoutParams;
import android.widget.FrameLayout;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.MIntegralSDK.PLUGIN_LOAD_STATUS;
import com.mintegral.msdk.appwall.g.a;
import com.mintegral.msdk.appwall.service.WallService;
import com.mintegral.msdk.base.common.c.b;
import com.mintegral.msdk.base.fragment.BaseFragment;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.base.utils.i;
import com.mintegral.msdk.out.MIntegralSDKFactory;
import io.fabric.sdk.android.services.events.EventsFilesManager;
import java.util.HashMap;
import java.util.Map;

public class MTGActivity extends FragmentActivity {
    public static Object extra;

    /* renamed from: a reason: collision with root package name */
    Map<String, Object> f2951a;
    BaseFragment b;
    a c;
    private com.mintegral.msdk.system.a d;
    private boolean e = false;
    private Intent f;
    private FrameLayout g;
    private String h;
    public boolean mIsReport = false;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        if (MIntegralConstans.APPWALL_IS_SHOW_WHEN_SCREEN_LOCK) {
            getWindow().addFlags(4718592);
        }
        requestWindowFeature(1);
        try {
            super.onCreate(bundle);
            this.h = getIntent().getStringExtra(MIntegralConstans.PROPERTIES_UNIT_ID);
            initWall(bundle, this.h);
        } catch (InstantiationException unused) {
            finish();
        }
    }

    public void initWall(Bundle bundle, String str) {
        i.a((Context) getApplication());
        String stringExtra = getIntent().getStringExtra(MIntegralConstans.PROPERTIES_UNIT_ID);
        String str2 = "wall";
        if (getIntent().hasExtra("type")) {
            str2 = getIntent().getStringExtra("type");
        }
        String str3 = null;
        if (getIntent().hasExtra(NotificationCompat.CATEGORY_MESSAGE)) {
            str3 = getIntent().getStringExtra(NotificationCompat.CATEGORY_MESSAGE);
        }
        this.f2951a = new HashMap();
        this.f2951a.put("type", str2);
        this.f2951a.put(NotificationCompat.CATEGORY_MESSAGE, str3);
        this.f2951a.put(MIntegralConstans.PROPERTIES_UNIT_ID, stringExtra);
        try {
            this.g = new FrameLayout(this);
            this.g.setLayoutParams(new LayoutParams(-1, -1));
            this.g.setId(16908300);
            setContentView(this.g);
            a(getIntent());
            this.d = MIntegralSDKFactory.getMIntegralSDK();
            if (this.d.getStatus() != PLUGIN_LOAD_STATUS.COMPLETED) {
                MIntegralSDKFactory.getMIntegralSDK().a(getApplication());
                finish();
            }
            this.b = WallService.getFragment(this.f2951a);
            if (this.b == null) {
                finish();
            }
            Bundle bundle2 = new Bundle();
            if (getIntent().hasExtra(MIntegralConstans.PROPERTIES_WALL_TITLE_BACKGROUND)) {
                bundle2.putParcelable(MIntegralConstans.PROPERTIES_WALL_TITLE_BACKGROUND, (Bitmap) getIntent().getParcelableExtra(MIntegralConstans.PROPERTIES_WALL_TITLE_BACKGROUND));
            }
            if (getIntent().hasExtra(MIntegralConstans.PROPERTIES_WALL_TITLE_LOGO)) {
                bundle2.putParcelable(MIntegralConstans.PROPERTIES_WALL_TITLE_LOGO, (Bitmap) getIntent().getParcelableExtra(MIntegralConstans.PROPERTIES_WALL_TITLE_LOGO));
            }
            if (getIntent().hasExtra(MIntegralConstans.PROPERTIES_WALL_TITLE_LOGO_ID)) {
                int intExtra = getIntent().getIntExtra(MIntegralConstans.PROPERTIES_WALL_TITLE_LOGO_ID, 0);
                if (intExtra > 0) {
                    bundle2.putInt(MIntegralConstans.PROPERTIES_WALL_TITLE_LOGO_ID, intExtra);
                }
            }
            if (getIntent().hasExtra(MIntegralConstans.PROPERTIES_WALL_TITLE_BACKGROUND_ID)) {
                int intExtra2 = getIntent().getIntExtra(MIntegralConstans.PROPERTIES_WALL_TITLE_BACKGROUND_ID, 0);
                if (intExtra2 > 0) {
                    bundle2.putInt(MIntegralConstans.PROPERTIES_WALL_TITLE_BACKGROUND_ID, intExtra2);
                }
            }
            if (getIntent().hasExtra(MIntegralConstans.PROPERTIES_WALL_MAIN_BACKGROUND_ID)) {
                int intExtra3 = getIntent().getIntExtra(MIntegralConstans.PROPERTIES_WALL_MAIN_BACKGROUND_ID, 0);
                if (intExtra3 > 0) {
                    bundle2.putInt(MIntegralConstans.PROPERTIES_WALL_MAIN_BACKGROUND_ID, intExtra3);
                }
            }
            if (getIntent().hasExtra(MIntegralConstans.PROPERTIES_WALL_TAB_BACKGROUND_ID)) {
                int intExtra4 = getIntent().getIntExtra(MIntegralConstans.PROPERTIES_WALL_TAB_BACKGROUND_ID, 0);
                if (intExtra4 > 0) {
                    bundle2.putInt(MIntegralConstans.PROPERTIES_WALL_TAB_BACKGROUND_ID, intExtra4);
                }
            }
            if (getIntent().hasExtra(MIntegralConstans.PROPERTIES_WALL_TAB_INDICATE_LINE_BACKGROUND_ID)) {
                int intExtra5 = getIntent().getIntExtra(MIntegralConstans.PROPERTIES_WALL_TAB_INDICATE_LINE_BACKGROUND_ID, 0);
                if (intExtra5 > 0) {
                    bundle2.putInt(MIntegralConstans.PROPERTIES_WALL_TAB_INDICATE_LINE_BACKGROUND_ID, intExtra5);
                }
            }
            if (getIntent().hasExtra(MIntegralConstans.PROPERTIES_WALL_BUTTON_BACKGROUND_ID)) {
                int intExtra6 = getIntent().getIntExtra(MIntegralConstans.PROPERTIES_WALL_BUTTON_BACKGROUND_ID, 0);
                if (intExtra6 > 0) {
                    bundle2.putInt(MIntegralConstans.PROPERTIES_WALL_BUTTON_BACKGROUND_ID, intExtra6);
                }
            }
            if (getIntent().hasExtra(MIntegralConstans.PROPERTIES_WALL_LOAD_ID)) {
                int intExtra7 = getIntent().getIntExtra(MIntegralConstans.PROPERTIES_WALL_LOAD_ID, 0);
                if (intExtra7 > 0) {
                    bundle2.putInt(MIntegralConstans.PROPERTIES_WALL_LOAD_ID, intExtra7);
                }
            }
            if (getIntent().hasExtra(MIntegralConstans.PROPERTIES_WALL_TITLE_BACKGROUND_COLOR)) {
                int intExtra8 = getIntent().getIntExtra(MIntegralConstans.PROPERTIES_WALL_TITLE_BACKGROUND_COLOR, 0);
                if (intExtra8 > 0) {
                    bundle2.putInt(MIntegralConstans.PROPERTIES_WALL_TITLE_BACKGROUND_COLOR, intExtra8);
                }
            }
            if (getIntent().hasExtra(MIntegralConstans.PROPERTIES_WALL_FACEBOOK_PLACEMENT_ID)) {
                String stringExtra2 = getIntent().getStringExtra(MIntegralConstans.PROPERTIES_WALL_FACEBOOK_PLACEMENT_ID);
                if (!TextUtils.isEmpty(stringExtra2)) {
                    bundle2.putString(MIntegralConstans.PROPERTIES_WALL_FACEBOOK_PLACEMENT_ID, stringExtra2);
                }
            }
            if (getIntent().hasExtra(MIntegralConstans.PROPERTIES_WALL_TAB_SELECTED_TEXT_COLOR)) {
                String stringExtra3 = getIntent().getStringExtra(MIntegralConstans.PROPERTIES_WALL_TAB_SELECTED_TEXT_COLOR);
                if (!TextUtils.isEmpty(stringExtra3)) {
                    bundle2.putString(MIntegralConstans.PROPERTIES_WALL_TAB_SELECTED_TEXT_COLOR, stringExtra3);
                }
            }
            if (getIntent().hasExtra(MIntegralConstans.PROPERTIES_WALL_TAB_UNSELECTED_TEXT_COLOR)) {
                String stringExtra4 = getIntent().getStringExtra(MIntegralConstans.PROPERTIES_WALL_TAB_UNSELECTED_TEXT_COLOR);
                if (!TextUtils.isEmpty(stringExtra4)) {
                    bundle2.putString(MIntegralConstans.PROPERTIES_WALL_TAB_UNSELECTED_TEXT_COLOR, stringExtra4);
                }
            }
            if (getIntent().hasExtra(MIntegralConstans.PROPERTIES_WALL_TITLE_LOGO_TEXT)) {
                String stringExtra5 = getIntent().getStringExtra(MIntegralConstans.PROPERTIES_WALL_TITLE_LOGO_TEXT);
                if (!TextUtils.isEmpty(stringExtra5)) {
                    bundle2.putCharSequence(MIntegralConstans.PROPERTIES_WALL_TITLE_LOGO_TEXT, stringExtra5);
                }
            }
            if (getIntent().hasExtra(MIntegralConstans.PROPERTIES_WALL_TITLE_LOGO_TEXT_COLOR)) {
                int intExtra9 = getIntent().getIntExtra(MIntegralConstans.PROPERTIES_WALL_TITLE_LOGO_TEXT_COLOR, 0);
                if (intExtra9 > 0) {
                    bundle2.putInt(MIntegralConstans.PROPERTIES_WALL_TITLE_LOGO_TEXT_COLOR, intExtra9);
                }
            }
            if (getIntent().hasExtra(MIntegralConstans.PROPERTIES_WALL_TITLE_LOGO_TEXT_SIZE)) {
                int intExtra10 = getIntent().getIntExtra(MIntegralConstans.PROPERTIES_WALL_TITLE_LOGO_TEXT_SIZE, 0);
                if (intExtra10 > 0) {
                    bundle2.putInt(MIntegralConstans.PROPERTIES_WALL_TITLE_LOGO_TEXT_SIZE, intExtra10);
                }
            }
            if (getIntent().hasExtra(MIntegralConstans.PROPERTIES_WALL_TITLE_LOGO_TEXT_TYPEFACE)) {
                int intExtra11 = getIntent().getIntExtra(MIntegralConstans.PROPERTIES_WALL_TITLE_LOGO_TEXT_TYPEFACE, 0);
                if (intExtra11 > 0) {
                    bundle2.putInt(MIntegralConstans.PROPERTIES_WALL_TITLE_LOGO_TEXT_TYPEFACE, intExtra11);
                }
            }
            if (getIntent().hasExtra(MIntegralConstans.CANCEL_ADMOB_AUTO_DOWNLOAD_IMAGE)) {
                bundle2.putBoolean(MIntegralConstans.CANCEL_ADMOB_AUTO_DOWNLOAD_IMAGE, getIntent().getBooleanExtra(MIntegralConstans.CANCEL_ADMOB_AUTO_DOWNLOAD_IMAGE, false));
            }
            if (getIntent().hasExtra(MIntegralConstans.PROPERTIES_WALL_CURRENT_TAB_ID)) {
                int intExtra12 = getIntent().getIntExtra(MIntegralConstans.PROPERTIES_WALL_CURRENT_TAB_ID, 0);
                if (intExtra12 > 0) {
                    bundle2.putInt(MIntegralConstans.PROPERTIES_WALL_CURRENT_TAB_ID, intExtra12);
                }
            }
            if (getIntent().hasExtra(MIntegralConstans.PROPERTIES_WALL_TAB_SHAPE_COLOR)) {
                int intExtra13 = getIntent().getIntExtra(MIntegralConstans.PROPERTIES_WALL_TAB_SHAPE_COLOR, 0);
                if (intExtra13 >= 0) {
                    bundle2.putInt(MIntegralConstans.PROPERTIES_WALL_TAB_SHAPE_COLOR, intExtra13);
                }
            }
            if (getIntent().hasExtra(MIntegralConstans.PROPERTIES_WALL_TAB_SHAPE_HEIGHT)) {
                int intExtra14 = getIntent().getIntExtra(MIntegralConstans.PROPERTIES_WALL_TAB_SHAPE_HEIGHT, 0);
                if (intExtra14 >= 0) {
                    bundle2.putInt(MIntegralConstans.PROPERTIES_WALL_TAB_SHAPE_HEIGHT, intExtra14);
                }
            }
            this.b.setArguments(bundle2);
            FragmentTransaction beginTransaction = getSupportFragmentManager().beginTransaction();
            beginTransaction.add(16908300, (Fragment) this.b);
            beginTransaction.commit();
        } catch (Throwable th) {
            g.c("MTGActivity", "", th);
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onResumeFragments() {
        super.onResumeFragments();
        if (this.e) {
            Intent intent = this.f;
            String str = "wall";
            if (intent != null) {
                if (intent.hasExtra("type")) {
                    str = intent.getStringExtra("type");
                }
                String str2 = null;
                if (intent.hasExtra(NotificationCompat.CATEGORY_MESSAGE)) {
                    str2 = intent.getStringExtra(NotificationCompat.CATEGORY_MESSAGE);
                }
                this.f2951a = new HashMap();
                this.f2951a.put("type", str);
                this.f2951a.put(NotificationCompat.CATEGORY_MESSAGE, str2);
                this.f2951a.put(MIntegralConstans.PROPERTIES_UNIT_ID, this.h);
                try {
                    this.g = new FrameLayout(this);
                    this.g.setLayoutParams(new LayoutParams(-1, -1));
                    this.g.setId(16908300);
                    a(this.f);
                    this.d = MIntegralSDKFactory.getMIntegralSDK();
                    if (this.d.getStatus() != PLUGIN_LOAD_STATUS.COMPLETED) {
                        MIntegralSDKFactory.getMIntegralSDK().a(getApplication());
                        finish();
                    }
                    this.b = WallService.getFragment(this.f2951a);
                    if (this.b != null) {
                        Bundle bundle = new Bundle();
                        if (intent.hasExtra(MIntegralConstans.PROPERTIES_WALL_TITLE_BACKGROUND)) {
                            bundle.putParcelable(MIntegralConstans.PROPERTIES_WALL_TITLE_BACKGROUND, (Bitmap) intent.getParcelableExtra(MIntegralConstans.PROPERTIES_WALL_TITLE_BACKGROUND));
                        }
                        if (intent.hasExtra(MIntegralConstans.PROPERTIES_WALL_TITLE_LOGO)) {
                            bundle.putParcelable(MIntegralConstans.PROPERTIES_WALL_TITLE_LOGO, (Bitmap) intent.getParcelableExtra(MIntegralConstans.PROPERTIES_WALL_TITLE_LOGO));
                        }
                        if (intent.hasExtra(MIntegralConstans.PROPERTIES_WALL_TITLE_LOGO_ID)) {
                            int intExtra = intent.getIntExtra(MIntegralConstans.PROPERTIES_WALL_TITLE_LOGO_ID, 0);
                            if (intExtra > 0) {
                                bundle.putInt(MIntegralConstans.PROPERTIES_WALL_TITLE_LOGO_ID, intExtra);
                            }
                        }
                        if (intent.hasExtra(MIntegralConstans.PROPERTIES_WALL_TITLE_BACKGROUND_ID)) {
                            int intExtra2 = intent.getIntExtra(MIntegralConstans.PROPERTIES_WALL_TITLE_BACKGROUND_ID, 0);
                            if (intExtra2 > 0) {
                                bundle.putInt(MIntegralConstans.PROPERTIES_WALL_TITLE_BACKGROUND_ID, intExtra2);
                            }
                        }
                        if (intent.hasExtra(MIntegralConstans.PROPERTIES_WALL_MAIN_BACKGROUND_ID)) {
                            int intExtra3 = intent.getIntExtra(MIntegralConstans.PROPERTIES_WALL_MAIN_BACKGROUND_ID, 0);
                            if (intExtra3 > 0) {
                                bundle.putInt(MIntegralConstans.PROPERTIES_WALL_MAIN_BACKGROUND_ID, intExtra3);
                            }
                        }
                        if (intent.hasExtra(MIntegralConstans.PROPERTIES_WALL_TAB_BACKGROUND_ID)) {
                            int intExtra4 = intent.getIntExtra(MIntegralConstans.PROPERTIES_WALL_TAB_BACKGROUND_ID, 0);
                            if (intExtra4 > 0) {
                                bundle.putInt(MIntegralConstans.PROPERTIES_WALL_TAB_BACKGROUND_ID, intExtra4);
                            }
                        }
                        if (intent.hasExtra(MIntegralConstans.PROPERTIES_WALL_TAB_INDICATE_LINE_BACKGROUND_ID)) {
                            int intExtra5 = intent.getIntExtra(MIntegralConstans.PROPERTIES_WALL_TAB_INDICATE_LINE_BACKGROUND_ID, 0);
                            if (intExtra5 > 0) {
                                bundle.putInt(MIntegralConstans.PROPERTIES_WALL_TAB_INDICATE_LINE_BACKGROUND_ID, intExtra5);
                            }
                        }
                        if (intent.hasExtra(MIntegralConstans.PROPERTIES_WALL_BUTTON_BACKGROUND_ID)) {
                            int intExtra6 = intent.getIntExtra(MIntegralConstans.PROPERTIES_WALL_BUTTON_BACKGROUND_ID, 0);
                            if (intExtra6 > 0) {
                                bundle.putInt(MIntegralConstans.PROPERTIES_WALL_BUTTON_BACKGROUND_ID, intExtra6);
                            }
                        }
                        if (intent.hasExtra(MIntegralConstans.PROPERTIES_WALL_LOAD_ID)) {
                            int intExtra7 = intent.getIntExtra(MIntegralConstans.PROPERTIES_WALL_LOAD_ID, 0);
                            if (intExtra7 > 0) {
                                bundle.putInt(MIntegralConstans.PROPERTIES_WALL_LOAD_ID, intExtra7);
                            }
                        }
                        if (intent.hasExtra(MIntegralConstans.PROPERTIES_WALL_TITLE_BACKGROUND_COLOR)) {
                            int intExtra8 = intent.getIntExtra(MIntegralConstans.PROPERTIES_WALL_TITLE_BACKGROUND_COLOR, 0);
                            if (intExtra8 > 0) {
                                bundle.putInt(MIntegralConstans.PROPERTIES_WALL_TITLE_BACKGROUND_COLOR, intExtra8);
                            }
                        }
                        if (intent.hasExtra(MIntegralConstans.PROPERTIES_WALL_TITLE_LOGO_TEXT)) {
                            String stringExtra = intent.getStringExtra(MIntegralConstans.PROPERTIES_WALL_TITLE_LOGO_TEXT);
                            if (!TextUtils.isEmpty(stringExtra)) {
                                bundle.putCharSequence(MIntegralConstans.PROPERTIES_WALL_TITLE_LOGO_TEXT, stringExtra);
                            }
                        }
                        if (intent.hasExtra(MIntegralConstans.PROPERTIES_WALL_TITLE_LOGO_TEXT_COLOR)) {
                            int intExtra9 = intent.getIntExtra(MIntegralConstans.PROPERTIES_WALL_TITLE_LOGO_TEXT_COLOR, 0);
                            if (intExtra9 > 0) {
                                bundle.putInt(MIntegralConstans.PROPERTIES_WALL_TITLE_LOGO_TEXT_COLOR, intExtra9);
                            }
                        }
                        if (intent.hasExtra(MIntegralConstans.PROPERTIES_WALL_TITLE_LOGO_TEXT_SIZE)) {
                            int intExtra10 = intent.getIntExtra(MIntegralConstans.PROPERTIES_WALL_TITLE_LOGO_TEXT_SIZE, 0);
                            if (intExtra10 > 0) {
                                bundle.putInt(MIntegralConstans.PROPERTIES_WALL_TITLE_LOGO_TEXT_SIZE, intExtra10);
                            }
                        }
                        if (intent.hasExtra(MIntegralConstans.PROPERTIES_WALL_TITLE_LOGO_TEXT_TYPEFACE)) {
                            int intExtra11 = intent.getIntExtra(MIntegralConstans.PROPERTIES_WALL_TITLE_LOGO_TEXT_TYPEFACE, 0);
                            if (intExtra11 > 0) {
                                bundle.putInt(MIntegralConstans.PROPERTIES_WALL_TITLE_LOGO_TEXT_TYPEFACE, intExtra11);
                            }
                        }
                        if (intent.hasExtra(MIntegralConstans.PROPERTIES_WALL_CURRENT_TAB_ID)) {
                            int intExtra12 = intent.getIntExtra(MIntegralConstans.PROPERTIES_WALL_CURRENT_TAB_ID, 0);
                            if (intExtra12 > 0) {
                                bundle.putInt(MIntegralConstans.PROPERTIES_WALL_CURRENT_TAB_ID, intExtra12);
                            }
                        }
                        if (intent.hasExtra(MIntegralConstans.PROPERTIES_WALL_TAB_SHAPE_COLOR)) {
                            int intExtra13 = intent.getIntExtra(MIntegralConstans.PROPERTIES_WALL_TAB_SHAPE_COLOR, 0);
                            if (intExtra13 >= 0) {
                                bundle.putInt(MIntegralConstans.PROPERTIES_WALL_TAB_SHAPE_COLOR, intExtra13);
                            }
                        }
                        if (this.f2951a.containsKey(MIntegralConstans.PROPERTIES_WALL_TAB_SHAPE_HEIGHT)) {
                            int intValue = ((Integer) this.f2951a.get(MIntegralConstans.PROPERTIES_WALL_TAB_SHAPE_HEIGHT)).intValue();
                            if (intValue >= 0) {
                                bundle.putInt(MIntegralConstans.PROPERTIES_WALL_TAB_SHAPE_HEIGHT, intValue);
                            }
                        }
                        this.b.setArguments(bundle);
                        FragmentTransaction beginTransaction = getSupportFragmentManager().beginTransaction();
                        beginTransaction.replace(16908300, this.b);
                        beginTransaction.commit();
                    }
                } catch (Exception e2) {
                    g.c("MTGActivity", "", e2);
                }
            }
            this.e = false;
        }
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        if (intent != null) {
            String stringExtra = intent.getStringExtra(MIntegralConstans.PROPERTIES_UNIT_ID);
            if (!TextUtils.isEmpty(stringExtra) && !stringExtra.equals(this.h)) {
                this.e = true;
                this.mIsReport = false;
                this.f = intent;
                this.h = stringExtra;
            }
        }
    }

    private void a(Intent intent) {
        if (VERSION.SDK_INT >= 19) {
            this.g.setFitsSystemWindows(true);
            this.g.setClipToPadding(true);
            if (intent == null) {
                intent = getIntent();
            }
            if (intent != null) {
                try {
                    Class.forName("com.mintegral.msdk.appwall.g.a");
                    this.c = new a(this);
                    if (intent.hasExtra(MIntegralConstans.PROPERTIES_WALL_STATUS_COLOR)) {
                        this.c.a(getResources().getColor(intent.getIntExtra(MIntegralConstans.PROPERTIES_WALL_STATUS_COLOR, 0)));
                        this.c.a();
                    }
                    if (intent.hasExtra(MIntegralConstans.PROPERTIES_WALL_NAVIGATION_COLOR)) {
                        this.c.b(getResources().getColor(intent.getIntExtra(MIntegralConstans.PROPERTIES_WALL_NAVIGATION_COLOR, 0)));
                        this.c.b();
                    }
                    if (intent.hasExtra(MIntegralConstans.PROPERTIES_WALL_CONFIGCHANGES)) {
                        setRequestedOrientation(intent.getIntExtra(MIntegralConstans.PROPERTIES_WALL_CONFIGCHANGES, -1));
                    }
                } catch (ClassNotFoundException unused) {
                    g.d("", "SystemBarTintManager  can't find");
                }
            }
        }
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        try {
            if (Boolean.valueOf(BaseFragment.c()).booleanValue()) {
                return true;
            }
            return super.dispatchKeyEvent(keyEvent);
        } catch (Exception e2) {
            e2.printStackTrace();
            return false;
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        try {
            b.a((Context) this).c();
            String str = this.h;
            Intent intent = new Intent();
            StringBuilder sb = new StringBuilder();
            sb.append(com.mintegral.msdk.base.controller.a.d().j());
            sb.append(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
            sb.append(str);
            sb.append("_close");
            intent.setAction(sb.toString());
            sendBroadcast(intent);
        } catch (Exception unused) {
            g.d("M_SDK", "AppWall imageCache clear fail");
        }
        super.onDestroy();
    }
}
