package com.mintegral.msdk.shell;

import android.content.Intent;
import android.os.IBinder;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.mtgdownload.c;
import com.mintegral.msdk.pluginFramework.PluginService;
import com.mintegral.msdk.pluginFramework.a;
import com.mintegral.msdk.pluginFramework.a.C0063a;

public class MTGService extends PluginService {
    public void onCreate() {
        super.onCreate();
        this.f2904a.f2905a.b();
    }

    public final a a() {
        try {
            return new a(new C0063a(new c()));
        } catch (Exception e) {
            g.c("Download", "Find Provider Error", e);
            return null;
        }
    }

    public IBinder onBind(Intent intent) {
        return this.f2904a.f2905a.a();
    }

    public int onStartCommand(Intent intent, int i, int i2) {
        return this.f2904a.f2905a.a(intent);
    }

    public void onDestroy() {
        this.f2904a.f2905a.c();
    }
}
