package com.mintegral.msdk;

public class MIntegralConstans {
    public static final String ADMOB_AD_TYPE = "admob_type";
    public static final String ADMOB_AD_TYPE_APP_INSTALL = "admob_type";
    public static final String ADMOB_AD_TYPE_CONTENT = "admob_content";
    public static final int AD_TYPE_ADMOB = 6;
    public static final int AD_TYPE_MTG = 1;
    public static final int AD_TYPE_MYOFFER = 2;
    public static final int AD_TYPE_MYTARGET = 7;
    public static boolean ALLOW_APK_DOWNLOAD = true;
    public static final String API_REUQEST_CATEGORY_APP = "2";
    public static final String API_REUQEST_CATEGORY_GAME = "1";
    public static boolean APPWALL_IS_SHOW_WHEN_SCREEN_LOCK = false;
    public static final String APPWALL_VIEW_LOAD_RESULT_LISTENER = "appwall_view_load_result_listener";
    public static final String APP_ID = "app_id";
    public static final String APP_KEY = "app_key";
    public static final String AUTHORITYIMEIMAC = "authority_imei_mac";
    public static final String AUTHORITY_ALL_INFO = "authority_all_info";
    public static final String AUTHORITY_ANDROID_ID = "authority_android_id";
    public static final String AUTHORITY_APPLIST = "authority_applist";
    public static final String AUTHORITY_APP_DOWNLOAD = "authority_app_download";
    public static final String AUTHORITY_APP_LOAD_FAILED = "Temporarily does not support the popup window";
    public static final String AUTHORITY_APP_PROGRESS = "authority_app_progress";
    public static String AUTHORITY_DEFAULTLOCAL_INFO_URL = "file:////android_asset/mintegral_jscommon_authtext.html";
    public static String AUTHORITY_DEFAULT_INFO_URL = "https://hybird.rayjump.com/rv/authoriztion.html";
    public static final String AUTHORITY_DEVICE_ID = "authority_device_id";
    public static final String AUTHORITY_GENERAL_DATA = "authority_general_data";
    public static final String AUTHORITY_GPS = "authority_gps";
    public static final String AUTHORITY_SERIAL_ID = "authority_serial_id";
    public static final String CANCEL_ADMOB_AUTO_DOWNLOAD_IMAGE = "cancel_admob_audo_download_image";
    public static boolean CUSTOMER_HANDLE_CLICK = false;
    public static boolean DEBUG = false;
    public static final String FB_MEDIA_CACHE_FLAG = "MediaCacheFlag";
    public static boolean HANDLE_EXCEPTION = true;
    public static final String ID_ADMOB_UNITID = "admob_unitid";
    public static final String ID_FACE_BOOK_PLACEMENT = "facebook_placementid";
    public static final String ID_MINTEGRAL_APPID = "mintegral_appid";
    public static final String ID_MINTEGRAL_APPKEY = "mintegral_appkey";
    public static final String ID_MINTEGRAL_STARTUPCRASH = "mintegral_appstartupcarsh";
    public static final String ID_MY_TARGET_AD_UNITID = "mytarget_unitid";
    public static boolean INIT_UA_IN = true;
    public static final int INTER_ACTIVE_VIDEO_PLAY_MUTE = 1;
    public static final int INTER_ACTIVE_VIDEO_PLAY_NOT_MUTE = 2;
    public static boolean IS_DOWANLOAD_FINSH_PLAY = false;
    public static boolean IS_SP_CBT_CF = true;
    public static final int IS_SWITCH_OFF = 0;
    public static final int IS_SWITCH_ON = 1;
    public static final String KEY_WORD = "key_word";
    public static final int LAYOUT_APPWALL = 3;
    public static final int LAYOUT_BANNER = 1;
    public static final int LAYOUT_INTERSTITIAL = 2;
    public static final int LAYOUT_NATIVE = 0;
    public static final String MYTARGET_AD_TYPE = "";
    public static final String NATIVE_INFO = "native_info";
    public static boolean NATIVE_SHOW_LOADINGPAGER = false;
    public static final int NATIVE_VIDEO_DISPALY_MODE_FB = 4;
    public static final int NATIVE_VIDEO_DISPALY_MODE_GIF = 3;
    public static final int NATIVE_VIDEO_DISPALY_MODE_IMAGE = 1;
    public static final int NATIVE_VIDEO_DISPALY_MODE_UNKNOW = 0;
    public static final int NATIVE_VIDEO_DISPALY_MODE_VIDEO = 2;
    public static final String NATIVE_VIDEO_HEIGHT = "native_video_height";
    public static final String NATIVE_VIDEO_SUPPORT = "videoSupport";
    public static final String NATIVE_VIDEO_VERSION = "2.0";
    public static final String NATIVE_VIDEO_WIDTH = "native_video_width";
    public static final String OFFER_WALL_REWARD_OPEN_WARN = "open_warn";
    public static final String OFFER_WALL_REWARD_VIDEO_RESUME_TEXT = "offerwall_reward_video_resume_text";
    public static final String OFFER_WALL_REWARD_VIDEO_STOP_TEXT = "offerwall_reward_video_stop_text";
    public static final String OFFER_WALL_REWARD_VIDEO_WARN_TEXT = "offerwall_reward_video_wanr_text";
    public static final String OFFER_WALL_TITLE_BACKGROUD_COLOR = "offerwall_backgroud_color";
    public static final String OFFER_WALL_TITLE_FONT_COLOR = "offerwall_title_font_color";
    public static final String OFFER_WALL_TITLE_FONT_SIZE = "offerwall_font_size";
    public static final String OFFER_WALL_TITLE_FONT_TYPEFACE = "offerwall_foint_typeface";
    public static final String OFFER_WALL_TITLE_TEXT = "offerwall_text";
    public static final String OFFER_WALL_USER_ID = "offerwall_user_id";
    public static final String PACKAGE_NAME_MANIFEST = "applicationID";
    public static final int PLAY_VIDEO_FINISH_OPERATE_TYPE_DEFAULT = -1;
    public static final int PLAY_VIDEO_FINISH_OPERATE_TYPE_SHOW_MINICARD = 2;
    public static final String PLUGIN_BANNER = "MVBannerPlugin";
    public static final String PLUGIN_FACEBOOK = "MVFacebookPlugin";
    public static final String PLUGIN_INTERSTITIAL = "MVInterstitialPlugin";
    public static final String PLUGIN_NAME = "plugin_name";
    public static final String PLUGIN_NATIVE = "MVNativePlugin";
    public static final String PLUGIN_WALL = "MVWallPlugin";
    public static final String PREIMAGE = "isPreloadImg";
    public static boolean PRELOAD_RESULT_IN_SUBTHREAD = false;
    public static final String PRELOAD_RESULT_LISTENER = "preload_result_listener";
    public static final String PROPERTIES_AD_FRAME_NUM = "ad_frame_num";
    public static final String PROPERTIES_AD_NUM = "ad_num";
    public static final String PROPERTIES_API_REUQEST_CATEGORY = "catetory";
    public static final String PROPERTIES_HANDLER_CONTROLLER = "handler_controller";
    public static final String PROPERTIES_LAYOUT_TYPE = "layout_type";
    public static final String PROPERTIES_UNIT_ID = "unit_id";
    public static final String PROPERTIES_WALL_BUTTON_BACKGROUND_ID = "wall_button_background_id";
    public static final String PROPERTIES_WALL_CONFIGCHANGES = "wall_configchanges";
    public static final String PROPERTIES_WALL_CURRENT_TAB_ID = "wall_current_tab_id";
    public static final String PROPERTIES_WALL_ENTRY = "wall_entry";
    public static final String PROPERTIES_WALL_FACEBOOK_PLACEMENT_ID = "wall_facebook_placement_id";
    public static final String PROPERTIES_WALL_LOAD_ID = "wall_load_id";
    public static final String PROPERTIES_WALL_MAIN_BACKGROUND_ID = "wall_main_background_id";
    public static final String PROPERTIES_WALL_NAVIGATION_COLOR = "wall_navigation_color";
    public static final String PROPERTIES_WALL_STATUS_COLOR = "wall_status_color";
    public static final String PROPERTIES_WALL_TAB_BACKGROUND_ID = "wall_tab_background_id";
    public static final String PROPERTIES_WALL_TAB_INDICATE_LINE_BACKGROUND_ID = "wall_tab_line_background_id";
    public static final String PROPERTIES_WALL_TAB_SELECTED_TEXT_COLOR = "wall_tab_selected_text_color";
    public static final String PROPERTIES_WALL_TAB_SHAPE_COLOR = "wall_tab_shape_color";
    public static final String PROPERTIES_WALL_TAB_SHAPE_HEIGHT = "wall_tab_shape_height";
    public static final String PROPERTIES_WALL_TAB_UNSELECTED_TEXT_COLOR = "wall_tab_unselected_text_color";
    public static final String PROPERTIES_WALL_TITLE_BACKGROUND = "wall_title_background";
    public static final String PROPERTIES_WALL_TITLE_BACKGROUND_COLOR = "wall_title_background_color";
    public static final String PROPERTIES_WALL_TITLE_BACKGROUND_ID = "wall_title_background_id";
    public static final String PROPERTIES_WALL_TITLE_LOGO = "wall_title_logo";
    public static final String PROPERTIES_WALL_TITLE_LOGO_ID = "wall_title_logo_id";
    public static final String PROPERTIES_WALL_TITLE_LOGO_TEXT = "wall_title_logo_text";
    public static final String PROPERTIES_WALL_TITLE_LOGO_TEXT_COLOR = "wall_title_logo_text_color";
    public static final String PROPERTIES_WALL_TITLE_LOGO_TEXT_SIZE = "wall_title_logo_text_size";
    public static final String PROPERTIES_WALL_TITLE_LOGO_TEXT_TYPEFACE = "wall_title_logo_text_typeface";
    public static int REQUEST_TIME_OUT = 8000;
    public static final int REWARD_VIDEO_PLAY_MUTE = 1;
    public static final int REWARD_VIDEO_PLAY_NOT_MUTE = 2;
    public static boolean RICH_NOTIFICATION = true;
    public static final String SDK_APP_ID = "sdk_app_id";
    public static final String SHORTCUTS_CTIME = "mtg_shortcuts_ctime";
    public static boolean SLIENT_DOWNLOAD = false;
    public static final int TEMPLATE_BIG_IMG = 2;
    public static final int TEMPLATE_MULTIPLE_IMG = 3;
    public static final int TITLE_TYPEFACE_DEFAULT = 1;
    public static final int TITLE_TYPEFACE_DEFAULT_BOLD = 2;
    public static final int TITLE_TYPEFACE_DEFAULT_MONOSPACE = 3;
    public static final int TITLE_TYPEFACE_DEFAULT_SANS_SERIF = 4;
    public static final int TITLE_TYPEFACE_DEFAULT_SERIF = 5;
    public static final String VIDEO_FEEDS_CTA_COLOR = "video_feeds_cta_color";
    public static final String VIDEO_FEEDS_DESC_FONT = "video_feeds_desc_font";
    public static final String VIDEO_FEEDS_DESC_TEXT_COLOR = "video_feeds_desc_color";
    public static final String VIDEO_FEEDS_DESC_TEXT_SIZE = "video_feeds_desc_size";
    public static final String VIDEO_FEEDS_MAIN_BG_COLOR = "video_feeds_main_color";
    public static final String VIDEO_FEEDS_TITLE_FONT = "video_feeds_title_font";
    public static final String VIDEO_FEEDS_TITLE_TEXT_COLOR = "video_feeds_title_color";
    public static final String VIDEO_FEEDS_TITLE_TEXT_SIZE = "video_feeds_title_size";
    public static final String WALL_ENTRY_ID_IMAGEVIEW_IMAGE = "imageview";
    public static final String WALL_ENTRY_ID_VIEWGROUP_NEWTIP = "newtip";
    public static final String WALL_VIEW_VIEWPAGER_NOSCROLL = "wall_view_viewpager_noscroll";
}
