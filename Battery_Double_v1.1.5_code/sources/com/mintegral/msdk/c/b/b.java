package com.mintegral.msdk.c.b;

import android.support.v4.app.NotificationCompat;
import com.mintegral.msdk.base.entity.CampaignUnit;
import org.json.JSONObject;

/* compiled from: ShortCutsResponseHandler */
public abstract class b extends com.mintegral.msdk.base.common.net.a.b {

    /* renamed from: a reason: collision with root package name */
    private long f2644a;

    public abstract void a(CampaignUnit campaignUnit);

    public abstract void b(CampaignUnit campaignUnit);

    public final /* synthetic */ void a(Object obj) {
        JSONObject jSONObject = (JSONObject) obj;
        if (1 == jSONObject.optInt("status")) {
            System.currentTimeMillis();
            CampaignUnit parseCampaignUnit = CampaignUnit.parseCampaignUnit(jSONObject.optJSONObject("data"));
            if (parseCampaignUnit == null || parseCampaignUnit.getAds() == null || parseCampaignUnit.getAds().size() <= 0) {
                jSONObject.optString(NotificationCompat.CATEGORY_MESSAGE);
                b(parseCampaignUnit);
                return;
            }
            a(parseCampaignUnit);
            return;
        }
        jSONObject.optString(NotificationCompat.CATEGORY_MESSAGE);
        b(null);
    }

    public final void a() {
        super.a();
        this.f2644a = System.currentTimeMillis();
    }

    public final void a(int i) {
        b(null);
    }
}
