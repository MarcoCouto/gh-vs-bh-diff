package com.mintegral.msdk.c;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.ProviderInfo;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.TextUtils;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.activity.MTGCommonActivity;
import com.mintegral.msdk.b.a;
import com.mintegral.msdk.base.b.f;
import com.mintegral.msdk.base.b.h;
import com.mintegral.msdk.base.b.i;
import com.mintegral.msdk.base.common.c.c;
import com.mintegral.msdk.base.entity.CampaignEx;
import java.lang.ref.WeakReference;
import java.util.Iterator;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: ShortCutsManager */
public class b {
    private static volatile b b;

    /* renamed from: a reason: collision with root package name */
    private WeakReference<Context> f2637a;
    /* access modifiers changed from: private */
    public Handler c = new Handler(Looper.getMainLooper()) {
        public final void handleMessage(Message message) {
            if (message.what == 10000) {
                try {
                    int i = message.arg1;
                    Object obj = message.obj;
                    String str = null;
                    if (obj instanceof String) {
                        str = (String) obj;
                    }
                    b.a(b.this, i, str);
                } catch (Exception e) {
                    if (MIntegralConstans.DEBUG) {
                        e.printStackTrace();
                    }
                }
            }
        }
    };

    private b(Context context) {
        this.f2637a = new WeakReference<>(context);
    }

    public static b a(Context context) {
        if (b == null) {
            synchronized (b.class) {
                if (b == null) {
                    b = new b(context);
                }
            }
        }
        return b;
    }

    /* access modifiers changed from: private */
    public boolean b(String str) {
        Context context = (Context) this.f2637a.get();
        if (context == null || context.getPackageManager().checkPermission(str, context.getPackageName()) != 0) {
            return false;
        }
        return true;
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x006e  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0072  */
    public final void a(final CampaignEx campaignEx, final Activity activity) {
        boolean z;
        final Context context = (Context) this.f2637a.get();
        if (context != null) {
            if (context != null) {
                f a2 = f.a((h) i.a(context));
                com.mintegral.msdk.b.b.a();
                a b2 = com.mintegral.msdk.b.b.b(com.mintegral.msdk.base.controller.a.d().j());
                if (b2 == null) {
                    com.mintegral.msdk.b.b.a();
                    b2 = com.mintegral.msdk.b.b.b();
                }
                List g = a2.g(b2.j());
                if (g != null && g.size() > 0) {
                    Iterator it = g.iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            break;
                        }
                        CampaignEx campaignEx2 = (CampaignEx) it.next();
                        if (campaignEx2.getId().equals(campaignEx.getId()) && campaignEx2.getAppName().equals(campaignEx.getAppName())) {
                            z = true;
                            break;
                        }
                    }
                    if (!z) {
                        b(activity);
                        return;
                    } else if (!TextUtils.isEmpty(campaignEx.getIconUrl())) {
                        com.mintegral.msdk.base.common.c.b.a(context).a(campaignEx.getIconUrl(), (c) new c() {
                            public final void onFailedLoad(String str, String str2) {
                                b.b(activity);
                            }

                            public final void onSuccessLoad(Bitmap bitmap, String str) {
                                f a2 = f.a((h) i.a(context));
                                com.mintegral.msdk.b.b.a();
                                a b2 = com.mintegral.msdk.b.b.b(com.mintegral.msdk.base.controller.a.d().j());
                                if (b2 == null) {
                                    com.mintegral.msdk.b.b.a();
                                    b2 = com.mintegral.msdk.b.b.b();
                                }
                                b.this.a(context, b2);
                                if (b.this.b("com.android.launcher.permission.INSTALL_SHORTCUT")) {
                                    int b3 = b.this.c(campaignEx.getAppName());
                                    b.a(context, bitmap, campaignEx, activity);
                                    b.a(b.this, campaignEx, b2, a2, b3);
                                }
                            }
                        });
                        return;
                    } else {
                        b(activity);
                        return;
                    }
                }
            }
            z = false;
            if (!z) {
            }
        }
    }

    private void a(Context context, CampaignEx campaignEx, int i) {
        Intent intent = new Intent("com.android.launcher.action.UNINSTALL_SHORTCUT");
        intent.putExtra("android.intent.extra.shortcut.NAME", campaignEx.getAppName());
        Intent intent2 = new Intent(context, MTGCommonActivity.class);
        intent2.setAction("android.intent.action.VIEW");
        intent.putExtra("android.intent.extra.shortcut.INTENT", intent2);
        context.sendBroadcast(intent);
        f a2 = f.a((h) i.a(context));
        com.mintegral.msdk.b.b.a();
        a b2 = com.mintegral.msdk.b.b.b(com.mintegral.msdk.base.controller.a.d().j());
        if (b2 == null) {
            com.mintegral.msdk.b.b.a();
            b2 = com.mintegral.msdk.b.b.b();
        }
        String j = b2.j();
        if (b("com.android.launcher.permission.READ_SETTINGS") || b("com.google.android.launcher.permission.READ_SETTINGS")) {
            if (c(campaignEx.getAppName()) < i) {
                a(campaignEx, 2, 1);
                campaignEx.setIsDeleted(1);
                ContentValues contentValues = new ContentValues();
                contentValues.put("is_deleted", Integer.valueOf(1));
                a2.a(campaignEx.getId(), contentValues);
            } else {
                a(campaignEx, 2, 0);
            }
        } else {
            a(campaignEx, 2, -1);
            ContentValues contentValues2 = new ContentValues();
            contentValues2.put("is_deleted", Integer.valueOf(1));
            a2.a(campaignEx.getId(), contentValues2);
        }
        a2.c(j);
    }

    public final int a() {
        Context context = (Context) this.f2637a.get();
        if (context == null) {
            return 0;
        }
        f a2 = f.a((h) i.a(context));
        com.mintegral.msdk.b.b.a();
        a b2 = com.mintegral.msdk.b.b.b(com.mintegral.msdk.base.controller.a.d().j());
        if (b2 == null) {
            com.mintegral.msdk.b.b.a();
            b2 = com.mintegral.msdk.b.b.b();
        }
        List g = a2.g(b2.j());
        if (g == null || g.size() <= 0) {
            return 0;
        }
        return g.size();
    }

    private boolean a(Context context, String str) {
        boolean z = false;
        if (context == null || TextUtils.isEmpty(str)) {
            return false;
        }
        String str2 = null;
        if (TextUtils.isEmpty(null)) {
            str2 = b(context);
        }
        if (!TextUtils.isEmpty(str2)) {
            try {
                Cursor a2 = a(str2, str, context);
                if (a2 != null && a2.getCount() > 0) {
                    z = true;
                }
                if (a2 != null && !a2.isClosed()) {
                    a2.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return z;
    }

    private static Cursor a(String str, String str2, Context context) {
        return context.getContentResolver().query(Uri.parse(str), new String[]{"title"}, "title=?", new String[]{str2}, null);
    }

    /* access modifiers changed from: private */
    public int c(String str) {
        Context context = (Context) this.f2637a.get();
        if (context == null || (!b("com.android.launcher.permission.READ_SETTINGS") && !b("com.google.android.launcher.permission.READ_SETTINGS"))) {
            return 0;
        }
        String b2 = b(context);
        if (!TextUtils.isEmpty(b2)) {
            try {
                Cursor a2 = a(b2, str, context);
                if (a2 != null && a2.getCount() > 0) {
                    return a2.getCount();
                }
                if (a2 != null && !a2.isClosed()) {
                    a2.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return 0;
    }

    public final String b() {
        Intent intent = new Intent("android.intent.action.MAIN");
        intent.addCategory("android.intent.category.HOME");
        Context context = (Context) this.f2637a.get();
        if (context == null) {
            return "";
        }
        ResolveInfo resolveActivity = context.getPackageManager().resolveActivity(intent, 0);
        if (resolveActivity == null || resolveActivity.activityInfo == null) {
            return "";
        }
        if (resolveActivity.activityInfo.packageName.equals("android")) {
            return "";
        }
        return resolveActivity.activityInfo.packageName;
    }

    private String b(Context context, String str) {
        if (TextUtils.isEmpty(str)) {
            return "";
        }
        try {
            List<PackageInfo> installedPackages = context.getPackageManager().getInstalledPackages(8);
            if (installedPackages == null) {
                return "";
            }
            for (PackageInfo packageInfo : installedPackages) {
                ProviderInfo[] providerInfoArr = packageInfo.providers;
                if (providerInfoArr != null) {
                    for (ProviderInfo providerInfo : providerInfoArr) {
                        if (!b("com.android.launcher.permission.READ_SETTINGS")) {
                            str = "com.google.android.launcher.permission.READ_SETTINGS";
                        }
                        if ((str.equals(providerInfo.readPermission) || str.equals(providerInfo.writePermission)) && !TextUtils.isEmpty(providerInfo.authority) && providerInfo.authority.contains(".launcher.settings")) {
                            return providerInfo.authority;
                        }
                    }
                    continue;
                }
            }
            return "";
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public final String c() {
        String str = b("com.android.launcher.permission.INSTALL_SHORTCUT") ? "1" : "0";
        String str2 = b("com.android.launcher.permission.UNINSTALL_SHORTCUT") ? "1" : "0";
        String str3 = (b("com.android.launcher.permission.READ_SETTINGS") || b("com.google.android.launcher.permission.READ_SETTINGS")) ? "1" : "0";
        StringBuilder sb = new StringBuilder(RequestParameters.LEFT_BRACKETS);
        sb.append(str);
        sb.append(",");
        sb.append(str2);
        sb.append(",");
        sb.append(str3);
        sb.append(RequestParameters.RIGHT_BRACKETS);
        return sb.toString();
    }

    private void a(CampaignEx campaignEx, int i, int i2) {
        if (this.f2637a.get() != null && campaignEx.getIsDeleted() != 1) {
            com.mintegral.msdk.base.common.d.c.a((Context) this.f2637a.get(), campaignEx, i, i2);
        }
    }

    public final void a(a aVar) {
        try {
            Context context = (Context) this.f2637a.get();
            if (context != null) {
                f a2 = f.a((h) i.a(context));
                List<CampaignEx> g = a2.g(aVar.j());
                if (g != null && g.size() > 0) {
                    for (CampaignEx campaignEx : g) {
                        if (campaignEx.getIsAddSuccesful() == 0 && campaignEx.getIsDeleted() == 0) {
                            if (c(campaignEx.getAppName()) != 0) {
                                a(campaignEx, 1, 1);
                                com.mintegral.msdk.base.a.a.a.a().a(MIntegralConstans.SHORTCUTS_CTIME, String.valueOf(System.currentTimeMillis()));
                            } else {
                                a(campaignEx, 1, 0);
                            }
                            campaignEx.setIsAddSuccesful(-1);
                            ContentValues contentValues = new ContentValues();
                            contentValues.put("is_add_sucesful", Integer.valueOf(campaignEx.getIsAddSuccesful()));
                            a2.a(campaignEx.getId(), contentValues);
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public final void a(final String str) {
        try {
            if (((Context) this.f2637a.get()) != null) {
                new Thread(new Runnable() {
                    final /* synthetic */ int b = 30000;

                    public final void run() {
                        a aVar;
                        if (str != null) {
                            com.mintegral.msdk.b.b.a();
                            aVar = com.mintegral.msdk.b.b.b(str);
                        } else {
                            aVar = null;
                        }
                        if (aVar == null) {
                            com.mintegral.msdk.b.b.a();
                            aVar = com.mintegral.msdk.b.b.b();
                        }
                        b.this.a(aVar);
                        Message obtain = Message.obtain();
                        obtain.arg1 = this.b;
                        obtain.obj = str;
                        b.this.c.sendEmptyMessage(10000);
                    }
                }).start();
            }
        } catch (Exception e) {
            if (MIntegralConstans.DEBUG) {
                e.printStackTrace();
            }
        }
    }

    private static boolean b(a aVar) {
        if (aVar == null) {
            return false;
        }
        int f = aVar.f();
        long currentTimeMillis = System.currentTimeMillis();
        long j = 0;
        if (!com.mintegral.msdk.base.a.a.a.a().a(MIntegralConstans.SHORTCUTS_CTIME).equals("")) {
            j = Long.parseLong(com.mintegral.msdk.base.a.a.a.a().a(MIntegralConstans.SHORTCUTS_CTIME));
        }
        if ((currentTimeMillis - j) / 1000 > ((long) f)) {
            return true;
        }
        return false;
    }

    public final void b(CampaignEx campaignEx, final Activity activity) {
        if (VERSION.SDK_INT < 26) {
            final Context context = (Context) this.f2637a.get();
            if (context != null) {
                com.mintegral.msdk.b.b.a();
                final a b2 = com.mintegral.msdk.b.b.b(com.mintegral.msdk.base.controller.a.d().j());
                if (b2 == null) {
                    com.mintegral.msdk.b.b.a();
                    b2 = com.mintegral.msdk.b.b.b();
                }
                if (!b(b2)) {
                    a.a().a(context, campaignEx, 1);
                    b(activity);
                    return;
                } else if (b2.h() == 1) {
                    a.a().a(context, campaignEx, (com.mintegral.msdk.c.a.a) new com.mintegral.msdk.c.a.a() {
                        public final void a(List<CampaignEx> list) {
                            if (list == null || list.size() <= 0) {
                                b.b(activity);
                                return;
                            }
                            WeakReference weakReference = new WeakReference(activity);
                            if (weakReference.get() != null) {
                                b.this.a((CampaignEx) list.get(0), (Activity) weakReference.get());
                            }
                        }

                        public final void a() {
                            b.b(activity);
                        }

                        public final void b() {
                            b.this.a(context, b2);
                            b.b(activity);
                        }
                    });
                    return;
                } else {
                    a.a().a(context, campaignEx, 1);
                }
            } else {
                return;
            }
        }
        b(activity);
    }

    /* access modifiers changed from: private */
    public static void b(Activity activity) {
        if (activity != null) {
            activity.finish();
        }
    }

    /* access modifiers changed from: private */
    public void a(Context context, a aVar) {
        if (!TextUtils.isEmpty(aVar.j())) {
            List<CampaignEx> g = f.a((h) i.a(context)).g(aVar.j());
            if (g != null && g.size() > 0 && b("com.android.launcher.permission.UNINSTALL_SHORTCUT")) {
                for (CampaignEx campaignEx : g) {
                    int c2 = c(campaignEx.getAppName());
                    Context context2 = (Context) this.f2637a.get();
                    if (context2 != null) {
                        if (b("com.android.launcher.permission.READ_SETTINGS") || b("com.google.android.launcher.permission.READ_SETTINGS")) {
                            if (a(context2, campaignEx.getAppName())) {
                                a(context2, campaignEx, c2);
                            }
                        } else if (b("com.android.launcher.permission.UNINSTALL_SHORTCUT")) {
                            a(context2, campaignEx, c2);
                        }
                    }
                }
            }
        }
    }

    private String b(Context context) {
        String b2 = b(context, "com.android.launcher.permission.READ_SETTINGS");
        if (b2 == null || b2.trim().equals("")) {
            String b3 = b();
            StringBuilder sb = new StringBuilder();
            sb.append(b3);
            sb.append(".permission.READ_SETTINGS");
            b2 = b(context, sb.toString());
        }
        if (TextUtils.isEmpty(b2)) {
            int i = VERSION.SDK_INT;
            b2 = i < 8 ? "com.android.launcher.settings" : i < 19 ? "com.android.launcher2.settings" : "com.android.launcher3.settings";
        }
        StringBuilder sb2 = new StringBuilder("content://");
        sb2.append(b2);
        sb2.append("/favorites?notify=true");
        return sb2.toString();
    }

    static /* synthetic */ void a(Context context, Bitmap bitmap, CampaignEx campaignEx, Activity activity) {
        JSONObject jSONObject;
        if (!TextUtils.isEmpty(campaignEx.getAppName()) && bitmap != null) {
            try {
                Intent intent = new Intent("com.android.launcher.action.INSTALL_SHORTCUT");
                intent.putExtra("android.intent.extra.shortcut.NAME", campaignEx.getAppName());
                intent.putExtra("duplicate", false);
                intent.putExtra("android.intent.extra.shortcut.ICON", Bitmap.createScaledBitmap(bitmap, IronSourceConstants.USING_CACHE_FOR_INIT_EVENT, IronSourceConstants.USING_CACHE_FOR_INIT_EVENT, true));
                Intent intent2 = new Intent(context, MTGCommonActivity.class);
                intent2.putExtra("intent_flag", "shortcuts");
                try {
                    jSONObject = CampaignEx.campaignToJsonObject(campaignEx);
                } catch (JSONException e) {
                    e.printStackTrace();
                    jSONObject = null;
                }
                if (jSONObject != null) {
                    intent2.putExtra("intent_jsonobject", jSONObject.toString());
                }
                intent2.setFlags(32768);
                intent.putExtra("android.intent.extra.shortcut.INTENT", intent2);
                context.sendBroadcast(intent);
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        b(activity);
    }

    static /* synthetic */ void a(b bVar, CampaignEx campaignEx, a aVar, f fVar, int i) {
        campaignEx.setTimestamp(0);
        if (bVar.b("com.android.launcher.permission.READ_SETTINGS") || bVar.b("com.google.android.launcher.permission.READ_SETTINGS")) {
            if (bVar.c(campaignEx.getAppName()) > i) {
                campaignEx.setIsAddSuccesful(1);
                fVar.a(campaignEx, aVar.j(), 0);
                bVar.a(campaignEx, 1, 1);
                campaignEx.setIsAddSuccesful(-1);
                com.mintegral.msdk.base.a.a.a.a().a(MIntegralConstans.SHORTCUTS_CTIME, String.valueOf(System.currentTimeMillis()));
                return;
            }
            campaignEx.setIsAddSuccesful(0);
            fVar.a(campaignEx, aVar.j(), 0);
            com.mintegral.msdk.base.a.a.a.a().a(MIntegralConstans.SHORTCUTS_CTIME, String.valueOf(System.currentTimeMillis()));
            return;
        }
        campaignEx.setIsAddSuccesful(1);
        bVar.a(campaignEx, 1, -1);
        campaignEx.setIsAddSuccesful(-1);
        fVar.a(campaignEx, aVar.j(), 0);
        com.mintegral.msdk.base.a.a.a.a().a(MIntegralConstans.SHORTCUTS_CTIME, String.valueOf(System.currentTimeMillis()));
    }

    static /* synthetic */ void a(b bVar, int i, String str) {
        try {
            final Context context = (Context) bVar.f2637a.get();
            if (context != null) {
                a aVar = null;
                if (str != null) {
                    com.mintegral.msdk.b.b.a();
                    aVar = com.mintegral.msdk.b.b.b(str);
                }
                if (aVar == null) {
                    com.mintegral.msdk.b.b.a();
                    aVar = com.mintegral.msdk.b.b.b();
                }
                if (b(aVar)) {
                    new Handler().postDelayed(new Runnable() {
                        public final void run() {
                            com.mintegral.msdk.b.b.a();
                            final a b2 = com.mintegral.msdk.b.b.b(com.mintegral.msdk.base.controller.a.d().j());
                            if (b2 == null) {
                                com.mintegral.msdk.b.b.a();
                                b2 = com.mintegral.msdk.b.b.b();
                            }
                            if (b2.h() == 1) {
                                a.a().a(context, a.c, (com.mintegral.msdk.c.a.a) new com.mintegral.msdk.c.a.a() {
                                    public final void a() {
                                    }

                                    public final void a(List<CampaignEx> list) {
                                        if (list != null && list.size() > 0) {
                                            b.this.a((CampaignEx) list.get(0), (Activity) null);
                                        }
                                    }

                                    public final void b() {
                                        b.this.a(context, b2);
                                    }
                                });
                            }
                        }
                    }, (long) i);
                }
            }
        } catch (Exception e) {
            if (MIntegralConstans.DEBUG) {
                e.printStackTrace();
            }
        }
    }
}
