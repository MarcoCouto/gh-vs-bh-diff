package com.mintegral.msdk.c;

import android.content.Context;
import android.os.Looper;
import android.text.TextUtils;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.base.b.f;
import com.mintegral.msdk.base.b.h;
import com.mintegral.msdk.base.b.i;
import com.mintegral.msdk.base.common.e.b;
import com.mintegral.msdk.base.common.net.d;
import com.mintegral.msdk.base.common.net.l;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.entity.CampaignUnit;
import com.mintegral.msdk.base.utils.CommonMD5;
import com.mintegral.msdk.base.utils.c;
import com.mintegral.msdk.base.utils.k;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ShortCutsDataManager */
public final class a {

    /* renamed from: a reason: collision with root package name */
    public static int f2633a = 3;
    public static int b = 1;
    public static int c = 4;
    public static int d = 5;
    private static String e = "ShortCutsDataManager";
    private com.mintegral.msdk.click.a f;

    /* renamed from: com.mintegral.msdk.c.a$a reason: collision with other inner class name */
    /* compiled from: ShortCutsDataManager */
    private static class C0053a {
        /* access modifiers changed from: private */

        /* renamed from: a reason: collision with root package name */
        public static a f2636a = new a(0);
    }

    /* synthetic */ a(byte b2) {
        this();
    }

    private a() {
    }

    public static a a() {
        return C0053a.f2636a;
    }

    public final void a(final Context context, final int i, final com.mintegral.msdk.c.a.a aVar) {
        new b(context).b(new com.mintegral.msdk.base.common.e.a() {
            public final void b() {
            }

            public final void a() {
                com.mintegral.msdk.c.b.a aVar = new com.mintegral.msdk.c.b.a(context);
                l lVar = new l();
                com.mintegral.msdk.b.b.a();
                com.mintegral.msdk.b.a b2 = com.mintegral.msdk.b.b.b(com.mintegral.msdk.base.controller.a.d().j());
                if (b2 == null) {
                    com.mintegral.msdk.b.b.a();
                    b2 = com.mintegral.msdk.b.b.b();
                }
                String j = b2.j();
                if (TextUtils.isEmpty(j)) {
                    aVar.a();
                    return;
                }
                lVar.a("app_id", com.mintegral.msdk.base.controller.a.d().j());
                StringBuilder sb = new StringBuilder();
                sb.append(com.mintegral.msdk.base.controller.a.d().j());
                sb.append(com.mintegral.msdk.base.controller.a.d().k());
                lVar.a("sign", CommonMD5.getMD5(sb.toString()));
                lVar.a("jm_a", b.a(context).c());
                StringBuilder sb2 = new StringBuilder();
                sb2.append(b.a(context).a());
                lVar.a("jm_n", sb2.toString());
                lVar.a("jm_l", b.a(context).b());
                lVar.a(MIntegralConstans.PROPERTIES_UNIT_ID, j);
                lVar.a("jm_dp_ads", f.a((h) i.a(com.mintegral.msdk.base.controller.a.d().h())).d(j));
                StringBuilder sb3 = new StringBuilder();
                sb3.append(i);
                lVar.a("req_type", sb3.toString());
                lVar.a("ad_type", "289");
                Looper.prepare();
                aVar.a(com.mintegral.msdk.base.common.a.k, lVar, (d<?>) new com.mintegral.msdk.c.b.b() {
                    public final void a(CampaignUnit campaignUnit) {
                        if (aVar != null) {
                            if (campaignUnit.getJmDo() == a.f2633a) {
                                aVar.b();
                            } else {
                                aVar.a(a.a(a.this, context, (List) campaignUnit.getAds()));
                            }
                        }
                    }

                    public final void b(CampaignUnit campaignUnit) {
                        if (aVar != null) {
                            if (campaignUnit == null || campaignUnit.getJmDo() != a.f2633a) {
                                aVar.a();
                            } else {
                                aVar.b();
                            }
                        }
                    }
                });
                Looper.loop();
            }
        });
    }

    public final void a(Context context, CampaignEx campaignEx, com.mintegral.msdk.c.a.a aVar) {
        a(context, d, aVar);
        a(context, campaignEx, 1);
    }

    public final void a(Context context, CampaignEx campaignEx, int i) {
        com.mintegral.msdk.b.b.a();
        com.mintegral.msdk.b.a b2 = com.mintegral.msdk.b.b.b(com.mintegral.msdk.base.controller.a.d().j());
        if (b2 == null) {
            com.mintegral.msdk.b.b.a();
            b2 = com.mintegral.msdk.b.b.b();
        }
        if (this.f == null) {
            this.f = new com.mintegral.msdk.click.a(context, b2.j());
        }
        if (i == 1) {
            this.f.b(campaignEx);
            return;
        }
        if (i == 2) {
            this.f.a(campaignEx);
        }
    }

    static /* synthetic */ List a(a aVar, Context context, List list) {
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < list.size(); i++) {
            CampaignEx campaignEx = (CampaignEx) list.get(i);
            if (campaignEx != null && (!k.a(context, campaignEx.getPackageName()) || k.a(campaignEx))) {
                arrayList.add(campaignEx);
                if (campaignEx != null && campaignEx.getLinkType() == 3) {
                    switch (campaignEx.getJmPd()) {
                        case 0:
                            break;
                        case 1:
                            aVar.a(context, campaignEx, 2);
                            break;
                        default:
                            if (c.p(context) != 9) {
                                break;
                            } else {
                                aVar.a(context, campaignEx, 2);
                                break;
                            }
                    }
                }
            }
        }
        return arrayList;
    }
}
