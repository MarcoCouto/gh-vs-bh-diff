package com.mintegral.msdk.base.webview;

import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.mintegral.msdk.base.fragment.BaseFragment;
import com.mintegral.msdk.base.utils.j;
import com.mintegral.msdk.base.webview.BrowserView.a;

public class WebViewFragment extends BaseFragment {
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        BrowserView browserView = new BrowserView(getActivity());
        browserView.loadUrl(getActivity().getIntent().getExtras().getString(NotificationCompat.CATEGORY_MESSAGE));
        browserView.setListener(new a() {
            public final boolean b(String str) {
                return false;
            }

            public final void a() {
                WebViewFragment.this.getActivity().finish();
            }

            public final void a(String str) {
                if (j.a.a(str) && j.a.a(WebViewFragment.this.getActivity().getApplicationContext(), str)) {
                    WebViewFragment.this.getActivity().finish();
                }
            }
        });
        return browserView;
    }
}
