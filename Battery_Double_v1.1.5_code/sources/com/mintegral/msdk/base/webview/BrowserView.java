package com.mintegral.msdk.base.webview;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.DownloadListener;
import android.webkit.JsPromptResult;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebView.HitTestResult;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.utils.c;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.base.utils.j;
import com.mintegral.msdk.base.utils.k;

public class BrowserView extends LinearLayout {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public String f2623a;
    /* access modifiers changed from: private */
    public ProgressBar b;
    /* access modifiers changed from: private */
    public WebView c;
    /* access modifiers changed from: private */
    public ToolBar d;
    /* access modifiers changed from: private */
    public a e;
    private CampaignEx f;

    public static final class MTGDownloadListener implements DownloadListener {
        private CampaignEx campaignEx;
        private String title;

        public MTGDownloadListener(CampaignEx campaignEx2) {
            this.campaignEx = campaignEx2;
        }

        public MTGDownloadListener() {
        }

        public final void setTitle(String str) {
            this.title = str;
        }

        public final void onDownloadStart(String str, String str2, String str3, String str4, long j) {
            j.a(this.title, str, this.campaignEx);
        }
    }

    public interface a {
        void a();

        void a(String str);

        boolean b(String str);
    }

    public BrowserView(Context context, CampaignEx campaignEx) {
        super(context);
        this.f = campaignEx;
        init();
    }

    public BrowserView(Context context) {
        super(context);
        init();
    }

    public BrowserView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init();
    }

    public void setListener(a aVar) {
        this.e = aVar;
    }

    public void loadUrl(String str) {
        this.c.loadUrl(str);
    }

    public void setWebView(WebView webView) {
        this.c = webView;
    }

    public void init() {
        WebChromeClient webChromeClient;
        setOrientation(1);
        setGravity(17);
        this.b = new ProgressBar(getContext());
        this.b.setLayoutParams(new LayoutParams(-1, 4));
        try {
            if (this.c == null) {
                WebView webView = new WebView(getContext());
                webView.getSettings().setJavaScriptEnabled(true);
                webView.getSettings().setCacheMode(-1);
                webView.setDownloadListener(new MTGDownloadListener(this.f));
                webView.setWebViewClient(new WebViewClient() {
                    public final void onPageStarted(WebView webView, String str, Bitmap bitmap) {
                        StringBuilder sb = new StringBuilder("开始! = ");
                        sb.append(str);
                        g.b("BrowserView", sb.toString());
                        BrowserView.this.f2623a = str;
                        if (BrowserView.this.e != null) {
                            BrowserView.this.e.a(str);
                        }
                        BrowserView.this.b.setVisible(true);
                        BrowserView.this.b.setProgressState(5);
                    }

                    public final boolean shouldOverrideUrlLoading(WebView webView, String str) {
                        StringBuilder sb = new StringBuilder("js大跳! = ");
                        sb.append(str);
                        g.b("BrowserView", sb.toString());
                        BrowserView.this.d.getItem("backward").setEnabled(true);
                        BrowserView.this.d.getItem("forward").setEnabled(false);
                        if (BrowserView.this.e != null) {
                            BrowserView.this.e.b(str);
                        }
                        HitTestResult hitTestResult = webView.getHitTestResult();
                        if (hitTestResult != null && hitTestResult.getType() == 7) {
                            g.b("BrowserView", "hint");
                        }
                        return false;
                    }
                });
                if (c.j() <= 10) {
                    webChromeClient = new WebChromeClient() {
                        public final boolean onJsAlert(WebView webView, String str, String str2, JsResult jsResult) {
                            return true;
                        }

                        public final boolean onJsConfirm(WebView webView, String str, String str2, JsResult jsResult) {
                            return true;
                        }

                        public final boolean onJsPrompt(WebView webView, String str, String str2, String str3, JsPromptResult jsPromptResult) {
                            return true;
                        }

                        public final void onProgressChanged(WebView webView, int i) {
                            if (i == 100) {
                                BrowserView.this.b.setProgressState(7);
                                new Handler().postDelayed(new Runnable() {
                                    public final void run() {
                                        BrowserView.this.b.setVisible(false);
                                    }
                                }, 200);
                            }
                        }
                    };
                } else {
                    webChromeClient = new WebChromeClient() {
                        public final void onProgressChanged(WebView webView, int i) {
                            if (i == 100) {
                                BrowserView.this.b.setProgressState(7);
                                new Handler().postDelayed(new Runnable() {
                                    public final void run() {
                                        BrowserView.this.b.setVisible(false);
                                    }
                                }, 200);
                            }
                        }
                    };
                }
                webView.setWebChromeClient(webChromeClient);
                this.c = webView;
            }
            LayoutParams layoutParams = new LayoutParams(-1, -1);
            layoutParams.weight = 1.0f;
            this.c.setLayoutParams(layoutParams);
        } catch (Throwable th) {
            g.c("BrowserView", "webview is error", th);
        }
        this.d = new ToolBar(getContext());
        this.d.setLayoutParams(new LayoutParams(-1, k.b(getContext(), 40.0f)));
        this.d.setBackgroundColor(-1);
        this.b.initResource(true);
        addView(this.b);
        addView(this.c);
        addView(this.d);
        this.d.getItem("backward").setEnabled(false);
        this.d.getItem("forward").setEnabled(false);
        this.d.setOnItemClickListener(new OnClickListener() {
            public final void onClick(View view) {
                BrowserView.this.c.stopLoading();
                String str = (String) view.getTag();
                if (TextUtils.equals(str, "backward")) {
                    BrowserView.this.d.getItem("forward").setEnabled(true);
                    if (BrowserView.this.c.canGoBack()) {
                        BrowserView.this.c.goBack();
                    }
                    BrowserView.this.d.getItem("backward").setEnabled(BrowserView.this.c.canGoBack());
                } else if (TextUtils.equals(str, "forward")) {
                    BrowserView.this.d.getItem("backward").setEnabled(true);
                    if (BrowserView.this.c.canGoForward()) {
                        BrowserView.this.c.goForward();
                    }
                    BrowserView.this.d.getItem("forward").setEnabled(BrowserView.this.c.canGoForward());
                } else if (TextUtils.equals(str, "refresh")) {
                    BrowserView.this.d.getItem("backward").setEnabled(BrowserView.this.c.canGoBack());
                    BrowserView.this.d.getItem("forward").setEnabled(BrowserView.this.c.canGoForward());
                    BrowserView.this.c.loadUrl(BrowserView.this.f2623a);
                } else {
                    if (TextUtils.equals(str, "exits") && BrowserView.this.e != null) {
                        BrowserView.this.e.a();
                    }
                }
            }
        });
    }
}
