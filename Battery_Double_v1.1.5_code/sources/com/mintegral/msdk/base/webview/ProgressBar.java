package com.mintegral.msdk.base.webview;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Looper;
import android.util.AttributeSet;
import android.view.View;
import com.mintegral.msdk.base.controller.a;
import com.mintegral.msdk.base.webview.a.C0052a;

public class ProgressBar extends View implements a {

    /* renamed from: a reason: collision with root package name */
    Runnable f2630a = new Runnable() {
        public final void run() {
            ProgressBar.this.invalidate();
        }
    };
    private Rect b = new Rect();
    private float c;
    private float d = 0.95f;
    private long e;
    private float f;
    private boolean g;
    private float h;
    private float i;
    private float j;
    private long k;
    private int l;
    private int m;
    private int n;
    private int o;
    private long p = 25;
    private Drawable q;
    private Drawable r;
    private Drawable s;
    private Drawable t;
    private boolean u = false;
    private C0052a v;
    private Handler w = new Handler(Looper.getMainLooper());
    private boolean x;
    private boolean y = false;

    public Bitmap getDrawingCache(boolean z) {
        return null;
    }

    public ProgressBar(Context context) {
        super(context);
        setWillNotDraw(false);
    }

    public ProgressBar(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        setWillNotDraw(false);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0044, code lost:
        if (r10.x != false) goto L_0x0060;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x004d, code lost:
        if (r10.x == false) goto L_0x004f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0055, code lost:
        if (r10.x != false) goto L_0x004f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x005e, code lost:
        if (r10.x != false) goto L_0x0060;
     */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x00a4  */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x0141  */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x0191  */
    /* JADX WARNING: Removed duplicated region for block: B:76:0x01af  */
    /* JADX WARNING: Removed duplicated region for block: B:78:? A[RETURN, SYNTHETIC] */
    public void draw(Canvas canvas) {
        if (!this.u) {
            this.u = true;
        }
        long currentTimeMillis = System.currentTimeMillis();
        long j2 = this.y ? 0 : currentTimeMillis - this.e;
        this.f = Math.abs(((float) j2) / 1000.0f);
        this.e = currentTimeMillis;
        this.k += j2;
        float f2 = 0.4f;
        if (!this.g) {
            if (this.k < 2000) {
                if (this.n != 1) {
                    if (this.m == 1) {
                    }
                    f2 = 0.2f;
                }
                this.j = f2;
                this.i += this.j * this.f;
                if (!this.g && this.i > this.d) {
                    this.i = this.d;
                }
                this.b.right = (int) (this.i * this.c);
                this.w.removeCallbacksAndMessages(null);
                this.w.postDelayed(this.f2630a, this.p);
                super.draw(canvas);
                float f3 = this.f;
                if (this.g) {
                    int i2 = (int) ((1.0f - (this.h / (this.c * 0.5f))) * 255.0f);
                    if (i2 < 0) {
                        i2 = 0;
                    }
                    if (this.h > this.c * 0.5f) {
                        setVisible(false);
                    }
                    if (this.r != null) {
                        this.r.setAlpha(i2);
                    }
                    if (this.s != null) {
                        this.s.setAlpha(i2);
                    }
                    if (this.q != null) {
                        this.q.setAlpha(i2);
                    }
                    canvas.save();
                    canvas.translate(this.h, 0.0f);
                }
                if (!(this.r == null || this.q == null)) {
                    this.r.setBounds(0, 0, (int) (((float) this.b.width()) - (((float) this.q.getIntrinsicWidth()) * 0.05f)), this.r.getIntrinsicHeight());
                    this.r.draw(canvas);
                }
                if (!(!this.g || this.s == null || this.q == null)) {
                    int intrinsicWidth = this.s.getIntrinsicWidth();
                    this.s.setBounds(0, 0, intrinsicWidth, this.s.getIntrinsicHeight());
                    canvas.save();
                    canvas.translate((float) (-intrinsicWidth), 0.0f);
                    this.s.draw(canvas);
                    canvas.restore();
                }
                if (this.q != null) {
                    canvas.save();
                    canvas.translate((float) (this.b.width() - getWidth()), 0.0f);
                    this.q.draw(canvas);
                    canvas.restore();
                }
                if (!this.g && Math.abs(this.i - this.d) < 1.0E-5f && this.t != null) {
                    this.l = (int) (((float) this.l) + (f3 * 0.2f * this.c));
                    if (this.l + this.t.getIntrinsicWidth() >= this.b.width()) {
                        this.l = -this.t.getIntrinsicWidth();
                    }
                    canvas.save();
                    canvas.translate((float) this.l, 0.0f);
                    this.t.draw(canvas);
                    canvas.restore();
                }
                if (this.g) {
                    canvas.restore();
                    return;
                }
                return;
            }
            f2 = 0.05f;
            this.j = f2;
            this.i += this.j * this.f;
            this.i = this.d;
            this.b.right = (int) (this.i * this.c);
            this.w.removeCallbacksAndMessages(null);
            this.w.postDelayed(this.f2630a, this.p);
            super.draw(canvas);
            float f32 = this.f;
            if (this.g) {
            }
            this.r.setBounds(0, 0, (int) (((float) this.b.width()) - (((float) this.q.getIntrinsicWidth()) * 0.05f)), this.r.getIntrinsicHeight());
            this.r.draw(canvas);
            int intrinsicWidth2 = this.s.getIntrinsicWidth();
            this.s.setBounds(0, 0, intrinsicWidth2, this.s.getIntrinsicHeight());
            canvas.save();
            canvas.translate((float) (-intrinsicWidth2), 0.0f);
            this.s.draw(canvas);
            canvas.restore();
            if (this.q != null) {
            }
            this.l = (int) (((float) this.l) + (f32 * 0.2f * this.c));
            if (this.l + this.t.getIntrinsicWidth() >= this.b.width()) {
            }
            canvas.save();
            canvas.translate((float) this.l, 0.0f);
            this.t.draw(canvas);
            canvas.restore();
            if (this.g) {
            }
        }
        f2 = 1.0f;
        this.j = f2;
        this.i += this.j * this.f;
        this.i = this.d;
        this.b.right = (int) (this.i * this.c);
        this.w.removeCallbacksAndMessages(null);
        this.w.postDelayed(this.f2630a, this.p);
        super.draw(canvas);
        float f322 = this.f;
        if (this.g) {
        }
        this.r.setBounds(0, 0, (int) (((float) this.b.width()) - (((float) this.q.getIntrinsicWidth()) * 0.05f)), this.r.getIntrinsicHeight());
        this.r.draw(canvas);
        int intrinsicWidth22 = this.s.getIntrinsicWidth();
        this.s.setBounds(0, 0, intrinsicWidth22, this.s.getIntrinsicHeight());
        canvas.save();
        canvas.translate((float) (-intrinsicWidth22), 0.0f);
        this.s.draw(canvas);
        canvas.restore();
        if (this.q != null) {
        }
        this.l = (int) (((float) this.l) + (f322 * 0.2f * this.c));
        if (this.l + this.t.getIntrinsicWidth() >= this.b.width()) {
        }
        canvas.save();
        canvas.translate((float) this.l, 0.0f);
        this.t.draw(canvas);
        canvas.restore();
        if (this.g) {
        }
    }

    public void onThemeChange() {
        if (this.u) {
            initResource(true);
        }
    }

    public void initResource(boolean z) {
        if (z || (this.t == null && this.q == null && this.r == null && this.s == null)) {
            this.t = getResources().getDrawable(getResources().getIdentifier("mintegral_cm_highlight", "drawable", a.d().a()));
            if (this.t != null) {
                this.t.setBounds(0, 0, this.t.getIntrinsicWidth(), this.t.getIntrinsicHeight());
            }
            this.q = getResources().getDrawable(getResources().getIdentifier("mintegral_cm_head", "drawable", a.d().a()));
            if (this.q != null) {
                this.q.setBounds(0, 0, this.q.getIntrinsicWidth(), this.q.getIntrinsicHeight());
            }
            this.r = getResources().getDrawable(getResources().getIdentifier("mintegral_cm_tail", "drawable", a.d().a()));
            this.s = getResources().getDrawable(getResources().getIdentifier("mintegral_cm_end_animation", "drawable", a.d().a()));
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        super.onMeasure(i2, i3);
    }

    public void onWindowFocusChanged(boolean z) {
        super.onWindowFocusChanged(z);
        if (this.t != null) {
            Drawable drawable = this.t;
            double intrinsicWidth = (double) this.t.getIntrinsicWidth();
            Double.isNaN(intrinsicWidth);
            drawable.setBounds(0, 0, (int) (intrinsicWidth * 1.5d), getHeight());
        }
        if (this.q != null) {
            this.q.setBounds(0, 0, getWidth(), getHeight());
        }
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        super.onLayout(z, i2, i3, i4, i5);
    }

    public void startEndAnimation() {
        if (!this.g) {
            this.g = true;
            this.h = 0.0f;
        }
    }

    public void setProgress(float f2, boolean z) {
        if (z && f2 >= 1.0f) {
            startEndAnimation();
        }
    }

    public void setVisible(boolean z) {
        if (z) {
            this.x = true;
            this.e = System.currentTimeMillis();
            this.f = 0.0f;
            this.k = 0;
            this.g = false;
            this.h = 0.0f;
            this.i = 0.0f;
            this.c = (float) getMeasuredWidth();
            this.y = false;
            this.m = 0;
            this.n = 0;
            this.o = 0;
            if (this.t != null) {
                this.l = -this.t.getIntrinsicWidth();
            } else {
                this.l = 0;
            }
            if (this.r != null) {
                this.r.setAlpha(255);
            }
            if (this.s != null) {
                this.s.setAlpha(255);
            }
            if (this.q != null) {
                this.q.setAlpha(255);
            }
            setVisibility(0);
            invalidate();
            return;
        }
        setVisibility(4);
    }

    public float getProgress() {
        return this.i;
    }

    public void setProgressBarListener(C0052a aVar) {
        this.v = aVar;
    }

    public void setVisibility(int i2) {
        super.setVisibility(i2);
    }

    public void setProgressState(int i2) {
        switch (i2) {
            case 5:
                this.m = 1;
                this.n = 0;
                this.o = 0;
                this.k = 0;
                return;
            case 6:
                this.n = 1;
                if (this.o == 1) {
                    startEndAnimation();
                }
                this.k = 0;
                return;
            case 7:
                startEndAnimation();
                return;
            case 8:
                this.o = 1;
                if (this.n == 1) {
                    startEndAnimation();
                    break;
                }
                break;
        }
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i2, int i3, int i4, int i5) {
        super.onSizeChanged(i2, i3, i4, i5);
        this.c = (float) getMeasuredWidth();
    }

    public void setPaused(boolean z) {
        this.y = z;
        if (!z) {
            this.e = System.currentTimeMillis();
        }
    }
}
