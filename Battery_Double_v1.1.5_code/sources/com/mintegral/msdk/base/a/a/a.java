package com.mintegral.msdk.base.a.a;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import com.mintegral.msdk.base.utils.g;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

/* compiled from: SharedPerferenceManager */
public class a {

    /* renamed from: a reason: collision with root package name */
    public static final String f2498a = "a";
    private static a c;
    SharedPreferences b;

    private a() {
    }

    public static a a() {
        if (c == null) {
            synchronized (a.class) {
                if (c == null) {
                    c = new a();
                }
            }
        }
        return c;
    }

    public final void a(String str, String str2) {
        try {
            Context h = com.mintegral.msdk.base.controller.a.d().h();
            if (h == null) {
                g.d(f2498a, "context is null in put");
                return;
            }
            if (this.b == null && h != null) {
                this.b = h.getSharedPreferences("mintegral", 0);
            }
            Editor edit = this.b.edit();
            edit.putString(str, str2);
            edit.apply();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public final String a(String str) {
        try {
            Context h = com.mintegral.msdk.base.controller.a.d().h();
            if (h == null) {
                g.d(f2498a, "context is null in get");
                return null;
            }
            if (this.b == null && h != null) {
                this.b = h.getSharedPreferences("mintegral", 0);
            }
            return this.b.getString(str, "");
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public final List<String> b() {
        ArrayList arrayList = new ArrayList();
        Context h = com.mintegral.msdk.base.controller.a.d().h();
        if (h == null) {
            g.d(f2498a, "context is null in get");
            return null;
        }
        if (this.b == null && h != null) {
            this.b = h.getSharedPreferences("mintegral", 0);
        }
        for (Entry key : this.b.getAll().entrySet()) {
            arrayList.add(key.getKey());
        }
        return arrayList;
    }
}
