package com.mintegral.msdk.base.common.net;

import com.mintegral.msdk.base.utils.g;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.message.BasicHeader;

/* compiled from: CommonHttpMultipartEntity */
final class e implements HttpEntity {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public static final byte[] f2556a = "\r\n".getBytes();
    /* access modifiers changed from: private */
    public static final byte[] b = "Content-Transfer-Encoding: 8bit\r\n".getBytes();
    private String c = UUID.randomUUID().toString();
    /* access modifiers changed from: private */
    public byte[] d;
    private byte[] e;
    private ByteArrayOutputStream f = new ByteArrayOutputStream();
    private List<a> g = new ArrayList();
    private g h;
    private int i;
    private int j;

    /* compiled from: CommonHttpMultipartEntity */
    private class a {
        private byte[] b;
        private File c;

        public a(String str, File file, String str2, String str3) {
            this.b = a(str, str2, str3);
            this.c = file;
        }

        private byte[] a(String str, String str2, String str3) {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            try {
                byteArrayOutputStream.write(e.this.d);
                byteArrayOutputStream.write(e.c(str, str2));
                byteArrayOutputStream.write(e.b(str3));
                byteArrayOutputStream.write(e.b);
                byteArrayOutputStream.write(e.f2556a);
            } catch (IOException e) {
                g.c("HttpMultipartEntity", "FileParam createHeader to RequestParamBufferStream exception", e);
            }
            return byteArrayOutputStream.toByteArray();
        }

        public final long a() {
            return ((long) this.b.length) + this.c.length() + ((long) e.f2556a.length);
        }

        /* JADX WARNING: Removed duplicated region for block: B:25:0x0059 A[SYNTHETIC, Splitter:B:25:0x0059] */
        /* JADX WARNING: Removed duplicated region for block: B:29:0x005f A[SYNTHETIC, Splitter:B:29:0x005f] */
        public final void a(OutputStream outputStream) throws IOException {
            outputStream.write(this.b);
            e.this.a(this.b.length);
            FileInputStream fileInputStream = null;
            try {
                FileInputStream fileInputStream2 = new FileInputStream(this.c);
                try {
                    byte[] bArr = new byte[4096];
                    while (true) {
                        int read = fileInputStream2.read(bArr);
                        if (read != -1) {
                            outputStream.write(bArr, 0, read);
                            e.this.a(read);
                        } else {
                            outputStream.write(e.f2556a);
                            e.this.a(e.f2556a.length);
                            outputStream.flush();
                            try {
                                fileInputStream2.close();
                                return;
                            } catch (IOException e) {
                                g.c("HttpMultipartEntity", "Cannot close input stream", e);
                                return;
                            }
                        }
                    }
                } catch (Throwable th) {
                    th = th;
                    if (fileInputStream2 != null) {
                    }
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                th.printStackTrace();
                if (fileInputStream != null) {
                }
            }
        }
    }

    public final void consumeContent() throws IOException {
    }

    public final Header getContentEncoding() {
        return null;
    }

    public final boolean isChunked() {
        return false;
    }

    public final boolean isRepeatable() {
        return true;
    }

    public final boolean isStreaming() {
        return false;
    }

    e(g gVar) {
        StringBuilder sb = new StringBuilder("--");
        sb.append(this.c);
        sb.append("\r\n");
        this.d = sb.toString().getBytes();
        StringBuilder sb2 = new StringBuilder("--");
        sb2.append(this.c);
        sb2.append("--\r\n");
        this.e = sb2.toString().getBytes();
        this.h = gVar;
    }

    /* access modifiers changed from: 0000 */
    public final void a(String str, File file, String str2, String str3) {
        List<a> list = this.g;
        a aVar = new a(str, file, str2, str3);
        list.add(aVar);
    }

    /* access modifiers changed from: 0000 */
    public final void a(String str, String str2, InputStream inputStream, String str3) throws IOException {
        this.f.write(this.d);
        this.f.write(c(str, str2));
        this.f.write(b(str3));
        this.f.write(b);
        this.f.write(f2556a);
        byte[] bArr = new byte[4096];
        while (true) {
            int read = inputStream.read(bArr);
            if (read != -1) {
                this.f.write(bArr, 0, read);
            } else {
                this.f.write(f2556a);
                this.f.flush();
                try {
                    inputStream.close();
                    return;
                } catch (IOException e2) {
                    g.c("HttpMultipartEntity", "Cannot close input stream", e2);
                    return;
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public static byte[] b(String str) {
        StringBuilder sb = new StringBuilder("Content-Type: ");
        sb.append(str);
        sb.append("\r\n");
        return sb.toString().getBytes();
    }

    /* access modifiers changed from: private */
    public static byte[] c(String str, String str2) {
        StringBuilder sb = new StringBuilder("Content-Disposition: form-data; name=\"");
        sb.append(str);
        sb.append("\"; filename=\"");
        sb.append(str2);
        sb.append("\"\r\n");
        return sb.toString().getBytes();
    }

    /* access modifiers changed from: private */
    public void a(int i2) {
        this.i += i2;
        if (this.h != null) {
            this.h.a(this.i, this.j);
        }
    }

    public final long getContentLength() {
        long size = (long) this.f.size();
        for (a a2 : this.g) {
            long a3 = a2.a();
            if (a3 < 0) {
                g.d("HttpMultipartEntity", "get FileParam length failed.");
                return -1;
            }
            size += a3;
        }
        return size + ((long) this.e.length);
    }

    public final Header getContentType() {
        StringBuilder sb = new StringBuilder("multipart/form-data; boundary=");
        sb.append(this.c);
        return new BasicHeader("Content-Type", sb.toString());
    }

    public final void writeTo(OutputStream outputStream) throws IOException {
        this.i = 0;
        this.j = (int) getContentLength();
        this.f.writeTo(outputStream);
        a(this.f.size());
        for (a a2 : this.g) {
            a2.a(outputStream);
        }
        outputStream.write(this.e);
        a(this.e.length);
    }

    public final InputStream getContent() throws IOException {
        return new ByteArrayInputStream(toString().getBytes());
    }

    /* access modifiers changed from: 0000 */
    public final void a(String str, String str2) {
        String str3 = "text/plain; charset=UTF-8";
        try {
            this.f.write(this.d);
            ByteArrayOutputStream byteArrayOutputStream = this.f;
            StringBuilder sb = new StringBuilder("Content-Disposition: form-data; name=\"");
            sb.append(str);
            sb.append("\"\r\n");
            byteArrayOutputStream.write(sb.toString().getBytes());
            this.f.write(b(str3));
            this.f.write(f2556a);
            this.f.write(str2.getBytes());
            this.f.write(f2556a);
        } catch (IOException e2) {
            g.c("HttpMultipartEntity", "addParam to RequestParamBufferStream exception", e2);
        }
    }
}
