package com.mintegral.msdk.base.common.net;

import android.content.Context;

/* compiled from: CommonBaseAsyncHttpRequest */
public abstract class b extends c {
    public abstract void a();

    public b(Context context) {
        super(context);
        a();
    }

    public b(Context context, int i) {
        super(context, i);
        a();
    }

    /* access modifiers changed from: protected */
    public final void a(d<?> dVar) {
        dVar.e();
    }
}
