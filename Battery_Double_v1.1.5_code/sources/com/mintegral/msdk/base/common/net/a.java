package com.mintegral.msdk.base.common.net;

import android.content.Context;
import com.mintegral.msdk.base.common.e.b;
import java.util.WeakHashMap;

/* compiled from: CommonAsyncHttpRequest */
public class a extends b {
    private static WeakHashMap<Context, b> c = new WeakHashMap<>();

    /* renamed from: a reason: collision with root package name */
    public b f2547a;
    private b d;

    public a(Context context) {
        super(context);
    }

    public a(Context context, int i) {
        super(context, i);
    }

    /* access modifiers changed from: protected */
    public void a(l lVar) {
        super.a(lVar);
    }

    public final void a() {
        if (c.get(this.b) != null) {
            this.d = (b) c.get(this.b);
            return;
        }
        this.d = this.f2547a == null ? new b(this.b, 5) : this.f2547a;
        c.put(this.b, this.d);
    }

    /* access modifiers changed from: protected */
    public final void a(f fVar) {
        this.d.b(fVar);
    }

    public static void b() {
        if (c != null) {
            c.clear();
        }
    }
}
