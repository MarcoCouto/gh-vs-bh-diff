package com.mintegral.msdk.base.common.net.a;

import android.content.Context;
import android.os.Build.VERSION;
import android.text.TextUtils;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.base.common.net.l;
import com.mintegral.msdk.base.controller.authoritycontroller.a;
import com.mintegral.msdk.base.utils.b;
import com.mintegral.msdk.base.utils.c;
import com.mintegral.msdk.base.utils.i;
import com.mintegral.msdk.base.utils.k;
import com.tapjoy.TapjoyConstants;
import org.json.JSONObject;

/* compiled from: CommonRequestParamsForAdd */
public final class d {

    /* renamed from: a reason: collision with root package name */
    private static String f2549a = "";
    private static String b = "";

    public static void a(l lVar, Context context) {
        a.a();
        if (a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA)) {
            StringBuilder sb = new StringBuilder();
            sb.append(i.c());
            lVar.a("cache1", sb.toString());
            StringBuilder sb2 = new StringBuilder();
            sb2.append(i.a());
            lVar.a("cache2", sb2.toString());
            StringBuilder sb3 = new StringBuilder();
            sb3.append(b.a());
            lVar.a("power_rate", sb3.toString());
            StringBuilder sb4 = new StringBuilder();
            sb4.append(b.b());
            lVar.a("charging", sb4.toString());
            StringBuilder sb5 = new StringBuilder();
            sb5.append(k.a(context, "com.tencent.mm"));
            lVar.a("has_wx", sb5.toString());
        }
        lVar.a("pkg_source", c.a(c.n(context), context));
        if (VERSION.SDK_INT > 18) {
            lVar.a("http_req", "2");
        }
        b(lVar);
    }

    public static void a(l lVar) {
        lVar.a("api_version", "1.7");
    }

    public static void c(l lVar) {
        try {
            if (TextUtils.isEmpty(com.mintegral.msdk.base.common.a.D)) {
                com.mintegral.msdk.base.common.a.D = com.mintegral.msdk.base.a.a.a.a().a("sys_id");
            }
            if (!TextUtils.isEmpty(com.mintegral.msdk.base.common.a.D)) {
                lVar.a("sys_id", com.mintegral.msdk.base.common.a.D);
            }
            if (TextUtils.isEmpty(com.mintegral.msdk.base.common.a.E)) {
                com.mintegral.msdk.base.common.a.E = com.mintegral.msdk.base.a.a.a.a().a("bkup_id");
            }
            if (!TextUtils.isEmpty(com.mintegral.msdk.base.common.a.E)) {
                lVar.a("bkup_id", com.mintegral.msdk.base.common.a.E);
            }
        } catch (Exception e) {
            if (MIntegralConstans.DEBUG) {
                e.printStackTrace();
            }
        }
    }

    public static void d(l lVar) {
        if (lVar != null) {
            a.a();
            if (!a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA)) {
                lVar.a("model");
                lVar.a("brand");
                lVar.a("screen_size");
                lVar.a("cache1");
                lVar.a("cache2");
                lVar.a("power_rate");
                lVar.a("charging");
                lVar.a("sub_ip");
                lVar.a("network_type");
                lVar.a("useragent");
                lVar.a("ua");
                lVar.a("language");
                lVar.a(TapjoyConstants.TJC_DEVICE_TIMEZONE);
                lVar.a("network_str");
                lVar.a(RequestParameters.NETWORK_MNC);
                lVar.a(RequestParameters.NETWORK_MCC);
                lVar.a(TapjoyConstants.TJC_DEVICE_OS_VERSION_NAME);
                lVar.a("gp_version");
            }
            a.a();
            if (!a.a(MIntegralConstans.AUTHORITY_DEVICE_ID)) {
                lVar.a("gaid");
            }
        }
    }

    public static void b(l lVar) {
        Context h = com.mintegral.msdk.base.controller.a.d().h();
        com.mintegral.msdk.b.b.a();
        com.mintegral.msdk.b.a b2 = com.mintegral.msdk.b.b.b(com.mintegral.msdk.base.controller.a.d().j());
        if (b2 != null) {
            JSONObject jSONObject = new JSONObject();
            try {
                if (b2.as() == 1) {
                    if (c.b(h) != null) {
                        a.a();
                        if (a.a(MIntegralConstans.AUTHORITYIMEIMAC)) {
                            jSONObject.put("imei", c.b(h));
                        }
                    }
                    if (c.h(h) != null) {
                        a.a();
                        if (a.a(MIntegralConstans.AUTHORITYIMEIMAC)) {
                            jSONObject.put("mac", c.h(h));
                        }
                    }
                }
                if (b2.au() == 1 && c.d(h) != null) {
                    a.a();
                    if (a.a(MIntegralConstans.AUTHORITY_ANDROID_ID)) {
                        jSONObject.put(TapjoyConstants.TJC_ANDROID_ID, c.d(h));
                    }
                }
                if (b2.aN() == 1) {
                    a.a();
                    a.a(MIntegralConstans.AUTHORITY_GPS);
                }
                a.a();
                if (a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA)) {
                    StringBuilder sb = new StringBuilder();
                    sb.append(c.n());
                    jSONObject.put("cid", sb.toString());
                }
                if (!TextUtils.isEmpty(jSONObject.toString())) {
                    if (!jSONObject.equals(f2549a)) {
                        b = com.mintegral.msdk.base.utils.a.b(jSONObject.toString());
                    }
                    if (!TextUtils.isEmpty(b)) {
                        lVar.a("dvi", b);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        int g = c.g();
        if (g != -1) {
            lVar.a("unknown_source", String.valueOf(g));
        }
        c(lVar);
    }
}
