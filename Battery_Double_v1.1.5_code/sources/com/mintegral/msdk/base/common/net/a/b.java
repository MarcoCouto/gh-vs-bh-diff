package com.mintegral.msdk.base.common.net.a;

import com.mintegral.msdk.base.common.net.d;
import com.mintegral.msdk.base.utils.g;
import org.apache.http.HttpEntity;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: CommonJSONObjectResponseHandler */
public abstract class b extends d<JSONObject> {
    /* access modifiers changed from: private */
    /* renamed from: e */
    public JSONObject b(HttpEntity httpEntity) throws Exception {
        try {
            return a(httpEntity);
        } catch (JSONException e) {
            g.a("JSONObjectResponseHandler", (Throwable) e);
            return null;
        }
    }
}
