package com.mintegral.msdk.base.common.net;

import android.content.Context;
import android.text.TextUtils;
import com.mintegral.msdk.base.common.e.a.C0049a;
import com.mintegral.msdk.base.utils.g;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.net.ConnectException;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.zip.GZIPInputStream;
import org.apache.http.Header;
import org.apache.http.HeaderElement;
import org.apache.http.HttpEntity;
import org.apache.http.HttpRequest;
import org.apache.http.HttpRequestInterceptor;
import org.apache.http.HttpResponse;
import org.apache.http.HttpResponseInterceptor;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.HttpEntityWrapper;
import org.apache.http.protocol.HttpContext;
import org.json.JSONObject;

/* compiled from: CommonHttpRequest */
public final class f extends com.mintegral.msdk.base.common.e.a {

    /* renamed from: a reason: collision with root package name */
    private int f2558a;
    private HttpRequestBase b;
    private g c;
    private k d;
    private Object e = new Object();
    private Thread f;
    private boolean g;
    private h k;
    private WeakReference<Context> l;

    /* compiled from: CommonHttpRequest */
    public static class a {
        private static final SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS", Locale.getDefault());

        /* renamed from: a reason: collision with root package name */
        String f2559a = "";
        long b = 0;
        long c = 0;
        long d = 0;
        long e = 0;

        public final String toString() {
            try {
                if (!TextUtils.isEmpty(this.f2559a)) {
                    JSONObject jSONObject = new JSONObject();
                    jSONObject.put("url", this.f2559a);
                    jSONObject.put("first_data_cost", this.b);
                    jSONObject.put("total_data", this.c);
                    jSONObject.put("read_cost", this.d);
                    jSONObject.put("total_cost", this.e);
                    jSONObject.put("record_time", f.format(Calendar.getInstance().getTime()));
                    return jSONObject.toString();
                }
            } catch (Exception unused) {
            }
            return super.toString();
        }
    }

    /* compiled from: CommonHttpRequest */
    private class b implements HttpRequestInterceptor {
        private b() {
        }

        /* synthetic */ b(f fVar, byte b) {
            this();
        }

        public final void process(HttpRequest httpRequest, HttpContext httpContext) {
            if (!httpRequest.containsHeader(io.fabric.sdk.android.services.network.HttpRequest.HEADER_ACCEPT_ENCODING)) {
                httpRequest.addHeader(io.fabric.sdk.android.services.network.HttpRequest.HEADER_ACCEPT_ENCODING, io.fabric.sdk.android.services.network.HttpRequest.ENCODING_GZIP);
            }
        }
    }

    /* compiled from: CommonHttpRequest */
    private class c implements HttpResponseInterceptor {
        private a b;

        /* compiled from: CommonHttpRequest */
        private class a extends HttpEntityWrapper {
            private GZIPInputStream b = null;

            public final long getContentLength() {
                return -1;
            }

            public a(HttpEntity httpEntity) {
                super(httpEntity);
            }

            public final InputStream getContent() throws IOException {
                if (this.b == null) {
                    this.b = new GZIPInputStream(this.wrappedEntity.getContent());
                }
                return this.b;
            }

            public final void a() {
                try {
                    this.b.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        private c() {
            this.b = null;
        }

        /* synthetic */ c(f fVar, byte b2) {
            this();
        }

        public final void process(HttpResponse httpResponse, HttpContext httpContext) {
            HttpEntity entity = httpResponse.getEntity();
            if (entity != null) {
                Header contentEncoding = entity.getContentEncoding();
                if (contentEncoding != null) {
                    for (HeaderElement name : contentEncoding.getElements()) {
                        if (name.getName().equalsIgnoreCase(io.fabric.sdk.android.services.network.HttpRequest.ENCODING_GZIP)) {
                            if (this.b == null) {
                                this.b = new a(entity);
                            }
                            httpResponse.setEntity(this.b);
                            return;
                        }
                    }
                }
            }
        }

        public final a a() {
            return this.b;
        }
    }

    /* compiled from: CommonHttpRequest */
    static class d {

        /* renamed from: a reason: collision with root package name */
        int f2563a;
        Header[] b;
        Object c;

        d() {
        }
    }

    f(Context context, HttpRequestBase httpRequestBase, g gVar, int i, int i2, int i3, boolean z) {
        this.l = new WeakReference<>(context);
        this.k = h.a(context.getApplicationContext());
        this.k.a();
        this.b = httpRequestBase;
        this.c = gVar;
        this.f2558a = i;
        this.d = new k(i2, i3);
        this.g = z;
    }

    /* JADX INFO: used method not loaded: com.mintegral.msdk.base.common.net.g.b(int):null, types can be incorrect */
    /* JADX WARNING: Code restructure failed: missing block: B:118:0x01de, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:119:0x01df, code lost:
        r7 = 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:122:0x01e4, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:141:0x0209, code lost:
        r8.a();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:146:0x021a, code lost:
        r1.c.b(5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:149:0x022a, code lost:
        r1.c.b(5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:150:0x022f, code lost:
        r0 = 5;
        r6 = -2;
        r7 = 1;
        r9 = 0;
        r11 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:151:0x0236, code lost:
        r21 = r2;
        r0 = 5;
        r11 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:208:0x0308, code lost:
        r8.a();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:216:0x0320, code lost:
        r1.c.b(r7);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:219:0x032e, code lost:
        r1.c.b(5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:221:0x0335, code lost:
        r7 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:95:0x0195, code lost:
        r13 = r6;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:66:0x0123 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:83:0x0161 */
    /* JADX WARNING: Removed duplicated region for block: B:104:0x01a5  */
    /* JADX WARNING: Removed duplicated region for block: B:116:0x01d2  */
    /* JADX WARNING: Removed duplicated region for block: B:117:0x01da  */
    /* JADX WARNING: Removed duplicated region for block: B:118:0x01de A[ExcHandler: all (th java.lang.Throwable), PHI: r12 
  PHI: (r12v11 java.lang.Object) = (r12v1 java.lang.Object), (r12v1 java.lang.Object), (r12v1 java.lang.Object), (r12v1 java.lang.Object), (r12v1 java.lang.Object), (r12v1 java.lang.Object), (r12v1 java.lang.Object), (r12v20 java.lang.Object), (r12v1 java.lang.Object), (r12v22 java.lang.Object), (r12v22 java.lang.Object), (r12v22 java.lang.Object), (r12v22 java.lang.Object) binds: [B:13:0x0071, B:14:?, B:19:0x007c, B:43:0x00c7, B:46:0x00e4, B:47:?, B:59:0x0110, B:83:0x0161, B:63:0x011c, B:66:0x0123, B:74:0x013d, B:79:0x0151, B:80:?] A[DONT_GENERATE, DONT_INLINE], Splitter:B:59:0x0110] */
    /* JADX WARNING: Removed duplicated region for block: B:121:? A[ExcHandler: OutOfMemoryError (unused java.lang.OutOfMemoryError), SYNTHETIC, Splitter:B:13:0x0071] */
    /* JADX WARNING: Removed duplicated region for block: B:122:0x01e4 A[ExcHandler: Exception (e java.lang.Exception), Splitter:B:13:0x0071] */
    /* JADX WARNING: Removed duplicated region for block: B:141:0x0209  */
    /* JADX WARNING: Removed duplicated region for block: B:144:0x0212  */
    /* JADX WARNING: Removed duplicated region for block: B:149:0x022a  */
    /* JADX WARNING: Removed duplicated region for block: B:151:0x0236  */
    /* JADX WARNING: Removed duplicated region for block: B:161:0x024e A[SYNTHETIC, Splitter:B:161:0x024e] */
    /* JADX WARNING: Removed duplicated region for block: B:174:0x0299 A[SYNTHETIC, Splitter:B:174:0x0299] */
    /* JADX WARNING: Removed duplicated region for block: B:185:0x02bf  */
    /* JADX WARNING: Removed duplicated region for block: B:187:0x02c4 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:189:0x02c8  */
    /* JADX WARNING: Removed duplicated region for block: B:196:0x02e4  */
    /* JADX WARNING: Removed duplicated region for block: B:208:0x0308  */
    /* JADX WARNING: Removed duplicated region for block: B:212:0x0311  */
    /* JADX WARNING: Removed duplicated region for block: B:219:0x032e  */
    /* JADX WARNING: Removed duplicated region for block: B:224:0x033c  */
    /* JADX WARNING: Removed duplicated region for block: B:226:0x0340  */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x0131 A[Catch:{ Exception -> 0x0197, OutOfMemoryError -> 0x0194, all -> 0x01de }] */
    /* JADX WARNING: Removed duplicated region for block: B:86:0x0177 A[Catch:{ Exception -> 0x0192, OutOfMemoryError -> 0x0195, all -> 0x01de }] */
    /* JADX WARNING: Removed duplicated region for block: B:87:0x017b A[Catch:{ Exception -> 0x0192, OutOfMemoryError -> 0x0195, all -> 0x01de }] */
    /* JADX WARNING: Removed duplicated region for block: B:94:? A[ExcHandler: OutOfMemoryError (unused java.lang.OutOfMemoryError), PHI: r12 
  PHI: (r12v17 java.lang.Object) = (r12v1 java.lang.Object), (r12v1 java.lang.Object), (r12v1 java.lang.Object), (r12v1 java.lang.Object), (r12v22 java.lang.Object), (r12v1 java.lang.Object) binds: [B:46:0x00e4, B:47:?, B:59:0x0110, B:63:0x011c, B:66:0x0123, B:52:0x00fe] A[DONT_GENERATE, DONT_INLINE], SYNTHETIC, Splitter:B:59:0x0110] */
    /* JADX WARNING: Removed duplicated region for block: B:96:? A[ExcHandler: OutOfMemoryError (unused java.lang.OutOfMemoryError), PHI: r12 
  PHI: (r12v18 java.lang.Object) = (r12v20 java.lang.Object), (r12v22 java.lang.Object), (r12v22 java.lang.Object), (r12v22 java.lang.Object) binds: [B:83:0x0161, B:74:0x013d, B:79:0x0151, B:80:?] A[DONT_GENERATE, DONT_INLINE], SYNTHETIC, Splitter:B:74:0x013d] */
    private d e() {
        int i;
        c cVar;
        i iVar;
        a aVar;
        int i2;
        boolean z;
        HttpResponse httpResponse;
        a aVar2 = new a();
        long nanoTime = System.nanoTime();
        this.f = Thread.currentThread();
        d dVar = new d();
        int i3 = -2;
        if (!f()) {
            dVar.f2563a = -2;
            return dVar;
        }
        if (this.l.get() != null) {
            this.c.b();
        }
        g.a("CommonHttpRequest", "request is started");
        this.d.a();
        int i4 = 1;
        byte b2 = 0;
        long j = nanoTime;
        int i5 = 0;
        boolean z2 = true;
        Object obj = null;
        while (true) {
            if (!z2) {
                break;
            }
            try {
                iVar = new i(this.b.getURI().toString(), this.f2558a, this.k.c());
                try {
                    aVar2.f2559a = this.b.getURI().toString();
                    iVar.addRequestInterceptor(new b(this, b2));
                    cVar = new c(this, b2);
                    try {
                        iVar.addResponseInterceptor(cVar);
                        try {
                            httpResponse = iVar.execute(this.b);
                        } catch (Throwable unused) {
                            httpResponse = null;
                        }
                        if (!this.k.b() || httpResponse == null) {
                            i5 = 7;
                        } else if (!f()) {
                            dVar.f2563a = i3;
                            if (cVar.a() != null) {
                                cVar.a().a();
                            }
                            iVar.a();
                            if (!z2 || obj == null) {
                                if (!z2 && f() && this.l.get() != null) {
                                    this.c.b(i);
                                } else if (this.l.get() == null) {
                                    this.c.b(5);
                                }
                                return dVar;
                            }
                        } else {
                            int statusCode = httpResponse.getStatusLine().getStatusCode();
                            aVar2.b = (System.nanoTime() - j) / 1000000;
                            long nanoTime2 = System.nanoTime();
                            String str = "CommonHttpRequest";
                            try {
                                StringBuilder sb = new StringBuilder("request status code ");
                                sb.append(statusCode);
                                g.a(str, sb.toString());
                                if (statusCode == 200 || statusCode == 206) {
                                    try {
                                        Header[] allHeaders = httpResponse.getAllHeaders();
                                        HttpEntity entity = httpResponse.getEntity();
                                        if (entity != null) {
                                            dVar.b = allHeaders;
                                            obj = this.c.b(entity);
                                            aVar2.c = entity.getContentLength();
                                            if (aVar2.c < 0) {
                                                Header[] headers = httpResponse.getHeaders(io.fabric.sdk.android.services.network.HttpRequest.HEADER_CONTENT_LENGTH);
                                                if (headers != null && headers.length > 0) {
                                                    Header header = headers[0];
                                                    String value = header.getValue();
                                                    if (!TextUtils.isEmpty(value) && value.matches("[0-9]+")) {
                                                        aVar2.c = (long) Integer.parseInt(header.getValue());
                                                    }
                                                }
                                            }
                                        } else {
                                            obj = null;
                                        }
                                        aVar2.d = (System.nanoTime() - nanoTime2) / 1000000;
                                        aVar2.e = (System.nanoTime() - nanoTime) / 1000000;
                                        if (obj == null) {
                                            if (this.l.get() != null) {
                                                this.c.a(allHeaders, obj);
                                            } else {
                                                this.c.b(5);
                                            }
                                            j = nanoTime2;
                                            i5 = 1;
                                            if (cVar.a() != null) {
                                                cVar.a().a();
                                            }
                                            iVar.a();
                                            if (!z2 && obj != null) {
                                                break;
                                            } else if (z2 && f() && this.l.get() != null) {
                                                this.c.b(i5);
                                                i3 = -2;
                                                i4 = 1;
                                                b2 = 0;
                                            } else if (this.l.get() != null) {
                                                this.c.b(5);
                                                i3 = -2;
                                                i4 = 1;
                                                b2 = 0;
                                            } else {
                                                aVar = aVar2;
                                                aVar2 = aVar;
                                                i3 = -2;
                                                i4 = 1;
                                                b2 = 0;
                                            }
                                        } else {
                                            j = nanoTime2;
                                            i5 = 8;
                                        }
                                    } catch (Exception e2) {
                                        e = e2;
                                        j = nanoTime2;
                                        i2 = 1;
                                        try {
                                            a a2 = this.d.a(e);
                                            i = a2.f2569a;
                                            try {
                                                z = a2.b;
                                                if (!z) {
                                                }
                                                aVar = aVar2;
                                                z2 = z;
                                                if (z2) {
                                                }
                                                cVar.a().a();
                                                if (iVar != null) {
                                                }
                                                if (z2) {
                                                }
                                                if (z2) {
                                                }
                                                if (this.l.get() == null) {
                                                }
                                                i5 = i;
                                                aVar2 = aVar;
                                                i3 = -2;
                                                i4 = 1;
                                                b2 = 0;
                                            } catch (Throwable th) {
                                                th = th;
                                                cVar.a().a();
                                                if (iVar != null) {
                                                }
                                                if (!z2) {
                                                }
                                                if (this.l.get() == null) {
                                                }
                                                throw th;
                                            }
                                        } catch (Throwable th2) {
                                            th = th2;
                                            i = i2;
                                            cVar.a().a();
                                            if (iVar != null) {
                                            }
                                            if (!z2) {
                                            }
                                            if (this.l.get() == null) {
                                            }
                                            throw th;
                                        }
                                    } catch (OutOfMemoryError unused2) {
                                    } catch (Throwable th3) {
                                    }
                                } else {
                                    try {
                                        throw new ConnectException();
                                    } catch (Exception e3) {
                                        e = e3;
                                        j = nanoTime2;
                                        i2 = 8;
                                        a a22 = this.d.a(e);
                                        i = a22.f2569a;
                                        z = a22.b;
                                        if (!z) {
                                        }
                                        aVar = aVar2;
                                        z2 = z;
                                        if (z2) {
                                        }
                                        cVar.a().a();
                                        if (iVar != null) {
                                        }
                                        if (z2) {
                                        }
                                        if (z2) {
                                        }
                                        if (this.l.get() == null) {
                                        }
                                        i5 = i;
                                        aVar2 = aVar;
                                        i3 = -2;
                                        i4 = 1;
                                        b2 = 0;
                                    } catch (OutOfMemoryError unused3) {
                                    } catch (Throwable th4) {
                                        th = th4;
                                        i = 8;
                                        cVar.a().a();
                                        if (iVar != null) {
                                        }
                                        if (!z2) {
                                        }
                                        if (this.l.get() == null) {
                                        }
                                        throw th;
                                    }
                                }
                            } catch (Exception e4) {
                                e = e4;
                                j = nanoTime2;
                                i2 = 1;
                                a a222 = this.d.a(e);
                                i = a222.f2569a;
                                z = a222.b;
                                if (!z) {
                                }
                                aVar = aVar2;
                                z2 = z;
                                if (z2) {
                                }
                                cVar.a().a();
                                if (iVar != null) {
                                }
                                if (z2) {
                                }
                                if (z2) {
                                }
                                if (this.l.get() == null) {
                                }
                                i5 = i;
                                aVar2 = aVar;
                                i3 = -2;
                                i4 = 1;
                                b2 = 0;
                            } catch (OutOfMemoryError unused32) {
                            } catch (Throwable th32) {
                            }
                        }
                        z2 = false;
                        if (cVar.a() != null) {
                        }
                        iVar.a();
                        if (!z2) {
                        }
                        if (z2) {
                        }
                        if (this.l.get() != null) {
                        }
                    } catch (Exception e5) {
                    } catch (OutOfMemoryError unused4) {
                    }
                } catch (Exception e6) {
                    e = e6;
                    cVar = null;
                    i2 = 1;
                    a a2222 = this.d.a(e);
                    i = a2222.f2569a;
                    z = a2222.b;
                    if (!z) {
                        try {
                            if (this.g && i.a(this.b.getURI().toString()) == com.mintegral.msdk.base.common.net.i.a.b && i == 6) {
                                String substring = this.b.getURI().toString().substring(5);
                                aVar = aVar2;
                                StringBuilder sb2 = new StringBuilder("http");
                                sb2.append(substring);
                                this.b.setURI(URI.create(sb2.toString()));
                                z2 = true;
                                if (z2) {
                                    if (f() && this.l.get() != null) {
                                        this.c.a(a2222.c, e);
                                    }
                                }
                                if (!(cVar == null || cVar.a() == null)) {
                                    cVar.a().a();
                                }
                                if (iVar != null) {
                                    iVar.a();
                                }
                                if (z2 || obj == null) {
                                    if (z2 && f() && this.l.get() != null) {
                                        this.c.b(i);
                                    } else if (this.l.get() == null) {
                                        this.c.b(5);
                                    }
                                    i5 = i;
                                    aVar2 = aVar;
                                    i3 = -2;
                                    i4 = 1;
                                    b2 = 0;
                                } else if (!f()) {
                                }
                            }
                        } catch (Throwable th5) {
                            th = th5;
                            z2 = z;
                            cVar.a().a();
                            if (iVar != null) {
                            }
                            if (!z2) {
                            }
                            if (this.l.get() == null) {
                            }
                            throw th;
                        }
                    }
                    aVar = aVar2;
                    z2 = z;
                    if (z2) {
                    }
                    cVar.a().a();
                    if (iVar != null) {
                    }
                    if (z2) {
                    }
                    if (z2) {
                    }
                    if (this.l.get() == null) {
                    }
                    i5 = i;
                    aVar2 = aVar;
                    i3 = -2;
                    i4 = 1;
                    b2 = 0;
                } catch (OutOfMemoryError unused5) {
                    cVar = null;
                    cVar.a().a();
                    if (iVar != null) {
                    }
                    if (f()) {
                    }
                    if (this.l.get() == null) {
                    }
                } catch (Throwable th6) {
                    th = th6;
                    i = 1;
                    cVar = null;
                    cVar.a().a();
                    if (iVar != null) {
                    }
                    if (!z2) {
                    }
                    if (this.l.get() == null) {
                    }
                    throw th;
                }
            } catch (Exception e7) {
                e = e7;
                iVar = null;
                cVar = null;
                i2 = 1;
                a a22222 = this.d.a(e);
                i = a22222.f2569a;
                z = a22222.b;
                if (!z) {
                }
                aVar = aVar2;
                z2 = z;
                if (z2) {
                }
                cVar.a().a();
                if (iVar != null) {
                }
                if (z2) {
                }
                if (z2) {
                }
                if (this.l.get() == null) {
                }
                i5 = i;
                aVar2 = aVar;
                i3 = -2;
                i4 = 1;
                b2 = 0;
            } catch (OutOfMemoryError unused6) {
                iVar = null;
                cVar = null;
                cVar.a().a();
                if (iVar != null) {
                }
                if (f()) {
                }
                if (this.l.get() == null) {
                }
            } catch (Throwable th7) {
                th = th7;
                i = 1;
                iVar = null;
                cVar = null;
                cVar.a().a();
                if (iVar != null) {
                }
                if (!z2) {
                }
                if (this.l.get() == null) {
                }
                throw th;
            }
        }
        if (!f()) {
            dVar.f2563a = -2;
            return dVar;
        }
        if (this.l.get() != null) {
            this.c.c();
        }
        g.a("CommonHttpRequest", "request is finished");
        dVar.f2563a = i;
        dVar.c = obj;
        return dVar;
    }

    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* JADX WARNING: Missing exception handler attribute for start block: B:2:0x0003 */
    /* JADX WARNING: Removed duplicated region for block: B:2:0x0003 A[LOOP:0: B:2:0x0003->B:19:0x0003, LOOP_START, SYNTHETIC] */
    private boolean f() {
        synchronized (this.e) {
            while (this.i == C0049a.c) {
                this.e.wait();
            }
        }
        return this.i == C0049a.b;
    }

    public final void a() {
        try {
            e();
        } catch (Exception e2) {
            g.c("CommonHttpRequest", "unknow exception", e2);
        }
    }

    public final void b() {
        g.a("CommonHttpRequest", "request is canceled");
        if (this.l.get() != null) {
            this.c.d();
        }
        if (this.f != null) {
            this.f.interrupt();
        }
        synchronized (this.e) {
            this.e.notifyAll();
        }
    }
}
