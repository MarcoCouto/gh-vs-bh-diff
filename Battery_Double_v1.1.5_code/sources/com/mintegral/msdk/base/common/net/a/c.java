package com.mintegral.msdk.base.common.net.a;

import android.support.v4.app.NotificationCompat;
import com.mintegral.msdk.base.utils.g;
import org.json.JSONObject;

/* compiled from: CommonMTGJSONObjectResponseHandler */
public abstract class c extends b {

    /* renamed from: a reason: collision with root package name */
    private static final String f2548a = "c";

    public abstract void a(String str);

    public abstract void a(JSONObject jSONObject);

    public final /* synthetic */ void a(Object obj) {
        JSONObject jSONObject = (JSONObject) obj;
        String str = f2548a;
        StringBuilder sb = new StringBuilder("content = ");
        sb.append(jSONObject);
        g.b(str, sb.toString());
        int optInt = jSONObject.optInt("status");
        if (optInt == 1 || optInt == 200) {
            a(jSONObject.optJSONObject("data"));
        } else {
            a(jSONObject.optString(NotificationCompat.CATEGORY_MESSAGE));
        }
    }

    public final void a(int i) {
        String str = f2548a;
        StringBuilder sb = new StringBuilder("errorCode = ");
        sb.append(i);
        g.d(str, sb.toString());
        a(c(i));
    }
}
