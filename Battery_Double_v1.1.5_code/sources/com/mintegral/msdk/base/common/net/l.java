package com.mintegral.msdk.base.common.net;

import android.text.TextUtils;
import com.mintegral.msdk.base.utils.g;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import org.apache.http.HttpEntity;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;

/* compiled from: CommonRequestParams */
public final class l {

    /* renamed from: a reason: collision with root package name */
    private Map<String, String> f2570a = new LinkedHashMap();
    private Map<String, b> b = new LinkedHashMap();
    private Map<String, a> c = new LinkedHashMap();
    private String d = "UTF-8";

    /* compiled from: CommonRequestParams */
    private static class a {

        /* renamed from: a reason: collision with root package name */
        File f2571a;
        String b;
        String c;
    }

    /* compiled from: CommonRequestParams */
    private static class b {

        /* renamed from: a reason: collision with root package name */
        InputStream f2572a;
        String b;
        String c;
    }

    public final void a(String str, String str2) {
        if (str2 == null) {
            g.d("RequestParams", "不能向接口传递null");
        }
        if (!TextUtils.isEmpty(str) && str2 != null) {
            this.f2570a.put(str, str2);
        }
    }

    public final void a(String str) {
        this.f2570a.remove(str);
        this.b.remove(str);
        this.c.remove(str);
    }

    public final String a() {
        return this.d;
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder(28);
        for (Entry entry : this.f2570a.entrySet()) {
            if (sb.length() > 0) {
                sb.append('&');
            }
            sb.append((String) entry.getKey());
            sb.append('=');
            sb.append((String) entry.getValue());
        }
        for (Entry entry2 : this.b.entrySet()) {
            if (sb.length() > 0) {
                sb.append('&');
            }
            sb.append((String) entry2.getKey());
            sb.append('=');
            sb.append("STREAM");
            if (!TextUtils.isEmpty(((b) entry2.getValue()).b)) {
                StringBuilder sb2 = new StringBuilder("_NAME_");
                sb2.append(((b) entry2.getValue()).b);
                sb.append(sb2.toString());
            }
        }
        for (Entry entry3 : this.c.entrySet()) {
            if (sb.length() > 0) {
                sb.append('&');
            }
            sb.append((String) entry3.getKey());
            sb.append('=');
            StringBuilder sb3 = new StringBuilder("FILE_NAME_");
            sb3.append(((a) entry3.getValue()).f2571a.getName());
            sb.append(sb3.toString());
        }
        return sb.toString();
    }

    /* access modifiers changed from: 0000 */
    public final HttpEntity a(g gVar) throws IOException {
        if (this.b.isEmpty() && this.c.isEmpty()) {
            return c();
        }
        e eVar = new e(gVar);
        for (Entry entry : this.f2570a.entrySet()) {
            eVar.a((String) entry.getKey(), (String) entry.getValue());
        }
        for (Entry entry2 : this.b.entrySet()) {
            b bVar = (b) entry2.getValue();
            if (bVar.f2572a != null) {
                eVar.a((String) entry2.getKey(), bVar.b, bVar.f2572a, bVar.c);
            }
        }
        for (Entry entry3 : this.c.entrySet()) {
            a aVar = (a) entry3.getValue();
            eVar.a((String) entry3.getKey(), aVar.f2571a, aVar.b, aVar.c);
        }
        return eVar;
    }

    public final String b() {
        try {
            return URLEncodedUtils.format(d(), this.d).replace("+", "%20");
        } catch (Throwable unused) {
            g.d("RequestParams", "URLEncodedUtils failed");
            return "";
        }
    }

    private HttpEntity c() {
        try {
            return new UrlEncodedFormEntity(d(), this.d);
        } catch (UnsupportedEncodingException e) {
            g.b("RequestParams", "createFormEntity failed", e);
            return null;
        }
    }

    private List<BasicNameValuePair> d() {
        LinkedList linkedList = new LinkedList();
        for (Entry entry : this.f2570a.entrySet()) {
            linkedList.add(new BasicNameValuePair((String) entry.getKey(), (String) entry.getValue()));
        }
        return linkedList;
    }
}
