package com.mintegral.msdk.base.common.net;

import android.os.Handler;
import android.os.Process;
import android.text.TextUtils;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.MIntegralUser;
import com.mintegral.msdk.base.controller.b;
import com.mintegral.msdk.base.utils.a;

public class Aa {
    private static final String C_END = "_mv_end";
    private static final String C_START = "mv_channel_";

    /* renamed from: a reason: collision with root package name */
    private static String f2546a = "";

    private static void b(String str) {
        String str2;
        if (!TextUtils.isEmpty(str)) {
            try {
                str2 = a.c(str);
            } catch (Throwable th) {
                th.printStackTrace();
                str2 = null;
            }
            if (!TextUtils.isEmpty(str2) && str2.startsWith(C_START) && str2.endsWith(C_END)) {
                Integer c = c(str2);
                if (c != null) {
                    f2546a = String.valueOf(c);
                    return;
                }
            }
            if (!MIntegralConstans.DEBUG) {
                f2546a = "";
            } else {
                g();
                throw new RuntimeException("please don't update this value");
            }
        }
    }

    private static Integer c(String str) {
        Integer num = null;
        if (str != null) {
            int indexOf = str.indexOf(C_START);
            int indexOf2 = str.indexOf(C_END);
            if (!(indexOf == -1 || indexOf2 == -1 || indexOf2 <= indexOf)) {
                try {
                    Integer valueOf = Integer.valueOf(str.substring(indexOf + 11, indexOf2));
                    try {
                        if (valueOf.intValue() > 0) {
                            return valueOf;
                        }
                        return null;
                    } catch (Throwable th) {
                        Throwable th2 = th;
                        num = valueOf;
                        th = th2;
                        th.printStackTrace();
                        return num;
                    }
                } catch (Throwable th3) {
                    th = th3;
                    th.printStackTrace();
                    return num;
                }
            }
        }
        return num;
    }

    public static String a() {
        return f2546a;
    }

    public static String b() {
        String str = null;
        try {
            MIntegralUser d = b.a().d();
            if (d != null) {
                String json = MIntegralUser.toJSON(d);
                if (!TextUtils.isEmpty(json)) {
                    str = a.b(json);
                }
                return str;
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
        return null;
    }

    private static void g() {
        new Handler().postDelayed(new Runnable() {
            public final void run() {
                try {
                    Process.killProcess(Process.myPid());
                } catch (Throwable th) {
                    th.printStackTrace();
                }
            }
        }, 500);
    }
}
