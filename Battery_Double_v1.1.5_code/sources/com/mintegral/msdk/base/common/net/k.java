package com.mintegral.msdk.base.common.net;

import android.os.SystemClock;
import com.mintegral.msdk.base.utils.g;
import java.io.IOException;
import java.net.ConnectException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import javax.net.ssl.SSLException;
import javax.net.ssl.SSLHandshakeException;
import javax.net.ssl.SSLKeyException;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLProtocolException;
import org.apache.http.NoHttpResponseException;

/* compiled from: CommonRequestExceptionManager */
public final class k {

    /* renamed from: a reason: collision with root package name */
    private static HashSet<Class<? extends Exception>> f2568a = new HashSet<>();
    private static HashSet<Class<? extends Exception>> b = new HashSet<>();
    private static HashMap<Class<? extends Exception>, Integer> c = new HashMap<>();
    private int d;
    private int e;
    private int f;

    /* compiled from: CommonRequestExceptionManager */
    static class a {

        /* renamed from: a reason: collision with root package name */
        int f2569a;
        boolean b;
        int c;

        a() {
        }
    }

    static {
        f2568a.add(NoHttpResponseException.class);
        f2568a.add(UnknownHostException.class);
        f2568a.add(SocketTimeoutException.class);
        f2568a.add(SocketException.class);
        f2568a.add(ConnectException.class);
        b.add(InterruptedException.class);
        b.add(SSLHandshakeException.class);
        b.add(SSLKeyException.class);
        b.add(SSLPeerUnverifiedException.class);
        b.add(SSLProtocolException.class);
        b.add(SSLException.class);
        b.add(NullPointerException.class);
        c.put(SSLException.class, Integer.valueOf(6));
        c.put(SocketTimeoutException.class, Integer.valueOf(3));
        c.put(SocketException.class, Integer.valueOf(2));
        c.put(NoHttpResponseException.class, Integer.valueOf(8));
        c.put(SSLHandshakeException.class, Integer.valueOf(6));
        c.put(SSLKeyException.class, Integer.valueOf(6));
        c.put(SSLPeerUnverifiedException.class, Integer.valueOf(6));
        c.put(SSLProtocolException.class, Integer.valueOf(6));
        c.put(SSLException.class, Integer.valueOf(6));
        c.put(InterruptedException.class, Integer.valueOf(-2));
        c.put(IOException.class, Integer.valueOf(2));
        c.put(NullPointerException.class, Integer.valueOf(4));
        c.put(Exception.class, Integer.valueOf(5));
    }

    k(int i, int i2) {
        this.d = i;
        this.e = i2;
    }

    /* access modifiers changed from: 0000 */
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0036  */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x005f  */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0023  */
    public final a a(Exception exc) {
        Integer num;
        a aVar = new a();
        if (this.f < this.d) {
            if (a(f2568a, exc)) {
                aVar.b = true;
                if (!aVar.b) {
                    g.a("CommonRequestExceptionManager", (Throwable) exc);
                    SystemClock.sleep((long) this.e);
                    int i = this.f + 1;
                    this.f = i;
                    aVar.c = i;
                } else {
                    StringBuilder sb = new StringBuilder("httprequest exception stop retry ");
                    sb.append(exc.getClass().getSimpleName());
                    g.b("CommonRequestExceptionManager", sb.toString(), exc);
                }
                num = (Integer) c.get(exc.getClass());
                if (num == null) {
                    num = Integer.valueOf(5);
                }
                aVar.f2569a = num.intValue();
                return aVar;
            }
            a(b, exc);
        }
        aVar.b = false;
        if (!aVar.b) {
        }
        num = (Integer) c.get(exc.getClass());
        if (num == null) {
        }
        aVar.f2569a = num.intValue();
        return aVar;
    }

    /* access modifiers changed from: 0000 */
    public final void a() {
        this.f = 0;
    }

    private static boolean a(HashSet<Class<? extends Exception>> hashSet, Throwable th) {
        Iterator it = hashSet.iterator();
        while (it.hasNext()) {
            if (((Class) it.next()).isInstance(th)) {
                return true;
            }
        }
        return false;
    }
}
