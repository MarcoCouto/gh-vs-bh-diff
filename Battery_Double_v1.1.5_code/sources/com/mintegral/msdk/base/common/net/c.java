package com.mintegral.msdk.base.common.net;

import android.content.Context;
import android.text.TextUtils;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.mintegral.msdk.base.common.e.a;
import com.mintegral.msdk.base.common.net.a.d;
import com.mintegral.msdk.base.utils.g;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;

/* compiled from: CommonBaseHttpRequest */
public abstract class c {

    /* renamed from: a reason: collision with root package name */
    private String f2550a;
    protected Context b;
    private int c;
    private int d;
    private int e;
    private boolean f;
    private int g;
    private int h;

    /* access modifiers changed from: protected */
    public abstract void a(d<?> dVar);

    /* access modifiers changed from: protected */
    public abstract void a(f fVar);

    public c(Context context) {
        this(context, 0);
    }

    public c(Context context, int i) {
        this.c = 15000;
        this.d = 0;
        this.e = 1000;
        this.f = true;
        this.g = 0;
        this.h = -1;
        this.b = context;
        this.d = i;
    }

    public final a a(String str, l lVar, d<?> dVar) {
        if (TextUtils.isEmpty(str)) {
            g.d("BaseHttpRequest", "http get request url cannot be empty");
            return new a() {
                public final void a() {
                }

                public final void b() {
                }
            };
        } else if (dVar != null) {
            this.f2550a = str;
            HashMap hashMap = new HashMap();
            if (lVar == null) {
                lVar = new l();
            }
            a(lVar);
            try {
                d.d(lVar);
            } catch (Exception e2) {
                g.a("BaseHttpRequest", e2.getMessage());
            }
            String replace = str.replace(" ", "%20");
            if (lVar != null) {
                String trim = lVar.b().trim();
                if (!TextUtils.isEmpty(trim)) {
                    if (!replace.endsWith("?") && !replace.endsWith(RequestParameters.AMPERSAND)) {
                        StringBuilder sb = new StringBuilder();
                        sb.append(replace);
                        sb.append(replace.contains("?") ? RequestParameters.AMPERSAND : "?");
                        replace = sb.toString();
                    }
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append(replace);
                    sb2.append(trim);
                    replace = sb2.toString();
                }
            }
            HttpGet httpGet = new HttpGet(replace);
            if (lVar != null) {
                try {
                    httpGet.setHeader("Charset", lVar.a());
                } catch (Throwable unused) {
                    g.d("BaseHttpRequest", "setHeader failed");
                }
            }
            StringBuilder sb3 = new StringBuilder("bytes=");
            sb3.append(this.g);
            sb3.append("-");
            String sb4 = sb3.toString();
            if (this.h > 0) {
                StringBuilder sb5 = new StringBuilder();
                sb5.append(sb4);
                sb5.append(this.h);
                sb4 = sb5.toString();
            }
            if (!(this.g == 0 && this.h == -1)) {
                httpGet.setHeader("Range", sb4);
            }
            StringBuilder sb6 = new StringBuilder("request url: ");
            sb6.append(str);
            g.a("BaseHttpRequest", sb6.toString());
            if (hashMap.size() > 0) {
                StringBuilder sb7 = new StringBuilder("request headers: ");
                sb7.append(hashMap.toString());
                g.a("BaseHttpRequest", sb7.toString());
            }
            if (lVar != null) {
                StringBuilder sb8 = new StringBuilder("request params: ");
                sb8.append(lVar.toString());
                g.a("BaseHttpRequest", sb8.toString());
            }
            g.a("BaseHttpRequest", "request method: GET");
            f a2 = a((HttpRequestBase) httpGet, (Map<String, String>) hashMap, (g) dVar);
            a(dVar);
            a(a2);
            return a2;
        } else {
            throw new UnsupportedOperationException("IResponseHandle cannot be null");
        }
    }

    public final a b(String str, l lVar, d<?> dVar) {
        return c(str, lVar, dVar);
    }

    private a c(String str, l lVar, d<?> dVar) {
        if (TextUtils.isEmpty(str)) {
            g.c("BaseHttpRequest", "http post request url cannot be empty");
            return new a() {
                public final void a() {
                }

                public final void b() {
                }
            };
        } else if (dVar != null) {
            this.f2550a = str;
            HashMap hashMap = new HashMap();
            if (lVar == null) {
                lVar = new l();
            }
            a(lVar);
            try {
                d.d(lVar);
            } catch (Exception e2) {
                g.a("BaseHttpRequest", e2.getMessage());
            }
            try {
                HttpPost httpPost = new HttpPost(str);
                if (lVar != null) {
                    HttpEntity a2 = lVar.a((g) dVar);
                    httpPost.setHeader(a2.getContentType());
                    httpPost.setHeader("Charset", lVar.a());
                    httpPost.setEntity(a2);
                }
                StringBuilder sb = new StringBuilder("bytes=");
                sb.append(this.g);
                sb.append("-");
                String sb2 = sb.toString();
                if (this.h > 0) {
                    StringBuilder sb3 = new StringBuilder();
                    sb3.append(sb2);
                    sb3.append(this.h);
                    sb2 = sb3.toString();
                }
                httpPost.setHeader("Range", sb2);
                StringBuilder sb4 = new StringBuilder("request url: ");
                sb4.append(str);
                g.a("BaseHttpRequest", sb4.toString());
                if (hashMap.size() > 0) {
                    StringBuilder sb5 = new StringBuilder("request headers: ");
                    sb5.append(hashMap.toString());
                    g.a("BaseHttpRequest", sb5.toString());
                }
                if (lVar != null) {
                    StringBuilder sb6 = new StringBuilder("request params: ");
                    sb6.append(lVar.toString());
                    g.a("BaseHttpRequest", sb6.toString());
                }
                g.a("BaseHttpRequest", "request method: POST");
                f a3 = a((HttpRequestBase) httpPost, (Map<String, String>) hashMap, (g) dVar);
                a(dVar);
                a(a3);
                return a3;
            } catch (IOException e3) {
                g.c("BaseHttpRequest", "write params an error occurred", e3);
                return new a() {
                    public final void a() {
                    }

                    public final void b() {
                    }
                };
            }
        } else {
            throw new UnsupportedOperationException("IResponseHandle cannot be null");
        }
    }

    public final void c() {
        this.c = 30000;
    }

    /* access modifiers changed from: protected */
    public void a(l lVar) {
        if (lVar != null) {
            String a2 = Aa.a();
            if (a2 == null) {
                a2 = "";
            }
            lVar.a("channel", a2);
            StringBuilder sb = new StringBuilder("excute addExtraParams , url:");
            sb.append(this.f2550a);
            g.a("BaseHttpRequest", sb.toString());
            if (!TextUtils.isEmpty(this.f2550a) && this.f2550a.contains("setting")) {
                String b2 = Aa.b();
                if (!TextUtils.isEmpty(b2)) {
                    lVar.a("keyword", b2);
                }
            }
            return;
        }
        g.d("BaseHttpRequest", "addExtraParams error, params is null,frame work error");
    }

    private f a(HttpRequestBase httpRequestBase, Map<String, String> map, g gVar) {
        if (!map.isEmpty()) {
            for (Entry entry : map.entrySet()) {
                if (!TextUtils.equals((CharSequence) entry.getKey(), "Content-Type") && !TextUtils.equals((CharSequence) entry.getKey(), "Charset")) {
                    httpRequestBase.setHeader((String) entry.getKey(), (String) entry.getValue());
                }
            }
        }
        f fVar = new f(this.b, httpRequestBase, gVar, this.c, this.d, this.e, this.f);
        return fVar;
    }
}
