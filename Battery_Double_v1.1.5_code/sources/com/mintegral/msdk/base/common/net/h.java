package com.mintegral.msdk.base.common.net;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Proxy;
import com.mintegral.msdk.base.utils.g;
import java.util.Locale;

/* compiled from: CommonNetConnectManager */
public final class h {
    private static h b;

    /* renamed from: a reason: collision with root package name */
    private final String f2564a = "NetConnectManager";
    private ConnectivityManager c;
    private j d;

    public static synchronized h a(Context context) {
        h hVar;
        synchronized (h.class) {
            if (b == null) {
                b = new h();
                if (context != null) {
                    b.c = (ConnectivityManager) context.getSystemService("connectivity");
                }
                b.d = new j();
            }
            hVar = b;
        }
        return hVar;
    }

    public final void a() {
        try {
            if (this.c != null) {
                NetworkInfo activeNetworkInfo = this.c.getActiveNetworkInfo();
                if (activeNetworkInfo != null) {
                    if ("wifi".equals(activeNetworkInfo.getTypeName().toLowerCase(Locale.US))) {
                        this.d.e = "wifi";
                        this.d.d = false;
                    } else {
                        if (activeNetworkInfo.getExtraInfo() != null) {
                            String lowerCase = activeNetworkInfo.getExtraInfo().toLowerCase(Locale.US);
                            if (lowerCase != null) {
                                if (!lowerCase.startsWith("cmwap") && !lowerCase.startsWith("uniwap")) {
                                    if (!lowerCase.startsWith("3gwap")) {
                                        if (lowerCase.startsWith("ctwap")) {
                                            this.d.d = true;
                                            this.d.f2567a = lowerCase;
                                            this.d.b = "10.0.0.200";
                                            this.d.c = "80";
                                        } else if (lowerCase.startsWith("cmnet") || lowerCase.startsWith("uninet") || lowerCase.startsWith("ctnet") || lowerCase.startsWith("3gnet")) {
                                            this.d.d = false;
                                            this.d.f2567a = lowerCase;
                                        }
                                        this.d.e = this.d.f2567a;
                                    }
                                }
                                this.d.d = true;
                                this.d.f2567a = lowerCase;
                                this.d.b = "10.0.0.172";
                                this.d.c = "80";
                                this.d.e = this.d.f2567a;
                            }
                        }
                        String defaultHost = Proxy.getDefaultHost();
                        int defaultPort = Proxy.getDefaultPort();
                        if (defaultHost == null || defaultHost.length() <= 0) {
                            this.d.d = false;
                            this.d.e = this.d.f2567a;
                        } else {
                            this.d.b = defaultHost;
                            if ("10.0.0.172".equals(this.d.b.trim())) {
                                this.d.d = true;
                                this.d.c = "80";
                            } else if ("10.0.0.200".equals(this.d.b.trim())) {
                                this.d.d = true;
                                this.d.c = "80";
                            } else {
                                this.d.d = false;
                                this.d.c = Integer.toString(defaultPort);
                            }
                            this.d.e = this.d.f2567a;
                        }
                    }
                    StringBuilder sb = new StringBuilder("current net connect type is ");
                    sb.append(this.d.e);
                    g.a("NetConnectManager", sb.toString());
                }
            }
        } catch (Exception unused) {
            g.d("NetConnectManager", "NETWORK FAILED");
        }
    }

    public final boolean b() {
        try {
            NetworkInfo activeNetworkInfo = this.c.getActiveNetworkInfo();
            if (activeNetworkInfo != null) {
                return activeNetworkInfo.isConnectedOrConnecting();
            }
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public final j c() {
        return this.d;
    }
}
