package com.mintegral.msdk.base.common.net;

import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import com.mintegral.msdk.base.utils.g;
import java.io.IOException;
import java.lang.ref.WeakReference;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.ParseException;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: CommonBaseResponseHandler */
public abstract class d<T> implements g {

    /* renamed from: a reason: collision with root package name */
    private boolean f2554a;
    private a b;
    private long c;
    private String d;

    /* compiled from: CommonBaseResponseHandler */
    private static class a extends Handler {

        /* renamed from: a reason: collision with root package name */
        private WeakReference<d<?>> f2555a;

        a(d<?> dVar) {
            this.f2555a = new WeakReference<>(dVar);
        }

        public final void handleMessage(Message message) {
            d dVar = (d) this.f2555a.get();
            if (dVar != null) {
                dVar.a(message);
            }
        }
    }

    public static String c(int i) {
        return i == 2 ? "Network error,I/O exception" : i == 3 ? "Network error,timeout exception" : i == 5 ? "Network unknown error" : i == 4 ? "Network error,please use 3gnet and retry" : i == -2 ? "Network is canceled" : i == 6 ? "Network error，https is not work,please check your phone time" : i == 7 ? "Network error,please check" : i == 8 ? "The server returns an exception" : "Network error,Load failed";
    }

    public void a() {
    }

    public abstract void a(int i);

    public abstract void a(T t);

    public d() {
        this("UTF-8");
    }

    private d(String str) {
        if (TextUtils.isEmpty(str)) {
            this.d = "UTF-8";
        } else {
            this.d = str;
        }
    }

    /* access modifiers changed from: private */
    public void a(Message message) {
        Object[] objArr = (Object[]) message.obj;
        switch (message.what) {
            case 100:
                a();
                return;
            case 101:
                a((T) objArr[1]);
                return;
            case 102:
                a(((Integer) objArr[0]).intValue());
                return;
            case 103:
                return;
            case 104:
                ((Integer) objArr[0]).intValue();
                return;
            case 105:
                return;
            case 106:
                ((Integer) objArr[0]).intValue();
                ((Integer) objArr[1]).intValue();
                break;
            case 107:
                ((Integer) objArr[0]).intValue();
                ((Integer) objArr[1]).intValue();
                return;
        }
    }

    private void b(Message message) {
        if (!this.f2554a) {
            a(message);
            return;
        }
        if (!Thread.currentThread().isInterrupted() && Thread.currentThread().isAlive()) {
            this.b.sendMessage(message);
        }
    }

    private Message a(int i, Object... objArr) {
        Message message;
        if (this.f2554a) {
            message = Message.obtain(this.b);
        } else {
            message = new Message();
        }
        message.what = i;
        message.obj = objArr;
        return message;
    }

    public final void b() {
        b(a(100, new Object[0]));
    }

    public final void a(Header[] headerArr, Object obj) {
        b(a(101, headerArr, obj));
    }

    public final void b(int i) {
        b(a(102, Integer.valueOf(i)));
    }

    public final void c() {
        b(a(103, new Object[0]));
    }

    public final void a(int i, Exception exc) {
        b(a(104, Integer.valueOf(i), exc));
    }

    public final void d() {
        b(a(105, new Object[0]));
    }

    public final void a(int i, int i2) {
        if (i2 > 0 && i >= 0 && System.currentTimeMillis() - this.c >= 1000) {
            this.c = System.currentTimeMillis();
            b(a(106, Integer.valueOf(i), Integer.valueOf(i2)));
        }
    }

    /* access modifiers changed from: 0000 */
    public final void e() {
        this.b = new a(this);
        this.f2554a = true;
    }

    /* access modifiers changed from: protected */
    public final String c(HttpEntity httpEntity) throws ParseException, IOException {
        return httpEntity != null ? EntityUtils.toString(httpEntity, this.d) : "";
    }

    /* access modifiers changed from: protected */
    public JSONObject a(HttpEntity httpEntity) throws ParseException, JSONException, IOException {
        String str;
        String str2 = "";
        try {
            str = c(httpEntity);
            try {
                return new JSONObject(str);
            } catch (Exception unused) {
                StringBuilder sb = new StringBuilder("wrong json  : ");
                sb.append(str);
                g.d("BaseResponseHandler", sb.toString());
                return null;
            } catch (Throwable unused2) {
                StringBuilder sb2 = new StringBuilder("wrong json : ");
                sb2.append(str);
                g.d("BaseResponseHandler", sb2.toString());
                return null;
            }
        } catch (Exception unused3) {
            str = str2;
            StringBuilder sb3 = new StringBuilder("wrong json  : ");
            sb3.append(str);
            g.d("BaseResponseHandler", sb3.toString());
            return null;
        } catch (Throwable unused4) {
            str = str2;
            StringBuilder sb22 = new StringBuilder("wrong json : ");
            sb22.append(str);
            g.d("BaseResponseHandler", sb22.toString());
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public final JSONArray d(HttpEntity httpEntity) throws ParseException, JSONException, IOException {
        return new JSONArray(c(httpEntity));
    }
}
