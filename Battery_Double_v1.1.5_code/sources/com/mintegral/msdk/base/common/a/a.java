package com.mintegral.msdk.base.common.a;

import android.graphics.Bitmap;
import java.util.LinkedHashMap;
import java.util.Map.Entry;

/* compiled from: LruMemoryCache */
public final class a implements b<String, Bitmap> {

    /* renamed from: a reason: collision with root package name */
    private final LinkedHashMap<String, Bitmap> f2509a;
    private final int b;
    private int c;

    public a(int i) {
        if (i > 0) {
            this.b = i;
            this.f2509a = new LinkedHashMap<>(0, 0.75f, true);
            return;
        }
        throw new IllegalArgumentException("maxSize <= 0");
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0017, code lost:
        return null;
     */
    public Bitmap a(String str) {
        if (str != null) {
            synchronized (this) {
                Bitmap bitmap = (Bitmap) this.f2509a.get(str);
                if (bitmap != null && !bitmap.isRecycled()) {
                    return bitmap;
                }
            }
        } else {
            throw new NullPointerException("key == null");
        }
    }

    /* access modifiers changed from: private */
    public boolean a(String str, Bitmap bitmap) {
        if (str == null || bitmap == null) {
            throw new NullPointerException("key == null || value == null");
        }
        synchronized (this) {
            this.c += a(bitmap);
            Bitmap bitmap2 = (Bitmap) this.f2509a.put(str, bitmap);
            if (bitmap2 != null) {
                this.c -= a(bitmap2);
            }
        }
        a(this.b);
        return true;
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(4:17|18|19|20) */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0074, code lost:
        throw new java.lang.IllegalStateException(r0.toString());
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:20:0x0052 */
    private void a(int i) {
        while (true) {
            synchronized (this) {
                if (this.c < 0 || (this.f2509a.isEmpty() && this.c != 0)) {
                    StringBuilder sb = new StringBuilder();
                    sb.append(getClass().getName());
                    sb.append(".sizeOf() is reporting inconsistent results!");
                } else if (this.c <= i) {
                    break;
                } else if (this.f2509a.isEmpty()) {
                    break;
                } else {
                    Entry entry = (Entry) this.f2509a.entrySet().iterator().next();
                    if (entry != null) {
                        String str = (String) entry.getKey();
                        int a2 = a((Bitmap) entry.getValue());
                        ((Bitmap) this.f2509a.remove(str)).recycle();
                        this.c -= a2;
                    } else {
                        return;
                    }
                }
            }
        }
    }

    public final void a() {
        a(-1);
    }

    private static int a(Bitmap bitmap) {
        return bitmap.getRowBytes() * bitmap.getHeight();
    }

    public final synchronized String toString() {
        return String.format("LruCache[maxSize=%d]", new Object[]{Integer.valueOf(this.b)});
    }
}
