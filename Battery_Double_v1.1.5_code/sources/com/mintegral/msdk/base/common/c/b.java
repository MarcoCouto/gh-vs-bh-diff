package com.mintegral.msdk.base.common.c;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.mintegral.msdk.base.common.a.a;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.base.utils.j;
import com.mintegral.msdk.base.utils.k;
import java.io.File;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;

/* compiled from: CommonImageLoader */
public final class b {

    /* renamed from: a reason: collision with root package name */
    private static b f2516a;
    private com.mintegral.msdk.base.common.e.b b;
    private com.mintegral.msdk.base.common.a.b<String, Bitmap> c;
    /* access modifiers changed from: private */
    public LinkedHashMap<String, List<c>> d = new LinkedHashMap<>();
    /* access modifiers changed from: private */
    public Handler e = new Handler(Looper.getMainLooper()) {
        public final void handleMessage(Message message) {
            try {
                if (message.what == 1) {
                    String string = message.getData().getString("message_key");
                    Bitmap a2 = a.a(message.getData().getString("message_bitmap"));
                    b.this.a(string, a2);
                    LinkedList linkedList = (LinkedList) b.this.d.get(string);
                    if (linkedList != null) {
                        Iterator it = linkedList.iterator();
                        while (it.hasNext()) {
                            c cVar = (c) it.next();
                            if (cVar != null) {
                                cVar.onSuccessLoad(a2, string);
                            }
                        }
                    }
                    b.this.d.remove(string);
                    return;
                }
                if (message.what == 2) {
                    String string2 = message.getData().getString("message_key");
                    String string3 = message.getData().getString("message_message");
                    LinkedList linkedList2 = (LinkedList) b.this.d.get(string2);
                    if (linkedList2 != null) {
                        Iterator it2 = linkedList2.iterator();
                        while (it2.hasNext()) {
                            c cVar2 = (c) it2.next();
                            if (cVar2 != null) {
                                cVar2.onFailedLoad(string3, string2);
                            }
                        }
                    }
                    b.this.d.remove(string2);
                }
            } catch (Throwable th) {
                th.printStackTrace();
            }
        }
    };

    public final void a(String str, Bitmap bitmap) {
        if (c(str) == null && bitmap != null) {
            this.c.a(str, bitmap);
        }
    }

    private Bitmap c(String str) {
        return (Bitmap) this.c.a(str);
    }

    private b(Context context) {
        this.b = new com.mintegral.msdk.base.common.e.b(context);
        this.c = new a(((int) Runtime.getRuntime().maxMemory()) / 5);
    }

    public static b a(Context context) {
        if (f2516a == null) {
            f2516a = new b(context);
        }
        return f2516a;
    }

    public final void a() {
        this.c.a();
        if (this.d != null) {
            this.d.clear();
        }
    }

    public static void b() {
        f2516a.c();
    }

    public final void a(String str, c cVar) {
        String a2 = j.a(str);
        if (!k.a(str) && !k.a(str) && !k.a(a2)) {
            File file = new File(a2);
            Bitmap c2 = c(str);
            if (c2 != null && !c2.isRecycled()) {
                cVar.onSuccessLoad(c2, str);
            } else if (file.exists()) {
                Bitmap a3 = a.a(a2);
                if (a3 == null || a3.isRecycled()) {
                    a(str, str, a2, true, cVar);
                    return;
                }
                StringBuilder sb = new StringBuilder("url image [");
                sb.append(str);
                sb.append("] is downloaded, save by file [");
                sb.append(a2);
                sb.append(RequestParameters.RIGHT_BRACKETS);
                g.a("ImageLoader", sb.toString());
                a(str, a3);
                cVar.onSuccessLoad(a3, str);
            } else {
                a(str, str, a2, false, cVar);
            }
        }
    }

    public final Bitmap a(String str) {
        if (k.a(str)) {
            return null;
        }
        String a2 = j.a(str);
        File file = new File(a2);
        if (c(str) != null) {
            return c(str);
        }
        if (file.exists()) {
            Bitmap a3 = a.a(a2);
            if (a3 != null) {
                a(str, a3);
                return a3;
            }
        }
        return null;
    }

    public final boolean b(String str) {
        if (k.a(str)) {
            return false;
        }
        File file = new File(j.a(str));
        if (c(str) == null && !file.exists()) {
            return false;
        }
        return true;
    }

    private void a(String str, String str2, String str3, boolean z, c cVar) {
        if (!this.d.containsKey(str2)) {
            LinkedList linkedList = new LinkedList();
            linkedList.add(cVar);
            this.d.put(str2, linkedList);
            AnonymousClass2 r6 = new d.a() {
                public final void a(String str, String str2) {
                    Message obtainMessage = b.this.e.obtainMessage();
                    obtainMessage.what = 1;
                    Bundle bundle = new Bundle();
                    bundle.putString("message_key", str);
                    bundle.putString("message_bitmap", str2);
                    obtainMessage.setData(bundle);
                    b.this.e.sendMessage(obtainMessage);
                }

                public final void b(String str, String str2) {
                    Message obtainMessage = b.this.e.obtainMessage();
                    obtainMessage.what = 2;
                    Bundle bundle = new Bundle();
                    bundle.putString("message_key", str);
                    bundle.putString("message_message", str2);
                    obtainMessage.setData(bundle);
                    b.this.e.sendMessage(obtainMessage);
                }
            };
            d dVar = new d(str, str2, str3);
            dVar.a(z);
            dVar.a((d.a) r6);
            this.b.b(dVar);
            return;
        }
        LinkedList linkedList2 = (LinkedList) this.d.get(str2);
        if (linkedList2 != null && !linkedList2.contains(cVar)) {
            linkedList2.add(cVar);
        }
        StringBuilder sb = new StringBuilder("loading:");
        sb.append(str2);
        g.a("ImageLoader", sb.toString());
    }

    public final void c() {
        if (this.c != null) {
            this.c.a();
        }
    }
}
