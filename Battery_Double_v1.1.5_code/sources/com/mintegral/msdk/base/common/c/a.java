package com.mintegral.msdk.base.common.c;

import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.base.utils.e;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;

/* compiled from: CommonBitmapUtil */
public final class a {
    public static Bitmap a(String str) {
        Bitmap bitmap = null;
        if (!e.a(str)) {
            return null;
        }
        Options options = new Options();
        try {
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(str, options);
            options.inJustDecodeBounds = false;
            options.inPurgeable = true;
            options.inInputShareable = true;
            options.inDither = true;
            bitmap = BitmapFactory.decodeFile(str, options);
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
            System.gc();
            b.b();
            try {
                options.inPreferredConfig = Config.RGB_565;
                Bitmap decodeFile = BitmapFactory.decodeFile(str, options);
                try {
                    options.inPreferredConfig = Config.ARGB_8888;
                    bitmap = decodeFile;
                } catch (OutOfMemoryError e2) {
                    e = e2;
                    bitmap = decodeFile;
                    e.printStackTrace();
                    return bitmap;
                }
            } catch (OutOfMemoryError e3) {
                e = e3;
                e.printStackTrace();
                return bitmap;
            }
        } catch (Exception e4) {
            e4.printStackTrace();
        }
        return bitmap;
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x002e A[Catch:{ all -> 0x0041 }] */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0033 A[SYNTHETIC, Splitter:B:21:0x0033] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0044 A[SYNTHETIC, Splitter:B:30:0x0044] */
    public static InputStream a(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream;
        try {
            byteArrayOutputStream = new ByteArrayOutputStream();
            try {
                bitmap.compress(CompressFormat.JPEG, 100, byteArrayOutputStream);
                ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(byteArrayOutputStream.toByteArray());
                try {
                    byteArrayOutputStream.close();
                    return byteArrayInputStream;
                } catch (Exception e) {
                    if (!MIntegralConstans.DEBUG) {
                        return byteArrayInputStream;
                    }
                    e.printStackTrace();
                    return byteArrayInputStream;
                }
            } catch (Exception e2) {
                e = e2;
                try {
                    if (MIntegralConstans.DEBUG) {
                    }
                    if (byteArrayOutputStream != null) {
                    }
                    return null;
                } catch (Throwable th) {
                    th = th;
                    if (byteArrayOutputStream != null) {
                        try {
                            byteArrayOutputStream.close();
                        } catch (Exception e3) {
                            if (MIntegralConstans.DEBUG) {
                                e3.printStackTrace();
                            }
                        }
                    }
                    throw th;
                }
            }
        } catch (Exception e4) {
            e = e4;
            byteArrayOutputStream = null;
            if (MIntegralConstans.DEBUG) {
                e.printStackTrace();
            }
            if (byteArrayOutputStream != null) {
                try {
                    byteArrayOutputStream.close();
                } catch (Exception e5) {
                    if (MIntegralConstans.DEBUG) {
                        e5.printStackTrace();
                    }
                }
            }
            return null;
        } catch (Throwable th2) {
            th = th2;
            byteArrayOutputStream = null;
            if (byteArrayOutputStream != null) {
            }
            throw th;
        }
    }
}
