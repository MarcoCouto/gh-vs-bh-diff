package com.mintegral.msdk.base.common.f;

import com.mintegral.msdk.base.common.d.c;
import com.mintegral.msdk.base.common.d.c.b;
import com.mintegral.msdk.base.utils.g;

/* compiled from: ReportTask */
public final class a implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private String f2543a;
    private String b;

    public a(String str, String str2) {
        this.f2543a = str;
        this.b = str2;
    }

    public final void run() {
        try {
            g.d("ReportTask", "start report");
            com.mintegral.msdk.base.common.d.c.a aVar = new com.mintegral.msdk.base.common.d.c.a(com.mintegral.msdk.base.controller.a.d().h());
            aVar.c();
            aVar.b(com.mintegral.msdk.base.common.a.f, c.a(this.f2543a, com.mintegral.msdk.base.controller.a.d().h(), this.b), new b() {
                public final void a(String str) {
                    g.d("ReportTask", str);
                }

                public final void b(String str) {
                    g.d("ReportTask", str);
                }
            });
        } catch (Throwable th) {
            g.d("ReportTask", th.getMessage());
        }
    }
}
