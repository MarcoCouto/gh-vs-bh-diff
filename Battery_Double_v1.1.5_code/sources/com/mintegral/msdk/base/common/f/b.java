package com.mintegral.msdk.base.common.f;

import android.support.v7.widget.helper.ItemTouchHelper.Callback;
import java.util.concurrent.Executors;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.ThreadPoolExecutor.DiscardPolicy;
import java.util.concurrent.TimeUnit;

/* compiled from: ThreadPoolUtils */
public final class b {

    /* renamed from: a reason: collision with root package name */
    public static ThreadPoolExecutor f2545a;

    public static ThreadPoolExecutor a() {
        if (f2545a == null) {
            ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(5, Callback.DEFAULT_DRAG_ANIMATION_DURATION, 15, TimeUnit.MILLISECONDS, new SynchronousQueue(), new ThreadFactory() {
                public final Thread newThread(Runnable runnable) {
                    Thread newThread = Executors.defaultThreadFactory().newThread(runnable);
                    newThread.setName("CommonThreadPool");
                    return newThread;
                }
            }, new DiscardPolicy());
            f2545a = threadPoolExecutor;
        }
        return f2545a;
    }
}
