package com.mintegral.msdk.base.common.e;

import android.content.Context;
import com.mintegral.msdk.base.common.e.a.C0049a;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/* compiled from: CommonTaskLoader */
public final class b {

    /* renamed from: a reason: collision with root package name */
    ThreadPoolExecutor f2541a;
    HashMap<Long, a> b;
    WeakReference<Context> c;

    public b(Context context, int i) {
        if (i == 0) {
            ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(1, 1, 15, TimeUnit.SECONDS, new LinkedBlockingDeque());
            this.f2541a = threadPoolExecutor;
        } else {
            ThreadPoolExecutor threadPoolExecutor2 = new ThreadPoolExecutor(i, i, 15, TimeUnit.SECONDS, new LinkedBlockingDeque());
            this.f2541a = threadPoolExecutor2;
        }
        this.f2541a.allowCoreThreadTimeOut(true);
        this.b = new HashMap<>();
        this.c = new WeakReference<>(context);
    }

    public b(Context context) {
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(0, Integer.MAX_VALUE, 15, TimeUnit.SECONDS, new SynchronousQueue());
        this.f2541a = threadPoolExecutor;
        this.f2541a.allowCoreThreadTimeOut(true);
        this.b = new HashMap<>();
        this.c = new WeakReference<>(context);
    }

    public b(Context context, byte b2) {
        int availableProcessors = (Runtime.getRuntime().availableProcessors() * 2) + 1;
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(availableProcessors, availableProcessors, 1, TimeUnit.SECONDS, new LinkedBlockingDeque());
        this.f2541a = threadPoolExecutor;
        this.f2541a.allowCoreThreadTimeOut(true);
        this.b = new HashMap<>();
        this.c = new WeakReference<>(context);
    }

    public final synchronized void a(a aVar) {
        if (aVar != null) {
            if (this.b.containsKey(Long.valueOf(a.c()))) {
                a aVar2 = (a) this.b.get(Long.valueOf(a.c()));
                if (aVar2 != null) {
                    aVar2.d();
                }
                this.b.remove(Long.valueOf(a.c()));
            }
        }
    }

    private synchronized void b(final a aVar, final com.mintegral.msdk.base.common.e.a.b bVar) {
        this.b.put(Long.valueOf(a.c()), aVar);
        aVar.j = new com.mintegral.msdk.base.common.e.a.b() {
            public final void a(int i) {
                if (i == C0049a.d) {
                    b.this.b.remove(Long.valueOf(a.c()));
                } else if (i == C0049a.e) {
                    b.this.b.remove(Long.valueOf(a.c()));
                } else if (i == C0049a.b && ((Context) b.this.c.get()) == null) {
                    b.this.a();
                }
                if (bVar != null) {
                    bVar.a(i);
                }
            }
        };
    }

    public final synchronized void a() {
        try {
            for (Entry value : this.b.entrySet()) {
                ((a) value.getValue()).d();
            }
            this.b.clear();
        } catch (Exception unused) {
        }
    }

    public final void b(a aVar) {
        b(aVar, null);
        this.f2541a.execute(aVar);
    }

    public final void a(a aVar, com.mintegral.msdk.base.common.e.a.b bVar) {
        b(aVar, bVar);
        this.f2541a.execute(aVar);
    }
}
