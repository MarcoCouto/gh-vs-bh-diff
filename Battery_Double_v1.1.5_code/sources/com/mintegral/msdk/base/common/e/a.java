package com.mintegral.msdk.base.common.e;

/* compiled from: CommonTask */
public abstract class a implements Runnable {
    public static long h;
    public int i = C0049a.f2540a;
    public b j;

    /* renamed from: com.mintegral.msdk.base.common.e.a$a reason: collision with other inner class name */
    /* compiled from: CommonTask */
    public enum C0049a {
        ;

        static {
            f = new int[]{f2540a, b, c, d, e};
        }
    }

    /* compiled from: CommonTask */
    public interface b {
        void a(int i);
    }

    public abstract void a();

    public abstract void b();

    public final void run() {
        try {
            if (this.i == C0049a.f2540a) {
                a(C0049a.b);
                a();
                a(C0049a.e);
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public a() {
        h++;
    }

    public static long c() {
        return h;
    }

    public final void d() {
        if (this.i != C0049a.d) {
            a(C0049a.d);
            b();
        }
    }

    private void a(int i2) {
        this.i = i2;
        if (this.j != null) {
            this.j.a(i2);
        }
    }
}
