package com.mintegral.msdk.base.common.d;

import android.content.Context;
import android.os.Build.VERSION;
import android.text.TextUtils;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.integralads.avid.library.inmobi.utils.AvidJSONUtil;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.b.b;
import com.mintegral.msdk.base.common.net.Aa;
import com.mintegral.msdk.base.common.net.l;
import com.mintegral.msdk.base.controller.a;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.entity.n;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.base.utils.r;
import com.mintegral.msdk.out.MTGConfiguration;
import com.mintegral.msdk.rover.e;
import com.tapjoy.TapjoyConstants;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import org.json.JSONArray;
import org.json.JSONObject;

/* compiled from: ReportUtil */
public class c {

    /* renamed from: a reason: collision with root package name */
    public static final String f2538a = "c";

    private static String b(Context context, String str) {
        String str2 = "";
        if (context == null) {
            return str2;
        }
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("pf", "1");
            jSONObject.put("ov", com.mintegral.msdk.base.utils.c.i());
            jSONObject.put("pn", com.mintegral.msdk.base.utils.c.n(context));
            jSONObject.put("vn", com.mintegral.msdk.base.utils.c.j(context));
            jSONObject.put("vc", com.mintegral.msdk.base.utils.c.i(context));
            jSONObject.put("ot", com.mintegral.msdk.base.utils.c.g(context));
            jSONObject.put("dm", com.mintegral.msdk.base.utils.c.c());
            jSONObject.put("bd", com.mintegral.msdk.base.utils.c.e());
            jSONObject.put("gaid", com.mintegral.msdk.base.utils.c.k());
            jSONObject.put(RequestParameters.NETWORK_MNC, com.mintegral.msdk.base.utils.c.b());
            jSONObject.put(RequestParameters.NETWORK_MCC, com.mintegral.msdk.base.utils.c.a());
            int p = com.mintegral.msdk.base.utils.c.p(context);
            jSONObject.put("nt", p);
            jSONObject.put("nts", com.mintegral.msdk.base.utils.c.a(context, p));
            jSONObject.put("l", com.mintegral.msdk.base.utils.c.f(context));
            jSONObject.put("tz", com.mintegral.msdk.base.utils.c.h());
            jSONObject.put("ua", com.mintegral.msdk.base.utils.c.f());
            jSONObject.put("app_id", a.d().j());
            jSONObject.put(MIntegralConstans.PROPERTIES_UNIT_ID, str);
            jSONObject.put("sv", MTGConfiguration.SDK_VERSION);
            jSONObject.put("gpv", com.mintegral.msdk.base.utils.c.q(context));
            StringBuilder sb = new StringBuilder();
            sb.append(com.mintegral.msdk.base.utils.c.l(context));
            sb.append(AvidJSONUtil.KEY_X);
            sb.append(com.mintegral.msdk.base.utils.c.m(context));
            jSONObject.put("ss", sb.toString());
            b.a();
            com.mintegral.msdk.b.a b = b.b(a.d().j());
            if (b != null) {
                JSONObject jSONObject2 = new JSONObject();
                try {
                    if (b.as() == 1) {
                        if (com.mintegral.msdk.base.utils.c.b(context) != null) {
                            jSONObject2.put("imei", com.mintegral.msdk.base.utils.c.b(context));
                        }
                        if (com.mintegral.msdk.base.utils.c.h(context) != null) {
                            jSONObject2.put("mac", com.mintegral.msdk.base.utils.c.h(context));
                        }
                    }
                    if (b.au() == 1 && com.mintegral.msdk.base.utils.c.d(context) != null) {
                        jSONObject2.put(TapjoyConstants.TJC_ANDROID_ID, com.mintegral.msdk.base.utils.c.d(context));
                    }
                    try {
                        if (!TextUtils.isEmpty(com.mintegral.msdk.base.utils.c.o())) {
                            jSONObject2.put("manufacturer", com.mintegral.msdk.base.utils.c.o());
                        }
                        if (!TextUtils.isEmpty(com.mintegral.msdk.base.utils.c.p())) {
                            jSONObject2.put("cpu2", com.mintegral.msdk.base.utils.c.p());
                        }
                        if (!TextUtils.isEmpty(com.mintegral.msdk.base.utils.c.r())) {
                            jSONObject2.put("tags", com.mintegral.msdk.base.utils.c.r());
                        }
                        if (!TextUtils.isEmpty(com.mintegral.msdk.base.utils.c.s())) {
                            jSONObject2.put("user", com.mintegral.msdk.base.utils.c.s());
                        }
                        if (!TextUtils.isEmpty(com.mintegral.msdk.base.utils.c.t())) {
                            jSONObject2.put("radio", com.mintegral.msdk.base.utils.c.t());
                        }
                        if (!TextUtils.isEmpty(com.mintegral.msdk.base.utils.c.u())) {
                            jSONObject2.put("bootloader", com.mintegral.msdk.base.utils.c.u());
                        }
                        if (!TextUtils.isEmpty(com.mintegral.msdk.base.utils.c.v())) {
                            jSONObject2.put("hardware", com.mintegral.msdk.base.utils.c.v());
                        }
                        if (!TextUtils.isEmpty(com.mintegral.msdk.base.utils.c.w())) {
                            jSONObject2.put("host", com.mintegral.msdk.base.utils.c.w());
                        }
                        if (!TextUtils.isEmpty(com.mintegral.msdk.base.utils.c.x())) {
                            jSONObject2.put("codename", com.mintegral.msdk.base.utils.c.x());
                        }
                        if (!TextUtils.isEmpty(com.mintegral.msdk.base.utils.c.y())) {
                            jSONObject2.put("incremental", com.mintegral.msdk.base.utils.c.y());
                        }
                        if (!TextUtils.isEmpty(com.mintegral.msdk.base.utils.c.z())) {
                            jSONObject2.put("serial", com.mintegral.msdk.base.utils.c.z());
                        }
                        if (!TextUtils.isEmpty(com.mintegral.msdk.base.utils.c.A())) {
                            jSONObject2.put("display", com.mintegral.msdk.base.utils.c.A());
                        }
                        if (!TextUtils.isEmpty(com.mintegral.msdk.base.utils.c.B())) {
                            jSONObject2.put("board", com.mintegral.msdk.base.utils.c.B());
                        }
                        if (!TextUtils.isEmpty(com.mintegral.msdk.base.utils.c.C())) {
                            jSONObject2.put("type", com.mintegral.msdk.base.utils.c.C());
                        }
                        if (!TextUtils.isEmpty(com.mintegral.msdk.base.utils.c.q())) {
                            jSONObject2.put("support", com.mintegral.msdk.base.utils.c.q());
                        }
                        if (!TextUtils.isEmpty(com.mintegral.msdk.base.utils.c.D())) {
                            jSONObject2.put("release", com.mintegral.msdk.base.utils.c.D());
                        }
                        if (com.mintegral.msdk.base.utils.c.E() != -1) {
                            jSONObject2.put("sdkint", com.mintegral.msdk.base.utils.c.E());
                        }
                        if (!TextUtils.isEmpty(com.mintegral.msdk.base.utils.c.t(context))) {
                            jSONObject2.put("battery", com.mintegral.msdk.base.utils.c.t(context));
                        }
                        if (com.mintegral.msdk.base.utils.c.s(context) != -1) {
                            jSONObject2.put("batterystatus", com.mintegral.msdk.base.utils.c.s(context));
                        }
                        if (com.mintegral.msdk.base.utils.c.F() != -1) {
                            jSONObject2.put("baseos", com.mintegral.msdk.base.utils.c.F());
                        }
                        if (!TextUtils.isEmpty(com.mintegral.msdk.base.utils.c.v(context))) {
                            jSONObject2.put("is24H", com.mintegral.msdk.base.utils.c.v(context));
                        }
                        if (com.mintegral.msdk.base.utils.c.w(context) != -1) {
                            jSONObject2.put("sensor", com.mintegral.msdk.base.utils.c.w(context));
                        }
                        if (!TextUtils.isEmpty(com.mintegral.msdk.base.utils.c.x(context))) {
                            jSONObject2.put("ime", com.mintegral.msdk.base.utils.c.x(context));
                        }
                        if (com.mintegral.msdk.base.utils.c.u(context) != -1) {
                            jSONObject2.put("phonetype", com.mintegral.msdk.base.utils.c.u(context));
                        }
                        if (!TextUtils.isEmpty(com.mintegral.msdk.base.utils.c.G())) {
                            jSONObject2.put("totalram", com.mintegral.msdk.base.utils.c.G());
                        }
                        if (!TextUtils.isEmpty(com.mintegral.msdk.base.utils.c.y(context))) {
                            jSONObject2.put("totalmemory", com.mintegral.msdk.base.utils.c.y(context));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    } catch (Throwable th) {
                        th.printStackTrace();
                    }
                    if (!TextUtils.isEmpty(jSONObject2.toString())) {
                        String b2 = com.mintegral.msdk.base.utils.a.b(jSONObject2.toString());
                        if (!TextUtils.isEmpty(b2)) {
                            jSONObject.put("dvi", URLEncoder.encode(b2, "utf-8"));
                        }
                    }
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
            return jSONObject.toString();
        } catch (Exception e3) {
            e3.printStackTrace();
            return str2;
        }
    }

    public static String a(int i, String str) {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("category", "adtrack");
            jSONObject.put("action", str);
            jSONObject.put("label", i);
            jSONObject.put("value", "");
        } catch (Exception unused) {
            g.d(f2538a, "ad track data failed !");
        }
        return jSONObject.toString();
    }

    public static l a(String str, String str2, Context context, String str3) {
        String str4;
        if (context != null) {
            context = context.getApplicationContext();
        }
        l lVar = new l();
        lVar.a("m_device_info", b(context, str3));
        lVar.a("m_action", str);
        try {
            if (!TextUtils.isEmpty(str2)) {
                String str5 = f2538a;
                StringBuilder sb = new StringBuilder("8.5.0 add channel ,before value : ");
                sb.append(str2);
                g.a(str5, sb.toString());
                String a2 = Aa.a();
                if (a2 == null) {
                    a2 = "";
                }
                JSONObject jSONObject = new JSONObject(str2);
                jSONObject.put("channel", a2);
                str4 = jSONObject.toString();
                try {
                    String str6 = f2538a;
                    StringBuilder sb2 = new StringBuilder("8.5.0 add channel ,update value : ");
                    sb2.append(str4);
                    g.a(str6, sb2.toString());
                } catch (Throwable th) {
                    Throwable th2 = th;
                    str2 = str4;
                    th = th2;
                }
                lVar.a("m_data", str4);
                lVar.a("m_sdk", "msdk");
                return lVar;
            }
        } catch (Throwable th3) {
            th = th3;
            th.printStackTrace();
            str4 = str2;
            lVar.a("m_data", str4);
            lVar.a("m_sdk", "msdk");
            return lVar;
        }
        str4 = str2;
        lVar.a("m_data", str4);
        lVar.a("m_sdk", "msdk");
        return lVar;
    }

    private static l a(Context context) {
        l lVar = new l();
        try {
            lVar.a(TapjoyConstants.TJC_PLATFORM, "1");
            lVar.a(CampaignEx.JSON_KEY_PACKAGE_NAME, URLEncoder.encode(com.mintegral.msdk.base.utils.c.n(context)));
            com.mintegral.msdk.base.controller.authoritycontroller.a.a();
            if (com.mintegral.msdk.base.controller.authoritycontroller.a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA)) {
                lVar.a(TapjoyConstants.TJC_DEVICE_OS_VERSION_NAME, VERSION.RELEASE);
                lVar.a("brand", URLEncoder.encode(com.mintegral.msdk.base.utils.c.e()));
                lVar.a("model", URLEncoder.encode(com.mintegral.msdk.base.utils.c.c()));
                lVar.a("gaid", com.mintegral.msdk.base.utils.c.k());
                lVar.a(RequestParameters.NETWORK_MNC, com.mintegral.msdk.base.utils.c.b());
                lVar.a(RequestParameters.NETWORK_MCC, com.mintegral.msdk.base.utils.c.a());
                int p = com.mintegral.msdk.base.utils.c.p(context);
                lVar.a("network_type", String.valueOf(p));
                lVar.a("network_str", com.mintegral.msdk.base.utils.c.a(context, p));
                lVar.a("language", URLEncoder.encode(com.mintegral.msdk.base.utils.c.f(context)));
                lVar.a(TapjoyConstants.TJC_DEVICE_TIMEZONE, URLEncoder.encode(com.mintegral.msdk.base.utils.c.h()));
                lVar.a("ua", URLEncoder.encode(com.mintegral.msdk.base.utils.c.f()));
                lVar.a("gp_version", URLEncoder.encode(com.mintegral.msdk.base.utils.c.q(context)));
            }
            lVar.a("sdk_version", MTGConfiguration.SDK_VERSION);
            lVar.a("app_version_name", URLEncoder.encode(com.mintegral.msdk.base.utils.c.j(context)));
            StringBuilder sb = new StringBuilder();
            sb.append(com.mintegral.msdk.base.utils.c.g(context));
            lVar.a("orientation", URLEncoder.encode(sb.toString()));
            try {
                com.mintegral.msdk.base.controller.authoritycontroller.a.a();
                if (com.mintegral.msdk.base.controller.authoritycontroller.a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA)) {
                    Class.forName("com.google.android.gms.common.GooglePlayServicesUtil");
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append(GooglePlayServicesUtil.GOOGLE_PLAY_SERVICES_VERSION_CODE);
                    lVar.a("gpsv", sb2.toString());
                }
            } catch (Exception unused) {
                g.d(f2538a, "can't find com.google.android.gms.common.GooglePlayServicesUtil class");
            } catch (Throwable th) {
                th.printStackTrace();
            }
            StringBuilder sb3 = new StringBuilder();
            sb3.append(com.mintegral.msdk.base.utils.c.l(context));
            sb3.append(AvidJSONUtil.KEY_X);
            sb3.append(com.mintegral.msdk.base.utils.c.m(context));
            lVar.a("screen_size", sb3.toString());
            b.a();
            com.mintegral.msdk.b.a b = b.b(a.d().j());
            if (b != null) {
                JSONObject jSONObject = new JSONObject();
                try {
                    if (b.as() == 1) {
                        if (com.mintegral.msdk.base.utils.c.b(context) != null) {
                            jSONObject.put("imei", com.mintegral.msdk.base.utils.c.b(context));
                        }
                        if (com.mintegral.msdk.base.utils.c.h(context) != null) {
                            jSONObject.put("mac", com.mintegral.msdk.base.utils.c.h(context));
                        }
                    }
                    if (b.au() == 1 && com.mintegral.msdk.base.utils.c.d(context) != null) {
                        jSONObject.put(TapjoyConstants.TJC_ANDROID_ID, com.mintegral.msdk.base.utils.c.d(context));
                    }
                    try {
                        if (!TextUtils.isEmpty(com.mintegral.msdk.base.utils.c.o())) {
                            jSONObject.put("manufacturer", com.mintegral.msdk.base.utils.c.o());
                        }
                        if (!TextUtils.isEmpty(com.mintegral.msdk.base.utils.c.p())) {
                            jSONObject.put("cpu2", com.mintegral.msdk.base.utils.c.p());
                        }
                        if (!TextUtils.isEmpty(com.mintegral.msdk.base.utils.c.r())) {
                            jSONObject.put("tags", com.mintegral.msdk.base.utils.c.r());
                        }
                        if (!TextUtils.isEmpty(com.mintegral.msdk.base.utils.c.s())) {
                            jSONObject.put("user", com.mintegral.msdk.base.utils.c.s());
                        }
                        if (!TextUtils.isEmpty(com.mintegral.msdk.base.utils.c.t())) {
                            jSONObject.put("radio", com.mintegral.msdk.base.utils.c.t());
                        }
                        if (!TextUtils.isEmpty(com.mintegral.msdk.base.utils.c.u())) {
                            jSONObject.put("bootloader", com.mintegral.msdk.base.utils.c.u());
                        }
                        if (!TextUtils.isEmpty(com.mintegral.msdk.base.utils.c.v())) {
                            jSONObject.put("hardware", com.mintegral.msdk.base.utils.c.v());
                        }
                        if (!TextUtils.isEmpty(com.mintegral.msdk.base.utils.c.w())) {
                            jSONObject.put("host", com.mintegral.msdk.base.utils.c.w());
                        }
                        if (!TextUtils.isEmpty(com.mintegral.msdk.base.utils.c.x())) {
                            jSONObject.put("codename", com.mintegral.msdk.base.utils.c.x());
                        }
                        if (!TextUtils.isEmpty(com.mintegral.msdk.base.utils.c.y())) {
                            jSONObject.put("incremental", com.mintegral.msdk.base.utils.c.y());
                        }
                        if (!TextUtils.isEmpty(com.mintegral.msdk.base.utils.c.z())) {
                            jSONObject.put("serial", com.mintegral.msdk.base.utils.c.z());
                        }
                        if (!TextUtils.isEmpty(com.mintegral.msdk.base.utils.c.A())) {
                            jSONObject.put("display", com.mintegral.msdk.base.utils.c.A());
                        }
                        if (!TextUtils.isEmpty(com.mintegral.msdk.base.utils.c.B())) {
                            jSONObject.put("board", com.mintegral.msdk.base.utils.c.B());
                        }
                        if (!TextUtils.isEmpty(com.mintegral.msdk.base.utils.c.C())) {
                            jSONObject.put("type", com.mintegral.msdk.base.utils.c.C());
                        }
                        if (!TextUtils.isEmpty(com.mintegral.msdk.base.utils.c.q())) {
                            jSONObject.put("support", com.mintegral.msdk.base.utils.c.q());
                        }
                        if (!TextUtils.isEmpty(com.mintegral.msdk.base.utils.c.D())) {
                            jSONObject.put("release", com.mintegral.msdk.base.utils.c.D());
                        }
                        if (com.mintegral.msdk.base.utils.c.E() != -1) {
                            jSONObject.put("sdkint", com.mintegral.msdk.base.utils.c.E());
                        }
                        if (!TextUtils.isEmpty(com.mintegral.msdk.base.utils.c.t(context))) {
                            jSONObject.put("battery", com.mintegral.msdk.base.utils.c.t(context));
                        }
                        if (com.mintegral.msdk.base.utils.c.s(context) != -1) {
                            jSONObject.put("batterystatus", com.mintegral.msdk.base.utils.c.s(context));
                        }
                        if (com.mintegral.msdk.base.utils.c.F() != -1) {
                            jSONObject.put("baseos", com.mintegral.msdk.base.utils.c.F());
                        }
                        if (!TextUtils.isEmpty(com.mintegral.msdk.base.utils.c.v(context))) {
                            jSONObject.put("is24H", com.mintegral.msdk.base.utils.c.v(context));
                        }
                        if (com.mintegral.msdk.base.utils.c.w(context) != -1) {
                            jSONObject.put("sensor", com.mintegral.msdk.base.utils.c.w(context));
                        }
                        if (!TextUtils.isEmpty(com.mintegral.msdk.base.utils.c.x(context))) {
                            jSONObject.put("ime", com.mintegral.msdk.base.utils.c.x(context));
                        }
                        if (com.mintegral.msdk.base.utils.c.u(context) != -1) {
                            jSONObject.put("phonetype", com.mintegral.msdk.base.utils.c.u(context));
                        }
                        if (!TextUtils.isEmpty(com.mintegral.msdk.base.utils.c.G())) {
                            jSONObject.put("totalram", com.mintegral.msdk.base.utils.c.G());
                        }
                        if (!TextUtils.isEmpty(com.mintegral.msdk.base.utils.c.y(context))) {
                            jSONObject.put("totalmemory", com.mintegral.msdk.base.utils.c.y(context));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    } catch (Throwable th2) {
                        th2.printStackTrace();
                    }
                    if (!TextUtils.isEmpty(jSONObject.toString())) {
                        String b2 = com.mintegral.msdk.base.utils.a.b(jSONObject.toString());
                        if (!TextUtils.isEmpty(b2)) {
                            lVar.a("dvi", b2);
                        }
                    }
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
        } catch (Exception e3) {
            if (MIntegralConstans.DEBUG) {
                e3.printStackTrace();
            }
        }
        return lVar;
    }

    public static l a(Context context, n nVar) {
        try {
            l a2 = a(context);
            String str = "app_id";
            try {
                StringBuilder sb = new StringBuilder();
                sb.append(a.d().j());
                a2.a(str, sb.toString());
                a2.a("data", URLEncoder.encode(nVar.d()));
                return a2;
            } catch (Exception unused) {
                return a2;
            }
        } catch (Exception unused2) {
            return null;
        }
    }

    public static l a(String str, Context context) {
        l a2 = a(context);
        StringBuilder sb = new StringBuilder();
        sb.append(a.d().j());
        a2.a("app_id", sb.toString());
        if (!TextUtils.isEmpty(str)) {
            try {
                a2.a("data", URLEncoder.encode(str, "utf-8"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        a2.a("m_sdk", "msdk");
        return a2;
    }

    public static l a(Context context, String str) {
        l a2 = a(context);
        StringBuilder sb = new StringBuilder();
        sb.append(a.d().j());
        a2.a("app_id", sb.toString());
        if (!TextUtils.isEmpty(str)) {
            try {
                a2.a("data", URLEncoder.encode(str, "utf-8"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        return a2;
    }

    public static l a(String str, Context context, String str2) {
        l a2 = a(context);
        StringBuilder sb = new StringBuilder();
        sb.append(a.d().j());
        a2.a("app_id", sb.toString());
        a2.a(MIntegralConstans.PROPERTIES_UNIT_ID, str2);
        if (!TextUtils.isEmpty(str)) {
            try {
                a2.a("data", URLEncoder.encode(str, "utf-8"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        a2.a("m_sdk", "msdk");
        return a2;
    }

    public static l b(String str, Context context) {
        l a2 = a(context);
        StringBuilder sb = new StringBuilder();
        sb.append(a.d().j());
        a2.a("app_id", sb.toString());
        if (!TextUtils.isEmpty(str)) {
            try {
                a2.a("data", URLEncoder.encode(str, "utf-8"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        a2.a("m_sdk", "msdk");
        return a2;
    }

    public static String a(String str, Map<String, Object> map) {
        StringBuilder sb = new StringBuilder();
        StringBuilder sb2 = new StringBuilder("key=");
        sb2.append(str);
        sb.append(sb2.toString());
        if (!map.isEmpty()) {
            for (Entry entry : map.entrySet()) {
                StringBuilder sb3 = new StringBuilder(RequestParameters.AMPERSAND);
                sb3.append((String) entry.getKey());
                sb3.append(RequestParameters.EQUAL);
                sb3.append(entry.getValue());
                sb.append(sb3.toString());
            }
        }
        sb.append("\n");
        return sb.toString();
    }

    public static l a(CampaignEx campaignEx, List<e> list) {
        l lVar = new l();
        JSONArray jSONArray = new JSONArray();
        JSONObject jSONObject = new JSONObject();
        if (campaignEx != null) {
            try {
                jSONObject.put("cid", campaignEx.getId());
                jSONObject.put(CampaignEx.ROVER_KEY_MARK, campaignEx.getRoverMark());
            } catch (Exception unused) {
            }
        }
        if (list != null && list.size() >= 0) {
            JSONArray jSONArray2 = new JSONArray();
            for (e eVar : list) {
                if (eVar != null) {
                    jSONArray2.put(eVar.a());
                }
            }
            jSONObject.put("urls", jSONArray2);
        }
        jSONArray.put(jSONObject);
        lVar.a("data", jSONArray.toString());
        return lVar;
    }

    public static boolean a() {
        try {
            if (System.currentTimeMillis() - 86400000 > ((Long) r.b(a.d().h(), "privateAuthorityTimesTamp", Long.valueOf(0))).longValue()) {
                return true;
            }
            return false;
        } catch (Throwable th) {
            th.printStackTrace();
            return false;
        }
    }

    public static void b() {
        try {
            r.a(a.d().h(), "privateAuthorityTimesTamp", Long.valueOf(System.currentTimeMillis()));
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public static void a(Context context, CampaignEx campaignEx, int i, int i2) {
        try {
            StringBuffer stringBuffer = new StringBuffer("key=2000056&");
            if (campaignEx != null) {
                StringBuilder sb = new StringBuilder("cid=");
                sb.append(campaignEx.getId());
                sb.append(RequestParameters.AMPERSAND);
                stringBuffer.append(sb.toString());
            }
            b.a();
            com.mintegral.msdk.b.a b = b.b(a.d().j());
            if (b == null) {
                b.a();
                b = b.b();
            }
            StringBuilder sb2 = new StringBuilder("unit_id=");
            sb2.append(b.j());
            sb2.append(RequestParameters.AMPERSAND);
            stringBuffer.append(sb2.toString());
            String k = com.mintegral.msdk.base.utils.c.k();
            if (!TextUtils.isEmpty(k)) {
                StringBuilder sb3 = new StringBuilder("gaid=");
                sb3.append(k);
                sb3.append(RequestParameters.AMPERSAND);
                stringBuffer.append(sb3.toString());
            }
            StringBuilder sb4 = new StringBuilder("action_type=");
            sb4.append(i);
            sb4.append(RequestParameters.AMPERSAND);
            stringBuffer.append(sb4.toString());
            StringBuilder sb5 = new StringBuilder("jm_a=");
            sb5.append(com.mintegral.msdk.c.b.a(context).c());
            sb5.append(RequestParameters.AMPERSAND);
            stringBuffer.append(sb5.toString());
            StringBuilder sb6 = new StringBuilder("jm_n=");
            sb6.append(com.mintegral.msdk.c.b.a(context).a());
            sb6.append(RequestParameters.AMPERSAND);
            stringBuffer.append(sb6.toString());
            if (campaignEx != null) {
                StringBuilder sb7 = new StringBuilder("rid_n=");
                sb7.append(campaignEx.getRequestIdNotice());
                sb7.append(RequestParameters.AMPERSAND);
                stringBuffer.append(sb7.toString());
            }
            StringBuilder sb8 = new StringBuilder("result_type=");
            sb8.append(i2);
            stringBuffer.append(sb8.toString());
            new b(context).b(stringBuffer.toString());
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }
}
