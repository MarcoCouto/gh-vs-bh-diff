package com.mintegral.msdk.base.common.d;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.entity.p;
import com.mintegral.msdk.base.utils.c;

/* compiled from: PlayableReportUtils */
public final class a {

    /* renamed from: a reason: collision with root package name */
    private static Handler f2520a = new Handler(Looper.getMainLooper());

    public static void a(CampaignEx campaignEx, String str, String str2, String str3) {
        if (campaignEx != null && campaignEx.isMraid()) {
            p pVar = new p("2000043", TextUtils.isEmpty(campaignEx.getMraid()) ^ true ? 1 : 3, "0", "", campaignEx.getId(), str2, str, String.valueOf(campaignEx.getKeyIaRst()));
            pVar.m(campaignEx.getId());
            pVar.k(campaignEx.getRequestIdNotice());
            pVar.h(str3);
            pVar.b(c.p(com.mintegral.msdk.base.controller.a.d().h()));
            pVar.a(campaignEx.isMraid() ? p.f2605a : p.b);
            a(pVar, com.mintegral.msdk.base.controller.a.d().h(), str2);
        }
    }

    public static void a(p pVar, Context context, String str) {
        if (pVar != null) {
            pVar.b(c.k());
            a(p.d(pVar), context, str);
        }
    }

    public static void b(p pVar, Context context, String str) {
        pVar.n("2000060");
        pVar.l(str);
        pVar.b(c.p(context));
        a(p.b(pVar), context, str);
    }

    public static void c(p pVar, Context context, String str) {
        pVar.n("2000059");
        pVar.l(str);
        pVar.b(c.p(context));
        pVar.b(c.k());
        a(p.c(pVar), context, str);
    }

    private static void a(String str, Context context, String str2) {
        if (context != null && !TextUtils.isEmpty(str) && !TextUtils.isEmpty(str2)) {
            com.mintegral.msdk.base.common.f.a aVar = new com.mintegral.msdk.base.common.f.a(str, str2);
            if (f2520a != null) {
                f2520a.post(aVar);
            }
        }
    }

    public static void d(p pVar, Context context, String str) {
        a(p.h(pVar), context, str);
    }

    public static void e(p pVar, Context context, String str) {
        a(p.i(pVar), context, str);
    }

    public static void f(p pVar, Context context, String str) {
        pVar.n("2000063");
        pVar.l(str);
        pVar.b(c.p(context));
        a(p.a(pVar), context, str);
    }
}
