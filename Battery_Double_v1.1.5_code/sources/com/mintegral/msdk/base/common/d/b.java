package com.mintegral.msdk.base.common.d;

import android.content.Context;
import android.os.Build.VERSION;
import android.text.TextUtils;
import com.facebook.share.internal.ShareConstants;
import com.integralads.avid.library.inmobi.utils.AvidJSONUtil;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.base.b.h;
import com.mintegral.msdk.base.b.i;
import com.mintegral.msdk.base.b.r;
import com.mintegral.msdk.base.b.t;
import com.mintegral.msdk.base.common.d.c.a;
import com.mintegral.msdk.base.common.net.d;
import com.mintegral.msdk.base.common.net.l;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.entity.n;
import com.mintegral.msdk.base.utils.CommonMD5;
import com.mintegral.msdk.base.utils.c;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.out.Frame;
import com.mintegral.msdk.out.MTGConfiguration;
import com.mintegral.msdk.rover.e;
import com.mintegral.msdk.rover.f;
import com.tapjoy.TapjoyConstants;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.io.File;
import java.net.URLEncoder;
import java.util.List;
import org.json.JSONObject;

/* compiled from: ReportController */
public class b {

    /* renamed from: a reason: collision with root package name */
    public static final String f2523a = "b";
    /* access modifiers changed from: private */
    public Context b;
    private int c = 0;

    public b(Context context, int i) {
        this.b = context;
        this.c = i;
    }

    public b(Context context) {
        this.b = context;
    }

    public final void a(int i, String str) {
        new a(this.b, 0).b(com.mintegral.msdk.base.common.a.f, c.a("event", c.a(i, ShareConstants.WEB_DIALOG_RESULT_PARAM_REQUEST_ID), this.b, str), new com.mintegral.msdk.base.common.d.c.b() {
            public final void b(String str) {
            }

            public final void a(String str) {
                g.b(b.f2523a, "report success");
            }
        });
    }

    public final void a(final n nVar, final Boolean bool) {
        if (nVar != null) {
            if (nVar.c().equals(HttpRequest.METHOD_GET)) {
                a aVar = new a(this.b, this.c);
                aVar.c();
                aVar.a(nVar.b(), (l) null, (d<?>) new com.mintegral.msdk.base.common.d.c.b() {
                    public final void b(String str) {
                    }

                    public final void a(String str) {
                        g.b(b.f2523a, "report success");
                        r.a((h) i.a(b.this.b)).a(nVar.b());
                        if (bool.booleanValue() && r.a((h) i.a(b.this.b)).d() > 20) {
                            com.mintegral.msdk.base.controller.b.a().c();
                        }
                    }
                });
            } else if (nVar.c().equals(HttpRequest.METHOD_POST)) {
                a aVar2 = new a(this.b, this.c);
                aVar2.c();
                if (!TextUtils.isEmpty(nVar.d())) {
                    aVar2.b(nVar.b(), c.a(nVar.d(), this.b, nVar.a()), new com.mintegral.msdk.base.common.d.c.b() {
                        public final void b(String str) {
                        }

                        public final void a(String str) {
                            g.b(b.f2523a, "report success");
                            r.a((h) i.a(b.this.b)).a(nVar.d(), nVar.b());
                            if (bool.booleanValue() && r.a((h) i.a(b.this.b)).d() > 20) {
                                com.mintegral.msdk.base.controller.b.a().c();
                            }
                        }
                    });
                }
            }
        }
    }

    public final void a(final n nVar) {
        a aVar = new a(this.b, this.c);
        aVar.c();
        aVar.b(com.mintegral.msdk.base.common.a.f, c.a(this.b, nVar), new com.mintegral.msdk.base.common.d.c.b() {
            public final void b(String str) {
            }

            public final void a(String str) {
                g.b(b.f2523a, "report success");
                try {
                    if (nVar != null) {
                        t.a((h) i.a(b.this.b)).a(String.valueOf(nVar.e()));
                    }
                } catch (Exception unused) {
                }
            }
        });
    }

    public final void a(String str) {
        c(str);
    }

    private void c(String str) {
        try {
            a aVar = new a(this.b, this.c);
            aVar.c();
            aVar.b(com.mintegral.msdk.base.common.a.f, c.a(str, this.b, ""), new com.mintegral.msdk.base.common.d.c.b() {
                public final void a(String str) {
                    String str2 = b.f2523a;
                    StringBuilder sb = new StringBuilder("reportPB success data:");
                    sb.append(str);
                    g.b(str2, sb.toString());
                }

                public final void b(String str) {
                    String str2 = b.f2523a;
                    StringBuilder sb = new StringBuilder("reportPB onFailed msg:");
                    sb.append(str);
                    g.b(str2, sb.toString());
                }
            });
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public final void b(String str) {
        c(str);
    }

    public final void a(String str, String str2, String str3, String str4) {
        try {
            a aVar = new a(this.b, this.c);
            aVar.c();
            StringBuilder sb = new StringBuilder("click_type=");
            sb.append(URLEncoder.encode(str, "utf-8"));
            sb.append("&cid=");
            sb.append(URLEncoder.encode(str2, "utf-8"));
            sb.append("&unit_id=");
            sb.append(URLEncoder.encode(str3, "utf-8"));
            sb.append("&key=");
            sb.append(URLEncoder.encode("2000027", "utf-8"));
            sb.append("&http_url=");
            sb.append(URLEncoder.encode(str4, "utf-8"));
            aVar.b(com.mintegral.msdk.base.common.a.f, c.a(sb.toString(), this.b, str3), new com.mintegral.msdk.base.common.d.c.b() {
                public final void a(String str) {
                    g.a("", "SSL REPORT SUCCESS");
                }

                public final void b(String str) {
                    g.a("", "SSL REPORT FAILED");
                }
            });
        } catch (Exception unused) {
            g.d(f2523a, "ssl  error report failed");
        }
    }

    public final void a(String str, final File file) {
        a aVar = new a(this.b, this.c);
        aVar.c();
        aVar.b(com.mintegral.msdk.base.common.a.f, c.a(this.b, str), new com.mintegral.msdk.base.common.d.c.b() {
            public final void a(String str) {
                g.b(b.f2523a, "report success exception");
                if (file != null) {
                    file.delete();
                }
            }

            public final void b(String str) {
                g.b(b.f2523a, "report failed exception");
            }
        });
    }

    public final void a(final String str, String str2, String str3, Frame frame) {
        a aVar = new a(this.b, this.c);
        aVar.c();
        l a2 = c.a(str2, this.b, str3);
        if (frame != null) {
            a2.a("session_id", frame.getSessionId());
            a2.a("parent_session_id", frame.getParentSessionId());
        }
        aVar.b(com.mintegral.msdk.base.common.a.f, a2, new com.mintegral.msdk.base.common.d.c.b() {
            public final void a(String str) {
                g.b(b.f2523a, "report success");
                if (!"net_time_stats".equals(str)) {
                    if ("click_duration".equals(str)) {
                        com.mintegral.msdk.base.controller.b.a().c();
                    } else if ("load_duration".equals(str)) {
                        com.mintegral.msdk.base.controller.b.a().c();
                        com.mintegral.msdk.base.controller.b.a().c();
                    }
                }
            }

            public final void b(String str) {
                g.b(b.f2523a, "report success");
            }
        });
    }

    public final void a(com.mintegral.msdk.base.entity.b bVar, String str) {
        a aVar = new a(this.b, this.c);
        aVar.c();
        StringBuilder sb = new StringBuilder();
        String k = bVar.k();
        com.mintegral.msdk.base.controller.authoritycontroller.a.a();
        if (com.mintegral.msdk.base.controller.authoritycontroller.a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA)) {
            StringBuilder sb2 = new StringBuilder("rid_n=");
            sb2.append(bVar.n());
            sb.append(sb2.toString());
            StringBuilder sb3 = new StringBuilder("&network_type=");
            sb3.append(bVar.a());
            sb.append(sb3.toString());
            StringBuilder sb4 = new StringBuilder("&network_str=");
            sb4.append(bVar.b());
            sb.append(sb4.toString());
            StringBuilder sb5 = new StringBuilder("&click_type=");
            sb5.append(bVar.g());
            sb.append(sb5.toString());
            StringBuilder sb6 = new StringBuilder("&type=");
            sb6.append(bVar.j());
            sb.append(sb6.toString());
            StringBuilder sb7 = new StringBuilder("&cid=");
            sb7.append(bVar.l());
            sb.append(sb7.toString());
            StringBuilder sb8 = new StringBuilder("&click_duration=");
            sb8.append(bVar.m());
            sb.append(sb8.toString());
            sb.append("&key=2000012");
            StringBuilder sb9 = new StringBuilder("&unit_id=");
            sb9.append(bVar.c());
            sb.append(sb9.toString());
            StringBuilder sb10 = new StringBuilder("&last_url=");
            sb10.append(k);
            sb.append(sb10.toString());
            StringBuilder sb11 = new StringBuilder("&code=");
            sb11.append(bVar.i());
            sb.append(sb11.toString());
            StringBuilder sb12 = new StringBuilder("&exception=");
            sb12.append(bVar.h());
            sb.append(sb12.toString());
            StringBuilder sb13 = new StringBuilder("&landing_type=");
            sb13.append(bVar.d());
            sb.append(sb13.toString());
            StringBuilder sb14 = new StringBuilder("&link_type=");
            sb14.append(bVar.e());
            sb.append(sb14.toString());
            StringBuilder sb15 = new StringBuilder("&click_time=");
            sb15.append(bVar.f());
            sb15.append("\n");
            sb.append(sb15.toString());
        } else {
            StringBuilder sb16 = new StringBuilder("rid_n=");
            sb16.append(bVar.n());
            sb.append(sb16.toString());
            StringBuilder sb17 = new StringBuilder("&click_type=");
            sb17.append(bVar.g());
            sb.append(sb17.toString());
            StringBuilder sb18 = new StringBuilder("&type=");
            sb18.append(bVar.j());
            sb.append(sb18.toString());
            StringBuilder sb19 = new StringBuilder("&cid=");
            sb19.append(bVar.l());
            sb.append(sb19.toString());
            StringBuilder sb20 = new StringBuilder("&click_duration=");
            sb20.append(bVar.m());
            sb.append(sb20.toString());
            sb.append("&key=2000012");
            StringBuilder sb21 = new StringBuilder("&unit_id=");
            sb21.append(bVar.c());
            sb.append(sb21.toString());
            StringBuilder sb22 = new StringBuilder("&last_url=");
            sb22.append(k);
            sb.append(sb22.toString());
            StringBuilder sb23 = new StringBuilder("&code=");
            sb23.append(bVar.i());
            sb.append(sb23.toString());
            StringBuilder sb24 = new StringBuilder("&exception=");
            sb24.append(bVar.h());
            sb.append(sb24.toString());
            StringBuilder sb25 = new StringBuilder("&landing_type=");
            sb25.append(bVar.d());
            sb.append(sb25.toString());
            StringBuilder sb26 = new StringBuilder("&link_type=");
            sb26.append(bVar.e());
            sb.append(sb26.toString());
            StringBuilder sb27 = new StringBuilder("&click_time=");
            sb27.append(bVar.f());
            sb27.append("\n");
            sb.append(sb27.toString());
        }
        String sb28 = sb.toString();
        if (!TextUtils.isEmpty(sb28)) {
            aVar.b(com.mintegral.msdk.base.common.a.f, c.a(sb28, this.b, str), new com.mintegral.msdk.base.common.d.c.b() {
                public final void b(String str) {
                }

                public final void a(String str) {
                    g.b(b.f2523a, "report success");
                }
            });
        }
    }

    public final void a(CampaignEx campaignEx, List<e> list, f fVar) {
        a aVar = new a(this.b, this.c);
        aVar.c();
        String b2 = b();
        l a2 = c.a(campaignEx, list);
        StringBuilder sb = new StringBuilder();
        sb.append(com.mintegral.msdk.rover.a.b);
        sb.append(b2);
        aVar.b(sb.toString(), a2, fVar);
    }

    private String b() {
        StringBuffer stringBuffer = new StringBuffer("?");
        stringBuffer.append("platform=");
        stringBuffer.append(URLEncoder.encode("1"));
        stringBuffer.append(RequestParameters.AMPERSAND);
        stringBuffer.append("os_version=");
        stringBuffer.append(URLEncoder.encode(VERSION.RELEASE));
        stringBuffer.append(RequestParameters.AMPERSAND);
        stringBuffer.append("package_name=");
        stringBuffer.append(URLEncoder.encode(c.n(this.b)));
        stringBuffer.append(RequestParameters.AMPERSAND);
        stringBuffer.append("app_version_name=");
        stringBuffer.append(URLEncoder.encode(c.j(this.b)));
        stringBuffer.append(RequestParameters.AMPERSAND);
        stringBuffer.append("app_version_code=");
        StringBuilder sb = new StringBuilder();
        sb.append(c.i(this.b));
        stringBuffer.append(URLEncoder.encode(sb.toString()));
        stringBuffer.append(RequestParameters.AMPERSAND);
        stringBuffer.append("screen_size=");
        StringBuilder sb2 = new StringBuilder();
        sb2.append(c.l(this.b));
        sb2.append(AvidJSONUtil.KEY_X);
        sb2.append(c.m(this.b));
        stringBuffer.append(URLEncoder.encode(sb2.toString()));
        stringBuffer.append(RequestParameters.AMPERSAND);
        stringBuffer.append("orientation=");
        StringBuilder sb3 = new StringBuilder();
        sb3.append(c.g(this.b));
        stringBuffer.append(URLEncoder.encode(sb3.toString()));
        stringBuffer.append(RequestParameters.AMPERSAND);
        stringBuffer.append("gaid=");
        stringBuffer.append(URLEncoder.encode(c.k()));
        stringBuffer.append(RequestParameters.AMPERSAND);
        String encode = URLEncoder.encode(c.e());
        stringBuffer.append("brand=");
        stringBuffer.append(encode);
        stringBuffer.append(RequestParameters.AMPERSAND);
        stringBuffer.append("mnc=");
        stringBuffer.append(URLEncoder.encode(c.b()));
        stringBuffer.append(RequestParameters.AMPERSAND);
        stringBuffer.append("mcc=");
        stringBuffer.append(URLEncoder.encode(c.a()));
        stringBuffer.append(RequestParameters.AMPERSAND);
        int p = c.p(this.b);
        stringBuffer.append("network_type=");
        stringBuffer.append(URLEncoder.encode(String.valueOf(p)));
        stringBuffer.append(RequestParameters.AMPERSAND);
        stringBuffer.append("network_str");
        stringBuffer.append(URLEncoder.encode(c.a(this.b, p)));
        stringBuffer.append(RequestParameters.AMPERSAND);
        stringBuffer.append("language=");
        stringBuffer.append(URLEncoder.encode(c.f(this.b)));
        stringBuffer.append(RequestParameters.AMPERSAND);
        stringBuffer.append("timezone=");
        stringBuffer.append(URLEncoder.encode(c.h()));
        stringBuffer.append(RequestParameters.AMPERSAND);
        String encode2 = URLEncoder.encode(c.f());
        stringBuffer.append("useragent=");
        stringBuffer.append(encode2);
        stringBuffer.append(RequestParameters.AMPERSAND);
        stringBuffer.append("sdk_version=");
        stringBuffer.append(URLEncoder.encode(MTGConfiguration.SDK_VERSION));
        stringBuffer.append(RequestParameters.AMPERSAND);
        String encode3 = URLEncoder.encode(c.q(this.b));
        stringBuffer.append("gp_version=");
        stringBuffer.append(encode3);
        stringBuffer.append(RequestParameters.AMPERSAND);
        stringBuffer.append("sign=");
        StringBuilder sb4 = new StringBuilder();
        sb4.append(com.mintegral.msdk.base.controller.a.d().j());
        sb4.append(com.mintegral.msdk.base.controller.a.d().k());
        stringBuffer.append(URLEncoder.encode(CommonMD5.getMD5(sb4.toString())));
        stringBuffer.append(RequestParameters.AMPERSAND);
        stringBuffer.append("app_id=");
        stringBuffer.append(URLEncoder.encode(com.mintegral.msdk.base.controller.a.d().j()));
        stringBuffer.append(RequestParameters.AMPERSAND);
        com.mintegral.msdk.b.b.a();
        com.mintegral.msdk.b.a b2 = com.mintegral.msdk.b.b.b(com.mintegral.msdk.base.controller.a.d().j());
        if (b2 != null) {
            JSONObject jSONObject = new JSONObject();
            try {
                if (b2.as() == 1) {
                    if (c.b(this.b) != null) {
                        jSONObject.put("imei", c.b(this.b));
                    }
                    if (c.h(this.b) != null) {
                        jSONObject.put("mac", c.h(this.b));
                    }
                }
                if (b2.au() == 1 && c.d(this.b) != null) {
                    jSONObject.put(TapjoyConstants.TJC_ANDROID_ID, c.d(this.b));
                }
                if (!TextUtils.isEmpty(jSONObject.toString())) {
                    String b3 = com.mintegral.msdk.base.utils.a.b(jSONObject.toString());
                    if (!TextUtils.isEmpty(b3)) {
                        stringBuffer.append("dvi=");
                        stringBuffer.append(b3);
                        stringBuffer.append(RequestParameters.AMPERSAND);
                    } else {
                        stringBuffer.append("dvi=&");
                    }
                } else {
                    stringBuffer.append("dvi=&");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            stringBuffer.append("dvi=&");
        }
        stringBuffer.append("unit_id=0");
        return stringBuffer.toString();
    }

    public final void a() {
        try {
            if (c.a()) {
                a aVar = new a(this.b);
                aVar.c();
                String j = com.mintegral.msdk.base.controller.a.d().j();
                com.mintegral.msdk.b.b.a();
                com.mintegral.msdk.b.a b2 = com.mintegral.msdk.b.b.b(j);
                if (b2 == null) {
                    com.mintegral.msdk.b.b.a();
                    b2 = com.mintegral.msdk.b.b.b();
                }
                int v = b2.v();
                int s = b2.s();
                StringBuilder sb = new StringBuilder("key=2000053&Appid=");
                sb.append(j);
                sb.append("&uptips2=");
                sb.append(s);
                sb.append("&info_status=");
                sb.append(com.mintegral.msdk.base.controller.authoritycontroller.a.a().d());
                sb.append("&iseu=");
                sb.append(v);
                String sb2 = sb.toString();
                String k = c.k();
                if (!TextUtils.isEmpty(k)) {
                    StringBuilder sb3 = new StringBuilder();
                    sb3.append(sb2);
                    sb3.append("&gaid=");
                    sb3.append(k);
                    sb2 = sb3.toString();
                }
                String str = f2523a;
                StringBuilder sb4 = new StringBuilder("reportPrivateAuthorityStatus  data:");
                sb4.append(sb2);
                g.a(str, sb4.toString());
                aVar.b(com.mintegral.msdk.base.common.a.f, c.a(sb2, this.b, ""), new com.mintegral.msdk.base.common.d.c.b() {
                    public final void a(String str) {
                        g.a("", "PrivateAuthorityStatus onSuccess ");
                    }

                    public final void b(String str) {
                        StringBuilder sb = new StringBuilder("PrivateAuthorityStatus onFailed:");
                        sb.append(str);
                        g.a("", sb.toString());
                    }
                });
                c.b();
            }
        } catch (Throwable unused) {
            g.d(f2523a, "PrivateAuthorityStatus onFailed");
        }
    }

    public final void a(int i, int i2, String str, String str2) {
        try {
            a aVar = new a(this.b, this.c);
            aVar.c();
            StringBuilder sb = new StringBuilder();
            sb.append("key=");
            sb.append(URLEncoder.encode("2000058", "utf-8"));
            sb.append("&appid=");
            sb.append(URLEncoder.encode(com.mintegral.msdk.base.controller.a.d().j(), "utf-8"));
            sb.append("&dl_service=");
            StringBuilder sb2 = new StringBuilder();
            sb2.append(com.mintegral.msdk.base.utils.t.b);
            sb.append(URLEncoder.encode(sb2.toString(), "utf-8"));
            sb.append("&dl_type=");
            sb.append(URLEncoder.encode(String.valueOf(i), "utf-8"));
            sb.append("&dl_link_type=");
            sb.append(URLEncoder.encode(String.valueOf(i2), "utf-8"));
            sb.append("&rid_n=");
            sb.append(URLEncoder.encode(str, "utf-8"));
            sb.append("&cid=");
            sb.append(URLEncoder.encode(str2, "utf-8"));
            sb.append("&dl_v4=");
            StringBuilder sb3 = new StringBuilder();
            sb3.append(com.mintegral.msdk.base.utils.t.e);
            sb.append(URLEncoder.encode(sb3.toString(), "utf-8"));
            sb.append("&dl_pkg=");
            StringBuilder sb4 = new StringBuilder();
            sb4.append(com.mintegral.msdk.base.utils.t.f2622a);
            sb.append(URLEncoder.encode(sb4.toString(), "utf-8"));
            sb.append("&dl_i_p=");
            StringBuilder sb5 = new StringBuilder();
            sb5.append(com.mintegral.msdk.base.utils.t.c);
            sb.append(URLEncoder.encode(sb5.toString(), "utf-8"));
            sb.append("&dl_fp=");
            StringBuilder sb6 = new StringBuilder();
            sb6.append(com.mintegral.msdk.base.utils.t.d);
            sb.append(URLEncoder.encode(sb6.toString(), "utf-8"));
            sb.append("&tgt_v=");
            StringBuilder sb7 = new StringBuilder();
            sb7.append(c.k(this.b));
            sb.append(URLEncoder.encode(sb7.toString(), "utf-8"));
            sb.append("&app_v_n=");
            StringBuilder sb8 = new StringBuilder();
            sb8.append(c.j(this.b));
            sb.append(URLEncoder.encode(sb8.toString(), "utf-8"));
            sb.append("&app_v_c=");
            StringBuilder sb9 = new StringBuilder();
            sb9.append(c.i(this.b));
            sb.append(URLEncoder.encode(sb9.toString(), "utf-8"));
            aVar.b(com.mintegral.msdk.base.common.a.f, c.a(sb.toString(), this.b, ""), new com.mintegral.msdk.base.common.d.c.b() {
                public final void a(String str) {
                    g.a("", "reportDownloadMethod REPORT SUCCESS");
                }

                public final void b(String str) {
                    g.a("", "reportDownloadMethod REPORT FAILED");
                }
            });
        } catch (Exception e) {
            if (MIntegralConstans.DEBUG) {
                e.printStackTrace();
            }
        }
    }

    public final void b(String str, String str2, String str3, String str4) {
        try {
            if (!TextUtils.isEmpty(str4)) {
                a aVar = new a(this.b, this.c);
                aVar.c();
                StringBuilder sb = new StringBuilder();
                sb.append("key=");
                sb.append(URLEncoder.encode("2000066", "utf-8"));
                sb.append("&rid_n=");
                sb.append(URLEncoder.encode(str, "utf-8"));
                sb.append("&cid=");
                sb.append(URLEncoder.encode(str2, "utf-8"));
                sb.append("&unit_id=");
                sb.append(URLEncoder.encode(str3, "utf-8"));
                sb.append("&err_method=");
                sb.append(str4);
                aVar.b(com.mintegral.msdk.base.common.a.f, c.a(sb.toString(), this.b, str3), new com.mintegral.msdk.base.common.d.c.b() {
                    public final void a(String str) {
                        g.a("", "MraidUnSupportMethod REPORT SUCCESS");
                    }

                    public final void b(String str) {
                        g.a("", "MraidUnSupportMethod REPORT FAILED");
                    }
                });
            }
        } catch (Exception e) {
            if (MIntegralConstans.DEBUG) {
                e.printStackTrace();
            }
        }
    }

    public final void c(String str, String str2, String str3, String str4) {
        try {
            a aVar = new a(this.b, this.c);
            aVar.c();
            StringBuilder sb = new StringBuilder();
            sb.append("key=");
            sb.append(URLEncoder.encode("2000065", "utf-8"));
            sb.append("&rid_n=");
            sb.append(URLEncoder.encode(str, "utf-8"));
            sb.append("&cid=");
            sb.append(URLEncoder.encode(str2, "utf-8"));
            sb.append("&unit_id=");
            sb.append(URLEncoder.encode(str3, "utf-8"));
            sb.append("&click_url=");
            sb.append(URLEncoder.encode(str4, "utf-8"));
            aVar.b(com.mintegral.msdk.base.common.a.f, c.a(sb.toString(), this.b, str3), new com.mintegral.msdk.base.common.d.c.b() {
                public final void a(String str) {
                    g.a("", "MraidClic REPORT SUCCESS");
                }

                public final void b(String str) {
                    g.a("", "MraidClic REPORT FAILED");
                }
            });
        } catch (Exception e) {
            if (MIntegralConstans.DEBUG) {
                e.printStackTrace();
            }
        }
    }
}
