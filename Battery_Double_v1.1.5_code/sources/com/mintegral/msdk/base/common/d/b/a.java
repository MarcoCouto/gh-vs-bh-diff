package com.mintegral.msdk.base.common.d.b;

import android.content.Context;
import com.mintegral.msdk.base.b.h;
import com.mintegral.msdk.base.b.i;
import com.mintegral.msdk.base.b.r;
import com.mintegral.msdk.base.entity.n;
import java.util.List;

/* compiled from: LogFileController */
public class a {

    /* renamed from: a reason: collision with root package name */
    public static final String f2537a = "a";
    private static a b;

    private a() {
    }

    public static a a() {
        if (b == null) {
            synchronized (a.class) {
                if (b == null) {
                    b = new a();
                }
            }
        }
        return b;
    }

    public static List<n> a(Context context) {
        if (context != null && r.a((h) i.a(context)).c() > 0) {
            return r.a((h) i.a(context)).e();
        }
        return null;
    }
}
