package com.mintegral.msdk.base.common.d.c;

import com.mintegral.msdk.base.common.net.a.e;
import com.mintegral.msdk.base.utils.g;

/* compiled from: ReportResponseHandler */
public abstract class b extends e {

    /* renamed from: a reason: collision with root package name */
    private static final String f2539a = "b";

    public abstract void a(String str);

    public abstract void b(String str);

    public final void c(String str) {
        a(str);
    }

    public final void a(int i) {
        String str = f2539a;
        StringBuilder sb = new StringBuilder("errorCode = ");
        sb.append(i);
        g.d(str, sb.toString());
        b(c(i));
    }
}
