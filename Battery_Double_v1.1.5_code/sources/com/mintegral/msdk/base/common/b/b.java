package com.mintegral.msdk.base.common.b;

import java.util.ArrayList;
import java.util.List;

/* compiled from: DirectoryContext */
public abstract class b {

    /* renamed from: a reason: collision with root package name */
    protected a f2512a;

    /* access modifiers changed from: protected */
    public abstract List<a> b();

    public b(String str) {
        a aVar = new a();
        aVar.a(str);
        aVar.a(c.AD_ROOT);
        List b = b();
        if (b.size() > 0) {
            aVar.a(b);
        }
        this.f2512a = aVar;
    }

    public final a a() {
        return this.f2512a;
    }

    protected static a a(ArrayList<a> arrayList, c cVar, String str) {
        a aVar = new a();
        aVar.a(cVar);
        aVar.a(str);
        arrayList.add(aVar);
        return aVar;
    }
}
