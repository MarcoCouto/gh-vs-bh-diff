package com.mintegral.msdk.base.common.b;

import android.util.Log;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.base.utils.i;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* compiled from: MIntegralDirManager */
public final class e {
    private static e c;

    /* renamed from: a reason: collision with root package name */
    private b f2514a;
    private ArrayList<a> b = new ArrayList<>();

    /* compiled from: MIntegralDirManager */
    private static final class a {

        /* renamed from: a reason: collision with root package name */
        public c f2515a;
        public File b;

        public a(c cVar, File file) {
            this.f2515a = cVar;
            this.b = file;
        }
    }

    private e(b bVar) {
        this.f2514a = bVar;
    }

    public static synchronized void a(b bVar) {
        synchronized (e.class) {
            if (c == null) {
                c = new e(bVar);
            }
        }
    }

    public static synchronized e a() {
        e eVar;
        synchronized (e.class) {
            if (c == null && com.mintegral.msdk.base.controller.a.d().h() != null) {
                i.a(com.mintegral.msdk.base.controller.a.d().h());
            }
            if (c == null) {
                Log.e("MIntegralDirManager", "mDirectoryManager == null");
            }
            eVar = c;
        }
        return eVar;
    }

    public static File a(c cVar) {
        try {
            if (a() != null) {
                Iterator it = a().b.iterator();
                while (it.hasNext()) {
                    a aVar = (a) it.next();
                    if (aVar.f2515a.equals(cVar)) {
                        return aVar.b;
                    }
                }
            }
        } catch (Throwable th) {
            g.c("MIntegralDirManager", th.getMessage(), th);
        }
        return null;
    }

    public static String b(c cVar) {
        File a2 = a(cVar);
        if (a2 != null) {
            return a2.getAbsolutePath();
        }
        return null;
    }

    public final boolean b() {
        return a(this.f2514a.a());
    }

    private boolean a(a aVar) {
        String str;
        a c2 = aVar.c();
        if (c2 == null) {
            str = aVar.b();
        } else {
            File a2 = a(c2.a());
            StringBuilder sb = new StringBuilder();
            sb.append(a2.getAbsolutePath());
            sb.append(File.separator);
            sb.append(aVar.b());
            str = sb.toString();
        }
        File file = new File(str);
        if (!(!file.exists() ? file.mkdirs() : true)) {
            return false;
        }
        this.b.add(new a(aVar.a(), file));
        List<a> d = aVar.d();
        if (d != null) {
            for (a a3 : d) {
                if (!a(a3)) {
                    return false;
                }
            }
        }
        return true;
    }
}
