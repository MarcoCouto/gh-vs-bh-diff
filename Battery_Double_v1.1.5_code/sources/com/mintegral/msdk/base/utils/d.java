package com.mintegral.msdk.base.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.channels.FileChannel.MapMode;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/* compiled from: CommonFileMD5 */
public class d {

    /* renamed from: a reason: collision with root package name */
    protected static char[] f2611a = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
    protected static MessageDigest b = null;
    private static String c = "CommonFileMD5";

    static {
        b = null;
        try {
            b = MessageDigest.getInstance(CommonMD5.TAG);
        } catch (NoSuchAlgorithmException e) {
            PrintStream printStream = System.err;
            StringBuilder sb = new StringBuilder();
            sb.append(d.class.getName());
            sb.append("初始化失败，MessageDigest不支持MD5Util.");
            printStream.println(sb.toString());
            e.printStackTrace();
        }
    }

    public static String a(File file) throws IOException {
        FileInputStream fileInputStream;
        try {
            fileInputStream = new FileInputStream(file);
            try {
                b.update(fileInputStream.getChannel().map(MapMode.READ_ONLY, 0, file.length()));
                byte[] digest = b.digest();
                String a2 = a(digest, digest.length);
                fileInputStream.close();
                return a2;
            } catch (Throwable th) {
                th = th;
                fileInputStream.close();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            fileInputStream = null;
            fileInputStream.close();
            throw th;
        }
    }

    private static String a(byte[] bArr, int i) {
        StringBuffer stringBuffer = new StringBuffer(i * 2);
        int i2 = i + 0;
        for (int i3 = 0; i3 < i2; i3++) {
            byte b2 = bArr[i3];
            char c2 = f2611a[(b2 & 240) >> 4];
            char c3 = f2611a[b2 & 15];
            stringBuffer.append(c2);
            stringBuffer.append(c3);
        }
        return stringBuffer.toString();
    }
}
