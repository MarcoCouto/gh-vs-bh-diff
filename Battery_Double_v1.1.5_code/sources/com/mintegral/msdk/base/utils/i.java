package com.mintegral.msdk.base.utils;

import android.content.Context;
import android.os.Build.VERSION;
import android.os.Environment;
import android.os.StatFs;
import com.ironsource.sdk.constants.Constants;
import com.mintegral.msdk.base.common.b.b;
import com.mintegral.msdk.base.common.b.d;
import com.mintegral.msdk.base.common.b.e;
import com.mintegral.msdk.base.controller.a;
import com.tapjoy.TapjoyConstants;
import java.io.File;
import java.util.UUID;

/* compiled from: CommonSDCardUtil */
public final class i {

    /* renamed from: a reason: collision with root package name */
    static boolean f2614a = false;
    static String b = "";
    private static boolean c = false;
    private static int d = -1;
    private static int e = -1;

    /* JADX WARNING: Code restructure failed: missing block: B:10:?, code lost:
        r1 = new java.lang.StringBuilder();
        r1.append(r4.getFilesDir().getAbsolutePath());
        r1.append(java.io.File.separator);
        b = r1.toString();
        b(r4);
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x003a */
    public static void a(Context context) {
        if (!c) {
            StringBuilder sb = new StringBuilder();
            sb.append(context.getFilesDir().getAbsolutePath());
            sb.append(File.separator);
            b = sb.toString();
            if (context.getPackageManager().checkPermission("android.permission.WRITE_EXTERNAL_STORAGE", context.getPackageName()) == 0) {
                f2614a = true;
            } else {
                f2614a = false;
            }
            b(context);
            c = true;
        }
    }

    private static void b(Context context) {
        e.a((b) new d(c(context)));
        e.a().b();
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x001f  */
    private static String c(Context context) {
        File file;
        if (VERSION.SDK_INT >= 18) {
            try {
                File externalFilesDir = context.getExternalFilesDir(null);
                if (externalFilesDir != null) {
                    file = a(externalFilesDir);
                    if (f2614a) {
                        if (file == null) {
                            StringBuilder sb = new StringBuilder();
                            sb.append(Environment.getExternalStorageDirectory().getPath());
                            sb.append(File.separator);
                            sb.append(Constants.JAVASCRIPT_INTERFACE_NAME);
                            sb.append(File.separator);
                            sb.append("data");
                            sb.append(File.separator);
                            sb.append(context.getPackageName());
                            file = a(new File(sb.toString()));
                        }
                        if (!(b() > 31457280)) {
                            file = null;
                        }
                    }
                    if (file == null || !file.exists()) {
                        file = context.getFilesDir().getAbsoluteFile();
                    }
                    return file.getAbsolutePath();
                }
            } catch (Throwable th) {
                g.c("common-exception", "hasSDCard is failed", th);
            }
        }
        file = null;
        if (f2614a) {
        }
        file = context.getFilesDir().getAbsoluteFile();
        return file.getAbsolutePath();
    }

    private static File a(File file) {
        StringBuilder sb = new StringBuilder();
        sb.append(UUID.randomUUID());
        File file2 = new File(file, sb.toString());
        if (file2.exists()) {
            file2.delete();
        }
        if (!file2.mkdirs()) {
            return null;
        }
        file2.delete();
        return file.getAbsoluteFile();
    }

    private static boolean d() {
        try {
            if ("mounted".equals(Environment.getExternalStorageState())) {
                return true;
            }
        } catch (Exception unused) {
            g.d("", "hasSDCard is failed");
        }
        return false;
    }

    public static int a() {
        try {
            Context h = a.d().h();
            long longValue = ((Long) r.b(h, "freeExternalSize", new Long(0))).longValue();
            long currentTimeMillis = System.currentTimeMillis();
            if (currentTimeMillis - longValue > TapjoyConstants.SESSION_ID_INACTIVITY_TIME || e == -1) {
                e = new Long((b() / 1000) / 1000).intValue();
                r.a(h, "freeExternalSize", Long.valueOf(currentTimeMillis));
            }
        } catch (Throwable th) {
            g.c("CommonSDCardUtil", th.getMessage(), th);
        }
        return e;
    }

    public static long b() {
        if (!d()) {
            return 0;
        }
        try {
            StatFs statFs = new StatFs(Environment.getExternalStorageDirectory().getPath());
            return ((long) statFs.getAvailableBlocks()) * ((long) statFs.getBlockSize());
        } catch (Exception e2) {
            e2.printStackTrace();
            return 0;
        }
    }

    public static int c() {
        if (d == -1) {
            try {
                d = new Long((e() / 1000) / 1000).intValue();
            } catch (Throwable th) {
                g.c("CommonSDCardUtil", th.getMessage(), th);
            }
        }
        return d;
    }

    private static long e() {
        if (!d()) {
            return 0;
        }
        try {
            StatFs statFs = new StatFs(Environment.getExternalStorageDirectory().getPath());
            return ((long) statFs.getBlockCount()) * ((long) statFs.getBlockSize());
        } catch (Exception e2) {
            e2.printStackTrace();
            return 0;
        }
    }
}
