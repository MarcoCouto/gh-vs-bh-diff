package com.mintegral.msdk.base.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.text.TextUtils;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.mintegral.msdk.base.controller.a;
import com.mintegral.msdk.base.entity.g;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: InstallAppManager */
public class l {
    private static l b;

    /* renamed from: a reason: collision with root package name */
    private Context f2620a;
    private SharedPreferences c;

    public static l a(Context context) {
        if (b == null) {
            synchronized (l.class) {
                if (b == null) {
                    b = new l(context);
                }
            }
        }
        return b;
    }

    private l(Context context) {
        this.f2620a = context;
    }

    public final CopyOnWriteArraySet<g> a(String str) {
        CopyOnWriteArraySet<g> copyOnWriteArraySet = new CopyOnWriteArraySet<>();
        if (this.f2620a != null) {
            try {
                this.c = this.f2620a.getSharedPreferences("installed", 0);
                if (this.c != null) {
                    SharedPreferences sharedPreferences = this.c;
                    StringBuilder sb = new StringBuilder();
                    sb.append(str);
                    sb.append("_installed");
                    String string = sharedPreferences.getString(sb.toString(), "");
                    if (!TextUtils.isEmpty(string)) {
                        JSONArray jSONArray = new JSONArray(string);
                        for (int i = 0; i < jSONArray.length(); i++) {
                            g gVar = new g();
                            JSONObject jSONObject = jSONArray.getJSONObject(i);
                            gVar.a(jSONObject.optString(RequestParameters.CAMPAIGN_ID));
                            gVar.b(jSONObject.optString("packageName"));
                            copyOnWriteArraySet.add(gVar);
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return copyOnWriteArraySet;
    }

    public final void a(Set<g> set) {
        if (set != null && set.size() >= 0) {
            try {
                String a2 = g.a(set);
                if (this.f2620a != null) {
                    this.c = this.f2620a.getSharedPreferences("installed", 0);
                    if (this.c != null) {
                        Editor edit = this.c.edit();
                        if (edit != null) {
                            StringBuilder sb = new StringBuilder();
                            sb.append(a.d().j());
                            sb.append("_installed");
                            edit.putString(sb.toString(), a2);
                            edit.apply();
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
