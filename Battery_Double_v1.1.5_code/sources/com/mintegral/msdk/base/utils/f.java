package com.mintegral.msdk.base.utils;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;

/* compiled from: CommonImageUtils */
public final class f {
    public static Bitmap a(Bitmap bitmap, int i) {
        Bitmap bitmap2;
        if (i == 0) {
            return bitmap;
        }
        try {
            int width = bitmap.getWidth();
            int height = bitmap.getHeight();
            bitmap2 = Bitmap.createBitmap(width, height, Config.ARGB_8888);
            try {
                Canvas canvas = new Canvas(bitmap2);
                Paint paint = new Paint();
                Rect rect = new Rect(0, 0, width, height);
                RectF rectF = new RectF(rect);
                float f = (float) i;
                paint.setAntiAlias(true);
                paint.setXfermode(new PorterDuffXfermode(Mode.SRC_OVER));
                canvas.drawARGB(0, 0, 0, 0);
                paint.setColor(-12434878);
                canvas.drawRoundRect(rectF, f, f, paint);
                paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
                canvas.drawBitmap(bitmap, rect, rect, paint);
            } catch (Error | Exception unused) {
            }
        } catch (Error | Exception unused2) {
            bitmap2 = null;
        }
        return bitmap2;
    }
}
