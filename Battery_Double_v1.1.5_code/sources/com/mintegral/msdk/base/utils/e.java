package com.mintegral.msdk.base.utils;

import android.content.Context;
import android.os.Environment;
import android.text.TextUtils;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.b.b;
import com.mintegral.msdk.base.common.b.c;
import com.mintegral.msdk.base.controller.a;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/* compiled from: CommonFileUtil */
public final class e {
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0038 A[SYNTHETIC, Splitter:B:24:0x0038] */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x003e A[SYNTHETIC, Splitter:B:28:0x003e] */
    private static long c(File file) throws Exception {
        long j = 0;
        FileInputStream fileInputStream = null;
        try {
            if (file.exists()) {
                FileInputStream fileInputStream2 = new FileInputStream(file);
                try {
                    j = (long) fileInputStream2.available();
                    fileInputStream = fileInputStream2;
                } catch (Exception e) {
                    e = e;
                    fileInputStream = fileInputStream2;
                    try {
                        e.printStackTrace();
                        if (fileInputStream != null) {
                        }
                        return j;
                    } catch (Throwable th) {
                        th = th;
                        if (fileInputStream != null) {
                            try {
                                fileInputStream.close();
                            } catch (Exception e2) {
                                e2.printStackTrace();
                            }
                        }
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    fileInputStream = fileInputStream2;
                    if (fileInputStream != null) {
                    }
                    throw th;
                }
            } else {
                file.createNewFile();
                g.d("获取文件大小", "文件不存在!");
            }
            if (fileInputStream != null) {
                try {
                    fileInputStream.close();
                } catch (Exception e3) {
                    e3.printStackTrace();
                }
            }
        } catch (Exception e4) {
            e = e4;
            e.printStackTrace();
            if (fileInputStream != null) {
                fileInputStream.close();
            }
            return j;
        }
        return j;
    }

    private static long d(File file) throws Exception {
        long j;
        File[] listFiles = file.listFiles();
        long j2 = 0;
        if (listFiles != null) {
            for (int i = 0; i < listFiles.length; i++) {
                if (listFiles[i].isDirectory()) {
                    j = d(listFiles[i]);
                } else {
                    j = c(listFiles[i]);
                }
                j2 += j;
            }
        }
        return j2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0040, code lost:
        if (r1 == null) goto L_0x0043;
     */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x004e A[SYNTHETIC, Splitter:B:30:0x004e] */
    public static String a(File file) {
        BufferedReader bufferedReader;
        StringBuffer stringBuffer;
        if (file == null) {
            return null;
        }
        try {
            bufferedReader = new BufferedReader(new FileReader(file));
            try {
                stringBuffer = new StringBuffer();
                while (true) {
                    try {
                        String readLine = bufferedReader.readLine();
                        if (readLine != null) {
                            StringBuilder sb = new StringBuilder();
                            sb.append(readLine);
                            sb.append("\n");
                            stringBuffer.append(sb.toString());
                        }
                    } catch (IOException e) {
                        e = e;
                        try {
                            e.printStackTrace();
                        } catch (Throwable th) {
                            th = th;
                            if (bufferedReader != null) {
                                try {
                                    bufferedReader.close();
                                } catch (IOException unused) {
                                }
                            }
                            throw th;
                        }
                    }
                    try {
                        break;
                    } catch (IOException unused2) {
                    }
                }
            } catch (IOException e2) {
                e = e2;
                stringBuffer = null;
                e.printStackTrace();
            }
        } catch (IOException e3) {
            e = e3;
            stringBuffer = null;
            bufferedReader = null;
            e.printStackTrace();
        } catch (Throwable th2) {
            th = th2;
            bufferedReader = null;
            if (bufferedReader != null) {
            }
            throw th;
        }
        bufferedReader.close();
        if (stringBuffer != null) {
            return stringBuffer.toString();
        }
        return null;
    }

    public static File[] b(String str) {
        try {
            File file = new File(str);
            if (file.exists()) {
                return file.listFiles();
            }
            return null;
        } catch (Exception unused) {
            return null;
        }
    }

    public static void a() {
        new Thread(new Runnable() {
            public final void run() {
                File[] b;
                try {
                    if (a.d().h() != null) {
                        String b2 = com.mintegral.msdk.base.common.b.e.b(c.MINTEGRAL_VC);
                        try {
                            File file = new File(b2);
                            if (file.exists() && file.isDirectory()) {
                                for (File file2 : e.b(b2)) {
                                    if (file2.exists() && file2.isFile()) {
                                        file2.delete();
                                    }
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } catch (Exception e2) {
                    if (MIntegralConstans.DEBUG) {
                        e2.printStackTrace();
                    }
                }
            }
        }).start();
    }

    public static void c(String str) {
        try {
            if (!TextUtils.isEmpty(str)) {
                File file = new File(str);
                if (file.exists() && file.isDirectory()) {
                    long currentTimeMillis = System.currentTimeMillis();
                    File[] listFiles = file.listFiles();
                    if (listFiles != null) {
                        for (File file2 : listFiles) {
                            if (file2.lastModified() + 259200000 < currentTimeMillis) {
                                b(file2);
                            }
                        }
                    }
                }
            }
        } catch (Exception unused) {
        }
    }

    public static void b() {
        new Thread(new Runnable() {
            public final void run() {
                File[] listFiles;
                try {
                    if (a.d().h() != null) {
                        e.d(com.mintegral.msdk.base.common.b.e.b(c.MINTEGRAL_VC));
                    }
                    b.a();
                    com.mintegral.msdk.b.a b = b.b(a.d().j());
                    if (b == null) {
                        b.a();
                        b = b.b();
                    }
                    long currentTimeMillis = System.currentTimeMillis() - ((long) (b.ao() * 1000));
                    try {
                        for (File file : new File(com.mintegral.msdk.base.common.b.e.b(c.MINTEGRAL_VC)).listFiles(new FileFilter() {
                            public final boolean accept(File file) {
                                return !file.isHidden();
                            }
                        })) {
                            if (file.lastModified() < currentTimeMillis && file.exists() && file.isFile()) {
                                file.delete();
                            }
                        }
                    } catch (Throwable th) {
                        g.c("CommonFileUtil", th.getMessage(), th);
                    }
                } catch (Exception e) {
                    if (MIntegralConstans.DEBUG) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }

    public static void b(File file) {
        try {
            if (file.isFile()) {
                file.delete();
                return;
            }
            if (file.isDirectory()) {
                File[] listFiles = file.listFiles();
                if (listFiles != null) {
                    if (listFiles.length != 0) {
                        for (File b : listFiles) {
                            b(b);
                        }
                        file.delete();
                    }
                }
                file.delete();
            }
        } catch (Exception unused) {
        }
    }

    private static void e(String str) {
        try {
            File[] listFiles = new File(str).listFiles(new FileFilter() {
                public final boolean accept(File file) {
                    return !file.isHidden();
                }
            });
            Arrays.sort(listFiles, new Comparator<File>() {
                public final boolean equals(Object obj) {
                    return true;
                }

                public final /* synthetic */ int compare(Object obj, Object obj2) {
                    long lastModified = ((File) obj).lastModified() - ((File) obj2).lastModified();
                    if (lastModified > 0) {
                        return 1;
                    }
                    return lastModified == 0 ? 0 : -1;
                }
            });
            int length = (listFiles.length - 1) / 2;
            for (int i = 0; i < length; i++) {
                File file = listFiles[i];
                if (file.exists() && file.isFile()) {
                    file.delete();
                }
            }
        } catch (Exception unused) {
            g.d("CommonFileUtil", "del memory failed");
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:25:0x0034 A[SYNTHETIC, Splitter:B:25:0x0034] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0040 A[SYNTHETIC, Splitter:B:32:0x0040] */
    public static boolean a(byte[] bArr, File file) {
        FileOutputStream fileOutputStream = null;
        try {
            if (file.getParentFile() != null && !file.exists()) {
                file.getParentFile().mkdirs();
            }
            FileOutputStream fileOutputStream2 = new FileOutputStream(file);
            try {
                fileOutputStream2.write(bArr);
                try {
                    fileOutputStream2.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return true;
            } catch (Exception e2) {
                e = e2;
                fileOutputStream = fileOutputStream2;
                try {
                    e.printStackTrace();
                    if (fileOutputStream != null) {
                    }
                    return false;
                } catch (Throwable th) {
                    th = th;
                    if (fileOutputStream != null) {
                    }
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                fileOutputStream = fileOutputStream2;
                if (fileOutputStream != null) {
                    try {
                        fileOutputStream.close();
                    } catch (IOException e3) {
                        e3.printStackTrace();
                    }
                }
                throw th;
            }
        } catch (Exception e4) {
            e = e4;
            e.printStackTrace();
            if (fileOutputStream != null) {
                try {
                    fileOutputStream.close();
                } catch (IOException e5) {
                    e5.printStackTrace();
                }
            }
            return false;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:76:0x00d7 A[Catch:{ all -> 0x00ef }] */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x00dc A[SYNTHETIC, Splitter:B:78:0x00dc] */
    /* JADX WARNING: Removed duplicated region for block: B:83:0x00e6 A[SYNTHETIC, Splitter:B:83:0x00e6] */
    /* JADX WARNING: Removed duplicated region for block: B:90:0x00f2 A[SYNTHETIC, Splitter:B:90:0x00f2] */
    /* JADX WARNING: Removed duplicated region for block: B:95:0x00fc A[SYNTHETIC, Splitter:B:95:0x00fc] */
    public static boolean a(String str, String str2) {
        FileOutputStream fileOutputStream;
        if (str == null || str2 == null) {
            return false;
        }
        if (!str2.endsWith("/")) {
            StringBuilder sb = new StringBuilder();
            sb.append(str2);
            sb.append("/");
            str2 = sb.toString();
        }
        File file = new File(str);
        if (!file.exists()) {
            return false;
        }
        InputStream inputStream = null;
        try {
            ZipFile zipFile = new ZipFile(file);
            Enumeration entries = zipFile.entries();
            fileOutputStream = null;
            while (entries.hasMoreElements()) {
                try {
                    ZipEntry zipEntry = (ZipEntry) entries.nextElement();
                    if (zipEntry == null) {
                        if (inputStream != null) {
                            try {
                                inputStream.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                        if (fileOutputStream != null) {
                            try {
                                fileOutputStream.close();
                            } catch (IOException e2) {
                                e2.printStackTrace();
                            }
                        }
                        return false;
                    }
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append(str2);
                    sb2.append(zipEntry.getName());
                    File file2 = new File(sb2.toString());
                    if (zipEntry.isDirectory()) {
                        file2.mkdirs();
                    } else {
                        if (!file2.getParentFile().exists()) {
                            file2.getParentFile().mkdirs();
                        }
                        FileOutputStream fileOutputStream2 = new FileOutputStream(file2);
                        try {
                            InputStream inputStream2 = zipFile.getInputStream(zipEntry);
                            try {
                                byte[] bArr = new byte[1024];
                                while (true) {
                                    int read = inputStream2.read(bArr, 0, 1024);
                                    if (read <= 0) {
                                        break;
                                    }
                                    fileOutputStream2.write(bArr, 0, read);
                                }
                                inputStream = inputStream2;
                                fileOutputStream = fileOutputStream2;
                            } catch (IOException e3) {
                                e = e3;
                                inputStream = inputStream2;
                                fileOutputStream = fileOutputStream2;
                                try {
                                    if (MIntegralConstans.DEBUG) {
                                    }
                                    if (inputStream != null) {
                                    }
                                    if (fileOutputStream != null) {
                                    }
                                    return false;
                                } catch (Throwable th) {
                                    th = th;
                                    if (inputStream != null) {
                                    }
                                    if (fileOutputStream != null) {
                                    }
                                    throw th;
                                }
                            } catch (Throwable th2) {
                                th = th2;
                                inputStream = inputStream2;
                                fileOutputStream = fileOutputStream2;
                                if (inputStream != null) {
                                }
                                if (fileOutputStream != null) {
                                }
                                throw th;
                            }
                        } catch (IOException e4) {
                            e = e4;
                            fileOutputStream = fileOutputStream2;
                            if (MIntegralConstans.DEBUG) {
                            }
                            if (inputStream != null) {
                            }
                            if (fileOutputStream != null) {
                            }
                            return false;
                        } catch (Throwable th3) {
                            th = th3;
                            fileOutputStream = fileOutputStream2;
                            if (inputStream != null) {
                            }
                            if (fileOutputStream != null) {
                            }
                            throw th;
                        }
                    }
                } catch (IOException e5) {
                    e = e5;
                    if (MIntegralConstans.DEBUG) {
                    }
                    if (inputStream != null) {
                    }
                    if (fileOutputStream != null) {
                    }
                    return false;
                }
            }
            zipFile.close();
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e6) {
                    e6.printStackTrace();
                }
            }
            if (fileOutputStream != null) {
                try {
                    fileOutputStream.close();
                } catch (IOException e7) {
                    e7.printStackTrace();
                }
            }
            return true;
        } catch (IOException e8) {
            e = e8;
            fileOutputStream = null;
            if (MIntegralConstans.DEBUG) {
                e.printStackTrace();
            }
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e9) {
                    e9.printStackTrace();
                }
            }
            if (fileOutputStream != null) {
                try {
                    fileOutputStream.close();
                } catch (IOException e10) {
                    e10.printStackTrace();
                }
            }
            return false;
        } catch (Throwable th4) {
            th = th4;
            fileOutputStream = null;
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e11) {
                    e11.printStackTrace();
                }
            }
            if (fileOutputStream != null) {
                try {
                    fileOutputStream.close();
                } catch (IOException e12) {
                    e12.printStackTrace();
                }
            }
            throw th;
        }
    }

    private static boolean f(String str) {
        try {
            Class.forName("android.os.FileUtils").getMethod("setPermissions", new Class[]{String.class, Integer.TYPE, Integer.TYPE, Integer.TYPE}).invoke(null, new Object[]{str, Integer.valueOf(IronSourceError.ERROR_CODE_KEY_NOT_SET), Integer.valueOf(-1), Integer.valueOf(-1)});
            return true;
        } catch (ClassNotFoundException e) {
            g.a("CommonFileUtil", "error when set permissions:", e);
            return false;
        } catch (NoSuchMethodException e2) {
            g.a("CommonFileUtil", "error when set permissions:", e2);
            return false;
        } catch (IllegalArgumentException e3) {
            g.a("CommonFileUtil", "error when set permissions:", e3);
            return false;
        } catch (IllegalAccessException e4) {
            g.a("CommonFileUtil", "error when set permissions:", e4);
            return false;
        } catch (InvocationTargetException e5) {
            g.a("CommonFileUtil", "error when set permissions:", e5);
            return false;
        }
    }

    public static boolean a(String str) {
        if (str == null || str.trim().length() == 0) {
            return false;
        }
        File file = new File(str);
        return file.exists() && file.isFile();
    }

    public static File a(String str, Context context, boolean[] zArr) throws IOException {
        if (Environment.getExternalStorageState().equals("mounted")) {
            String b = com.mintegral.msdk.base.common.b.e.b(c.MINTEGRAL_700_APK);
            StringBuilder sb = new StringBuilder();
            sb.append(b);
            sb.append("/download/.mtg");
            sb.append(str);
            File file = new File(sb.toString());
            file.mkdirs();
            if (file.exists()) {
                zArr[0] = true;
                return file;
            }
        }
        String absolutePath = context.getCacheDir().getAbsolutePath();
        new File(absolutePath).mkdir();
        f(absolutePath);
        StringBuilder sb2 = new StringBuilder();
        sb2.append(absolutePath);
        sb2.append("/mtgdownload");
        String sb3 = sb2.toString();
        new File(sb3).mkdir();
        f(sb3);
        File file2 = new File(sb3);
        zArr[0] = false;
        return file2;
    }

    static /* synthetic */ void d(String str) {
        try {
            if (d(new File(str)) > 52428800) {
                e(str);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } catch (Throwable unused) {
            g.d("CommonFileUtil", "clean memory failed");
        }
    }
}
