package com.mintegral.msdk.base.utils;

import java.util.ArrayList;
import org.json.JSONArray;

/* compiled from: JsonUtils */
public final class m {
    public static ArrayList<String> a(JSONArray jSONArray) {
        if (jSONArray == null) {
            return null;
        }
        int length = jSONArray.length();
        ArrayList<String> arrayList = new ArrayList<>(length);
        for (int i = 0; i < length; i++) {
            arrayList.add(jSONArray.optString(i));
        }
        return arrayList;
    }
}
