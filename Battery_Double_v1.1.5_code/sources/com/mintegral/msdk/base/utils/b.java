package com.mintegral.msdk.base.utils;

import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.text.TextUtils;
import com.mintegral.msdk.base.controller.a;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

/* compiled from: CommonBatteryStatusUtil */
public final class b {

    /* renamed from: a reason: collision with root package name */
    private static int f2607a = -1;
    private static int b = -1;
    private static int c = -1;
    private static boolean d = false;
    private static int e = 0;
    private static boolean f = false;
    private static boolean g = false;

    public static int a() {
        int i = 50;
        try {
            Intent registerReceiver = a.d().h().registerReceiver(null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
            int[] iArr = new int[2];
            if (registerReceiver != null) {
                iArr[0] = registerReceiver.getIntExtra("level", 50);
                iArr[1] = registerReceiver.getIntExtra("plugged", 0);
                a(iArr[1] != 0);
                iArr[0] = a(iArr[0], iArr[1]);
                a(iArr[0]);
            } else {
                iArr[0] = 50;
                iArr[1] = 0;
            }
            return iArr[0];
        } catch (Exception unused) {
            if (f2607a != -1) {
                i = f2607a;
            }
            return i;
        }
    }

    private static int a(int i, int i2) {
        String str = Build.MODEL;
        if (str.equalsIgnoreCase("SCH-i909") || str.equalsIgnoreCase("SCH-I535") || str.equalsIgnoreCase("SCH-W899")) {
            if (i > 100) {
                i /= 10;
            }
        } else if (!str.trim().toUpperCase().contains("XT702") && !str.trim().toUpperCase().contains("XT907") && !str.trim().toUpperCase().contains("XT1058") && !str.trim().toUpperCase().contains("XT1080") && Build.MANUFACTURER.equalsIgnoreCase("motorola")) {
            File file = new File("/sys/class/power_supply/battery/charge_counter");
            int parseInt = file.exists() ? Integer.parseInt(a(file)) : 0;
            if (parseInt <= 0) {
                File file2 = new File("/sys/class/power_supply/battery/capacity");
                if (file2.exists()) {
                    Integer.parseInt(a(file2));
                }
            }
            if (parseInt <= 100 && parseInt > 0 && i % 10 == 0) {
                i = parseInt;
            }
        }
        if (i <= 100) {
            return i;
        }
        if (i2 != 0 && i < 110) {
            return 100;
        }
        do {
            i /= 10;
        } while (i > 100);
        return i;
    }

    public static int b() {
        int i;
        try {
            i = a.d().h().registerReceiver(null, new IntentFilter("android.intent.action.BATTERY_CHANGED")).getIntExtra("status", 1);
        } catch (Throwable th) {
            g.c("BatteryStatusUtil", th.getMessage(), th);
            i = 0;
        }
        if (i == 2 || i == 5) {
            return 1;
        }
        return 0;
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x002a A[SYNTHETIC, Splitter:B:16:0x002a] */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0030 A[SYNTHETIC, Splitter:B:22:0x0030] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x003d  */
    /* JADX WARNING: Removed duplicated region for block: B:29:? A[RETURN, SYNTHETIC] */
    private static String a(File file) {
        FileInputStream fileInputStream;
        StringBuilder sb = new StringBuilder();
        char[] cArr = new char[1024];
        FileInputStream fileInputStream2 = null;
        try {
            fileInputStream = new FileInputStream(file);
            try {
                InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream);
                while (true) {
                    int read = inputStreamReader.read(cArr);
                    if (read >= 0) {
                        a(read, cArr, sb);
                    } else {
                        try {
                            break;
                        } catch (IOException unused) {
                        }
                    }
                }
                fileInputStream.close();
            } catch (IOException unused2) {
                fileInputStream2 = fileInputStream;
                if (fileInputStream2 != null) {
                }
                String sb2 = sb.toString();
                if (TextUtils.isEmpty(sb2)) {
                }
            } catch (Throwable th) {
                th = th;
                if (fileInputStream != null) {
                }
                throw th;
            }
        } catch (IOException unused3) {
            if (fileInputStream2 != null) {
                fileInputStream2.close();
            }
            String sb22 = sb.toString();
            if (TextUtils.isEmpty(sb22)) {
            }
        } catch (Throwable th2) {
            th = th2;
            fileInputStream = null;
            if (fileInputStream != null) {
                try {
                    fileInputStream.close();
                } catch (IOException unused4) {
                }
            }
            throw th;
        }
        String sb222 = sb.toString();
        return TextUtils.isEmpty(sb222) ? "0" : sb222;
    }

    private static void a(int i, char[] cArr, StringBuilder sb) {
        for (int i2 = 0; i2 < i; i2++) {
            if (!(cArr[i2] == 10 || cArr[i2] == 13)) {
                sb.append(cArr[i2]);
            }
        }
    }

    private static synchronized void a(int i) {
        synchronized (b.class) {
            e = i;
        }
    }

    private static synchronized void a(boolean z) {
        synchronized (b.class) {
            f = z;
        }
    }
}
