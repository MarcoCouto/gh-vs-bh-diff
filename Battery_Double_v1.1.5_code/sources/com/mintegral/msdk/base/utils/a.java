package com.mintegral.msdk.base.utils;

import android.text.TextUtils;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Map;

/* compiled from: CommonBase64Util */
public class a {

    /* renamed from: a reason: collision with root package name */
    private static final String f2606a = "a";
    private static Map<Character, Character> b;
    private static Map<Character, Character> c;
    private static char[] d = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '/'};
    private static byte[] e = {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 62, -1, -1, -1, 63, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, -1, -1, -1, -1, -1, -1, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, -1, -1, -1, -1, -1, -1, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, -1, -1, -1, -1, -1};

    static {
        HashMap hashMap = new HashMap();
        c = hashMap;
        hashMap.put(Character.valueOf('v'), Character.valueOf('A'));
        c.put(Character.valueOf('S'), Character.valueOf('B'));
        c.put(Character.valueOf('o'), Character.valueOf('C'));
        c.put(Character.valueOf('a'), Character.valueOf('D'));
        c.put(Character.valueOf('j'), Character.valueOf('E'));
        c.put(Character.valueOf('c'), Character.valueOf('F'));
        c.put(Character.valueOf('7'), Character.valueOf('G'));
        c.put(Character.valueOf('d'), Character.valueOf('H'));
        c.put(Character.valueOf('R'), Character.valueOf('I'));
        c.put(Character.valueOf('z'), Character.valueOf('J'));
        c.put(Character.valueOf('p'), Character.valueOf('K'));
        c.put(Character.valueOf('W'), Character.valueOf('L'));
        c.put(Character.valueOf('i'), Character.valueOf('M'));
        c.put(Character.valueOf('f'), Character.valueOf('N'));
        c.put(Character.valueOf('G'), Character.valueOf('O'));
        c.put(Character.valueOf('y'), Character.valueOf('P'));
        c.put(Character.valueOf('N'), Character.valueOf('Q'));
        c.put(Character.valueOf('x'), Character.valueOf('R'));
        c.put(Character.valueOf('Z'), Character.valueOf('S'));
        c.put(Character.valueOf('n'), Character.valueOf('T'));
        c.put(Character.valueOf('V'), Character.valueOf('U'));
        c.put(Character.valueOf('5'), Character.valueOf('V'));
        c.put(Character.valueOf('k'), Character.valueOf('W'));
        c.put(Character.valueOf('+'), Character.valueOf('X'));
        c.put(Character.valueOf('D'), Character.valueOf('Y'));
        c.put(Character.valueOf('H'), Character.valueOf('Z'));
        c.put(Character.valueOf('L'), Character.valueOf('a'));
        c.put(Character.valueOf('Y'), Character.valueOf('b'));
        c.put(Character.valueOf('h'), Character.valueOf('c'));
        c.put(Character.valueOf('J'), Character.valueOf('d'));
        c.put(Character.valueOf('4'), Character.valueOf('e'));
        c.put(Character.valueOf('6'), Character.valueOf('f'));
        c.put(Character.valueOf('l'), Character.valueOf('g'));
        c.put(Character.valueOf('t'), Character.valueOf('h'));
        c.put(Character.valueOf('0'), Character.valueOf('i'));
        c.put(Character.valueOf('U'), Character.valueOf('j'));
        c.put(Character.valueOf('3'), Character.valueOf('k'));
        c.put(Character.valueOf('Q'), Character.valueOf('l'));
        c.put(Character.valueOf('r'), Character.valueOf('m'));
        c.put(Character.valueOf('g'), Character.valueOf('n'));
        c.put(Character.valueOf('E'), Character.valueOf('o'));
        c.put(Character.valueOf('u'), Character.valueOf('p'));
        c.put(Character.valueOf('q'), Character.valueOf('q'));
        c.put(Character.valueOf('8'), Character.valueOf('r'));
        c.put(Character.valueOf('s'), Character.valueOf('s'));
        c.put(Character.valueOf('w'), Character.valueOf('t'));
        c.put(Character.valueOf('/'), Character.valueOf('u'));
        c.put(Character.valueOf('X'), Character.valueOf('v'));
        c.put(Character.valueOf('M'), Character.valueOf('w'));
        c.put(Character.valueOf('e'), Character.valueOf('x'));
        c.put(Character.valueOf('B'), Character.valueOf('y'));
        c.put(Character.valueOf('A'), Character.valueOf('z'));
        c.put(Character.valueOf('T'), Character.valueOf('0'));
        c.put(Character.valueOf('2'), Character.valueOf('1'));
        c.put(Character.valueOf('F'), Character.valueOf('2'));
        c.put(Character.valueOf('b'), Character.valueOf('3'));
        c.put(Character.valueOf('9'), Character.valueOf('4'));
        c.put(Character.valueOf('P'), Character.valueOf('5'));
        c.put(Character.valueOf('1'), Character.valueOf('6'));
        c.put(Character.valueOf('O'), Character.valueOf('7'));
        c.put(Character.valueOf('I'), Character.valueOf('8'));
        c.put(Character.valueOf('K'), Character.valueOf('9'));
        c.put(Character.valueOf('m'), Character.valueOf('+'));
        c.put(Character.valueOf('C'), Character.valueOf('/'));
        HashMap hashMap2 = new HashMap();
        b = hashMap2;
        hashMap2.put(Character.valueOf('A'), Character.valueOf('v'));
        b.put(Character.valueOf('B'), Character.valueOf('S'));
        b.put(Character.valueOf('C'), Character.valueOf('o'));
        b.put(Character.valueOf('D'), Character.valueOf('a'));
        b.put(Character.valueOf('E'), Character.valueOf('j'));
        b.put(Character.valueOf('F'), Character.valueOf('c'));
        b.put(Character.valueOf('G'), Character.valueOf('7'));
        b.put(Character.valueOf('H'), Character.valueOf('d'));
        b.put(Character.valueOf('I'), Character.valueOf('R'));
        b.put(Character.valueOf('J'), Character.valueOf('z'));
        b.put(Character.valueOf('K'), Character.valueOf('p'));
        b.put(Character.valueOf('L'), Character.valueOf('W'));
        b.put(Character.valueOf('M'), Character.valueOf('i'));
        b.put(Character.valueOf('N'), Character.valueOf('f'));
        b.put(Character.valueOf('O'), Character.valueOf('G'));
        b.put(Character.valueOf('P'), Character.valueOf('y'));
        b.put(Character.valueOf('Q'), Character.valueOf('N'));
        b.put(Character.valueOf('R'), Character.valueOf('x'));
        b.put(Character.valueOf('S'), Character.valueOf('Z'));
        b.put(Character.valueOf('T'), Character.valueOf('n'));
        b.put(Character.valueOf('U'), Character.valueOf('V'));
        b.put(Character.valueOf('V'), Character.valueOf('5'));
        b.put(Character.valueOf('W'), Character.valueOf('k'));
        b.put(Character.valueOf('X'), Character.valueOf('+'));
        b.put(Character.valueOf('Y'), Character.valueOf('D'));
        b.put(Character.valueOf('Z'), Character.valueOf('H'));
        b.put(Character.valueOf('a'), Character.valueOf('L'));
        b.put(Character.valueOf('b'), Character.valueOf('Y'));
        b.put(Character.valueOf('c'), Character.valueOf('h'));
        b.put(Character.valueOf('d'), Character.valueOf('J'));
        b.put(Character.valueOf('e'), Character.valueOf('4'));
        b.put(Character.valueOf('f'), Character.valueOf('6'));
        b.put(Character.valueOf('g'), Character.valueOf('l'));
        b.put(Character.valueOf('h'), Character.valueOf('t'));
        b.put(Character.valueOf('i'), Character.valueOf('0'));
        b.put(Character.valueOf('j'), Character.valueOf('U'));
        b.put(Character.valueOf('k'), Character.valueOf('3'));
        b.put(Character.valueOf('l'), Character.valueOf('Q'));
        b.put(Character.valueOf('m'), Character.valueOf('r'));
        b.put(Character.valueOf('n'), Character.valueOf('g'));
        b.put(Character.valueOf('o'), Character.valueOf('E'));
        b.put(Character.valueOf('p'), Character.valueOf('u'));
        b.put(Character.valueOf('q'), Character.valueOf('q'));
        b.put(Character.valueOf('r'), Character.valueOf('8'));
        b.put(Character.valueOf('s'), Character.valueOf('s'));
        b.put(Character.valueOf('t'), Character.valueOf('w'));
        b.put(Character.valueOf('u'), Character.valueOf('/'));
        b.put(Character.valueOf('v'), Character.valueOf('X'));
        b.put(Character.valueOf('w'), Character.valueOf('M'));
        b.put(Character.valueOf('x'), Character.valueOf('e'));
        b.put(Character.valueOf('y'), Character.valueOf('B'));
        b.put(Character.valueOf('z'), Character.valueOf('A'));
        b.put(Character.valueOf('0'), Character.valueOf('T'));
        b.put(Character.valueOf('1'), Character.valueOf('2'));
        b.put(Character.valueOf('2'), Character.valueOf('F'));
        b.put(Character.valueOf('3'), Character.valueOf('b'));
        b.put(Character.valueOf('4'), Character.valueOf('9'));
        b.put(Character.valueOf('5'), Character.valueOf('P'));
        b.put(Character.valueOf('6'), Character.valueOf('1'));
        b.put(Character.valueOf('7'), Character.valueOf('O'));
        b.put(Character.valueOf('8'), Character.valueOf('I'));
        b.put(Character.valueOf('9'), Character.valueOf('K'));
        b.put(Character.valueOf('+'), Character.valueOf('m'));
        b.put(Character.valueOf('/'), Character.valueOf('C'));
    }

    private a() {
    }

    public static String a(byte[] bArr) {
        StringBuffer stringBuffer = new StringBuffer();
        int length = bArr.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                break;
            }
            int i2 = i + 1;
            byte b2 = bArr[i] & 255;
            if (i2 == length) {
                stringBuffer.append(d[b2 >>> 2]);
                stringBuffer.append(d[(b2 & 3) << 4]);
                stringBuffer.append("==");
                break;
            }
            int i3 = i2 + 1;
            byte b3 = bArr[i2] & 255;
            if (i3 == length) {
                stringBuffer.append(d[b2 >>> 2]);
                stringBuffer.append(d[((b2 & 3) << 4) | ((b3 & 240) >>> 4)]);
                stringBuffer.append(d[(b3 & 15) << 2]);
                stringBuffer.append(RequestParameters.EQUAL);
                break;
            }
            int i4 = i3 + 1;
            byte b4 = bArr[i3] & 255;
            stringBuffer.append(d[b2 >>> 2]);
            stringBuffer.append(d[((b2 & 3) << 4) | ((b3 & 240) >>> 4)]);
            stringBuffer.append(d[((b3 & 15) << 2) | ((b4 & 192) >>> 6)]);
            stringBuffer.append(d[b4 & 63]);
            i = i4;
        }
        return stringBuffer.toString();
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0030  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0054  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0078 A[LOOP:0: B:1:0x000b->B:32:0x0078, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0082 A[EDGE_INSN: B:37:0x0082->B:33:0x0082 ?: BREAK  , SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x0082 A[EDGE_INSN: B:38:0x0082->B:33:0x0082 ?: BREAK  , SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0082 A[EDGE_INSN: B:40:0x0082->B:33:0x0082 ?: BREAK  , SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x0082 A[EDGE_INSN: B:41:0x0082->B:33:0x0082 ?: BREAK  , SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f A[LOOP:2: B:7:0x001f->B:10:0x002c, LOOP_START, PHI: r4 
  PHI: (r4v1 int) = (r4v0 int), (r4v8 int) binds: [B:6:0x001d, B:10:0x002c] A[DONT_GENERATE, DONT_INLINE]] */
    public static byte[] a(String str) {
        byte b2;
        byte b3;
        byte b4;
        byte b5;
        byte[] bytes = str.getBytes();
        int length = bytes.length;
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(length);
        int i = 0;
        while (i < length) {
            while (true) {
                int i2 = i + 1;
                b2 = e[bytes[i]];
                if (i2 >= length || b2 != -1) {
                    if (b2 != -1) {
                        break;
                    }
                    while (true) {
                        int i3 = i2 + 1;
                        b3 = e[bytes[i2]];
                        if (i3 >= length || b3 != -1) {
                            if (b3 != -1) {
                                break;
                            }
                            byteArrayOutputStream.write((b2 << 2) | ((b3 & 48) >>> 4));
                            while (true) {
                                int i4 = i3 + 1;
                                byte b6 = bytes[i3];
                                if (b6 == 61) {
                                    return byteArrayOutputStream.toByteArray();
                                }
                                b4 = e[b6];
                                if (i4 >= length || b4 != -1) {
                                    if (b4 != -1) {
                                        break;
                                    }
                                    byteArrayOutputStream.write(((b3 & 15) << 4) | ((b4 & 60) >>> 2));
                                    while (true) {
                                        int i5 = i4 + 1;
                                        byte b7 = bytes[i4];
                                        if (b7 == 61) {
                                            return byteArrayOutputStream.toByteArray();
                                        }
                                        b5 = e[b7];
                                        if (i5 >= length || b5 != -1) {
                                            if (b5 != -1) {
                                                break;
                                            }
                                            byteArrayOutputStream.write(b5 | ((b4 & 3) << 6));
                                            i = i5;
                                        } else {
                                            i4 = i5;
                                        }
                                    }
                                    if (b5 != -1) {
                                    }
                                } else {
                                    i3 = i4;
                                }
                            }
                            if (b4 != -1) {
                            }
                        } else {
                            i2 = i3;
                        }
                    }
                    if (b3 != -1) {
                    }
                } else {
                    i = i2;
                }
            }
            if (b2 != -1) {
            }
        }
        return byteArrayOutputStream.toByteArray();
    }

    public static String b(String str) {
        if (TextUtils.isEmpty(str)) {
            return "";
        }
        return h.b(str);
    }

    public static String c(String str) {
        return h.a(str);
    }
}
