package com.mintegral.msdk.base.utils;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.hardware.SensorManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.Looper;
import android.provider.Settings.Global;
import android.provider.Settings.Secure;
import android.provider.Settings.System;
import android.support.v4.media.session.PlaybackStateCompat;
import android.telephony.CellLocation;
import android.telephony.TelephonyManager;
import android.telephony.gsm.GsmCellLocation;
import android.text.TextUtils;
import android.text.format.Formatter;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodInfo;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import com.facebook.places.model.PlaceFields;
import com.github.mikephil.charting.utils.Utils;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.base.controller.a;
import com.mintegral.msdk.optimize.SensitiveDataUtil;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.UUID;

/* compiled from: CommonDeviceUtil */
public final class c {

    /* renamed from: a reason: collision with root package name */
    private static String f2608a = null;
    private static String b = null;
    private static int c = 0;
    /* access modifiers changed from: private */
    public static int d = 0;
    private static String e = null;
    private static String f = null;
    /* access modifiers changed from: private */
    public static String g = "";
    private static String h = "";
    private static String i = "";
    private static String j = "";
    private static int k = 0;
    private static String l = "";
    private static String m = "";
    private static String n = "";
    /* access modifiers changed from: private */
    public static String o = "";
    private static String p = "";
    private static String q = "";
    private static String r = "";
    private static String s = "";
    private static int t = -1;
    private static String u = "";
    private static int v;

    /* JADX WARNING: Removed duplicated region for block: B:19:0x00b0 A[Catch:{ Exception -> 0x00e8 }] */
    public static void a(Context context) {
        try {
            i();
            n(context);
            j(context);
            i(context);
            g(context);
            c();
            e();
            d(context);
            b(context);
            h(context);
            k();
            f(context);
            h();
            q(context);
            try {
                int j2 = j();
                if (j2 < 17 || j2 >= 21) {
                    if (j2 >= 21 || (j2 < 17 && j2 > 10)) {
                        t = Secure.getInt(a.d().h().getContentResolver(), "install_non_market_apps", 0);
                    }
                    StringBuilder sb = new StringBuilder("getUnknowSourceStateForPrivate:");
                    sb.append(t);
                    g.d("CommonDeviceUtil", sb.toString());
                    com.mintegral.msdk.base.common.a.s = k.a("android.permission.WRITE_EXTERNAL_STORAGE", context);
                    com.mintegral.msdk.base.common.a.r = k.a("android.permission.ACCESS_NETWORK_STATE", context);
                    com.mintegral.msdk.base.common.a.u = k.a("android.permission.GET_TASKS", context);
                    com.mintegral.msdk.base.common.a.t = k.a("android.permission.ACCESS_COARSE_LOCATION", context);
                    if (k.a("android.permission.READ_PHONE_STATE", context)) {
                        com.mintegral.msdk.base.controller.authoritycontroller.a.a();
                        if (com.mintegral.msdk.base.controller.authoritycontroller.a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA)) {
                            String simOperator = ((TelephonyManager) context.getSystemService(PlaceFields.PHONE)).getSimOperator();
                            if (k.b(simOperator) && simOperator.length() > 3) {
                                r = simOperator.substring(0, 3);
                                s = simOperator.substring(3, simOperator.length());
                            }
                        }
                    }
                    k(context);
                }
                t = Global.getInt(a.d().h().getContentResolver(), "install_non_market_apps", 0);
                StringBuilder sb2 = new StringBuilder("getUnknowSourceStateForPrivate:");
                sb2.append(t);
                g.d("CommonDeviceUtil", sb2.toString());
                com.mintegral.msdk.base.common.a.s = k.a("android.permission.WRITE_EXTERNAL_STORAGE", context);
                com.mintegral.msdk.base.common.a.r = k.a("android.permission.ACCESS_NETWORK_STATE", context);
                com.mintegral.msdk.base.common.a.u = k.a("android.permission.GET_TASKS", context);
                com.mintegral.msdk.base.common.a.t = k.a("android.permission.ACCESS_COARSE_LOCATION", context);
                if (k.a("android.permission.READ_PHONE_STATE", context)) {
                }
                k(context);
            } catch (Throwable th) {
                g.c("CommonDeviceUtil", th.getMessage(), th);
            }
        } catch (Exception unused) {
        }
    }

    public static String b(Context context) {
        com.mintegral.msdk.base.controller.authoritycontroller.a.a();
        if (!com.mintegral.msdk.base.controller.authoritycontroller.a.a(MIntegralConstans.AUTHORITYIMEIMAC)) {
            return "";
        }
        if (context == null) {
            return "";
        }
        try {
            return SensitiveDataUtil.getIMEI(context);
        } catch (Throwable th) {
            g.c("CommonDeviceUtil", th.getMessage(), th);
            return "";
        }
    }

    public static String a() {
        return r;
    }

    public static String b() {
        return s;
    }

    public static String c(Context context) {
        if (context == null) {
            return l;
        }
        try {
            if (TextUtils.isEmpty(l)) {
                l = a.b(SensitiveDataUtil.getAndroidID(context));
            }
        } catch (Throwable unused) {
        }
        return l;
    }

    public static String d(Context context) {
        com.mintegral.msdk.base.controller.authoritycontroller.a.a();
        if (!com.mintegral.msdk.base.controller.authoritycontroller.a.a(MIntegralConstans.AUTHORITY_ANDROID_ID)) {
            return "";
        }
        if (context == null) {
            return e;
        }
        try {
            if (TextUtils.isEmpty(e)) {
                e = SensitiveDataUtil.getAndroidID(context);
            }
        } catch (Throwable th) {
            g.c("CommonDeviceUtil", th.getMessage(), th);
        }
        return e;
    }

    public static String e(Context context) {
        if (context == null) {
            return "";
        }
        f = null;
        try {
            if (TextUtils.isEmpty(f)) {
                String d2 = d(context);
                f = d2;
                f = CommonMD5.getUPMD5(d2);
            }
        } catch (Throwable th) {
            g.c("CommonDeviceUtil", th.getMessage(), th);
        }
        return f;
    }

    public static String c() {
        com.mintegral.msdk.base.controller.authoritycontroller.a.a();
        if (!com.mintegral.msdk.base.controller.authoritycontroller.a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA)) {
            return "";
        }
        return Build.MODEL;
    }

    public static String d() {
        com.mintegral.msdk.base.controller.authoritycontroller.a.a();
        if (!com.mintegral.msdk.base.controller.authoritycontroller.a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA)) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        sb.append(Build.MANUFACTURER);
        sb.append(" ");
        sb.append(Build.MODEL);
        return sb.toString();
    }

    public static String e() {
        com.mintegral.msdk.base.controller.authoritycontroller.a.a();
        if (!com.mintegral.msdk.base.controller.authoritycontroller.a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA)) {
            return "";
        }
        return Build.BRAND;
    }

    public static String f(Context context) {
        com.mintegral.msdk.base.controller.authoritycontroller.a.a();
        if (!com.mintegral.msdk.base.controller.authoritycontroller.a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA)) {
            return "";
        }
        if (!TextUtils.isEmpty(n)) {
            return n;
        }
        if (context == null) {
            return "en";
        }
        String language = context.getResources().getConfiguration().locale.getLanguage();
        n = language;
        return language;
    }

    public static int g(Context context) {
        if (context == null || context.getResources() == null) {
            return 1;
        }
        Configuration configuration = context.getResources().getConfiguration();
        if (configuration == null) {
            return 1;
        }
        int i2 = configuration.orientation;
        if (i2 == 2) {
            return 2;
        }
        return i2 == 1 ? 1 : 1;
    }

    public static String h(Context context) {
        com.mintegral.msdk.base.controller.authoritycontroller.a.a();
        if (!com.mintegral.msdk.base.controller.authoritycontroller.a.a(MIntegralConstans.AUTHORITYIMEIMAC)) {
            return "";
        }
        if (context == null) {
            return m;
        }
        try {
            if (TextUtils.isEmpty(m)) {
                m = SensitiveDataUtil.getMacAddress(context);
            }
        } catch (Throwable th) {
            g.c("CommonDeviceUtil", th.getMessage(), th);
        }
        return m;
    }

    public static int i(Context context) {
        if (context == null) {
            return k;
        }
        if (k != 0) {
            return k;
        }
        try {
            int i2 = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode;
            k = i2;
            return i2;
        } catch (Exception e2) {
            e2.printStackTrace();
            return -1;
        }
    }

    public static String j(Context context) {
        if (context == null) {
            return j;
        }
        try {
            if (!TextUtils.isEmpty(j)) {
                return j;
            }
            String str = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
            j = str;
            return str;
        } catch (Exception e2) {
            e2.printStackTrace();
            return "";
        }
    }

    public static int k(Context context) {
        if (context == null) {
            return v;
        }
        if (v == 0) {
            try {
                v = context.getApplicationInfo().targetSdkVersion;
            } catch (Exception e2) {
                g.d("CommonDeviceUtil", e2.getMessage());
            }
        }
        return v;
    }

    public static int l(Context context) {
        com.mintegral.msdk.base.controller.authoritycontroller.a.a();
        if (!com.mintegral.msdk.base.controller.authoritycontroller.a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA) || context == null) {
            return 0;
        }
        try {
            DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
            HashMap A = A(context);
            return A.get("width") == null ? displayMetrics.widthPixels : ((Integer) A.get("width")).intValue();
        } catch (Exception e2) {
            e2.printStackTrace();
            return 0;
        }
    }

    public static int m(Context context) {
        com.mintegral.msdk.base.controller.authoritycontroller.a.a();
        if (!com.mintegral.msdk.base.controller.authoritycontroller.a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA) || context == null) {
            return 0;
        }
        try {
            DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
            HashMap A = A(context);
            return A.get("height") == null ? displayMetrics.heightPixels : ((Integer) A.get("height")).intValue();
        } catch (Exception e2) {
            e2.printStackTrace();
            return 0;
        }
    }

    private static HashMap A(Context context) {
        HashMap hashMap = new HashMap();
        if (context == null) {
            return hashMap;
        }
        try {
            Display defaultDisplay = ((WindowManager) context.getSystemService("window")).getDefaultDisplay();
            DisplayMetrics displayMetrics = new DisplayMetrics();
            Class.forName("android.view.Display").getMethod("getRealMetrics", new Class[]{DisplayMetrics.class}).invoke(defaultDisplay, new Object[]{displayMetrics});
            hashMap.put("height", Integer.valueOf(displayMetrics.heightPixels));
            hashMap.put("width", Integer.valueOf(displayMetrics.widthPixels));
        } catch (Exception e2) {
            g.c("CommonDeviceUtil", e2.getMessage(), e2);
        }
        return hashMap;
    }

    public static String n(Context context) {
        if (context == null) {
            return i;
        }
        try {
            if (!TextUtils.isEmpty(i)) {
                return i;
            }
            String str = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).packageName;
            i = str;
            return str;
        } catch (Exception e2) {
            e2.printStackTrace();
            return "";
        }
    }

    public static String o(final Context context) {
        com.mintegral.msdk.base.controller.authoritycontroller.a.a();
        if (!com.mintegral.msdk.base.controller.authoritycontroller.a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA)) {
            return null;
        }
        if (TextUtils.isEmpty(g)) {
            try {
                g = r.b(context, "mintegral_ua", "").toString();
            } catch (Throwable th) {
                g.c("CommonDeviceUtil", th.getMessage(), th);
            }
        }
        try {
            if (Looper.myLooper() == Looper.getMainLooper()) {
                g.a("CommonDeviceUtil", "get ua in mainThread");
                if (TextUtils.isEmpty(g)) {
                    if (K() > 17) {
                        g = WebSettings.getDefaultUserAgent(context);
                        StringBuilder sb = new StringBuilder("getDefaultUserAgent:");
                        sb.append(g);
                        g.a("CommonDeviceUtil", sb.toString());
                    }
                    if (TextUtils.isEmpty(g)) {
                        try {
                            Constructor declaredConstructor = WebSettings.class.getDeclaredConstructor(new Class[]{Context.class, WebView.class});
                            declaredConstructor.setAccessible(true);
                            g = ((WebSettings) declaredConstructor.newInstance(new Object[]{context, null})).getUserAgentString();
                            declaredConstructor.setAccessible(false);
                            StringBuilder sb2 = new StringBuilder("invoke getUserAgentString:");
                            sb2.append(g);
                            g.a("CommonDeviceUtil", sb2.toString());
                        } catch (Throwable th2) {
                            th2.printStackTrace();
                        }
                        if (TextUtils.isEmpty(g)) {
                            try {
                                g = new WebView(context).getSettings().getUserAgentString();
                                StringBuilder sb3 = new StringBuilder("getUserAgentString:");
                                sb3.append(g);
                                g.a("CommonDeviceUtil", sb3.toString());
                            } catch (Throwable th3) {
                                th3.printStackTrace();
                            }
                        }
                        if (TextUtils.isEmpty(g)) {
                            J();
                        }
                    }
                } else {
                    try {
                        new Thread(new Runnable() {
                            public final void run() {
                                String str = null;
                                try {
                                    if (c.K() > 17) {
                                        str = WebSettings.getDefaultUserAgent(context);
                                    }
                                    if (!TextUtils.isEmpty(str) && !str.equals(c.g)) {
                                        c.g = str;
                                        c.B(context);
                                    }
                                } catch (Throwable th) {
                                    th.printStackTrace();
                                }
                            }
                        }).start();
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                }
            } else {
                g.a("CommonDeviceUtil", "get ua in subThread");
                J();
            }
        } catch (Throwable th4) {
            g.c("CommonDeviceUtil", th4.getMessage(), th4);
        }
        B(context);
        return g;
    }

    /* access modifiers changed from: private */
    public static void B(Context context) {
        try {
            r.a(context, "mintegral_ua", g);
        } catch (Throwable th) {
            g.c("CommonDeviceUtil", th.getMessage(), th);
        }
    }

    public static String f() {
        if (TextUtils.isEmpty(g)) {
            o(a.d().h());
        }
        return g;
    }

    private static void J() {
        String str = VERSION.RELEASE;
        String c2 = c();
        String str2 = Build.ID;
        if (TextUtils.isEmpty(str) || TextUtils.isEmpty(c2) || TextUtils.isEmpty(str2)) {
            g = "Mozilla/5.0 (Linux; Android 4.0.4; Galaxy Nexus Build/IMM76B) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.133 Mobile Safari/535.19";
        } else {
            StringBuilder sb = new StringBuilder("Mozilla/5.0 (Linux; Android ");
            sb.append(str);
            sb.append("; ");
            sb.append(c2);
            sb.append(" Build/");
            sb.append(str2);
            sb.append(") AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.133 Mobile Safari/535.19");
            g = sb.toString();
        }
        StringBuilder sb2 = new StringBuilder("append ua:");
        sb2.append(g);
        g.a("CommonDeviceUtil", sb2.toString());
    }

    public static int p(final Context context) {
        try {
            com.mintegral.msdk.base.controller.authoritycontroller.a.a();
            if (!com.mintegral.msdk.base.controller.authoritycontroller.a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA)) {
                return d;
            }
            if (context == null) {
                return d;
            }
            if (d != 0) {
                new Thread(new Runnable() {
                    public final void run() {
                        try {
                            com.mintegral.msdk.base.controller.authoritycontroller.a.a();
                            if (com.mintegral.msdk.base.controller.authoritycontroller.a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA) && context != null) {
                                ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
                                if (connectivityManager != null && com.mintegral.msdk.base.common.a.r) {
                                    NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
                                    if (activeNetworkInfo == null) {
                                        c.d = 0;
                                    } else if (activeNetworkInfo.getType() == 1) {
                                        c.d = 9;
                                    } else {
                                        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(PlaceFields.PHONE);
                                        if (telephonyManager == null) {
                                            c.d = 0;
                                            return;
                                        }
                                        c.d = telephonyManager.getNetworkType();
                                    }
                                }
                            }
                        } catch (Exception e) {
                            g.c("CommonDeviceUtil", e.getMessage(), e);
                            c.d = 0;
                        }
                    }
                }).start();
                return d;
            }
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
            if (connectivityManager == null) {
                return d;
            }
            if (com.mintegral.msdk.base.common.a.r) {
                NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
                if (activeNetworkInfo == null) {
                    d = 0;
                    return 0;
                } else if (activeNetworkInfo.getType() == 1) {
                    d = 9;
                    return 9;
                } else {
                    TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(PlaceFields.PHONE);
                    if (telephonyManager == null) {
                        d = 0;
                        return 0;
                    }
                    int networkType = telephonyManager.getNetworkType();
                    d = networkType;
                    switch (networkType) {
                        case 1:
                        case 2:
                        case 4:
                        case 7:
                        case 11:
                            return 2;
                        case 3:
                        case 5:
                        case 6:
                        case 8:
                        case 9:
                        case 10:
                        case 12:
                        case 14:
                        case 15:
                            return 3;
                        case 13:
                            return 4;
                        default:
                            return 0;
                    }
                }
            } else {
                d = 0;
                return d;
            }
        } catch (Exception e2) {
            g.c("CommonDeviceUtil", e2.getMessage(), e2);
            d = 0;
            return 0;
        }
    }

    public static int g() {
        return t;
    }

    public static String a(Context context, int i2) {
        String str = "";
        if (i2 == 0 || i2 == 9) {
            return str;
        }
        try {
            if (com.mintegral.msdk.base.common.a.r) {
                TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(PlaceFields.PHONE);
                if (telephonyManager == null) {
                    return "";
                }
                str = String.valueOf(telephonyManager.getNetworkType());
            }
        } catch (Throwable th) {
            g.c("CommonDeviceUtil", th.getMessage(), th);
        }
        return str;
    }

    public static String h() {
        try {
            com.mintegral.msdk.base.controller.authoritycontroller.a.a();
            if (com.mintegral.msdk.base.controller.authoritycontroller.a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA)) {
                if (j() <= 25) {
                    if (TextUtils.isEmpty(o)) {
                        new Thread(new Runnable() {
                            public final void run() {
                                try {
                                    c.o = TimeZone.getDefault().getDisplayName(false, 0, Locale.ENGLISH);
                                } catch (Throwable th) {
                                    th.printStackTrace();
                                }
                            }
                        }).start();
                        return o;
                    }
                    return o;
                }
            }
            return "";
        } catch (Throwable th) {
            g.c("CommonDeviceUtil", th.getMessage(), th);
        }
    }

    public static String i() {
        com.mintegral.msdk.base.controller.authoritycontroller.a.a();
        if (!com.mintegral.msdk.base.controller.authoritycontroller.a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA)) {
            return "";
        }
        if (TextUtils.isEmpty(h)) {
            h = String.valueOf(j());
        }
        return h;
    }

    /* access modifiers changed from: private */
    public static int K() {
        try {
            if (TextUtils.isEmpty(h)) {
                return j();
            }
            return 0;
        } catch (Exception e2) {
            e2.printStackTrace();
            return 0;
        }
    }

    public static int j() {
        try {
            return VERSION.SDK_INT;
        } catch (Exception e2) {
            e2.printStackTrace();
            return 0;
        }
    }

    public static void a(String str) {
        b = a.b(str);
        f2608a = str;
    }

    public static String k() {
        com.mintegral.msdk.base.controller.authoritycontroller.a.a();
        if (!com.mintegral.msdk.base.controller.authoritycontroller.a.a(MIntegralConstans.AUTHORITY_DEVICE_ID)) {
            return "";
        }
        if (f2608a == null) {
            return "";
        }
        return f2608a;
    }

    public static String l() {
        com.mintegral.msdk.base.controller.authoritycontroller.a.a();
        if (!com.mintegral.msdk.base.controller.authoritycontroller.a.a(MIntegralConstans.AUTHORITY_DEVICE_ID)) {
            return "";
        }
        if (b == null) {
            return "";
        }
        return b;
    }

    public static String q(Context context) {
        if (context == null) {
            return "";
        }
        if (!TextUtils.isEmpty(p)) {
            return p;
        }
        try {
            String str = context.getPackageManager().getPackageInfo("com.android.vending", 0).versionName;
            p = str;
            return str;
        } catch (Exception unused) {
            return "";
        }
    }

    public static UUID m() {
        try {
            return UUID.randomUUID();
        } catch (Throwable th) {
            g.c("CommonDeviceUtil", th.getMessage(), th);
            return null;
        }
    }

    public static int n() {
        try {
            if (!com.mintegral.msdk.base.common.a.t) {
                return 0;
            }
            TelephonyManager telephonyManager = (TelephonyManager) a.d().h().getSystemService(PlaceFields.PHONE);
            if (telephonyManager == null) {
                return 0;
            }
            CellLocation cellLocation = telephonyManager.getCellLocation();
            if (cellLocation == null || !(cellLocation instanceof GsmCellLocation)) {
                return 0;
            }
            return ((GsmCellLocation) cellLocation).getCid();
        } catch (Throwable th) {
            g.c("CommonDeviceUtil", th.getMessage(), th);
            return 0;
        }
    }

    public static String a(String str, Context context) {
        try {
            if (!TextUtils.isEmpty(u)) {
                return u;
            }
            if (!TextUtils.isEmpty(str) && context != null) {
                u = context.getPackageManager().getInstallerPackageName(str);
                StringBuilder sb = new StringBuilder("PKGSource:");
                sb.append(u);
                g.a("CommonDeviceUtil", sb.toString());
            }
            return u;
        } catch (Exception e2) {
            g.c("CommonDeviceUtil", e2.getMessage(), e2);
        }
    }

    public static boolean r(Context context) {
        if (context == null) {
            return false;
        }
        try {
            Display defaultDisplay = ((WindowManager) context.getSystemService("window")).getDefaultDisplay();
            DisplayMetrics displayMetrics = new DisplayMetrics();
            defaultDisplay.getMetrics(displayMetrics);
            if (Math.sqrt(Math.pow((double) (((float) displayMetrics.widthPixels) / displayMetrics.xdpi), 2.0d) + Math.pow((double) (((float) displayMetrics.heightPixels) / displayMetrics.ydpi), 2.0d)) >= 6.0d) {
                return true;
            }
        } catch (Exception e2) {
            g.c("CommonDeviceUtil", e2.getMessage(), e2);
        }
        return false;
    }

    public static String o() {
        com.mintegral.msdk.base.controller.authoritycontroller.a.a();
        if (!com.mintegral.msdk.base.controller.authoritycontroller.a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA)) {
            return "";
        }
        return Build.MANUFACTURER;
    }

    public static String p() {
        com.mintegral.msdk.base.controller.authoritycontroller.a.a();
        if (!com.mintegral.msdk.base.controller.authoritycontroller.a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA)) {
            return "";
        }
        return Build.CPU_ABI2;
    }

    public static String q() {
        com.mintegral.msdk.base.controller.authoritycontroller.a.a();
        if (!com.mintegral.msdk.base.controller.authoritycontroller.a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA)) {
            return "";
        }
        return Build.CPU_ABI;
    }

    public static String r() {
        com.mintegral.msdk.base.controller.authoritycontroller.a.a();
        if (!com.mintegral.msdk.base.controller.authoritycontroller.a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA)) {
            return "";
        }
        return Build.TAGS;
    }

    public static String s() {
        com.mintegral.msdk.base.controller.authoritycontroller.a.a();
        if (!com.mintegral.msdk.base.controller.authoritycontroller.a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA)) {
            return "";
        }
        return Build.USER;
    }

    public static String t() {
        com.mintegral.msdk.base.controller.authoritycontroller.a.a();
        if (!com.mintegral.msdk.base.controller.authoritycontroller.a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA)) {
            return "";
        }
        return Build.RADIO;
    }

    public static String u() {
        com.mintegral.msdk.base.controller.authoritycontroller.a.a();
        if (!com.mintegral.msdk.base.controller.authoritycontroller.a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA)) {
            return "";
        }
        return Build.BOOTLOADER;
    }

    public static String v() {
        com.mintegral.msdk.base.controller.authoritycontroller.a.a();
        if (!com.mintegral.msdk.base.controller.authoritycontroller.a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA)) {
            return "";
        }
        return Build.HARDWARE;
    }

    public static String w() {
        com.mintegral.msdk.base.controller.authoritycontroller.a.a();
        if (!com.mintegral.msdk.base.controller.authoritycontroller.a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA)) {
            return "";
        }
        return Build.HOST;
    }

    public static String x() {
        com.mintegral.msdk.base.controller.authoritycontroller.a.a();
        if (!com.mintegral.msdk.base.controller.authoritycontroller.a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA)) {
            return "";
        }
        return VERSION.CODENAME;
    }

    public static String y() {
        com.mintegral.msdk.base.controller.authoritycontroller.a.a();
        if (!com.mintegral.msdk.base.controller.authoritycontroller.a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA)) {
            return "";
        }
        return VERSION.INCREMENTAL;
    }

    public static String z() {
        com.mintegral.msdk.base.controller.authoritycontroller.a.a();
        if (!com.mintegral.msdk.base.controller.authoritycontroller.a.a(MIntegralConstans.AUTHORITY_SERIAL_ID)) {
            return "";
        }
        return Build.SERIAL;
    }

    public static String A() {
        com.mintegral.msdk.base.controller.authoritycontroller.a.a();
        if (!com.mintegral.msdk.base.controller.authoritycontroller.a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA)) {
            return "";
        }
        return Build.DISPLAY;
    }

    public static String B() {
        com.mintegral.msdk.base.controller.authoritycontroller.a.a();
        if (!com.mintegral.msdk.base.controller.authoritycontroller.a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA)) {
            return "";
        }
        return Build.BOARD;
    }

    public static String C() {
        com.mintegral.msdk.base.controller.authoritycontroller.a.a();
        if (!com.mintegral.msdk.base.controller.authoritycontroller.a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA)) {
            return "";
        }
        return Build.TYPE;
    }

    public static String D() {
        com.mintegral.msdk.base.controller.authoritycontroller.a.a();
        if (!com.mintegral.msdk.base.controller.authoritycontroller.a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA)) {
            return "";
        }
        return VERSION.RELEASE;
    }

    public static int E() {
        com.mintegral.msdk.base.controller.authoritycontroller.a.a();
        if (!com.mintegral.msdk.base.controller.authoritycontroller.a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA)) {
            return -1;
        }
        return VERSION.SDK_INT;
    }

    public static int F() {
        com.mintegral.msdk.base.controller.authoritycontroller.a.a();
        return !com.mintegral.msdk.base.controller.authoritycontroller.a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA) ? -1 : 1;
    }

    public static int s(Context context) {
        com.mintegral.msdk.base.controller.authoritycontroller.a.a();
        if (!com.mintegral.msdk.base.controller.authoritycontroller.a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA) || context == null) {
            return -1;
        }
        int i2 = 0;
        try {
            Intent registerReceiver = context.registerReceiver(null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
            if (!(registerReceiver == null || registerReceiver.getExtras() == null)) {
                int intExtra = registerReceiver.getIntExtra("status", -1);
                if (intExtra == 2 || intExtra == 5) {
                    i2 = 1;
                }
            }
        } catch (Exception e2) {
            g.c("CommonDeviceUtil", e2.getMessage(), e2);
        }
        return i2;
    }

    public static String t(Context context) {
        com.mintegral.msdk.base.controller.authoritycontroller.a.a();
        if (!com.mintegral.msdk.base.controller.authoritycontroller.a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA)) {
            return "";
        }
        double d2 = Utils.DOUBLE_EPSILON;
        if (context != null) {
            return "";
        }
        try {
            d2 = ((Double) Class.forName("com.android.internal.os.PowerProfile").getMethod("getBatteryCapacity", new Class[0]).invoke(Class.forName("com.android.internal.os.PowerProfile").getConstructor(new Class[]{Context.class}).newInstance(new Object[]{context}), new Object[0])).doubleValue();
        } catch (Exception e2) {
            g.c("CommonDeviceUtil", e2.getMessage(), e2);
        } catch (Throwable th) {
            g.c("CommonDeviceUtil", th.getMessage(), th);
        }
        StringBuilder sb = new StringBuilder();
        sb.append(d2);
        sb.append(" mAh");
        return String.valueOf(sb.toString());
    }

    public static int u(Context context) {
        com.mintegral.msdk.base.controller.authoritycontroller.a.a();
        int i2 = -1;
        if (!com.mintegral.msdk.base.controller.authoritycontroller.a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA) || context == null) {
            return -1;
        }
        try {
            String simOperator = ((TelephonyManager) context.getSystemService(PlaceFields.PHONE)).getSimOperator();
            if (!"46000".equals(simOperator) && !"46002".equals(simOperator) && !"46007".equals(simOperator) && !"46008".equals(simOperator)) {
                if (!"45412".equals(simOperator)) {
                    if (!"46001".equals(simOperator) && !"46006".equals(simOperator)) {
                        if (!"46009".equals(simOperator)) {
                            i2 = ("46003".equals(simOperator) || "46005".equals(simOperator) || "46011".equals(simOperator) || "45502".equals(simOperator) || "45507".equals(simOperator)) ? 2 : -2;
                            return i2;
                        }
                    }
                    i2 = 1;
                    return i2;
                }
            }
            i2 = 0;
        } catch (Exception e2) {
            g.c("CommonDeviceUtil", e2.getMessage(), e2);
        }
        return i2;
    }

    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:16:0x004c */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x0079 A[SYNTHETIC, Splitter:B:36:0x0079] */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x007e A[SYNTHETIC, Splitter:B:40:0x007e] */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x0091 A[SYNTHETIC, Splitter:B:48:0x0091] */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x0096 A[SYNTHETIC, Splitter:B:52:0x0096] */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x00ae A[SYNTHETIC, Splitter:B:58:0x00ae] */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x00b3 A[SYNTHETIC, Splitter:B:62:0x00b3] */
    public static String G() {
        FileReader fileReader;
        BufferedReader bufferedReader;
        Exception e2;
        Throwable th;
        com.mintegral.msdk.base.controller.authoritycontroller.a.a();
        if (!com.mintegral.msdk.base.controller.authoritycontroller.a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA)) {
            return "";
        }
        int i2 = 0;
        try {
            fileReader = new FileReader("/proc/meminfo");
            try {
                bufferedReader = new BufferedReader(fileReader, 8192);
                try {
                    String str = bufferedReader.readLine().split("\\s+")[1];
                    bufferedReader.close();
                    if (str != null) {
                        i2 = (int) Math.ceil(new Float(Float.valueOf(str).floatValue() / 1048576.0f).doubleValue());
                    }
                    fileReader.close();
                } catch (Exception e3) {
                    e2 = e3;
                    g.c("CommonDeviceUtil", e2.getMessage(), e2);
                    if (fileReader != null) {
                    }
                    if (bufferedReader != null) {
                    }
                    StringBuilder sb = new StringBuilder();
                    sb.append(i2);
                    sb.append("GB");
                    return sb.toString();
                } catch (Throwable th2) {
                    th = th2;
                    try {
                        g.c("CommonDeviceUtil", th.getMessage(), th);
                        if (fileReader != null) {
                        }
                        if (bufferedReader != null) {
                        }
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append(i2);
                        sb2.append("GB");
                        return sb2.toString();
                    } catch (Throwable th3) {
                        th = th3;
                        if (fileReader != null) {
                        }
                        if (bufferedReader != null) {
                        }
                        throw th;
                    }
                }
                try {
                    bufferedReader.close();
                } catch (IOException e4) {
                    e4.printStackTrace();
                }
            } catch (Exception e5) {
                e2 = e5;
                bufferedReader = null;
                g.c("CommonDeviceUtil", e2.getMessage(), e2);
                if (fileReader != null) {
                }
                if (bufferedReader != null) {
                }
                StringBuilder sb22 = new StringBuilder();
                sb22.append(i2);
                sb22.append("GB");
                return sb22.toString();
            } catch (Throwable th4) {
                th = th4;
                bufferedReader = null;
                if (fileReader != null) {
                }
                if (bufferedReader != null) {
                }
                throw th;
            }
        } catch (Exception e6) {
            fileReader = null;
            e2 = e6;
            bufferedReader = null;
            g.c("CommonDeviceUtil", e2.getMessage(), e2);
            if (fileReader != null) {
                try {
                    fileReader.close();
                } catch (IOException unused) {
                }
            }
            if (bufferedReader != null) {
                bufferedReader.close();
            }
            StringBuilder sb222 = new StringBuilder();
            sb222.append(i2);
            sb222.append("GB");
            return sb222.toString();
        } catch (Throwable th5) {
            th = th5;
            bufferedReader = null;
            fileReader = null;
            if (fileReader != null) {
                try {
                    fileReader.close();
                } catch (IOException unused2) {
                }
            }
            if (bufferedReader != null) {
                try {
                    bufferedReader.close();
                } catch (IOException e7) {
                    e7.printStackTrace();
                }
            }
            throw th;
        }
        StringBuilder sb2222 = new StringBuilder();
        sb2222.append(i2);
        sb2222.append("GB");
        return sb2222.toString();
    }

    public static String v(Context context) {
        com.mintegral.msdk.base.controller.authoritycontroller.a.a();
        if (!com.mintegral.msdk.base.controller.authoritycontroller.a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA)) {
            return "";
        }
        if (context == null) {
            return "";
        }
        try {
            return System.getString(context.getContentResolver(), "time_12_24");
        } catch (Exception e2) {
            g.c("CommonDeviceUtil", e2.getMessage(), e2);
            return "";
        } catch (Throwable th) {
            g.c("CommonDeviceUtil", th.getMessage(), th);
            return "";
        }
    }

    public static int w(Context context) {
        com.mintegral.msdk.base.controller.authoritycontroller.a.a();
        if (!com.mintegral.msdk.base.controller.authoritycontroller.a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA) || context == null) {
            return -1;
        }
        try {
            return ((SensorManager) context.getSystemService("sensor")).getSensorList(-1).size();
        } catch (Exception e2) {
            g.c("CommonDeviceUtil", e2.getMessage(), e2);
            return -1;
        } catch (Throwable th) {
            g.c("CommonDeviceUtil", th.getMessage(), th);
            return -1;
        }
    }

    public static String x(Context context) {
        com.mintegral.msdk.base.controller.authoritycontroller.a.a();
        if (!com.mintegral.msdk.base.controller.authoritycontroller.a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA)) {
            return "";
        }
        if (context == null) {
            return "";
        }
        try {
            List inputMethodList = ((InputMethodManager) context.getSystemService("input_method")).getInputMethodList();
            StringBuilder sb = new StringBuilder();
            for (int i2 = 0; i2 < inputMethodList.size(); i2++) {
                CharSequence loadLabel = ((InputMethodInfo) inputMethodList.get(i2)).loadLabel(context.getPackageManager());
                StringBuilder sb2 = new StringBuilder("keybroad");
                sb2.append(i2);
                sb2.append(loadLabel);
                sb2.append(" ");
                sb.append(sb2.toString());
            }
            return sb.toString();
        } catch (Exception e2) {
            g.c("CommonDeviceUtil", e2.getMessage(), e2);
            return "";
        } catch (Throwable th) {
            g.c("CommonDeviceUtil", th.getMessage(), th);
            return "";
        }
    }

    /* JADX WARNING: Unknown top exception splitter block from list: {B:53:0x00a2=Splitter:B:53:0x00a2, B:42:0x0085=Splitter:B:42:0x0085} */
    public static String y(Context context) {
        FileReader fileReader;
        BufferedReader bufferedReader;
        com.mintegral.msdk.base.controller.authoritycontroller.a.a();
        if (!com.mintegral.msdk.base.controller.authoritycontroller.a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA)) {
            return "";
        }
        if (context == null) {
            return "";
        }
        BufferedReader bufferedReader2 = null;
        try {
            fileReader = new FileReader("/proc/meminfo");
            try {
                bufferedReader = new BufferedReader(fileReader, 8192);
            } catch (IOException e2) {
                e = e2;
                g.c("CommonDeviceUtil", e.getMessage(), e);
                try {
                    bufferedReader2.close();
                } catch (IOException e3) {
                    g.c("CommonDeviceUtil", e3.getMessage(), e3);
                }
                fileReader.close();
                return "";
            } catch (Throwable th) {
                th = th;
                try {
                    g.c("CommonDeviceUtil", th.getMessage(), th);
                    try {
                        bufferedReader2.close();
                    } catch (IOException e4) {
                        g.c("CommonDeviceUtil", e4.getMessage(), e4);
                    }
                    try {
                        fileReader.close();
                    } catch (IOException e5) {
                        g.c("CommonDeviceUtil", e5.getMessage(), e5);
                    }
                    return "";
                } catch (Throwable th2) {
                    th = th2;
                    try {
                        bufferedReader2.close();
                    } catch (IOException e6) {
                        g.c("CommonDeviceUtil", e6.getMessage(), e6);
                    }
                    try {
                        fileReader.close();
                    } catch (IOException e7) {
                        g.c("CommonDeviceUtil", e7.getMessage(), e7);
                    }
                    throw th;
                }
            }
            try {
                String formatFileSize = Formatter.formatFileSize(context, Long.valueOf(bufferedReader.readLine().split("\\s+")[1]).longValue() * PlaybackStateCompat.ACTION_PLAY_FROM_MEDIA_ID);
                try {
                    bufferedReader.close();
                } catch (IOException e8) {
                    g.c("CommonDeviceUtil", e8.getMessage(), e8);
                }
                try {
                    fileReader.close();
                } catch (IOException e9) {
                    g.c("CommonDeviceUtil", e9.getMessage(), e9);
                }
                return formatFileSize;
            } catch (IOException e10) {
                e = e10;
                bufferedReader2 = bufferedReader;
                g.c("CommonDeviceUtil", e.getMessage(), e);
                bufferedReader2.close();
                fileReader.close();
                return "";
            } catch (Throwable th3) {
                th = th3;
                bufferedReader2 = bufferedReader;
                bufferedReader2.close();
                fileReader.close();
                throw th;
            }
        } catch (IOException e11) {
            e = e11;
            fileReader = null;
            g.c("CommonDeviceUtil", e.getMessage(), e);
            bufferedReader2.close();
            fileReader.close();
            return "";
        } catch (Throwable th4) {
            th = th4;
            fileReader = null;
            bufferedReader2.close();
            fileReader.close();
            throw th;
        }
    }
}
