package com.mintegral.msdk.base.utils;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Process;
import android.text.TextUtils;
import android.util.Log;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.b.b;
import com.mintegral.msdk.base.b.g;
import com.mintegral.msdk.base.b.h;
import com.mintegral.msdk.base.b.i;
import com.mintegral.msdk.base.b.s;
import com.mintegral.msdk.base.common.b.c;
import com.mintegral.msdk.base.common.b.e;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.out.IDownloadListener;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.Iterator;
import java.util.List;

/* compiled from: CommonSDKUtil */
public final class j {

    /* renamed from: a reason: collision with root package name */
    public static boolean f2615a = false;

    /* compiled from: CommonSDKUtil */
    public static class a {
        private static Intent a() {
            return new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=com.package.name"));
        }

        private static List<ResolveInfo> a(Context context) {
            try {
                return context.getPackageManager().queryIntentActivities(a(), 0);
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        public static boolean a(String str) {
            return b(str) || c(str);
        }

        public static boolean b(String str) {
            try {
                if (!TextUtils.isEmpty(str)) {
                    return Uri.parse(str).getScheme().equals("market");
                }
            } catch (Throwable th) {
                g.d("SDKUtil", Log.getStackTraceString(th));
            }
            return false;
        }

        private static boolean c(String str) {
            try {
                if (!TextUtils.isEmpty(str)) {
                    Uri parse = Uri.parse(str);
                    if (parse.getHost().equals("play.google.com") || parse.getHost().equals("market.android.com")) {
                        return true;
                    }
                    return false;
                }
            } catch (Throwable th) {
                g.d("SDKUtil", Log.getStackTraceString(th));
            }
            return false;
        }

        public static boolean a(Context context, String str) {
            String str2 = "SDKUtil";
            try {
                StringBuilder sb = new StringBuilder("try google play: url = ");
                sb.append(str);
                g.d(str2, sb.toString());
                List a2 = a(context);
                if (a2 != null) {
                    if (a2.size() > 0) {
                        if (!b(str)) {
                            if (c(str)) {
                                String substring = str.substring(str.indexOf("details?id="));
                                StringBuilder sb2 = new StringBuilder("market://");
                                sb2.append(substring);
                                str = sb2.toString();
                            } else {
                                str = null;
                            }
                        }
                        if (TextUtils.isEmpty(str)) {
                            return false;
                        }
                        Intent a3 = a();
                        a3.setData(Uri.parse(str));
                        a3.addFlags(268435456);
                        Iterator it = a2.iterator();
                        while (true) {
                            if (it.hasNext()) {
                                if (((ResolveInfo) it.next()).activityInfo.packageName.equals("com.android.vending")) {
                                    a3.setPackage("com.android.vending");
                                    break;
                                }
                            } else {
                                break;
                            }
                        }
                        StringBuilder sb3 = new StringBuilder("open google play: details = ");
                        sb3.append(str);
                        g.b("SDKUtil", sb3.toString());
                        context.startActivity(a3);
                        return true;
                    }
                }
                return false;
            } catch (Throwable th) {
                g.d("SDKUtil", Log.getStackTraceString(th));
                return false;
            }
        }
    }

    public static void a(Context context, String str) {
        String str2 = "SDKUtil";
        try {
            StringBuilder sb = new StringBuilder("gotoGoogle = ");
            sb.append(str);
            g.b(str2, sb.toString());
            Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(str));
            intent.addFlags(268435456);
            boolean z = false;
            List queryIntentActivities = context.getPackageManager().queryIntentActivities(intent, 0);
            if (queryIntentActivities.size() > 0) {
                z = true;
            }
            if (!str.startsWith("market://")) {
                if (str.startsWith("https://play.google.com/")) {
                    String replace = str.replace("https://play.google.com/store/apps/details?id=", "");
                    StringBuilder sb2 = new StringBuilder("market://details?id=");
                    sb2.append(replace);
                    String sb3 = sb2.toString();
                    g.b("SDKUtil", "gotoGoogle = replace url");
                    a(context, sb3);
                }
            } else if (!z) {
                String replace2 = str.replace("market://details?id=", "");
                StringBuilder sb4 = new StringBuilder("https://play.google.com/store/apps/details?id=");
                sb4.append(replace2);
                String sb5 = sb4.toString();
                g.b("SDKUtil", "gotoGoogle = openurl");
                b(context, sb5);
            } else {
                Iterator it = queryIntentActivities.iterator();
                while (true) {
                    if (it.hasNext()) {
                        if (((ResolveInfo) it.next()).activityInfo.packageName.equals("com.android.vending")) {
                            intent.setClassName("com.android.vending", "com.android.vending.AssetBrowserActivity");
                            break;
                        }
                    } else {
                        break;
                    }
                }
                g.b("SDKUtil", "gotoGoogle = startActivity");
                try {
                    context.startActivity(intent);
                } catch (Exception e) {
                    g.c("SDKUtil", "start intent", e);
                    String replace3 = str.replace("market://details?id=", "");
                    StringBuilder sb6 = new StringBuilder("https://play.google.com/store/apps/details?id=");
                    sb6.append(replace3);
                    String sb7 = sb6.toString();
                    g.b("SDKUtil", "gotoGoogle = openurl");
                    b(context, sb7);
                }
            }
        } catch (Exception e2) {
            g.d("SDKUtil", "gotoGoogle = error");
            g.c("SDKUtil", "Exception", e2);
        }
    }

    public static void a(Context context, String str, CampaignEx campaignEx) {
        if (context != null) {
            if (f2615a) {
                b(context, str);
                return;
            }
            try {
                Class.forName("com.mintegral.msdk.activity.MTGCommonActivity");
                Intent intent = new Intent(context, Class.forName("com.mintegral.msdk.activity.MTGCommonActivity"));
                if (!TextUtils.isEmpty(str)) {
                    if (a.b(str)) {
                        String replace = str.replace("market://details?id=", "");
                        try {
                            StringBuilder sb = new StringBuilder("https://play.google.com/store/apps/details?id=");
                            sb.append(replace);
                            str = sb.toString();
                        } catch (Exception e) {
                            e = e;
                            str = replace;
                            b(context, str);
                            g.c("MTGCommonActivity", "", e);
                        }
                    }
                    g.b("SDKUtil", "openInnerBrowserUrl = openurl");
                    intent.putExtra("url", str);
                    StringBuilder sb2 = new StringBuilder("webview url = ");
                    sb2.append(str);
                    g.b("url", sb2.toString());
                    intent.setFlags(268435456);
                    intent.putExtra("mvcommon", campaignEx);
                    context.startActivity(intent);
                }
            } catch (Exception e2) {
                e = e2;
                b(context, str);
                g.c("MTGCommonActivity", "", e);
            }
        }
    }

    public static void b(Context context, String str) {
        if (str != null && context != null) {
            try {
                if (a.b(str)) {
                    String replace = str.replace("market://details?id=", "");
                    try {
                        StringBuilder sb = new StringBuilder("https://play.google.com/store/apps/details?id=");
                        sb.append(replace);
                        str = sb.toString();
                    } catch (Exception e) {
                        String str2 = replace;
                        e = e;
                        str = str2;
                        g.d("SDKUtil", "openBrowserUrl = error");
                        e.printStackTrace();
                        try {
                            Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(str));
                            intent.addFlags(268468224);
                            context.startActivity(intent);
                        } catch (Exception e2) {
                            g.d("SDKUtil", "openBrowserUrl = error2");
                            e2.printStackTrace();
                            return;
                        }
                    }
                }
                g.b("SDKUtil", "openBrowserUrl = openurl");
                Intent intent2 = new Intent("android.intent.action.VIEW", Uri.parse(str));
                intent2.addFlags(268435456);
                ResolveInfo resolveActivity = context.getPackageManager().resolveActivity(intent2, 65536);
                if (resolveActivity != null) {
                    intent2.setClassName(resolveActivity.activityInfo.packageName, resolveActivity.activityInfo.name);
                }
                context.startActivity(intent2);
            } catch (Exception e3) {
                e = e3;
                g.d("SDKUtil", "openBrowserUrl = error");
                e.printStackTrace();
                Intent intent3 = new Intent("android.intent.action.VIEW", Uri.parse(str));
                intent3.addFlags(268468224);
                context.startActivity(intent3);
            }
        }
    }

    public static String a(String str) {
        String str2;
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        File a2 = e.a(c.MINTEGRAL_700_IMG);
        if (TextUtils.isEmpty(str)) {
            str2 = "";
        } else if (str.lastIndexOf("/") == -1) {
            StringBuilder sb = new StringBuilder();
            sb.append(str.hashCode());
            str2 = sb.toString();
        } else {
            StringBuilder sb2 = new StringBuilder();
            sb2.append(str.hashCode() + str.substring(str.lastIndexOf("/") + 1).hashCode());
            str2 = sb2.toString();
        }
        return new File(a2, str2).getAbsolutePath();
    }

    public static void a(final String str, Context context) {
        AnonymousClass1 r0 = new com.mintegral.msdk.base.common.e.a() {
            public final void b() {
            }

            public final void a() {
                try {
                    b.a();
                    com.mintegral.msdk.b.a b = b.b(str);
                    if (b == null) {
                        b.a();
                        b = b.b();
                    }
                    s.a((h) i.a(com.mintegral.msdk.base.controller.a.d().h())).a(Long.valueOf(b.aa()));
                    com.mintegral.msdk.base.b.b.a((h) i.a(com.mintegral.msdk.base.controller.a.d().h())).c();
                    com.mintegral.msdk.base.b.c.a((h) i.a(com.mintegral.msdk.base.controller.a.d().h())).a(Long.valueOf(b.Y()));
                } catch (Throwable th) {
                    th.printStackTrace();
                }
            }
        };
        if (context != null) {
            new com.mintegral.msdk.base.common.e.b(context).b(r0);
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(5:23|24|25|26|27) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:26:0x00f5 */
    public static void a(String str, final String str2, final CampaignEx campaignEx) {
        try {
            String obj = r.b(com.mintegral.msdk.base.controller.a.d().h(), str2, "").toString();
            if (!TextUtils.isEmpty(obj)) {
                File file = new File(obj);
                if (file.exists()) {
                    com.mintegral.msdk.click.b.a(com.mintegral.msdk.base.controller.a.d().h(), Uri.fromFile(file), str2);
                    return;
                }
            } else {
                Context h = com.mintegral.msdk.base.controller.a.d().h();
                StringBuilder sb = new StringBuilder();
                sb.append(str2);
                sb.append("process");
                int intValue = ((Integer) r.b(h, sb.toString(), Integer.valueOf(0))).intValue();
                int myPid = Process.myPid();
                if (intValue != 0 && intValue == myPid) {
                    Context h2 = com.mintegral.msdk.base.controller.a.d().h();
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append(str2);
                    sb2.append("isDowning");
                    long longValue = ((Long) r.b(h2, sb2.toString(), Long.valueOf(0))).longValue();
                    long currentTimeMillis = System.currentTimeMillis() - longValue;
                    if (longValue != 0 && currentTimeMillis < 36000000) {
                        Context h3 = com.mintegral.msdk.base.controller.a.d().h();
                        StringBuilder sb3 = new StringBuilder();
                        sb3.append(str2);
                        sb3.append("downloadType");
                        if (((Integer) r.b(h3, sb3.toString(), Integer.valueOf(-1))).intValue() == 1) {
                            com.mintegral.msdk.click.a.b(campaignEx, com.mintegral.msdk.base.controller.a.d().h(), "downloading");
                            return;
                        } else {
                            com.mintegral.msdk.click.a.a(campaignEx, com.mintegral.msdk.base.controller.a.d().h(), "downloading");
                            return;
                        }
                    }
                }
            }
            if (t.f2622a == -1) {
                Class.forName("com.mintegral.msdk.mtgdownload.b");
                Class.forName("com.mintegral.msdk.mtgdownload.g");
                t.f2622a = 1;
                t.f2622a = 0;
            }
            if (t.f2622a == 1) {
                a(str2, 1, campaignEx);
                Context h4 = com.mintegral.msdk.base.controller.a.d().h();
                boolean a2 = t.a(h4);
                boolean c = t.c(h4);
                if (!t.b(h4)) {
                    com.mintegral.msdk.click.b.b(h4, str2);
                } else if (!c) {
                    a(campaignEx, str2);
                } else if (!a2) {
                    a(campaignEx, str2);
                } else {
                    Context h5 = com.mintegral.msdk.base.controller.a.d().h();
                    StringBuilder sb4 = new StringBuilder();
                    sb4.append(str2);
                    sb4.append("isDowning");
                    r.a(h5, sb4.toString(), Long.valueOf(System.currentTimeMillis()));
                    Context h6 = com.mintegral.msdk.base.controller.a.d().h();
                    StringBuilder sb5 = new StringBuilder();
                    sb5.append(str2);
                    sb5.append("process");
                    r.a(h6, sb5.toString(), Integer.valueOf(Process.myPid()));
                    Class cls = Class.forName("com.mintegral.msdk.mtgdownload.g");
                    Object newInstance = cls.getConstructor(new Class[]{Context.class, String.class}).newInstance(new Object[]{com.mintegral.msdk.base.controller.a.d().h(), str2});
                    if (!TextUtils.isEmpty(str)) {
                        cls.getMethod("setTitle", new Class[]{String.class}).invoke(newInstance, new Object[]{str});
                    }
                    cls.getMethod("setDownloadListener", new Class[]{IDownloadListener.class}).invoke(newInstance, new Object[]{new IDownloadListener() {
                        final /* synthetic */ boolean c = true;

                        public final void onProgressUpdate(int i) {
                        }

                        public final void onStatus(int i) {
                        }

                        public final void onStart() {
                            com.mintegral.msdk.click.a.b(campaignEx, com.mintegral.msdk.base.controller.a.d().h(), "start");
                        }

                        public final void onEnd(int i, int i2, String str) {
                            Context h = com.mintegral.msdk.base.controller.a.d().h();
                            StringBuilder sb = new StringBuilder();
                            sb.append(str2);
                            sb.append("isDowning");
                            r.a(h, sb.toString(), Long.valueOf(0));
                            Context h2 = com.mintegral.msdk.base.controller.a.d().h();
                            StringBuilder sb2 = new StringBuilder();
                            sb2.append(str2);
                            sb2.append("process");
                            r.a(h2, sb2.toString(), Integer.valueOf(0));
                            StringBuilder sb3 = new StringBuilder("download listener onEnd result = ");
                            sb3.append(i);
                            sb3.append(" nid = ");
                            sb3.append(i2);
                            sb3.append(" file = ");
                            sb3.append(str);
                            sb3.append("-sdkutil:");
                            sb3.append(this.c);
                            g.b("SDKUtil", sb3.toString());
                            if (!TextUtils.isEmpty(str)) {
                                File file = new File(str);
                                if (file.exists() && this.c) {
                                    com.mintegral.msdk.click.b.a(com.mintegral.msdk.base.controller.a.d().h(), Uri.fromFile(file), str2);
                                } else if (!this.c) {
                                    r.a(com.mintegral.msdk.base.controller.a.d().h(), str2, str);
                                }
                            }
                            com.mintegral.msdk.click.a.b(campaignEx, com.mintegral.msdk.base.controller.a.d().h(), TtmlNode.END);
                            g.b(i.a(com.mintegral.msdk.base.controller.a.d().h())).a(campaignEx);
                        }
                    }});
                    cls.getMethod("start", new Class[0]).invoke(newInstance, new Object[0]);
                }
            } else {
                a(campaignEx, str2);
            }
        } catch (Throwable unused) {
            t.f2622a = -1;
            g.b("downloadapk", "can't find download jar, use simple method");
            a(campaignEx, str2);
        }
    }

    public static void a(String str, int i, CampaignEx campaignEx) {
        Context h = com.mintegral.msdk.base.controller.a.d().h();
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append("downloadType");
        r.a(h, sb.toString(), Integer.valueOf(i));
        if (campaignEx != null) {
            Context h2 = com.mintegral.msdk.base.controller.a.d().h();
            StringBuilder sb2 = new StringBuilder();
            sb2.append(str);
            sb2.append("linkType");
            r.a(h2, sb2.toString(), Integer.valueOf(campaignEx.getLinkType()));
            Context h3 = com.mintegral.msdk.base.controller.a.d().h();
            StringBuilder sb3 = new StringBuilder();
            sb3.append(str);
            sb3.append("rid");
            r.a(h3, sb3.toString(), campaignEx.getRequestIdNotice());
            Context h4 = com.mintegral.msdk.base.controller.a.d().h();
            StringBuilder sb4 = new StringBuilder();
            sb4.append(str);
            sb4.append("cid");
            r.a(h4, sb4.toString(), campaignEx.getId());
            return;
        }
        Context h5 = com.mintegral.msdk.base.controller.a.d().h();
        StringBuilder sb5 = new StringBuilder();
        sb5.append(str);
        sb5.append("linkType");
        r.a(h5, sb5.toString(), Integer.valueOf(-1));
        Context h6 = com.mintegral.msdk.base.controller.a.d().h();
        StringBuilder sb6 = new StringBuilder();
        sb6.append(str);
        sb6.append("rid");
        r.a(h6, sb6.toString(), "");
        Context h7 = com.mintegral.msdk.base.controller.a.d().h();
        StringBuilder sb7 = new StringBuilder();
        sb7.append(str);
        sb7.append("cid");
        r.a(h7, sb7.toString(), "");
    }

    private static void a(final CampaignEx campaignEx, final String str) {
        try {
            a(str, 2, campaignEx);
            Context h = com.mintegral.msdk.base.controller.a.d().h();
            if (!t.b(h)) {
                com.mintegral.msdk.click.b.b(h, str);
                return;
            }
            Context h2 = com.mintegral.msdk.base.controller.a.d().h();
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append("isDowning");
            r.a(h2, sb.toString(), Long.valueOf(System.currentTimeMillis()));
            Context h3 = com.mintegral.msdk.base.controller.a.d().h();
            StringBuilder sb2 = new StringBuilder();
            sb2.append(str);
            sb2.append("process");
            r.a(h3, sb2.toString(), Integer.valueOf(Process.myPid()));
            new Thread(new Runnable() {
                final /* synthetic */ boolean c = true;

                public final void run() {
                    j.a(com.mintegral.msdk.base.controller.a.d().h(), campaignEx, str, this.c);
                }
            }).start();
        } catch (Throwable th) {
            if (MIntegralConstans.DEBUG) {
                th.printStackTrace();
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:46:0x0118 A[Catch:{ all -> 0x0134 }] */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x0120 A[SYNTHETIC, Splitter:B:49:0x0120] */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x012a A[SYNTHETIC, Splitter:B:54:0x012a] */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x0138 A[SYNTHETIC, Splitter:B:63:0x0138] */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x0142 A[SYNTHETIC, Splitter:B:68:0x0142] */
    public static void a(Context context, CampaignEx campaignEx, String str, boolean z) {
        OutputStream outputStream;
        InputStream inputStream;
        OutputStream outputStream2 = null;
        try {
            File a2 = e.a("/apk", context, new boolean[1]);
            String a3 = com.mintegral.msdk.click.b.a(str);
            StringBuilder sb = new StringBuilder();
            sb.append(a3);
            sb.append(".apk");
            File file = new File(a2, sb.toString());
            if (file.exists()) {
                file.delete();
            }
            if (z) {
                com.mintegral.msdk.click.a.a(campaignEx, context, "start");
            } else {
                com.mintegral.msdk.click.a.a(campaignEx, context, "shortcuts_start");
            }
            URLConnection openConnection = new URL(str).openConnection();
            openConnection.setConnectTimeout(8000);
            int contentLength = openConnection.getContentLength();
            inputStream = openConnection.getInputStream();
            try {
                byte[] bArr = new byte[1024];
                outputStream = new FileOutputStream(file, true);
                int i = 0;
                while (true) {
                    try {
                        int read = inputStream.read(bArr);
                        if (read == -1) {
                            break;
                        }
                        outputStream.write(bArr, 0, read);
                        i += read;
                    } catch (Throwable th) {
                        th = th;
                        if (outputStream != null) {
                            try {
                                outputStream.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                        if (inputStream != null) {
                            try {
                                inputStream.close();
                            } catch (IOException e2) {
                                e2.printStackTrace();
                            }
                        }
                        throw th;
                    }
                }
                if (i == contentLength) {
                    Context h = com.mintegral.msdk.base.controller.a.d().h();
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append(str);
                    sb2.append("isDowning");
                    r.a(h, sb2.toString(), Long.valueOf(0));
                    Context h2 = com.mintegral.msdk.base.controller.a.d().h();
                    StringBuilder sb3 = new StringBuilder();
                    sb3.append(str);
                    sb3.append("process");
                    r.a(h2, sb3.toString(), Integer.valueOf(0));
                    com.mintegral.msdk.click.a.a(campaignEx, context, TtmlNode.END);
                    g.b(i.a(com.mintegral.msdk.base.controller.a.d().h())).a(campaignEx);
                    if (file.exists() && z) {
                        com.mintegral.msdk.click.b.a(context, Uri.fromFile(file), str);
                        r.a(com.mintegral.msdk.base.controller.a.d().h(), str, file.getAbsolutePath());
                    } else if (!z) {
                        r.a(com.mintegral.msdk.base.controller.a.d().h(), str, file.getAbsolutePath());
                    }
                }
                try {
                    outputStream.close();
                } catch (IOException e3) {
                    e3.printStackTrace();
                }
                if (inputStream != null) {
                    try {
                        inputStream.close();
                    } catch (IOException e4) {
                        e4.printStackTrace();
                    }
                }
            } catch (Throwable th2) {
                th = th2;
                if (MIntegralConstans.DEBUG) {
                    th.printStackTrace();
                }
                com.mintegral.msdk.click.b.b(context, str);
                if (outputStream2 != null) {
                    try {
                        outputStream2.close();
                    } catch (IOException e5) {
                        e5.printStackTrace();
                    }
                }
                if (inputStream != null) {
                    try {
                        inputStream.close();
                    } catch (IOException e6) {
                        e6.printStackTrace();
                    }
                }
            }
        } catch (Throwable th3) {
            th = th3;
            inputStream = null;
            outputStream = null;
            if (outputStream != null) {
            }
            if (inputStream != null) {
            }
            throw th;
        }
    }
}
