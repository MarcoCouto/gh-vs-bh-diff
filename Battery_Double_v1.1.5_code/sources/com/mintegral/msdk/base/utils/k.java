package com.mintegral.msdk.base.utils;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build.VERSION;
import android.os.Looper;
import android.os.PowerManager;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import com.github.mikephil.charting.utils.Utils;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.b.b;
import com.mintegral.msdk.base.b.h;
import com.mintegral.msdk.base.b.i;
import com.mintegral.msdk.base.b.m;
import com.mintegral.msdk.base.b.s;
import com.mintegral.msdk.base.controller.a;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.entity.l;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.UUID;
import java.util.regex.Pattern;
import org.json.JSONArray;
import org.json.JSONException;

/* compiled from: CommonUtil */
public final class k {

    /* renamed from: a reason: collision with root package name */
    static List<String> f2619a = null;
    private static String b = "[一-龥]";
    private static Pattern c = Pattern.compile("[一-龥]");
    private static int d = 1;
    private static boolean e = true;

    public static <T extends String> boolean a(T t) {
        return t == null || t.length() == 0;
    }

    public static <T extends String> boolean b(T t) {
        return t != null && t.length() > 0;
    }

    public static boolean a(Context context) {
        try {
            NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
            if (activeNetworkInfo != null && "wifi".equals(activeNetworkInfo.getTypeName().toLowerCase(Locale.US))) {
                return true;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return false;
    }

    public static boolean b(Context context) {
        try {
            if (((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo() != null) {
                return true;
            }
            return false;
        } catch (Exception e2) {
            e2.printStackTrace();
            return false;
        }
    }

    public static boolean a(CampaignEx campaignEx) {
        return campaignEx != null && !TextUtils.isEmpty(campaignEx.getDeepLinkURL());
    }

    public static boolean a(Context context, String str) {
        if (f2619a != null) {
            return f2619a.contains(str);
        }
        f2619a = new ArrayList();
        try {
            PackageManager packageManager = context.getPackageManager();
            if (a.b == null || a.b.size() == 0) {
                a.b = new ArrayList();
                List installedPackages = packageManager.getInstalledPackages(0);
                List b2 = a.d().b();
                for (int i = 0; i < installedPackages.size(); i++) {
                    PackageInfo packageInfo = (PackageInfo) installedPackages.get(i);
                    if ((packageInfo.applicationInfo.flags & 1) <= 0) {
                        a.b.add(packageInfo.packageName);
                    } else if (b2 != null && b2.size() > 0 && b2.contains(packageInfo.packageName)) {
                        a.b.add(packageInfo.packageName);
                    }
                }
            }
            for (int i2 = 0; i2 < a.b.size(); i2++) {
                f2619a.add((String) a.b.get(i2));
            }
            return f2619a.contains(str);
        } catch (Throwable th) {
            if (MIntegralConstans.DEBUG) {
                th.printStackTrace();
            }
            return false;
        }
    }

    public static boolean a() {
        return e;
    }

    public static float c(Context context) {
        try {
            float f = context.getResources().getDisplayMetrics().density;
            if (f == 0.0f) {
                return 2.5f;
            }
            return f;
        } catch (Exception e2) {
            e2.printStackTrace();
            return 2.5f;
        }
    }

    public static int a(Context context, float f) {
        float f2 = 2.5f;
        try {
            float f3 = context.getResources().getDisplayMetrics().density;
            if (f3 != 0.0f) {
                f2 = f3;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return (int) ((f / f2) + 0.5f);
    }

    public static int b(Context context, float f) {
        if (context == null) {
            return 0;
        }
        Resources resources = context.getResources();
        if (resources == null) {
            return 0;
        }
        return (int) ((f * resources.getDisplayMetrics().density) + 0.5f);
    }

    public static int d(Context context) {
        return (int) ((context.getResources().getDisplayMetrics().scaledDensity * 30.0f) + 0.5f);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0024, code lost:
        if (r2 != null) goto L_0x0026;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0031, code lost:
        if (r2 != null) goto L_0x0026;
     */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x002d A[SYNTHETIC, Splitter:B:19:0x002d] */
    public static long a(File file) throws Exception {
        long j = 0;
        FileInputStream fileInputStream = null;
        try {
            if (file.exists()) {
                FileInputStream fileInputStream2 = new FileInputStream(file);
                try {
                    j = (long) fileInputStream2.available();
                    fileInputStream = fileInputStream2;
                } catch (Exception unused) {
                    fileInputStream = fileInputStream2;
                } catch (Throwable th) {
                    th = th;
                    fileInputStream = fileInputStream2;
                    if (fileInputStream != null) {
                    }
                    throw th;
                }
            } else {
                file.createNewFile();
                g.d("获取文件大小", "文件不存在!");
            }
        } catch (Exception unused2) {
        } catch (Throwable th2) {
            th = th2;
            if (fileInputStream != null) {
                try {
                    fileInputStream.close();
                } catch (Exception unused3) {
                }
            }
            throw th;
        }
        return j;
    }

    public static int e(Context context) {
        try {
            Class cls = Class.forName("com.android.internal.R$dimen");
            return context.getResources().getDimensionPixelSize(Integer.parseInt(cls.getField("status_bar_height").get(cls.newInstance()).toString()));
        } catch (Exception e2) {
            e2.printStackTrace();
            return 0;
        }
    }

    public static float f(Context context) {
        return (float) context.getResources().getDisplayMetrics().widthPixels;
    }

    public static int b() {
        int i = d;
        d = i + 1;
        return i;
    }

    public static String c() {
        String str = "";
        try {
            JSONArray jSONArray = new JSONArray();
            a.d();
            List<Long> g = a.g();
            if (g != null && g.size() > 0) {
                for (Long longValue : g) {
                    jSONArray.put(longValue.longValue());
                }
            }
            if (jSONArray.length() > 0) {
                return a(jSONArray);
            }
            return str;
        } catch (Exception e2) {
            e2.printStackTrace();
            return str;
        }
    }

    public static String d() {
        String str = "";
        try {
            b.a();
            com.mintegral.msdk.b.a b2 = b.b(a.d().j());
            JSONArray jSONArray = new JSONArray();
            if (b2 != null && b2.aK() == 1) {
                StringBuilder sb = new StringBuilder("excludes cfc:");
                sb.append(b2.aK());
                g.b("CommonUtils", sb.toString());
                long[] c2 = m.a((h) i.a(a.d().h())).c();
                if (c2 != null) {
                    for (long j : c2) {
                        StringBuilder sb2 = new StringBuilder("excludes campaignIds:");
                        sb2.append(c2);
                        g.b("CommonUtils", sb2.toString());
                        jSONArray.put(j);
                    }
                }
            }
            if (jSONArray.length() > 0) {
                str = a(jSONArray);
            }
            StringBuilder sb3 = new StringBuilder("get excludes:");
            sb3.append(str);
            g.b("CommonUtils", sb3.toString());
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return str;
    }

    public static String a(JSONArray jSONArray) {
        String j = a.d().j();
        b.a();
        com.mintegral.msdk.b.a b2 = b.b(j);
        if (b2 == null) {
            b.a();
            b2 = b.b();
        }
        int ae = b2.ae();
        if (jSONArray.length() <= ae) {
            return jSONArray.toString();
        }
        JSONArray jSONArray2 = new JSONArray();
        for (int i = 0; i < ae; i++) {
            try {
                jSONArray2.put(jSONArray.get(i));
            } catch (JSONException e2) {
                e2.printStackTrace();
            }
        }
        return jSONArray2.toString();
    }

    public static boolean a(String str, Context context) {
        boolean z;
        try {
            if (context.getPackageManager().checkPermission(str, context.getPackageName()) == 0) {
                z = true;
                StringBuilder sb = new StringBuilder("Permission ");
                sb.append(str);
                sb.append(" is granted");
                g.a("CommonUtils", sb.toString());
            } else {
                StringBuilder sb2 = new StringBuilder("Permission ");
                sb2.append(str);
                sb2.append(" is NOT granted");
                g.a("CommonUtils", sb2.toString());
                z = false;
            }
            return z;
        } catch (Exception unused) {
            return false;
        }
    }

    public static boolean b(CampaignEx campaignEx) {
        if (campaignEx != null) {
            try {
                return campaignEx.getRetarget_offer() == 1;
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        return false;
    }

    public static void a(List<CampaignEx> list) {
        if (list != null && list.size() != 0) {
            ArrayList arrayList = new ArrayList();
            int size = list.size();
            for (int i = 0; i < size; i++) {
                CampaignEx campaignEx = (CampaignEx) list.get(i);
                if ((!a(a.d().h(), campaignEx.getPackageName()) && campaignEx.getOfferType() == 99) || a(campaignEx)) {
                    arrayList.add(campaignEx);
                }
            }
            s.a((h) i.a(a.d().h())).a((List<CampaignEx>) arrayList);
        }
    }

    public static int g(Context context) {
        if (context == null) {
            return -1;
        }
        try {
            List installedPackages = context.getPackageManager().getInstalledPackages(0);
            if (installedPackages == null || installedPackages.size() <= 0) {
                return -1;
            }
            return installedPackages.size();
        } catch (Exception e2) {
            e2.printStackTrace();
            return -1;
        }
    }

    public static double c(String str) {
        try {
            if (!TextUtils.isEmpty(str)) {
                return Double.parseDouble(str);
            }
            return Utils.DOUBLE_EPSILON;
        } catch (Exception e2) {
            e2.printStackTrace();
            return Utils.DOUBLE_EPSILON;
        }
    }

    public static int a(Object obj) {
        if (obj == null) {
            return 0;
        }
        try {
            if (obj instanceof String) {
                return Integer.parseInt((String) obj);
            }
            return 0;
        } catch (Throwable th) {
            g.c("CommonUtils", th.getMessage(), th);
            return 0;
        }
    }

    public static double a(Double d2) {
        String str = "CommonUtils";
        try {
            StringBuilder sb = new StringBuilder("format before num:");
            sb.append(d2);
            g.b(str, sb.toString());
            String format = new DecimalFormat("0.00", DecimalFormatSymbols.getInstance(Locale.US)).format(d2);
            StringBuilder sb2 = new StringBuilder("format after format:");
            sb2.append(format);
            g.b("CommonUtils", sb2.toString());
            if (s.b(format)) {
                return Double.parseDouble(format);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return Utils.DOUBLE_EPSILON;
    }

    private static DisplayMetrics o(Context context) {
        if (context == null) {
            return null;
        }
        DisplayMetrics displayMetrics = new DisplayMetrics();
        try {
            Display defaultDisplay = ((WindowManager) context.getSystemService("window")).getDefaultDisplay();
            Class.forName("android.view.Display").getMethod("getRealMetrics", new Class[]{DisplayMetrics.class}).invoke(defaultDisplay, new Object[]{displayMetrics});
        } catch (Throwable th) {
            th.printStackTrace();
            displayMetrics = context.getResources().getDisplayMetrics();
        }
        return displayMetrics;
    }

    public static int h(Context context) {
        if (context == null) {
            return 0;
        }
        try {
            return o(context).heightPixels;
        } catch (Exception e2) {
            e2.printStackTrace();
            return 0;
        }
    }

    public static int i(Context context) {
        if (context == null) {
            return 0;
        }
        try {
            return o(context).widthPixels;
        } catch (Exception e2) {
            e2.printStackTrace();
            return 0;
        }
    }

    public static int j(Context context) {
        if (context == null) {
            return 0;
        }
        try {
            return context.getResources().getDisplayMetrics().widthPixels;
        } catch (Exception e2) {
            e2.printStackTrace();
            return 0;
        }
    }

    public static int k(Context context) {
        if (context == null) {
            return 0;
        }
        try {
            return context.getResources().getDisplayMetrics().heightPixels;
        } catch (Exception e2) {
            e2.printStackTrace();
            return 0;
        }
    }

    public static void a(View view) {
        if (view != null) {
            try {
                if (VERSION.SDK_INT >= 11) {
                    view.setSystemUiVisibility(4102);
                }
            } catch (Throwable th) {
                th.printStackTrace();
            }
        }
    }

    public static int l(Context context) {
        try {
            if (context.getResources().getIdentifier("config_showNavigationBar", "bool", "android") != 0) {
                return context.getResources().getDimensionPixelSize(context.getResources().getIdentifier("navigation_bar_height", "dimen", "android"));
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return 0;
    }

    public static boolean m(Context context) {
        boolean z;
        Throwable th;
        try {
            Resources resources = context.getResources();
            int identifier = resources.getIdentifier("config_showNavigationBar", "bool", "android");
            z = identifier > 0 ? resources.getBoolean(identifier) : false;
            try {
                Class cls = Class.forName("android.os.SystemProperties");
                String str = (String) cls.getMethod("get", new Class[]{String.class}).invoke(cls, new Object[]{"qemu.hw.mainkeys"});
                if ("1".equals(str)) {
                    return false;
                }
                if ("0".equals(str)) {
                    return true;
                }
                return z;
            } catch (Throwable th2) {
                th = th2;
                g.c("CommonUtils", th.getMessage(), th);
                return z;
            }
        } catch (Throwable th3) {
            th = th3;
            z = false;
            g.c("CommonUtils", th.getMessage(), th);
            return z;
        }
    }

    public static void a(ImageView imageView) {
        if (imageView != null) {
            try {
                imageView.setImageResource(0);
                imageView.setImageDrawable(null);
                imageView.setImageURI(null);
                imageView.setImageBitmap(null);
            } catch (Throwable th) {
                if (MIntegralConstans.DEBUG) {
                    th.printStackTrace();
                }
            }
        }
    }

    public static List<String> b(JSONArray jSONArray) {
        ArrayList arrayList = null;
        if (jSONArray != null) {
            try {
                if (jSONArray.length() > 0) {
                    ArrayList arrayList2 = new ArrayList();
                    for (int i = 0; i < jSONArray.length(); i++) {
                        String optString = jSONArray.optString(i);
                        if (s.b(optString)) {
                            arrayList2.add(optString);
                        }
                    }
                    arrayList = arrayList2;
                }
            } catch (Throwable th) {
                g.c("CommonUtils", th.getMessage(), th);
                return null;
            }
        }
        return arrayList;
    }

    public static String d(String str) {
        try {
            if (s.b(str)) {
                return URLEncoder.encode(str, "utf-8");
            }
        } catch (Throwable th) {
            g.c("CommonUtils", th.getMessage(), th);
        }
        return "";
    }

    public static boolean e() {
        return Looper.getMainLooper().getThread() == Thread.currentThread();
    }

    public static boolean n(Context context) {
        try {
            return ((PowerManager) context.getSystemService("power")).isScreenOn();
        } catch (Throwable th) {
            th.printStackTrace();
            return false;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:58:0x0117, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:?, code lost:
        r2.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x011c, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x011d, code lost:
        com.mintegral.msdk.base.utils.g.c("CommonUtils", r1.getMessage(), r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x0126, code lost:
        return null;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [B:18:0x0069, B:56:0x0113] */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x007a A[SYNTHETIC, Splitter:B:28:0x007a] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00a5 A[Catch:{ Exception -> 0x0117, Throwable -> 0x011c }] */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x00dc A[Catch:{ Exception -> 0x0117, Throwable -> 0x011c }] */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x0113 A[SYNTHETIC, Splitter:B:56:0x0113] */
    public static List<l> f() {
        BufferedReader bufferedReader;
        int i;
        List installedPackages;
        int i2;
        g.b("CommonUtils", "============getActivePbDatas strat");
        ArrayList arrayList = new ArrayList();
        try {
            bufferedReader = new BufferedReader(new InputStreamReader(Runtime.getRuntime().exec("ps").getInputStream()), 1024);
            while (true) {
                try {
                    String readLine = bufferedReader.readLine();
                    if (readLine == null) {
                        try {
                            break;
                        } catch (Exception e2) {
                            e = e2;
                        }
                    } else if (readLine.startsWith("u0_")) {
                        String[] split = readLine.split(" ");
                        if (split != null && split.length > 0) {
                            String str = split[split.length - 1];
                            arrayList.add(str);
                            StringBuilder sb = new StringBuilder("line:");
                            sb.append(readLine);
                            sb.append("  =====pg:");
                            sb.append(str);
                            g.d("CommonUtils", sb.toString());
                        }
                    }
                } catch (Exception e3) {
                    e = e3;
                    try {
                        e.printStackTrace();
                        if (bufferedReader != null) {
                            try {
                                bufferedReader.close();
                            } catch (Exception e4) {
                                e = e4;
                            }
                        }
                        ArrayList arrayList2 = new ArrayList();
                        installedPackages = a.d().h().getPackageManager().getInstalledPackages(0);
                        List b2 = a.d().b();
                        while (i2 < installedPackages.size()) {
                        }
                        ArrayList arrayList3 = new ArrayList();
                        while (i < arrayList.size()) {
                        }
                        g.b("CommonUtils", "=============getActivePbDatas end");
                        return arrayList3;
                    } catch (Throwable th) {
                        th = th;
                        if (bufferedReader != null) {
                            bufferedReader.close();
                        }
                        throw th;
                    }
                }
            }
            bufferedReader.close();
        } catch (Exception e5) {
            e = e5;
            bufferedReader = null;
            e.printStackTrace();
            if (bufferedReader != null) {
            }
            ArrayList arrayList22 = new ArrayList();
            installedPackages = a.d().h().getPackageManager().getInstalledPackages(0);
            List b22 = a.d().b();
            while (i2 < installedPackages.size()) {
            }
            ArrayList arrayList32 = new ArrayList();
            while (i < arrayList.size()) {
            }
            g.b("CommonUtils", "=============getActivePbDatas end");
            return arrayList32;
        } catch (Throwable th2) {
            th = th2;
            bufferedReader = null;
            if (bufferedReader != null) {
            }
            throw th;
        }
        ArrayList arrayList222 = new ArrayList();
        installedPackages = a.d().h().getPackageManager().getInstalledPackages(0);
        List b222 = a.d().b();
        for (i2 = 0; i2 < installedPackages.size(); i2++) {
            PackageInfo packageInfo = (PackageInfo) installedPackages.get(i2);
            if ((packageInfo.applicationInfo.flags & 1) <= 0) {
                arrayList222.add(packageInfo.packageName);
            } else if (b222 != null && b222.size() > 0 && b222.contains(packageInfo.packageName)) {
                arrayList222.add(packageInfo.packageName);
            }
        }
        ArrayList arrayList322 = new ArrayList();
        for (i = 0; i < arrayList.size(); i++) {
            String str2 = (String) arrayList.get(i);
            if (arrayList222.contains(str2)) {
                StringBuilder sb2 = new StringBuilder("后台活跃包名 pg:");
                sb2.append(str2);
                g.b("CommonUtils", sb2.toString());
                l lVar = new l();
                lVar.f2601a = str2;
                arrayList322.add(lVar);
            }
        }
        g.b("CommonUtils", "=============getActivePbDatas end");
        return arrayList322;
        e.printStackTrace();
        ArrayList arrayList2222 = new ArrayList();
        installedPackages = a.d().h().getPackageManager().getInstalledPackages(0);
        List b2222 = a.d().b();
        while (i2 < installedPackages.size()) {
        }
        ArrayList arrayList3222 = new ArrayList();
        while (i < arrayList.size()) {
        }
        g.b("CommonUtils", "=============getActivePbDatas end");
        return arrayList3222;
    }

    public static String g() {
        String str = "";
        try {
            StringBuilder sb = new StringBuilder();
            sb.append(UUID.randomUUID().toString());
            sb.append(System.currentTimeMillis());
            str = sb.toString();
        } catch (Throwable th) {
            th.printStackTrace();
        }
        if (!s.a(str)) {
            return str;
        }
        StringBuilder sb2 = new StringBuilder();
        sb2.append(System.currentTimeMillis());
        return sb2.toString();
    }

    public static String b(List<String> list) {
        if (list != null) {
            try {
                if (list.size() != 0) {
                    JSONArray jSONArray = new JSONArray();
                    for (String str : list) {
                        if (s.b(str)) {
                            jSONArray.put(str);
                        }
                    }
                    return jSONArray.toString();
                }
            } catch (Throwable th) {
                g.c("CommonUtils", th.getMessage(), th);
                return "[]";
            }
        }
        return "[]";
    }
}
