package com.mintegral.msdk.base.utils;

import android.content.Context;
import android.content.pm.ProviderInfo;
import android.content.pm.ServiceInfo;
import android.os.Build.VERSION;
import android.support.v4.app.NotificationCompat.Builder;
import com.mintegral.msdk.MIntegralConstans;
import com.vungle.warren.ui.JavascriptBridge.MraidHandler;

/* compiled from: TargetAdaptionUtils */
public final class t {

    /* renamed from: a reason: collision with root package name */
    public static int f2622a = -1;
    public static int b = -1;
    public static int c = -1;
    public static int d = -1;
    public static int e = -1;

    public static boolean a(Context context) {
        if (e == 1) {
            return true;
        }
        boolean z = false;
        if (e == 0) {
            return false;
        }
        if (context == null) {
            try {
                e = -1;
                return false;
            } catch (Exception unused) {
                e = -1;
            } catch (NoSuchMethodError unused2) {
                e = 0;
            } catch (Throwable unused3) {
                e = -1;
            }
        } else {
            if (c.k(context) >= 26 && VERSION.SDK_INT >= 26) {
                new Builder(context, MraidHandler.DOWNLOAD_ACTION);
                e = 1;
            }
            z = true;
            return z;
        }
    }

    public static boolean b(Context context) {
        boolean z = true;
        if (c == 1) {
            return true;
        }
        if (c == 0) {
            return false;
        }
        if (context == null) {
            try {
                c = -1;
                return false;
            } catch (Exception unused) {
                c = -1;
                z = false;
                return z;
            } catch (Throwable unused2) {
                c = -1;
                z = false;
                return z;
            }
        } else {
            if (c.k(context) >= 26 && VERSION.SDK_INT >= 26) {
                String[] strArr = context.getPackageManager().getPackageInfo(context.getPackageName(), 4096).requestedPermissions;
                if (strArr != null) {
                    if (strArr.length != 0) {
                        int length = strArr.length;
                        int i = 0;
                        boolean z2 = true;
                        while (true) {
                            if (i >= length) {
                                z = z2;
                                break;
                            } else if (strArr[i].equals("android.permission.REQUEST_INSTALL_PACKAGES")) {
                                c = 1;
                                break;
                            } else {
                                c = 0;
                                i++;
                                z2 = false;
                            }
                        }
                    } else {
                        c = 0;
                        return false;
                    }
                } else {
                    c = -1;
                    return false;
                }
            }
            return z;
        }
    }

    public static boolean c(Context context) {
        if (b == 1) {
            return true;
        }
        boolean z = false;
        if (b == 0) {
            return false;
        }
        if (context == null) {
            try {
                b = -1;
                return false;
            } catch (Exception unused) {
                b = -1;
            } catch (Throwable unused2) {
                b = -1;
            }
        } else {
            ServiceInfo[] serviceInfoArr = context.getPackageManager().getPackageInfo(context.getPackageName(), 4).services;
            if (serviceInfoArr == null) {
                b = -1;
                return false;
            } else if (serviceInfoArr.length == 0) {
                b = 0;
                return false;
            } else {
                int length = serviceInfoArr.length;
                int i = 0;
                boolean z2 = true;
                while (true) {
                    if (i >= length) {
                        z = z2;
                        break;
                    } else if (serviceInfoArr[i].name.equals("com.mintegral.msdk.shell.MTGService")) {
                        b = 1;
                        z = true;
                        break;
                    } else {
                        b = 0;
                        i++;
                        z2 = false;
                    }
                }
                return z;
            }
        }
    }

    public static boolean d(Context context) {
        if (d == 1) {
            return true;
        }
        boolean z = false;
        if (d == 0) {
            return false;
        }
        if (context == null) {
            try {
                d = -1;
                return false;
            } catch (Exception unused) {
                d = -1;
            } catch (Throwable unused2) {
                d = -1;
            }
        } else {
            ProviderInfo[] providerInfoArr = context.getPackageManager().getPackageInfo(context.getPackageName(), 8).providers;
            if (providerInfoArr == null) {
                d = -1;
                return false;
            } else if (providerInfoArr.length == 0) {
                d = 0;
                return false;
            } else {
                int length = providerInfoArr.length;
                int i = 0;
                boolean z2 = true;
                while (true) {
                    if (i >= length) {
                        z = z2;
                        break;
                    } else if (providerInfoArr[i].name.equals("com.mintegral.msdk.base.utils.MTGFileProvider")) {
                        d = 1;
                        z = true;
                        break;
                    } else {
                        d = 0;
                        i++;
                        z2 = false;
                    }
                }
                return z;
            }
        }
    }

    public static boolean e(Context context) {
        try {
            if (c.k(context) < 24 || VERSION.SDK_INT < 24) {
                return false;
            }
            return true;
        } catch (Throwable th) {
            if (!MIntegralConstans.DEBUG) {
                return false;
            }
            th.printStackTrace();
            return false;
        }
    }
}
