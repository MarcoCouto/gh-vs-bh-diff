package com.mintegral.msdk.base.utils;

import android.content.Context;
import com.mintegral.msdk.base.controller.a;

/* compiled from: ResourceUtil */
public final class p {
    public static int a(Context context, String str, String str2) {
        String str3 = "";
        try {
            str3 = a.d().a();
        } catch (Exception unused) {
            String str4 = "ResourceUtil";
            try {
                StringBuilder sb = new StringBuilder("MTGSDKContext.getInstance() is null resName:");
                sb.append(str);
                g.d(str4, sb.toString());
            } catch (Exception unused2) {
                StringBuilder sb2 = new StringBuilder("Resource not found resName:");
                sb2.append(str);
                g.d("ResourceUtil", sb2.toString());
                return -1;
            }
        }
        if (s.a(str3) && context != null) {
            str3 = context.getPackageName();
        }
        if (!s.a(str3) && context != null) {
            return context.getResources().getIdentifier(str, str2, str3);
        }
        return -1;
    }
}
