package com.mintegral.msdk.base.utils;

import android.util.Log;
import com.mintegral.msdk.MIntegralConstans;

/* compiled from: CommonLogUtil */
public final class g {

    /* renamed from: a reason: collision with root package name */
    public static boolean f2612a = false;
    public static boolean b = false;
    private static boolean c = false;
    private static boolean d = false;
    private static boolean e = false;
    private static boolean f = false;
    private static boolean g = false;
    private static boolean h = false;

    static {
        if (!MIntegralConstans.DEBUG) {
        }
    }

    public static void a(String str, String str2) {
        if (d) {
            Log.d(str, str2);
        }
    }

    public static void a(String str, String str2, Throwable th) {
        if (d) {
            Log.d(str, str2, th);
        }
    }

    public static void b(String str, String str2) {
        if (e) {
            Log.i(str, str2);
        }
    }

    public static void c(String str, String str2) {
        if (f) {
            Log.w(str, str2);
        }
    }

    public static void b(String str, String str2, Throwable th) {
        if (f) {
            Log.w(str, str2, th);
        }
    }

    public static void a(String str, Throwable th) {
        if (f) {
            Log.w(str, th);
        }
    }

    public static void d(String str, String str2) {
        if (g && str2 != null) {
            Log.e(str, str2);
        }
    }

    public static void c(String str, String str2, Throwable th) {
        if (!(!g || str2 == null || th == null)) {
            Log.e(str, str2, th);
        }
        if (h) {
        }
    }
}
