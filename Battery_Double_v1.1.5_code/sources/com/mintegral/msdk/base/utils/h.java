package com.mintegral.msdk.base.utils;

import com.google.android.exoplayer2.extractor.ts.PsExtractor;
import java.util.HashMap;
import java.util.Map;

/* compiled from: CommonMVEncoder */
public final class h {

    /* renamed from: a reason: collision with root package name */
    private static Map<Character, Character> f2613a;
    private static final char[] b = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '/'};
    private static char[] c = new char[b.length];
    private static final byte[] d = new byte[128];

    static {
        HashMap hashMap = new HashMap();
        f2613a = hashMap;
        hashMap.put(Character.valueOf('A'), Character.valueOf('v'));
        f2613a.put(Character.valueOf('B'), Character.valueOf('S'));
        f2613a.put(Character.valueOf('C'), Character.valueOf('o'));
        f2613a.put(Character.valueOf('D'), Character.valueOf('a'));
        f2613a.put(Character.valueOf('E'), Character.valueOf('j'));
        f2613a.put(Character.valueOf('F'), Character.valueOf('c'));
        f2613a.put(Character.valueOf('G'), Character.valueOf('7'));
        f2613a.put(Character.valueOf('H'), Character.valueOf('d'));
        f2613a.put(Character.valueOf('I'), Character.valueOf('R'));
        f2613a.put(Character.valueOf('J'), Character.valueOf('z'));
        f2613a.put(Character.valueOf('K'), Character.valueOf('p'));
        f2613a.put(Character.valueOf('L'), Character.valueOf('W'));
        f2613a.put(Character.valueOf('M'), Character.valueOf('i'));
        f2613a.put(Character.valueOf('N'), Character.valueOf('f'));
        f2613a.put(Character.valueOf('O'), Character.valueOf('G'));
        f2613a.put(Character.valueOf('P'), Character.valueOf('y'));
        f2613a.put(Character.valueOf('Q'), Character.valueOf('N'));
        f2613a.put(Character.valueOf('R'), Character.valueOf('x'));
        f2613a.put(Character.valueOf('S'), Character.valueOf('Z'));
        f2613a.put(Character.valueOf('T'), Character.valueOf('n'));
        f2613a.put(Character.valueOf('U'), Character.valueOf('V'));
        f2613a.put(Character.valueOf('V'), Character.valueOf('5'));
        f2613a.put(Character.valueOf('W'), Character.valueOf('k'));
        f2613a.put(Character.valueOf('X'), Character.valueOf('+'));
        f2613a.put(Character.valueOf('Y'), Character.valueOf('D'));
        f2613a.put(Character.valueOf('Z'), Character.valueOf('H'));
        f2613a.put(Character.valueOf('a'), Character.valueOf('L'));
        f2613a.put(Character.valueOf('b'), Character.valueOf('Y'));
        f2613a.put(Character.valueOf('c'), Character.valueOf('h'));
        f2613a.put(Character.valueOf('d'), Character.valueOf('J'));
        f2613a.put(Character.valueOf('e'), Character.valueOf('4'));
        f2613a.put(Character.valueOf('f'), Character.valueOf('6'));
        f2613a.put(Character.valueOf('g'), Character.valueOf('l'));
        f2613a.put(Character.valueOf('h'), Character.valueOf('t'));
        f2613a.put(Character.valueOf('i'), Character.valueOf('0'));
        f2613a.put(Character.valueOf('j'), Character.valueOf('U'));
        f2613a.put(Character.valueOf('k'), Character.valueOf('3'));
        f2613a.put(Character.valueOf('l'), Character.valueOf('Q'));
        f2613a.put(Character.valueOf('m'), Character.valueOf('r'));
        f2613a.put(Character.valueOf('n'), Character.valueOf('g'));
        f2613a.put(Character.valueOf('o'), Character.valueOf('E'));
        f2613a.put(Character.valueOf('p'), Character.valueOf('u'));
        f2613a.put(Character.valueOf('q'), Character.valueOf('q'));
        f2613a.put(Character.valueOf('r'), Character.valueOf('8'));
        f2613a.put(Character.valueOf('s'), Character.valueOf('s'));
        f2613a.put(Character.valueOf('t'), Character.valueOf('w'));
        f2613a.put(Character.valueOf('u'), Character.valueOf('/'));
        f2613a.put(Character.valueOf('v'), Character.valueOf('X'));
        f2613a.put(Character.valueOf('w'), Character.valueOf('M'));
        f2613a.put(Character.valueOf('x'), Character.valueOf('e'));
        f2613a.put(Character.valueOf('y'), Character.valueOf('B'));
        f2613a.put(Character.valueOf('z'), Character.valueOf('A'));
        f2613a.put(Character.valueOf('0'), Character.valueOf('T'));
        f2613a.put(Character.valueOf('1'), Character.valueOf('2'));
        f2613a.put(Character.valueOf('2'), Character.valueOf('F'));
        f2613a.put(Character.valueOf('3'), Character.valueOf('b'));
        f2613a.put(Character.valueOf('4'), Character.valueOf('9'));
        f2613a.put(Character.valueOf('5'), Character.valueOf('P'));
        f2613a.put(Character.valueOf('6'), Character.valueOf('1'));
        f2613a.put(Character.valueOf('7'), Character.valueOf('O'));
        f2613a.put(Character.valueOf('8'), Character.valueOf('I'));
        f2613a.put(Character.valueOf('9'), Character.valueOf('K'));
        f2613a.put(Character.valueOf('+'), Character.valueOf('m'));
        f2613a.put(Character.valueOf('/'), Character.valueOf('C'));
        for (int i = 0; i < b.length; i++) {
            c[i] = ((Character) f2613a.get(Character.valueOf(b[i]))).charValue();
        }
        for (int i2 = 0; i2 < d.length; i2++) {
            d[i2] = Byte.MAX_VALUE;
        }
        for (int i3 = 0; i3 < c.length; i3++) {
            d[c[i3]] = (byte) i3;
        }
    }

    private static int a(char[] cArr, byte[] bArr, int i) {
        try {
            char c2 = cArr[3] == '=' ? (char) 2 : 3;
            if (cArr[2] == '=') {
                c2 = 1;
            }
            byte b2 = d[cArr[0]];
            byte b3 = d[cArr[1]];
            byte b4 = d[cArr[2]];
            byte b5 = d[cArr[3]];
            switch (c2) {
                case 2:
                    int i2 = i + 1;
                    bArr[i] = (byte) ((3 & (b3 >> 4)) | ((b2 << 2) & 252));
                    bArr[i2] = (byte) (((b3 << 4) & PsExtractor.VIDEO_STREAM_MASK) | ((b4 >> 2) & 15));
                    return 2;
                case 3:
                    int i3 = i + 1;
                    bArr[i] = (byte) (((b2 << 2) & 252) | ((b3 >> 4) & 3));
                    int i4 = i3 + 1;
                    bArr[i3] = (byte) (((b3 << 4) & PsExtractor.VIDEO_STREAM_MASK) | ((b4 >> 2) & 15));
                    bArr[i4] = (byte) ((b5 & 63) | ((b4 << 6) & 192));
                    return 3;
                default:
                    bArr[i] = (byte) (((b2 << 2) & 252) | (3 & (b3 >> 4)));
                    return 1;
            }
        } catch (Exception unused) {
            return 0;
        }
    }

    public static String a(String str) {
        byte[] c2 = c(str);
        if (c2 == null || c2.length <= 0) {
            return null;
        }
        return new String(c2);
    }

    private static byte[] c(String str) {
        int i;
        try {
            int length = str.length();
            int i2 = 259;
            if (length < 259) {
                i2 = length;
            }
            char[] cArr = new char[i2];
            byte[] bArr = new byte[(((length >> 2) * 3) + 3)];
            int i3 = 0;
            int i4 = 0;
            int i5 = 0;
            while (i3 < length) {
                int i6 = i3 + 256;
                if (i6 <= length) {
                    str.getChars(i3, i6, cArr, i5);
                    i = i5 + 256;
                } else {
                    str.getChars(i3, length, cArr, i5);
                    i = (length - i3) + i5;
                }
                int i7 = i4;
                int i8 = i5;
                while (i5 < i) {
                    char c2 = cArr[i5];
                    if (c2 == '=' || (c2 < d.length && d[c2] != Byte.MAX_VALUE)) {
                        int i9 = i8 + 1;
                        cArr[i8] = c2;
                        if (i9 == 4) {
                            i7 += a(cArr, bArr, i7);
                            i8 = 0;
                        } else {
                            i8 = i9;
                        }
                    }
                    i5++;
                }
                i5 = i8;
                i3 = i6;
                i4 = i7;
            }
            if (i4 == bArr.length) {
                return bArr;
            }
            byte[] bArr2 = new byte[i4];
            System.arraycopy(bArr, 0, bArr2, 0, i4);
            return bArr2;
        } catch (Exception unused) {
            return null;
        }
    }

    public static String b(String str) {
        byte[] bytes = str.getBytes();
        return a(bytes, bytes.length);
    }

    private static String a(byte[] bArr, int i) {
        if (i <= 0) {
            return "";
        }
        try {
            char[] cArr = new char[(((i / 3) << 2) + 4)];
            int i2 = 0;
            int i3 = 0;
            while (i >= 3) {
                int i4 = ((bArr[i2] & 255) << 16) + ((bArr[i2 + 1] & 255) << 8) + (bArr[i2 + 2] & 255);
                int i5 = i3 + 1;
                cArr[i3] = c[i4 >> 18];
                int i6 = i5 + 1;
                cArr[i5] = c[(i4 >> 12) & 63];
                int i7 = i6 + 1;
                cArr[i6] = c[(i4 >> 6) & 63];
                i3 = i7 + 1;
                cArr[i7] = c[i4 & 63];
                i2 += 3;
                i -= 3;
            }
            if (i == 1) {
                byte b2 = bArr[i2] & 255;
                int i8 = i3 + 1;
                cArr[i3] = c[b2 >> 2];
                int i9 = i8 + 1;
                cArr[i8] = c[(b2 << 4) & 63];
                int i10 = i9 + 1;
                cArr[i9] = '=';
                i3 = i10 + 1;
                cArr[i10] = '=';
            } else if (i == 2) {
                int i11 = ((bArr[i2] & 255) << 8) + (bArr[i2 + 1] & 255);
                int i12 = i3 + 1;
                cArr[i3] = c[i11 >> 10];
                int i13 = i12 + 1;
                cArr[i12] = c[(i11 >> 4) & 63];
                int i14 = i13 + 1;
                cArr[i13] = c[(i11 << 2) & 63];
                i3 = i14 + 1;
                cArr[i14] = '=';
            }
            return new String(cArr, 0, i3);
        } catch (Exception unused) {
            return null;
        }
    }
}
