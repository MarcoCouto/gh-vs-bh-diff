package com.mintegral.msdk.base.utils;

import android.text.TextUtils;

/* compiled from: StringUtils */
public final class s {
    public static boolean a(String str) {
        return str == null || TextUtils.isEmpty(str.trim()) || "null".equals(str);
    }

    public static boolean b(String str) {
        return str != null && !TextUtils.isEmpty(str.trim()) && !"null".equals(str);
    }
}
