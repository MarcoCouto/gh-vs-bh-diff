package com.mintegral.msdk.base.entity;

import java.util.Set;

/* compiled from: InstallApp */
public final class g {

    /* renamed from: a reason: collision with root package name */
    private String f2596a;
    private String b;

    public g() {
    }

    public g(String str, String str2) {
        this.f2596a = str;
        this.b = str2;
    }

    public final String a() {
        return this.f2596a;
    }

    public final void a(String str) {
        this.f2596a = str;
    }

    public final String b() {
        return this.b;
    }

    public final void b(String str) {
        this.b = str;
    }

    public static String a(Set<g> set) {
        if (set != null) {
            try {
                if (set.size() > 0) {
                    String str = "[{$native_info}]";
                    StringBuffer stringBuffer = new StringBuffer();
                    for (g gVar : set) {
                        stringBuffer.append("{\"campaignId\":");
                        StringBuilder sb = new StringBuilder();
                        sb.append(gVar.f2596a);
                        sb.append(",");
                        stringBuffer.append(sb.toString());
                        stringBuffer.append("\"packageName\":");
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append(gVar.b);
                        sb2.append("},");
                        stringBuffer.append(sb2.toString());
                    }
                    return str.replace("{$native_info}", stringBuffer.subSequence(0, stringBuffer.lastIndexOf(",")));
                }
            } catch (Exception unused) {
            }
        }
        return null;
    }

    public final int hashCode() {
        int i;
        int i2 = 0;
        if (this.f2596a == null) {
            i = 0;
        } else {
            i = this.f2596a.hashCode();
        }
        int i3 = (i + 31) * 31;
        if (this.b != null) {
            i2 = this.b.hashCode();
        }
        return i3 + i2;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        g gVar = (g) obj;
        if (this.f2596a == null) {
            if (gVar.f2596a != null) {
                return false;
            }
        } else if (!this.f2596a.equals(gVar.f2596a)) {
            return false;
        }
        if (this.b == null) {
            if (gVar.b != null) {
                return false;
            }
        } else if (!this.b.equals(gVar.b)) {
            return false;
        }
        return true;
    }
}
