package com.mintegral.msdk.base.entity;

/* compiled from: OfferWallClick */
public final class j {

    /* renamed from: a reason: collision with root package name */
    private String f2599a;
    private String b;
    private long c;
    private String d;
    private String e;
    private String f;
    private int g;

    public j() {
    }

    public j(String str, String str2, String str3, long j, String str4, String str5, int i) {
        this.f = str;
        this.f2599a = str2;
        this.b = str3;
        this.c = j;
        this.d = str4;
        this.e = str5;
        this.g = i;
    }

    public final String a() {
        return this.b;
    }

    public final String b() {
        return this.e;
    }
}
