package com.mintegral.msdk.base.entity;

/* compiled from: LoadTime */
public final class h {

    /* renamed from: a reason: collision with root package name */
    private int f2597a;
    private String b;
    private int c;
    private String d;
    private int e;
    private int f;
    private int g;
    private String h;

    public h(int i, String str, int i2, String str2, int i3, int i4, int i5) {
        this.f2597a = i;
        this.b = str;
        this.c = i2;
        this.d = str2;
        this.e = i3;
        this.f = i4;
        this.g = i5;
    }

    public h() {
    }

    public final int a() {
        return this.f2597a;
    }

    public final void b() {
        this.f2597a = 1;
    }

    public final String c() {
        return this.b;
    }

    public final void a(String str) {
        this.b = str;
    }

    public final int d() {
        return this.c;
    }

    public final void a(int i) {
        this.c = i;
    }

    public final String e() {
        return this.d;
    }

    public final void b(String str) {
        this.d = str;
    }

    public final int f() {
        return this.e;
    }

    public final void b(int i) {
        this.e = i;
    }

    public final int g() {
        return this.f;
    }

    public final void c(int i) {
        this.f = i;
    }

    public final int h() {
        return this.g;
    }

    public final void d(int i) {
        this.g = i;
    }

    public final String i() {
        return this.h;
    }

    public final void c(String str) {
        this.h = str;
    }
}
