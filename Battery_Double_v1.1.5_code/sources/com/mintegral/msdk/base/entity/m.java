package com.mintegral.msdk.base.entity;

/* compiled from: PInfo */
public final class m {

    /* renamed from: a reason: collision with root package name */
    private String f2602a;
    private int b;
    private long c;
    private String d;

    public final String a() {
        return this.f2602a;
    }

    public final void a(String str) {
        this.f2602a = str;
    }

    public final int b() {
        return this.b;
    }

    public final void a(int i) {
        this.b = i;
    }

    public final long c() {
        return this.c;
    }

    public final void a(long j) {
        this.c = j;
    }

    public final String d() {
        return this.d;
    }

    public final void b(String str) {
        this.d = str;
    }
}
