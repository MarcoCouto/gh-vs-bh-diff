package com.mintegral.msdk.base.entity;

import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.base.controller.authoritycontroller.a;
import com.mintegral.msdk.base.utils.g;
import java.net.URLEncoder;

/* compiled from: DevDataEntity */
public final class c {
    public static String a(int i, String str) {
        try {
            StringBuffer stringBuffer = new StringBuffer();
            a.a();
            if (!a.a(MIntegralConstans.AUTHORITY_APPLIST)) {
                return null;
            }
            StringBuilder sb = new StringBuilder("app_num=");
            sb.append(i);
            stringBuffer.append(sb.toString());
            if (str == null) {
                str = "";
            }
            StringBuilder sb2 = new StringBuilder("&install_id=");
            sb2.append(str);
            stringBuffer.append(sb2.toString());
            String b = com.mintegral.msdk.base.utils.a.b(stringBuffer.toString());
            StringBuffer stringBuffer2 = new StringBuffer();
            stringBuffer2.append("key=2000032");
            StringBuilder sb3 = new StringBuilder("&content=");
            sb3.append(URLEncoder.encode(b, "utf-8"));
            stringBuffer2.append(sb3.toString());
            String stringBuffer3 = stringBuffer2.toString();
            StringBuilder sb4 = new StringBuilder("devDataStr:");
            sb4.append(stringBuffer3);
            g.d("DevDataEntity", sb4.toString());
            return stringBuffer3;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
