package com.mintegral.msdk.base.entity;

import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.base.controller.authoritycontroller.a;
import java.util.List;

/* compiled from: ClickTime */
public final class b {

    /* renamed from: a reason: collision with root package name */
    private String f2592a;
    private String b;
    private String c;
    private String d;
    private int e;
    private String f;
    private String g;
    private String h;
    private int i;
    private int j;
    private String k;
    private int l;
    private int m;
    private String n;
    private int o;
    private String p;
    private int q;

    public b() {
    }

    public b(String str, String str2, String str3, String str4, int i2, String str5, String str6, String str7, int i3, int i4, String str8, int i5, int i6, String str9, int i7, int i8, String str10) {
        this.f2592a = str;
        this.b = str2;
        this.c = str3;
        this.d = str4;
        this.e = i2;
        this.f = str5;
        this.g = str6;
        this.h = str7;
        this.i = i3;
        this.j = i4;
        this.k = str8;
        this.l = i5;
        this.m = i6;
        this.n = str9;
        this.o = i7;
        this.p = str10;
        this.q = i8;
    }

    public final int a() {
        return this.o;
    }

    public final void a(int i2) {
        this.o = i2;
    }

    public final String b() {
        return this.p;
    }

    public final void a(String str) {
        this.p = str;
    }

    public final String c() {
        return this.k;
    }

    public final void b(String str) {
        this.k = str;
    }

    public final int d() {
        return this.l;
    }

    public final void b(int i2) {
        this.l = i2;
    }

    public final int e() {
        return this.m;
    }

    public final void c(int i2) {
        this.m = i2;
    }

    public final String f() {
        return this.n;
    }

    public final void c(String str) {
        this.n = str;
    }

    public final int g() {
        return this.j;
    }

    public final void d(int i2) {
        this.j = 1;
    }

    public final String h() {
        return this.f;
    }

    public final void d(String str) {
        this.f = str;
    }

    public final int i() {
        return this.e;
    }

    public final void e(int i2) {
        this.e = i2;
    }

    public final void e(String str) {
        this.g = str;
    }

    public final void f(String str) {
        this.h = str;
    }

    public final int j() {
        return this.i;
    }

    public final void f(int i2) {
        this.i = i2;
    }

    public final String k() {
        return this.d;
    }

    public final void g(String str) {
        this.d = str;
    }

    public final String l() {
        return this.b;
    }

    public final void h(String str) {
        this.b = str;
    }

    public final String m() {
        return this.c;
    }

    public final void i(String str) {
        this.c = str;
    }

    public final String n() {
        return this.f2592a;
    }

    public final void j(String str) {
        this.f2592a = str;
    }

    public static String a(List<b> list) {
        if (list == null || list.size() <= 0) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        for (b bVar : list) {
            a.a();
            if (a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA)) {
                StringBuilder sb2 = new StringBuilder("rid_n=");
                sb2.append(bVar.f2592a);
                sb.append(sb2.toString());
                StringBuilder sb3 = new StringBuilder("&network_type=");
                sb3.append(bVar.o);
                sb.append(sb3.toString());
                StringBuilder sb4 = new StringBuilder("&network_str=");
                sb4.append(bVar.p);
                sb.append(sb4.toString());
                StringBuilder sb5 = new StringBuilder("&cid=");
                sb5.append(bVar.b);
                sb.append(sb5.toString());
                StringBuilder sb6 = new StringBuilder("&click_type=");
                sb6.append(bVar.j);
                sb.append(sb6.toString());
                StringBuilder sb7 = new StringBuilder("&type=");
                sb7.append(bVar.i);
                sb.append(sb7.toString());
                StringBuilder sb8 = new StringBuilder("&click_duration=");
                sb8.append(bVar.c);
                sb.append(sb8.toString());
                sb.append("&key=2000013");
                StringBuilder sb9 = new StringBuilder("&unit_id=");
                sb9.append(bVar.k);
                sb.append(sb9.toString());
                StringBuilder sb10 = new StringBuilder("&last_url=");
                sb10.append(bVar.d);
                sb.append(sb10.toString());
                StringBuilder sb11 = new StringBuilder("&content=");
                sb11.append(bVar.h);
                sb.append(sb11.toString());
                StringBuilder sb12 = new StringBuilder("&code=");
                sb12.append(bVar.e);
                sb.append(sb12.toString());
                StringBuilder sb13 = new StringBuilder("&exception=");
                sb13.append(bVar.f);
                sb.append(sb13.toString());
                StringBuilder sb14 = new StringBuilder("&header=");
                sb14.append(bVar.g);
                sb.append(sb14.toString());
                StringBuilder sb15 = new StringBuilder("&landing_type=");
                sb15.append(bVar.l);
                sb.append(sb15.toString());
                StringBuilder sb16 = new StringBuilder("&link_type=");
                sb16.append(bVar.m);
                sb.append(sb16.toString());
                StringBuilder sb17 = new StringBuilder("&click_time=");
                sb17.append(bVar.n);
                sb17.append("\n");
                sb.append(sb17.toString());
            } else {
                StringBuilder sb18 = new StringBuilder("rid_n=");
                sb18.append(bVar.f2592a);
                sb.append(sb18.toString());
                StringBuilder sb19 = new StringBuilder("&cid=");
                sb19.append(bVar.b);
                sb.append(sb19.toString());
                StringBuilder sb20 = new StringBuilder("&click_type=");
                sb20.append(bVar.j);
                sb.append(sb20.toString());
                StringBuilder sb21 = new StringBuilder("&type=");
                sb21.append(bVar.i);
                sb.append(sb21.toString());
                StringBuilder sb22 = new StringBuilder("&click_duration=");
                sb22.append(bVar.c);
                sb.append(sb22.toString());
                sb.append("&key=2000013");
                StringBuilder sb23 = new StringBuilder("&unit_id=");
                sb23.append(bVar.k);
                sb.append(sb23.toString());
                StringBuilder sb24 = new StringBuilder("&last_url=");
                sb24.append(bVar.d);
                sb.append(sb24.toString());
                StringBuilder sb25 = new StringBuilder("&content=");
                sb25.append(bVar.h);
                sb.append(sb25.toString());
                StringBuilder sb26 = new StringBuilder("&code=");
                sb26.append(bVar.e);
                sb.append(sb26.toString());
                StringBuilder sb27 = new StringBuilder("&exception=");
                sb27.append(bVar.f);
                sb.append(sb27.toString());
                StringBuilder sb28 = new StringBuilder("&header=");
                sb28.append(bVar.g);
                sb.append(sb28.toString());
                StringBuilder sb29 = new StringBuilder("&landing_type=");
                sb29.append(bVar.l);
                sb.append(sb29.toString());
                StringBuilder sb30 = new StringBuilder("&link_type=");
                sb30.append(bVar.m);
                sb.append(sb30.toString());
                StringBuilder sb31 = new StringBuilder("&click_time=");
                sb31.append(bVar.n);
                sb31.append("\n");
                sb.append(sb31.toString());
            }
        }
        return sb.toString();
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder("ClickTime [campaignId=");
        sb.append(this.b);
        sb.append(", click_duration=");
        sb.append(this.c);
        sb.append(", lastUrl=");
        sb.append(this.d);
        sb.append(", code=");
        sb.append(this.e);
        sb.append(", excepiton=");
        sb.append(this.f);
        sb.append(", header=");
        sb.append(this.g);
        sb.append(", content=");
        sb.append(this.h);
        sb.append(", type=");
        sb.append(this.i);
        sb.append(", click_type=");
        sb.append(this.j);
        sb.append(RequestParameters.RIGHT_BRACKETS);
        return sb.toString();
    }
}
