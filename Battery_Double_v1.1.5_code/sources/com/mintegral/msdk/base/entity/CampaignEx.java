package com.mintegral.msdk.base.entity;

import android.net.Uri;
import android.text.TextUtils;
import com.facebook.share.internal.MessengerShareContentUtility;
import com.integralads.avid.library.inmobi.utils.AvidJSONUtil;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.base.utils.k;
import com.mintegral.msdk.base.utils.s;
import com.mintegral.msdk.click.CommonJumpLoader.JumpLoaderResult;
import com.mintegral.msdk.out.Campaign;
import com.mintegral.msdk.system.NoProGuard;
import com.vungle.warren.model.AdvertisementDBAdapter.AdvertisementColumns;
import java.io.Serializable;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class CampaignEx extends Campaign implements NoProGuard, Serializable {
    public static final int CAMPAIN_NV_T2_VALUE_3 = 3;
    public static final int CAMPAIN_NV_T2_VALUE_4 = 4;
    public static final String CLICKMODE_ON = "5";
    public static final int CLICK_TIMEOUT_INTERVAL_DEFAULT_VALUE = 2;
    public static final int C_UA_DEFAULT_VALUE = 1;
    public static final String ENDCARD_URL = "endcard_url";
    public static final int FLAG_DEFAULT_SPARE_OFFER = -1;
    public static final int FLAG_IS_SPARE_OFFER = 1;
    public static final int FLAG_NOT_SPARE_OFFER = 0;
    public static final int IMP_UA_DEFAULT_VALUE = 1;
    public static final String JSON_AD_IMP_KEY = "sec";
    public static final String JSON_AD_IMP_VALUE = "url";
    public static final String JSON_KEY_ADVIMP = "adv_imp";
    public static final String JSON_KEY_ADV_ID = "adv_id";
    public static final String JSON_KEY_AD_AKS = "aks";
    public static final String JSON_KEY_AD_AL = "al";
    public static final String JSON_KEY_AD_K = "k";
    public static final String JSON_KEY_AD_MP = "mp";
    public static final String JSON_KEY_AD_Q = "q";
    public static final String JSON_KEY_AD_R = "r";
    public static final String JSON_KEY_AD_SOURCE_ID = "ad_source_id";
    public static final String JSON_KEY_AD_TRACKING_APK_END = "apk_download_end";
    public static final String JSON_KEY_AD_TRACKING_APK_INSTALL = "apk_install";
    public static final String JSON_KEY_AD_TRACKING_APK_START = "apk_download_start";
    public static final String JSON_KEY_AD_TRACKING_DROPOUT_TRACK = "dropout_track";
    public static final String JSON_KEY_AD_TRACKING_IMPRESSION_T2 = "impression_t2";
    public static final String JSON_KEY_AD_TRACKING_PLYCMPT_TRACK = "plycmpt_track";
    public static final String JSON_KEY_AD_URL_LIST = "ad_url_list";
    public static final String JSON_KEY_APP_SIZE = "app_size";
    public static final String JSON_KEY_BTY = "ctype";
    public static final String JSON_KEY_CAMPAIGN_UNITID = "unitId";
    public static final String JSON_KEY_CLICK_INTERVAL = "c_ct";
    public static final String JSON_KEY_CLICK_MODE = "click_mode";
    public static final String JSON_KEY_CLICK_TIMEOUT_INTERVAL = "c_toi";
    public static final String JSON_KEY_CLICK_URL = "click_url";
    public static final String JSON_KEY_CTA_TEXT = "ctatext";
    public static final String JSON_KEY_C_UA = "c_ua";
    public static final String JSON_KEY_DEEP_LINK_URL = "deep_link";
    public static final String JSON_KEY_DESC = "desc";
    public static final String JSON_KEY_ENDCARD_CLICK = "endcard_click_result";
    public static final String JSON_KEY_FCA = "fca";
    public static final String JSON_KEY_FCB = "fcb";
    public static final String JSON_KEY_GIF_URL = "gif_url";
    public static final String JSON_KEY_GUIDELINES = "guidelines";
    public static final String JSON_KEY_ICON_URL = "icon_url";
    public static final String JSON_KEY_ID = "id";
    public static final String JSON_KEY_IMAGE_SIZE = "image_size";
    public static final String JSON_KEY_IMAGE_URL = "image_url";
    public static final String JSON_KEY_IMPRESSION_URL = "impression_url";
    public static final String JSON_KEY_IMP_UA = "imp_ua";
    public static final String JSON_KEY_JM_PD = "jm_pd";
    public static final String JSON_KEY_LANDING_TYPE = "landing_type";
    public static final String JSON_KEY_LINK_TYPE = "link_type";
    public static final String JSON_KEY_MRAID = "mraid";
    public static final String JSON_KEY_NOTICE_URL = "notice_url";
    public static final String JSON_KEY_NV_T2 = "nv_t2";
    public static final String JSON_KEY_OFFER_TYPE = "offer_type";
    public static final String JSON_KEY_PACKAGE_NAME = "package_name";
    public static final String JSON_KEY_PLCT = "plct";
    public static final String JSON_KEY_PLCTB = "plctb";
    public static final String JSON_KEY_PRE_CLICK = "ttc";
    public static final String JSON_KEY_PRE_CLICK_ERROR_INTERVAL = "ttc_pe";
    public static final String JSON_KEY_PRE_CLICK_INTERVAL = "ttc_ct";
    public static final String JSON_KEY_PRE_CLICK_OTHER_INTERVAL = "ttc_po";
    public static final String JSON_KEY_PUB_IMP = "pub_imp";
    public static final String JSON_KEY_RETARGET_OFFER = "retarget_offer";
    public static final String JSON_KEY_REWARD_AMOUNT = "reward_amount";
    public static final String JSON_KEY_REWARD_NAME = "reward_name";
    public static final String JSON_KEY_REWARD_TEMPLATE = "rv";
    public static final String JSON_KEY_REWARD_VIDEO_MD5 = "md5_file";
    public static final String JSON_KEY_STAR = "rating";
    public static final String JSON_KEY_ST_IEX = "iex";
    public static final String JSON_KEY_ST_TS = "ts";
    public static final String JSON_KEY_TEMPLATE = "template";
    public static final String JSON_KEY_TITLE = "title";
    public static final String JSON_KEY_TTC_CT2 = "ttc_ct2";
    public static final String JSON_KEY_TTC_TYPE = "ttc_type";
    public static final String JSON_KEY_T_IMP = "t_imp";
    public static final String JSON_KEY_VIDEO_LENGTHL = "video_length";
    public static final String JSON_KEY_VIDEO_RESOLUTION = "video_resolution";
    public static final String JSON_KEY_VIDEO_SIZE = "video_size";
    public static final String JSON_KEY_VIDEO_URL = "video_url";
    public static final String JSON_KEY_WATCH_MILE = "watch_mile";
    public static final String JSON_NATIVE_VIDEO_AD_TRACKING = "ad_tracking";
    public static final String JSON_NATIVE_VIDEO_CLICK = "click";
    public static final String JSON_NATIVE_VIDEO_CLOSE = "close";
    public static final String JSON_NATIVE_VIDEO_COMPLETE = "complete";
    public static final String JSON_NATIVE_VIDEO_ENDCARD = "endcard";
    public static final String JSON_NATIVE_VIDEO_ENDCARD_SHOW = "endcard_show";
    public static final String JSON_NATIVE_VIDEO_ERROR = "error";
    public static final String JSON_NATIVE_VIDEO_FIRST_QUARTILE = "first_quartile";
    public static final String JSON_NATIVE_VIDEO_MIDPOINT = "midpoint";
    public static final String JSON_NATIVE_VIDEO_MUTE = "mute";
    public static final String JSON_NATIVE_VIDEO_PAUSE = "pause";
    public static final String JSON_NATIVE_VIDEO_PLAY_PERCENTAGE = "play_percentage";
    public static final String JSON_NATIVE_VIDEO_RESUME = "resume";
    public static final String JSON_NATIVE_VIDEO_START = "start";
    public static final String JSON_NATIVE_VIDEO_THIRD_QUARTILE = "third_quartile";
    public static final String JSON_NATIVE_VIDEO_UNMUTE = "unmute";
    public static final String JSON_NATIVE_VIDEO_VIDEO_CLICK = "video_click";
    public static final String JSON_NATIVE_VIDOE_IMPRESSION = "impression";
    public static final String KET_ADCHOICE = "adchoice";
    public static final String KEY_AD_TYPE = "ad_type";
    public static final String KEY_BIND_ID = "bind_id";
    public static final String KEY_GH_ID = "gh_id";
    public static final String KEY_GH_PATH = "gh_path";
    public static final String KEY_IA_CACHE = "ia_cache";
    public static final String KEY_IA_EXT1 = "ia_ext1";
    public static final String KEY_IA_EXT2 = "ia_ext2";
    public static final String KEY_IA_ICON = "ia_icon";
    public static final String KEY_IA_ORI = "ia_ori";
    public static final String KEY_IA_RST = "ia_rst";
    public static final String KEY_IA_URL = "ia_url";
    public static final String KEY_IS_DOWNLOAD = "is_download_zip";
    public static final String KEY_OC_TIME = "oc_time";
    public static final String KEY_OC_TYPE = "oc_type";
    public static final String KEY_T_LIST = "t_list";
    public static final int LANDING_TYPE_VALUE_OPEN_BROWSER = 1;
    public static final int LANDING_TYPE_VALUE_OPEN_GP_BY_PACKAGE = 3;
    public static final int LANDING_TYPE_VALUE_OPEN_WEBVIEW = 2;
    public static final int LINK_TYPE_1 = 1;
    public static final int LINK_TYPE_2 = 2;
    public static final int LINK_TYPE_3 = 3;
    public static final int LINK_TYPE_4 = 4;
    public static final int LINK_TYPE_8 = 8;
    public static final int LINK_TYPE_9 = 9;
    public static final String LOOPBACK = "loopback";
    public static final String LOOPBACK_DOMAIN = "domain";
    public static final String LOOPBACK_KEY = "key";
    public static final String LOOPBACK_VALUE = "value";
    public static final String PLAYABLE_ADS_WITHOUT_VIDEO = "playable_ads_without_video";
    public static final int PLAYABLE_ADS_WITHOUT_VIDEO_DEFAULT = 1;
    public static final int PLAYABLE_ADS_WITHOUT_VIDEO_ENDCARD = 2;
    public static final int RETAR_GETING_IS = 1;
    public static final int RETAR_GETING_NOT = 2;
    public static final String ROVER_KEY_IS_POST = "isPost";
    public static final String ROVER_KEY_MARK = "mark";
    public static final String TAG = "CampaignEx";
    public static final int TTC_CT2_DEFAULT_VALUE = 1800;
    public static final int TTC_CT_DEFAULT_VALUE = 604800;
    public static final String VIDEO_END_TYPE = "video_end_type";
    public static final int VIDEO_END_TYPE_BROWSER = 5;
    public static final int VIDEO_END_TYPE_DEFAULT = 2;
    public static final int VIDEO_END_TYPE_FINISH = 1;
    public static final int VIDEO_END_TYPE_REULSE = 2;
    public static final int VIDEO_END_TYPE_VAST = 3;
    public static final int VIDEO_END_TYPE_WEBVIEW = 4;
    private static final long serialVersionUID = 1;
    private int adType;
    private String ad_url_list;
    private a adchoice;
    private String advId;
    private String advImp;
    private HashMap<String, String> aks;
    private String al;
    private String bidToken = "";
    private String bindId;
    private int bty;
    private int cUA = 1;
    private int cacheLevel;
    private String campaignUnitId;
    private int clickInterval;
    private int clickTimeOutInterval = 2;
    private String clickURL = "";
    private String click_mode;
    private String deepLinkUrl = "";
    private String endScreenUrl;
    private int endcard_click_result;
    private String endcard_url;
    private int fca;
    private int fcb;
    private String ghId;
    private String ghPath;
    private String gifUrl;
    private String guidelines;
    private boolean hasReportAdTrackPause = false;
    private String htmlUrl;
    private String ia_ext1;
    private String ia_ext2;
    private int iex;
    private String imageSize = "";
    private int impUA = 1;
    private String impressionURL = "";
    private String interactiveCache;
    private int isAddSuccesful;
    private boolean isBidCampaign;
    private int isClick;
    private int isDeleted;
    private int isDownLoadZip;
    private boolean isMraid;
    private boolean isReport;
    private boolean isReportClick;
    private int jmPd;
    private JumpLoaderResult jumpResult;
    private String k;
    private String keyIaIcon;
    private int keyIaOri;
    private int keyIaRst;
    private String keyIaUrl;
    private String label;
    private String landingType;
    private int linkType;
    private Map<String, String> loopbackMap;
    private String loopbackString;
    private b mediaViewHolder;
    private String mp;
    private String mraid;
    private i nativeVideoTracking;
    private String nativeVideoTrackingString;
    private String noticeUrl = "";
    private int nvT2 = 6;
    private int oc_time;
    private int oc_type = 0;
    private int offerType;
    private String onlyImpressionURL = "";
    private String pkgSource;
    private int playable_ads_without_video = 1;
    private long plct = 0;
    private long plctb = 0;
    private boolean preClick = false;
    private int preClickInterval;
    private String q;
    private String r;
    private String requestId;
    private String requestIdNotice;
    private int retarget_offer;
    private int rewardAmount;
    private int rewardPlayStatus;
    private c rewardTemplateMode;
    private String reward_name;
    private int roverIsPost;
    private String roverMark;
    private int spareOfferFlag = -1;
    private int t_imp;
    private String t_list;
    private int tab = -1;
    private int template;
    private long ts;
    private int ttc_ct2;
    private int ttc_type;
    private int videoLength;
    public String videoMD5Value = "";
    private String videoResolution;
    private int videoSize;
    private String videoUrlEncode = "";
    private int video_end_type = 2;
    private int watchMile;

    public static final class a implements Serializable {

        /* renamed from: a reason: collision with root package name */
        private String f2587a = "";
        private String b = "";
        private String c = "";
        private String d = "";
        private String e = "";
        private String f = "";
        private String g = "";
        private String h = "";
        private int i = 0;
        private int j = 0;
        private String k = "";

        public final int a() {
            return this.i;
        }

        public final int b() {
            return this.j;
        }

        public final String c() {
            return this.k;
        }

        public final String d() {
            return this.b;
        }

        public final String e() {
            return this.c;
        }

        public final String f() {
            return this.d;
        }

        public static a a(String str) {
            try {
                if (TextUtils.isEmpty(str)) {
                    return null;
                }
                return a(new JSONObject(str));
            } catch (Exception e2) {
                if (MIntegralConstans.DEBUG) {
                    e2.printStackTrace();
                }
                return null;
            } catch (Throwable th) {
                if (MIntegralConstans.DEBUG) {
                    th.printStackTrace();
                }
                return null;
            }
        }

        /* JADX WARNING: Removed duplicated region for block: B:12:0x0063  */
        private static a a(JSONObject jSONObject) {
            a aVar;
            try {
                aVar = new a();
                try {
                    aVar.c = jSONObject.optString("adchoice_icon");
                    aVar.b = jSONObject.optString("adchoice_link");
                    String optString = jSONObject.optString("adchoice_size");
                    aVar.d = optString;
                    aVar.f2587a = jSONObject.optString("ad_logo_link");
                    aVar.h = jSONObject.optString("adv_logo");
                    aVar.g = jSONObject.optString("adv_name");
                    aVar.f = jSONObject.optString("platform_logo");
                    aVar.e = jSONObject.optString("platform_name");
                    aVar.j = b(optString);
                    aVar.i = c(optString);
                    aVar.k = jSONObject.toString();
                } catch (Exception e2) {
                    e = e2;
                } catch (Throwable th) {
                    th = th;
                    if (MIntegralConstans.DEBUG) {
                    }
                    return aVar;
                }
            } catch (Exception e3) {
                e = e3;
                aVar = null;
                if (MIntegralConstans.DEBUG) {
                    e.printStackTrace();
                }
                return aVar;
            } catch (Throwable th2) {
                th = th2;
                aVar = null;
                if (MIntegralConstans.DEBUG) {
                    th.printStackTrace();
                }
                return aVar;
            }
            return aVar;
        }

        private static int b(String str) {
            if (TextUtils.isEmpty(str)) {
                return 0;
            }
            try {
                if (str.contains(AvidJSONUtil.KEY_X)) {
                    String[] split = str.split(AvidJSONUtil.KEY_X);
                    if (split != null && split.length > 1) {
                        return Integer.parseInt(split[1]);
                    }
                }
                return 0;
            } catch (NumberFormatException unused) {
                return 0;
            } catch (Exception unused2) {
                return 0;
            }
        }

        private static int c(String str) {
            if (TextUtils.isEmpty(str)) {
                return 0;
            }
            try {
                if (str.contains(AvidJSONUtil.KEY_X)) {
                    String[] split = str.split(AvidJSONUtil.KEY_X);
                    if (split != null && split.length > 0) {
                        return Integer.parseInt(split[0]);
                    }
                }
                return 0;
            } catch (NumberFormatException unused) {
                return 0;
            } catch (Exception unused2) {
                return 0;
            }
        }
    }

    public static final class b implements Serializable {

        /* renamed from: a reason: collision with root package name */
        public boolean f2588a = false;
        public boolean b = false;
        public boolean c = false;
        public boolean d = false;
        public boolean e = false;
        public boolean f = false;
        public boolean g = false;
        public boolean h = false;
        public boolean i = false;
        public boolean j = false;
        public boolean k = false;
        public Map<Integer, String> l;
    }

    public static final class c implements Serializable {

        /* renamed from: a reason: collision with root package name */
        private String f2589a;
        private int b;
        private int c;
        private String d;
        private String e;
        private List<a> f;

        public static final class a implements Serializable {

            /* renamed from: a reason: collision with root package name */
            public String f2590a;
            public List<String> b = new ArrayList();
        }

        private c(String str) {
            this.f2589a = str;
        }

        public final String a() {
            return this.f2589a;
        }

        public final int b() {
            return this.c;
        }

        public final String c() {
            return this.d;
        }

        public final String d() {
            return this.e;
        }

        public final List<a> e() {
            return this.f;
        }

        public static c a(String str) {
            try {
                if (s.b(str)) {
                    return a(new JSONObject(str));
                }
            } catch (Throwable th) {
                g.c(CampaignEx.TAG, th.getMessage(), th);
            }
            return null;
        }

        public static c a(JSONObject jSONObject) {
            if (jSONObject != null) {
                try {
                    if (s.b(jSONObject.toString())) {
                        c cVar = new c(jSONObject.toString());
                        cVar.b = jSONObject.optInt("video_template", 1);
                        cVar.e = jSONObject.optString(AdvertisementColumns.COLUMN_TEMPLATE_URL);
                        cVar.c = jSONObject.optInt("orientation");
                        cVar.d = jSONObject.optString("paused_url");
                        JSONObject optJSONObject = jSONObject.optJSONObject(MessengerShareContentUtility.MEDIA_IMAGE);
                        if (optJSONObject != null) {
                            ArrayList arrayList = new ArrayList();
                            Iterator keys = optJSONObject.keys();
                            while (keys != null && keys.hasNext()) {
                                String str = (String) keys.next();
                                List b2 = k.b(optJSONObject.optJSONArray(str));
                                if (b2 != null && b2.size() > 0) {
                                    a aVar = new a();
                                    aVar.f2590a = str;
                                    aVar.b.addAll(b2);
                                    arrayList.add(aVar);
                                }
                            }
                            cVar.f = arrayList;
                        }
                        return cVar;
                    }
                } catch (Throwable th) {
                    g.c(CampaignEx.TAG, th.getMessage(), th);
                }
            }
            return null;
        }
    }

    public int getSpareOfferFlag() {
        return this.spareOfferFlag;
    }

    public void setSpareOfferFlag(int i) {
        this.spareOfferFlag = i;
    }

    public long getPlct() {
        return this.plct;
    }

    public void setPlct(long j) {
        this.plct = j;
    }

    public long getPlctb() {
        return this.plctb;
    }

    public void setPlctb(long j) {
        this.plctb = j;
    }

    public a getAdchoice() {
        return this.adchoice;
    }

    public void setAdchoice(a aVar) {
        this.adchoice = aVar;
    }

    public int getOc_type() {
        return this.oc_type;
    }

    public void setOc_type(int i) {
        this.oc_type = i;
    }

    public int getOc_time() {
        return this.oc_time;
    }

    public void setOc_time(int i) {
        this.oc_time = i;
    }

    public String getT_list() {
        return this.t_list;
    }

    public void setT_list(String str) {
        this.t_list = str;
    }

    public String getGhId() {
        return this.ghId;
    }

    public void setGhId(String str) {
        this.ghId = str;
    }

    public String getGhPath() {
        return this.ghPath;
    }

    public void setGhPath(String str) {
        this.ghPath = str;
    }

    public String getBindId() {
        return this.bindId;
    }

    public void setBindId(String str) {
        this.bindId = str;
    }

    public String getInteractiveCache() {
        return this.interactiveCache;
    }

    public void setInteractiveCache(String str) {
        this.interactiveCache = str;
    }

    public int getIsDownLoadZip() {
        return this.isDownLoadZip;
    }

    public void setIsDownLoadZip(int i) {
        this.isDownLoadZip = i;
    }

    public int getAdType() {
        return this.adType;
    }

    public void setAdType(int i) {
        this.adType = i;
    }

    public String getIa_ext1() {
        return this.ia_ext1;
    }

    public void setIa_ext1(String str) {
        this.ia_ext1 = str;
    }

    public String getIa_ext2() {
        return this.ia_ext2;
    }

    public void setIa_ext2(String str) {
        this.ia_ext2 = str;
    }

    public String getKeyIaIcon() {
        return this.keyIaIcon;
    }

    public void setKeyIaIcon(String str) {
        this.keyIaIcon = str;
    }

    public int getKeyIaRst() {
        return this.keyIaRst;
    }

    public void setKeyIaRst(int i) {
        this.keyIaRst = i;
    }

    public String getKeyIaUrl() {
        return this.keyIaUrl;
    }

    public void setKeyIaUrl(String str) {
        this.keyIaUrl = str;
    }

    public int getKeyIaOri() {
        return this.keyIaOri;
    }

    public void setKeyIaOri(int i) {
        this.keyIaOri = i;
    }

    public int getIsAddSuccesful() {
        return this.isAddSuccesful;
    }

    public void setIsAddSuccesful(int i) {
        this.isAddSuccesful = i;
    }

    public int getJmPd() {
        return this.jmPd;
    }

    public int getIsDeleted() {
        return this.isDeleted;
    }

    public void setIsDeleted(int i) {
        this.isDeleted = i;
    }

    public int getIsClick() {
        return this.isClick;
    }

    public void setIsClick(int i) {
        this.isClick = i;
    }

    public void setJmPd(int i) {
        this.jmPd = i;
    }

    public int getNvT2() {
        return this.nvT2;
    }

    public void setNvT2(int i) {
        this.nvT2 = i;
    }

    public String getGifUrl() {
        return this.gifUrl;
    }

    public void setGifUrl(String str) {
        this.gifUrl = str;
    }

    public HashMap<String, String> getAks() {
        return this.aks;
    }

    public void setAks(HashMap<String, String> hashMap) {
        this.aks = hashMap;
    }

    public String getK() {
        return this.k;
    }

    public void setK(String str) {
        this.k = str;
    }

    public String getQ() {
        return this.q;
    }

    public void setQ(String str) {
        this.q = str;
    }

    public String getR() {
        return this.r;
    }

    public void setR(String str) {
        this.r = str;
    }

    public String getAl() {
        return this.al;
    }

    public void setAl(String str) {
        this.al = str;
    }

    public String getMp() {
        return this.mp;
    }

    public void setMp(String str) {
        this.mp = str;
    }

    public boolean isBidCampaign() {
        return this.isBidCampaign;
    }

    public void setIsBidCampaign(boolean z) {
        this.isBidCampaign = z;
    }

    public String getBidToken() {
        return this.bidToken;
    }

    public void setBidToken(String str) {
        this.bidToken = str;
    }

    public int getEndcard_click_result() {
        return this.endcard_click_result;
    }

    public void setEndcard_click_result(int i) {
        this.endcard_click_result = i;
    }

    public int getImpUA() {
        return this.impUA;
    }

    public void setImpUA(int i) {
        this.impUA = i;
    }

    public int getcUA() {
        return this.cUA;
    }

    public void setcUA(int i) {
        this.cUA = i;
    }

    public String getVideoMD5Value() {
        return this.videoMD5Value;
    }

    public void setVideoMD5Value(String str) {
        this.videoMD5Value = str;
    }

    public int getVideo_end_type() {
        return this.video_end_type;
    }

    public void setVideo_end_type(int i) {
        this.video_end_type = i;
    }

    public String getMraid() {
        return this.mraid;
    }

    public void setMraid(String str) {
        this.mraid = str;
    }

    public boolean isMraid() {
        return this.isMraid;
    }

    public void setIsMraid(boolean z) {
        this.isMraid = z;
    }

    public String getendcard_url() {
        return this.endcard_url;
    }

    public void setendcard_url(String str) {
        this.endcard_url = str;
    }

    public int getPlayable_ads_without_video() {
        return this.playable_ads_without_video;
    }

    public void setPlayable_ads_without_video(int i) {
        this.playable_ads_without_video = i;
    }

    public Map<String, String> getLoopbackMap() {
        return this.loopbackMap;
    }

    public void setLoopbackMap(Map<String, String> map) {
        this.loopbackMap = map;
    }

    public String getLoopbackString() {
        return this.loopbackString;
    }

    public void setLoopbackString(String str) {
        this.loopbackString = str;
    }

    public String getCampaignUnitId() {
        return this.campaignUnitId;
    }

    public void setCampaignUnitId(String str) {
        this.campaignUnitId = str;
    }

    public String getNativeVideoTrackingString() {
        return this.nativeVideoTrackingString;
    }

    public void setNativeVideoTrackingString(String str) {
        this.nativeVideoTrackingString = str;
    }

    public i getNativeVideoTracking() {
        return this.nativeVideoTracking;
    }

    public void setNativeVideoTracking(i iVar) {
        this.nativeVideoTracking = iVar;
    }

    public String getRoverMark() {
        return this.roverMark;
    }

    public void setRoverMark(String str) {
        this.roverMark = str;
    }

    public int getRoverIsPost() {
        return this.roverIsPost;
    }

    public void setRoverIsPost(int i) {
        this.roverIsPost = i;
    }

    public String getAd_url_list() {
        return this.ad_url_list;
    }

    public void setAd_url_list(String str) {
        this.ad_url_list = str;
    }

    public String getLabel() {
        return this.label;
    }

    public void setLabel(String str) {
        this.label = str;
    }

    public String getPkgSource() {
        return this.pkgSource;
    }

    public void setPkgSource(String str) {
        this.pkgSource = str;
    }

    public int getIex() {
        return this.iex;
    }

    public void setIex(int i) {
        this.iex = i;
    }

    public long getTs() {
        return this.ts;
    }

    public void setTs(long j) {
        this.ts = j;
    }

    public b getMediaViewHolder() {
        return this.mediaViewHolder;
    }

    public void setMediaViewHolder(b bVar) {
        this.mediaViewHolder = bVar;
    }

    public c getRewardTemplateMode() {
        return this.rewardTemplateMode;
    }

    public void setRewardTemplateMode(c cVar) {
        this.rewardTemplateMode = cVar;
    }

    public int getRetarget_offer() {
        return this.retarget_offer;
    }

    public void setRetarget_offer(int i) {
        this.retarget_offer = i;
    }

    public int getTtc_ct2() {
        return this.ttc_ct2;
    }

    public void setTtc_ct2(int i) {
        this.ttc_ct2 = i;
    }

    public int getTtc_type() {
        return this.ttc_type;
    }

    public void setTtc_type(int i) {
        this.ttc_type = i;
    }

    public String getAdvId() {
        return this.advId;
    }

    public void setAdvId(String str) {
        this.advId = str;
    }

    public int getRewardPlayStatus() {
        return this.rewardPlayStatus;
    }

    public void setRewardPlayStatus(int i) {
        this.rewardPlayStatus = i;
    }

    public String getGuidelines() {
        return this.guidelines;
    }

    public void setGuidelines(String str) {
        this.guidelines = str;
    }

    public int getOfferType() {
        return this.offerType;
    }

    public void setOfferType(int i) {
        this.offerType = i;
    }

    public String getHtmlUrl() {
        return this.htmlUrl;
    }

    public void setHtmlUrl(String str) {
        this.htmlUrl = str;
    }

    public String getEndScreenUrl() {
        return this.endScreenUrl;
    }

    public void setEndScreenUrl(String str) {
        this.endScreenUrl = str;
    }

    public int getRewardAmount() {
        return this.rewardAmount;
    }

    public void setRewardAmount(int i) {
        this.rewardAmount = i;
    }

    public String getRewardName() {
        return this.reward_name;
    }

    public void setRewardName(String str) {
        this.reward_name = str;
    }

    public int getLinkType() {
        return this.linkType;
    }

    public void setLinkType(int i) {
        this.linkType = i;
    }

    public int getBty() {
        return this.bty;
    }

    public void setBty(int i) {
        this.bty = i;
    }

    public String getAdvImp() {
        return this.advImp;
    }

    public void setAdvImp(String str) {
        this.advImp = str;
    }

    public Map<Integer, String> getAdvImpList() {
        return generateAdImpression(this.advImp);
    }

    public int getTImp() {
        return this.t_imp;
    }

    public void setTImp(int i) {
        this.t_imp = i;
    }

    public String getVideoUrlEncode() {
        return this.videoUrlEncode;
    }

    public void setVideoUrlEncode(String str) {
        this.videoUrlEncode = str;
    }

    public int getVideoLength() {
        return this.videoLength;
    }

    public void setVideoLength(int i) {
        this.videoLength = i;
    }

    public int getVideoSize() {
        return this.videoSize;
    }

    public void setVideoSize(int i) {
        this.videoSize = i;
    }

    public String getVideoResolution() {
        return this.videoResolution;
    }

    public void setVideoResolution(String str) {
        this.videoResolution = str;
    }

    public int getWatchMile() {
        return this.watchMile;
    }

    public void setWatchMile(int i) {
        this.watchMile = i;
    }

    public int getCacheLevel() {
        return this.cacheLevel;
    }

    public void setCacheLevel(int i) {
        this.cacheLevel = i;
    }

    public void setReport(boolean z) {
        this.isReport = z;
    }

    public boolean isReport() {
        return this.isReport;
    }

    public boolean isReportClick() {
        return this.isReportClick;
    }

    public void setReportClick(boolean z) {
        this.isReportClick = z;
    }

    public int getClickInterval() {
        return this.clickInterval;
    }

    public void setClickInterval(int i) {
        this.clickInterval = i;
    }

    public int getPreClickInterval() {
        return this.preClickInterval;
    }

    public void setPreClickInterval(int i) {
        this.preClickInterval = i;
    }

    public int getClickTimeOutInterval() {
        return this.clickTimeOutInterval;
    }

    public void setClickTimeOutInterval(int i) {
        this.clickTimeOutInterval = i;
    }

    public String getRequestId() {
        try {
            if (!TextUtils.isEmpty(this.requestId)) {
                return this.requestId;
            }
            if (!TextUtils.isEmpty(this.onlyImpressionURL)) {
                Uri parse = Uri.parse(this.onlyImpressionURL);
                if (parse != null) {
                    this.requestId = parse.getQueryParameter(JSON_KEY_AD_K);
                    setRequestId(this.requestId);
                }
                return this.requestId;
            }
            return null;
        } catch (Exception unused) {
        }
    }

    public void setRequestId(String str) {
        this.requestId = str;
    }

    public String getRequestIdNotice() {
        try {
            if (!TextUtils.isEmpty(this.requestIdNotice)) {
                return this.requestIdNotice;
            }
            if (!TextUtils.isEmpty(this.noticeUrl)) {
                Uri parse = Uri.parse(this.noticeUrl);
                if (parse != null) {
                    this.requestIdNotice = parse.getQueryParameter(JSON_KEY_AD_K);
                    setRequestIdNotice(this.requestIdNotice);
                }
                return this.requestIdNotice;
            }
            return "";
        } catch (Exception unused) {
        }
    }

    public void setRequestIdNotice(String str) {
        this.requestIdNotice = str;
    }

    public String getClick_mode() {
        return this.click_mode;
    }

    public void setClick_mode(String str) {
        this.click_mode = str;
    }

    public String getLandingType() {
        return this.landingType;
    }

    public void setLandingType(String str) {
        this.landingType = str;
    }

    public int getFca() {
        return this.fca;
    }

    public void setFca(int i) {
        this.fca = i;
    }

    public int getFcb() {
        return this.fcb;
    }

    public void setFcb(int i) {
        this.fcb = i;
    }

    public int getTab() {
        return this.tab;
    }

    public void setTab(int i) {
        this.tab = i;
    }

    public String getClickURL() {
        return this.clickURL;
    }

    public void setClickURL(String str) {
        this.clickURL = str;
    }

    public String getDeepLinkURL() {
        return this.deepLinkUrl;
    }

    public void setDeepLinkUrl(String str) {
        this.deepLinkUrl = str;
    }

    public String getImpressionURL() {
        return this.impressionURL;
    }

    public void setImpressionURL(String str) {
        this.impressionURL = str;
    }

    public String getNoticeUrl() {
        return this.noticeUrl;
    }

    public void setNoticeUrl(String str) {
        this.noticeUrl = str;
    }

    public boolean isPreClick() {
        return this.preClick;
    }

    public void setPreClick(boolean z) {
        this.preClick = z;
    }

    public String getOnlyImpressionURL() {
        return this.onlyImpressionURL;
    }

    public void setOnlyImpressionURL(String str) {
        this.onlyImpressionURL = str;
    }

    public JumpLoaderResult getJumpResult() {
        return this.jumpResult;
    }

    public void setJumpResult(JumpLoaderResult jumpLoaderResult) {
        this.jumpResult = jumpLoaderResult;
    }

    public int getTemplate() {
        return this.template;
    }

    public void setTemplate(int i) {
        this.template = i;
    }

    public String getImageSize() {
        return this.imageSize;
    }

    public void setImageSize(String str) {
        this.imageSize = str;
    }

    public String getHost() {
        String str = "";
        if (TextUtils.isEmpty(getNoticeUrl())) {
            return str;
        }
        try {
            URL url = new URL(getNoticeUrl());
            StringBuilder sb = new StringBuilder();
            sb.append(url.getProtocol());
            sb.append("://");
            sb.append(url.getHost());
            return sb.toString();
        } catch (Exception e) {
            e.printStackTrace();
            return str;
        }
    }

    public static CampaignEx parseSettingCampaign(JSONObject jSONObject) {
        if (jSONObject == null) {
            return null;
        }
        CampaignEx campaignEx = new CampaignEx();
        campaignEx.setId(jSONObject.optString("campaignid"));
        campaignEx.setPackageName(jSONObject.optString("packageName"));
        campaignEx.setAppName(jSONObject.optString("title"));
        campaignEx.setAdCall(jSONObject.optString("cta"));
        campaignEx.setAppDesc(jSONObject.optString(JSON_KEY_DESC));
        campaignEx.setImpressionURL(jSONObject.optString(JSON_KEY_IMPRESSION_URL));
        campaignEx.setImageUrl(jSONObject.optString("image_url"));
        campaignEx.setPlct(jSONObject.optLong(JSON_KEY_PLCT));
        campaignEx.setPlctb(jSONObject.optLong(JSON_KEY_PLCTB));
        return campaignEx;
    }

    public static CampaignEx parseShortCutsCampaign(JSONObject jSONObject) {
        CampaignEx campaignEx = null;
        if (jSONObject == null) {
            return null;
        }
        try {
            CampaignEx campaignEx2 = new CampaignEx();
            try {
                campaignEx2.setId(jSONObject.optString("id"));
                campaignEx2.setAppName(jSONObject.optString("title"));
                campaignEx2.setAppDesc(jSONObject.optString(JSON_KEY_DESC));
                campaignEx2.setPackageName(jSONObject.optString(JSON_KEY_PACKAGE_NAME));
                campaignEx2.setIconUrl(jSONObject.optString(JSON_KEY_ICON_URL));
                campaignEx2.setImageUrl(jSONObject.optString("image_url"));
                campaignEx2.setSize(jSONObject.optString(JSON_KEY_APP_SIZE));
                campaignEx2.setImageSize(jSONObject.optString(JSON_KEY_IMAGE_SIZE));
                campaignEx2.setImpressionURL(jSONObject.optString(JSON_KEY_IMPRESSION_URL));
                campaignEx2.setClickURL(jSONObject.optString("click_url"));
                campaignEx2.setDeepLinkUrl(jSONObject.optString(JSON_KEY_DEEP_LINK_URL));
                campaignEx2.setNoticeUrl(jSONObject.optString(JSON_KEY_NOTICE_URL));
                campaignEx2.setPreClick(jSONObject.optBoolean(JSON_KEY_PRE_CLICK));
                campaignEx2.setTemplate(jSONObject.optInt("template"));
                campaignEx2.setType(jSONObject.optInt(JSON_KEY_AD_SOURCE_ID));
                campaignEx2.setFca(jSONObject.optInt(JSON_KEY_FCA));
                campaignEx2.setFcb(jSONObject.optInt(JSON_KEY_FCB));
                campaignEx2.setEndcard_click_result(jSONObject.optInt(JSON_KEY_ENDCARD_CLICK));
                if (!TextUtils.isEmpty(jSONObject.optString(JSON_KEY_STAR))) {
                    campaignEx2.setRating(Double.parseDouble(jSONObject.optString(JSON_KEY_STAR, "0")));
                }
                campaignEx2.setClick_mode(jSONObject.optString(JSON_KEY_CLICK_MODE));
                campaignEx2.setLandingType(jSONObject.optString(JSON_KEY_LANDING_TYPE));
                campaignEx2.setLinkType(jSONObject.optInt(JSON_KEY_LINK_TYPE, 4));
                campaignEx2.setClickInterval(jSONObject.optInt(JSON_KEY_CLICK_INTERVAL));
                campaignEx2.setPreClickInterval(jSONObject.optInt(JSON_KEY_PRE_CLICK_INTERVAL, TTC_CT_DEFAULT_VALUE));
                campaignEx2.setAdCall(jSONObject.optString(JSON_KEY_CTA_TEXT));
                campaignEx2.setAd_url_list(jSONObject.optString(JSON_KEY_AD_URL_LIST));
                campaignEx2.setAdvId(jSONObject.optString(JSON_KEY_ADV_ID));
                campaignEx2.setTtc_type(jSONObject.optInt(JSON_KEY_TTC_TYPE, 3));
                campaignEx2.setTtc_ct2(jSONObject.optInt(JSON_KEY_TTC_CT2, 1800));
                campaignEx2.setRetarget_offer(jSONObject.optInt(JSON_KEY_RETARGET_OFFER, 2));
                jSONObject.optString("video_url");
                campaignEx2.setVideoLength(jSONObject.optInt(JSON_KEY_VIDEO_LENGTHL));
                campaignEx2.setVideoSize(jSONObject.optInt(JSON_KEY_VIDEO_SIZE));
                campaignEx2.setVideoResolution(jSONObject.optString(JSON_KEY_VIDEO_RESOLUTION));
                campaignEx2.setWatchMile(jSONObject.optInt(JSON_KEY_WATCH_MILE));
                campaignEx2.setTimestamp(System.currentTimeMillis());
                campaignEx2.setBty(jSONObject.optInt(JSON_KEY_BTY));
                campaignEx2.setAdvImp(jSONObject.optString(JSON_KEY_ADVIMP));
                campaignEx2.setTImp(jSONObject.optInt(JSON_KEY_T_IMP));
                campaignEx2.setGuidelines(jSONObject.optString(JSON_KEY_GUIDELINES));
                campaignEx2.setOfferType(jSONObject.optInt(JSON_KEY_OFFER_TYPE));
                campaignEx2.setRewardName(jSONObject.optString(JSON_KEY_REWARD_NAME));
                campaignEx2.setRewardAmount(jSONObject.optInt(JSON_KEY_REWARD_AMOUNT));
                campaignEx2.setRoverMark(jSONObject.optString(ROVER_KEY_MARK));
                campaignEx2.setRoverIsPost(jSONObject.optInt(ROVER_KEY_IS_POST));
                try {
                    if (jSONObject.has(JSON_NATIVE_VIDEO_AD_TRACKING)) {
                        String optString = jSONObject.optString(JSON_NATIVE_VIDEO_AD_TRACKING);
                        if (!TextUtils.isEmpty(optString)) {
                            campaignEx2.setNativeVideoTrackingString(optString);
                            campaignEx2.setNativeVideoTracking(TrackingStr2Object(optString));
                        }
                    }
                } catch (Exception unused) {
                    g.d("", "ad_tracking parser error");
                }
                campaignEx2.setVideo_end_type(jSONObject.optInt(VIDEO_END_TYPE, 2));
                campaignEx2.setendcard_url(jSONObject.optString(ENDCARD_URL));
                campaignEx2.setPlayable_ads_without_video(jSONObject.optInt(PLAYABLE_ADS_WITHOUT_VIDEO, 1));
                try {
                    if (jSONObject.has(LOOPBACK)) {
                        String optString2 = jSONObject.optString(LOOPBACK);
                        if (!TextUtils.isEmpty(optString2)) {
                            campaignEx2.setLoopbackString(optString2);
                            campaignEx2.setLoopbackMap(loopbackStrToMap(optString2));
                        }
                    }
                } catch (Exception unused2) {
                    g.d("", "loopback parser error");
                }
                if (jSONObject.has(JSON_KEY_REWARD_VIDEO_MD5)) {
                    campaignEx2.setVideoMD5Value(jSONObject.optString(JSON_KEY_REWARD_VIDEO_MD5));
                }
                if (jSONObject.has(JSON_KEY_NV_T2)) {
                    campaignEx2.setNvT2(jSONObject.optInt(JSON_KEY_NV_T2));
                }
                if (jSONObject.has(JSON_KEY_GIF_URL)) {
                    campaignEx2.setGifUrl(jSONObject.optString(JSON_KEY_GIF_URL));
                }
                campaignEx2.setRewardTemplateMode(c.a(jSONObject.optJSONObject(JSON_KEY_REWARD_TEMPLATE)));
                campaignEx2.setClickTimeOutInterval(jSONObject.optInt(JSON_KEY_CLICK_TIMEOUT_INTERVAL, 2));
                campaignEx2.setImpUA(jSONObject.optInt(JSON_KEY_IMP_UA, 1));
                campaignEx2.setcUA(jSONObject.optInt(JSON_KEY_C_UA, 1));
                campaignEx2.setJmPd(jSONObject.optInt(JSON_KEY_JM_PD));
                campaignEx2.setKeyIaIcon(jSONObject.optString("ia_icon"));
                campaignEx2.setKeyIaRst(jSONObject.optInt("ia_rst"));
                campaignEx2.setKeyIaUrl(jSONObject.optString("ia_url"));
                campaignEx2.setKeyIaOri(jSONObject.optInt("ia_ori"));
                campaignEx2.setAdType(jSONObject.optInt("ad_type"));
                campaignEx2.setIa_ext1(jSONObject.optString(KEY_IA_EXT1));
                campaignEx2.setIa_ext2(jSONObject.optString(KEY_IA_EXT2));
                campaignEx2.setIsDownLoadZip(jSONObject.optInt(KEY_IS_DOWNLOAD));
                campaignEx2.setInteractiveCache(jSONObject.optString(KEY_IA_CACHE));
                String optString3 = jSONObject.optString(KEY_GH_ID);
                if (!TextUtils.isEmpty(optString3)) {
                    campaignEx2.setGhId(optString3);
                    String optString4 = jSONObject.optString(KEY_GH_PATH);
                    if (!TextUtils.isEmpty(optString4)) {
                        campaignEx2.setGhPath(com.mintegral.msdk.base.utils.a.c(optString4));
                    }
                    campaignEx2.setBindId(jSONObject.optString(KEY_BIND_ID));
                }
                campaignEx2.setOc_time(jSONObject.optInt(KEY_OC_TIME));
                campaignEx2.setOc_type(jSONObject.optInt(KEY_OC_TYPE));
                campaignEx2.setT_list(jSONObject.optString(KEY_T_LIST));
                campaignEx2.setAdchoice(a.a(jSONObject.optString(KET_ADCHOICE, "")));
                campaignEx2.setPlct(jSONObject.optLong(JSON_KEY_PLCT));
                campaignEx2.setPlctb(jSONObject.optLong(JSON_KEY_PLCTB));
                return campaignEx2;
            } catch (Exception unused3) {
                campaignEx = campaignEx2;
                g.d(TAG, "parse campaign json exception");
                return campaignEx;
            }
        } catch (Exception unused4) {
            g.d(TAG, "parse campaign json exception");
            return campaignEx;
        }
    }

    private static String replaceValueByKey(CampaignUnit campaignUnit, CampaignEx campaignEx, String str) {
        if (campaignUnit == null || TextUtils.isEmpty(str) || campaignEx == null) {
            return str;
        }
        try {
            HashMap rks = campaignUnit.getRks();
            if (rks != null) {
                rks.entrySet().iterator();
                for (Entry entry : rks.entrySet()) {
                    String str2 = (String) entry.getKey();
                    String str3 = (String) entry.getValue();
                    StringBuilder sb = new StringBuilder("\\{");
                    sb.append(str2);
                    sb.append("\\}");
                    str = str.replaceAll(sb.toString(), str3);
                }
            }
            HashMap aks2 = campaignEx.getAks();
            if (aks2 != null) {
                aks2.entrySet().iterator();
                for (Entry entry2 : aks2.entrySet()) {
                    String str4 = (String) entry2.getKey();
                    String str5 = (String) entry2.getValue();
                    StringBuilder sb2 = new StringBuilder("\\{");
                    sb2.append(str4);
                    sb2.append("\\}");
                    str = str.replaceAll(sb2.toString(), str5);
                }
            }
            String replaceAll = str.replaceAll("\\{c\\}", URLEncoder.encode(campaignUnit.assembCParams(), "utf-8"));
            try {
                Matcher matcher = Pattern.compile("=\\{.*?\\}").matcher(replaceAll);
                while (true) {
                    str = replaceAll;
                    if (!matcher.find()) {
                        break;
                    }
                    replaceAll = str.replace(matcher.group(0), RequestParameters.EQUAL);
                }
            } catch (Throwable th) {
                str = replaceAll;
                th = th;
                g.c(TAG, th.getMessage(), th);
                return str;
            }
        } catch (Throwable th2) {
            th = th2;
            g.c(TAG, th.getMessage(), th);
            return str;
        }
        return str;
    }

    public static CampaignEx parseCampaign(JSONObject jSONObject, String str, String str2, String str3, boolean z, CampaignUnit campaignUnit) {
        return parseCampaign(jSONObject, str, str2, str3, z, campaignUnit, "");
    }

    public static CampaignEx parseCampaign(JSONObject jSONObject, String str, String str2, String str3, boolean z, CampaignUnit campaignUnit, String str4) {
        CampaignEx campaignEx = null;
        if (jSONObject == null) {
            return null;
        }
        try {
            CampaignEx campaignEx2 = new CampaignEx();
            try {
                String optString = jSONObject.optString(JSON_KEY_AD_AKS);
                if (!TextUtils.isEmpty(optString)) {
                    JSONObject jSONObject2 = new JSONObject(optString);
                    Iterator keys = jSONObject2.keys();
                    HashMap hashMap = new HashMap();
                    while (keys != null && keys.hasNext()) {
                        String str5 = (String) keys.next();
                        hashMap.put(str5, jSONObject2.optString(str5));
                    }
                    campaignEx2.setAks(hashMap);
                }
                if (!TextUtils.isEmpty(str4)) {
                    campaignEx2.setBidToken(str4);
                    campaignEx2.setIsBidCampaign(true);
                }
                campaignEx2.setId(jSONObject.optString("id"));
                campaignEx2.setAppName(jSONObject.optString("title"));
                campaignEx2.setAppDesc(jSONObject.optString(JSON_KEY_DESC));
                campaignEx2.setPackageName(jSONObject.optString(JSON_KEY_PACKAGE_NAME));
                campaignEx2.setIconUrl(jSONObject.optString(JSON_KEY_ICON_URL));
                campaignEx2.setImageUrl(jSONObject.optString("image_url"));
                campaignEx2.setSize(jSONObject.optString(JSON_KEY_APP_SIZE));
                campaignEx2.setImageSize(jSONObject.optString(JSON_KEY_IMAGE_SIZE));
                campaignEx2.setImpressionURL(replaceValueByKey(campaignUnit, campaignEx2, jSONObject.optString(JSON_KEY_IMPRESSION_URL)));
                campaignEx2.setClickURL(replaceValueByKey(campaignUnit, campaignEx2, jSONObject.optString("click_url")));
                campaignEx2.setDeepLinkUrl(replaceValueByKey(campaignUnit, campaignEx2, jSONObject.optString(JSON_KEY_DEEP_LINK_URL)));
                campaignEx2.setNoticeUrl(replaceValueByKey(campaignUnit, campaignEx2, jSONObject.optString(JSON_KEY_NOTICE_URL)));
                campaignEx2.setPreClick(jSONObject.optBoolean(JSON_KEY_PRE_CLICK));
                campaignEx2.setTemplate(jSONObject.optInt("template"));
                campaignEx2.setType(jSONObject.optInt(JSON_KEY_AD_SOURCE_ID));
                campaignEx2.setFca(jSONObject.optInt(JSON_KEY_FCA));
                campaignEx2.setFcb(jSONObject.optInt(JSON_KEY_FCB));
                campaignEx2.setEndcard_click_result(jSONObject.optInt(JSON_KEY_ENDCARD_CLICK));
                if (!TextUtils.isEmpty(jSONObject.optString(JSON_KEY_STAR))) {
                    campaignEx2.setRating(Double.parseDouble(jSONObject.optString(JSON_KEY_STAR, "0")));
                }
                campaignEx2.setClick_mode(jSONObject.optString(JSON_KEY_CLICK_MODE));
                campaignEx2.setLandingType(jSONObject.optString(JSON_KEY_LANDING_TYPE));
                campaignEx2.setLinkType(jSONObject.optInt(JSON_KEY_LINK_TYPE, 4));
                campaignEx2.setClickInterval(jSONObject.optInt(JSON_KEY_CLICK_INTERVAL));
                campaignEx2.setPreClickInterval(jSONObject.optInt(JSON_KEY_PRE_CLICK_INTERVAL, TTC_CT_DEFAULT_VALUE));
                campaignEx2.setAdCall(jSONObject.optString(JSON_KEY_CTA_TEXT));
                campaignEx2.setAd_url_list(jSONObject.optString(JSON_KEY_AD_URL_LIST));
                campaignEx2.setAdvId(jSONObject.optString(JSON_KEY_ADV_ID));
                campaignEx2.setTtc_type(jSONObject.optInt(JSON_KEY_TTC_TYPE, 3));
                campaignEx2.setTtc_ct2(jSONObject.optInt(JSON_KEY_TTC_CT2, 1800));
                campaignEx2.setRetarget_offer(jSONObject.optInt(JSON_KEY_RETARGET_OFFER, 2));
                String optString2 = jSONObject.optString("video_url");
                if (!TextUtils.isEmpty(optString2)) {
                    if (z) {
                        campaignEx2.setVideoUrlEncode(optString2);
                    } else {
                        campaignEx2.setVideoUrlEncode(com.mintegral.msdk.base.utils.a.c(optString2));
                    }
                }
                campaignEx2.setVideoLength(jSONObject.optInt(JSON_KEY_VIDEO_LENGTHL));
                campaignEx2.setVideoSize(jSONObject.optInt(JSON_KEY_VIDEO_SIZE));
                campaignEx2.setVideoResolution(jSONObject.optString(JSON_KEY_VIDEO_RESOLUTION));
                campaignEx2.setWatchMile(jSONObject.optInt(JSON_KEY_WATCH_MILE));
                campaignEx2.setTimestamp(System.currentTimeMillis());
                campaignEx2.setOnlyImpressionURL(replaceValueByKey(campaignUnit, campaignEx2, str));
                campaignEx2.setBty(jSONObject.optInt(JSON_KEY_BTY));
                campaignEx2.setAdvImp(jSONObject.optString(JSON_KEY_ADVIMP));
                campaignEx2.setTImp(jSONObject.optInt(JSON_KEY_T_IMP));
                campaignEx2.setHtmlUrl(str2);
                campaignEx2.setEndScreenUrl(str3);
                campaignEx2.setGuidelines(jSONObject.optString(JSON_KEY_GUIDELINES));
                campaignEx2.setOfferType(jSONObject.optInt(JSON_KEY_OFFER_TYPE));
                campaignEx2.setRewardName(jSONObject.optString(JSON_KEY_REWARD_NAME));
                campaignEx2.setRewardAmount(jSONObject.optInt(JSON_KEY_REWARD_AMOUNT));
                campaignEx2.setRoverMark(jSONObject.optString(ROVER_KEY_MARK));
                campaignEx2.setRoverIsPost(jSONObject.optInt(ROVER_KEY_IS_POST));
                try {
                    if (jSONObject.has(JSON_NATIVE_VIDEO_AD_TRACKING)) {
                        String replaceValueByKey = replaceValueByKey(campaignUnit, campaignEx2, jSONObject.optString(JSON_NATIVE_VIDEO_AD_TRACKING));
                        if (!TextUtils.isEmpty(replaceValueByKey)) {
                            campaignEx2.setNativeVideoTrackingString(replaceValueByKey);
                            campaignEx2.setNativeVideoTracking(TrackingStr2Object(replaceValueByKey));
                        }
                    }
                } catch (Exception unused) {
                    g.d("", "ad_tracking parser error");
                }
                campaignEx2.setVideo_end_type(jSONObject.optInt(VIDEO_END_TYPE, 2));
                campaignEx2.setendcard_url(jSONObject.optString(ENDCARD_URL));
                campaignEx2.setPlayable_ads_without_video(jSONObject.optInt(PLAYABLE_ADS_WITHOUT_VIDEO, 1));
                try {
                    if (jSONObject.has(LOOPBACK)) {
                        String optString3 = jSONObject.optString(LOOPBACK);
                        if (!TextUtils.isEmpty(optString3)) {
                            campaignEx2.setLoopbackString(optString3);
                            campaignEx2.setLoopbackMap(loopbackStrToMap(optString3));
                        }
                    }
                } catch (Exception unused2) {
                    g.d("", "loopback parser error");
                }
                if (jSONObject.has(JSON_KEY_REWARD_VIDEO_MD5)) {
                    campaignEx2.setVideoMD5Value(jSONObject.optString(JSON_KEY_REWARD_VIDEO_MD5));
                }
                if (jSONObject.has(JSON_KEY_NV_T2)) {
                    campaignEx2.setNvT2(jSONObject.optInt(JSON_KEY_NV_T2));
                }
                if (jSONObject.has(JSON_KEY_GIF_URL)) {
                    campaignEx2.setGifUrl(jSONObject.optString(JSON_KEY_GIF_URL));
                }
                campaignEx2.setRewardTemplateMode(c.a(jSONObject.optJSONObject(JSON_KEY_REWARD_TEMPLATE)));
                campaignEx2.setClickTimeOutInterval(jSONObject.optInt(JSON_KEY_CLICK_TIMEOUT_INTERVAL, 2));
                campaignEx2.setImpUA(jSONObject.optInt(JSON_KEY_IMP_UA, 1));
                campaignEx2.setcUA(jSONObject.optInt(JSON_KEY_C_UA, 1));
                campaignEx2.setJmPd(jSONObject.optInt(JSON_KEY_JM_PD));
                campaignEx2.setKeyIaIcon(jSONObject.optString("ia_icon"));
                campaignEx2.setKeyIaRst(jSONObject.optInt("ia_rst"));
                campaignEx2.setKeyIaUrl(jSONObject.optString("ia_url"));
                campaignEx2.setKeyIaOri(jSONObject.optInt("ia_ori"));
                campaignEx2.setAdType(campaignUnit.getAdType());
                campaignEx2.setIa_ext1(jSONObject.optString(KEY_IA_EXT1));
                campaignEx2.setIa_ext2(jSONObject.optString(KEY_IA_EXT2));
                campaignEx2.setIsDownLoadZip(jSONObject.optInt(KEY_IS_DOWNLOAD));
                campaignEx2.setInteractiveCache(jSONObject.optString(KEY_IA_CACHE));
                String optString4 = jSONObject.optString(KEY_GH_ID);
                if (!TextUtils.isEmpty(optString4)) {
                    campaignEx2.setGhId(optString4);
                    String optString5 = jSONObject.optString(KEY_GH_PATH);
                    if (!TextUtils.isEmpty(optString5)) {
                        campaignEx2.setGhPath(com.mintegral.msdk.base.utils.a.c(optString5));
                    }
                    campaignEx2.setBindId(jSONObject.optString(KEY_BIND_ID));
                }
                campaignEx2.setOc_time(jSONObject.optInt(KEY_OC_TIME));
                campaignEx2.setOc_type(jSONObject.optInt(KEY_OC_TYPE));
                campaignEx2.setT_list(jSONObject.optString(KEY_T_LIST));
                campaignEx2.setAdchoice(a.a(jSONObject.optString(KET_ADCHOICE, "")));
                campaignEx2.setPlct(jSONObject.optLong(JSON_KEY_PLCT));
                campaignEx2.setPlctb(jSONObject.optLong(JSON_KEY_PLCTB));
                String optString6 = jSONObject.optString(JSON_KEY_MRAID);
                if (!TextUtils.isEmpty(optString6)) {
                    campaignEx2.setIsMraid(true);
                    campaignEx2.setMraid(optString6);
                } else {
                    campaignEx2.setIsMraid(false);
                }
                return campaignEx2;
            } catch (Exception unused3) {
                campaignEx = campaignEx2;
                g.d(TAG, "parse campaign json exception");
                return campaignEx;
            }
        } catch (Exception unused4) {
            g.d(TAG, "parse campaign json exception");
            return campaignEx;
        }
    }

    public static Map<String, String> loopbackStrToMap(String str) {
        Map<String, String> map = null;
        try {
            if (TextUtils.isEmpty(str)) {
                return null;
            }
            HashMap hashMap = new HashMap();
            try {
                JSONObject jSONObject = new JSONObject(str);
                hashMap.put("domain", jSONObject.getString("domain"));
                hashMap.put("key", jSONObject.getString("key"));
                hashMap.put("value", jSONObject.getString("value"));
                return hashMap;
            } catch (Throwable unused) {
                map = hashMap;
                g.d("", "loopbackStrToMap error");
                return map;
            }
        } catch (Throwable unused2) {
            g.d("", "loopbackStrToMap error");
            return map;
        }
    }

    public static i TrackingStr2Object(String str) {
        try {
            if (TextUtils.isEmpty(str)) {
                return null;
            }
            JSONObject jSONObject = new JSONObject(str);
            i iVar = new i();
            iVar.r(processNativeVideoTrackingArray(jSONObject.optJSONArray("impression")));
            iVar.g(processNativeVideoTrackingArray(jSONObject.optJSONArray("start")));
            iVar.h(processNativeVideoTrackingArray(jSONObject.optJSONArray(JSON_NATIVE_VIDEO_FIRST_QUARTILE)));
            iVar.i(processNativeVideoTrackingArray(jSONObject.optJSONArray("midpoint")));
            iVar.j(processNativeVideoTrackingArray(jSONObject.optJSONArray(JSON_NATIVE_VIDEO_THIRD_QUARTILE)));
            iVar.k(processNativeVideoTrackingArray(jSONObject.optJSONArray("complete")));
            iVar.a(parsePlayCentage(jSONObject.optJSONArray(JSON_NATIVE_VIDEO_PLAY_PERCENTAGE)));
            iVar.l(processNativeVideoTrackingArray(jSONObject.optJSONArray("mute")));
            iVar.m(processNativeVideoTrackingArray(jSONObject.optJSONArray("unmute")));
            iVar.n(processNativeVideoTrackingArray(jSONObject.optJSONArray("click")));
            iVar.o(processNativeVideoTrackingArray(jSONObject.optJSONArray(JSON_NATIVE_VIDEO_PAUSE)));
            iVar.p(processNativeVideoTrackingArray(jSONObject.optJSONArray(JSON_NATIVE_VIDEO_RESUME)));
            iVar.q(processNativeVideoTrackingArray(jSONObject.optJSONArray("error")));
            iVar.s(processNativeVideoTrackingArray(jSONObject.optJSONArray(JSON_NATIVE_VIDEO_ENDCARD)));
            iVar.u(processNativeVideoTrackingArray(jSONObject.optJSONArray("close")));
            iVar.t(processNativeVideoTrackingArray(jSONObject.optJSONArray(JSON_NATIVE_VIDEO_ENDCARD_SHOW)));
            iVar.v(processNativeVideoTrackingArray(jSONObject.optJSONArray("video_click")));
            iVar.f(processNativeVideoTrackingArray(jSONObject.optJSONArray(JSON_KEY_AD_TRACKING_IMPRESSION_T2)));
            iVar.c(processNativeVideoTrackingArray(jSONObject.optJSONArray(JSON_KEY_AD_TRACKING_APK_START)));
            iVar.d(processNativeVideoTrackingArray(jSONObject.optJSONArray(JSON_KEY_AD_TRACKING_APK_END)));
            iVar.e(processNativeVideoTrackingArray(jSONObject.optJSONArray(JSON_KEY_AD_TRACKING_APK_INSTALL)));
            iVar.a(processNativeVideoTrackingArray(jSONObject.optJSONArray(JSON_KEY_AD_TRACKING_DROPOUT_TRACK)));
            iVar.b(processNativeVideoTrackingArray(jSONObject.optJSONArray(JSON_KEY_AD_TRACKING_PLYCMPT_TRACK)));
            iVar.w(processNativeVideoTrackingArray(jSONObject.optJSONArray(JSON_KEY_PUB_IMP)));
            return iVar;
        } catch (JSONException unused) {
            g.d(TAG, "parse error");
            return null;
        }
    }

    private static String[] processNativeVideoTrackingArray(JSONArray jSONArray) {
        if (jSONArray == null || jSONArray.length() <= 0) {
            return null;
        }
        String[] strArr = new String[jSONArray.length()];
        for (int i = 0; i < jSONArray.length(); i++) {
            strArr[i] = jSONArray.optString(i);
        }
        return strArr;
    }

    private Map<Integer, String> generateAdImpression(String str) {
        Map<Integer, String> map = null;
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            JSONArray jSONArray = new JSONArray(str);
            if (jSONArray.length() <= 0) {
                return null;
            }
            HashMap hashMap = new HashMap();
            int i = 0;
            while (i < jSONArray.length()) {
                try {
                    JSONObject optJSONObject = jSONArray.optJSONObject(i);
                    int optInt = optJSONObject.optInt(JSON_AD_IMP_KEY);
                    hashMap.put(Integer.valueOf(optInt), optJSONObject.optString("url"));
                    i++;
                } catch (Exception e) {
                    e = e;
                    map = hashMap;
                    e.printStackTrace();
                    return map;
                }
            }
            return hashMap;
        } catch (Exception e2) {
            e = e2;
            e.printStackTrace();
            return map;
        }
    }

    public List<String> getAdUrlList() {
        String ad_url_list2 = getAd_url_list();
        List<String> list = null;
        try {
            if (!TextUtils.isEmpty(ad_url_list2)) {
                JSONArray jSONArray = new JSONArray(ad_url_list2);
                ArrayList arrayList = new ArrayList();
                int i = 0;
                while (i < jSONArray.length()) {
                    try {
                        arrayList.add(jSONArray.optString(i));
                        i++;
                    } catch (Exception e) {
                        Exception exc = e;
                        list = arrayList;
                        e = exc;
                        e.printStackTrace();
                        return list;
                    }
                }
                return arrayList;
            }
        } catch (Exception e2) {
            e = e2;
            e.printStackTrace();
            return list;
        }
        return list;
    }

    public static JSONArray parseCamplistToJson(List<CampaignEx> list) {
        JSONArray jSONArray = null;
        if (list != null) {
            try {
                if (list.size() > 0) {
                    JSONArray jSONArray2 = new JSONArray();
                    try {
                        for (CampaignEx campaignToJsonObject : list) {
                            try {
                                jSONArray2.put(campaignToJsonObject(campaignToJsonObject));
                            } catch (Throwable th) {
                                th.printStackTrace();
                            }
                        }
                        return jSONArray2;
                    } catch (Exception e) {
                        e = e;
                        jSONArray = jSONArray2;
                    }
                }
            } catch (Exception e2) {
                e = e2;
                e.printStackTrace();
                return jSONArray;
            }
        }
        return jSONArray;
    }

    public static JSONObject campaignToJsonObject(CampaignEx campaignEx) throws JSONException {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("id", campaignEx.getId());
        if (!TextUtils.isEmpty(campaignEx.getCampaignUnitId())) {
            jSONObject.put("unitId", campaignEx.getCampaignUnitId());
        }
        jSONObject.put("title", campaignEx.getAppName());
        jSONObject.put(JSON_KEY_DESC, campaignEx.getAppDesc());
        jSONObject.put(JSON_KEY_PACKAGE_NAME, campaignEx.getPackageName());
        jSONObject.put(JSON_KEY_ICON_URL, campaignEx.getIconUrl());
        jSONObject.put("image_url", campaignEx.getImageUrl());
        jSONObject.put(JSON_KEY_APP_SIZE, campaignEx.getSize());
        jSONObject.put(JSON_KEY_IMAGE_SIZE, campaignEx.getImageSize());
        jSONObject.put(JSON_KEY_IMPRESSION_URL, campaignEx.getImpressionURL());
        jSONObject.put("click_url", campaignEx.getClickURL());
        jSONObject.put(JSON_KEY_DEEP_LINK_URL, campaignEx.getDeepLinkURL());
        jSONObject.put(JSON_KEY_NOTICE_URL, campaignEx.getNoticeUrl());
        jSONObject.put(JSON_KEY_PRE_CLICK, campaignEx.isPreClick());
        jSONObject.put("template", campaignEx.getTemplate());
        jSONObject.put(JSON_KEY_AD_SOURCE_ID, campaignEx.getType());
        jSONObject.put(JSON_KEY_FCA, campaignEx.getFca());
        jSONObject.put(JSON_KEY_FCB, campaignEx.getFcb());
        String str = JSON_KEY_STAR;
        StringBuilder sb = new StringBuilder();
        sb.append(campaignEx.getRating());
        jSONObject.put(str, sb.toString());
        jSONObject.put(JSON_KEY_CLICK_MODE, campaignEx.getClick_mode());
        jSONObject.put(JSON_KEY_LANDING_TYPE, campaignEx.getLandingType());
        jSONObject.put(JSON_KEY_LINK_TYPE, campaignEx.getLinkType());
        jSONObject.put(JSON_KEY_CLICK_INTERVAL, campaignEx.getClickInterval());
        jSONObject.put(JSON_KEY_PRE_CLICK_INTERVAL, campaignEx.getPreClickInterval());
        jSONObject.put(JSON_KEY_CTA_TEXT, campaignEx.getAdCall());
        jSONObject.put(JSON_KEY_ADV_ID, campaignEx.getAdvId());
        jSONObject.put(JSON_KEY_TTC_TYPE, campaignEx.getTtc_type());
        jSONObject.put(JSON_KEY_ENDCARD_CLICK, campaignEx.getEndcard_click_result());
        jSONObject.put(JSON_KEY_TTC_CT2, campaignEx.getTtc_ct2());
        jSONObject.put(JSON_KEY_RETARGET_OFFER, campaignEx.getRetarget_offer());
        jSONObject.put("video_url", campaignEx.getVideoUrlEncode());
        jSONObject.put(JSON_KEY_VIDEO_LENGTHL, campaignEx.getVideoLength());
        jSONObject.put(JSON_KEY_VIDEO_SIZE, campaignEx.getVideoSize());
        jSONObject.put(JSON_KEY_VIDEO_RESOLUTION, campaignEx.getVideoResolution());
        jSONObject.put(JSON_KEY_WATCH_MILE, campaignEx.getWatchMile());
        jSONObject.put(JSON_KEY_AD_URL_LIST, campaignEx.getAd_url_list());
        jSONObject.put("only_impression_url", campaignEx.getOnlyImpressionURL());
        jSONObject.put(JSON_KEY_BTY, campaignEx.getBty());
        jSONObject.put(JSON_KEY_T_IMP, campaignEx.getTImp());
        jSONObject.put(JSON_KEY_ADVIMP, campaignEx.getAdvImp());
        jSONObject.put("html_url", campaignEx.getHtmlUrl());
        jSONObject.put("end_screen_url", campaignEx.getEndScreenUrl());
        jSONObject.put(JSON_KEY_GUIDELINES, campaignEx.getGuidelines());
        jSONObject.put(JSON_KEY_OFFER_TYPE, campaignEx.getOfferType());
        jSONObject.put(JSON_KEY_REWARD_AMOUNT, campaignEx.getRewardAmount());
        jSONObject.put(JSON_KEY_REWARD_NAME, campaignEx.getRewardName());
        jSONObject.put(LOOPBACK, campaignEx.getLoopbackString());
        if (s.b(campaignEx.getNativeVideoTrackingString())) {
            jSONObject.put(JSON_NATIVE_VIDEO_AD_TRACKING, new JSONObject(campaignEx.getNativeVideoTrackingString()));
        }
        jSONObject.put(VIDEO_END_TYPE, campaignEx.getVideo_end_type());
        jSONObject.put(ENDCARD_URL, campaignEx.getendcard_url());
        jSONObject.put(PLAYABLE_ADS_WITHOUT_VIDEO, campaignEx.getPlayable_ads_without_video());
        if (!(campaignEx == null || campaignEx.getRewardTemplateMode() == null || !s.b(campaignEx.getRewardTemplateMode().a()))) {
            jSONObject.put(JSON_KEY_REWARD_TEMPLATE, new JSONObject(campaignEx.getRewardTemplateMode().a()));
        }
        jSONObject.put(JSON_KEY_REWARD_VIDEO_MD5, campaignEx.getVideoMD5Value());
        jSONObject.put(JSON_KEY_CLICK_TIMEOUT_INTERVAL, campaignEx.getClickTimeOutInterval());
        String str2 = TAG;
        StringBuilder sb2 = new StringBuilder("camapignJsonObject:");
        sb2.append(jSONObject.toString());
        g.b(str2, sb2.toString());
        jSONObject.put(JSON_KEY_C_UA, campaignEx.getcUA());
        jSONObject.put(JSON_KEY_IMP_UA, campaignEx.getImpUA());
        jSONObject.put(JSON_KEY_JM_PD, campaignEx.getJmPd());
        jSONObject.put("ia_icon", campaignEx.getKeyIaIcon());
        jSONObject.put("ia_rst", campaignEx.getKeyIaRst());
        jSONObject.put("ia_url", campaignEx.getKeyIaUrl());
        jSONObject.put("ia_ori", campaignEx.getKeyIaOri());
        jSONObject.put("ad_type", campaignEx.getAdType());
        jSONObject.put(KEY_IA_EXT1, campaignEx.getIa_ext1());
        jSONObject.put(KEY_IA_EXT2, campaignEx.getIa_ext2());
        jSONObject.put(KEY_IS_DOWNLOAD, campaignEx.getIsDownLoadZip());
        jSONObject.put(KEY_IA_CACHE, campaignEx.getInteractiveCache());
        jSONObject.put(KEY_GH_ID, campaignEx.getGhId());
        jSONObject.put(KEY_GH_PATH, com.mintegral.msdk.base.utils.a.b(campaignEx.getGhPath()));
        jSONObject.put(KEY_BIND_ID, campaignEx.getBindId());
        jSONObject.put(KEY_OC_TYPE, campaignEx.getOc_type());
        jSONObject.put(KEY_OC_TIME, campaignEx.getOc_time());
        jSONObject.put(KEY_T_LIST, campaignEx.getT_list());
        a adchoice2 = campaignEx.getAdchoice();
        if (adchoice2 != null) {
            jSONObject.put(KET_ADCHOICE, new JSONObject(adchoice2.c()));
        }
        jSONObject.put(JSON_KEY_PLCT, campaignEx.getPlct());
        jSONObject.put(JSON_KEY_PLCTB, campaignEx.getPlctb());
        return jSONObject;
    }

    public String matchLoopback(String str) {
        String replace;
        try {
            if (TextUtils.isEmpty(str)) {
                return str;
            }
            Map loopbackMap2 = getLoopbackMap();
            if (loopbackMap2 == null || loopbackMap2.size() <= 0) {
                return str;
            }
            Uri parse = Uri.parse(str);
            String host = parse.getHost();
            String str2 = (String) loopbackMap2.get("domain");
            if (TextUtils.isEmpty(host) || !host.contains(str2)) {
                return str;
            }
            String str3 = (String) loopbackMap2.get("key");
            String str4 = (String) loopbackMap2.get("value");
            if (!str.contains(str3) && TextUtils.isEmpty(parse.getQueryParameter(str3)) && !TextUtils.isEmpty(str3) && !TextUtils.isEmpty(str4)) {
                StringBuilder sb = new StringBuilder();
                sb.append(str);
                sb.append(RequestParameters.AMPERSAND);
                sb.append(str3);
                sb.append(RequestParameters.EQUAL);
                sb.append(str4);
                replace = sb.toString();
            } else if (TextUtils.isEmpty(str3) || TextUtils.isEmpty(str4)) {
                return str;
            } else {
                String str5 = "";
                if (!TextUtils.isEmpty(parse.getQueryParameter(str3))) {
                    str5 = parse.getQueryParameter(str3);
                }
                StringBuilder sb2 = new StringBuilder();
                sb2.append(str3);
                sb2.append(RequestParameters.EQUAL);
                sb2.append(str5);
                String sb3 = sb2.toString();
                StringBuilder sb4 = new StringBuilder();
                sb4.append(str3);
                sb4.append(RequestParameters.EQUAL);
                sb4.append(str4);
                replace = str.replace(sb3, sb4.toString());
            }
            return replace;
        } catch (Throwable unused) {
            g.d("", "matchLoopback error");
            return str;
        }
    }

    private static List<Map<Integer, String>> parsePlayCentage(JSONArray jSONArray) {
        ArrayList arrayList = new ArrayList();
        if (jSONArray != null) {
            int i = 0;
            while (i < jSONArray.length()) {
                try {
                    String string = jSONArray.getString(i);
                    if (!TextUtils.isEmpty(string)) {
                        JSONObject jSONObject = new JSONObject(string);
                        HashMap hashMap = new HashMap();
                        int i2 = jSONObject.getInt("rate");
                        hashMap.put(Integer.valueOf(i2), jSONObject.getString("url"));
                        arrayList.add(hashMap);
                    }
                    i++;
                } catch (Throwable unused) {
                    g.d("com.mintegral.msdk", "parsePlayCentage error");
                }
            }
        }
        return arrayList;
    }

    public boolean isHasReportAdTrackPause() {
        return this.hasReportAdTrackPause;
    }

    public void setHasReportAdTrackPause(boolean z) {
        this.hasReportAdTrackPause = z;
    }

    public boolean isSpareOffer(long j, long j2) {
        if (isEffectiveOffer(j)) {
            return false;
        }
        long currentTimeMillis = System.currentTimeMillis();
        if (getPlctb() > 0) {
            if (getTimestamp() + (getPlctb() * 1000) >= currentTimeMillis) {
                return true;
            }
            return false;
        } else if (getTimestamp() + j2 >= currentTimeMillis) {
            return true;
        } else {
            return false;
        }
    }

    public boolean isEffectiveOffer(long j) {
        long currentTimeMillis = System.currentTimeMillis();
        return getPlct() > 0 ? getTimestamp() + (getPlct() * 1000) >= currentTimeMillis : getTimestamp() + j >= currentTimeMillis;
    }
}
