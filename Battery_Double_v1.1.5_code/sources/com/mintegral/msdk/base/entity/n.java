package com.mintegral.msdk.base.entity;

/* compiled from: ReportData */
public final class n {

    /* renamed from: a reason: collision with root package name */
    private String f2603a;
    private String b;
    private String c;
    private String d;
    private int e;

    public final String a() {
        return this.d;
    }

    public final void a(String str) {
        this.d = str;
    }

    public n() {
    }

    public n(String str, String str2, String str3, String str4) {
        this.f2603a = str;
        this.b = str2;
        this.c = str3;
        this.d = str4;
    }

    public n(String str, String str2, String str3, String str4, int i) {
        this.f2603a = str;
        this.b = str2;
        this.c = str3;
        this.d = str4;
        this.e = i;
    }

    public final String b() {
        return this.f2603a;
    }

    public final String c() {
        return this.b;
    }

    public final String d() {
        return this.c;
    }

    public final void b(String str) {
        this.c = str;
    }

    public final int e() {
        return this.e;
    }
}
