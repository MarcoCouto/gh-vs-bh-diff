package com.mintegral.msdk.base.entity;

import android.os.Build.VERSION;
import android.text.TextUtils;
import com.integralads.avid.library.inmobi.utils.AvidJSONUtil;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.base.controller.a;
import com.mintegral.msdk.base.utils.c;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.out.Frame;
import com.mintegral.msdk.out.MTGConfiguration;
import com.mintegral.msdk.system.NoProGuard;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class CampaignUnit implements NoProGuard, Serializable {
    public static final String JSON_KEY_ADS = "ads";
    public static final String JSON_KEY_AD_TYPE = "ad_type";
    public static final String JSON_KEY_CSP = "csp";
    public static final String JSON_KEY_DO = "do";
    public static final String JSON_KEY_END_SCREEN_URL = "end_screen_url";
    public static final String JSON_KEY_FRAME_ADS = "frames";
    public static final String JSON_KEY_HTML_URL = "html_url";
    public static final String JSON_KEY_IA_EXT1 = "ia_all_ext1";
    public static final String JSON_KEY_IA_EXT2 = "ia_all_ext2";
    public static final String JSON_KEY_IA_ICON = "ia_icon";
    public static final String JSON_KEY_IA_ORI = "ia_ori";
    public static final String JSON_KEY_IA_RST = "ia_rst";
    public static final String JSON_KEY_IA_URL = "ia_url";
    public static final String JSON_KEY_JM_DO = "jm_do";
    public static final String JSON_KEY_ONLY_IMPRESSION_URL = "only_impression_url";
    public static final String JSON_KEY_PARENT_SESSION_ID = "parent_session_id";
    public static final String JSON_KEY_RKS = "rks";
    public static final String JSON_KEY_SESSION_ID = "session_id";
    public static final String JSON_KEY_SH = "sh";
    public static final String JSON_KEY_TEMPLATE = "template";
    public static final String JSON_KEY_UNIT_SIZE = "unit_size";
    private static final String TAG = "CampaignUnit";
    private static final long serialVersionUID = 1;
    private int adType;
    public ArrayList<CampaignEx> ads;
    private StringBuffer cParams = new StringBuffer();
    private String csp;
    private String domain;
    private String htmlUrl;
    private String ia_all_ext1;
    private String ia_all_ext2;
    private String ia_icon;
    private int ia_ori;
    private int ia_rst;
    private String ia_url;
    private int jmDo;
    private List<Frame> listFrames;
    private String onlyImpressionUrl;
    private String parentSessionId;
    private HashMap<String, String> rks;
    private String sessionId;
    private String sh;
    private int template;
    private String unitSize;

    private Object nullToEmpty(Object obj) {
        return obj == null ? "" : obj;
    }

    public String getIa_icon() {
        return this.ia_icon;
    }

    public void setIa_icon(String str) {
        this.ia_icon = str;
    }

    public int getIa_rst() {
        return this.ia_rst;
    }

    public void setIa_rst(int i) {
        this.ia_rst = i;
    }

    public String getIa_url() {
        return this.ia_url;
    }

    public void setIa_url(String str) {
        this.ia_url = str;
    }

    public int getIa_ori() {
        return this.ia_ori;
    }

    public void setIa_ori(int i) {
        this.ia_ori = i;
    }

    public String getIa_all_ext1() {
        return this.ia_all_ext1;
    }

    public void setIa_all_ext1(String str) {
        this.ia_all_ext1 = str;
    }

    public String getIa_all_ext2() {
        return this.ia_all_ext2;
    }

    public void setIa_all_ext2(String str) {
        this.ia_all_ext2 = str;
    }

    public HashMap<String, String> getRks() {
        return this.rks;
    }

    public void setRks(HashMap<String, String> hashMap) {
        this.rks = hashMap;
    }

    public String getCsp() {
        return this.csp;
    }

    public void setCsp(String str) {
        this.csp = str;
    }

    public String getDomain() {
        return this.domain;
    }

    public void setDomain(String str) {
        this.domain = str;
    }

    public String getSh() {
        return this.sh;
    }

    public void setSh(String str) {
        this.sh = str;
    }

    public int getJmDo() {
        return this.jmDo;
    }

    public void setJmDo(int i) {
        this.jmDo = i;
    }

    public List<Frame> getListFrames() {
        return this.listFrames;
    }

    public void setListFrames(List<Frame> list) {
        this.listFrames = list;
    }

    public String getSessionId() {
        return this.sessionId;
    }

    public void setSessionId(String str) {
        this.sessionId = str;
    }

    public String getParentSessionId() {
        return this.parentSessionId;
    }

    public void setParentSessionId(String str) {
        this.parentSessionId = str;
    }

    public int getAdType() {
        return this.adType;
    }

    public void setAdType(int i) {
        this.adType = i;
    }

    public String getUnitSize() {
        return this.unitSize;
    }

    public void setUnitSize(String str) {
        this.unitSize = str;
    }

    public String getHtmlUrl() {
        return this.htmlUrl;
    }

    public void setHtmlUrl(String str) {
        this.htmlUrl = str;
    }

    public String getOnlyImpressionUrl() {
        return this.onlyImpressionUrl;
    }

    public void setOnlyImpressionUrl(String str) {
        this.onlyImpressionUrl = str;
    }

    public ArrayList<CampaignEx> getAds() {
        return this.ads;
    }

    public void setAds(ArrayList<CampaignEx> arrayList) {
        this.ads = arrayList;
    }

    public int getTemplate() {
        return this.template;
    }

    public void setTemplate(int i) {
        this.template = i;
    }

    public String assembCParams() {
        if (this.cParams != null && this.cParams.length() > 0) {
            return this.cParams.toString();
        }
        try {
            String k = c.k();
            a.d().h();
            String b = c.b();
            a.d().h();
            String a2 = c.a();
            String str = "";
            String str2 = "";
            com.mintegral.msdk.base.controller.authoritycontroller.a.a();
            if (com.mintegral.msdk.base.controller.authoritycontroller.a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA)) {
                str = String.valueOf(c.p(a.d().h()));
                StringBuilder sb = new StringBuilder();
                sb.append(c.l(a.d().h()));
                sb.append(AvidJSONUtil.KEY_X);
                sb.append(c.m(a.d().h()));
                str2 = sb.toString();
            }
            StringBuffer stringBuffer = this.cParams;
            stringBuffer.append(getAdType());
            stringBuffer.append("|");
            stringBuffer.append(nullToEmpty("1"));
            stringBuffer.append("|");
            stringBuffer.append(nullToEmpty(VERSION.RELEASE));
            stringBuffer.append("|");
            stringBuffer.append(nullToEmpty(MTGConfiguration.SDK_VERSION));
            stringBuffer.append("|");
            stringBuffer.append(nullToEmpty(c.c()));
            stringBuffer.append("|");
            stringBuffer.append(nullToEmpty(str2));
            stringBuffer.append("|");
            stringBuffer.append(nullToEmpty(Integer.valueOf(c.g(a.d().h()))));
            stringBuffer.append("|");
            stringBuffer.append(nullToEmpty(c.f(a.d().h())));
            stringBuffer.append("|");
            stringBuffer.append(nullToEmpty(str));
            stringBuffer.append("|");
            stringBuffer.append(nullToEmpty(a2));
            stringBuffer.append(nullToEmpty(b));
            stringBuffer.append("|");
            stringBuffer.append(nullToEmpty(c.b(a.d().h())));
            stringBuffer.append("|");
            stringBuffer.append(nullToEmpty(c.h(a.d().h())));
            stringBuffer.append("|");
            stringBuffer.append(nullToEmpty(c.d(a.d().h())));
            stringBuffer.append("|");
            stringBuffer.append(nullToEmpty(k));
            stringBuffer.append("|");
            stringBuffer.append(nullToEmpty(""));
            stringBuffer.append("|");
            stringBuffer.append(nullToEmpty(c.e()));
            stringBuffer.append("|");
            stringBuffer.append(nullToEmpty(""));
            stringBuffer.append("|");
            StringBuilder sb2 = new StringBuilder();
            sb2.append(com.mintegral.msdk.base.common.a.D);
            sb2.append(",");
            sb2.append(com.mintegral.msdk.base.common.a.E);
            stringBuffer.append(nullToEmpty(sb2.toString()));
            this.cParams = stringBuffer;
        } catch (Throwable th) {
            g.c(TAG, th.getMessage(), th);
        }
        return this.cParams.toString();
    }

    public static CampaignUnit parseCampaignUnit(JSONObject jSONObject) {
        return parseCampaignUnit(jSONObject, "");
    }

    public static CampaignUnit parseCampaignUnit(JSONObject jSONObject, String str) {
        CampaignUnit campaignUnit;
        JSONObject jSONObject2 = jSONObject;
        if (jSONObject2 == null) {
            return null;
        }
        try {
            campaignUnit = new CampaignUnit();
            try {
                String optString = jSONObject2.optString(JSON_KEY_RKS);
                if (!TextUtils.isEmpty(optString)) {
                    JSONObject jSONObject3 = new JSONObject(optString);
                    Iterator keys = jSONObject3.keys();
                    HashMap hashMap = new HashMap();
                    while (keys != null && keys.hasNext()) {
                        String str2 = (String) keys.next();
                        hashMap.put(str2, jSONObject3.optString(str2));
                    }
                    campaignUnit.setRks(hashMap);
                }
                campaignUnit.setSessionId(jSONObject2.optString("session_id"));
                campaignUnit.setParentSessionId(jSONObject2.optString("parent_session_id"));
                campaignUnit.setAdType(jSONObject2.optInt("ad_type"));
                campaignUnit.setUnitSize(jSONObject2.optString("unit_size"));
                campaignUnit.setHtmlUrl(jSONObject2.optString("html_url"));
                campaignUnit.setOnlyImpressionUrl(jSONObject2.optString("only_impression_url"));
                campaignUnit.setTemplate(jSONObject2.optInt("template"));
                campaignUnit.setJmDo(jSONObject2.optInt(JSON_KEY_JM_DO));
                campaignUnit.setIa_icon(jSONObject2.optString("ia_icon"));
                campaignUnit.setIa_rst(jSONObject2.optInt("ia_rst"));
                campaignUnit.setIa_url(jSONObject2.optString("ia_url"));
                campaignUnit.setIa_ori(jSONObject2.optInt("ia_ori"));
                campaignUnit.setIa_all_ext1(jSONObject2.optString(JSON_KEY_IA_EXT1));
                campaignUnit.setIa_all_ext2(jSONObject2.optString(JSON_KEY_IA_EXT2));
                JSONArray optJSONArray = jSONObject2.optJSONArray(JSON_KEY_ADS);
                JSONArray optJSONArray2 = jSONObject2.optJSONArray("frames");
                if (optJSONArray2 != null && optJSONArray2.length() > 0) {
                    ArrayList arrayList = new ArrayList();
                    for (int i = 0; i < optJSONArray2.length(); i++) {
                        JSONObject optJSONObject = optJSONArray2.optJSONObject(i);
                        JSONArray jSONArray = optJSONObject.getJSONArray(JSON_KEY_ADS);
                        ArrayList arrayList2 = new ArrayList();
                        int i2 = 0;
                        while (i2 < jSONArray.length()) {
                            int i3 = i2;
                            CampaignEx parseCampaign = CampaignEx.parseCampaign(jSONArray.optJSONObject(i2), jSONObject2.optString("only_impression_url"), jSONObject2.optString("html_url"), jSONObject2.optString("end_screen_url"), false, campaignUnit, str);
                            parseCampaign.setKeyIaUrl(campaignUnit.getIa_url());
                            parseCampaign.setKeyIaOri(campaignUnit.getIa_ori());
                            parseCampaign.setKeyIaRst(campaignUnit.getIa_rst());
                            parseCampaign.setKeyIaIcon(campaignUnit.getIa_icon());
                            parseCampaign.setAdType(jSONObject2.optInt("ad_type"));
                            parseCampaign.setIa_ext1(jSONObject2.optString(CampaignEx.KEY_IA_EXT1));
                            parseCampaign.setIa_ext2(jSONObject2.optString(CampaignEx.KEY_IA_EXT2));
                            arrayList2.add(parseCampaign);
                            i2 = i3 + 1;
                        }
                        Frame frame = new Frame();
                        frame.setParentSessionId(jSONObject2.optString("parent_session_id"));
                        frame.setSessionId(jSONObject2.optString("session_id"));
                        frame.setCampaigns(arrayList2);
                        frame.setTemplate(optJSONObject.optInt("template"));
                        arrayList.add(frame);
                    }
                    campaignUnit.setListFrames(arrayList);
                    return campaignUnit;
                } else if (optJSONArray == null || optJSONArray.length() <= 0) {
                    return campaignUnit;
                } else {
                    ArrayList arrayList3 = new ArrayList();
                    for (int i4 = 0; i4 < optJSONArray.length(); i4++) {
                        arrayList3.add(CampaignEx.parseCampaign(optJSONArray.optJSONObject(i4), jSONObject2.optString("only_impression_url"), jSONObject2.optString("html_url"), jSONObject2.optString("end_screen_url"), false, campaignUnit, str));
                    }
                    campaignUnit.setAds(arrayList3);
                    return campaignUnit;
                }
            } catch (Exception unused) {
                g.d(TAG, "parse campaign unit exception");
                return campaignUnit;
            }
        } catch (Exception unused2) {
            campaignUnit = null;
            g.d(TAG, "parse campaign unit exception");
            return campaignUnit;
        }
    }
}
