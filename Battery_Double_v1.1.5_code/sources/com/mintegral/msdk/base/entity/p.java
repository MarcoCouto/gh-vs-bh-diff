package com.mintegral.msdk.base.entity;

import android.content.Context;
import android.text.TextUtils;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.base.controller.authoritycontroller.a;
import com.mintegral.msdk.base.utils.c;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.base.utils.k;
import java.net.URLEncoder;
import java.util.List;

/* compiled from: VideoReportData */
public final class p {

    /* renamed from: a reason: collision with root package name */
    public static int f2605a = 1;
    public static int b;
    private String A;
    private String B;
    private String C;
    private String D;
    private String E;
    private String F;
    private String G;
    private int H = 0;
    private String c;
    private String d;
    private String e;
    private String f;
    private int g;
    private String h;
    private int i;
    private int j;
    private int k;
    private String l;
    private String m;
    private int n;
    private int o;
    private String p;
    private int q;
    private int r;
    private int s = 0;
    private String t;
    private String u;
    private String v;
    private String w;
    private String x;
    private String y;
    private String z;

    public final String a() {
        return this.c;
    }

    public final void a(String str) {
        this.c = str;
    }

    public final String b() {
        return this.d;
    }

    public final void b(String str) {
        this.d = str;
    }

    public final String c() {
        return this.e;
    }

    public final void c(String str) {
        this.e = str;
    }

    public final String d() {
        return this.F;
    }

    public final void d(String str) {
        this.F = str;
    }

    public final String e() {
        return this.E;
    }

    public final void e(String str) {
        this.E = str;
    }

    public final String f() {
        return this.D;
    }

    public final void f(String str) {
        this.D = str;
    }

    public final String g() {
        return this.C;
    }

    public final void g(String str) {
        this.C = str;
    }

    public final String h() {
        return this.G;
    }

    public final void h(String str) {
        this.G = str;
    }

    public final String i() {
        return this.w;
    }

    public final void i(String str) {
        this.w = str;
    }

    public final void j(String str) {
        this.y = str;
    }

    public final void a(int i2) {
        this.H = i2;
    }

    public p() {
    }

    public p(String str, int i2, int i3, String str2, int i4, int i5, String str3) {
        this.f = str;
        this.g = i2;
        this.h = str3;
        this.k = i3;
        this.l = str2;
        this.r = i4;
        this.s = i5;
    }

    public p(String str, int i2, String str2, String str3, String str4) {
        this.f = str;
        this.h = str4;
        this.g = i2;
        this.l = str2;
        this.m = str3;
    }

    public p(String str, int i2, int i3, String str2, int i4, String str3, int i5, String str4) {
        this.f = str;
        this.g = i2;
        this.h = str4;
        this.k = i3;
        this.l = str2;
        this.o = i4;
        this.p = str3;
        this.q = i5;
    }

    public p(Context context, CampaignEx campaignEx, int i2, String str, int i3, int i4) {
        switch (i4) {
            case 1:
                this.f = "2000022";
                break;
            case 2:
                this.f = "2000025";
                break;
            case 3:
                this.f = "2000022";
                break;
        }
        this.g = c.p(context);
        this.h = c.a(context, this.g);
        this.k = campaignEx.getVideoLength();
        this.l = campaignEx.getNoticeUrl() == null ? campaignEx.getClickURL() : campaignEx.getNoticeUrl();
        this.o = i2;
        this.p = str;
        if (i3 == 0) {
            i3 = campaignEx.getVideoSize();
        }
        this.q = i3;
    }

    public p(String str, String str2, String str3, String str4, String str5, String str6, int i2, String str7) {
        this.f = str;
        this.z = str2;
        this.x = str3;
        this.A = str4;
        this.u = str5;
        this.v = str6;
        this.g = i2;
        this.h = str7;
    }

    public p(String str) {
        this.B = str;
    }

    public p(String str, int i2, String str2, String str3, String str4, String str5, String str6, String str7) {
        this.f = str;
        this.o = i2;
        this.p = str2;
        this.D = str3;
        this.v = str4;
        this.u = str5;
        this.m = str6;
        this.C = str7;
        if (Integer.valueOf(str2).intValue() > 60000) {
            this.o = 2;
        }
    }

    public p(String str, String str2, String str3, String str4, int i2) {
        this.f = str;
        this.v = str2;
        this.t = str3;
        this.u = str4;
        this.g = i2;
    }

    public p(String str, String str2, String str3, String str4, int i2, int i3, String str5) {
        this.f = str;
        this.v = str2;
        this.t = str3;
        this.u = str4;
        this.g = i2;
        this.m = str5;
        this.n = i3;
    }

    public final String j() {
        return this.t;
    }

    public final void k(String str) {
        this.t = str;
    }

    public final String k() {
        return this.u;
    }

    public final void l(String str) {
        this.u = str;
    }

    public final String l() {
        return this.v;
    }

    public final void m(String str) {
        this.v = str;
    }

    public final String m() {
        return this.f;
    }

    public final void n(String str) {
        this.f = str;
    }

    public final int n() {
        return this.j;
    }

    public final int o() {
        return this.k;
    }

    public final String p() {
        return this.l;
    }

    public final String q() {
        return this.m;
    }

    public final void o(String str) {
        this.m = str;
    }

    public final String r() {
        return this.p;
    }

    public final void p(String str) {
        this.p = str;
    }

    public final int s() {
        return this.q;
    }

    public final int t() {
        return this.g;
    }

    public final void b(int i2) {
        this.g = i2;
    }

    public final String u() {
        return this.h;
    }

    public final void q(String str) {
        this.h = str;
    }

    public final int v() {
        return this.i;
    }

    public final int w() {
        return this.o;
    }

    public final void c(int i2) {
        this.o = i2;
    }

    public static String a(List<p> list) {
        if (list == null || list.size() <= 0) {
            return null;
        }
        StringBuffer stringBuffer = new StringBuffer();
        for (p pVar : list) {
            StringBuilder sb = new StringBuilder("key=");
            sb.append(pVar.f);
            sb.append(RequestParameters.AMPERSAND);
            stringBuffer.append(sb.toString());
            StringBuilder sb2 = new StringBuilder("resource_type=");
            sb2.append(pVar.c);
            sb2.append(RequestParameters.AMPERSAND);
            stringBuffer.append(sb2.toString());
            StringBuilder sb3 = new StringBuilder("unit_id=");
            sb3.append(pVar.u);
            sb3.append(RequestParameters.AMPERSAND);
            stringBuffer.append(sb3.toString());
            StringBuilder sb4 = new StringBuilder("ad_type=");
            sb4.append(pVar.G);
            stringBuffer.append(sb4.toString());
            StringBuilder sb5 = new StringBuilder("cid=");
            sb5.append(pVar.v);
            sb5.append(RequestParameters.AMPERSAND);
            stringBuffer.append(sb5.toString());
            StringBuilder sb6 = new StringBuilder("rid_n=");
            sb6.append(pVar.t);
            sb6.append(RequestParameters.AMPERSAND);
            stringBuffer.append(sb6.toString());
            StringBuilder sb7 = new StringBuilder("reason=");
            sb7.append(pVar.m);
            sb7.append(RequestParameters.AMPERSAND);
            stringBuffer.append(sb7.toString());
            StringBuilder sb8 = new StringBuilder("creative=");
            sb8.append(pVar.e);
            stringBuffer.append(sb8.toString());
            a.a();
            if (a.a(MIntegralConstans.AUTHORITY_DEVICE_ID)) {
                StringBuilder sb9 = new StringBuilder("devid=");
                sb9.append(pVar.d);
                sb9.append(RequestParameters.AMPERSAND);
                stringBuffer.append(sb9.toString());
            }
            a.a();
            if (a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA)) {
                StringBuilder sb10 = new StringBuilder("network_type=");
                sb10.append(pVar.g);
                sb10.append(RequestParameters.AMPERSAND);
                stringBuffer.append(sb10.toString());
                stringBuffer.append("\n");
            } else {
                stringBuffer.append("\n");
            }
        }
        return stringBuffer.toString();
    }

    public static String b(List<p> list) {
        if (list == null || list.size() <= 0) {
            return null;
        }
        StringBuffer stringBuffer = new StringBuffer();
        for (p pVar : list) {
            a.a();
            if (a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA)) {
                StringBuilder sb = new StringBuilder("key=");
                sb.append(pVar.f);
                sb.append(RequestParameters.AMPERSAND);
                stringBuffer.append(sb.toString());
                StringBuilder sb2 = new StringBuilder("network_type=");
                sb2.append(pVar.g);
                sb2.append(RequestParameters.AMPERSAND);
                stringBuffer.append(sb2.toString());
                StringBuilder sb3 = new StringBuilder("network_str=");
                sb3.append(pVar.h);
                sb3.append(RequestParameters.AMPERSAND);
                stringBuffer.append(sb3.toString());
                StringBuilder sb4 = new StringBuilder("reason=");
                sb4.append(pVar.m);
                sb4.append(RequestParameters.AMPERSAND);
                stringBuffer.append(sb4.toString());
                StringBuilder sb5 = new StringBuilder("cid=");
                sb5.append(pVar.v);
                sb5.append(RequestParameters.AMPERSAND);
                stringBuffer.append(sb5.toString());
                StringBuilder sb6 = new StringBuilder("video_url=");
                sb6.append(pVar.E);
                sb6.append(RequestParameters.AMPERSAND);
                stringBuffer.append(sb6.toString());
                StringBuilder sb7 = new StringBuilder("rid_n=");
                sb7.append(pVar.t);
                sb7.append(RequestParameters.AMPERSAND);
                stringBuffer.append(sb7.toString());
                StringBuilder sb8 = new StringBuilder("unit_id=");
                sb8.append(pVar.u);
                sb8.append(RequestParameters.AMPERSAND);
                stringBuffer.append(sb8.toString());
                StringBuilder sb9 = new StringBuilder("offer_url=");
                sb9.append(pVar.l);
                stringBuffer.append(sb9.toString());
                stringBuffer.append("\n");
            } else {
                StringBuilder sb10 = new StringBuilder("key=");
                sb10.append(pVar.f);
                sb10.append(RequestParameters.AMPERSAND);
                stringBuffer.append(sb10.toString());
                StringBuilder sb11 = new StringBuilder("reason=");
                sb11.append(pVar.m);
                sb11.append(RequestParameters.AMPERSAND);
                stringBuffer.append(sb11.toString());
                StringBuilder sb12 = new StringBuilder("cid=");
                sb12.append(pVar.v);
                sb12.append(RequestParameters.AMPERSAND);
                stringBuffer.append(sb12.toString());
                StringBuilder sb13 = new StringBuilder("video_url=");
                sb13.append(pVar.E);
                sb13.append(RequestParameters.AMPERSAND);
                stringBuffer.append(sb13.toString());
                StringBuilder sb14 = new StringBuilder("rid_n=");
                sb14.append(pVar.t);
                sb14.append(RequestParameters.AMPERSAND);
                stringBuffer.append(sb14.toString());
                StringBuilder sb15 = new StringBuilder("unit_id=");
                sb15.append(pVar.u);
                sb15.append(RequestParameters.AMPERSAND);
                stringBuffer.append(sb15.toString());
                StringBuilder sb16 = new StringBuilder("offer_url=");
                sb16.append(pVar.l);
                stringBuffer.append(sb16.toString());
                stringBuffer.append("\n");
            }
        }
        return stringBuffer.toString();
    }

    public static String c(List<p> list) {
        if (list == null || list.size() <= 0) {
            return null;
        }
        StringBuffer stringBuffer = new StringBuffer();
        for (p pVar : list) {
            a.a();
            if (a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA)) {
                StringBuilder sb = new StringBuilder("key=");
                sb.append(pVar.f);
                sb.append(RequestParameters.AMPERSAND);
                stringBuffer.append(sb.toString());
                StringBuilder sb2 = new StringBuilder("network_type=");
                sb2.append(pVar.g);
                sb2.append(RequestParameters.AMPERSAND);
                stringBuffer.append(sb2.toString());
                StringBuilder sb3 = new StringBuilder("network_str=");
                sb3.append(pVar.h);
                sb3.append(RequestParameters.AMPERSAND);
                stringBuffer.append(sb3.toString());
                StringBuilder sb4 = new StringBuilder("result=");
                sb4.append(pVar.o);
                sb4.append(RequestParameters.AMPERSAND);
                stringBuffer.append(sb4.toString());
                StringBuilder sb5 = new StringBuilder("duration=");
                sb5.append(pVar.p);
                sb5.append(RequestParameters.AMPERSAND);
                stringBuffer.append(sb5.toString());
                StringBuilder sb6 = new StringBuilder("video_size=");
                sb6.append(pVar.q);
                sb6.append(RequestParameters.AMPERSAND);
                stringBuffer.append(sb6.toString());
                StringBuilder sb7 = new StringBuilder("video_length=");
                sb7.append(pVar.k);
                sb7.append(RequestParameters.AMPERSAND);
                stringBuffer.append(sb7.toString());
                StringBuilder sb8 = new StringBuilder("reason=");
                sb8.append(pVar.m);
                sb8.append(RequestParameters.AMPERSAND);
                stringBuffer.append(sb8.toString());
                StringBuilder sb9 = new StringBuilder("cid=");
                sb9.append(pVar.v);
                sb9.append(RequestParameters.AMPERSAND);
                stringBuffer.append(sb9.toString());
                StringBuilder sb10 = new StringBuilder("video_url=");
                sb10.append(pVar.E);
                sb10.append(RequestParameters.AMPERSAND);
                stringBuffer.append(sb10.toString());
                StringBuilder sb11 = new StringBuilder("rid_n=");
                sb11.append(pVar.t);
                sb11.append(RequestParameters.AMPERSAND);
                stringBuffer.append(sb11.toString());
                StringBuilder sb12 = new StringBuilder("unit_id=");
                sb12.append(pVar.u);
                sb12.append(RequestParameters.AMPERSAND);
                stringBuffer.append(sb12.toString());
                StringBuilder sb13 = new StringBuilder("offer_url=");
                sb13.append(pVar.l);
                sb13.append(RequestParameters.AMPERSAND);
                stringBuffer.append(sb13.toString());
            } else {
                StringBuilder sb14 = new StringBuilder("key=");
                sb14.append(pVar.f);
                sb14.append(RequestParameters.AMPERSAND);
                stringBuffer.append(sb14.toString());
                StringBuilder sb15 = new StringBuilder("result=");
                sb15.append(pVar.o);
                sb15.append(RequestParameters.AMPERSAND);
                stringBuffer.append(sb15.toString());
                StringBuilder sb16 = new StringBuilder("duration=");
                sb16.append(pVar.p);
                sb16.append(RequestParameters.AMPERSAND);
                stringBuffer.append(sb16.toString());
                StringBuilder sb17 = new StringBuilder("video_size=");
                sb17.append(pVar.q);
                sb17.append(RequestParameters.AMPERSAND);
                stringBuffer.append(sb17.toString());
                StringBuilder sb18 = new StringBuilder("video_length=");
                sb18.append(pVar.k);
                sb18.append(RequestParameters.AMPERSAND);
                stringBuffer.append(sb18.toString());
                StringBuilder sb19 = new StringBuilder("reason=");
                sb19.append(pVar.m);
                sb19.append(RequestParameters.AMPERSAND);
                stringBuffer.append(sb19.toString());
                StringBuilder sb20 = new StringBuilder("cid=");
                sb20.append(pVar.v);
                sb20.append(RequestParameters.AMPERSAND);
                stringBuffer.append(sb20.toString());
                StringBuilder sb21 = new StringBuilder("video_url=");
                sb21.append(pVar.E);
                sb21.append(RequestParameters.AMPERSAND);
                stringBuffer.append(sb21.toString());
                StringBuilder sb22 = new StringBuilder("rid_n=");
                sb22.append(pVar.t);
                sb22.append(RequestParameters.AMPERSAND);
                stringBuffer.append(sb22.toString());
                StringBuilder sb23 = new StringBuilder("unit_id=");
                sb23.append(pVar.u);
                sb23.append(RequestParameters.AMPERSAND);
                stringBuffer.append(sb23.toString());
                StringBuilder sb24 = new StringBuilder("offer_url=");
                sb24.append(pVar.l);
                sb24.append(RequestParameters.AMPERSAND);
                stringBuffer.append(sb24.toString());
            }
            if (pVar.c != null) {
                StringBuilder sb25 = new StringBuilder("resource_type=");
                sb25.append(pVar.c);
                sb25.append(RequestParameters.AMPERSAND);
                stringBuffer.append(sb25.toString());
            }
            if (pVar.e != null) {
                StringBuilder sb26 = new StringBuilder("creative=");
                sb26.append(pVar.e);
                sb26.append(RequestParameters.AMPERSAND);
                stringBuffer.append(sb26.toString());
            }
            stringBuffer.append("\n");
        }
        return stringBuffer.toString();
    }

    public static String a(p pVar) {
        if (pVar == null) {
            return null;
        }
        StringBuffer stringBuffer = new StringBuffer();
        StringBuilder sb = new StringBuilder("key=");
        sb.append(pVar.f);
        sb.append(RequestParameters.AMPERSAND);
        stringBuffer.append(sb.toString());
        StringBuilder sb2 = new StringBuilder("cid=");
        sb2.append(pVar.v);
        sb2.append(RequestParameters.AMPERSAND);
        stringBuffer.append(sb2.toString());
        StringBuilder sb3 = new StringBuilder("unit_id=");
        sb3.append(pVar.u);
        sb3.append(RequestParameters.AMPERSAND);
        stringBuffer.append(sb3.toString());
        StringBuilder sb4 = new StringBuilder("network_type=");
        sb4.append(pVar.g);
        sb4.append(RequestParameters.AMPERSAND);
        stringBuffer.append(sb4.toString());
        StringBuilder sb5 = new StringBuilder("rid_n=");
        sb5.append(pVar.t);
        sb5.append(RequestParameters.AMPERSAND);
        stringBuffer.append(sb5.toString());
        StringBuilder sb6 = new StringBuilder("reason=");
        sb6.append(pVar.m);
        stringBuffer.append(sb6.toString());
        stringBuffer.append("\n");
        return stringBuffer.toString();
    }

    public static String b(p pVar) {
        if (pVar == null) {
            return null;
        }
        StringBuffer stringBuffer = new StringBuffer();
        StringBuilder sb = new StringBuilder("key=");
        sb.append(pVar.f);
        sb.append(RequestParameters.AMPERSAND);
        stringBuffer.append(sb.toString());
        StringBuilder sb2 = new StringBuilder("cid=");
        sb2.append(pVar.v);
        sb2.append(RequestParameters.AMPERSAND);
        stringBuffer.append(sb2.toString());
        StringBuilder sb3 = new StringBuilder("unit_id=");
        sb3.append(pVar.u);
        sb3.append(RequestParameters.AMPERSAND);
        stringBuffer.append(sb3.toString());
        StringBuilder sb4 = new StringBuilder("network_type=");
        sb4.append(pVar.g);
        sb4.append(RequestParameters.AMPERSAND);
        stringBuffer.append(sb4.toString());
        StringBuilder sb5 = new StringBuilder("mraid_type=");
        sb5.append(pVar.H);
        sb5.append(RequestParameters.AMPERSAND);
        stringBuffer.append(sb5.toString());
        StringBuilder sb6 = new StringBuilder("rid_n=");
        sb6.append(pVar.t);
        stringBuffer.append(sb6.toString());
        stringBuffer.append("\n");
        return stringBuffer.toString();
    }

    public static String c(p pVar) {
        if (pVar == null) {
            return null;
        }
        StringBuffer stringBuffer = new StringBuffer();
        StringBuilder sb = new StringBuilder("key=");
        sb.append(pVar.f);
        sb.append(RequestParameters.AMPERSAND);
        stringBuffer.append(sb.toString());
        StringBuilder sb2 = new StringBuilder("result=");
        sb2.append(pVar.o);
        sb2.append(RequestParameters.AMPERSAND);
        stringBuffer.append(sb2.toString());
        StringBuilder sb3 = new StringBuilder("duration=");
        sb3.append(pVar.p);
        sb3.append(RequestParameters.AMPERSAND);
        stringBuffer.append(sb3.toString());
        StringBuilder sb4 = new StringBuilder("endcard_url=");
        sb4.append(pVar.D);
        sb4.append(RequestParameters.AMPERSAND);
        stringBuffer.append(sb4.toString());
        StringBuilder sb5 = new StringBuilder("cid=");
        sb5.append(pVar.v);
        sb5.append(RequestParameters.AMPERSAND);
        stringBuffer.append(sb5.toString());
        StringBuilder sb6 = new StringBuilder("reason=");
        sb6.append(pVar.m);
        sb6.append(RequestParameters.AMPERSAND);
        stringBuffer.append(sb6.toString());
        StringBuilder sb7 = new StringBuilder("type=");
        sb7.append(pVar.C);
        sb7.append(RequestParameters.AMPERSAND);
        stringBuffer.append(sb7.toString());
        StringBuilder sb8 = new StringBuilder("ad_type=");
        sb8.append(pVar.G);
        sb8.append(RequestParameters.AMPERSAND);
        stringBuffer.append(sb8.toString());
        StringBuilder sb9 = new StringBuilder("unit_id=");
        sb9.append(pVar.u);
        sb9.append(RequestParameters.AMPERSAND);
        stringBuffer.append(sb9.toString());
        StringBuilder sb10 = new StringBuilder("devid=");
        sb10.append(pVar.d);
        sb10.append(RequestParameters.AMPERSAND);
        stringBuffer.append(sb10.toString());
        StringBuilder sb11 = new StringBuilder("mraid_type=");
        sb11.append(pVar.H);
        sb11.append(RequestParameters.AMPERSAND);
        stringBuffer.append(sb11.toString());
        StringBuilder sb12 = new StringBuilder("network_type=");
        sb12.append(pVar.g);
        sb12.append(RequestParameters.AMPERSAND);
        stringBuffer.append(sb12.toString());
        if (pVar.c != null) {
            StringBuilder sb13 = new StringBuilder("resource_type=");
            sb13.append(pVar.c);
            sb13.append(RequestParameters.AMPERSAND);
            stringBuffer.append(sb13.toString());
        }
        StringBuilder sb14 = new StringBuilder("rid_n=");
        sb14.append(pVar.t);
        stringBuffer.append(sb14.toString());
        stringBuffer.append("\n");
        return stringBuffer.toString();
    }

    public static String d(p pVar) {
        if (pVar == null) {
            return null;
        }
        StringBuffer stringBuffer = new StringBuffer();
        StringBuilder sb = new StringBuilder("key=");
        sb.append(pVar.f);
        sb.append(RequestParameters.AMPERSAND);
        stringBuffer.append(sb.toString());
        StringBuilder sb2 = new StringBuilder("result=");
        sb2.append(pVar.o);
        sb2.append(RequestParameters.AMPERSAND);
        stringBuffer.append(sb2.toString());
        StringBuilder sb3 = new StringBuilder("duration=");
        sb3.append(pVar.p);
        sb3.append(RequestParameters.AMPERSAND);
        stringBuffer.append(sb3.toString());
        StringBuilder sb4 = new StringBuilder("cid=");
        sb4.append(pVar.v);
        sb4.append(RequestParameters.AMPERSAND);
        stringBuffer.append(sb4.toString());
        StringBuilder sb5 = new StringBuilder("unit_id=");
        sb5.append(pVar.u);
        sb5.append(RequestParameters.AMPERSAND);
        stringBuffer.append(sb5.toString());
        StringBuilder sb6 = new StringBuilder("reason=");
        sb6.append(pVar.m);
        sb6.append(RequestParameters.AMPERSAND);
        stringBuffer.append(sb6.toString());
        StringBuilder sb7 = new StringBuilder("ad_type=");
        sb7.append(pVar.G);
        sb7.append(RequestParameters.AMPERSAND);
        stringBuffer.append(sb7.toString());
        StringBuilder sb8 = new StringBuilder("rid_n=");
        sb8.append(pVar.t);
        sb8.append(RequestParameters.AMPERSAND);
        stringBuffer.append(sb8.toString());
        StringBuilder sb9 = new StringBuilder("network_type=");
        sb9.append(pVar.g);
        sb9.append(RequestParameters.AMPERSAND);
        stringBuffer.append(sb9.toString());
        StringBuilder sb10 = new StringBuilder("mraid_type=");
        sb10.append(pVar.H);
        sb10.append(RequestParameters.AMPERSAND);
        stringBuffer.append(sb10.toString());
        StringBuilder sb11 = new StringBuilder("devid=");
        sb11.append(pVar.d);
        sb11.append(RequestParameters.AMPERSAND);
        stringBuffer.append(sb11.toString());
        if (pVar.c != null) {
            StringBuilder sb12 = new StringBuilder("resource_type=");
            sb12.append(pVar.c);
            sb12.append(RequestParameters.AMPERSAND);
            stringBuffer.append(sb12.toString());
        }
        if (!TextUtils.isEmpty(pVar.D)) {
            StringBuilder sb13 = new StringBuilder("endcard_url=");
            sb13.append(pVar.D);
            sb13.append(RequestParameters.AMPERSAND);
            stringBuffer.append(sb13.toString());
        }
        StringBuilder sb14 = new StringBuilder("type=");
        sb14.append(pVar.C);
        stringBuffer.append(sb14.toString());
        stringBuffer.append("\n");
        return stringBuffer.toString();
    }

    public static String d(List<p> list) {
        if (list == null || list.size() <= 0) {
            return null;
        }
        StringBuffer stringBuffer = new StringBuffer();
        for (p pVar : list) {
            StringBuilder sb = new StringBuilder("key=");
            sb.append(pVar.f);
            sb.append(RequestParameters.AMPERSAND);
            stringBuffer.append(sb.toString());
            StringBuilder sb2 = new StringBuilder("result=");
            sb2.append(pVar.o);
            sb2.append(RequestParameters.AMPERSAND);
            stringBuffer.append(sb2.toString());
            StringBuilder sb3 = new StringBuilder("duration=");
            sb3.append(pVar.p);
            sb3.append(RequestParameters.AMPERSAND);
            stringBuffer.append(sb3.toString());
            StringBuilder sb4 = new StringBuilder("endcard_url=");
            sb4.append(pVar.D);
            sb4.append(RequestParameters.AMPERSAND);
            stringBuffer.append(sb4.toString());
            StringBuilder sb5 = new StringBuilder("cid=");
            sb5.append(pVar.v);
            sb5.append(RequestParameters.AMPERSAND);
            stringBuffer.append(sb5.toString());
            StringBuilder sb6 = new StringBuilder("unit_id=");
            sb6.append(pVar.u);
            sb6.append(RequestParameters.AMPERSAND);
            stringBuffer.append(sb6.toString());
            StringBuilder sb7 = new StringBuilder("reason=");
            sb7.append(pVar.m);
            sb7.append(RequestParameters.AMPERSAND);
            stringBuffer.append(sb7.toString());
            StringBuilder sb8 = new StringBuilder("ad_type=");
            sb8.append(pVar.G);
            sb8.append(RequestParameters.AMPERSAND);
            stringBuffer.append(sb8.toString());
            StringBuilder sb9 = new StringBuilder("rid_n=");
            sb9.append(pVar.t);
            sb9.append(RequestParameters.AMPERSAND);
            stringBuffer.append(sb9.toString());
            StringBuilder sb10 = new StringBuilder("type=");
            sb10.append(pVar.C);
            stringBuffer.append(sb10.toString());
            stringBuffer.append("\n");
        }
        return stringBuffer.toString();
    }

    public static String e(List<p> list) {
        if (list == null || list.size() <= 0) {
            return null;
        }
        StringBuffer stringBuffer = new StringBuffer();
        for (p pVar : list) {
            a.a();
            if (a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA)) {
                StringBuilder sb = new StringBuilder("key=");
                sb.append(pVar.f);
                sb.append(RequestParameters.AMPERSAND);
                stringBuffer.append(sb.toString());
                StringBuilder sb2 = new StringBuilder("network_type=");
                sb2.append(pVar.g);
                sb2.append(RequestParameters.AMPERSAND);
                stringBuffer.append(sb2.toString());
                StringBuilder sb3 = new StringBuilder("result=");
                sb3.append(pVar.o);
                sb3.append(RequestParameters.AMPERSAND);
                stringBuffer.append(sb3.toString());
                StringBuilder sb4 = new StringBuilder("cid=");
                sb4.append(pVar.v);
                sb4.append(RequestParameters.AMPERSAND);
                stringBuffer.append(sb4.toString());
                StringBuilder sb5 = new StringBuilder("template_url=");
                sb5.append(pVar.w);
                sb5.append(RequestParameters.AMPERSAND);
                stringBuffer.append(sb5.toString());
                StringBuilder sb6 = new StringBuilder("reason=");
                sb6.append(pVar.m);
                sb6.append(RequestParameters.AMPERSAND);
                stringBuffer.append(sb6.toString());
                StringBuilder sb7 = new StringBuilder("rid_n=");
                sb7.append(pVar.t);
                sb7.append(RequestParameters.AMPERSAND);
                stringBuffer.append(sb7.toString());
                StringBuilder sb8 = new StringBuilder("unit_id=");
                sb8.append(pVar.u);
                sb8.append(RequestParameters.AMPERSAND);
                stringBuffer.append(sb8.toString());
                stringBuffer.append("\n");
            } else {
                StringBuilder sb9 = new StringBuilder("key=");
                sb9.append(pVar.f);
                sb9.append(RequestParameters.AMPERSAND);
                stringBuffer.append(sb9.toString());
                StringBuilder sb10 = new StringBuilder("result=");
                sb10.append(pVar.o);
                sb10.append(RequestParameters.AMPERSAND);
                stringBuffer.append(sb10.toString());
                StringBuilder sb11 = new StringBuilder("cid=");
                sb11.append(pVar.v);
                sb11.append(RequestParameters.AMPERSAND);
                stringBuffer.append(sb11.toString());
                StringBuilder sb12 = new StringBuilder("template_url=");
                sb12.append(pVar.w);
                sb12.append(RequestParameters.AMPERSAND);
                stringBuffer.append(sb12.toString());
                StringBuilder sb13 = new StringBuilder("reason=");
                sb13.append(pVar.m);
                sb13.append(RequestParameters.AMPERSAND);
                stringBuffer.append(sb13.toString());
                StringBuilder sb14 = new StringBuilder("rid_n=");
                sb14.append(pVar.t);
                sb14.append(RequestParameters.AMPERSAND);
                stringBuffer.append(sb14.toString());
                StringBuilder sb15 = new StringBuilder("unit_id=");
                sb15.append(pVar.u);
                sb15.append(RequestParameters.AMPERSAND);
                stringBuffer.append(sb15.toString());
                stringBuffer.append("\n");
            }
        }
        return stringBuffer.toString();
    }

    public static String f(List<p> list) {
        if (list == null || list.size() <= 0) {
            return null;
        }
        StringBuffer stringBuffer = new StringBuffer();
        for (p pVar : list) {
            a.a();
            if (a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA)) {
                StringBuilder sb = new StringBuilder("key=");
                sb.append(pVar.f);
                sb.append(RequestParameters.AMPERSAND);
                stringBuffer.append(sb.toString());
                StringBuilder sb2 = new StringBuilder("network_type=");
                sb2.append(pVar.g);
                sb2.append(RequestParameters.AMPERSAND);
                stringBuffer.append(sb2.toString());
                StringBuilder sb3 = new StringBuilder("cid=");
                sb3.append(pVar.v);
                sb3.append(RequestParameters.AMPERSAND);
                stringBuffer.append(sb3.toString());
                StringBuilder sb4 = new StringBuilder("image_url=");
                sb4.append(pVar.F);
                sb4.append(RequestParameters.AMPERSAND);
                stringBuffer.append(sb4.toString());
                StringBuilder sb5 = new StringBuilder("reason=");
                sb5.append(pVar.m);
                sb5.append(RequestParameters.AMPERSAND);
                stringBuffer.append(sb5.toString());
                StringBuilder sb6 = new StringBuilder("rid_n=");
                sb6.append(pVar.t);
                sb6.append(RequestParameters.AMPERSAND);
                stringBuffer.append(sb6.toString());
                StringBuilder sb7 = new StringBuilder("unit_id=");
                sb7.append(pVar.u);
                sb7.append(RequestParameters.AMPERSAND);
                stringBuffer.append(sb7.toString());
                stringBuffer.append("\n");
            } else {
                StringBuilder sb8 = new StringBuilder("key=");
                sb8.append(pVar.f);
                sb8.append(RequestParameters.AMPERSAND);
                stringBuffer.append(sb8.toString());
                StringBuilder sb9 = new StringBuilder("cid=");
                sb9.append(pVar.v);
                sb9.append(RequestParameters.AMPERSAND);
                stringBuffer.append(sb9.toString());
                StringBuilder sb10 = new StringBuilder("image_url=");
                sb10.append(pVar.F);
                sb10.append(RequestParameters.AMPERSAND);
                stringBuffer.append(sb10.toString());
                StringBuilder sb11 = new StringBuilder("reason=");
                sb11.append(pVar.m);
                sb11.append(RequestParameters.AMPERSAND);
                stringBuffer.append(sb11.toString());
                StringBuilder sb12 = new StringBuilder("rid_n=");
                sb12.append(pVar.t);
                sb12.append(RequestParameters.AMPERSAND);
                stringBuffer.append(sb12.toString());
                StringBuilder sb13 = new StringBuilder("unit_id=");
                sb13.append(pVar.u);
                sb13.append(RequestParameters.AMPERSAND);
                stringBuffer.append(sb13.toString());
                stringBuffer.append("\n");
            }
        }
        return stringBuffer.toString();
    }

    public static String e(p pVar) {
        try {
            StringBuffer stringBuffer = new StringBuffer();
            a.a();
            if (a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA)) {
                StringBuilder sb = new StringBuilder("key=");
                sb.append(pVar.f);
                sb.append(RequestParameters.AMPERSAND);
                stringBuffer.append(sb.toString());
                StringBuilder sb2 = new StringBuilder("network_type=");
                sb2.append(pVar.g);
                sb2.append(RequestParameters.AMPERSAND);
                stringBuffer.append(sb2.toString());
                StringBuilder sb3 = new StringBuilder("network_str=");
                sb3.append(pVar.h);
                sb3.append(RequestParameters.AMPERSAND);
                stringBuffer.append(sb3.toString());
                StringBuilder sb4 = new StringBuilder("video_length=");
                sb4.append(pVar.k);
                sb4.append(RequestParameters.AMPERSAND);
                stringBuffer.append(sb4.toString());
                StringBuilder sb5 = new StringBuilder("ctype=");
                sb5.append(pVar.s);
                sb5.append(RequestParameters.AMPERSAND);
                stringBuffer.append(sb5.toString());
            } else {
                StringBuilder sb6 = new StringBuilder("key=");
                sb6.append(pVar.f);
                sb6.append(RequestParameters.AMPERSAND);
                stringBuffer.append(sb6.toString());
                StringBuilder sb7 = new StringBuilder("video_length=");
                sb7.append(pVar.k);
                sb7.append(RequestParameters.AMPERSAND);
                stringBuffer.append(sb7.toString());
                StringBuilder sb8 = new StringBuilder("ctype=");
                sb8.append(pVar.s);
                sb8.append(RequestParameters.AMPERSAND);
                stringBuffer.append(sb8.toString());
            }
            if (!TextUtils.isEmpty(pVar.l)) {
                StringBuilder sb9 = new StringBuilder("offer_url=");
                sb9.append(URLEncoder.encode(pVar.l, "utf-8"));
                sb9.append(RequestParameters.AMPERSAND);
                stringBuffer.append(sb9.toString());
            }
            StringBuilder sb10 = new StringBuilder("time=");
            sb10.append(pVar.r);
            stringBuffer.append(sb10.toString());
            return stringBuffer.toString();
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public static String f(p pVar) {
        try {
            StringBuffer stringBuffer = new StringBuffer();
            a.a();
            if (a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA)) {
                StringBuilder sb = new StringBuilder("key=");
                sb.append(pVar.f);
                sb.append(RequestParameters.AMPERSAND);
                stringBuffer.append(sb.toString());
                StringBuilder sb2 = new StringBuilder("error=");
                sb2.append(k.d(pVar.y));
                sb2.append(RequestParameters.AMPERSAND);
                stringBuffer.append(sb2.toString());
                StringBuilder sb3 = new StringBuilder("template_url=");
                sb3.append(k.d(pVar.w));
                sb3.append(RequestParameters.AMPERSAND);
                stringBuffer.append(sb3.toString());
                StringBuilder sb4 = new StringBuilder("unit_id=");
                sb4.append(k.d(pVar.u));
                sb4.append(RequestParameters.AMPERSAND);
                stringBuffer.append(sb4.toString());
                StringBuilder sb5 = new StringBuilder("cid=");
                sb5.append(k.d(pVar.v));
                sb5.append(RequestParameters.AMPERSAND);
                stringBuffer.append(sb5.toString());
                StringBuilder sb6 = new StringBuilder("network_str=");
                sb6.append(pVar.h);
                sb6.append(RequestParameters.AMPERSAND);
                stringBuffer.append(sb6.toString());
                StringBuilder sb7 = new StringBuilder("network_type=");
                sb7.append(pVar.g);
                stringBuffer.append(sb7.toString());
            } else {
                StringBuilder sb8 = new StringBuilder("key=");
                sb8.append(pVar.f);
                sb8.append(RequestParameters.AMPERSAND);
                stringBuffer.append(sb8.toString());
                StringBuilder sb9 = new StringBuilder("error=");
                sb9.append(k.d(pVar.y));
                sb9.append(RequestParameters.AMPERSAND);
                stringBuffer.append(sb9.toString());
                StringBuilder sb10 = new StringBuilder("template_url=");
                sb10.append(k.d(pVar.w));
                sb10.append(RequestParameters.AMPERSAND);
                stringBuffer.append(sb10.toString());
                StringBuilder sb11 = new StringBuilder("unit_id=");
                sb11.append(k.d(pVar.u));
                sb11.append(RequestParameters.AMPERSAND);
                stringBuffer.append(sb11.toString());
                StringBuilder sb12 = new StringBuilder("cid=");
                sb12.append(k.d(pVar.v));
                sb12.append(RequestParameters.AMPERSAND);
                stringBuffer.append(sb12.toString());
            }
            return stringBuffer.toString();
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public static String g(p pVar) {
        if (pVar != null) {
            try {
                StringBuffer stringBuffer = new StringBuffer();
                a.a();
                if (a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA)) {
                    StringBuilder sb = new StringBuilder("key=");
                    sb.append(pVar.f);
                    sb.append(RequestParameters.AMPERSAND);
                    stringBuffer.append(sb.toString());
                    StringBuilder sb2 = new StringBuilder("event=");
                    sb2.append(k.d(pVar.z));
                    sb2.append(RequestParameters.AMPERSAND);
                    stringBuffer.append(sb2.toString());
                    StringBuilder sb3 = new StringBuilder("template=");
                    sb3.append(k.d(pVar.x));
                    sb3.append(RequestParameters.AMPERSAND);
                    stringBuffer.append(sb3.toString());
                    StringBuilder sb4 = new StringBuilder("layout=");
                    sb4.append(k.d(pVar.A));
                    sb4.append(RequestParameters.AMPERSAND);
                    stringBuffer.append(sb4.toString());
                    StringBuilder sb5 = new StringBuilder("unit_id=");
                    sb5.append(k.d(pVar.u));
                    sb5.append(RequestParameters.AMPERSAND);
                    stringBuffer.append(sb5.toString());
                    StringBuilder sb6 = new StringBuilder("cid=");
                    sb6.append(k.d(pVar.v));
                    sb6.append(RequestParameters.AMPERSAND);
                    stringBuffer.append(sb6.toString());
                    StringBuilder sb7 = new StringBuilder("network_str=");
                    sb7.append(pVar.h);
                    sb7.append(RequestParameters.AMPERSAND);
                    stringBuffer.append(sb7.toString());
                    StringBuilder sb8 = new StringBuilder("network_type=");
                    sb8.append(pVar.g);
                    stringBuffer.append(sb8.toString());
                } else {
                    StringBuilder sb9 = new StringBuilder("key=");
                    sb9.append(pVar.f);
                    sb9.append(RequestParameters.AMPERSAND);
                    stringBuffer.append(sb9.toString());
                    StringBuilder sb10 = new StringBuilder("event=");
                    sb10.append(k.d(pVar.z));
                    sb10.append(RequestParameters.AMPERSAND);
                    stringBuffer.append(sb10.toString());
                    StringBuilder sb11 = new StringBuilder("template=");
                    sb11.append(k.d(pVar.x));
                    sb11.append(RequestParameters.AMPERSAND);
                    stringBuffer.append(sb11.toString());
                    StringBuilder sb12 = new StringBuilder("layout=");
                    sb12.append(k.d(pVar.A));
                    sb12.append(RequestParameters.AMPERSAND);
                    stringBuffer.append(sb12.toString());
                    StringBuilder sb13 = new StringBuilder("unit_id=");
                    sb13.append(k.d(pVar.u));
                    sb13.append(RequestParameters.AMPERSAND);
                    stringBuffer.append(sb13.toString());
                    StringBuilder sb14 = new StringBuilder("cid=");
                    sb14.append(k.d(pVar.v));
                    sb14.append(RequestParameters.AMPERSAND);
                    stringBuffer.append(sb14.toString());
                }
                return stringBuffer.toString();
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        return null;
    }

    public static String g(List<p> list) {
        if (list != null) {
            try {
                if (list.size() > 0) {
                    StringBuffer stringBuffer = new StringBuffer();
                    for (p pVar : list) {
                        stringBuffer.append(pVar.B);
                        stringBuffer.append("\n");
                    }
                    return stringBuffer.toString();
                }
            } catch (Throwable th) {
                g.c("VideoReportData", th.getMessage(), th);
            }
        }
        return null;
    }

    public static String h(p pVar) {
        if (pVar == null) {
            return "";
        }
        StringBuffer stringBuffer = new StringBuffer();
        StringBuilder sb = new StringBuilder("key=");
        sb.append(pVar.f);
        sb.append(RequestParameters.AMPERSAND);
        stringBuffer.append(sb.toString());
        StringBuilder sb2 = new StringBuilder("cid=");
        sb2.append(pVar.v);
        sb2.append(RequestParameters.AMPERSAND);
        stringBuffer.append(sb2.toString());
        StringBuilder sb3 = new StringBuilder("rid_n=");
        sb3.append(pVar.t);
        sb3.append(RequestParameters.AMPERSAND);
        stringBuffer.append(sb3.toString());
        StringBuilder sb4 = new StringBuilder("unit_id=");
        sb4.append(pVar.u);
        sb4.append(RequestParameters.AMPERSAND);
        stringBuffer.append(sb4.toString());
        StringBuilder sb5 = new StringBuilder("network_type=");
        sb5.append(pVar.g);
        sb5.append(RequestParameters.AMPERSAND);
        stringBuffer.append(sb5.toString());
        StringBuilder sb6 = new StringBuilder("mraid_type=");
        sb6.append(pVar.H);
        sb6.append(RequestParameters.AMPERSAND);
        stringBuffer.append(sb6.toString());
        stringBuffer.append("platform=1");
        return stringBuffer.toString();
    }

    public static String i(p pVar) {
        if (pVar == null) {
            return "";
        }
        StringBuffer stringBuffer = new StringBuffer();
        StringBuilder sb = new StringBuilder("key=");
        sb.append(pVar.f);
        sb.append(RequestParameters.AMPERSAND);
        stringBuffer.append(sb.toString());
        StringBuilder sb2 = new StringBuilder("cid=");
        sb2.append(pVar.v);
        sb2.append(RequestParameters.AMPERSAND);
        stringBuffer.append(sb2.toString());
        StringBuilder sb3 = new StringBuilder("rid_n=");
        sb3.append(pVar.t);
        sb3.append(RequestParameters.AMPERSAND);
        stringBuffer.append(sb3.toString());
        StringBuilder sb4 = new StringBuilder("unit_id=");
        sb4.append(pVar.u);
        sb4.append(RequestParameters.AMPERSAND);
        stringBuffer.append(sb4.toString());
        StringBuilder sb5 = new StringBuilder("reason=");
        sb5.append(pVar.m);
        sb5.append(RequestParameters.AMPERSAND);
        stringBuffer.append(sb5.toString());
        StringBuilder sb6 = new StringBuilder("case=");
        sb6.append(pVar.n);
        sb6.append(RequestParameters.AMPERSAND);
        stringBuffer.append(sb6.toString());
        StringBuilder sb7 = new StringBuilder("network_type=");
        sb7.append(pVar.g);
        stringBuffer.append(sb7.toString());
        return stringBuffer.toString();
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder("RewardReportData [key=");
        sb.append(this.f);
        sb.append(", networkType=");
        sb.append(this.g);
        sb.append(", isCompleteView=");
        sb.append(this.i);
        sb.append(", watchedMillis=");
        sb.append(this.j);
        sb.append(", videoLength=");
        sb.append(this.k);
        sb.append(", offerUrl=");
        sb.append(this.l);
        sb.append(", reason=");
        sb.append(this.m);
        sb.append(", result=");
        sb.append(this.o);
        sb.append(", duration=");
        sb.append(this.p);
        sb.append(", videoSize=");
        sb.append(this.q);
        sb.append(RequestParameters.RIGHT_BRACKETS);
        return sb.toString();
    }
}
