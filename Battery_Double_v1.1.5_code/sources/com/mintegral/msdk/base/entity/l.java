package com.mintegral.msdk.base.entity;

import com.mintegral.msdk.base.utils.a;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.base.utils.k;
import com.mintegral.msdk.base.utils.s;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

/* compiled from: PBInfo */
public final class l {

    /* renamed from: a reason: collision with root package name */
    public String f2601a;
    public String b;
    public String c = "0";

    public static String a(String str, List<String> list, List<String> list2, List<String> list3, List<String> list4, boolean z) {
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("key=2000042");
            StringBuilder sb2 = new StringBuilder();
            String b2 = k.b(list);
            String b3 = k.b(list2);
            String b4 = k.b(list3);
            String b5 = k.b(list4);
            StringBuilder sb3 = new StringBuilder("pid=");
            sb3.append(str);
            sb2.append(sb3.toString());
            StringBuilder sb4 = new StringBuilder("&full=");
            sb4.append(b2);
            sb2.append(sb4.toString());
            StringBuilder sb5 = new StringBuilder("&add=");
            sb5.append(b3);
            sb2.append(sb5.toString());
            StringBuilder sb6 = new StringBuilder("&dele=");
            sb6.append(b4);
            sb2.append(sb6.toString());
            StringBuilder sb7 = new StringBuilder("&detail=");
            sb7.append(b5);
            sb2.append(sb7.toString());
            String str2 = "1";
            if (!z) {
                str2 = "0";
            }
            StringBuilder sb8 = new StringBuilder("&first_time=");
            sb8.append(str2);
            sb2.append(sb8.toString());
            String b6 = a.b(sb2.toString());
            StringBuilder sb9 = new StringBuilder("content:");
            sb9.append(sb2.toString());
            g.b("PBInfo", sb9.toString());
            StringBuilder sb10 = new StringBuilder("&content=");
            sb10.append(URLEncoder.encode(b6, "utf-8"));
            sb.append(sb10.toString());
            String sb11 = sb.toString();
            StringBuilder sb12 = new StringBuilder("pbSb report:");
            sb12.append(sb11);
            g.d("PBInfo", sb12.toString());
            return sb11;
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static List<String> a(List<l> list) {
        ArrayList arrayList = new ArrayList();
        if (list == null) {
            try {
                if (list.size() > 0) {
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return arrayList;
        }
        for (l lVar : list) {
            if (s.b(lVar.f2601a)) {
                arrayList.add(lVar.f2601a);
            }
        }
        return arrayList;
    }
}
