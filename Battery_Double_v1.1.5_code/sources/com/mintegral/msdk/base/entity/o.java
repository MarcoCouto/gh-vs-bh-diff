package com.mintegral.msdk.base.entity;

/* compiled from: VideoBean */
public final class o {

    /* renamed from: a reason: collision with root package name */
    private String f2604a;
    private long b;
    private int c;
    private int d;
    private long e;

    public final long a() {
        return this.e;
    }

    public final void a(long j) {
        this.e = j;
    }

    public final void a(String str) {
        this.f2604a = str;
    }

    public final long b() {
        return this.b;
    }

    public final void b(long j) {
        this.b = j;
    }

    public final int c() {
        return this.c;
    }

    public final void a(int i) {
        this.c = i;
    }

    public final int d() {
        return this.d;
    }

    public final void b(int i) {
        this.d = i;
    }
}
