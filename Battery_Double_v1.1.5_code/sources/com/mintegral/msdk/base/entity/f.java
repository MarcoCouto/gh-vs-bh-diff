package com.mintegral.msdk.base.entity;

/* compiled from: Frequence */
public final class f {

    /* renamed from: a reason: collision with root package name */
    private String f2595a;
    private int b;
    private int c;
    private int d;
    private int e;
    private long f;

    public final String a() {
        return this.f2595a;
    }

    public final void a(String str) {
        this.f2595a = str;
    }

    public final int b() {
        return this.b;
    }

    public final void a(int i) {
        this.b = i;
    }

    public final int c() {
        return this.c;
    }

    public final void b(int i) {
        this.c = i;
    }

    public final int d() {
        return this.d;
    }

    public final void e() {
        this.d = 0;
    }

    public final int f() {
        return this.e;
    }

    public final void g() {
        this.e = 0;
    }

    public final long h() {
        return this.f;
    }

    public final void a(long j) {
        this.f = j;
    }
}
