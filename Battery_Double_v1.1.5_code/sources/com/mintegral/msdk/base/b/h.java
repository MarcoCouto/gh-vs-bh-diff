package com.mintegral.msdk.base.b;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/* compiled from: CommonAbsDBHelper */
public abstract class h {

    /* renamed from: a reason: collision with root package name */
    private a f2502a;

    /* compiled from: CommonAbsDBHelper */
    private class a extends SQLiteOpenHelper {
        public a(Context context, String str) {
            super(context, str, null, 40);
        }

        public final void onCreate(SQLiteDatabase sQLiteDatabase) {
            h.this.a(sQLiteDatabase);
        }

        public final void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
            h.this.b(sQLiteDatabase);
        }

        public final void onDowngrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
            h.this.c(sQLiteDatabase);
        }
    }

    /* access modifiers changed from: protected */
    public abstract void a(SQLiteDatabase sQLiteDatabase);

    /* access modifiers changed from: protected */
    public abstract void b(SQLiteDatabase sQLiteDatabase);

    /* access modifiers changed from: protected */
    public abstract String c();

    /* access modifiers changed from: protected */
    public abstract void c(SQLiteDatabase sQLiteDatabase);

    public h(Context context) {
        this.f2502a = new a(context, c());
    }

    public final SQLiteDatabase a() {
        return this.f2502a.getReadableDatabase();
    }

    public final synchronized SQLiteDatabase b() {
        SQLiteDatabase sQLiteDatabase;
        sQLiteDatabase = null;
        try {
            sQLiteDatabase = this.f2502a.getWritableDatabase();
        } catch (Exception unused) {
        }
        return sQLiteDatabase;
    }
}
