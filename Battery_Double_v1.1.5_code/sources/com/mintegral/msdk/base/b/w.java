package com.mintegral.msdk.base.b;

import android.content.ContentValues;
import android.database.Cursor;
import android.text.TextUtils;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.entity.p;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.base.utils.s;
import com.vungle.warren.model.AdvertisementDBAdapter.AdvertisementColumns;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

/* compiled from: VideoReportDataDao */
public class w extends a<p> {
    private static final String b = "com.mintegral.msdk.base.b.w";
    private static w c;

    private w(h hVar) {
        super(hVar);
    }

    public static w a(h hVar) {
        if (c == null) {
            synchronized (w.class) {
                if (c == null) {
                    c = new w(hVar);
                }
            }
        }
        return c;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:103:0x02e6, code lost:
        return r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:104:0x02e7, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:105:0x02e8, code lost:
        r3 = r13;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:106:0x02ea, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:107:0x02eb, code lost:
        r3 = r13;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:121:?, code lost:
        r10.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:125:0x0310, code lost:
        r3.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x01f0, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:95:0x02d7, code lost:
        r0 = th;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:104:0x02e7 A[ExcHandler: all (th java.lang.Throwable), PHI: r13 
  PHI: (r13v2 android.database.Cursor) = (r13v0 android.database.Cursor), (r13v3 android.database.Cursor), (r13v3 android.database.Cursor), (r13v3 android.database.Cursor), (r13v3 android.database.Cursor), (r13v3 android.database.Cursor), (r13v3 android.database.Cursor), (r13v3 android.database.Cursor), (r13v3 android.database.Cursor), (r13v3 android.database.Cursor), (r13v3 android.database.Cursor), (r13v3 android.database.Cursor), (r13v3 android.database.Cursor), (r13v3 android.database.Cursor), (r13v3 android.database.Cursor), (r13v3 android.database.Cursor) binds: [B:8:0x0021, B:22:0x0121, B:29:0x0150, B:36:0x016b, B:42:0x017b, B:50:0x0194, B:46:0x0183, B:47:?, B:39:0x0171, B:40:?, B:32:0x0156, B:33:?, B:25:0x0127, B:26:?, B:16:0x00fc, B:17:?] A[DONT_GENERATE, DONT_INLINE], Splitter:B:8:0x0021] */
    /* JADX WARNING: Removed duplicated region for block: B:120:0x0308 A[SYNTHETIC, Splitter:B:120:0x0308] */
    /* JADX WARNING: Removed duplicated region for block: B:125:0x0310 A[Catch:{ all -> 0x030c }] */
    /* JADX WARNING: Removed duplicated region for block: B:95:0x02d7 A[ExcHandler: all (th java.lang.Throwable), PHI: r28 
  PHI: (r28v0 android.database.Cursor) = (r28v1 android.database.Cursor), (r28v6 android.database.Cursor) binds: [B:71:0x020e, B:57:0x01ca] A[DONT_GENERATE, DONT_INLINE], Splitter:B:57:0x01ca] */
    public final synchronized List<p> a(String str) {
        Cursor cursor;
        Cursor cursor2;
        ArrayList arrayList;
        p pVar;
        ArrayList arrayList2;
        Cursor cursor3;
        p pVar2;
        String string;
        String string2;
        p pVar3;
        String str2;
        String str3;
        p pVar4;
        String str4;
        String str5 = str;
        synchronized (this) {
            if (!TextUtils.isEmpty(str)) {
                try {
                    Cursor query = a().query("reward_report", null, "key=?", new String[]{str5}, null, null, null);
                    if (query != null) {
                        try {
                            if (query.getCount() > 0) {
                                ArrayList arrayList3 = new ArrayList();
                                while (query.moveToNext()) {
                                    String string3 = query.getString(query.getColumnIndex("key"));
                                    int i = query.getInt(query.getColumnIndex("networkType"));
                                    String string4 = query.getString(query.getColumnIndex("network_str"));
                                    query.getInt(query.getColumnIndex("isCompleteView"));
                                    query.getInt(query.getColumnIndex("watchedMillis"));
                                    int i2 = query.getInt(query.getColumnIndex("videoLength"));
                                    String string5 = query.getString(query.getColumnIndex("offerUrl"));
                                    String string6 = query.getString(query.getColumnIndex(IronSourceConstants.EVENTS_ERROR_REASON));
                                    int i3 = query.getInt(query.getColumnIndex(IronSourceConstants.EVENTS_RESULT));
                                    String string7 = query.getString(query.getColumnIndex(IronSourceConstants.EVENTS_DURATION));
                                    int i4 = query.getInt(query.getColumnIndex("videoSize"));
                                    String string8 = query.getString(query.getColumnIndex(RequestParameters.CAMPAIGN_ID));
                                    String string9 = query.getString(query.getColumnIndex("video_url"));
                                    String string10 = query.getString(query.getColumnIndex("unitId"));
                                    String string11 = query.getString(query.getColumnIndex("rid"));
                                    String string12 = query.getString(query.getColumnIndex("ad_type"));
                                    String string13 = query.getString(query.getColumnIndex("resource_type"));
                                    String str6 = string10;
                                    String string14 = query.getString(query.getColumnIndex("device_id"));
                                    String string15 = query.getString(query.getColumnIndex("creative"));
                                    if (str5.equals("2000021")) {
                                        String str7 = str6;
                                        p pVar5 = new p(string3, i, string5, string6, string4);
                                        pVar5.m(string8);
                                        pVar5.e(string9);
                                        pVar5.k(string11);
                                        pVar5.l(str7);
                                        pVar = pVar5;
                                        cursor = query;
                                        arrayList = arrayList3;
                                    } else {
                                        arrayList = arrayList3;
                                        String str8 = str6;
                                        if (str5.equals("2000022")) {
                                            p pVar6 = new p(string3, i, i2, string5, i3, string7, i4, string4);
                                            pVar6.m(string8);
                                            pVar6.e(string9);
                                            pVar6.k(string11);
                                            pVar6.l(str8);
                                            pVar6.o(string6);
                                            pVar6.k(string11);
                                            pVar6.h(string12);
                                            pVar = pVar6;
                                        } else if (str5.equals("2000025")) {
                                            pVar = new p(string3, i, i2, string5, i3, string7, i4, string4);
                                        } else if (str5.equals("2000024")) {
                                            pVar = new p(string3, i, string5, string6, string4);
                                        } else if ("2000039".equals(str5)) {
                                            pVar = new p(query.getString(query.getColumnIndex("h5_click_data")));
                                        } else if ("2000043".equals(str5)) {
                                            try {
                                                string = query.getString(query.getColumnIndex("type"));
                                                string2 = query.getString(query.getColumnIndex(CampaignEx.ENDCARD_URL));
                                                pVar3 = pVar3;
                                                cursor3 = query;
                                                str2 = string14;
                                                str3 = string15;
                                                pVar4 = pVar3;
                                                str4 = string8;
                                            } catch (Exception e) {
                                                e = e;
                                                cursor3 = query;
                                                cursor2 = cursor3;
                                                try {
                                                    e.printStackTrace();
                                                    g.d(b, e.getMessage());
                                                    if (cursor2 != null) {
                                                    }
                                                    return null;
                                                } catch (Throwable th) {
                                                    th = th;
                                                    cursor = cursor2;
                                                    if (cursor != null) {
                                                    }
                                                    throw th;
                                                }
                                            } catch (Throwable th2) {
                                                th = th2;
                                                cursor3 = query;
                                                cursor = cursor3;
                                                if (cursor != null) {
                                                }
                                                throw th;
                                            }
                                            try {
                                                pVar3 = new p(str, i3, string7, string2, string8, str8, string6, string);
                                                pVar4.k(string11);
                                                if (!TextUtils.isEmpty(str4)) {
                                                    pVar4.m(str4);
                                                }
                                                pVar4.h(string12);
                                                pVar4.a(string13);
                                                pVar4.b(str2);
                                                pVar4.c(str3);
                                                pVar = pVar4;
                                                cursor = cursor3;
                                                str5 = str;
                                            } catch (Exception e2) {
                                                e = e2;
                                                cursor = cursor3;
                                                cursor2 = cursor;
                                                e.printStackTrace();
                                                g.d(b, e.getMessage());
                                                if (cursor2 != null) {
                                                }
                                                return null;
                                            } catch (Throwable th3) {
                                            }
                                        } else {
                                            String str9 = str8;
                                            String str10 = string8;
                                            cursor3 = query;
                                            String str11 = string15;
                                            String str12 = string14;
                                            str5 = str;
                                            if ("2000045".equals(str5)) {
                                                pVar2 = new p();
                                                pVar2.n(str5);
                                                pVar2.b(i);
                                                pVar2.c(i3);
                                                pVar2.m(str10);
                                                cursor = cursor3;
                                                try {
                                                    pVar2.i(cursor.getString(cursor.getColumnIndex(AdvertisementColumns.COLUMN_TEMPLATE_URL)));
                                                    pVar2.o(string6);
                                                    pVar2.k(string11);
                                                    pVar2.l(str9);
                                                } catch (Exception e3) {
                                                    e = e3;
                                                    cursor2 = cursor;
                                                    e.printStackTrace();
                                                    g.d(b, e.getMessage());
                                                    if (cursor2 != null) {
                                                    }
                                                    return null;
                                                } catch (Throwable th4) {
                                                    th = th4;
                                                    if (cursor != null) {
                                                    }
                                                    throw th;
                                                }
                                            } else {
                                                cursor = cursor3;
                                                String str13 = str9;
                                                if ("2000044".equals(str5)) {
                                                    pVar2 = new p();
                                                    pVar2.n(str5);
                                                    pVar2.b(i);
                                                    pVar2.m(str10);
                                                    pVar2.d(cursor.getString(cursor.getColumnIndex("image_url")));
                                                    pVar2.o(string6);
                                                    pVar2.k(string11);
                                                    pVar2.l(str13);
                                                } else if ("2000054".equals(str5)) {
                                                    p pVar7 = new p();
                                                    pVar7.n(str5);
                                                    pVar7.a(string13);
                                                    pVar7.l(str13);
                                                    pVar7.b(str12);
                                                    pVar7.h(string12);
                                                    pVar7.m(str10);
                                                    pVar7.k(string11);
                                                    pVar7.c(i3);
                                                    pVar7.o(string6);
                                                    pVar7.b(i);
                                                    pVar7.c(str11);
                                                    pVar = pVar7;
                                                } else {
                                                    pVar = null;
                                                }
                                            }
                                            pVar = pVar2;
                                        }
                                        cursor = query;
                                    }
                                    if (pVar != null) {
                                        arrayList2 = arrayList;
                                        arrayList2.add(pVar);
                                    } else {
                                        arrayList2 = arrayList;
                                    }
                                    b().delete("reward_report", "id = ?", new String[]{String.valueOf(cursor.getInt(cursor.getColumnIndex("id")))});
                                    arrayList3 = arrayList2;
                                    query = cursor;
                                }
                                Cursor cursor4 = query;
                                ArrayList arrayList4 = arrayList3;
                                if (cursor4 != null) {
                                    cursor4.close();
                                }
                            }
                        } catch (Exception e4) {
                            e = e4;
                            cursor2 = query;
                        } catch (Throwable th5) {
                        }
                    }
                    Cursor cursor5 = query;
                    if (cursor5 != null) {
                        cursor5.close();
                    }
                } catch (Exception e5) {
                    e = e5;
                    cursor2 = null;
                    e.printStackTrace();
                    g.d(b, e.getMessage());
                    if (cursor2 != null) {
                    }
                    return null;
                } catch (Throwable th6) {
                    th = th6;
                    cursor = null;
                    if (cursor != null) {
                    }
                    throw th;
                }
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x0032 A[SYNTHETIC, Splitter:B:22:0x0032] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0039 A[SYNTHETIC, Splitter:B:27:0x0039] */
    public final synchronized int c() {
        int i;
        Cursor cursor = null;
        i = 0;
        try {
            Cursor rawQuery = a().rawQuery("select count(*) from reward_report", null);
            if (rawQuery != null) {
                try {
                    if (rawQuery.moveToFirst()) {
                        i = rawQuery.getInt(0);
                    }
                } catch (Exception e) {
                    Cursor cursor2 = rawQuery;
                    e = e;
                    cursor = cursor2;
                    try {
                        e.printStackTrace();
                        if (cursor != null) {
                        }
                        return i;
                    } catch (Throwable th) {
                        th = th;
                        if (cursor != null) {
                        }
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    cursor = rawQuery;
                    if (cursor != null) {
                        cursor.close();
                    }
                    throw th;
                }
            }
            if (rawQuery != null) {
                try {
                    rawQuery.close();
                } catch (Throwable th3) {
                    throw th3;
                }
            }
        } catch (Exception e2) {
            e = e2;
            e.printStackTrace();
            if (cursor != null) {
                cursor.close();
            }
            return i;
        }
        return i;
    }

    public final synchronized long a(p pVar) {
        if (pVar != null) {
            try {
                ContentValues contentValues = new ContentValues();
                contentValues.put("key", pVar.m());
                contentValues.put("networkType", Integer.valueOf(pVar.t()));
                contentValues.put("network_str", pVar.u());
                contentValues.put("isCompleteView", Integer.valueOf(pVar.v()));
                contentValues.put("watchedMillis", Integer.valueOf(pVar.n()));
                contentValues.put("videoLength", Integer.valueOf(pVar.o()));
                if (!TextUtils.isEmpty(pVar.p())) {
                    contentValues.put("offerUrl", URLEncoder.encode(pVar.p(), "utf-8"));
                }
                if (!TextUtils.isEmpty(pVar.q())) {
                    contentValues.put(IronSourceConstants.EVENTS_ERROR_REASON, URLEncoder.encode(pVar.q(), "utf-8"));
                }
                contentValues.put(IronSourceConstants.EVENTS_RESULT, Integer.valueOf(pVar.w()));
                contentValues.put(IronSourceConstants.EVENTS_DURATION, pVar.r());
                contentValues.put("videoSize", Integer.valueOf(pVar.s()));
                contentValues.put("type", pVar.g());
                String f = pVar.f();
                if (!TextUtils.isEmpty(f)) {
                    contentValues.put(CampaignEx.ENDCARD_URL, URLEncoder.encode(f, "utf-8"));
                }
                String e = pVar.e();
                if (!TextUtils.isEmpty(e)) {
                    contentValues.put("video_url", URLEncoder.encode(e, "utf-8"));
                }
                String j = pVar.j();
                if (!TextUtils.isEmpty(j)) {
                    contentValues.put("rid", URLEncoder.encode(j, "utf-8"));
                }
                String i = pVar.i();
                if (!TextUtils.isEmpty(i)) {
                    contentValues.put(AdvertisementColumns.COLUMN_TEMPLATE_URL, URLEncoder.encode(i, "utf-8"));
                }
                String d = pVar.d();
                if (!TextUtils.isEmpty(d)) {
                    contentValues.put("image_url", URLEncoder.encode(d, "utf-8"));
                }
                String h = pVar.h();
                if (!TextUtils.isEmpty(h)) {
                    contentValues.put("ad_type", URLEncoder.encode(h, "utf-8"));
                }
                contentValues.put("unitId", pVar.k());
                contentValues.put(RequestParameters.CAMPAIGN_ID, pVar.l());
                if ("2000039".equals(pVar.m())) {
                    String g = p.g(pVar);
                    if (s.b(g)) {
                        contentValues.put("h5_click_data", g);
                    }
                }
                String a2 = pVar.a();
                if (!TextUtils.isEmpty(a2)) {
                    contentValues.put("resource_type", URLEncoder.encode(a2, "utf-8"));
                }
                String b2 = pVar.b();
                if (!TextUtils.isEmpty(b2)) {
                    contentValues.put("device_id", URLEncoder.encode(b2, "utf-8"));
                }
                String c2 = pVar.c();
                if (!TextUtils.isEmpty(c2)) {
                    contentValues.put("creative", URLEncoder.encode(c2, "utf-8"));
                }
                return b().insert("reward_report", null, contentValues);
            } catch (Exception e2) {
                e2.printStackTrace();
                g.d(b, e2.getMessage());
            }
        }
        return -1;
    }
}
