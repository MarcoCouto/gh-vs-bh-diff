package com.mintegral.msdk.base.b;

import android.content.ContentValues;
import android.database.Cursor;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.base.entity.h;
import java.util.ArrayList;
import java.util.List;

/* compiled from: UnitIDDao */
public class u extends a<h> {
    private static u b;

    private u(h hVar) {
        super(hVar);
    }

    public static u a(h hVar) {
        if (b == null) {
            synchronized (u.class) {
                if (b == null) {
                    b = new u(hVar);
                }
            }
        }
        return b;
    }

    /* JADX WARNING: Removed duplicated region for block: B:29:0x005a A[SYNTHETIC, Splitter:B:29:0x005a] */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x0063 A[SYNTHETIC, Splitter:B:36:0x0063] */
    public final synchronized List<String> a(int i) {
        List<String> list;
        Cursor cursor;
        List<String> list2;
        Exception e;
        StringBuilder sb = new StringBuilder("select * from unit_id WHERE ad_type = ");
        sb.append(i);
        String sb2 = sb.toString();
        list = null;
        try {
            cursor = a().rawQuery(sb2, null);
            if (cursor != null) {
                try {
                    if (cursor.getCount() > 0) {
                        list2 = new ArrayList<>(cursor.getCount());
                        while (cursor.moveToNext()) {
                            try {
                                list2.add(cursor.getString(cursor.getColumnIndex("unitId")));
                            } catch (Exception e2) {
                                e = e2;
                                try {
                                    e.printStackTrace();
                                    if (cursor != null) {
                                    }
                                    list = list2;
                                    return list;
                                } catch (Throwable th) {
                                    th = th;
                                    if (cursor != null) {
                                        cursor.close();
                                    }
                                    throw th;
                                }
                            }
                        }
                        list = list2;
                    }
                } catch (Exception e3) {
                    Exception exc = e3;
                    list2 = null;
                    e = exc;
                    e.printStackTrace();
                    if (cursor != null) {
                        cursor.close();
                    }
                    list = list2;
                    return list;
                }
            }
            if (cursor != null) {
                cursor.close();
            }
        } catch (Exception e4) {
            list2 = null;
            e = e4;
            cursor = null;
            e.printStackTrace();
            if (cursor != null) {
            }
            list = list2;
            return list;
        } catch (Throwable th2) {
            th = th2;
            cursor = null;
            if (cursor != null) {
            }
            throw th;
        }
        return list;
    }

    public final synchronized void a(String str) {
        if (b() != null) {
            b().delete(MIntegralConstans.PROPERTIES_UNIT_ID, "unitId = ?", new String[]{str});
        }
    }

    public final synchronized void a(String str, int i) {
        if (b() != null) {
            ContentValues contentValues = new ContentValues();
            contentValues.put("unitId", str);
            contentValues.put("ad_type", Integer.valueOf(i));
            b().insert(MIntegralConstans.PROPERTIES_UNIT_ID, null, contentValues);
        }
    }
}
