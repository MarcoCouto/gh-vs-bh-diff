package com.mintegral.msdk.base.b;

import android.content.ContentValues;
import android.database.Cursor;
import com.facebook.internal.NativeProtocol;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.entity.o;
import com.mintegral.msdk.out.Campaign;

/* compiled from: VideoDao */
public class v extends a<Campaign> {
    private static v b;

    private v(h hVar) {
        super(hVar);
    }

    public static v a(h hVar) {
        if (b == null) {
            synchronized (v.class) {
                if (b == null) {
                    b = new v(hVar);
                }
            }
        }
        return b;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x008e, code lost:
        if (r7 != null) goto L_0x0090;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:?, code lost:
        r7.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x009f, code lost:
        if (r7 != null) goto L_0x0090;
     */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x009a A[Catch:{ Exception -> 0x009e, all -> 0x0094 }] */
    public final synchronized o a(String str, String str2) {
        o oVar;
        Cursor cursor;
        Throwable th;
        oVar = new o();
        StringBuilder sb = new StringBuilder(" WHERE video_url = '");
        sb.append(str);
        sb.append("' AND ad_bid_token = '");
        sb.append(str2);
        sb.append("'");
        String sb2 = sb.toString();
        StringBuilder sb3 = new StringBuilder("SELECT * FROM video");
        sb3.append(sb2);
        try {
            cursor = a().rawQuery(sb3.toString(), null);
            if (cursor != null) {
                try {
                    if (cursor.getCount() > 0) {
                        while (cursor.moveToNext()) {
                            oVar.a(cursor.getString(cursor.getColumnIndex("video_url")));
                            oVar.b(cursor.getInt(cursor.getColumnIndex("video_state")));
                            oVar.b(cursor.getLong(cursor.getColumnIndex("pregeress_size")));
                            oVar.a(cursor.getInt(cursor.getColumnIndex("total_size")));
                            oVar.a(cursor.getLong(cursor.getColumnIndex("video_download_start")) * 1000);
                        }
                    }
                } catch (Exception unused) {
                } catch (Throwable th2) {
                    th = th2;
                    if (cursor != null) {
                    }
                    throw th;
                }
            }
        } catch (Exception unused2) {
            cursor = null;
        } catch (Throwable th3) {
            th = th3;
            cursor = null;
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
        return oVar;
    }

    public final synchronized void a(String str) {
        try {
            StringBuilder sb = new StringBuilder("video_url = '");
            sb.append(str);
            sb.append("'");
            String sb2 = sb.toString();
            if (b() != null) {
                b().delete("video", sb2, null);
            }
        } catch (Exception unused) {
        }
    }

    public final synchronized long a(CampaignEx campaignEx, long j) {
        if (campaignEx == null) {
            return 0;
        }
        try {
            if (b() == null) {
                return -1;
            }
            ContentValues contentValues = new ContentValues();
            contentValues.put("id", campaignEx.getId());
            contentValues.put(CampaignEx.JSON_KEY_PACKAGE_NAME, campaignEx.getPackageName());
            contentValues.put(NativeProtocol.BRIDGE_ARG_APP_NAME_STRING, campaignEx.getAppName());
            contentValues.put("app_desc", campaignEx.getAppDesc());
            contentValues.put(CampaignEx.JSON_KEY_APP_SIZE, campaignEx.getSize());
            contentValues.put(CampaignEx.JSON_KEY_IMAGE_SIZE, campaignEx.getImageSize());
            contentValues.put(CampaignEx.JSON_KEY_ICON_URL, campaignEx.getIconUrl());
            contentValues.put("image_url", campaignEx.getImageUrl());
            contentValues.put(CampaignEx.JSON_KEY_IMPRESSION_URL, campaignEx.getImpressionURL());
            contentValues.put(CampaignEx.JSON_KEY_NOTICE_URL, campaignEx.getNoticeUrl());
            contentValues.put("download_url", campaignEx.getClickURL());
            contentValues.put("only_impression", campaignEx.getOnlyImpressionURL());
            contentValues.put(CampaignEx.JSON_KEY_ST_TS, Long.valueOf(campaignEx.getTimestamp()));
            contentValues.put("template", Integer.valueOf(campaignEx.getTemplate()));
            contentValues.put(CampaignEx.JSON_KEY_CLICK_MODE, campaignEx.getClick_mode());
            contentValues.put(CampaignEx.JSON_KEY_LANDING_TYPE, campaignEx.getLandingType());
            contentValues.put(CampaignEx.JSON_KEY_LINK_TYPE, Integer.valueOf(campaignEx.getLinkType()));
            contentValues.put("star", Double.valueOf(campaignEx.getRating()));
            contentValues.put("cti", Integer.valueOf(campaignEx.getClickInterval()));
            contentValues.put("cpti", Integer.valueOf(campaignEx.getPreClickInterval()));
            contentValues.put("preclick", Boolean.valueOf(campaignEx.isPreClick()));
            contentValues.put("level", Integer.valueOf(campaignEx.getCacheLevel()));
            contentValues.put("adSource", Integer.valueOf(campaignEx.getType()));
            contentValues.put("ad_call", campaignEx.getAdCall());
            contentValues.put("fc_a", Integer.valueOf(campaignEx.getFca()));
            contentValues.put(CampaignEx.JSON_KEY_AD_URL_LIST, campaignEx.getAd_url_list());
            contentValues.put("video_url", campaignEx.getVideoUrlEncode());
            contentValues.put("total_size", Long.valueOf(j));
            contentValues.put("video_state", Integer.valueOf(0));
            contentValues.put("video_download_start", Long.valueOf(System.currentTimeMillis() / 1000));
            contentValues.put("ad_bid_token", campaignEx.getBidToken());
            if (b(campaignEx.getVideoUrlEncode())) {
                return 0;
            }
            return b().insert("video", null, contentValues);
        } catch (Exception unused) {
            return -1;
        }
    }

    private synchronized boolean b(String str) {
        StringBuilder sb = new StringBuilder("SELECT id FROM video WHERE video_url = '");
        sb.append(str);
        sb.append("'");
        Cursor rawQuery = a().rawQuery(sb.toString(), null);
        if (rawQuery == null || rawQuery.getCount() <= 0) {
            if (rawQuery != null) {
                rawQuery.close();
            }
            return false;
        }
        rawQuery.close();
        return true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x004c, code lost:
        r0 = r4;
     */
    public final long a(String str, long j, int i) {
        int i2 = -1;
        try {
            if (b() == null) {
                return -1;
            }
            ContentValues contentValues = new ContentValues();
            contentValues.put("pregeress_size", Long.valueOf(j));
            contentValues.put("video_state", Integer.valueOf(i));
            if (b(str)) {
                StringBuilder sb = new StringBuilder("video_url = '");
                sb.append(str);
                sb.append("'");
                String sb2 = sb.toString();
                synchronized (new Object()) {
                    try {
                        int update = b().update("video", contentValues, sb2, null);
                        try {
                        } catch (Throwable th) {
                            th = th;
                            i2 = update;
                            throw th;
                        }
                    } catch (Throwable th2) {
                        th = th2;
                        throw th;
                    }
                }
            }
            return (long) i2;
        } catch (Exception unused) {
        }
    }
}
