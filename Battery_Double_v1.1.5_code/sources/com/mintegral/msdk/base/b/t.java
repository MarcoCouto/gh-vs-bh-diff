package com.mintegral.msdk.base.b;

import android.content.ContentValues;
import android.database.Cursor;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.entity.n;
import java.util.ArrayList;
import java.util.List;

/* compiled from: SettingCampaignDao */
public class t extends a {
    private static t b;

    private t(h hVar) {
        super(hVar);
    }

    public static t a(h hVar) {
        if (b == null) {
            synchronized (t.class) {
                if (b == null) {
                    b = new t(hVar);
                }
            }
        }
        return b;
    }

    /* JADX WARNING: Removed duplicated region for block: B:25:0x003f A[SYNTHETIC, Splitter:B:25:0x003f] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0046 A[SYNTHETIC, Splitter:B:30:0x0046] */
    public final synchronized int c() {
        int i;
        i = 0;
        Cursor cursor = null;
        try {
            Cursor query = a().query("settingCampaign", new String[]{" count(*) "}, null, null, null, null, null);
            if (query != null) {
                try {
                    if (query.moveToFirst()) {
                        i = query.getInt(0);
                    }
                } catch (Exception e) {
                    Cursor cursor2 = query;
                    e = e;
                    cursor = cursor2;
                    try {
                        e.printStackTrace();
                        if (cursor != null) {
                            cursor.close();
                        }
                        return i;
                    } catch (Throwable th) {
                        th = th;
                        if (cursor != null) {
                        }
                        throw th;
                    }
                } catch (Throwable th2) {
                    Cursor cursor3 = query;
                    th = th2;
                    cursor = cursor3;
                    if (cursor != null) {
                        cursor.close();
                    }
                    throw th;
                }
            }
            if (query != null) {
                query.close();
            }
        } catch (Exception e2) {
            e = e2;
            e.printStackTrace();
            if (cursor != null) {
            }
            return i;
        }
        return i;
    }

    public final synchronized int d() {
        try {
            if (b() == null) {
                return -1;
            }
            return b().delete("settingCampaign", null, null);
        } catch (Exception unused) {
            return -1;
        }
    }

    public final synchronized int a(String str) {
        String str2 = "id=?";
        try {
            String[] strArr = {str};
            if (b() == null) {
                return -1;
            }
            return b().delete("settingCampaign", str2, strArr);
        } catch (Exception unused) {
            return -1;
        }
    }

    public final synchronized int b(String str) {
        String str2 = "iex=?";
        try {
            String[] strArr = {str};
            if (b() == null) {
                return -1;
            }
            return b().delete("settingCampaign", str2, strArr);
        } catch (Exception unused) {
            return -1;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:27:0x0078 A[SYNTHETIC, Splitter:B:27:0x0078] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0081 A[SYNTHETIC, Splitter:B:34:0x0081] */
    public final synchronized List<n> e() {
        List<n> list;
        Cursor cursor;
        List<n> list2;
        Exception e;
        list = null;
        try {
            cursor = a().rawQuery("select * from settingCampaign ORDER BY iex LIMIT 3", null);
            if (cursor != null) {
                try {
                    if (cursor.getCount() > 0) {
                        list2 = new ArrayList<>();
                        while (cursor.moveToNext()) {
                            try {
                                n nVar = new n(cursor.getString(cursor.getColumnIndex("url")), cursor.getString(cursor.getColumnIndex("method")), cursor.getString(cursor.getColumnIndex("data")), cursor.getString(cursor.getColumnIndex(CampaignEx.JSON_KEY_ST_IEX)), cursor.getInt(cursor.getColumnIndex("id")));
                                list2.add(nVar);
                            } catch (Exception e2) {
                                e = e2;
                                try {
                                    e.printStackTrace();
                                    if (cursor != null) {
                                    }
                                    list = list2;
                                    return list;
                                } catch (Throwable th) {
                                    th = th;
                                    if (cursor != null) {
                                    }
                                    throw th;
                                }
                            }
                        }
                        list = list2;
                    }
                } catch (Exception e3) {
                    Exception exc = e3;
                    list2 = null;
                    e = exc;
                    e.printStackTrace();
                    if (cursor != null) {
                        cursor.close();
                    }
                    list = list2;
                    return list;
                }
            }
            if (cursor != null) {
                try {
                    cursor.close();
                } catch (Throwable th2) {
                    throw th2;
                }
            }
        } catch (Exception e4) {
            list2 = null;
            e = e4;
            cursor = null;
            e.printStackTrace();
            if (cursor != null) {
            }
            list = list2;
            return list;
        } catch (Throwable th3) {
            Throwable th4 = th3;
            cursor = null;
            th = th4;
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
        return list;
    }

    public final synchronized long a(n nVar) {
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put("url", nVar.b());
            contentValues.put("method", nVar.c());
            contentValues.put("data", nVar.d());
            contentValues.put(CampaignEx.JSON_KEY_ST_IEX, nVar.a());
            if (b() == null) {
                return -1;
            }
            return b().insert("settingCampaign", null, contentValues);
        } catch (Exception unused) {
            return -1;
        }
    }
}
