package com.mintegral.msdk.base.b;

import android.content.ContentValues;
import android.database.Cursor;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.entity.l;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.out.Campaign;
import com.vungle.warren.model.ReportDBAdapter.ReportColumns;
import java.util.ArrayList;
import java.util.List;

/* compiled from: PBInfoDao */
public class p extends a<Campaign> {
    /* access modifiers changed from: private */
    public static final String b = "com.mintegral.msdk.base.b.p";
    private static p c;

    private p(h hVar) {
        super(hVar);
    }

    public static p a(h hVar) {
        if (c == null) {
            synchronized (p.class) {
                if (c == null) {
                    c = new p(hVar);
                }
            }
        }
        return c;
    }

    public final synchronized void c() {
        try {
            if (b() != null) {
                b().delete("pbinfo", null, null);
                g.b(b, "deleteAll");
            }
        } catch (Throwable th) {
            g.c(b, th.getMessage(), th);
        }
    }

    public final synchronized void a(String str) {
        try {
            StringBuilder sb = new StringBuilder("package_name = '");
            sb.append(str);
            sb.append("'");
            String sb2 = sb.toString();
            if (b() != null) {
                b().delete("pbinfo", sb2, null);
                String str2 = b;
                StringBuilder sb3 = new StringBuilder("deleteByPKG：");
                sb3.append(str);
                g.b(str2, sb3.toString());
            }
        } catch (Throwable th) {
            g.c(b, th.getMessage(), th);
        }
    }

    public final synchronized long a(l lVar, String str) {
        if (lVar == null) {
            return 0;
        }
        try {
            if (b() == null) {
                return -1;
            }
            ContentValues contentValues = new ContentValues();
            contentValues.put(CampaignEx.JSON_KEY_PACKAGE_NAME, lVar.f2601a);
            contentValues.put(ReportColumns.TABLE_NAME, lVar.c);
            contentValues.put("uuid", str);
            if (b(lVar.f2601a)) {
                String str2 = b;
                StringBuilder sb = new StringBuilder("insertOrUpdate update:");
                sb.append(lVar.f2601a);
                g.b(str2, sb.toString());
                StringBuilder sb2 = new StringBuilder("package_name = '");
                sb2.append(lVar.f2601a);
                sb2.append("'");
                return (long) b().update("pbinfo", contentValues, sb2.toString(), null);
            }
            String str3 = b;
            StringBuilder sb3 = new StringBuilder("insertOrUpdate insert:");
            sb3.append(lVar.f2601a);
            g.b(str3, sb3.toString());
            return b().insert("pbinfo", null, contentValues);
        } catch (Throwable th) {
            g.c(b, th.getMessage(), th);
            return -1;
        }
    }

    public final synchronized void a(final List<l> list, final String str) {
        if (list != null) {
            if (list.size() != 0) {
                new Thread(new Runnable() {
                    public final void run() {
                        for (l lVar : list) {
                            String e = p.b;
                            StringBuilder sb = new StringBuilder("insertOrUpdate strat list size:");
                            sb.append(list.size());
                            sb.append(" uuid:");
                            sb.append(str);
                            g.b(e, sb.toString());
                            p.this.a(lVar, str);
                        }
                    }
                }).start();
                return;
            }
        }
        g.b(b, "insertOrUpdate size为空 return");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0032, code lost:
        return false;
     */
    private synchronized boolean b(String str) {
        try {
            StringBuilder sb = new StringBuilder("SELECT package_name FROM pbinfo WHERE package_name='");
            sb.append(str);
            sb.append("'");
            Cursor rawQuery = a().rawQuery(sb.toString(), null);
            if (rawQuery != null && rawQuery.getCount() > 0) {
                rawQuery.close();
                return true;
            } else if (rawQuery != null) {
                rawQuery.close();
            }
        } catch (Throwable th) {
            g.c(b, th.getMessage(), th);
            return false;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:31:0x00b0 A[SYNTHETIC, Splitter:B:31:0x00b0] */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x00c4 A[SYNTHETIC, Splitter:B:41:0x00c4] */
    public final synchronized List<l> d() {
        List<l> list;
        Cursor cursor;
        List<l> list2;
        Throwable th;
        list = null;
        try {
            cursor = a().rawQuery("SELECT * FROM pbinfo", null);
            if (cursor != null) {
                try {
                    if (cursor.getCount() > 0) {
                        list2 = new ArrayList<>();
                        while (cursor.moveToNext()) {
                            try {
                                l lVar = new l();
                                lVar.f2601a = cursor.getString(cursor.getColumnIndex(CampaignEx.JSON_KEY_PACKAGE_NAME));
                                lVar.c = cursor.getString(cursor.getColumnIndex(ReportColumns.TABLE_NAME));
                                lVar.b = cursor.getString(cursor.getColumnIndex("uuid"));
                                String str = b;
                                StringBuilder sb = new StringBuilder("getAllPBInfo pb：");
                                sb.append(lVar.f2601a);
                                sb.append(" uuid:");
                                sb.append(lVar.b);
                                g.b(str, sb.toString());
                                list2.add(lVar);
                            } catch (Throwable th2) {
                                th = th2;
                                try {
                                    g.c(b, th.getMessage(), th);
                                    if (cursor != null) {
                                    }
                                    list = list2;
                                    return list;
                                } catch (Throwable th3) {
                                    th = th3;
                                    if (cursor != null) {
                                        try {
                                            cursor.close();
                                        } catch (Throwable th4) {
                                            g.c(b, th4.getMessage(), th4);
                                        }
                                    }
                                    throw th;
                                }
                            }
                        }
                        String str2 = b;
                        StringBuilder sb2 = new StringBuilder("getAllPBInfo lis.size:");
                        sb2.append(list2.size());
                        g.b(str2, sb2.toString());
                        list = list2;
                    }
                } catch (Throwable th5) {
                    Throwable th6 = th5;
                    list2 = null;
                    th = th6;
                    g.c(b, th.getMessage(), th);
                    if (cursor != null) {
                    }
                    list = list2;
                    return list;
                }
            }
            if (cursor != null) {
                try {
                    cursor.close();
                } catch (Throwable th7) {
                    g.c(b, th7.getMessage(), th7);
                }
            }
        } catch (Throwable th8) {
            Throwable th9 = th8;
            cursor = null;
            th = th9;
            if (cursor != null) {
            }
            throw th;
        }
        return list;
    }
}
