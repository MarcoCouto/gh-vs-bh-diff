package com.mintegral.msdk.base.b;

import android.content.ContentValues;
import android.database.Cursor;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.entity.f;

/* compiled from: FrequenceDao */
public class m extends a<f> {
    private static m b;

    private m(h hVar) {
        super(hVar);
    }

    public static m a(h hVar) {
        if (b == null) {
            synchronized (m.class) {
                if (b == null) {
                    b = new m(hVar);
                }
            }
        }
        return b;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0036, code lost:
        return r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0039, code lost:
        if (r0 != null) goto L_0x003b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
        r0.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x004a, code lost:
        if (r0 != null) goto L_0x003b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x004e, code lost:
        return null;
     */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0045 A[Catch:{ Exception -> 0x0049, all -> 0x003f }] */
    public final synchronized long[] c() {
        Cursor cursor;
        Throwable th;
        try {
            cursor = a().rawQuery("SELECT id FROM frequence WHERE fc_a<impression_count", null);
            if (cursor != null) {
                try {
                    if (cursor.getCount() > 0) {
                        long[] jArr = new long[cursor.getCount()];
                        int i = 0;
                        while (cursor.moveToNext()) {
                            jArr[i] = cursor.getLong(cursor.getColumnIndexOrThrow("id"));
                            i++;
                        }
                        if (cursor != null) {
                            cursor.close();
                        }
                    }
                } catch (Exception unused) {
                } catch (Throwable th2) {
                    th = th2;
                    if (cursor != null) {
                    }
                    throw th;
                }
            }
        } catch (Exception unused2) {
            cursor = null;
        } catch (Throwable th3) {
            th = th3;
            cursor = null;
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    public final synchronized void d() {
        try {
            long currentTimeMillis = System.currentTimeMillis() - 86400000;
            StringBuilder sb = new StringBuilder("ts<");
            sb.append(currentTimeMillis);
            String sb2 = sb.toString();
            if (b() != null) {
                b().delete("frequence", sb2, null);
            }
        } catch (Exception unused) {
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0028, code lost:
        if (r2 == null) goto L_0x002d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x002f, code lost:
        return true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0036, code lost:
        if (r2 == null) goto L_0x003b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x003c, code lost:
        return false;
     */
    public final synchronized boolean a(String str) {
        Cursor cursor = null;
        try {
            synchronized (str) {
                try {
                    StringBuilder sb = new StringBuilder("SELECT id FROM frequence WHERE id='");
                    sb.append(str);
                    sb.append("'");
                    Cursor rawQuery = a().rawQuery(sb.toString(), null);
                    if (rawQuery != null) {
                        try {
                            if (rawQuery.getCount() > 0) {
                            }
                        } catch (Throwable th) {
                            Cursor cursor2 = rawQuery;
                            th = th;
                            cursor = cursor2;
                            throw th;
                        }
                    }
                } catch (Throwable th2) {
                    th = th2;
                    throw th;
                }
            }
        } catch (Exception unused) {
            if (cursor != null) {
                cursor.close();
            }
            return false;
        } catch (Throwable th3) {
            if (cursor != null) {
                cursor.close();
            }
            throw th3;
        }
    }

    public final synchronized void b(String str) {
        if (a(str)) {
            StringBuilder sb = new StringBuilder("UPDATE frequence Set impression_count=impression_count+1 WHERE id=");
            sb.append(str);
            String sb2 = sb.toString();
            if (a() != null) {
                a().execSQL(sb2);
            }
        }
    }

    private synchronized long b(f fVar) {
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put("id", fVar.a());
            contentValues.put("fc_a", Integer.valueOf(fVar.b()));
            contentValues.put("fc_b", Integer.valueOf(fVar.c()));
            contentValues.put(CampaignEx.JSON_KEY_ST_TS, Long.valueOf(fVar.h()));
            contentValues.put("impression_count", Integer.valueOf(fVar.d()));
            contentValues.put("click_count", Integer.valueOf(fVar.f()));
            contentValues.put(CampaignEx.JSON_KEY_ST_TS, Long.valueOf(fVar.h()));
            if (b() == null) {
                return -1;
            }
            return b().insert("frequence", null, contentValues);
        } catch (Exception unused) {
            return -1;
        }
    }

    public final synchronized void a(f fVar) {
        if (!a(fVar.a())) {
            b(fVar);
        }
    }
}
