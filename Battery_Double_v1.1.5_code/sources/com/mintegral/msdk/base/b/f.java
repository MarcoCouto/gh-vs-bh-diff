package com.mintegral.msdk.base.b;

import android.content.ContentValues;
import android.database.Cursor;
import android.text.TextUtils;
import com.facebook.internal.NativeProtocol;
import com.mansoon.BatteryDouble.Config;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.b.b;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.entity.CampaignEx.a;
import com.mintegral.msdk.base.entity.CampaignEx.c;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.out.Campaign;
import com.vungle.warren.model.AdvertisementDBAdapter.AdvertisementColumns;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/* compiled from: CampaignDao */
public class f extends a<Campaign> {
    private static final String b = "com.mintegral.msdk.base.b.f";
    private static f c;

    protected f(h hVar) {
        super(hVar);
    }

    public static f a(h hVar) {
        if (c == null) {
            synchronized (f.class) {
                if (c == null) {
                    c = new f(hVar);
                }
            }
        }
        return c;
    }

    public final synchronized void a(String str, int i) {
        try {
            StringBuilder sb = new StringBuilder("unitid = ");
            sb.append(str);
            sb.append(" AND level = 2 AND adSource = ");
            sb.append(i);
            String sb2 = sb.toString();
            if (b() != null) {
                b().delete("campaign", sb2, null);
            }
        } catch (Exception unused) {
        }
    }

    public final synchronized void a(String str, int i, int i2, boolean z) {
        try {
            StringBuilder sb = new StringBuilder("unitid = ");
            sb.append(str);
            sb.append(" AND level = ");
            sb.append(i);
            sb.append(" AND adSource = ");
            sb.append(i2);
            String sb2 = sb.toString();
            StringBuilder sb3 = new StringBuilder();
            sb3.append(sb2);
            sb3.append(" AND is_bid_campaign = ");
            sb3.append(z ? 1 : 0);
            String sb4 = sb3.toString();
            if (b() != null) {
                b().delete("campaign", sb4, null);
            }
        } catch (Exception unused) {
        }
    }

    public final synchronized void a(String str, String str2) {
        try {
            StringBuilder sb = new StringBuilder("id = '");
            sb.append(str);
            sb.append("' AND unitid = ");
            sb.append(str2);
            String sb2 = sb.toString();
            if (b() != null) {
                b().delete("campaign", sb2, null);
            }
        } catch (Exception unused) {
        }
    }

    public final synchronized void a(String str, String str2, boolean z) {
        try {
            StringBuilder sb = new StringBuilder("id = '");
            sb.append(str);
            sb.append("' AND unitid = ");
            sb.append(str2);
            sb.append(" AND is_bid_campaign = ");
            sb.append(z ? "1" : "0");
            String sb2 = sb.toString();
            if (b() != null) {
                b().delete("campaign", sb2, null);
            }
        } catch (Exception unused) {
        }
    }

    public final synchronized void a(String str) {
        try {
            StringBuilder sb = new StringBuilder("id = '");
            sb.append(str);
            sb.append("'");
            String sb2 = sb.toString();
            if (b() != null) {
                b().delete("campaign", sb2, null);
            }
        } catch (Exception e) {
            e.printStackTrace();
            g.d(b, e.getMessage());
        }
    }

    public final synchronized void a(String str, String str2, int i, int i2, boolean z) {
        String str3;
        try {
            StringBuilder sb = new StringBuilder("id = '");
            sb.append(str);
            sb.append("' AND unitid = ");
            sb.append(str2);
            sb.append(" AND level = ");
            sb.append(i);
            sb.append(" AND adSource = ");
            sb.append(i2);
            String sb2 = sb.toString();
            if (z) {
                StringBuilder sb3 = new StringBuilder();
                sb3.append(sb2);
                sb3.append(" AND is_bid_campaign = 1");
                str3 = sb3.toString();
            } else {
                StringBuilder sb4 = new StringBuilder();
                sb4.append(sb2);
                sb4.append(" AND is_bid_campaign = 0");
                str3 = sb4.toString();
            }
            if (b() != null) {
                b().delete("campaign", str3, null);
            }
        } catch (Exception unused) {
        }
    }

    public final synchronized void a(int i, String str) {
        try {
            StringBuilder sb = new StringBuilder("tab = ");
            sb.append(i);
            sb.append(" AND unitid = ");
            sb.append(str);
            String sb2 = sb.toString();
            if (b() != null) {
                b().delete("campaign", sb2, null);
            }
        } catch (Exception unused) {
        }
    }

    public final synchronized void b(String str) {
        try {
            StringBuilder sb = new StringBuilder("unitid = ");
            sb.append(str);
            String sb2 = sb.toString();
            if (b() != null) {
                b().delete("campaign", sb2, null);
            }
        } catch (Exception unused) {
        }
    }

    public final synchronized void b(String str, int i) {
        try {
            StringBuilder sb = new StringBuilder("unitid = ");
            sb.append(str);
            sb.append(" AND ia_rst = ");
            sb.append(i);
            String sb2 = sb.toString();
            if (b() != null) {
                b().delete("campaign", sb2, null);
            }
        } catch (Exception unused) {
        }
    }

    public final synchronized void b(String str, String str2) {
        try {
            StringBuilder sb = new StringBuilder("unitid = ");
            sb.append(str);
            sb.append(" AND ia_ext1 = ");
            sb.append(str2);
            String sb2 = sb.toString();
            if (b() != null) {
                b().delete("campaign", sb2, null);
            }
        } catch (Exception unused) {
        }
    }

    public final synchronized void c(String str) {
        try {
            long currentTimeMillis = System.currentTimeMillis() - 604800000;
            StringBuilder sb = new StringBuilder("unitid = ");
            sb.append(str);
            sb.append(" AND short_ctime<");
            sb.append(currentTimeMillis);
            String sb2 = sb.toString();
            if (b() != null) {
                b().delete("campaign", sb2, null);
            }
        } catch (Exception unused) {
        }
    }

    public final synchronized void a(String str, ContentValues contentValues) {
        try {
            b().update("campaign", contentValues, "id = ?", new String[]{str});
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:100:?, code lost:
        r1.put("dp", r36);
        r1.put("c", r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:75:0x0259, code lost:
        r36 = r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x025b, code lost:
        r8 = r30;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:79:0x025e, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:80:0x0260, code lost:
        r36 = r2;
        r8 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:0x0267, code lost:
        if (r1 == null) goto L_0x0280;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:88:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:92:0x0276, code lost:
        if (r1 == null) goto L_0x0280;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:94:0x0278, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:98:0x0282, code lost:
        r1 = r29;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:79:0x025e A[ExcHandler: all (th java.lang.Throwable), Splitter:B:9:0x009b] */
    /* JADX WARNING: Removed duplicated region for block: B:87:0x026e A[SYNTHETIC, Splitter:B:87:0x026e] */
    public final synchronized String d(String str) {
        JSONObject jSONObject;
        JSONArray jSONArray;
        JSONObject jSONObject2;
        Cursor cursor;
        int i;
        int i2;
        JSONObject jSONObject3;
        int i3;
        int i4;
        JSONObject jSONObject4;
        int i5;
        jSONObject = new JSONObject();
        try {
            StringBuilder sb = new StringBuilder("SELECT * FROM campaign WHERE unitid = '");
            sb.append(str);
            sb.append("'");
            String sb2 = sb.toString();
            JSONArray jSONArray2 = new JSONArray();
            JSONObject jSONObject5 = new JSONObject();
            JSONObject jSONObject6 = new JSONObject();
            JSONObject jSONObject7 = new JSONObject();
            JSONObject jSONObject8 = new JSONObject();
            JSONObject jSONObject9 = new JSONObject();
            JSONObject jSONObject10 = new JSONObject();
            JSONObject jSONObject11 = new JSONObject();
            JSONObject jSONObject12 = new JSONObject();
            Long valueOf = Long.valueOf(System.currentTimeMillis());
            long longValue = valueOf.longValue() - 86400000;
            long longValue2 = valueOf.longValue() - 172800000;
            long longValue3 = valueOf.longValue() - 259200000;
            long longValue4 = valueOf.longValue() - 345600000;
            long longValue5 = valueOf.longValue() - 432000000;
            long longValue6 = valueOf.longValue() - 518400000;
            long longValue7 = valueOf.longValue() - 604800000;
            JSONObject jSONObject13 = jSONObject;
            try {
                cursor = a().rawQuery(sb2, null);
                if (cursor != null) {
                    try {
                        if (cursor.getCount() > 0) {
                            JSONObject jSONObject14 = jSONObject5;
                            JSONObject jSONObject15 = jSONObject11;
                            JSONObject jSONObject16 = jSONObject12;
                            int i6 = 0;
                            int i7 = 0;
                            int i8 = 0;
                            int i9 = 0;
                            int i10 = 0;
                            int i11 = 0;
                            int i12 = 0;
                            while (cursor.moveToNext()) {
                                JSONObject jSONObject17 = jSONObject10;
                                if (cursor.getInt(cursor.getColumnIndex("is_deleted")) == 0) {
                                    jSONArray2.put(cursor.getString(cursor.getColumnIndex("id")));
                                }
                                long j = cursor.getLong(cursor.getColumnIndex("short_ctime"));
                                jSONArray = jSONArray2;
                                if (j > longValue && i10 < 10) {
                                    i10++;
                                    jSONObject6.put(cursor.getString(cursor.getColumnIndex("id")), cursor.getInt(cursor.getColumnIndex("is_click")));
                                } else if (j < longValue && j > longValue2 && i6 < 10) {
                                    i6++;
                                    jSONObject7.put(cursor.getString(cursor.getColumnIndex("id")), cursor.getInt(cursor.getColumnIndex("is_click")));
                                } else if (j < longValue2 && j > longValue3 && i7 < 10) {
                                    i7++;
                                    jSONObject8.put(cursor.getString(cursor.getColumnIndex("id")), cursor.getInt(cursor.getColumnIndex("is_click")));
                                } else if (j < longValue3 && j > longValue4 && i9 < 10) {
                                    i9++;
                                    jSONObject9.put(cursor.getString(cursor.getColumnIndex("id")), cursor.getInt(cursor.getColumnIndex("is_click")));
                                } else if (j >= longValue4 || j <= longValue5 || i8 >= 10) {
                                    int i13 = i6;
                                    JSONObject jSONObject18 = jSONObject17;
                                    if (j >= longValue5 || j <= longValue6) {
                                        i = i7;
                                        jSONObject3 = jSONObject15;
                                        i2 = i11;
                                    } else {
                                        i2 = i11;
                                        if (i2 < 10) {
                                            i11 = i2 + 1;
                                            int i14 = i7;
                                            JSONObject jSONObject19 = jSONObject15;
                                            jSONObject19.put(cursor.getString(cursor.getColumnIndex("id")), cursor.getInt(cursor.getColumnIndex("is_click")));
                                            jSONObject10 = jSONObject18;
                                            jSONObject15 = jSONObject19;
                                            jSONArray2 = jSONArray;
                                            i6 = i13;
                                            i7 = i14;
                                        } else {
                                            i = i7;
                                            jSONObject3 = jSONObject15;
                                        }
                                    }
                                    if (j >= longValue6 || j <= longValue7) {
                                        i4 = i2;
                                        i3 = i8;
                                        jSONObject4 = jSONObject16;
                                        i5 = i12;
                                    } else {
                                        i4 = i2;
                                        i5 = i12;
                                        if (i5 < 10) {
                                            i12 = i5 + 1;
                                            i3 = i8;
                                            jSONObject4 = jSONObject16;
                                            jSONObject4.put(cursor.getString(cursor.getColumnIndex("id")), cursor.getInt(cursor.getColumnIndex("is_click")));
                                            jSONObject10 = jSONObject18;
                                            jSONObject15 = jSONObject3;
                                            jSONObject16 = jSONObject4;
                                            jSONArray2 = jSONArray;
                                            i6 = i13;
                                            i7 = i;
                                            i11 = i4;
                                            i8 = i3;
                                        } else {
                                            i3 = i8;
                                            jSONObject4 = jSONObject16;
                                        }
                                    }
                                    i12 = i5;
                                    jSONObject10 = jSONObject18;
                                    jSONObject15 = jSONObject3;
                                    jSONObject16 = jSONObject4;
                                    jSONArray2 = jSONArray;
                                    i6 = i13;
                                    i7 = i;
                                    i11 = i4;
                                    i8 = i3;
                                } else {
                                    i8++;
                                    int i15 = i6;
                                    JSONObject jSONObject20 = jSONObject17;
                                    jSONObject20.put(cursor.getString(cursor.getColumnIndex("id")), cursor.getInt(cursor.getColumnIndex("is_click")));
                                    jSONObject10 = jSONObject20;
                                    jSONArray2 = jSONArray;
                                    i6 = i15;
                                }
                                jSONObject10 = jSONObject17;
                                jSONArray2 = jSONArray;
                            }
                            jSONArray = jSONArray2;
                            JSONObject jSONObject21 = jSONObject10;
                            JSONObject jSONObject22 = jSONObject16;
                            JSONObject jSONObject23 = jSONObject15;
                            jSONObject2 = jSONObject14;
                            jSONObject2.put("1", jSONObject6);
                            jSONObject2.put("2", jSONObject7);
                            jSONObject2.put("3", jSONObject8);
                            jSONObject2.put(Config.DATA_HISTORY_DEFAULT, jSONObject9);
                            jSONObject2.put("5", jSONObject21);
                            jSONObject2.put("6", jSONObject23);
                            jSONObject2.put("7", jSONObject22);
                        }
                    } catch (Throwable th) {
                    }
                }
                jSONArray = jSONArray2;
                jSONObject2 = jSONObject5;
            } catch (Throwable th2) {
                th = th2;
                jSONObject = jSONObject13;
                th.printStackTrace();
                return jSONObject.toString();
            }
        } catch (Throwable th3) {
            th = th3;
        }
        return jSONObject.toString();
    }

    /* JADX WARNING: Removed duplicated region for block: B:24:0x0053 A[SYNTHETIC, Splitter:B:24:0x0053] */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x005a A[SYNTHETIC, Splitter:B:29:0x005a] */
    public final synchronized int b(int i, String str) {
        int i2;
        StringBuilder sb = new StringBuilder("tab = ");
        sb.append(i);
        sb.append(" AND unitid = ");
        sb.append(str);
        i2 = 0;
        Cursor cursor = null;
        try {
            Cursor query = a().query("campaign", new String[]{" count(id) "}, sb.toString(), null, null, null, null);
            if (query != null) {
                try {
                    if (query.moveToFirst()) {
                        i2 = query.getInt(0);
                    }
                } catch (Exception e) {
                    Cursor cursor2 = query;
                    e = e;
                    cursor = cursor2;
                    try {
                        e.printStackTrace();
                        if (cursor != null) {
                        }
                        return i2;
                    } catch (Throwable th) {
                        th = th;
                        if (cursor != null) {
                        }
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    cursor = query;
                    if (cursor != null) {
                        cursor.close();
                    }
                    throw th;
                }
            }
            if (query != null) {
                query.close();
            }
        } catch (Exception e2) {
            e = e2;
            e.printStackTrace();
            if (cursor != null) {
                cursor.close();
            }
            return i2;
        }
        return i2;
    }

    public final synchronized void c() {
        try {
            long currentTimeMillis = System.currentTimeMillis() - 3600000;
            StringBuilder sb = new StringBuilder("ts<");
            sb.append(currentTimeMillis);
            sb.append(" AND ts>0");
            String sb2 = sb.toString();
            if (b() != null) {
                b().delete("campaign", sb2, null);
            }
        } catch (Exception unused) {
        }
    }

    public final synchronized void a(long j, String str) {
        try {
            long currentTimeMillis = System.currentTimeMillis();
            long j2 = currentTimeMillis - j;
            StringBuilder sb = new StringBuilder("(plctb>0 and (plctb* 1000+ts)<");
            sb.append(currentTimeMillis);
            sb.append(") or (plctb<=0 and ts<");
            sb.append(j2);
            sb.append(") and unitid=?");
            String sb2 = sb.toString();
            String[] strArr = {str};
            if (b() != null) {
                b().delete("campaign", sb2, strArr);
            }
        } catch (Exception e) {
            g.d(b, e.getMessage());
        }
    }

    public final synchronized void b(long j, String str) {
        try {
            long currentTimeMillis = System.currentTimeMillis() - j;
            StringBuilder sb = new StringBuilder("ts<");
            sb.append(currentTimeMillis);
            sb.append(" and unitid=?");
            String sb2 = sb.toString();
            String[] strArr = {str};
            if (b() != null) {
                b().delete("campaign", sb2, strArr);
            }
        } catch (Exception unused) {
        }
    }

    public final synchronized long a(CampaignEx campaignEx, String str, int i) {
        String str2;
        if (campaignEx == null) {
            return 0;
        }
        try {
            if (b() == null) {
                return -1;
            }
            ContentValues contentValues = new ContentValues();
            contentValues.put("id", campaignEx.getId());
            contentValues.put("unitid", str);
            contentValues.put("tab", Integer.valueOf(campaignEx.getTab()));
            contentValues.put(CampaignEx.JSON_KEY_PACKAGE_NAME, campaignEx.getPackageName());
            contentValues.put(NativeProtocol.BRIDGE_ARG_APP_NAME_STRING, campaignEx.getAppName());
            contentValues.put("app_desc", campaignEx.getAppDesc());
            contentValues.put(CampaignEx.JSON_KEY_APP_SIZE, campaignEx.getSize());
            contentValues.put(CampaignEx.JSON_KEY_IMAGE_SIZE, campaignEx.getImageSize());
            contentValues.put(CampaignEx.JSON_KEY_ICON_URL, campaignEx.getIconUrl());
            contentValues.put("image_url", campaignEx.getImageUrl());
            contentValues.put(CampaignEx.JSON_KEY_IMPRESSION_URL, campaignEx.getImpressionURL());
            contentValues.put(CampaignEx.JSON_KEY_NOTICE_URL, campaignEx.getNoticeUrl());
            contentValues.put("download_url", campaignEx.getClickURL());
            contentValues.put("deeplink_url", campaignEx.getDeepLinkURL());
            contentValues.put("only_impression", campaignEx.getOnlyImpressionURL());
            contentValues.put(CampaignEx.JSON_KEY_ST_TS, Long.valueOf(campaignEx.getTimestamp()));
            contentValues.put("template", Integer.valueOf(campaignEx.getTemplate()));
            contentValues.put(CampaignEx.JSON_KEY_CLICK_MODE, campaignEx.getClick_mode());
            contentValues.put(CampaignEx.JSON_KEY_LANDING_TYPE, campaignEx.getLandingType());
            contentValues.put(CampaignEx.JSON_KEY_LINK_TYPE, Integer.valueOf(campaignEx.getLinkType()));
            contentValues.put("star", Double.valueOf(campaignEx.getRating()));
            contentValues.put("cti", Integer.valueOf(campaignEx.getClickInterval()));
            contentValues.put("cpti", Integer.valueOf(campaignEx.getPreClickInterval()));
            contentValues.put("preclick", Boolean.valueOf(campaignEx.isPreClick()));
            contentValues.put("level", Integer.valueOf(campaignEx.getCacheLevel()));
            contentValues.put("adSource", Integer.valueOf(campaignEx.getType()));
            contentValues.put("ad_call", campaignEx.getAdCall());
            contentValues.put("fc_a", Integer.valueOf(campaignEx.getFca()));
            contentValues.put("fc_b", Integer.valueOf(campaignEx.getFcb()));
            contentValues.put(CampaignEx.JSON_KEY_AD_URL_LIST, campaignEx.getAd_url_list());
            contentValues.put("video_url", campaignEx.getVideoUrlEncode());
            contentValues.put(CampaignEx.JSON_KEY_VIDEO_SIZE, Integer.valueOf(campaignEx.getVideoSize()));
            contentValues.put(CampaignEx.JSON_KEY_VIDEO_LENGTHL, Integer.valueOf(campaignEx.getVideoLength()));
            contentValues.put(CampaignEx.JSON_KEY_VIDEO_RESOLUTION, campaignEx.getVideoResolution());
            contentValues.put(CampaignEx.JSON_KEY_ENDCARD_CLICK, Integer.valueOf(campaignEx.getEndcard_click_result()));
            contentValues.put(CampaignEx.JSON_KEY_WATCH_MILE, Integer.valueOf(campaignEx.getWatchMile()));
            contentValues.put("advImp", campaignEx.getAdvImp());
            contentValues.put("bty", Integer.valueOf(campaignEx.getBty()));
            contentValues.put(CampaignEx.JSON_KEY_T_IMP, Integer.valueOf(campaignEx.getTImp()));
            contentValues.put(CampaignEx.JSON_KEY_GUIDELINES, campaignEx.getGuidelines());
            contentValues.put(CampaignEx.JSON_KEY_OFFER_TYPE, Integer.valueOf(campaignEx.getOfferType()));
            contentValues.put("html_url", campaignEx.getHtmlUrl());
            contentValues.put("end_screen_url", campaignEx.getEndScreenUrl());
            contentValues.put(CampaignEx.JSON_KEY_REWARD_AMOUNT, Integer.valueOf(campaignEx.getRewardAmount()));
            contentValues.put(CampaignEx.JSON_KEY_REWARD_NAME, campaignEx.getRewardName());
            contentValues.put("reward_play_status", Integer.valueOf(campaignEx.getRewardPlayStatus()));
            contentValues.put(CampaignEx.JSON_KEY_ADV_ID, campaignEx.getAdvId());
            contentValues.put(CampaignEx.JSON_KEY_TTC_CT2, Integer.valueOf(campaignEx.getTtc_ct2() * 1000));
            contentValues.put(CampaignEx.JSON_KEY_TTC_TYPE, Integer.valueOf(campaignEx.getTtc_type()));
            contentValues.put("retarget", Integer.valueOf(campaignEx.getRetarget_offer()));
            contentValues.put("native_ad_tracking", campaignEx.getNativeVideoTrackingString());
            contentValues.put(CampaignEx.PLAYABLE_ADS_WITHOUT_VIDEO, Integer.valueOf(campaignEx.getPlayable_ads_without_video()));
            contentValues.put(CampaignEx.ENDCARD_URL, campaignEx.getendcard_url());
            contentValues.put(CampaignEx.VIDEO_END_TYPE, Integer.valueOf(campaignEx.getVideo_end_type()));
            contentValues.put(CampaignEx.LOOPBACK, campaignEx.getLoopbackString());
            contentValues.put(CampaignEx.JSON_KEY_REWARD_VIDEO_MD5, campaignEx.getVideoMD5Value());
            contentValues.put(CampaignEx.JSON_KEY_NV_T2, Integer.valueOf(campaignEx.getNvT2()));
            contentValues.put(CampaignEx.JSON_KEY_GIF_URL, campaignEx.getGifUrl());
            if (campaignEx.getRewardTemplateMode() != null) {
                contentValues.put("reward_teamplate", campaignEx.getRewardTemplateMode().a());
            }
            contentValues.put("c_coi", Integer.valueOf(campaignEx.getClickTimeOutInterval()));
            contentValues.put(CampaignEx.JSON_KEY_C_UA, Integer.valueOf(campaignEx.getcUA()));
            contentValues.put(CampaignEx.JSON_KEY_IMP_UA, Integer.valueOf(campaignEx.getImpUA()));
            contentValues.put(CampaignEx.JSON_KEY_JM_PD, Integer.valueOf(campaignEx.getJmPd()));
            contentValues.put("is_deleted", Integer.valueOf(campaignEx.getIsDeleted()));
            contentValues.put("is_click", Integer.valueOf(campaignEx.getIsClick()));
            contentValues.put("is_add_sucesful", Integer.valueOf(campaignEx.getIsAddSuccesful()));
            contentValues.put("short_ctime", String.valueOf(System.currentTimeMillis()));
            contentValues.put("ia_icon", campaignEx.getKeyIaIcon());
            contentValues.put("ia_url", campaignEx.getKeyIaUrl());
            contentValues.put("ia_rst", Integer.valueOf(campaignEx.getKeyIaRst()));
            contentValues.put("ia_ori", Integer.valueOf(campaignEx.getKeyIaOri()));
            contentValues.put("ad_type", Integer.valueOf(campaignEx.getAdType()));
            contentValues.put(CampaignEx.KEY_IA_EXT1, campaignEx.getIa_ext1());
            contentValues.put(CampaignEx.KEY_IA_EXT2, campaignEx.getIa_ext2());
            contentValues.put(CampaignEx.KEY_IS_DOWNLOAD, Integer.valueOf(campaignEx.getIsDownLoadZip()));
            contentValues.put(CampaignEx.KEY_IA_CACHE, campaignEx.getInteractiveCache());
            contentValues.put(CampaignEx.KEY_GH_ID, campaignEx.getGhId());
            contentValues.put(CampaignEx.KEY_GH_PATH, campaignEx.getGhPath());
            contentValues.put(CampaignEx.KEY_BIND_ID, campaignEx.getBindId());
            contentValues.put(CampaignEx.KEY_OC_TIME, Integer.valueOf(campaignEx.getOc_time()));
            contentValues.put(CampaignEx.KEY_OC_TYPE, Integer.valueOf(campaignEx.getOc_type()));
            contentValues.put(CampaignEx.KEY_T_LIST, campaignEx.getT_list());
            a adchoice = campaignEx.getAdchoice();
            if (adchoice != null) {
                contentValues.put(CampaignEx.KET_ADCHOICE, adchoice.c());
                contentValues.put("adchoice_size_height", Integer.valueOf(adchoice.b()));
                contentValues.put("adchoice_size_width", Integer.valueOf(adchoice.a()));
            }
            contentValues.put(CampaignEx.JSON_KEY_PLCT, Long.valueOf(campaignEx.getPlct()));
            contentValues.put(CampaignEx.JSON_KEY_PLCTB, Long.valueOf(campaignEx.getPlctb()));
            contentValues.put("is_bid_campaign", Boolean.valueOf(campaignEx.isBidCampaign()));
            contentValues.put(AdvertisementColumns.COLUMN_BID_TOKEN, campaignEx.getBidToken());
            contentValues.put(CampaignEx.JSON_KEY_MRAID, campaignEx.getMraid());
            contentValues.put("is_mraid_campaign", Boolean.valueOf(campaignEx.isMraid()));
            if (a(campaignEx.getId(), campaignEx.getTab(), str, i, campaignEx.getType(), campaignEx.isBidCampaign())) {
                if (campaignEx.isBidCampaign()) {
                    StringBuilder sb = new StringBuilder("unitid = ");
                    sb.append(str);
                    sb.append(" AND is_bid_campaign = 1");
                    str2 = sb.toString();
                } else {
                    StringBuilder sb2 = new StringBuilder("id = ");
                    sb2.append(campaignEx.getId());
                    sb2.append(" AND unitid = ");
                    sb2.append(str);
                    sb2.append(" AND is_bid_campaign = 0");
                    str2 = sb2.toString();
                }
                return (long) b().update("campaign", contentValues, str2, null);
            }
            return b().insert("campaign", null, contentValues);
        } catch (Exception unused) {
            return -1;
        }
    }

    public final synchronized long a(CampaignEx campaignEx, String str) {
        if (campaignEx == null) {
            return 0;
        }
        try {
            if (b() == null) {
                return -1;
            }
            ContentValues contentValues = new ContentValues();
            contentValues.put("id", campaignEx.getId());
            contentValues.put("unitid", str);
            contentValues.put("tab", Integer.valueOf(campaignEx.getTab()));
            contentValues.put(CampaignEx.JSON_KEY_PACKAGE_NAME, campaignEx.getPackageName());
            contentValues.put(NativeProtocol.BRIDGE_ARG_APP_NAME_STRING, campaignEx.getAppName());
            contentValues.put("app_desc", campaignEx.getAppDesc());
            contentValues.put(CampaignEx.JSON_KEY_APP_SIZE, campaignEx.getSize());
            contentValues.put(CampaignEx.JSON_KEY_IMAGE_SIZE, campaignEx.getImageSize());
            contentValues.put(CampaignEx.JSON_KEY_ICON_URL, campaignEx.getIconUrl());
            contentValues.put("image_url", campaignEx.getImageUrl());
            contentValues.put(CampaignEx.JSON_KEY_IMPRESSION_URL, campaignEx.getImpressionURL());
            contentValues.put(CampaignEx.JSON_KEY_NOTICE_URL, campaignEx.getNoticeUrl());
            contentValues.put("download_url", campaignEx.getClickURL());
            contentValues.put("deeplink_url", campaignEx.getDeepLinkURL());
            contentValues.put("only_impression", campaignEx.getOnlyImpressionURL());
            contentValues.put(CampaignEx.JSON_KEY_ST_TS, Long.valueOf(campaignEx.getTimestamp()));
            contentValues.put("template", Integer.valueOf(campaignEx.getTemplate()));
            contentValues.put(CampaignEx.JSON_KEY_CLICK_MODE, campaignEx.getClick_mode());
            contentValues.put(CampaignEx.JSON_KEY_LANDING_TYPE, campaignEx.getLandingType());
            contentValues.put(CampaignEx.JSON_KEY_LINK_TYPE, Integer.valueOf(campaignEx.getLinkType()));
            contentValues.put("star", Double.valueOf(campaignEx.getRating()));
            contentValues.put("cti", Integer.valueOf(campaignEx.getClickInterval()));
            contentValues.put("cpti", Integer.valueOf(campaignEx.getPreClickInterval()));
            contentValues.put("preclick", Boolean.valueOf(campaignEx.isPreClick()));
            contentValues.put("level", Integer.valueOf(campaignEx.getCacheLevel()));
            contentValues.put("adSource", Integer.valueOf(campaignEx.getType()));
            contentValues.put("ad_call", campaignEx.getAdCall());
            contentValues.put("fc_a", Integer.valueOf(campaignEx.getFca()));
            contentValues.put("fc_b", Integer.valueOf(campaignEx.getFcb()));
            contentValues.put(CampaignEx.JSON_KEY_AD_URL_LIST, campaignEx.getAd_url_list());
            contentValues.put("video_url", campaignEx.getVideoUrlEncode());
            contentValues.put(CampaignEx.JSON_KEY_VIDEO_SIZE, Integer.valueOf(campaignEx.getVideoSize()));
            contentValues.put(CampaignEx.JSON_KEY_VIDEO_LENGTHL, Integer.valueOf(campaignEx.getVideoLength()));
            contentValues.put(CampaignEx.JSON_KEY_VIDEO_RESOLUTION, campaignEx.getVideoResolution());
            contentValues.put(CampaignEx.JSON_KEY_ENDCARD_CLICK, Integer.valueOf(campaignEx.getEndcard_click_result()));
            contentValues.put(CampaignEx.JSON_KEY_WATCH_MILE, Integer.valueOf(campaignEx.getWatchMile()));
            contentValues.put("advImp", campaignEx.getAdvImp());
            contentValues.put("bty", Integer.valueOf(campaignEx.getBty()));
            contentValues.put(CampaignEx.JSON_KEY_T_IMP, Integer.valueOf(campaignEx.getTImp()));
            contentValues.put(CampaignEx.JSON_KEY_GUIDELINES, campaignEx.getGuidelines());
            contentValues.put(CampaignEx.JSON_KEY_OFFER_TYPE, Integer.valueOf(campaignEx.getOfferType()));
            contentValues.put("html_url", campaignEx.getHtmlUrl());
            contentValues.put("end_screen_url", campaignEx.getEndScreenUrl());
            contentValues.put(CampaignEx.JSON_KEY_REWARD_AMOUNT, Integer.valueOf(campaignEx.getRewardAmount()));
            contentValues.put(CampaignEx.JSON_KEY_REWARD_NAME, campaignEx.getRewardName());
            contentValues.put("reward_play_status", Integer.valueOf(campaignEx.getRewardPlayStatus()));
            contentValues.put(CampaignEx.JSON_KEY_ADV_ID, campaignEx.getAdvId());
            contentValues.put(CampaignEx.JSON_KEY_TTC_CT2, Integer.valueOf(campaignEx.getTtc_ct2() * 1000));
            contentValues.put(CampaignEx.JSON_KEY_TTC_TYPE, Integer.valueOf(campaignEx.getTtc_type()));
            contentValues.put("retarget", Integer.valueOf(campaignEx.getRetarget_offer()));
            contentValues.put("native_ad_tracking", campaignEx.getNativeVideoTrackingString());
            contentValues.put(CampaignEx.PLAYABLE_ADS_WITHOUT_VIDEO, Integer.valueOf(campaignEx.getPlayable_ads_without_video()));
            contentValues.put(CampaignEx.ENDCARD_URL, campaignEx.getendcard_url());
            contentValues.put(CampaignEx.VIDEO_END_TYPE, Integer.valueOf(campaignEx.getVideo_end_type()));
            contentValues.put(CampaignEx.LOOPBACK, campaignEx.getLoopbackString());
            contentValues.put(CampaignEx.JSON_KEY_REWARD_VIDEO_MD5, campaignEx.getVideoMD5Value());
            contentValues.put(CampaignEx.JSON_KEY_NV_T2, Integer.valueOf(campaignEx.getNvT2()));
            contentValues.put(CampaignEx.JSON_KEY_GIF_URL, campaignEx.getGifUrl());
            if (campaignEx.getRewardTemplateMode() != null) {
                contentValues.put("reward_teamplate", campaignEx.getRewardTemplateMode().a());
            }
            contentValues.put("c_coi", Integer.valueOf(campaignEx.getClickTimeOutInterval()));
            contentValues.put(CampaignEx.JSON_KEY_C_UA, Integer.valueOf(campaignEx.getcUA()));
            contentValues.put(CampaignEx.JSON_KEY_IMP_UA, Integer.valueOf(campaignEx.getImpUA()));
            contentValues.put(CampaignEx.JSON_KEY_JM_PD, Integer.valueOf(campaignEx.getJmPd()));
            contentValues.put("is_deleted", Integer.valueOf(campaignEx.getIsDeleted()));
            contentValues.put("is_click", Integer.valueOf(campaignEx.getIsClick()));
            contentValues.put("is_add_sucesful", Integer.valueOf(campaignEx.getIsAddSuccesful()));
            contentValues.put("short_ctime", String.valueOf(System.currentTimeMillis()));
            contentValues.put("ia_icon", campaignEx.getKeyIaIcon());
            contentValues.put("ia_url", campaignEx.getKeyIaUrl());
            contentValues.put("ia_rst", Integer.valueOf(campaignEx.getKeyIaRst()));
            contentValues.put("ia_ori", Integer.valueOf(campaignEx.getKeyIaOri()));
            contentValues.put("ad_type", Integer.valueOf(campaignEx.getAdType()));
            contentValues.put(CampaignEx.KEY_IA_EXT1, campaignEx.getIa_ext1());
            contentValues.put(CampaignEx.KEY_IA_EXT2, campaignEx.getIa_ext2());
            contentValues.put(CampaignEx.KEY_IS_DOWNLOAD, Integer.valueOf(campaignEx.getIsDownLoadZip()));
            contentValues.put(CampaignEx.KEY_IA_CACHE, campaignEx.getInteractiveCache());
            contentValues.put(CampaignEx.KEY_GH_ID, campaignEx.getGhId());
            contentValues.put(CampaignEx.KEY_GH_PATH, campaignEx.getGhPath());
            contentValues.put(CampaignEx.KEY_BIND_ID, campaignEx.getBindId());
            contentValues.put(CampaignEx.KEY_OC_TYPE, Integer.valueOf(campaignEx.getOc_type()));
            contentValues.put(CampaignEx.KEY_OC_TIME, Integer.valueOf(campaignEx.getOc_time()));
            contentValues.put(CampaignEx.KEY_T_LIST, campaignEx.getT_list());
            a adchoice = campaignEx.getAdchoice();
            if (adchoice != null) {
                contentValues.put(CampaignEx.KET_ADCHOICE, adchoice.c());
                contentValues.put("adchoice_size_height", Integer.valueOf(adchoice.b()));
                contentValues.put("adchoice_size_width", Integer.valueOf(adchoice.a()));
            }
            contentValues.put(CampaignEx.JSON_KEY_PLCT, Long.valueOf(campaignEx.getPlct()));
            contentValues.put(CampaignEx.JSON_KEY_PLCTB, Long.valueOf(campaignEx.getPlctb()));
            contentValues.put("is_bid_campaign", Boolean.valueOf(campaignEx.isBidCampaign()));
            contentValues.put(AdvertisementColumns.COLUMN_BID_TOKEN, campaignEx.getBidToken());
            contentValues.put(CampaignEx.JSON_KEY_MRAID, campaignEx.getMraid());
            contentValues.put("is_mraid_campaign", Boolean.valueOf(campaignEx.isMraid()));
            return b().insert("campaign", null, contentValues);
        } catch (Exception unused) {
            return -1;
        }
    }

    public final synchronized void a(final List<CampaignEx> list, final String str) {
        if (list.size() != 0) {
            new Thread(new Runnable() {
                public final void run() {
                    for (CampaignEx a2 : list) {
                        f.this.a(a2, str, 0);
                    }
                }
            }).start();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x001d, code lost:
        return;
     */
    public final synchronized void b(final List<CampaignEx> list, final String str) {
        if (list != null) {
            if (list.size() != 0) {
                new Thread(new Runnable() {
                    public final void run() {
                        for (CampaignEx a2 : list) {
                            f.this.a(a2, str);
                        }
                    }
                }).start();
            }
        }
    }

    public final synchronized int a(String str, long j) {
        try {
            b.a();
            com.mintegral.msdk.b.a b2 = b.b(com.mintegral.msdk.base.controller.a.d().j());
            if (b2 == null) {
                b.a();
                b2 = b.b();
            }
            long ak = b2.ak() * 1000;
            a(ak, str);
            List<CampaignEx> a2 = a(str, 1, false);
            if (a2 != null) {
                if (!a2.isEmpty()) {
                    for (CampaignEx isSpareOffer : a2) {
                        if (!isSpareOffer.isSpareOffer(j, ak)) {
                            return 0;
                        }
                    }
                    return 1;
                }
            }
            return -1;
        } catch (Exception unused) {
            return 0;
        }
    }

    public final synchronized void e(String str) {
        StringBuilder sb = new StringBuilder("UPDATE campaign SET is_download_zip='1' WHERE ia_url='");
        sb.append(str);
        sb.append("'");
        a().execSQL(sb.toString());
    }

    public final synchronized boolean a(String str, int i, String str2, int i2, int i3, boolean z) {
        String str3;
        String str4 = "SELECT id FROM campaign WHERE ";
        if (z) {
            StringBuilder sb = new StringBuilder();
            sb.append(str4);
            sb.append("unitid = ");
            sb.append(str2);
            sb.append(" AND is_bid_campaign = 1");
            str3 = sb.toString();
        } else {
            StringBuilder sb2 = new StringBuilder();
            sb2.append(str4);
            sb2.append("id='");
            sb2.append(str);
            sb2.append("' AND tab=");
            sb2.append(i);
            sb2.append(" AND unitid = '");
            sb2.append(str2);
            sb2.append("' AND level = ");
            sb2.append(i2);
            sb2.append(" AND adSource = ");
            sb2.append(i3);
            sb2.append(" AND is_bid_campaign = 0");
            str3 = sb2.toString();
        }
        Cursor rawQuery = a().rawQuery(str3, null);
        if (rawQuery == null || rawQuery.getCount() <= 0) {
            if (rawQuery != null) {
                rawQuery.close();
            }
            return false;
        }
        rawQuery.close();
        return true;
    }

    public final List<CampaignEx> a(String str, int i, int i2, int i3) {
        StringBuilder sb = new StringBuilder(" WHERE unitid = '");
        sb.append(str);
        sb.append("' AND level = ");
        sb.append(i2);
        sb.append(" AND adSource = ");
        sb.append(i3);
        String sb2 = sb.toString();
        String str2 = "";
        if (i > 0) {
            StringBuilder sb3 = new StringBuilder(" LIMIT ");
            sb3.append(i);
            str2 = sb3.toString();
        }
        StringBuilder sb4 = new StringBuilder("SELECT * FROM campaign");
        sb4.append(sb2);
        sb4.append(str2);
        return l(sb4.toString());
    }

    public final synchronized List<CampaignEx> a(String str, int i, boolean z) {
        StringBuilder sb;
        StringBuilder sb2 = new StringBuilder(" WHERE unitid = '");
        sb2.append(str);
        sb2.append("' AND level = 0 AND adSource = ");
        sb2.append(i);
        String sb3 = sb2.toString();
        if (z) {
            StringBuilder sb4 = new StringBuilder();
            sb4.append(sb3);
            sb4.append(" AND is_bid_campaign = 1");
            sb3 = sb4.toString();
        }
        sb = new StringBuilder("SELECT * FROM campaign");
        sb.append(sb3);
        sb.append("");
        return l(sb.toString());
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0017, code lost:
        if (r10.getCount() <= 0) goto L_0x046e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0019, code lost:
        r1 = new java.util.ArrayList();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0022, code lost:
        if (r10.moveToNext() == false) goto L_0x0467;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0024, code lost:
        r2 = new com.mintegral.msdk.base.entity.CampaignEx();
        r2.setId(r10.getString(r10.getColumnIndex("id")));
        r2.setTab(r10.getInt(r10.getColumnIndex("tab")));
        r2.setPackageName(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_PACKAGE_NAME)));
        r2.setAppName(r10.getString(r10.getColumnIndex(com.facebook.internal.NativeProtocol.BRIDGE_ARG_APP_NAME_STRING)));
        r2.setAppDesc(r10.getString(r10.getColumnIndex("app_desc")));
        r2.setSize(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_APP_SIZE)));
        r2.setImageSize(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_IMAGE_SIZE)));
        r2.setIconUrl(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_ICON_URL)));
        r2.setImageUrl(r10.getString(r10.getColumnIndex("image_url")));
        r2.setImpressionURL(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_IMPRESSION_URL)));
        r2.setNoticeUrl(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_NOTICE_URL)));
        r2.setClickURL(r10.getString(r10.getColumnIndex("download_url")));
        r2.setDeepLinkUrl(r10.getString(r10.getColumnIndex("deeplink_url")));
        r2.setOnlyImpressionURL(r10.getString(r10.getColumnIndex("only_impression")));
        r4 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x00eb, code lost:
        if (r10.getInt(r10.getColumnIndex("preclick")) != 1) goto L_0x00ef;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x00ed, code lost:
        r3 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x00ef, code lost:
        r3 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x00f0, code lost:
        r2.setPreClick(r3);
        r2.setTemplate(r10.getInt(r10.getColumnIndex("template")));
        r2.setLandingType(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_LANDING_TYPE)));
        r2.setLinkType(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_LINK_TYPE)));
        r2.setClick_mode(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_CLICK_MODE)));
        r2.setRating(java.lang.Double.parseDouble(r10.getString(r10.getColumnIndex("star"))));
        r2.setClickInterval(r10.getInt(r10.getColumnIndex("cti")));
        r2.setPreClickInterval(r10.getInt(r10.getColumnIndex("cpti")));
        r2.setTimestamp(r10.getLong(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_ST_TS)));
        r2.setCacheLevel(r10.getInt(r10.getColumnIndex("level")));
        r2.setType(r10.getInt(r10.getColumnIndex("adSource")));
        r2.setAdCall(r10.getString(r10.getColumnIndex("ad_call")));
        r2.setFca(r10.getInt(r10.getColumnIndex("fc_a")));
        r2.setFcb(r10.getInt(r10.getColumnIndex("fc_b")));
        r2.setAd_url_list(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_AD_URL_LIST)));
        r2.setVideoLength(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_VIDEO_LENGTHL)));
        r2.setVideoSize(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_VIDEO_SIZE)));
        r2.setVideoResolution(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_VIDEO_RESOLUTION)));
        r2.setEndcard_click_result(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_ENDCARD_CLICK)));
        r2.setVideoUrlEncode(r10.getString(r10.getColumnIndex("video_url")));
        r2.setWatchMile(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_WATCH_MILE)));
        r2.setTImp(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_T_IMP)));
        r2.setBty(r10.getInt(r10.getColumnIndex("bty")));
        r2.setAdvImp(r10.getString(r10.getColumnIndex("advImp")));
        r2.setOfferType(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_OFFER_TYPE)));
        r2.setGuidelines(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_GUIDELINES)));
        r2.setHtmlUrl(r10.getString(r10.getColumnIndex("html_url")));
        r2.setEndScreenUrl(r10.getString(r10.getColumnIndex("end_screen_url")));
        r2.setRewardName(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_REWARD_NAME)));
        r2.setRewardAmount(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_REWARD_AMOUNT)));
        r2.setRewardPlayStatus(r10.getInt(r10.getColumnIndex("reward_play_status")));
        r2.setAdvId(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_ADV_ID)));
        r2.setTtc_ct2(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_TTC_CT2)));
        r2.setTtc_type(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_TTC_TYPE)));
        r2.setRetarget_offer(r10.getInt(r10.getColumnIndex("retarget")));
        r2.setCampaignUnitId(r10.getString(r10.getColumnIndex("unitid")));
        r2.setNativeVideoTracking(com.mintegral.msdk.base.entity.CampaignEx.TrackingStr2Object(r10.getString(r10.getColumnIndex("native_ad_tracking"))));
        r2.setNativeVideoTrackingString(r10.getString(r10.getColumnIndex("native_ad_tracking")));
        r2.setVideo_end_type(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.VIDEO_END_TYPE)));
        r2.setendcard_url(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.ENDCARD_URL)));
        r2.setPlayable_ads_without_video(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.PLAYABLE_ADS_WITHOUT_VIDEO)));
        r2.setLoopbackString(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.LOOPBACK)));
        r2.setLoopbackMap(com.mintegral.msdk.base.entity.CampaignEx.loopbackStrToMap(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.LOOPBACK))));
        r2.setRewardTemplateMode(com.mintegral.msdk.base.entity.CampaignEx.c.a(r10.getString(r10.getColumnIndex("reward_teamplate"))));
        r2.setVideoMD5Value(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_REWARD_VIDEO_MD5)));
        r2.setGifUrl(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_GIF_URL)));
        r2.setNvT2(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_NV_T2)));
        r2.setClickTimeOutInterval(r10.getInt(r10.getColumnIndex("c_coi")));
        r2.setcUA(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_C_UA)));
        r2.setImpUA(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_IMP_UA)));
        r2.setGhId(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.KEY_GH_ID)));
        r2.setGhPath(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.KEY_GH_PATH)));
        r2.setBindId(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.KEY_BIND_ID)));
        r2.setOc_time(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.KEY_OC_TIME)));
        r2.setOc_type(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.KEY_OC_TYPE)));
        r2.setT_list(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.KEY_T_LIST)));
        r3 = r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.KET_ADCHOICE));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x03dc, code lost:
        if (android.text.TextUtils.isEmpty(r3) != false) goto L_0x03e5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x03de, code lost:
        r2.setAdchoice(com.mintegral.msdk.base.entity.CampaignEx.a.a(r3));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x03e5, code lost:
        r2.setAdchoiceSizeHeight(r10.getInt(r10.getColumnIndex("adchoice_size_height")));
        r2.setAdchoiceSizeWidth(r10.getInt(r10.getColumnIndex("adchoice_size_width")));
        r2.setPlct(r10.getLong(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_PLCT)));
        r2.setPlctb(r10.getLong(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_PLCTB)));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0423, code lost:
        if (r10.getInt(r10.getColumnIndex("is_bid_campaign")) != 1) goto L_0x0427;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0425, code lost:
        r3 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0427, code lost:
        r3 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0428, code lost:
        r2.setIsBidCampaign(r3);
        r2.setBidToken(r10.getString(r10.getColumnIndex(com.vungle.warren.model.AdvertisementDBAdapter.AdvertisementColumns.COLUMN_BID_TOKEN)));
        r2.setAdType(r10.getInt(r10.getColumnIndex("ad_type")));
        r2.setMraid(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_MRAID)));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x045c, code lost:
        if (r10.getInt(r10.getColumnIndex("is_mraid_campaign")) != 1) goto L_0x045f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x045e, code lost:
        r4 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x045f, code lost:
        r2.setIsMraid(r4);
        r1.add(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0467, code lost:
        if (r10 == null) goto L_0x046c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:?, code lost:
        r10.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x046d, code lost:
        return r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x046e, code lost:
        if (r10 == null) goto L_0x048d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:?, code lost:
        r10.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x0478, code lost:
        r1 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x048a, code lost:
        if (r10 != null) goto L_0x0470;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x048e, code lost:
        return null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x0496, code lost:
        r10 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x049a, code lost:
        throw r10;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0011, code lost:
        if (r10 == null) goto L_0x046e;
     */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x0492 A[SYNTHETIC, Splitter:B:60:0x0492] */
    private synchronized List<CampaignEx> l(String str) {
        Cursor cursor;
        try {
            synchronized (new Object()) {
                try {
                    cursor = a().rawQuery(str, null);
                    try {
                    } catch (Throwable th) {
                        th = th;
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    cursor = null;
                    throw th;
                }
            }
        } catch (Exception e) {
            e = e;
            cursor = null;
            try {
                if (MIntegralConstans.DEBUG) {
                    e.printStackTrace();
                }
            } catch (Throwable th3) {
                th = th3;
                if (cursor != null) {
                }
                throw th;
            }
        } catch (Throwable th4) {
            th = th4;
            cursor = null;
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x003c, code lost:
        if (r10.getCount() <= 0) goto L_0x0492;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x003e, code lost:
        r1 = new java.util.ArrayList();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0047, code lost:
        if (r10.moveToNext() == false) goto L_0x048c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0049, code lost:
        r2 = new com.mintegral.msdk.base.entity.CampaignEx();
        r2.setId(r10.getString(r10.getColumnIndex("id")));
        r2.setTab(r10.getInt(r10.getColumnIndex("tab")));
        r2.setPackageName(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_PACKAGE_NAME)));
        r2.setAppName(r10.getString(r10.getColumnIndex(com.facebook.internal.NativeProtocol.BRIDGE_ARG_APP_NAME_STRING)));
        r2.setAppDesc(r10.getString(r10.getColumnIndex("app_desc")));
        r2.setSize(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_APP_SIZE)));
        r2.setImageSize(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_IMAGE_SIZE)));
        r2.setIconUrl(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_ICON_URL)));
        r2.setImageUrl(r10.getString(r10.getColumnIndex("image_url")));
        r2.setImpressionURL(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_IMPRESSION_URL)));
        r2.setNoticeUrl(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_NOTICE_URL)));
        r2.setClickURL(r10.getString(r10.getColumnIndex("download_url")));
        r2.setDeepLinkUrl(r10.getString(r10.getColumnIndex("deeplink_url")));
        r2.setOnlyImpressionURL(r10.getString(r10.getColumnIndex("only_impression")));
        r4 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0110, code lost:
        if (r10.getInt(r10.getColumnIndex("preclick")) != 1) goto L_0x0114;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0112, code lost:
        r3 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0114, code lost:
        r3 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0115, code lost:
        r2.setPreClick(r3);
        r2.setTemplate(r10.getInt(r10.getColumnIndex("template")));
        r2.setLandingType(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_LANDING_TYPE)));
        r2.setLinkType(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_LINK_TYPE)));
        r2.setClick_mode(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_CLICK_MODE)));
        r2.setRating(java.lang.Double.parseDouble(r10.getString(r10.getColumnIndex("star"))));
        r2.setClickInterval(r10.getInt(r10.getColumnIndex("cti")));
        r2.setPreClickInterval(r10.getInt(r10.getColumnIndex("cpti")));
        r2.setTimestamp(r10.getLong(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_ST_TS)));
        r2.setCacheLevel(r10.getInt(r10.getColumnIndex("level")));
        r2.setType(r10.getInt(r10.getColumnIndex("adSource")));
        r2.setAdCall(r10.getString(r10.getColumnIndex("ad_call")));
        r2.setFca(r10.getInt(r10.getColumnIndex("fc_a")));
        r2.setFcb(r10.getInt(r10.getColumnIndex("fc_b")));
        r2.setAd_url_list(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_AD_URL_LIST)));
        r2.setVideoLength(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_VIDEO_LENGTHL)));
        r2.setVideoSize(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_VIDEO_SIZE)));
        r2.setVideoResolution(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_VIDEO_RESOLUTION)));
        r2.setEndcard_click_result(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_ENDCARD_CLICK)));
        r2.setVideoUrlEncode(r10.getString(r10.getColumnIndex("video_url")));
        r2.setWatchMile(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_WATCH_MILE)));
        r2.setTImp(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_T_IMP)));
        r2.setBty(r10.getInt(r10.getColumnIndex("bty")));
        r2.setAdvImp(r10.getString(r10.getColumnIndex("advImp")));
        r2.setOfferType(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_OFFER_TYPE)));
        r2.setGuidelines(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_GUIDELINES)));
        r2.setHtmlUrl(r10.getString(r10.getColumnIndex("html_url")));
        r2.setEndScreenUrl(r10.getString(r10.getColumnIndex("end_screen_url")));
        r2.setRewardName(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_REWARD_NAME)));
        r2.setRewardAmount(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_REWARD_AMOUNT)));
        r2.setRewardPlayStatus(r10.getInt(r10.getColumnIndex("reward_play_status")));
        r2.setAdvId(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_ADV_ID)));
        r2.setTtc_ct2(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_TTC_CT2)));
        r2.setTtc_type(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_TTC_TYPE)));
        r2.setRetarget_offer(r10.getInt(r10.getColumnIndex("retarget")));
        r2.setCampaignUnitId(r10.getString(r10.getColumnIndex("unitid")));
        r2.setNativeVideoTracking(com.mintegral.msdk.base.entity.CampaignEx.TrackingStr2Object(r10.getString(r10.getColumnIndex("native_ad_tracking"))));
        r2.setNativeVideoTrackingString(r10.getString(r10.getColumnIndex("native_ad_tracking")));
        r2.setVideo_end_type(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.VIDEO_END_TYPE)));
        r2.setendcard_url(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.ENDCARD_URL)));
        r2.setPlayable_ads_without_video(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.PLAYABLE_ADS_WITHOUT_VIDEO)));
        r2.setLoopbackString(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.LOOPBACK)));
        r2.setLoopbackMap(com.mintegral.msdk.base.entity.CampaignEx.loopbackStrToMap(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.LOOPBACK))));
        r2.setRewardTemplateMode(com.mintegral.msdk.base.entity.CampaignEx.c.a(r10.getString(r10.getColumnIndex("reward_teamplate"))));
        r2.setVideoMD5Value(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_REWARD_VIDEO_MD5)));
        r2.setGifUrl(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_GIF_URL)));
        r2.setNvT2(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_NV_T2)));
        r2.setClickTimeOutInterval(r10.getInt(r10.getColumnIndex("c_coi")));
        r2.setcUA(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_C_UA)));
        r2.setImpUA(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_IMP_UA)));
        r2.setGhId(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.KEY_GH_ID)));
        r2.setGhPath(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.KEY_GH_PATH)));
        r2.setBindId(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.KEY_BIND_ID)));
        r2.setOc_time(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.KEY_OC_TIME)));
        r2.setOc_type(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.KEY_OC_TYPE)));
        r2.setT_list(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.KEY_T_LIST)));
        r3 = r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.KET_ADCHOICE));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0401, code lost:
        if (android.text.TextUtils.isEmpty(r3) != false) goto L_0x040a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0403, code lost:
        r2.setAdchoice(com.mintegral.msdk.base.entity.CampaignEx.a.a(r3));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x040a, code lost:
        r2.setAdchoiceSizeHeight(r10.getInt(r10.getColumnIndex("adchoice_size_height")));
        r2.setAdchoiceSizeWidth(r10.getInt(r10.getColumnIndex("adchoice_size_width")));
        r2.setPlct(r10.getLong(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_PLCT)));
        r2.setPlctb(r10.getLong(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_PLCTB)));
        r2.setAdType(r10.getInt(r10.getColumnIndex("ad_type")));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0455, code lost:
        if (r10.getInt(r10.getColumnIndex("is_bid_campaign")) != 1) goto L_0x0459;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0457, code lost:
        r3 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0459, code lost:
        r3 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x045a, code lost:
        r2.setIsBidCampaign(r3);
        r2.setBidToken(r10.getString(r10.getColumnIndex(com.vungle.warren.model.AdvertisementDBAdapter.AdvertisementColumns.COLUMN_BID_TOKEN)));
        r2.setMraid(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_MRAID)));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0481, code lost:
        if (r10.getInt(r10.getColumnIndex("is_mraid_campaign")) != 1) goto L_0x0484;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0483, code lost:
        r4 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0484, code lost:
        r2.setIsMraid(r4);
        r1.add(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x048c, code lost:
        if (r10 == null) goto L_0x0491;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x048e, code lost:
        r10.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0491, code lost:
        return r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0492, code lost:
        if (r10 == null) goto L_0x04ad;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x0499, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x04a8, code lost:
        if (r10 == null) goto L_0x04ad;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x04aa, code lost:
        r10.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x04ad, code lost:
        return null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0036, code lost:
        if (r10 == null) goto L_0x0492;
     */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x04a3  */
    public final List<CampaignEx> f(String str) {
        Cursor cursor;
        StringBuilder sb = new StringBuilder(" WHERE unitid = '");
        sb.append(str);
        sb.append("' AND level = 0 AND adSource = 1 AND reward_play_status = 0");
        String sb2 = sb.toString();
        StringBuilder sb3 = new StringBuilder("SELECT * FROM campaign");
        sb3.append(sb2);
        sb3.append("");
        String sb4 = sb3.toString();
        try {
            synchronized (new Object()) {
                try {
                    cursor = a().rawQuery(sb4, null);
                    try {
                    } catch (Throwable th) {
                        th = th;
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    cursor = null;
                    throw th;
                }
            }
        } catch (Exception unused) {
            cursor = null;
        } catch (Throwable th3) {
            Throwable th4 = th3;
            cursor = null;
            if (cursor != null) {
                cursor.close();
            }
            throw th4;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0044, code lost:
        if (r9.getCount() <= 0) goto L_0x04f5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0046, code lost:
        r0 = new java.util.ArrayList();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x004f, code lost:
        if (r9.moveToNext() == false) goto L_0x04ef;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0051, code lost:
        r1 = new com.mintegral.msdk.base.entity.CampaignEx();
        r1.setId(r9.getString(r9.getColumnIndex("id")));
        r1.setTab(r9.getInt(r9.getColumnIndex("tab")));
        r1.setPackageName(r9.getString(r9.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_PACKAGE_NAME)));
        r1.setAppName(r9.getString(r9.getColumnIndex(com.facebook.internal.NativeProtocol.BRIDGE_ARG_APP_NAME_STRING)));
        r1.setAppDesc(r9.getString(r9.getColumnIndex("app_desc")));
        r1.setSize(r9.getString(r9.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_APP_SIZE)));
        r1.setImageSize(r9.getString(r9.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_IMAGE_SIZE)));
        r1.setIconUrl(r9.getString(r9.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_ICON_URL)));
        r1.setImageUrl(r9.getString(r9.getColumnIndex("image_url")));
        r1.setImpressionURL(r9.getString(r9.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_IMPRESSION_URL)));
        r1.setNoticeUrl(r9.getString(r9.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_NOTICE_URL)));
        r1.setClickURL(r9.getString(r9.getColumnIndex("download_url")));
        r1.setDeepLinkUrl(r9.getString(r9.getColumnIndex("deeplink_url")));
        r1.setOnlyImpressionURL(r9.getString(r9.getColumnIndex("only_impression")));
        r3 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0118, code lost:
        if (r9.getInt(r9.getColumnIndex("preclick")) != 1) goto L_0x011c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x011a, code lost:
        r2 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x011c, code lost:
        r2 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x011d, code lost:
        r1.setPreClick(r2);
        r1.setTemplate(r9.getInt(r9.getColumnIndex("template")));
        r1.setLandingType(r9.getString(r9.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_LANDING_TYPE)));
        r1.setLinkType(r9.getInt(r9.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_LINK_TYPE)));
        r1.setClick_mode(r9.getString(r9.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_CLICK_MODE)));
        r1.setRating(java.lang.Double.parseDouble(r9.getString(r9.getColumnIndex("star"))));
        r1.setClickInterval(r9.getInt(r9.getColumnIndex("cti")));
        r1.setPreClickInterval(r9.getInt(r9.getColumnIndex("cpti")));
        r1.setTimestamp(r9.getLong(r9.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_ST_TS)));
        r1.setCacheLevel(r9.getInt(r9.getColumnIndex("level")));
        r1.setAdCall(r9.getString(r9.getColumnIndex("ad_call")));
        r1.setFca(r9.getInt(r9.getColumnIndex("fc_a")));
        r1.setFcb(r9.getInt(r9.getColumnIndex("fc_b")));
        r1.setAd_url_list(r9.getString(r9.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_AD_URL_LIST)));
        r1.setVideoLength(r9.getInt(r9.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_VIDEO_LENGTHL)));
        r1.setVideoSize(r9.getInt(r9.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_VIDEO_SIZE)));
        r1.setVideoResolution(r9.getString(r9.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_VIDEO_RESOLUTION)));
        r1.setEndcard_click_result(r9.getInt(r9.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_ENDCARD_CLICK)));
        r1.setVideoUrlEncode(r9.getString(r9.getColumnIndex("video_url")));
        r1.setWatchMile(r9.getInt(r9.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_WATCH_MILE)));
        r1.setTImp(r9.getInt(r9.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_T_IMP)));
        r1.setBty(r9.getInt(r9.getColumnIndex("bty")));
        r1.setAdvImp(r9.getString(r9.getColumnIndex("advImp")));
        r1.setGuidelines(r9.getString(r9.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_GUIDELINES)));
        r1.setOfferType(r9.getInt(r9.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_OFFER_TYPE)));
        r1.setHtmlUrl(r9.getString(r9.getColumnIndex("html_url")));
        r1.setGuidelines(r9.getString(r9.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_GUIDELINES)));
        r1.setHtmlUrl(r9.getString(r9.getColumnIndex("html_url")));
        r1.setEndScreenUrl(r9.getString(r9.getColumnIndex("end_screen_url")));
        r1.setRewardName(r9.getString(r9.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_REWARD_NAME)));
        r1.setRewardAmount(r9.getInt(r9.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_REWARD_AMOUNT)));
        r1.setRewardPlayStatus(r9.getInt(r9.getColumnIndex("reward_play_status")));
        r1.setAdvId(r9.getString(r9.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_ADV_ID)));
        r1.setTtc_ct2(r9.getInt(r9.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_TTC_CT2)));
        r1.setTtc_type(r9.getInt(r9.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_TTC_TYPE)));
        r1.setRetarget_offer(r9.getInt(r9.getColumnIndex("retarget")));
        r1.setCampaignUnitId(r9.getString(r9.getColumnIndex("unitid")));
        r1.setNativeVideoTracking(com.mintegral.msdk.base.entity.CampaignEx.TrackingStr2Object(r9.getString(r9.getColumnIndex("native_ad_tracking"))));
        r1.setNativeVideoTrackingString(r9.getString(r9.getColumnIndex("native_ad_tracking")));
        r1.setVideo_end_type(r9.getInt(r9.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.VIDEO_END_TYPE)));
        r1.setendcard_url(r9.getString(r9.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.ENDCARD_URL)));
        r1.setPlayable_ads_without_video(r9.getInt(r9.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.PLAYABLE_ADS_WITHOUT_VIDEO)));
        r1.setLoopbackString(r9.getString(r9.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.LOOPBACK)));
        r1.setLoopbackMap(com.mintegral.msdk.base.entity.CampaignEx.loopbackStrToMap(r9.getString(r9.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.LOOPBACK))));
        r1.setRewardTemplateMode(com.mintegral.msdk.base.entity.CampaignEx.c.a(r9.getString(r9.getColumnIndex("reward_teamplate"))));
        r1.setVideoMD5Value(r9.getString(r9.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_REWARD_VIDEO_MD5)));
        r1.setGifUrl(r9.getString(r9.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_GIF_URL)));
        r1.setNvT2(r9.getInt(r9.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_NV_T2)));
        r1.setClickTimeOutInterval(r9.getInt(r9.getColumnIndex("c_coi")));
        r1.setcUA(r9.getInt(r9.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_C_UA)));
        r1.setImpUA(r9.getInt(r9.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_IMP_UA)));
        r1.setKeyIaOri(r9.getInt(r9.getColumnIndex("ia_ori")));
        r1.setAdType(r9.getInt(r9.getColumnIndex("ad_type")));
        r1.setIa_ext1(r9.getString(r9.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.KEY_IA_EXT1)));
        r1.setIa_ext2(r9.getString(r9.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.KEY_IA_EXT2)));
        r1.setKeyIaRst(r9.getInt(r9.getColumnIndex("ia_rst")));
        r1.setKeyIaUrl(r9.getString(r9.getColumnIndex("ia_url")));
        r1.setKeyIaIcon(r9.getString(r9.getColumnIndex("ia_icon")));
        r1.setGhId(r9.getString(r9.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.KEY_GH_ID)));
        r1.setGhPath(r9.getString(r9.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.KEY_GH_PATH)));
        r1.setBindId(r9.getString(r9.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.KEY_BIND_ID)));
        r1.setOc_time(r9.getInt(r9.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.KEY_OC_TIME)));
        r1.setOc_type(r9.getInt(r9.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.KEY_OC_TYPE)));
        r1.setT_list(r9.getString(r9.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.KEY_T_LIST)));
        r2 = r9.getString(r9.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.KET_ADCHOICE));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0471, code lost:
        if (android.text.TextUtils.isEmpty(r2) != false) goto L_0x047a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0473, code lost:
        r1.setAdchoice(com.mintegral.msdk.base.entity.CampaignEx.a.a(r2));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x047a, code lost:
        r1.setAdchoiceSizeHeight(r9.getInt(r9.getColumnIndex("adchoice_size_height")));
        r1.setAdchoiceSizeWidth(r9.getInt(r9.getColumnIndex("adchoice_size_width")));
        r1.setPlct(r9.getLong(r9.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_PLCT)));
        r1.setPlctb(r9.getLong(r9.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_PLCTB)));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x04b8, code lost:
        if (r9.getInt(r9.getColumnIndex("is_bid_campaign")) != 1) goto L_0x04bc;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x04ba, code lost:
        r2 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x04bc, code lost:
        r2 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x04bd, code lost:
        r1.setIsBidCampaign(r2);
        r1.setBidToken(r9.getString(r9.getColumnIndex(com.vungle.warren.model.AdvertisementDBAdapter.AdvertisementColumns.COLUMN_BID_TOKEN)));
        r1.setMraid(r9.getString(r9.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_MRAID)));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x04e4, code lost:
        if (r9.getInt(r9.getColumnIndex("is_mraid_campaign")) != 1) goto L_0x04e7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x04e6, code lost:
        r3 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x04e7, code lost:
        r1.setIsMraid(r3);
        r0.add(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x04ef, code lost:
        if (r9 == null) goto L_0x04f4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x04f1, code lost:
        r9.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x04f4, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x04f5, code lost:
        if (r9 == null) goto L_0x0510;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x04fc, code lost:
        r10 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x050b, code lost:
        if (r9 == null) goto L_0x0510;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x050d, code lost:
        r9.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x0510, code lost:
        return null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x003e, code lost:
        if (r9 == null) goto L_0x04f5;
     */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x0506  */
    public final List<CampaignEx> c(int i, String str) {
        Cursor cursor;
        StringBuilder sb = new StringBuilder(" WHERE tab = ");
        sb.append(i);
        sb.append(" AND unitid = '");
        sb.append(str);
        sb.append("'");
        String sb2 = sb.toString();
        StringBuilder sb3 = new StringBuilder("SELECT * FROM campaign");
        sb3.append(sb2);
        sb3.append(" LIMIT 20");
        String sb4 = sb3.toString();
        try {
            synchronized (new Object()) {
                try {
                    cursor = a().rawQuery(sb4, null);
                    try {
                    } catch (Throwable th) {
                        th = th;
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    cursor = null;
                    throw th;
                }
            }
        } catch (Exception unused) {
            cursor = null;
        } catch (Throwable th3) {
            Throwable th4 = th3;
            cursor = null;
            if (cursor != null) {
                cursor.close();
            }
            throw th4;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0037, code lost:
        if (r10.getCount() <= 0) goto L_0x0529;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0039, code lost:
        r1 = new java.util.ArrayList();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0042, code lost:
        if (r10.moveToNext() == false) goto L_0x0523;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0044, code lost:
        r2 = new com.mintegral.msdk.base.entity.CampaignEx();
        r2.setId(r10.getString(r10.getColumnIndex("id")));
        r2.setTab(r10.getInt(r10.getColumnIndex("tab")));
        r2.setPackageName(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_PACKAGE_NAME)));
        r2.setAppName(r10.getString(r10.getColumnIndex(com.facebook.internal.NativeProtocol.BRIDGE_ARG_APP_NAME_STRING)));
        r2.setAppDesc(r10.getString(r10.getColumnIndex("app_desc")));
        r2.setSize(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_APP_SIZE)));
        r2.setImageSize(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_IMAGE_SIZE)));
        r2.setIconUrl(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_ICON_URL)));
        r2.setImageUrl(r10.getString(r10.getColumnIndex("image_url")));
        r2.setImpressionURL(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_IMPRESSION_URL)));
        r2.setNoticeUrl(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_NOTICE_URL)));
        r2.setClickURL(r10.getString(r10.getColumnIndex("download_url")));
        r2.setDeepLinkUrl(r10.getString(r10.getColumnIndex("deeplink_url")));
        r2.setOnlyImpressionURL(r10.getString(r10.getColumnIndex("only_impression")));
        r4 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x010b, code lost:
        if (r10.getInt(r10.getColumnIndex("preclick")) != 1) goto L_0x010f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x010d, code lost:
        r3 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x010f, code lost:
        r3 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0110, code lost:
        r2.setPreClick(r3);
        r2.setTemplate(r10.getInt(r10.getColumnIndex("template")));
        r2.setLandingType(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_LANDING_TYPE)));
        r2.setLinkType(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_LINK_TYPE)));
        r2.setClick_mode(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_CLICK_MODE)));
        r2.setRating(java.lang.Double.parseDouble(r10.getString(r10.getColumnIndex("star"))));
        r2.setClickInterval(r10.getInt(r10.getColumnIndex("cti")));
        r2.setPreClickInterval(r10.getInt(r10.getColumnIndex("cpti")));
        r2.setTimestamp(r10.getLong(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_ST_TS)));
        r2.setCacheLevel(r10.getInt(r10.getColumnIndex("level")));
        r2.setAdCall(r10.getString(r10.getColumnIndex("ad_call")));
        r2.setFca(r10.getInt(r10.getColumnIndex("fc_a")));
        r2.setFcb(r10.getInt(r10.getColumnIndex("fc_b")));
        r2.setAd_url_list(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_AD_URL_LIST)));
        r2.setVideoLength(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_VIDEO_LENGTHL)));
        r2.setVideoSize(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_VIDEO_SIZE)));
        r2.setVideoResolution(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_VIDEO_RESOLUTION)));
        r2.setEndcard_click_result(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_ENDCARD_CLICK)));
        r2.setVideoUrlEncode(r10.getString(r10.getColumnIndex("video_url")));
        r2.setWatchMile(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_WATCH_MILE)));
        r2.setTImp(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_T_IMP)));
        r2.setBty(r10.getInt(r10.getColumnIndex("bty")));
        r2.setAdvImp(r10.getString(r10.getColumnIndex("advImp")));
        r2.setGuidelines(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_GUIDELINES)));
        r2.setOfferType(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_OFFER_TYPE)));
        r2.setHtmlUrl(r10.getString(r10.getColumnIndex("html_url")));
        r2.setGuidelines(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_GUIDELINES)));
        r2.setHtmlUrl(r10.getString(r10.getColumnIndex("html_url")));
        r2.setEndScreenUrl(r10.getString(r10.getColumnIndex("end_screen_url")));
        r2.setRewardName(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_REWARD_NAME)));
        r2.setRewardAmount(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_REWARD_AMOUNT)));
        r2.setRewardPlayStatus(r10.getInt(r10.getColumnIndex("reward_play_status")));
        r2.setAdvId(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_ADV_ID)));
        r2.setTtc_ct2(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_TTC_CT2)));
        r2.setTtc_type(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_TTC_TYPE)));
        r2.setRetarget_offer(r10.getInt(r10.getColumnIndex("retarget")));
        r2.setCampaignUnitId(r10.getString(r10.getColumnIndex("unitid")));
        r2.setNativeVideoTracking(com.mintegral.msdk.base.entity.CampaignEx.TrackingStr2Object(r10.getString(r10.getColumnIndex("native_ad_tracking"))));
        r2.setNativeVideoTrackingString(r10.getString(r10.getColumnIndex("native_ad_tracking")));
        r2.setVideo_end_type(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.VIDEO_END_TYPE)));
        r2.setendcard_url(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.ENDCARD_URL)));
        r2.setPlayable_ads_without_video(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.PLAYABLE_ADS_WITHOUT_VIDEO)));
        r2.setLoopbackString(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.LOOPBACK)));
        r2.setLoopbackMap(com.mintegral.msdk.base.entity.CampaignEx.loopbackStrToMap(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.LOOPBACK))));
        r2.setRewardTemplateMode(com.mintegral.msdk.base.entity.CampaignEx.c.a(r10.getString(r10.getColumnIndex("reward_teamplate"))));
        r2.setVideoMD5Value(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_REWARD_VIDEO_MD5)));
        r2.setGifUrl(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_GIF_URL)));
        r2.setNvT2(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_NV_T2)));
        r2.setClickTimeOutInterval(r10.getInt(r10.getColumnIndex("c_coi")));
        r2.setcUA(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_C_UA)));
        r2.setImpUA(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_IMP_UA)));
        r2.setIsDeleted(r10.getInt(r10.getColumnIndex("is_deleted")));
        r2.setIsClick(r10.getInt(r10.getColumnIndex("is_click")));
        r2.setIsAddSuccesful(r10.getInt(r10.getColumnIndex("is_add_sucesful")));
        r2.setKeyIaOri(r10.getInt(r10.getColumnIndex("ia_ori")));
        r2.setAdType(r10.getInt(r10.getColumnIndex("ad_type")));
        r2.setIa_ext1(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.KEY_IA_EXT1)));
        r2.setIa_ext2(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.KEY_IA_EXT2)));
        r2.setKeyIaRst(r10.getInt(r10.getColumnIndex("ia_rst")));
        r2.setKeyIaUrl(r10.getString(r10.getColumnIndex("ia_url")));
        r2.setKeyIaIcon(r10.getString(r10.getColumnIndex("ia_icon")));
        r2.setIsDownLoadZip(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.KEY_IS_DOWNLOAD)));
        r2.setInteractiveCache(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.KEY_IA_CACHE)));
        r2.setGhId(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.KEY_GH_ID)));
        r2.setGhPath(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.KEY_GH_PATH)));
        r2.setBindId(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.KEY_BIND_ID)));
        r2.setOc_time(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.KEY_OC_TIME)));
        r2.setOc_type(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.KEY_OC_TYPE)));
        r2.setT_list(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.KEY_T_LIST)));
        r3 = r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.KET_ADCHOICE));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x04a5, code lost:
        if (android.text.TextUtils.isEmpty(r3) != false) goto L_0x04ae;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x04a7, code lost:
        r2.setAdchoice(com.mintegral.msdk.base.entity.CampaignEx.a.a(r3));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x04ae, code lost:
        r2.setAdchoiceSizeHeight(r10.getInt(r10.getColumnIndex("adchoice_size_height")));
        r2.setAdchoiceSizeWidth(r10.getInt(r10.getColumnIndex("adchoice_size_width")));
        r2.setPlct(r10.getLong(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_PLCT)));
        r2.setPlctb(r10.getLong(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_PLCTB)));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x04ec, code lost:
        if (r10.getInt(r10.getColumnIndex("is_bid_campaign")) != 1) goto L_0x04f0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x04ee, code lost:
        r3 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x04f0, code lost:
        r3 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x04f1, code lost:
        r2.setIsBidCampaign(r3);
        r2.setBidToken(r10.getString(r10.getColumnIndex(com.vungle.warren.model.AdvertisementDBAdapter.AdvertisementColumns.COLUMN_BID_TOKEN)));
        r2.setMraid(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_MRAID)));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0518, code lost:
        if (r10.getInt(r10.getColumnIndex("is_mraid_campaign")) != 1) goto L_0x051b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x051a, code lost:
        r4 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x051b, code lost:
        r2.setIsMraid(r4);
        r1.add(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0523, code lost:
        if (r10 == null) goto L_0x0528;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0525, code lost:
        r10.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0528, code lost:
        return r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0529, code lost:
        if (r10 == null) goto L_0x0544;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x0530, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x053f, code lost:
        if (r10 == null) goto L_0x0544;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x0541, code lost:
        r10.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x0544, code lost:
        return null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0031, code lost:
        if (r10 == null) goto L_0x0529;
     */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x053a  */
    public final List<CampaignEx> g(String str) {
        Cursor cursor;
        StringBuilder sb = new StringBuilder(" WHERE unitid = '");
        sb.append(str);
        sb.append("' AND is_deleted=0");
        String sb2 = sb.toString();
        StringBuilder sb3 = new StringBuilder("SELECT * FROM campaign");
        sb3.append(sb2);
        String sb4 = sb3.toString();
        try {
            synchronized (new Object()) {
                try {
                    cursor = a().rawQuery(sb4, null);
                    try {
                    } catch (Throwable th) {
                        th = th;
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    cursor = null;
                    throw th;
                }
            }
        } catch (Exception unused) {
            cursor = null;
        } catch (Throwable th3) {
            Throwable th4 = th3;
            cursor = null;
            if (cursor != null) {
                cursor.close();
            }
            throw th4;
        }
    }

    public final List<CampaignEx> h(String str) {
        StringBuilder sb = new StringBuilder(" WHERE unitid = '");
        sb.append(str);
        sb.append("'");
        String sb2 = sb.toString();
        StringBuilder sb3 = new StringBuilder("SELECT * FROM campaign");
        sb3.append(sb2);
        return m(sb3.toString());
    }

    public final List<CampaignEx> a(JSONArray jSONArray, String str) {
        if (jSONArray == null) {
            return null;
        }
        try {
            StringBuffer stringBuffer = new StringBuffer(" WHERE unitid = '");
            stringBuffer.append(str);
            stringBuffer.append("' AND (");
            for (int i = 0; i < jSONArray.length(); i++) {
                Long valueOf = Long.valueOf(jSONArray.getLong(i));
                if (i != 0) {
                    stringBuffer.append(" or ");
                }
                stringBuffer.append("id = '");
                stringBuffer.append(valueOf);
                stringBuffer.append("'");
            }
            stringBuffer.append(" )");
            String str2 = b;
            StringBuilder sb = new StringBuilder("tabWhereSb : ");
            sb.append(stringBuffer);
            g.a(str2, sb.toString());
            StringBuilder sb2 = new StringBuilder("SELECT * FROM campaign");
            sb2.append(stringBuffer);
            sb2.append(" AND ia_cache = 'onelevel'");
            return m(sb2.toString());
        } catch (Throwable th) {
            th.printStackTrace();
            return null;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0016, code lost:
        if (r10.getCount() <= 0) goto L_0x0508;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0018, code lost:
        r1 = new java.util.ArrayList();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0021, code lost:
        if (r10.moveToNext() == false) goto L_0x0502;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0023, code lost:
        r2 = new com.mintegral.msdk.base.entity.CampaignEx();
        r2.setId(r10.getString(r10.getColumnIndex("id")));
        r2.setTab(r10.getInt(r10.getColumnIndex("tab")));
        r2.setPackageName(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_PACKAGE_NAME)));
        r2.setAppName(r10.getString(r10.getColumnIndex(com.facebook.internal.NativeProtocol.BRIDGE_ARG_APP_NAME_STRING)));
        r2.setAppDesc(r10.getString(r10.getColumnIndex("app_desc")));
        r2.setSize(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_APP_SIZE)));
        r2.setImageSize(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_IMAGE_SIZE)));
        r2.setIconUrl(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_ICON_URL)));
        r2.setImageUrl(r10.getString(r10.getColumnIndex("image_url")));
        r2.setImpressionURL(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_IMPRESSION_URL)));
        r2.setNoticeUrl(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_NOTICE_URL)));
        r2.setClickURL(r10.getString(r10.getColumnIndex("download_url")));
        r2.setDeepLinkUrl(r10.getString(r10.getColumnIndex("deeplink_url")));
        r2.setOnlyImpressionURL(r10.getString(r10.getColumnIndex("only_impression")));
        r4 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x00ea, code lost:
        if (r10.getInt(r10.getColumnIndex("preclick")) != 1) goto L_0x00ee;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x00ec, code lost:
        r3 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x00ee, code lost:
        r3 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x00ef, code lost:
        r2.setPreClick(r3);
        r2.setTemplate(r10.getInt(r10.getColumnIndex("template")));
        r2.setLandingType(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_LANDING_TYPE)));
        r2.setLinkType(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_LINK_TYPE)));
        r2.setClick_mode(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_CLICK_MODE)));
        r2.setRating(java.lang.Double.parseDouble(r10.getString(r10.getColumnIndex("star"))));
        r2.setClickInterval(r10.getInt(r10.getColumnIndex("cti")));
        r2.setPreClickInterval(r10.getInt(r10.getColumnIndex("cpti")));
        r2.setTimestamp(r10.getLong(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_ST_TS)));
        r2.setCacheLevel(r10.getInt(r10.getColumnIndex("level")));
        r2.setAdCall(r10.getString(r10.getColumnIndex("ad_call")));
        r2.setFca(r10.getInt(r10.getColumnIndex("fc_a")));
        r2.setFcb(r10.getInt(r10.getColumnIndex("fc_b")));
        r2.setAd_url_list(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_AD_URL_LIST)));
        r2.setVideoLength(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_VIDEO_LENGTHL)));
        r2.setVideoSize(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_VIDEO_SIZE)));
        r2.setVideoResolution(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_VIDEO_RESOLUTION)));
        r2.setEndcard_click_result(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_ENDCARD_CLICK)));
        r2.setVideoUrlEncode(r10.getString(r10.getColumnIndex("video_url")));
        r2.setWatchMile(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_WATCH_MILE)));
        r2.setTImp(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_T_IMP)));
        r2.setBty(r10.getInt(r10.getColumnIndex("bty")));
        r2.setAdvImp(r10.getString(r10.getColumnIndex("advImp")));
        r2.setGuidelines(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_GUIDELINES)));
        r2.setOfferType(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_OFFER_TYPE)));
        r2.setHtmlUrl(r10.getString(r10.getColumnIndex("html_url")));
        r2.setGuidelines(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_GUIDELINES)));
        r2.setHtmlUrl(r10.getString(r10.getColumnIndex("html_url")));
        r2.setEndScreenUrl(r10.getString(r10.getColumnIndex("end_screen_url")));
        r2.setRewardName(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_REWARD_NAME)));
        r2.setRewardAmount(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_REWARD_AMOUNT)));
        r2.setRewardPlayStatus(r10.getInt(r10.getColumnIndex("reward_play_status")));
        r2.setAdvId(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_ADV_ID)));
        r2.setTtc_ct2(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_TTC_CT2)));
        r2.setTtc_type(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_TTC_TYPE)));
        r2.setRetarget_offer(r10.getInt(r10.getColumnIndex("retarget")));
        r2.setCampaignUnitId(r10.getString(r10.getColumnIndex("unitid")));
        r2.setNativeVideoTracking(com.mintegral.msdk.base.entity.CampaignEx.TrackingStr2Object(r10.getString(r10.getColumnIndex("native_ad_tracking"))));
        r2.setNativeVideoTrackingString(r10.getString(r10.getColumnIndex("native_ad_tracking")));
        r2.setVideo_end_type(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.VIDEO_END_TYPE)));
        r2.setendcard_url(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.ENDCARD_URL)));
        r2.setPlayable_ads_without_video(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.PLAYABLE_ADS_WITHOUT_VIDEO)));
        r2.setLoopbackString(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.LOOPBACK)));
        r2.setLoopbackMap(com.mintegral.msdk.base.entity.CampaignEx.loopbackStrToMap(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.LOOPBACK))));
        r2.setRewardTemplateMode(com.mintegral.msdk.base.entity.CampaignEx.c.a(r10.getString(r10.getColumnIndex("reward_teamplate"))));
        r2.setVideoMD5Value(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_REWARD_VIDEO_MD5)));
        r2.setGifUrl(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_GIF_URL)));
        r2.setNvT2(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_NV_T2)));
        r2.setClickTimeOutInterval(r10.getInt(r10.getColumnIndex("c_coi")));
        r2.setcUA(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_C_UA)));
        r2.setImpUA(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_IMP_UA)));
        r2.setIsDeleted(r10.getInt(r10.getColumnIndex("is_deleted")));
        r2.setIsClick(r10.getInt(r10.getColumnIndex("is_click")));
        r2.setIsAddSuccesful(r10.getInt(r10.getColumnIndex("is_add_sucesful")));
        r2.setKeyIaOri(r10.getInt(r10.getColumnIndex("ia_ori")));
        r2.setAdType(r10.getInt(r10.getColumnIndex("ad_type")));
        r2.setIa_ext1(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.KEY_IA_EXT1)));
        r2.setIa_ext2(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.KEY_IA_EXT2)));
        r2.setKeyIaRst(r10.getInt(r10.getColumnIndex("ia_rst")));
        r2.setKeyIaUrl(r10.getString(r10.getColumnIndex("ia_url")));
        r2.setKeyIaIcon(r10.getString(r10.getColumnIndex("ia_icon")));
        r2.setIsDownLoadZip(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.KEY_IS_DOWNLOAD)));
        r2.setInteractiveCache(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.KEY_IA_CACHE)));
        r2.setGhId(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.KEY_GH_ID)));
        r2.setGhPath(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.KEY_GH_PATH)));
        r2.setBindId(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.KEY_BIND_ID)));
        r2.setOc_time(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.KEY_OC_TIME)));
        r2.setOc_type(r10.getInt(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.KEY_OC_TYPE)));
        r2.setT_list(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.KEY_T_LIST)));
        r3 = r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.KET_ADCHOICE));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0484, code lost:
        if (android.text.TextUtils.isEmpty(r3) != false) goto L_0x048d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0486, code lost:
        r2.setAdchoice(com.mintegral.msdk.base.entity.CampaignEx.a.a(r3));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x048d, code lost:
        r2.setAdchoiceSizeHeight(r10.getInt(r10.getColumnIndex("adchoice_size_height")));
        r2.setAdchoiceSizeWidth(r10.getInt(r10.getColumnIndex("adchoice_size_width")));
        r2.setPlct(r10.getLong(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_PLCT)));
        r2.setPlctb(r10.getLong(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_PLCTB)));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x04cb, code lost:
        if (r10.getInt(r10.getColumnIndex("is_bid_campaign")) != 1) goto L_0x04cf;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x04cd, code lost:
        r3 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x04cf, code lost:
        r3 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x04d0, code lost:
        r2.setIsBidCampaign(r3);
        r2.setBidToken(r10.getString(r10.getColumnIndex(com.vungle.warren.model.AdvertisementDBAdapter.AdvertisementColumns.COLUMN_BID_TOKEN)));
        r2.setMraid(r10.getString(r10.getColumnIndex(com.mintegral.msdk.base.entity.CampaignEx.JSON_KEY_MRAID)));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x04f7, code lost:
        if (r10.getInt(r10.getColumnIndex("is_mraid_campaign")) != 1) goto L_0x04fa;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x04f9, code lost:
        r4 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x04fa, code lost:
        r2.setIsMraid(r4);
        r1.add(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0502, code lost:
        if (r10 == null) goto L_0x0507;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0504, code lost:
        r10.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0507, code lost:
        return r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0508, code lost:
        if (r10 == null) goto L_0x0523;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x050f, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x051e, code lost:
        if (r10 == null) goto L_0x0523;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x0520, code lost:
        r10.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x0523, code lost:
        return null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0010, code lost:
        if (r10 == null) goto L_0x0508;
     */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x0519  */
    private List<CampaignEx> m(String str) {
        Cursor cursor;
        try {
            synchronized (new Object()) {
                try {
                    cursor = a().rawQuery(str, null);
                    try {
                    } catch (Throwable th) {
                        th = th;
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    cursor = null;
                    throw th;
                }
            }
        } catch (Exception unused) {
            cursor = null;
        } catch (Throwable th3) {
            Throwable th4 = th3;
            cursor = null;
            if (cursor != null) {
                cursor.close();
            }
            throw th4;
        }
    }

    /* JADX WARNING: type inference failed for: r0v0 */
    /* JADX WARNING: type inference failed for: r0v1, types: [android.database.Cursor] */
    /* JADX WARNING: type inference failed for: r1v0 */
    /* JADX WARNING: type inference failed for: r0v2, types: [android.database.Cursor] */
    /* JADX WARNING: type inference failed for: r0v3, types: [java.util.List<java.lang.String>] */
    /* JADX WARNING: type inference failed for: r1v1 */
    /* JADX WARNING: type inference failed for: r5v7, types: [android.database.Cursor] */
    /* JADX WARNING: type inference failed for: r0v4, types: [java.util.List<java.lang.String>] */
    /* JADX WARNING: type inference failed for: r0v5 */
    /* JADX WARNING: type inference failed for: r1v5 */
    /* JADX WARNING: type inference failed for: r0v7 */
    /* JADX WARNING: type inference failed for: r1v7, types: [java.util.List, java.util.ArrayList] */
    /* JADX WARNING: type inference failed for: r0v9 */
    /* JADX WARNING: type inference failed for: r0v11 */
    /* JADX WARNING: type inference failed for: r0v15 */
    /* JADX WARNING: type inference failed for: r0v16 */
    /* JADX WARNING: type inference failed for: r1v8 */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x003f, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0040, code lost:
        r3 = r0;
        r0 = r5;
        r5 = r3;
        r1 = r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0044, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0045, code lost:
        r3 = r0;
        r0 = r5;
        r5 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x005e, code lost:
        r0.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0065, code lost:
        r0.close();
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Multi-variable type inference failed. Error: jadx.core.utils.exceptions.JadxRuntimeException: No candidate types for var: r0v11
  assigns: [?[OBJECT, ARRAY], ?[int, float, boolean, short, byte, char, OBJECT, ARRAY]]
  uses: [java.util.List<java.lang.String>, ?[int, boolean, OBJECT, ARRAY, byte, short, char], android.database.Cursor]
  mth insns count: 51
    	at jadx.core.dex.visitors.typeinference.TypeSearch.fillTypeCandidates(TypeSearch.java:237)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.typeinference.TypeSearch.run(TypeSearch.java:53)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.runMultiVariableSearch(TypeInferenceVisitor.java:99)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.visit(TypeInferenceVisitor.java:92)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:27)
    	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$1(DepthTraversal.java:14)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
    	at jadx.core.ProcessClass.process(ProcessClass.java:30)
    	at jadx.core.ProcessClass.lambda$processDependencies$0(ProcessClass.java:49)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:49)
    	at jadx.core.ProcessClass.process(ProcessClass.java:35)
    	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
    	at jadx.api.JavaClass.decompile(JavaClass.java:62)
    	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
     */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0044 A[ExcHandler: all (r0v6 'th' java.lang.Throwable A[CUSTOM_DECLARE]), Splitter:B:4:0x001e] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x005e  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0065  */
    /* JADX WARNING: Unknown variable types count: 7 */
    public final List<String> i(String str) {
        ? r1;
        ? r0;
        ? r02 = 0;
        try {
            StringBuilder sb = new StringBuilder("select id from campaign where unitid='");
            sb.append(str);
            sb.append("' and reward_play_status=1");
            ? rawQuery = a().rawQuery(sb.toString(), null);
            if (rawQuery != 0) {
                try {
                    if (rawQuery.getCount() > 0) {
                        ? arrayList = new ArrayList();
                        while (rawQuery.moveToNext()) {
                            arrayList.add(rawQuery.getString(rawQuery.getColumnIndex("id")));
                        }
                        r02 = arrayList;
                    }
                } catch (Exception e) {
                    r0 = rawQuery;
                    e = e;
                    r1 = 0;
                    try {
                        e.printStackTrace();
                        if (r0 != 0) {
                        }
                        return r1;
                    } catch (Throwable th) {
                        th = th;
                        ? r03 = r0;
                        if (r03 != 0) {
                        }
                        throw th;
                    }
                } catch (Throwable th2) {
                }
            }
            if (rawQuery == 0) {
                return r02;
            }
            rawQuery.close();
            return r02;
        } catch (Exception e2) {
            e = e2;
            r1 = 0;
            r0 = r02;
            e.printStackTrace();
            if (r0 != 0) {
            }
            return r1;
        }
    }

    private Cursor n(String str) {
        try {
            StringBuilder sb = new StringBuilder("SELECT * FROM campaign where unitid ='");
            sb.append(str);
            sb.append("'");
            return a().rawQuery(sb.toString(), null);
        } catch (Throwable th) {
            th.printStackTrace();
            return null;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0028, code lost:
        if (r4 != null) goto L_0x002a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x002a, code lost:
        r4.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0038, code lost:
        if (r4 != null) goto L_0x002a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x003b, code lost:
        return r0;
     */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x003f  */
    public final List<String> j(String str) {
        Cursor cursor;
        Exception e;
        ArrayList arrayList = new ArrayList();
        try {
            cursor = n(str);
            if (cursor != null) {
                try {
                    if (cursor.getCount() > 0) {
                        while (cursor.moveToNext()) {
                            arrayList.add(cursor.getString(cursor.getColumnIndex("video_url")));
                        }
                    }
                } catch (Exception e2) {
                    e = e2;
                    try {
                        e.printStackTrace();
                    } catch (Throwable th) {
                        th = th;
                        if (cursor != null) {
                        }
                        throw th;
                    }
                }
            }
        } catch (Exception e3) {
            e = e3;
            cursor = null;
            e.printStackTrace();
        } catch (Throwable th2) {
            th = th2;
            cursor = null;
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0033, code lost:
        if (r4 != null) goto L_0x0035;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0035, code lost:
        r4.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0043, code lost:
        if (r4 != null) goto L_0x0035;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0046, code lost:
        return r0;
     */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x004a  */
    public final List<String> k(String str) {
        Cursor cursor;
        Exception e;
        ArrayList arrayList = new ArrayList();
        try {
            cursor = n(str);
            if (cursor != null) {
                try {
                    if (cursor.getCount() > 0) {
                        while (cursor.moveToNext()) {
                            arrayList.add((cursor == null || cursor.getCount() <= 0) ? "" : cursor.getString(cursor.getColumnIndex("id")));
                        }
                    }
                } catch (Exception e2) {
                    e = e2;
                    try {
                        e.printStackTrace();
                    } catch (Throwable th) {
                        th = th;
                        if (cursor != null) {
                            cursor.close();
                        }
                        throw th;
                    }
                }
            }
        } catch (Exception e3) {
            e = e3;
            cursor = null;
            e.printStackTrace();
        } catch (Throwable th2) {
            th = th2;
            cursor = null;
            if (cursor != null) {
            }
            throw th;
        }
    }

    private static CampaignEx a(Cursor cursor) {
        if (cursor == null || cursor.getCount() <= 0) {
            return null;
        }
        CampaignEx campaignEx = new CampaignEx();
        campaignEx.setId(cursor.getString(cursor.getColumnIndex("id")));
        campaignEx.setTab(cursor.getInt(cursor.getColumnIndex("tab")));
        campaignEx.setPackageName(cursor.getString(cursor.getColumnIndex(CampaignEx.JSON_KEY_PACKAGE_NAME)));
        campaignEx.setAppName(cursor.getString(cursor.getColumnIndex(NativeProtocol.BRIDGE_ARG_APP_NAME_STRING)));
        campaignEx.setAppDesc(cursor.getString(cursor.getColumnIndex("app_desc")));
        campaignEx.setSize(cursor.getString(cursor.getColumnIndex(CampaignEx.JSON_KEY_APP_SIZE)));
        campaignEx.setImageSize(cursor.getString(cursor.getColumnIndex(CampaignEx.JSON_KEY_IMAGE_SIZE)));
        campaignEx.setIconUrl(cursor.getString(cursor.getColumnIndex(CampaignEx.JSON_KEY_ICON_URL)));
        campaignEx.setImageUrl(cursor.getString(cursor.getColumnIndex("image_url")));
        campaignEx.setImpressionURL(cursor.getString(cursor.getColumnIndex(CampaignEx.JSON_KEY_IMPRESSION_URL)));
        campaignEx.setNoticeUrl(cursor.getString(cursor.getColumnIndex(CampaignEx.JSON_KEY_NOTICE_URL)));
        campaignEx.setClickURL(cursor.getString(cursor.getColumnIndex("download_url")));
        campaignEx.setDeepLinkUrl(cursor.getString(cursor.getColumnIndex("deeplink_url")));
        campaignEx.setOnlyImpressionURL(cursor.getString(cursor.getColumnIndex("only_impression")));
        boolean z = false;
        campaignEx.setPreClick(cursor.getInt(cursor.getColumnIndex("preclick")) == 1);
        campaignEx.setTemplate(cursor.getInt(cursor.getColumnIndex("template")));
        campaignEx.setLandingType(cursor.getString(cursor.getColumnIndex(CampaignEx.JSON_KEY_LANDING_TYPE)));
        campaignEx.setLinkType(cursor.getInt(cursor.getColumnIndex(CampaignEx.JSON_KEY_LINK_TYPE)));
        campaignEx.setClick_mode(cursor.getString(cursor.getColumnIndex(CampaignEx.JSON_KEY_CLICK_MODE)));
        campaignEx.setRating(Double.parseDouble(cursor.getString(cursor.getColumnIndex("star"))));
        campaignEx.setClickInterval(cursor.getInt(cursor.getColumnIndex("cti")));
        campaignEx.setPreClickInterval(cursor.getInt(cursor.getColumnIndex("cpti")));
        campaignEx.setTimestamp(cursor.getLong(cursor.getColumnIndex(CampaignEx.JSON_KEY_ST_TS)));
        campaignEx.setCacheLevel(cursor.getInt(cursor.getColumnIndex("level")));
        campaignEx.setAdCall(cursor.getString(cursor.getColumnIndex("ad_call")));
        campaignEx.setFcb(cursor.getInt(cursor.getColumnIndex("fc_b")));
        campaignEx.setAd_url_list(cursor.getString(cursor.getColumnIndex(CampaignEx.JSON_KEY_AD_URL_LIST)));
        campaignEx.setVideoLength(cursor.getInt(cursor.getColumnIndex(CampaignEx.JSON_KEY_VIDEO_LENGTHL)));
        campaignEx.setVideoSize(cursor.getInt(cursor.getColumnIndex(CampaignEx.JSON_KEY_VIDEO_SIZE)));
        campaignEx.setVideoResolution(cursor.getString(cursor.getColumnIndex(CampaignEx.JSON_KEY_VIDEO_RESOLUTION)));
        campaignEx.setEndcard_click_result(cursor.getInt(cursor.getColumnIndex(CampaignEx.JSON_KEY_ENDCARD_CLICK)));
        campaignEx.setVideoUrlEncode(cursor.getString(cursor.getColumnIndex("video_url")));
        campaignEx.setWatchMile(cursor.getInt(cursor.getColumnIndex(CampaignEx.JSON_KEY_WATCH_MILE)));
        campaignEx.setTImp(cursor.getInt(cursor.getColumnIndex(CampaignEx.JSON_KEY_T_IMP)));
        campaignEx.setBty(cursor.getInt(cursor.getColumnIndex("bty")));
        campaignEx.setAdvImp(cursor.getString(cursor.getColumnIndex("advImp")));
        campaignEx.setGuidelines(cursor.getString(cursor.getColumnIndex(CampaignEx.JSON_KEY_GUIDELINES)));
        campaignEx.setOfferType(cursor.getInt(cursor.getColumnIndex(CampaignEx.JSON_KEY_OFFER_TYPE)));
        campaignEx.setHtmlUrl(cursor.getString(cursor.getColumnIndex("html_url")));
        campaignEx.setGuidelines(cursor.getString(cursor.getColumnIndex(CampaignEx.JSON_KEY_GUIDELINES)));
        campaignEx.setHtmlUrl(cursor.getString(cursor.getColumnIndex("html_url")));
        campaignEx.setEndScreenUrl(cursor.getString(cursor.getColumnIndex("end_screen_url")));
        campaignEx.setRewardName(cursor.getString(cursor.getColumnIndex(CampaignEx.JSON_KEY_REWARD_NAME)));
        campaignEx.setRewardAmount(cursor.getInt(cursor.getColumnIndex(CampaignEx.JSON_KEY_REWARD_AMOUNT)));
        campaignEx.setRewardPlayStatus(cursor.getInt(cursor.getColumnIndex("reward_play_status")));
        campaignEx.setAdvId(cursor.getString(cursor.getColumnIndex(CampaignEx.JSON_KEY_ADV_ID)));
        campaignEx.setTtc_ct2(cursor.getInt(cursor.getColumnIndex(CampaignEx.JSON_KEY_TTC_CT2)));
        campaignEx.setTtc_type(cursor.getInt(cursor.getColumnIndex(CampaignEx.JSON_KEY_TTC_TYPE)));
        campaignEx.setRetarget_offer(cursor.getInt(cursor.getColumnIndex("retarget")));
        campaignEx.setCampaignUnitId(cursor.getString(cursor.getColumnIndex("unitid")));
        campaignEx.setNativeVideoTracking(CampaignEx.TrackingStr2Object(cursor.getString(cursor.getColumnIndex("native_ad_tracking"))));
        campaignEx.setNativeVideoTrackingString(cursor.getString(cursor.getColumnIndex("native_ad_tracking")));
        campaignEx.setVideo_end_type(cursor.getInt(cursor.getColumnIndex(CampaignEx.VIDEO_END_TYPE)));
        campaignEx.setendcard_url(cursor.getString(cursor.getColumnIndex(CampaignEx.ENDCARD_URL)));
        campaignEx.setPlayable_ads_without_video(cursor.getInt(cursor.getColumnIndex(CampaignEx.PLAYABLE_ADS_WITHOUT_VIDEO)));
        campaignEx.setLoopbackString(cursor.getString(cursor.getColumnIndex(CampaignEx.LOOPBACK)));
        campaignEx.setLoopbackMap(CampaignEx.loopbackStrToMap(cursor.getString(cursor.getColumnIndex(CampaignEx.LOOPBACK))));
        campaignEx.setRewardTemplateMode(c.a(cursor.getString(cursor.getColumnIndex("reward_teamplate"))));
        campaignEx.setVideoMD5Value(cursor.getString(cursor.getColumnIndex(CampaignEx.JSON_KEY_REWARD_VIDEO_MD5)));
        campaignEx.setGifUrl(cursor.getString(cursor.getColumnIndex(CampaignEx.JSON_KEY_GIF_URL)));
        campaignEx.setNvT2(cursor.getInt(cursor.getColumnIndex(CampaignEx.JSON_KEY_NV_T2)));
        campaignEx.setClickTimeOutInterval(cursor.getInt(cursor.getColumnIndex("c_coi")));
        campaignEx.setcUA(cursor.getInt(cursor.getColumnIndex(CampaignEx.JSON_KEY_C_UA)));
        campaignEx.setImpUA(cursor.getInt(cursor.getColumnIndex(CampaignEx.JSON_KEY_IMP_UA)));
        campaignEx.setJmPd(cursor.getInt(cursor.getColumnIndex(CampaignEx.JSON_KEY_JM_PD)));
        campaignEx.setIsDeleted(cursor.getInt(cursor.getColumnIndex("is_deleted")));
        campaignEx.setIsClick(cursor.getInt(cursor.getColumnIndex("is_click")));
        campaignEx.setIsAddSuccesful(cursor.getInt(cursor.getColumnIndex("is_add_sucesful")));
        campaignEx.setIsDownLoadZip(cursor.getInt(cursor.getColumnIndex(CampaignEx.KEY_IS_DOWNLOAD)));
        campaignEx.setInteractiveCache(cursor.getString(cursor.getColumnIndex(CampaignEx.KEY_IA_CACHE)));
        campaignEx.setKeyIaOri(cursor.getInt(cursor.getColumnIndex("ia_ori")));
        campaignEx.setAdType(cursor.getInt(cursor.getColumnIndex("ad_type")));
        campaignEx.setIa_ext1(cursor.getString(cursor.getColumnIndex(CampaignEx.KEY_IA_EXT1)));
        campaignEx.setIa_ext2(cursor.getString(cursor.getColumnIndex(CampaignEx.KEY_IA_EXT2)));
        campaignEx.setKeyIaRst(cursor.getInt(cursor.getColumnIndex("ia_rst")));
        campaignEx.setKeyIaUrl(cursor.getString(cursor.getColumnIndex("ia_url")));
        campaignEx.setKeyIaIcon(cursor.getString(cursor.getColumnIndex("ia_icon")));
        campaignEx.setGhId(cursor.getString(cursor.getColumnIndex(CampaignEx.KEY_GH_ID)));
        campaignEx.setGhPath(cursor.getString(cursor.getColumnIndex(CampaignEx.KEY_GH_PATH)));
        campaignEx.setBindId(cursor.getString(cursor.getColumnIndex(CampaignEx.KEY_BIND_ID)));
        campaignEx.setOc_time(cursor.getInt(cursor.getColumnIndex(CampaignEx.KEY_OC_TIME)));
        campaignEx.setOc_type(cursor.getInt(cursor.getColumnIndex(CampaignEx.KEY_OC_TYPE)));
        campaignEx.setT_list(cursor.getString(cursor.getColumnIndex(CampaignEx.KEY_T_LIST)));
        String string = cursor.getString(cursor.getColumnIndex(CampaignEx.KET_ADCHOICE));
        if (!TextUtils.isEmpty(string)) {
            campaignEx.setAdchoice(a.a(string));
        }
        campaignEx.setAdchoiceSizeHeight(cursor.getInt(cursor.getColumnIndex("adchoice_size_height")));
        campaignEx.setAdchoiceSizeWidth(cursor.getInt(cursor.getColumnIndex("adchoice_size_width")));
        campaignEx.setPlct(cursor.getLong(cursor.getColumnIndex(CampaignEx.JSON_KEY_PLCT)));
        campaignEx.setPlctb(cursor.getLong(cursor.getColumnIndex(CampaignEx.JSON_KEY_PLCTB)));
        campaignEx.setIsBidCampaign(cursor.getInt(cursor.getColumnIndex("is_bid_campaign")) == 1);
        campaignEx.setBidToken(cursor.getString(cursor.getColumnIndex(AdvertisementColumns.COLUMN_BID_TOKEN)));
        campaignEx.setMraid(cursor.getString(cursor.getColumnIndex(CampaignEx.JSON_KEY_MRAID)));
        if (cursor.getInt(cursor.getColumnIndex("is_mraid_campaign")) == 1) {
            z = true;
        }
        campaignEx.setIsMraid(z);
        return campaignEx;
    }

    /* JADX WARNING: type inference failed for: r0v0 */
    /* JADX WARNING: type inference failed for: r0v1, types: [android.database.Cursor] */
    /* JADX WARNING: type inference failed for: r0v2, types: [android.database.Cursor] */
    /* JADX WARNING: type inference failed for: r5v5, types: [android.database.Cursor] */
    /* JADX WARNING: type inference failed for: r0v3 */
    /* JADX WARNING: type inference failed for: r5v6, types: [com.mintegral.msdk.base.entity.CampaignEx] */
    /* JADX WARNING: type inference failed for: r0v4 */
    /* JADX WARNING: type inference failed for: r0v5 */
    /* JADX WARNING: type inference failed for: r0v6 */
    /* JADX WARNING: type inference failed for: r6v11, types: [com.mintegral.msdk.base.entity.CampaignEx] */
    /* JADX WARNING: type inference failed for: r0v7 */
    /* JADX WARNING: type inference failed for: r0v8 */
    /* JADX WARNING: type inference failed for: r0v9 */
    /* JADX WARNING: type inference failed for: r0v10 */
    /* JADX WARNING: Multi-variable type inference failed. Error: jadx.core.utils.exceptions.JadxRuntimeException: No candidate types for var: r0v6
  assigns: []
  uses: []
  mth insns count: 44
    	at jadx.core.dex.visitors.typeinference.TypeSearch.fillTypeCandidates(TypeSearch.java:237)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.typeinference.TypeSearch.run(TypeSearch.java:53)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.runMultiVariableSearch(TypeInferenceVisitor.java:99)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.visit(TypeInferenceVisitor.java:92)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:27)
    	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$1(DepthTraversal.java:14)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
    	at jadx.core.ProcessClass.process(ProcessClass.java:30)
    	at jadx.core.ProcessClass.lambda$processDependencies$0(ProcessClass.java:49)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:49)
    	at jadx.core.ProcessClass.process(ProcessClass.java:35)
    	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
    	at jadx.api.JavaClass.decompile(JavaClass.java:62)
    	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
     */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0050  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0056  */
    /* JADX WARNING: Removed duplicated region for block: B:31:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Unknown variable types count: 6 */
    public final CampaignEx c(String str, String str2) {
        ? r0;
        CampaignEx campaignEx;
        ? r02;
        ? r03 = 0;
        try {
            StringBuilder sb = new StringBuilder("SELECT * FROM campaign where unitid ='");
            sb.append(str2);
            sb.append("' and id = '");
            sb.append(str);
            sb.append("'");
            ? rawQuery = a().rawQuery(sb.toString(), null);
            if (rawQuery != 0) {
                try {
                    if (rawQuery.getCount() > 0) {
                        while (rawQuery.moveToNext()) {
                            r03 = a((Cursor) rawQuery);
                        }
                        r03 = r03;
                    }
                } catch (Exception e) {
                    e = e;
                    r02 = rawQuery;
                    campaignEx = null;
                    try {
                        e.printStackTrace();
                        if (r02 != 0) {
                            return campaignEx;
                        }
                        r02.close();
                        return campaignEx;
                    } catch (Throwable th) {
                        th = th;
                        r0 = r02;
                        if (r0 != 0) {
                            r0.close();
                        }
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    r0 = rawQuery;
                    if (r0 != 0) {
                    }
                    throw th;
                }
            }
            if (rawQuery != 0) {
                rawQuery.close();
            }
            return r03;
        } catch (Exception e2) {
            e = e2;
            campaignEx = null;
            r02 = r03;
            e.printStackTrace();
            if (r02 != 0) {
            }
        }
    }

    /* JADX WARNING: type inference failed for: r0v0 */
    /* JADX WARNING: type inference failed for: r0v1, types: [android.database.Cursor] */
    /* JADX WARNING: type inference failed for: r0v2, types: [android.database.Cursor] */
    /* JADX WARNING: type inference failed for: r5v5, types: [android.database.Cursor] */
    /* JADX WARNING: type inference failed for: r0v3 */
    /* JADX WARNING: type inference failed for: r5v6, types: [com.mintegral.msdk.base.entity.CampaignEx] */
    /* JADX WARNING: type inference failed for: r0v4 */
    /* JADX WARNING: type inference failed for: r0v5 */
    /* JADX WARNING: type inference failed for: r0v6 */
    /* JADX WARNING: type inference failed for: r6v11, types: [com.mintegral.msdk.base.entity.CampaignEx] */
    /* JADX WARNING: type inference failed for: r0v7 */
    /* JADX WARNING: type inference failed for: r0v8 */
    /* JADX WARNING: type inference failed for: r0v9 */
    /* JADX WARNING: type inference failed for: r0v10 */
    /* JADX WARNING: Multi-variable type inference failed. Error: jadx.core.utils.exceptions.JadxRuntimeException: No candidate types for var: r0v6
  assigns: []
  uses: []
  mth insns count: 44
    	at jadx.core.dex.visitors.typeinference.TypeSearch.fillTypeCandidates(TypeSearch.java:237)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.typeinference.TypeSearch.run(TypeSearch.java:53)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.runMultiVariableSearch(TypeInferenceVisitor.java:99)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.visit(TypeInferenceVisitor.java:92)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:27)
    	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$1(DepthTraversal.java:14)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
    	at jadx.core.ProcessClass.process(ProcessClass.java:30)
    	at jadx.core.ProcessClass.lambda$processDependencies$0(ProcessClass.java:49)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:49)
    	at jadx.core.ProcessClass.process(ProcessClass.java:35)
    	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
    	at jadx.api.JavaClass.decompile(JavaClass.java:62)
    	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
     */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0050  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0056  */
    /* JADX WARNING: Removed duplicated region for block: B:31:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Unknown variable types count: 6 */
    public final CampaignEx d(String str, String str2) {
        ? r0;
        CampaignEx campaignEx;
        ? r02;
        ? r03 = 0;
        try {
            StringBuilder sb = new StringBuilder("SELECT * FROM campaign where bid_token ='");
            sb.append(str2);
            sb.append("' and unitid ='");
            sb.append(str);
            sb.append("'");
            ? rawQuery = a().rawQuery(sb.toString(), null);
            if (rawQuery != 0) {
                try {
                    if (rawQuery.getCount() > 0) {
                        while (rawQuery.moveToNext()) {
                            r03 = a((Cursor) rawQuery);
                        }
                        r03 = r03;
                    }
                } catch (Exception e) {
                    e = e;
                    r02 = rawQuery;
                    campaignEx = null;
                    try {
                        e.printStackTrace();
                        if (r02 != 0) {
                            return campaignEx;
                        }
                        r02.close();
                        return campaignEx;
                    } catch (Throwable th) {
                        th = th;
                        r0 = r02;
                        if (r0 != 0) {
                            r0.close();
                        }
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    r0 = rawQuery;
                    if (r0 != 0) {
                    }
                    throw th;
                }
            }
            if (rawQuery != 0) {
                rawQuery.close();
            }
            return r03;
        } catch (Exception e2) {
            e = e2;
            campaignEx = null;
            r02 = r03;
            e.printStackTrace();
            if (r02 != 0) {
            }
        }
    }
}
