package com.mintegral.msdk.base.b;

import android.database.sqlite.SQLiteDatabase;

/* compiled from: BaseDao */
public class a<T> {

    /* renamed from: a reason: collision with root package name */
    protected h f2499a = null;

    public a(h hVar) {
        this.f2499a = hVar;
    }

    /* access modifiers changed from: protected */
    public final synchronized SQLiteDatabase a() {
        return this.f2499a.a();
    }

    /* access modifiers changed from: protected */
    public final synchronized SQLiteDatabase b() {
        return this.f2499a.b();
    }
}
