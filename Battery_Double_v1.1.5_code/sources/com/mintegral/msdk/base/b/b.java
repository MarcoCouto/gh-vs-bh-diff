package com.mintegral.msdk.base.b;

import android.content.ContentValues;
import android.database.Cursor;
import com.mintegral.msdk.out.Campaign;

/* compiled from: CDIDtimeDao */
public class b extends a<Campaign> {
    private static final String b = "com.mintegral.msdk.base.b.b";
    private static b c;

    private b(h hVar) {
        super(hVar);
    }

    public static b a(h hVar) {
        if (c == null) {
            synchronized (b.class) {
                if (c == null) {
                    c = new b(hVar);
                }
            }
        }
        return c;
    }

    public final synchronized void c() {
        try {
            long currentTimeMillis = System.currentTimeMillis() - 86400000;
            StringBuilder sb = new StringBuilder("d_time<");
            sb.append(currentTimeMillis);
            String sb2 = sb.toString();
            if (b() != null) {
                b().delete("c_did_time", sb2, null);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public final synchronized long a(String str, long j) {
        try {
            if (b() == null) {
                return -1;
            }
            ContentValues contentValues = new ContentValues();
            contentValues.put("did", str);
            contentValues.put("d_time", Long.valueOf(j));
            if (b(str)) {
                StringBuilder sb = new StringBuilder("did = '");
                sb.append(str);
                sb.append("'");
                return (long) b().update("c_did_time", contentValues, sb.toString(), null);
            }
            return b().insert("c_did_time", null, contentValues);
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    private synchronized boolean b(String str) {
        StringBuilder sb = new StringBuilder("SELECT d_time FROM c_did_time WHERE did='");
        sb.append(str);
        sb.append("'");
        Cursor rawQuery = a().rawQuery(sb.toString(), null);
        if (rawQuery == null || rawQuery.getCount() <= 0) {
            if (rawQuery != null) {
                rawQuery.close();
            }
            return false;
        }
        rawQuery.close();
        return true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x002f, code lost:
        if (r8.getCount() > 0) goto L_0x003b;
     */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0049 A[SYNTHETIC, Splitter:B:21:0x0049] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0050 A[SYNTHETIC, Splitter:B:26:0x0050] */
    public final synchronized boolean a(String str) {
        boolean z;
        z = true;
        Cursor cursor = null;
        try {
            long currentTimeMillis = System.currentTimeMillis() - 86400000;
            StringBuilder sb = new StringBuilder("SELECT * FROM c_did_time where did = '");
            sb.append(str);
            sb.append("' AND d_time > ");
            sb.append(currentTimeMillis);
            Cursor rawQuery = a().rawQuery(sb.toString(), null);
            if (rawQuery != null) {
                try {
                } catch (Exception e) {
                    Exception exc = e;
                    cursor = rawQuery;
                    e = exc;
                    try {
                        e.printStackTrace();
                        if (cursor != null) {
                            cursor.close();
                        }
                        return z;
                    } catch (Throwable th) {
                        th = th;
                        if (cursor != null) {
                            cursor.close();
                        }
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    cursor = rawQuery;
                    if (cursor != null) {
                    }
                    throw th;
                }
            }
            z = false;
            if (rawQuery != null) {
                try {
                    rawQuery.close();
                } catch (Throwable th3) {
                    throw th3;
                }
            }
        } catch (Exception e2) {
            e = e2;
            e.printStackTrace();
            if (cursor != null) {
            }
            return z;
        }
        return z;
    }
}
