package com.mintegral.msdk.base.b;

import android.content.ContentValues;
import android.database.Cursor;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.out.Campaign;

/* compiled from: DailyPlayCapDao */
public class j extends a<Campaign> {
    private static final String b = "com.mintegral.msdk.base.b.j";
    private static j c;

    private j(h hVar) {
        super(hVar);
    }

    public static j a(h hVar) {
        if (c == null) {
            synchronized (j.class) {
                if (c == null) {
                    c = new j(hVar);
                }
            }
        }
        return c;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0025, code lost:
        if (r2.getCount() <= 0) goto L_0x0068;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x002b, code lost:
        if (r2.moveToFirst() == false) goto L_0x0068;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x002d, code lost:
        r3 = r2.getLong(r2.getColumnIndex("first_insert_timestamp"));
        r5 = (long) r2.getInt(r2.getColumnIndex("play_time"));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0046, code lost:
        if (r3 == 0) goto L_0x0068;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0052, code lost:
        if ((java.lang.System.currentTimeMillis() - 86400000) <= r3) goto L_0x0058;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0054, code lost:
        b(r12);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0058, code lost:
        if (r13 <= 0) goto L_0x0068;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x005d, code lost:
        if (r5 < ((long) r13)) goto L_0x0068;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x005f, code lost:
        r1 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0062, code lost:
        r12 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0063, code lost:
        r0 = r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0065, code lost:
        r12 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0066, code lost:
        r0 = r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0068, code lost:
        if (r2 == null) goto L_0x008f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0074, code lost:
        r12 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:?, code lost:
        r13 = b;
        r2 = new java.lang.StringBuilder("isOverCap is error");
        r2.append(r12);
        com.mintegral.msdk.base.utils.g.b(r13, r2.toString());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x008a, code lost:
        if (r0 != null) goto L_0x008c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:?, code lost:
        r0.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x008f, code lost:
        return r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x0090, code lost:
        if (r0 != null) goto L_0x0092;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:?, code lost:
        r0.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x0095, code lost:
        throw r12;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x001f, code lost:
        if (r2 == null) goto L_0x0068;
     */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x0092 A[SYNTHETIC, Splitter:B:47:0x0092] */
    public final boolean a(String str, int i) {
        Cursor cursor = null;
        boolean z = false;
        try {
            StringBuilder sb = new StringBuilder("SELECT * FROM dailyplaycap where unit_id ='");
            sb.append(str);
            sb.append("'");
            String sb2 = sb.toString();
            synchronized (this) {
                try {
                    Cursor rawQuery = a().rawQuery(sb2, null);
                    try {
                    } catch (Throwable th) {
                        th = th;
                        cursor = rawQuery;
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    throw th;
                }
            }
        } catch (Throwable th3) {
            th = th3;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:?, code lost:
        com.mintegral.msdk.base.utils.g.b(b, "resetTimeAndTimestamp error");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x004a, code lost:
        return;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x0042 */
    private synchronized void b(String str) {
        if (b() != null) {
            ContentValues contentValues = new ContentValues();
            contentValues.put("first_insert_timestamp", Integer.valueOf(0));
            contentValues.put("play_time", Integer.valueOf(0));
            StringBuilder sb = new StringBuilder("unit_id = '");
            sb.append(str);
            sb.append("'");
            b().update("dailyplaycap", contentValues, sb.toString(), null);
        }
    }

    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* JADX WARNING: Missing exception handler attribute for start block: B:59:0x00e9 */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0075 A[Catch:{ Exception -> 0x006c, all -> 0x0068 }] */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x00d8 A[SYNTHETIC, Splitter:B:46:0x00d8] */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x00e3 A[SYNTHETIC, Splitter:B:56:0x00e3] */
    public final synchronized void a(String str) {
        long j;
        long j2;
        String str2 = str;
        synchronized (this) {
            Cursor cursor = null;
            try {
                if (b() != null) {
                    ContentValues contentValues = new ContentValues();
                    long currentTimeMillis = System.currentTimeMillis();
                    if (c(str)) {
                        StringBuilder sb = new StringBuilder("SELECT * FROM dailyplaycap where unit_id ='");
                        sb.append(str2);
                        sb.append("'");
                        Cursor rawQuery = a().rawQuery(sb.toString(), null);
                        if (rawQuery != null) {
                            try {
                                if (rawQuery.getCount() > 0) {
                                    rawQuery.moveToFirst();
                                    j2 = rawQuery.getLong(rawQuery.getColumnIndex("first_insert_timestamp"));
                                    j = (long) rawQuery.getInt(rawQuery.getColumnIndex("play_time"));
                                    if (currentTimeMillis - 86400000 > j2) {
                                        b(str);
                                        j = 0;
                                    }
                                    if (j2 == 0) {
                                        contentValues.put("first_insert_timestamp", Long.valueOf(currentTimeMillis));
                                    }
                                    contentValues.put("play_time", Long.valueOf(j + 1));
                                    StringBuilder sb2 = new StringBuilder("unit_id = '");
                                    sb2.append(str2);
                                    sb2.append("'");
                                    b().update("dailyplaycap", contentValues, sb2.toString(), null);
                                    cursor = rawQuery;
                                }
                            } catch (Exception e) {
                                e = e;
                                cursor = rawQuery;
                                try {
                                    e.printStackTrace();
                                    if (cursor != null) {
                                    }
                                    return;
                                } catch (Throwable th) {
                                    th = th;
                                    if (cursor != null) {
                                        cursor.close();
                                    }
                                    throw th;
                                }
                            } catch (Throwable th2) {
                                th = th2;
                                cursor = rawQuery;
                                if (cursor != null) {
                                }
                                throw th;
                            }
                        }
                        j2 = 0;
                        j = 0;
                        if (j2 == 0) {
                        }
                        contentValues.put("play_time", Long.valueOf(j + 1));
                        StringBuilder sb22 = new StringBuilder("unit_id = '");
                        sb22.append(str2);
                        sb22.append("'");
                        b().update("dailyplaycap", contentValues, sb22.toString(), null);
                        cursor = rawQuery;
                    } else {
                        contentValues.put("first_insert_timestamp", Long.valueOf(currentTimeMillis));
                        contentValues.put("play_time", Long.valueOf(1));
                        contentValues.put(MIntegralConstans.PROPERTIES_UNIT_ID, str2);
                        b().insert("dailyplaycap", null, contentValues);
                    }
                    if (cursor != null) {
                        try {
                            cursor.close();
                            return;
                        } catch (Throwable unused) {
                            return;
                        }
                    }
                } else {
                    return;
                }
            } catch (Exception e2) {
                e = e2;
                e.printStackTrace();
                if (cursor != null) {
                    try {
                        cursor.close();
                        return;
                    } catch (Throwable unused2) {
                        return;
                    }
                }
                return;
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0032, code lost:
        return false;
     */
    private synchronized boolean c(String str) {
        try {
            StringBuilder sb = new StringBuilder("SELECT play_time FROM dailyplaycap WHERE unit_id='");
            sb.append(str);
            sb.append("'");
            Cursor rawQuery = a().rawQuery(sb.toString(), null);
            if (rawQuery != null && rawQuery.getCount() > 0) {
                rawQuery.close();
                return true;
            } else if (rawQuery != null) {
                rawQuery.close();
            }
        } catch (Throwable unused) {
            return false;
        }
    }
}
