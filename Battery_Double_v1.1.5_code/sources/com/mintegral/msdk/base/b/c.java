package com.mintegral.msdk.base.b;

import com.mintegral.msdk.out.Campaign;

/* compiled from: CTtimeDao */
public class c extends a<Campaign> {
    private static final String b = "com.mintegral.msdk.base.b.c";
    private static c c;

    private c(h hVar) {
        super(hVar);
    }

    public static c a(h hVar) {
        if (c == null) {
            synchronized (c.class) {
                if (c == null) {
                    c = new c(hVar);
                }
            }
        }
        return c;
    }

    public final synchronized void a(Long l) {
        try {
            long currentTimeMillis = System.currentTimeMillis() - (l.longValue() * 1000);
            StringBuilder sb = new StringBuilder("t_time<");
            sb.append(currentTimeMillis);
            String sb2 = sb.toString();
            if (b() != null) {
                b().delete("c_t_time", sb2, null);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
