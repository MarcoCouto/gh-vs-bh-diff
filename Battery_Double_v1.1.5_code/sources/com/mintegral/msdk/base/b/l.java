package com.mintegral.msdk.base.b;

import android.content.ContentValues;
import android.database.Cursor;
import com.ironsource.sdk.constants.LocationConst;
import com.mintegral.msdk.base.entity.e;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ExcludeInfoDao */
public class l extends a<e> {
    private static l b;

    private l(h hVar) {
        super(hVar);
    }

    public static l a(h hVar) {
        if (b == null) {
            synchronized (l.class) {
                if (b == null) {
                    b = new l(hVar);
                }
            }
        }
        return b;
    }

    public final synchronized void a(e eVar) {
        try {
            if (b() != null) {
                ContentValues contentValues = new ContentValues();
                contentValues.put("id", eVar.a());
                contentValues.put(LocationConst.TIME, Long.valueOf(eVar.d()));
                contentValues.put("unitId", eVar.b());
                contentValues.put("type", Integer.valueOf(eVar.c()));
                if (a(eVar.b(), eVar.a())) {
                    StringBuilder sb = new StringBuilder("id = ");
                    sb.append(eVar.a());
                    sb.append(" AND unitId = ");
                    sb.append(eVar.b());
                    b().update("exclude_info", contentValues, sb.toString(), null);
                    return;
                }
                b().insert("exclude_info", null, contentValues);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x003a, code lost:
        return false;
     */
    private synchronized boolean a(String str, String str2) {
        try {
            StringBuilder sb = new StringBuilder("select id from exclude_info where unitId='");
            sb.append(str);
            sb.append("' and id='");
            sb.append(str2);
            sb.append("'");
            Cursor rawQuery = a().rawQuery(sb.toString(), null);
            if (rawQuery != null && rawQuery.getCount() > 0) {
                rawQuery.close();
                return true;
            } else if (rawQuery != null) {
                rawQuery.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /* JADX WARNING: type inference failed for: r0v0 */
    /* JADX WARNING: type inference failed for: r6v2, types: [android.database.Cursor] */
    /* JADX WARNING: type inference failed for: r0v2, types: [java.util.List<java.lang.String>] */
    /* JADX WARNING: type inference failed for: r1v0 */
    /* JADX WARNING: type inference failed for: r0v3, types: [android.database.Cursor] */
    /* JADX WARNING: type inference failed for: r0v4 */
    /* JADX WARNING: type inference failed for: r4v0 */
    /* JADX WARNING: type inference failed for: r6v5 */
    /* JADX WARNING: type inference failed for: r1v1 */
    /* JADX WARNING: type inference failed for: r6v8, types: [android.database.Cursor] */
    /* JADX WARNING: type inference failed for: r0v6 */
    /* JADX WARNING: type inference failed for: r0v7 */
    /* JADX WARNING: type inference failed for: r1v5 */
    /* JADX WARNING: type inference failed for: r1v7, types: [java.util.List, java.util.ArrayList] */
    /* JADX WARNING: type inference failed for: r0v10 */
    /* JADX WARNING: type inference failed for: r0v12 */
    /* JADX WARNING: type inference failed for: r0v16 */
    /* JADX WARNING: type inference failed for: r6v11 */
    /* JADX WARNING: type inference failed for: r1v8 */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x003d, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x003e, code lost:
        r4 = r0;
        r0 = r6;
        r6 = r4;
        r1 = r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0042, code lost:
        r0 = th;
        r6 = r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:?, code lost:
        r0.close();
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Multi-variable type inference failed. Error: jadx.core.utils.exceptions.JadxRuntimeException: No candidate types for var: r0v12
  assigns: [?[OBJECT, ARRAY], ?[int, float, boolean, short, byte, char, OBJECT, ARRAY]]
  uses: [java.util.List<java.lang.String>, ?[int, boolean, OBJECT, ARRAY, byte, short, char], android.database.Cursor, ?[OBJECT, ARRAY]]
  mth insns count: 57
    	at jadx.core.dex.visitors.typeinference.TypeSearch.fillTypeCandidates(TypeSearch.java:237)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.typeinference.TypeSearch.run(TypeSearch.java:53)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.runMultiVariableSearch(TypeInferenceVisitor.java:99)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.visit(TypeInferenceVisitor.java:92)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:27)
    	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$1(DepthTraversal.java:14)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
    	at jadx.core.ProcessClass.process(ProcessClass.java:30)
    	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
    	at jadx.api.JavaClass.decompile(JavaClass.java:62)
    	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
     */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0042 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:11:0x001c] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x005c A[SYNTHETIC, Splitter:B:35:0x005c] */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x0064 A[SYNTHETIC, Splitter:B:41:0x0064] */
    /* JADX WARNING: Unknown variable types count: 7 */
    public final synchronized List<String> a(String str) {
        ? r1;
        ? r0;
        ? r02 = 0;
        try {
            if (a() == null) {
                return null;
            }
            ? rawQuery = a().rawQuery("select id from exclude_info where unitId=?", new String[]{str});
            if (rawQuery != 0) {
                try {
                    if (rawQuery.getCount() > 0) {
                        ? arrayList = new ArrayList();
                        while (rawQuery.moveToNext()) {
                            arrayList.add(rawQuery.getString(rawQuery.getColumnIndex("id")));
                        }
                        r02 = arrayList;
                    }
                } catch (Exception e) {
                    r0 = rawQuery;
                    e = e;
                    r1 = 0;
                    try {
                        e.printStackTrace();
                        if (r0 != 0) {
                        }
                        r02 = r1;
                        return r02;
                    } catch (Throwable th) {
                        ? r4 = r0;
                        Throwable th2 = th;
                        ? r6 = r4;
                        if (r6 != 0) {
                            r6.close();
                        }
                        throw th2;
                    }
                } catch (Throwable th3) {
                }
            }
            if (rawQuery != 0) {
                try {
                    rawQuery.close();
                } catch (Throwable th4) {
                    throw th4;
                }
            }
        } catch (Exception e2) {
            e = e2;
            r1 = 0;
            r0 = r02;
            e.printStackTrace();
            if (r0 != 0) {
            }
            r02 = r1;
            return r02;
        }
    }

    public final synchronized void b(String str) {
        try {
            long currentTimeMillis = System.currentTimeMillis() - 86400000;
            StringBuilder sb = new StringBuilder("time<");
            sb.append(currentTimeMillis);
            sb.append(" and unitId=?");
            String sb2 = sb.toString();
            String[] strArr = {str};
            if (b() != null) {
                b().delete("exclude_info", sb2, strArr);
            }
        } catch (Exception unused) {
        }
    }
}
