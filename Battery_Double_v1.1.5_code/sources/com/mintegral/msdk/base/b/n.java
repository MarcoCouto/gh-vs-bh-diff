package com.mintegral.msdk.base.b;

import android.content.ContentValues;
import android.database.Cursor;
import com.ironsource.sdk.constants.LocationConst;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.entity.h;
import java.util.ArrayList;
import java.util.List;

/* compiled from: LoadTimeDao */
public class n extends a<h> {
    private static n b;

    private n(h hVar) {
        super(hVar);
    }

    public static n a(h hVar) {
        if (b == null) {
            synchronized (n.class) {
                if (b == null) {
                    b = new n(hVar);
                }
            }
        }
        return b;
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x0032 A[SYNTHETIC, Splitter:B:22:0x0032] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0039 A[SYNTHETIC, Splitter:B:27:0x0039] */
    public final synchronized int c() {
        int i;
        Cursor cursor = null;
        i = 0;
        try {
            Cursor rawQuery = a().rawQuery("select count(*) from load_stat", null);
            if (rawQuery != null) {
                try {
                    if (rawQuery.moveToFirst()) {
                        i = rawQuery.getInt(0);
                    }
                } catch (Exception e) {
                    Cursor cursor2 = rawQuery;
                    e = e;
                    cursor = cursor2;
                    try {
                        e.printStackTrace();
                        if (cursor != null) {
                        }
                        return i;
                    } catch (Throwable th) {
                        th = th;
                        if (cursor != null) {
                        }
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    cursor = rawQuery;
                    if (cursor != null) {
                        cursor.close();
                    }
                    throw th;
                }
            }
            if (rawQuery != null) {
                try {
                    rawQuery.close();
                } catch (Throwable th3) {
                    throw th3;
                }
            }
        } catch (Exception e2) {
            e = e2;
            e.printStackTrace();
            if (cursor != null) {
                cursor.close();
            }
            return i;
        }
        return i;
    }

    /* JADX WARNING: Removed duplicated region for block: B:32:0x00b8 A[SYNTHETIC, Splitter:B:32:0x00b8] */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00c1 A[SYNTHETIC, Splitter:B:39:0x00c1] */
    public final synchronized List<h> d() {
        List<h> list;
        Cursor cursor;
        List<h> list2;
        Exception e;
        list = null;
        try {
            cursor = a().rawQuery("select * from load_stat LIMIT 20", null);
            if (cursor != null) {
                try {
                    if (cursor.getCount() > 0) {
                        list2 = new ArrayList<>();
                        int i = 0;
                        while (cursor.moveToNext() && i < 20) {
                            try {
                                i++;
                                h hVar = new h(cursor.getInt(cursor.getColumnIndex(CampaignEx.JSON_KEY_AD_SOURCE_ID)), cursor.getString(cursor.getColumnIndex(LocationConst.TIME)), cursor.getInt(cursor.getColumnIndex("adNum")), cursor.getString(cursor.getColumnIndex("unitId")), cursor.getInt(cursor.getColumnIndex("fb")), cursor.getInt(cursor.getColumnIndex("timeout")), cursor.getInt(cursor.getColumnIndex("network_type")));
                                list2.add(hVar);
                                int i2 = cursor.getInt(cursor.getColumnIndex("id"));
                                if (b() != null) {
                                    b().delete("load_stat", "id = ?", new String[]{String.valueOf(i2)});
                                }
                            } catch (Exception e2) {
                                e = e2;
                                try {
                                    e.printStackTrace();
                                    if (cursor != null) {
                                    }
                                    list = list2;
                                    return list;
                                } catch (Throwable th) {
                                    th = th;
                                    if (cursor != null) {
                                    }
                                    throw th;
                                }
                            }
                        }
                        list = list2;
                    }
                } catch (Exception e3) {
                    Exception exc = e3;
                    list2 = null;
                    e = exc;
                    e.printStackTrace();
                    if (cursor != null) {
                        cursor.close();
                    }
                    list = list2;
                    return list;
                }
            }
            if (cursor != null) {
                cursor.close();
            }
        } catch (Exception e4) {
            list2 = null;
            e = e4;
            cursor = null;
            e.printStackTrace();
            if (cursor != null) {
            }
            list = list2;
            return list;
        } catch (Throwable th2) {
            th = th2;
            cursor = null;
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
        return list;
    }

    public final synchronized void a(h hVar) {
        if (b() != null) {
            ContentValues contentValues = new ContentValues();
            contentValues.put(LocationConst.TIME, hVar.c());
            contentValues.put(CampaignEx.JSON_KEY_AD_SOURCE_ID, Integer.valueOf(hVar.a()));
            contentValues.put("adNum", Integer.valueOf(hVar.d()));
            contentValues.put("unitId", hVar.e());
            contentValues.put("fb", Integer.valueOf(hVar.f()));
            contentValues.put("timeout", Integer.valueOf(hVar.g()));
            contentValues.put("network_type", Integer.valueOf(hVar.h()));
            b().insert("load_stat", null, contentValues);
        }
    }
}
