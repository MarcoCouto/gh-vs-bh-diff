package com.mintegral.msdk.base.b;

import android.content.ContentValues;
import android.database.Cursor;
import com.mansoon.BatteryDouble.Config;
import com.mintegral.msdk.base.entity.d;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: DisplayResourceTypeDao */
public class k extends a<d> {
    private static final String b = "com.mintegral.msdk.base.b.k";
    private static k c;

    private k(h hVar) {
        super(hVar);
    }

    public static k a(h hVar) {
        if (c == null) {
            synchronized (k.class) {
                if (c == null) {
                    c = new k(hVar);
                }
            }
        }
        return c;
    }

    public final synchronized void a(d dVar) {
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put("campaign_id", Long.valueOf(dVar.f2593a));
            contentValues.put("click_time", Long.valueOf(dVar.b));
            contentValues.put("is_click", Integer.valueOf(dVar.d));
            contentValues.put("resource_type", Integer.valueOf(dVar.c));
            if (b() != null) {
                b().insert("display_resource_type", null, contentValues);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public final String c() {
        boolean z;
        long currentTimeMillis = System.currentTimeMillis();
        List<d> a2 = a(currentTimeMillis);
        if (a2 == null) {
            return "";
        }
        String[] strArr = {"1", "2", "3", Config.DATA_HISTORY_DEFAULT, "5", "6", "7"};
        JSONObject jSONObject = new JSONObject();
        ArrayList arrayList = new ArrayList(7);
        for (int i = 0; i < 7; i++) {
            arrayList.add(new JSONArray());
        }
        try {
            for (d dVar : a2) {
                JSONArray jSONArray = (JSONArray) arrayList.get((int) ((currentTimeMillis - dVar.b) / 86400000));
                if (jSONArray.length() == 0) {
                    JSONObject jSONObject2 = new JSONObject();
                    jSONObject2.put(String.valueOf(dVar.c), dVar.d);
                    jSONArray.put(jSONObject2);
                } else {
                    int i2 = 0;
                    while (true) {
                        if (i2 >= jSONArray.length()) {
                            z = true;
                            break;
                        }
                        JSONObject jSONObject3 = jSONArray.getJSONObject(i2);
                        String valueOf = String.valueOf(dVar.c);
                        if (jSONObject3.has(valueOf)) {
                            if (jSONObject3.getInt(valueOf) == 0 && dVar.d == 1) {
                                jSONObject3.put(valueOf, dVar.d);
                            }
                            z = false;
                        } else {
                            i2++;
                        }
                    }
                    if (z) {
                        JSONObject jSONObject4 = new JSONObject();
                        jSONObject4.put(String.valueOf(dVar.c), dVar.d);
                        jSONArray.put(jSONObject4);
                    }
                }
            }
            for (int i3 = 0; i3 < 7; i3++) {
                jSONObject.put(strArr[i3], arrayList.get(i3));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jSONObject.toString();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0075, code lost:
        if (r10 == null) goto L_0x0088;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:?, code lost:
        r10.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0085, code lost:
        if (r10 == null) goto L_0x0088;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0089, code lost:
        return r11;
     */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x008d A[SYNTHETIC, Splitter:B:37:0x008d] */
    private synchronized List<d> a(long j) {
        Cursor cursor;
        List<d> list;
        Exception e;
        long j2 = j - 604800000;
        StringBuilder sb = new StringBuilder("select * from display_resource_type where click_time >= ");
        sb.append(j2);
        String sb2 = sb.toString();
        if (a() == null) {
            return null;
        }
        try {
            cursor = a().rawQuery(sb2, null);
            if (cursor != null) {
                try {
                    if (cursor.moveToFirst()) {
                        list = new ArrayList<>(cursor.getCount());
                        do {
                            try {
                                d dVar = new d(cursor.getLong(cursor.getColumnIndex("campaign_id")), cursor.getLong(cursor.getColumnIndex("click_time")), cursor.getInt(cursor.getColumnIndex("resource_type")), cursor.getInt(cursor.getColumnIndex("is_click")));
                                list.add(dVar);
                            } catch (Exception e2) {
                                e = e2;
                                try {
                                    e.printStackTrace();
                                } catch (Throwable th) {
                                    th = th;
                                    if (cursor != null) {
                                    }
                                    throw th;
                                }
                            }
                        } while (cursor.moveToNext());
                    }
                } catch (Exception e3) {
                    e = e3;
                    list = null;
                    e.printStackTrace();
                }
            }
            list = null;
        } catch (Exception e4) {
            list = null;
            e = e4;
            cursor = null;
            e.printStackTrace();
        } catch (Throwable th2) {
            th = th2;
            cursor = null;
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }
}
