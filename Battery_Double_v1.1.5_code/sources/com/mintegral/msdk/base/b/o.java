package com.mintegral.msdk.base.b;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.text.TextUtils;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.ironsource.sdk.constants.LocationConst;
import com.mintegral.msdk.base.controller.a;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.entity.j;
import com.mintegral.msdk.base.utils.g;
import java.util.ArrayList;
import java.util.List;

/* compiled from: OfferWallClickDao */
public class o extends a<j> {
    private static final String b = "com.mintegral.msdk.base.b.o";
    private static o c;

    private o(h hVar) {
        super(hVar);
    }

    public static o c() {
        try {
            if (c == null) {
                synchronized (w.class) {
                    if (c == null) {
                        Context h = a.d().h();
                        if (h != null) {
                            c = new o(i.a(h));
                        } else {
                            g.d(b, "OfferWallClickDao get Context is null");
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return c;
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x0044 A[SYNTHETIC, Splitter:B:22:0x0044] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x004b A[SYNTHETIC, Splitter:B:27:0x004b] */
    private synchronized int c(String str) {
        int i;
        Cursor cursor = null;
        i = 0;
        try {
            StringBuilder sb = new StringBuilder("select count(*) from offer_wall_click where host='");
            sb.append(str);
            sb.append("'");
            Cursor rawQuery = a().rawQuery(sb.toString(), null);
            if (rawQuery != null) {
                try {
                    if (rawQuery.moveToFirst()) {
                        i = rawQuery.getInt(0);
                    }
                } catch (Exception e) {
                    Exception exc = e;
                    cursor = rawQuery;
                    e = exc;
                    try {
                        e.printStackTrace();
                        if (cursor != null) {
                        }
                        return i;
                    } catch (Throwable th) {
                        th = th;
                        if (cursor != null) {
                        }
                        throw th;
                    }
                } catch (Throwable th2) {
                    Throwable th3 = th2;
                    cursor = rawQuery;
                    th = th3;
                    if (cursor != null) {
                        cursor.close();
                    }
                    throw th;
                }
            }
            if (rawQuery != null) {
                try {
                    rawQuery.close();
                } catch (Throwable th4) {
                    throw th4;
                }
            }
        } catch (Exception e2) {
            e = e2;
            e.printStackTrace();
            if (cursor != null) {
                cursor.close();
            }
            return i;
        }
        return i;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0036, code lost:
        return;
     */
    public final synchronized void a(long j, String str) {
        try {
            if (!TextUtils.isEmpty(str)) {
                long currentTimeMillis = System.currentTimeMillis() - j;
                StringBuilder sb = new StringBuilder("time<");
                sb.append(currentTimeMillis);
                sb.append(" and unitId=? and install_status=0");
                String sb2 = sb.toString();
                String[] strArr = {str};
                if (b() != null) {
                    b().delete("offer_wall_click", sb2, strArr);
                }
            } else {
                g.d(b, "unitId not for null");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:30:0x0057 A[SYNTHETIC, Splitter:B:30:0x0057] */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x005f A[SYNTHETIC, Splitter:B:36:0x005f] */
    private synchronized boolean d(String str) {
        Cursor cursor = null;
        try {
            StringBuilder sb = new StringBuilder("select * from offer_wall_click where clickId = '");
            sb.append(str);
            sb.append("'");
            Cursor rawQuery = a().rawQuery(sb.toString(), null);
            if (rawQuery != null) {
                try {
                    if (rawQuery.getCount() > 0) {
                        while (rawQuery.moveToNext()) {
                            if (rawQuery.getString(rawQuery.getColumnIndex("clickId")).equals(str)) {
                                if (rawQuery != null) {
                                    try {
                                        rawQuery.close();
                                    } catch (Throwable th) {
                                        throw th;
                                    }
                                }
                                return true;
                            }
                        }
                    }
                } catch (Exception e) {
                    e = e;
                    cursor = rawQuery;
                    try {
                        e.printStackTrace();
                        if (cursor != null) {
                        }
                        return false;
                    } catch (Throwable th2) {
                        th = th2;
                        if (cursor != null) {
                        }
                        throw th;
                    }
                } catch (Throwable th3) {
                    th = th3;
                    cursor = rawQuery;
                    if (cursor != null) {
                        cursor.close();
                    }
                    throw th;
                }
            }
            if (rawQuery != null) {
                rawQuery.close();
            }
        } catch (Exception e2) {
            e = e2;
            e.printStackTrace();
            if (cursor != null) {
                cursor.close();
            }
            return false;
        }
        return false;
    }

    /* JADX WARNING: type inference failed for: r1v0 */
    /* JADX WARNING: type inference failed for: r1v1, types: [java.util.List<java.lang.String>] */
    /* JADX WARNING: type inference failed for: r1v2, types: [android.database.Cursor] */
    /* JADX WARNING: type inference failed for: r1v3, types: [android.database.Cursor] */
    /* JADX WARNING: type inference failed for: r0v2 */
    /* JADX WARNING: type inference failed for: r1v4 */
    /* JADX WARNING: type inference failed for: r0v4 */
    /* JADX WARNING: type inference failed for: r5v6, types: [android.database.Cursor] */
    /* JADX WARNING: type inference failed for: r1v5 */
    /* JADX WARNING: type inference failed for: r1v6 */
    /* JADX WARNING: type inference failed for: r0v8 */
    /* JADX WARNING: type inference failed for: r1v7 */
    /* JADX WARNING: type inference failed for: r0v11, types: [java.util.List, java.util.ArrayList] */
    /* JADX WARNING: type inference failed for: r1v9 */
    /* JADX WARNING: type inference failed for: r1v11 */
    /* JADX WARNING: type inference failed for: r1v15 */
    /* JADX WARNING: type inference failed for: r0v12 */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0046, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0047, code lost:
        r3 = r1;
        r1 = r5;
        r5 = r3;
        r0 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x004b, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x004c, code lost:
        r1 = r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x006a, code lost:
        r1.close();
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Multi-variable type inference failed. Error: jadx.core.utils.exceptions.JadxRuntimeException: No candidate types for var: r1v0
  assigns: [?[int, float, boolean, short, byte, char, OBJECT, ARRAY], ?[OBJECT, ARRAY]]
  uses: [java.util.List<java.lang.String>, ?[int, boolean, OBJECT, ARRAY, byte, short, char], android.database.Cursor]
  mth insns count: 54
    	at jadx.core.dex.visitors.typeinference.TypeSearch.fillTypeCandidates(TypeSearch.java:237)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.typeinference.TypeSearch.run(TypeSearch.java:53)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.runMultiVariableSearch(TypeInferenceVisitor.java:99)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.visit(TypeInferenceVisitor.java:92)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:27)
    	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$1(DepthTraversal.java:14)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
    	at jadx.core.ProcessClass.process(ProcessClass.java:30)
    	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
    	at jadx.api.JavaClass.decompile(JavaClass.java:62)
    	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
     */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x004b A[ExcHandler: all (th java.lang.Throwable), Splitter:B:8:0x0025] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0063 A[SYNTHETIC, Splitter:B:32:0x0063] */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x006a  */
    /* JADX WARNING: Unknown variable types count: 5 */
    public final synchronized List<String> a(String str) {
        ? r1;
        ? r0;
        r1 = 0;
        if (!TextUtils.isEmpty(str)) {
            try {
                StringBuilder sb = new StringBuilder("SELECT * FROM offer_wall_click where unitId=");
                sb.append(str);
                sb.append(" and install_status=1");
                ? rawQuery = a().rawQuery(sb.toString(), null);
                if (rawQuery != 0) {
                    try {
                        if (rawQuery.getCount() > 0) {
                            ? arrayList = new ArrayList();
                            while (rawQuery.moveToNext()) {
                                arrayList.add(rawQuery.getString(rawQuery.getColumnIndex(RequestParameters.CAMPAIGN_ID)));
                            }
                            r1 = arrayList;
                        }
                    } catch (Exception e) {
                        r1 = rawQuery;
                        e = e;
                        r0 = 0;
                        try {
                            e.printStackTrace();
                            if (r1 != 0) {
                            }
                            r1 = r0;
                            return r1;
                        } catch (Throwable th) {
                            th = th;
                            ? r12 = r1;
                            if (r12 != 0) {
                            }
                            throw th;
                        }
                    } catch (Throwable th2) {
                    }
                }
                if (rawQuery != 0) {
                    rawQuery.close();
                }
            } catch (Exception e2) {
                e = e2;
                r0 = 0;
                e.printStackTrace();
                if (r1 != 0) {
                }
                r1 = r0;
                return r1;
            }
        }
        return r1;
    }

    /* JADX WARNING: Removed duplicated region for block: B:30:0x00a1 A[SYNTHETIC, Splitter:B:30:0x00a1] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00a9 A[Catch:{ all -> 0x00a6 }] */
    public final synchronized List<j> b(String str) {
        List<j> list;
        Cursor cursor;
        Exception e;
        List<j> list2;
        list = null;
        if (!TextUtils.isEmpty(str)) {
            try {
                StringBuilder sb = new StringBuilder("SELECT * FROM offer_wall_click where unitId=");
                sb.append(str);
                sb.append(" and install_status=0");
                cursor = a().rawQuery(sb.toString(), null);
                if (cursor != null) {
                    try {
                        if (cursor.getCount() > 0) {
                            list2 = new ArrayList<>();
                            while (cursor.moveToNext()) {
                                try {
                                    String string = cursor.getString(cursor.getColumnIndex("unitId"));
                                    String string2 = cursor.getString(cursor.getColumnIndex("clickId"));
                                    j jVar = new j(cursor.getString(cursor.getColumnIndex(RequestParameters.CAMPAIGN_ID)), cursor.getString(cursor.getColumnIndex("noticeUrl")), string2, cursor.getLong(cursor.getColumnIndex(LocationConst.TIME)), string, cursor.getString(cursor.getColumnIndex("host")), cursor.getInt(cursor.getColumnIndex("install_status")));
                                    list2.add(jVar);
                                } catch (Exception e2) {
                                    e = e2;
                                    try {
                                        e.printStackTrace();
                                        if (cursor != null) {
                                        }
                                        list = list2;
                                        return list;
                                    } catch (Throwable th) {
                                        th = th;
                                        if (cursor != null) {
                                            cursor.close();
                                        }
                                        throw th;
                                    }
                                }
                            }
                            list = list2;
                        }
                    } catch (Exception e3) {
                        e = e3;
                        list2 = null;
                        e.printStackTrace();
                        if (cursor != null) {
                        }
                        list = list2;
                        return list;
                    }
                }
                if (cursor != null) {
                    cursor.close();
                }
            } catch (Exception e4) {
                list2 = null;
                e = e4;
                cursor = null;
                e.printStackTrace();
                if (cursor != null) {
                    cursor.close();
                }
                list = list2;
                return list;
            } catch (Throwable th2) {
                th = th2;
                cursor = null;
                if (cursor != null) {
                }
                throw th;
            }
        }
        return list;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0031, code lost:
        return;
     */
    public final synchronized void a(String str, String str2) {
        if (!TextUtils.isEmpty(str) && !TextUtils.isEmpty(str2)) {
            try {
                StringBuilder sb = new StringBuilder("UPDATE offer_wall_click Set install_status==1 WHERE campaignId=");
                sb.append(str);
                sb.append(" AND unitId=");
                sb.append(str2);
                String sb2 = sb.toString();
                if (a() != null) {
                    a().execSQL(sb2);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:23:0x00a6, code lost:
        return;
     */
    public final synchronized void a(CampaignEx campaignEx, String str) {
        try {
            if (b() != null) {
                if (campaignEx != null || !TextUtils.isEmpty(str)) {
                    ContentValues contentValues = new ContentValues();
                    contentValues.put("clickId", campaignEx.getRequestId());
                    contentValues.put(LocationConst.TIME, Long.valueOf(System.currentTimeMillis()));
                    contentValues.put("noticeUrl", campaignEx.getNoticeUrl());
                    contentValues.put("unitId", str);
                    contentValues.put("host", campaignEx.getHost());
                    contentValues.put(RequestParameters.CAMPAIGN_ID, campaignEx.getId());
                    contentValues.put("install_status", Integer.valueOf(0));
                    if (TextUtils.isEmpty(campaignEx.getRequestId()) || !d(campaignEx.getRequestId())) {
                        if (c(campaignEx.getHost()) > 99) {
                            b().execSQL("delete from offer_wall_click WHERE id in(select id from offer_wall_click order by time desc limit 1)");
                        }
                        b().insert("offer_wall_click", null, contentValues);
                        return;
                    }
                    String[] strArr = {String.valueOf(campaignEx.getRequestId())};
                    b().update("offer_wall_click", contentValues, "clickId=?", strArr);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
