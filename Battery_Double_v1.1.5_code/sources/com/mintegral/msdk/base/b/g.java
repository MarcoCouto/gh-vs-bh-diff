package com.mintegral.msdk.base.b;

import android.content.ContentValues;
import android.database.Cursor;
import com.facebook.internal.NativeProtocol;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.entity.CampaignEx.c;

/* compiled from: CampaignReportDao */
public class g extends f {
    private static final String b = "com.mintegral.msdk.base.b.g";
    private static g c;

    private g(h hVar) {
        super(hVar);
    }

    public static g b(h hVar) {
        if (c == null) {
            synchronized (g.class) {
                if (c == null) {
                    c = new g(hVar);
                }
            }
        }
        return c;
    }

    public final synchronized long a(CampaignEx campaignEx) {
        if (campaignEx == null) {
            return 0;
        }
        try {
            if (b() == null) {
                return -1;
            }
            ContentValues contentValues = new ContentValues();
            contentValues.put("id", campaignEx.getId());
            contentValues.put("unitid", campaignEx.getCampaignUnitId());
            contentValues.put("tab", Integer.valueOf(campaignEx.getTab()));
            contentValues.put(CampaignEx.JSON_KEY_PACKAGE_NAME, campaignEx.getPackageName());
            contentValues.put(NativeProtocol.BRIDGE_ARG_APP_NAME_STRING, campaignEx.getAppName());
            contentValues.put("app_desc", campaignEx.getAppDesc());
            contentValues.put(CampaignEx.JSON_KEY_APP_SIZE, campaignEx.getSize());
            contentValues.put(CampaignEx.JSON_KEY_IMAGE_SIZE, campaignEx.getImageSize());
            contentValues.put(CampaignEx.JSON_KEY_ICON_URL, campaignEx.getIconUrl());
            contentValues.put("image_url", campaignEx.getImageUrl());
            contentValues.put(CampaignEx.JSON_KEY_IMPRESSION_URL, campaignEx.getImpressionURL());
            contentValues.put(CampaignEx.JSON_KEY_NOTICE_URL, campaignEx.getNoticeUrl());
            contentValues.put("download_url", campaignEx.getClickURL());
            contentValues.put("only_impression", campaignEx.getOnlyImpressionURL());
            contentValues.put(CampaignEx.JSON_KEY_ST_TS, Long.valueOf(campaignEx.getTimestamp()));
            contentValues.put("template", Integer.valueOf(campaignEx.getTemplate()));
            contentValues.put(CampaignEx.JSON_KEY_CLICK_MODE, campaignEx.getClick_mode());
            contentValues.put(CampaignEx.JSON_KEY_LANDING_TYPE, campaignEx.getLandingType());
            contentValues.put(CampaignEx.JSON_KEY_LINK_TYPE, Integer.valueOf(campaignEx.getLinkType()));
            contentValues.put("star", Double.valueOf(campaignEx.getRating()));
            contentValues.put("cti", Integer.valueOf(campaignEx.getClickInterval()));
            contentValues.put("cpti", Integer.valueOf(campaignEx.getPreClickInterval()));
            contentValues.put("preclick", Boolean.valueOf(campaignEx.isPreClick()));
            contentValues.put("level", Integer.valueOf(campaignEx.getCacheLevel()));
            contentValues.put("adSource", Integer.valueOf(campaignEx.getType()));
            contentValues.put("ad_call", campaignEx.getAdCall());
            contentValues.put("fc_a", Integer.valueOf(campaignEx.getFca()));
            contentValues.put(CampaignEx.JSON_KEY_AD_URL_LIST, campaignEx.getAd_url_list());
            contentValues.put("video_url", campaignEx.getVideoUrlEncode());
            contentValues.put(CampaignEx.JSON_KEY_VIDEO_SIZE, Integer.valueOf(campaignEx.getVideoSize()));
            contentValues.put(CampaignEx.JSON_KEY_VIDEO_LENGTHL, Integer.valueOf(campaignEx.getVideoLength()));
            contentValues.put(CampaignEx.JSON_KEY_VIDEO_RESOLUTION, campaignEx.getVideoResolution());
            contentValues.put(CampaignEx.JSON_KEY_ENDCARD_CLICK, Integer.valueOf(campaignEx.getEndcard_click_result()));
            contentValues.put(CampaignEx.JSON_KEY_WATCH_MILE, Integer.valueOf(campaignEx.getWatchMile()));
            contentValues.put("advImp", campaignEx.getAdvImp());
            contentValues.put("bty", Integer.valueOf(campaignEx.getBty()));
            contentValues.put(CampaignEx.JSON_KEY_T_IMP, Integer.valueOf(campaignEx.getTImp()));
            contentValues.put(CampaignEx.JSON_KEY_GUIDELINES, campaignEx.getGuidelines());
            contentValues.put(CampaignEx.JSON_KEY_OFFER_TYPE, Integer.valueOf(campaignEx.getOfferType()));
            contentValues.put("html_url", campaignEx.getHtmlUrl());
            contentValues.put("end_screen_url", campaignEx.getEndScreenUrl());
            contentValues.put(CampaignEx.JSON_KEY_REWARD_AMOUNT, Integer.valueOf(campaignEx.getRewardAmount()));
            contentValues.put(CampaignEx.JSON_KEY_REWARD_NAME, campaignEx.getRewardName());
            contentValues.put("reward_play_status", Integer.valueOf(campaignEx.getRewardPlayStatus()));
            contentValues.put(CampaignEx.JSON_KEY_ADV_ID, campaignEx.getAdvId());
            contentValues.put(CampaignEx.JSON_KEY_TTC_CT2, Integer.valueOf(campaignEx.getTtc_ct2() * 1000));
            contentValues.put(CampaignEx.JSON_KEY_TTC_TYPE, Integer.valueOf(campaignEx.getTtc_type()));
            contentValues.put("retarget", Integer.valueOf(campaignEx.getRetarget_offer()));
            contentValues.put("native_ad_tracking", campaignEx.getNativeVideoTrackingString());
            contentValues.put(CampaignEx.PLAYABLE_ADS_WITHOUT_VIDEO, Integer.valueOf(campaignEx.getPlayable_ads_without_video()));
            contentValues.put(CampaignEx.ENDCARD_URL, campaignEx.getendcard_url());
            contentValues.put(CampaignEx.VIDEO_END_TYPE, Integer.valueOf(campaignEx.getVideo_end_type()));
            contentValues.put(CampaignEx.LOOPBACK, campaignEx.getLoopbackString());
            contentValues.put(CampaignEx.JSON_KEY_REWARD_VIDEO_MD5, campaignEx.getVideoMD5Value());
            contentValues.put(CampaignEx.JSON_KEY_NV_T2, Integer.valueOf(campaignEx.getNvT2()));
            contentValues.put(CampaignEx.JSON_KEY_GIF_URL, campaignEx.getGifUrl());
            if (campaignEx.getRewardTemplateMode() != null) {
                contentValues.put("reward_teamplate", campaignEx.getRewardTemplateMode().a());
            }
            contentValues.put("c_coi", Integer.valueOf(campaignEx.getClickTimeOutInterval()));
            contentValues.put(CampaignEx.JSON_KEY_C_UA, Integer.valueOf(campaignEx.getcUA()));
            contentValues.put(CampaignEx.JSON_KEY_IMP_UA, Integer.valueOf(campaignEx.getImpUA()));
            contentValues.put(CampaignEx.KEY_GH_ID, campaignEx.getGhId());
            contentValues.put(CampaignEx.KEY_GH_PATH, campaignEx.getGhPath());
            contentValues.put(CampaignEx.KEY_BIND_ID, campaignEx.getBindId());
            return b().insert("report_campaign", null, contentValues);
        } catch (Exception unused) {
            return -1;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:30:0x03bb  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x03c3  */
    public final CampaignEx l(String str) {
        Cursor cursor;
        CampaignEx campaignEx;
        Exception e;
        CampaignEx campaignEx2 = null;
        try {
            StringBuilder sb = new StringBuilder("SELECT * FROM report_campaign where package_name ='");
            sb.append(str);
            sb.append("'");
            cursor = a().rawQuery(sb.toString(), null);
            if (cursor != null) {
                try {
                    if (cursor.getCount() > 0 && cursor.moveToNext()) {
                        campaignEx = new CampaignEx();
                        try {
                            campaignEx.setId(cursor.getString(cursor.getColumnIndex("id")));
                            campaignEx.setTab(cursor.getInt(cursor.getColumnIndex("tab")));
                            campaignEx.setPackageName(cursor.getString(cursor.getColumnIndex(CampaignEx.JSON_KEY_PACKAGE_NAME)));
                            campaignEx.setAppName(cursor.getString(cursor.getColumnIndex(NativeProtocol.BRIDGE_ARG_APP_NAME_STRING)));
                            campaignEx.setAppDesc(cursor.getString(cursor.getColumnIndex("app_desc")));
                            campaignEx.setSize(cursor.getString(cursor.getColumnIndex(CampaignEx.JSON_KEY_APP_SIZE)));
                            campaignEx.setImageSize(cursor.getString(cursor.getColumnIndex(CampaignEx.JSON_KEY_IMAGE_SIZE)));
                            campaignEx.setIconUrl(cursor.getString(cursor.getColumnIndex(CampaignEx.JSON_KEY_ICON_URL)));
                            campaignEx.setImageUrl(cursor.getString(cursor.getColumnIndex("image_url")));
                            campaignEx.setImpressionURL(cursor.getString(cursor.getColumnIndex(CampaignEx.JSON_KEY_IMPRESSION_URL)));
                            campaignEx.setNoticeUrl(cursor.getString(cursor.getColumnIndex(CampaignEx.JSON_KEY_NOTICE_URL)));
                            campaignEx.setClickURL(cursor.getString(cursor.getColumnIndex("download_url")));
                            campaignEx.setOnlyImpressionURL(cursor.getString(cursor.getColumnIndex("only_impression")));
                            boolean z = true;
                            if (cursor.getInt(cursor.getColumnIndex("preclick")) != 1) {
                                z = false;
                            }
                            campaignEx.setPreClick(z);
                            campaignEx.setTemplate(cursor.getInt(cursor.getColumnIndex("template")));
                            campaignEx.setLandingType(cursor.getString(cursor.getColumnIndex(CampaignEx.JSON_KEY_LANDING_TYPE)));
                            campaignEx.setLinkType(cursor.getInt(cursor.getColumnIndex(CampaignEx.JSON_KEY_LINK_TYPE)));
                            campaignEx.setClick_mode(cursor.getString(cursor.getColumnIndex(CampaignEx.JSON_KEY_CLICK_MODE)));
                            campaignEx.setRating(Double.parseDouble(cursor.getString(cursor.getColumnIndex("star"))));
                            campaignEx.setClickInterval(cursor.getInt(cursor.getColumnIndex("cti")));
                            campaignEx.setPreClickInterval(cursor.getInt(cursor.getColumnIndex("cpti")));
                            campaignEx.setTimestamp(cursor.getLong(cursor.getColumnIndex(CampaignEx.JSON_KEY_ST_TS)));
                            campaignEx.setCacheLevel(cursor.getInt(cursor.getColumnIndex("level")));
                            campaignEx.setAdCall(cursor.getString(cursor.getColumnIndex("ad_call")));
                            campaignEx.setFca(cursor.getInt(cursor.getColumnIndex("fc_a")));
                            campaignEx.setAd_url_list(cursor.getString(cursor.getColumnIndex(CampaignEx.JSON_KEY_AD_URL_LIST)));
                            campaignEx.setVideoLength(cursor.getInt(cursor.getColumnIndex(CampaignEx.JSON_KEY_VIDEO_LENGTHL)));
                            campaignEx.setVideoSize(cursor.getInt(cursor.getColumnIndex(CampaignEx.JSON_KEY_VIDEO_SIZE)));
                            campaignEx.setVideoResolution(cursor.getString(cursor.getColumnIndex(CampaignEx.JSON_KEY_VIDEO_RESOLUTION)));
                            campaignEx.setEndcard_click_result(cursor.getInt(cursor.getColumnIndex(CampaignEx.JSON_KEY_ENDCARD_CLICK)));
                            campaignEx.setVideoUrlEncode(cursor.getString(cursor.getColumnIndex("video_url")));
                            campaignEx.setWatchMile(cursor.getInt(cursor.getColumnIndex(CampaignEx.JSON_KEY_WATCH_MILE)));
                            campaignEx.setTImp(cursor.getInt(cursor.getColumnIndex(CampaignEx.JSON_KEY_T_IMP)));
                            campaignEx.setBty(cursor.getInt(cursor.getColumnIndex("bty")));
                            campaignEx.setAdvImp(cursor.getString(cursor.getColumnIndex("advImp")));
                            campaignEx.setGuidelines(cursor.getString(cursor.getColumnIndex(CampaignEx.JSON_KEY_GUIDELINES)));
                            campaignEx.setOfferType(cursor.getInt(cursor.getColumnIndex(CampaignEx.JSON_KEY_OFFER_TYPE)));
                            campaignEx.setHtmlUrl(cursor.getString(cursor.getColumnIndex("html_url")));
                            campaignEx.setGuidelines(cursor.getString(cursor.getColumnIndex(CampaignEx.JSON_KEY_GUIDELINES)));
                            campaignEx.setHtmlUrl(cursor.getString(cursor.getColumnIndex("html_url")));
                            campaignEx.setEndScreenUrl(cursor.getString(cursor.getColumnIndex("end_screen_url")));
                            campaignEx.setRewardName(cursor.getString(cursor.getColumnIndex(CampaignEx.JSON_KEY_REWARD_NAME)));
                            campaignEx.setRewardAmount(cursor.getInt(cursor.getColumnIndex(CampaignEx.JSON_KEY_REWARD_AMOUNT)));
                            campaignEx.setRewardPlayStatus(cursor.getInt(cursor.getColumnIndex("reward_play_status")));
                            campaignEx.setAdvId(cursor.getString(cursor.getColumnIndex(CampaignEx.JSON_KEY_ADV_ID)));
                            campaignEx.setTtc_ct2(cursor.getInt(cursor.getColumnIndex(CampaignEx.JSON_KEY_TTC_CT2)));
                            campaignEx.setTtc_type(cursor.getInt(cursor.getColumnIndex(CampaignEx.JSON_KEY_TTC_TYPE)));
                            campaignEx.setRetarget_offer(cursor.getInt(cursor.getColumnIndex("retarget")));
                            campaignEx.setCampaignUnitId(cursor.getString(cursor.getColumnIndex("unitid")));
                            campaignEx.setNativeVideoTracking(CampaignEx.TrackingStr2Object(cursor.getString(cursor.getColumnIndex("native_ad_tracking"))));
                            campaignEx.setNativeVideoTrackingString(cursor.getString(cursor.getColumnIndex("native_ad_tracking")));
                            campaignEx.setVideo_end_type(cursor.getInt(cursor.getColumnIndex(CampaignEx.VIDEO_END_TYPE)));
                            campaignEx.setendcard_url(cursor.getString(cursor.getColumnIndex(CampaignEx.ENDCARD_URL)));
                            campaignEx.setPlayable_ads_without_video(cursor.getInt(cursor.getColumnIndex(CampaignEx.PLAYABLE_ADS_WITHOUT_VIDEO)));
                            campaignEx.setLoopbackString(cursor.getString(cursor.getColumnIndex(CampaignEx.LOOPBACK)));
                            campaignEx.setLoopbackMap(CampaignEx.loopbackStrToMap(cursor.getString(cursor.getColumnIndex(CampaignEx.LOOPBACK))));
                            campaignEx.setRewardTemplateMode(c.a(cursor.getString(cursor.getColumnIndex("reward_teamplate"))));
                            campaignEx.setVideoMD5Value(cursor.getString(cursor.getColumnIndex(CampaignEx.JSON_KEY_REWARD_VIDEO_MD5)));
                            campaignEx.setGifUrl(cursor.getString(cursor.getColumnIndex(CampaignEx.JSON_KEY_GIF_URL)));
                            campaignEx.setNvT2(cursor.getInt(cursor.getColumnIndex(CampaignEx.JSON_KEY_NV_T2)));
                            campaignEx.setClickTimeOutInterval(cursor.getInt(cursor.getColumnIndex("c_coi")));
                            campaignEx.setcUA(cursor.getInt(cursor.getColumnIndex(CampaignEx.JSON_KEY_C_UA)));
                            campaignEx.setImpUA(cursor.getInt(cursor.getColumnIndex(CampaignEx.JSON_KEY_IMP_UA)));
                            campaignEx.setGhId(cursor.getString(cursor.getColumnIndex(CampaignEx.KEY_GH_ID)));
                            campaignEx.setGhPath(cursor.getString(cursor.getColumnIndex(CampaignEx.KEY_GH_PATH)));
                            campaignEx.setBindId(cursor.getString(cursor.getColumnIndex(CampaignEx.KEY_BIND_ID)));
                            campaignEx2 = campaignEx;
                        } catch (Exception e2) {
                            e = e2;
                            try {
                                e.printStackTrace();
                                if (cursor != null) {
                                }
                                return campaignEx;
                            } catch (Throwable th) {
                                th = th;
                                if (cursor != null) {
                                    cursor.close();
                                }
                                throw th;
                            }
                        }
                    }
                } catch (Exception e3) {
                    Exception exc = e3;
                    campaignEx = null;
                    e = exc;
                    e.printStackTrace();
                    if (cursor != null) {
                        cursor.close();
                    }
                    return campaignEx;
                }
            }
            if (cursor == null) {
                return campaignEx2;
            }
            cursor.close();
            return campaignEx2;
        } catch (Exception e4) {
            campaignEx = null;
            e = e4;
            cursor = null;
            e.printStackTrace();
            if (cursor != null) {
            }
            return campaignEx;
        } catch (Throwable th2) {
            th = th2;
            cursor = null;
            if (cursor != null) {
            }
            throw th;
        }
    }

    public final synchronized void m(String str) {
        try {
            StringBuilder sb = new StringBuilder("package_name = '");
            sb.append(str);
            sb.append("'");
            String sb2 = sb.toString();
            if (b() != null) {
                b().delete("report_campaign", sb2, null);
            }
        } catch (Exception unused) {
        }
    }
}
