package com.mintegral.msdk.base.b;

import android.content.ContentValues;
import android.database.Cursor;
import android.text.TextUtils;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.mintegral.msdk.b.a;
import com.mintegral.msdk.b.b;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.base.utils.q;
import com.mintegral.msdk.click.CommonJumpLoader.JumpLoaderResult;
import java.util.HashSet;
import java.util.Iterator;
import org.json.JSONArray;

/* compiled from: CampaignClickDao */
public class d extends a<JumpLoaderResult> {
    public static final String b = "d";
    private static d c;
    private a d;
    private int e = 100;

    private d(h hVar) {
        super(hVar);
    }

    public static d a(h hVar) {
        if (c == null) {
            synchronized (d.class) {
                if (c == null) {
                    c = new d(hVar);
                }
            }
        }
        return c;
    }

    public final synchronized void c() {
        try {
            StringBuilder sb = new StringBuilder("(pts not ");
            sb.append(null);
            sb.append(" AND (  ( ttc_type = 2 AND (  ( cps = 1 AND (");
            sb.append(System.currentTimeMillis());
            sb.append(" - pts) > cpti )  OR  (cps = 0 AND (");
            sb.append(System.currentTimeMillis());
            sb.append(" - pts) > cpei )  )  ) OR ( ttc_type = 1 AND  ( ");
            sb.append(System.currentTimeMillis());
            sb.append(" - pts )  > ttc_ct2 ) ) ) OR ( pts is ");
            sb.append(null);
            sb.append(" AND  ( ");
            sb.append(System.currentTimeMillis());
            sb.append(" - ts) > cti)");
            b().delete("campaignclick", sb.toString(), null);
        } catch (Exception e2) {
            g.d(b, e2.getMessage());
        }
    }

    public final synchronized long a(CampaignEx campaignEx, String str) {
        if (campaignEx == null) {
            return 0;
        }
        try {
            String a2 = q.a((Object) campaignEx.getJumpResult());
            ContentValues contentValues = new ContentValues();
            contentValues.put("id", campaignEx.getId());
            contentValues.put("unitid", str);
            contentValues.put(IronSourceConstants.EVENTS_RESULT, a2);
            contentValues.put("cpti", Integer.valueOf(campaignEx.getPreClickInterval() * 1000));
            contentValues.put("cti", Integer.valueOf(campaignEx.getClickInterval() * 1000));
            contentValues.put(CampaignEx.JSON_KEY_PACKAGE_NAME, campaignEx.getPackageName());
            b.a();
            a b2 = b.b(com.mintegral.msdk.base.controller.a.d().j());
            contentValues.put(CampaignEx.JSON_KEY_ST_TS, Long.valueOf(System.currentTimeMillis()));
            if (b2 != null && b2.aT() > 0) {
                contentValues.put("cpei", Integer.valueOf(b2.aT() * 1000));
            }
            if (b2 != null && b2.aU() > 0) {
                contentValues.put("cpoci", Integer.valueOf(b2.aU() * 1000));
            }
            if (c(campaignEx.getId(), str)) {
                StringBuilder sb = new StringBuilder("id = ");
                sb.append(campaignEx.getId());
                sb.append(" AND unitid = ");
                sb.append(str);
                String sb2 = sb.toString();
                if (b() == null) {
                    return -1;
                }
                return (long) b().update("campaignclick", contentValues, sb2, null);
            }
            return b().insert("campaignclick", null, contentValues);
        } catch (Exception unused) {
            return -1;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:44:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:40:0x0102 */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x010b A[SYNTHETIC, Splitter:B:43:0x010b] */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x0112 A[SYNTHETIC, Splitter:B:48:0x0112] */
    public final synchronized String a(String str) {
        String str2;
        str2 = "";
        if (this.d == null) {
            b.a();
            this.d = b.b(com.mintegral.msdk.base.controller.a.d().j());
        }
        if (this.d != null && this.d.ae() > 0) {
            this.e = this.d.ae();
        }
        HashSet hashSet = new HashSet();
        StringBuilder sb = new StringBuilder("SELECT id FROM campaignclick WHERE (ttc_type = 2 AND ( (cps = 1 AND unitid = '");
        sb.append(str);
        sb.append("' AND (cpti + pts) > ");
        sb.append(System.currentTimeMillis());
        sb.append(") OR  (cps = 0 AND (pts + cpei) > ");
        sb.append(System.currentTimeMillis());
        sb.append(") OR (unitid <> '");
        sb.append(str);
        sb.append("' AND (pts + cpoci) > ");
        sb.append(System.currentTimeMillis());
        sb.append(" AND cps = 1 ))  ) OR (ttc_type = 1 AND ( (unitid = '");
        sb.append(str);
        sb.append("' AND (");
        sb.append(System.currentTimeMillis());
        sb.append(" - pts ) <= ttc_ct2 ) OR (unitid <> '");
        sb.append(str);
        sb.append("' AND (");
        sb.append(System.currentTimeMillis());
        sb.append(" - pts) <= n4 ) ) ) ORDER BY pts DESC  LIMIT ");
        sb.append(this.e);
        String sb2 = sb.toString();
        Cursor cursor = null;
        Cursor cursor2 = a().rawQuery(sb2, null);
        if (cursor2 != null) {
            try {
                if (cursor2.getCount() > 0) {
                    int i = 0;
                    cursor2.moveToFirst();
                    while (!cursor2.isAfterLast() && i < 200) {
                        hashSet.add(String.valueOf(cursor2.getInt(cursor2.getColumnIndex("id"))));
                        cursor2.moveToNext();
                        i++;
                    }
                }
            } catch (Exception unused) {
                cursor = cursor2;
                try {
                    g.d(b, "AvoidRepetition report fail");
                    if (cursor != null) {
                    }
                    return str2;
                } catch (Throwable th) {
                    th = th;
                    cursor2 = cursor;
                    if (cursor2 != null) {
                    }
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                if (cursor2 != null) {
                    cursor2.close();
                }
                throw th;
            }
        }
        Iterator it = hashSet.iterator();
        JSONArray jSONArray = new JSONArray();
        while (it.hasNext()) {
            jSONArray.put(Integer.valueOf((String) it.next()));
        }
        String jSONArray2 = jSONArray.toString();
        if (cursor2 != null) {
            cursor2.close();
        }
        str2 = jSONArray2;
        return str2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0041, code lost:
        return false;
     */
    public final synchronized boolean a(String str, String str2) {
        try {
            StringBuilder sb = new StringBuilder("SELECT id FROM campaignclick WHERE id='");
            sb.append(str);
            sb.append("' AND unitid='");
            sb.append(str2);
            sb.append("' AND cti + ts > ");
            sb.append(System.currentTimeMillis());
            Cursor rawQuery = a().rawQuery(sb.toString(), null);
            if (rawQuery != null && rawQuery.getCount() > 0) {
                rawQuery.close();
                return true;
            } else if (rawQuery != null) {
                rawQuery.close();
            }
        } catch (Exception e2) {
            g.d(b, e2.getMessage());
            return false;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0046, code lost:
        return r12;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0049, code lost:
        if (r11 != null) goto L_0x004b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
        r11.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0057, code lost:
        if (r11 != null) goto L_0x004b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x005b, code lost:
        return null;
     */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x005f A[SYNTHETIC, Splitter:B:33:0x005f] */
    public final synchronized JumpLoaderResult b(String str, String str2) {
        Cursor cursor;
        try {
            cursor = a().query("campaignclick", null, "id=? AND unitid=?", new String[]{str, str2}, null, null, null, null);
            if (cursor != null) {
                try {
                    if (cursor.getCount() > 0 && cursor.moveToFirst()) {
                        String string = cursor.getString(cursor.getColumnIndex(IronSourceConstants.EVENTS_RESULT));
                        if (!TextUtils.isEmpty(string)) {
                            JumpLoaderResult jumpLoaderResult = (JumpLoaderResult) q.a(string);
                            if (cursor != null) {
                                try {
                                    cursor.close();
                                } catch (Throwable th) {
                                    throw th;
                                }
                            }
                        }
                    }
                } catch (Exception e2) {
                    e = e2;
                    try {
                        e.printStackTrace();
                    } catch (Throwable th2) {
                        th = th2;
                        if (cursor != null) {
                        }
                        throw th;
                    }
                }
            }
        } catch (Exception e3) {
            e = e3;
            cursor = null;
            e.printStackTrace();
        } catch (Throwable th3) {
            th = th3;
            cursor = null;
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    private synchronized boolean c(String str, String str2) {
        StringBuilder sb = new StringBuilder("SELECT id FROM campaignclick WHERE id='");
        sb.append(str);
        sb.append("' AND unitid= '");
        sb.append(str2);
        sb.append("'");
        Cursor rawQuery = a().rawQuery(sb.toString(), null);
        if (rawQuery == null || rawQuery.getCount() <= 0) {
            if (rawQuery != null) {
                rawQuery.close();
            }
            return false;
        }
        rawQuery.close();
        return true;
    }
}
