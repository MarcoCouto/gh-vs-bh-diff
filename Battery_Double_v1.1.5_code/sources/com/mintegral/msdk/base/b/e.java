package com.mintegral.msdk.base.b;

import android.database.Cursor;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.entity.b;
import java.util.ArrayList;
import java.util.List;

/* compiled from: CampaignClickTimeDao */
public class e extends a<b> {
    private static e b;

    private e(h hVar) {
        super(hVar);
    }

    public static e a(h hVar) {
        if (b == null) {
            synchronized (e.class) {
                if (b == null) {
                    b = new e(hVar);
                }
            }
        }
        return b;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0108, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x010b, code lost:
        if (r1 != null) goto L_0x010d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0119, code lost:
        if (r1 != null) goto L_0x010d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x011d, code lost:
        return null;
     */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0121 A[SYNTHETIC, Splitter:B:40:0x0121] */
    public final synchronized List<b> c() {
        Cursor cursor;
        String str = "select * from click_time LIMIT 20";
        if (b() == null) {
            return null;
        }
        try {
            cursor = b().rawQuery(str, null);
            if (cursor != null) {
                try {
                    if (cursor.getCount() > 0) {
                        ArrayList arrayList = new ArrayList();
                        int i = 0;
                        while (cursor.moveToNext() && i < 20) {
                            i++;
                            int i2 = cursor.getInt(cursor.getColumnIndex("id"));
                            String string = cursor.getString(cursor.getColumnIndex(RequestParameters.CAMPAIGN_ID));
                            int i3 = cursor.getInt(cursor.getColumnIndex("click_type"));
                            String string2 = cursor.getString(cursor.getColumnIndex("click_duration"));
                            String string3 = cursor.getString(cursor.getColumnIndex("last_url"));
                            int i4 = cursor.getInt(cursor.getColumnIndex("type"));
                            int i5 = cursor.getInt(cursor.getColumnIndex("code"));
                            String string4 = cursor.getString(cursor.getColumnIndex("header"));
                            String string5 = cursor.getString(cursor.getColumnIndex("exception"));
                            String string6 = cursor.getString(cursor.getColumnIndex("content"));
                            String string7 = cursor.getString(cursor.getColumnIndex(MIntegralConstans.PROPERTIES_UNIT_ID));
                            String string8 = cursor.getString(cursor.getColumnIndex("rid"));
                            int i6 = cursor.getInt(cursor.getColumnIndex(CampaignEx.JSON_KEY_LANDING_TYPE));
                            int i7 = cursor.getInt(cursor.getColumnIndex(CampaignEx.JSON_KEY_LINK_TYPE));
                            int i8 = cursor.getInt(cursor.getColumnIndex("network_type"));
                            String string9 = cursor.getString(cursor.getColumnIndex("network_str"));
                            b bVar = new b(string8, string, string2, string3, i5, string5, string4, string6, i4, i3, string7, i6, i7, cursor.getString(cursor.getColumnIndex("click_time")), i8, cursor.getInt(cursor.getColumnIndex("market_result")), string9);
                            arrayList.add(bVar);
                            b().delete("click_time", "id = ?", new String[]{String.valueOf(i2)});
                        }
                        if (cursor != null) {
                            cursor.close();
                        }
                    }
                } catch (Exception e) {
                    e = e;
                    try {
                        e.printStackTrace();
                    } catch (Throwable th) {
                        th = th;
                        if (cursor != null) {
                        }
                        throw th;
                    }
                }
            }
        } catch (Exception e2) {
            e = e2;
            cursor = null;
            e.printStackTrace();
        } catch (Throwable th2) {
            th = th2;
            cursor = null;
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x0032 A[SYNTHETIC, Splitter:B:22:0x0032] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0039 A[SYNTHETIC, Splitter:B:27:0x0039] */
    public final synchronized int d() {
        int i;
        Cursor cursor = null;
        i = 0;
        try {
            Cursor rawQuery = a().rawQuery("select count(*) from click_time", null);
            if (rawQuery != null) {
                try {
                    if (rawQuery.moveToFirst()) {
                        i = rawQuery.getInt(0);
                    }
                } catch (Exception e) {
                    Cursor cursor2 = rawQuery;
                    e = e;
                    cursor = cursor2;
                    try {
                        e.printStackTrace();
                        if (cursor != null) {
                        }
                        return i;
                    } catch (Throwable th) {
                        th = th;
                        if (cursor != null) {
                        }
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    cursor = rawQuery;
                    if (cursor != null) {
                        cursor.close();
                    }
                    throw th;
                }
            }
            if (rawQuery != null) {
                try {
                    rawQuery.close();
                } catch (Throwable th3) {
                    throw th3;
                }
            }
        } catch (Exception e2) {
            e = e2;
            e.printStackTrace();
            if (cursor != null) {
                cursor.close();
            }
            return i;
        }
        return i;
    }
}
