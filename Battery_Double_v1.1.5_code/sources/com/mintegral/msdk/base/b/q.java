package com.mintegral.msdk.base.b;

import android.content.ContentValues;
import android.database.Cursor;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.entity.m;
import com.mintegral.msdk.out.Campaign;
import java.util.ArrayList;
import java.util.List;

/* compiled from: PInfoDao */
public class q extends a<Campaign> {
    private static final String b = "com.mintegral.msdk.base.b.q";
    private static q c;

    private q(h hVar) {
        super(hVar);
    }

    public static q a(h hVar) {
        if (c == null) {
            synchronized (q.class) {
                if (c == null) {
                    c = new q(hVar);
                }
            }
        }
        return c;
    }

    public final synchronized long a(m mVar) {
        if (mVar == null) {
            return 0;
        }
        try {
            if (b() == null) {
                return -1;
            }
            ContentValues contentValues = new ContentValues();
            contentValues.put(CampaignEx.JSON_KEY_PACKAGE_NAME, mVar.a());
            contentValues.put("label", Integer.valueOf(mVar.b()));
            contentValues.put("modify_time", Long.valueOf(mVar.c()));
            contentValues.put("pkg_source", mVar.d());
            if (a(mVar.a())) {
                StringBuilder sb = new StringBuilder("package_name = '");
                sb.append(mVar.a());
                sb.append("'");
                return (long) b().update("pinfo", contentValues, sb.toString(), null);
            }
            return b().insert("pinfo", null, contentValues);
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x001d, code lost:
        return;
     */
    public final synchronized void a(final List<m> list) {
        if (list != null) {
            if (list.size() != 0) {
                new Thread(new Runnable() {
                    public final void run() {
                        for (m a2 : list) {
                            q.this.a(a2);
                        }
                    }
                }).start();
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0032, code lost:
        return false;
     */
    private synchronized boolean a(String str) {
        try {
            StringBuilder sb = new StringBuilder("SELECT package_name FROM pinfo WHERE package_name='");
            sb.append(str);
            sb.append("'");
            Cursor rawQuery = a().rawQuery(sb.toString(), null);
            if (rawQuery != null && rawQuery.getCount() > 0) {
                rawQuery.close();
                return true;
            } else if (rawQuery != null) {
                rawQuery.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:27:0x0079 A[SYNTHETIC, Splitter:B:27:0x0079] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0082 A[SYNTHETIC, Splitter:B:34:0x0082] */
    public final synchronized List<m> c() {
        List<m> list;
        Cursor cursor;
        List<m> list2;
        Exception e;
        list = null;
        try {
            cursor = a().rawQuery("SELECT * FROM pinfo", null);
            if (cursor != null) {
                try {
                    if (cursor.getCount() > 0) {
                        list2 = new ArrayList<>();
                        while (cursor.moveToNext()) {
                            try {
                                m mVar = new m();
                                mVar.a(cursor.getString(cursor.getColumnIndex(CampaignEx.JSON_KEY_PACKAGE_NAME)));
                                mVar.a(cursor.getInt(cursor.getColumnIndex("label")));
                                mVar.a(cursor.getLong(cursor.getColumnIndex("modify_time")));
                                mVar.b(cursor.getString(cursor.getColumnIndex("pkg_source")));
                                list2.add(mVar);
                            } catch (Exception e2) {
                                e = e2;
                                try {
                                    e.printStackTrace();
                                    if (cursor != null) {
                                    }
                                    list = list2;
                                    return list;
                                } catch (Throwable th) {
                                    th = th;
                                    if (cursor != null) {
                                    }
                                    throw th;
                                }
                            }
                        }
                        list = list2;
                    }
                } catch (Exception e3) {
                    Exception exc = e3;
                    list2 = null;
                    e = exc;
                    e.printStackTrace();
                    if (cursor != null) {
                        cursor.close();
                    }
                    list = list2;
                    return list;
                }
            }
            if (cursor != null) {
                try {
                    cursor.close();
                } catch (Throwable th2) {
                    throw th2;
                }
            }
        } catch (Exception e4) {
            list2 = null;
            e = e4;
            cursor = null;
            e.printStackTrace();
            if (cursor != null) {
            }
            list = list2;
            return list;
        } catch (Throwable th3) {
            Throwable th4 = th3;
            cursor = null;
            th = th4;
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
        return list;
    }

    /* JADX WARNING: Removed duplicated region for block: B:27:0x0085 A[SYNTHETIC, Splitter:B:27:0x0085] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x008e A[SYNTHETIC, Splitter:B:34:0x008e] */
    public final synchronized List<m> a(int i) {
        List<m> list;
        Cursor cursor;
        List<m> list2;
        Exception e;
        list = null;
        try {
            StringBuilder sb = new StringBuilder("SELECT * FROM pinfo WHERE label <> 0  ORDER BY label DESC LIMIT ");
            sb.append(i);
            cursor = a().rawQuery(sb.toString(), null);
            if (cursor != null) {
                try {
                    if (cursor.getCount() > 0) {
                        list2 = new ArrayList<>();
                        while (cursor.moveToNext()) {
                            try {
                                m mVar = new m();
                                mVar.a(cursor.getString(cursor.getColumnIndex(CampaignEx.JSON_KEY_PACKAGE_NAME)));
                                mVar.a(cursor.getInt(cursor.getColumnIndex("label")));
                                mVar.a(cursor.getLong(cursor.getColumnIndex("modify_time")));
                                mVar.b(cursor.getString(cursor.getColumnIndex("pkg_source")));
                                list2.add(mVar);
                            } catch (Exception e2) {
                                e = e2;
                                try {
                                    e.printStackTrace();
                                    if (cursor != null) {
                                    }
                                    list = list2;
                                    return list;
                                } catch (Throwable th) {
                                    th = th;
                                    if (cursor != null) {
                                    }
                                    throw th;
                                }
                            }
                        }
                        list = list2;
                    }
                } catch (Exception e3) {
                    Exception exc = e3;
                    list2 = null;
                    e = exc;
                    e.printStackTrace();
                    if (cursor != null) {
                    }
                    list = list2;
                    return list;
                }
            }
            if (cursor != null) {
                try {
                    cursor.close();
                } catch (Throwable th2) {
                    throw th2;
                }
            }
        } catch (Exception e4) {
            list2 = null;
            e = e4;
            cursor = null;
            e.printStackTrace();
            if (cursor != null) {
                cursor.close();
            }
            list = list2;
            return list;
        } catch (Throwable th3) {
            th = th3;
            cursor = null;
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
        return list;
    }
}
