package com.mintegral.msdk.base.b;

import android.database.Cursor;
import com.mansoon.BatteryDouble.Config;
import com.mintegral.msdk.base.entity.n;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ReportErrorDao */
public class r extends a {
    private static r b;

    private r(h hVar) {
        super(hVar);
    }

    public static r a(h hVar) {
        if (b == null) {
            synchronized (r.class) {
                if (b == null) {
                    b = new r(hVar);
                }
            }
        }
        return b;
    }

    /* JADX WARNING: Removed duplicated region for block: B:25:0x003f A[SYNTHETIC, Splitter:B:25:0x003f] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0046 A[SYNTHETIC, Splitter:B:30:0x0046] */
    public final synchronized int c() {
        int i;
        i = 0;
        Cursor cursor = null;
        try {
            Cursor query = a().query("reporterror", new String[]{" count(*) "}, null, null, null, null, null);
            if (query != null) {
                try {
                    if (query.moveToFirst()) {
                        i = query.getInt(0);
                    }
                } catch (Exception e) {
                    Cursor cursor2 = query;
                    e = e;
                    cursor = cursor2;
                    try {
                        e.printStackTrace();
                        if (cursor != null) {
                            cursor.close();
                        }
                        return i;
                    } catch (Throwable th) {
                        th = th;
                        if (cursor != null) {
                        }
                        throw th;
                    }
                } catch (Throwable th2) {
                    Cursor cursor3 = query;
                    th = th2;
                    cursor = cursor3;
                    if (cursor != null) {
                        cursor.close();
                    }
                    throw th;
                }
            }
            if (query != null) {
                query.close();
            }
        } catch (Exception e2) {
            e = e2;
            e.printStackTrace();
            if (cursor != null) {
            }
            return i;
        }
        return i;
    }

    public final synchronized int a(String str) {
        String str2 = "url=?";
        try {
            String[] strArr = {str};
            if (b() == null) {
                return -1;
            }
            return b().delete("reporterror", str2, strArr);
        } catch (Exception unused) {
            return -1;
        }
    }

    public final synchronized int a(String str, String str2) {
        String str3 = "url=? and data=?";
        if (str == null) {
            str = "";
        }
        try {
            String[] strArr = {str2, str};
            if (b() == null) {
                return -1;
            }
            return b().delete("reporterror", str3, strArr);
        } catch (Exception unused) {
            return -1;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x0036 A[SYNTHETIC, Splitter:B:20:0x0036] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x003e A[SYNTHETIC, Splitter:B:26:0x003e] */
    public final synchronized int d() {
        int i;
        Cursor cursor = null;
        try {
            Cursor query = a().query("reporterror", new String[]{" count(*) "}, null, null, null, null, null, null);
            try {
                i = query.getCount();
                if (query != null) {
                    query.close();
                }
            } catch (Exception e) {
                Cursor cursor2 = query;
                e = e;
                cursor = cursor2;
                try {
                    e.printStackTrace();
                    if (cursor != null) {
                    }
                    i = 0;
                    return i;
                } catch (Throwable th) {
                    th = th;
                    if (cursor != null) {
                    }
                    throw th;
                }
            } catch (Throwable th2) {
                Cursor cursor3 = query;
                th = th2;
                cursor = cursor3;
                if (cursor != null) {
                    cursor.close();
                }
                throw th;
            }
        } catch (Exception e2) {
            e = e2;
            e.printStackTrace();
            if (cursor != null) {
                cursor.close();
            }
            i = 0;
            return i;
        }
        return i;
    }

    /* JADX WARNING: Removed duplicated region for block: B:27:0x0075 A[SYNTHETIC, Splitter:B:27:0x0075] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x007e A[SYNTHETIC, Splitter:B:34:0x007e] */
    public final synchronized List<n> e() {
        List<n> list;
        Cursor cursor;
        List<n> list2;
        Exception e;
        list = null;
        try {
            cursor = a().query("reporterror", null, null, null, null, null, null, Config.UPLOAD_DEFAULT_RATE);
            if (cursor != null) {
                try {
                    if (cursor.getCount() > 0) {
                        list2 = new ArrayList<>();
                        while (cursor.moveToNext()) {
                            try {
                                list2.add(new n(cursor.getString(cursor.getColumnIndex("url")), cursor.getString(cursor.getColumnIndex("method")), cursor.getString(cursor.getColumnIndex("data")), cursor.getString(cursor.getColumnIndex("unitId"))));
                            } catch (Exception e2) {
                                e = e2;
                                try {
                                    e.printStackTrace();
                                    if (cursor != null) {
                                    }
                                    list = list2;
                                    return list;
                                } catch (Throwable th) {
                                    th = th;
                                    if (cursor != null) {
                                    }
                                    throw th;
                                }
                            }
                        }
                        list = list2;
                    }
                } catch (Exception e3) {
                    Exception exc = e3;
                    list2 = null;
                    e = exc;
                    e.printStackTrace();
                    if (cursor != null) {
                    }
                    list = list2;
                    return list;
                }
            }
            if (cursor != null) {
                try {
                    cursor.close();
                } catch (Throwable th2) {
                    throw th2;
                }
            }
        } catch (Exception e4) {
            list2 = null;
            e = e4;
            cursor = null;
            e.printStackTrace();
            if (cursor != null) {
                cursor.close();
            }
            list = list2;
            return list;
        } catch (Throwable th3) {
            Throwable th4 = th3;
            cursor = null;
            th = th4;
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
        return list;
    }
}
