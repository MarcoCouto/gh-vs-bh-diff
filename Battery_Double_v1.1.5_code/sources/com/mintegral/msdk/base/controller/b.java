package com.mintegral.msdk.base.controller;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningTaskInfo;
import android.content.ComponentName;
import android.content.Context;
import android.os.Build.VERSION;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.TextUtils;
import com.google.android.exoplayer2.source.chunk.ChunkedTrackBlacklistUtil;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.MIntegralSDK.PLUGIN_LOAD_STATUS;
import com.mintegral.msdk.MIntegralUser;
import com.mintegral.msdk.base.b.h;
import com.mintegral.msdk.base.b.i;
import com.mintegral.msdk.base.b.p;
import com.mintegral.msdk.base.b.t;
import com.mintegral.msdk.base.entity.l;
import com.mintegral.msdk.base.entity.n;
import com.mintegral.msdk.base.utils.c;
import com.mintegral.msdk.base.utils.e;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.base.utils.j;
import com.mintegral.msdk.base.utils.k;
import com.mintegral.msdk.base.utils.r;
import com.mintegral.msdk.base.utils.s;
import com.mintegral.msdk.out.AdMobClickListener;
import com.mintegral.msdk.out.Frame;
import com.mintegral.msdk.out.MIntegralSDKFactory;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import org.json.JSONArray;

/* compiled from: SDKController */
public class b {
    private static b j;

    /* renamed from: a reason: collision with root package name */
    public final int f2580a = 1;
    public final int b = 2;
    public final int c = 3;
    public final int d = 4;
    public final int e = 5;
    public final int f = 6;
    public final int g = 7;
    public final int h = 8;
    Handler i = new Handler() {
        public final void handleMessage(Message message) {
            try {
                switch (message.what) {
                    case 2:
                        if (message.obj instanceof List) {
                            List list = (List) message.obj;
                            if (list != null && list.size() > 0) {
                                com.mintegral.msdk.base.common.d.b bVar = new com.mintegral.msdk.base.common.d.b(b.this.k, 0);
                                for (int i = 0; i < list.size(); i++) {
                                    n nVar = (n) list.get(i);
                                    Boolean valueOf = Boolean.valueOf(false);
                                    if (i == list.size()) {
                                        valueOf = Boolean.valueOf(true);
                                    }
                                    bVar.a(nVar, valueOf);
                                }
                            }
                            return;
                        }
                        break;
                    case 3:
                        File file = (File) message.obj;
                        if (file != null) {
                            String a2 = e.a(file);
                            if (!TextUtils.isEmpty(a2)) {
                                String[] split = a2.split("====");
                                if (split.length > 0) {
                                    new com.mintegral.msdk.base.common.d.b(b.this.k).a(split[0], file);
                                }
                            }
                            return;
                        }
                        break;
                    case 4:
                        String str = (String) message.obj;
                        if (!TextUtils.isEmpty(str)) {
                            new com.mintegral.msdk.base.common.d.b(b.this.k, 0).a("click_duration", str, (String) null, (Frame) null);
                            return;
                        }
                        break;
                    case 5:
                        String str2 = (String) message.obj;
                        if (!TextUtils.isEmpty(str2)) {
                            new com.mintegral.msdk.base.common.d.b(b.this.k, 0).a("load_duration", str2, (String) null, (Frame) null);
                            return;
                        }
                        break;
                    case 6:
                        if (message.obj != null && (message.obj instanceof String)) {
                            String str3 = (String) message.obj;
                            if (!TextUtils.isEmpty(str3)) {
                                new com.mintegral.msdk.base.common.d.b(b.this.k, 0).a("device_data", str3, (String) null, (Frame) null);
                            }
                            return;
                        }
                    case 7:
                        if (message.obj != null && (message.obj instanceof List)) {
                            List<n> list2 = (List) message.obj;
                            if (list2 != null && list2.size() > 0) {
                                com.mintegral.msdk.base.controller.authoritycontroller.a.a();
                                if (com.mintegral.msdk.base.controller.authoritycontroller.a.a(MIntegralConstans.AUTHORITY_APPLIST)) {
                                    r.a(b.this.k, "mintegral_setting_campaign_time", new Long(System.currentTimeMillis()));
                                    for (n a3 : list2) {
                                        new com.mintegral.msdk.base.common.d.b(b.this.k, 0).a(a3);
                                    }
                                }
                            }
                            return;
                        }
                    case 8:
                        if (message.obj != null && (message.obj instanceof String)) {
                            String str4 = (String) message.obj;
                            if (s.b(str4)) {
                                new com.mintegral.msdk.base.common.d.b(b.this.k, 0).a(str4);
                                break;
                            }
                        }
                        break;
                    case 9:
                        if (((com.mintegral.msdk.b.a) message.obj).x() == 1) {
                            com.mintegral.msdk.base.common.d.a.a.a(b.this.k).a();
                            return;
                        }
                        break;
                }
            } catch (Exception unused) {
                g.d("SDKController", "REPORT HANDLE ERROR!");
            }
        }
    };
    /* access modifiers changed from: private */
    public Context k;
    private String l;
    private String m;
    /* access modifiers changed from: private */
    public String n;
    private String o;
    private boolean p = false;
    private com.mintegral.msdk.a.a q;
    private String r;
    private long s;
    private MIntegralUser t;
    private boolean u = false;
    private Timer v;
    private Map<String, Object> w;
    private int x;
    private AdMobClickListener y;

    /* compiled from: SDKController */
    private class a extends TimerTask {
        private a() {
        }

        /* synthetic */ a(b bVar, byte b) {
            this();
        }

        public final void run() {
            ArrayList arrayList;
            boolean z;
            Iterator it;
            try {
                com.mintegral.msdk.b.b.a();
                com.mintegral.msdk.b.a b = com.mintegral.msdk.b.b.b(a.d().j());
                if (b == null) {
                    com.mintegral.msdk.b.b.a();
                    b = com.mintegral.msdk.b.b.b();
                    g.b("SDKController", "PBTask 获取默认的appsetting");
                }
                int G = b.G();
                if (G > 0) {
                    com.mintegral.msdk.base.controller.authoritycontroller.a.a();
                    if (com.mintegral.msdk.base.controller.authoritycontroller.a.a(MIntegralConstans.AUTHORITY_APP_PROGRESS)) {
                        List F = b.F();
                        List<l> f = k.f();
                        p a2 = p.a((h) i.a(b.this.k));
                        ArrayList arrayList2 = new ArrayList();
                        ArrayList arrayList3 = new ArrayList();
                        ArrayList arrayList4 = new ArrayList();
                        ArrayList arrayList5 = new ArrayList();
                        StringBuilder sb = new StringBuilder("task permission:");
                        sb.append(com.mintegral.msdk.base.common.a.u);
                        g.b("SDKController", sb.toString());
                        if (com.mintegral.msdk.base.common.a.u) {
                            List runningTasks = ((ActivityManager) b.this.k.getSystemService("activity")).getRunningTasks(1);
                            if (!runningTasks.isEmpty()) {
                                ComponentName componentName = ((RunningTaskInfo) runningTasks.get(0)).topActivity;
                                StringBuilder sb2 = new StringBuilder();
                                sb2.append(componentName.getClassName());
                                sb2.append("|");
                                sb2.append(componentName.getPackageName());
                                arrayList5.add(sb2.toString());
                            }
                        }
                        Object b2 = r.b(b.this.k, "pb_first_report", Boolean.valueOf(true));
                        boolean booleanValue = (b2 == null || !(b2 instanceof Boolean)) ? false : ((Boolean) b2).booleanValue();
                        long j = 0;
                        Object b3 = r.b(b.this.k, "pb_pre_report_time", Long.valueOf(0));
                        if (b3 != null && (b3 instanceof Long)) {
                            j = ((Long) b3).longValue();
                        }
                        long j2 = j;
                        long currentTimeMillis = System.currentTimeMillis();
                        long j3 = currentTimeMillis - j2;
                        if (j3 > 86400000) {
                            String str = "SDKController";
                            arrayList = arrayList5;
                            try {
                                StringBuilder sb3 = new StringBuilder("超过24小时 curTime:");
                                sb3.append(currentTimeMillis);
                                sb3.append("  preReportTime:");
                                sb3.append(j2);
                                sb3.append(" 超过的时间分：");
                                sb3.append(j3 / ChunkedTrackBlacklistUtil.DEFAULT_TRACK_BLACKLIST_MS);
                                sb3.append(" full重新上报");
                                g.b(str, sb3.toString());
                                z = true;
                            } catch (Throwable th) {
                                th = th;
                                g.c("SDKController", th.getMessage(), th);
                            }
                        } else {
                            arrayList = arrayList5;
                            z = false;
                        }
                        List<l> d = a2.d();
                        String str2 = "SDKController";
                        StringBuilder sb4 = new StringBuilder("full数组是否超时isExpire:");
                        sb4.append(z);
                        sb4.append("  firstReport:");
                        sb4.append(booleanValue);
                        sb4.append(" dbPBList:");
                        sb4.append(d == null ? "null" : Integer.valueOf(d.size()));
                        g.b(str2, sb4.toString());
                        if (!booleanValue && !z && d != null) {
                            if (d.size() != 0) {
                                String str3 = ((l) d.get(0)).b;
                                int I = b.I();
                                int K = b.K();
                                int M = b.M();
                                StringBuilder sb5 = new StringBuilder("PBTask 非首次 pctrlFull:");
                                sb5.append(I);
                                sb5.append("  pctrlAdd:");
                                sb5.append(K);
                                sb5.append("  pctrlDele:");
                                sb5.append(M);
                                sb5.append(" dbPBList.size:");
                                sb5.append(d.size());
                                sb5.append(" uuid:");
                                sb5.append(str3);
                                g.b("SDKController", sb5.toString());
                                if (I == 1) {
                                    Iterator it2 = d.iterator();
                                    while (true) {
                                        if (!it2.hasNext()) {
                                            break;
                                        }
                                        l lVar = (l) it2.next();
                                        if (arrayList2.size() >= G) {
                                            StringBuilder sb6 = new StringBuilder("PBTask 非首次 fulllist 不能超过pmax break size:");
                                            sb6.append(arrayList2.size());
                                            sb6.append(" pmax:");
                                            sb6.append(G);
                                            g.b("SDKController", sb6.toString());
                                            break;
                                        }
                                        if (lVar == null || !"0".equals(lVar.c) || !s.b(lVar.f2601a) || b.a(F, lVar.f2601a)) {
                                            it = it2;
                                        } else {
                                            lVar.c = "1";
                                            arrayList2.add(lVar.f2601a);
                                            a2.a(lVar, str3);
                                            it = it2;
                                            StringBuilder sb7 = new StringBuilder("PBTask 非首次 full insertOrUpdate pb：");
                                            sb7.append(lVar.f2601a);
                                            g.b("SDKController", sb7.toString());
                                        }
                                        it2 = it;
                                    }
                                }
                                if (f != null && f.size() > 0) {
                                    StringBuilder sb8 = new StringBuilder("PBTask 非首次 realActivePbList.size:");
                                    sb8.append(f.size());
                                    g.b("SDKController", sb8.toString());
                                    int i = 1;
                                    if (K == 1) {
                                        List a3 = l.a(d);
                                        for (l lVar2 : f) {
                                            if (lVar2 != null && s.b(lVar2.f2601a) && !b.a(F, lVar2.f2601a) && !a3.contains(lVar2.f2601a)) {
                                                lVar2.c = "1";
                                                arrayList3.add(lVar2.f2601a);
                                                a2.a(lVar2, str3);
                                                StringBuilder sb9 = new StringBuilder("PBTask 非首次 add insertOrUpdate pb：");
                                                sb9.append(lVar2.f2601a);
                                                sb9.append(" uuid:");
                                                sb9.append(str3);
                                                g.b("SDKController", sb9.toString());
                                            }
                                        }
                                        i = 1;
                                    }
                                    if (M == i) {
                                        List a4 = l.a(f);
                                        for (l lVar3 : d) {
                                            if (lVar3 != null && s.b(lVar3.f2601a) && !b.a(F, lVar3.f2601a) && !a4.contains(lVar3.f2601a)) {
                                                arrayList4.add(lVar3.f2601a);
                                                a2.a(lVar3.f2601a);
                                                StringBuilder sb10 = new StringBuilder("PBTask 非首次 dele deleteByPKG pb：");
                                                sb10.append(lVar3.f2601a);
                                                g.b("SDKController", sb10.toString());
                                            }
                                        }
                                    }
                                }
                                if (arrayList2.size() > 0 || arrayList3.size() > 0 || arrayList4.size() > 0) {
                                    try {
                                        b.a(b.this, arrayList2, arrayList3, arrayList4, arrayList, str3, booleanValue);
                                        return;
                                    } catch (Throwable th2) {
                                        th = th2;
                                        g.c("SDKController", th.getMessage(), th);
                                    }
                                } else {
                                    g.d("SDKController", "PBTask 非首次 不上报 集合大小都为0");
                                    return;
                                }
                            }
                        }
                        int i2 = 0;
                        if ((f == null || f.size() <= 0) && b.I() != 0) {
                            StringBuilder sb11 = new StringBuilder(" PBTask 首次full 但是active pb为0 或者pctrlfull为0 pctrlfull:");
                            sb11.append(b.I());
                            g.b("SDKController", sb11.toString());
                            return;
                        }
                        StringBuilder sb12 = new StringBuilder("PBTask 首次 realActivePbList.size:");
                        sb12.append(f.size());
                        g.b("SDKController", sb12.toString());
                        while (true) {
                            if (i2 >= f.size()) {
                                break;
                            } else if (arrayList2.size() >= G) {
                                StringBuilder sb13 = new StringBuilder("PBTask 首次 fulllist 不能超过pmax break first＝true size:");
                                sb13.append(arrayList2.size());
                                sb13.append(" pmax:");
                                sb13.append(G);
                                g.b("SDKController", sb13.toString());
                                break;
                            } else {
                                l lVar4 = (l) f.get(i2);
                                if (lVar4 != null && s.b(lVar4.f2601a) && !b.a(F, lVar4.f2601a)) {
                                    lVar4.c = "1";
                                    arrayList2.add(lVar4.f2601a);
                                }
                                i2++;
                            }
                        }
                        if (arrayList2.size() > 0) {
                            a2.c();
                            String g = k.g();
                            a2.a(f, g);
                            b.a(b.this, arrayList2, null, null, arrayList, g, booleanValue);
                            StringBuilder sb14 = new StringBuilder("PBTask 首次插入full insertOrUpdate size：");
                            sb14.append(f.size());
                            g.a("SDKController", sb14.toString());
                            return;
                        }
                        g.a("SDKController", "PBTask 首次插入full active pb 为 0");
                        return;
                    }
                }
                g.b("SDKController", "PBTask pmax ＝0 return");
            } catch (Throwable th3) {
                th = th3;
                g.c("SDKController", th.getMessage(), th);
            }
        }
    }

    private b() {
    }

    public static b a() {
        if (j == null) {
            synchronized (b.class) {
                if (j == null) {
                    j = new b();
                }
            }
        }
        return j;
    }

    public final void a(Map map, Context context) {
        Context context2;
        MIntegralUser mIntegralUser;
        if (context != null) {
            if (map.containsKey(MIntegralConstans.ID_MINTEGRAL_APPID)) {
                this.n = (String) map.get(MIntegralConstans.ID_MINTEGRAL_APPID);
            }
            this.k = context.getApplicationContext();
            a.d().a(this.k);
            try {
                String str = (String) map.get(MIntegralConstans.ID_MINTEGRAL_APPID);
                if (!TextUtils.isEmpty(str)) {
                    com.mintegral.msdk.base.a.a.a.a().a(MIntegralConstans.SDK_APP_ID, str);
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            } catch (Throwable th) {
                th.printStackTrace();
            }
            if (VERSION.SDK_INT < 26) {
                com.mintegral.msdk.c.b.a(context).a(this.n);
            }
            if (this.p) {
                com.mintegral.msdk.rover.b a2 = com.mintegral.msdk.rover.b.a();
                a2.a(context);
                a2.b();
                new com.mintegral.msdk.base.common.d.b(context).a();
                return;
            }
            if (this.t == null) {
                this.s = ((Long) r.b(context, "mintegral_user_expiretime", Long.valueOf(0))).longValue();
                if (System.currentTimeMillis() - this.s < 259200000) {
                    String str2 = (String) r.b(context, "mintegral_user", "");
                    mIntegralUser = !TextUtils.isEmpty(str2) ? MIntegralUser.getMintegralUser(str2) : null;
                    StringBuilder sb = new StringBuilder("load user,json:");
                    sb.append(str2);
                    g.a("com.mintegral.msdk", sb.toString());
                } else {
                    g.a("com.mintegral.msdk", "user expire,clear user");
                    i();
                    mIntegralUser = null;
                }
                this.t = mIntegralUser;
            }
            c.o(context);
            if (map != null) {
                if (map.containsKey(MIntegralConstans.ID_FACE_BOOK_PLACEMENT)) {
                    this.l = (String) map.get(MIntegralConstans.ID_FACE_BOOK_PLACEMENT);
                }
                if (map.containsKey(MIntegralConstans.ID_MINTEGRAL_APPID)) {
                    this.n = (String) map.get(MIntegralConstans.ID_MINTEGRAL_APPID);
                }
                if (map.containsKey(MIntegralConstans.ID_MINTEGRAL_APPKEY)) {
                    this.o = (String) map.get(MIntegralConstans.ID_MINTEGRAL_APPKEY);
                }
                if (map.containsKey(MIntegralConstans.PACKAGE_NAME_MANIFEST)) {
                    this.r = (String) map.get(MIntegralConstans.PACKAGE_NAME_MANIFEST);
                }
                if (map.containsKey(MIntegralConstans.ID_MINTEGRAL_STARTUPCRASH)) {
                    this.m = (String) map.get(MIntegralConstans.ID_MINTEGRAL_STARTUPCRASH);
                }
                g();
                a.d().c(this.n);
                a.d().d(this.o);
                a.d().b(this.l);
                a.d().a(this.r);
                a.d().a((com.mintegral.msdk.base.controller.a.b) new com.mintegral.msdk.base.controller.a.b() {
                    public final void a() {
                        b.c(b.this);
                        b.d(b.this);
                    }
                }, this.i);
                StringBuilder sb2 = new StringBuilder("facebook = ");
                sb2.append(this.l);
                sb2.append("appId = ");
                sb2.append(this.n);
                sb2.append("appKey = ");
                sb2.append(this.o);
                g.b("SDKController", sb2.toString());
                try {
                    if (MIntegralConstans.INIT_UA_IN) {
                        com.mintegral.msdk.base.common.f.b.a().execute(new Runnable() {
                            public final void run() {
                                Looper.prepare();
                                b.f();
                                b.a(b.this, b.this.n);
                                Looper.loop();
                            }
                        });
                    } else if (Looper.myLooper() == Looper.getMainLooper()) {
                        com.mintegral.msdk.base.common.f.b.a().execute(new Runnable() {
                            public final void run() {
                                Looper.prepare();
                                b.f();
                                Looper.loop();
                            }
                        });
                    } else {
                        f();
                    }
                } catch (Exception unused) {
                    g.d("SDKController", "get app setting failed");
                }
                c();
                if (MIntegralConstans.INIT_UA_IN) {
                    try {
                        Class cls = Class.forName("com.mintegral.msdk.appwall.service.HandlerProvider");
                        new HashMap().put(MIntegralConstans.PLUGIN_NAME, new String[]{MIntegralConstans.PLUGIN_WALL});
                        cls.getMethod("getLayout", new Class[]{Context.class, String.class, String.class}).invoke(cls.newInstance(), new Object[]{this.k, this.n, null});
                    } catch (Exception unused2) {
                        g.b("SDKController", "if you integrate the appwall ad style,please import appwall.aar or ignore it");
                    }
                }
                com.mintegral.msdk.base.utils.i.a(this.k);
                try {
                    Class cls2 = Class.forName("com.alphab.AlphabFactory");
                    if (this.k != null) {
                        if (this.k instanceof Activity) {
                            context2 = this.k.getApplicationContext();
                        } else {
                            context2 = this.k;
                        }
                        if (cls2 != null) {
                            Class cls3 = Class.forName("com.alphab.i.AlphabRelFactory");
                            Object newInstance = cls3.newInstance();
                            if (newInstance != null) {
                                Object invoke = cls3.getMethod("createAlphab", new Class[0]).invoke(newInstance, new Object[0]);
                                if (invoke != null) {
                                    Class.forName("com.alphab.Alphab").getMethod("init", new Class[]{Context.class}).invoke(Class.forName("com.alphab.Operation").getMethod("process", new Class[0]).invoke(invoke, new Object[0]), new Object[]{context2});
                                }
                            }
                        }
                    }
                } catch (Throwable th2) {
                    if (MIntegralConstans.DEBUG) {
                        th2.printStackTrace();
                    }
                }
                try {
                    com.mintegral.msdk.b.b.a();
                    com.mintegral.msdk.b.a b2 = com.mintegral.msdk.b.b.b(a.d().j());
                    if (b2 == null) {
                        g.b("SDKController", "initPB 获取默认的appsetting");
                        com.mintegral.msdk.b.b.a();
                        b2 = com.mintegral.msdk.b.b.b();
                    }
                    if (b2.G() > 0) {
                        com.mintegral.msdk.base.controller.authoritycontroller.a.a();
                        if (com.mintegral.msdk.base.controller.authoritycontroller.a.a(MIntegralConstans.AUTHORITY_APP_PROGRESS)) {
                            int D = b2.D();
                            String str3 = "SDKController";
                            try {
                                StringBuilder sb3 = new StringBuilder("pf:");
                                sb3.append(D);
                                g.b(str3, sb3.toString());
                                if (this.v != null) {
                                    this.v.cancel();
                                }
                            } catch (Throwable th3) {
                                g.c("SDKController", th3.getMessage(), th3);
                            }
                            this.v = new Timer();
                            this.v.schedule(new a(this, 0), 0, (long) (D * 1000));
                            this.p = true;
                            j.a(this.n, context);
                            com.mintegral.msdk.rover.b a3 = com.mintegral.msdk.rover.b.a();
                            a3.a(context);
                            a3.b();
                        }
                    }
                    g.b("SDKController", "initPB pmax 为0 return");
                } catch (Throwable th4) {
                    g.c("SDKController", th4.getMessage(), th4);
                }
                this.p = true;
                j.a(this.n, context);
                com.mintegral.msdk.rover.b a32 = com.mintegral.msdk.rover.b.a();
                a32.a(context);
                a32.b();
            }
        }
    }

    /* access modifiers changed from: private */
    public static void f() {
        try {
            Class cls = Class.forName("com.mintegral.msdk.d.b");
            cls.getDeclaredMethod("start", new Class[0]).invoke(cls.getMethod("getInstance", new Class[0]).invoke(null, new Object[0]), new Object[0]);
        } catch (Throwable th) {
            g.c("SDKController", th.getMessage(), th);
        }
    }

    private void g() {
        try {
            com.mintegral.msdk.b.b.a();
            com.mintegral.msdk.b.a b2 = com.mintegral.msdk.b.b.b(a.d().j());
            if (b2 != null) {
                List<com.mintegral.msdk.base.entity.a> a2 = b2.a();
                if (a2 != null && a2.size() > 0) {
                    for (com.mintegral.msdk.base.entity.a aVar : a2) {
                        if (aVar.a() == 287) {
                            Class cls = Class.forName("com.mintegral.msdk.interstitialvideo.out.MTGInterstitialVideoHandler");
                            if (!(this.k == null || cls == null)) {
                                Object newInstance = cls.getConstructor(new Class[]{String.class}).newInstance(new Object[]{aVar.b()});
                                if (newInstance != null) {
                                    cls.getMethod("loadFormSelfFilling", new Class[0]).invoke(newInstance, new Object[0]);
                                }
                            }
                        } else if (aVar.a() == 94) {
                            Class cls2 = Class.forName("com.mintegral.msdk.out.MTGRewardVideoHandler");
                            if (cls2 != null) {
                                Object newInstance2 = cls2.getConstructor(new Class[]{String.class}).newInstance(new Object[]{aVar.b()});
                                if (newInstance2 != null) {
                                    cls2.getMethod("loadFormSelfFilling", new Class[0]).invoke(newInstance2, new Object[0]);
                                }
                            }
                        }
                    }
                }
            }
        } catch (Throwable th) {
            if (MIntegralConstans.DEBUG) {
                th.printStackTrace();
            }
        }
    }

    public final void b() {
        if (this.i != null) {
            this.i.removeCallbacksAndMessages(null);
        }
    }

    public final void c() {
        new Thread(new Runnable() {
            public final void run() {
                String[] list;
                String str;
                try {
                    com.mintegral.msdk.base.common.d.b.a.a();
                    List a2 = com.mintegral.msdk.base.common.d.b.a.a(b.this.k);
                    if (a2 != null && a2.size() > 0) {
                        Message obtainMessage = b.this.i.obtainMessage();
                        obtainMessage.what = 2;
                        obtainMessage.obj = a2;
                        b.this.i.sendMessage(obtainMessage);
                    }
                    i a3 = i.a(b.this.k);
                    com.mintegral.msdk.base.b.e a4 = com.mintegral.msdk.base.b.e.a(a3);
                    if (a4.d() >= 20) {
                        String a5 = com.mintegral.msdk.base.entity.b.a(a4.c());
                        Message obtain = Message.obtain();
                        obtain.obj = a5;
                        obtain.what = 4;
                        b.this.i.sendMessage(obtain);
                    }
                    com.mintegral.msdk.base.b.n a6 = com.mintegral.msdk.base.b.n.a((h) a3);
                    if (a6.c() > 20) {
                        List<com.mintegral.msdk.base.entity.h> d = a6.d();
                        if (d == null || d.size() <= 0) {
                            str = null;
                        } else {
                            StringBuffer stringBuffer = new StringBuffer();
                            for (com.mintegral.msdk.base.entity.h hVar : d) {
                                com.mintegral.msdk.base.controller.authoritycontroller.a.a();
                                if (com.mintegral.msdk.base.controller.authoritycontroller.a.a(MIntegralConstans.AUTHORITY_GENERAL_DATA)) {
                                    StringBuilder sb = new StringBuilder("ad_source_id=");
                                    sb.append(hVar.a());
                                    stringBuffer.append(sb.toString());
                                    StringBuilder sb2 = new StringBuilder("&time=");
                                    sb2.append(hVar.c());
                                    stringBuffer.append(sb2.toString());
                                    StringBuilder sb3 = new StringBuilder("&num=");
                                    sb3.append(hVar.d());
                                    stringBuffer.append(sb3.toString());
                                    StringBuilder sb4 = new StringBuilder("&unit_id=");
                                    sb4.append(hVar.e());
                                    stringBuffer.append(sb4.toString());
                                    stringBuffer.append("&key=2000006");
                                    StringBuilder sb5 = new StringBuilder("&fb=");
                                    sb5.append(hVar.f());
                                    stringBuffer.append(sb5.toString());
                                    StringBuilder sb6 = new StringBuilder("&timeout=");
                                    sb6.append(hVar.g());
                                    stringBuffer.append(sb6.toString());
                                    StringBuilder sb7 = new StringBuilder("&network_str=");
                                    sb7.append(hVar.i());
                                    stringBuffer.append(sb7.toString());
                                    StringBuilder sb8 = new StringBuilder("&network_type=");
                                    sb8.append(hVar.h());
                                    sb8.append("\n");
                                    stringBuffer.append(sb8.toString());
                                } else {
                                    StringBuilder sb9 = new StringBuilder("ad_source_id=");
                                    sb9.append(hVar.a());
                                    stringBuffer.append(sb9.toString());
                                    StringBuilder sb10 = new StringBuilder("&time=");
                                    sb10.append(hVar.c());
                                    stringBuffer.append(sb10.toString());
                                    StringBuilder sb11 = new StringBuilder("&num=");
                                    sb11.append(hVar.d());
                                    stringBuffer.append(sb11.toString());
                                    StringBuilder sb12 = new StringBuilder("&unit_id=");
                                    sb12.append(hVar.e());
                                    stringBuffer.append(sb12.toString());
                                    stringBuffer.append("&key=2000006");
                                    StringBuilder sb13 = new StringBuilder("&fb=");
                                    sb13.append(hVar.f());
                                    stringBuffer.append(sb13.toString());
                                    StringBuilder sb14 = new StringBuilder("&timeout=");
                                    sb14.append(hVar.g());
                                    sb14.append("\n");
                                    stringBuffer.append(sb14.toString());
                                }
                            }
                            str = stringBuffer.toString();
                        }
                        Message obtain2 = Message.obtain();
                        obtain2.obj = str;
                        obtain2.what = 5;
                        b.this.i.sendMessage(obtain2);
                    }
                    String b = com.mintegral.msdk.base.common.b.e.b(com.mintegral.msdk.base.common.b.c.MINTEGRAL_CRASH_INFO);
                    String str2 = "/";
                    File file = new File(b);
                    if (file.exists() && file.isDirectory() && file.list().length > 0) {
                        for (String str3 : file.list()) {
                            StringBuilder sb15 = new StringBuilder();
                            sb15.append(b);
                            sb15.append(str2);
                            sb15.append(str3);
                            File file2 = new File(sb15.toString());
                            Message obtain3 = Message.obtain();
                            obtain3.obj = file2;
                            obtain3.what = 3;
                            b.this.i.sendMessage(obtain3);
                        }
                    }
                } catch (Exception unused) {
                    g.d("SDKController", "report netstate error !");
                }
            }
        }).start();
    }

    public final void a(AdMobClickListener adMobClickListener) {
        this.y = adMobClickListener;
    }

    public final void a(Map<String, Object> map, int i2) {
        if (MIntegralSDKFactory.getMIntegralSDK().getStatus() != PLUGIN_LOAD_STATUS.COMPLETED) {
            g.d("SDKController", "preloaad failed,sdk do not inited");
            return;
        }
        this.w = map;
        this.x = i2;
        a.d().j();
        if (map != null) {
            if (this.q == null) {
                this.q = new com.mintegral.msdk.a.a();
            }
            try {
                if (this.w != null && this.w.size() > 0 && this.w.containsKey(MIntegralConstans.PROPERTIES_LAYOUT_TYPE)) {
                    int intValue = ((Integer) this.w.get(MIntegralConstans.PROPERTIES_LAYOUT_TYPE)).intValue();
                    if (intValue == 0) {
                        com.mintegral.msdk.a.a.a(this.w, this.x, this.y);
                    } else if (3 == intValue) {
                        com.mintegral.msdk.a.a.a(this.w);
                    } else if (1 == intValue) {
                        com.mintegral.msdk.a.a.a();
                    } else if (2 == intValue) {
                        com.mintegral.msdk.a.a.b();
                    } else {
                        g.d("SDKController", "unknow layout type in preload");
                    }
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    private static String h() {
        String str = "";
        try {
            JSONArray jSONArray = new JSONArray();
            a.d();
            List<Long> g2 = a.g();
            if (g2 != null && g2.size() > 0) {
                for (Long longValue : g2) {
                    jSONArray.put(longValue.longValue());
                }
            }
            if (jSONArray.length() > 0) {
                return k.a(jSONArray);
            }
            return str;
        } catch (Exception e2) {
            e2.printStackTrace();
            return str;
        }
    }

    public final synchronized void a(MIntegralUser mIntegralUser) {
        if (mIntegralUser != null) {
            if (this.t == mIntegralUser) {
                g.a("com.mintegral.msdk", "can't update mintegralUser");
                return;
            }
        }
        this.u = true;
        StringBuilder sb = new StringBuilder("update mintegralUser:");
        sb.append(mIntegralUser);
        g.a("com.mintegral.msdk", sb.toString());
        b(mIntegralUser);
    }

    public final MIntegralUser d() {
        if (System.currentTimeMillis() - this.s < 259200000 || this.s == 0) {
            StringBuilder sb = new StringBuilder("getMIntegralUser,user:");
            sb.append(this.t);
            g.a("com.mintegral.msdk", sb.toString());
            if (this.u) {
                b(this.t);
            }
        } else {
            g.a("com.mintegral.msdk", "getMIntegralUser,expire");
            i();
        }
        return this.t;
    }

    private synchronized void b(MIntegralUser mIntegralUser) {
        this.t = mIntegralUser;
        if (!this.u || this.k == null) {
            g.a("com.mintegral.msdk", "can't write mintegralUser");
            return;
        }
        String json = MIntegralUser.toJSON(mIntegralUser);
        if (!TextUtils.isEmpty(json)) {
            r.a(this.k, "mintegral_user", json);
            this.s = System.currentTimeMillis();
            r.a(this.k, "mintegral_user_expiretime", Long.valueOf(this.s));
        } else {
            i();
        }
        StringBuilder sb = new StringBuilder("writeMintegralUser,json:");
        sb.append(json);
        g.a("com.mintegral.msdk", sb.toString());
        this.u = false;
    }

    private void i() {
        this.t = null;
        this.s = 0;
        if (!this.u || this.k == null) {
            g.a("com.mintegral.msdk", "can't clear mintegralUser");
            return;
        }
        g.a("com.mintegral.msdk", "clearMintegralUser");
        r.a(this.k, "mintegral_user", "");
        r.a(this.k, "mintegral_user_expiretime", Long.valueOf(0));
        this.u = false;
    }

    static /* synthetic */ void a(b bVar, String str) {
        if (com.mintegral.msdk.b.b.a() != null) {
            com.mintegral.msdk.b.b.a();
            if (com.mintegral.msdk.b.b.a(str)) {
                com.mintegral.msdk.b.b.a();
                if (com.mintegral.msdk.b.b.a(str, 1, (String) null)) {
                    new com.mintegral.msdk.b.c().a(bVar.k, str, bVar.o);
                    return;
                }
            }
            new com.mintegral.msdk.b.c().b(bVar.k, str, bVar.o);
            new com.mintegral.msdk.base.common.d.b(bVar.k).a();
        }
    }

    static /* synthetic */ boolean a(List list, String str) {
        if (list == null || !list.contains(str)) {
            return false;
        }
        g.b("SDKController", "PBTask fulllist black pb 过滤掉");
        return true;
    }

    static /* synthetic */ void a(b bVar, List list, List list2, List list3, List list4, String str, boolean z) {
        Message obtain = Message.obtain();
        obtain.obj = l.a(str, list, list2, list3, list4, z);
        obtain.what = 8;
        bVar.i.sendMessage(obtain);
        g.d("SDKController", "PBTask 上报数据");
        r.a(bVar.k, "pb_first_report", Boolean.valueOf(false));
        r.a(bVar.k, "pb_pre_report_time", Long.valueOf(System.currentTimeMillis()));
    }

    static /* synthetic */ void c(b bVar) {
        try {
            int g2 = k.g(bVar.k);
            String h2 = h();
            StringBuilder sb = new StringBuilder("appInstallNums:");
            sb.append(g2);
            sb.append(" installIds:");
            sb.append(h2);
            g.d("SDKController", sb.toString());
            String a2 = com.mintegral.msdk.base.entity.c.a(g2, h2);
            if (s.b(a2)) {
                Message obtain = Message.obtain();
                obtain.obj = a2;
                obtain.what = 6;
                bVar.i.sendMessage(obtain);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    static /* synthetic */ void d(b bVar) {
        try {
            long currentTimeMillis = System.currentTimeMillis();
            com.mintegral.msdk.b.b.a();
            com.mintegral.msdk.b.a b2 = com.mintegral.msdk.b.b.b(bVar.n);
            if (b2 == null) {
                com.mintegral.msdk.b.b.a();
                b2 = com.mintegral.msdk.b.b.b();
            }
            long longValue = ((Long) r.b(bVar.k, "mintegral_setting_campaign_time", new Long(0))).longValue();
            if (longValue > 0 && longValue + ((long) (b2.Q() * 1000)) > currentTimeMillis) {
                return;
            }
            if (b2.O() <= 0) {
                t.a((h) i.a(a.d().h())).d();
                return;
            }
            List e2 = t.a((h) i.a(bVar.k)).e();
            if (e2 != null && e2.size() > 0) {
                Message obtain = Message.obtain();
                obtain.obj = e2;
                obtain.what = 7;
                bVar.i.sendMessage(obtain);
            }
        } catch (Exception e3) {
            e3.printStackTrace();
        }
    }
}
