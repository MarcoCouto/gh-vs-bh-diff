package com.mintegral.msdk.base.controller.authoritycontroller;

public interface CallBackForDeveloper {
    void onAuthorityInfoBean(AuthorityInfoBean authorityInfoBean);

    void onShowPopWindowStatusFaile(String str);

    void onShowPopWindowStatusSucessful();
}
