package com.mintegral.msdk.base.controller.authoritycontroller;

import android.text.TextUtils;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.b.b;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: SDKAuthorityController */
public class a {
    private static volatile a c;

    /* renamed from: a reason: collision with root package name */
    public ArrayList<String> f2579a = new ArrayList<>();
    public CallBackForDeveloper b;
    private AuthorityInfoBean d = new AuthorityInfoBean();

    private a() {
        try {
            if (com.mintegral.msdk.base.a.a.a.a().a(MIntegralConstans.AUTHORITY_GENERAL_DATA).equals("")) {
                this.d.b(1);
            }
            if (com.mintegral.msdk.base.a.a.a.a().a(MIntegralConstans.AUTHORITY_DEVICE_ID).equals("")) {
                this.d.c(1);
            }
            if (com.mintegral.msdk.base.a.a.a.a().a(MIntegralConstans.AUTHORITY_GPS).equals("")) {
                this.d.d(1);
            }
            if (com.mintegral.msdk.base.a.a.a.a().a(MIntegralConstans.AUTHORITYIMEIMAC).equals("")) {
                this.d.e(1);
            }
            if (com.mintegral.msdk.base.a.a.a.a().a(MIntegralConstans.AUTHORITY_ANDROID_ID).equals("")) {
                this.d.f(1);
            }
            if (com.mintegral.msdk.base.a.a.a.a().a(MIntegralConstans.AUTHORITY_SERIAL_ID).equals("")) {
                this.d.g(1);
            }
            if (com.mintegral.msdk.base.a.a.a.a().a(MIntegralConstans.AUTHORITY_APPLIST).equals("")) {
                this.d.h(1);
            }
            if (com.mintegral.msdk.base.a.a.a.a().a(MIntegralConstans.AUTHORITY_APP_DOWNLOAD).equals("")) {
                this.d.i(1);
            }
            if (com.mintegral.msdk.base.a.a.a.a().a(MIntegralConstans.AUTHORITY_APP_PROGRESS).equals("")) {
                this.d.j(1);
            }
            this.f2579a.add(MIntegralConstans.AUTHORITY_GENERAL_DATA);
            this.f2579a.add(MIntegralConstans.AUTHORITY_DEVICE_ID);
            this.f2579a.add(MIntegralConstans.AUTHORITY_GPS);
            this.f2579a.add(MIntegralConstans.AUTHORITYIMEIMAC);
            this.f2579a.add(MIntegralConstans.AUTHORITY_ANDROID_ID);
            this.f2579a.add(MIntegralConstans.AUTHORITY_APPLIST);
            this.f2579a.add(MIntegralConstans.AUTHORITY_APP_DOWNLOAD);
            this.f2579a.add(MIntegralConstans.AUTHORITY_APP_PROGRESS);
            this.f2579a.add(MIntegralConstans.AUTHORITY_SERIAL_ID);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static a a() {
        if (c == null) {
            synchronized (a.class) {
                if (c == null) {
                    c = new a();
                }
            }
        }
        return c;
    }

    public final void a(String str, int i) {
        if (this.d != null) {
            if (str.equals(MIntegralConstans.AUTHORITY_GENERAL_DATA)) {
                this.d.b(i);
            } else if (str.equals(MIntegralConstans.AUTHORITY_DEVICE_ID)) {
                this.d.c(i);
            } else if (str.equals(MIntegralConstans.AUTHORITY_GPS)) {
                this.d.d(i);
            } else if (str.equals(MIntegralConstans.AUTHORITYIMEIMAC)) {
                this.d.e(i);
            } else if (str.equals(MIntegralConstans.AUTHORITY_ANDROID_ID)) {
                this.d.f(i);
            } else if (str.equals(MIntegralConstans.AUTHORITY_APPLIST)) {
                this.d.h(i);
            } else if (str.equals(MIntegralConstans.AUTHORITY_APP_DOWNLOAD)) {
                this.d.i(i);
            } else if (str.equals(MIntegralConstans.AUTHORITY_APP_PROGRESS)) {
                this.d.j(i);
            } else if (str.equals(MIntegralConstans.AUTHORITY_ALL_INFO)) {
                this.d.a(i);
            } else if (str.equals(MIntegralConstans.AUTHORITY_SERIAL_ID)) {
                this.d.g(i);
            }
        }
    }

    public final AuthorityInfoBean b() {
        if (this.d != null) {
            return this.d;
        }
        return new AuthorityInfoBean().a(1);
    }

    private static int c(String str) {
        try {
            if (!TextUtils.isEmpty(str)) {
                return Integer.parseInt(com.mintegral.msdk.base.a.a.a.a().a(str));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    private static int d(String str) {
        b.a();
        com.mintegral.msdk.b.a b2 = b.b(com.mintegral.msdk.base.controller.a.d().j());
        if (b2 == null) {
            b.a();
            b2 = b.b();
        }
        if (str.equals(MIntegralConstans.AUTHORITY_GENERAL_DATA)) {
            return b2.m();
        }
        if (str.equals(MIntegralConstans.AUTHORITY_DEVICE_ID)) {
            return b2.o();
        }
        if (str.equals(MIntegralConstans.AUTHORITY_GPS)) {
            return b2.aN();
        }
        if (str.equals(MIntegralConstans.AUTHORITYIMEIMAC)) {
            return b2.as();
        }
        if (str.equals(MIntegralConstans.AUTHORITY_ANDROID_ID)) {
            return b2.au();
        }
        if (str.equals(MIntegralConstans.AUTHORITY_APPLIST)) {
            return b2.O();
        }
        if (str.equals(MIntegralConstans.AUTHORITY_APP_DOWNLOAD)) {
            return b2.W();
        }
        if (str.equals(MIntegralConstans.AUTHORITY_APP_PROGRESS)) {
            return b2.G();
        }
        if (str.equals(MIntegralConstans.AUTHORITY_SERIAL_ID)) {
            return b2.k();
        }
        return -1;
    }

    public static boolean a(String str) {
        boolean z;
        b.a();
        com.mintegral.msdk.b.a b2 = b.b(com.mintegral.msdk.base.controller.a.d().j());
        if (b2 == null) {
            b.a();
            b2 = b.b();
        }
        int q = b2.q();
        if (q != 0 ? !(q == 1 && d(str) == 1) : !(c(str) == 1 && d(str) == 1)) {
            z = false;
        } else {
            z = true;
        }
        if (!str.equals(MIntegralConstans.AUTHORITY_APPLIST) && !str.equals(MIntegralConstans.AUTHORITY_APP_PROGRESS)) {
            return z;
        }
        return (c(str) == 1) && (d(str) != 0);
    }

    public static String c() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put(MIntegralConstans.AUTHORITY_GENERAL_DATA, com.mintegral.msdk.base.a.a.a.a().a(MIntegralConstans.AUTHORITY_GENERAL_DATA));
            jSONObject.put(MIntegralConstans.AUTHORITY_DEVICE_ID, com.mintegral.msdk.base.a.a.a.a().a(MIntegralConstans.AUTHORITY_DEVICE_ID));
            jSONObject.put(MIntegralConstans.AUTHORITY_GPS, com.mintegral.msdk.base.a.a.a.a().a(MIntegralConstans.AUTHORITY_GPS));
            jSONObject.put(MIntegralConstans.AUTHORITYIMEIMAC, com.mintegral.msdk.base.a.a.a.a().a(MIntegralConstans.AUTHORITYIMEIMAC));
            jSONObject.put(MIntegralConstans.AUTHORITY_ANDROID_ID, com.mintegral.msdk.base.a.a.a.a().a(MIntegralConstans.AUTHORITY_ANDROID_ID));
            jSONObject.put(MIntegralConstans.AUTHORITY_APPLIST, com.mintegral.msdk.base.a.a.a.a().a(MIntegralConstans.AUTHORITY_APPLIST));
            jSONObject.put(MIntegralConstans.AUTHORITY_APP_DOWNLOAD, com.mintegral.msdk.base.a.a.a.a().a(MIntegralConstans.AUTHORITY_APP_DOWNLOAD));
            jSONObject.put(MIntegralConstans.AUTHORITY_APP_PROGRESS, com.mintegral.msdk.base.a.a.a.a().a(MIntegralConstans.AUTHORITY_APP_PROGRESS));
            return jSONObject.toString();
        } catch (JSONException e) {
            e.printStackTrace();
            return "";
        }
    }

    public final void b(String str) {
        try {
            if (!(this.d == null || str == null)) {
                JSONObject jSONObject = new JSONObject(str);
                this.d.b(Integer.parseInt(jSONObject.getString(MIntegralConstans.AUTHORITY_GENERAL_DATA)));
                this.d.c(Integer.parseInt(jSONObject.getString(MIntegralConstans.AUTHORITY_DEVICE_ID)));
                this.d.d(Integer.parseInt(jSONObject.getString(MIntegralConstans.AUTHORITY_GPS)));
                this.d.e(Integer.parseInt(jSONObject.getString(MIntegralConstans.AUTHORITYIMEIMAC)));
                this.d.f(Integer.parseInt(jSONObject.getString(MIntegralConstans.AUTHORITY_ANDROID_ID)));
                this.d.h(Integer.parseInt(jSONObject.getString(MIntegralConstans.AUTHORITY_APPLIST)));
                this.d.i(Integer.parseInt(jSONObject.getString(MIntegralConstans.AUTHORITY_APP_DOWNLOAD)));
                this.d.j(Integer.parseInt(jSONObject.getString(MIntegralConstans.AUTHORITY_APP_PROGRESS)));
                this.d.g(Integer.parseInt(jSONObject.getString(MIntegralConstans.AUTHORITY_SERIAL_ID)));
                if (this.d != null) {
                    this.b.onAuthorityInfoBean(this.d);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public final String d() {
        JSONArray jSONArray = new JSONArray();
        for (int i = 0; i < this.f2579a.size(); i++) {
            JSONObject jSONObject = new JSONObject();
            try {
                jSONObject.put("keyname", String.valueOf(this.f2579a.get(i)));
                jSONObject.put("client_status", c((String) this.f2579a.get(i)));
                jSONObject.put("server_status", d((String) this.f2579a.get(i)));
                jSONArray.put(jSONObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return jSONArray.toString();
    }

    public final void a(int i) {
        if (this.d != null) {
            this.d.a(i);
        }
    }

    public final boolean e() {
        if (this.d == null) {
            return false;
        }
        int authDeviceIdStatus = this.d.getAuthDeviceIdStatus();
        int authAndroidIdStatus = this.d.getAuthAndroidIdStatus();
        int authGpsStatus = this.d.getAuthGpsStatus();
        int authImeiAndMacStatus = this.d.getAuthImeiAndMacStatus();
        int authGenDataStatus = this.d.getAuthGenDataStatus();
        int authAppListStatus = this.d.getAuthAppListStatus();
        int authAppDownloadStatus = this.d.getAuthAppDownloadStatus();
        int authAppProgressStatus = this.d.getAuthAppProgressStatus();
        if (this.d.getAuthSerialIdStatus() == 1 && authAndroidIdStatus == 1 && authAppDownloadStatus == 1 && authAppListStatus == 1 && authAppProgressStatus == 1 && authDeviceIdStatus == 1 && authGenDataStatus == 1 && authGpsStatus == 1 && authImeiAndMacStatus == 1) {
            return true;
        }
        return false;
    }
}
