package com.mintegral.msdk.base.controller.authoritycontroller;

import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.base.a.a.a;

public class AuthorityInfoBean {
    public int getAuthGenDataStatus() {
        if (!a.a().a(MIntegralConstans.AUTHORITY_GENERAL_DATA).equals("")) {
            return Integer.parseInt(a.a().a(MIntegralConstans.AUTHORITY_GENERAL_DATA));
        }
        return 1;
    }

    public int getAuthDeviceIdStatus() {
        if (!a.a().a(MIntegralConstans.AUTHORITY_DEVICE_ID).equals("")) {
            return Integer.parseInt(a.a().a(MIntegralConstans.AUTHORITY_DEVICE_ID));
        }
        return 1;
    }

    public int getAuthGpsStatus() {
        if (!a.a().a(MIntegralConstans.AUTHORITY_GPS).equals("")) {
            return Integer.parseInt(a.a().a(MIntegralConstans.AUTHORITY_GPS));
        }
        return 1;
    }

    public int getAuthImeiAndMacStatus() {
        if (!a.a().a(MIntegralConstans.AUTHORITYIMEIMAC).equals("")) {
            return Integer.parseInt(a.a().a(MIntegralConstans.AUTHORITYIMEIMAC));
        }
        return 1;
    }

    public int getAuthAndroidIdStatus() {
        if (!a.a().a(MIntegralConstans.AUTHORITY_ANDROID_ID).equals("")) {
            return Integer.parseInt(a.a().a(MIntegralConstans.AUTHORITY_ANDROID_ID));
        }
        return 1;
    }

    public int getAuthSerialIdStatus() {
        if (!a.a().a(MIntegralConstans.AUTHORITY_SERIAL_ID).equals("")) {
            return Integer.parseInt(a.a().a(MIntegralConstans.AUTHORITY_SERIAL_ID));
        }
        return 1;
    }

    public int getAuthAppListStatus() {
        if (!a.a().a(MIntegralConstans.AUTHORITY_APPLIST).equals("")) {
            return Integer.parseInt(a.a().a(MIntegralConstans.AUTHORITY_APPLIST));
        }
        return 1;
    }

    public int getAuthAppDownloadStatus() {
        if (!a.a().a(MIntegralConstans.AUTHORITY_APP_DOWNLOAD).equals("")) {
            return Integer.parseInt(a.a().a(MIntegralConstans.AUTHORITY_APP_DOWNLOAD));
        }
        return 1;
    }

    public int getAuthAppProgressStatus() {
        if (!a.a().a(MIntegralConstans.AUTHORITY_APP_PROGRESS).equals("")) {
            return Integer.parseInt(a.a().a(MIntegralConstans.AUTHORITY_APP_PROGRESS));
        }
        return 1;
    }

    /* access modifiers changed from: protected */
    public final AuthorityInfoBean a(int i) {
        a.a().a(MIntegralConstans.AUTHORITY_GENERAL_DATA, String.valueOf(i));
        a.a().a(MIntegralConstans.AUTHORITY_DEVICE_ID, String.valueOf(i));
        a.a().a(MIntegralConstans.AUTHORITY_GPS, String.valueOf(i));
        a.a().a(MIntegralConstans.AUTHORITYIMEIMAC, String.valueOf(i));
        a.a().a(MIntegralConstans.AUTHORITY_ANDROID_ID, String.valueOf(i));
        a.a().a(MIntegralConstans.AUTHORITY_APPLIST, String.valueOf(i));
        a.a().a(MIntegralConstans.AUTHORITY_APP_DOWNLOAD, String.valueOf(i));
        a.a().a(MIntegralConstans.AUTHORITY_APP_PROGRESS, String.valueOf(i));
        a.a().a(MIntegralConstans.AUTHORITY_SERIAL_ID, String.valueOf(i));
        return this;
    }

    /* access modifiers changed from: protected */
    public final AuthorityInfoBean b(int i) {
        a.a().a(MIntegralConstans.AUTHORITY_GENERAL_DATA, String.valueOf(i));
        return this;
    }

    /* access modifiers changed from: protected */
    public final AuthorityInfoBean c(int i) {
        a.a().a(MIntegralConstans.AUTHORITY_DEVICE_ID, String.valueOf(i));
        return this;
    }

    /* access modifiers changed from: protected */
    public final AuthorityInfoBean d(int i) {
        a.a().a(MIntegralConstans.AUTHORITY_GPS, String.valueOf(i));
        return this;
    }

    /* access modifiers changed from: protected */
    public final AuthorityInfoBean e(int i) {
        a.a().a(MIntegralConstans.AUTHORITYIMEIMAC, String.valueOf(i));
        return this;
    }

    /* access modifiers changed from: protected */
    public final AuthorityInfoBean f(int i) {
        a.a().a(MIntegralConstans.AUTHORITY_ANDROID_ID, String.valueOf(i));
        return this;
    }

    /* access modifiers changed from: protected */
    public final AuthorityInfoBean g(int i) {
        a.a().a(MIntegralConstans.AUTHORITY_SERIAL_ID, String.valueOf(i));
        return this;
    }

    /* access modifiers changed from: protected */
    public final AuthorityInfoBean h(int i) {
        a.a().a(MIntegralConstans.AUTHORITY_APPLIST, String.valueOf(i));
        return this;
    }

    /* access modifiers changed from: protected */
    public final AuthorityInfoBean i(int i) {
        a.a().a(MIntegralConstans.AUTHORITY_APP_DOWNLOAD, String.valueOf(i));
        return this;
    }

    /* access modifiers changed from: protected */
    public final AuthorityInfoBean j(int i) {
        a.a().a(MIntegralConstans.AUTHORITY_APP_PROGRESS, String.valueOf(i));
        return this;
    }
}
