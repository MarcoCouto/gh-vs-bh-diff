package com.mintegral.msdk.base.controller;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import android.os.Message;
import android.os.Parcel;
import android.os.RemoteException;
import android.text.TextUtils;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.ads.identifier.AdvertisingIdClient.Info;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.base.b.h;
import com.mintegral.msdk.base.b.i;
import com.mintegral.msdk.base.b.q;
import com.mintegral.msdk.base.entity.g;
import com.mintegral.msdk.base.entity.m;
import com.mintegral.msdk.base.utils.c;
import com.mintegral.msdk.base.utils.l;
import com.mintegral.msdk.base.utils.r;
import com.mintegral.msdk.base.utils.s;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.LinkedBlockingQueue;
import org.json.JSONArray;

/* compiled from: MTGSDKContext */
public class a {

    /* renamed from: a reason: collision with root package name */
    public static final String f2573a = "a";
    public static List<String> b = new ArrayList();
    private static a c;
    private static CopyOnWriteArraySet<g> j = new CopyOnWriteArraySet<>();
    /* access modifiers changed from: private */
    public Context d;
    private String e;
    /* access modifiers changed from: private */
    public String f;
    private String g;
    private boolean h = false;
    private List<String> i = null;
    private String k;

    /* renamed from: com.mintegral.msdk.base.controller.a$a reason: collision with other inner class name */
    /* compiled from: MTGSDKContext */
    public class C0050a {

        /* renamed from: com.mintegral.msdk.base.controller.a$a$a reason: collision with other inner class name */
        /* compiled from: MTGSDKContext */
        public final class C0051a {
            private final String b;
            private final boolean c;

            C0051a(String str, boolean z) {
                this.b = str;
                this.c = z;
            }

            public final String a() {
                return this.b;
            }
        }

        /* renamed from: com.mintegral.msdk.base.controller.a$a$b */
        /* compiled from: MTGSDKContext */
        private final class b implements ServiceConnection {

            /* renamed from: a reason: collision with root package name */
            boolean f2577a;
            private final LinkedBlockingQueue<IBinder> c;

            public final void onServiceDisconnected(ComponentName componentName) {
            }

            private b() {
                this.f2577a = false;
                this.c = new LinkedBlockingQueue<>(1);
            }

            /* synthetic */ b(C0050a aVar, byte b2) {
                this();
            }

            public final void onServiceConnected(ComponentName componentName, IBinder iBinder) {
                try {
                    this.c.put(iBinder);
                } catch (InterruptedException unused) {
                }
            }

            public final IBinder a() throws InterruptedException {
                if (!this.f2577a) {
                    this.f2577a = true;
                    return (IBinder) this.c.take();
                }
                throw new IllegalStateException();
            }
        }

        /* renamed from: com.mintegral.msdk.base.controller.a$a$c */
        /* compiled from: MTGSDKContext */
        private final class c implements IInterface {
            private IBinder b;

            public c(IBinder iBinder) {
                this.b = iBinder;
            }

            public final IBinder asBinder() {
                return this.b;
            }

            /* JADX INFO: finally extract failed */
            public final String a() throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(AdvertisingInterface.ADVERTISING_ID_SERVICE_INTERFACE_TOKEN);
                    this.b.transact(1, obtain, obtain2, 0);
                    obtain2.readException();
                    String readString = obtain2.readString();
                    obtain2.recycle();
                    obtain.recycle();
                    return readString;
                } catch (Throwable th) {
                    obtain2.recycle();
                    obtain.recycle();
                    throw th;
                }
            }

            public final boolean b() throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                boolean z = false;
                try {
                    obtain.writeInterfaceToken(AdvertisingInterface.ADVERTISING_ID_SERVICE_INTERFACE_TOKEN);
                    obtain.writeInt(1);
                    this.b.transact(2, obtain, obtain2, 0);
                    obtain2.readException();
                    if (obtain2.readInt() != 0) {
                        z = true;
                    }
                } catch (Throwable th) {
                    obtain2.recycle();
                    obtain.recycle();
                    throw th;
                }
                obtain2.recycle();
                obtain.recycle();
                return z;
            }
        }

        public C0050a() {
        }

        public final C0051a a(Context context) throws Exception {
            if (Looper.myLooper() != Looper.getMainLooper()) {
                try {
                    context.getPackageManager().getPackageInfo("com.android.vending", 0);
                    b bVar = new b(this, 0);
                    Intent intent = new Intent(AdvertisingInfoServiceStrategy.GOOGLE_PLAY_SERVICES_INTENT);
                    intent.setPackage("com.google.android.gms");
                    if (context.bindService(intent, bVar, 1)) {
                        try {
                            c cVar = new c(bVar.a());
                            C0051a aVar = new C0051a(cVar.a(), cVar.b());
                            context.unbindService(bVar);
                            return aVar;
                        } catch (Exception e) {
                            throw e;
                        } catch (Throwable th) {
                            context.unbindService(bVar);
                            throw th;
                        }
                    } else {
                        throw new IOException("Google Play connection failed");
                    }
                } catch (Exception e2) {
                    throw e2;
                }
            } else {
                throw new IllegalStateException("Cannot be called from the main thread");
            }
        }
    }

    /* compiled from: MTGSDKContext */
    public interface b {
        void a();
    }

    public final String a() {
        try {
            if (this.d != null) {
                return this.d.getPackageName();
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return null;
    }

    public final List<String> b() {
        return this.i;
    }

    public final void a(String str) {
        try {
            this.k = str;
            if (!TextUtils.isEmpty(str) && this.d != null) {
                r.a(this.d, "applicationIds", str);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public static Set<g> c() {
        return j;
    }

    private a() {
    }

    public static a d() {
        if (c == null) {
            synchronized (a.class) {
                if (c == null) {
                    c = new a();
                }
            }
        }
        return c;
    }

    public final void a(final b bVar, final Handler handler) {
        if (!this.h) {
            String str = "-1";
            try {
                Object b2 = r.b(this.d, "ga_id", str);
                if (b2 != null && (b2 instanceof String)) {
                    String str2 = (String) b2;
                    if (s.b(str2) && !str.equals(str2)) {
                        String str3 = f2573a;
                        StringBuilder sb = new StringBuilder("sp init gaid:");
                        sb.append(str2);
                        com.mintegral.msdk.base.utils.g.b(str3, sb.toString());
                        com.mintegral.msdk.base.controller.authoritycontroller.a.a();
                        if (com.mintegral.msdk.base.controller.authoritycontroller.a.a(MIntegralConstans.AUTHORITY_DEVICE_ID)) {
                            c.a(str2);
                        }
                    }
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
            new Thread(new Runnable() {
                public final void run() {
                    com.mintegral.msdk.base.controller.authoritycontroller.a.a();
                    if (com.mintegral.msdk.base.controller.authoritycontroller.a.a(MIntegralConstans.AUTHORITY_DEVICE_ID)) {
                        try {
                            Class.forName("com.google.android.gms.ads.identifier.AdvertisingIdClient");
                            Info advertisingIdInfo = AdvertisingIdClient.getAdvertisingIdInfo(a.this.d);
                            c.a(advertisingIdInfo.getId());
                            a.a(a.this, advertisingIdInfo.getId());
                        } catch (Exception unused) {
                            com.mintegral.msdk.base.utils.g.c(a.f2573a, "GET ADID ERROR TRY TO GET FROM GOOGLE PLAY APP");
                            try {
                                C0051a a2 = new C0050a().a(a.this.d);
                                c.a(a2.a());
                                a.a(a.this, a2.a());
                            } catch (Exception unused2) {
                                com.mintegral.msdk.base.utils.g.c(a.f2573a, "GET ADID FROM GOOGLE PLAY APP ERROR");
                            }
                        } catch (Throwable th) {
                            th.printStackTrace();
                        }
                    }
                    try {
                        com.mintegral.msdk.b.b.a();
                        com.mintegral.msdk.b.a b2 = com.mintegral.msdk.b.b.b(a.d().j());
                        if (b2 == null) {
                            com.mintegral.msdk.b.b.a();
                            b2 = com.mintegral.msdk.b.b.b();
                        }
                        Message obtain = Message.obtain();
                        obtain.obj = b2;
                        obtain.what = 9;
                        handler.sendMessage(obtain);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try {
                        c.a(a.this.d);
                        a.e();
                        com.mintegral.msdk.b.b.a(a.this.d, a.this.f);
                        a.this.l();
                        a.c(a.this);
                        a.this.a(bVar);
                    } catch (Exception unused3) {
                    }
                }
            }).start();
        }
    }

    public static synchronized void e() {
        synchronized (a.class) {
            try {
                if (!TextUtils.isEmpty(d().j())) {
                    com.mintegral.msdk.b.b.a();
                    com.mintegral.msdk.b.a b2 = com.mintegral.msdk.b.b.b(d().j());
                    if (b2 != null) {
                        String aP = b2.aP();
                        if (!TextUtils.isEmpty(aP)) {
                            String c2 = com.mintegral.msdk.base.utils.a.c(aP);
                            if (!TextUtils.isEmpty(c2)) {
                                JSONArray jSONArray = new JSONArray(c2);
                                if (jSONArray.length() > 0) {
                                    ArrayList arrayList = new ArrayList();
                                    for (int i2 = 0; i2 < jSONArray.length(); i2++) {
                                        arrayList.add(jSONArray.optString(i2));
                                    }
                                    d().i = arrayList;
                                }
                            }
                        }
                    }
                }
            } catch (Exception unused) {
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00a1, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x001b, code lost:
        return;
     */
    public final synchronized void a(b bVar) {
        try {
            CopyOnWriteArraySet<g> a2 = l.a(this.d).a(this.f);
            j = a2;
            if (a2.size() != 0) {
                CopyOnWriteArraySet copyOnWriteArraySet = new CopyOnWriteArraySet();
                Iterator it = j.iterator();
                if (it != null) {
                    while (it.hasNext()) {
                        try {
                            g gVar = (g) it.next();
                            if (!(b == null || b.size() <= 0 || gVar == null)) {
                                for (int i2 = 0; i2 < b.size(); i2++) {
                                    String str = (String) b.get(i2);
                                    String b2 = gVar.b();
                                    if (!TextUtils.isEmpty(str) && !TextUtils.isEmpty(b2) && str.equals(b2)) {
                                        copyOnWriteArraySet.add(gVar);
                                    }
                                }
                            }
                        } catch (Exception e2) {
                            e2.printStackTrace();
                            com.mintegral.msdk.base.utils.g.d(f2573a, "remove list error");
                        }
                    }
                }
                if (j != null) {
                    j.clear();
                }
                if (copyOnWriteArraySet.size() > 0) {
                    j.addAll(copyOnWriteArraySet);
                }
                l.a(this.d).a((Set<g>) j);
                if (bVar != null) {
                    bVar.a();
                }
            } else if (bVar != null) {
                bVar.a();
            }
        } catch (Throwable unused) {
        }
    }

    public final void f() {
        try {
            if (j != null && j.size() > 0) {
                l.a(this.d).a((Set<g>) j);
            }
        } catch (Throwable unused) {
        }
    }

    public static List<Long> g() {
        try {
            if (j != null && j.size() > 0) {
                Iterator it = j.iterator();
                ArrayList arrayList = new ArrayList();
                while (it.hasNext()) {
                    g gVar = (g) it.next();
                    if (!arrayList.contains(gVar.a())) {
                        arrayList.add(Long.valueOf(Long.parseLong(gVar.a())));
                    }
                }
                return arrayList;
            }
        } catch (NumberFormatException e2) {
            e2.printStackTrace();
        } catch (Throwable unused) {
        }
        return null;
    }

    public final Context h() {
        return this.d;
    }

    public final void a(Context context) {
        this.d = context;
    }

    public final String i() {
        return this.e;
    }

    public final void b(String str) {
        this.e = str;
    }

    public final String j() {
        try {
            if (!TextUtils.isEmpty(this.f)) {
                return this.f;
            }
            if (this.d != null) {
                return (String) r.b(this.d, "sp_appId", "");
            }
            return "";
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public final void c(String str) {
        try {
            this.f = str;
            if (!TextUtils.isEmpty(str) && this.d != null) {
                r.a(this.d, "sp_appId", str);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public final String k() {
        if (!TextUtils.isEmpty(this.g)) {
            return this.g;
        }
        if (this.d != null) {
            return (String) r.b(this.d, "sp_appKey", "");
        }
        return null;
    }

    public final void d(String str) {
        try {
            this.g = str;
            if (!TextUtils.isEmpty(str) && this.d != null) {
                r.a(this.d, "sp_appKey", str);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public final List<String> l() {
        try {
            List<String> list = d().i;
            if (b != null && b.size() > 0) {
                return b;
            }
            List installedPackages = this.d.getPackageManager().getInstalledPackages(0);
            for (int i2 = 0; i2 < installedPackages.size(); i2++) {
                if ((((PackageInfo) installedPackages.get(i2)).applicationInfo.flags & 1) <= 0) {
                    b.add(((PackageInfo) installedPackages.get(i2)).packageName);
                } else if (list != null && list.size() > 0 && list.contains(((PackageInfo) installedPackages.get(i2)).packageName)) {
                    b.add(((PackageInfo) installedPackages.get(i2)).packageName);
                }
            }
            return b;
        } catch (Exception unused) {
            com.mintegral.msdk.base.utils.g.d(f2573a, "get package info list error");
            return null;
        }
    }

    static /* synthetic */ void a(a aVar, String str) {
        try {
            if (s.b(str)) {
                String str2 = f2573a;
                StringBuilder sb = new StringBuilder("saveGAID gaid:");
                sb.append(str);
                com.mintegral.msdk.base.utils.g.b(str2, sb.toString());
                r.a(aVar.d, "ga_id", str);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    static /* synthetic */ void c(a aVar) {
        ArrayList arrayList;
        try {
            long currentTimeMillis = System.currentTimeMillis();
            if (b != null && b.size() > 0) {
                ArrayList<m> arrayList2 = new ArrayList<>();
                PackageManager packageManager = aVar.d.getPackageManager();
                for (String str : b) {
                    if (!TextUtils.isEmpty(str)) {
                        m mVar = new m();
                        mVar.a(currentTimeMillis);
                        mVar.a(str);
                        mVar.a(1);
                        if (packageManager != null) {
                            mVar.b(packageManager.getInstallerPackageName(str));
                        }
                        arrayList2.add(mVar);
                    }
                }
                if (arrayList2.size() > 0) {
                    List<m> c2 = q.a((h) i.a(aVar.d)).c();
                    if (c2 != null) {
                        arrayList = new ArrayList(c2);
                    } else {
                        arrayList = new ArrayList();
                    }
                    if (c2 == null || c2.size() <= 0) {
                        q.a((h) i.a(aVar.d)).a((List<m>) arrayList2);
                    } else {
                        boolean z = false;
                        for (m mVar2 : c2) {
                            boolean z2 = false;
                            for (m mVar3 : arrayList2) {
                                if (mVar3.a().equals(mVar2.a())) {
                                    mVar3.a(4);
                                    z2 = true;
                                }
                            }
                            if (!(z2 || mVar2.b() == 0 || mVar2.b() == 1)) {
                                m mVar4 = new m();
                                mVar4.a(2);
                                mVar4.a(mVar2.a());
                                mVar4.a(currentTimeMillis);
                                mVar4.b(mVar2.d());
                                arrayList.add(mVar4);
                                z = true;
                            }
                        }
                        for (m mVar5 : arrayList2) {
                            if (mVar5.b() != 4) {
                                m mVar6 = new m();
                                mVar6.a(3);
                                mVar6.a(mVar5.a());
                                mVar6.a(currentTimeMillis);
                                mVar6.b(mVar5.d());
                                arrayList.add(mVar6);
                                z = true;
                            }
                        }
                        if (z) {
                            q.a((h) i.a(aVar.d)).a((List<m>) arrayList);
                        }
                    }
                }
            }
        } catch (Exception e2) {
            if (MIntegralConstans.DEBUG) {
                e2.printStackTrace();
            }
        }
    }
}
