package com.mintegral.msdk.interstitial.d;

import android.support.v4.app.NotificationCompat;
import com.mintegral.msdk.base.common.net.a.b;
import com.mintegral.msdk.base.entity.CampaignUnit;
import com.mintegral.msdk.base.utils.g;
import org.json.JSONObject;

/* compiled from: InterstitialResponseHandler */
public abstract class c extends b {

    /* renamed from: a reason: collision with root package name */
    private int f2685a;
    private long b;

    public abstract void b(int i, String str);

    public abstract void b(CampaignUnit campaignUnit);

    public final /* synthetic */ void a(Object obj) {
        JSONObject jSONObject = (JSONObject) obj;
        if (this.f2685a == 0) {
            StringBuilder sb = new StringBuilder("content = ");
            sb.append(jSONObject);
            g.d("", sb.toString());
            int optInt = jSONObject.optInt("status");
            if (1 == optInt) {
                System.currentTimeMillis();
                CampaignUnit parseCampaignUnit = CampaignUnit.parseCampaignUnit(jSONObject.optJSONObject("data"));
                if (parseCampaignUnit == null || parseCampaignUnit.getAds() == null || parseCampaignUnit.getAds().size() <= 0) {
                    b(optInt, jSONObject.optString(NotificationCompat.CATEGORY_MESSAGE));
                } else {
                    b(parseCampaignUnit);
                }
            } else {
                b(optInt, jSONObject.optString(NotificationCompat.CATEGORY_MESSAGE));
            }
        } else {
            if (this.f2685a == 1) {
                StringBuilder sb2 = new StringBuilder("content frames = ");
                sb2.append(jSONObject);
                g.d("", sb2.toString());
                int optInt2 = jSONObject.optInt("status");
                if (1 == optInt2) {
                    System.currentTimeMillis();
                    CampaignUnit parseCampaignUnit2 = CampaignUnit.parseCampaignUnit(jSONObject.optJSONObject("data"));
                    if (parseCampaignUnit2 == null || parseCampaignUnit2.getListFrames() == null || parseCampaignUnit2.getListFrames().size() <= 0) {
                        b(optInt2, jSONObject.optString(NotificationCompat.CATEGORY_MESSAGE));
                    } else {
                        parseCampaignUnit2.getListFrames();
                    }
                } else {
                    b(optInt2, jSONObject.optString(NotificationCompat.CATEGORY_MESSAGE));
                }
            }
        }
    }

    public final void a() {
        super.a();
        this.b = System.currentTimeMillis();
    }

    public final void a(int i) {
        StringBuilder sb = new StringBuilder("errorCode = ");
        sb.append(i);
        g.d("", sb.toString());
        b(i, c(i));
    }
}
