package com.mintegral.msdk.interstitial.d;

import com.mintegral.msdk.base.entity.CampaignUnit;

/* compiled from: InterstitialLoadVideoResponseHandler */
public abstract class b extends c {
    public abstract void a(int i, String str);

    public abstract void a(CampaignUnit campaignUnit);

    public final void b(CampaignUnit campaignUnit) {
        a(campaignUnit);
    }

    public final void b(int i, String str) {
        a(i, str);
    }
}
