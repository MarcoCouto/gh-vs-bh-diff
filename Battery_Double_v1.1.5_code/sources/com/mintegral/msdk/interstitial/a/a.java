package com.mintegral.msdk.interstitial.a;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.TextUtils;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.b.d;
import com.mintegral.msdk.base.b.h;
import com.mintegral.msdk.base.b.i;
import com.mintegral.msdk.base.b.m;
import com.mintegral.msdk.base.common.b.c;
import com.mintegral.msdk.base.common.b.e;
import com.mintegral.msdk.base.common.net.l;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.entity.CampaignUnit;
import com.mintegral.msdk.base.entity.f;
import com.mintegral.msdk.base.utils.CommonMD5;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.base.utils.k;
import com.mintegral.msdk.base.utils.n;
import com.mintegral.msdk.interstitial.c.a.C0056a;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;

/* compiled from: IntersAdapter */
public final class a {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public Context f2673a;
    /* access modifiers changed from: private */
    public String b;
    private int c;
    /* access modifiers changed from: private */
    public boolean d;
    private int e;
    /* access modifiers changed from: private */
    public String f;
    /* access modifiers changed from: private */
    public String g;
    private String h;
    /* access modifiers changed from: private */
    public C0056a i;
    private d j;
    /* access modifiers changed from: private */
    public Handler k;
    /* access modifiers changed from: private */
    public b l;
    /* access modifiers changed from: private */
    public boolean m = false;
    /* access modifiers changed from: private */
    public boolean n = false;

    /* renamed from: com.mintegral.msdk.interstitial.a.a$a reason: collision with other inner class name */
    /* compiled from: IntersAdapter */
    public class C0055a implements Runnable {
        public C0055a() {
        }

        public final void run() {
            String str = "IntersAdapter";
            try {
                StringBuilder sb = new StringBuilder("=====getTtcRunnable 开始获取 mTtcIds:");
                sb.append(a.this.f);
                sb.append("  mExcludes:");
                sb.append(a.this.g);
                g.b(str, sb.toString());
                if (a.this.f2673a != null) {
                    i a2 = i.a(a.this.f2673a);
                    if (a2 != null) {
                        com.mintegral.msdk.base.b.d a3 = com.mintegral.msdk.base.b.d.a((h) a2);
                        a3.c();
                        a.this.f = a3.a(a.this.b);
                    }
                }
                a.this.g = a.g();
                StringBuilder sb2 = new StringBuilder("=====getTtcRunnable 获取完毕 mTtcIds:");
                sb2.append(a.this.f);
                sb2.append("  mExcludes:");
                sb2.append(a.this.g);
                g.b("IntersAdapter", sb2.toString());
                if (a.this.n) {
                    StringBuilder sb3 = new StringBuilder("=====getTtcRunnable 获取ttcid和excludeids超时 mIsGetTtcExcIdsTimeout：");
                    sb3.append(a.this.n);
                    sb3.append(" mIsGetTtcExcIdsSuccess:");
                    sb3.append(a.this.m);
                    g.b("IntersAdapter", sb3.toString());
                    return;
                }
                StringBuilder sb4 = new StringBuilder("=====getTtcRunnable 获取ttcid和excludeids没有超时 mIsGetTtcExcIdsTimeout:");
                sb4.append(a.this.n);
                sb4.append(" mIsGetTtcExcIdsSuccess:");
                sb4.append(a.this.m);
                g.b("IntersAdapter", sb4.toString());
                if (a.this.l != null) {
                    g.b("IntersAdapter", "=====getTtcRunnable 删除 获取ttcid的超时任务");
                    a.this.k.removeCallbacks(a.this.l);
                }
                a.this.m = true;
                StringBuilder sb5 = new StringBuilder("=====getTtcRunnable 给handler发送消息 mTtcIds:");
                sb5.append(a.this.f);
                sb5.append("  mExcludes:");
                sb5.append(a.this.g);
                g.b("IntersAdapter", sb5.toString());
                if (a.this.k != null) {
                    a.this.k.sendEmptyMessage(1);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /* compiled from: IntersAdapter */
    public class b implements Runnable {
        public b() {
        }

        public final void run() {
            String str = "IntersAdapter";
            try {
                StringBuilder sb = new StringBuilder("=====超时task 开始执行 mTtcIds:");
                sb.append(a.this.f);
                sb.append("  mExcludes:");
                sb.append(a.this.g);
                g.b(str, sb.toString());
                if (a.this.m) {
                    StringBuilder sb2 = new StringBuilder("超时task 已经成功获取ttcid excludeids mIsGetTtcExcIdsTimeout:");
                    sb2.append(a.this.n);
                    sb2.append(" mIsGetTtcExcIdsSuccess:");
                    sb2.append(a.this.m);
                    sb2.append("超时task不做处理");
                    g.b("IntersAdapter", sb2.toString());
                    return;
                }
                StringBuilder sb3 = new StringBuilder("获取ttcid excludeids超时 mIsGetTtcExcIdsTimeout:");
                sb3.append(a.this.n);
                sb3.append(" mIsGetTtcExcIdsSuccess:");
                sb3.append(a.this.m);
                g.b("IntersAdapter", sb3.toString());
                a.this.n = true;
                if (a.this.k != null) {
                    a.this.k.sendEmptyMessage(2);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public a(Context context, String str, String str2, boolean z) {
        this.f2673a = context;
        this.b = str;
        this.h = str2;
        this.d = z;
        String j2 = com.mintegral.msdk.base.controller.a.d().j();
        com.mintegral.msdk.b.b.a();
        this.j = com.mintegral.msdk.b.b.c(j2, str);
        if (this.j == null) {
            g.b("IntersAdapter", "获取默认的unitsetting");
            this.j = d.e(this.b);
        }
        this.k = new Handler(Looper.getMainLooper()) {
            public final void handleMessage(Message message) {
                try {
                    switch (message.what) {
                        case 1:
                            StringBuilder sb = new StringBuilder("handler id获取成功 开始load mTtcIds:");
                            sb.append(a.this.f);
                            sb.append("  mExcludes:");
                            sb.append(a.this.g);
                            g.b("IntersAdapter", sb.toString());
                            a.this.c();
                            return;
                        case 2:
                            StringBuilder sb2 = new StringBuilder("handler id获取超时  开始load mTtcIds:");
                            sb2.append(a.this.f);
                            sb2.append("  mExcludes:");
                            sb2.append(a.this.g);
                            g.b("IntersAdapter", sb2.toString());
                            a.this.c();
                            return;
                        case 3:
                            if (a.this.i != null) {
                                g.b("IntersAdapter", "handler 数据load成功");
                                a.this.i.a(a.this.d);
                                return;
                            }
                            break;
                        case 4:
                            if (a.this.i != null) {
                                g.b("IntersAdapter", "handler 数据load失败");
                                if (message.obj != null && (message.obj instanceof String)) {
                                    a.this.i.a(a.this.d, (String) message.obj);
                                    break;
                                }
                            }
                            break;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
    }

    public final CampaignEx a() {
        try {
            if (TextUtils.isEmpty(this.b)) {
                return null;
            }
            h();
            List i2 = i();
            if (i2 != null) {
                if (i2.size() > 0) {
                    int i3 = 0;
                    while (i3 < i2.size()) {
                        CampaignEx campaignEx = (CampaignEx) i2.get(i3);
                        StringBuilder sb = new StringBuilder("html url:");
                        sb.append(campaignEx.getHtmlUrl());
                        g.a("IntersAdapter", sb.toString());
                        if (campaignEx == null || (TextUtils.isEmpty(campaignEx.getHtmlUrl()) && TextUtils.isEmpty(campaignEx.getMraid()))) {
                            i3++;
                        } else {
                            StringBuilder sb2 = new StringBuilder("adapter htmlurl:");
                            sb2.append(campaignEx.getHtmlUrl());
                            sb2.append(" id:");
                            sb2.append(campaignEx.getId());
                            g.b("IntersAdapter", sb2.toString());
                            return campaignEx;
                        }
                    }
                    return null;
                }
            }
            g.b("IntersAdapter", "adapter allCamp is null");
            return null;
        } catch (Exception e2) {
            e2.printStackTrace();
            g.d("IntersAdapter", "==getIntersAvaCampaign 获取campaign 出错");
        }
    }

    public final void b() {
        if (this.f2673a == null) {
            a("context is null");
        } else if (TextUtils.isEmpty(this.b)) {
            a("unitid is null");
        } else if (this.j == null) {
            a("unitSetting is null please call load");
        } else {
            int q = this.j.q();
            if (q <= 0) {
                StringBuilder sb = new StringBuilder("aqn为-1和0 不请求 直接返回失败 apiRepNum：");
                sb.append(q);
                g.b("IntersAdapter", sb.toString());
                a("controller don't request ad");
                return;
            }
            g.b("IntersAdapter", "load 开始清除过期数据");
            h();
            List i2 = i();
            if (i2 == null || i2.size() <= 0) {
                new Thread(new C0055a()).start();
                if (this.k != null) {
                    this.l = new b();
                    this.k.postDelayed(this.l, 90000);
                    return;
                }
                g.b("IntersAdapter", "handler 为空 直接load");
                c();
                return;
            }
            StringBuilder sb2 = new StringBuilder("load 本地已有缓存数量：");
            sb2.append(i2.size());
            g.b("IntersAdapter", sb2.toString());
            f();
        }
    }

    public final void c() {
        try {
            if (this.f2673a == null) {
                a("context is null");
            } else if (TextUtils.isEmpty(this.b)) {
                a("unitid is null");
            } else if (this.j == null) {
                a("unitSetting is null please call load");
            } else {
                g.b("IntersAdapter", "load 开始准备请求参数");
                String j2 = com.mintegral.msdk.base.controller.a.d().j();
                StringBuilder sb = new StringBuilder();
                sb.append(com.mintegral.msdk.base.controller.a.d().j());
                sb.append(com.mintegral.msdk.base.controller.a.d().k());
                String md5 = CommonMD5.getMD5(sb.toString());
                int i2 = this.d ? 3 : 2;
                int i3 = 1;
                this.e = 1;
                if (this.j.r() > 0) {
                    this.e = this.j.r();
                }
                if (this.j.q() > 0) {
                    i3 = this.j.q();
                }
                String str = "1";
                String str2 = "1";
                String str3 = this.g;
                String str4 = this.f;
                String n2 = n();
                String o = o();
                this.c = k();
                String m2 = m();
                if (TextUtils.isEmpty(this.h)) {
                    this.h = "0";
                }
                l lVar = new l();
                n.a(lVar, "app_id", j2);
                n.a(lVar, MIntegralConstans.PROPERTIES_UNIT_ID, this.b);
                n.a(lVar, "sign", md5);
                n.a(lVar, "category", this.h);
                n.a(lVar, "req_type", String.valueOf(i2));
                n.a(lVar, "ad_num", String.valueOf(i3));
                StringBuilder sb2 = new StringBuilder();
                sb2.append(this.e);
                n.a(lVar, "tnum", sb2.toString());
                n.a(lVar, "only_impression", str);
                n.a(lVar, "ping_mode", str2);
                n.a(lVar, "ttc_ids", str4);
                n.a(lVar, "display_cids", n2);
                n.a(lVar, "exclude_ids", str3);
                n.a(lVar, "install_ids", o);
                n.a(lVar, CampaignEx.JSON_KEY_AD_SOURCE_ID, "1");
                n.a(lVar, "session_id", m2);
                n.a(lVar, "ad_type", "279");
                StringBuilder sb3 = new StringBuilder();
                sb3.append(this.c);
                n.a(lVar, "offset", sb3.toString());
                new com.mintegral.msdk.interstitial.d.a(this.f2673a).a(com.mintegral.msdk.base.common.a.k, lVar, (com.mintegral.msdk.base.common.net.d<?>) new com.mintegral.msdk.interstitial.d.b() {
                    public final void a(CampaignUnit campaignUnit) {
                        try {
                            g.b("IntersAdapter", "onLoadCompaginSuccess 数据刚请求回来");
                            a.a(a.this, campaignUnit);
                        } catch (Exception e) {
                            e.printStackTrace();
                            g.b("IntersAdapter", "onLoadCompaginSuccess 数据刚请求失败");
                            a.this.a("can't show because unknow error");
                            a.this.l();
                        }
                    }

                    public final void a(int i, String str) {
                        g.d("IntersAdapter", str);
                        StringBuilder sb = new StringBuilder("onLoadCompaginFailed load失败 errorCode:");
                        sb.append(i);
                        sb.append(" msg:");
                        sb.append(str);
                        g.b("IntersAdapter", sb.toString());
                        a.this.a(str);
                        a.this.l();
                    }
                });
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            a("can't show because unknow error");
            l();
        }
    }

    /* access modifiers changed from: private */
    public void f() {
        if (this.k != null) {
            this.k.sendEmptyMessage(3);
        }
    }

    /* access modifiers changed from: private */
    public void a(String str) {
        try {
            if (this.k != null) {
                Message obtain = Message.obtain();
                obtain.obj = str;
                obtain.what = 4;
                this.k.sendMessage(obtain);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    public static String g() {
        String str = "";
        try {
            com.mintegral.msdk.b.b.a();
            com.mintegral.msdk.b.a b2 = com.mintegral.msdk.b.b.b(com.mintegral.msdk.base.controller.a.d().j());
            JSONArray jSONArray = new JSONArray();
            if (b2 != null && b2.aK() == 1) {
                StringBuilder sb = new StringBuilder("excludes cfc:");
                sb.append(b2.aK());
                g.b("IntersAdapter", sb.toString());
                long[] c2 = m.a((h) i.a(com.mintegral.msdk.base.controller.a.d().h())).c();
                if (c2 != null) {
                    for (long j2 : c2) {
                        StringBuilder sb2 = new StringBuilder("excludes campaignIds:");
                        sb2.append(c2);
                        g.b("IntersAdapter", sb2.toString());
                        jSONArray.put(j2);
                    }
                }
            }
            if (jSONArray.length() > 0) {
                str = k.a(jSONArray);
            }
            StringBuilder sb3 = new StringBuilder("get excludes:");
            sb3.append(str);
            g.b("IntersAdapter", sb3.toString());
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return str;
    }

    private void h() {
        try {
            if (com.mintegral.msdk.interstitial.b.a.a() != null) {
                com.mintegral.msdk.b.b.a();
                com.mintegral.msdk.b.a b2 = com.mintegral.msdk.b.b.b(com.mintegral.msdk.base.controller.a.d().j());
                if (b2 == null) {
                    com.mintegral.msdk.b.b.a();
                    b2 = com.mintegral.msdk.b.b.b();
                }
                com.mintegral.msdk.interstitial.b.a.a().a(b2.aF() * 1000, this.b);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    private List<CampaignEx> i() {
        try {
            if (com.mintegral.msdk.interstitial.b.a.a() != null) {
                return com.mintegral.msdk.interstitial.b.a.a().a(this.b);
            }
            return null;
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public final void a(C0056a aVar) {
        this.i = aVar;
    }

    private List<CampaignEx> a(List<CampaignEx> list) {
        ArrayList arrayList = new ArrayList();
        if (list != null) {
            try {
                if (list.size() > 0) {
                    StringBuilder sb = new StringBuilder("onload 总共返回 的compaign有：");
                    sb.append(list.size());
                    g.b("IntersAdapter", sb.toString());
                    int r = this.j.r();
                    int i2 = 0;
                    while (i2 < list.size() && i2 < this.e && arrayList.size() < r) {
                        CampaignEx campaignEx = (CampaignEx) list.get(i2);
                        if (campaignEx != null && campaignEx.getOfferType() == 1 && TextUtils.isEmpty(campaignEx.getVideoUrlEncode())) {
                            g.b("IntersAdapter", "offertype=1 但是videourl为空");
                        } else if (campaignEx != null && ((!TextUtils.isEmpty(campaignEx.getHtmlUrl()) || campaignEx.isMraid()) && campaignEx.getOfferType() != 99)) {
                            if (!k.a(this.f2673a, campaignEx.getPackageName())) {
                                arrayList.add(campaignEx);
                            } else if (k.b(campaignEx) || k.a(campaignEx)) {
                                arrayList.add(campaignEx);
                            }
                        }
                        i2++;
                    }
                    StringBuilder sb2 = new StringBuilder("onload 返回有以下有效的compaign：");
                    sb2.append(arrayList.size());
                    g.b("IntersAdapter", sb2.toString());
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        return arrayList;
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0098 A[SYNTHETIC, Splitter:B:26:0x0098] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00c4 A[SYNTHETIC, Splitter:B:38:0x00c4] */
    public List<CampaignEx> b(List<CampaignEx> list) {
        ArrayList arrayList = new ArrayList(list.size());
        for (CampaignEx campaignEx : list) {
            if (campaignEx.isMraid() && !TextUtils.isEmpty(campaignEx.getMraid())) {
                FileOutputStream fileOutputStream = null;
                try {
                    String b2 = e.b(c.MINTEGRAL_700_HTML);
                    String md5 = CommonMD5.getMD5(campaignEx.getMraid());
                    if (TextUtils.isEmpty(md5)) {
                        md5 = String.valueOf(System.currentTimeMillis());
                    }
                    File file = new File(b2, md5.concat(".html"));
                    FileOutputStream fileOutputStream2 = new FileOutputStream(file);
                    try {
                        fileOutputStream2.write(campaignEx.getMraid().getBytes());
                        fileOutputStream2.flush();
                        campaignEx.setMraid(file.getAbsolutePath());
                        com.mintegral.msdk.base.common.d.a.a(campaignEx, "", this.b, "5");
                    } catch (Exception e2) {
                        FileOutputStream fileOutputStream3 = fileOutputStream2;
                        e = e2;
                        fileOutputStream = fileOutputStream3;
                        try {
                            e.printStackTrace();
                            campaignEx.setMraid("");
                            com.mintegral.msdk.base.common.d.a.a(campaignEx, e.getMessage(), this.b, "5");
                            if (fileOutputStream != null) {
                            }
                            File file2 = new File(campaignEx.getMraid());
                            a("mraid resource write fail");
                        } catch (Throwable th) {
                            th = th;
                            if (fileOutputStream != null) {
                                try {
                                    fileOutputStream.close();
                                } catch (Exception e3) {
                                    e3.printStackTrace();
                                }
                            }
                            throw th;
                        }
                    } catch (Throwable th2) {
                        th = th2;
                        fileOutputStream = fileOutputStream2;
                        if (fileOutputStream != null) {
                        }
                        throw th;
                    }
                    try {
                        fileOutputStream2.close();
                    } catch (Exception e4) {
                        e4.printStackTrace();
                    }
                } catch (Exception e5) {
                    e = e5;
                    e.printStackTrace();
                    campaignEx.setMraid("");
                    com.mintegral.msdk.base.common.d.a.a(campaignEx, e.getMessage(), this.b, "5");
                    if (fileOutputStream != null) {
                        fileOutputStream.close();
                    }
                    File file22 = new File(campaignEx.getMraid());
                    a("mraid resource write fail");
                }
                File file222 = new File(campaignEx.getMraid());
                if (!file222.exists() || !file222.isFile() || !file222.canRead()) {
                    a("mraid resource write fail");
                }
            }
            arrayList.add(campaignEx);
        }
        return arrayList;
    }

    private int j() {
        try {
            Map<String, Integer> map = com.mintegral.msdk.interstitial.c.a.d;
            int intValue = (TextUtils.isEmpty(this.b) || map == null || !map.containsKey(this.b)) ? 1 : ((Integer) map.get(this.b)).intValue();
            if (intValue <= 0) {
                return 1;
            }
            return intValue;
        } catch (Exception e2) {
            e2.printStackTrace();
            return 1;
        }
    }

    private int k() {
        int i2 = 0;
        try {
            int a2 = !TextUtils.isEmpty(this.b) ? com.mintegral.msdk.interstitial.c.a.a(this.b) : 0;
            if (a2 <= j()) {
                i2 = a2;
            }
            StringBuilder sb = new StringBuilder("getCurrentOffset:");
            sb.append(i2);
            g.b("IntersAdapter", sb.toString());
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return i2;
    }

    /* access modifiers changed from: private */
    public void l() {
        try {
            if (!TextUtils.isEmpty(this.b)) {
                com.mintegral.msdk.interstitial.c.a.a(this.b, 0);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    private static String m() {
        String str = "";
        try {
            if (!TextUtils.isEmpty(com.mintegral.msdk.interstitial.c.a.f2680a)) {
                return com.mintegral.msdk.interstitial.c.a.f2680a;
            }
            return str;
        } catch (Exception e2) {
            e2.printStackTrace();
            return str;
        }
    }

    private String n() {
        String str = "";
        try {
            if (com.mintegral.msdk.interstitial.c.a.b == null) {
                return str;
            }
            Map<String, List<String>> map = com.mintegral.msdk.interstitial.c.a.b;
            if (TextUtils.isEmpty(this.b) || !map.containsKey(this.b)) {
                return str;
            }
            List list = (List) map.get(this.b);
            if (list == null || list.size() <= 0) {
                return str;
            }
            return list.toString();
        } catch (Exception e2) {
            e2.printStackTrace();
            return str;
        }
    }

    private static String o() {
        String str = "";
        try {
            JSONArray jSONArray = new JSONArray();
            com.mintegral.msdk.base.controller.a.d();
            List<Long> g2 = com.mintegral.msdk.base.controller.a.g();
            if (g2 != null && g2.size() > 0) {
                for (Long longValue : g2) {
                    jSONArray.put(longValue.longValue());
                }
            }
            if (jSONArray.length() > 0) {
                return k.a(jSONArray);
            }
            return str;
        } catch (Exception e2) {
            e2.printStackTrace();
            return str;
        }
    }

    public final boolean d() {
        return this.d;
    }

    static /* synthetic */ void a(a aVar, CampaignUnit campaignUnit) {
        if (campaignUnit == null || campaignUnit.getAds() == null || campaignUnit.getAds().size() <= 0) {
            aVar.a("no server ads available");
            return;
        }
        final ArrayList ads = campaignUnit.getAds();
        final List a2 = aVar.a((List<CampaignEx>) ads);
        if (campaignUnit != null) {
            String sessionId = campaignUnit.getSessionId();
            if (!TextUtils.isEmpty(sessionId)) {
                StringBuilder sb = new StringBuilder("onload sessionId:");
                sb.append(sessionId);
                g.b("IntersAdapter", sb.toString());
                com.mintegral.msdk.interstitial.c.a.f2680a = sessionId;
            }
        }
        String str = "IntersAdapter";
        try {
            StringBuilder sb2 = new StringBuilder("onload offset相加前 ");
            sb2.append(aVar.c);
            sb2.append(" mTnum:");
            sb2.append(aVar.e);
            g.b(str, sb2.toString());
            aVar.c += aVar.e;
            int j2 = aVar.j();
            if (aVar.c > j2) {
                StringBuilder sb3 = new StringBuilder("onload 重置offset为0 :");
                sb3.append(j2);
                g.b("IntersAdapter", sb3.toString());
                aVar.c = 0;
            }
            StringBuilder sb4 = new StringBuilder("onload 算出 下次的offset是:");
            sb4.append(aVar.c);
            g.b("IntersAdapter", sb4.toString());
            if (!TextUtils.isEmpty(aVar.b)) {
                com.mintegral.msdk.interstitial.c.a.a(aVar.b, aVar.c);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        if (ads != null && ads.size() > 0) {
            g.b("IntersAdapter", "在主线程 开始处理vba");
            if (ads == null || ads.size() == 0 || aVar.j == null || aVar.f2673a == null) {
                g.b("IntersAdapter", "处理vba null retun");
            }
        }
        k.a((List<CampaignEx>) ads);
        new Thread(new Runnable() {
            public final void run() {
                g.b("IntersAdapter", "在子线程处理业务逻辑 开始");
                if (a2 == null || a2.size() <= 0) {
                    g.d("IntersAdapter", "onload load失败 size:0");
                    a.this.a("no ads available");
                } else {
                    StringBuilder sb = new StringBuilder("onload load成功 size:");
                    sb.append(a2.size());
                    g.d("IntersAdapter", sb.toString());
                    StringBuilder sb2 = new StringBuilder("onload 把广告存在本地 size:");
                    sb2.append(a2.size());
                    g.b("IntersAdapter", sb2.toString());
                    a.a(a.this.b, a.this.b(a2));
                    a.this.f();
                }
                m.a((h) i.a(a.this.f2673a)).d();
                if (ads != null && ads.size() > 0) {
                    a.b(a.this, ads);
                }
                g.b("IntersAdapter", "在子线程处理业务逻辑 完成");
            }
        }).start();
    }

    /* JADX WARNING: Incorrect type for immutable var: ssa=java.util.List, code=java.util.List<com.mintegral.msdk.base.entity.CampaignEx>, for r3v0, types: [java.util.List, java.util.List<com.mintegral.msdk.base.entity.CampaignEx>] */
    static /* synthetic */ void a(String str, List<CampaignEx> list) {
        if (com.mintegral.msdk.interstitial.b.a.a() != null) {
            com.mintegral.msdk.interstitial.b.a a2 = com.mintegral.msdk.interstitial.b.a.a();
            try {
                if (!TextUtils.isEmpty(str) && list != null && list.size() > 0) {
                    for (CampaignEx b2 : list) {
                        a2.b(b2, str);
                    }
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    static /* synthetic */ void b(a aVar, List list) {
        g.b("IntersAdapter", "onload 开始 更新本机已安装广告列表");
        if (aVar.f2673a == null || list == null || list.size() == 0) {
            g.b("IntersAdapter", "onload 列表为空 不做更新本机已安装广告列表");
            return;
        }
        m a2 = m.a((h) i.a(aVar.f2673a));
        boolean z = false;
        for (int i2 = 0; i2 < list.size(); i2++) {
            CampaignEx campaignEx = (CampaignEx) list.get(i2);
            if (campaignEx != null) {
                if (k.a(aVar.f2673a, campaignEx.getPackageName())) {
                    if (com.mintegral.msdk.base.controller.a.c() != null) {
                        com.mintegral.msdk.base.controller.a.c().add(new com.mintegral.msdk.base.entity.g(campaignEx.getId(), campaignEx.getPackageName()));
                        z = true;
                    }
                } else if (a2 != null && !a2.a(campaignEx.getId())) {
                    f fVar = new f();
                    fVar.a(campaignEx.getId());
                    fVar.a(campaignEx.getFca());
                    fVar.b(campaignEx.getFcb());
                    fVar.g();
                    fVar.e();
                    fVar.a(System.currentTimeMillis());
                    a2.a(fVar);
                }
            }
        }
        if (z) {
            g.b("IntersAdapter", "更新安装列表");
            com.mintegral.msdk.base.controller.a.d().f();
        }
    }
}
