package com.mintegral.msdk.interstitial.jscommon;

import android.content.Context;
import android.text.TextUtils;
import android.util.Base64;
import com.mintegral.msdk.base.b.h;
import com.mintegral.msdk.base.b.m;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.interstitial.b.a;
import com.mintegral.msdk.interstitial.view.MTGInterstitialActivity;
import com.mintegral.msdk.mtgjscommon.windvane.i;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class interstitial extends i {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public static final String f2686a = "com.mintegral.msdk.interstitial.jscommon.interstitial";
    private Object b;

    public void getInfo(Object obj, String str) {
        try {
            String str2 = f2686a;
            StringBuilder sb = new StringBuilder("======前端调用 getInfo() 获取campaign数据 params:");
            sb.append(str);
            g.b(str2, sb.toString());
            this.b = obj;
            if (this.mContext == null) {
                g.d(f2686a, "getInfo() context 为空 return");
                b();
                return;
            }
            int a2 = a(this.mContext);
            String c = c();
            if (TextUtils.isEmpty(c)) {
                g.d(f2686a, "getInfo() unitid is null");
                b();
                return;
            }
            String str3 = f2686a;
            StringBuilder sb2 = new StringBuilder("getInfo() mCurrentCallState:");
            sb2.append(a2);
            sb2.append(" unitid:");
            sb2.append(c);
            g.b(str3, sb2.toString());
            if (a2 == 1) {
                try {
                    g.b(f2686a, "instersGetInfo hideLoading");
                    try {
                        if (this.mContext != null) {
                            if (a(this.mContext) == 1) {
                                try {
                                    MTGInterstitialActivity mTGInterstitialActivity = (MTGInterstitialActivity) this.mContext;
                                    if (mTGInterstitialActivity != null) {
                                        mTGInterstitialActivity.hideLoading();
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                    try {
                        if (this.mContext != null) {
                            if (a(this.mContext) == 1) {
                                MTGInterstitialActivity mTGInterstitialActivity2 = (MTGInterstitialActivity) this.mContext;
                                if (mTGInterstitialActivity2 != null) {
                                    mTGInterstitialActivity2.showWebView();
                                }
                            }
                        }
                    } catch (Exception e3) {
                        e3.printStackTrace();
                    }
                    if (this.mContext == null) {
                        g.b(f2686a, "instersGetInfo context 为空 通知前端没有数据");
                        b();
                    } else if (a(this.mContext) != 1) {
                        g.d(f2686a, "instersGetInfo mCurrentCallState!=INTERSTITIAL_CALL");
                        b();
                    } else {
                        if (this.mContext instanceof MTGInterstitialActivity) {
                            ((MTGInterstitialActivity) this.mContext).mIsMtgPage = true;
                            g.b(f2686a, "set mIsMtgPage true");
                        }
                        final String c2 = c();
                        if (TextUtils.isEmpty(c2)) {
                            g.d(f2686a, "instersGetInfo unitid is null");
                            b();
                            return;
                        }
                        final List a3 = a(c2);
                        g.b(f2686a, "instersGetInfo 开始从缓存里取数据");
                        if (a3 != null) {
                            String str4 = f2686a;
                            StringBuilder sb3 = new StringBuilder("instersGetInfo 从缓存里取到的数据 不为空 size：");
                            sb3.append(a3.size());
                            g.d(str4, sb3.toString());
                            String a4 = a(a3);
                            if (TextUtils.isEmpty(a4)) {
                                g.d(f2686a, "instersGetInfo campListJson is null return");
                                b();
                                return;
                            }
                            com.mintegral.msdk.mtgjscommon.windvane.g.a();
                            com.mintegral.msdk.mtgjscommon.windvane.g.a(obj, a4);
                            a(c2, a3);
                            try {
                                if (!TextUtils.isEmpty(c2)) {
                                    if (a3 != null) {
                                        if (a3.size() != 0) {
                                            new Thread(new Runnable() {
                                                public final void run() {
                                                    for (int i = 0; i < a3.size(); i++) {
                                                        CampaignEx campaignEx = (CampaignEx) a3.get(i);
                                                        if (campaignEx != null) {
                                                            m.a((h) com.mintegral.msdk.base.b.i.a(interstitial.this.mContext)).b(campaignEx.getId());
                                                            String a2 = interstitial.f2686a;
                                                            StringBuilder sb = new StringBuilder("======更新frequence：");
                                                            sb.append(campaignEx.getId());
                                                            sb.append(" sUnitId:");
                                                            sb.append(c2);
                                                            g.b(a2, sb.toString());
                                                        }
                                                    }
                                                }
                                            }).start();
                                        }
                                    }
                                }
                            } catch (Exception e4) {
                                e4.printStackTrace();
                            }
                            try {
                                new Thread(new Runnable() {
                                    public final void run() {
                                        g.b(interstitial.f2686a, "清除本地的InterstitialCampaign集合");
                                        a a2 = a.a();
                                        if (a2 != null) {
                                            a2.a(a3, c2);
                                        }
                                    }
                                }).start();
                            } catch (Exception e5) {
                                e5.printStackTrace();
                            }
                        } else {
                            b();
                        }
                    }
                } catch (Exception e6) {
                    e6.printStackTrace();
                    b();
                }
            }
        } catch (Exception e7) {
            e7.printStackTrace();
            b();
        }
    }

    public void install(Object obj, String str) {
        try {
            String str2 = f2686a;
            StringBuilder sb = new StringBuilder("======前端调用 install()  params:");
            sb.append(str);
            g.b(str2, sb.toString());
            if (this.mContext == null) {
                g.d(f2686a, "install() context 为空 return");
                return;
            }
            g.b(f2686a, "install() 开始tracking跳转");
            if (this.mContext instanceof MTGInterstitialActivity) {
                ((MTGInterstitialActivity) this.mContext).clickTracking();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void b() {
        try {
            g.b(f2686a, "通知h5 没有数据");
            com.mintegral.msdk.mtgjscommon.windvane.g.a();
            com.mintegral.msdk.mtgjscommon.windvane.g.a(this.b, "");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static List<CampaignEx> a(String str) {
        try {
            if (TextUtils.isEmpty(str) || a.a() == null) {
                return null;
            }
            return a.a().a(str);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static String a(List<CampaignEx> list) {
        String str = "";
        if (list == null) {
            return str;
        }
        try {
            if (list.size() <= 0) {
                return str;
            }
            JSONArray parseCamplistToJson = CampaignEx.parseCamplistToJson(list);
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("campaignList", parseCamplistToJson);
            String jSONObject2 = jSONObject.toString();
            String str2 = f2686a;
            StringBuilder sb = new StringBuilder("===========campListJson:");
            sb.append(jSONObject2);
            g.b(str2, sb.toString());
            if (!TextUtils.isEmpty(jSONObject2)) {
                return Base64.encodeToString(jSONObject2.getBytes(), 2);
            }
            return str;
        } catch (Exception e) {
            e.printStackTrace();
            return str;
        }
    }

    public List<String> getExcludeIdList(String str) {
        List<String> list;
        List<String> list2 = null;
        try {
            if (TextUtils.isEmpty(str)) {
                return null;
            }
            String optString = new JSONObject(str).optString("exclude_ids");
            if (!TextUtils.isEmpty(optString)) {
                JSONArray jSONArray = new JSONArray(optString);
                if (jSONArray.length() > 0) {
                    list = new ArrayList<>();
                    int i = 0;
                    while (i < jSONArray.length()) {
                        try {
                            if (!TextUtils.isEmpty(jSONArray.optString(i))) {
                                list.add(jSONArray.optString(i));
                            }
                            i++;
                        } catch (Exception e) {
                            Exception exc = e;
                            list2 = list;
                            e = exc;
                            e.printStackTrace();
                            list = list2;
                            return list;
                        }
                    }
                    return list;
                }
            }
            list = list2;
            return list;
        } catch (Exception e2) {
            e = e2;
            e.printStackTrace();
            list = list2;
            return list;
        }
    }

    private static int a(Context context) {
        return (context == null || !(context instanceof MTGInterstitialActivity)) ? -1 : 1;
    }

    private String c() {
        try {
            if (this.mContext != null && a(this.mContext) == 1) {
                try {
                    if (this.mContext != null && (this.mContext instanceof MTGInterstitialActivity)) {
                        MTGInterstitialActivity mTGInterstitialActivity = (MTGInterstitialActivity) this.mContext;
                        if (mTGInterstitialActivity != null) {
                            return mTGInterstitialActivity.mUnitid;
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return null;
    }

    private static void a(String str, List<CampaignEx> list) {
        try {
            if (!TextUtils.isEmpty(str) && list != null) {
                if (list.size() != 0) {
                    for (int i = 0; i < list.size(); i++) {
                        CampaignEx campaignEx = (CampaignEx) list.get(i);
                        if (campaignEx != null) {
                            String str2 = f2686a;
                            StringBuilder sb = new StringBuilder("======更新displayid：");
                            sb.append(campaignEx.getId());
                            g.b(str2, sb.toString());
                            com.mintegral.msdk.interstitial.c.a.a(str, campaignEx.getId());
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
