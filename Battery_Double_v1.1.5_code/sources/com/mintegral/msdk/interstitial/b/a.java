package com.mintegral.msdk.interstitial.b;

import android.content.Context;
import android.text.TextUtils;
import com.mintegral.msdk.base.b.f;
import com.mintegral.msdk.base.b.h;
import com.mintegral.msdk.base.b.i;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.utils.g;
import java.util.ArrayList;
import java.util.List;

/* compiled from: InterstitialCamapignCache */
public class a {

    /* renamed from: a reason: collision with root package name */
    private static final String f2679a = "com.mintegral.msdk.interstitial.b.a";
    private static a c;
    private f b;

    private a() {
        try {
            Context h = com.mintegral.msdk.base.controller.a.d().h();
            if (h != null) {
                this.b = f.a((h) i.a(h));
            } else {
                g.d(f2679a, "InterstitialCamapignCache get Context is null");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static a a() {
        if (c == null) {
            try {
                synchronized (a.class) {
                    if (c == null) {
                        c = new a();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return c;
    }

    public final List<CampaignEx> a(String str) {
        List<CampaignEx> list = null;
        try {
            if (TextUtils.isEmpty(str)) {
                return null;
            }
            List<CampaignEx> a2 = this.b.a(str, 1, 0, 1);
            if (a2 == null) {
                return null;
            }
            ArrayList arrayList = new ArrayList();
            try {
                for (CampaignEx add : a2) {
                    arrayList.add(add);
                }
                return arrayList;
            } catch (Exception e) {
                e = e;
                list = arrayList;
                e.printStackTrace();
                return list;
            }
        } catch (Exception e2) {
            e = e2;
            e.printStackTrace();
            return list;
        }
    }

    public final void a(CampaignEx campaignEx, String str) {
        if (campaignEx != null) {
            try {
                if (!TextUtils.isEmpty(str)) {
                    this.b.a(campaignEx.getId(), str);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public final void a(List<CampaignEx> list, String str) {
        if (list != null && list.size() > 0 && !TextUtils.isEmpty(str)) {
            for (CampaignEx a2 : list) {
                a(a2, str);
            }
        }
    }

    public final void b(CampaignEx campaignEx, String str) {
        try {
            if (!(this.b == null || campaignEx == null || TextUtils.isEmpty(str))) {
                this.b.a(campaignEx, str, 0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public final synchronized void a(long j, String str) {
        try {
            if (!(this.b == null || j == 0 || TextUtils.isEmpty(str))) {
                this.b.b(j, str);
            }
        } catch (Exception e) {
            e.printStackTrace();
            g.d(f2679a, e.getMessage());
        }
    }
}
