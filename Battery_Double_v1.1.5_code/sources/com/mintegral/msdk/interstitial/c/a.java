package com.mintegral.msdk.interstitial.c;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.TextUtils;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.b.d;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.interstitial.view.MTGInterstitialActivity;
import com.mintegral.msdk.out.InterstitialListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* compiled from: InterstitialController */
public final class a {

    /* renamed from: a reason: collision with root package name */
    public static String f2680a;
    public static Map<String, List<String>> b = new HashMap();
    public static Map<String, Integer> c = new HashMap();
    public static Map<String, Integer> d = new HashMap();
    public static Map<String, c> e = new HashMap();
    /* access modifiers changed from: private */
    public String f = "InterstitialController";
    private Context g;
    private String h;
    private String i;
    /* access modifiers changed from: private */
    public Handler j;
    private d k;
    /* access modifiers changed from: private */
    public InterstitialListener l;
    private boolean m = false;

    /* renamed from: com.mintegral.msdk.interstitial.c.a$a reason: collision with other inner class name */
    /* compiled from: InterstitialController */
    public class C0056a {
        private com.mintegral.msdk.interstitial.a.a b;
        private b c;

        public C0056a(com.mintegral.msdk.interstitial.a.a aVar, b bVar) {
            this.b = aVar;
            this.c = bVar;
        }

        public final void a(boolean z) {
            try {
                if (this.c != null) {
                    if (a.this.j != null) {
                        a.this.j.removeCallbacks(this.c);
                    }
                    if (z) {
                        a.this.b(false);
                    } else if (a.this.l != null) {
                        a.e(a.this);
                    }
                    g.d(a.this.f, "onInterstitialLoadSuccess remove task ");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public final void a(boolean z, String str) {
            try {
                if (this.b != null) {
                    this.b.a((C0056a) null);
                    this.b = null;
                }
                if (this.c != null) {
                    g.d(a.this.f, "LoadIntersInnerListener onIntersLoadFail remove task");
                    if (a.this.j != null) {
                        a.this.j.removeCallbacks(this.c);
                    }
                    if (z) {
                        if (a.this.l != null) {
                            a.this.c(str);
                        }
                    } else if (a.this.l != null) {
                        a.this.b(str);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /* compiled from: InterstitialController */
    public class b implements Runnable {
        private com.mintegral.msdk.interstitial.a.a b;

        public b(com.mintegral.msdk.interstitial.a.a aVar) {
            this.b = aVar;
        }

        public final void run() {
            try {
                g.d(a.this.f, "CommonCancelTimeTask");
                if (this.b != null) {
                    if (this.b.d()) {
                        a.this.c("load timeout");
                    } else if (a.this.l != null) {
                        a.this.b("load timeout");
                    }
                    this.b.a((C0056a) null);
                    this.b = null;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /* compiled from: InterstitialController */
    public class c {
        public c() {
        }

        public final void a(String str) {
            try {
                a.this.c(str);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public final void a() {
            try {
                a.f(a.this);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public final void b() {
            try {
                if (a.this.j != null) {
                    a.this.j.sendEmptyMessage(7);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public final void c() {
            try {
                if (a.this.j != null) {
                    a.this.j.sendEmptyMessage(6);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static void a(String str, int i2) {
        try {
            if (c != null && !TextUtils.isEmpty(str)) {
                c.put(str, Integer.valueOf(i2));
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public static int a(String str) {
        try {
            if (!TextUtils.isEmpty(str) && c != null && c.containsKey(str)) {
                Integer num = (Integer) c.get(str);
                if (num != null) {
                    return num.intValue();
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return 0;
    }

    public final void a(InterstitialListener interstitialListener) {
        this.l = interstitialListener;
    }

    public static void a(String str, String str2) {
        if (b == null) {
            b = new HashMap();
        }
        if (!b.containsKey(str) || TextUtils.isEmpty(str2)) {
            ArrayList arrayList = new ArrayList();
            arrayList.add(str2);
            if (b != null) {
                b.put(str, arrayList);
            }
            return;
        }
        List list = (List) b.get(str);
        if (list.size() == 20) {
            list.remove(0);
        }
        list.add(str2);
    }

    public a() {
        try {
            this.j = new Handler(Looper.getMainLooper()) {
                public final void handleMessage(Message message) {
                    if (message != null) {
                        switch (message.what) {
                            case 1:
                                if (a.this.l != null) {
                                    a.this.l.onInterstitialLoadSuccess();
                                    g.b(a.this.f, "handler 数据load成功");
                                    return;
                                }
                                break;
                            case 2:
                                if (a.this.l != null) {
                                    String str = "";
                                    if (message.obj != null && (message.obj instanceof String)) {
                                        str = (String) message.obj;
                                    }
                                    if (TextUtils.isEmpty(str)) {
                                        str = "can't show because unknow error";
                                    }
                                    a.this.l.onInterstitialLoadFail(str);
                                    String b = a.this.f;
                                    StringBuilder sb = new StringBuilder("handler 数据load失败:");
                                    sb.append(str);
                                    g.b(b, sb.toString());
                                    return;
                                }
                                break;
                            case 3:
                                if (a.this.l != null) {
                                    a.this.l.onInterstitialShowSuccess();
                                    g.b(a.this.f, "handler 数据show成功");
                                    return;
                                }
                                break;
                            case 4:
                                if (a.this.l != null) {
                                    String str2 = "";
                                    if (message.obj != null && (message.obj instanceof String)) {
                                        str2 = (String) message.obj;
                                    }
                                    if (TextUtils.isEmpty(str2)) {
                                        str2 = "can't show because unknow error";
                                    }
                                    a.this.l.onInterstitialShowFail(str2);
                                    String b2 = a.this.f;
                                    StringBuilder sb2 = new StringBuilder("handler 数据show失败:");
                                    sb2.append(str2);
                                    g.b(b2, sb2.toString());
                                    return;
                                }
                                break;
                            case 6:
                                if (a.this.l != null) {
                                    a.this.l.onInterstitialAdClick();
                                    return;
                                }
                                break;
                            case 7:
                                if (a.this.l != null) {
                                    a.this.l.onInterstitialClosed();
                                    break;
                                }
                                break;
                        }
                    }
                }
            };
        } catch (Exception e2) {
            try {
                e2.printStackTrace();
            } catch (Exception e3) {
                e3.printStackTrace();
            }
        }
    }

    public final boolean a(Context context, Map<String, Object> map) {
        try {
            this.m = false;
            if (map == null) {
                g.c(this.f, "init error params==null");
                return false;
            } else if (context == null) {
                g.c(this.f, "init context ==null");
                return false;
            } else {
                if (map.containsKey(MIntegralConstans.PROPERTIES_UNIT_ID)) {
                    if (map.get(MIntegralConstans.PROPERTIES_UNIT_ID) instanceof String) {
                        if (map.containsKey(MIntegralConstans.PROPERTIES_API_REUQEST_CATEGORY) && (map.get(MIntegralConstans.PROPERTIES_API_REUQEST_CATEGORY) instanceof String)) {
                            this.i = (String) map.get(MIntegralConstans.PROPERTIES_API_REUQEST_CATEGORY);
                        }
                        this.h = (String) map.get(MIntegralConstans.PROPERTIES_UNIT_ID);
                        this.g = context;
                        this.m = true;
                        return this.m;
                    }
                }
                g.c(this.f, "init error,make sure you have unitid");
                return false;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            this.m = false;
        }
    }

    public final void a() {
        try {
            if (this.g == null) {
                b("context is null");
                g.b(this.f, "load context is null");
            } else if (TextUtils.isEmpty(this.h)) {
                b("unitid is null");
                g.b(this.f, "load unitid is null");
            } else if (!this.m) {
                b("init error");
                g.b(this.f, "load init error");
            } else {
                c();
                try {
                    if (this.k != null) {
                        int r = this.k.r();
                        int x = this.k.x();
                        if (r <= 0) {
                            r = 1;
                        }
                        if (x <= 0) {
                            x = 1;
                        }
                        int i2 = x * r;
                        if (d != null && !TextUtils.isEmpty(this.h)) {
                            d.put(this.h, Integer.valueOf(i2));
                        }
                        String str = this.f;
                        StringBuilder sb = new StringBuilder("maxOffset:");
                        sb.append(i2);
                        sb.append(" apiCacheNum:");
                        sb.append(r);
                        sb.append(" mUnitId:");
                        sb.append(this.h);
                        g.b(str, sb.toString());
                    }
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
                a(false);
            }
        } catch (Exception e3) {
            e3.printStackTrace();
            b("can't show because unknow error");
        }
    }

    public final void b() {
        try {
            if (this.g == null) {
                c("context is null");
                g.b(this.f, "show context is null");
            } else if (TextUtils.isEmpty(this.h)) {
                c("unitid is null");
                g.b(this.f, "show unitid is null");
            } else if (!this.m) {
                c("init error");
                g.b(this.f, "show init error");
            } else {
                c();
                b(true);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            c("can't show because unknow error");
        }
    }

    private void c() {
        try {
            g.b(this.f, "initUnitSetting");
            try {
                new com.mintegral.msdk.b.c().a(this.g, null, null, this.h);
            } catch (Exception e2) {
                e2.printStackTrace();
            }
            com.mintegral.msdk.b.b.a();
            this.k = com.mintegral.msdk.b.b.c(com.mintegral.msdk.base.controller.a.d().j(), this.h);
            if (this.k == null) {
                this.k = d.e(this.h);
                g.b(this.f, "获取默认的unitsetting");
            }
        } catch (Exception e3) {
            e3.printStackTrace();
        }
    }

    private void a(boolean z) {
        try {
            com.mintegral.msdk.interstitial.a.a aVar = new com.mintegral.msdk.interstitial.a.a(this.g, this.h, this.i, z);
            b bVar = new b(aVar);
            aVar.a(new C0056a(aVar, bVar));
            if (this.j != null) {
                this.j.postDelayed(bVar, 30000);
            }
            aVar.b();
        } catch (Exception e2) {
            e2.printStackTrace();
            if (!z) {
                b("can't show because unknow error");
            }
        }
    }

    /* access modifiers changed from: private */
    public void b(boolean z) {
        try {
            String str = this.f;
            StringBuilder sb = new StringBuilder("showInterstitial isShowCall:");
            sb.append(z);
            g.b(str, sb.toString());
            CampaignEx a2 = new com.mintegral.msdk.interstitial.a.a(this.g, this.h, this.i, true).a();
            if (a2 != null) {
                c cVar = new c();
                if (e != null && !TextUtils.isEmpty(this.h)) {
                    e.put(this.h, cVar);
                }
                Intent intent = new Intent(this.g, MTGInterstitialActivity.class);
                intent.addFlags(67108864);
                intent.addFlags(268435456);
                if (!TextUtils.isEmpty(this.h)) {
                    intent.putExtra("unitId", this.h);
                }
                if (a2 != null) {
                    intent.putExtra("campaign", a2);
                }
                if (this.g != null) {
                    this.g.startActivity(intent);
                }
            } else if (z) {
                g.d(this.f, "showInterstitial 发现cmapaign为空 去load一遍=========");
                a(true);
            } else {
                c("no ads available can show");
                g.b(this.f, "showInterstitial 发现cmapaign为空");
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            if (this.l != null) {
                c("can't show because unknow error");
            }
        }
    }

    /* access modifiers changed from: private */
    public void b(String str) {
        try {
            if (this.j != null) {
                Message obtain = Message.obtain();
                obtain.obj = str;
                obtain.what = 2;
                this.j.sendMessage(obtain);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    public void c(String str) {
        try {
            if (this.j != null) {
                Message obtain = Message.obtain();
                obtain.obj = str;
                obtain.what = 4;
                this.j.sendMessage(obtain);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    static /* synthetic */ void e(a aVar) {
        if (aVar.j != null) {
            aVar.j.sendEmptyMessage(1);
        }
    }

    static /* synthetic */ void f(a aVar) {
        if (aVar.j != null) {
            aVar.j.sendEmptyMessage(3);
        }
    }
}
