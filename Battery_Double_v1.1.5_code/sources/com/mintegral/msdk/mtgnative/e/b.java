package com.mintegral.msdk.mtgnative.e;

import android.content.Context;
import android.text.TextUtils;
import com.mintegral.msdk.base.b.h;
import com.mintegral.msdk.base.b.i;
import com.mintegral.msdk.base.b.m;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.mtgnative.d.a;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;

/* compiled from: NativeReportUtils */
public final class b {
    public static synchronized void a(CampaignEx campaignEx, Context context, String str, a aVar) {
        synchronized (b.class) {
            if (!campaignEx.isReport()) {
                campaignEx.setReport(true);
                a(campaignEx, context, str);
                b(campaignEx, context, str);
                List a2 = a(campaignEx);
                if (a2.size() != 0) {
                    for (int i = 0; i < a2.size(); i++) {
                        String str2 = (String) a2.get(i);
                        if (!TextUtils.isEmpty(str2)) {
                            com.mintegral.msdk.click.a.a(context, campaignEx, str, str2, false, false);
                        }
                    }
                }
                if (aVar != null) {
                    aVar.onLoggingImpression(campaignEx.getType());
                }
            }
        }
    }

    private static synchronized void a(final CampaignEx campaignEx, final Context context, String str) {
        synchronized (b.class) {
            if (!TextUtils.isEmpty(campaignEx.getImpressionURL())) {
                new Thread(new Runnable() {
                    public final void run() {
                        try {
                            m.a((h) i.a(context)).b(campaignEx.getId());
                        } catch (Exception unused) {
                            g.d("NativeReportUtils", "campain can't insert db");
                        }
                    }
                }).start();
                com.mintegral.msdk.click.a.a(context, campaignEx, str, campaignEx.getImpressionURL(), false, true);
            }
            if (!(TextUtils.isEmpty(str) || campaignEx.getNativeVideoTracking() == null || campaignEx.getNativeVideoTracking().n() == null)) {
                com.mintegral.msdk.click.a.a(context, campaignEx, str, campaignEx.getNativeVideoTracking().n(), false);
            }
        }
    }

    private static synchronized void b(CampaignEx campaignEx, Context context, String str) {
        synchronized (b.class) {
            if (!TextUtils.isEmpty(campaignEx.getOnlyImpressionURL()) && com.mintegral.msdk.mtgnative.c.b.c != null && !com.mintegral.msdk.mtgnative.c.b.c.containsKey(campaignEx.getOnlyImpressionURL())) {
                com.mintegral.msdk.mtgnative.c.b.c.put(campaignEx.getOnlyImpressionURL(), Long.valueOf(System.currentTimeMillis()));
                com.mintegral.msdk.click.a.a(context, campaignEx, str, campaignEx.getOnlyImpressionURL(), false, true);
            }
        }
    }

    private static List<String> a(CampaignEx campaignEx) {
        ArrayList arrayList = new ArrayList();
        if (campaignEx == null) {
            return arrayList;
        }
        String ad_url_list = campaignEx.getAd_url_list();
        if (TextUtils.isEmpty(ad_url_list)) {
            return arrayList;
        }
        try {
            JSONArray jSONArray = new JSONArray(ad_url_list);
            for (int i = 0; i < jSONArray.length(); i++) {
                arrayList.add((String) jSONArray.get(i));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return arrayList;
    }
}
