package com.mintegral.msdk.mtgnative.e;

import android.content.Context;
import android.text.TextUtils;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.mintegral.msdk.base.common.d.c.b;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.utils.c;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.out.Campaign;
import java.util.List;

/* compiled from: NativeReport */
public final class a {
    public static void a(Context context, List<Campaign> list, String str) {
        if (!(context == null || list == null)) {
            try {
                if (list.size() > 0 && !TextUtils.isEmpty(str)) {
                    StringBuffer stringBuffer = new StringBuffer("key=2000048&");
                    if (list != null && list.size() > 0) {
                        StringBuilder sb = new StringBuilder("cid=");
                        sb.append(((Campaign) list.get(0)).getId());
                        sb.append(RequestParameters.AMPERSAND);
                        stringBuffer.append(sb.toString());
                    }
                    StringBuilder sb2 = new StringBuilder("network_type=");
                    sb2.append(c.p(context));
                    sb2.append(RequestParameters.AMPERSAND);
                    stringBuffer.append(sb2.toString());
                    StringBuilder sb3 = new StringBuilder("unit_id=");
                    sb3.append(str);
                    sb3.append(RequestParameters.AMPERSAND);
                    stringBuffer.append(sb3.toString());
                    if (list != null && list.size() > 1) {
                        String requestId = ((CampaignEx) list.get(0)).getRequestId();
                        StringBuilder sb4 = new StringBuilder("rid_n=");
                        sb4.append(requestId);
                        stringBuffer.append(sb4.toString());
                    } else if (list.size() == 1) {
                        String requestIdNotice = ((CampaignEx) list.get(0)).getRequestIdNotice();
                        StringBuilder sb5 = new StringBuilder("rid_n=");
                        sb5.append(requestIdNotice);
                        stringBuffer.append(sb5.toString());
                    }
                    String stringBuffer2 = stringBuffer.toString();
                    if (context != null && !TextUtils.isEmpty(stringBuffer2) && !TextUtils.isEmpty(str)) {
                        com.mintegral.msdk.base.common.d.c.a aVar = new com.mintegral.msdk.base.common.d.c.a(context);
                        aVar.c();
                        aVar.b(com.mintegral.msdk.base.common.a.f, com.mintegral.msdk.base.common.d.c.a(stringBuffer2, context, str), new b() {
                            public final void a(String str) {
                                g.d("NativeReport", str);
                            }

                            public final void b(String str) {
                                g.d("NativeReport", str);
                            }
                        });
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                g.d("NativeReport", e.getMessage());
            } catch (Throwable th) {
                th.printStackTrace();
            }
        }
    }

    public static void a(Context context, String str, String str2, boolean z) {
        if (context != null) {
            try {
                if (!TextUtils.isEmpty(str) && !TextUtils.isEmpty(str2)) {
                    StringBuffer stringBuffer = new StringBuffer("key=2000047&");
                    StringBuilder sb = new StringBuilder("network_type=");
                    sb.append(c.p(context));
                    sb.append(RequestParameters.AMPERSAND);
                    stringBuffer.append(sb.toString());
                    StringBuilder sb2 = new StringBuilder("unit_id=");
                    sb2.append(str2);
                    sb2.append(RequestParameters.AMPERSAND);
                    stringBuffer.append(sb2.toString());
                    if (!TextUtils.isEmpty(com.mintegral.msdk.base.common.a.D)) {
                        StringBuilder sb3 = new StringBuilder("sys_id=");
                        sb3.append(com.mintegral.msdk.base.common.a.D);
                        sb3.append(RequestParameters.AMPERSAND);
                        stringBuffer.append(sb3.toString());
                    }
                    if (!TextUtils.isEmpty(com.mintegral.msdk.base.common.a.E)) {
                        StringBuilder sb4 = new StringBuilder("bkup_id=");
                        sb4.append(com.mintegral.msdk.base.common.a.E);
                        sb4.append(RequestParameters.AMPERSAND);
                        stringBuffer.append(sb4.toString());
                    }
                    if (z) {
                        stringBuffer.append("hb=1&");
                    }
                    StringBuilder sb5 = new StringBuilder("reason=");
                    sb5.append(str);
                    stringBuffer.append(sb5.toString());
                    String stringBuffer2 = stringBuffer.toString();
                    if (context != null && !TextUtils.isEmpty(stringBuffer2)) {
                        com.mintegral.msdk.base.common.d.c.a aVar = new com.mintegral.msdk.base.common.d.c.a(context);
                        aVar.c();
                        aVar.b(com.mintegral.msdk.base.common.a.f, com.mintegral.msdk.base.common.d.c.b(stringBuffer2, context), new b() {
                            public final void a(String str) {
                                g.d("NativeReport", str);
                            }

                            public final void b(String str) {
                                g.d("NativeReport", str);
                            }
                        });
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                g.d("NativeReport", e.getMessage());
            } catch (Throwable th) {
                th.printStackTrace();
            }
        }
    }
}
