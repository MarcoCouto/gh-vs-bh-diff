package com.mintegral.msdk.mtgnative.b;

import java.io.PrintWriter;
import java.io.StringWriter;

/* compiled from: NativeConst */
public final class a {

    /* renamed from: a reason: collision with root package name */
    public static final String f2757a = com.mintegral.msdk.base.common.a.k;
    public static final String b = com.mintegral.msdk.base.common.a.i;
    public static final String c = com.mintegral.msdk.base.common.a.l;

    public static String a(Exception exc) {
        StringWriter stringWriter = new StringWriter();
        exc.printStackTrace(new PrintWriter(stringWriter));
        return stringWriter.toString();
    }
}
