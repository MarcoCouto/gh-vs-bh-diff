package com.mintegral.msdk.mtgnative.d;

import android.content.Context;
import android.text.TextUtils;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.out.Campaign;
import com.mintegral.msdk.out.Frame;
import com.mintegral.msdk.out.NativeListener.NativeAdListener;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/* compiled from: AdListenerProxy */
public class a implements NativeAdListener {

    /* renamed from: a reason: collision with root package name */
    private static final String f2798a = "a";
    private NativeAdListener b;
    private boolean c = false;
    private String d;
    private Context e;
    private boolean f;

    public final void a(boolean z) {
        this.f = z;
    }

    public a() {
    }

    public a(NativeAdListener nativeAdListener) {
        this.b = nativeAdListener;
    }

    public final boolean a() {
        return this.c;
    }

    public final void b() {
        this.c = true;
    }

    public void onAdLoaded(List<Campaign> list, int i) {
        CopyOnWriteArrayList copyOnWriteArrayList;
        try {
            this.c = false;
            synchronized (list) {
                copyOnWriteArrayList = new CopyOnWriteArrayList(list);
            }
            if (this.b != null) {
                if (copyOnWriteArrayList.size() > 0) {
                    this.b.onAdLoaded(copyOnWriteArrayList, i);
                    return;
                }
                this.b.onAdLoaded(list, i);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public void onAdLoadError(String str) {
        this.c = false;
        String str2 = f2798a;
        StringBuilder sb = new StringBuilder("onAdLoadError,message:");
        sb.append(str);
        g.a(str2, sb.toString());
        if (this.b != null) {
            this.b.onAdLoadError(str);
            if (!TextUtils.isEmpty(this.d)) {
                com.mintegral.msdk.mtgnative.e.a.a(this.e, str, this.d, this.f);
            }
        }
    }

    public void onAdClick(Campaign campaign) {
        String str = f2798a;
        StringBuilder sb = new StringBuilder("onAdClick,campaign:");
        sb.append(campaign);
        g.a(str, sb.toString());
        if (this.b != null) {
            this.b.onAdClick(campaign);
        }
    }

    public void onAdFramesLoaded(List<Frame> list) {
        if (this.b != null) {
            this.b.onAdFramesLoaded(list);
        }
    }

    public void onLoggingImpression(int i) {
        String str = f2798a;
        StringBuilder sb = new StringBuilder("onLoggingImpression,adsourceType:");
        sb.append(i);
        g.a(str, sb.toString());
        if (this.b != null) {
            this.b.onLoggingImpression(i);
        }
    }
}
