package com.mintegral.msdk.mtgnative.a;

import com.mintegral.msdk.out.Campaign;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* compiled from: NativeCacheFactory */
public final class f {

    /* renamed from: a reason: collision with root package name */
    public static Map<Integer, b<String, List<Campaign>>> f2756a = new HashMap();

    public static b<String, List<Campaign>> a(int i) {
        b<String, List<Campaign>> bVar;
        if (f2756a.containsKey(Integer.valueOf(i))) {
            return (b) f2756a.get(Integer.valueOf(i));
        }
        switch (i) {
            case 1:
                bVar = new a<>(i);
                break;
            case 2:
                bVar = new a<>(i);
                break;
            case 3:
                bVar = new d<>();
                break;
            case 6:
                bVar = new c<>();
                break;
            case 7:
                bVar = new e<>();
                break;
            default:
                bVar = new a<>(i);
                break;
        }
        f2756a.put(Integer.valueOf(i), bVar);
        return bVar;
    }
}
