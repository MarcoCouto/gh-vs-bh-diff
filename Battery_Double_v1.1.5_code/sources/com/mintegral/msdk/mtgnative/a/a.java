package com.mintegral.msdk.mtgnative.a;

import android.text.TextUtils;
import com.mintegral.msdk.b.b;
import com.mintegral.msdk.base.b.f;
import com.mintegral.msdk.base.b.h;
import com.mintegral.msdk.base.b.i;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.out.Campaign;
import java.util.ArrayList;
import java.util.List;

/* compiled from: APICache */
public final class a extends b<String, List<Campaign>> {

    /* renamed from: a reason: collision with root package name */
    private f f2752a = f.a((h) i.a(com.mintegral.msdk.base.controller.a.d().h()));
    private int b;

    public final void a(String str) {
    }

    public final /* synthetic */ Object a(Object obj, int i) {
        String str = (String) obj;
        List a2 = this.f2752a.a(str, i, 2, this.b);
        ArrayList arrayList = null;
        if (a2 == null) {
            return null;
        }
        if (a(a2, 2)) {
            this.f2752a.a(str, this.b);
        } else {
            arrayList = new ArrayList();
            arrayList.addAll(a2);
        }
        return arrayList;
    }

    public final /* synthetic */ void a(Object obj, Object obj2) {
        String str = (String) obj;
        List list = (List) obj2;
        if (!TextUtils.isEmpty(str) && list.size() > 0) {
            this.f2752a.a(str, 1, this.b, false);
            this.f2752a.a(str, 2, this.b, false);
            for (int i = 0; i < list.size(); i++) {
                CampaignEx campaignEx = (CampaignEx) list.get(i);
                campaignEx.setCacheLevel(1);
                this.f2752a.a(campaignEx, str, 1);
            }
        }
    }

    public final /* synthetic */ void a(Object obj, Object obj2, String str) {
        String str2 = (String) obj;
        List list = (List) obj2;
        boolean z = !TextUtils.isEmpty(str);
        if (!TextUtils.isEmpty(str2) && list.size() > 0) {
            this.f2752a.a(str2, 1, this.b, z);
            this.f2752a.a(str2, 2, this.b, z);
            for (int i = 0; i < list.size(); i++) {
                CampaignEx campaignEx = (CampaignEx) list.get(i);
                campaignEx.setCacheLevel(1);
                this.f2752a.a(campaignEx, str2, 1);
            }
        }
    }

    public final /* synthetic */ Object b(Object obj, int i) {
        String str = (String) obj;
        List a2 = this.f2752a.a(str, i, 1, this.b);
        ArrayList arrayList = null;
        if (a2 == null) {
            return null;
        }
        if (a(a2, 1)) {
            for (int i2 = 0; i2 < a2.size(); i2++) {
                CampaignEx campaignEx = (CampaignEx) a2.get(i2);
                campaignEx.setCacheLevel(2);
                this.f2752a.a(campaignEx, str, 1);
            }
        } else {
            arrayList = new ArrayList();
            arrayList.addAll(a2);
        }
        return arrayList;
    }

    public a(int i) {
        this.b = i;
    }

    private static boolean a(List<CampaignEx> list, int i) {
        if (list == null || list.size() <= 0) {
            return false;
        }
        long currentTimeMillis = System.currentTimeMillis();
        long timestamp = ((CampaignEx) list.get(0)).getTimestamp();
        long j = 0;
        switch (i) {
            case 1:
                j = a();
                break;
            case 2:
                b.a();
                com.mintegral.msdk.b.a b2 = b.b(com.mintegral.msdk.base.controller.a.d().j());
                if (b2 == null) {
                    b.a();
                    b2 = b.b();
                }
                j = 1000 * b2.ak();
                break;
        }
        if (currentTimeMillis - timestamp > j) {
            return true;
        }
        return false;
    }

    public final void a(String str, Campaign campaign, String str2) {
        boolean z = !TextUtils.isEmpty(str2);
        if (campaign != null && !TextUtils.isEmpty(str)) {
            try {
                CampaignEx campaignEx = (CampaignEx) campaign;
                if (this.f2752a.a(campaignEx.getId(), campaignEx.getTab(), str, campaignEx.getCacheLevel(), campaignEx.getType(), z)) {
                    this.f2752a.a(campaignEx.getId(), str, campaignEx.getCacheLevel(), this.b, z);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
