package com.mintegral.msdk.mtgnative.a;

import android.text.TextUtils;
import com.mintegral.msdk.b.a;
import com.mintegral.msdk.base.utils.s;
import com.mintegral.msdk.out.Campaign;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/* compiled from: AbsNativeCache */
public abstract class b<K, V> {
    public V a(K k, int i) {
        return null;
    }

    public abstract void a(K k, V v);

    public abstract void a(K k, V v, String str);

    public abstract void a(String str);

    public abstract void a(String str, Campaign campaign, String str2);

    public abstract V b(K k, int i);

    public static long a() {
        com.mintegral.msdk.b.b.a();
        a b = com.mintegral.msdk.b.b.b(com.mintegral.msdk.base.controller.a.d().j());
        if (b == null) {
            com.mintegral.msdk.b.b.a();
            b = com.mintegral.msdk.b.b.b();
        }
        return b.aF() * 1000;
    }

    public static void a(String str, List<Campaign> list, Map<String, Map<Long, Object>> map) {
        if (!TextUtils.isEmpty(str) && list != null && list.size() > 0) {
            HashMap hashMap = new HashMap();
            hashMap.put(Long.valueOf(System.currentTimeMillis()), list);
            map.put(str, hashMap);
        }
    }

    public static List<Campaign> a(String str, Map<String, Map<Long, Object>> map, int i) {
        if (!TextUtils.isEmpty(str) && map != null && map.containsKey(str)) {
            Map map2 = (Map) map.get(str);
            if (map2 != null && map2.size() > 0) {
                Iterator it = map2.entrySet().iterator();
                while (it.hasNext()) {
                    Entry entry = (Entry) it.next();
                    if (System.currentTimeMillis() - ((Long) entry.getKey()).longValue() > a()) {
                        it.remove();
                        return null;
                    }
                    List list = (List) entry.getValue();
                    if (list != null && list.size() > 0) {
                        if (i != 0) {
                            return list.subList(0, Math.min(list.size(), i));
                        }
                        ArrayList arrayList = new ArrayList();
                        arrayList.addAll(list);
                        it.remove();
                        return arrayList;
                    }
                }
            }
        }
        return null;
    }

    public static void a(String str, Map<String, Map<Long, Object>> map) {
        try {
            if (!s.a(str) && map != null) {
                if (map.containsKey(str)) {
                    Iterator it = map.keySet().iterator();
                    while (it.hasNext()) {
                        String str2 = (String) it.next();
                        if (s.b(str2) && str2.equals(str)) {
                            it.remove();
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
