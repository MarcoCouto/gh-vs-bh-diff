package com.mintegral.msdk.mtgnative.f;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.base.utils.k;
import com.mintegral.msdk.mtgnative.c.b;
import com.mintegral.msdk.mtgnative.c.c;
import com.mintegral.msdk.out.AdMobClickListener;
import com.mintegral.msdk.out.Campaign;
import com.mintegral.msdk.out.NativeListener.NativeTrackingListener;
import java.util.List;
import java.util.Map;

/* compiled from: NativeProvider */
public class a {

    /* renamed from: a reason: collision with root package name */
    private b f2800a;
    private Handler b = new Handler(Looper.getMainLooper());
    private com.mintegral.msdk.mtgnative.d.a c;
    private NativeTrackingListener d;

    public final void a(com.mintegral.msdk.mtgnative.d.a aVar) {
        this.c = aVar;
    }

    public final void a(NativeTrackingListener nativeTrackingListener) {
        this.d = nativeTrackingListener;
    }

    public a() {
    }

    public a(com.mintegral.msdk.mtgnative.d.a aVar, NativeTrackingListener nativeTrackingListener) {
        this.c = aVar;
        this.d = nativeTrackingListener;
    }

    public final void a(Context context, Map<String, Object> map) {
        this.f2800a = new b(this.c, this.d, map, context);
    }

    public final void a() {
        a(0, "");
    }

    public final void a(String str) {
        a(0, str);
    }

    public final void b() {
        a(1, "");
    }

    public static void c() {
        try {
            b.b();
        } catch (Exception unused) {
            g.d("NativeProvider", "clear cache failed");
        }
    }

    public final void d() {
        try {
            this.f2800a.a();
        } catch (Exception unused) {
            g.d("NativeProvider", "release failed");
        }
    }

    public final void a(View view, List<View> list, Campaign campaign) {
        g.b("NativeProvider", "native provider registerView");
        if (this.f2800a != null) {
            this.f2800a.a(campaign, view, list);
        }
    }

    public final void a(View view, Campaign campaign) {
        g.b("NativeProvider", "native provider registerView");
        if (this.f2800a != null) {
            this.f2800a.a(campaign, view);
        }
    }

    public final void b(View view, Campaign campaign) {
        g.b("NativeProvider", "native provider unregisterView");
        if (this.f2800a != null) {
            this.f2800a.b(campaign, view);
        }
    }

    public final void b(View view, List<View> list, Campaign campaign) {
        g.b("NativeProvider", "native provider unregisterView");
        if (this.f2800a != null) {
            this.f2800a.b(campaign, view, list);
        }
    }

    public static void preload(Map<String, Object> map, int i, AdMobClickListener adMobClickListener) {
        g.b("NativeProvider", "native provider preload");
        new c().a(map, i, adMobClickListener);
    }

    /* access modifiers changed from: private */
    public void e() {
        this.c.onAdLoadError("current request is loading");
        this.c.b();
    }

    private void a(int i, String str) {
        if (this.f2800a != null) {
            if (this.c == null || !this.c.a()) {
                if (this.c != null) {
                    this.c.b();
                }
                this.f2800a.a(i, str);
            } else if (k.e()) {
                e();
            } else {
                this.b.post(new Runnable() {
                    public final void run() {
                        a.this.e();
                    }
                });
            }
        }
    }
}
