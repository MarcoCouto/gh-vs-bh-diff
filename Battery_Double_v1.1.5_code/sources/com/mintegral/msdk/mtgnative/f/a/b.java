package com.mintegral.msdk.mtgnative.f.a;

import android.support.v4.app.NotificationCompat;
import com.mintegral.msdk.base.entity.CampaignUnit;
import com.mintegral.msdk.out.Frame;
import java.util.List;
import org.json.JSONObject;

/* compiled from: NativeResponseHandler */
public abstract class b extends com.mintegral.msdk.base.common.net.a.b {

    /* renamed from: a reason: collision with root package name */
    private int f2802a;
    private long b;
    private String c;

    public abstract void a(int i, String str);

    public void a(long j) {
    }

    public abstract void a(CampaignUnit campaignUnit);

    public abstract void b(List<Frame> list);

    public final /* synthetic */ void a(Object obj) {
        JSONObject jSONObject = (JSONObject) obj;
        if (this.f2802a == 0) {
            int optInt = jSONObject.optInt("status");
            if (1 == optInt) {
                a(System.currentTimeMillis() - this.b);
                CampaignUnit parseCampaignUnit = CampaignUnit.parseCampaignUnit(jSONObject.optJSONObject("data"));
                if (parseCampaignUnit == null || parseCampaignUnit.getAds() == null || parseCampaignUnit.getAds().size() <= 0) {
                    a(optInt, jSONObject.optString(NotificationCompat.CATEGORY_MESSAGE));
                } else {
                    a(parseCampaignUnit);
                }
            } else {
                a(optInt, jSONObject.optString(NotificationCompat.CATEGORY_MESSAGE));
            }
        } else {
            if (this.f2802a == 1) {
                int optInt2 = jSONObject.optInt("status");
                if (1 == optInt2) {
                    a(System.currentTimeMillis() - this.b);
                    CampaignUnit parseCampaignUnit2 = CampaignUnit.parseCampaignUnit(jSONObject.optJSONObject("data"));
                    if (parseCampaignUnit2 == null || parseCampaignUnit2.getListFrames() == null || parseCampaignUnit2.getListFrames().size() <= 0) {
                        a(optInt2, jSONObject.optString(NotificationCompat.CATEGORY_MESSAGE));
                    } else {
                        b(parseCampaignUnit2.getListFrames());
                    }
                } else {
                    a(optInt2, jSONObject.optString(NotificationCompat.CATEGORY_MESSAGE));
                }
            }
        }
    }

    public final void b(String str) {
        this.c = str;
    }

    public final String g() {
        return this.c;
    }

    public final void h(int i) {
        this.f2802a = i;
    }

    public final int h() {
        return this.f2802a;
    }

    public final void a() {
        super.a();
        this.b = System.currentTimeMillis();
    }

    public final void a(int i) {
        a(i, c(i));
    }
}
