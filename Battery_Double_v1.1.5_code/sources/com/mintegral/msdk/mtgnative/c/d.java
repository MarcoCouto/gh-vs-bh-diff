package com.mintegral.msdk.mtgnative.c;

import android.content.Context;
import android.text.TextUtils;
import com.mintegral.msdk.base.b.h;
import com.mintegral.msdk.base.b.i;
import com.mintegral.msdk.base.b.w;
import com.mintegral.msdk.base.common.d.c;
import com.mintegral.msdk.base.common.d.c.a;
import com.mintegral.msdk.base.common.d.c.b;
import com.mintegral.msdk.base.entity.p;
import com.mintegral.msdk.base.utils.g;
import java.util.List;

/* compiled from: NativeVideoTrackingReport */
public class d {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public static final String f2796a = "com.mintegral.msdk.mtgnative.c.d";

    public static void a(Context context, String str) {
        if (context != null) {
            try {
                w a2 = w.a((h) i.a(context));
                if (!TextUtils.isEmpty(str) && a2 != null && a2.c() > 0) {
                    List a3 = a2.a("2000022");
                    List a4 = a2.a("2000021");
                    List a5 = a2.a("2000043");
                    String b = p.b(a4);
                    String c = p.c(a3);
                    String d = p.d(a5);
                    StringBuilder sb = new StringBuilder();
                    if (!TextUtils.isEmpty(b)) {
                        sb.append(b);
                    }
                    if (!TextUtils.isEmpty(c)) {
                        sb.append(c);
                    }
                    if (!TextUtils.isEmpty(d)) {
                        sb.append(d);
                    }
                    if (!TextUtils.isEmpty(sb.toString())) {
                        String sb2 = sb.toString();
                        if (context != null && !TextUtils.isEmpty(sb2) && !TextUtils.isEmpty(str)) {
                            try {
                                a aVar = new a(context);
                                aVar.c();
                                aVar.b(com.mintegral.msdk.base.common.a.f, c.a(sb2, context, str), new b() {
                                    public final void a(String str) {
                                        g.d(d.f2796a, str);
                                    }

                                    public final void b(String str) {
                                        g.d(d.f2796a, str);
                                    }
                                });
                            } catch (Exception e) {
                                e.printStackTrace();
                                g.d(f2796a, e.getMessage());
                            }
                        }
                    }
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }
}
