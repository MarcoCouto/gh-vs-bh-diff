package com.mintegral.msdk.mtgnative.c;

import android.graphics.Rect;
import android.os.Handler;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnPreDrawListener;
import com.mintegral.msdk.base.utils.g;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

/* compiled from: ImpressionTracker */
public final class a {

    /* renamed from: a reason: collision with root package name */
    private WeakReference<ViewTreeObserver> f2758a;
    private List<View> b;
    private OnPreDrawListener c = null;
    private C0061a d;
    private Handler e;
    private boolean f;

    /* renamed from: com.mintegral.msdk.mtgnative.c.a$a reason: collision with other inner class name */
    /* compiled from: ImpressionTracker */
    public interface C0061a {
        void a(ArrayList<View> arrayList);
    }

    public a(List<View> list, C0061a aVar, Handler handler) {
        this.d = aVar;
        this.e = handler;
        this.b = list;
        try {
            b();
        } catch (Throwable th) {
            g.c("ImpressionTracker", th.getMessage(), th);
        }
        try {
            this.c = new OnPreDrawListener() {
                public final boolean onPreDraw() {
                    a.this.b();
                    return true;
                }
            };
        } catch (Throwable th2) {
            g.c("ImpressionTracker", th2.getMessage(), th2);
        }
    }

    /* access modifiers changed from: private */
    public void b() {
        if (!this.f) {
            if (this.e != null) {
                this.e.postDelayed(new Runnable() {
                    public final void run() {
                        a.b(a.this);
                    }
                }, 100);
            }
            this.f = true;
        }
    }

    public final void a(View view) {
        View view2 = null;
        if (view != null) {
            view2 = f.a(view.getContext(), view);
        } else if (this.b != null && this.b.size() > 0) {
            for (int i = 0; i < this.b.size(); i++) {
                View view3 = (View) this.b.get(i);
                if (view3 != null) {
                    view2 = f.a(view3.getContext(), view3);
                    if (view2 != null) {
                        break;
                    }
                }
            }
        }
        if (view2 != null) {
            ViewTreeObserver viewTreeObserver = view2.getViewTreeObserver();
            if (viewTreeObserver == null || viewTreeObserver.isAlive()) {
                this.f2758a = new WeakReference<>(viewTreeObserver);
                if (this.c != null) {
                    viewTreeObserver.addOnPreDrawListener(this.c);
                }
            }
        }
    }

    public final void a() {
        try {
            this.f = false;
            if (!(this.f2758a == null || this.f2758a.get() == null)) {
                ViewTreeObserver viewTreeObserver = (ViewTreeObserver) this.f2758a.get();
                if (viewTreeObserver != null && viewTreeObserver.isAlive()) {
                    viewTreeObserver.removeOnPreDrawListener(this.c);
                }
                this.f2758a.clear();
            }
            this.d = null;
            this.c = null;
            if (this.b != null) {
                this.b.clear();
            }
            this.b = null;
        } catch (Throwable unused) {
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:24:0x006f A[SYNTHETIC, Splitter:B:24:0x006f] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0073 A[Catch:{ Exception -> 0x0092 }] */
    static /* synthetic */ void b(a aVar) {
        boolean z;
        try {
            aVar.f = false;
            if (aVar.b != null && aVar.b.size() > 0) {
                ArrayList arrayList = new ArrayList();
                ArrayList arrayList2 = new ArrayList();
                for (int i = 0; i < aVar.b.size(); i++) {
                    View view = (View) aVar.b.get(i);
                    if (view != null) {
                        if (view.getVisibility() == 0) {
                            Rect rect = new Rect();
                            if (view.getGlobalVisibleRect(rect)) {
                                long height = (long) (rect.height() * rect.width());
                                long height2 = (long) (view.getHeight() * view.getWidth());
                                if (height2 > 0) {
                                    double d2 = (double) height;
                                    double d3 = (double) height2;
                                    Double.isNaN(d3);
                                    if (d2 > d3 * 0.4d) {
                                        z = true;
                                        if (!z) {
                                            arrayList.add(view);
                                        } else {
                                            arrayList2.add(view);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    z = false;
                    if (!z) {
                    }
                }
                if (aVar.d != null) {
                    aVar.d.a(arrayList);
                }
                if (arrayList.size() > 0) {
                    aVar.a();
                }
                arrayList.clear();
                arrayList2.clear();
            }
        } catch (Exception unused) {
        }
    }
}
