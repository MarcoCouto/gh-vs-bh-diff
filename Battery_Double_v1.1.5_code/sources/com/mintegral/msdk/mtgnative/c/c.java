package com.mintegral.msdk.mtgnative.c;

import android.content.Context;
import android.os.Build.VERSION;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.b.d;
import com.mintegral.msdk.base.b.i;
import com.mintegral.msdk.base.b.m;
import com.mintegral.msdk.base.common.e.a.C0049a;
import com.mintegral.msdk.base.common.net.l;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.entity.CampaignUnit;
import com.mintegral.msdk.base.entity.h;
import com.mintegral.msdk.base.entity.k;
import com.mintegral.msdk.base.utils.CommonMD5;
import com.mintegral.msdk.base.utils.e;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.mtgnative.a.f;
import com.mintegral.msdk.out.AdMobClickListener;
import com.mintegral.msdk.out.Campaign;
import com.mintegral.msdk.out.Frame;
import com.mintegral.msdk.out.PreloadListener;
import com.vungle.warren.model.AdvertisementDBAdapter.AdvertisementColumns;
import io.fabric.sdk.android.services.events.EventsFilesManager;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.UUID;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: NativePreloadController */
public class c {
    private static int A = -1;
    private static int B = -2;
    /* access modifiers changed from: private */
    public static final String d = "c";
    /* access modifiers changed from: private */
    public static Map<String, Map<Long, Object>> e = new HashMap();
    private static Map<String, Boolean> f = new HashMap();
    private static Map<String, k> g = new HashMap();
    /* access modifiers changed from: private */
    public static Map<String, Integer> h = new HashMap();
    private static Map<String, Integer> i = new HashMap();
    private static c j = null;
    /* access modifiers changed from: private */
    public boolean C;
    private int D;
    private int E;
    /* access modifiers changed from: private */
    public com.mintegral.msdk.base.common.e.b F = new com.mintegral.msdk.base.common.e.b(com.mintegral.msdk.base.controller.a.d().h());
    /* access modifiers changed from: private */
    public Map<String, Boolean> G = new HashMap();

    /* renamed from: a reason: collision with root package name */
    Queue<Integer> f2784a = null;
    Queue<Long> b = null;
    protected List<Integer> c;
    private com.mintegral.msdk.b.c k;
    private com.mintegral.msdk.click.a l;
    /* access modifiers changed from: private */
    public d m;
    private String n;
    /* access modifiers changed from: private */
    public Handler o;
    private int p = 0;
    private e q;
    private h r;
    private boolean s;
    private boolean t;
    /* access modifiers changed from: private */
    public String u = "";
    /* access modifiers changed from: private */
    public i v;
    private Map<String, Object> w;
    private List<Integer> x;
    private List<Integer> y;
    /* access modifiers changed from: private */
    public boolean z;

    /* compiled from: NativePreloadController */
    public class a extends com.mintegral.msdk.mtgnative.f.a.b implements com.mintegral.msdk.base.common.e.c {
        /* access modifiers changed from: private */
        public int b;
        /* access modifiers changed from: private */
        public int c;
        /* access modifiers changed from: private */
        public int d;
        /* access modifiers changed from: private */
        public int e;
        /* access modifiers changed from: private */
        public int f;
        /* access modifiers changed from: private */
        public String g;
        /* access modifiers changed from: private */
        public AdMobClickListener h;
        private List<String> i = null;
        /* access modifiers changed from: private */
        public com.mintegral.msdk.base.common.e.d j;
        /* access modifiers changed from: private */
        public boolean k = false;
        /* access modifiers changed from: private */
        public Runnable l;
        /* access modifiers changed from: private */
        public boolean m = true;
        /* access modifiers changed from: private */
        public com.mintegral.msdk.a.a.a n;

        public final void a(AdMobClickListener adMobClickListener) {
            this.h = adMobClickListener;
        }

        public final void a(String str) {
            this.g = str;
        }

        public final void a(List<String> list) {
            this.i = list;
        }

        public final void a(long j2) {
            super.a(j2);
            if (this.j != null) {
                this.j.a(String.valueOf(j2));
            }
        }

        public final void d(int i2) {
            this.f = i2;
        }

        public final void e(int i2) {
            this.d = i2;
        }

        public final void f(int i2) {
            this.e = i2;
        }

        public final void g(int i2) {
            this.c = i2;
        }

        public a(int i2) {
            this.b = i2;
        }

        public final void a(com.mintegral.msdk.base.common.e.d dVar) {
            this.j = dVar;
        }

        public final void a(com.mintegral.msdk.a.a.a aVar) {
            this.n = aVar;
        }

        public final void f() {
            this.m = true;
        }

        public final void a_() {
            this.k = true;
        }

        public final void a(final CampaignUnit campaignUnit) {
            c.this.C = true;
            c.a(true, this.n, (String) null);
            c.a(new Thread(new Runnable() {
                public final void run() {
                    boolean z;
                    int i = 1;
                    if (!MIntegralConstans.PRELOAD_RESULT_IN_SUBTHREAD || Looper.myLooper() != null) {
                        z = false;
                    } else {
                        Looper.prepare();
                        z = true;
                    }
                    if (a.this.l != null) {
                        g.b(c.d, "REMOVE CANCEL TASK ON SUCCESS");
                        c.this.o.removeCallbacks(a.this.l);
                    }
                    g.d(c.d, "onSuccess");
                    com.mintegral.msdk.base.utils.k.a((List<CampaignEx>) campaignUnit.getAds());
                    Map g = c.e;
                    StringBuilder sb = new StringBuilder("0_");
                    sb.append(a.this.g);
                    if (g.containsKey(sb.toString())) {
                        Map g2 = c.e;
                        StringBuilder sb2 = new StringBuilder("0_");
                        sb2.append(a.this.g);
                        g2.remove(sb2.toString());
                    }
                    if (a.this.j != null && campaignUnit.getAds().size() > 0) {
                        a.this.j.a(campaignUnit.getAds().size());
                        a.this.j.a();
                    }
                    if (a.this.f > 0) {
                        if (campaignUnit.ads.size() > a.this.f) {
                            a.this.c = a.this.f;
                        } else {
                            a.this.c = campaignUnit.ads.size();
                        }
                    } else if (a.this.f == -1) {
                        a.this.c = 0;
                    } else if (a.this.f == -3) {
                        a.this.c = campaignUnit.ads.size();
                    } else if (a.this.f == -2) {
                        if (campaignUnit.getTemplate() == 3) {
                            if (a.this.e != 0) {
                                a.this.c = a.this.e;
                            }
                        } else if (a.this.d != 0) {
                            a.this.c = a.this.d;
                        }
                        if (a.this.c <= 0) {
                            a.this.c = ((Integer) c.h.get(a.this.g)).intValue();
                        }
                    }
                    if (campaignUnit.ads.size() < a.this.c) {
                        a.this.c = campaignUnit.ads.size();
                    }
                    ArrayList arrayList = new ArrayList();
                    ArrayList arrayList2 = new ArrayList();
                    boolean z2 = false;
                    for (int i2 = 0; i2 < campaignUnit.ads.size(); i2++) {
                        CampaignEx campaignEx = (CampaignEx) campaignUnit.ads.get(i2);
                        campaignEx.setCampaignUnitId(a.this.g);
                        boolean a2 = com.mintegral.msdk.base.utils.k.a(com.mintegral.msdk.base.controller.a.d().h(), campaignEx.getPackageName());
                        if (a2 && com.mintegral.msdk.base.controller.a.c() != null) {
                            com.mintegral.msdk.base.controller.a.c().add(new com.mintegral.msdk.base.entity.g(campaignEx.getId(), campaignEx.getPackageName()));
                            z2 = true;
                        }
                        if (arrayList.size() < a.this.c && campaignEx.getOfferType() != 99) {
                            if (!a2) {
                                arrayList.add(campaignEx);
                                if (!TextUtils.isEmpty(campaignEx.getVideoUrlEncode())) {
                                    arrayList2.add(campaignEx);
                                }
                            } else if (com.mintegral.msdk.base.utils.k.b(campaignEx) || com.mintegral.msdk.base.utils.k.a(campaignEx)) {
                                arrayList.add(campaignEx);
                                if (!TextUtils.isEmpty(campaignEx.getVideoUrlEncode())) {
                                    arrayList2.add(campaignEx);
                                }
                            }
                        }
                        c.a(c.this.z, (Campaign) campaignEx);
                    }
                    c.a((List) arrayList2, a.this.g);
                    if (z2) {
                        com.mintegral.msdk.base.controller.a.d().f();
                    }
                    if (campaignUnit.getAds().get(0) != null) {
                        i = ((CampaignEx) campaignUnit.getAds().get(0)).getType();
                    }
                    com.mintegral.msdk.mtgnative.a.b a3 = f.a(i);
                    if (a3 != null) {
                        a3.a(a.this.g, arrayList);
                    }
                    c.a(a.this.b, a.this.g);
                    if (Looper.myLooper() != null && z) {
                        Looper.loop();
                    }
                }
            }));
        }

        public final void a(final int i2, final String str) {
            c.a(new Thread(new Runnable() {
                public final void run() {
                    boolean z;
                    if (!MIntegralConstans.PRELOAD_RESULT_IN_SUBTHREAD || Looper.myLooper() != null) {
                        z = false;
                    } else {
                        Looper.prepare();
                        z = true;
                    }
                    if (!a.this.k) {
                        if (a.this.l != null) {
                            g.b(c.d, "REMOVE CANCEL TASK ON onFailed");
                            c.this.o.removeCallbacks(a.this.l);
                        }
                        if (a.this.h() == 1 || a.this.m) {
                            c.this.a(str, a.this.h(), a.this.g, a.this.n, a.this.h);
                        }
                    } else if (a.this.m) {
                        c.this.a(str, a.this.h(), a.this.g, a.this.n, a.this.h);
                    }
                    if (i2 == -1) {
                        c.b(a.this.b, a.this.g);
                    }
                    if (Looper.myLooper() != null && z) {
                        Looper.loop();
                    }
                }
            }));
        }

        public final void a(Runnable runnable) {
            this.l = runnable;
        }

        public final void b(final List<Frame> list) {
            c.a(new Thread(new Runnable() {
                public final void run() {
                    boolean z;
                    if (!MIntegralConstans.PRELOAD_RESULT_IN_SUBTHREAD || Looper.myLooper() != null) {
                        z = false;
                    } else {
                        Looper.prepare();
                        z = true;
                    }
                    if (!a.this.k && a.this.l != null) {
                        g.b(c.d, "REMOVE CANCEL TASK ON onAdLoaded");
                        c.this.o.removeCallbacks(a.this.l);
                    }
                    if (a.this.j != null && list.size() > 0) {
                        a.this.j.a(list.size());
                        a.this.j.a();
                    }
                    if (list != null && list.size() > 0) {
                        for (Frame campaigns : list) {
                            for (Campaign a2 : campaigns.getCampaigns()) {
                                c.a(c.this.z, a2);
                            }
                        }
                        HashMap hashMap = new HashMap();
                        hashMap.put(Long.valueOf(System.currentTimeMillis()), list);
                        Map g = c.e;
                        StringBuilder sb = new StringBuilder("1_");
                        sb.append(a.this.g);
                        if (g.containsKey(sb.toString())) {
                            Map g2 = c.e;
                            StringBuilder sb2 = new StringBuilder("1_");
                            sb2.append(a.this.g);
                            g2.remove(sb2.toString());
                        }
                        Map g3 = c.e;
                        StringBuilder sb3 = new StringBuilder("1_");
                        sb3.append(a.this.g);
                        g3.put(sb3.toString(), hashMap);
                    }
                    if (Looper.myLooper() != null && z) {
                        Looper.loop();
                    }
                }
            }));
        }
    }

    /* compiled from: NativePreloadController */
    public class b implements Runnable {
        private int b = 1;
        private com.mintegral.msdk.base.common.e.c c;
        private com.mintegral.msdk.base.common.e.a d;
        private int e;
        private String f;
        private com.mintegral.msdk.a.a.a g;
        private boolean h = false;
        private AdMobClickListener i;

        public final void a(AdMobClickListener adMobClickListener) {
            this.i = adMobClickListener;
        }

        public final void a(boolean z) {
            this.h = z;
        }

        public final void a(com.mintegral.msdk.a.a.a aVar) {
            this.g = aVar;
        }

        public final void a(String str) {
            this.f = str;
        }

        public b(com.mintegral.msdk.base.common.e.c cVar, int i2, String str) {
            this.c = cVar;
            this.e = i2;
            this.f = str;
        }

        public final void run() {
            String f2 = c.d;
            StringBuilder sb = new StringBuilder("cancel task adsource is = ");
            sb.append(this.b);
            g.b(f2, sb.toString());
            this.c.a_();
            switch (this.b) {
                case 1:
                    c.this.C = true;
                    if (this.h || this.e == 1) {
                        c.this.a("REQUEST_TIMEOUT", this.e, this.f, this.g, this.i);
                        return;
                    }
                case 2:
                    c.this.a("REQUEST_TIMEOUT", this.e, this.f, this.g, this.i);
                    break;
            }
        }

        public final void a(com.mintegral.msdk.base.common.e.a aVar) {
            this.d = aVar;
        }
    }

    public c() {
        if (Looper.myLooper() == null) {
            Looper.prepare();
        }
        this.o = new Handler() {
            public final void handleMessage(Message message) {
            }
        };
    }

    /* JADX WARNING: type inference failed for: r14v1, types: [java.lang.Object] */
    /* JADX WARNING: type inference failed for: r9v2, types: [java.util.List] */
    /* JADX WARNING: type inference failed for: r9v3 */
    /* JADX WARNING: type inference failed for: r9v5, types: [java.util.List] */
    /* JADX WARNING: type inference failed for: r14v2 */
    /* JADX WARNING: type inference failed for: r14v3 */
    /* JADX WARNING: type inference failed for: r9v10 */
    /* JADX WARNING: type inference failed for: r14v4 */
    /* JADX WARNING: type inference failed for: r14v5 */
    /* JADX WARNING: type inference failed for: r14v6 */
    /* JADX WARNING: Can't wrap try/catch for region: R(2:64|65) */
    /* JADX WARNING: Code restructure failed: missing block: B:167:?, code lost:
        com.mintegral.msdk.base.utils.g.d(d, "init cam cache failed");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:168:0x0455, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:?, code lost:
        com.mintegral.msdk.base.utils.g.d(d, "ADNUM MUST BE INTEGER");
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* JADX WARNING: Missing exception handler attribute for start block: B:166:0x044e */
    /* JADX WARNING: Missing exception handler attribute for start block: B:64:0x01b1 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:127:0x0317 A[Catch:{ Exception -> 0x045f }] */
    /* JADX WARNING: Removed duplicated region for block: B:128:0x0322 A[Catch:{ Exception -> 0x045f }] */
    /* JADX WARNING: Removed duplicated region for block: B:134:0x034a A[Catch:{ Throwable -> 0x044e }] */
    /* JADX WARNING: Removed duplicated region for block: B:155:0x03be A[Catch:{ Throwable -> 0x044e }] */
    /* JADX WARNING: Removed duplicated region for block: B:171:0x0459 A[Catch:{ Exception -> 0x045f }] */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x020d A[Catch:{ Exception -> 0x045f }] */
    /* JADX WARNING: Removed duplicated region for block: B:85:0x022e A[Catch:{ Exception -> 0x045f }] */
    /* JADX WARNING: Unknown variable types count: 4 */
    public final void a(Map<String, Object> map, int i2, AdMobClickListener adMobClickListener) {
        com.mintegral.msdk.a.a.a aVar;
        String str;
        String str2;
        com.mintegral.msdk.a.a.a aVar2;
        ? r14;
        com.mintegral.msdk.a.a.a aVar3;
        ? r9;
        Map<String, Object> map2 = map;
        int i3 = i2;
        this.w = map2;
        this.C = false;
        if (!map2.containsKey(MIntegralConstans.PROPERTIES_UNIT_ID)) {
            g.c(d, "preload error,make sure you have unitid");
            return;
        }
        String str3 = (String) map2.get(MIntegralConstans.PROPERTIES_UNIT_ID);
        int i4 = (i3 + 1) % 2;
        Map<String, Map<Long, Object>> map3 = e;
        StringBuilder sb = new StringBuilder();
        sb.append(i4);
        sb.append(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
        sb.append(str3);
        if (map3.containsKey(sb.toString())) {
            Map<String, Map<Long, Object>> map4 = e;
            StringBuilder sb2 = new StringBuilder();
            sb2.append(i3);
            sb2.append(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
            sb2.append(str3);
            if (!map4.containsKey(sb2.toString())) {
                g.d(d, "An id can have only one AD form");
                return;
            }
        }
        if (TextUtils.isEmpty(str3)) {
            g.c(d, "preload error,make sure you have correct unitid");
            return;
        }
        if (map2.containsKey(MIntegralConstans.PREIMAGE)) {
            this.z = ((Boolean) map2.get(MIntegralConstans.PREIMAGE)).booleanValue();
        }
        if (map2.containsKey(MIntegralConstans.CANCEL_ADMOB_AUTO_DOWNLOAD_IMAGE) && (map2.get(MIntegralConstans.CANCEL_ADMOB_AUTO_DOWNLOAD_IMAGE) instanceof Boolean)) {
            this.t = ((Boolean) map2.get(MIntegralConstans.CANCEL_ADMOB_AUTO_DOWNLOAD_IMAGE)).booleanValue();
        }
        if (this.l == null) {
            this.l = new com.mintegral.msdk.click.a(com.mintegral.msdk.base.controller.a.d().h(), str3);
        } else {
            this.l.a(str3);
        }
        if (map2.containsKey(MIntegralConstans.PROPERTIES_AD_FRAME_NUM)) {
            this.p = ((Integer) map2.get(MIntegralConstans.PROPERTIES_AD_FRAME_NUM)).intValue();
        }
        if (map2.containsKey(MIntegralConstans.PROPERTIES_API_REUQEST_CATEGORY)) {
            this.n = (String) map2.get(MIntegralConstans.PROPERTIES_API_REUQEST_CATEGORY);
        }
        if (map2.containsKey(MIntegralConstans.FB_MEDIA_CACHE_FLAG)) {
            this.s = ((Boolean) map2.get(MIntegralConstans.FB_MEDIA_CACHE_FLAG)).booleanValue();
        }
        if (f.containsKey(str3) && ((Boolean) f.get(str3)).booleanValue()) {
            Map<String, Map<Long, Object>> map5 = e;
            StringBuilder sb3 = new StringBuilder();
            sb3.append(i3);
            sb3.append(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
            sb3.append(str3);
            Map map6 = (Map) map5.get(sb3.toString());
            com.mintegral.msdk.b.b.a();
            com.mintegral.msdk.b.a b2 = com.mintegral.msdk.b.b.b(com.mintegral.msdk.base.controller.a.d().j());
            if (map6 != null && map6.size() > 0) {
                Long l2 = (Long) map6.keySet().iterator().next();
                long currentTimeMillis = System.currentTimeMillis();
                if (b2 == null) {
                    com.mintegral.msdk.b.b.a();
                    b2 = com.mintegral.msdk.b.b.b();
                }
                if (currentTimeMillis - l2.longValue() >= b2.aF() * 1000) {
                    map5.remove(str3);
                } else if (i3 == 1) {
                    return;
                }
            }
        }
        f.put(str3, Boolean.valueOf(true));
        this.E = 1;
        try {
            if (map2.containsKey("ad_num")) {
                this.E = ((Integer) map2.get("ad_num")).intValue();
                if (this.E <= 0) {
                    this.E = 1;
                }
                if (this.E > 10) {
                    this.E = 10;
                }
            }
            h.put(str3, Integer.valueOf(this.E));
            if (map2.containsKey(MIntegralConstans.PRELOAD_RESULT_LISTENER)) {
                Object obj = map2.get(MIntegralConstans.PRELOAD_RESULT_LISTENER);
                if (obj != null) {
                    aVar = new com.mintegral.msdk.a.a.a((PreloadListener) obj);
                    if (map2.containsKey("app_id") || !map2.containsKey(MIntegralConstans.APP_KEY) || !map2.containsKey(MIntegralConstans.KEY_WORD)) {
                        str2 = null;
                        str = null;
                    } else {
                        str2 = (String) map2.get("app_id");
                        str = (String) map2.get(MIntegralConstans.APP_KEY);
                    }
                    if (this.k == null) {
                        this.k = new com.mintegral.msdk.b.c();
                    }
                    this.k.a(com.mintegral.msdk.base.controller.a.d().h(), str2, str, str3);
                    com.mintegral.msdk.b.b.a();
                    this.m = com.mintegral.msdk.b.b.c(str2, str3);
                    if (this.m == null) {
                        this.m = d.b(str3);
                    }
                    this.x = this.m.t();
                    this.c = this.m.u();
                    this.y = this.m.u();
                    if (this.x != null || this.x.size() <= 0) {
                        aVar2 = aVar;
                        if (aVar2 != null) {
                            aVar2.onPreloadFaild("don't have sorceList");
                        }
                    }
                    this.f2784a = new LinkedList();
                    for (Integer add : this.x) {
                        this.f2784a.add(add);
                    }
                    if (this.y != null && this.y.size() > 0) {
                        this.b = new LinkedList();
                        for (Integer intValue : this.y) {
                            this.b.add(Long.valueOf((long) (intValue.intValue() * 1000)));
                        }
                    }
                    this.r = new h();
                    this.r.b(str3);
                    if (this.x.contains(Integer.valueOf(1)) && i3 == 0) {
                        try {
                            com.mintegral.msdk.a.a.a aVar4 = aVar;
                            ? r142 = 0;
                            try {
                                a(1, (long) (((Integer) this.c.get(this.x.indexOf(Integer.valueOf(1)))).intValue() * 1000), i2, str3, false, aVar, adMobClickListener);
                                com.mintegral.msdk.mtgnative.a.b a2 = f.a(1);
                                if (a2 != null) {
                                    List list = (List) a2.b(str3, 0);
                                    if (list != null && list.size() > 0) {
                                        aVar3 = aVar4;
                                        try {
                                            a(true, aVar3, (String) null);
                                            r14 = r142;
                                        } catch (Exception unused) {
                                            r14 = r142;
                                        }
                                        a(i3, str3, aVar3, adMobClickListener);
                                        i.put(str3, Integer.valueOf(this.m != null ? this.m.x() * this.E : 1));
                                        Class.forName("com.mintegral.msdk.nativex.view.MTGMediaView");
                                        Class.forName("com.mintegral.msdk.videocommon.download.c");
                                        d.a(com.mintegral.msdk.base.controller.a.d().h(), str3);
                                        e.b();
                                        if (!TextUtils.isEmpty(str3)) {
                                            com.mintegral.msdk.base.b.f.a((com.mintegral.msdk.base.b.h) i.a(com.mintegral.msdk.base.controller.a.d().h())).c();
                                            int a3 = a(map2.containsKey(MIntegralConstans.NATIVE_INFO) ? (String) map2.get(MIntegralConstans.NATIVE_INFO) : r14);
                                            if (a3 <= 0) {
                                                a3 = this.E;
                                            }
                                            com.mintegral.msdk.b.b.a();
                                            this.m = com.mintegral.msdk.b.b.c("", str3);
                                            if (this.m == null) {
                                                this.m = d.b(str3);
                                            }
                                            this.x = this.m.t();
                                            if (this.x != null && this.x.size() > 0 && this.x.contains(Integer.valueOf(1))) {
                                                com.mintegral.msdk.mtgnative.a.b a4 = f.a(1);
                                                if (a4 != null) {
                                                    r9 = (List) a4.b(str3, a3);
                                                    if (r9 != 0) {
                                                        ArrayList arrayList = new ArrayList();
                                                        for (int i5 = 0; i5 < r9.size(); i5++) {
                                                            CampaignEx campaignEx = (CampaignEx) r9.get(i5);
                                                            if (!TextUtils.isEmpty(campaignEx.getVideoUrlEncode())) {
                                                                arrayList.add(campaignEx);
                                                            }
                                                        }
                                                        if (arrayList.size() > 0) {
                                                            Class cls = Class.forName("com.mintegral.msdk.videocommon.download.c");
                                                            Object invoke = cls.getMethod("getInstance", new Class[0]).invoke(r14, new Object[0]);
                                                            cls.getMethod("createUnitCache", new Class[]{Context.class, String.class, List.class, Integer.TYPE, Class.forName("com.mintegral.msdk.videocommon.listener.a")}).invoke(invoke, new Object[]{com.mintegral.msdk.base.controller.a.d().h(), str3, arrayList, Integer.valueOf(1), r14});
                                                            cls.getMethod("load", new Class[]{String.class}).invoke(invoke, new Object[]{str3});
                                                        }
                                                    }
                                                }
                                            }
                                            r9 = r14;
                                            if (r9 != 0) {
                                            }
                                        }
                                        return;
                                    }
                                }
                            } catch (Exception unused2) {
                            }
                            aVar3 = aVar4;
                            r14 = r142;
                        } catch (Exception unused3) {
                        }
                        a(i3, str3, aVar3, adMobClickListener);
                        i.put(str3, Integer.valueOf(this.m != null ? this.m.x() * this.E : 1));
                        Class.forName("com.mintegral.msdk.nativex.view.MTGMediaView");
                        Class.forName("com.mintegral.msdk.videocommon.download.c");
                        d.a(com.mintegral.msdk.base.controller.a.d().h(), str3);
                        e.b();
                        if (!TextUtils.isEmpty(str3)) {
                        }
                        return;
                    }
                    aVar3 = aVar;
                    r14 = 0;
                    a(i3, str3, aVar3, adMobClickListener);
                    i.put(str3, Integer.valueOf(this.m != null ? this.m.x() * this.E : 1));
                    Class.forName("com.mintegral.msdk.nativex.view.MTGMediaView");
                    Class.forName("com.mintegral.msdk.videocommon.download.c");
                    d.a(com.mintegral.msdk.base.controller.a.d().h(), str3);
                    e.b();
                    if (!TextUtils.isEmpty(str3)) {
                    }
                    return;
                }
            }
            aVar = null;
            if (map2.containsKey("app_id")) {
            }
            str2 = null;
            str = null;
            if (this.k == null) {
            }
            this.k.a(com.mintegral.msdk.base.controller.a.d().h(), str2, str, str3);
            com.mintegral.msdk.b.b.a();
            this.m = com.mintegral.msdk.b.b.c(str2, str3);
            if (this.m == null) {
            }
            this.x = this.m.t();
            this.c = this.m.u();
            this.y = this.m.u();
            if (this.x != null) {
            }
            aVar2 = aVar;
            if (aVar2 != null) {
            }
        } catch (Exception e2) {
            g.d(d, com.mintegral.msdk.mtgnative.b.a.a(e2));
        }
    }

    private static int a(String str) {
        if (str == null) {
            return 0;
        }
        try {
            JSONArray jSONArray = new JSONArray(str);
            if (jSONArray.length() > 0) {
                for (int i2 = 0; i2 < jSONArray.length(); i2++) {
                    JSONObject jSONObject = (JSONObject) jSONArray.opt(i2);
                    if (2 == jSONObject.optInt("id", 0)) {
                        return jSONObject.optInt("ad_num");
                    }
                }
            }
        } catch (Exception e2) {
            g.d(d, com.mintegral.msdk.mtgnative.b.a.a(e2));
        }
        return 0;
    }

    private void a(int i2, String str, com.mintegral.msdk.a.a.a aVar, AdMobClickListener adMobClickListener) {
        if (this.f2784a != null && this.f2784a.size() > 0) {
            try {
                int intValue = ((Integer) this.f2784a.poll()).intValue();
                long j2 = (long) MIntegralConstans.REQUEST_TIME_OUT;
                if (this.b != null && this.b.size() > 0) {
                    j2 = ((Long) this.b.poll()).longValue();
                }
                String str2 = d;
                StringBuilder sb = new StringBuilder("preload start queue adsource = ");
                sb.append(intValue);
                g.b(str2, sb.toString());
                if (intValue != 1) {
                    com.mintegral.msdk.mtgnative.a.b a2 = f.a(intValue);
                    if (a2 != null) {
                        List list = (List) a2.b(str, this.E);
                        if (list != null && list.size() > 0) {
                            a(true, aVar, (String) null);
                            return;
                        }
                    }
                }
                switch (intValue) {
                    case 1:
                        a(intValue, j2, i2, str, true, aVar, adMobClickListener);
                        return;
                    case 2:
                        a(2, j2, i2, str, aVar, false, adMobClickListener);
                        return;
                    default:
                        a(intValue, j2, i2, str, aVar, false, adMobClickListener);
                        return;
                }
            } catch (Throwable unused) {
                g.d(d, "queue poll exception");
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:128:0x029b A[Catch:{ Exception -> 0x04d1 }] */
    /* JADX WARNING: Removed duplicated region for block: B:140:0x02ea A[Catch:{ Exception -> 0x04d1 }] */
    /* JADX WARNING: Removed duplicated region for block: B:148:0x030a A[Catch:{ Exception -> 0x04d1 }, LOOP:2: B:146:0x0304->B:148:0x030a, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:151:0x031e A[Catch:{ Exception -> 0x04d1 }] */
    /* JADX WARNING: Removed duplicated region for block: B:154:0x032f A[Catch:{ Exception -> 0x04d1 }] */
    /* JADX WARNING: Removed duplicated region for block: B:168:0x0368 A[Catch:{ Exception -> 0x04d1 }] */
    /* JADX WARNING: Removed duplicated region for block: B:171:0x037f A[Catch:{ Exception -> 0x04d1 }] */
    /* JADX WARNING: Removed duplicated region for block: B:173:0x0382 A[Catch:{ Exception -> 0x04d1 }] */
    /* JADX WARNING: Removed duplicated region for block: B:176:0x03b8 A[Catch:{ Exception -> 0x04d1 }] */
    /* JADX WARNING: Removed duplicated region for block: B:179:0x03c7 A[Catch:{ Exception -> 0x04d1 }] */
    /* JADX WARNING: Removed duplicated region for block: B:192:0x0472 A[Catch:{ Exception -> 0x04d1 }] */
    /* JADX WARNING: Removed duplicated region for block: B:195:0x0477 A[Catch:{ Exception -> 0x04d1 }] */
    /* JADX WARNING: Removed duplicated region for block: B:199:0x04a0 A[Catch:{ Exception -> 0x04d1 }] */
    /* JADX WARNING: Removed duplicated region for block: B:200:0x04a9 A[Catch:{ Exception -> 0x04d1 }] */
    public final void a(int i2, long j2, int i3, String str, com.mintegral.msdk.a.a.a aVar, boolean z2, AdMobClickListener adMobClickListener) {
        String str2;
        int i4;
        int i5;
        int i6;
        JSONArray jSONArray;
        List<Long> g2;
        int i7;
        boolean z3;
        int i8;
        com.mintegral.msdk.base.common.e.a aVar2;
        int i9;
        String str3;
        String str4;
        int i10;
        int i11;
        int i12 = i2;
        long j3 = j2;
        int i13 = i3;
        String str5 = str;
        com.mintegral.msdk.a.a.a aVar3 = aVar;
        boolean z4 = z2;
        AdMobClickListener adMobClickListener2 = adMobClickListener;
        try {
            com.mintegral.msdk.base.b.f.a((com.mintegral.msdk.base.b.h) i.a(com.mintegral.msdk.base.controller.a.d().h())).c();
            int q2 = this.m.q();
            int r2 = this.m.r();
            g.b(d, "START LOAD MTG MVNATIVE");
            com.mintegral.msdk.mtgnative.f.a.a aVar4 = new com.mintegral.msdk.mtgnative.f.a.a(com.mintegral.msdk.base.controller.a.d().h());
            l lVar = new l();
            String j4 = com.mintegral.msdk.base.controller.a.d().j();
            String k2 = com.mintegral.msdk.base.controller.a.d().k();
            String str6 = j4;
            if (this.w == null || !this.w.containsKey("app_id") || !this.w.containsKey(MIntegralConstans.APP_KEY) || !this.w.containsKey(MIntegralConstans.KEY_WORD) || this.w.get(MIntegralConstans.KEY_WORD) == null) {
                str2 = str6;
            } else {
                String str7 = this.w.get("app_id") instanceof String ? (String) this.w.get("app_id") : str6;
                if (this.w.get(MIntegralConstans.APP_KEY) instanceof String) {
                    k2 = (String) this.w.get(MIntegralConstans.APP_KEY);
                }
                String str8 = this.w.get(MIntegralConstans.KEY_WORD) instanceof String ? (String) this.w.get(MIntegralConstans.KEY_WORD) : null;
                if (!TextUtils.isEmpty(str8)) {
                    lVar.a("smart", com.mintegral.msdk.base.utils.a.b(str8));
                }
                str2 = str7;
            }
            lVar.a("app_id", str2);
            lVar.a(MIntegralConstans.PROPERTIES_UNIT_ID, str5);
            com.mintegral.msdk.mtgnative.f.a.a aVar5 = aVar4;
            lVar.a("req_type", "1");
            if (!TextUtils.isEmpty(this.n)) {
                lVar.a("category", this.n);
            }
            StringBuilder sb = new StringBuilder();
            sb.append(str2);
            sb.append(k2);
            lVar.a("sign", CommonMD5.getMD5(sb.toString()));
            lVar.a("only_impression", "1");
            int intValue = ((Integer) h.get(str5)).intValue();
            if (!(q2 == A || q2 == 0)) {
                if (intValue != 0) {
                    if (i13 == 0) {
                        com.mintegral.msdk.mtgnative.a.b a2 = f.a(i2);
                        if (z4) {
                            if (a2 != null) {
                                List list = (List) a2.b(str5, intValue);
                                if (list != null && list.size() > 0) {
                                    a(true, aVar3, (String) null);
                                    return;
                                }
                            }
                            if (this.C) {
                                a("", i3, str, aVar, adMobClickListener);
                            }
                            return;
                        } else if (!(i12 == 1 || a2 == null)) {
                            List list2 = (List) a2.b(str5, intValue);
                            if (list2 != null && list2.size() > 0) {
                                a(true, aVar3, (String) null);
                                return;
                            }
                        }
                    }
                    if (intValue != 0) {
                        lVar.a("ad_num", String.valueOf(intValue));
                    }
                    if (this.p != 0) {
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append(this.p);
                        lVar.a("frame_num", sb2.toString());
                    }
                    if (this.w.containsKey(MIntegralConstans.NATIVE_INFO)) {
                        String str9 = (String) this.w.get(MIntegralConstans.NATIVE_INFO);
                        if (!TextUtils.isEmpty(str9)) {
                            String str10 = d;
                            StringBuilder sb3 = new StringBuilder("nativeinfo");
                            sb3.append(str9);
                            g.d(str10, sb3.toString());
                            try {
                                JSONArray jSONArray2 = new JSONArray(str9);
                                if (jSONArray2.length() > 0) {
                                    str4 = str9;
                                    int i14 = 0;
                                    int i15 = 0;
                                    int i16 = 0;
                                    while (i14 < jSONArray2.length()) {
                                        try {
                                            JSONObject jSONObject = (JSONObject) jSONArray2.opt(i14);
                                            i11 = i15;
                                            try {
                                                int optInt = jSONObject.optInt("id", 0);
                                                if (2 == optInt) {
                                                    int optInt2 = jSONObject.optInt("ad_num");
                                                    if (q2 > 0) {
                                                        try {
                                                            jSONObject.put("ad_num", q2);
                                                        } catch (JSONException e2) {
                                                            e = e2;
                                                            i4 = optInt2;
                                                            i5 = i16;
                                                        }
                                                    }
                                                    i15 = optInt2;
                                                } else {
                                                    if (3 == optInt) {
                                                        i5 = jSONObject.optInt("ad_num");
                                                        if (q2 > 0) {
                                                            try {
                                                                jSONObject.put("ad_num", q2);
                                                            } catch (JSONException e3) {
                                                                e = e3;
                                                            }
                                                        }
                                                        i16 = i5;
                                                    }
                                                    i15 = i11;
                                                }
                                                i14++;
                                                boolean z5 = z2;
                                            } catch (JSONException e4) {
                                                e = e4;
                                                i5 = i16;
                                                i4 = i11;
                                                g.d(d, com.mintegral.msdk.mtgnative.b.a.a(e));
                                                str3 = str4;
                                                lVar.a(MIntegralConstans.NATIVE_INFO, str3);
                                                q2 = intValue;
                                                lVar.a("ad_num", String.valueOf(q2));
                                                lVar.a("ping_mode", "1");
                                                com.mintegral.msdk.b.b.a();
                                                com.mintegral.msdk.b.a b2 = com.mintegral.msdk.b.b.b(com.mintegral.msdk.base.controller.a.d().j());
                                                if (!b.a(this.w)) {
                                                }
                                                jSONArray = new JSONArray();
                                                com.mintegral.msdk.base.controller.a.d();
                                                g2 = com.mintegral.msdk.base.controller.a.g();
                                                for (Long longValue : g2) {
                                                }
                                                if (jSONArray.length() > 0) {
                                                }
                                                if (g.containsKey(str5)) {
                                                }
                                                i7 = 0;
                                                lVar.a("ttc_ids", this.u);
                                                if (!this.w.containsKey(MIntegralConstans.NATIVE_INFO)) {
                                                }
                                                if (i12 == i9) {
                                                }
                                                lVar.a("offset", String.valueOf(i7));
                                                lVar.a("ad_type", "42");
                                                lVar.a(CampaignEx.JSON_KEY_AD_SOURCE_ID, String.valueOf(i2));
                                                if (!TextUtils.isEmpty(b.a(str))) {
                                                }
                                                if (VERSION.SDK_INT >= 14) {
                                                }
                                                this.q = new e(this.r);
                                                this.q.b();
                                                this.q.b((int) j3);
                                                a aVar6 = new a(i12);
                                                aVar6.a(str5);
                                                aVar6.g(intValue);
                                                aVar6.e(i4);
                                                aVar6.f(i5);
                                                aVar6.d(i6);
                                                AdMobClickListener adMobClickListener3 = adMobClickListener;
                                                aVar6.a(adMobClickListener3);
                                                aVar6.a((com.mintegral.msdk.base.common.e.d) this.q);
                                                aVar6.a(aVar3);
                                                if (i12 == 1) {
                                                }
                                                aVar6.f();
                                                aVar6.a(b.b(this.w));
                                                i8 = i3;
                                                b bVar = new b(aVar6, i8, str5);
                                                aVar6.a((Runnable) bVar);
                                                aVar6.h(i8);
                                                bVar.a(aVar3);
                                                bVar.a(adMobClickListener3);
                                                bVar.a(z3);
                                                bVar.a(str5);
                                                if (i8 != 0) {
                                                }
                                                bVar.a(aVar2);
                                                this.o.postDelayed(bVar, j3);
                                                return;
                                            }
                                        } catch (JSONException e5) {
                                            e = e5;
                                            i11 = i15;
                                            i5 = i16;
                                            i4 = i11;
                                            g.d(d, com.mintegral.msdk.mtgnative.b.a.a(e));
                                            str3 = str4;
                                            lVar.a(MIntegralConstans.NATIVE_INFO, str3);
                                            q2 = intValue;
                                            lVar.a("ad_num", String.valueOf(q2));
                                            lVar.a("ping_mode", "1");
                                            com.mintegral.msdk.b.b.a();
                                            com.mintegral.msdk.b.a b22 = com.mintegral.msdk.b.b.b(com.mintegral.msdk.base.controller.a.d().j());
                                            if (!b.a(this.w)) {
                                            }
                                            jSONArray = new JSONArray();
                                            com.mintegral.msdk.base.controller.a.d();
                                            g2 = com.mintegral.msdk.base.controller.a.g();
                                            while (r4.hasNext()) {
                                            }
                                            if (jSONArray.length() > 0) {
                                            }
                                            if (g.containsKey(str5)) {
                                            }
                                            i7 = 0;
                                            lVar.a("ttc_ids", this.u);
                                            if (!this.w.containsKey(MIntegralConstans.NATIVE_INFO)) {
                                            }
                                            if (i12 == i9) {
                                            }
                                            lVar.a("offset", String.valueOf(i7));
                                            lVar.a("ad_type", "42");
                                            lVar.a(CampaignEx.JSON_KEY_AD_SOURCE_ID, String.valueOf(i2));
                                            if (!TextUtils.isEmpty(b.a(str))) {
                                            }
                                            if (VERSION.SDK_INT >= 14) {
                                            }
                                            this.q = new e(this.r);
                                            this.q.b();
                                            this.q.b((int) j3);
                                            a aVar62 = new a(i12);
                                            aVar62.a(str5);
                                            aVar62.g(intValue);
                                            aVar62.e(i4);
                                            aVar62.f(i5);
                                            aVar62.d(i6);
                                            AdMobClickListener adMobClickListener32 = adMobClickListener;
                                            aVar62.a(adMobClickListener32);
                                            aVar62.a((com.mintegral.msdk.base.common.e.d) this.q);
                                            aVar62.a(aVar3);
                                            if (i12 == 1) {
                                            }
                                            aVar62.f();
                                            aVar62.a(b.b(this.w));
                                            i8 = i3;
                                            b bVar2 = new b(aVar62, i8, str5);
                                            aVar62.a((Runnable) bVar2);
                                            aVar62.h(i8);
                                            bVar2.a(aVar3);
                                            bVar2.a(adMobClickListener32);
                                            bVar2.a(z3);
                                            bVar2.a(str5);
                                            if (i8 != 0) {
                                            }
                                            bVar2.a(aVar2);
                                            this.o.postDelayed(bVar2, j3);
                                            return;
                                        }
                                    }
                                    i10 = i16;
                                    i4 = i15;
                                } else {
                                    str4 = str9;
                                    i10 = 0;
                                    i4 = 0;
                                }
                                try {
                                    this.D = Math.max(i4, i5);
                                    str3 = jSONArray2.toString();
                                } catch (JSONException e6) {
                                    e = e6;
                                    g.d(d, com.mintegral.msdk.mtgnative.b.a.a(e));
                                    str3 = str4;
                                    lVar.a(MIntegralConstans.NATIVE_INFO, str3);
                                    q2 = intValue;
                                    lVar.a("ad_num", String.valueOf(q2));
                                    lVar.a("ping_mode", "1");
                                    com.mintegral.msdk.b.b.a();
                                    com.mintegral.msdk.b.a b222 = com.mintegral.msdk.b.b.b(com.mintegral.msdk.base.controller.a.d().j());
                                    if (!b.a(this.w)) {
                                    }
                                    jSONArray = new JSONArray();
                                    com.mintegral.msdk.base.controller.a.d();
                                    g2 = com.mintegral.msdk.base.controller.a.g();
                                    while (r4.hasNext()) {
                                    }
                                    if (jSONArray.length() > 0) {
                                    }
                                    if (g.containsKey(str5)) {
                                    }
                                    i7 = 0;
                                    lVar.a("ttc_ids", this.u);
                                    if (!this.w.containsKey(MIntegralConstans.NATIVE_INFO)) {
                                    }
                                    if (i12 == i9) {
                                    }
                                    lVar.a("offset", String.valueOf(i7));
                                    lVar.a("ad_type", "42");
                                    lVar.a(CampaignEx.JSON_KEY_AD_SOURCE_ID, String.valueOf(i2));
                                    if (!TextUtils.isEmpty(b.a(str))) {
                                    }
                                    if (VERSION.SDK_INT >= 14) {
                                    }
                                    this.q = new e(this.r);
                                    this.q.b();
                                    this.q.b((int) j3);
                                    a aVar622 = new a(i12);
                                    aVar622.a(str5);
                                    aVar622.g(intValue);
                                    aVar622.e(i4);
                                    aVar622.f(i5);
                                    aVar622.d(i6);
                                    AdMobClickListener adMobClickListener322 = adMobClickListener;
                                    aVar622.a(adMobClickListener322);
                                    aVar622.a((com.mintegral.msdk.base.common.e.d) this.q);
                                    aVar622.a(aVar3);
                                    if (i12 == 1) {
                                    }
                                    aVar622.f();
                                    aVar622.a(b.b(this.w));
                                    i8 = i3;
                                    b bVar22 = new b(aVar622, i8, str5);
                                    aVar622.a((Runnable) bVar22);
                                    aVar622.h(i8);
                                    bVar22.a(aVar3);
                                    bVar22.a(adMobClickListener322);
                                    bVar22.a(z3);
                                    bVar22.a(str5);
                                    if (i8 != 0) {
                                    }
                                    bVar22.a(aVar2);
                                    this.o.postDelayed(bVar22, j3);
                                    return;
                                }
                            } catch (JSONException e7) {
                                e = e7;
                                str4 = str9;
                                i5 = 0;
                                i4 = 0;
                                g.d(d, com.mintegral.msdk.mtgnative.b.a.a(e));
                                str3 = str4;
                                lVar.a(MIntegralConstans.NATIVE_INFO, str3);
                                q2 = intValue;
                                lVar.a("ad_num", String.valueOf(q2));
                                lVar.a("ping_mode", "1");
                                com.mintegral.msdk.b.b.a();
                                com.mintegral.msdk.b.a b2222 = com.mintegral.msdk.b.b.b(com.mintegral.msdk.base.controller.a.d().j());
                                if (!b.a(this.w)) {
                                }
                                jSONArray = new JSONArray();
                                com.mintegral.msdk.base.controller.a.d();
                                g2 = com.mintegral.msdk.base.controller.a.g();
                                while (r4.hasNext()) {
                                }
                                if (jSONArray.length() > 0) {
                                }
                                if (g.containsKey(str5)) {
                                }
                                i7 = 0;
                                lVar.a("ttc_ids", this.u);
                                if (!this.w.containsKey(MIntegralConstans.NATIVE_INFO)) {
                                }
                                if (i12 == i9) {
                                }
                                lVar.a("offset", String.valueOf(i7));
                                lVar.a("ad_type", "42");
                                lVar.a(CampaignEx.JSON_KEY_AD_SOURCE_ID, String.valueOf(i2));
                                if (!TextUtils.isEmpty(b.a(str))) {
                                }
                                if (VERSION.SDK_INT >= 14) {
                                }
                                this.q = new e(this.r);
                                this.q.b();
                                this.q.b((int) j3);
                                a aVar6222 = new a(i12);
                                aVar6222.a(str5);
                                aVar6222.g(intValue);
                                aVar6222.e(i4);
                                aVar6222.f(i5);
                                aVar6222.d(i6);
                                AdMobClickListener adMobClickListener3222 = adMobClickListener;
                                aVar6222.a(adMobClickListener3222);
                                aVar6222.a((com.mintegral.msdk.base.common.e.d) this.q);
                                aVar6222.a(aVar3);
                                if (i12 == 1) {
                                }
                                aVar6222.f();
                                aVar6222.a(b.b(this.w));
                                i8 = i3;
                                b bVar222 = new b(aVar6222, i8, str5);
                                aVar6222.a((Runnable) bVar222);
                                aVar6222.h(i8);
                                bVar222.a(aVar3);
                                bVar222.a(adMobClickListener3222);
                                bVar222.a(z3);
                                bVar222.a(str5);
                                if (i8 != 0) {
                                }
                                bVar222.a(aVar2);
                                this.o.postDelayed(bVar222, j3);
                                return;
                            }
                            lVar.a(MIntegralConstans.NATIVE_INFO, str3);
                        } else {
                            i5 = 0;
                            i4 = 0;
                        }
                        q2 = intValue;
                    } else {
                        if (q2 == B || q2 == 0) {
                            q2 = intValue;
                        }
                        i5 = 0;
                        i4 = 0;
                    }
                    lVar.a("ad_num", String.valueOf(q2));
                    lVar.a("ping_mode", "1");
                    com.mintegral.msdk.b.b.a();
                    com.mintegral.msdk.b.a b22222 = com.mintegral.msdk.b.b.b(com.mintegral.msdk.base.controller.a.d().j());
                    if (!b.a(this.w)) {
                        g.b(d, "没有param 参数 要传exclude_ids");
                        JSONArray jSONArray3 = new JSONArray();
                        if (b22222 != null && b22222.aK() == 1) {
                            long[] c2 = m.a((com.mintegral.msdk.base.b.h) i.a(com.mintegral.msdk.base.controller.a.d().h())).c();
                            if (c2 != null) {
                                int length = c2.length;
                                int i17 = 0;
                                while (i17 < length) {
                                    int i18 = r2;
                                    jSONArray3.put(c2[i17]);
                                    i17++;
                                    r2 = i18;
                                    AdMobClickListener adMobClickListener4 = adMobClickListener;
                                }
                            }
                        }
                        i6 = r2;
                        if (jSONArray3.length() > 0) {
                            lVar.a("exclude_ids", com.mintegral.msdk.base.utils.k.a(jSONArray3));
                        }
                    } else {
                        i6 = r2;
                    }
                    jSONArray = new JSONArray();
                    com.mintegral.msdk.base.controller.a.d();
                    g2 = com.mintegral.msdk.base.controller.a.g();
                    if (g2 != null && g2.size() > 0) {
                        while (r4.hasNext()) {
                            jSONArray.put(longValue.longValue());
                        }
                    }
                    if (jSONArray.length() > 0) {
                        lVar.a("install_ids", com.mintegral.msdk.base.utils.k.a(jSONArray));
                    }
                    if (g.containsKey(str5)) {
                        k kVar = (k) g.get(str5);
                        if (kVar != null) {
                            switch (i12) {
                                case 1:
                                    i7 = kVar.b();
                                    break;
                                case 2:
                                    i7 = kVar.a();
                                    break;
                                default:
                                    i7 = kVar.b();
                                    break;
                            }
                            if (i12 == 1 && !TextUtils.isEmpty(this.u)) {
                                lVar.a("ttc_ids", this.u);
                            }
                            if (!this.w.containsKey(MIntegralConstans.NATIVE_INFO)) {
                                i9 = 1;
                                if (i12 == 1) {
                                    StringBuilder sb4 = new StringBuilder();
                                    sb4.append(this.D);
                                    lVar.a("tnum", sb4.toString());
                                    lVar.a("offset", String.valueOf(i7));
                                    lVar.a("ad_type", "42");
                                    lVar.a(CampaignEx.JSON_KEY_AD_SOURCE_ID, String.valueOf(i2));
                                    if (!TextUtils.isEmpty(b.a(str))) {
                                        lVar.a("display_cids", b.a(str));
                                    }
                                    if (VERSION.SDK_INT >= 14) {
                                        if (this.w.containsKey(MIntegralConstans.NATIVE_VIDEO_WIDTH) && (this.w.get(MIntegralConstans.NATIVE_VIDEO_WIDTH) instanceof Integer)) {
                                            String str11 = AdvertisementColumns.COLUMN_VIDEO_WIDTH;
                                            StringBuilder sb5 = new StringBuilder();
                                            sb5.append(((Integer) this.w.get(MIntegralConstans.NATIVE_VIDEO_WIDTH)).intValue());
                                            lVar.a(str11, sb5.toString());
                                        }
                                        if (this.w.containsKey(MIntegralConstans.NATIVE_VIDEO_HEIGHT) && (this.w.get(MIntegralConstans.NATIVE_VIDEO_HEIGHT) instanceof Integer)) {
                                            String str12 = AdvertisementColumns.COLUMN_VIDEO_HEIGHT;
                                            StringBuilder sb6 = new StringBuilder();
                                            sb6.append(((Integer) this.w.get(MIntegralConstans.NATIVE_VIDEO_HEIGHT)).intValue());
                                            lVar.a(str12, sb6.toString());
                                        }
                                        lVar.a("video_version", MIntegralConstans.NATIVE_VIDEO_VERSION);
                                    }
                                    this.q = new e(this.r);
                                    this.q.b();
                                    this.q.b((int) j3);
                                    a aVar62222 = new a(i12);
                                    aVar62222.a(str5);
                                    aVar62222.g(intValue);
                                    aVar62222.e(i4);
                                    aVar62222.f(i5);
                                    aVar62222.d(i6);
                                    AdMobClickListener adMobClickListener32222 = adMobClickListener;
                                    aVar62222.a(adMobClickListener32222);
                                    aVar62222.a((com.mintegral.msdk.base.common.e.d) this.q);
                                    aVar62222.a(aVar3);
                                    if (i12 == 1) {
                                        z3 = z2;
                                        if (z3) {
                                        }
                                        aVar62222.a(b.b(this.w));
                                        i8 = i3;
                                        b bVar2222 = new b(aVar62222, i8, str5);
                                        aVar62222.a((Runnable) bVar2222);
                                        aVar62222.h(i8);
                                        bVar2222.a(aVar3);
                                        bVar2222.a(adMobClickListener32222);
                                        bVar2222.a(z3);
                                        bVar2222.a(str5);
                                        if (i8 != 0) {
                                            aVar2 = aVar5.a(com.mintegral.msdk.mtgnative.b.a.f2757a, lVar, (com.mintegral.msdk.base.common.net.d<?>) aVar62222);
                                        } else {
                                            com.mintegral.msdk.mtgnative.f.a.a aVar7 = aVar5;
                                            if (i8 == 1) {
                                                aVar2 = aVar7.a(com.mintegral.msdk.mtgnative.b.a.c, lVar, (com.mintegral.msdk.base.common.net.d<?>) aVar62222);
                                            } else {
                                                aVar2 = null;
                                            }
                                        }
                                        bVar2222.a(aVar2);
                                        this.o.postDelayed(bVar2222, j3);
                                        return;
                                    }
                                    z3 = z2;
                                    aVar62222.f();
                                    aVar62222.a(b.b(this.w));
                                    i8 = i3;
                                    b bVar22222 = new b(aVar62222, i8, str5);
                                    aVar62222.a((Runnable) bVar22222);
                                    aVar62222.h(i8);
                                    bVar22222.a(aVar3);
                                    bVar22222.a(adMobClickListener32222);
                                    bVar22222.a(z3);
                                    bVar22222.a(str5);
                                    if (i8 != 0) {
                                    }
                                    bVar22222.a(aVar2);
                                    this.o.postDelayed(bVar22222, j3);
                                    return;
                                }
                            } else {
                                i9 = 1;
                            }
                            if (i12 == i9) {
                                StringBuilder sb7 = new StringBuilder();
                                sb7.append(this.E);
                                lVar.a("tnum", sb7.toString());
                            }
                            lVar.a("offset", String.valueOf(i7));
                            lVar.a("ad_type", "42");
                            lVar.a(CampaignEx.JSON_KEY_AD_SOURCE_ID, String.valueOf(i2));
                            if (!TextUtils.isEmpty(b.a(str))) {
                            }
                            if (VERSION.SDK_INT >= 14) {
                            }
                            this.q = new e(this.r);
                            this.q.b();
                            this.q.b((int) j3);
                            a aVar622222 = new a(i12);
                            aVar622222.a(str5);
                            aVar622222.g(intValue);
                            aVar622222.e(i4);
                            aVar622222.f(i5);
                            aVar622222.d(i6);
                            AdMobClickListener adMobClickListener322222 = adMobClickListener;
                            aVar622222.a(adMobClickListener322222);
                            aVar622222.a((com.mintegral.msdk.base.common.e.d) this.q);
                            aVar622222.a(aVar3);
                            if (i12 == 1) {
                            }
                            aVar622222.f();
                            aVar622222.a(b.b(this.w));
                            i8 = i3;
                            b bVar222222 = new b(aVar622222, i8, str5);
                            aVar622222.a((Runnable) bVar222222);
                            aVar622222.h(i8);
                            bVar222222.a(aVar3);
                            bVar222222.a(adMobClickListener322222);
                            bVar222222.a(z3);
                            bVar222222.a(str5);
                            if (i8 != 0) {
                            }
                            bVar222222.a(aVar2);
                            this.o.postDelayed(bVar222222, j3);
                            return;
                        }
                    }
                    i7 = 0;
                    lVar.a("ttc_ids", this.u);
                    if (!this.w.containsKey(MIntegralConstans.NATIVE_INFO)) {
                    }
                    if (i12 == i9) {
                    }
                    lVar.a("offset", String.valueOf(i7));
                    lVar.a("ad_type", "42");
                    lVar.a(CampaignEx.JSON_KEY_AD_SOURCE_ID, String.valueOf(i2));
                    if (!TextUtils.isEmpty(b.a(str))) {
                    }
                    if (VERSION.SDK_INT >= 14) {
                    }
                    this.q = new e(this.r);
                    this.q.b();
                    this.q.b((int) j3);
                    a aVar6222222 = new a(i12);
                    aVar6222222.a(str5);
                    aVar6222222.g(intValue);
                    aVar6222222.e(i4);
                    aVar6222222.f(i5);
                    aVar6222222.d(i6);
                    AdMobClickListener adMobClickListener3222222 = adMobClickListener;
                    aVar6222222.a(adMobClickListener3222222);
                    aVar6222222.a((com.mintegral.msdk.base.common.e.d) this.q);
                    aVar6222222.a(aVar3);
                    if (i12 == 1) {
                    }
                    aVar6222222.f();
                    aVar6222222.a(b.b(this.w));
                    i8 = i3;
                    b bVar2222222 = new b(aVar6222222, i8, str5);
                    aVar6222222.a((Runnable) bVar2222222);
                    aVar6222222.h(i8);
                    bVar2222222.a(aVar3);
                    bVar2222222.a(adMobClickListener3222222);
                    bVar2222222.a(z3);
                    bVar2222222.a(str5);
                    if (i8 != 0) {
                    }
                    bVar2222222.a(aVar2);
                    this.o.postDelayed(bVar2222222, j3);
                    return;
                }
            }
            AdMobClickListener adMobClickListener5 = adMobClickListener2;
            int i19 = i13;
            a("The request was refused", i3, str, aVar, adMobClickListener);
        } catch (Exception e8) {
            g.d(d, com.mintegral.msdk.mtgnative.b.a.a(e8));
            g.d(d, e8.getMessage());
        }
    }

    public static void a(int i2, String str) {
        k kVar;
        if (g.containsKey(str)) {
            kVar = (k) g.get(str);
        } else {
            kVar = new k();
        }
        int intValue = ((Integer) h.get(str)).intValue();
        int i3 = 1;
        if (i.containsKey(str)) {
            i3 = ((Integer) i.get(str)).intValue();
        }
        switch (i2) {
            case 1:
                int b2 = kVar.b() + intValue;
                if (b2 > i3) {
                    b2 = 0;
                }
                kVar.b(b2);
                break;
            case 2:
                int a2 = kVar.a() + intValue;
                if (a2 > i3) {
                    a2 = 0;
                }
                kVar.a(a2);
                break;
        }
        g.put(str, kVar);
    }

    public static void b(int i2, String str) {
        if (g.containsKey(str)) {
            k kVar = (k) g.get(str);
            switch (i2) {
                case 1:
                    kVar.b(0);
                    break;
                case 2:
                    kVar.a(0);
                    break;
            }
            g.put(str, kVar);
        }
    }

    public final void a(String str, int i2, String str2, com.mintegral.msdk.a.a.a aVar, AdMobClickListener adMobClickListener) {
        Log.e(d, str);
        try {
            if ((this.f2784a == null || this.f2784a.size() > 0) && this.f2784a != null) {
                a(i2, str2, aVar, adMobClickListener);
            } else {
                a(false, aVar, str);
            }
        } catch (Exception e2) {
            if (MIntegralConstans.DEBUG) {
                e2.printStackTrace();
            }
        }
    }

    public static Map<String, Map<Long, Object>> a() {
        return e;
    }

    public static Map<String, Boolean> b() {
        return f;
    }

    public static Map<String, k> c() {
        return g;
    }

    public static Map<String, Integer> d() {
        return h;
    }

    public static Map<String, Integer> e() {
        return i;
    }

    private void a(int i2, long j2, int i3, String str, boolean z2, com.mintegral.msdk.a.a.a aVar, AdMobClickListener adMobClickListener) {
        final String str2 = str;
        final boolean z3 = z2;
        final UUID m2 = com.mintegral.msdk.base.utils.c.m();
        if (m2 == null) {
            Map<String, Boolean> map = this.G;
            StringBuilder sb = new StringBuilder();
            sb.append(str2);
            sb.append(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
            sb.append(z3);
            sb.append("_ttc");
            map.put(sb.toString(), Boolean.valueOf(false));
            Map<String, Boolean> map2 = this.G;
            StringBuilder sb2 = new StringBuilder();
            sb2.append(str2);
            sb2.append(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
            sb2.append(z3);
            sb2.append("_post");
            map2.put(sb2.toString(), Boolean.valueOf(false));
        } else {
            Map<String, Boolean> map3 = this.G;
            StringBuilder sb3 = new StringBuilder();
            sb3.append(m2);
            sb3.append(str2);
            sb3.append(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
            sb3.append(z3);
            sb3.append("_ttc");
            map3.put(sb3.toString(), Boolean.valueOf(false));
            Map<String, Boolean> map4 = this.G;
            StringBuilder sb4 = new StringBuilder();
            sb4.append(m2);
            sb4.append(str2);
            sb4.append(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
            sb4.append(z3);
            sb4.append("_post");
            map4.put(sb4.toString(), Boolean.valueOf(false));
        }
        AnonymousClass2 r11 = new com.mintegral.msdk.base.common.e.a() {
            public final void b() {
            }

            public final void a() {
                if (c.this.v == null) {
                    c.this.v = i.a(com.mintegral.msdk.base.controller.a.d().h());
                }
                com.mintegral.msdk.base.b.d a2 = com.mintegral.msdk.base.b.d.a((com.mintegral.msdk.base.b.h) c.this.v);
                a2.c();
                c.this.u = a2.a(str2);
                if (c.this.G != null && !c.this.G.isEmpty()) {
                    if (m2 == null) {
                        Map e = c.this.G;
                        StringBuilder sb = new StringBuilder();
                        sb.append(str2);
                        sb.append(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
                        sb.append(z3);
                        sb.append("_ttc");
                        if (e.containsKey(sb.toString())) {
                            Map e2 = c.this.G;
                            StringBuilder sb2 = new StringBuilder();
                            sb2.append(str2);
                            sb2.append(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
                            sb2.append(z3);
                            sb2.append("_ttc");
                            e2.put(sb2.toString(), Boolean.valueOf(true));
                            return;
                        }
                    }
                    if (m2 != null) {
                        Map e3 = c.this.G;
                        StringBuilder sb3 = new StringBuilder();
                        sb3.append(m2);
                        sb3.append(str2);
                        sb3.append(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
                        sb3.append(z3);
                        sb3.append("_ttc");
                        if (e3.containsKey(sb3.toString())) {
                            Map e4 = c.this.G;
                            StringBuilder sb4 = new StringBuilder();
                            sb4.append(m2);
                            sb4.append(str2);
                            sb4.append(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
                            sb4.append(z3);
                            sb4.append("_ttc");
                            e4.put(sb4.toString(), Boolean.valueOf(true));
                        }
                    }
                }
            }
        };
        final String str3 = str;
        final boolean z4 = z2;
        final UUID uuid = m2;
        final AnonymousClass2 r5 = r11;
        final int i4 = i2;
        final long j3 = j2;
        final int i5 = i3;
        AnonymousClass3 r13 = r0;
        final com.mintegral.msdk.a.a.a aVar2 = aVar;
        AnonymousClass2 r14 = r11;
        final AdMobClickListener adMobClickListener2 = adMobClickListener;
        AnonymousClass3 r0 = new com.mintegral.msdk.base.common.e.a() {
            public final void b() {
            }

            public final void a() {
                boolean z = false;
                if (c.this.G != null && !c.this.G.isEmpty()) {
                    Map e2 = c.this.G;
                    StringBuilder sb = new StringBuilder();
                    sb.append(str3);
                    sb.append(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
                    sb.append(z4);
                    sb.append("_ttc");
                    if (e2.containsKey(sb.toString())) {
                        Map e3 = c.this.G;
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append(str3);
                        sb2.append(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
                        sb2.append(z4);
                        sb2.append("_ttc");
                        boolean booleanValue = ((Boolean) e3.get(sb2.toString())).booleanValue();
                        Map e4 = c.this.G;
                        StringBuilder sb3 = new StringBuilder();
                        sb3.append(str3);
                        sb3.append(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
                        sb3.append(z4);
                        sb3.append("_ttc");
                        e4.remove(sb3.toString());
                        z = booleanValue;
                    }
                    Map e5 = c.this.G;
                    StringBuilder sb4 = new StringBuilder();
                    sb4.append(uuid);
                    sb4.append(str3);
                    sb4.append(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
                    sb4.append(z4);
                    sb4.append("_ttc");
                    if (e5.containsKey(sb4.toString())) {
                        Map e6 = c.this.G;
                        StringBuilder sb5 = new StringBuilder();
                        sb5.append(uuid);
                        sb5.append(str3);
                        sb5.append(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
                        sb5.append(z4);
                        sb5.append("_ttc");
                        z = ((Boolean) e6.get(sb5.toString())).booleanValue();
                        Map e7 = c.this.G;
                        StringBuilder sb6 = new StringBuilder();
                        sb6.append(uuid);
                        sb6.append(str3);
                        sb6.append(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
                        sb6.append(z4);
                        sb6.append("_ttc");
                        e7.remove(sb6.toString());
                    }
                }
                if (!z) {
                    c.this.o.post(new Runnable() {
                        public final void run() {
                            if (c.this.G != null && !c.this.G.isEmpty()) {
                                Map e = c.this.G;
                                StringBuilder sb = new StringBuilder();
                                sb.append(str3);
                                sb.append(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
                                sb.append(z4);
                                sb.append("_post");
                                if (e.containsKey(sb.toString())) {
                                    Map e2 = c.this.G;
                                    StringBuilder sb2 = new StringBuilder();
                                    sb2.append(str3);
                                    sb2.append(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
                                    sb2.append(z4);
                                    sb2.append("_post");
                                    e2.put(sb2.toString(), Boolean.valueOf(true));
                                }
                                Map e3 = c.this.G;
                                StringBuilder sb3 = new StringBuilder();
                                sb3.append(uuid);
                                sb3.append(str3);
                                sb3.append(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
                                sb3.append(z4);
                                sb3.append("_post");
                                if (e3.containsKey(sb3.toString())) {
                                    Map e4 = c.this.G;
                                    StringBuilder sb4 = new StringBuilder();
                                    sb4.append(uuid);
                                    sb4.append(str3);
                                    sb4.append(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
                                    sb4.append(z4);
                                    sb4.append("_post");
                                    e4.put(sb4.toString(), Boolean.valueOf(true));
                                }
                            }
                            if (c.this.F != null) {
                                c.this.F.a(r5);
                            }
                            c cVar = c.this;
                            int i = i4;
                            long j = j3;
                            int i2 = i5;
                            c.this.m;
                            cVar.a(i, j, i2, str3, aVar2, z4, adMobClickListener2);
                        }
                    });
                }
            }
        };
        this.o.postDelayed(r13, 90000);
        final String str4 = str;
        final int i6 = i2;
        final long j4 = j2;
        final int i7 = i3;
        final com.mintegral.msdk.a.a.a aVar3 = aVar;
        final AdMobClickListener adMobClickListener3 = adMobClickListener;
        AnonymousClass4 r02 = new com.mintegral.msdk.base.common.e.a.b() {
            public final void a(int i2) {
                if (i2 == C0049a.e) {
                    c.this.o.post(new Runnable() {
                        public final void run() {
                            boolean z = false;
                            if (c.this.G != null && !c.this.G.isEmpty()) {
                                Map e = c.this.G;
                                StringBuilder sb = new StringBuilder();
                                sb.append(str4);
                                sb.append(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
                                sb.append(z4);
                                sb.append("_post");
                                if (e.containsKey(sb.toString())) {
                                    Map e2 = c.this.G;
                                    StringBuilder sb2 = new StringBuilder();
                                    sb2.append(str4);
                                    sb2.append(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
                                    sb2.append(z4);
                                    sb2.append("_post");
                                    boolean booleanValue = ((Boolean) e2.get(sb2.toString())).booleanValue();
                                    Map e3 = c.this.G;
                                    StringBuilder sb3 = new StringBuilder();
                                    sb3.append(str4);
                                    sb3.append(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
                                    sb3.append(z4);
                                    sb3.append("_post");
                                    e3.remove(sb3.toString());
                                    z = booleanValue;
                                }
                                Map e4 = c.this.G;
                                StringBuilder sb4 = new StringBuilder();
                                sb4.append(uuid);
                                sb4.append(str4);
                                sb4.append(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
                                sb4.append(z4);
                                sb4.append("_post");
                                if (e4.containsKey(sb4.toString())) {
                                    Map e5 = c.this.G;
                                    StringBuilder sb5 = new StringBuilder();
                                    sb5.append(uuid);
                                    sb5.append(str4);
                                    sb5.append(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
                                    sb5.append(z4);
                                    sb5.append("_post");
                                    z = ((Boolean) e5.get(sb5.toString())).booleanValue();
                                    Map e6 = c.this.G;
                                    StringBuilder sb6 = new StringBuilder();
                                    sb6.append(uuid);
                                    sb6.append(str4);
                                    sb6.append(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
                                    sb6.append(z4);
                                    sb6.append("_post");
                                    e6.remove(sb6.toString());
                                }
                            }
                            if (!z) {
                                c cVar = c.this;
                                int i = i6;
                                long j = j4;
                                int i2 = i7;
                                c.this.m;
                                cVar.a(i, j, i2, str4, aVar3, z4, adMobClickListener3);
                            }
                        }
                    });
                }
            }
        };
        if (this.F == null) {
            this.F = new com.mintegral.msdk.base.common.e.b(com.mintegral.msdk.base.controller.a.d().h());
        }
        if (this.F != null) {
            this.F.a(r14, r02);
        }
    }

    public static void a(boolean z2, com.mintegral.msdk.a.a.a aVar, String str) {
        if (z2) {
            if (aVar != null && !aVar.a()) {
                aVar.b();
                aVar.onPreloadSucceed();
            }
        } else if (aVar != null && !aVar.a()) {
            aVar.b();
            aVar.onPreloadFaild(str);
        }
    }

    public static void a(boolean z2, Campaign campaign) {
        if (campaign != null) {
            if (z2 && campaign.getIconDrawable() == null) {
                campaign.loadIconUrlAsyncWithBlock(null);
            }
            if (z2 && campaign.getBigDrawable() == null) {
                campaign.loadImageUrlAsyncWithBlock(null);
            }
        }
    }

    public static void a(Thread thread) {
        if (!MIntegralConstans.PRELOAD_RESULT_IN_SUBTHREAD) {
            thread.run();
        } else {
            thread.start();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:?, code lost:
        com.mintegral.msdk.base.utils.g.d(d, "please import the videocommon aar");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0083, code lost:
        return;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x007c */
    static /* synthetic */ void a(List list, String str) {
        try {
            if (list.size() > 0) {
                Class.forName("com.mintegral.msdk.nativex.view.MTGMediaView");
                Class.forName("com.mintegral.msdk.videocommon.download.c");
                Class cls = Class.forName("com.mintegral.msdk.videocommon.download.c");
                Object invoke = cls.getMethod("getInstance", new Class[0]).invoke(null, new Object[0]);
                if (invoke != null) {
                    cls.getMethod("createUnitCache", new Class[]{Context.class, String.class, List.class, Integer.TYPE, Class.forName("com.mintegral.msdk.videocommon.listener.a")}).invoke(invoke, new Object[]{com.mintegral.msdk.base.controller.a.d().h(), str, list, Integer.valueOf(1), null});
                    cls.getMethod("load", new Class[]{String.class}).invoke(invoke, new Object[]{str});
                }
                return;
            }
            g.b(d, "onload 不用下载视频素材 size为0");
        } catch (Exception e2) {
            g.d(d, com.mintegral.msdk.mtgnative.b.a.a(e2));
        }
    }
}
