package com.mintegral.msdk.mtgnative.c;

import android.content.Context;
import android.text.TextUtils;
import com.mintegral.msdk.base.b.i;
import com.mintegral.msdk.base.b.n;
import com.mintegral.msdk.base.common.e.d;
import com.mintegral.msdk.base.controller.a;
import com.mintegral.msdk.base.entity.h;
import com.mintegral.msdk.base.utils.c;
import com.mintegral.msdk.base.utils.k;

/* compiled from: RequestTimeListenerImpl */
public final class e implements d {

    /* renamed from: a reason: collision with root package name */
    private h f2797a;
    private i b = null;
    private Context c;

    public e(h hVar) {
        this.f2797a = hVar;
        this.c = a.d().h();
        this.b = i.a(this.c);
        if (!(this.f2797a == null || this.c == null)) {
            int p = c.p(this.c);
            this.f2797a.d(p);
            this.f2797a.c(c.a(this.c, p));
            if (k.a()) {
                this.f2797a.b(1);
                return;
            }
            this.f2797a.b(2);
        }
    }

    public final void a(String str) {
        if (!TextUtils.isEmpty(str)) {
            this.f2797a.a(str);
        }
    }

    public final void b() {
        if (this.f2797a != null) {
            this.f2797a.b();
        }
    }

    public final void b(int i) {
        if (this.f2797a != null) {
            this.f2797a.c(i);
        }
    }

    public final void a(int i) {
        if (this.f2797a != null) {
            this.f2797a.a(i);
        }
    }

    public final void a() {
        if (this.f2797a != null) {
            n.a((com.mintegral.msdk.base.b.h) this.b).a(this.f2797a);
        }
    }
}
