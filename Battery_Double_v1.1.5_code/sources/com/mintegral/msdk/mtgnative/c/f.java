package com.mintegral.msdk.mtgnative.c;

import android.app.Activity;
import android.content.Context;
import android.support.v4.view.ViewCompat;
import android.view.View;

/* compiled from: Views */
public final class f {
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x0007 */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x000d A[Catch:{ Throwable -> 0x0019 }, RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x000e A[Catch:{ Throwable -> 0x0019 }] */
    private static View a(View view) {
        if (view == null) {
            return null;
        }
        ViewCompat.isAttachedToWindow(view);
        try {
            View rootView = view.getRootView();
            if (rootView != null) {
                return null;
            }
            View findViewById = rootView.findViewById(16908290);
            return findViewById != null ? findViewById : rootView;
        } catch (Throwable unused) {
            return null;
        }
    }

    public static View a(Context context, View view) {
        View view2 = null;
        if (context != null && (context instanceof Activity)) {
            view2 = ((Activity) context).getWindow().getDecorView().findViewById(16908290);
        }
        return view2 != null ? view2 : a(view);
    }
}
