package com.mintegral.msdk.mtgnative.c;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Build.VERSION;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import com.google.android.exoplayer2.source.chunk.ChunkedTrackBlacklistUtil;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.base.b.i;
import com.mintegral.msdk.base.b.m;
import com.mintegral.msdk.base.b.w;
import com.mintegral.msdk.base.common.e.a.C0049a;
import com.mintegral.msdk.base.common.net.l;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.entity.CampaignUnit;
import com.mintegral.msdk.base.entity.h;
import com.mintegral.msdk.base.entity.p;
import com.mintegral.msdk.base.utils.CommonMD5;
import com.mintegral.msdk.base.utils.k;
import com.mintegral.msdk.mtgnative.c.a.C0061a;
import com.mintegral.msdk.out.Campaign;
import com.mintegral.msdk.out.Frame;
import com.mintegral.msdk.out.NativeListener.NativeAdListener;
import com.mintegral.msdk.out.NativeListener.NativeTrackingListener;
import com.mintegral.msdk.videocommon.download.j;
import com.vungle.warren.model.AdvertisementDBAdapter.AdvertisementColumns;
import io.fabric.sdk.android.services.events.EventsFilesManager;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: NativeController */
public class b {
    private static Map<String, List<String>> E = new LinkedHashMap();
    /* access modifiers changed from: private */
    public static boolean F = false;
    private static boolean ac = false;
    public static Map<String, Long> c = new HashMap();
    /* access modifiers changed from: private */
    public static final String e = "b";
    /* access modifiers changed from: private */
    public boolean A;
    /* access modifiers changed from: private */
    public boolean B = false;
    /* access modifiers changed from: private */
    public boolean C = false;
    private boolean D = false;
    private int G = 0;
    private int H = 0;
    private int I = 0;
    private int J = 0;
    private e K;
    private h L;
    private boolean M;
    /* access modifiers changed from: private */
    public com.mintegral.msdk.base.common.e.b N;
    /* access modifiers changed from: private */
    public Map<String, Boolean> O = new HashMap();
    private List<a> P;
    private List<C0061a> Q;
    /* access modifiers changed from: private */
    public CopyOnWriteArrayList<com.mintegral.msdk.base.common.e.a> R = new CopyOnWriteArrayList<>();
    private int S = 1;
    /* access modifiers changed from: private */
    public String T = "";
    /* access modifiers changed from: private */
    public i U;
    private int V = 2;
    private boolean W;
    private a X;
    private boolean Y;
    private boolean Z;

    /* renamed from: a reason: collision with root package name */
    protected List<Integer> f2761a;
    private Timer aa;
    /* access modifiers changed from: private */
    public String ab = "";
    private com.mintegral.msdk.b.d ad;
    private long ae;
    /* access modifiers changed from: private */
    public int af;
    private int ag;
    /* access modifiers changed from: private */
    public boolean ah;
    /* access modifiers changed from: private */
    public int ai;
    /* access modifiers changed from: private */
    public int aj;
    private boolean ak;
    protected List<Integer> b;
    Map<String, Object> d;
    private com.mintegral.msdk.b.c f;
    /* access modifiers changed from: private */
    public com.mintegral.msdk.mtgnative.d.a g;
    private NativeTrackingListener h;
    /* access modifiers changed from: private */
    public Context i;
    /* access modifiers changed from: private */
    public String j;
    private Queue<Integer> k;
    private Queue<Long> l;
    private String m;
    private String n;
    private String o;
    private String p;
    /* access modifiers changed from: private */
    public e q;
    /* access modifiers changed from: private */
    public com.mintegral.msdk.base.common.d.b r;
    /* access modifiers changed from: private */
    public String s;
    /* access modifiers changed from: private */
    public com.mintegral.msdk.click.a t;
    private int u = 1;
    /* access modifiers changed from: private */
    public int v = 1;
    /* access modifiers changed from: private */
    public int w = -1;
    /* access modifiers changed from: private */
    public int x = 0;
    private String y = "both";
    private String z;

    /* compiled from: NativeController */
    public class a extends com.mintegral.msdk.mtgnative.f.a.b implements com.mintegral.msdk.base.common.e.c {
        private boolean b = false;
        private Runnable c;
        private com.mintegral.msdk.base.common.e.d d;
        private boolean e = true;
        private List<String> f = null;

        public a() {
        }

        public final void a(com.mintegral.msdk.base.common.e.d dVar) {
            this.d = dVar;
        }

        public final void a(boolean z) {
            this.e = z;
        }

        public final void a_() {
            this.b = true;
        }

        public final void a(List<String> list) {
            this.f = list;
        }

        public final void a(long j) {
            super.a(j);
            if (this.d != null) {
                this.d.a(String.valueOf(j));
            }
        }

        public final void a(CampaignUnit campaignUnit) {
            int i;
            b.this.ah = true;
            m a2 = m.a((com.mintegral.msdk.base.b.h) i.a(b.this.i));
            a2.d();
            if (this.c != null) {
                com.mintegral.msdk.base.utils.g.b(b.e, "REMOVE CANCEL TASK ON SUCCESS");
                b.this.q.removeCallbacks(this.c);
            }
            com.mintegral.msdk.base.utils.g.d(b.e, "onSuccess");
            k.a((List<CampaignEx>) campaignUnit.getAds());
            if (campaignUnit == null || campaignUnit.getAds() == null || campaignUnit.getAds().size() <= 0) {
                b bVar = b.this;
                StringBuilder sb = new StringBuilder("0_");
                sb.append(b.this.j);
                bVar.j = sb.toString();
                c.b(b.this.w, b.this.j);
                b.this.x = 0;
                return;
            }
            if (this.d != null) {
                this.d.a(campaignUnit.getAds().size());
                this.d.a();
            }
            b.this.s = campaignUnit.getSessionId();
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            ArrayList arrayList3 = new ArrayList();
            if (b.this.af <= 0) {
                if (b.this.af == -3) {
                    b.this.af = campaignUnit.getAds().size();
                } else {
                    b.this.af = b.this.v;
                }
                if (b.this.ai != 0 && campaignUnit.getTemplate() == 2) {
                    b.this.af = b.this.ai;
                }
                if (b.this.aj != 0 && campaignUnit.getTemplate() == 3) {
                    b.this.af = b.this.aj;
                }
            }
            int i2 = 0;
            boolean z = false;
            while (true) {
                i = 1;
                if (i2 >= campaignUnit.getAds().size()) {
                    break;
                }
                CampaignEx campaignEx = (CampaignEx) campaignUnit.getAds().get(i2);
                campaignEx.setCampaignUnitId(b.this.j);
                if (!TextUtils.isEmpty(b.this.ab)) {
                    campaignEx.setBidToken(b.this.ab);
                    campaignEx.setIsBidCampaign(true);
                }
                if (b.F) {
                    campaignEx.loadIconUrlAsyncWithBlock(null);
                    campaignEx.loadImageUrlAsyncWithBlock(null);
                }
                if (campaignEx != null) {
                    boolean a3 = k.a(b.this.i, campaignEx.getPackageName());
                    if (a3 && com.mintegral.msdk.base.controller.a.c() != null) {
                        com.mintegral.msdk.base.controller.a.c().add(new com.mintegral.msdk.base.entity.g(campaignEx.getId(), campaignEx.getPackageName()));
                        z = true;
                    }
                    if (i2 < b.this.v && campaignEx.getOfferType() != 99) {
                        if (!a3) {
                            arrayList.add(campaignEx);
                            if (!TextUtils.isEmpty(campaignEx.getVideoUrlEncode())) {
                                arrayList3.add(campaignEx);
                            }
                        } else if (k.b(campaignEx) || k.a(campaignEx)) {
                            arrayList.add(campaignEx);
                            if (!TextUtils.isEmpty(campaignEx.getVideoUrlEncode())) {
                                arrayList3.add(campaignEx);
                            }
                        }
                    }
                    if (i2 < b.this.af && campaignEx.getOfferType() != 99) {
                        if (!a3) {
                            arrayList2.add(campaignEx);
                        } else if (k.b(campaignEx) || k.a(campaignEx)) {
                            arrayList2.add(campaignEx);
                        }
                    }
                    if (!a2.a(campaignEx.getId())) {
                        com.mintegral.msdk.base.entity.f fVar = new com.mintegral.msdk.base.entity.f();
                        fVar.a(campaignEx.getId());
                        fVar.a(campaignEx.getFca());
                        fVar.b(campaignEx.getFcb());
                        fVar.g();
                        fVar.e();
                        fVar.a(System.currentTimeMillis());
                        a2.a(fVar);
                    }
                }
                i2++;
            }
            b.b(b.this, (List) arrayList3);
            if (z) {
                com.mintegral.msdk.base.controller.a.d().f();
            }
            int type = campaignUnit.getAds().get(0) != null ? ((CampaignEx) campaignUnit.getAds().get(0)).getType() : 1;
            com.mintegral.msdk.mtgnative.a.b a4 = com.mintegral.msdk.mtgnative.a.f.a(type);
            if (a4 != null) {
                a4.a(b.this.j, arrayList2, b.this.ab);
            }
            if (arrayList.size() == 0) {
                if (b.this.A || type != 1) {
                    b.this.a("APP ALREADY INSTALLED", h(), g());
                }
                return;
            }
            if (b.this.A || type != 1) {
                b.this.a(b.this.a(type, b.this.c((List<Campaign>) arrayList)));
            }
            if (!b.this.B && b.this.C && !b.this.A) {
                b.this.a(b.this.a(type, b.this.c((List<Campaign>) arrayList)));
            }
            if (!c.b().containsKey(b.this.j) || !((Boolean) c.b().get(b.this.j)).booleanValue()) {
                if (c.e().containsKey(b.this.j)) {
                    i = ((Integer) c.e().get(b.this.j)).intValue();
                }
                int d2 = b.this.v + b.this.x;
                if (d2 > i) {
                    d2 = 0;
                }
                b.this.x = d2;
                return;
            }
            c.a(b.this.w, b.this.j);
        }

        public final void a(int i, String str) {
            b.this.ah = true;
            if (!this.b) {
                if (i == -1) {
                    c.b(b.this.w, b.this.j);
                    b.this.x = 0;
                }
                if (this.c != null) {
                    com.mintegral.msdk.base.utils.g.b(b.e, "REMOVE CANCEL TASK ON onFailed");
                    b.this.q.removeCallbacks(this.c);
                }
                if (!b.this.B && (h() == 1 || this.e)) {
                    b.this.a(str, h(), g());
                }
            } else if (!b.this.B && this.e) {
                com.mintegral.msdk.base.utils.g.b(b.e, "onFailed onnative fail");
                b.this.a(str, h(), g());
            }
        }

        public final void a(Runnable runnable) {
            this.c = runnable;
        }

        public final void b(List<Frame> list) {
            if (!this.b) {
                if (this.c != null) {
                    com.mintegral.msdk.base.utils.g.b(b.e, "REMOVE CANCEL TASK ON onAdLoaded");
                    b.this.q.removeCallbacks(this.c);
                }
                if (list == null || list.size() == 0) {
                    if (b.this.g != null) {
                        b.this.B = true;
                        b.this.g.onAdLoadError("frame is empty");
                    }
                    return;
                }
                if (this.d != null) {
                    this.d.a(list.size());
                    this.d.a();
                }
                for (Frame campaigns : list) {
                    List<Campaign> campaigns2 = campaigns.getCampaigns();
                    if (campaigns2 == null || campaigns2.size() == 0) {
                        if (b.this.g != null) {
                            b.this.B = true;
                            b.this.g.onAdLoadError("ads in frame is empty");
                        }
                        return;
                    }
                    for (Campaign campaign : campaigns2) {
                        if (b.F) {
                            campaign.loadImageUrlAsyncWithBlock(null);
                            campaign.loadIconUrlAsyncWithBlock(null);
                        }
                    }
                }
                if (b.this.g != null) {
                    b.this.g.onAdFramesLoaded(list);
                }
            }
        }
    }

    /* renamed from: com.mintegral.msdk.mtgnative.c.b$b reason: collision with other inner class name */
    /* compiled from: NativeController */
    public interface C0062b {
        void a();
    }

    /* compiled from: NativeController */
    public class c implements Runnable {
        private int b = 1;
        private com.mintegral.msdk.base.common.e.c c;
        private com.mintegral.msdk.base.common.e.a d;
        private int e;
        private String f;

        public c(com.mintegral.msdk.base.common.e.c cVar, int i, String str) {
            this.c = cVar;
            this.e = i;
            this.f = str;
        }

        public final void run() {
            String c2 = b.e;
            StringBuilder sb = new StringBuilder("cancel task adsource is = ");
            sb.append(this.b);
            com.mintegral.msdk.base.utils.g.b(c2, sb.toString());
            this.c.a_();
            switch (this.b) {
                case 1:
                    b.this.ah = true;
                    if (b.this.A || this.e == 1) {
                        b.this.a("REQUEST_TIMEOUT", this.e, this.f);
                        return;
                    }
                case 2:
                    if (!b.this.B || this.e == 1) {
                        b.this.a("REQUEST_TIMEOUT", this.e, this.f);
                        break;
                    }
                case 3:
                    return;
            }
        }

        public final void a(com.mintegral.msdk.base.common.e.a aVar) {
            this.d = aVar;
        }
    }

    /* compiled from: NativeController */
    private static class d implements com.mintegral.msdk.videocommon.download.g.a {

        /* renamed from: a reason: collision with root package name */
        private String f2780a;
        private CampaignEx b;
        private long c = System.currentTimeMillis();
        private boolean d = true;

        public d(String str, CampaignEx campaignEx, boolean z) {
            this.f2780a = str;
            this.b = campaignEx;
            this.d = z;
        }

        public final void a(String str) {
            try {
                if (this.d) {
                    long currentTimeMillis = System.currentTimeMillis() - this.c;
                    w.a((com.mintegral.msdk.base.b.h) i.a(com.mintegral.msdk.base.controller.a.d().h()));
                    p pVar = new p("2000043", 20, String.valueOf(currentTimeMillis), str, this.b.getId(), this.f2780a, "", "2");
                    if (this.b != null && !TextUtils.isEmpty(this.b.getId())) {
                        pVar.m(this.b.getId());
                    }
                    pVar.k(this.b.getRequestIdNotice());
                    pVar.h("1");
                    com.mintegral.msdk.base.common.d.a.a(pVar, com.mintegral.msdk.base.controller.a.d().h(), this.f2780a);
                }
            } catch (Exception e) {
                com.mintegral.msdk.base.utils.g.d(b.e, com.mintegral.msdk.mtgnative.b.a.a(e));
            }
        }

        public final void a(String str, String str2) {
            try {
                if (this.d) {
                    long currentTimeMillis = System.currentTimeMillis() - this.c;
                    w.a((com.mintegral.msdk.base.b.h) i.a(com.mintegral.msdk.base.controller.a.d().h()));
                    p pVar = new p("2000043", 21, String.valueOf(currentTimeMillis), str, this.b.getId(), this.f2780a, str2, "2");
                    pVar.k(this.b.getRequestIdNotice());
                    if (this.b != null && !TextUtils.isEmpty(this.b.getId())) {
                        pVar.m(this.b.getId());
                    }
                    pVar.h("1");
                    com.mintegral.msdk.base.common.d.a.a(pVar, com.mintegral.msdk.base.controller.a.d().h(), this.f2780a);
                }
            } catch (Exception e) {
                com.mintegral.msdk.base.utils.g.d(b.e, com.mintegral.msdk.mtgnative.b.a.a(e));
            }
        }
    }

    /* compiled from: NativeController */
    private static class e extends Handler {

        /* renamed from: a reason: collision with root package name */
        private WeakReference<b> f2781a;

        public e(b bVar) {
            this.f2781a = new WeakReference<>(bVar);
        }

        public final void handleMessage(Message message) {
            super.handleMessage(message);
            try {
                if (!(message.what != 0 || this.f2781a == null || this.f2781a.get() == null)) {
                    ((b) this.f2781a.get()).r.a(message.arg1, (String) message.obj);
                }
                if (message.what == 1) {
                    b bVar = null;
                    if (!(this.f2781a == null || this.f2781a.get() == null)) {
                        bVar = (b) this.f2781a.get();
                    }
                    if (bVar != null) {
                        bVar.C = true;
                        com.mintegral.msdk.base.utils.g.b(b.e, "time out return");
                        String c = bVar.j;
                        bVar.v;
                        bVar.ab;
                        List b = bVar.b(c);
                        if (!bVar.B) {
                            String c2 = b.e;
                            StringBuilder sb = new StringBuilder("time out return isReturn = ");
                            sb.append(bVar.B);
                            com.mintegral.msdk.base.utils.g.b(c2, sb.toString());
                            bVar.a(b);
                        }
                    }
                }
            } catch (Exception e) {
                com.mintegral.msdk.base.utils.g.d(b.e, com.mintegral.msdk.mtgnative.b.a.a(e));
            }
        }
    }

    /* compiled from: NativeController */
    private static class f extends com.mintegral.msdk.base.common.e.a {

        /* renamed from: a reason: collision with root package name */
        private CampaignEx f2782a;
        private WeakReference<View> b;
        private WeakReference<List<View>> c;
        private WeakReference<b> d;

        public final void b() {
        }

        public f(CampaignEx campaignEx, View view, List<View> list, b bVar) {
            this.f2782a = campaignEx;
            this.b = new WeakReference<>(view);
            this.c = new WeakReference<>(list);
            this.d = new WeakReference<>(bVar);
        }

        public final void a() {
            com.mintegral.msdk.base.utils.g.d(b.e, "waitSomeTimeToReport run");
            try {
                if (!(this.d == null || this.b == null || this.c == null)) {
                    View view = (View) this.b.get();
                    List list = (List) this.c.get();
                    b bVar = (b) this.d.get();
                    if (!(view == null || list == null || bVar == null)) {
                        b.a(bVar, this.f2782a, view, list);
                    }
                }
            } catch (Exception e) {
                com.mintegral.msdk.base.utils.g.d(b.e, com.mintegral.msdk.mtgnative.b.a.a(e));
            }
        }
    }

    /* compiled from: NativeController */
    private static final class g implements com.mintegral.msdk.videocommon.download.g.c {

        /* renamed from: a reason: collision with root package name */
        String f2783a;
        CampaignEx b;
        private long c = System.currentTimeMillis();
        private boolean d = true;

        public g(String str, CampaignEx campaignEx, boolean z) {
            this.f2783a = str;
            this.b = campaignEx;
            this.d = z;
        }

        public final void a(String str) {
            try {
                if (this.d) {
                    long currentTimeMillis = System.currentTimeMillis() - this.c;
                    w.a((com.mintegral.msdk.base.b.h) i.a(com.mintegral.msdk.base.controller.a.d().h()));
                    p pVar = new p("2000043", 1, String.valueOf(currentTimeMillis), str, this.b.getId(), this.f2783a, "", "1");
                    if (this.b != null && !TextUtils.isEmpty(this.b.getId())) {
                        pVar.m(this.b.getId());
                    }
                    pVar.k(this.b.getRequestIdNotice());
                    pVar.h("2");
                    com.mintegral.msdk.base.common.d.a.a(pVar, com.mintegral.msdk.base.controller.a.d().h(), this.f2783a);
                }
            } catch (Exception e) {
                com.mintegral.msdk.base.utils.g.d(b.e, com.mintegral.msdk.mtgnative.b.a.a(e));
            }
        }

        public final void a(String str, String str2) {
            try {
                if (this.d) {
                    long currentTimeMillis = System.currentTimeMillis() - this.c;
                    w.a((com.mintegral.msdk.base.b.h) i.a(com.mintegral.msdk.base.controller.a.d().h()));
                    p pVar = new p("2000043", 3, String.valueOf(currentTimeMillis), str2, this.b.getId(), this.f2783a, str, "1");
                    if (this.b != null && !TextUtils.isEmpty(this.b.getId())) {
                        pVar.m(this.b.getId());
                    }
                    pVar.k(this.b.getRequestIdNotice());
                    pVar.h("2");
                    com.mintegral.msdk.base.common.d.a.a(pVar, com.mintegral.msdk.base.controller.a.d().h(), this.f2783a);
                }
            } catch (Exception e) {
                com.mintegral.msdk.base.utils.g.d(b.e, com.mintegral.msdk.mtgnative.b.a.a(e));
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:96:0x02be A[Catch:{ Throwable -> 0x0348 }] */
    public b(com.mintegral.msdk.mtgnative.d.a aVar, NativeTrackingListener nativeTrackingListener, Map<String, Object> map, Context context) {
        List list;
        this.i = context;
        this.d = map;
        this.f = new com.mintegral.msdk.b.c();
        this.g = aVar;
        this.h = nativeTrackingListener;
        this.P = new ArrayList();
        this.Q = new ArrayList();
        this.j = (String) map.get(MIntegralConstans.PROPERTIES_UNIT_ID);
        if (TextUtils.isEmpty(this.j)) {
            com.mintegral.msdk.base.utils.g.c(e, "load error,make sure you have correct unitid");
            return;
        }
        if (map.containsKey(MIntegralConstans.PREIMAGE)) {
            F = ((Boolean) map.get(MIntegralConstans.PREIMAGE)).booleanValue();
        }
        if (map.containsKey(MIntegralConstans.CANCEL_ADMOB_AUTO_DOWNLOAD_IMAGE) && (map.get(MIntegralConstans.CANCEL_ADMOB_AUTO_DOWNLOAD_IMAGE) instanceof Boolean)) {
            this.W = ((Boolean) map.get(MIntegralConstans.CANCEL_ADMOB_AUTO_DOWNLOAD_IMAGE)).booleanValue();
        }
        this.k = new LinkedList();
        this.l = new LinkedList();
        this.N = new com.mintegral.msdk.base.common.e.b(this.i);
        if (Looper.myLooper() == null) {
            Looper.prepare();
        }
        this.q = new e(this);
        if (map.containsKey(MIntegralConstans.ID_FACE_BOOK_PLACEMENT)) {
            this.m = (String) map.get(MIntegralConstans.ID_FACE_BOOK_PLACEMENT);
        } else {
            this.m = com.mintegral.msdk.base.controller.a.d().i();
        }
        if (map.containsKey(MIntegralConstans.ID_ADMOB_UNITID)) {
            this.n = (String) map.get(MIntegralConstans.ID_ADMOB_UNITID);
        }
        if (map.containsKey("admob_type")) {
            this.y = (String) map.get("admob_type");
        }
        if (map.containsKey(MIntegralConstans.ID_MY_TARGET_AD_UNITID)) {
            this.o = (String) map.get(MIntegralConstans.ID_MY_TARGET_AD_UNITID);
        }
        if (map.containsKey(MIntegralConstans.PROPERTIES_API_REUQEST_CATEGORY)) {
            this.z = (String) map.get(MIntegralConstans.PROPERTIES_API_REUQEST_CATEGORY);
        }
        if (map.containsKey(MIntegralConstans.FB_MEDIA_CACHE_FLAG)) {
            this.M = ((Boolean) map.get(MIntegralConstans.FB_MEDIA_CACHE_FLAG)).booleanValue();
        }
        try {
            if (!(c.b().containsKey(this.j) ? ((Boolean) c.b().get(this.j)).booleanValue() : false)) {
                if (map.containsKey("ad_num")) {
                    int intValue = ((Integer) map.get("ad_num")).intValue();
                    if (intValue <= 0) {
                        intValue = 1;
                    }
                    if (intValue > 10) {
                        intValue = 10;
                    }
                    this.v = intValue;
                    this.u = intValue;
                }
                if (map.containsKey(MIntegralConstans.PROPERTIES_AD_FRAME_NUM)) {
                    this.I = ((Integer) map.get(MIntegralConstans.PROPERTIES_AD_FRAME_NUM)).intValue();
                }
            } else if (c.d().containsKey(this.j)) {
                this.v = ((Integer) c.d().get(this.j)).intValue();
                if (map.containsKey("ad_num")) {
                    int intValue2 = ((Integer) map.get("ad_num")).intValue();
                    this.G = intValue2;
                    this.u = intValue2;
                }
                if (map.containsKey(MIntegralConstans.PROPERTIES_AD_FRAME_NUM)) {
                    int intValue3 = ((Integer) map.get(MIntegralConstans.PROPERTIES_AD_FRAME_NUM)).intValue();
                    this.H = intValue3;
                    this.I = intValue3;
                }
            }
        } catch (Exception e2) {
            com.mintegral.msdk.base.utils.g.d(e, com.mintegral.msdk.mtgnative.b.a.a(e2));
        }
        this.r = new com.mintegral.msdk.base.common.d.b(this.i);
        this.t = new com.mintegral.msdk.click.a(this.i, this.j);
        try {
            Class.forName("com.mintegral.msdk.nativex.view.MTGMediaView");
            Class.forName("com.mintegral.msdk.videocommon.download.c");
            this.Z = true;
            if (this.d != null && (this.d.containsKey(MIntegralConstans.NATIVE_VIDEO_WIDTH) || this.d.containsKey(MIntegralConstans.NATIVE_VIDEO_HEIGHT) || map.containsKey(MIntegralConstans.NATIVE_VIDEO_SUPPORT))) {
                this.Y = true;
            }
            d.a(this.i, this.j);
            com.mintegral.msdk.base.utils.e.b();
            if (!TextUtils.isEmpty(this.j)) {
                com.mintegral.msdk.base.b.f.a((com.mintegral.msdk.base.b.h) i.a(this.i)).c();
                int c2 = c(map.containsKey(MIntegralConstans.NATIVE_INFO) ? (String) map.get(MIntegralConstans.NATIVE_INFO) : null);
                String str = this.j;
                if (c2 <= 0) {
                    c2 = this.u;
                }
                com.mintegral.msdk.b.b.a();
                this.ad = com.mintegral.msdk.b.b.c("", str);
                if (this.ad == null) {
                    this.ad = com.mintegral.msdk.b.d.b(str);
                }
                this.f2761a = this.ad.t();
                if (this.f2761a != null && this.f2761a.size() > 0 && this.f2761a.contains(Integer.valueOf(1))) {
                    com.mintegral.msdk.mtgnative.a.b a2 = com.mintegral.msdk.mtgnative.a.f.a(1);
                    if (a2 != null) {
                        list = (List) a2.b(str, c2);
                        if (list != null) {
                            ArrayList arrayList = new ArrayList();
                            for (int i2 = 0; i2 < list.size(); i2++) {
                                CampaignEx campaignEx = (CampaignEx) list.get(i2);
                                if (!TextUtils.isEmpty(campaignEx.getVideoUrlEncode())) {
                                    arrayList.add(campaignEx);
                                }
                            }
                            if (arrayList.size() > 0) {
                                Class cls = Class.forName("com.mintegral.msdk.videocommon.download.c");
                                Class cls2 = Class.forName("com.mintegral.msdk.videocommon.listener.a");
                                Object invoke = cls.getMethod("getInstance", new Class[0]).invoke(null, new Object[0]);
                                cls.getMethod("createUnitCache", new Class[]{Context.class, String.class, List.class, Integer.TYPE, cls2}).invoke(invoke, new Object[]{context, this.j, arrayList, Integer.valueOf(1), null});
                                cls.getMethod("load", new Class[]{String.class}).invoke(invoke, new Object[]{this.j});
                            }
                        }
                    }
                }
                list = null;
                if (list != null) {
                }
            }
        } catch (Throwable unused) {
            com.mintegral.msdk.base.utils.g.d(e, "please import the nativex aar");
        }
    }

    private static int c(String str) {
        if (str == null) {
            return 0;
        }
        try {
            JSONArray jSONArray = new JSONArray(str);
            if (jSONArray.length() > 0) {
                for (int i2 = 0; i2 < jSONArray.length(); i2++) {
                    JSONObject jSONObject = (JSONObject) jSONArray.opt(i2);
                    if (2 == jSONObject.optInt("id", 0)) {
                        return jSONObject.optInt("ad_num");
                    }
                }
            }
        } catch (Exception e2) {
            com.mintegral.msdk.base.utils.g.d(e, com.mintegral.msdk.mtgnative.b.a.a(e2));
        }
        return 0;
    }

    private static void a(String str, String str2) {
        if (E.containsKey(str)) {
            List list = (List) E.get(str);
            if (list.size() == 20) {
                list.remove(0);
            }
            list.add(str2);
            return;
        }
        ArrayList arrayList = new ArrayList();
        arrayList.add(str2);
        E.put(str, arrayList);
    }

    public static String a(String str) {
        List list = (List) E.get(str);
        if (list == null || list.size() <= 0) {
            return null;
        }
        return list.toString();
    }

    public final void a(int i2, String str) {
        String str2;
        this.B = false;
        this.C = false;
        this.D = false;
        this.ah = false;
        this.A = false;
        this.ab = str;
        if (!TextUtils.isEmpty(str)) {
            this.g.a(true);
        } else {
            this.g.a(false);
        }
        String str3 = null;
        this.X = null;
        if (c != null && c.size() > 0) {
            c.clear();
        }
        if (!((c.b() == null || !c.b().containsKey(this.j)) ? false : ((Boolean) c.b().get(this.j)).booleanValue()) || i2 != 1 || !a(i2)) {
            if (!this.d.containsKey("app_id") || !this.d.containsKey(MIntegralConstans.APP_KEY) || !this.d.containsKey(MIntegralConstans.KEY_WORD)) {
                str2 = null;
            } else {
                str3 = (String) this.d.get("app_id");
                str2 = (String) this.d.get(MIntegralConstans.APP_KEY);
            }
            this.f.a(this.i, str3, str2, this.j);
            com.mintegral.msdk.b.b.a();
            this.ad = com.mintegral.msdk.b.b.c(str3, this.j);
            if (this.ad == null) {
                this.ad = com.mintegral.msdk.b.d.b(this.j);
            } else {
                if (!TextUtils.isEmpty(this.ad.s())) {
                    this.m = this.ad.s();
                }
                if (!TextUtils.isEmpty(this.ad.p())) {
                    this.n = this.ad.p();
                }
                if (!TextUtils.isEmpty(this.ad.o())) {
                    this.o = this.ad.o();
                }
            }
            c.e().put(this.j, Integer.valueOf(this.ad.x() * this.v));
            this.f2761a = this.ad.t();
            this.b = this.ad.u();
            this.ag = this.ad.q();
            this.af = this.ad.r();
            this.L = new h();
            this.L.b(this.j);
            this.v = this.u;
            if (this.f2761a == null || this.f2761a.size() == 0) {
                com.mintegral.msdk.mtgnative.d.a aVar = this.g;
                if (aVar != null) {
                    this.B = true;
                    aVar.onAdLoadError("don't have sorceList");
                }
                return;
            }
            if (this.d.containsKey(MIntegralConstans.NATIVE_INFO) && this.p == null) {
                this.p = (String) this.d.get(MIntegralConstans.NATIVE_INFO);
                try {
                    if (this.p != null) {
                        JSONArray jSONArray = new JSONArray(this.p);
                        if (jSONArray.length() > 0) {
                            for (int i3 = 0; i3 < jSONArray.length(); i3++) {
                                JSONObject jSONObject = (JSONObject) jSONArray.opt(i3);
                                int optInt = jSONObject.optInt("id", 0);
                                if (2 == optInt) {
                                    this.ai = jSONObject.optInt("ad_num");
                                    if (this.ag > 0) {
                                        jSONObject.remove("ad_num");
                                        jSONObject.put("ad_num", this.ag);
                                    }
                                } else if (3 == optInt) {
                                    this.aj = jSONObject.optInt("ad_num");
                                    if (this.ag > 0) {
                                        jSONObject.remove("ad_num");
                                        jSONObject.put("ad_num", this.ag);
                                    }
                                }
                            }
                        }
                        this.S = Math.max(this.ai, this.aj);
                        this.p = jSONArray.toString();
                    }
                } catch (JSONException e2) {
                    com.mintegral.msdk.base.utils.g.d(e, com.mintegral.msdk.mtgnative.b.a.a(e2));
                }
            }
            if ((this.f2761a.contains(Integer.valueOf(1)) && ((Integer) this.f2761a.get(0)).intValue() != 1) || i2 != 0 || !a(b(this.j))) {
                this.ak = true;
                if (this.f2761a.contains(Integer.valueOf(1)) && i2 == 0 && ((Integer) this.f2761a.get(0)).intValue() != 1) {
                    int intValue = ((Integer) this.f2761a.get(0)).intValue();
                    com.mintegral.msdk.mtgnative.a.b a2 = com.mintegral.msdk.mtgnative.a.f.a(intValue);
                    if (intValue != 2 || !this.d.containsKey(MIntegralConstans.NATIVE_INFO)) {
                        this.v = this.u;
                    } else {
                        this.v = this.S;
                    }
                    if (a2 == null || !a(a(intValue, c((List) a2.b(this.j, this.v))))) {
                        this.A = false;
                        this.ak = false;
                        try {
                            a((long) (((Integer) this.b.get(this.f2761a.indexOf(Integer.valueOf(1)))).intValue() * 1000), i2, false, this.j, this.ab);
                        } catch (Exception unused) {
                        }
                    }
                }
                this.q.sendEmptyMessageDelayed(1, (long) (this.ad.n() * 1000));
                if (this.f2761a != null && this.f2761a.size() > 0) {
                    if (this.k != null && this.k.size() > 0) {
                        com.mintegral.msdk.base.utils.g.b(e, "setRequestQueue clear requestqueue");
                        this.k.clear();
                    }
                    for (Integer num : this.f2761a) {
                        if (this.k != null) {
                            this.k.add(num);
                        }
                    }
                }
                if (this.b != null && this.b.size() > 0) {
                    if (this.l != null && this.l.size() > 0) {
                        this.l.clear();
                    }
                    for (Integer num2 : this.b) {
                        if (this.l != null) {
                            this.l.add(Long.valueOf((long) (num2.intValue() * 1000)));
                        }
                    }
                }
                b(i2, this.ab);
            }
        }
    }

    private boolean a(int i2) {
        int i3;
        int i4;
        int i5;
        int i6 = 0;
        if (c.b().containsKey(this.j) && ((Boolean) c.b().get(this.j)).booleanValue()) {
            Map a2 = c.a();
            StringBuilder sb = new StringBuilder();
            sb.append(i2);
            sb.append(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
            sb.append(this.j);
            Map map = (Map) a2.get(sb.toString());
            Integer num = (Integer) c.d().get(this.j);
            com.mintegral.msdk.b.b.a();
            com.mintegral.msdk.b.a b2 = com.mintegral.msdk.b.b.b(com.mintegral.msdk.base.controller.a.d().j());
            if (num != null) {
                this.v = num.intValue();
            }
            if (map != null && map.size() > 0) {
                Long l2 = (Long) map.keySet().iterator().next();
                long currentTimeMillis = System.currentTimeMillis();
                if (b2 == null) {
                    com.mintegral.msdk.b.b.a();
                    b2 = com.mintegral.msdk.b.b.b();
                }
                if (currentTimeMillis - l2.longValue() >= b2.aF() * 1000) {
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append(i2);
                    sb2.append(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
                    sb2.append(this.j);
                    a2.remove(sb2.toString());
                } else if (i2 == 1) {
                    List<Frame> list = (List) map.get(l2);
                    if (list == null || list.size() <= 0) {
                        return false;
                    }
                    com.mintegral.msdk.mtgnative.d.a aVar = this.g;
                    if (aVar == null) {
                        return false;
                    }
                    if (this.I >= list.size()) {
                        StringBuilder sb3 = new StringBuilder();
                        sb3.append(i2);
                        sb3.append(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
                        sb3.append(this.j);
                        a2.remove(sb3.toString());
                        aVar.onAdFramesLoaded(list);
                        return true;
                    } else if (this.I == 0) {
                        return false;
                    } else {
                        List subList = list.subList(0, this.H);
                        aVar.onAdFramesLoaded(list);
                        list.removeAll(subList);
                        map.put(l2, subList);
                        ArrayList arrayList = new ArrayList();
                        for (Frame frame : list) {
                            if (i6 >= this.I) {
                                arrayList.add(frame);
                            }
                            i6++;
                        }
                        map.put(l2, arrayList);
                        StringBuilder sb4 = new StringBuilder();
                        sb4.append(i2);
                        sb4.append(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
                        sb4.append(this.j);
                        a2.put(sb4.toString(), map);
                        aVar.onAdFramesLoaded(subList);
                        return true;
                    }
                } else {
                    List list2 = (List) map.get(l2);
                    if (list2 != null && list2.size() > 0) {
                        ArrayList arrayList2 = new ArrayList();
                        if (((CampaignEx) list2.get(0)).getType() == 1) {
                            if (TextUtils.isEmpty(this.p)) {
                                i3 = Math.min(this.G, list2.size());
                            } else {
                                try {
                                    JSONArray jSONArray = new JSONArray(this.p);
                                    if (jSONArray.length() > 0) {
                                        i5 = 0;
                                        i4 = 0;
                                        for (int i7 = 0; i7 < jSONArray.length(); i7++) {
                                            JSONObject jSONObject = (JSONObject) jSONArray.opt(i7);
                                            int optInt = jSONObject.optInt("id", 0);
                                            if (2 == optInt) {
                                                i4 = jSONObject.optInt("ad_num");
                                            } else if (3 == optInt) {
                                                i5 = jSONObject.optInt("ad_num");
                                            }
                                        }
                                    } else {
                                        i5 = 0;
                                        i4 = 0;
                                    }
                                    if (3 == ((CampaignEx) list2.get(0)).getTemplate()) {
                                        i3 = Math.min(i5, list2.size());
                                    } else {
                                        i3 = Math.min(i4, list2.size());
                                    }
                                } catch (Exception unused) {
                                    com.mintegral.msdk.base.utils.g.d(e, "load from catch error in get nativeinfo adnum");
                                    i3 = 0;
                                }
                            }
                            if (i3 <= 0) {
                                return false;
                            }
                            Iterator it = list2.iterator();
                            while (it.hasNext() && i6 != i3) {
                                CampaignEx campaignEx = (CampaignEx) it.next();
                                campaignEx.getTemplate();
                                arrayList2.add(campaignEx);
                                it.remove();
                                i6++;
                            }
                        } else {
                            int min = Math.min(this.G, list2.size());
                            if (min <= 0) {
                                return false;
                            }
                            Iterator it2 = list2.iterator();
                            while (it2.hasNext() && i6 != min) {
                                CampaignEx campaignEx2 = (CampaignEx) it2.next();
                                campaignEx2.getTemplate();
                                arrayList2.add(campaignEx2);
                                it2.remove();
                                i6++;
                            }
                        }
                        a((List<Campaign>) arrayList2);
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0083, code lost:
        if (a(a(r3, c((java.util.List) r0.b(r11.j, r11.v)))) == false) goto L_0x0085;
     */
    private void b(int i2, String str) {
        if (this.k == null || this.k.size() <= 0) {
            if (!this.B && this.g != null) {
                com.mintegral.msdk.base.utils.g.b(e, "no ad source return");
                this.B = true;
                this.g.onAdLoadError("no ad source");
            }
            return;
        }
        int intValue = ((Integer) this.k.poll()).intValue();
        this.ae = (long) MIntegralConstans.REQUEST_TIME_OUT;
        if (this.l != null && this.l.size() > 0) {
            this.ae = ((Long) this.l.poll()).longValue();
        }
        String str2 = e;
        StringBuilder sb = new StringBuilder("start queue adsource = ");
        sb.append(intValue);
        com.mintegral.msdk.base.utils.g.b(str2, sb.toString());
        long j2 = this.ae;
        if (i2 == 0) {
            com.mintegral.msdk.mtgnative.a.b a2 = com.mintegral.msdk.mtgnative.a.f.a(intValue);
            if (a2 != null) {
                if ((intValue == 1 || intValue == 2) && this.d.containsKey(MIntegralConstans.NATIVE_INFO)) {
                    this.v = this.S;
                } else {
                    this.v = this.u;
                }
            }
        }
        this.A = false;
        switch (intValue) {
            case 1:
                a(j2, i2, true, this.j, str);
                return;
            case 2:
                a(2, j2, i2, str);
                return;
            default:
                a(intValue, j2, i2, str);
                break;
        }
    }

    public final void a(String str, int i2, String str2) {
        String str3 = e;
        StringBuilder sb = new StringBuilder("request error msg = ");
        sb.append(str);
        com.mintegral.msdk.base.utils.g.b(str3, sb.toString());
        if ((this.k == null || this.k.size() > 0) && this.k != null) {
            com.mintegral.msdk.base.utils.g.b(e, "request queue in request error");
            b(i2, str2);
        } else if (this.g != null && !this.B) {
            this.B = true;
            this.g.onAdLoadError(str);
            String str4 = e;
            StringBuilder sb2 = new StringBuilder("requestError return listener isReturn = ");
            sb2.append(this.B);
            com.mintegral.msdk.base.utils.g.b(str4, sb2.toString());
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:186:?, code lost:
        r14 = r6.a(com.mintegral.msdk.mtgnative.b.a.b, r9, (com.mintegral.msdk.base.common.net.d<?>) r7);
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* JADX WARNING: Missing exception handler attribute for start block: B:185:0x049c */
    /* JADX WARNING: Removed duplicated region for block: B:169:0x0463 A[Catch:{ Exception -> 0x04c4, all -> 0x04c1 }] */
    /* JADX WARNING: Removed duplicated region for block: B:188:0x04ac A[Catch:{ Exception -> 0x04c4, all -> 0x04c1 }] */
    /* JADX WARNING: Removed duplicated region for block: B:191:0x04b1 A[Catch:{ Exception -> 0x04c4, all -> 0x04c1 }] */
    public final synchronized void a(int i2, long j2, int i3, String str) {
        com.mintegral.msdk.mtgnative.f.a.a aVar;
        com.mintegral.msdk.mtgnative.f.a.a aVar2;
        String str2;
        int i4 = i2;
        long j3 = j2;
        int i5 = i3;
        String str3 = str;
        synchronized (this) {
            if (this.d.containsKey(MIntegralConstans.NATIVE_INFO)) {
                this.v = Math.max(this.ai, this.aj);
            }
            if (i5 == 0) {
                com.mintegral.msdk.mtgnative.a.b a2 = com.mintegral.msdk.mtgnative.a.f.a(i2);
                if (this.A) {
                    if (a2 == null || !a(a(1, c((List) a2.b(this.j, this.v))))) {
                        try {
                            String str4 = e;
                            StringBuilder sb = new StringBuilder("isGetApiReulst = ");
                            sb.append(this.ah);
                            sb.append(" isReturn = ");
                            sb.append(this.B);
                            com.mintegral.msdk.base.utils.g.b(str4, sb.toString());
                            if (this.X != null && !this.ah) {
                                this.X.a(true);
                            }
                            if (this.ah && !this.B) {
                                com.mintegral.msdk.base.utils.g.b(e, "222222");
                                a("mtg load failed", i5, str3);
                            }
                            if (!this.ak) {
                                return;
                            }
                        } catch (Exception e2) {
                            Exception exc = e2;
                            com.mintegral.msdk.base.utils.g.d(e, com.mintegral.msdk.mtgnative.b.a.a(exc));
                            com.mintegral.msdk.base.utils.g.d(e, exc.getMessage());
                            return;
                        } catch (Throwable th) {
                            throw th;
                        }
                    } else {
                        return;
                    }
                } else if (!(i4 == 1 || a2 == null)) {
                    if (a(a(i4, c((List) a2.b(this.j, this.v))))) {
                        return;
                    }
                }
            }
            if (this.w == -1) {
                this.w = i4;
            } else if (this.w != i4) {
                this.x = 0;
            }
            com.mintegral.msdk.base.b.f.a((com.mintegral.msdk.base.b.h) i.a(this.i)).c();
            com.mintegral.msdk.base.utils.g.b(e, "START LOAD MTG MVNATIVE");
            com.mintegral.msdk.mtgnative.f.a.a aVar3 = new com.mintegral.msdk.mtgnative.f.a.a(this.i);
            l lVar = new l();
            String j4 = com.mintegral.msdk.base.controller.a.d().j();
            String k2 = com.mintegral.msdk.base.controller.a.d().k();
            com.mintegral.msdk.base.common.e.a aVar4 = null;
            if (this.d != null && this.d.containsKey("app_id") && this.d.containsKey(MIntegralConstans.APP_KEY) && this.d.containsKey(MIntegralConstans.KEY_WORD) && this.d.get(MIntegralConstans.KEY_WORD) != null) {
                if (this.d.get("app_id") instanceof String) {
                    j4 = (String) this.d.get("app_id");
                }
                if (this.d.get(MIntegralConstans.APP_KEY) instanceof String) {
                    k2 = (String) this.d.get(MIntegralConstans.APP_KEY);
                }
                String str5 = this.d.get(MIntegralConstans.KEY_WORD) instanceof String ? (String) this.d.get(MIntegralConstans.KEY_WORD) : null;
                if (!TextUtils.isEmpty(str5)) {
                    lVar.a("smart", com.mintegral.msdk.base.utils.a.b(str5));
                }
            }
            lVar.a("app_id", j4);
            lVar.a(MIntegralConstans.PROPERTIES_UNIT_ID, this.j);
            lVar.a("req_type", "2");
            if (!TextUtils.isEmpty(this.z)) {
                lVar.a("category", this.z);
            }
            StringBuilder sb2 = new StringBuilder();
            sb2.append(j4);
            sb2.append(k2);
            lVar.a("sign", CommonMD5.getMD5(sb2.toString()));
            if (this.ag <= 0 || i5 != 0) {
                StringBuilder sb3 = new StringBuilder();
                sb3.append(this.u);
                lVar.a("ad_num", sb3.toString());
            } else {
                StringBuilder sb4 = new StringBuilder();
                sb4.append(this.ag);
                lVar.a("ad_num", sb4.toString());
            }
            lVar.a("only_impression", "1");
            lVar.a("ping_mode", "1");
            if (this.I != 0) {
                StringBuilder sb5 = new StringBuilder();
                sb5.append(this.I);
                lVar.a("frame_num", sb5.toString());
            }
            if (!TextUtils.isEmpty(this.p)) {
                lVar.a(MIntegralConstans.NATIVE_INFO, this.p);
                if (i4 == 1 && !com.mintegral.msdk.click.a.f2646a) {
                    StringBuilder sb6 = new StringBuilder();
                    sb6.append(this.S);
                    lVar.a("tnum", sb6.toString());
                }
            } else if (i4 == 1 && !com.mintegral.msdk.click.a.f2646a) {
                StringBuilder sb7 = new StringBuilder();
                sb7.append(this.u);
                lVar.a("tnum", sb7.toString());
            }
            if (i4 == 1 && !TextUtils.isEmpty(this.T)) {
                lVar.a("ttc_ids", this.T);
            }
            if (!TextUtils.isEmpty(a(this.j))) {
                lVar.a("display_cids", a(this.j));
            }
            if (VERSION.SDK_INT >= 14) {
                if (this.d.containsKey(MIntegralConstans.NATIVE_VIDEO_WIDTH) && (this.d.get(MIntegralConstans.NATIVE_VIDEO_WIDTH) instanceof Integer)) {
                    String str6 = AdvertisementColumns.COLUMN_VIDEO_WIDTH;
                    StringBuilder sb8 = new StringBuilder();
                    sb8.append(((Integer) this.d.get(MIntegralConstans.NATIVE_VIDEO_WIDTH)).intValue());
                    lVar.a(str6, sb8.toString());
                }
                if (this.d.containsKey(MIntegralConstans.NATIVE_VIDEO_HEIGHT) && (this.d.get(MIntegralConstans.NATIVE_VIDEO_HEIGHT) instanceof Integer)) {
                    String str7 = AdvertisementColumns.COLUMN_VIDEO_HEIGHT;
                    StringBuilder sb9 = new StringBuilder();
                    sb9.append(((Integer) this.d.get(MIntegralConstans.NATIVE_VIDEO_HEIGHT)).intValue());
                    lVar.a(str7, sb9.toString());
                }
                if (this.d.containsKey(MIntegralConstans.NATIVE_VIDEO_SUPPORT) && (this.d.get(MIntegralConstans.NATIVE_VIDEO_SUPPORT) instanceof Boolean)) {
                    ((Boolean) this.d.get(MIntegralConstans.NATIVE_VIDEO_SUPPORT)).booleanValue();
                }
                lVar.a("video_version", MIntegralConstans.NATIVE_VIDEO_VERSION);
            }
            com.mintegral.msdk.b.b.a();
            com.mintegral.msdk.b.a b2 = com.mintegral.msdk.b.b.b(com.mintegral.msdk.base.controller.a.d().j());
            if (b2 == null) {
                com.mintegral.msdk.b.b.a();
                b2 = com.mintegral.msdk.b.b.b();
            }
            if (!a(this.d)) {
                com.mintegral.msdk.base.utils.g.b(e, "没有param 参数 要传exclude_ids");
                JSONArray jSONArray = new JSONArray();
                if (b2 != null && b2.aK() == 1) {
                    long[] c2 = m.a((com.mintegral.msdk.base.b.h) i.a(this.i)).c();
                    if (c2 != null) {
                        int length = c2.length;
                        int i6 = 0;
                        while (i6 < length) {
                            com.mintegral.msdk.mtgnative.f.a.a aVar5 = aVar3;
                            jSONArray.put(c2[i6]);
                            i6++;
                            aVar3 = aVar5;
                        }
                    }
                }
                aVar = aVar3;
                if (jSONArray.length() > 0) {
                    lVar.a("exclude_ids", k.a(jSONArray));
                }
            } else {
                aVar = aVar3;
            }
            JSONArray jSONArray2 = new JSONArray();
            com.mintegral.msdk.base.controller.a.d();
            List<Long> g2 = com.mintegral.msdk.base.controller.a.g();
            if (g2 != null && g2.size() > 0) {
                for (Long longValue : g2) {
                    jSONArray2.put(longValue.longValue());
                }
            }
            if (jSONArray2.length() > 0) {
                lVar.a("install_ids", k.a(jSONArray2));
            }
            if (c.b().containsKey(this.j) && ((Boolean) c.b().get(this.j)).booleanValue() && c.c().get(this.j) != null) {
                com.mintegral.msdk.base.entity.k kVar = (com.mintegral.msdk.base.entity.k) c.c().get(this.j);
                if (kVar != null) {
                    if (i4 == 1) {
                        this.x = kVar.b();
                    } else if (i4 == 2) {
                        this.x = kVar.a();
                    }
                }
            }
            StringBuilder sb10 = new StringBuilder();
            sb10.append(this.x);
            lVar.a("offset", sb10.toString());
            lVar.a("ad_type", "42");
            lVar.a(CampaignEx.JSON_KEY_AD_SOURCE_ID, String.valueOf(i2));
            if (!TextUtils.isEmpty(this.s)) {
                lVar.a("session_id", this.s);
            }
            this.K = new e(this.L);
            this.K.b();
            this.K.b((int) j3);
            a aVar6 = new a();
            aVar6.a(b(this.d));
            aVar6.a((com.mintegral.msdk.base.common.e.d) this.K);
            if (i4 == 1) {
                if (!this.A) {
                    this.X = aVar6;
                    aVar6.a(false);
                    c cVar = new c(aVar6, i5, str3);
                    aVar6.a((Runnable) cVar);
                    aVar6.h(i5);
                    aVar6.b(str3);
                    if (i5 == 0) {
                        aVar2 = aVar;
                    } else if (!TextUtils.isEmpty(str)) {
                        lVar.a("token", str3);
                        try {
                            String[] split = str3.split(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
                            if (split == null || split.length <= 1) {
                                str2 = com.mintegral.msdk.mtgnative.b.a.b;
                            } else {
                                StringBuilder sb11 = new StringBuilder("https://");
                                sb11.append(split[1]);
                                sb11.append("-hb.rayjump.com/load");
                                str2 = sb11.toString();
                            }
                            String str8 = str2;
                            aVar2 = aVar;
                            aVar4 = aVar2.a(str8, lVar, (com.mintegral.msdk.base.common.net.d<?>) aVar6);
                        } catch (Exception unused) {
                            aVar2 = aVar;
                        }
                    } else {
                        aVar2 = aVar;
                        aVar4 = aVar2.a(com.mintegral.msdk.mtgnative.b.a.f2757a, lVar, (com.mintegral.msdk.base.common.net.d<?>) aVar6);
                    }
                    if (i5 == 1) {
                        aVar4 = aVar2.a(com.mintegral.msdk.mtgnative.b.a.c, lVar, (com.mintegral.msdk.base.common.net.d<?>) aVar6);
                    }
                    cVar.a(aVar4);
                    this.q.postDelayed(cVar, j3);
                }
            }
            aVar6.a(true);
            c cVar2 = new c(aVar6, i5, str3);
            aVar6.a((Runnable) cVar2);
            aVar6.h(i5);
            aVar6.b(str3);
            if (i5 == 0) {
            }
            if (i5 == 1) {
            }
            cVar2.a(aVar4);
            this.q.postDelayed(cVar2, j3);
        }
    }

    public final void a(Campaign campaign, View view, List<View> list) {
        try {
            if (campaign.getType() != 3) {
                if (this.h != null) {
                    this.t.a(this.h);
                }
                com.mintegral.msdk.mtgnative.a.f.a(campaign.getType()).a(this.j, campaign, this.ab);
                final CampaignEx campaignEx = (CampaignEx) campaign;
                Class cls = null;
                try {
                    cls = Class.forName("com.mintegral.msdk.nativex.view.MTGMediaView");
                } catch (Throwable unused) {
                    com.mintegral.msdk.base.utils.g.d("", "MTGMediaView can't found");
                }
                a(this.j, campaignEx.getId());
                if (campaignEx != null) {
                    if (view != null) {
                        if (cls == null || !cls.isInstance(view)) {
                            view.setOnClickListener(new com.mintegral.msdk.widget.a() {
                                /* access modifiers changed from: protected */
                                public final void a() {
                                    b.this.t;
                                    com.mintegral.msdk.click.a.f2646a = false;
                                    b.this.t.a(campaignEx, (NativeAdListener) b.this.g);
                                    b.a(b.this, campaignEx);
                                }
                            });
                        } else {
                            return;
                        }
                    }
                    if (list != null && list.size() > 0) {
                        for (View view2 : list) {
                            if (cls != null && cls.isInstance(view2)) {
                                break;
                            }
                            view2.setOnClickListener(new com.mintegral.msdk.widget.a() {
                                /* access modifiers changed from: protected */
                                public final void a() {
                                    b.this.t;
                                    com.mintegral.msdk.click.a.f2646a = false;
                                    b.this.t.a(campaignEx, (NativeAdListener) b.this.g);
                                    b.a(b.this, campaignEx);
                                }
                            });
                        }
                    }
                    if (!campaignEx.isReport()) {
                        a(campaignEx, view, list);
                        String str = e;
                        StringBuilder sb = new StringBuilder("sendImpression");
                        sb.append(campaignEx);
                        Log.e(str, sb.toString());
                    }
                }
            }
        } catch (Exception unused2) {
            com.mintegral.msdk.base.utils.g.d(e, "registerview exception!");
        }
    }

    public final void a(Campaign campaign, View view) {
        try {
            if (campaign.getType() != 3) {
                if (this.h != null) {
                    this.t.a(this.h);
                }
                com.mintegral.msdk.mtgnative.a.f.a(campaign.getType()).a(this.j, campaign, this.ab);
                final CampaignEx campaignEx = (CampaignEx) campaign;
                a(this.j, campaignEx.getId());
                if (campaignEx != null) {
                    AnonymousClass8 r0 = new com.mintegral.msdk.widget.a() {
                        /* access modifiers changed from: protected */
                        public final void a() {
                            b.this.t;
                            com.mintegral.msdk.click.a.f2646a = false;
                            b.this.t.a(campaignEx, (NativeAdListener) b.this.g);
                            b.a(b.this, campaignEx);
                        }
                    };
                    try {
                        a(view, (OnClickListener) r0, Class.forName("com.mintegral.msdk.nativex.view.MTGMediaView"));
                    } catch (Throwable unused) {
                        a(view, (OnClickListener) r0, (Class) null);
                    }
                    if (!campaignEx.isReport()) {
                        ArrayList arrayList = new ArrayList();
                        arrayList.add(view);
                        a(campaignEx, view, (List<View>) arrayList);
                        String str = e;
                        StringBuilder sb = new StringBuilder("sendImpression");
                        sb.append(campaignEx);
                        com.mintegral.msdk.base.utils.g.b(str, sb.toString());
                    }
                }
            }
        } catch (Exception unused2) {
            com.mintegral.msdk.base.utils.g.d(e, "registerview exception!");
        }
    }

    private void a(CampaignEx campaignEx, View view, List<View> list) {
        try {
            com.mintegral.msdk.base.utils.g.d(e, "waitSomeTimeToReport start");
            int i2 = 0;
            if (this.ad != null) {
                i2 = this.ad.a();
            }
            final f fVar = new f(campaignEx, view, list, this);
            if (this.R == null) {
                this.R = new CopyOnWriteArrayList<>();
            }
            this.R.add(fVar);
            fVar.j = new com.mintegral.msdk.base.common.e.a.b() {
                public final void a(int i) {
                    if (i == C0049a.e && b.this.R != null && b.this.R.size() > 0 && b.this.R.contains(fVar)) {
                        b.this.R.remove(fVar);
                    }
                }
            };
            if (this.q != null) {
                this.q.postDelayed(fVar, (long) (i2 * 1000));
            }
        } catch (Exception e2) {
            com.mintegral.msdk.base.utils.g.d(e, com.mintegral.msdk.mtgnative.b.a.a(e2));
        }
    }

    private void a(View view, OnClickListener onClickListener, Class cls) {
        if (view == null || onClickListener == null) {
            com.mintegral.msdk.base.utils.g.d("", "traverseView  subview or mOnClickListener is null");
            return;
        }
        if (cls != null) {
            try {
                if (cls.isInstance(view)) {
                    return;
                }
            } catch (Throwable unused) {
                com.mintegral.msdk.base.utils.g.d("", "traverseView  failed");
                return;
            }
        }
        view.setOnClickListener(onClickListener);
        if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            for (int i2 = 0; i2 < viewGroup.getChildCount(); i2++) {
                a(viewGroup.getChildAt(i2), onClickListener, cls);
            }
        }
    }

    public final void b(Campaign campaign, View view) {
        b(campaign, view, null);
    }

    public final void b(Campaign campaign, View view, List<View> list) {
        if (campaign != null) {
            switch (campaign.getType()) {
                case 1:
                case 2:
                    Class cls = null;
                    try {
                        cls = Class.forName("com.mintegral.msdk.nativex.view.MTGMediaView");
                    } catch (Throwable unused) {
                    }
                    if (list == null || list.size() <= 0) {
                        if (view != null) {
                            a(view, cls);
                            break;
                        }
                    } else {
                        for (View a2 : list) {
                            a(a2, cls);
                        }
                        return;
                    }
                    break;
            }
        }
    }

    private void a(View view, Class cls) {
        if (view == null) {
            try {
                com.mintegral.msdk.base.utils.g.d("", "traverseView  subview  is null");
            } catch (Throwable unused) {
                com.mintegral.msdk.base.utils.g.d("", "traverseView  failed");
            }
        } else if (cls == null || !cls.isInstance(view)) {
            view.setOnClickListener(null);
            if (view instanceof ViewGroup) {
                ViewGroup viewGroup = (ViewGroup) view;
                for (int i2 = 0; i2 < viewGroup.getChildCount(); i2++) {
                    a(viewGroup.getChildAt(i2), cls);
                }
            }
        }
    }

    public final void a() {
        if (this.N != null) {
            this.N.a();
            this.N = null;
        }
        if (this.q != null) {
            this.q.removeCallbacksAndMessages(null);
        }
        com.mintegral.msdk.base.common.net.a.b();
        this.h = null;
        this.t.b();
        try {
            if (this.i != null) {
                com.mintegral.msdk.base.common.c.b.a(this.i).c();
            }
            if (this.P != null && this.P.size() > 0) {
                for (a a2 : this.P) {
                    a2.a();
                }
                this.P.clear();
                this.P = null;
            }
            if (this.Q != null && this.Q.size() > 0) {
                Iterator it = this.Q.iterator();
                while (it.hasNext()) {
                    it.next();
                }
                this.Q.clear();
                this.Q = null;
            }
            if (this.R != null && this.R.size() > 0) {
                Iterator it2 = this.R.iterator();
                if (it2.hasNext()) {
                    com.mintegral.msdk.base.common.e.a aVar = (com.mintegral.msdk.base.common.e.a) it2.next();
                    if (aVar != null) {
                        aVar.d();
                        this.q.removeCallbacks(aVar);
                    }
                }
                this.R.clear();
            }
        } catch (Exception unused) {
        }
    }

    public final List<Campaign> b(String str) {
        int i2;
        int i3;
        List list = null;
        if (this.f2761a != null) {
            ArrayList arrayList = new ArrayList(this.f2761a);
            List list2 = null;
            for (int i4 = 0; i4 < arrayList.size(); i4++) {
                com.mintegral.msdk.mtgnative.a.b a2 = com.mintegral.msdk.mtgnative.a.f.a(((Integer) arrayList.get(i4)).intValue());
                if (a2 != null) {
                    if ((((Integer) arrayList.get(i4)).intValue() == 1 || ((Integer) arrayList.get(i4)).intValue() == 2) && this.d.containsKey(MIntegralConstans.NATIVE_INFO)) {
                        i3 = this.S;
                    } else {
                        i3 = this.u;
                    }
                    list2 = a(((Integer) arrayList.get(i4)).intValue(), (List) a2.b(str, i3));
                    if (list2 != null) {
                        break;
                    }
                }
            }
            list = list2;
            if (list == null) {
                for (int i5 = 0; i5 < arrayList.size(); i5++) {
                    com.mintegral.msdk.mtgnative.a.b a3 = com.mintegral.msdk.mtgnative.a.f.a(((Integer) arrayList.get(i5)).intValue());
                    if ((((Integer) arrayList.get(i5)).intValue() == 1 || ((Integer) arrayList.get(i5)).intValue() == 2) && this.d.containsKey(MIntegralConstans.NATIVE_INFO)) {
                        i2 = this.S;
                    } else {
                        i2 = this.u;
                    }
                    list = a(((Integer) arrayList.get(i5)).intValue(), (List) a3.a(str, i2));
                    if (list != null) {
                        break;
                    }
                }
            }
        }
        return c(list);
    }

    /* access modifiers changed from: private */
    public boolean a(final List<Campaign> list) {
        if ((!TextUtils.isEmpty(this.ab) && list != null && list.size() == 0) || list == null || list.size() <= 0) {
            return false;
        }
        final com.mintegral.msdk.mtgnative.d.a aVar = this.g;
        if (aVar != null) {
            CampaignEx campaignEx = (CampaignEx) list.get(0);
            final int template = campaignEx != null ? campaignEx.getTemplate() : 2;
            m a2 = m.a((com.mintegral.msdk.base.b.h) i.a(this.i));
            a2.d();
            for (int i2 = 0; i2 < list.size(); i2++) {
                CampaignEx campaignEx2 = (CampaignEx) list.get(i2);
                if (!a2.a(campaignEx2.getId())) {
                    com.mintegral.msdk.base.entity.f fVar = new com.mintegral.msdk.base.entity.f();
                    fVar.a(campaignEx2.getId());
                    fVar.a(campaignEx2.getFca());
                    fVar.b(campaignEx2.getFcb());
                    fVar.g();
                    fVar.e();
                    fVar.a(System.currentTimeMillis());
                    a2.a(fVar);
                }
            }
            int type = campaignEx.getType();
            if (!this.Z || !this.Y || type == 3 || type == 6 || type == 7) {
                a(list, template, (NativeAdListener) aVar);
            } else if (e().e() == 3) {
                final List b2 = b(list);
                if (list == null || list.size() <= 0) {
                    a((NativeAdListener) aVar, "has no ads");
                } else {
                    a(list, (C0062b) new C0062b() {
                        public final void a() {
                            List a2 = b.a(b.this, b2, true);
                            if (a2 == null || a2.size() <= 0) {
                                b.this.a(aVar, "has no ads");
                            } else {
                                b.this.a(a2, template, aVar);
                            }
                        }
                    });
                }
            } else {
                ArrayList arrayList = null;
                if (list != null) {
                    for (Campaign campaign : list) {
                        if (campaign instanceof CampaignEx) {
                            CampaignEx campaignEx3 = (CampaignEx) campaign;
                            if (TextUtils.isEmpty(campaignEx3.getImageUrl()) && !TextUtils.isEmpty(campaignEx3.getVideoUrlEncode())) {
                                if (arrayList == null) {
                                    arrayList = new ArrayList();
                                }
                                arrayList.add(campaign);
                            }
                        }
                    }
                }
                if (arrayList == null || arrayList.size() <= 0) {
                    a(list, template, (NativeAdListener) aVar);
                } else {
                    a((List<Campaign>) arrayList, (C0062b) new C0062b() {
                        public final void a() {
                            List a2 = b.a(b.this, list, false);
                            if (a2 == null || a2.size() <= 0) {
                                b.this.a(aVar, "has no ads");
                            } else {
                                b.this.a(a2, template, aVar);
                            }
                        }
                    });
                }
            }
        }
        return true;
    }

    /* access modifiers changed from: private */
    public void a(final List<Campaign> list, final int i2, final NativeAdListener nativeAdListener) {
        this.q.post(new Runnable() {
            public final void run() {
                b.this.B = true;
                nativeAdListener.onAdLoaded(list, i2);
                com.mintegral.msdk.mtgnative.e.a.a(b.this.i, list, b.this.j);
            }
        });
    }

    /* access modifiers changed from: private */
    public void a(final NativeAdListener nativeAdListener, final String str) {
        this.q.post(new Runnable() {
            public final void run() {
                nativeAdListener.onAdLoadError(str);
            }
        });
    }

    /* access modifiers changed from: private */
    public List<Campaign> a(int i2, List<Campaign> list) {
        int i3 = 1;
        if (i2 != 1 || !this.d.containsKey(MIntegralConstans.NATIVE_INFO) || list == null || list.size() <= 0) {
            return list;
        }
        CampaignEx campaignEx = (CampaignEx) list.get(0);
        if (campaignEx != null) {
            i3 = campaignEx.getTemplate();
        }
        if (i3 != 2) {
            return (i3 != 3 || list == null || list.size() < this.aj) ? list : list.subList(0, this.aj);
        }
        if (list == null || list.size() < this.ai) {
            return list;
        }
        return list.subList(0, this.ai);
    }

    private void a(long j2, int i2, boolean z2, String str, String str2) {
        final boolean z3 = z2;
        final String str3 = str;
        final UUID m2 = com.mintegral.msdk.base.utils.c.m();
        if (m2 == null) {
            Map<String, Boolean> map = this.O;
            StringBuilder sb = new StringBuilder();
            sb.append(str3);
            sb.append(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
            sb.append(z3);
            sb.append("_ttc");
            map.put(sb.toString(), Boolean.valueOf(false));
            Map<String, Boolean> map2 = this.O;
            StringBuilder sb2 = new StringBuilder();
            sb2.append(str3);
            sb2.append(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
            sb2.append(z3);
            sb2.append("_post");
            map2.put(sb2.toString(), Boolean.valueOf(false));
        } else {
            Map<String, Boolean> map3 = this.O;
            StringBuilder sb3 = new StringBuilder();
            sb3.append(m2);
            sb3.append(str3);
            sb3.append(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
            sb3.append(z3);
            sb3.append("_ttc");
            map3.put(sb3.toString(), Boolean.valueOf(false));
            Map<String, Boolean> map4 = this.O;
            StringBuilder sb4 = new StringBuilder();
            sb4.append(m2);
            sb4.append(str3);
            sb4.append(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
            sb4.append(z3);
            sb4.append("_post");
            map4.put(sb4.toString(), Boolean.valueOf(false));
        }
        AnonymousClass2 r14 = new com.mintegral.msdk.base.common.e.a() {
            public final void b() {
            }

            public final void a() {
                if (b.this.U == null) {
                    b.this.U = i.a(b.this.i);
                }
                com.mintegral.msdk.base.b.d a2 = com.mintegral.msdk.base.b.d.a((com.mintegral.msdk.base.b.h) b.this.U);
                a2.c();
                b.this.T = a2.a(str3);
                if (b.this.O != null && !b.this.O.isEmpty()) {
                    if (m2 == null) {
                        Map v = b.this.O;
                        StringBuilder sb = new StringBuilder();
                        sb.append(str3);
                        sb.append(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
                        sb.append(z3);
                        sb.append("_ttc");
                        if (v.containsKey(sb.toString())) {
                            Map v2 = b.this.O;
                            StringBuilder sb2 = new StringBuilder();
                            sb2.append(str3);
                            sb2.append(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
                            sb2.append(z3);
                            sb2.append("_ttc");
                            v2.put(sb2.toString(), Boolean.valueOf(true));
                            return;
                        }
                    }
                    if (m2 != null) {
                        Map v3 = b.this.O;
                        StringBuilder sb3 = new StringBuilder();
                        sb3.append(m2);
                        sb3.append(str3);
                        sb3.append(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
                        sb3.append(z3);
                        sb3.append("_ttc");
                        if (v3.containsKey(sb3.toString())) {
                            Map v4 = b.this.O;
                            StringBuilder sb4 = new StringBuilder();
                            sb4.append(m2);
                            sb4.append(str3);
                            sb4.append(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
                            sb4.append(z3);
                            sb4.append("_ttc");
                            v4.put(sb4.toString(), Boolean.valueOf(true));
                        }
                    }
                }
            }
        };
        final String str4 = str;
        final boolean z4 = z2;
        final UUID uuid = m2;
        final AnonymousClass2 r5 = r14;
        final long j3 = j2;
        final int i3 = i2;
        final String str5 = str2;
        AnonymousClass3 r0 = new com.mintegral.msdk.base.common.e.a() {
            public final void b() {
            }

            public final void a() {
                boolean z = false;
                if (b.this.O != null && !b.this.O.isEmpty()) {
                    Map v = b.this.O;
                    StringBuilder sb = new StringBuilder();
                    sb.append(str4);
                    sb.append(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
                    sb.append(z4);
                    sb.append("_ttc");
                    if (v.containsKey(sb.toString())) {
                        Map v2 = b.this.O;
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append(str4);
                        sb2.append(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
                        sb2.append(z4);
                        sb2.append("_ttc");
                        boolean booleanValue = ((Boolean) v2.get(sb2.toString())).booleanValue();
                        Map v3 = b.this.O;
                        StringBuilder sb3 = new StringBuilder();
                        sb3.append(str4);
                        sb3.append(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
                        sb3.append(z4);
                        sb3.append("_ttc");
                        v3.remove(sb3.toString());
                        z = booleanValue;
                    }
                    Map v4 = b.this.O;
                    StringBuilder sb4 = new StringBuilder();
                    sb4.append(uuid);
                    sb4.append(str4);
                    sb4.append(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
                    sb4.append(z4);
                    sb4.append("_ttc");
                    if (v4.containsKey(sb4.toString())) {
                        Map v5 = b.this.O;
                        StringBuilder sb5 = new StringBuilder();
                        sb5.append(uuid);
                        sb5.append(str4);
                        sb5.append(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
                        sb5.append(z4);
                        sb5.append("_ttc");
                        z = ((Boolean) v5.get(sb5.toString())).booleanValue();
                        Map v6 = b.this.O;
                        StringBuilder sb6 = new StringBuilder();
                        sb6.append(uuid);
                        sb6.append(str4);
                        sb6.append(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
                        sb6.append(z4);
                        sb6.append("_ttc");
                        v6.remove(sb6.toString());
                    }
                }
                if (!z) {
                    b.this.q.post(new Runnable() {
                        public final void run() {
                            if (b.this.O != null && !b.this.O.isEmpty()) {
                                Map v = b.this.O;
                                StringBuilder sb = new StringBuilder();
                                sb.append(str4);
                                sb.append(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
                                sb.append(z4);
                                sb.append("_post");
                                if (v.containsKey(sb.toString())) {
                                    Map v2 = b.this.O;
                                    StringBuilder sb2 = new StringBuilder();
                                    sb2.append(str4);
                                    sb2.append(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
                                    sb2.append(z4);
                                    sb2.append("_post");
                                    v2.put(sb2.toString(), Boolean.valueOf(true));
                                }
                                Map v3 = b.this.O;
                                StringBuilder sb3 = new StringBuilder();
                                sb3.append(uuid);
                                sb3.append(str4);
                                sb3.append(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
                                sb3.append(z4);
                                sb3.append("_post");
                                if (v3.containsKey(sb3.toString())) {
                                    Map v4 = b.this.O;
                                    StringBuilder sb4 = new StringBuilder();
                                    sb4.append(uuid);
                                    sb4.append(str4);
                                    sb4.append(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
                                    sb4.append(z4);
                                    sb4.append("_post");
                                    v4.put(sb4.toString(), Boolean.valueOf(true));
                                }
                            }
                            if (z4) {
                                b.this.A = true;
                            } else {
                                b.this.A = false;
                            }
                            if (b.this.N != null) {
                                b.this.N.a(r5);
                            }
                            b.this.a(1, j3, i3, str5);
                        }
                    });
                }
            }
        };
        this.q.postDelayed(r0, 90000);
        final boolean z5 = z2;
        final String str6 = str;
        final long j4 = j2;
        final int i4 = i2;
        final String str7 = str2;
        AnonymousClass4 r02 = new com.mintegral.msdk.base.common.e.a.b() {
            public final void a(int i) {
                if (i == C0049a.e) {
                    b.this.q.post(new Runnable() {
                        public final void run() {
                            if (z5) {
                                b.this.A = true;
                            }
                            boolean z = false;
                            if (b.this.O != null && !b.this.O.isEmpty()) {
                                Map v = b.this.O;
                                StringBuilder sb = new StringBuilder();
                                sb.append(str6);
                                sb.append(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
                                sb.append(z5);
                                sb.append("_post");
                                if (v.containsKey(sb.toString())) {
                                    Map v2 = b.this.O;
                                    StringBuilder sb2 = new StringBuilder();
                                    sb2.append(str6);
                                    sb2.append(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
                                    sb2.append(z5);
                                    sb2.append("_post");
                                    z = ((Boolean) v2.get(sb2.toString())).booleanValue();
                                    Map v3 = b.this.O;
                                    StringBuilder sb3 = new StringBuilder();
                                    sb3.append(str6);
                                    sb3.append(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
                                    sb3.append(z5);
                                    sb3.append("_post");
                                    v3.remove(sb3.toString());
                                }
                                Map v4 = b.this.O;
                                StringBuilder sb4 = new StringBuilder();
                                sb4.append(uuid);
                                sb4.append(str6);
                                sb4.append(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
                                sb4.append(z5);
                                sb4.append("_post");
                                if (v4.containsKey(sb4.toString())) {
                                    Map v5 = b.this.O;
                                    StringBuilder sb5 = new StringBuilder();
                                    sb5.append(uuid);
                                    sb5.append(str6);
                                    sb5.append(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
                                    sb5.append(z5);
                                    sb5.append("_post");
                                    z = ((Boolean) v5.get(sb5.toString())).booleanValue();
                                    Map v6 = b.this.O;
                                    StringBuilder sb6 = new StringBuilder();
                                    sb6.append(uuid);
                                    sb6.append(str6);
                                    sb6.append(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
                                    sb6.append(z5);
                                    sb6.append("_post");
                                    v6.remove(sb6.toString());
                                }
                            }
                            if (!z) {
                                b.this.a(1, j4, i4, str7);
                            }
                        }
                    });
                }
            }
        };
        if (this.N == null) {
            this.N = new com.mintegral.msdk.base.common.e.b(this.i);
        }
        if (this.N != null) {
            this.N.a(r14, r02);
        }
    }

    public static boolean a(Map<String, Object> map) {
        if (map != null) {
            try {
                if (map.containsKey("app_id") && map.containsKey(MIntegralConstans.APP_KEY) && map.containsKey(MIntegralConstans.KEY_WORD) && map.get(MIntegralConstans.KEY_WORD) != null) {
                    com.mintegral.msdk.base.utils.g.b(e, "有smart 参数");
                    return true;
                }
            } catch (Exception e2) {
                com.mintegral.msdk.base.utils.g.d(e, com.mintegral.msdk.mtgnative.b.a.a(e2));
            }
        }
        com.mintegral.msdk.base.utils.g.b(e, "木有smart 参数");
        return false;
    }

    public static List<String> b(Map<String, Object> map) {
        ArrayList arrayList;
        ArrayList arrayList2 = null;
        try {
            if (map.get(MIntegralConstans.KEY_WORD) instanceof String) {
                String str = (String) map.get(MIntegralConstans.KEY_WORD);
                if (TextUtils.isEmpty(str)) {
                    return null;
                }
                JSONArray jSONArray = new JSONArray(str);
                if (jSONArray.length() == 0) {
                    return null;
                }
                arrayList = new ArrayList();
                int i2 = 0;
                while (i2 < jSONArray.length()) {
                    try {
                        JSONObject optJSONObject = jSONArray.optJSONObject(i2);
                        if (optJSONObject != null) {
                            String optString = optJSONObject.optString(TtmlNode.TAG_P);
                            if (!TextUtils.isEmpty(optString)) {
                                arrayList.add(optString);
                            }
                        }
                        i2++;
                    } catch (Exception e2) {
                        Exception exc = e2;
                        arrayList2 = arrayList;
                        e = exc;
                        com.mintegral.msdk.base.utils.g.d(e, com.mintegral.msdk.mtgnative.b.a.a(e));
                        arrayList = arrayList2;
                        return arrayList;
                    }
                }
                return arrayList;
            }
        } catch (Exception e3) {
            e = e3;
            com.mintegral.msdk.base.utils.g.d(e, com.mintegral.msdk.mtgnative.b.a.a(e));
            arrayList = arrayList2;
            return arrayList;
        }
        arrayList = arrayList2;
        return arrayList;
    }

    public static void b() {
        com.mintegral.msdk.base.utils.e.a();
    }

    public static void a(String str, CampaignEx campaignEx) {
        if (!TextUtils.isEmpty(str) && campaignEx != null && com.mintegral.msdk.base.controller.a.d().h() != null) {
            com.mintegral.msdk.base.b.l a2 = com.mintegral.msdk.base.b.l.a((com.mintegral.msdk.base.b.h) i.a(com.mintegral.msdk.base.controller.a.d().h()));
            com.mintegral.msdk.base.entity.e eVar = new com.mintegral.msdk.base.entity.e();
            eVar.a(System.currentTimeMillis());
            eVar.b(str);
            eVar.a(campaignEx.getId());
            a2.a(eVar);
        }
    }

    /* access modifiers changed from: private */
    public com.mintegral.msdk.b.d e() {
        com.mintegral.msdk.b.b.a();
        this.ad = com.mintegral.msdk.b.b.c("", this.j);
        if (this.ad == null) {
            this.ad = com.mintegral.msdk.b.d.b(this.j);
        }
        return this.ad;
    }

    private List<Campaign> b(List<Campaign> list) {
        if (list != null) {
            CampaignEx campaignEx = null;
            for (int size = list.size() - 1; size >= 0; size--) {
                try {
                    campaignEx = (CampaignEx) list.get(size);
                } catch (Throwable th) {
                    th.printStackTrace();
                }
                if (campaignEx != null && TextUtils.isEmpty(campaignEx.getVideoUrlEncode())) {
                    Campaign campaign = (Campaign) list.remove(size);
                    com.mintegral.msdk.mtgnative.a.f.a(campaignEx.getType()).a(this.j, campaign, this.ab);
                    String str = e;
                    StringBuilder sb = new StringBuilder("remove no videoURL ads:");
                    sb.append(campaign);
                    com.mintegral.msdk.base.utils.g.a(str, sb.toString());
                }
            }
        }
        return list;
    }

    private void a(List<Campaign> list, C0062b bVar) {
        f();
        final long currentTimeMillis = System.currentTimeMillis();
        this.aa = new Timer();
        Timer timer = this.aa;
        final C0062b bVar2 = bVar;
        final List<Campaign> list2 = list;
        AnonymousClass6 r0 = new TimerTask() {
            public final void run() {
                boolean z;
                com.mintegral.msdk.base.utils.g.a(b.e, "search campain status");
                if (System.currentTimeMillis() - currentTimeMillis >= ChunkedTrackBlacklistUtil.DEFAULT_TRACK_BLACKLIST_MS) {
                    bVar2.a();
                    b.this.f();
                    return;
                }
                int p = com.mintegral.msdk.base.utils.c.p(b.this.i);
                int l = b.this.e().l();
                if (p != 9 && l == 2) {
                    bVar2.a();
                    b.this.f();
                } else if (l == 3) {
                    bVar2.a();
                    b.this.f();
                } else {
                    loop0:
                    while (true) {
                        z = false;
                        for (Campaign campaign : list2) {
                            String id = campaign.getId();
                            if (campaign instanceof CampaignEx) {
                                StringBuilder sb = new StringBuilder();
                                sb.append(id);
                                CampaignEx campaignEx = (CampaignEx) campaign;
                                sb.append(campaignEx.getVideoUrlEncode());
                                sb.append(campaignEx.getBidToken());
                                id = sb.toString();
                            }
                            com.mintegral.msdk.videocommon.download.a a2 = com.mintegral.msdk.videocommon.download.c.getInstance().a(b.this.j, id);
                            if (a2 != null && com.mintegral.msdk.videocommon.download.k.a(a2, b.this.e().d())) {
                                z = true;
                            }
                        }
                        break loop0;
                    }
                    if (z) {
                        bVar2.a();
                        b.this.f();
                    }
                }
            }
        };
        timer.schedule(r0, 0, 1000);
    }

    /* access modifiers changed from: private */
    public void f() {
        if (this.aa != null) {
            this.aa.cancel();
            this.aa = null;
        }
    }

    /* access modifiers changed from: private */
    public List<Campaign> c(List<Campaign> list) {
        if (list == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList(list.size());
        ArrayList arrayList2 = new ArrayList(list.size());
        for (Campaign campaign : list) {
            if (campaign instanceof CampaignEx) {
                CampaignEx campaignEx = (CampaignEx) campaign;
                if (TextUtils.isEmpty(this.ab) && TextUtils.isEmpty(campaignEx.getBidToken()) && !campaignEx.isBidCampaign()) {
                    arrayList2.add(campaignEx);
                } else if (!TextUtils.isEmpty(this.ab) && TextUtils.equals(campaignEx.getBidToken(), this.ab)) {
                    arrayList.add(campaign);
                }
            } else {
                arrayList2.add(campaign);
            }
        }
        return TextUtils.isEmpty(this.ab) ? arrayList2 : arrayList;
    }

    static /* synthetic */ void a(b bVar, CampaignEx campaignEx) {
        if (!campaignEx.isReportClick()) {
            campaignEx.setReportClick(true);
            if (campaignEx != null && campaignEx.getNativeVideoTracking() != null && campaignEx.getNativeVideoTracking().j() != null) {
                com.mintegral.msdk.click.a.a(bVar.i, campaignEx, campaignEx.getCampaignUnitId(), campaignEx.getNativeVideoTracking().j(), false);
            }
        }
    }

    static /* synthetic */ void a(b bVar, final CampaignEx campaignEx, View view, List list) {
        try {
            com.mintegral.msdk.base.utils.g.d(e, "trackView start");
            AnonymousClass10 r0 = new C0061a() {
                public final void a(ArrayList<View> arrayList) {
                    com.mintegral.msdk.base.utils.g.d(b.e, "trackView onVisibilityChanged");
                    if (arrayList.size() > 0) {
                        com.mintegral.msdk.base.utils.g.d(b.e, "trackView send");
                        com.mintegral.msdk.mtgnative.e.b.a(campaignEx, b.this.i, b.this.j, b.this.g);
                    }
                }
            };
            if (list != null) {
                a aVar = new a(list, r0, new Handler(Looper.getMainLooper()));
                if (view != null) {
                    aVar.a(view);
                }
                if (bVar.P != null) {
                    bVar.P.add(aVar);
                }
                if (bVar.Q != null) {
                    bVar.Q.add(r0);
                }
            }
        } catch (Exception unused) {
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:30:?, code lost:
        com.mintegral.msdk.base.utils.g.d(e, "please import the videocommon and nativex aar");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0112, code lost:
        return;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* JADX WARNING: Incorrect type for immutable var: ssa=java.util.List, code=java.util.List<com.mintegral.msdk.base.entity.CampaignEx>, for r13v0, types: [java.util.List, java.util.List<com.mintegral.msdk.base.entity.CampaignEx>] */
    /* JADX WARNING: Missing exception handler attribute for start block: B:29:0x010b */
    static /* synthetic */ void b(b bVar, List<CampaignEx> list) {
        try {
            if (list.size() > 0) {
                com.mintegral.msdk.base.utils.g.b(e, "===创建下载任务");
                Class.forName("com.mintegral.msdk.nativex.view.MTGMediaView");
                Class.forName("com.mintegral.msdk.videocommon.download.c");
                Class cls = Class.forName("com.mintegral.msdk.videocommon.download.c");
                Class cls2 = Class.forName("com.mintegral.msdk.videocommon.listener.a");
                Object invoke = cls.getMethod("getInstance", new Class[0]).invoke(null, new Object[0]);
                if (invoke != null) {
                    cls.getMethod("createUnitCache", new Class[]{Context.class, String.class, List.class, Integer.TYPE, cls2}).invoke(invoke, new Object[]{bVar.i, bVar.j, list, Integer.valueOf(1), null});
                    cls.getMethod("load", new Class[]{String.class}).invoke(invoke, new Object[]{bVar.j});
                }
                for (CampaignEx campaignEx : list) {
                    if (campaignEx != null && !TextUtils.isEmpty(campaignEx.getImageUrl())) {
                        com.mintegral.msdk.base.common.c.b.a(com.mintegral.msdk.base.controller.a.d().h()).a(campaignEx.getImageUrl(), (com.mintegral.msdk.base.common.c.c) new com.mintegral.msdk.base.common.c.c() {
                            public final void onFailedLoad(String str, String str2) {
                            }

                            public final void onSuccessLoad(Bitmap bitmap, String str) {
                            }
                        });
                    }
                    try {
                        Class.forName("com.mintegral.msdk.videocommon.download.g");
                        String str = campaignEx.getendcard_url();
                        if (!TextUtils.isEmpty(str)) {
                            if (!str.contains(".zip") || !str.contains("md5filename")) {
                                com.mintegral.msdk.videocommon.download.g.a().a(str, (com.mintegral.msdk.videocommon.download.g.a) new d(bVar.j, campaignEx, TextUtils.isEmpty(com.mintegral.msdk.videocommon.download.h.a().a(str))));
                            } else {
                                com.mintegral.msdk.videocommon.download.g.a().a(str, (com.mintegral.msdk.videocommon.download.g.c) new g(bVar.j, campaignEx, TextUtils.isEmpty(j.a().a(str))));
                            }
                        }
                    } catch (Exception unused) {
                    }
                }
                return;
            }
            com.mintegral.msdk.base.utils.g.b(e, "onload 不用下载视频素材 size为0");
        } catch (Throwable th) {
            if (MIntegralConstans.DEBUG) {
                th.printStackTrace();
            }
        }
    }

    static /* synthetic */ List a(b bVar, List list, boolean z2) {
        if (list != null) {
            for (int size = list.size() - 1; size >= 0; size--) {
                Campaign campaign = (Campaign) list.get(size);
                String id = campaign.getId();
                boolean z3 = campaign instanceof CampaignEx;
                if (z3) {
                    StringBuilder sb = new StringBuilder();
                    sb.append(id);
                    CampaignEx campaignEx = (CampaignEx) campaign;
                    sb.append(campaignEx.getVideoUrlEncode());
                    sb.append(campaignEx.getBidToken());
                    id = sb.toString();
                }
                com.mintegral.msdk.videocommon.download.a a2 = com.mintegral.msdk.videocommon.download.c.getInstance().a(bVar.j, id);
                if (z2) {
                    if (a2 == null || !com.mintegral.msdk.videocommon.download.k.a(a2, bVar.e().d())) {
                        com.mintegral.msdk.mtgnative.a.f.a(campaign.getType()).a(bVar.j, (Campaign) list.remove(size), bVar.ab);
                    }
                } else if (z3) {
                    CampaignEx campaignEx2 = (CampaignEx) campaign;
                    if (TextUtils.isEmpty(campaignEx2.getImageUrl()) && !TextUtils.isEmpty(campaignEx2.getVideoUrlEncode()) && (a2 == null || !com.mintegral.msdk.videocommon.download.k.a(a2, bVar.e().d()))) {
                        com.mintegral.msdk.mtgnative.a.f.a(campaign.getType()).a(bVar.j, (Campaign) list.remove(size), bVar.ab);
                    }
                }
            }
        }
        return list;
    }
}
