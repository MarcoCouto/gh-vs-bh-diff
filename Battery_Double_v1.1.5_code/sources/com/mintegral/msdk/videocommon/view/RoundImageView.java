package com.mintegral.msdk.videocommon.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader.TileMode;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.widget.ImageView;
import com.mintegral.msdk.base.utils.k;

public class RoundImageView extends ImageView {

    /* renamed from: a reason: collision with root package name */
    private int f3074a;
    private int b;
    private Paint c;
    private int d;
    private Matrix e;
    private BitmapShader f;
    private int g;
    private RectF h;

    public RoundImageView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.e = new Matrix();
        this.c = new Paint();
        this.c.setAntiAlias(true);
        this.b = (int) TypedValue.applyDimension(1, 5.0f, getResources().getDisplayMetrics());
        this.f3074a = 1;
    }

    public RoundImageView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public RoundImageView(Context context) {
        super(context);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        if (this.f3074a == 0) {
            this.g = Math.min(getMeasuredWidth(), getMeasuredHeight());
            this.d = this.g / 2;
            setMeasuredDimension(this.g, this.g);
        }
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        Bitmap bitmap;
        if (getDrawable() != null) {
            Drawable drawable = getDrawable();
            if (drawable != null) {
                if (drawable instanceof BitmapDrawable) {
                    bitmap = ((BitmapDrawable) drawable).getBitmap();
                } else {
                    int intrinsicWidth = drawable.getIntrinsicWidth();
                    int intrinsicHeight = drawable.getIntrinsicHeight();
                    Bitmap createBitmap = Bitmap.createBitmap(intrinsicWidth, intrinsicHeight, Config.ARGB_8888);
                    Canvas canvas2 = new Canvas(createBitmap);
                    drawable.setBounds(0, 0, intrinsicWidth, intrinsicHeight);
                    drawable.draw(canvas2);
                    bitmap = createBitmap;
                }
                this.f = new BitmapShader(bitmap, TileMode.CLAMP, TileMode.CLAMP);
                float f2 = 1.0f;
                if (this.f3074a == 0) {
                    f2 = (((float) this.g) * 1.0f) / ((float) Math.min(bitmap.getWidth(), bitmap.getHeight()));
                } else if (this.f3074a == 1) {
                    f2 = Math.max((((float) getWidth()) * 1.0f) / ((float) bitmap.getWidth()), (((float) getHeight()) * 1.0f) / ((float) bitmap.getHeight()));
                }
                this.e.setScale(f2, f2);
                this.f.setLocalMatrix(this.e);
                this.c.setShader(this.f);
            }
            if (this.f3074a == 1) {
                canvas.drawRoundRect(this.h, (float) this.b, (float) this.b, this.c);
            } else {
                canvas.drawCircle((float) this.d, (float) this.d, (float) this.d, this.c);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        if (this.f3074a == 1) {
            this.h = new RectF(0.0f, 0.0f, (float) getWidth(), (float) getHeight());
        }
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        Bundle bundle = new Bundle();
        bundle.putParcelable("state_instance", super.onSaveInstanceState());
        bundle.putInt("state_type", this.f3074a);
        bundle.putInt("state_border_radius", this.b);
        return bundle;
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable parcelable) {
        if (parcelable instanceof Bundle) {
            Bundle bundle = (Bundle) parcelable;
            super.onRestoreInstanceState(bundle.getParcelable("state_instance"));
            this.f3074a = bundle.getInt("state_type");
            this.b = bundle.getInt("state_border_radius");
            return;
        }
        super.onRestoreInstanceState(parcelable);
    }

    public void setBorderRadius(int i) {
        int b2 = k.b(getContext(), (float) i);
        if (this.b != b2) {
            this.b = b2;
            invalidate();
        }
    }

    public void setType(int i) {
        if (this.f3074a != i) {
            this.f3074a = i;
            if (!(this.f3074a == 1 || this.f3074a == 0)) {
                this.f3074a = 0;
            }
            requestLayout();
        }
    }
}
