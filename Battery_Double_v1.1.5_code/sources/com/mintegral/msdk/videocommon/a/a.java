package com.mintegral.msdk.videocommon.a;

import android.content.Context;
import android.text.TextUtils;
import com.mintegral.msdk.base.b.f;
import com.mintegral.msdk.base.b.h;
import com.mintegral.msdk.base.b.i;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.videocommon.e.b;
import java.util.ArrayList;
import java.util.List;

/* compiled from: VideoCampaignCache */
public class a {

    /* renamed from: a reason: collision with root package name */
    private static final String f3037a = "com.mintegral.msdk.videocommon.a.a";
    private static a b;
    private f c;

    private a() {
        try {
            Context h = com.mintegral.msdk.base.controller.a.d().h();
            if (h != null) {
                this.c = f.a((h) i.a(h));
            } else {
                g.d(f3037a, "RewardCampaignCache get Context is null");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static a a() {
        if (b == null) {
            synchronized (a.class) {
                if (b == null) {
                    b = new a();
                }
            }
        }
        return b;
    }

    public final boolean a(String str, long j) {
        long currentTimeMillis = System.currentTimeMillis();
        long j2 = currentTimeMillis - j;
        try {
            if (!TextUtils.isEmpty(str)) {
                List<CampaignEx> a2 = this.c.a(str, 0, 0, 1);
                if (a2 != null) {
                    for (CampaignEx campaignEx : a2) {
                        if ((campaignEx.getPlct() > 0 && (campaignEx.getPlct() * 1000) + campaignEx.getTimestamp() >= currentTimeMillis) || (campaignEx.getPlct() <= 0 && campaignEx.getTimestamp() >= j2)) {
                            return false;
                        }
                    }
                }
            }
        } catch (Throwable th) {
            g.a(f3037a, th.getMessage(), th);
        }
        return true;
    }

    public final List<CampaignEx> a(String str, int i) {
        List<CampaignEx> list;
        try {
            if (!TextUtils.isEmpty(str)) {
                List<CampaignEx> a2 = this.c.a(str, 0, 0, i);
                if (a2 != null) {
                    list = new ArrayList<>();
                    try {
                        for (CampaignEx campaignEx : a2) {
                            if (campaignEx != null) {
                                list.add(campaignEx);
                            }
                        }
                        return list;
                    } catch (Exception e) {
                        e = e;
                        e.printStackTrace();
                        return list;
                    }
                }
            }
            list = null;
            return list;
        } catch (Exception e2) {
            e = e2;
            list = null;
            e.printStackTrace();
            return list;
        }
    }

    public final CampaignEx a(String str, String str2) {
        if (this.c == null || TextUtils.isEmpty(str2) || TextUtils.isEmpty(str)) {
            return null;
        }
        return this.c.d(str, str2);
    }

    public static boolean a(CampaignEx campaignEx) {
        try {
            b.a();
            com.mintegral.msdk.videocommon.e.a b2 = b.b();
            long e = b2 != null ? b2.e() : 0;
            long currentTimeMillis = System.currentTimeMillis();
            if (campaignEx != null) {
                long plct = campaignEx.getPlct() * 1000;
                long timestamp = currentTimeMillis - campaignEx.getTimestamp();
                if ((plct > 0 && plct >= timestamp) || (plct <= 0 && e >= timestamp)) {
                    return false;
                }
            }
            return true;
        } catch (Exception e2) {
            e2.printStackTrace();
            return true;
        }
    }

    public final List<CampaignEx> a(String str, int i, boolean z) {
        List<CampaignEx> list = null;
        try {
            b.a();
            com.mintegral.msdk.videocommon.e.a b2 = b.b();
            long e = b2 != null ? b2.e() : 0;
            if (!TextUtils.isEmpty(str)) {
                List<CampaignEx> a2 = this.c.a(str, i, z);
                long currentTimeMillis = System.currentTimeMillis();
                if (a2 != null) {
                    ArrayList arrayList = new ArrayList();
                    try {
                        for (CampaignEx campaignEx : a2) {
                            if (campaignEx != null) {
                                long plct = campaignEx.getPlct() * 1000;
                                long timestamp = currentTimeMillis - campaignEx.getTimestamp();
                                if ((plct > 0 && plct >= timestamp) || (plct <= 0 && e >= timestamp)) {
                                    arrayList.add(campaignEx);
                                }
                            }
                        }
                        list = arrayList;
                    } catch (Exception e2) {
                        e = e2;
                        list = arrayList;
                        e.printStackTrace();
                        return list;
                    }
                }
            }
            return list;
        } catch (Exception e3) {
            e = e3;
            e.printStackTrace();
            return list;
        }
    }

    public final List<CampaignEx> a(String str, boolean z) {
        List<CampaignEx> list = null;
        try {
            String j = com.mintegral.msdk.base.controller.a.d().j();
            com.mintegral.msdk.b.b.a();
            com.mintegral.msdk.b.a b2 = com.mintegral.msdk.b.b.b(j);
            long ak = b2 != null ? b2.ak() * 1000 : 0;
            if (!TextUtils.isEmpty(str)) {
                try {
                    List<CampaignEx> a2 = this.c.a(str, 1, z);
                    long currentTimeMillis = System.currentTimeMillis();
                    if (a2 != null) {
                        ArrayList arrayList = new ArrayList();
                        try {
                            for (CampaignEx campaignEx : a2) {
                                if (campaignEx != null) {
                                    long plctb = campaignEx.getPlctb() * 1000;
                                    long timestamp = currentTimeMillis - campaignEx.getTimestamp();
                                    if ((plctb <= 0 && ak >= timestamp) || (plctb > 0 && plctb >= timestamp)) {
                                        arrayList.add(campaignEx);
                                    }
                                }
                            }
                            list = arrayList;
                        } catch (Exception e) {
                            e = e;
                            list = arrayList;
                            e.printStackTrace();
                            return list;
                        }
                    }
                } catch (Exception e2) {
                    e = e2;
                    e.printStackTrace();
                    return list;
                }
            }
            return list;
        } catch (Exception e3) {
            e = e3;
            e.printStackTrace();
            return list;
        }
    }

    public final void a(CampaignEx campaignEx, String str) {
        if (campaignEx != null) {
            try {
                if (!TextUtils.isEmpty(str)) {
                    this.c.a(campaignEx.getId(), str, campaignEx.isBidCampaign());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public final void b(CampaignEx campaignEx) {
        if (campaignEx != null) {
            try {
                if (!TextUtils.isEmpty(campaignEx.getId())) {
                    this.c.a(campaignEx.getId());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public final void a(String str, List<CampaignEx> list) {
        try {
            if (!TextUtils.isEmpty(str) && list != null && list.size() > 0) {
                for (CampaignEx campaignEx : list) {
                    if (campaignEx != null) {
                        try {
                            if (!TextUtils.isEmpty(str)) {
                                this.c.a(campaignEx, str, 0);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public final synchronized void a(long j, String str) {
        try {
            this.c.b(j, str);
        } catch (Exception e) {
            e.printStackTrace();
            g.d(f3037a, e.getMessage());
        }
    }

    public final synchronized void b(long j, String str) {
        try {
            this.c.a(j, str);
        } catch (Exception e) {
            e.printStackTrace();
            g.d(f3037a, e.getMessage());
        }
    }
}
