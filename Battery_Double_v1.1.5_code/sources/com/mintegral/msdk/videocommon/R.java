package com.mintegral.msdk.videocommon;

public final class R {

    public static final class color {
        public static final int mintegral_video_common_alertview_bg = 2131099802;
        public static final int mintegral_video_common_alertview_cancel_button_bg_default = 2131099803;
        public static final int mintegral_video_common_alertview_cancel_button_bg_pressed = 2131099804;
        public static final int mintegral_video_common_alertview_cancel_button_textcolor = 2131099805;
        public static final int mintegral_video_common_alertview_confirm_button_bg_default = 2131099806;
        public static final int mintegral_video_common_alertview_confirm_button_bg_pressed = 2131099807;
        public static final int mintegral_video_common_alertview_confirm_button_textcolor = 2131099808;
        public static final int mintegral_video_common_alertview_content_textcolor = 2131099809;
        public static final int mintegral_video_common_alertview_title_textcolor = 2131099810;

        private color() {
        }
    }

    public static final class dimen {
        public static final int mintegral_video_common_alertview_bg_padding = 2131165357;
        public static final int mintegral_video_common_alertview_button_height = 2131165358;
        public static final int mintegral_video_common_alertview_button_margintop = 2131165359;
        public static final int mintegral_video_common_alertview_button_radius = 2131165360;
        public static final int mintegral_video_common_alertview_button_textsize = 2131165361;
        public static final int mintegral_video_common_alertview_button_width = 2131165362;
        public static final int mintegral_video_common_alertview_content_margintop = 2131165363;
        public static final int mintegral_video_common_alertview_content_size = 2131165364;
        public static final int mintegral_video_common_alertview_contentview_maxwidth = 2131165365;
        public static final int mintegral_video_common_alertview_contentview_minwidth = 2131165366;
        public static final int mintegral_video_common_alertview_title_size = 2131165367;

        private dimen() {
        }
    }

    public static final class drawable {
        public static final int mintegral_video_common_alertview_bg = 2131231117;
        public static final int mintegral_video_common_alertview_cancel_bg = 2131231118;
        public static final int mintegral_video_common_alertview_cancel_bg_nor = 2131231119;
        public static final int mintegral_video_common_alertview_cancel_bg_pressed = 2131231120;
        public static final int mintegral_video_common_alertview_confirm_bg = 2131231121;
        public static final int mintegral_video_common_alertview_confirm_bg_nor = 2131231122;
        public static final int mintegral_video_common_alertview_confirm_bg_pressed = 2131231123;
        public static final int mintegral_video_common_full_star = 2131231124;
        public static final int mintegral_video_common_full_while_star = 2131231125;
        public static final int mintegral_video_common_half_star = 2131231126;

        private drawable() {
        }
    }

    public static final class id {
        public static final int mintegral_video_common_alertview_cancel_button = 2131296536;
        public static final int mintegral_video_common_alertview_confirm_button = 2131296537;
        public static final int mintegral_video_common_alertview_contentview = 2131296538;
        public static final int mintegral_video_common_alertview_titleview = 2131296539;

        private id() {
        }
    }

    public static final class layout {
        public static final int mintegral_video_common_alertview = 2131427425;

        private layout() {
        }
    }

    private R() {
    }
}
