package com.mintegral.msdk.videocommon.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import com.mintegral.msdk.b.a;
import com.mintegral.msdk.b.b;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.base.utils.p;
import com.mintegral.msdk.base.utils.r;
import java.util.Locale;

public class MTGAlertDialog extends Dialog {

    /* renamed from: a reason: collision with root package name */
    private a f3046a;
    private TextView b;
    private TextView c;
    private Button d;
    private Button e;

    public MTGAlertDialog(Context context, final a aVar) {
        super(context);
        requestWindowFeature(1);
        View inflate = LayoutInflater.from(context).inflate(p.a(context, "mintegral_video_common_alertview", TtmlNode.TAG_LAYOUT), null);
        this.f3046a = aVar;
        if (inflate != null) {
            setContentView(inflate);
            try {
                this.b = (TextView) inflate.findViewById(p.a(context, "mintegral_video_common_alertview_titleview", "id"));
            } catch (Exception e2) {
                g.a("MTGAlertDialog", e2.getMessage());
            }
            try {
                this.c = (TextView) inflate.findViewById(p.a(context, "mintegral_video_common_alertview_contentview", "id"));
                this.d = (Button) inflate.findViewById(p.a(context, "mintegral_video_common_alertview_confirm_button", "id"));
                this.e = (Button) inflate.findViewById(p.a(context, "mintegral_video_common_alertview_cancel_button", "id"));
            } catch (Exception e3) {
                g.a("MTGAlertDialog", e3.getMessage());
            }
        }
        if (this.e != null) {
            this.e.setOnClickListener(new OnClickListener() {
                public final void onClick(View view) {
                    if (aVar != null) {
                        aVar.a();
                    }
                    MTGAlertDialog.this.cancel();
                    MTGAlertDialog.this.clear();
                }
            });
        }
        if (this.d != null) {
            this.d.setOnClickListener(new OnClickListener() {
                public final void onClick(View view) {
                    if (aVar != null) {
                        aVar.b();
                    }
                    MTGAlertDialog.this.cancel();
                    MTGAlertDialog.this.clear();
                }
            });
        }
        setCanceledOnTouchOutside(false);
        setCancelable(false);
        Window window = getWindow();
        if (window != null) {
            window.setFlags(1024, 1024);
            window.setBackgroundDrawable(new ColorDrawable(0));
            window.setLayout(-1, -1);
            window.setGravity(17);
        }
    }

    public void clear() {
        if (this.f3046a != null) {
            this.f3046a = null;
        }
    }

    public void setTitle(String str) {
        if (this.b != null) {
            this.b.setText(str);
        }
    }

    public void setContent(String str) {
        if (this.c != null) {
            this.c.setText(str);
        }
    }

    public void setConfirmText(String str) {
        if (this.d != null) {
            this.d.setText(str);
        }
    }

    public void setCancelText(String str) {
        if (this.e != null) {
            this.e.setText(str);
        }
    }

    private void a(String str, String str2, String str3, String str4) {
        setTitle(str);
        setContent(str2);
        setConfirmText(str3);
        setCancelText(str4);
    }

    public void makeRVAlertView(String str) {
        try {
            Context context = getContext();
            StringBuilder sb = new StringBuilder("Mintegral_ConfirmTitle");
            sb.append(str);
            String obj = r.b(context, sb.toString(), "").toString();
            Context context2 = getContext();
            StringBuilder sb2 = new StringBuilder("Mintegral_ConfirmContent");
            sb2.append(str);
            String obj2 = r.b(context2, sb2.toString(), "").toString();
            Context context3 = getContext();
            StringBuilder sb3 = new StringBuilder("Mintegral_CancelText");
            sb3.append(str);
            String obj3 = r.b(context3, sb3.toString(), "").toString();
            Context context4 = getContext();
            StringBuilder sb4 = new StringBuilder("Mintegral_ConfirmText");
            sb4.append(str);
            String obj4 = r.b(context4, sb4.toString(), "").toString();
            b.a();
            a b2 = b.b(com.mintegral.msdk.base.controller.a.d().j());
            if (!TextUtils.isEmpty(obj) || !TextUtils.isEmpty(obj2) || !TextUtils.isEmpty(obj3) || !TextUtils.isEmpty(obj4)) {
                String language = Locale.getDefault().getLanguage();
                if (TextUtils.isEmpty(obj)) {
                    if (b2 != null) {
                        obj = b2.aW();
                    } else if (TextUtils.isEmpty(language) || !language.equals("zh")) {
                        setTitle("Confirm to close? ");
                    } else {
                        setTitle("确认关闭？");
                    }
                }
                if (TextUtils.isEmpty(obj2)) {
                    if (b2 != null) {
                        obj2 = b2.aX();
                    } else if (TextUtils.isEmpty(language) || !language.equals("zh")) {
                        setContent("You will not be rewarded after closing the window");
                    } else {
                        setContent("关闭后您将不会获得任何奖励噢~ ");
                    }
                }
                if (TextUtils.isEmpty(obj4)) {
                    if (b2 != null) {
                        obj4 = b2.aY();
                    } else if (TextUtils.isEmpty(language) || !language.equals("zh")) {
                        setConfirmText("Close it");
                    } else {
                        setConfirmText("确认关闭");
                    }
                }
                if (TextUtils.isEmpty(obj3)) {
                    if (b2 != null) {
                        obj3 = b2.aZ();
                    } else if (TextUtils.isEmpty(language) || !language.equals("zh")) {
                        setCancelText("Continue");
                    } else {
                        setCancelText("继续观看");
                    }
                }
                a(obj, obj2, obj4, obj3);
            } else if (b2 != null) {
                a(b2.aW(), b2.aX(), b2.aY(), b2.aZ());
            } else {
                String language2 = Locale.getDefault().getLanguage();
                if (TextUtils.isEmpty(language2) || !language2.equals("zh")) {
                    setTitle("Confirm to close? ");
                    setContent("You will not be rewarded after closing the window");
                    setConfirmText("Close it");
                    setCancelText("Continue");
                    return;
                }
                setTitle("确认关闭？");
                setContent("关闭后您将不会获得任何奖励噢~ ");
                setConfirmText("确认关闭");
                setCancelText("继续观看");
            }
        } catch (Exception e2) {
            g.a("MTGAlertDialog", e2.getMessage());
        }
    }

    public void makePlayableAlertView() {
        b.a();
        a b2 = b.b(com.mintegral.msdk.base.controller.a.d().j());
        if (b2 != null) {
            a(b2.aW(), b2.aX(), b2.aY(), b2.ba());
            return;
        }
        String language = Locale.getDefault().getLanguage();
        if (TextUtils.isEmpty(language) || !language.equals("zh")) {
            setTitle("Confirm to close? ");
            setContent("You will not be rewarded after closing the window");
            setConfirmText("Close it");
            setCancelText("Continue");
            return;
        }
        setTitle("确认关闭？");
        setContent("关闭后您将不会获得任何奖励噢~ ");
        setConfirmText("确认关闭");
        setCancelText("继续试玩");
    }
}
