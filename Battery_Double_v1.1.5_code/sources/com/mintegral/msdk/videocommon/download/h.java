package com.mintegral.msdk.videocommon.download;

import android.text.TextUtils;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.base.common.b.c;
import com.mintegral.msdk.base.common.b.e;
import com.mintegral.msdk.base.utils.CommonMD5;
import com.mintegral.msdk.base.utils.g;
import java.io.File;

/* compiled from: HTMLResourceManager */
public final class h {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public String f3061a;

    /* compiled from: HTMLResourceManager */
    private static class a {

        /* renamed from: a reason: collision with root package name */
        public static h f3063a = new h(0);
    }

    /* synthetic */ h(byte b) {
        this();
    }

    private h() {
        this.f3061a = e.b(c.MINTEGRAL_700_HTML);
    }

    public static h a() {
        return a.f3063a;
    }

    public final void b() {
        try {
            if (!TextUtils.isEmpty(this.f3061a)) {
                a.f3057a.a(new com.mintegral.msdk.base.common.e.a() {
                    public final void b() {
                    }

                    public final void a() {
                        com.mintegral.msdk.base.utils.e.c(h.this.f3061a);
                    }
                });
            }
        } catch (Exception e) {
            if (MIntegralConstans.DEBUG) {
                e.printStackTrace();
            }
        }
    }

    public final boolean a(String str, byte[] bArr) {
        String str2 = "HTMLResourceManager";
        try {
            StringBuilder sb = new StringBuilder("saveResHtmlFile url:");
            sb.append(str);
            g.b(str2, sb.toString());
            if (bArr != null && bArr.length > 0) {
                StringBuilder sb2 = new StringBuilder();
                sb2.append(this.f3061a);
                sb2.append("/");
                sb2.append(CommonMD5.getMD5(str));
                sb2.append(".html");
                String sb3 = sb2.toString();
                StringBuilder sb4 = new StringBuilder("saveResHtmlFile folderName:");
                sb4.append(sb3);
                g.b("HTMLResourceManager", sb4.toString());
                if (com.mintegral.msdk.base.utils.e.a(bArr, new File(sb3))) {
                    return true;
                }
            }
        } catch (Exception e) {
            if (MIntegralConstans.DEBUG) {
                e.printStackTrace();
            }
        }
        return false;
    }

    public final String a(String str) {
        try {
            String md5 = CommonMD5.getMD5(str);
            StringBuilder sb = new StringBuilder();
            sb.append(this.f3061a);
            sb.append("/");
            sb.append(md5);
            sb.append(".html");
            File file = new File(sb.toString());
            if (file.exists()) {
                return com.mintegral.msdk.base.utils.e.a(file);
            }
        } catch (Throwable th) {
            if (MIntegralConstans.DEBUG) {
                th.printStackTrace();
            }
        }
        return null;
    }
}
