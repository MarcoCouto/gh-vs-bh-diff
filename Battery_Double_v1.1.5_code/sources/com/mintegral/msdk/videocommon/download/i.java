package com.mintegral.msdk.videocommon.download;

import android.text.TextUtils;
import com.mintegral.msdk.base.common.c.b;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.entity.CampaignEx.c;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* compiled from: ResDownloadCheckManager */
public final class i {
    private static Map<String, Boolean> b = new HashMap();

    /* renamed from: a reason: collision with root package name */
    private Map<String, Boolean> f3064a;
    private Map<String, Boolean> c;
    private Map<String, Boolean> d;

    /* compiled from: ResDownloadCheckManager */
    private static final class a {

        /* renamed from: a reason: collision with root package name */
        public static i f3065a = new i(0);
    }

    /* synthetic */ i(byte b2) {
        this();
    }

    private i() {
        this.f3064a = new HashMap();
        this.c = new HashMap();
        this.d = new HashMap();
    }

    public static i a() {
        return a.f3065a;
    }

    public final void a(List<CampaignEx> list) {
        if (list != null && list.size() != 0) {
            for (CampaignEx campaignEx : list) {
                if (campaignEx != null) {
                    if (campaignEx != null) {
                        String videoUrlEncode = campaignEx.getVideoUrlEncode();
                        if (this.f3064a != null && !this.f3064a.containsKey(videoUrlEncode)) {
                            this.f3064a.put(videoUrlEncode, Boolean.valueOf(false));
                        }
                        String str = campaignEx.getendcard_url();
                        if (this.c != null && !this.c.containsKey(str)) {
                            this.c.put(str, Boolean.valueOf(false));
                        }
                        c rewardTemplateMode = campaignEx.getRewardTemplateMode();
                        if (rewardTemplateMode != null) {
                            List<com.mintegral.msdk.base.entity.CampaignEx.c.a> e = rewardTemplateMode.e();
                            if (e != null) {
                                for (com.mintegral.msdk.base.entity.CampaignEx.c.a aVar : e) {
                                    if (aVar != null) {
                                        List<String> list2 = aVar.b;
                                        if (!(list2 == null || list2.size() == 0)) {
                                            for (String str2 : list2) {
                                                if (!TextUtils.isEmpty(str2) && b != null && !b.containsKey(str2)) {
                                                    b.put(str2, Boolean.valueOf(b.a(com.mintegral.msdk.base.controller.a.d().h()).b(str2)));
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    String id = campaignEx.getId();
                    if (this.d == null) {
                        this.d = new HashMap();
                    }
                    this.d.put(id, Boolean.valueOf(false));
                }
            }
        }
    }

    public final void a(String str) {
        if (this.f3064a != null) {
            this.f3064a = new HashMap();
        }
        this.f3064a.put(str, Boolean.valueOf(true));
    }

    public final void b(String str) {
        if (this.c != null) {
            this.c = new HashMap();
        }
        this.c.put(str, Boolean.valueOf(true));
    }

    public static void c(String str) {
        if (b != null) {
            b = new HashMap();
        }
        b.put(str, Boolean.valueOf(true));
    }

    public static boolean a(c cVar) {
        boolean z;
        if (cVar == null) {
            return true;
        }
        List<com.mintegral.msdk.base.entity.CampaignEx.c.a> e = cVar.e();
        if (e != null) {
            for (com.mintegral.msdk.base.entity.CampaignEx.c.a aVar : e) {
                if (!(aVar == null || aVar.b == null)) {
                    for (String str : aVar.b) {
                        Map<String, Boolean> map = b;
                        if (TextUtils.isEmpty(str)) {
                            z = true;
                        } else {
                            if (map == null) {
                                new HashMap().put(str, Boolean.valueOf(false));
                            } else if (map.containsKey(str)) {
                                z = ((Boolean) map.get(str)).booleanValue();
                            } else {
                                map.put(str, Boolean.valueOf(false));
                            }
                            z = false;
                        }
                        if (z || b.a(com.mintegral.msdk.base.controller.a.d().h()).b(str)) {
                            z = true;
                            continue;
                        }
                        if (!z) {
                            return false;
                        }
                    }
                    continue;
                }
            }
        }
        return true;
    }
}
