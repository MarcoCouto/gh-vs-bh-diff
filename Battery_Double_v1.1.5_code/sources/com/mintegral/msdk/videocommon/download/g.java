package com.mintegral.msdk.videocommon.download;

import android.text.TextUtils;
import com.mintegral.msdk.MIntegralConstans;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.CopyOnWriteArrayList;

/* compiled from: H5DownLoadManager */
public class g {
    private static g d;
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public CopyOnWriteArrayList<String> f3058a;
    private ConcurrentMap<String, b> b;
    private boolean c = false;
    private j e;
    /* access modifiers changed from: private */
    public h f;

    /* compiled from: H5DownLoadManager */
    public interface a {
        void a(String str);

        void a(String str, String str2);
    }

    /* compiled from: H5DownLoadManager */
    public interface b {
        void a(String str);

        void a(byte[] bArr, String str);
    }

    /* compiled from: H5DownLoadManager */
    public interface c {
        void a(String str);

        void a(String str, String str2);
    }

    private g() {
        try {
            this.e = j.a();
            this.f = a.f3063a;
            this.f3058a = new CopyOnWriteArrayList<>();
            this.b = new ConcurrentHashMap();
        } catch (Throwable th) {
            com.mintegral.msdk.base.utils.g.c("H5DownLoadManager", th.getMessage(), th);
        }
    }

    public static g a() {
        if (d == null) {
            synchronized (g.class) {
                if (d == null) {
                    d = new g();
                }
            }
        }
        return d;
    }

    public final String a(String str) {
        if (this.e != null) {
            return this.e.a(str);
        }
        return null;
    }

    public final void a(String str, c cVar) {
        try {
            if (!TextUtils.isEmpty(this.e.a(str))) {
                cVar.a(str);
            } else if (this.b.containsKey(str)) {
                b bVar = (b) this.b.get(str);
                if (bVar != null) {
                    bVar.a(cVar);
                }
            } else {
                b bVar2 = new b(this.b, this.e, cVar, str);
                this.b.put(str, bVar2);
                e.a(str, bVar2);
            }
        } catch (Exception e2) {
            cVar.a("downloadzip failed", str);
            if (MIntegralConstans.DEBUG) {
                e2.printStackTrace();
            }
        }
    }

    public final void b(String str) {
        a(str, (a) null);
    }

    public final void a(final String str, final a aVar) {
        String str2 = "H5DownLoadManager";
        try {
            StringBuilder sb = new StringBuilder("download url:");
            sb.append(str);
            com.mintegral.msdk.base.utils.g.d(str2, sb.toString());
            if (!this.f3058a.contains(str)) {
                this.f3058a.add(str);
                a.f3057a.a(new com.mintegral.msdk.base.common.e.a() {
                    public final void b() {
                    }

                    public final void a() {
                        if (TextUtils.isEmpty(g.this.f.a(str))) {
                            e.a(str, new b() {
                                public final void a(byte[] bArr, String str) {
                                    try {
                                        g.this.f3058a.remove(str);
                                        if (bArr != null && bArr.length > 0) {
                                            if (g.this.f.a(str, bArr)) {
                                                if (aVar != null) {
                                                    aVar.a(str);
                                                }
                                            } else if (aVar != null) {
                                                aVar.a(str, "save file failed");
                                            }
                                        }
                                    } catch (Exception e) {
                                        if (MIntegralConstans.DEBUG) {
                                            e.printStackTrace();
                                        }
                                        if (aVar != null) {
                                            aVar.a(str, e.getMessage());
                                        }
                                    }
                                }

                                public final void a(String str) {
                                    try {
                                        g.this.f3058a.remove(str);
                                        if (aVar != null) {
                                            aVar.a(str, str);
                                        }
                                    } catch (Exception e) {
                                        if (MIntegralConstans.DEBUG) {
                                            e.printStackTrace();
                                        }
                                        if (aVar != null) {
                                            aVar.a(str, str);
                                        }
                                    }
                                }
                            });
                            return;
                        }
                        if (aVar != null) {
                            aVar.a(str);
                        }
                    }
                });
            }
        } catch (Throwable th) {
            if (MIntegralConstans.DEBUG) {
                th.printStackTrace();
            }
        }
    }
}
