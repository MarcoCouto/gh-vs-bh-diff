package com.mintegral.msdk.videocommon.download;

import android.content.Context;
import android.text.TextUtils;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.b.b;
import com.mintegral.msdk.b.d;
import com.mintegral.msdk.base.b.f;
import com.mintegral.msdk.base.b.h;
import com.mintegral.msdk.base.b.i;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.base.utils.s;
import com.mintegral.msdk.videocommon.e.c;
import com.mintegral.msdk.videocommon.listener.a;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutorService;

/* compiled from: UnitCacheCtroller */
public final class k {

    /* renamed from: a reason: collision with root package name */
    d f3069a = null;
    private List<CampaignEx> b = new ArrayList();
    /* access modifiers changed from: private */
    public boolean c = true;
    private a d;
    private d e = new d() {
        public final void a(long j, int i) {
            if (i == 5 || i == 4) {
                k.this.c = true;
                k.this.a();
            }
            if (i == 2) {
                k.this.c = true;
            }
        }
    };
    private CopyOnWriteArrayList<Map<String, a>> f = new CopyOnWriteArrayList<>();
    private Context g = com.mintegral.msdk.base.controller.a.d().h();
    private ExecutorService h;
    private long i = 3600;
    private String j;
    private c k;
    private int l = 2;
    private int m = 1;

    public k(List<CampaignEx> list, ExecutorService executorService, String str, int i2) {
        if (!(this.b == null || list == null)) {
            this.b.addAll(list);
        }
        this.h = executorService;
        this.j = str;
        this.m = i2;
        b(this.b);
    }

    public final void a(a aVar) {
        this.d = aVar;
    }

    public final void a(List<CampaignEx> list) {
        if (!(this.b == null || list == null)) {
            this.b.addAll(list);
        }
        b(this.b);
    }

    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* JADX WARNING: Missing exception handler attribute for start block: B:98:0x01c4 */
    private void b(List<CampaignEx> list) {
        boolean z;
        if (list != null && list.size() != 0) {
            if (this.f != null) {
                try {
                    synchronized (this.f) {
                        int i2 = 0;
                        while (i2 < this.f.size()) {
                            Map map = (Map) this.f.get(i2);
                            for (Entry value : map.entrySet()) {
                                a aVar = (a) value.getValue();
                                if (!(aVar == null || aVar.k() == null || !aVar.a())) {
                                    aVar.l();
                                    this.f.remove(map);
                                    i2--;
                                }
                            }
                            i2++;
                        }
                    }
                } catch (Throwable unused) {
                    g.d("UnitCacheCtroller", "cleanDisplayTask ERROR");
                }
            }
            switch (this.m) {
                case 1:
                    try {
                        if (!TextUtils.isEmpty(this.j)) {
                            b.a();
                            this.f3069a = b.c(com.mintegral.msdk.base.controller.a.d().j(), this.j);
                            if (this.f3069a == null) {
                                this.f3069a = d.b(this.j);
                            }
                            if (this.f3069a != null) {
                                this.i = this.f3069a.j();
                                this.l = this.f3069a.l();
                                break;
                            }
                        }
                    } catch (Exception unused2) {
                        g.d("UnitCacheCtroller", "make sure your had put feeds jar into your project");
                        return;
                    }
                    break;
                case 2:
                    try {
                        if (!TextUtils.isEmpty(this.j)) {
                            b.a();
                            d c2 = b.c(com.mintegral.msdk.base.controller.a.d().j(), this.j);
                            if (c2 == null) {
                                c2 = d.c(this.j);
                            }
                            if (c2 != null) {
                                this.i = c2.j();
                                this.l = c2.l();
                                break;
                            }
                        }
                    } catch (Exception unused3) {
                        g.d("UnitCacheCtroller", "make sure your had put feeds jar into your project");
                        return;
                    }
                    break;
                case 3:
                    try {
                        Class.forName("com.mintegral.msdk.videocommon.e.a");
                        com.mintegral.msdk.videocommon.e.b.a();
                        com.mintegral.msdk.videocommon.e.a b2 = com.mintegral.msdk.videocommon.e.b.b();
                        if (b2 == null) {
                            com.mintegral.msdk.videocommon.e.b.a();
                            com.mintegral.msdk.videocommon.e.b.c();
                        }
                        if (b2 != null) {
                            this.i = b2.g();
                        }
                        if (!TextUtils.isEmpty(this.j)) {
                            com.mintegral.msdk.videocommon.e.b.a();
                            this.k = com.mintegral.msdk.videocommon.e.b.a(com.mintegral.msdk.base.controller.a.d().j(), this.j);
                        }
                        if (this.k != null) {
                            this.l = this.k.G();
                            break;
                        }
                    } catch (Exception unused4) {
                        g.d("UnitCacheCtroller", "make sure your had put reward jar into your project");
                        return;
                    }
                    break;
            }
            for (int i3 = 0; i3 < list.size(); i3++) {
                CampaignEx campaignEx = (CampaignEx) list.get(i3);
                StringBuilder sb = new StringBuilder();
                sb.append(campaignEx.getId());
                sb.append(campaignEx.getVideoUrlEncode());
                sb.append(campaignEx.getBidToken());
                String sb2 = sb.toString();
                if (campaignEx != null && ((a(campaignEx) || !TextUtils.isEmpty(campaignEx.getVideoUrlEncode())) && this.f != null)) {
                    synchronized (this.f) {
                        Iterator it = this.f.iterator();
                        while (true) {
                            if (it.hasNext()) {
                                Map map2 = (Map) it.next();
                                if (map2 != null && map2.containsKey(sb2)) {
                                    a aVar2 = (a) map2.get(sb2);
                                    aVar2.a(campaignEx);
                                    aVar2.a(false);
                                    z = true;
                                }
                            } else {
                                z = false;
                            }
                        }
                        if (!z) {
                            a aVar3 = new a(this.g, campaignEx, this.h, this.j);
                            aVar3.b(this.m);
                            HashMap hashMap = new HashMap();
                            hashMap.put(sb2, aVar3);
                            this.f.add(hashMap);
                        }
                    }
                }
            }
            if (this.b != null && this.b.size() > 0) {
                this.b.clear();
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:108:0x0262 A[Catch:{ Throwable -> 0x032e }] */
    /* JADX WARNING: Removed duplicated region for block: B:114:0x028b A[Catch:{ Throwable -> 0x032e }] */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x013c A[Catch:{ Throwable -> 0x032e }] */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x0145 A[Catch:{ Throwable -> 0x032e }] */
    public final a a(int i2, boolean z) {
        int i3;
        int i4;
        List list;
        int i5;
        boolean z2;
        boolean z3 = z;
        if (this.f != null) {
            try {
                synchronized (this.f) {
                    String str = "UnitCacheCtroller";
                    StringBuilder sb = new StringBuilder("UnitCache isReady ===== campaignDownLoadTaskList.size() = ");
                    sb.append(this.f.size());
                    g.a(str, sb.toString());
                    f a2 = f.a((h) i.a(com.mintegral.msdk.base.controller.a.d().h()));
                    List k2 = a2.k(this.j);
                    List j2 = a2.j(this.j);
                    long currentTimeMillis = System.currentTimeMillis();
                    int i6 = 0;
                    while (i3 < this.f.size()) {
                        Map map = (Map) this.f.get(i3);
                        for (Entry value : map.entrySet()) {
                            a aVar = (a) value.getValue();
                            if (aVar != null) {
                                if (aVar.k() != null) {
                                    CampaignEx k3 = aVar.k();
                                    if ((z3 && !k3.isBidCampaign()) || (!z3 && k3.isBidCampaign())) {
                                        StringBuilder sb2 = new StringBuilder("UnitCache isReady ==== isBidCampaign = ");
                                        sb2.append(z3);
                                        sb2.append(" campaign.isBidCampaign() = ");
                                        sb2.append(k3.isBidCampaign());
                                        g.b("UnitCacheCtroller", sb2.toString());
                                    } else if (!k2.contains(k3.getId())) {
                                        g.b("UnitCacheCtroller", "UnitCache isReady ====  Campaign Expired continue");
                                    } else {
                                        String str2 = k3.getendcard_url();
                                        String videoUrlEncode = k3.getVideoUrlEncode();
                                        String str3 = "";
                                        if (!(k3 == null || k3.getRewardTemplateMode() == null)) {
                                            str3 = k3.getRewardTemplateMode().d();
                                        }
                                        String str4 = str3;
                                        CampaignEx.c rewardTemplateMode = k3.getRewardTemplateMode();
                                        list = k2;
                                        if (this.m == 3) {
                                            if (!j2.contains(videoUrlEncode)) {
                                                g.b("UnitCacheCtroller", "UnitCache isReady ====  videoUrlList not contain videoURl continue");
                                            } else {
                                                StringBuilder sb3 = new StringBuilder("check template");
                                                sb3.append(str4);
                                                g.a("UnitCacheCtroller", sb3.toString());
                                                if (!TextUtils.isEmpty(str4)) {
                                                    StringBuilder sb4 = new StringBuilder("check template 下载情况：");
                                                    sb4.append(g.a().a(str4));
                                                    g.a("UnitCacheCtroller", sb4.toString());
                                                    if (com.mintegral.msdk.videocommon.a.a(i2, k3) == null) {
                                                        z2 = false;
                                                        if (z2) {
                                                            g.b("UnitCacheCtroller", "UnitCache isReady ====  templateZipDownload check false continue");
                                                        } else if (b(str2, k3)) {
                                                            if (aVar.a()) {
                                                                aVar.l();
                                                                g.b("UnitCacheCtroller", "isready endcard下载完 但是offer展示过 continue");
                                                            } else if (s.a(videoUrlEncode)) {
                                                                if (a(rewardTemplateMode)) {
                                                                    g.b("UnitCacheCtroller", "endcard为基准 endcard和图片下载完成 videourl为空不用下载 return task");
                                                                    return aVar;
                                                                }
                                                            } else if (a(aVar, d()) && a(rewardTemplateMode)) {
                                                                g.b("UnitCacheCtroller", "endcard为基准 endcard 图片 和 videourl 下载完成 return task");
                                                                return aVar;
                                                            }
                                                        }
                                                    }
                                                } else {
                                                    int i7 = i2;
                                                }
                                                z2 = true;
                                                if (z2) {
                                                }
                                            }
                                            i4 = i3;
                                            k2 = list;
                                            i3 = i4;
                                            z3 = z;
                                        } else {
                                            int i8 = i2;
                                        }
                                        boolean j3 = aVar.j();
                                        int h2 = aVar.h();
                                        StringBuilder sb5 = new StringBuilder("isready unit state:");
                                        sb5.append(h2);
                                        g.a("UnitCacheCtroller", sb5.toString());
                                        if (h2 != 5) {
                                            long b2 = aVar.b();
                                            if (aVar.h() == 1) {
                                                Runnable e2 = aVar.e();
                                                i5 = i3;
                                                if (currentTimeMillis - b2 > this.i * 1000 || e2 == null) {
                                                    aVar.a("download timeout");
                                                    aVar.o();
                                                    this.f.remove(map);
                                                    i3 = i5 - 1;
                                                    if (this.m == 1 || this.m == 3) {
                                                        g.b("UnitCacheCtroller", "isready download !timeout continue");
                                                    }
                                                    if (this.m == 2) {
                                                        if (h2 != 4) {
                                                            if (h2 != 2) {
                                                                if (h2 == 1) {
                                                                    if (aVar.a()) {
                                                                        g.b("UnitCacheCtroller", "isready run 已经被展示过 continue");
                                                                    } else if (!MIntegralConstans.IS_DOWANLOAD_FINSH_PLAY && a(aVar, d()) && a(str2, k3) && a(rewardTemplateMode)) {
                                                                        StringBuilder sb6 = new StringBuilder("isready  IS_DOWANLOAD_FINSH_PLAY is :");
                                                                        sb6.append(MIntegralConstans.IS_DOWANLOAD_FINSH_PLAY);
                                                                        g.b("UnitCacheCtroller", sb6.toString());
                                                                        return aVar;
                                                                    }
                                                                }
                                                                if (this.m == 3 && a(aVar, d()) && a(str2, k3) && a(rewardTemplateMode)) {
                                                                    g.b("UnitCacheCtroller", "isready ad_type == CommonConst.REWARD_VIDEO_AD_TYPE ready_rate 三个条件is true");
                                                                    return aVar;
                                                                }
                                                            }
                                                        }
                                                        this.f.remove(map);
                                                        aVar.o();
                                                        i3--;
                                                        g.b("UnitCacheCtroller", "isready stop continue");
                                                    } else if (aVar.a()) {
                                                        aVar.l();
                                                        this.f.remove(map);
                                                        i3--;
                                                    } else {
                                                        StringBuilder sb7 = new StringBuilder("==========isready ad_type is :");
                                                        sb7.append(this.m);
                                                        g.b("UnitCacheCtroller", sb7.toString());
                                                        return aVar;
                                                    }
                                                }
                                            } else {
                                                i5 = i3;
                                            }
                                            i3 = i5;
                                            if (this.m == 2) {
                                            }
                                        } else if (aVar.a()) {
                                            aVar.l();
                                            this.f.remove(map);
                                            i3--;
                                            g.b("UnitCacheCtroller", "isready state == DownLoadConstant.DOWNLOAD_DONE 但是offer展示过 continue");
                                        } else if (!j3) {
                                            aVar.i();
                                            if (this.m == 2) {
                                                StringBuilder sb8 = new StringBuilder("isready ==========done but isEffectivePath:");
                                                sb8.append(j3);
                                                sb8.append(" is feed");
                                                sb8.append(this.m);
                                                g.b("UnitCacheCtroller", sb8.toString());
                                                return aVar;
                                            }
                                            g.b("UnitCacheCtroller", "isready !isEffectivePath continue");
                                            i4 = i3;
                                            k2 = list;
                                            i3 = i4;
                                            z3 = z;
                                        } else if (!a(str2, k3) || !a(rewardTemplateMode)) {
                                            g.b("UnitCacheCtroller", "isready done but continue");
                                            return null;
                                        } else {
                                            g.b("UnitCacheCtroller", "isready videourl为基准 state＝done endcard 图片 和 videourl 下载完成 return task");
                                            return aVar;
                                        }
                                        k2 = list;
                                        z3 = z;
                                    }
                                    list = k2;
                                    i4 = i3;
                                    k2 = list;
                                    i3 = i4;
                                    z3 = z;
                                }
                            }
                            list = k2;
                            i4 = i3;
                            g.b("UnitCacheCtroller", "UnitCache isReady ==== task 或者 campaign为空 continue");
                            k2 = list;
                            i3 = i4;
                            z3 = z;
                        }
                        List list2 = k2;
                        i6 = i3 + 1;
                        z3 = z;
                    }
                }
            } catch (Throwable th) {
                g.c("UnitCacheCtroller", th.getMessage(), th);
            }
        }
        return null;
    }

    public static boolean a(a aVar, int i2) {
        long m2 = aVar.m();
        long d2 = aVar.d();
        StringBuilder sb = new StringBuilder("=========downloaded_file_size:");
        sb.append(m2);
        sb.append("--file_size:");
        sb.append(d2);
        sb.append("---ready_rate:");
        sb.append(i2);
        sb.append("--getVideoUrlEncode:");
        sb.append(aVar.k().getVideoUrlEncode());
        g.b("UnitCacheCtroller", sb.toString());
        if (i2 == 0) {
            return aVar.k() != null && !TextUtils.isEmpty(aVar.k().getVideoUrlEncode());
        }
        if (d2 > 0 && m2 * 100 >= d2 * ((long) i2)) {
            if (i2 != 100 || aVar.h() == 5) {
                return true;
            }
            aVar.o();
            return false;
        }
    }

    private static boolean a(String str, CampaignEx campaignEx) {
        try {
            if (s.a(str)) {
                g.b("UnitCacheCtroller", "checkEndcardDownload endcardUrl is null return true");
                return true;
            }
            if (b(str, campaignEx)) {
                g.b("UnitCacheCtroller", "checkEndcardDownload endcardUrl done return true");
                return true;
            }
            g.b("UnitCacheCtroller", "checkEndcardDownload endcardUrl return false");
            return false;
        } catch (Throwable th) {
            g.c("UnitCacheCtroller", th.getMessage(), th);
        }
    }

    private static boolean b(String str, CampaignEx campaignEx) {
        if (campaignEx.isMraid()) {
            g.b("UnitCacheCtroller", "Campaign is Mraid, do not need download endcardurl");
            return true;
        } else if (s.b(g.a().a(str))) {
            StringBuilder sb = new StringBuilder("endcard zip 下载完成 return true endcardUrl:");
            sb.append(str);
            g.b("UnitCacheCtroller", sb.toString());
            return true;
        } else if (s.b(a.f3063a.a(str))) {
            StringBuilder sb2 = new StringBuilder("endcard url 源码 下载完成 return true endcardUrl:");
            sb2.append(str);
            g.b("UnitCacheCtroller", sb2.toString());
            return true;
        } else {
            StringBuilder sb3 = new StringBuilder("checkEndcardZipOrSourceDownLoad endcardUrl return false endcardUrl:");
            sb3.append(str);
            g.b("UnitCacheCtroller", sb3.toString());
            return false;
        }
    }

    private static boolean a(CampaignEx.c cVar) {
        try {
            i.a();
            boolean a2 = i.a(cVar);
            StringBuilder sb = new StringBuilder("checkImageListDownload checkImageState:");
            sb.append(a2);
            g.b("UnitCacheCtroller", sb.toString());
            return a2;
        } catch (Throwable th) {
            g.c("UnitCacheCtroller", th.getMessage(), th);
            g.b("UnitCacheCtroller", "checkImageListDownload checkImageState:false");
            return false;
        }
    }

    public final a b(int i2, boolean z) {
        try {
            return a(i2, z);
        } catch (Throwable th) {
            g.c("UnitCacheCtroller", th.getMessage(), th);
            return null;
        }
    }

    public final a a(String str) {
        if (this.f != null) {
            synchronized (this.f) {
                try {
                    Iterator it = this.f.iterator();
                    while (it.hasNext()) {
                        Map map = (Map) it.next();
                        if (map != null && map.containsKey(str)) {
                            a aVar = (a) map.get(str);
                            return aVar;
                        }
                    }
                } catch (Throwable th) {
                    throw th;
                }
            }
        }
        return null;
        return null;
    }

    private static boolean a(CopyOnWriteArrayList<Map<String, a>> copyOnWriteArrayList) {
        try {
            Iterator it = copyOnWriteArrayList.iterator();
            while (it.hasNext()) {
                Map map = (Map) it.next();
                if (map != null) {
                    for (Entry value : map.entrySet()) {
                        if (((a) value.getValue()).h() == 1) {
                            return true;
                        }
                    }
                    continue;
                }
            }
        } catch (Throwable th) {
            if (MIntegralConstans.DEBUG) {
                th.printStackTrace();
            }
        }
        return false;
    }

    public final void b() {
        if (this.f != null) {
            try {
                synchronized (this.f) {
                    Iterator it = this.f.iterator();
                    while (it.hasNext()) {
                        Map map = (Map) it.next();
                        if (map != null) {
                            for (Entry value : map.entrySet()) {
                                a aVar = (a) value.getValue();
                                if (aVar != null && aVar.h() == 1 && aVar.g()) {
                                    g.b("UnitCacheCtroller", "暂停所有下载");
                                    aVar.a("playing and stop download");
                                    aVar.o();
                                    this.f.remove(map);
                                    return;
                                }
                            }
                            continue;
                        }
                    }
                }
            } catch (Throwable unused) {
            }
        }
    }

    public final CopyOnWriteArrayList<Map<String, a>> c() {
        return this.f;
    }

    private static boolean a(CampaignEx campaignEx) {
        if (campaignEx != null) {
            try {
                if (campaignEx.getPlayable_ads_without_video() == 2) {
                    return true;
                }
            } catch (Throwable th) {
                if (MIntegralConstans.DEBUG) {
                    th.printStackTrace();
                }
            }
        }
        return false;
    }

    private int d() {
        try {
            if (this.k == null) {
                this.k = com.mintegral.msdk.videocommon.e.b.d();
            }
            return this.k.t();
        } catch (Throwable th) {
            g.c("UnitCacheCtroller", th.getMessage(), th);
            return 100;
        }
    }

    public final void a() {
        if (this.f != null) {
            try {
                synchronized (this.f) {
                    long currentTimeMillis = System.currentTimeMillis();
                    int i2 = 0;
                    while (i2 < this.f.size()) {
                        Map map = (Map) this.f.get(i2);
                        for (Entry value : map.entrySet()) {
                            a aVar = (a) value.getValue();
                            if (aVar != null) {
                                if (currentTimeMillis - aVar.b() > this.i * 1000 && aVar.h() == 1) {
                                    aVar.a("download timeout");
                                    aVar.o();
                                    this.f.remove(map);
                                    i2--;
                                }
                                if (!(aVar.h() == 1 || aVar.h() == 5 || aVar.h() == 0)) {
                                    aVar.o();
                                    this.f.remove(map);
                                    i2--;
                                }
                            }
                        }
                        i2++;
                    }
                }
            } catch (Throwable th) {
                th.printStackTrace();
            }
        }
        if (this.f != null) {
            try {
                synchronized (this.f) {
                    if (!a(this.f)) {
                        this.c = true;
                    }
                    Iterator it = this.f.iterator();
                    while (it.hasNext()) {
                        Map map2 = (Map) it.next();
                        if (map2 != null) {
                            for (Entry value2 : map2.entrySet()) {
                                a aVar2 = (a) value2.getValue();
                                if (aVar2 != null && !aVar2.a()) {
                                    if (this.m == 2) {
                                        this.c = true;
                                    }
                                    int h2 = aVar2.h();
                                    CampaignEx k2 = aVar2.k();
                                    if (k2 != null && h2 == 0) {
                                        c instance = c.getInstance();
                                        k2.getId();
                                        h2 = instance.a(k2.getVideoUrlEncode(), aVar2.n());
                                    }
                                    aVar2.a(this.d);
                                    if (!(h2 == 1 || h2 == 5 || h2 == 4)) {
                                        if (com.mintegral.msdk.base.utils.c.p(this.g) != 9 && this.l == 2) {
                                            return;
                                        }
                                        if (this.l != 3) {
                                            if (h2 == 2 || this.c) {
                                                this.c = false;
                                                aVar2.b(this.e);
                                                int d2 = d();
                                                if (this.m == 1) {
                                                    if (this.f3069a == null) {
                                                        this.f3069a = d.b(this.j);
                                                    }
                                                    d2 = this.f3069a.d();
                                                }
                                                aVar2.a(d2);
                                                aVar2.f();
                                                if (this.m == 3) {
                                                    return;
                                                }
                                            }
                                        } else {
                                            return;
                                        }
                                    }
                                }
                            }
                            continue;
                        }
                    }
                }
            } catch (Throwable th2) {
                th2.printStackTrace();
            }
        }
    }
}
