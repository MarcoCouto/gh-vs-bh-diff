package com.mintegral.msdk.videocommon.download;

import android.content.Context;
import android.text.TextUtils;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.videocommon.a;
import com.mintegral.msdk.videocommon.e.b;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/* compiled from: DownLoadManager */
public class c {

    /* renamed from: a reason: collision with root package name */
    private static c f3054a;
    private ThreadPoolExecutor b;
    private boolean c = false;
    private ConcurrentHashMap<String, k> d = new ConcurrentHashMap<>();

    private c() {
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(5, 5, 15, TimeUnit.SECONDS, new LinkedBlockingDeque());
        this.b = threadPoolExecutor;
        this.b.allowCoreThreadTimeOut(true);
    }

    public static c getInstance() {
        if (f3054a == null) {
            synchronized (c.class) {
                if (f3054a == null) {
                    f3054a = new c();
                }
            }
        }
        return f3054a;
    }

    public final a a(String str, String str2) {
        k a2 = a(str);
        if (a2 != null) {
            return a2.a(str2);
        }
        return null;
    }

    public final a a(int i, String str, boolean z) {
        k a2 = a(str);
        if (a2 != null) {
            return a2.b(i, z);
        }
        return null;
    }

    private k a(String str) {
        if (this.d == null || !this.d.containsKey(str)) {
            return null;
        }
        return (k) this.d.get(str);
    }

    /* JADX WARNING: Removed duplicated region for block: B:50:0x00a7  */
    public final boolean b(int i, String str, boolean z) {
        boolean z2;
        try {
            k a2 = a(str);
            z2 = (a2 == null || a2.a(i, z) == null) ? false : true;
            try {
                CopyOnWriteArrayList c2 = a2.c();
                if (c2 != null && c2.size() > 0) {
                    ArrayList arrayList = new ArrayList();
                    for (int i2 = 0; i2 < c2.size(); i2++) {
                        Map map = (Map) c2.get(i2);
                        if (map != null) {
                            Set entrySet = map.entrySet();
                            if (entrySet == null || entrySet.size() <= 0) {
                                break;
                            }
                            Iterator it = entrySet.iterator();
                            if (it == null) {
                                break;
                            } else if (it.hasNext()) {
                                arrayList.add(((a) ((Entry) it.next()).getValue()).k().getRequestIdNotice());
                            }
                        }
                    }
                    ConcurrentHashMap a3 = a.a(i, z);
                    if (a3 != null && a3.size() > 0 && arrayList.size() > 0) {
                        Iterator it2 = a3.entrySet().iterator();
                        while (it2.hasNext()) {
                            if (!arrayList.contains(((Entry) it2.next()).getKey())) {
                                it2.remove();
                            }
                        }
                    }
                }
            } catch (Exception e) {
                try {
                    if (MIntegralConstans.DEBUG) {
                        e.printStackTrace();
                    }
                } catch (Exception e2) {
                    e = e2;
                    if (MIntegralConstans.DEBUG) {
                    }
                    return z2;
                }
            }
        } catch (Exception e3) {
            e = e3;
            z2 = false;
            if (MIntegralConstans.DEBUG) {
                e.printStackTrace();
            }
            return z2;
        }
        return z2;
    }

    public void load(String str) {
        k a2 = a(str);
        if (a2 != null) {
            a2.a();
        }
    }

    public k createUnitCache(Context context, String str, List list, int i, com.mintegral.msdk.videocommon.listener.a aVar) {
        k kVar;
        if (TextUtils.isEmpty(str) || list == null) {
            return null;
        }
        if (this.d.containsKey(str)) {
            kVar = (k) this.d.get(str);
            kVar.a(aVar);
            kVar.a(list);
        } else {
            kVar = new k(list, this.b, str, i);
            kVar.a(aVar);
            this.d.put(str, kVar);
        }
        return kVar;
    }

    public final void a(boolean z) {
        this.c = z;
        if (this.d != null) {
            for (Entry value : this.d.entrySet()) {
                k kVar = (k) value.getValue();
                if (kVar != null) {
                    kVar.b();
                }
            }
        }
    }

    public final int a(String str, d dVar) {
        if (this.d != null) {
            for (Entry value : this.d.entrySet()) {
                k kVar = (k) value.getValue();
                if (kVar != null) {
                    CopyOnWriteArrayList c2 = kVar.c();
                    if (c2 != null) {
                        int size = c2.size();
                        for (int i = 0; i < size; i++) {
                            Map map = (Map) c2.get(i);
                            if (map != null) {
                                Iterator it = map.entrySet().iterator();
                                if (it.hasNext()) {
                                    a aVar = (a) ((Entry) it.next()).getValue();
                                    if (aVar != null && aVar.g()) {
                                        CampaignEx k = aVar.k();
                                        if (k != null) {
                                            String videoUrlEncode = k.getVideoUrlEncode();
                                            if (!TextUtils.isEmpty(str) && !TextUtils.isEmpty(videoUrlEncode) && str.equals(videoUrlEncode) && aVar.g()) {
                                                aVar.a(dVar);
                                                return aVar.h();
                                            }
                                        } else {
                                            continue;
                                        }
                                    }
                                } else {
                                    continue;
                                }
                            }
                        }
                        continue;
                    } else {
                        continue;
                    }
                }
            }
        }
        return 0;
    }

    public final void b(boolean z) {
        if (!z) {
            this.c = false;
        } else if (this.c) {
            return;
        }
        if (this.d != null) {
            for (Entry value : this.d.entrySet()) {
                ((k) value.getValue()).a();
            }
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(3:15|16|(4:18|(1:20)|21|(2:23|31)(2:24|32))(1:30)) */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0050, code lost:
        if (android.text.TextUtils.isEmpty(r1) == false) goto L_0x0052;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0052, code lost:
        com.mintegral.msdk.b.b.a();
        r4 = com.mintegral.msdk.b.b.c(com.mintegral.msdk.base.controller.a.d().j(), r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0061, code lost:
        if (r4 == null) goto L_0x0063;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0063, code lost:
        r4 = com.mintegral.msdk.b.d.c(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x006b, code lost:
        if (r4.l() == 2) goto L_0x006d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x006d, code lost:
        r2.b();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0071, code lost:
        r2.a();
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:15:0x004c */
    public final void a() {
        if (this.d != null) {
            for (Entry entry : this.d.entrySet()) {
                k kVar = (k) entry.getValue();
                String str = (String) entry.getKey();
                Class.forName("com.mintegral.msdk.videocommon.e.a");
                b.a();
                com.mintegral.msdk.videocommon.e.c a2 = b.a(com.mintegral.msdk.base.controller.a.d().j(), str);
                if (a2 != null) {
                    if (a2.G() == 2) {
                        kVar.b();
                    } else {
                        kVar.a();
                    }
                }
            }
        }
    }
}
