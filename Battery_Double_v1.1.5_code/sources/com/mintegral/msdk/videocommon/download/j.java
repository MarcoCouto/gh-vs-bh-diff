package com.mintegral.msdk.videocommon.download;

import android.net.Uri;
import android.text.TextUtils;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.base.common.b.c;
import com.mintegral.msdk.base.common.b.e;
import com.mintegral.msdk.base.utils.CommonMD5;
import com.mintegral.msdk.base.utils.d;
import com.mintegral.msdk.base.utils.g;
import com.vungle.warren.model.Advertisement;
import java.io.File;
import java.util.List;

/* compiled from: ResourceManager */
public final class j {

    /* renamed from: a reason: collision with root package name */
    private static String f3066a = "ResourceManager";
    /* access modifiers changed from: private */
    public String b;

    /* compiled from: ResourceManager */
    private static class a {

        /* renamed from: a reason: collision with root package name */
        public static j f3068a = new j(0);
    }

    /* synthetic */ j(byte b2) {
        this();
    }

    private j() {
        this.b = e.b(c.MINTEGRAL_700_RES);
    }

    public static j a() {
        return a.f3068a;
    }

    public final void b() {
        try {
            if (!TextUtils.isEmpty(this.b)) {
                a.f3057a.a(new com.mintegral.msdk.base.common.e.a() {
                    public final void b() {
                    }

                    public final void a() {
                        com.mintegral.msdk.base.utils.e.c(j.this.b);
                    }
                });
            }
        } catch (Exception e) {
            if (MIntegralConstans.DEBUG) {
                e.printStackTrace();
            }
        }
    }

    public final boolean a(String str, byte[] bArr) {
        if (bArr != null) {
            try {
                if (bArr.length > 0) {
                    StringBuilder sb = new StringBuilder();
                    sb.append(this.b);
                    sb.append("/");
                    sb.append(CommonMD5.getMD5(str));
                    sb.append(".zip");
                    String sb2 = sb.toString();
                    File file = new File(sb2);
                    if (com.mintegral.msdk.base.utils.e.a(bArr, file)) {
                        List queryParameters = Uri.parse(str).getQueryParameters("md5filename");
                        if (queryParameters != null && queryParameters.size() > 0) {
                            String str2 = (String) queryParameters.get(0);
                            if (!TextUtils.isEmpty(str2) && str2.equals(d.a(file))) {
                                StringBuilder sb3 = new StringBuilder();
                                sb3.append(this.b);
                                sb3.append("/");
                                sb3.append(CommonMD5.getMD5(str));
                                if (com.mintegral.msdk.base.utils.e.a(sb2, sb3.toString())) {
                                    com.mintegral.msdk.base.utils.e.b(file);
                                    return true;
                                }
                            }
                        }
                    }
                    com.mintegral.msdk.base.utils.e.b(file);
                }
            } catch (Exception e) {
                if (MIntegralConstans.DEBUG) {
                    e.printStackTrace();
                }
            }
        }
        return false;
    }

    public final String a(String str) {
        String str2;
        String str3;
        try {
            String md5 = CommonMD5.getMD5(str);
            StringBuilder sb = new StringBuilder();
            sb.append(this.b);
            sb.append("/");
            sb.append(md5);
            String sb2 = sb.toString();
            List queryParameters = Uri.parse(str).getQueryParameters("foldername");
            String str4 = f3066a;
            StringBuilder sb3 = new StringBuilder("check zip 下载情况：url:");
            sb3.append(str);
            g.a(str4, sb3.toString());
            String str5 = f3066a;
            StringBuilder sb4 = new StringBuilder("check zip 下载情况：indexHtml:");
            sb4.append(queryParameters);
            g.a(str5, sb4.toString());
            if (queryParameters != null && queryParameters.size() > 0) {
                String str6 = (String) queryParameters.get(0);
                if (!TextUtils.isEmpty(str6)) {
                    StringBuilder sb5 = new StringBuilder();
                    sb5.append(sb2);
                    sb5.append("/");
                    sb5.append(str6);
                    sb5.append("/");
                    sb5.append(str6);
                    sb5.append(".html");
                    String sb6 = sb5.toString();
                    if (com.mintegral.msdk.base.utils.e.a(sb6)) {
                        try {
                            str2 = str.substring(str.indexOf("?") + 1);
                        } catch (Exception unused) {
                            str2 = "";
                        }
                        if (!TextUtils.isEmpty(str2)) {
                            StringBuilder sb7 = new StringBuilder("?");
                            sb7.append(str2);
                            str3 = sb7.toString();
                        } else {
                            str3 = "";
                        }
                        StringBuilder sb8 = new StringBuilder(Advertisement.FILE_SCHEME);
                        sb8.append(sb6);
                        sb8.append(str3);
                        return sb8.toString();
                    }
                }
            }
        } catch (Exception e) {
            if (MIntegralConstans.DEBUG) {
                e.printStackTrace();
            }
        }
        return null;
    }
}
