package com.mintegral.msdk.videocommon.download;

import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.videocommon.download.g.c;
import java.util.concurrent.ConcurrentMap;

/* compiled from: DownLoadH5SourceListener */
public final class b implements com.mintegral.msdk.videocommon.download.g.b {

    /* renamed from: a reason: collision with root package name */
    private static String f3053a = "DownLoadH5SourceListener";
    private ConcurrentMap<String, b> b;
    private j c;
    private c d;
    private String e;

    public b(ConcurrentMap<String, b> concurrentMap, j jVar, c cVar, String str) {
        this.b = concurrentMap;
        this.c = jVar;
        this.d = cVar;
        this.e = str;
    }

    public final void a(c cVar) {
        this.d = cVar;
    }

    /* JADX WARNING: Removed duplicated region for block: B:33:0x0058  */
    public final void a(byte[] bArr, String str) {
        String str2 = "";
        try {
            if (this.b == null) {
                if (this.d != null) {
                    this.d.a("mResDownloadingMap  is null", str);
                }
                return;
            }
            if (this.b.containsKey(str)) {
                this.b.remove(str);
            }
            if (bArr == null || bArr.length <= 0) {
                str2 = "response data is error";
                if (this.d != null) {
                    this.d.a(str2, str);
                }
            }
            if (!this.c.a(str, bArr)) {
                str2 = "data save failed";
            } else if (this.d != null) {
                this.d.a(str);
                return;
            }
            if (this.d != null) {
            }
        } catch (Exception e2) {
            if (MIntegralConstans.DEBUG) {
                e2.printStackTrace();
            }
            str2 = e2.getMessage();
        } catch (Throwable th) {
            g.c(f3053a, th.getMessage(), th);
        }
    }

    public final void a(String str) {
        try {
            if (this.b == null) {
                if (this.d != null) {
                    this.d.a("mResDownloadingMap  is null", this.e);
                }
                return;
            }
            if (this.b.containsKey(this.e)) {
                this.b.remove(this.e);
            }
            if (this.d != null) {
                this.d.a(str, this.e);
            }
        } catch (Exception e2) {
            if (MIntegralConstans.DEBUG) {
                e2.printStackTrace();
            }
            str = e2.getMessage();
        } catch (Throwable th) {
            g.c(f3053a, th.getMessage(), th);
        }
    }
}
