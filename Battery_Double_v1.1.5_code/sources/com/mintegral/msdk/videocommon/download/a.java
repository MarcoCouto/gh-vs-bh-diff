package com.mintegral.msdk.videocommon.download;

import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.TextUtils;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.base.b.h;
import com.mintegral.msdk.base.b.i;
import com.mintegral.msdk.base.b.v;
import com.mintegral.msdk.base.b.w;
import com.mintegral.msdk.base.common.b.c;
import com.mintegral.msdk.base.common.b.e;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.entity.o;
import com.mintegral.msdk.base.entity.p;
import com.mintegral.msdk.base.utils.CommonMD5;
import com.mintegral.msdk.base.utils.d;
import com.mintegral.msdk.base.utils.g;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.io.File;
import java.io.FileOutputStream;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutorService;

/* compiled from: CampaignDownLoadTask */
public final class a implements Serializable {
    private int A = 100;
    private boolean B = false;
    /* access modifiers changed from: private */
    public v C;
    private boolean D = false;
    /* access modifiers changed from: private */
    public String E;
    private d F = new d() {
        public final void a(long j, int i) {
            if (!a.this.m) {
                a.a(a.this, j, i);
            }
        }
    };
    /* access modifiers changed from: private */
    public Handler G = new Handler(Looper.getMainLooper()) {
        public final void handleMessage(Message message) {
            a.this.C = v.a((h) i.a(com.mintegral.msdk.base.controller.a.d().h()));
            switch (message.what) {
                case 1:
                    a.a(a.this, a.this.s, a.this.c);
                    return;
                case 2:
                    if (a.this.c != 2) {
                        a.this.c = 2;
                        a.a(a.this, a.this.s, a.this.c);
                        a.this.o();
                        return;
                    }
                    break;
                case 3:
                    if (!(a.this.c == 4 || a.this.c == 2 || a.this.c == 5)) {
                        a.this.c = 4;
                        a.a(a.this, a.this.s, a.this.c);
                        a.this.o();
                        return;
                    }
                case 4:
                    a.this.c = 5;
                    a.this.a(1, "");
                    a.a(a.this, a.this.s, a.this.c);
                    return;
                case 5:
                    a.this.f();
                    break;
            }
        }
    };
    private int H;
    private File I;

    /* renamed from: a reason: collision with root package name */
    private boolean f3049a = false;
    private Runnable b;
    /* access modifiers changed from: private */
    public int c = 0;
    private CopyOnWriteArrayList<d> d = new CopyOnWriteArrayList<>();
    private d e;
    private ExecutorService f;
    private Class g;
    private Object h;
    private Class i;
    private Object j;
    /* access modifiers changed from: private */
    public CampaignEx k;
    private String l;
    /* access modifiers changed from: private */
    public boolean m = false;
    private Context n;
    /* access modifiers changed from: private */
    public int o;
    private String p;
    /* access modifiers changed from: private */
    public String q;
    /* access modifiers changed from: private */
    public boolean r = true;
    /* access modifiers changed from: private */
    public long s = 0;
    private int t = 0;
    private String u;
    private boolean v = false;
    private boolean w = false;
    private long x;
    private com.mintegral.msdk.videocommon.listener.a y;
    private com.mintegral.msdk.videocommon.listener.a z;

    public a(Context context, CampaignEx campaignEx, ExecutorService executorService, String str) {
        if (context != null || campaignEx != null) {
            this.x = System.currentTimeMillis();
            this.n = com.mintegral.msdk.base.controller.a.d().h();
            this.k = campaignEx;
            this.l = str;
            this.f = executorService;
            if (this.k != null) {
                this.p = this.k.getVideoUrlEncode();
                StringBuilder sb = new StringBuilder("=========CampaignDownLoadTask: title:");
                sb.append(campaignEx.getAppName());
                g.d("CampaignDownLoadTask", sb.toString());
            }
            String str2 = this.p;
            String str3 = "";
            if (!TextUtils.isEmpty(str2)) {
                str3 = CommonMD5.getMD5(str2.trim());
            }
            this.E = str3;
            this.q = e.b(c.MINTEGRAL_VC);
            StringBuilder sb2 = new StringBuilder();
            sb2.append(this.q);
            sb2.append(File.separator);
            sb2.append(this.E);
            this.u = sb2.toString();
            StringBuilder sb3 = new StringBuilder("videoLocalPath:");
            sb3.append(this.u);
            g.b("CampaignDownLoadTask", sb3.toString());
            try {
                if (!TextUtils.isEmpty(this.p)) {
                    File file = null;
                    if (!TextUtils.isEmpty(this.q)) {
                        file = new File(this.q);
                        if (!file.exists()) {
                            file.mkdirs();
                        }
                    }
                    if (file != null && file.exists() && (this.I == null || !this.I.exists())) {
                        StringBuilder sb4 = new StringBuilder();
                        sb4.append(file);
                        sb4.append("/.nomedia");
                        this.I = new File(sb4.toString());
                        if (!this.I.exists()) {
                            this.I.createNewFile();
                        }
                    }
                    c(true);
                    b(this.p);
                }
            } catch (Exception e2) {
                g.b("CampaignDownLoadTask", e2.getMessage());
            }
        }
    }

    public final boolean a() {
        return this.D;
    }

    public final void a(boolean z2) {
        this.D = z2;
    }

    public final long b() {
        return this.x;
    }

    public final void b(boolean z2) {
        this.w = z2;
    }

    public final String c() {
        return this.u;
    }

    public final long d() {
        return (long) this.o;
    }

    public final Runnable e() {
        return this.b;
    }

    private void b(final String str) {
        this.b = new Runnable() {
            /* JADX WARNING: type inference failed for: r0v4 */
            /* JADX WARNING: type inference failed for: r6v0, types: [java.io.OutputStream] */
            /* JADX WARNING: type inference failed for: r3v0, types: [java.io.InputStream] */
            /* JADX WARNING: type inference failed for: r6v1 */
            /* JADX WARNING: type inference failed for: r3v4 */
            /* JADX WARNING: type inference failed for: r6v2, types: [java.io.OutputStream] */
            /* JADX WARNING: type inference failed for: r3v5, types: [java.io.InputStream] */
            /* JADX WARNING: type inference failed for: r6v3 */
            /* JADX WARNING: type inference failed for: r3v7 */
            /* JADX WARNING: type inference failed for: r6v4 */
            /* JADX WARNING: type inference failed for: r6v5, types: [java.io.OutputStream] */
            /* JADX WARNING: type inference failed for: r3v9, types: [java.io.InputStream] */
            /* JADX WARNING: type inference failed for: r6v6 */
            /* JADX WARNING: type inference failed for: r3v11 */
            /* JADX WARNING: type inference failed for: r6v7 */
            /* JADX WARNING: type inference failed for: r6v8 */
            /* JADX WARNING: type inference failed for: r3v14 */
            /* JADX WARNING: type inference failed for: r6v9 */
            /* JADX WARNING: type inference failed for: r6v10 */
            /* JADX WARNING: type inference failed for: r6v11 */
            /* JADX WARNING: type inference failed for: r6v12 */
            /* JADX WARNING: type inference failed for: r6v14, types: [java.io.OutputStream] */
            /* JADX WARNING: type inference failed for: r0v29, types: [java.io.InputStream] */
            /* JADX WARNING: type inference failed for: r3v34, types: [java.io.InputStream] */
            /* JADX WARNING: type inference failed for: r6v15 */
            /* JADX WARNING: type inference failed for: r6v16 */
            /* JADX WARNING: type inference failed for: r6v17 */
            /* JADX WARNING: type inference failed for: r6v21, types: [java.io.OutputStream, java.io.FileOutputStream] */
            /* JADX WARNING: type inference failed for: r0v55 */
            /* JADX WARNING: type inference failed for: r6v22 */
            /* JADX WARNING: type inference failed for: r0v97 */
            /* JADX WARNING: type inference failed for: r6v23 */
            /* JADX WARNING: type inference failed for: r3v42 */
            /* JADX WARNING: type inference failed for: r6v24 */
            /* JADX WARNING: type inference failed for: r6v25 */
            /* JADX WARNING: type inference failed for: r3v43 */
            /* JADX WARNING: type inference failed for: r3v44 */
            /* JADX WARNING: type inference failed for: r6v26 */
            /* JADX WARNING: type inference failed for: r6v27 */
            /* JADX WARNING: type inference failed for: r6v28 */
            /* JADX WARNING: type inference failed for: r3v45 */
            /* JADX WARNING: type inference failed for: r3v46 */
            /* JADX WARNING: type inference failed for: r6v29 */
            /* JADX WARNING: type inference failed for: r6v30 */
            /* JADX WARNING: type inference failed for: r3v47 */
            /* JADX WARNING: type inference failed for: r3v48 */
            /* JADX WARNING: type inference failed for: r3v49 */
            /* JADX WARNING: type inference failed for: r3v50 */
            /* JADX WARNING: type inference failed for: r3v51 */
            /* JADX WARNING: type inference failed for: r6v31 */
            /* JADX WARNING: type inference failed for: r6v32 */
            /* JADX WARNING: type inference failed for: r6v33 */
            /* JADX WARNING: Code restructure failed: missing block: B:162:0x02df, code lost:
                r0 = move-exception;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:165:?, code lost:
                com.mintegral.msdk.base.utils.g.c("CampaignDownLoadTask", r0.getMessage(), r0);
             */
            /* JADX WARNING: Code restructure failed: missing block: B:217:0x0392, code lost:
                r0 = th;
                r3 = r3;
                r6 = r6;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:218:0x0394, code lost:
                r0 = e;
                r3 = r3;
                r6 = r6;
             */
            /* JADX WARNING: Failed to process nested try/catch */
            /* JADX WARNING: Multi-variable type inference failed. Error: jadx.core.utils.exceptions.JadxRuntimeException: No candidate types for var: r6v1
  assigns: []
  uses: []
  mth insns count: 499
            	at jadx.core.dex.visitors.typeinference.TypeSearch.fillTypeCandidates(TypeSearch.java:237)
            	at java.util.ArrayList.forEach(Unknown Source)
            	at jadx.core.dex.visitors.typeinference.TypeSearch.run(TypeSearch.java:53)
            	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.runMultiVariableSearch(TypeInferenceVisitor.java:99)
            	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.visit(TypeInferenceVisitor.java:92)
            	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:27)
            	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$1(DepthTraversal.java:14)
            	at java.util.ArrayList.forEach(Unknown Source)
            	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
            	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$0(DepthTraversal.java:13)
            	at java.util.ArrayList.forEach(Unknown Source)
            	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:13)
            	at jadx.core.ProcessClass.process(ProcessClass.java:30)
            	at jadx.core.ProcessClass.lambda$processDependencies$0(ProcessClass.java:49)
            	at java.util.ArrayList.forEach(Unknown Source)
            	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:49)
            	at jadx.core.ProcessClass.process(ProcessClass.java:35)
            	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
            	at jadx.api.JavaClass.decompile(JavaClass.java:62)
            	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
             */
            /* JADX WARNING: Removed duplicated region for block: B:170:0x02f9  */
            /* JADX WARNING: Removed duplicated region for block: B:172:0x0300 A[SYNTHETIC, Splitter:B:172:0x0300] */
            /* JADX WARNING: Removed duplicated region for block: B:177:0x0310 A[SYNTHETIC, Splitter:B:177:0x0310] */
            /* JADX WARNING: Removed duplicated region for block: B:182:0x0320 A[SYNTHETIC, Splitter:B:182:0x0320] */
            /* JADX WARNING: Removed duplicated region for block: B:218:0x0394 A[ExcHandler: SocketTimeoutException (e java.net.SocketTimeoutException), Splitter:B:59:0x0149] */
            /* JADX WARNING: Removed duplicated region for block: B:248:0x03e4  */
            /* JADX WARNING: Removed duplicated region for block: B:250:0x03eb A[SYNTHETIC, Splitter:B:250:0x03eb] */
            /* JADX WARNING: Removed duplicated region for block: B:255:0x03fb A[SYNTHETIC, Splitter:B:255:0x03fb] */
            /* JADX WARNING: Removed duplicated region for block: B:260:0x040b A[SYNTHETIC, Splitter:B:260:0x040b] */
            /* JADX WARNING: Removed duplicated region for block: B:274:0x0433  */
            /* JADX WARNING: Removed duplicated region for block: B:276:0x043a A[SYNTHETIC, Splitter:B:276:0x043a] */
            /* JADX WARNING: Removed duplicated region for block: B:281:0x044a A[SYNTHETIC, Splitter:B:281:0x044a] */
            /* JADX WARNING: Removed duplicated region for block: B:286:0x045a A[SYNTHETIC, Splitter:B:286:0x045a] */
            /* JADX WARNING: Removed duplicated region for block: B:297:0x0476  */
            /* JADX WARNING: Removed duplicated region for block: B:299:0x047d A[SYNTHETIC, Splitter:B:299:0x047d] */
            /* JADX WARNING: Removed duplicated region for block: B:304:0x048d A[SYNTHETIC, Splitter:B:304:0x048d] */
            /* JADX WARNING: Removed duplicated region for block: B:309:0x049d A[SYNTHETIC, Splitter:B:309:0x049d] */
            /* JADX WARNING: Unknown top exception splitter block from list: {B:244:0x03d0=Splitter:B:244:0x03d0, B:270:0x0422=Splitter:B:270:0x0422} */
            /* JADX WARNING: Unknown top exception splitter block from list: {B:290:0x0468=Splitter:B:290:0x0468, B:264:0x0419=Splitter:B:264:0x0419, B:186:0x032e=Splitter:B:186:0x032e} */
            /* JADX WARNING: Unknown variable types count: 18 */
            public final void run() {
                ? r6;
                HttpURLConnection httpURLConnection;
                ? r3;
                ? r62;
                ? r32;
                ? r63;
                ? r33;
                SocketTimeoutException e;
                ? r64;
                ? r65;
                ? r66;
                ? r34;
                Throwable th;
                ? r67;
                ? r68;
                ? r69;
                ? r610;
                ? r0;
                g.a("CampaignDownLoadTask", "=======开始下载");
                a.this.c(false);
                if (a.this.c == 1) {
                    a.this.c = 2;
                }
                ? r02 = 0;
                try {
                    if (!(a.this.c == 0 || a.this.c == 5 || a.this.c == 1)) {
                        a.this.o();
                        g.a("CampaignDownLoadTask", "=======删除资源");
                    }
                    if (a.this.c != 1) {
                        if (a.this.c != 5) {
                            if (TextUtils.isEmpty(str)) {
                                if (a.this.c == 1) {
                                    a.this.c = 2;
                                }
                                return;
                            }
                            URL url = new URL(str);
                            httpURLConnection = (HttpURLConnection) url.openConnection();
                            try {
                                httpURLConnection.setConnectTimeout(15000);
                                httpURLConnection.setReadTimeout(600000);
                                httpURLConnection.setRequestMethod(HttpRequest.METHOD_GET);
                                httpURLConnection.setInstanceFollowRedirects(true);
                                StringBuilder sb = new StringBuilder("=========CampaignDownLoadTask,run url:");
                                sb.append(url);
                                g.d("CampaignDownLoadTask", sb.toString());
                                long b2 = com.mintegral.msdk.base.utils.i.b();
                                int responseCode = httpURLConnection.getResponseCode();
                                if (responseCode != 200) {
                                    if (responseCode != 206) {
                                        a.c(a.this, "http response failed");
                                        r610 = 0;
                                        r0 = r02;
                                        if (a.this.c == 1) {
                                            a.this.c = 2;
                                        }
                                        if (httpURLConnection != null) {
                                            try {
                                                httpURLConnection.disconnect();
                                            } catch (Throwable th2) {
                                                g.c("CampaignDownLoadTask", th2.getMessage(), th2);
                                            }
                                        }
                                        if (r0 != 0) {
                                            try {
                                                r0.close();
                                            } catch (Throwable th3) {
                                                g.c("CampaignDownLoadTask", th3.getMessage(), th3);
                                            }
                                        }
                                        if (r610 != 0) {
                                            try {
                                                r610.flush();
                                            } catch (Throwable th4) {
                                                g.c("CampaignDownLoadTask", th4.getMessage(), th4);
                                            }
                                            try {
                                                r610.close();
                                                return;
                                            } catch (Throwable th5) {
                                                g.c("CampaignDownLoadTask", th5.getMessage(), th5);
                                                return;
                                            }
                                        }
                                    }
                                }
                                ? inputStream = httpURLConnection.getInputStream();
                                try {
                                    a.this.o = httpURLConnection.getContentLength();
                                    StringBuilder sb2 = new StringBuilder("=========正在下载中,空间:");
                                    sb2.append(b2);
                                    sb2.append(",contentLength:");
                                    sb2.append(a.this.o);
                                    g.d("CampaignDownLoadTask", sb2.toString());
                                    if (b2 == 0 || (b2 != 0 && b2 > ((long) a.this.o))) {
                                        a.this.r = true;
                                        File file = new File(a.this.q, a.this.E);
                                        a.this.c = 1;
                                        ? fileOutputStream = new FileOutputStream(file);
                                        try {
                                            if (a.this.C == null) {
                                                a.this.C = v.a((h) i.a(com.mintegral.msdk.base.controller.a.d().h()));
                                            }
                                            a.this.C.a(a.this.k, (long) a.this.o);
                                            byte[] bArr = new byte[4096];
                                            g.d("CampaignDownLoadTask", "=========开始下载，while循环读流");
                                            int i = 0;
                                            do {
                                                int read = inputStream.read(bArr);
                                                if (read != -1) {
                                                    if (file.exists()) {
                                                        fileOutputStream.write(bArr, 0, read);
                                                        i += read;
                                                        a.a(a.this, (long) i, a.this.c);
                                                        if (a.this.c == 2) {
                                                            break;
                                                        }
                                                    } else {
                                                        if (a.this.c == 1) {
                                                            a.this.c = 2;
                                                        }
                                                        if (httpURLConnection != null) {
                                                            try {
                                                                httpURLConnection.disconnect();
                                                            } catch (Throwable th6) {
                                                                g.c("CampaignDownLoadTask", th6.getMessage(), th6);
                                                            }
                                                        }
                                                        if (inputStream != 0) {
                                                            try {
                                                                inputStream.close();
                                                            } catch (Throwable th7) {
                                                                g.c("CampaignDownLoadTask", th7.getMessage(), th7);
                                                            }
                                                        }
                                                        try {
                                                            fileOutputStream.flush();
                                                        } catch (Throwable th8) {
                                                            g.c("CampaignDownLoadTask", th8.getMessage(), th8);
                                                        }
                                                        try {
                                                            fileOutputStream.close();
                                                            return;
                                                        } catch (Throwable th9) {
                                                            g.c("CampaignDownLoadTask", th9.getMessage(), th9);
                                                            return;
                                                        }
                                                    }
                                                } else {
                                                    String videoMD5Value = a.this.k.getVideoMD5Value();
                                                    if (!TextUtils.isEmpty(videoMD5Value)) {
                                                        if (videoMD5Value.equals(d.a(file))) {
                                                            a.a(a.this, a.this.s, true);
                                                            if (a.this.c == 1) {
                                                                a.this.c = 2;
                                                            }
                                                            if (httpURLConnection != null) {
                                                                try {
                                                                    httpURLConnection.disconnect();
                                                                } catch (Throwable th10) {
                                                                    g.c("CampaignDownLoadTask", th10.getMessage(), th10);
                                                                }
                                                            }
                                                            if (inputStream != 0) {
                                                                try {
                                                                    inputStream.close();
                                                                } catch (Throwable th11) {
                                                                    g.c("CampaignDownLoadTask", th11.getMessage(), th11);
                                                                }
                                                            }
                                                            try {
                                                                fileOutputStream.flush();
                                                            } catch (Throwable th12) {
                                                                g.c("CampaignDownLoadTask", th12.getMessage(), th12);
                                                            }
                                                            try {
                                                                fileOutputStream.close();
                                                                return;
                                                            } catch (Throwable th13) {
                                                                g.c("CampaignDownLoadTask", th13.getMessage(), th13);
                                                                return;
                                                            }
                                                        }
                                                        a.this.c("MD5 check failed");
                                                        r0 = inputStream;
                                                        r610 = fileOutputStream;
                                                        if (a.this.c == 1) {
                                                        }
                                                        if (httpURLConnection != null) {
                                                        }
                                                        if (r0 != 0) {
                                                        }
                                                        if (r610 != 0) {
                                                        }
                                                    }
                                                    a.a(a.this, a.this.s, false);
                                                    if (a.this.c == 1) {
                                                        a.this.c = 2;
                                                    }
                                                    if (httpURLConnection != null) {
                                                        try {
                                                            httpURLConnection.disconnect();
                                                        } catch (Throwable th14) {
                                                            g.c("CampaignDownLoadTask", th14.getMessage(), th14);
                                                        }
                                                    }
                                                    if (inputStream != 0) {
                                                        try {
                                                            inputStream.close();
                                                        } catch (Throwable th15) {
                                                            g.c("CampaignDownLoadTask", th15.getMessage(), th15);
                                                        }
                                                    }
                                                    try {
                                                        fileOutputStream.flush();
                                                    } catch (Throwable th16) {
                                                        g.c("CampaignDownLoadTask", th16.getMessage(), th16);
                                                    }
                                                    try {
                                                        fileOutputStream.close();
                                                        return;
                                                    } catch (Throwable th17) {
                                                        g.c("CampaignDownLoadTask", th17.getMessage(), th17);
                                                        return;
                                                    }
                                                }
                                            } while (a.this.c != 4);
                                        } catch (SocketTimeoutException e2) {
                                        } catch (Throwable th18) {
                                            g.c("CampaignDownLoadTask", th18.getMessage(), th18);
                                        }
                                        int i2 = a.this.c == 4 ? 3 : 2;
                                        Message obtain = Message.obtain();
                                        obtain.what = i2;
                                        a.this.G.sendMessage(obtain);
                                        if (a.this.c == 1) {
                                            a.this.c = 2;
                                        }
                                        if (httpURLConnection != null) {
                                            try {
                                                httpURLConnection.disconnect();
                                            } catch (Throwable th19) {
                                                g.c("CampaignDownLoadTask", th19.getMessage(), th19);
                                            }
                                        }
                                        if (inputStream != 0) {
                                            try {
                                                inputStream.close();
                                            } catch (Throwable th20) {
                                                g.c("CampaignDownLoadTask", th20.getMessage(), th20);
                                            }
                                        }
                                        try {
                                            fileOutputStream.flush();
                                        } catch (Throwable th21) {
                                            g.c("CampaignDownLoadTask", th21.getMessage(), th21);
                                        }
                                        try {
                                            fileOutputStream.close();
                                            return;
                                        } catch (Throwable th22) {
                                            g.c("CampaignDownLoadTask", th22.getMessage(), th22);
                                            return;
                                        }
                                    } else {
                                        if (a.this.c == 1) {
                                            a.this.c = 2;
                                        }
                                        if (httpURLConnection != null) {
                                            try {
                                                httpURLConnection.disconnect();
                                            } catch (Throwable th23) {
                                                g.c("CampaignDownLoadTask", th23.getMessage(), th23);
                                            }
                                        }
                                        if (inputStream != 0) {
                                            try {
                                                inputStream.close();
                                                return;
                                            } catch (Throwable th24) {
                                                g.c("CampaignDownLoadTask", th24.getMessage(), th24);
                                            }
                                        }
                                        return;
                                    }
                                } catch (SocketTimeoutException e3) {
                                    r63 = 0;
                                    e = e3;
                                    r33 = inputStream;
                                    r62 = r63;
                                    r32 = r33;
                                    a.c(a.this, e.getMessage());
                                    r62 = r63;
                                    r32 = r33;
                                    if (a.this.c == 1) {
                                        a.this.c = 2;
                                    }
                                    if (httpURLConnection != null) {
                                        try {
                                            httpURLConnection.disconnect();
                                        } catch (Throwable th25) {
                                            g.c("CampaignDownLoadTask", th25.getMessage(), th25);
                                        }
                                    }
                                    if (r33 != 0) {
                                        try {
                                            r33.close();
                                        } catch (Throwable th26) {
                                            g.c("CampaignDownLoadTask", th26.getMessage(), th26);
                                        }
                                    }
                                    if (r63 != 0) {
                                        try {
                                            r63.flush();
                                        } catch (Throwable th27) {
                                            g.c("CampaignDownLoadTask", th27.getMessage(), th27);
                                        }
                                        r63.close();
                                        return;
                                    }
                                } catch (Throwable th28) {
                                    r6 = 0;
                                    th = th28;
                                    r3 = inputStream;
                                    if (a.this.c == 1) {
                                        a.this.c = 2;
                                    }
                                    if (httpURLConnection != null) {
                                        try {
                                            httpURLConnection.disconnect();
                                        } catch (Throwable th29) {
                                            g.c("CampaignDownLoadTask", th29.getMessage(), th29);
                                        }
                                    }
                                    if (r3 != 0) {
                                        try {
                                            r3.close();
                                        } catch (Throwable th30) {
                                            g.c("CampaignDownLoadTask", th30.getMessage(), th30);
                                        }
                                    }
                                    if (r6 != 0) {
                                        try {
                                            r6.flush();
                                        } catch (Throwable th31) {
                                            g.c("CampaignDownLoadTask", th31.getMessage(), th31);
                                        }
                                        try {
                                            r6.close();
                                        } catch (Throwable th32) {
                                            g.c("CampaignDownLoadTask", th32.getMessage(), th32);
                                        }
                                    }
                                    throw th;
                                }
                            } catch (SocketTimeoutException e4) {
                                e = e4;
                                r65 = 0;
                                e = e;
                                r33 = r64;
                                r63 = r64;
                                r62 = r63;
                                r32 = r33;
                                a.c(a.this, e.getMessage());
                                r62 = r63;
                                r32 = r33;
                                if (a.this.c == 1) {
                                }
                                if (httpURLConnection != null) {
                                }
                                if (r33 != 0) {
                                }
                                if (r63 != 0) {
                                }
                            } catch (Throwable th33) {
                                th = th33;
                                r69 = 0;
                                th = th;
                                r3 = r68;
                                r6 = r68;
                                if (a.this.c == 1) {
                                }
                                if (httpURLConnection != null) {
                                }
                                if (r3 != 0) {
                                }
                                if (r6 != 0) {
                                }
                                throw th;
                            }
                        }
                    }
                    g.a("CampaignDownLoadTask", "=======正在下载中或下载完成");
                    if (a.this.c == 1) {
                        a.this.c = 2;
                    }
                } catch (SocketTimeoutException e5) {
                    e = e5;
                    httpURLConnection = null;
                    r65 = 0;
                    e = e;
                    r33 = r64;
                    r63 = r64;
                    r62 = r63;
                    r32 = r33;
                    a.c(a.this, e.getMessage());
                    r62 = r63;
                    r32 = r33;
                    if (a.this.c == 1) {
                    }
                    if (httpURLConnection != null) {
                    }
                    if (r33 != 0) {
                    }
                    if (r63 != 0) {
                    }
                } catch (Throwable th34) {
                    th = th34;
                    httpURLConnection = null;
                    r69 = 0;
                    th = th;
                    r3 = r68;
                    r6 = r68;
                    if (a.this.c == 1) {
                    }
                    if (httpURLConnection != null) {
                    }
                    if (r3 != 0) {
                    }
                    if (r6 != 0) {
                    }
                    throw th;
                }
            }
        };
    }

    /* access modifiers changed from: private */
    public void c(String str) {
        if (this.y != null) {
            g.d("CampaignDownLoadTask", "video load retry fail");
            this.y.a(str, this.p);
        }
        if (this.z != null) {
            this.z.a(str, this.p);
        }
        Message obtain = Message.obtain();
        obtain.what = 3;
        this.G.sendMessage(obtain);
    }

    public final void f() {
        g.d("CampaignDownLoadTask", "start()");
        if (this.b != null) {
            this.f.execute(this.b);
            this.m = true;
            return;
        }
        b(this.p);
        this.f.execute(this.b);
        this.m = true;
    }

    public final boolean g() {
        return this.m;
    }

    public final void a(String str) {
        q();
        a(2, str);
        this.c = 4;
    }

    public final int h() {
        return this.c;
    }

    public final void i() {
        this.c = 0;
        if (this.C == null) {
            this.C = v.a((h) i.a(com.mintegral.msdk.base.controller.a.d().h()));
        }
        this.C.a(this.p, 0, 0);
    }

    public final boolean j() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.q);
        sb.append(File.separator);
        sb.append(this.E);
        String sb2 = sb.toString();
        File file = new File(sb2);
        boolean z2 = false;
        try {
            if (file.exists() && file.isFile() && file.canRead()) {
                this.u = sb2;
                z2 = true;
            }
        } catch (Throwable th) {
            if (MIntegralConstans.DEBUG) {
                th.printStackTrace();
            }
        }
        if (this.c == 5 && !z2) {
            p();
        }
        return z2;
    }

    public final CampaignEx k() {
        return this.k;
    }

    public final void a(CampaignEx campaignEx) {
        this.k = campaignEx;
    }

    /* access modifiers changed from: private */
    public void c(boolean z2) {
        o a2 = v.a((h) i.a(com.mintegral.msdk.base.controller.a.d().h())).a(this.p, this.k.getBidToken());
        this.s = a2.b();
        if (this.c != 2) {
            this.c = a2.d();
        }
        if (z2 && this.c == 1) {
            this.c = 2;
        }
        this.o = a2.c();
        if (a2.a() > 0) {
            this.x = a2.a();
        }
        if (this.c != 5 || this.v) {
            if (this.c != 0) {
                StringBuilder sb = new StringBuilder();
                sb.append(this.q);
                sb.append(File.separator);
                sb.append(this.E);
                this.u = sb.toString();
            }
            return;
        }
        StringBuilder sb2 = new StringBuilder();
        sb2.append(this.q);
        sb2.append(File.separator);
        sb2.append(this.E);
        if (new File(sb2.toString()).exists()) {
            StringBuilder sb3 = new StringBuilder();
            sb3.append(this.q);
            sb3.append(File.separator);
            sb3.append(this.E);
            this.u = sb3.toString();
            return;
        }
        p();
        g.b("CampaignDownLoadTask", "restore state==5 文件不存在");
    }

    public final void a(com.mintegral.msdk.videocommon.listener.a aVar) {
        this.y = aVar;
    }

    public final void b(com.mintegral.msdk.videocommon.listener.a aVar) {
        this.z = aVar;
    }

    public final void a(int i2) {
        this.A = i2;
        StringBuilder sb = new StringBuilder("mReadyRate:");
        sb.append(this.A);
        g.b("CampaignDownLoadTask", sb.toString());
    }

    public final void a(d dVar) {
        if (this.d != null) {
            this.d.add(dVar);
        }
    }

    public final void b(d dVar) {
        this.e = dVar;
    }

    public final void l() {
        if (this.d != null) {
            this.d = null;
        }
    }

    public final long m() {
        return this.s;
    }

    public final d n() {
        return this.F;
    }

    private void p() {
        if (this.C == null) {
            this.C = v.a((h) i.a(com.mintegral.msdk.base.controller.a.d().h()));
        }
        try {
            this.C.a(this.p);
            File file = new File(this.u);
            if (file.exists() && file.isFile()) {
                file.delete();
            }
        } catch (Throwable unused) {
            g.d("CampaignDownLoadTask", "del DB or file failed");
        } finally {
            this.c = 0;
        }
    }

    public final void o() {
        try {
            p();
            if (this.k == null || this.k.getPlayable_ads_without_video() != 2) {
                com.mintegral.msdk.videocommon.a.a a2 = com.mintegral.msdk.videocommon.a.a.a();
                if (a2 != null) {
                    a2.b(this.k);
                }
                this.c = 0;
            }
        } catch (Exception unused) {
            g.d("CampaignDownLoadTask", "del file is failed");
        } finally {
            this.c = 0;
        }
    }

    private void q() {
        try {
            if (this.g == null || this.h == null) {
                this.g = Class.forName("com.mintegral.msdk.reward.b.a");
                this.h = this.g.newInstance();
                this.g.getMethod("insertExcludeId", new Class[]{String.class, CampaignEx.class}).invoke(this.h, new Object[]{this.l, this.k});
            }
            if (this.i == null || this.j == null) {
                this.i = Class.forName("com.mintegral.msdk.mtgnative.c.b");
                this.j = this.i.newInstance();
                this.i.getMethod("insertExcludeId", new Class[]{String.class, CampaignEx.class}).invoke(this.j, new Object[]{this.l, this.k});
            }
        } catch (Exception unused) {
            g.d("CampaignDownLoadTask", "");
        }
    }

    /* access modifiers changed from: private */
    public void a(int i2, String str) {
        w a2 = w.a((h) i.a(com.mintegral.msdk.base.controller.a.d().h()));
        long j2 = 0;
        if (this.x != 0) {
            j2 = System.currentTimeMillis() - this.x;
        }
        p pVar = new p(this.n, this.k, i2, Long.toString(j2), this.o, this.H);
        pVar.m(this.k.getId());
        pVar.e(this.k.getVideoUrlEncode());
        pVar.o(str);
        pVar.k(this.k.getRequestIdNotice());
        pVar.l(this.l);
        a2.a(pVar);
    }

    public final void b(int i2) {
        this.H = i2;
    }

    static /* synthetic */ void a(a aVar, long j2, int i2) {
        aVar.s = j2;
        if (100 * j2 >= ((long) (aVar.A * aVar.o)) && !aVar.B && i2 != 4) {
            if (aVar.A != 100 || i2 == 5) {
                aVar.B = true;
                StringBuilder sb = new StringBuilder("video load sucessed state:");
                sb.append(i2);
                sb.append("  mReadyRate:");
                sb.append(aVar.A);
                g.d("CampaignDownLoadTask", sb.toString());
                if (aVar.j()) {
                    if (aVar.y != null) {
                        aVar.y.a(aVar.p);
                    }
                    if (aVar.z != null) {
                        aVar.z.a(aVar.p);
                    }
                } else {
                    if (aVar.y != null) {
                        aVar.y.a("file is not effective", aVar.p);
                    }
                    if (aVar.z != null) {
                        aVar.z.a("file is not effective", aVar.p);
                    }
                }
            } else {
                return;
            }
        }
        if (aVar.m) {
            if (aVar.d != null) {
                Iterator it = aVar.d.iterator();
                while (it.hasNext()) {
                    d dVar = (d) it.next();
                    if (dVar != null) {
                        dVar.a(j2, i2);
                    }
                }
            }
            if (aVar.e != null && (aVar.c == 5 || aVar.c == 4 || aVar.c == 2)) {
                aVar.e.a(j2, i2);
                aVar.e = null;
            }
        }
        if (!aVar.f3049a && j2 > 0) {
            aVar.f3049a = true;
            if (aVar.C == null) {
                aVar.C = v.a((h) i.a(com.mintegral.msdk.base.controller.a.d().h()));
            }
            aVar.C.a(aVar.p, j2, aVar.c);
        }
    }

    static /* synthetic */ void a(a aVar, long j2, boolean z2) {
        if (j2 == ((long) aVar.o) || z2) {
            g.d("CampaignDownLoadTask", "=========下载完成");
            Message obtain = Message.obtain();
            obtain.what = 4;
            aVar.G.sendMessage(obtain);
            aVar.C.a(aVar.p, j2, 5);
            return;
        }
        aVar.c("File downloaded is smaller than the file");
    }

    static /* synthetic */ void c(a aVar, String str) {
        aVar.t++;
        StringBuilder sb = new StringBuilder("retryReq");
        sb.append(aVar.t);
        g.a("CampaignDownLoadTask", sb.toString());
        try {
            if (aVar.n != null) {
                Object systemService = aVar.n.getSystemService("connectivity");
                ConnectivityManager connectivityManager = null;
                if (systemService instanceof ConnectivityManager) {
                    connectivityManager = (ConnectivityManager) systemService;
                }
                if (!(connectivityManager == null || connectivityManager.getActiveNetworkInfo() == null || connectivityManager.getActiveNetworkInfo().isAvailable())) {
                    return;
                }
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
        if (aVar.t <= 1) {
            aVar.p();
            aVar.G.sendEmptyMessageDelayed(5, 2000);
            return;
        }
        aVar.q();
        aVar.a(3, str);
        aVar.G.sendEmptyMessage(3);
        if (aVar.y != null) {
            g.d("CampaignDownLoadTask", "video load retry fail");
            aVar.y.a(str, aVar.p);
        }
        if (aVar.z != null) {
            aVar.z.a(str, aVar.p);
        }
    }
}
