package com.mintegral.msdk.videocommon.listener;

public interface InterVideoOutListener {
    void onAdClose(boolean z, String str, float f);

    void onAdShow();

    void onEndcardShow(String str);

    void onLoadSuccess(String str);

    void onShowFail(String str);

    void onVideoAdClicked(String str);

    void onVideoComplete(String str);

    void onVideoLoadFail(String str);

    void onVideoLoadSuccess(String str);
}
