package com.mintegral.msdk.videocommon.b;

import org.json.JSONObject;

/* compiled from: AdParams */
public final class a {

    /* renamed from: a reason: collision with root package name */
    private String f3038a;
    private String b;

    private a(String str, String str2) {
        this.f3038a = str;
        this.b = str2;
    }

    public static a a(JSONObject jSONObject) {
        if (jSONObject != null) {
            try {
                return new a(jSONObject.optString("appId"), jSONObject.optString("placementId"));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}
