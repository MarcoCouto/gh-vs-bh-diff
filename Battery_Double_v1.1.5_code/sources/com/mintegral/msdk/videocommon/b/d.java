package com.mintegral.msdk.videocommon.b;

import android.text.TextUtils;
import com.mintegral.msdk.videocommon.e.a;
import com.mintegral.msdk.videocommon.e.b;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;

/* compiled from: Reward */
public final class d {

    /* renamed from: a reason: collision with root package name */
    private String f3041a;
    private int b;

    public d(String str, int i) {
        this.f3041a = str;
        this.b = i;
    }

    public final String a() {
        return this.f3041a;
    }

    public final int b() {
        return this.b;
    }

    private static d c() {
        return new d("Virtual Item", 1);
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x0029 A[Catch:{ Exception -> 0x002e }] */
    public static d a(String str) {
        d dVar;
        d dVar2 = null;
        try {
            b.a();
            a b2 = b.b();
            if (TextUtils.isEmpty(str)) {
                dVar = c();
            } else {
                if (!(b2 == null || b2.m() == null)) {
                    dVar = (d) b2.m().get(str);
                }
                if (dVar2 == null) {
                    return c();
                }
                return dVar2;
            }
            dVar2 = dVar;
            if (dVar2 == null) {
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dVar2;
    }

    public static Map<String, d> a(JSONArray jSONArray) {
        if (jSONArray != null && jSONArray.length() > 0) {
            try {
                HashMap hashMap = new HashMap();
                for (int i = 0; i < jSONArray.length(); i++) {
                    JSONObject optJSONObject = jSONArray.optJSONObject(i);
                    hashMap.put(optJSONObject.optString("id"), new d(optJSONObject.optString("name"), optJSONObject.optInt("amount")));
                }
                return hashMap;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder("Reward{name='");
        sb.append(this.f3041a);
        sb.append('\'');
        sb.append(", amount=");
        sb.append(this.b);
        sb.append('}');
        return sb.toString();
    }
}
