package com.mintegral.msdk.videocommon.b;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/* compiled from: Cbprule */
public final class c {

    /* renamed from: a reason: collision with root package name */
    private int f3040a;
    private List<Integer> b;

    private c(int i, List<Integer> list) {
        this.f3040a = i;
        this.b = list;
    }

    public final int a() {
        return this.f3040a;
    }

    public final List<Integer> b() {
        return this.b;
    }

    public static c a(JSONObject jSONObject) {
        c cVar;
        c cVar2 = new c(1, Arrays.asList(new Integer[]{Integer.valueOf(1), Integer.valueOf(2), Integer.valueOf(3), Integer.valueOf(4)}));
        if (jSONObject == null) {
            return cVar2;
        }
        try {
            int optInt = jSONObject.optInt("type");
            ArrayList arrayList = new ArrayList();
            JSONArray optJSONArray = jSONObject.optJSONArray("value");
            if (optJSONArray != null && optJSONArray.length() > 0) {
                for (int i = 0; i < optJSONArray.length(); i++) {
                    arrayList.add(Integer.valueOf(optJSONArray.optInt(i)));
                }
            }
            cVar = new c(optInt, arrayList);
        } catch (Exception e) {
            e.printStackTrace();
            cVar = cVar2;
        }
        return cVar;
    }
}
