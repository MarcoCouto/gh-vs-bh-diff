package com.mintegral.msdk.videocommon.e;

import android.text.TextUtils;
import com.mintegral.msdk.base.b.i;
import com.mintegral.msdk.base.controller.a;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.videocommon.b.b;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import org.json.JSONArray;
import org.json.JSONObject;

/* compiled from: RewardUnitSetting */
public final class c {
    private static i C;
    private int A;
    private int B = 20;
    private int D;

    /* renamed from: a reason: collision with root package name */
    private String f3073a;
    private List<b> b;
    private long c;
    private int d = -1;
    private int e = 0;
    private int f = 0;
    private int g = 1;
    private int h = 1;
    private int i = 1;
    private int j = 1;
    private double k = 1.0d;
    private int l = 2;
    private int m = 5;
    private int n = 2;
    private int o = 3;
    private int p = 80;
    private int q = 100;
    private com.mintegral.msdk.videocommon.b.c r;
    private double s = 1.0d;
    private int t = -1;
    private ArrayList<Integer> u;
    private int v = 0;
    private int w = 0;
    private int x = -1;
    private int y = -1;
    private int z = -1;

    public final int a() {
        return this.v;
    }

    public final void b() {
        this.v = 0;
    }

    public final int c() {
        return this.w;
    }

    public final void d() {
        this.w = 0;
    }

    public final int e() {
        return this.x;
    }

    public final void f() {
        this.x = -1;
    }

    public final int g() {
        return this.y;
    }

    public final void h() {
        this.y = 1;
    }

    public final int i() {
        return this.z;
    }

    public final void j() {
        this.z = -1;
    }

    public final int k() {
        return this.d;
    }

    public final int l() {
        return this.f;
    }

    public final int m() {
        return this.B;
    }

    public final int n() {
        return this.A;
    }

    public final void o() {
        this.A = 1;
    }

    public final int p() {
        return this.D;
    }

    public final void q() {
        this.D = 1;
    }

    public final void r() {
        this.o = 3;
    }

    public final void s() {
        this.p = 80;
    }

    public final int t() {
        return this.q;
    }

    public final void u() {
        this.q = 100;
    }

    public final int v() {
        return this.h;
    }

    public final void w() {
        this.h = 1;
    }

    public final int x() {
        return this.i;
    }

    public final void y() {
        this.i = 1;
    }

    public final int z() {
        return this.j;
    }

    public final void A() {
        this.j = 1;
    }

    public final void B() {
        this.k = 1.0d;
    }

    public final double C() {
        return this.k;
    }

    public final void D() {
        this.l = 2;
    }

    public final int E() {
        return this.m;
    }

    public final void F() {
        this.m = 1;
    }

    public final int G() {
        return this.n;
    }

    public final void H() {
        this.n = 1;
    }

    public final void I() {
        this.g = 1;
    }

    public final long J() {
        return this.c;
    }

    public final List<b> K() {
        return this.b;
    }

    public final void a(List<b> list) {
        this.b = list;
    }

    public final com.mintegral.msdk.videocommon.b.c L() {
        return this.r;
    }

    public final double M() {
        return this.s;
    }

    public final int a(int i2) {
        if (this.t == -1) {
            if (i2 == 94) {
                return 2;
            }
            if (i2 == 287) {
                return 3;
            }
        }
        return this.t;
    }

    public final boolean b(int i2) {
        if (this.u == null || this.u.size() <= 0) {
            return false;
        }
        return this.u.contains(Integer.valueOf(i2));
    }

    public final Queue<Integer> N() {
        Queue<Integer> queue;
        Exception e2;
        try {
            if (this.b == null || this.b.size() <= 0) {
                return null;
            }
            queue = new LinkedList<>();
            int i2 = 0;
            while (i2 < this.b.size()) {
                try {
                    queue.add(Integer.valueOf(((b) this.b.get(i2)).a()));
                    i2++;
                } catch (Exception e3) {
                    e2 = e3;
                    e2.printStackTrace();
                    return queue;
                }
            }
            return queue;
        } catch (Exception e4) {
            Exception exc = e4;
            queue = null;
            e2 = exc;
            e2.printStackTrace();
            return queue;
        }
    }

    public final Queue<Integer> O() {
        Queue<Integer> queue;
        Exception e2;
        try {
            if (this.b == null || this.b.size() <= 0) {
                return null;
            }
            queue = new LinkedList<>();
            int i2 = 0;
            while (i2 < this.b.size()) {
                try {
                    queue.add(Integer.valueOf(((b) this.b.get(i2)).b()));
                    i2++;
                } catch (Exception e3) {
                    e2 = e3;
                    e2.printStackTrace();
                    return queue;
                }
            }
            return queue;
        } catch (Exception e4) {
            Exception exc = e4;
            queue = null;
            e2 = exc;
            e2.printStackTrace();
            return queue;
        }
    }

    public static c a(String str) {
        if (C == null) {
            C = i.a(a.d().h());
        }
        c cVar = null;
        if (!TextUtils.isEmpty(str)) {
            try {
                JSONObject jSONObject = new JSONObject(str);
                JSONArray optJSONArray = jSONObject.optJSONArray("unitSetting");
                if (optJSONArray != null) {
                    int i2 = 0;
                    JSONObject optJSONObject = optJSONArray.optJSONObject(0);
                    if (optJSONObject != null) {
                        String optString = optJSONObject.optString("unitId");
                        if (!TextUtils.isEmpty(optString)) {
                            c cVar2 = new c();
                            try {
                                List<b> a2 = b.a(optJSONObject.optJSONArray("adSourceList"));
                                cVar2.f3073a = optString;
                                cVar2.b = a2;
                                cVar2.g = optJSONObject.optInt("callbackType");
                                int optInt = optJSONObject.optInt("aqn", 1);
                                if (optInt <= 0) {
                                    optInt = 1;
                                }
                                cVar2.h = optInt;
                                int optInt2 = optJSONObject.optInt("acn", 1);
                                if (optInt2 < 0) {
                                    optInt2 = 1;
                                }
                                cVar2.i = optInt2;
                                cVar2.j = optJSONObject.optInt("vcn", 5);
                                cVar2.k = optJSONObject.optDouble("cbp", 1.0d);
                                cVar2.l = optJSONObject.optInt(CampaignEx.JSON_KEY_TTC_TYPE, 2);
                                cVar2.m = optJSONObject.optInt("offset", 5);
                                cVar2.n = optJSONObject.optInt("dlnet", 2);
                                cVar2.D = optJSONObject.optInt("endscreen_type", 1);
                                cVar2.o = optJSONObject.optInt("tv_start", 3);
                                cVar2.p = optJSONObject.optInt("tv_end", 80);
                                cVar2.q = optJSONObject.optInt("ready_rate", 100);
                                cVar2.c = jSONObject.optLong("current_time");
                                cVar2.v = optJSONObject.optInt("orientation", 0);
                                cVar2.w = optJSONObject.optInt("daily_play_cap", 0);
                                cVar2.x = optJSONObject.optInt("video_skip_time", -1);
                                cVar2.y = optJSONObject.optInt("video_skip_result", 1);
                                cVar2.z = optJSONObject.optInt("video_interactive_type", -1);
                                cVar2.A = optJSONObject.optInt("close_button_delay", 1);
                                cVar2.d = optJSONObject.optInt("playclosebtn_tm", -1);
                                cVar2.e = optJSONObject.optInt("play_ctdown", 0);
                                cVar2.f = optJSONObject.optInt("close_alert", 0);
                                cVar2.B = optJSONObject.optInt("rdrct", 20);
                                cVar2.t = optJSONObject.optInt("rfpv", -1);
                                cVar2.s = optJSONObject.optDouble("vdcmp", 1.0d);
                                cVar2.r = com.mintegral.msdk.videocommon.b.c.a(optJSONObject.optJSONObject("cbprule"));
                                JSONArray optJSONArray2 = optJSONObject.optJSONArray("atl_type");
                                ArrayList<Integer> arrayList = new ArrayList<>();
                                if (optJSONArray2 != null) {
                                    while (i2 < optJSONArray2.length()) {
                                        try {
                                            arrayList.add(Integer.valueOf(optJSONArray2.getInt(i2)));
                                            i2++;
                                        } catch (Exception e2) {
                                            e2.printStackTrace();
                                        }
                                    }
                                } else {
                                    arrayList.add(Integer.valueOf(1));
                                    arrayList.add(Integer.valueOf(2));
                                    arrayList.add(Integer.valueOf(3));
                                    arrayList.add(Integer.valueOf(4));
                                }
                                cVar2.u = arrayList;
                                cVar = cVar2;
                            } catch (Exception e3) {
                                e = e3;
                                cVar = cVar2;
                                e.printStackTrace();
                                return cVar;
                            }
                        }
                    }
                }
                return cVar;
            } catch (Exception e4) {
                e = e4;
                e.printStackTrace();
                return cVar;
            }
        }
        return cVar;
    }

    public final JSONObject P() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("unitId", this.f3073a);
            jSONObject.put("callbackType", this.g);
            if (this.b != null && this.b.size() > 0) {
                JSONArray jSONArray = new JSONArray();
                for (b bVar : this.b) {
                    JSONObject jSONObject2 = new JSONObject();
                    jSONObject2.put("id", bVar.a());
                    jSONObject2.put("timeout", bVar.b());
                    jSONArray.put(jSONObject2);
                }
                jSONObject.put("adSourceList", jSONArray);
            }
            jSONObject.put("aqn", this.h);
            jSONObject.put("acn", this.i);
            jSONObject.put("vcn", this.j);
            jSONObject.put("cbp", this.k);
            jSONObject.put(CampaignEx.JSON_KEY_TTC_TYPE, this.l);
            jSONObject.put("offset", this.m);
            jSONObject.put("dlnet", this.n);
            jSONObject.put("tv_start", this.o);
            jSONObject.put("tv_end", this.p);
            jSONObject.put("ready_rate", this.q);
            jSONObject.put("endscreen_type", this.D);
            jSONObject.put("daily_play_cap", this.w);
            jSONObject.put("video_skip_time", this.x);
            jSONObject.put("video_skip_result", this.y);
            jSONObject.put("video_interactive_type", this.z);
            jSONObject.put("orientation", this.v);
            jSONObject.put("close_button_delay", this.A);
            jSONObject.put("playclosebtn_tm", this.d);
            jSONObject.put("play_ctdown", this.e);
            jSONObject.put("close_alert", this.f);
            jSONObject.put("rfpv", this.t);
            jSONObject.put("vdcmp", this.s);
            if (this.r != null) {
                JSONObject jSONObject3 = new JSONObject();
                jSONObject3.put("type", this.r.a());
                JSONArray jSONArray2 = new JSONArray();
                for (Integer put : this.r.b()) {
                    jSONArray2.put(put);
                }
                jSONObject3.put("value", jSONArray2);
                jSONObject.put("cbprule", jSONObject3);
            }
            JSONArray jSONArray3 = new JSONArray();
            if (this.u != null) {
                if (this.u.size() > 0) {
                    Iterator it = this.u.iterator();
                    while (it.hasNext()) {
                        jSONArray3.put((Integer) it.next());
                    }
                }
                jSONObject.put("atl_type", jSONArray3);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return jSONObject;
    }
}
