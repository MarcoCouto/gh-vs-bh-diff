package com.mintegral.msdk.videocommon.e;

import android.text.TextUtils;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.mintegral.msdk.base.a.a.a;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.videocommon.b.d;
import com.mintegral.msdk.videocommon.c.c;
import io.fabric.sdk.android.services.events.EventsFilesManager;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;

/* compiled from: RewardSettingManager */
public class b {

    /* renamed from: a reason: collision with root package name */
    public static a f3072a = null;
    private static Map<String, c> b = new HashMap();
    private static b c;

    private b() {
    }

    public static b a() {
        if (c == null) {
            synchronized (b.class) {
                if (c == null) {
                    c = new b();
                }
            }
        }
        return c;
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x006b A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x006c  */
    public static a b() {
        boolean z;
        if (f3072a != null) {
            return f3072a;
        }
        a a2 = a.a();
        StringBuilder sb = new StringBuilder("reward_");
        sb.append(com.mintegral.msdk.base.controller.a.d().j());
        String a3 = a2.a(sb.toString());
        if (!TextUtils.isEmpty(a3)) {
            a a4 = a.a(a3);
            if (a4 != null) {
                long a5 = a4.a();
                long currentTimeMillis = System.currentTimeMillis();
                long k = a4.k() + a5;
                if (k > currentTimeMillis) {
                    StringBuilder sb2 = new StringBuilder("app setting nexttime is not ready  [settingNextRequestTime= ");
                    sb2.append(k);
                    sb2.append(" currentTime = ");
                    sb2.append(currentTimeMillis);
                    sb2.append(RequestParameters.RIGHT_BRACKETS);
                    g.b("RewardSettingManager", sb2.toString());
                    z = false;
                    if (z) {
                        return a4;
                    }
                    d(com.mintegral.msdk.base.controller.a.d().j(), com.mintegral.msdk.base.controller.a.d().k());
                    return c();
                }
            }
            g.b("RewardSettingManager", "app setting timeout or not exists");
            z = true;
            if (z) {
            }
        } else {
            d(com.mintegral.msdk.base.controller.a.d().j(), com.mintegral.msdk.base.controller.a.d().k());
            return c();
        }
    }

    private static void d(String str, String str2) {
        new com.mintegral.msdk.videocommon.c.a().a(com.mintegral.msdk.base.controller.a.d().h(), str, str2);
    }

    public static void a(String str, String str2, String str3, c cVar) {
        new com.mintegral.msdk.videocommon.c.a().a(com.mintegral.msdk.base.controller.a.d().h(), str, str2, str3, cVar);
    }

    public static c a(String str, String str2) {
        StringBuilder sb = new StringBuilder("reward_");
        sb.append(str);
        sb.append(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
        sb.append(str2);
        String sb2 = sb.toString();
        if (b.containsKey(sb2)) {
            return (c) b.get(sb2);
        }
        c a2 = c.a(a.a().a(sb2));
        if (a(a2)) {
            return null;
        }
        b.put(sb2, a2);
        return a2;
    }

    public static c b(String str, String str2) {
        StringBuilder sb = new StringBuilder("reward_");
        sb.append(str);
        sb.append(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
        sb.append(str2);
        String sb2 = sb.toString();
        if (b.containsKey(sb2)) {
            return (c) b.get(sb2);
        }
        c a2 = c.a(a.a().a(sb2));
        if (a(a2)) {
            return a2 == null ? d() : a2;
        }
        b.put(sb2, a2);
        return a2;
    }

    public static a c() {
        a aVar = new a();
        HashMap hashMap = new HashMap();
        hashMap.put("1", Integer.valueOf(1000));
        hashMap.put("9", Integer.valueOf(1000));
        hashMap.put("8", Integer.valueOf(1000));
        HashMap hashMap2 = new HashMap();
        hashMap2.put("1", new d("Virtual Item", 1));
        aVar.a((Map<String, Integer>) hashMap);
        aVar.b(hashMap2);
        aVar.b();
        aVar.d();
        aVar.f();
        aVar.h();
        aVar.j();
        return aVar;
    }

    private static boolean a(c cVar) {
        a b2 = b();
        if (!(b2 == null || cVar == null)) {
            long c2 = b2.c();
            long currentTimeMillis = System.currentTimeMillis();
            long J = cVar.J() + c2;
            if (J > currentTimeMillis) {
                StringBuilder sb = new StringBuilder("unit setting  nexttime is not ready  [settingNextRequestTime= ");
                sb.append(J);
                sb.append(" currentTime = ");
                sb.append(currentTimeMillis);
                sb.append(RequestParameters.RIGHT_BRACKETS);
                g.b("RewardSettingManager", sb.toString());
                return false;
            }
        }
        return true;
    }

    public static void a(String str, String str2, String str3) {
        StringBuilder sb = new StringBuilder("reward_");
        sb.append(str);
        sb.append(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
        sb.append(str2);
        String sb2 = sb.toString();
        a.a().a(sb2, str3);
        b.put(sb2, c.a(str3));
    }

    public static void c(String str, String str2) {
        StringBuilder sb = new StringBuilder("reward_");
        sb.append(str);
        a.a().a(sb.toString(), str2);
        f3072a = a.a(str2);
    }

    public static boolean a(String str) {
        try {
            if (!TextUtils.isEmpty(str)) {
                JSONArray optJSONArray = new JSONObject(str).optJSONArray("unitSetting");
                if (optJSONArray != null) {
                    String optString = optJSONArray.optJSONObject(0).optString("unitId");
                    if (optJSONArray != null && optJSONArray.length() > 0 && !TextUtils.isEmpty(optString)) {
                        return true;
                    }
                }
            }
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static c d() {
        c cVar = new c();
        try {
            ArrayList arrayList = new ArrayList();
            arrayList.add(new com.mintegral.msdk.videocommon.b.b(1, 15000, null));
            cVar.a((List<com.mintegral.msdk.videocommon.b.b>) arrayList);
            cVar.y();
            cVar.w();
            cVar.A();
            cVar.I();
            cVar.B();
            cVar.D();
            cVar.F();
            cVar.H();
            cVar.r();
            cVar.s();
            cVar.u();
            cVar.q();
            cVar.j();
            cVar.h();
            cVar.f();
            cVar.d();
            cVar.b();
            cVar.o();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return cVar;
    }
}
