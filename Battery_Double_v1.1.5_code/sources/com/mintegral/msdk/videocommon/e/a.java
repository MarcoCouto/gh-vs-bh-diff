package com.mintegral.msdk.videocommon.e;

import android.text.TextUtils;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.reward.player.MTGRewardVideoActivity;
import com.mintegral.msdk.videocommon.b.d;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.json.JSONObject;

/* compiled from: RewardSetting */
public class a {

    /* renamed from: a reason: collision with root package name */
    private Map<String, Integer> f3071a;
    private Map<String, d> b;
    private long c;
    private long d;
    private long e;
    private long f;
    private long g;
    private long h;

    public final long a() {
        return this.c * 1000;
    }

    public final void b() {
        this.c = 43200;
    }

    public final long c() {
        return this.d * 1000;
    }

    public final void d() {
        this.d = 5400;
    }

    public final long e() {
        return this.e * 1000;
    }

    public final void f() {
        this.e = 3600;
    }

    public final long g() {
        return this.f;
    }

    public final void h() {
        this.f = 3600;
    }

    public final long i() {
        return this.g;
    }

    public final void j() {
        this.g = 5;
    }

    public final long k() {
        return this.h;
    }

    public final Map<String, Integer> l() {
        if (this.f3071a == null) {
            this.f3071a = new HashMap();
            this.f3071a.put("1", Integer.valueOf(1000));
            this.f3071a.put("9", Integer.valueOf(1000));
            this.f3071a.put("8", Integer.valueOf(1000));
        }
        return this.f3071a;
    }

    public final void a(Map<String, Integer> map) {
        this.f3071a = map;
    }

    public final Map<String, d> m() {
        return this.b;
    }

    public final void b(Map<String, d> map) {
        this.b = map;
    }

    public static a a(String str) {
        a aVar;
        a aVar2 = null;
        if (!TextUtils.isEmpty(str)) {
            try {
                aVar = new a();
            } catch (Exception e2) {
                e = e2;
                e.printStackTrace();
                return aVar2;
            }
            try {
                JSONObject jSONObject = new JSONObject(str);
                JSONObject optJSONObject = jSONObject.optJSONObject("caplist");
                if (optJSONObject != null && optJSONObject.length() > 0) {
                    HashMap hashMap = new HashMap();
                    Iterator keys = optJSONObject.keys();
                    while (keys != null && keys.hasNext()) {
                        String str2 = (String) keys.next();
                        int intValue = Integer.valueOf(optJSONObject.optInt(str2, 1000)).intValue();
                        if (!TextUtils.isEmpty(str2)) {
                            if (TextUtils.isEmpty(str2) || intValue != 0) {
                                hashMap.put(str2, Integer.valueOf(intValue));
                            } else {
                                hashMap.put(str2, Integer.valueOf(1000));
                            }
                        }
                    }
                    aVar.f3071a = hashMap;
                }
                aVar.b = d.a(jSONObject.optJSONArray(MTGRewardVideoActivity.INTENT_REWARD));
                aVar.c = jSONObject.optLong("getpf", 43200);
                aVar.d = jSONObject.optLong("ruct", 5400);
                aVar.e = jSONObject.optLong(CampaignEx.JSON_KEY_PLCT, 3600);
                aVar.f = jSONObject.optLong("dlct", 3600);
                aVar.g = jSONObject.optLong("vcct", 5);
                aVar.h = jSONObject.optLong("current_time");
                return aVar;
            } catch (Exception e3) {
                e = e3;
                aVar2 = aVar;
                e.printStackTrace();
                return aVar2;
            }
        }
        return aVar2;
    }
}
