package com.mintegral.msdk.videocommon.c;

import android.content.Context;
import android.text.TextUtils;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.mintegral.msdk.base.common.net.a.c;
import com.mintegral.msdk.base.common.net.d;
import com.mintegral.msdk.base.common.net.l;
import com.mintegral.msdk.base.utils.CommonMD5;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.videocommon.e.b;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: RewardSettingController */
public class a {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public static final String f3042a = "com.mintegral.msdk.videocommon.c.a";

    public final void a(Context context, final String str, String str2) {
        b bVar = new b(context);
        l lVar = new l();
        lVar.a("app_id", str);
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(str2);
        lVar.a("sign", CommonMD5.getMD5(sb.toString()));
        bVar.a(com.mintegral.msdk.base.common.a.p, lVar, (d<?>) new c() {
            public final void a(JSONObject jSONObject) {
                if (jSONObject != null) {
                    try {
                        jSONObject.put("current_time", System.currentTimeMillis());
                        b.a();
                        b.c(str, jSONObject.toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            public final void a(String str) {
                g.d(a.f3042a, str);
            }
        });
    }

    public final void a(Context context, final String str, String str2, final String str3, final c cVar) {
        b bVar = new b(context);
        l lVar = new l();
        lVar.a("app_id", str);
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(str2);
        lVar.a("sign", CommonMD5.getMD5(sb.toString()));
        StringBuilder sb2 = new StringBuilder(RequestParameters.LEFT_BRACKETS);
        sb2.append(str3);
        sb2.append(RequestParameters.RIGHT_BRACKETS);
        lVar.a("unit_ids", sb2.toString());
        bVar.a(com.mintegral.msdk.base.common.a.p, lVar, (d<?>) new c() {
            public final void a(JSONObject jSONObject) {
                if (jSONObject != null) {
                    try {
                        if (b.a(jSONObject.toString())) {
                            jSONObject.put("current_time", System.currentTimeMillis());
                            b.a();
                            b.a(str, str3, jSONObject.toString());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            public final void a(String str) {
                TextUtils.isEmpty(str);
            }
        });
    }
}
