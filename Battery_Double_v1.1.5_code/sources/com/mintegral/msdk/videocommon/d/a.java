package com.mintegral.msdk.videocommon.d;

import android.content.Context;
import android.text.TextUtils;
import com.mintegral.msdk.base.b.h;
import com.mintegral.msdk.base.b.i;
import com.mintegral.msdk.base.b.w;
import com.mintegral.msdk.base.common.d.c;
import com.mintegral.msdk.base.common.d.c.b;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.entity.p;
import com.mintegral.msdk.base.utils.g;
import java.util.List;

/* compiled from: VideoReport */
public class a {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public static final String f3045a = "com.mintegral.msdk.videocommon.d.a";

    private static void a(Context context, String str, String str2) {
        if (context != null && !TextUtils.isEmpty(str) && !TextUtils.isEmpty(str2)) {
            try {
                com.mintegral.msdk.base.common.d.c.a aVar = new com.mintegral.msdk.base.common.d.c.a(context);
                aVar.c();
                aVar.b(com.mintegral.msdk.base.common.a.f, c.a(str, context, str2), new b() {
                    public final void a(String str) {
                    }

                    public final void b(String str) {
                        g.d(a.f3045a, str);
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
                g.d(f3045a, e.getMessage());
            }
        }
    }

    public static void a(Context context, CampaignEx campaignEx, int i, String str) {
        if (context != null && campaignEx != null && !TextUtils.isEmpty(str)) {
            int p = com.mintegral.msdk.base.utils.c.p(context);
            p pVar = new p("2000023", p, campaignEx.getVideoLength(), campaignEx.getNoticeUrl() != null ? campaignEx.getNoticeUrl() : campaignEx.getClickURL(), i, campaignEx.getBty(), com.mintegral.msdk.base.utils.c.a(context, p));
            a(context, p.e(pVar), str);
        }
    }

    public static void a(Context context, String str) {
        if (context != null) {
            try {
                w a2 = w.a((h) i.a(context));
                if (a2 != null && a2.c() > 30 && !TextUtils.isEmpty(str)) {
                    List a3 = a2.a("2000025");
                    List a4 = a2.a("2000024");
                    List a5 = a2.a("2000044");
                    String b = p.b(a4);
                    String c = p.c(a3);
                    String c2 = p.c(a5);
                    StringBuilder sb = new StringBuilder();
                    if (!TextUtils.isEmpty(b)) {
                        sb.append(b);
                    }
                    if (!TextUtils.isEmpty(c)) {
                        sb.append(c);
                    }
                    if (!TextUtils.isEmpty(c2)) {
                        sb.append(c2);
                    }
                    if (!TextUtils.isEmpty(sb.toString())) {
                        a(context, sb.toString(), str);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
