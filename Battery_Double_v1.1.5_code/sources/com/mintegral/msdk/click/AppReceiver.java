package com.mintegral.msdk.click;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import com.mintegral.msdk.base.b.g;
import com.mintegral.msdk.base.b.i;
import com.mintegral.msdk.base.entity.CampaignEx;

public class AppReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        if (TextUtils.equals(intent.getAction(), "android.intent.action.PACKAGE_ADDED")) {
            CampaignEx l = g.b(i.a(context)).l(intent.getData().getSchemeSpecificPart());
            if (l != null) {
                a.b(l, context, "install");
            }
        }
    }
}
