package com.mintegral.msdk.click;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;
import android.webkit.URLUtil;
import com.mintegral.msdk.base.common.e.a.C0049a;
import com.mintegral.msdk.base.common.e.a.b;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.click.CommonJumpLoader.JumpLoaderResult;
import com.mintegral.msdk.rover.d;
import java.net.URI;
import java.util.concurrent.Semaphore;

/* compiled from: WebViewSpiderLoader */
public final class g extends c implements b {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public boolean f2664a = false;
    /* access modifiers changed from: private */
    public long b = 0;
    /* access modifiers changed from: private */
    public d c;
    /* access modifiers changed from: private */
    public JumpLoaderResult d;
    /* access modifiers changed from: private */
    public boolean e = true;
    /* access modifiers changed from: private */
    public boolean f;
    private Context g;
    private com.mintegral.msdk.base.common.e.b h;
    /* access modifiers changed from: private */
    public com.mintegral.msdk.click.e.a i;
    private Handler j = new Handler(Looper.getMainLooper());
    /* access modifiers changed from: private */
    public boolean k;

    /* compiled from: WebViewSpiderLoader */
    private class a extends com.mintegral.msdk.base.common.e.a {
        private final Semaphore b = new Semaphore(0);
        private final Context c;
        private String d;
        private String e;
        private String f;
        private String g;
        private d k;
        private CampaignEx l;
        private boolean m;
        private boolean n;
        private a o = new a() {
            public final boolean a(String str) {
                boolean a2 = a.this.a(str);
                a.a(a.this, false, true, str, "");
                if (a2) {
                    a();
                }
                return a2;
            }

            public final boolean b(String str) {
                boolean a2 = a.this.a(str);
                a.a(a.this, false, true, str, "");
                if (a2) {
                    a.a(a.this, true, true, str, "");
                    a();
                }
                return a2;
            }

            public final boolean c(String str) {
                a.a(a.this, false, false, str, "");
                return false;
            }

            public final void a(String str, String str2) {
                a.this.a(str);
                g.this.d.setContent(str2);
                a.a(a.this, true, false, str, "timeout");
                a();
            }

            private void a() {
                synchronized (g.this) {
                    g.this.d.setSuccess(true);
                    a.this.b.release();
                }
            }

            public final void a(String str, String str2, String str3) {
                if (!TextUtils.isEmpty(str2)) {
                    g.this.d.setExceptionMsg(str2);
                }
                if (!TextUtils.isEmpty(str3)) {
                    g.this.d.setContent(str3);
                }
                a.this.a(str);
                a.a(a.this, true, false, str, str2);
                a();
            }
        };

        public final void b() {
        }

        public a(Context context, String str, String str2, String str3, String str4, d dVar, CampaignEx campaignEx, boolean z, boolean z2) {
            this.c = context;
            this.d = str;
            this.e = str2;
            this.f = str3;
            this.g = str4;
            this.k = dVar;
            this.l = campaignEx;
            this.m = z;
            this.n = z2;
        }

        private JumpLoaderResult a(String str, boolean z, boolean z2, CampaignEx campaignEx) {
            boolean z3;
            String str2 = str;
            com.mintegral.msdk.base.utils.g.d("302", "startJavaHTTPSpider");
            if (g.this.f) {
                String a2 = com.mintegral.msdk.b.a.a(this.c, str2);
                if (!TextUtils.isEmpty(a2)) {
                    StringBuilder sb = new StringBuilder();
                    sb.append(str2);
                    sb.append(a2);
                    str2 = sb.toString();
                }
            }
            if (this.l != null) {
                str2 = this.l.matchLoopback(str2);
            }
            JumpLoaderResult jumpLoaderResult = new JumpLoaderResult();
            e eVar = new e();
            String str3 = "";
            String str4 = "";
            try {
                if (!b(str2)) {
                    URI create = URI.create(str2);
                    String scheme = create.getScheme();
                    try {
                        str4 = create.getHost();
                    } catch (Exception unused) {
                    }
                    str3 = scheme;
                }
            } catch (Exception unused2) {
            }
            String str5 = str4;
            String str6 = str3;
            String str7 = str2;
            int i = 0;
            while (true) {
                if (i >= 10) {
                    break;
                } else if (!g.this.e) {
                    return null;
                } else {
                    long currentTimeMillis = System.currentTimeMillis();
                    g.this.i = eVar.a(str7, z, z2, campaignEx);
                    int currentTimeMillis2 = (int) (System.currentTimeMillis() - currentTimeMillis);
                    if (g.this.i == null) {
                        jumpLoaderResult.setUrl(str7);
                        jumpLoaderResult.setSuccess(false);
                        if (this.k != null) {
                            this.k.b(str7, com.mintegral.msdk.rover.a.c, currentTimeMillis2, 0, "", "headerFiled is null");
                        }
                    } else if (!TextUtils.isEmpty(g.this.i.h)) {
                        jumpLoaderResult.setUrl(str7);
                        jumpLoaderResult.setExceptionMsg(g.this.i.h);
                        jumpLoaderResult.setType(1);
                        jumpLoaderResult.setHeader(g.this.i.a());
                        jumpLoaderResult.setSuccess(false);
                        if (this.k != null) {
                            this.k.b(str7, com.mintegral.msdk.rover.a.c, currentTimeMillis2, g.this.i.f, g.this.i.toString(), g.this.i.h);
                        }
                    } else {
                        jumpLoaderResult.setSuccess(true);
                        if (this.k != null) {
                            z3 = true;
                            this.k.a(str7, com.mintegral.msdk.rover.a.c, currentTimeMillis2, g.this.i.f, g.this.i.toString(), g.this.i.h);
                        } else {
                            z3 = true;
                        }
                        int i2 = g.this.i.f;
                        if (i2 == 301 || i2 == 302 || i2 == 307) {
                            jumpLoaderResult.setIs302Jump(z3);
                            if (TextUtils.isEmpty(g.this.i.f2657a)) {
                                jumpLoaderResult.setjumpDone(z3);
                                jumpLoaderResult.setUrl(str7);
                                break;
                            }
                            str7 = g.this.i.f2657a;
                            if (b(str7)) {
                                if (!str7.startsWith("/") || TextUtils.isEmpty(str6) || TextUtils.isEmpty(str5)) {
                                    jumpLoaderResult.setjumpDone(z3);
                                    jumpLoaderResult.setUrl(str7);
                                } else {
                                    StringBuilder sb2 = new StringBuilder();
                                    sb2.append(str6);
                                    sb2.append("://");
                                    sb2.append(str5);
                                    sb2.append(str7);
                                    str7 = sb2.toString();
                                    str6 = null;
                                    str5 = null;
                                }
                            } else if (!b(str7)) {
                                try {
                                    URI create2 = URI.create(str7);
                                    String scheme2 = create2.getScheme();
                                    try {
                                        str5 = create2.getHost();
                                    } catch (Exception unused3) {
                                    }
                                    str6 = scheme2;
                                } catch (Exception unused4) {
                                }
                            }
                            if (com.mintegral.msdk.base.utils.j.a.a(str7)) {
                                jumpLoaderResult.setjumpDone(z3);
                                jumpLoaderResult.setUrl(str7);
                                break;
                            }
                            if (g.this.f) {
                                String a3 = com.mintegral.msdk.b.a.a(this.c, str7);
                                if (!TextUtils.isEmpty(a3)) {
                                    StringBuilder sb3 = new StringBuilder();
                                    sb3.append(str7);
                                    sb3.append(a3);
                                    str7 = sb3.toString();
                                }
                            }
                            if (this.l != null) {
                                str7 = this.l.matchLoopback(str7);
                            }
                            i++;
                        } else {
                            if (g.this.i.f == 200) {
                                jumpLoaderResult.setjumpDone(z3);
                                jumpLoaderResult.setUrl(str7);
                                jumpLoaderResult.setContent(g.this.i.g == null ? null : g.this.i.g);
                            } else {
                                jumpLoaderResult.setjumpDone(false);
                                jumpLoaderResult.setUrl(str7);
                            }
                        }
                    }
                }
            }
            jumpLoaderResult.setjumpDone(z3);
            jumpLoaderResult.setUrl(str7);
            return jumpLoaderResult;
        }

        /* access modifiers changed from: private */
        public boolean a(String str) {
            if (this.l != null) {
                this.l.getLinkType();
            }
            if (com.mintegral.msdk.base.utils.j.a.a(str)) {
                g.this.d.setCode(1);
                g.this.d.setUrl(str);
                g.this.d.setjumpDone(true);
                return true;
            } else if (c(str)) {
                g.this.d.setCode(3);
                g.this.d.setUrl(str);
                g.this.d.setjumpDone(true);
                return true;
            } else {
                g.this.d.setCode(2);
                g.this.d.setUrl(str);
                return false;
            }
        }

        private static boolean b(String str) {
            return !URLUtil.isNetworkUrl(str);
        }

        private static boolean c(String str) {
            return !TextUtils.isEmpty(str) && str.toLowerCase().contains("apk");
        }

        public final void a() {
            g.this.d = new JumpLoaderResult();
            g.this.d.setUrl(this.d);
            g.this.d = a(this.d, this.m, this.n, this.l);
            if (!TextUtils.isEmpty(g.this.d.getExceptionMsg())) {
                g.this.d.setSuccess(true);
            }
            if (g.this.e && g.this.d.isSuccess()) {
                if (g.this.i != null) {
                    g.this.d.setStatusCode(g.this.i.f);
                }
                if (c(g.this.d.getUrl()) || com.mintegral.msdk.base.utils.j.a.a(g.this.d.getUrl()) || 200 != g.this.i.f || TextUtils.isEmpty(g.this.d.getContent()) || g.this.d.getContent().contains("EXCEPTION_CAMPAIGN_NOT_ACTIVE")) {
                    if (this.k != null) {
                        this.k.a(g.this.d.getUrl(), com.mintegral.msdk.rover.a.c, 0, "", "");
                    }
                    if (g.this.i != null) {
                        g.this.d.setType(1);
                        g.this.d.setExceptionMsg(g.this.i.h);
                        g.this.d.setStatusCode(g.this.i.f);
                        g.this.d.setHeader(g.this.i.a());
                        g.this.d.setContent(g.this.i.g);
                    }
                    a(g.this.d.getUrl());
                    return;
                }
                g.this.d.setType(2);
                if (!TextUtils.isEmpty(g.this.d.getContent())) {
                    Log.e("302", "startWebViewHtmlParser");
                    new f(g.this.k).a(this.e, this.f, this.g, this.c, g.this.d.getUrl(), g.this.d.getContent(), this.o);
                    com.mintegral.msdk.base.utils.g.d("302", "startWebViewHtmlParser");
                } else {
                    com.mintegral.msdk.base.utils.g.a("302", "startWebViewSpider");
                    try {
                        new f(g.this.k).a(this.e, this.f, this.g, this.c, g.this.d.getUrl(), this.o);
                    } catch (Exception unused) {
                        com.mintegral.msdk.base.utils.g.d("TAG", "webview spider start error");
                    }
                }
                this.b.acquireUninterruptibly();
            }
        }

        static /* synthetic */ void a(a aVar, boolean z, boolean z2, String str, String str2) {
            int i;
            long d2 = g.this.b;
            if (d2 == 0) {
                g.this.b = System.currentTimeMillis();
                i = 0;
            } else {
                long currentTimeMillis = System.currentTimeMillis();
                int i2 = (int) (currentTimeMillis - d2);
                g.this.b = currentTimeMillis;
                i = i2;
            }
            if (z) {
                if (z2) {
                    if (aVar.k != null && !g.this.f2664a) {
                        g.this.f2664a = true;
                        aVar.k.a(str, com.mintegral.msdk.rover.a.d, i, "", str2);
                    }
                } else if (aVar.k != null && !g.this.f2664a) {
                    g.this.f2664a = true;
                    aVar.k.b(str, com.mintegral.msdk.rover.a.d, i, 0, "", str2);
                }
            } else if (aVar.k != null) {
                aVar.k.a(str, com.mintegral.msdk.rover.a.d, i, 0, "", str2);
            }
        }
    }

    public g(Context context, boolean z) {
        this.g = context;
        this.k = z;
        if (z) {
            this.h = new com.mintegral.msdk.base.common.e.b(context, 1);
        } else {
            this.h = new com.mintegral.msdk.base.common.e.b(context);
        }
    }

    public final void a(String str, d dVar, boolean z, String str2, String str3, String str4, d dVar2, CampaignEx campaignEx, boolean z2, boolean z3) {
        this.c = dVar;
        this.f = z;
        com.mintegral.msdk.base.common.e.b bVar = this.h;
        a aVar = new a(this.g, str, str2, str3, str4, dVar2, campaignEx, z2, z3);
        bVar.a(aVar, this);
    }

    public final void b() {
        this.e = false;
    }

    public final void a(int i2) {
        if (i2 == C0049a.e && this.e) {
            this.j.post(new Runnable() {
                public final void run() {
                    if (g.this.c != null) {
                        if (g.this.d.isSuccess()) {
                            g.this.c.a(g.this.d);
                            return;
                        }
                        g.this.c.a(g.this.d, g.this.d.getMsg());
                    }
                }
            });
        }
    }
}
