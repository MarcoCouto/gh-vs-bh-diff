package com.mintegral.msdk.click;

import android.text.TextUtils;
import android.webkit.URLUtil;
import com.mansoon.BatteryDouble.models.Network;
import com.mintegral.msdk.b.b;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.utils.c;
import com.mintegral.msdk.base.utils.g;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.zip.GZIPInputStream;

/* compiled from: JavaHttpSpider */
public class e {

    /* renamed from: a reason: collision with root package name */
    private static final String f2656a = "e";
    private com.mintegral.msdk.b.a b;
    private String c;
    private boolean d = true;
    private final int e = 3145728;
    private a f;

    /* compiled from: JavaHttpSpider */
    public static class a {

        /* renamed from: a reason: collision with root package name */
        public String f2657a;
        public String b;
        public String c;
        public String d;
        public int e;
        public int f;
        public String g;
        public String h;

        public final String toString() {
            StringBuilder sb = new StringBuilder("http响应头：...\n");
            sb.append("statusCode=");
            sb.append(this.f);
            sb.append(", ");
            sb.append("location=");
            sb.append(this.f2657a);
            sb.append(", ");
            sb.append("contentType=");
            sb.append(this.b);
            sb.append(", ");
            sb.append("contentLength=");
            sb.append(this.e);
            sb.append(", ");
            sb.append("contentEncoding=");
            sb.append(this.c);
            sb.append(", ");
            sb.append("referer=");
            sb.append(this.d);
            return sb.toString();
        }

        public final String a() {
            StringBuilder sb = new StringBuilder();
            sb.append("statusCode=");
            sb.append(this.f);
            sb.append(", ");
            sb.append("location=");
            sb.append(this.f2657a);
            sb.append(", ");
            sb.append("contentType=");
            sb.append(this.b);
            sb.append(", ");
            sb.append("contentLength=");
            sb.append(this.e);
            sb.append(", ");
            sb.append("contentEncoding=");
            sb.append(this.c);
            sb.append(", ");
            sb.append("referer=");
            sb.append(this.d);
            return sb.toString();
        }
    }

    public final void a() {
        this.d = false;
    }

    public e() {
        b.a();
        this.b = b.b(com.mintegral.msdk.base.controller.a.d().j());
        if (this.b == null) {
            b.a();
            this.b = b.b();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:57:0x0130, code lost:
        r4 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x0132, code lost:
        r4 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x0133, code lost:
        r1 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x014c, code lost:
        r1.disconnect();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x0152, code lost:
        r0.disconnect();
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:51:0x0126 */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x012a  */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x0130 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:7:0x002c] */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x014c  */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x0152  */
    public final a a(String str, boolean z, boolean z2, CampaignEx campaignEx) {
        String replace;
        HttpURLConnection httpURLConnection;
        HttpURLConnection httpURLConnection2 = null;
        if (!URLUtil.isNetworkUrl(str)) {
            return null;
        }
        replace = str.replace(" ", "%20");
        URLUtil.isHttpsUrl(replace);
        g.b(f2656a, replace);
        this.f = new a();
        try {
            httpURLConnection = (HttpURLConnection) new URL(replace).openConnection();
            try {
                httpURLConnection.setRequestMethod(HttpRequest.METHOD_GET);
                if ((!z && !z2) || campaignEx == null) {
                    httpURLConnection.setRequestProperty("User-Agent", c.f());
                }
                if (z && campaignEx != null && campaignEx.getcUA() == 1) {
                    httpURLConnection.setRequestProperty("User-Agent", c.f());
                }
                if (z2 && campaignEx != null && campaignEx.getImpUA() == 1) {
                    httpURLConnection.setRequestProperty("User-Agent", c.f());
                }
                httpURLConnection.setRequestProperty(HttpRequest.HEADER_ACCEPT_ENCODING, HttpRequest.ENCODING_GZIP);
                if (this.b.aD() && !TextUtils.isEmpty(this.c)) {
                    httpURLConnection.setRequestProperty("referer", this.c);
                }
                httpURLConnection.setConnectTimeout(60000);
                httpURLConnection.setReadTimeout(60000);
                httpURLConnection.setInstanceFollowRedirects(false);
                httpURLConnection.connect();
                this.f.f2657a = httpURLConnection.getHeaderField("Location");
                this.f.d = httpURLConnection.getHeaderField(HttpRequest.HEADER_REFERER);
                this.f.f = httpURLConnection.getResponseCode();
                this.f.b = httpURLConnection.getContentType();
                this.f.e = httpURLConnection.getContentLength();
                this.f.c = httpURLConnection.getContentEncoding();
                g.b(f2656a, this.f.toString());
                boolean equalsIgnoreCase = HttpRequest.ENCODING_GZIP.equalsIgnoreCase(this.f.c);
                if (this.f.f == 200 && this.d && this.f.e > 0 && this.f.e < 3145728 && !TextUtils.isEmpty(replace) && !replace.endsWith(".apk")) {
                    String a2 = a(httpURLConnection.getInputStream(), equalsIgnoreCase);
                    if (!TextUtils.isEmpty(a2)) {
                        byte[] bytes = a2.getBytes();
                        if (bytes != null && bytes.length > 0 && bytes.length < 3145728) {
                            this.f.g = a2.trim();
                        }
                    }
                }
            } catch (Throwable th) {
            }
            this.c = replace;
            if (httpURLConnection != null) {
                httpURLConnection.disconnect();
            }
            return this.f;
        } catch (Throwable th2) {
            th = th2;
            try {
                this.f.h = th.getMessage();
                g.c("http jump", Network.NETWORKSTATUS_CONNECTING);
                a aVar = this.f;
                if (httpURLConnection2 != null) {
                }
                return aVar;
            } catch (Throwable th3) {
                th = th3;
                httpURLConnection = httpURLConnection2;
                if (httpURLConnection != null) {
                }
                throw th;
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x0034 A[Catch:{ all -> 0x000f }] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0048 A[SYNTHETIC, Splitter:B:25:0x0048] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0057 A[SYNTHETIC, Splitter:B:32:0x0057] */
    private String a(InputStream inputStream, boolean z) {
        StringBuilder sb = new StringBuilder();
        BufferedReader bufferedReader = null;
        if (z) {
            try {
                inputStream = new GZIPInputStream(inputStream);
            } catch (Exception e2) {
                e = e2;
                try {
                    if (this.f == null) {
                        this.f = new a();
                        this.f.h = e.getMessage();
                    }
                    e.printStackTrace();
                    if (bufferedReader != null) {
                        bufferedReader.close();
                    }
                    return sb.toString();
                } catch (Throwable th) {
                    th = th;
                    if (bufferedReader != null) {
                    }
                    throw th;
                }
            }
        }
        BufferedReader bufferedReader2 = new BufferedReader(new InputStreamReader(inputStream));
        while (true) {
            try {
                String readLine = bufferedReader2.readLine();
                if (readLine != null) {
                    sb.append(readLine);
                } else {
                    try {
                        break;
                    } catch (Exception e3) {
                        e3.printStackTrace();
                    }
                }
            } catch (Exception e4) {
                e = e4;
                bufferedReader = bufferedReader2;
                if (this.f == null) {
                }
                e.printStackTrace();
                if (bufferedReader != null) {
                }
                return sb.toString();
            } catch (Throwable th2) {
                th = th2;
                bufferedReader = bufferedReader2;
                if (bufferedReader != null) {
                    try {
                        bufferedReader.close();
                    } catch (Exception e5) {
                        e5.printStackTrace();
                    }
                }
                throw th;
            }
        }
        bufferedReader2.close();
        return sb.toString();
    }
}
