package com.mintegral.msdk.click;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.http.SslError;
import android.os.Handler;
import android.os.Looper;
import android.support.graphics.drawable.PathInterpolatorCompat;
import android.text.TextUtils;
import android.webkit.JsPromptResult;
import android.webkit.JsResult;
import android.webkit.SslErrorHandler;
import android.webkit.URLUtil;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.b.b;
import com.mintegral.msdk.base.utils.g;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.util.HashMap;

/* compiled from: WebViewSpider */
public class f {

    /* renamed from: a reason: collision with root package name */
    public static long f2658a = 0;
    /* access modifiers changed from: private */
    public static final String d = "f";
    boolean b;
    boolean c;
    /* access modifiers changed from: private */
    public int e = 15000;
    /* access modifiers changed from: private */
    public int f = PathInterpolatorCompat.MAX_NUM_POINTS;
    private Handler g = new Handler(Looper.getMainLooper());
    private com.mintegral.msdk.b.a h;
    /* access modifiers changed from: private */
    public a i;
    /* access modifiers changed from: private */
    public String j;
    private String k;
    /* access modifiers changed from: private */
    public WebView l;
    /* access modifiers changed from: private */
    public boolean m;
    /* access modifiers changed from: private */
    public String n;
    /* access modifiers changed from: private */
    public int o;
    /* access modifiers changed from: private */
    public boolean p = false;
    /* access modifiers changed from: private */
    public boolean q;
    private final Runnable r = new Runnable() {
        public final void run() {
            f.this.p = true;
            f.this.o = 1;
            String a2 = f.d;
            StringBuilder sb = new StringBuilder("js超时！超时上限：");
            sb.append(f.this.f);
            sb.append("ms");
            g.d(a2, sb.toString());
            f.p(f.this);
        }
    };
    private final Runnable s = new Runnable() {
        public final void run() {
            f.this.p = true;
            f.this.o = 2;
            String a2 = f.d;
            StringBuilder sb = new StringBuilder("http超时！超时上限：");
            sb.append(f.this.e);
            sb.append("ms");
            g.d(a2, sb.toString());
            f.p(f.this);
        }
    };

    /* compiled from: WebViewSpider */
    interface a {
        void a(String str, String str2);

        void a(String str, String str2, String str3);

        boolean a(String str);

        boolean b(String str);

        boolean c(String str);
    }

    public f(boolean z) {
        b.a();
        this.h = b.b(com.mintegral.msdk.base.controller.a.d().j());
        if (this.h == null) {
            b.a();
            this.h = b.b();
        }
        this.m = this.h.aD();
        if (z) {
            this.e = (int) this.h.aw();
            this.f = (int) this.h.aw();
            return;
        }
        this.e = (int) this.h.ay();
        this.f = (int) this.h.ay();
    }

    public final void a(String str, String str2, String str3, Context context, String str4, String str5, a aVar) {
        if (aVar != null) {
            this.k = str5;
            this.j = str4;
            this.i = aVar;
            a(str, str2, str3, context);
            return;
        }
        throw new NullPointerException("OverrideUrlLoadingListener can not be null");
    }

    public final void a(String str, String str2, String str3, Context context, String str4, a aVar) {
        if (aVar != null) {
            this.j = str4;
            this.i = aVar;
            a(str, str2, str3, context);
            return;
        }
        throw new NullPointerException("OverrideUrlLoadingListener can not be null");
    }

    private void a(String str, String str2, String str3, Context context) {
        if (Thread.currentThread() == Looper.getMainLooper().getThread()) {
            a(str, str2, str3, context, this.j);
            return;
        }
        Handler handler = this.g;
        final String str4 = str;
        final String str5 = str2;
        final String str6 = str3;
        final Context context2 = context;
        AnonymousClass1 r1 = new Runnable() {
            public final void run() {
                f.this.a(str4, str5, str6, context2, f.this.j);
            }
        };
        handler.post(r1);
    }

    /* access modifiers changed from: private */
    public void b() {
        d();
        c();
    }

    /* access modifiers changed from: private */
    public void c() {
        this.g.removeCallbacks(this.s);
    }

    /* access modifiers changed from: private */
    public void d() {
        this.g.removeCallbacks(this.r);
    }

    /* access modifiers changed from: private */
    public void a(String str, String str2, String str3, Context context, String str4) {
        try {
            this.l = new WebView(context);
            this.l.getSettings().setJavaScriptEnabled(true);
            this.l.getSettings().setCacheMode(2);
            this.l.getSettings().setLoadsImagesAutomatically(false);
            WebView webView = this.l;
            final String str5 = str3;
            final String str6 = str2;
            final Context context2 = context;
            final String str7 = str;
            AnonymousClass2 r1 = new WebViewClient() {
                public final void onPageStarted(WebView webView, String str, Bitmap bitmap) {
                    try {
                        webView.loadUrl("javascript:window.navigator.vibrate([]);");
                        boolean z = false;
                        if (f.this.q) {
                            f.this.o = 0;
                            f.c(f.this);
                            return;
                        }
                        f.this.c = false;
                        if (webView.getTag() == null) {
                            webView.setTag("has_first_started");
                        } else {
                            f.this.b = true;
                        }
                        synchronized (f.d) {
                            String str2 = "加载页面-开始：";
                            if (f.this.b || f.this.c) {
                                z = true;
                            }
                            if (z) {
                                StringBuilder sb = new StringBuilder();
                                sb.append(str2);
                                sb.append("（重定向）");
                                str2 = sb.toString();
                            }
                            if (URLUtil.isHttpsUrl(str)) {
                                String a2 = f.d;
                                StringBuilder sb2 = new StringBuilder();
                                sb2.append(str2);
                                sb2.append(str);
                                g.d(a2, sb2.toString());
                            } else {
                                String a3 = f.d;
                                StringBuilder sb3 = new StringBuilder();
                                sb3.append(str2);
                                sb3.append(str);
                                g.b(a3, sb3.toString());
                            }
                            f.this.j = str;
                            if (f.this.i == null || !f.this.i.a(str)) {
                                f.f(f.this);
                            } else {
                                f.this.q = true;
                                f.c(f.this);
                            }
                        }
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                }

                /* JADX WARNING: Code restructure failed: missing block: B:17:0x0069, code lost:
                    if (com.mintegral.msdk.click.f.i(r3.e) == false) goto L_0x0095;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:18:0x006b, code lost:
                    r4 = new java.util.HashMap();
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:19:0x007a, code lost:
                    if (com.mintegral.msdk.click.f.j(r3.e).getUrl() == null) goto L_0x008b;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:20:0x007c, code lost:
                    r4.put(io.fabric.sdk.android.services.network.HttpRequest.HEADER_REFERER, com.mintegral.msdk.click.f.j(r3.e).getUrl());
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:21:0x008b, code lost:
                    com.mintegral.msdk.click.f.j(r3.e).loadUrl(r5, r4);
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:22:0x0095, code lost:
                    com.mintegral.msdk.click.f.j(r3.e).loadUrl(r5);
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:23:0x009e, code lost:
                    return true;
                 */
                public final boolean shouldOverrideUrlLoading(WebView webView, String str) {
                    synchronized (f.d) {
                        String a2 = f.d;
                        StringBuilder sb = new StringBuilder("override js跳转：");
                        sb.append(str);
                        g.a(a2, sb.toString());
                        f.this.c = true;
                        f.this.d();
                        if (f.this.q) {
                            f.this.c();
                            f.c(f.this);
                            return true;
                        }
                        f.this.j = str;
                        if (f.this.i != null && f.this.i.b(str)) {
                            f.this.q = true;
                            f.this.c();
                            f.c(f.this);
                            return true;
                        }
                    }
                }

                public final void onReceivedError(WebView webView, int i, String str, String str2) {
                    String a2 = f.d;
                    StringBuilder sb = new StringBuilder("onReceivedError: errno = ");
                    sb.append(i);
                    sb.append(", url: ");
                    sb.append(webView.getUrl());
                    sb.append(",\n onReceivedError：, description: ");
                    sb.append(str);
                    sb.append(", failingUrl: ");
                    sb.append(str2);
                    g.b(a2, sb.toString());
                    synchronized (f.d) {
                        f.this.q = true;
                        f.this.b();
                        f.c(f.this);
                    }
                    if (f.this.i != null) {
                        f.this.i.a(webView.getUrl(), str, f.this.n);
                    }
                }

                public final void onReceivedSslError(WebView webView, SslErrorHandler sslErrorHandler, SslError sslError) {
                    try {
                        String a2 = f.d;
                        StringBuilder sb = new StringBuilder("onReceivedSslError IS_SP_CBT_CF:");
                        sb.append(MIntegralConstans.IS_SP_CBT_CF);
                        g.a(a2, sb.toString());
                        if (MIntegralConstans.IS_SP_CBT_CF && sslErrorHandler != null) {
                            sslErrorHandler.cancel();
                        }
                        if (!TextUtils.isEmpty(str5) && !TextUtils.isEmpty(str6)) {
                            new com.mintegral.msdk.base.common.d.b(context2).a(str7, str6, str5, webView.getUrl());
                        }
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                }

                public final void onPageFinished(WebView webView, String str) {
                    super.onPageFinished(webView, str);
                    try {
                        webView.loadUrl("javascript:window.navigator.vibrate([]);");
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                }
            };
            webView.setWebViewClient(r1);
            this.l.setWebChromeClient(new WebChromeClient() {
                public final boolean onJsAlert(WebView webView, String str, String str2, JsResult jsResult) {
                    return true;
                }

                public final boolean onJsConfirm(WebView webView, String str, String str2, JsResult jsResult) {
                    return true;
                }

                public final boolean onJsPrompt(WebView webView, String str, String str2, String str3, JsPromptResult jsPromptResult) {
                    return true;
                }

                public final void onProgressChanged(WebView webView, int i) {
                    if (i == 100) {
                        try {
                            String a2 = f.d;
                            StringBuilder sb = new StringBuilder("加载页面-进度完成：");
                            sb.append(webView.getUrl());
                            g.b(a2, sb.toString());
                            webView.loadUrl("javascript:window.navigator.vibrate([]);");
                            if (!f.this.q && !f.this.c) {
                                f.m(f.this);
                            }
                            if (f.this.i != null) {
                                f.this.i.c(webView.getUrl());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
            if (!TextUtils.isEmpty(this.k)) {
                this.l.getSettings().setDefaultTextEncodingName("utf-8");
                this.f = 2000;
                this.e = 2000;
                g.b(d, this.k);
                this.l.loadDataWithBaseURL(str4, this.k, "*/*", "utf-8", str4);
            } else if (this.m) {
                HashMap hashMap = new HashMap();
                if (this.l.getUrl() != null) {
                    hashMap.put(HttpRequest.HEADER_REFERER, this.l.getUrl());
                }
                this.l.loadUrl(str4, hashMap);
            } else {
                this.l.loadUrl(str4);
            }
        } catch (Throwable th) {
            try {
                if (this.i != null) {
                    this.i.a(this.j, th.getMessage(), this.n);
                }
            } catch (Exception unused) {
            }
        }
    }

    /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x0016 */
    static /* synthetic */ void c(f fVar) {
        synchronized (d) {
            try {
                fVar.b();
                if (fVar.i != null) {
                    fVar.i.a(fVar.j, fVar.n);
                }
            } catch (Exception unused) {
                g.d(d, "webview colse to failed");
            } catch (Throwable ) {
                g.d(d, "webview colse to failed");
            }
        }
    }

    static /* synthetic */ void f(f fVar) {
        fVar.c();
        fVar.g.postDelayed(fVar.s, (long) fVar.e);
    }

    static /* synthetic */ void m(f fVar) {
        fVar.d();
        fVar.g.postDelayed(fVar.r, (long) fVar.f);
    }

    /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x001b */
    static /* synthetic */ void p(f fVar) {
        synchronized (d) {
            try {
                fVar.b();
                fVar.l.destroy();
                if (fVar.i != null) {
                    fVar.i.a(fVar.j, fVar.n);
                }
            } catch (Exception unused) {
                g.d(d, "webview colse to failed");
            } catch (Throwable ) {
                g.d(d, "webview colse to failed");
            }
        }
    }
}
