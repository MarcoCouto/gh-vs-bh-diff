package com.mintegral.msdk.click;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.TextUtils;
import android.webkit.MimeTypeMap;
import android.widget.Toast;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.base.controller.a;
import com.mintegral.msdk.base.utils.MTGFileProvider;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.base.utils.r;
import com.mintegral.msdk.base.utils.t;
import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/* compiled from: CommonClickUtil */
public final class b {

    /* renamed from: a reason: collision with root package name */
    static Handler f2654a = new Handler(Looper.getMainLooper()) {
        public final void handleMessage(Message message) {
            super.handleMessage(message);
            try {
                if (message.what == 1000) {
                    int i = message.arg1;
                    int i2 = message.arg2;
                    Bundle data = message.getData();
                    String str = "";
                    String str2 = "";
                    if (data != null) {
                        str = data.getString("rid");
                        str2 = data.getString("cid");
                    }
                    new com.mintegral.msdk.base.common.d.b(a.d().h()).a(i, i2, str, str2);
                }
            } catch (Throwable th) {
                if (MIntegralConstans.DEBUG) {
                    th.printStackTrace();
                }
            }
        }
    };

    public static String a(String str) {
        if (TextUtils.isEmpty(str)) {
            return "";
        }
        if (str.lastIndexOf("/") == -1) {
            StringBuilder sb = new StringBuilder();
            sb.append(str.hashCode());
            return sb.toString();
        }
        StringBuilder sb2 = new StringBuilder();
        sb2.append(str.hashCode() + str.substring(str.lastIndexOf("/") + 1).hashCode());
        return sb2.toString();
    }

    public static void a(Context context, String str) {
        if (str != null && context != null) {
            try {
                Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(str));
                intent.addFlags(268435456);
                ResolveInfo resolveActivity = context.getPackageManager().resolveActivity(intent, 65536);
                if (resolveActivity != null) {
                    intent.setClassName(resolveActivity.activityInfo.packageName, resolveActivity.activityInfo.name);
                }
                context.startActivity(intent);
            } catch (Exception e) {
                e.printStackTrace();
                try {
                    Intent intent2 = new Intent("android.intent.action.VIEW", Uri.parse(str));
                    intent2.addFlags(268435456);
                    context.startActivity(intent2);
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
        }
    }

    /* JADX WARNING: Missing exception handler attribute for start block: B:27:0x0111 */
    public static void a(Context context, Uri uri, String str) {
        File file;
        try {
            file = new File(new URI(uri.toString()));
        } catch (URISyntaxException e) {
            e.printStackTrace();
            file = null;
        }
        Intent intent = new Intent();
        intent.addFlags(268435456);
        intent.setAction("android.intent.action.VIEW");
        try {
            if (!t.b(context)) {
                b(context, str);
            } else if (t.d(context)) {
                if (VERSION.SDK_INT >= 24) {
                    StringBuilder sb = new StringBuilder();
                    sb.append(context.getApplicationContext().getPackageName());
                    sb.append(".mtgFileProvider");
                    Uri uriForFile = MTGFileProvider.getUriForFile(context, sb.toString(), file);
                    if (uriForFile != null) {
                        intent.addFlags(1);
                        intent.setDataAndType(uriForFile, context.getContentResolver().getType(uriForFile));
                        context.startActivity(intent);
                    } else {
                        b(context, str);
                    }
                } else {
                    intent.setDataAndType(Uri.fromFile(file), a(file));
                    context.startActivity(intent);
                }
            } else if (t.e(context)) {
                b(context, str);
            } else {
                intent.setDataAndType(Uri.fromFile(file), a(file));
                context.startActivity(intent);
            }
            StringBuilder sb2 = new StringBuilder();
            sb2.append(str);
            sb2.append("downloadType");
            int intValue = ((Integer) r.b(context, sb2.toString(), Integer.valueOf(-1))).intValue();
            StringBuilder sb3 = new StringBuilder();
            sb3.append(str);
            sb3.append("linkType");
            int intValue2 = ((Integer) r.b(context, sb3.toString(), Integer.valueOf(-1))).intValue();
            StringBuilder sb4 = new StringBuilder();
            sb4.append(str);
            sb4.append("rid");
            String str2 = (String) r.b(context, sb4.toString(), "");
            StringBuilder sb5 = new StringBuilder();
            sb5.append(str);
            sb5.append("cid");
            String str3 = (String) r.b(context, sb5.toString(), "");
            if (intValue != 3) {
                a(intValue, intValue2, str2, str3);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            b(context, str);
            StringBuilder sb6 = new StringBuilder();
            sb6.append(str);
            sb6.append("downloadType");
            int intValue3 = ((Integer) r.b(context, sb6.toString(), Integer.valueOf(-1))).intValue();
            StringBuilder sb7 = new StringBuilder();
            sb7.append(str);
            sb7.append("linkType");
            int intValue4 = ((Integer) r.b(context, sb7.toString(), Integer.valueOf(-1))).intValue();
            StringBuilder sb8 = new StringBuilder();
            sb8.append(str);
            sb8.append("rid");
            String str4 = (String) r.b(context, sb8.toString(), "");
            StringBuilder sb9 = new StringBuilder();
            sb9.append(str);
            sb9.append("cid");
            String str5 = (String) r.b(context, sb9.toString(), "");
            if (intValue3 != 3) {
                a(intValue3, intValue4, str4, str5);
            }
        } catch (Throwable th) {
            StringBuilder sb10 = new StringBuilder();
            sb10.append(str);
            sb10.append("downloadType");
            int intValue5 = ((Integer) r.b(context, sb10.toString(), Integer.valueOf(-1))).intValue();
            StringBuilder sb11 = new StringBuilder();
            sb11.append(str);
            sb11.append("linkType");
            int intValue6 = ((Integer) r.b(context, sb11.toString(), Integer.valueOf(-1))).intValue();
            StringBuilder sb12 = new StringBuilder();
            sb12.append(str);
            sb12.append("rid");
            String str6 = (String) r.b(context, sb12.toString(), "");
            StringBuilder sb13 = new StringBuilder();
            sb13.append(str);
            sb13.append("cid");
            String str7 = (String) r.b(context, sb13.toString(), "");
            if (intValue5 != 3) {
                a(intValue5, intValue6, str6, str7);
            }
            throw th;
        }
    }

    private static void a(int i, int i2, String str, String str2) {
        try {
            if (Looper.myLooper() == Looper.getMainLooper()) {
                new com.mintegral.msdk.base.common.d.b(a.d().h()).a(i, i2, str, str2);
                return;
            }
            Message obtain = Message.obtain();
            obtain.what = 1000;
            obtain.arg1 = i;
            obtain.arg2 = i2;
            Bundle bundle = new Bundle();
            bundle.putString("rid", str);
            bundle.putString("cid", str2);
            obtain.setData(bundle);
            f2654a.sendMessage(obtain);
        } catch (Throwable th) {
            if (MIntegralConstans.DEBUG) {
                th.printStackTrace();
            }
        }
    }

    public static void b(Context context, String str) {
        try {
            Context h = a.d().h();
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append("downloadType");
            r.a(h, sb.toString(), Integer.valueOf(3));
            a(context, str);
            StringBuilder sb2 = new StringBuilder();
            sb2.append(str);
            sb2.append("downloadType");
            int intValue = ((Integer) r.b(context, sb2.toString(), Integer.valueOf(-1))).intValue();
            StringBuilder sb3 = new StringBuilder();
            sb3.append(str);
            sb3.append("linkType");
            int intValue2 = ((Integer) r.b(context, sb3.toString(), Integer.valueOf(-1))).intValue();
            StringBuilder sb4 = new StringBuilder();
            sb4.append(str);
            sb4.append("rid");
            String str2 = (String) r.b(context, sb4.toString(), "");
            StringBuilder sb5 = new StringBuilder();
            sb5.append(str);
            sb5.append("cid");
            a(intValue, intValue2, str2, (String) r.b(context, sb5.toString(), ""));
        } catch (Throwable th) {
            if (MIntegralConstans.DEBUG) {
                th.printStackTrace();
            }
        }
    }

    private static String a(File file) {
        String name = file.getName();
        return MimeTypeMap.getSingleton().getMimeTypeFromExtension(name.substring(name.lastIndexOf(".") + 1, name.length()).toLowerCase());
    }

    public static boolean c(Context context, String str) {
        if (str == null || "".equals(str)) {
            return false;
        }
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(str, 8192);
            if (packageInfo != null) {
                return str.equals(packageInfo.packageName);
            }
            return false;
        } catch (Exception unused) {
            return false;
        }
    }

    public static boolean d(Context context, String str) {
        try {
            if (TextUtils.isEmpty(str)) {
                return false;
            }
            context.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)));
            return true;
        } catch (Throwable th) {
            g.c("SDKCLICK", th.getMessage(), th);
            return false;
        }
    }

    public static void e(Context context, String str) {
        try {
            if (!TextUtils.isEmpty(str) && c(context, str)) {
                Intent launchIntentForPackage = context.getPackageManager().getLaunchIntentForPackage(str);
                if (launchIntentForPackage != null) {
                    List queryIntentActivities = context.getPackageManager().queryIntentActivities(launchIntentForPackage, 0);
                    if (queryIntentActivities != null && queryIntentActivities.size() > 0) {
                        ResolveInfo resolveInfo = (ResolveInfo) queryIntentActivities.iterator().next();
                        if (resolveInfo != null) {
                            ComponentName componentName = new ComponentName(resolveInfo.activityInfo.packageName, resolveInfo.activityInfo.name);
                            Intent intent = new Intent();
                            intent.setComponent(componentName);
                            intent.addFlags(268435456);
                            context.startActivity(intent);
                        }
                    }
                }
            }
        } catch (Exception e) {
            Toast.makeText(context, "The app connot start up", 0).show();
            e.printStackTrace();
        }
    }

    private static String a(String str, List<String> list, String str2) {
        if (list != null) {
            for (String str3 : list) {
                if (!TextUtils.isEmpty(str3)) {
                    str = str.replaceAll(str3, str2);
                }
            }
        }
        return str;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0052, code lost:
        r0 = (com.mintegral.msdk.b.a.C0048a) r1.get(r3);
        r1 = a(r6, r0.d(), java.lang.String.valueOf((float) com.mintegral.msdk.base.utils.c.m(com.mintegral.msdk.base.controller.a.d().h())));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:?, code lost:
        r6 = a(r1, r0.c(), java.lang.String.valueOf((float) com.mintegral.msdk.base.utils.c.l(com.mintegral.msdk.base.controller.a.d().h())));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
        r7 = a(r6, r0.a(), r7);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:?, code lost:
        r6 = a(r7, r0.b(), r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x009b, code lost:
        r6 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x009c, code lost:
        r5 = r7;
        r7 = r6;
        r6 = r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x00a0, code lost:
        r6 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x00a1, code lost:
        r7 = r6;
        r6 = r1;
     */
    public static String a(String str, String str2, String str3) {
        try {
            if (TextUtils.isEmpty(str)) {
                return str;
            }
            String host = Uri.parse(str).getHost();
            com.mintegral.msdk.b.b.a();
            com.mintegral.msdk.b.a b = com.mintegral.msdk.b.b.b(a.d().j());
            if (b != null) {
                Map bf = b.bf();
                if (bf != null && !TextUtils.isEmpty(host)) {
                    Iterator it = bf.entrySet().iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            break;
                        }
                        String str4 = (String) ((Entry) it.next()).getKey();
                        if (!TextUtils.isEmpty(str4) && host.contains(str4)) {
                            break;
                        }
                    }
                }
            }
            return str;
        } catch (Exception e) {
            e = e;
            e.printStackTrace();
            return str;
        }
    }
}
