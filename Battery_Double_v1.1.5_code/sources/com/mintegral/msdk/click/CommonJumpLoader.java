package com.mintegral.msdk.click;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import com.mintegral.msdk.base.common.e.b;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.rover.d;
import com.mintegral.msdk.system.NoProGuard;
import java.io.Serializable;

public final class CommonJumpLoader extends c {

    /* renamed from: a reason: collision with root package name */
    JumpLoaderResult f2645a = null;
    private int b = 0;
    private String c = null;
    private d d = null;
    private boolean e = true;
    private b f;
    private g g;
    private Handler h = new Handler(Looper.getMainLooper());

    public static class JumpLoaderResult implements NoProGuard, Serializable {
        public static final int CODE_DOWNLOAD = 3;
        public static final int CODE_LINK = 2;
        public static final int CODE_MARKET = 1;
        public static final int CODE_NULL = 4;
        private static final long serialVersionUID = 1;
        private int code;
        private String content;
        private String exceptionMsg;
        private String header;
        private boolean is302Jump;
        private boolean jumpDone;
        private String msg;
        private String noticeurl;
        private int statusCode;
        private boolean success;
        private int type;
        private String url;

        public int getStatusCode() {
            return this.statusCode;
        }

        public void setStatusCode(int i) {
            this.statusCode = i;
        }

        public boolean isIs302Jump() {
            return this.is302Jump;
        }

        public void setIs302Jump(boolean z) {
            this.is302Jump = z;
        }

        public int getType() {
            return this.type;
        }

        public void setType(int i) {
            this.type = i;
        }

        public String getHeader() {
            return this.header;
        }

        public void setHeader(String str) {
            this.header = str;
        }

        public String getExceptionMsg() {
            return this.exceptionMsg;
        }

        public void setExceptionMsg(String str) {
            this.exceptionMsg = str;
        }

        public String getContent() {
            return this.content;
        }

        public void setContent(String str) {
            this.content = str;
        }

        public String getNoticeurl() {
            return this.noticeurl;
        }

        public void setNoticeurl(String str) {
            this.noticeurl = str;
        }

        public boolean isSuccess() {
            return this.success;
        }

        public void setSuccess(boolean z) {
            this.success = z;
        }

        public String getMsg() {
            return this.msg;
        }

        public void setMsg(String str) {
            this.msg = str;
        }

        public int getCode() {
            return this.code;
        }

        public void setCode(int i) {
            this.code = i;
        }

        public String getUrl() {
            return this.url;
        }

        public void setUrl(String str) {
            this.url = str;
        }

        public boolean isjumpDone() {
            return this.jumpDone;
        }

        public void setjumpDone(boolean z) {
            this.jumpDone = z;
        }
    }

    public final boolean a() {
        return this.e;
    }

    public CommonJumpLoader(Context context, boolean z) {
        if (z) {
            this.f = new b(context, 2);
        } else {
            this.f = new b(context);
        }
        this.g = new g(context, z);
    }

    public final void a(String str, String str2, CampaignEx campaignEx, d dVar, String str3, boolean z, boolean z2) {
        this.c = str3;
        this.d = dVar;
        this.f2645a = null;
        String str4 = "";
        boolean z3 = false;
        if (campaignEx != null) {
            if ("5".equals(campaignEx.getClick_mode()) || "6".equals(campaignEx.getClick_mode())) {
                z3 = true;
            }
            str4 = campaignEx.getId();
        }
        this.g.a(str3, dVar, z3, str, str4, str2, null, campaignEx, z, z2);
    }

    public final void a(String str, String str2, CampaignEx campaignEx, d dVar, d dVar2) {
        this.c = new String(campaignEx.getClickURL());
        this.d = dVar;
        this.f2645a = null;
        this.g.a(campaignEx.getClickURL(), dVar, "5".equals(campaignEx.getClick_mode()) || "6".equals(campaignEx.getClick_mode()), str, campaignEx.getId(), str2, dVar2, campaignEx, true, false);
    }

    public final void b() {
        this.e = false;
    }
}
