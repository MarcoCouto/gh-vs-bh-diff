package com.mintegral.msdk.click;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.text.TextUtils;
import android.widget.Toast;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.base.b.d;
import com.mintegral.msdk.base.b.h;
import com.mintegral.msdk.base.b.i;
import com.mintegral.msdk.base.common.d.b;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.utils.c;
import com.mintegral.msdk.base.utils.e;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.base.utils.j;
import com.mintegral.msdk.base.utils.k;
import com.mintegral.msdk.base.utils.r;
import com.mintegral.msdk.base.utils.s;
import com.mintegral.msdk.base.utils.t;
import com.mintegral.msdk.click.CommonJumpLoader.JumpLoaderResult;
import com.mintegral.msdk.out.AppWallTrackingListener;
import com.mintegral.msdk.out.Campaign;
import com.mintegral.msdk.out.Frame;
import com.mintegral.msdk.out.IDownloadListener;
import com.mintegral.msdk.out.LoadingActivity;
import com.mintegral.msdk.out.NativeListener.NativeAdListener;
import com.mintegral.msdk.out.NativeListener.NativeTrackingListener;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

/* compiled from: CommonClickControl */
public class a {

    /* renamed from: a reason: collision with root package name */
    public static boolean f2646a = false;
    public static Set<String> b = new HashSet();
    public static final String c = a.class.getName();
    public static Map<String, Long> d = new HashMap();
    public static Map<String, Long> e = new HashMap();
    public static Set<String> f = new HashSet();
    static Handler g = new Handler(Looper.getMainLooper()) {
        public final void handleMessage(Message message) {
            super.handleMessage(message);
            switch (message.what) {
                case 0:
                    Object obj = message.obj;
                    if (obj != null && (obj instanceof Context)) {
                        a.a((Context) obj);
                        return;
                    }
                case 1:
                    a.f();
                    break;
            }
        }
    };
    private String h = "CommonClickControl";
    /* access modifiers changed from: private */
    public String i;
    private long j;
    private long k;
    /* access modifiers changed from: private */
    public i l = null;
    /* access modifiers changed from: private */
    public Context m = null;
    private CommonJumpLoader n;
    private HashMap<String, CommonJumpLoader> o;
    private AppWallTrackingListener p;
    /* access modifiers changed from: private */
    public NativeTrackingListener q = null;
    private b r;
    /* access modifiers changed from: private */
    public boolean s;
    private com.mintegral.msdk.b.a t;
    private boolean u;
    /* access modifiers changed from: private */
    public boolean v = false;
    private boolean w = true;
    private Handler x = new Handler() {
        public final void handleMessage(Message message) {
            super.handleMessage(message);
            switch (message.what) {
                case 0:
                    if (a.this.q != null) {
                        a.this.q.onDownloadStart(null);
                        return;
                    }
                    break;
                case 1:
                    if (a.this.q != null) {
                        a.this.q.onDownloadProgress(message.arg1);
                        return;
                    }
                    break;
                case 2:
                    if (a.this.q != null) {
                        a.this.q.onDownloadFinish((Campaign) message.obj);
                        break;
                    }
                    break;
            }
        }
    };
    /* access modifiers changed from: private */
    public boolean y;

    public a(Context context, String str) {
        com.mintegral.msdk.b.b.a();
        this.t = com.mintegral.msdk.b.b.b(str);
        if (this.t == null) {
            com.mintegral.msdk.b.b.a();
            this.t = com.mintegral.msdk.b.b.b();
        }
        this.u = this.t.aA();
        this.m = context;
        this.i = str;
        if (this.l == null) {
            this.l = i.a(this.m);
        }
        this.r = new b(this.m);
        this.o = new HashMap<>();
    }

    public final void a(String str) {
        this.i = str;
    }

    public final void a(NativeTrackingListener nativeTrackingListener) {
        this.q = nativeTrackingListener;
    }

    public final void a() {
        this.w = false;
    }

    public final void a(AppWallTrackingListener appWallTrackingListener) {
        this.p = appWallTrackingListener;
    }

    public final void b() {
        try {
            if (this.o != null) {
                Set<Entry> entrySet = this.o.entrySet();
                if (entrySet != null && entrySet.size() > 0) {
                    for (Entry entry : entrySet) {
                        if (entry != null) {
                            CommonJumpLoader commonJumpLoader = (CommonJumpLoader) entry.getValue();
                            if (commonJumpLoader != null) {
                                commonJumpLoader.b();
                            }
                        }
                    }
                }
            }
            this.q = null;
        } catch (Exception unused) {
        }
    }

    public final void a(CampaignEx campaignEx, NativeAdListener nativeAdListener) {
        if (!(nativeAdListener == null || campaignEx == null)) {
            nativeAdListener.onAdClick(campaignEx);
        }
        g.b("Mintegral SDK M", "clickStart");
        b(campaignEx);
    }

    public final void c() {
        if (this.n != null && this.n.a()) {
            this.n.b();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0012, code lost:
        if (3 == r6.getLinkType()) goto L_0x0014;
     */
    private static boolean c(CampaignEx campaignEx) {
        if (campaignEx != null) {
            try {
                if (2 != campaignEx.getLinkType()) {
                }
                String id = campaignEx.getId();
                if (e != null) {
                    if (e.containsKey(id)) {
                        Long l2 = (Long) e.get(id);
                        if (l2 != null) {
                            long currentTimeMillis = System.currentTimeMillis();
                            if (l2.longValue() > currentTimeMillis || f.contains(campaignEx.getId())) {
                                StringBuilder sb = new StringBuilder("点击时间未超过coit ");
                                sb.append(currentTimeMillis);
                                sb.append("|");
                                sb.append(l2);
                                g.b("Mintegral SDK M", sb.toString());
                                return false;
                            }
                        }
                    }
                    StringBuilder sb2 = new StringBuilder("未发现有点击或点击超时保存点击时间 interval = ");
                    sb2.append(campaignEx.getClickTimeOutInterval());
                    g.b("Mintegral SDK M", sb2.toString());
                    e.put(campaignEx.getId(), Long.valueOf(System.currentTimeMillis() + ((long) (campaignEx.getClickTimeOutInterval() * 1000))));
                }
            } catch (Exception e2) {
                if (MIntegralConstans.DEBUG) {
                    e2.printStackTrace();
                }
            }
            return true;
        }
    }

    public final void a(CampaignEx campaignEx) {
        try {
            if (c(campaignEx)) {
                d a2 = d.a((h) this.l);
                a2.c();
                JumpLoaderResult b2 = a2.b(campaignEx.getId(), this.i);
                if (b2 != null) {
                    if (b2.getNoticeurl() != null) {
                        b2.setNoticeurl(null);
                    }
                    campaignEx.setJumpResult(b2);
                    String str = this.i;
                    campaignEx.getTtc_type();
                    a2.a(campaignEx, str);
                }
                if (b.c(this.m, campaignEx.getPackageName())) {
                    StringBuilder sb = new StringBuilder();
                    sb.append(campaignEx.getPackageName());
                    sb.append(" is intalled.");
                    g.a("Mintegral SDK M", sb.toString());
                    return;
                }
                a(campaignEx, campaignEx.getTtc_type(), false, Boolean.valueOf(false));
            }
        } catch (Throwable th) {
            g.c("Mintegral SDK M", th.getMessage(), th);
        }
    }

    private void a(CampaignEx campaignEx, String str) {
        if (!TextUtils.isEmpty(str)) {
            a(this.m, campaignEx, this.i, str, true, false);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:51:0x0162 A[Catch:{ Throwable -> 0x02ed }] */
    /* JADX WARNING: Removed duplicated region for block: B:80:0x01fa A[Catch:{ Throwable -> 0x02ed }] */
    public final void b(CampaignEx campaignEx) {
        boolean z;
        try {
            this.s = false;
            if (c(campaignEx)) {
                String noticeUrl = campaignEx.getNoticeUrl();
                d a2 = d.a((h) this.l);
                a2.c();
                JumpLoaderResult b2 = a2.b(campaignEx.getId(), this.i);
                if (b2 != null) {
                    if (b2.getNoticeurl() != null) {
                        b2.setNoticeurl(null);
                    }
                    campaignEx.setJumpResult(b2);
                    String str = this.i;
                    campaignEx.getTtc_type();
                    a2.a(campaignEx, str);
                }
                if (k.a(campaignEx)) {
                    if (b.d(this.m, campaignEx.getDeepLinkURL())) {
                        StringBuilder sb = new StringBuilder();
                        sb.append(noticeUrl);
                        sb.append("&opdptype=1");
                        a(campaignEx, sb.toString());
                        return;
                    }
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append(noticeUrl);
                    sb2.append("&opdptype=0");
                    noticeUrl = sb2.toString();
                }
                a(campaignEx, noticeUrl);
                if (!TextUtils.isEmpty(campaignEx.getGhId())) {
                    Context context = this.m;
                    String ghId = campaignEx.getGhId();
                    String ghPath = campaignEx.getGhPath();
                    String bindId = campaignEx.getBindId();
                    if (context != null && !TextUtils.isEmpty(ghId) && !TextUtils.isEmpty(bindId)) {
                        String str2 = "";
                        if (TextUtils.isEmpty(ghPath)) {
                            ghPath = str2;
                        }
                        Cursor query = context.getContentResolver().query(Uri.parse(com.mintegral.msdk.base.utils.a.c("DFK/J75/JaEXWFfXYZPTHkPUHkPTWr2wWgf3LBPUYF2wWgSBYbHuH75BWFetJkPULcJDnkQ/L+SBYFJBDkT=")), null, null, new String[]{bindId, ghId, ghPath, "0"}, null);
                        if (query != null) {
                            query.close();
                        }
                    }
                } else if (b.c(this.m, campaignEx.getPackageName())) {
                    b.e(this.m, campaignEx.getPackageName());
                    StringBuilder sb3 = new StringBuilder();
                    sb3.append(campaignEx.getPackageName());
                    sb3.append(" is intalled.");
                    g.a("Mintegral SDK M", sb3.toString());
                    if (this.q != null) {
                        this.q.onStartRedirection(campaignEx, campaignEx.getClickURL());
                    }
                    a(campaignEx, campaignEx.getTtc_type(), true, Boolean.valueOf(true));
                    if (this.q != null) {
                        this.q.onFinishRedirection(campaignEx, campaignEx.getClickURL());
                    }
                } else {
                    int linkType = campaignEx.getLinkType();
                    int e2 = e();
                    StringBuilder sb4 = new StringBuilder("======302跳转前linkType:");
                    sb4.append(linkType);
                    sb4.append(" openType:");
                    sb4.append(e2);
                    sb4.append("======landingType：");
                    sb4.append(campaignEx.getLandingType());
                    g.b("Mintegral SDK M", sb4.toString());
                    if (!(linkType == 8 || linkType == 9)) {
                        if (linkType != 4) {
                            z = false;
                            if (!z) {
                                String clickURL = campaignEx.getClickURL();
                                if (this.q != null) {
                                    this.q.onStartRedirection(campaignEx, clickURL);
                                }
                                if (TextUtils.isEmpty(clickURL)) {
                                    StringBuilder sb5 = new StringBuilder("linketype=");
                                    sb5.append(linkType);
                                    sb5.append(" clickurl 为空");
                                    g.b("Mintegral SDK M", sb5.toString());
                                    if (this.q != null) {
                                        this.q.onRedirectionFailed(campaignEx, clickURL);
                                    }
                                    a(b2, campaignEx, true);
                                    return;
                                } else if (linkType == 8) {
                                    g.b("Mintegral SDK M", "linketype=8 用webview 打开");
                                    j.a(this.m, clickURL, campaignEx);
                                    a(b2, campaignEx, false);
                                    if (this.q != null) {
                                        this.q.onFinishRedirection(campaignEx, clickURL);
                                    }
                                    return;
                                } else if (linkType == 9) {
                                    g.b("Mintegral SDK M", "linketype=9 用浏览器 打开");
                                    j.b(this.m, clickURL);
                                    a(b2, campaignEx, false);
                                    if (this.q != null) {
                                        this.q.onFinishRedirection(campaignEx, clickURL);
                                    }
                                    return;
                                } else {
                                    if (linkType == 4) {
                                        if (e2 == 2) {
                                            g.b("Mintegral SDK M", "linketype=4 opent=2 用webview 打开");
                                            j.a(this.m, clickURL, campaignEx);
                                        } else {
                                            g.b("Mintegral SDK M", "linketype=4 opent=不为2 用Browser 打开");
                                            j.b(this.m, clickURL);
                                        }
                                    }
                                    if (this.q != null) {
                                        this.q.onFinishRedirection(campaignEx, clickURL);
                                    }
                                    a(b2, campaignEx, false);
                                    return;
                                }
                            } else if (linkType == 2) {
                                StringBuilder sb6 = new StringBuilder("linktype为2 开始做302跳转");
                                sb6.append(campaignEx.getClickURL());
                                g.b("Mintegral SDK M", sb6.toString());
                                if (!campaignEx.getClickURL().startsWith("market://")) {
                                    if (!campaignEx.getClickURL().startsWith("https://play.google.com/")) {
                                        a(campaignEx, campaignEx.getTtc_type(), false, Boolean.valueOf(true));
                                        return;
                                    }
                                }
                                if (this.q != null) {
                                    this.q.onStartRedirection(campaignEx, campaignEx.getClickURL());
                                }
                                if (!com.mintegral.msdk.base.utils.j.a.a(this.m, campaignEx.getClickURL())) {
                                    a(e2, campaignEx.getClickURL(), campaignEx);
                                }
                                a(b2, campaignEx, false);
                                StringBuilder sb7 = new StringBuilder("不用做302跳转 最终地址已经是gp了：");
                                sb7.append(campaignEx.getClickURL());
                                g.b("Mintegral SDK M", sb7.toString());
                                if (this.q != null) {
                                    this.q.onFinishRedirection(campaignEx, campaignEx.getClickURL());
                                }
                                return;
                            } else if (linkType == 3) {
                                StringBuilder sb8 = new StringBuilder("linktype为3 开始做302跳转");
                                sb8.append(campaignEx.getClickURL());
                                g.b("Mintegral SDK M", sb8.toString());
                                a(campaignEx, campaignEx.getTtc_type(), false, Boolean.valueOf(true));
                                return;
                            } else {
                                String clickURL2 = campaignEx.getClickURL();
                                if (TextUtils.isEmpty(clickURL2)) {
                                    StringBuilder sb9 = new StringBuilder("linketype=");
                                    sb9.append(linkType);
                                    sb9.append(" clickurl 为空");
                                    g.b("Mintegral SDK M", sb9.toString());
                                    if (this.q != null) {
                                        this.q.onRedirectionFailed(campaignEx, clickURL2);
                                    }
                                    a(b2, campaignEx, true);
                                    return;
                                }
                                if (this.q != null) {
                                    this.q.onFinishRedirection(campaignEx, clickURL2);
                                }
                                g.b("Mintegral SDK M", "linketyp不是23489的值 用浏览器 打开");
                                j.b(this.m, clickURL2);
                                a(b2, campaignEx, false);
                                return;
                            }
                        }
                    }
                    z = true;
                    if (!z) {
                    }
                }
            }
        } catch (Throwable th) {
            g.c("Mintegral SDK M", th.getMessage(), th);
        }
    }

    public static void a(Context context, CampaignEx campaignEx, String str, String str2, boolean z, boolean z2) {
        if (context != null) {
            new CommonJumpLoader(context, true).a("2", str, campaignEx, new d() {
                public final void a(Object obj) {
                }

                public final void a(Object obj, String str) {
                }
            }, str2, z, z2);
        }
    }

    public static void a(Context context, CampaignEx campaignEx, String str, String[] strArr, boolean z) {
        if (context != null && campaignEx != null && !TextUtils.isEmpty(str) && strArr != null) {
            CommonJumpLoader commonJumpLoader = new CommonJumpLoader(context, true);
            for (String a2 : strArr) {
                commonJumpLoader.a("2", str, campaignEx, new d() {
                    public final void a(Object obj) {
                    }

                    public final void a(Object obj, String str) {
                    }
                }, a2, false, z);
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:45:0x011a A[Catch:{ Exception -> 0x0178 }] */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x0123 A[Catch:{ Exception -> 0x0178 }] */
    private void a(final CampaignEx campaignEx, int i2, boolean z, Boolean bool) {
        final boolean z2;
        final boolean z3;
        CampaignEx campaignEx2 = campaignEx;
        final boolean z4 = z;
        try {
            this.j = System.currentTimeMillis();
            if (this.q == null || z4) {
                z2 = true;
            } else {
                this.q.onStartRedirection(campaignEx, campaignEx.getClickURL());
                z2 = !this.q.onInterceptDefaultLoadingDialog();
            }
            if (this.o.containsKey(campaignEx.getClickURL())) {
                ((CommonJumpLoader) this.o.get(campaignEx.getClickURL())).b();
                this.o.remove(campaignEx.getClickURL());
            }
            this.y = false;
            if (campaignEx.getJumpResult() != null) {
                if (!z4) {
                    a(campaignEx, campaignEx.getJumpResult(), true, this.v, bool);
                }
                this.y = true;
                this.v = false;
                z3 = false;
            } else {
                z3 = true;
            }
            if (!d.a((h) this.l).a(campaignEx.getId(), this.i) || campaignEx.getJumpResult() == null) {
                d a2 = d.a((h) this.l);
                a2.c();
                JumpLoaderResult b2 = a2.b(campaignEx.getId(), this.i);
                if (b2 == null || z4) {
                    if (campaignEx.getClick_mode().equals("6") && !campaignEx.getPackageName().isEmpty() && campaignEx.getLinkType() == 2 && !z4) {
                        Context context = this.m;
                        StringBuilder sb = new StringBuilder("market://details?id=");
                        sb.append(campaignEx.getPackageName());
                        com.mintegral.msdk.base.utils.j.a.a(context, sb.toString());
                        if (this.q != null && z3) {
                            this.q.onDismissLoading(campaignEx);
                            this.q.onFinishRedirection(campaignEx, null);
                        }
                        this.y = true;
                        z3 = false;
                    }
                    if (z4) {
                        this.y = true;
                        this.v = false;
                    }
                    g.b("Mintegral SDK M", "Start 302 Redirection... ");
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        public final void run() {
                            if (z2 && !a.f2646a && !a.this.y && MIntegralConstans.NATIVE_SHOW_LOADINGPAGER && !z4) {
                                a.a(a.this, campaignEx);
                            }
                            if (!z2 && a.this.q != null && !a.f2646a && !a.this.y && MIntegralConstans.NATIVE_SHOW_LOADINGPAGER) {
                                a.this.q.onShowLoading(campaignEx);
                            }
                        }
                    });
                    if (this.n != null) {
                        this.n.b();
                    }
                    if (f != null || !f.contains(campaignEx.getId())) {
                        f.add(campaignEx.getId());
                        this.n = new CommonJumpLoader(this.m, false);
                        CommonJumpLoader commonJumpLoader = this.n;
                        String str = this.i;
                        final CampaignEx campaignEx3 = campaignEx;
                        final Boolean bool2 = bool;
                        final int i3 = i2;
                        final boolean z5 = z2;
                        AnonymousClass8 r1 = new d() {
                            public final void a(Object obj) {
                                if (a.f != null) {
                                    a.f.remove(campaignEx3.getId());
                                }
                                if (obj != null && (obj instanceof JumpLoaderResult)) {
                                    JumpLoaderResult jumpLoaderResult = (JumpLoaderResult) obj;
                                    StringBuilder sb = new StringBuilder("Redirection done...   code: ");
                                    sb.append(jumpLoaderResult.getCode());
                                    g.b("Mintegral SDK M", sb.toString());
                                    a.this.s = true;
                                    campaignEx3.setJumpResult(jumpLoaderResult);
                                    a.this.a(campaignEx3, jumpLoaderResult, z3, a.this.v, bool2);
                                    if (jumpLoaderResult.isjumpDone()) {
                                        d.a((h) a.this.l).a(campaignEx3, a.this.i);
                                    }
                                    new Handler(Looper.getMainLooper()).post(new Runnable(z5, campaignEx3) {
                                        public final void run() {
                                            if (r3 && !a.f2646a && MIntegralConstans.NATIVE_SHOW_LOADINGPAGER) {
                                                a.g(a.this);
                                            }
                                            if (a.this.q != null && !a.f2646a && MIntegralConstans.NATIVE_SHOW_LOADINGPAGER) {
                                                a.this.q.onDismissLoading(r4);
                                            }
                                        }
                                    });
                                }
                            }

                            public final void a(Object obj, String str) {
                                if (a.f != null) {
                                    a.f.remove(campaignEx3.getId());
                                }
                                if (obj != null && (obj instanceof JumpLoaderResult)) {
                                    a.this.s = true;
                                    a.this.a((JumpLoaderResult) obj, campaignEx3, true);
                                }
                                if (a.this.q != null) {
                                    a.this.q.onRedirectionFailed(campaignEx3, str);
                                }
                                new Handler(Looper.getMainLooper()).post(new Runnable(z5, campaignEx3) {
                                    public final void run() {
                                        if (r3 && !a.f2646a && MIntegralConstans.NATIVE_SHOW_LOADINGPAGER) {
                                            a.g(a.this);
                                        }
                                        if (a.this.q != null && !a.f2646a && MIntegralConstans.NATIVE_SHOW_LOADINGPAGER) {
                                            a.this.q.onDismissLoading(r4);
                                        }
                                    }
                                });
                            }
                        };
                        commonJumpLoader.a("1", str, campaignEx, r1, null);
                    } else {
                        if (this.q != null) {
                            this.q.onDismissLoading(campaignEx);
                            this.q.onFinishRedirection(campaignEx, campaignEx.getClickURL());
                        }
                        g.b("Mintegral SDK M", "点击正在tracking");
                        return;
                    }
                } else {
                    campaignEx.setJumpResult(b2);
                    if (z3) {
                        if (!z4) {
                            a(campaignEx, b2, z3, this.v, bool);
                        }
                        this.y = true;
                        this.v = false;
                    }
                    g.b("Mintegral SDK M", "Start 302 Redirection... ");
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        public final void run() {
                            if (z2 && !a.f2646a && !a.this.y && MIntegralConstans.NATIVE_SHOW_LOADINGPAGER && !z4) {
                                a.a(a.this, campaignEx);
                            }
                            if (!z2 && a.this.q != null && !a.f2646a && !a.this.y && MIntegralConstans.NATIVE_SHOW_LOADINGPAGER) {
                                a.this.q.onShowLoading(campaignEx);
                            }
                        }
                    });
                    if (this.n != null) {
                    }
                    if (f != null) {
                    }
                    f.add(campaignEx.getId());
                    this.n = new CommonJumpLoader(this.m, false);
                    CommonJumpLoader commonJumpLoader2 = this.n;
                    String str2 = this.i;
                    final CampaignEx campaignEx32 = campaignEx;
                    final Boolean bool22 = bool;
                    final int i32 = i2;
                    final boolean z52 = z2;
                    AnonymousClass8 r12 = new d() {
                        public final void a(Object obj) {
                            if (a.f != null) {
                                a.f.remove(campaignEx32.getId());
                            }
                            if (obj != null && (obj instanceof JumpLoaderResult)) {
                                JumpLoaderResult jumpLoaderResult = (JumpLoaderResult) obj;
                                StringBuilder sb = new StringBuilder("Redirection done...   code: ");
                                sb.append(jumpLoaderResult.getCode());
                                g.b("Mintegral SDK M", sb.toString());
                                a.this.s = true;
                                campaignEx32.setJumpResult(jumpLoaderResult);
                                a.this.a(campaignEx32, jumpLoaderResult, z3, a.this.v, bool22);
                                if (jumpLoaderResult.isjumpDone()) {
                                    d.a((h) a.this.l).a(campaignEx32, a.this.i);
                                }
                                new Handler(Looper.getMainLooper()).post(new Runnable(z52, campaignEx32) {
                                    public final void run() {
                                        if (r3 && !a.f2646a && MIntegralConstans.NATIVE_SHOW_LOADINGPAGER) {
                                            a.g(a.this);
                                        }
                                        if (a.this.q != null && !a.f2646a && MIntegralConstans.NATIVE_SHOW_LOADINGPAGER) {
                                            a.this.q.onDismissLoading(r4);
                                        }
                                    }
                                });
                            }
                        }

                        public final void a(Object obj, String str) {
                            if (a.f != null) {
                                a.f.remove(campaignEx32.getId());
                            }
                            if (obj != null && (obj instanceof JumpLoaderResult)) {
                                a.this.s = true;
                                a.this.a((JumpLoaderResult) obj, campaignEx32, true);
                            }
                            if (a.this.q != null) {
                                a.this.q.onRedirectionFailed(campaignEx32, str);
                            }
                            new Handler(Looper.getMainLooper()).post(new Runnable(z52, campaignEx32) {
                                public final void run() {
                                    if (r3 && !a.f2646a && MIntegralConstans.NATIVE_SHOW_LOADINGPAGER) {
                                        a.g(a.this);
                                    }
                                    if (a.this.q != null && !a.f2646a && MIntegralConstans.NATIVE_SHOW_LOADINGPAGER) {
                                        a.this.q.onDismissLoading(r4);
                                    }
                                }
                            });
                        }
                    };
                    commonJumpLoader2.a("1", str2, campaignEx, r12, null);
                }
                z3 = false;
                g.b("Mintegral SDK M", "Start 302 Redirection... ");
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    public final void run() {
                        if (z2 && !a.f2646a && !a.this.y && MIntegralConstans.NATIVE_SHOW_LOADINGPAGER && !z4) {
                            a.a(a.this, campaignEx);
                        }
                        if (!z2 && a.this.q != null && !a.f2646a && !a.this.y && MIntegralConstans.NATIVE_SHOW_LOADINGPAGER) {
                            a.this.q.onShowLoading(campaignEx);
                        }
                    }
                });
                if (this.n != null) {
                }
                if (f != null) {
                }
                f.add(campaignEx.getId());
                this.n = new CommonJumpLoader(this.m, false);
                CommonJumpLoader commonJumpLoader22 = this.n;
                String str22 = this.i;
                final CampaignEx campaignEx322 = campaignEx;
                final Boolean bool222 = bool;
                final int i322 = i2;
                final boolean z522 = z2;
                AnonymousClass8 r122 = new d() {
                    public final void a(Object obj) {
                        if (a.f != null) {
                            a.f.remove(campaignEx322.getId());
                        }
                        if (obj != null && (obj instanceof JumpLoaderResult)) {
                            JumpLoaderResult jumpLoaderResult = (JumpLoaderResult) obj;
                            StringBuilder sb = new StringBuilder("Redirection done...   code: ");
                            sb.append(jumpLoaderResult.getCode());
                            g.b("Mintegral SDK M", sb.toString());
                            a.this.s = true;
                            campaignEx322.setJumpResult(jumpLoaderResult);
                            a.this.a(campaignEx322, jumpLoaderResult, z3, a.this.v, bool222);
                            if (jumpLoaderResult.isjumpDone()) {
                                d.a((h) a.this.l).a(campaignEx322, a.this.i);
                            }
                            new Handler(Looper.getMainLooper()).post(new Runnable(z522, campaignEx322) {
                                public final void run() {
                                    if (r3 && !a.f2646a && MIntegralConstans.NATIVE_SHOW_LOADINGPAGER) {
                                        a.g(a.this);
                                    }
                                    if (a.this.q != null && !a.f2646a && MIntegralConstans.NATIVE_SHOW_LOADINGPAGER) {
                                        a.this.q.onDismissLoading(r4);
                                    }
                                }
                            });
                        }
                    }

                    public final void a(Object obj, String str) {
                        if (a.f != null) {
                            a.f.remove(campaignEx322.getId());
                        }
                        if (obj != null && (obj instanceof JumpLoaderResult)) {
                            a.this.s = true;
                            a.this.a((JumpLoaderResult) obj, campaignEx322, true);
                        }
                        if (a.this.q != null) {
                            a.this.q.onRedirectionFailed(campaignEx322, str);
                        }
                        new Handler(Looper.getMainLooper()).post(new Runnable(z522, campaignEx322) {
                            public final void run() {
                                if (r3 && !a.f2646a && MIntegralConstans.NATIVE_SHOW_LOADINGPAGER) {
                                    a.g(a.this);
                                }
                                if (a.this.q != null && !a.f2646a && MIntegralConstans.NATIVE_SHOW_LOADINGPAGER) {
                                    a.this.q.onDismissLoading(r4);
                                }
                            }
                        });
                    }
                };
                commonJumpLoader22.a("1", str22, campaignEx, r122, null);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public final void a(Campaign campaign, String str) {
        try {
            if (!TextUtils.isEmpty(str)) {
                if (campaign != null) {
                    CampaignEx campaignEx = null;
                    if (campaign != null && (campaign instanceof CampaignEx)) {
                        campaignEx = (CampaignEx) campaign;
                    }
                    if (!str.startsWith("market://")) {
                        if (!str.startsWith("https://play.google.com/")) {
                            MIntegralConstans.ALLOW_APK_DOWNLOAD = com.mintegral.msdk.b.a.aV();
                            if (str.toLowerCase().endsWith(".apk") && !MIntegralConstans.ALLOW_APK_DOWNLOAD) {
                                if (campaignEx != null && !TextUtils.isEmpty(campaignEx.getPackageName())) {
                                    Context context = this.m;
                                    StringBuilder sb = new StringBuilder("market://details?id=");
                                    sb.append(campaignEx.getPackageName());
                                    if (!com.mintegral.msdk.base.utils.j.a.a(context, sb.toString())) {
                                        try {
                                            this.x.post(new Runnable() {
                                                public final void run() {
                                                    Toast.makeText(a.this.m, "Opps!Access Unavailable", 0).show();
                                                }
                                            });
                                            return;
                                        } catch (Exception unused) {
                                            g.d("Mintegral SDK M", "Opps!Access Unavailable.");
                                            return;
                                        }
                                    } else if (MIntegralConstans.ALLOW_APK_DOWNLOAD) {
                                        a(campaignEx, str, true);
                                        return;
                                    }
                                } else if (MIntegralConstans.ALLOW_APK_DOWNLOAD) {
                                    a(campaignEx, str, true);
                                }
                                return;
                            }
                            return;
                        }
                    }
                    if (!com.mintegral.msdk.base.utils.j.a.a(this.m, str) && campaignEx != null) {
                        if (!TextUtils.isEmpty(campaignEx.getPackageName())) {
                            Context context2 = this.m;
                            StringBuilder sb2 = new StringBuilder("market://details?id=");
                            sb2.append(campaignEx.getPackageName());
                            com.mintegral.msdk.base.utils.j.a.a(context2, sb2.toString());
                        } else if (e() == 2) {
                            j.a(this.m, campaignEx.getClickURL(), campaignEx);
                        } else {
                            j.b(this.m, campaignEx.getClickURL());
                        }
                    }
                    StringBuilder sb3 = new StringBuilder("Jump to Google Play: ");
                    sb3.append(str);
                    g.b("Mintegral SDK M", sb3.toString());
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    public void a(JumpLoaderResult jumpLoaderResult, CampaignEx campaignEx, boolean z) {
        if (this.s && campaignEx != null && jumpLoaderResult != null) {
            try {
                this.k = System.currentTimeMillis() - this.j;
                com.mintegral.msdk.base.entity.b bVar = new com.mintegral.msdk.base.entity.b();
                int p2 = c.p(this.m);
                bVar.a(p2);
                bVar.a(c.a(this.m, p2));
                bVar.j(campaignEx.getRequestIdNotice());
                bVar.d(1);
                StringBuilder sb = new StringBuilder();
                sb.append(this.k);
                bVar.i(sb.toString());
                bVar.h(campaignEx.getId());
                bVar.f(jumpLoaderResult.getType());
                if (!TextUtils.isEmpty(jumpLoaderResult.getUrl())) {
                    bVar.g(URLEncoder.encode(jumpLoaderResult.getUrl(), "utf-8"));
                }
                StringBuilder sb2 = new StringBuilder();
                sb2.append(this.j / 1000);
                bVar.c(sb2.toString());
                bVar.b(Integer.parseInt(campaignEx.getLandingType()));
                bVar.c(campaignEx.getLinkType());
                bVar.b(this.i);
                if (jumpLoaderResult != null) {
                    bVar.f(jumpLoaderResult.getType());
                    if (!TextUtils.isEmpty(jumpLoaderResult.getUrl())) {
                        bVar.g(URLEncoder.encode(jumpLoaderResult.getUrl(), "utf-8"));
                    }
                    if (this.u) {
                        bVar.e(jumpLoaderResult.getStatusCode());
                        if (!TextUtils.isEmpty(jumpLoaderResult.getHeader())) {
                            bVar.e(URLEncoder.encode(jumpLoaderResult.getHeader(), "utf-8"));
                        }
                        if (!TextUtils.isEmpty(jumpLoaderResult.getContent())) {
                            bVar.f(URLEncoder.encode(jumpLoaderResult.getContent(), "UTF-8"));
                        }
                        if (!TextUtils.isEmpty(jumpLoaderResult.getExceptionMsg())) {
                            bVar.d(URLEncoder.encode(jumpLoaderResult.getExceptionMsg(), "utf-8"));
                        }
                    }
                    if (z) {
                        this.r.a(bVar, this.i);
                        return;
                    }
                    ArrayList arrayList = new ArrayList();
                    arrayList.add(bVar);
                    String a2 = com.mintegral.msdk.base.entity.b.a((List<com.mintegral.msdk.base.entity.b>) arrayList);
                    if (s.b(a2)) {
                        new b(this.m, 0).a("click_jump_success", a2, (String) null, (Frame) null);
                    }
                }
            } catch (Throwable th) {
                th.printStackTrace();
            }
        }
    }

    private void a(int i2, String str, CampaignEx campaignEx) {
        try {
            if (!TextUtils.isEmpty(str)) {
                if (i2 == 2) {
                    j.a(this.m, str, campaignEx);
                    return;
                }
                j.b(this.m, str);
            }
        } catch (Throwable th) {
            g.c("Mintegral SDK M", th.getMessage(), th);
        }
    }

    private boolean a(CampaignEx campaignEx, JumpLoaderResult jumpLoaderResult, boolean z, int i2) {
        boolean z2 = false;
        if (z) {
            try {
                int intValue = Integer.valueOf(campaignEx.getLandingType()).intValue();
                if (intValue == 1) {
                    j.b(this.m, jumpLoaderResult.getUrl());
                } else if (intValue == 2) {
                    j.a(this.m, jumpLoaderResult.getUrl(), campaignEx);
                } else if (campaignEx.getPackageName() != null) {
                    Context context = this.m;
                    StringBuilder sb = new StringBuilder("market://details?id=");
                    sb.append(campaignEx.getPackageName());
                    if (!com.mintegral.msdk.base.utils.j.a.a(context, sb.toString())) {
                        a(i2, jumpLoaderResult.getUrl(), campaignEx);
                    }
                } else {
                    a(i2, jumpLoaderResult.getUrl(), campaignEx);
                }
                z2 = true;
            } catch (Throwable th) {
                g.c("Mintegral SDK M", th.getMessage(), th);
            }
        }
        if (z2) {
            a(jumpLoaderResult, campaignEx, true);
            if (this.q != null && z) {
                this.q.onFinishRedirection(campaignEx, jumpLoaderResult.getUrl());
            }
        } else {
            a(jumpLoaderResult, campaignEx, true);
            if (this.q != null && z) {
                this.q.onRedirectionFailed(campaignEx, jumpLoaderResult.getUrl());
            }
        }
        return z2;
    }

    private boolean a(CampaignEx campaignEx, JumpLoaderResult jumpLoaderResult, boolean z) {
        boolean z2 = false;
        if (z) {
            try {
                j.b(this.m, campaignEx.getClickURL());
                z2 = true;
            } catch (Throwable th) {
                g.c("Mintegral SDK M", th.getMessage(), th);
            }
        }
        if (z2) {
            a(jumpLoaderResult, campaignEx, true);
            if (this.q != null && z) {
                this.q.onFinishRedirection(campaignEx, jumpLoaderResult.getUrl());
            }
        } else {
            a(jumpLoaderResult, campaignEx, true);
            if (this.q != null && z) {
                this.q.onRedirectionFailed(campaignEx, jumpLoaderResult.getUrl());
            }
        }
        return z2;
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x00e9, code lost:
        if (com.mintegral.msdk.base.utils.j.a.a(r11, r2.toString()) == false) goto L_0x00eb;
     */
    public void a(CampaignEx campaignEx, JumpLoaderResult jumpLoaderResult, boolean z, boolean z2, Boolean bool) {
        try {
            if (this.w) {
                MIntegralConstans.ALLOW_APK_DOWNLOAD = com.mintegral.msdk.b.a.aV();
                if (campaignEx != null) {
                    if (jumpLoaderResult != null) {
                        int e2 = e();
                        int code = jumpLoaderResult.getCode();
                        if (!bool.booleanValue()) {
                            if (!TextUtils.isEmpty(jumpLoaderResult.getUrl())) {
                                a(campaignEx, jumpLoaderResult.getUrl(), bool.booleanValue());
                            }
                            return;
                        } else if (!TextUtils.isEmpty(jumpLoaderResult.getUrl()) || !z) {
                            if (code == 1) {
                                StringBuilder sb = new StringBuilder("Jump to Google Play: ");
                                sb.append(jumpLoaderResult.getUrl());
                                g.b("Mintegral SDK M", sb.toString());
                                if (TextUtils.isEmpty(campaignEx.getPackageName()) || TextUtils.isEmpty(jumpLoaderResult.getUrl()) || !jumpLoaderResult.getUrl().contains(campaignEx.getPackageName()) || !z) {
                                    if (z) {
                                        if (!TextUtils.isEmpty(campaignEx.getPackageName())) {
                                            Context context = this.m;
                                            StringBuilder sb2 = new StringBuilder("market://details?id=");
                                            sb2.append(campaignEx.getPackageName());
                                        }
                                        a(e2, jumpLoaderResult.getUrl(), campaignEx);
                                        StringBuilder sb3 = new StringBuilder("code market This pkg is ");
                                        sb3.append(campaignEx.getPackageName());
                                        g.d("Mintegral SDK M", sb3.toString());
                                    }
                                } else if (!com.mintegral.msdk.base.utils.j.a.a(this.m, jumpLoaderResult.getUrl())) {
                                    a(e2, jumpLoaderResult.getUrl(), campaignEx);
                                }
                                if (this.q != null && z) {
                                    this.q.onFinishRedirection(campaignEx, jumpLoaderResult.getUrl());
                                }
                            } else if (code == 3) {
                                if (z) {
                                    if (MIntegralConstans.ALLOW_APK_DOWNLOAD) {
                                        StringBuilder sb4 = new StringBuilder("Jump to download: ");
                                        sb4.append(jumpLoaderResult.getUrl());
                                        g.b("Mintegral SDK M", sb4.toString());
                                        a(campaignEx, jumpLoaderResult.getUrl(), bool.booleanValue());
                                    } else {
                                        j.b(this.m, jumpLoaderResult.getUrl());
                                    }
                                }
                                if (this.q != null && z) {
                                    this.q.onFinishRedirection(campaignEx, jumpLoaderResult.getUrl());
                                }
                            } else if (z) {
                                StringBuilder sb5 = new StringBuilder("Jump to Web: ");
                                sb5.append(jumpLoaderResult.getUrl());
                                g.b("Mintegral SDK M", sb5.toString());
                                if (3 == campaignEx.getLinkType()) {
                                    a(campaignEx, jumpLoaderResult, z);
                                    return;
                                } else if (2 == campaignEx.getLinkType()) {
                                    a(campaignEx, jumpLoaderResult, z, e());
                                    return;
                                } else {
                                    j.b(this.m, jumpLoaderResult.getUrl());
                                    if (this.q != null && z) {
                                        this.q.onFinishRedirection(campaignEx, jumpLoaderResult.getUrl());
                                    }
                                }
                            }
                            if (a(campaignEx.getLinkType(), jumpLoaderResult.getUrl())) {
                                a(jumpLoaderResult, campaignEx, false);
                            } else {
                                a(jumpLoaderResult, campaignEx, true);
                            }
                            if (this.q != null && !z && z2) {
                                this.q.onFinishRedirection(campaignEx, jumpLoaderResult.getUrl());
                            }
                            return;
                        } else {
                            int linkType = campaignEx.getLinkType();
                            if (linkType == 2) {
                                a(campaignEx, jumpLoaderResult, z, e());
                                return;
                            } else if (linkType == 3) {
                                a(campaignEx, jumpLoaderResult, z);
                                return;
                            } else {
                                j.b(this.m, campaignEx.getClickURL());
                                a(jumpLoaderResult, campaignEx, true);
                                if (this.q != null && z) {
                                    this.q.onFinishRedirection(campaignEx, jumpLoaderResult.getUrl());
                                }
                                return;
                            }
                        }
                    }
                }
                if (z) {
                    a(jumpLoaderResult, campaignEx, true);
                    if (this.q != null && z) {
                        this.q.onRedirectionFailed(null, null);
                    }
                }
            }
        } catch (Exception e3) {
            e3.printStackTrace();
        }
    }

    private int e() {
        try {
            if (this.t != null) {
                return this.t.ad();
            }
            return 1;
        } catch (Exception e2) {
            e2.printStackTrace();
            return 1;
        }
    }

    private static boolean a(int i2, String str) {
        if (i2 == 2) {
            try {
                if (com.mintegral.msdk.base.utils.j.a.a(str)) {
                    return true;
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        } else if (!TextUtils.isEmpty(str)) {
            return true;
        }
        return false;
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(5:25|26|27|28|29) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:28:0x00e7 */
    private void a(final CampaignEx campaignEx, final String str, final boolean z) {
        try {
            String obj = r.b(com.mintegral.msdk.base.controller.a.d().h(), str, "").toString();
            if (!TextUtils.isEmpty(obj)) {
                File file = new File(obj);
                if (file.exists()) {
                    if (z) {
                        b.a(this.m, Uri.fromFile(file), str);
                    }
                    return;
                }
            } else {
                Context h2 = com.mintegral.msdk.base.controller.a.d().h();
                StringBuilder sb = new StringBuilder();
                sb.append(str);
                sb.append("process");
                int intValue = ((Integer) r.b(h2, sb.toString(), Integer.valueOf(0))).intValue();
                int myPid = Process.myPid();
                if (intValue != 0 && intValue == myPid) {
                    Context h3 = com.mintegral.msdk.base.controller.a.d().h();
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append(str);
                    sb2.append("isDowning");
                    long longValue = ((Long) r.b(h3, sb2.toString(), Long.valueOf(0))).longValue();
                    long currentTimeMillis = System.currentTimeMillis() - longValue;
                    if (longValue != 0 && currentTimeMillis < 600000) {
                        if (z) {
                            Context h4 = com.mintegral.msdk.base.controller.a.d().h();
                            StringBuilder sb3 = new StringBuilder();
                            sb3.append(str);
                            sb3.append("downloadType");
                            if (((Integer) r.b(h4, sb3.toString(), Integer.valueOf(-1))).intValue() == 1) {
                                b(campaignEx, this.m, "downloading");
                                return;
                            }
                            a(campaignEx, this.m, "downloading");
                        }
                        return;
                    }
                }
            }
            if (t.f2622a == -1) {
                Class.forName("com.mintegral.msdk.mtgdownload.b");
                Class.forName("com.mintegral.msdk.mtgdownload.g");
                t.f2622a = 1;
                t.f2622a = 0;
            }
            if (t.f2622a != 1 || !z) {
                b(campaignEx, str, z);
                return;
            }
            j.a(str, 1, campaignEx);
            Context h5 = com.mintegral.msdk.base.controller.a.d().h();
            boolean a2 = t.a(h5);
            boolean c2 = t.c(h5);
            if (!t.b(h5)) {
                b.b(h5, str);
            } else if (!c2) {
                b(campaignEx, str, z);
            } else if (!a2) {
                b(campaignEx, str, z);
            } else {
                Context h6 = com.mintegral.msdk.base.controller.a.d().h();
                StringBuilder sb4 = new StringBuilder();
                sb4.append(str);
                sb4.append("isDowning");
                r.a(h6, sb4.toString(), Long.valueOf(System.currentTimeMillis()));
                Context h7 = com.mintegral.msdk.base.controller.a.d().h();
                StringBuilder sb5 = new StringBuilder();
                sb5.append(str);
                sb5.append("process");
                r.a(h7, sb5.toString(), Integer.valueOf(Process.myPid()));
                Class cls = Class.forName("com.mintegral.msdk.mtgdownload.g");
                Object newInstance = cls.getConstructor(new Class[]{Context.class, String.class}).newInstance(new Object[]{com.mintegral.msdk.base.controller.a.d().h(), str});
                cls.getMethod("setTitle", new Class[]{String.class}).invoke(newInstance, new Object[]{campaignEx.getAppName()});
                cls.getMethod("setDownloadListener", new Class[]{IDownloadListener.class}).invoke(newInstance, new Object[]{new IDownloadListener() {
                    public final void onProgressUpdate(int i) {
                    }

                    public final void onStatus(int i) {
                    }

                    public final void onStart() {
                        a.b(campaignEx, a.this.m, "start");
                        if (a.this.q != null) {
                            a.this.q.onDownloadStart(null);
                        }
                    }

                    public final void onEnd(int i, int i2, String str) {
                        StringBuilder sb = new StringBuilder("download listener onEnd result = ");
                        sb.append(i);
                        sb.append(" nid = ");
                        sb.append(i2);
                        sb.append(" file = ");
                        sb.append(str);
                        sb.append("-sdkclick:");
                        sb.append(z);
                        g.b("Mintegral SDK M", sb.toString());
                        Context h = com.mintegral.msdk.base.controller.a.d().h();
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append(str);
                        sb2.append("isDowning");
                        r.a(h, sb2.toString(), Long.valueOf(0));
                        Context h2 = com.mintegral.msdk.base.controller.a.d().h();
                        StringBuilder sb3 = new StringBuilder();
                        sb3.append(str);
                        sb3.append("process");
                        r.a(h2, sb3.toString(), Integer.valueOf(0));
                        if (!TextUtils.isEmpty(str)) {
                            a.b(campaignEx, a.this.m, TtmlNode.END);
                            com.mintegral.msdk.base.b.g.b(i.a(com.mintegral.msdk.base.controller.a.d().h())).a(campaignEx);
                            File file = new File(str);
                            if (file.exists() && z) {
                                b.a(a.this.m, Uri.fromFile(file), str);
                                r.a(com.mintegral.msdk.base.controller.a.d().h(), str, str);
                            } else if (!z) {
                                r.a(com.mintegral.msdk.base.controller.a.d().h(), str, str);
                            }
                        }
                        if (a.this.q != null && i == 1) {
                            a.this.q.onDownloadFinish(campaignEx);
                        }
                    }
                }});
                cls.getMethod("start", new Class[0]).invoke(newInstance, new Object[0]);
            }
        } catch (Throwable unused) {
            t.f2622a = -1;
            g.b("downloadapk", "can't find download jar, use simple method");
            b(campaignEx, str, z);
        }
    }

    public static void a(CampaignEx campaignEx, Context context, String str) {
        try {
            if (str.equals("start") || str.equals("downloading")) {
                if (Looper.myLooper() == Looper.getMainLooper()) {
                    f();
                } else {
                    Message obtainMessage = g.obtainMessage(1);
                    obtainMessage.obj = context;
                    g.sendMessage(obtainMessage);
                }
            }
            c(campaignEx, context, str);
        } catch (Throwable th) {
            g.c("Mintegral SDK M", th.getMessage(), th);
        }
    }

    private static void c(CampaignEx campaignEx, Context context, String str) {
        if (campaignEx != null) {
            try {
                if (campaignEx.getNativeVideoTracking() != null) {
                    int i2 = 0;
                    if (!str.equals("start")) {
                        if (!str.equals("shortcuts_start")) {
                            if (str.equals(TtmlNode.END)) {
                                if (campaignEx.getNativeVideoTracking().d() != null) {
                                    while (i2 < campaignEx.getNativeVideoTracking().d().length) {
                                        a(context, campaignEx, campaignEx.getCampaignUnitId(), campaignEx.getNativeVideoTracking().d()[i2], false, false);
                                        i2++;
                                    }
                                    return;
                                }
                            } else if (str.equals("install")) {
                                if (campaignEx.getNativeVideoTracking().e() != null) {
                                    while (i2 < campaignEx.getNativeVideoTracking().e().length) {
                                        a(context, campaignEx, campaignEx.getCampaignUnitId(), campaignEx.getNativeVideoTracking().e()[i2], false, false);
                                        i2++;
                                    }
                                }
                                com.mintegral.msdk.base.b.g.b(i.a(context)).m(campaignEx.getPackageName());
                            }
                        }
                    }
                    if (campaignEx.getNativeVideoTracking().c() != null) {
                        while (i2 < campaignEx.getNativeVideoTracking().c().length) {
                            a(context, campaignEx, campaignEx.getCampaignUnitId(), campaignEx.getNativeVideoTracking().c()[i2], false, false);
                            i2++;
                        }
                    }
                }
            } catch (Throwable th) {
                g.c("Mintegral SDK M", th.getMessage(), th);
            }
        }
    }

    public static void b(CampaignEx campaignEx, Context context, String str) {
        try {
            if (str.equals("start") || str.equals("downloading")) {
                if (Looper.myLooper() == Looper.getMainLooper()) {
                    a(context);
                } else {
                    Message obtainMessage = g.obtainMessage(0);
                    obtainMessage.obj = context;
                    g.sendMessage(obtainMessage);
                }
            }
            c(campaignEx, context, str);
        } catch (Throwable th) {
            g.c("Mintegral SDK M", th.getMessage(), th);
        }
    }

    public static void a(Context context) {
        com.mintegral.msdk.b.b.a();
        com.mintegral.msdk.b.a b2 = com.mintegral.msdk.b.b.b(com.mintegral.msdk.base.controller.a.d().j());
        if (b2 != null) {
            if (com.mintegral.msdk.base.controller.a.d().h() != null || context == null) {
                Toast.makeText(com.mintegral.msdk.base.controller.a.d().h(), b2.V(), 0).show();
            } else {
                Toast.makeText(context, b2.V(), 0).show();
            }
        }
    }

    /* access modifiers changed from: private */
    public static void f() {
        try {
            String language = Locale.getDefault().getLanguage();
            if (TextUtils.isEmpty(language) || !language.equals("zh")) {
                Toast.makeText(com.mintegral.msdk.base.controller.a.d().h(), "Downloading....", 0).show();
            } else {
                Toast.makeText(com.mintegral.msdk.base.controller.a.d().h(), "正在下载中,请稍候...", 0).show();
            }
        } catch (Exception unused) {
        }
    }

    private void b(final CampaignEx campaignEx, final String str, final boolean z) {
        try {
            j.a(str, 2, campaignEx);
            Context h2 = com.mintegral.msdk.base.controller.a.d().h();
            if (!t.b(h2)) {
                b.b(h2, str);
                return;
            }
            Context h3 = com.mintegral.msdk.base.controller.a.d().h();
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append("isDowning");
            r.a(h3, sb.toString(), Long.valueOf(System.currentTimeMillis()));
            Context h4 = com.mintegral.msdk.base.controller.a.d().h();
            StringBuilder sb2 = new StringBuilder();
            sb2.append(str);
            sb2.append("process");
            r.a(h4, sb2.toString(), Integer.valueOf(Process.myPid()));
            new Thread(new Runnable() {
                public final void run() {
                    a.a(a.this, campaignEx, str, z);
                }
            }).start();
        } catch (Throwable th) {
            if (MIntegralConstans.DEBUG) {
                th.printStackTrace();
            }
        }
    }

    static /* synthetic */ void a(a aVar, CampaignEx campaignEx) {
        try {
            Intent intent = new Intent(aVar.m, LoadingActivity.class);
            intent.setFlags(268435456);
            intent.putExtra(CampaignEx.JSON_KEY_ICON_URL, campaignEx.getIconUrl());
            aVar.m.startActivity(intent);
        } catch (Exception e2) {
            g.c("Mintegral SDK M", "Exception", e2);
        }
    }

    static /* synthetic */ void g(a aVar) {
        try {
            Intent intent = new Intent();
            intent.setAction("ExitApp");
            aVar.m.sendBroadcast(intent);
        } catch (Exception e2) {
            g.c("Mintegral SDK M", "Exception", e2);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:60:0x0175 A[Catch:{ all -> 0x019b }] */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x0185 A[SYNTHETIC, Splitter:B:63:0x0185] */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x0190 A[SYNTHETIC, Splitter:B:68:0x0190] */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x01a0 A[SYNTHETIC, Splitter:B:77:0x01a0] */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x01ab A[SYNTHETIC, Splitter:B:82:0x01ab] */
    static /* synthetic */ void a(a aVar, CampaignEx campaignEx, String str, boolean z) {
        OutputStream outputStream;
        InputStream inputStream;
        Throwable th;
        int i2;
        a aVar2 = aVar;
        CampaignEx campaignEx2 = campaignEx;
        String str2 = str;
        g.d("Mintegral SDK M", "CommonclickControl    simple");
        OutputStream outputStream2 = null;
        try {
            File a2 = e.a("/apk", aVar2.m, new boolean[1]);
            String a3 = b.a(str);
            StringBuilder sb = new StringBuilder();
            sb.append(a3);
            sb.append(".apk");
            File file = new File(a2, sb.toString());
            if (file.exists()) {
                file.delete();
            }
            int i3 = 0;
            aVar2.x.sendMessage(aVar2.x.obtainMessage(0));
            if (z) {
                a(campaignEx2, aVar2.m, "start");
            } else {
                a(campaignEx2, aVar2.m, "shortcuts_start");
            }
            URLConnection openConnection = new URL(str2).openConnection();
            openConnection.setConnectTimeout(8000);
            int contentLength = openConnection.getContentLength();
            double d2 = (double) contentLength;
            Double.isNaN(d2);
            double d3 = 100.0d / d2;
            inputStream = openConnection.getInputStream();
            try {
                byte[] bArr = new byte[1024];
                outputStream = new FileOutputStream(file, true);
                int i4 = 0;
                int i5 = 0;
                while (true) {
                    try {
                        int read = inputStream.read(bArr);
                        if (read == -1) {
                            break;
                        }
                        outputStream.write(bArr, i3, read);
                        i4 += read;
                        File file2 = file;
                        double d4 = (double) i4;
                        Double.isNaN(d4);
                        int i6 = (int) (d4 * d3);
                        if (i5 >= 512 || i6 == 100) {
                            i2 = 1;
                            Message obtainMessage = aVar2.x.obtainMessage(1);
                            obtainMessage.arg1 = i6;
                            aVar2.x.sendMessage(obtainMessage);
                            i5 = 0;
                        } else {
                            i2 = 1;
                        }
                        i5 += i2;
                        file = file2;
                        i3 = 0;
                    } catch (Throwable th2) {
                        th = th2;
                        if (outputStream != null) {
                            try {
                                outputStream.close();
                            } catch (IOException e2) {
                                e2.printStackTrace();
                            }
                        }
                        if (inputStream != null) {
                            try {
                                inputStream.close();
                            } catch (IOException e3) {
                                e3.printStackTrace();
                            }
                        }
                        throw th;
                    }
                }
                File file3 = file;
                if (i4 == contentLength) {
                    Context h2 = com.mintegral.msdk.base.controller.a.d().h();
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append(str2);
                    sb2.append("isDowning");
                    r.a(h2, sb2.toString(), Long.valueOf(0));
                    Context h3 = com.mintegral.msdk.base.controller.a.d().h();
                    StringBuilder sb3 = new StringBuilder();
                    sb3.append(str2);
                    sb3.append("process");
                    r.a(h3, sb3.toString(), Integer.valueOf(0));
                    Message obtainMessage2 = aVar2.x.obtainMessage(2);
                    obtainMessage2.obj = campaignEx2;
                    com.mintegral.msdk.base.b.g.b(i.a(com.mintegral.msdk.base.controller.a.d().h())).a(campaignEx2);
                    aVar2.x.sendMessage(obtainMessage2);
                    if (file3.exists() && z) {
                        b.a(aVar2.m, Uri.fromFile(file3), str2);
                        r.a(com.mintegral.msdk.base.controller.a.d().h(), str2, file3.getAbsolutePath());
                    } else if (!z) {
                        r.a(com.mintegral.msdk.base.controller.a.d().h(), str2, file3.getAbsolutePath());
                    }
                }
                try {
                    outputStream.close();
                } catch (IOException e4) {
                    e4.printStackTrace();
                }
                if (inputStream != null) {
                    try {
                        inputStream.close();
                    } catch (IOException e5) {
                        e5.printStackTrace();
                    }
                }
            } catch (Throwable th3) {
                th = th3;
                if (MIntegralConstans.DEBUG) {
                }
                b.b(com.mintegral.msdk.base.controller.a.d().h(), str2);
                if (outputStream2 != null) {
                }
                if (inputStream != null) {
                }
            }
        } catch (Throwable th4) {
            th = th4;
            inputStream = null;
            outputStream = null;
            if (outputStream != null) {
            }
            if (inputStream != null) {
            }
            throw th;
        }
    }
}
