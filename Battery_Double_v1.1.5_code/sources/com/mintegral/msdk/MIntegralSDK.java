package com.mintegral.msdk;

import android.app.Application;
import android.content.Context;
import com.mintegral.msdk.base.controller.authoritycontroller.AuthorityInfoBean;
import com.mintegral.msdk.base.controller.authoritycontroller.CallBackForDeveloper;
import com.mintegral.msdk.out.AdMobClickListener;
import java.util.Map;

public interface MIntegralSDK {

    public enum PLUGIN_LOAD_STATUS {
        INITIAL,
        INCOMPLETED,
        COMPLETED
    }

    boolean getConsentStatus(Context context);

    Map<String, String> getMTGConfigurationMap(String str, String str2);

    PLUGIN_LOAD_STATUS getStatus();

    void init(Map<String, String> map, Application application);

    void init(Map<String, String> map, Context context);

    void initAsync(Map<String, String> map, Application application);

    void initAsync(Map<String, String> map, Context context);

    void preload(Map<String, Object> map);

    void preloadFrame(Map<String, Object> map);

    void release();

    void reportUser(MIntegralUser mIntegralUser);

    void setAdMobClickListener(AdMobClickListener adMobClickListener);

    void setConsentStatus(Context context, int i);

    void setThirdPartyFeatures(Map<String, Object> map);

    void setUserPrivateInfoType(Context context, String str, int i);

    void showUserPrivateInfoTips(Context context, CallBackForDeveloper callBackForDeveloper);

    AuthorityInfoBean userPrivateInfo(Context context);
}
