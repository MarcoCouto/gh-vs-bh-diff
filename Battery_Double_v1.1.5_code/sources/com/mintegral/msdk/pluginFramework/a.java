package com.mintegral.msdk.pluginFramework;

import android.content.Intent;
import android.os.IBinder;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.mtgdownload.c;

/* compiled from: PluginServiceAgent */
public final class a {

    /* renamed from: a reason: collision with root package name */
    public C0063a f2905a;

    /* renamed from: com.mintegral.msdk.pluginFramework.a$a reason: collision with other inner class name */
    /* compiled from: PluginServiceAgent */
    public static class C0063a {

        /* renamed from: a reason: collision with root package name */
        c f2906a;

        public C0063a(c cVar) {
            this.f2906a = cVar;
        }

        public final IBinder a() {
            try {
                return this.f2906a.a();
            } catch (Exception e) {
                g.c("PluginServiceContext", "invoke onBind error", e);
                return null;
            }
        }

        public final int a(Intent intent) {
            try {
                return this.f2906a.a(intent);
            } catch (Exception e) {
                g.c("PluginServiceContext", "invoke onStartCommand error", e);
                return 0;
            }
        }

        public final void b() {
            try {
                this.f2906a.b();
            } catch (Exception e) {
                g.c("PluginServiceContext", "invoke onCreate error", e);
            }
        }

        public final void c() {
            try {
                this.f2906a.c();
            } catch (Exception e) {
                g.c("PluginServiceContext", "invoke onDestroy error", e);
            }
        }
    }

    public a(C0063a aVar) {
        this.f2905a = aVar;
    }
}
