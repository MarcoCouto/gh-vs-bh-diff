package com.mintegral.msdk.pluginFramework;

import android.app.Service;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.content.res.Resources.Theme;
import com.mintegral.msdk.base.utils.g;

public abstract class PluginService extends Service {

    /* renamed from: a reason: collision with root package name */
    protected a f2904a = null;

    public abstract a a();

    public ClassLoader getClassLoader() {
        return null;
    }

    public void onCreate() {
        try {
            this.f2904a = a();
            try {
                this.f2904a.f2905a.f2906a.a((Service) this);
            } catch (Exception e) {
                g.c("PluginServiceContext", "invoke onDestroy error", e);
            }
            super.onCreate();
        } catch (Exception e2) {
            g.c("Download", "", e2);
        }
    }

    public AssetManager getAssets() {
        if (this.f2904a != null) {
            return null;
        }
        return super.getAssets();
    }

    public Resources getResources() {
        if (this.f2904a != null) {
            return null;
        }
        return super.getResources();
    }

    public Theme getTheme() {
        return super.getTheme();
    }
}
