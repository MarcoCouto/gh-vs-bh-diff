package com.mintegral.msdk.playercommon;

import android.media.MediaPlayer;
import android.media.MediaPlayer.OnBufferingUpdateListener;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnInfoListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.media.MediaPlayer.OnSeekCompleteListener;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.TextUtils;
import android.view.SurfaceHolder;
import android.view.View;
import com.mintegral.msdk.base.utils.g;
import java.util.Timer;
import java.util.TimerTask;

/* compiled from: VideoFeedsPlayer */
public final class b implements OnBufferingUpdateListener, OnCompletionListener, OnErrorListener, OnInfoListener, OnPreparedListener {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public boolean f2871a = false;
    /* access modifiers changed from: private */
    public boolean b = false;
    /* access modifiers changed from: private */
    public boolean c = false;
    /* access modifiers changed from: private */
    public boolean d = false;
    private boolean e = false;
    /* access modifiers changed from: private */
    public boolean f = true;
    private SurfaceHolder g;
    private int h = 5;
    /* access modifiers changed from: private */
    public int i;
    private Timer j;
    private Timer k;
    /* access modifiers changed from: private */
    public c l;
    /* access modifiers changed from: private */
    public c m;
    private Object n = new Object();
    private String o;
    private String p;
    /* access modifiers changed from: private */
    public MediaPlayer q;
    /* access modifiers changed from: private */
    public View r;
    /* access modifiers changed from: private */
    public View s;
    private boolean t;
    /* access modifiers changed from: private */
    public boolean u = false;
    private final Handler v = new Handler(Looper.getMainLooper()) {
        public final void handleMessage(Message message) {
            super.handleMessage(message);
        }
    };

    /* compiled from: VideoFeedsPlayer */
    private class a extends TimerTask {
        private a() {
        }

        /* synthetic */ a(b bVar, byte b) {
            this();
        }

        public final void run() {
            try {
                if (b.this.q != null && b.this.q.isPlaying()) {
                    b.this.i = b.this.q.getCurrentPosition();
                    int round = Math.round(((float) b.this.i) / 1000.0f);
                    StringBuilder sb = new StringBuilder("currentPosition:");
                    sb.append(round);
                    sb.append(" mCurrentPosition:");
                    sb.append(b.this.i);
                    g.b("VideoFeedsPlayer", sb.toString());
                    int i = 0;
                    if (b.this.q != null && b.this.q.getDuration() > 0) {
                        i = b.this.q.getDuration() / 1000;
                    }
                    if (round >= 0 && i > 0 && b.this.q.isPlaying()) {
                        b.a(b.this, round, i);
                    }
                    b.this.f2871a = false;
                    if (!b.this.d) {
                        b.this.p();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public final void onBufferingUpdate(MediaPlayer mediaPlayer, int i2) {
    }

    public final boolean a(String str, View view, c cVar) {
        boolean z;
        try {
            synchronized (this.n) {
                if (this.q == null) {
                    this.q = new MediaPlayer();
                    this.q.reset();
                } else {
                    this.q.release();
                    this.q = new MediaPlayer();
                    this.q.reset();
                }
                if (view == null) {
                    g.b("VideoFeedsPlayer", "loadingView为空");
                    b("MediaPlayer init error");
                    return false;
                }
                if (!TextUtils.isEmpty(str)) {
                    this.p = str;
                }
                this.l = cVar;
                this.r = view;
                this.q.setOnCompletionListener(this);
                this.q.setOnErrorListener(this);
                this.q.setOnPreparedListener(this);
                this.q.setOnInfoListener(this);
                this.q.setOnBufferingUpdateListener(this);
                z = true;
            }
        } catch (Throwable th) {
            th.printStackTrace();
            b(th.toString());
            z = false;
        }
        return z;
    }

    public final void a(SurfaceHolder surfaceHolder) {
        try {
            if (this.q != null) {
                this.q.setDisplay(surfaceHolder);
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public final void a(String str, int i2) {
        try {
            synchronized (this.n) {
                String str2 = "VideoFeedsPlayer";
                StringBuilder sb = new StringBuilder("进来播放 currentionPosition:");
                sb.append(this.i);
                g.d(str2, sb.toString());
                if (i2 > 0) {
                    this.i = i2;
                }
                if (TextUtils.isEmpty(str)) {
                    b("play url is null");
                    return;
                }
                this.o = str;
                this.c = false;
                this.f = true;
                a();
                e();
                StringBuilder sb2 = new StringBuilder("mPlayUrl:");
                sb2.append(this.o);
                g.b("VideoFeedsPlayer", sb2.toString());
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            f();
            p();
            b("mediaplayer cannot play");
        }
    }

    private void n() {
        try {
            if (this.j != null) {
                this.j.cancel();
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    private void o() {
        try {
            if (this.k != null) {
                this.k.cancel();
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    private void a(final String str) {
        if (!this.e) {
            g.d("VideoFeedsPlayer", "不需要缓冲超时功能");
            return;
        }
        o();
        this.k = new Timer();
        this.k.schedule(new TimerTask() {
            public final void run() {
                try {
                    if (!b.this.c || b.this.d) {
                        g.d("VideoFeedsPlayer", "缓冲超时");
                        b.a(b.this, str);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, (long) (this.h * 1000));
    }

    public final void a(int i2) {
        if (i2 > 0) {
            this.h = i2;
        }
        this.e = true;
        StringBuilder sb = new StringBuilder("mIsNeedBufferingTimeout:");
        sb.append(this.e);
        sb.append("  mMaxBufferTime:");
        sb.append(this.h);
        g.b("VideoFeedsPlayer", sb.toString());
    }

    public final void onCompletion(MediaPlayer mediaPlayer) {
        try {
            this.f2871a = true;
            this.b = false;
            this.i = 0;
            p();
            try {
                if (this.v != null) {
                    this.v.post(new Runnable() {
                        public final void run() {
                            if (b.this.l != null) {
                                b.this.l.onPlayCompleted();
                            }
                            if (b.this.m != null) {
                                b.this.m.onPlayCompleted();
                            }
                        }
                    });
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
            g.b("VideoFeedsPlayer", "======onCompletion");
        } catch (Exception e3) {
            e3.printStackTrace();
        }
    }

    public final void onPrepared(MediaPlayer mediaPlayer) {
        try {
            g.b("VideoFeedsPlayer", "进来了 onprepar listener");
            if (mediaPlayer == null || !mediaPlayer.isPlaying()) {
                StringBuilder sb = new StringBuilder("onPrepared:");
                sb.append(this.c);
                g.b("VideoFeedsPlayer", sb.toString());
                if (this.f) {
                    this.q.seekTo(this.i);
                    this.q.setOnSeekCompleteListener(new OnSeekCompleteListener() {
                        public final void onSeekComplete(MediaPlayer mediaPlayer) {
                            try {
                                if (b.this.f) {
                                    b.this.p();
                                    b.this.c = true;
                                    if (b.this.q != null) {
                                        b.this.b = true;
                                        if (!b.this.u) {
                                            b.b(b.this, b.this.q.getDuration() / 1000);
                                            StringBuilder sb = new StringBuilder("onPlayStarted()，getCurrentPosition:");
                                            sb.append(b.this.q.getCurrentPosition());
                                            g.b("VideoFeedsPlayer", sb.toString());
                                            b.this.u = true;
                                        }
                                        b.this.q.start();
                                    }
                                    b.this.q();
                                    b.m(b.this);
                                    StringBuilder sb2 = new StringBuilder("onprepare mCurrentPosition:");
                                    sb2.append(b.this.i);
                                    sb2.append(" onprepare 开始播放 mHasPrepare：");
                                    sb2.append(b.this.c);
                                    g.b("VideoFeedsPlayer", sb2.toString());
                                }
                            } catch (Throwable th) {
                                th.printStackTrace();
                            }
                        }
                    });
                    return;
                }
                g.b("VideoFeedsPlayer", "此时在后台 不做处理");
                return;
            }
            g.b("VideoFeedsPlayer", "onprepare 正在播放");
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public final void a() {
        try {
            if (this.v != null) {
                this.v.post(new Runnable() {
                    public final void run() {
                        if (b.this.s != null) {
                            b.this.s.setVisibility(0);
                        }
                    }
                });
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    public void p() {
        try {
            if (this.v != null) {
                this.v.post(new Runnable() {
                    public final void run() {
                        if (b.this.r != null) {
                            b.this.r.setVisibility(8);
                        }
                        if (b.this.s != null) {
                            b.this.s.setVisibility(8);
                        }
                    }
                });
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    public void q() {
        try {
            if (this.v != null) {
                this.v.post(new Runnable() {
                    public final void run() {
                        if (b.this.l != null) {
                            b.this.l.OnBufferingEnd();
                        }
                        if (b.this.m != null) {
                            b.this.m.OnBufferingEnd();
                        }
                    }
                });
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    private void b(final String str) {
        try {
            if (this.v != null) {
                this.v.post(new Runnable() {
                    public final void run() {
                        if (b.this.l != null) {
                            b.this.l.onPlayError(str);
                        }
                        if (b.this.m != null) {
                            b.this.m.onPlayError(str);
                        }
                    }
                });
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public final void b() {
        try {
            g.b("VideoFeedsPlayer", "player pause");
            if (this.c && this.q != null && this.q.isPlaying()) {
                StringBuilder sb = new StringBuilder("pause isPalying:");
                sb.append(this.q.isPlaying());
                sb.append(" mIsPlaying:");
                sb.append(this.b);
                g.b("VideoFeedsPlayer", sb.toString());
                p();
                this.q.pause();
                this.b = false;
            }
        } catch (Exception unused) {
        }
    }

    public final void c() {
        try {
            if (this.c && this.q != null && this.q.isPlaying()) {
                p();
                this.q.stop();
                this.c = false;
                this.b = false;
                this.f2871a = true;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public final void d() {
        try {
            if (!this.c) {
                g.b("VideoFeedsPlayer", "!mHasPrepare");
                return;
            }
            if (this.q != null && !this.q.isPlaying()) {
                a();
                this.q.start();
                this.b = true;
                g.b("VideoFeedsPlayer", "start");
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public final void b(final int i2) {
        try {
            if (this.c && this.q != null && !this.q.isPlaying()) {
                if (i2 > 0) {
                    this.q.seekTo(i2);
                    this.q.setOnSeekCompleteListener(new OnSeekCompleteListener() {
                        public final void onSeekComplete(MediaPlayer mediaPlayer) {
                            b.this.q.start();
                            b.this.b = true;
                            StringBuilder sb = new StringBuilder("==================start 指定进度 curposition:");
                            sb.append(i2);
                            g.b("VideoFeedsPlayer", sb.toString());
                        }
                    });
                    return;
                }
                this.q.start();
                this.b = true;
                g.b("VideoFeedsPlayer", "=========start 指定进度");
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public final void c(final int i2) {
        try {
            this.i = i2;
            if (!this.c) {
                g.a("VideoFeedsPlayer", "seekTo return mHasPrepare false");
                return;
            }
            if (this.q != null) {
                this.q.seekTo(i2);
                this.q.setOnSeekCompleteListener(new OnSeekCompleteListener() {
                    public final void onSeekComplete(MediaPlayer mediaPlayer) {
                        try {
                            if (!b.this.q.isPlaying()) {
                                b.this.q.start();
                                g.b("VideoFeedsPlayer", "seekTo start");
                            }
                            b.this.b = true;
                            StringBuilder sb = new StringBuilder("==================seekTo 指定进度 curposition:");
                            sb.append(i2);
                            g.b("VideoFeedsPlayer", sb.toString());
                        } catch (Throwable th) {
                            th.printStackTrace();
                        }
                    }
                });
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public final void e() {
        try {
            g.b("VideoFeedsPlayer", "setDataSource");
            if (this.q != null) {
                this.q.reset();
                this.q.setDataSource(this.o);
                if (this.g != null) {
                    this.q.setDisplay(this.g);
                }
                this.c = false;
                this.q.prepareAsync();
                a("mediaplayer prepare timeout");
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            p();
            b("illegal video address");
            final String str = "illegal video address";
            try {
                if (this.v != null) {
                    this.v.post(new Runnable() {
                        public final void run() {
                            if (b.this.m != null) {
                                b.this.m.onPlaySetDataSourceError(str);
                            }
                            if (b.this.l != null) {
                                b.this.l.onPlaySetDataSourceError(str);
                            }
                        }
                    });
                }
            } catch (Exception e3) {
                e3.printStackTrace();
            }
        }
    }

    public final void a(c cVar) {
        this.m = cVar;
    }

    public final void f() {
        try {
            g.b("VideoFeedsPlayer", "release");
            n();
            o();
            if (this.q != null) {
                c();
                this.q.release();
                this.q = null;
                this.m = null;
                this.l = null;
            }
            p();
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public final void g() {
        try {
            if (this.q != null) {
                this.q.setVolume(0.0f, 0.0f);
                this.t = true;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public final void h() {
        try {
            if (this.q != null) {
                this.q.setVolume(1.0f, 1.0f);
                this.t = false;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public final int i() {
        return this.i;
    }

    public final boolean j() {
        try {
            if (this.q != null && this.q.isPlaying()) {
                return true;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return false;
    }

    public final boolean onError(MediaPlayer mediaPlayer, int i2, int i3) {
        String str = "VideoFeedsPlayer";
        try {
            StringBuilder sb = new StringBuilder("onError what:");
            sb.append(i2);
            sb.append(" extra:");
            sb.append(i3);
            g.d(str, sb.toString());
            p();
            this.c = false;
            b("unknow error");
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return true;
    }

    public final boolean onInfo(MediaPlayer mediaPlayer, int i2, int i3) {
        String str = "VideoFeedsPlayer";
        try {
            StringBuilder sb = new StringBuilder("onInfo what:");
            sb.append(i2);
            g.d(str, sb.toString());
            switch (i2) {
                case 701:
                    StringBuilder sb2 = new StringBuilder("BUFFERING_START:");
                    sb2.append(i2);
                    g.d("VideoFeedsPlayer", sb2.toString());
                    this.d = true;
                    a();
                    a("play buffering tiemout");
                    break;
                case 702:
                    StringBuilder sb3 = new StringBuilder("BUFFERING_END:");
                    sb3.append(i2);
                    g.d("VideoFeedsPlayer", sb3.toString());
                    this.d = false;
                    p();
                    q();
                    break;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return false;
    }

    public final boolean k() {
        return this.c;
    }

    public final void a(boolean z) {
        try {
            this.f = z;
            String str = "VideoFeedsPlayer";
            StringBuilder sb = new StringBuilder("isFrontDesk:");
            sb.append(z ? "设置在前台" : "设置在后台");
            g.d(str, sb.toString());
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public final boolean l() {
        return this.f2871a;
    }

    public final boolean m() {
        return this.t;
    }

    static /* synthetic */ void a(b bVar, final int i2, final int i3) {
        try {
            if (bVar.v != null) {
                bVar.v.post(new Runnable() {
                    public final void run() {
                        if (b.this.l != null) {
                            b.this.l.onPlayProgress(i2, i3);
                        }
                        if (b.this.m != null) {
                            b.this.m.onPlayProgress(i2, i3);
                        }
                    }
                });
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    static /* synthetic */ void a(b bVar, final String str) {
        try {
            if (bVar.v != null) {
                bVar.v.post(new Runnable() {
                    public final void run() {
                        if (b.this.l != null) {
                            b.this.l.OnBufferingStart(str);
                        }
                        if (b.this.m != null) {
                            b.this.m.OnBufferingStart(str);
                        }
                    }
                });
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    static /* synthetic */ void b(b bVar, final int i2) {
        try {
            if (bVar.v != null) {
                bVar.v.post(new Runnable() {
                    public final void run() {
                        if (b.this.l != null) {
                            b.this.l.onPlayStarted(i2);
                        }
                        if (b.this.m != null) {
                            b.this.m.onPlayStarted(i2);
                        }
                    }
                });
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    static /* synthetic */ void m(b bVar) {
        try {
            bVar.n();
            bVar.j = new Timer();
            bVar.j.schedule(new a(bVar, 0), 0, 1000);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }
}
