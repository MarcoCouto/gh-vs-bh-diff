package com.mintegral.msdk.playercommon;

public final class BuildConfig {
    public static final String APPLICATION_ID = "com.mintegral.msdk.playercommon";
    public static final String BUILD_TYPE = "release";
    public static final boolean DEBUG = false;
    public static final String FLAVOR = "";
    public static final int VERSION_CODE = 1;
    public static final String VERSION_NAME = "1.0";
}
