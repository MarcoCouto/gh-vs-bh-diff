package com.mintegral.msdk.playercommon;

import android.content.Context;
import android.os.Build.VERSION;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;
import android.view.View;
import android.widget.LinearLayout;
import com.ironsource.sdk.constants.Constants.ParametersKeys;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.base.utils.p;

public class PlayerView extends LinearLayout {
    public static final String TAG = "PlayerView";

    /* renamed from: a reason: collision with root package name */
    private LinearLayout f2869a;
    private LinearLayout b;
    /* access modifiers changed from: private */
    public b c;
    private String d;
    private boolean e = false;
    /* access modifiers changed from: private */
    public boolean f = true;
    /* access modifiers changed from: private */
    public boolean g = false;
    /* access modifiers changed from: private */
    public boolean h = false;
    /* access modifiers changed from: private */
    public boolean i = false;
    /* access modifiers changed from: private */
    public SurfaceHolder j;

    private class a implements Callback {
        private a() {
        }

        /* synthetic */ a(PlayerView playerView, byte b) {
            this();
        }

        public final void surfaceCreated(SurfaceHolder surfaceHolder) {
            try {
                g.b(PlayerView.TAG, "surfaceCreated");
                if (!(PlayerView.this.c == null || surfaceHolder == null)) {
                    PlayerView.this.j = surfaceHolder;
                    PlayerView.this.c.a(surfaceHolder);
                }
                PlayerView.this.f = false;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public final void surfaceDestroyed(SurfaceHolder surfaceHolder) {
            try {
                g.b(PlayerView.TAG, "surfaceDestroyed ");
                PlayerView.this.g = true;
                PlayerView.this.i = true;
                PlayerView.this.c.b();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public final void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i2, int i3) {
            try {
                g.b(PlayerView.TAG, "surfaceChanged");
                if (PlayerView.this.g && !PlayerView.this.h && !PlayerView.this.isComplete()) {
                    if (PlayerView.this.c.k()) {
                        g.b(PlayerView.TAG, "surfaceChanged  start====");
                        PlayerView.this.resumeStar();
                    } else {
                        g.b(PlayerView.TAG, "surfaceChanged  PLAY====");
                        PlayerView.this.playVideo(0);
                    }
                }
                PlayerView.this.g = false;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static void isCompletePlay() {
    }

    public PlayerView(Context context) {
        super(context);
        a();
    }

    public PlayerView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a();
    }

    public void addSurfaceView() {
        try {
            g.b(TAG, "addSurfaceView");
            SurfaceView surfaceView = new SurfaceView(getContext());
            this.j = surfaceView.getHolder();
            this.j.setType(3);
            this.j.setKeepScreenOn(true);
            this.j.addCallback(new a(this, 0));
            this.f2869a.addView(surfaceView, -1, -1);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public void removeSurface() {
        try {
            g.b(TAG, "removeSurface");
            this.f2869a.removeAllViews();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public boolean initVFPData(String str, String str2, c cVar) {
        if (TextUtils.isEmpty(str)) {
            g.b(TAG, "playUrl==null");
            return false;
        }
        this.d = str;
        this.c.a(str2, (View) this.b, cVar);
        this.e = true;
        return true;
    }

    public void playVideo(int i2) {
        try {
            if (this.c == null) {
                g.b(TAG, "player init error 播放失败");
            } else if (!this.e) {
                g.b(TAG, "vfp init failed 播放失败");
            } else {
                this.c.a(this.d, i2);
                this.i = false;
            }
        } catch (Throwable th) {
            g.c(TAG, th.getMessage(), th);
        }
    }

    public void playVideo() {
        playVideo(0);
    }

    public void onPause() {
        try {
            pause();
            if (this.c != null) {
                this.c.a(false);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public void pause() {
        try {
            if (this.c != null) {
                this.c.b();
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public void resumeStar() {
        try {
            if (VERSION.SDK_INT >= 24) {
                setDataSource();
            } else {
                start();
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public void setDataSource() {
        try {
            if (this.c != null) {
                this.c.a();
                this.c.e();
                this.i = false;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public void start() {
        try {
            if (this.c != null) {
                this.c.d();
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public void start(int i2) {
        try {
            if (this.c != null) {
                this.c.b(i2);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public void seekTo(int i2) {
        try {
            if (this.c != null) {
                this.c.c(i2);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public void stop() {
        try {
            if (this.c != null) {
                this.c.c();
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public void openSound() {
        if (this.c != null) {
            this.c.h();
        }
    }

    public void closeSound() {
        if (this.c != null) {
            this.c.g();
        }
    }

    public void onResume() {
        try {
            this.c.a(true);
            if (this.c != null && !this.f && !this.g && !isComplete()) {
                g.b(TAG, "onresume========");
                if (this.c.k()) {
                    resumeStar();
                    return;
                }
                playVideo(0);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public void coverUnlockResume() {
        try {
            this.c.a(true);
            if (this.c != null) {
                g.b(TAG, "coverUnlockResume========");
                if (this.c.k()) {
                    if (!this.i) {
                        start();
                    }
                }
                playVideo(0);
            }
        } catch (Throwable th) {
            if (MIntegralConstans.DEBUG) {
                th.printStackTrace();
            }
        }
    }

    public void release() {
        try {
            if (this.c != null) {
                this.c.f();
            }
            if (VERSION.SDK_INT >= 14 && this.j != null) {
                g.d(TAG, "mSurfaceHolder release");
                this.j.getSurface().release();
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public int getCurPosition() {
        int i2 = 0;
        try {
            if (this.c != null) {
                i2 = this.c.i();
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        StringBuilder sb = new StringBuilder(ParametersKeys.POSITION);
        sb.append(i2);
        Log.i("jindu---->", sb.toString());
        return i2;
    }

    public boolean isComplete() {
        boolean z = false;
        try {
            if (this.c != null && this.c.l()) {
                z = true;
            }
            return z;
        } catch (Throwable th) {
            g.c(TAG, th.getMessage(), th);
            return false;
        }
    }

    public void initBufferIngParam(int i2) {
        if (this.c != null) {
            this.c.a(i2);
        }
    }

    public boolean isPlayIng() {
        try {
            if (this.c != null) {
                return this.c.j();
            }
        } catch (Throwable th) {
            if (MIntegralConstans.DEBUG) {
                th.printStackTrace();
            }
        }
        return false;
    }

    public void setIsCovered(boolean z) {
        try {
            this.h = z;
            String str = TAG;
            StringBuilder sb = new StringBuilder("mIsCovered:");
            sb.append(z);
            g.d(str, sb.toString());
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public boolean isSilent() {
        return this.c.m();
    }

    private void a() {
        try {
            View inflate = LayoutInflater.from(getContext()).inflate(p.a(getContext(), "mintegral_playercommon_player_view", TtmlNode.TAG_LAYOUT), null);
            if (inflate != null) {
                this.f2869a = (LinearLayout) inflate.findViewById(p.a(getContext(), "mintegral_playercommon_ll_sur_container", "id"));
                this.b = (LinearLayout) inflate.findViewById(p.a(getContext(), "mintegral_playercommon_ll_loading", "id"));
                addSurfaceView();
                addView(inflate, -1, -1);
            }
            this.c = new b();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }
}
