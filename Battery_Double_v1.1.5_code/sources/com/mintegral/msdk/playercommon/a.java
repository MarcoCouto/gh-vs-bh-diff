package com.mintegral.msdk.playercommon;

import com.mintegral.msdk.base.utils.g;

/* compiled from: DefaultVideoFeedsPlayerListener */
public class a implements c {
    public void onPlayStarted(int i) {
        StringBuilder sb = new StringBuilder("onPlayStarted:");
        sb.append(i);
        g.a("DefaultVideoFeedsPlayerListener", sb.toString());
    }

    public void onPlayCompleted() {
        g.a("DefaultVideoFeedsPlayerListener", "onPlayCompleted");
    }

    public void onPlayError(String str) {
        StringBuilder sb = new StringBuilder("onPlayError:");
        sb.append(str);
        g.a("DefaultVideoFeedsPlayerListener", sb.toString());
    }

    public void onPlayProgress(int i, int i2) {
        StringBuilder sb = new StringBuilder("onPlayProgress:");
        sb.append(i);
        sb.append(",allDuration:");
        sb.append(i2);
        g.a("DefaultVideoFeedsPlayerListener", sb.toString());
    }

    public void OnBufferingStart(String str) {
        StringBuilder sb = new StringBuilder("OnBufferingStart:");
        sb.append(str);
        g.a("DefaultVideoFeedsPlayerListener", sb.toString());
    }

    public void OnBufferingEnd() {
        g.a("DefaultVideoFeedsPlayerListener", "OnBufferingEnd");
    }

    public void onPlaySetDataSourceError(String str) {
        StringBuilder sb = new StringBuilder("onPlaySetDataSourceError:");
        sb.append(str);
        g.a("DefaultVideoFeedsPlayerListener", sb.toString());
    }

    public void onPlayProgressMS(int i, int i2) {
        g.a("DefaultVideoFeedsPlayerListener", "onPlayProgressMS:");
    }
}
