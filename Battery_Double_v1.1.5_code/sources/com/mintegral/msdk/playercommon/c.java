package com.mintegral.msdk.playercommon;

/* compiled from: VideoFeedsPlayerListener */
public interface c {
    void OnBufferingEnd();

    void OnBufferingStart(String str);

    void onPlayCompleted();

    void onPlayError(String str);

    void onPlayProgress(int i, int i2);

    void onPlayProgressMS(int i, int i2);

    void onPlaySetDataSourceError(String str);

    void onPlayStarted(int i);
}
