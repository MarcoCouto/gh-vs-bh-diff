package com.mintegral.msdk.rover;

import com.ironsource.sdk.constants.LocationConst;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: RoverReportData */
public final class e {

    /* renamed from: a reason: collision with root package name */
    private String f2950a;
    private int b;
    private int c;
    private int d;
    private String e;
    private String f;

    public final void a(String str) {
        this.f2950a = str;
    }

    public final void a(int i) {
        this.b = i;
    }

    public final void b(int i) {
        this.c = i;
    }

    public final void c(int i) {
        this.d = i;
    }

    public final void b(String str) {
        this.e = str;
    }

    public final void c(String str) {
        this.f = str;
    }

    public final JSONObject a() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("url", this.f2950a);
            jSONObject.put("type", this.b);
            jSONObject.put(LocationConst.TIME, this.c);
            jSONObject.put("code", this.d);
            jSONObject.put("header", this.e);
            jSONObject.put("exception", this.f);
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
        return jSONObject;
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder("");
        sb.append("url=");
        sb.append(this.f2950a);
        sb.append(", ");
        sb.append("type=");
        sb.append(this.b);
        sb.append(", ");
        sb.append("time=");
        sb.append(this.c);
        sb.append(", ");
        sb.append("code=");
        sb.append(this.d);
        sb.append(", ");
        sb.append("header=");
        sb.append(this.e);
        sb.append(", ");
        sb.append("exception=");
        sb.append(this.f);
        return sb.toString();
    }
}
