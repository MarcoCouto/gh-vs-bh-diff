package com.mintegral.msdk.rover;

import android.content.Context;
import android.os.Looper;
import android.text.TextUtils;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.base.common.d.b;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.utils.a;
import com.mintegral.msdk.click.CommonJumpLoader;
import com.mintegral.msdk.click.d;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* compiled from: RoverDoing */
public final class c {
    private static String b = "0";

    /* renamed from: a reason: collision with root package name */
    d f2946a = new d() {
        public final void a(String str, int i, int i2, String str2, String str3) {
            c.a(c.this, str, i, i2, 0, str2, str3);
        }

        public final void a(String str, int i, int i2, int i3, String str2, String str3) {
            c.a(c.this, str, i, i2, i3, str2, str3);
        }

        public final void b(String str, int i, int i2, int i3, String str2, String str3) {
            c.a(c.this, str, i, i2, i3, str2, str3);
        }
    };
    private List<e> c = new ArrayList();
    /* access modifiers changed from: private */
    public CampaignEx d;
    private Context e;
    private Map<String, String> f = new HashMap();

    public c(CampaignEx campaignEx, Context context) {
        CommonJumpLoader commonJumpLoader = new CommonJumpLoader(context, true);
        this.d = campaignEx;
        this.e = context;
        commonJumpLoader.a("2", b, campaignEx, new d() {
            public final void a(Object obj) {
                if (c.this.d != null && c.this.d.getRoverIsPost() == 1) {
                    c.b(c.this);
                }
            }

            public final void a(Object obj, String str) {
                if (c.this.d != null && c.this.d.getRoverIsPost() == 1) {
                    c.b(c.this);
                }
            }
        }, this.f2946a);
    }

    static /* synthetic */ void a(c cVar, String str, int i, int i2, int i3, String str2, String str3) {
        String str4;
        try {
            if (cVar.f == null) {
                cVar.f = new HashMap();
            }
            if (!cVar.f.containsKey(str)) {
                cVar.f.put(str, null);
                if (TextUtils.isEmpty(str) || !str.contains("data:*/*;charset=utf-8;base64")) {
                    e eVar = new e();
                    eVar.a(str);
                    eVar.a(i);
                    eVar.b(i2);
                    eVar.c(i3);
                    eVar.b(a.b(str2));
                    if (str3 == null) {
                        str4 = "";
                    } else {
                        str4 = a.b(str3);
                    }
                    eVar.c(str4);
                    if (cVar.c != null) {
                        cVar.c.add(eVar);
                        return;
                    }
                    cVar.c = new ArrayList();
                    cVar.c.add(eVar);
                }
            }
        } catch (Exception e2) {
            if (MIntegralConstans.DEBUG) {
                e2.printStackTrace();
            }
        }
    }

    static /* synthetic */ void b(c cVar) {
        if (cVar.e != null) {
            b bVar = new b(cVar.e);
            AnonymousClass2 r1 = new f() {
            };
            if (Looper.myLooper() != Looper.getMainLooper()) {
                Looper.prepare();
            }
            bVar.a(cVar.d, cVar.c, r1);
            if (Looper.myLooper() != Looper.getMainLooper()) {
                Looper.loop();
            }
        }
    }
}
