package com.mintegral.msdk.rover;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import com.mintegral.msdk.b.a;
import com.mintegral.msdk.base.common.net.d;
import com.mintegral.msdk.base.common.net.l;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.base.utils.r;
import java.util.ArrayList;

/* compiled from: RoverController */
public class b {
    private static b d;

    /* renamed from: a reason: collision with root package name */
    Context f2943a;
    long b = 259200000;
    private Handler c = new Handler() {
        public final void handleMessage(Message message) {
            b.a(b.this);
        }
    };

    private b() {
    }

    public final void a(Context context) {
        this.f2943a = context;
    }

    public static b a() {
        if (d == null) {
            synchronized (b.class) {
                if (d == null) {
                    d = new b();
                }
            }
        }
        return d;
    }

    public final void b() {
        if (this.f2943a == null) {
            g.d("RoverController", "Context is null");
            return;
        }
        if (c()) {
            Object b2 = r.b(this.f2943a, a.f, Long.valueOf(0));
            long longValue = b2 instanceof Long ? ((Long) b2).longValue() : 1;
            long currentTimeMillis = System.currentTimeMillis();
            int i = a.e;
            com.mintegral.msdk.b.b.a();
            a b3 = com.mintegral.msdk.b.b.b(com.mintegral.msdk.base.controller.a.d().j());
            if (b3 != null && b3.ac() > 0) {
                i = (int) b3.ac();
            }
            if (currentTimeMillis - longValue > ((long) i) && longValue != 1) {
                this.c.sendEmptyMessageDelayed(0, 30000);
            }
        }
    }

    private boolean c() {
        boolean z = false;
        if (this.f2943a != null) {
            try {
                long j = this.f2943a.getPackageManager().getPackageInfo(this.f2943a.getPackageName(), 0).lastUpdateTime;
                long currentTimeMillis = System.currentTimeMillis();
                if (currentTimeMillis - j > this.b) {
                    z = true;
                }
                StringBuilder sb = new StringBuilder("currentTime=");
                sb.append(currentTimeMillis);
                sb.append(",lastUpdateTime:");
                sb.append(j);
                g.a("", sb.toString());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return z;
    }

    static /* synthetic */ void a(b bVar) {
        r.a(bVar.f2943a, a.f, Long.valueOf(System.currentTimeMillis()));
        new g(bVar.f2943a, 0).a(a.f2942a, new l(), (d<?>) new h() {
            public final void a(RoverCampaignUnit roverCampaignUnit) {
                ArrayList<CampaignEx> arrayList = roverCampaignUnit.ads;
                if (arrayList != null) {
                    for (CampaignEx campaignEx : arrayList) {
                        if (campaignEx != null) {
                            new c(campaignEx, b.this.f2943a);
                        }
                    }
                }
            }
        });
    }
}
