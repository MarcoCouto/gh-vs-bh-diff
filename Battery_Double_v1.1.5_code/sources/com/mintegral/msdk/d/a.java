package com.mintegral.msdk.d;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import com.mintegral.msdk.base.b.f;
import com.mintegral.msdk.base.b.h;
import com.mintegral.msdk.base.b.i;
import com.mintegral.msdk.base.b.u;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.reward.a.b;
import com.mintegral.msdk.reward.a.c;
import java.util.LinkedList;
import java.util.List;

/* compiled from: LoopTimer */
public final class a {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public long f2668a;
    /* access modifiers changed from: private */
    public boolean b;
    private LinkedList<String> c;
    private LinkedList<String> d;
    private int e;
    private int f;
    private f g;
    private com.mintegral.msdk.videocommon.e.a h;
    private u i;
    private i j;
    /* access modifiers changed from: private */
    public Handler k;

    /* renamed from: com.mintegral.msdk.d.a$a reason: collision with other inner class name */
    /* compiled from: LoopTimer */
    static class C0054a {

        /* renamed from: a reason: collision with root package name */
        static a f2671a = new a(0);
    }

    /* synthetic */ a(byte b2) {
        this();
    }

    private a() {
        this.b = false;
        this.c = new LinkedList<>();
        this.d = new LinkedList<>();
        this.e = 0;
        this.f = 0;
        this.k = new Handler() {
            /* JADX WARNING: Code restructure failed: missing block: B:13:0x002d, code lost:
                return;
             */
            public final void handleMessage(Message message) {
                synchronized (a.this) {
                    switch (message.what) {
                        case 1:
                            if (!a.this.b) {
                                a.c(a.this);
                                sendMessageDelayed(obtainMessage(1), a.this.f2668a);
                                break;
                            } else {
                                return;
                            }
                        case 2:
                            a.d(a.this);
                            break;
                    }
                }
            }
        };
    }

    private boolean c(String str) {
        try {
            if (this.g == null) {
                return true;
            }
            long j2 = 0;
            if (this.h != null) {
                j2 = this.h.e();
            }
            int a2 = this.g.a(str, j2);
            if (a2 != -1) {
                if (a2 == 1) {
                    return true;
                }
            } else if (!TextUtils.isEmpty(str)) {
                if (this.c != null && this.c.contains(str)) {
                    this.c.remove(str);
                } else if (this.d != null && this.d.contains(str)) {
                    this.d.remove(str);
                }
                if (this.i != null) {
                    this.i.a(str);
                }
            }
            this.k.sendMessage(this.k.obtainMessage(2));
            return false;
        } catch (Throwable th) {
            g.c("LoopTimer", th.getMessage(), th);
            return true;
        }
    }

    private void a(String str, boolean z) {
        try {
            Context h2 = com.mintegral.msdk.base.controller.a.d().h();
            if (h2 != null) {
                final c cVar = new c(h2, str);
                cVar.a(z);
                cVar.a((b) new b() {
                    public final void a() {
                    }

                    public final void a(String str) {
                        a.this.k.sendMessage(a.this.k.obtainMessage(2));
                        cVar.a((b) null);
                    }

                    public final void b() {
                        a.this.k.sendMessage(a.this.k.obtainMessage(2));
                        cVar.a((b) null);
                    }
                });
                cVar.f();
            }
        } catch (Exception e2) {
            g.c("LoopTimer", e2.getMessage(), e2);
        }
    }

    public final void a(String str) {
        if (!this.c.contains(str)) {
            this.c.add(str);
            if (this.i != null) {
                this.i.a(str, 94);
            }
        }
    }

    public final void b(String str) {
        if (!this.d.contains(str)) {
            this.d.add(str);
            if (this.i != null) {
                this.i.a(str, 287);
            }
        }
    }

    public final void a(long j2) {
        if (this.j == null) {
            this.j = i.a(com.mintegral.msdk.base.controller.a.d().h());
        }
        if (this.i == null) {
            this.i = u.a((h) this.j);
        }
        List<String> a2 = this.i.a(287);
        if (a2 != null) {
            this.d.addAll(a2);
            for (String b2 : a2) {
                b(b2);
            }
        }
        List<String> a3 = this.i.a(94);
        if (a3 != null) {
            this.c.addAll(a3);
            for (String a4 : a3) {
                a(a4);
            }
        }
        if (this.g == null) {
            this.g = f.a((h) this.j);
        }
        if (this.h == null) {
            com.mintegral.msdk.videocommon.e.b.a();
            this.h = com.mintegral.msdk.videocommon.e.b.b();
        }
        this.f2668a = j2;
        this.b = false;
        this.k.sendMessageDelayed(this.k.obtainMessage(1), this.f2668a);
    }

    static /* synthetic */ void c(a aVar) {
        if (aVar.c != null && aVar.c.size() > 0 && aVar.e != 0 && aVar.c.size() > aVar.e) {
            return;
        }
        if (aVar.d == null || aVar.d.size() <= 0 || aVar.f == 0 || aVar.d.size() == aVar.f) {
            aVar.f = 0;
            aVar.e = 0;
            aVar.k.sendMessage(aVar.k.obtainMessage(2));
        }
    }

    static /* synthetic */ void d(a aVar) {
        try {
            if (aVar.c != null && aVar.c.size() > 0 && aVar.e < aVar.c.size()) {
                String str = (String) aVar.c.get(aVar.e);
                aVar.e++;
                if (aVar.c(str)) {
                    aVar.a(str, false);
                }
            } else if (aVar.d != null && aVar.d.size() > 0 && aVar.f < aVar.d.size()) {
                String str2 = (String) aVar.d.get(aVar.f);
                aVar.f++;
                if (aVar.c(str2)) {
                    aVar.a(str2, true);
                }
            }
        } catch (Throwable th) {
            g.c("LoopTimer", th.getMessage(), th);
        }
    }
}
