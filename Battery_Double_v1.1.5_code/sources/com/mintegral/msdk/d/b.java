package com.mintegral.msdk.d;

/* compiled from: TimerController */
public class b {

    /* compiled from: TimerController */
    static class a {

        /* renamed from: a reason: collision with root package name */
        static b f2672a = new b(0);
    }

    /* synthetic */ b(byte b) {
        this();
    }

    private b() {
    }

    public static b getInstance() {
        return a.f2672a;
    }

    public void start() {
        com.mintegral.msdk.b.b.a();
        com.mintegral.msdk.b.a b = com.mintegral.msdk.b.b.b(com.mintegral.msdk.base.controller.a.d().j());
        if (b == null) {
            com.mintegral.msdk.b.b.a();
            b = com.mintegral.msdk.b.b.b();
        }
        int b2 = b.b();
        if (b2 > 0) {
            C0054a.f2671a.a((long) (b2 * 1000));
        }
    }

    public void addRewardList(String str) {
        C0054a.f2671a.a(str);
    }

    public void addInterstitialList(String str) {
        C0054a.f2671a.b(str);
    }
}
