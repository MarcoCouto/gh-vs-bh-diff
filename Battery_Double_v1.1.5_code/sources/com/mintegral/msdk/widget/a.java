package com.mintegral.msdk.widget;

import android.view.View;
import android.view.View.OnClickListener;
import java.util.Calendar;

/* compiled from: MIntegralNoDoubleClick */
public abstract class a implements OnClickListener {

    /* renamed from: a reason: collision with root package name */
    private long f3079a = 0;

    /* access modifiers changed from: protected */
    public abstract void a();

    public void onClick(View view) {
        long timeInMillis = Calendar.getInstance().getTimeInMillis();
        if (timeInMillis - this.f3079a > 2000) {
            this.f3079a = timeInMillis;
            a();
        }
    }
}
