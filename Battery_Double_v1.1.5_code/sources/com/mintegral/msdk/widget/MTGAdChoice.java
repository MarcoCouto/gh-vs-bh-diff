package com.mintegral.msdk.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.ImageView.ScaleType;
import com.mintegral.msdk.b.a;
import com.mintegral.msdk.b.b;
import com.mintegral.msdk.base.common.c.c;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.base.utils.j;
import com.mintegral.msdk.out.Campaign;

public class MTGAdChoice extends MTGImageView {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public static String f3075a = "MTGAdChoice";
    private String b = "";
    private String c = "";
    private String d = "";
    private Context e;

    public MTGAdChoice(Context context) {
        super(context);
        this.e = context;
        b();
    }

    public MTGAdChoice(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.e = context;
        b();
    }

    public MTGAdChoice(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.e = context;
        b();
    }

    private void b() {
        setScaleType(ScaleType.FIT_CENTER);
        setClickable(true);
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x0043  */
    public void setCampaign(Campaign campaign) {
        boolean z;
        if (campaign instanceof CampaignEx) {
            CampaignEx campaignEx = (CampaignEx) campaign;
            if (!(campaignEx == null || campaignEx.getAdchoice() == null)) {
                String e2 = campaignEx.getAdchoice().e();
                this.b = e2;
                if (!TextUtils.isEmpty(e2)) {
                    String d2 = campaignEx.getAdchoice().d();
                    this.d = d2;
                    if (!TextUtils.isEmpty(d2) && !TextUtils.isEmpty(campaignEx.getAdchoice().f())) {
                        z = true;
                        if (!z) {
                            b.a();
                            a b2 = b.b(com.mintegral.msdk.base.controller.a.d().j());
                            if (b2 != null) {
                                String bc = b2.bc();
                                this.b = bc;
                                if (!TextUtils.isEmpty(bc)) {
                                    String be = b2.be();
                                    this.d = be;
                                    if (!TextUtils.isEmpty(be) && !TextUtils.isEmpty(b2.bd())) {
                                        z = true;
                                    }
                                }
                            }
                            z = false;
                        }
                        setImageUrl(this.b);
                        if (z && this.e != null) {
                            com.mintegral.msdk.base.common.c.b.a(this.e).a(this.b, (c) new c() {
                                public final void onSuccessLoad(Bitmap bitmap, String str) {
                                    MTGAdChoice.this.setImageBitmap(bitmap);
                                }

                                public final void onFailedLoad(String str, String str2) {
                                    String a2 = MTGAdChoice.f3075a;
                                    StringBuilder sb = new StringBuilder("AD choice load failed:");
                                    sb.append(str);
                                    g.d(a2, sb.toString());
                                }
                            });
                            return;
                        }
                    }
                }
            }
            z = false;
            if (!z) {
            }
            setImageUrl(this.b);
            if (z) {
            }
        }
    }

    public boolean performClick() {
        if (!TextUtils.isEmpty(this.d)) {
            j.b(this.e, this.d);
        }
        return true;
    }
}
