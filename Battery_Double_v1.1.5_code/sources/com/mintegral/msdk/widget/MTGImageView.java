package com.mintegral.msdk.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.widget.ImageView;
import com.mintegral.msdk.base.common.c.b;
import com.mintegral.msdk.base.common.c.c;
import com.mintegral.msdk.base.utils.g;

public class MTGImageView extends ImageView {

    /* renamed from: a reason: collision with root package name */
    private Bitmap f3077a = null;
    private String b;

    public MTGImageView(Context context) {
        super(context);
    }

    public MTGImageView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public MTGImageView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public void setImageUrl(String str) {
        this.b = str;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        if (this.f3077a == null || !this.f3077a.isRecycled()) {
            super.onDraw(canvas);
            return;
        }
        g.d("mtg-widget-imageview", "onDraw bitmap recycled");
        if (getContext() != null) {
            b.a(getContext()).a(this.b, (c) new c() {
                public final void onSuccessLoad(Bitmap bitmap, String str) {
                    MTGImageView.this.setImageBitmap(bitmap);
                }

                public final void onFailedLoad(String str, String str2) {
                    StringBuilder sb = new StringBuilder();
                    sb.append(str2);
                    sb.append(" load failed:");
                    sb.append(str);
                    g.d("mtg-widget-imageview", sb.toString());
                }
            });
        }
    }

    public void setImageBitmap(Bitmap bitmap) {
        this.f3077a = bitmap;
        if (bitmap == null || !bitmap.isRecycled()) {
            super.setImageBitmap(bitmap);
            return;
        }
        this.f3077a = null;
        super.setImageBitmap(null);
        g.d("mtg-widget-imageview", "setImageBitmap recycled");
    }
}
