package com.mintegral.msdk.activity;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment.InstantiationException;
import android.text.TextUtils;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.Toast;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.b.b;
import com.mintegral.msdk.base.b.f;
import com.mintegral.msdk.base.b.h;
import com.mintegral.msdk.base.b.i;
import com.mintegral.msdk.base.controller.a;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.utils.j;
import com.mintegral.msdk.base.webview.BrowserView;
import org.json.JSONException;
import org.json.JSONObject;

public class MTGCommonActivity extends Activity {

    /* renamed from: a reason: collision with root package name */
    String f2486a = "";
    private CampaignEx b;

    public void onCreate(Bundle bundle) {
        String stringExtra = getIntent().getStringExtra("intent_flag");
        String stringExtra2 = getIntent().getStringExtra("intent_jsonobject");
        if (TextUtils.isEmpty(stringExtra) || !stringExtra.equals("shortcuts") || TextUtils.isEmpty(stringExtra2)) {
            if (MIntegralConstans.APPWALL_IS_SHOW_WHEN_SCREEN_LOCK) {
                getWindow().addFlags(4718592);
            }
            if (a.d().h() == null) {
                a.d().a(getApplicationContext());
            }
            super.onCreate(bundle);
            requestWindowFeature(1);
            try {
                super.onCreate(bundle);
                this.f2486a = getIntent().getStringExtra("url");
                if (!TextUtils.isEmpty(this.f2486a)) {
                    String str = this.f2486a;
                    BrowserView browserView = new BrowserView((Context) this, this.b);
                    browserView.loadUrl(str);
                    browserView.setListener(new BrowserView.a() {
                        public final void a(String str) {
                        }

                        public final void a() {
                            MTGCommonActivity.this.finish();
                        }

                        public final boolean b(String str) {
                            if (j.a.a(str) && j.a.a(MTGCommonActivity.this, str)) {
                                MTGCommonActivity.this.finish();
                            }
                            return false;
                        }
                    });
                    setContentView(browserView);
                    this.b = (CampaignEx) getIntent().getSerializableExtra("mvcommon");
                    return;
                }
                Toast.makeText(this, "Error: no data", 0).show();
            } catch (InstantiationException unused) {
                finish();
            }
        } else {
            super.onCreate(bundle);
            try {
                this.b = CampaignEx.parseShortCutsCampaign(new JSONObject(stringExtra2));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            try {
                b.a();
                com.mintegral.msdk.b.a b2 = b.b(a.d().j());
                if (b2 == null) {
                    b.a();
                    b2 = b.b();
                }
                CampaignEx c = f.a((h) i.a((Context) this)).c(this.b.getId(), b2.j());
                if (c != null && !TextUtils.isEmpty(this.b.getImpressionURL()) && !TextUtils.isEmpty(b2.j()) && c.getIsClick() == 0) {
                    com.mintegral.msdk.click.a.a((Context) this, this.b, b2.j(), this.b.getImpressionURL(), false, true);
                }
                this.b.setIsClick(1);
                ContentValues contentValues = new ContentValues();
                contentValues.put("is_click", Integer.valueOf(1));
                f.a((h) i.a((Context) this)).a(this.b.getId(), contentValues);
                LinearLayout linearLayout = new LinearLayout(this);
                linearLayout.setLayoutParams(new LayoutParams(-1, -1));
                setContentView(linearLayout);
                com.mintegral.msdk.c.b.a((Context) this).a(b2);
                com.mintegral.msdk.c.b.a((Context) this).b(this.b, (Activity) this);
            } catch (Exception e2) {
                finish();
                e2.printStackTrace();
            }
        }
    }

    public void setTheme(int i) {
        String stringExtra = getIntent().getStringExtra("intent_flag");
        if (!TextUtils.isEmpty(stringExtra) && stringExtra.equals("shortcuts")) {
            super.setTheme(16973840);
        }
    }
}
