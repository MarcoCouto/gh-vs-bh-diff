package com.mintegral.msdk.reward.b;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.TextUtils;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.base.b.h;
import com.mintegral.msdk.base.b.i;
import com.mintegral.msdk.base.b.l;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.entity.e;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.base.utils.k;
import com.mintegral.msdk.base.utils.r;
import com.mintegral.msdk.base.utils.s;
import com.mintegral.msdk.videocommon.download.j;
import com.mintegral.msdk.videocommon.listener.InterVideoOutListener;
import io.fabric.sdk.android.services.events.EventsFilesManager;
import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;

/* compiled from: RewardVideoController */
public class a {

    /* renamed from: a reason: collision with root package name */
    public static String f2921a;
    public static Map<String, d> b = new HashMap();
    private static Map<String, Integer> x = new HashMap();
    private i A = null;
    /* access modifiers changed from: private */
    public Context c;
    private int d;
    private int e;
    private int f;
    private com.mintegral.msdk.reward.a.c g;
    /* access modifiers changed from: private */
    public com.mintegral.msdk.videocommon.e.c h;
    private String i;
    /* access modifiers changed from: private */
    public Queue<Integer> j;
    private com.mintegral.msdk.videocommon.e.a k;
    /* access modifiers changed from: private */
    public InterVideoOutListener l;
    /* access modifiers changed from: private */
    public c m;
    private String n;
    /* access modifiers changed from: private */
    public String o;
    /* access modifiers changed from: private */
    public String p;
    private int q = 0;
    private Queue<Integer> r;
    private Queue<Integer> s;
    /* access modifiers changed from: private */
    public Handler t = new Handler(Looper.getMainLooper()) {
        public final void handleMessage(Message message) {
            switch (message.what) {
                case 8:
                    if (a.this.c(true)) {
                        if (a.this.m != null) {
                            c.a(a.this.m, a.this.o);
                            return;
                        }
                    } else if (a.this.m != null) {
                        c.b(a.this.m, "load timeout");
                        return;
                    }
                    break;
                case 9:
                    if (a.this.l != null) {
                        Object obj = message.obj;
                        String str = "";
                        if (obj instanceof String) {
                            str = obj.toString();
                        }
                        a.this.l.onVideoLoadSuccess(str);
                        return;
                    }
                    break;
                case 16:
                    if (a.this.l != null) {
                        Object obj2 = message.obj;
                        String str2 = "";
                        if (obj2 instanceof String) {
                            str2 = obj2.toString();
                        }
                        com.mintegral.msdk.reward.d.a.a(a.this.c, str2, a.this.o, a.this.w);
                        a.this.l.onVideoLoadFail(str2);
                        return;
                    }
                    break;
                case 17:
                    if (a.this.l != null) {
                        Object obj3 = message.obj;
                        String str3 = "";
                        if (obj3 instanceof String) {
                            str3 = obj3.toString();
                        }
                        a.this.l.onLoadSuccess(str3);
                        break;
                    }
                    break;
            }
        }
    };
    private int u = 2;
    /* access modifiers changed from: private */
    public boolean v = false;
    /* access modifiers changed from: private */
    public boolean w = false;
    /* access modifiers changed from: private */
    public Queue<Integer> y;
    private String z;

    /* renamed from: com.mintegral.msdk.reward.b.a$a reason: collision with other inner class name */
    /* compiled from: RewardVideoController */
    public class C0065a implements Runnable {
        private com.mintegral.msdk.reward.a.a b;
        private int c;
        private boolean d;

        public C0065a(com.mintegral.msdk.reward.a.a aVar, int i, boolean z) {
            this.b = aVar;
            this.c = i;
            this.d = z;
        }

        public final void run() {
            StringBuilder sb = new StringBuilder("adSource=");
            sb.append(this.c);
            sb.append(" CommonCancelTimeTask mIsDevCall：");
            sb.append(this.d);
            g.d("RewardVideoController", sb.toString());
            a.this.a(a.this.j, a.this.y, true, this.d, "");
        }
    }

    /* compiled from: RewardVideoController */
    public class b implements com.mintegral.msdk.reward.a.b {
        private com.mintegral.msdk.reward.a.a b;
        private boolean c;
        private Runnable d;

        public b(com.mintegral.msdk.reward.a.a aVar, boolean z) {
            this.b = aVar;
            this.c = z;
        }

        public final void a(Runnable runnable) {
            this.d = runnable;
        }

        public final void b() {
            if (this.d != null) {
                g.d("RewardVideoController", "onCampaignLoadSuccess remove task ");
                a.this.t.removeCallbacks(this.d);
            }
            if (a.this.m != null && this.c) {
                c.c(a.this.m, a.this.o);
            }
        }

        public final void a() {
            if (this.d != null) {
                g.d("RewardVideoController", "onVideoLoadSuccess remove task ");
                a.this.t.removeCallbacks(this.d);
            }
            if (a.this.m != null && this.c) {
                c.a(a.this.m, a.this.o);
            }
        }

        public final void a(String str) {
            if (this.d != null) {
                g.d("RewardVideoController", "onVideoLoadFail remove task");
                a.this.t.removeCallbacks(this.d);
            }
            if (this.b != null) {
                this.b.a(null);
                this.b = null;
            }
            if (a.this.j == null || a.this.j.size() <= 0) {
                if (a.this.m != null && this.c) {
                    c.b(a.this.m, str);
                }
                return;
            }
            a.this.a(a.this.j, a.this.y, true, this.c, "");
        }
    }

    /* compiled from: RewardVideoController */
    private static final class c {

        /* renamed from: a reason: collision with root package name */
        private WeakReference<InterVideoOutListener> f2926a;
        /* access modifiers changed from: private */
        public int b;
        private Handler c;

        /* synthetic */ c(InterVideoOutListener interVideoOutListener, Handler handler, byte b2) {
            this(interVideoOutListener, handler);
        }

        private c(InterVideoOutListener interVideoOutListener, Handler handler) {
            this.b = 0;
            this.f2926a = new WeakReference<>(interVideoOutListener);
            this.c = handler;
        }

        static /* synthetic */ void a(c cVar, String str) {
            if (cVar.f2926a != null && cVar.f2926a.get() != null && cVar.b == 1) {
                cVar.b = 2;
                if (cVar.c != null) {
                    Message obtain = Message.obtain();
                    obtain.obj = str;
                    obtain.what = 9;
                    cVar.c.sendMessage(obtain);
                }
            }
        }

        static /* synthetic */ void b(c cVar, String str) {
            if (cVar.f2926a != null && cVar.f2926a.get() != null && cVar.b == 1) {
                cVar.b = 2;
                if (cVar.c != null) {
                    Message obtain = Message.obtain();
                    obtain.obj = str;
                    obtain.what = 16;
                    cVar.c.sendMessage(obtain);
                }
            }
        }

        static /* synthetic */ void c(c cVar, String str) {
            if (cVar.f2926a != null && cVar.f2926a.get() != null && cVar.b == 1 && cVar.c != null) {
                Message obtain = Message.obtain();
                obtain.obj = str;
                obtain.what = 17;
                cVar.c.sendMessage(obtain);
            }
        }
    }

    /* compiled from: RewardVideoController */
    private static final class d implements com.mintegral.msdk.reward.a.d {

        /* renamed from: a reason: collision with root package name */
        private a f2927a;
        private int b;

        public final boolean b() {
            return false;
        }

        /* synthetic */ d(a aVar, int i, byte b2) {
            this(aVar, i);
        }

        private d(a aVar, int i) {
            this.f2927a = aVar;
            this.b = i;
        }

        public final void a() {
            try {
                if (this.f2927a != null) {
                    this.f2927a.b(this.b);
                    if (this.f2927a.l != null) {
                        this.f2927a.l.onAdShow();
                    }
                }
            } catch (Throwable th) {
                g.c("RewardVideoController", th.getMessage(), th);
            }
        }

        public final void a(boolean z, com.mintegral.msdk.videocommon.b.d dVar) {
            try {
                if (!(this.f2927a == null || this.f2927a.l == null)) {
                    if (dVar == null) {
                        dVar = com.mintegral.msdk.videocommon.b.d.a(this.f2927a.p);
                    }
                    this.f2927a.l.onAdClose(z, dVar.a(), (float) dVar.b());
                    g.a("RewardVideoController", "onAdClose start release");
                    this.f2927a = null;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public final void a(String str) {
            if (this.f2927a != null) {
                if (this.f2927a.l != null) {
                    this.f2927a.l.onShowFail(str);
                }
                if (!this.f2927a.w && !this.f2927a.v && this.f2927a.h.b(4)) {
                    this.f2927a.b(false);
                }
            }
        }

        public final void b(String str) {
            if (this.f2927a != null && this.f2927a.l != null) {
                this.f2927a.l.onVideoAdClicked(str);
            }
        }

        public final void c(String str) {
            g.a("RewardVideoController", "onVideoComplete start");
            if (this.f2927a != null && this.f2927a.l != null) {
                this.f2927a.l.onVideoComplete(str);
                g.a("RewardVideoController", "onEndcardShow callback");
            }
        }

        public final void d(String str) {
            g.a("RewardVideoController", "onEndcardShow start");
            if (this.f2927a != null && this.f2927a.l != null) {
                this.f2927a.l.onEndcardShow(str);
                g.a("RewardVideoController", "onEndcardShow callback");
            }
        }

        public final void e(String str) {
            if (this.f2927a != null && !this.f2927a.w && !this.f2927a.v && this.f2927a.h.b(2)) {
                this.f2927a.b(false);
            }
        }
    }

    public final void a(boolean z2) {
        this.v = z2;
    }

    public final void a() {
        this.w = true;
    }

    public final void a(int i2) {
        this.u = i2;
    }

    public final void a(String str, String str2, String str3, String str4) {
        if (!TextUtils.isEmpty(str)) {
            Context context = this.c;
            StringBuilder sb = new StringBuilder("Mintegral_ConfirmTitle");
            sb.append(this.o);
            r.a(context, sb.toString(), str.trim());
        }
        if (!TextUtils.isEmpty(str2)) {
            Context context2 = this.c;
            StringBuilder sb2 = new StringBuilder("Mintegral_ConfirmContent");
            sb2.append(this.o);
            r.a(context2, sb2.toString(), str2.trim());
        }
        if (!TextUtils.isEmpty(str4)) {
            Context context3 = this.c;
            StringBuilder sb3 = new StringBuilder("Mintegral_CancelText");
            sb3.append(this.o);
            r.a(context3, sb3.toString(), str4.trim());
        }
        if (!TextUtils.isEmpty(str3)) {
            Context context4 = this.c;
            StringBuilder sb4 = new StringBuilder("Mintegral_ConfirmText");
            sb4.append(this.o);
            r.a(context4, sb4.toString(), str3.trim());
        }
    }

    public static void a(String str, int i2) {
        try {
            if (x != null && s.b(str)) {
                x.put(str, Integer.valueOf(i2));
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public static int a(String str) {
        try {
            if (s.b(str) && x != null && x.containsKey(str)) {
                Integer num = (Integer) x.get(str);
                if (num != null) {
                    return num.intValue();
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return 0;
    }

    public static void insertExcludeId(String str, CampaignEx campaignEx) {
        if (!TextUtils.isEmpty(str) && campaignEx != null && com.mintegral.msdk.base.controller.a.d().h() != null) {
            l a2 = l.a((h) i.a(com.mintegral.msdk.base.controller.a.d().h()));
            e eVar = new e();
            eVar.a(System.currentTimeMillis());
            eVar.b(str);
            eVar.a(campaignEx.getId());
            a2.a(eVar);
        }
    }

    public final void a(InterVideoOutListener interVideoOutListener) {
        this.l = interVideoOutListener;
        this.m = new c(interVideoOutListener, this.t, 0);
    }

    public final void b(String str) {
        try {
            this.c = com.mintegral.msdk.base.controller.a.d().h();
            this.o = str;
            com.mintegral.msdk.videocommon.e.b.a();
            this.k = com.mintegral.msdk.videocommon.e.b.b();
            com.mintegral.msdk.reward.d.a.a(this.c, this.o);
            com.mintegral.msdk.base.utils.e.b();
            j.a().b();
            com.mintegral.msdk.videocommon.download.h.a().b();
            if (!TextUtils.isEmpty(this.o)) {
                List a2 = com.mintegral.msdk.videocommon.a.a.a().a(this.o, 1);
                if (a2 != null && a2.size() > 0) {
                    com.mintegral.msdk.videocommon.download.c.getInstance().createUnitCache(this.c, this.o, a2, 3, null);
                }
            }
            if (this.A == null) {
                this.A = i.a(com.mintegral.msdk.base.controller.a.d().h());
            }
        } catch (Throwable th) {
            g.c("RewardVideoController", th.getMessage(), th);
        }
    }

    public static void b() {
        com.mintegral.msdk.base.utils.e.a();
    }

    public final void b(boolean z2) {
        a(z2, "");
    }

    public final void a(boolean z2, String str) {
        boolean z3;
        try {
            if (TextUtils.isEmpty(str)) {
                if (this.v) {
                    com.mintegral.msdk.d.b.getInstance().addInterstitialList(this.o);
                } else {
                    com.mintegral.msdk.d.b.getInstance().addRewardList(this.o);
                }
            }
            if (this.w && TextUtils.isEmpty(str)) {
                if (this.t != null) {
                    Message obtain = Message.obtain();
                    obtain.obj = "bidToken is empty";
                    obtain.what = 16;
                    this.t.sendMessage(obtain);
                }
            } else if (com.mintegral.msdk.system.a.f2952a == null) {
                if (this.l != null && z2) {
                    com.mintegral.msdk.reward.d.a.a(this.c, "init error", this.o, this.w);
                    this.l.onVideoLoadFail("init error");
                }
            } else {
                com.mintegral.msdk.videocommon.e.b.a();
                this.h = com.mintegral.msdk.videocommon.e.b.a(com.mintegral.msdk.base.controller.a.d().j(), this.o);
                if (this.h == null) {
                    this.z = com.mintegral.msdk.base.controller.a.d().j();
                    com.mintegral.msdk.videocommon.e.b.a();
                    com.mintegral.msdk.videocommon.e.b.a(this.z, com.mintegral.msdk.base.controller.a.d().k(), this.o, new com.mintegral.msdk.videocommon.c.c() {
                    });
                    com.mintegral.msdk.videocommon.e.b.a();
                    this.h = com.mintegral.msdk.videocommon.e.b.b(com.mintegral.msdk.base.controller.a.d().j(), this.o);
                }
                this.j = this.h.N();
                this.y = this.h.O();
                if (c()) {
                    try {
                        List K = this.h.K();
                        if (K != null && K.size() > 0) {
                            for (int i2 = 0; i2 < K.size(); i2++) {
                                com.mintegral.msdk.videocommon.b.b bVar = (com.mintegral.msdk.videocommon.b.b) K.get(i2);
                                Context context = this.c;
                                StringBuilder sb = new StringBuilder();
                                sb.append(this.i);
                                sb.append(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
                                sb.append(bVar.a());
                                r.a(context, sb.toString(), Integer.valueOf(0));
                            }
                        }
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                }
                if (this.m != null) {
                    if (this.m.b == 1 && this.l != null) {
                        if (z2) {
                            this.l.onVideoLoadFail("current unit is loading");
                            com.mintegral.msdk.reward.d.a.a(this.c, "current unit is loading", this.o, this.w);
                            this.m.b = 1;
                        }
                        return;
                    } else if (z2) {
                        this.m.b = 1;
                    }
                }
                if (!c(true) || this.m == null) {
                    z3 = z2;
                } else {
                    c.c(this.m, this.o);
                    c.a(this.m, this.o);
                    if (!this.w) {
                        z3 = false;
                    }
                    return;
                }
                if (!z3 || !d()) {
                    a(this.j, this.y, true, z3, str);
                    return;
                }
                if (this.m != null) {
                    c.b(this.m, "Play more than limit");
                }
                if (!this.w && this.h.b(3)) {
                    a(this.j, this.y, true, false, "");
                }
            }
        } catch (Exception e3) {
            e3.printStackTrace();
        }
    }

    private boolean c() {
        int i2;
        try {
            List K = this.h.K();
            Map l2 = this.k.l();
            if (K != null && K.size() > 0) {
                for (int i3 = 0; i3 < K.size(); i3++) {
                    com.mintegral.msdk.videocommon.b.b bVar = (com.mintegral.msdk.videocommon.b.b) K.get(i3);
                    StringBuilder sb = new StringBuilder();
                    sb.append(bVar.a());
                    if (l2.containsKey(sb.toString())) {
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append(bVar.a());
                        i2 = ((Integer) l2.get(sb2.toString())).intValue();
                    } else {
                        i2 = 0;
                    }
                    Context context = this.c;
                    StringBuilder sb3 = new StringBuilder();
                    sb3.append(this.i);
                    sb3.append(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
                    sb3.append(bVar.a());
                    Object b2 = r.b(context, sb3.toString(), Integer.valueOf(0));
                    if ((b2 != null ? ((Integer) b2).intValue() : 0) < i2) {
                        return false;
                    }
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            g.d("RewardVideoController", e2.getMessage());
        }
        return true;
    }

    /* access modifiers changed from: private */
    public void a(Queue<Integer> queue, Queue<Integer> queue2, boolean z2, boolean z3, String str) {
        int i2 = 8;
        if (queue != null) {
            try {
                if (queue.size() > 0) {
                    int intValue = ((Integer) queue.poll()).intValue();
                    if (queue2 != null && queue2.size() > 0) {
                        i2 = ((Integer) queue2.poll()).intValue();
                    }
                    if (z2) {
                        com.mintegral.msdk.videocommon.e.b.a();
                        this.k = com.mintegral.msdk.videocommon.e.b.b();
                        Integer num = (Integer) this.k.l().get(String.valueOf(intValue));
                        Context context = this.c;
                        StringBuilder sb = new StringBuilder();
                        sb.append(this.i);
                        sb.append(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
                        sb.append(intValue);
                        Object b2 = r.b(context, sb.toString(), Integer.valueOf(0));
                        int intValue2 = b2 != null ? ((Integer) b2).intValue() : 0;
                        if (num == null) {
                            num = Integer.valueOf(1000);
                        }
                        if (intValue2 >= num.intValue() && this.j.size() == 0) {
                            Context context2 = this.c;
                            StringBuilder sb2 = new StringBuilder();
                            sb2.append(this.i);
                            sb2.append(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
                            sb2.append(intValue);
                            r.a(context2, sb2.toString(), Integer.valueOf(0));
                            a(z3, str);
                            return;
                        } else if (intValue2 >= num.intValue() && this.j.size() > 0) {
                            if (this.r == null) {
                                this.r = new LinkedList();
                            }
                            if (this.s == null) {
                                this.s = new LinkedList();
                            }
                            this.r.add(Integer.valueOf(intValue));
                            this.s.add(Integer.valueOf(i2));
                            a(this.j, this.y, true, z3, str);
                            return;
                        }
                    }
                    if (intValue != 1) {
                        a(queue, queue2, z2, z3, str);
                        return;
                    }
                    try {
                        if (this.g == null || !this.o.equals(this.g.a())) {
                            this.g = new com.mintegral.msdk.reward.a.c(this.c, this.o);
                            this.g.a(this.v);
                            this.g.b(this.w);
                        }
                        this.g.a(this.u);
                        this.g.b();
                        C0065a aVar = new C0065a(this.g, intValue, z3);
                        b bVar = new b(this.g, z3);
                        bVar.a((Runnable) aVar);
                        this.g.a((com.mintegral.msdk.reward.a.b) bVar);
                        this.t.postDelayed(aVar, (long) (i2 * 1000));
                        this.g.a(intValue, i2, z3, str);
                        return;
                    } catch (Exception e2) {
                        e2.printStackTrace();
                        return;
                    }
                }
            } catch (Exception e3) {
                e3.printStackTrace();
                if (this.m != null && z3) {
                    c.b(this.m, "can't show because unknow error");
                }
                g.d("RewardVideoController", e3.getMessage());
                return;
            }
        }
        if (this.m != null && z3) {
            c.b(this.m, "no ads source");
        }
    }

    public final void b(int i2) {
        try {
            com.mintegral.msdk.base.b.j a2 = com.mintegral.msdk.base.b.j.a((h) this.A);
            if (a2 != null) {
                a2.a(this.o);
            }
        } catch (Throwable unused) {
            g.d("RewardVideoController", "can't find DailyPlayCapDao");
        }
        if (i2 == 1) {
            Context context = this.c;
            StringBuilder sb = new StringBuilder();
            sb.append(this.i);
            sb.append(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
            sb.append(i2);
            r.a(context, sb.toString(), Integer.valueOf(this.f + 1));
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:30:0x00a0 A[Catch:{ Exception -> 0x0186 }] */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x00f2 A[Catch:{ Exception -> 0x0186 }] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x0113 A[Catch:{ Exception -> 0x0186 }] */
    private void c(String str) {
        int i2;
        Context context;
        StringBuilder sb;
        try {
            if (this.h == null) {
                g.b("RewardVideoController", "unitSetting==null");
                if (this.l != null) {
                    this.l.onShowFail("can't show because load is failed");
                }
                if (!this.v && !this.w && this.h.b(4)) {
                    a(false, "");
                }
                return;
            }
            Queue N = this.h.N();
            while (N != null && N.size() > 0) {
                int intValue = ((Integer) N.poll()).intValue();
                if (intValue == 1) {
                    com.mintegral.msdk.reward.a.c cVar = new com.mintegral.msdk.reward.a.c(this.c, this.o);
                    cVar.a(this.v);
                    cVar.b(this.w);
                    if (this.k != null) {
                        Map l2 = this.k.l();
                        if (l2 != null && l2.containsKey("1")) {
                            i2 = ((Integer) l2.get("1")).intValue();
                            context = this.c;
                            sb = new StringBuilder();
                            sb.append(this.i);
                            sb.append(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
                            sb.append(intValue);
                            if (r.b(context, sb.toString(), Integer.valueOf(0)) != null) {
                                Context context2 = this.c;
                                StringBuilder sb2 = new StringBuilder();
                                sb2.append(this.i);
                                sb2.append(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
                                sb2.append(intValue);
                                this.f = ((Integer) r.b(context2, sb2.toString(), Integer.valueOf(0))).intValue();
                            }
                            StringBuilder sb3 = new StringBuilder("unitSettingMvApiCap:");
                            sb3.append(i2);
                            sb3.append(" mMvApiSpCap:");
                            sb3.append(this.f);
                            g.b("RewardVideoController", sb3.toString());
                            g.d("RewardVideoController", "controller 819");
                            if (!cVar.d()) {
                                if (this.f < i2 || i2 <= 0) {
                                    g.b("RewardVideoController", "invoke adapter show");
                                    d dVar = new d(this, intValue, 0);
                                    b.put(this.o, dVar);
                                    cVar.a((com.mintegral.msdk.reward.a.d) dVar, str, this.n, this.u);
                                    return;
                                }
                            } else if (cVar.e() && (this.f < i2 || i2 <= 0)) {
                                g.b("RewardVideoController", "invoke adapter show");
                                d dVar2 = new d(this, intValue, 0);
                                b.put(this.o, dVar2);
                                cVar.a((com.mintegral.msdk.reward.a.d) dVar2, str, this.n, this.u);
                                return;
                            }
                        }
                    }
                    i2 = 0;
                    context = this.c;
                    sb = new StringBuilder();
                    sb.append(this.i);
                    sb.append(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
                    sb.append(intValue);
                    if (r.b(context, sb.toString(), Integer.valueOf(0)) != null) {
                    }
                    StringBuilder sb32 = new StringBuilder("unitSettingMvApiCap:");
                    sb32.append(i2);
                    sb32.append(" mMvApiSpCap:");
                    sb32.append(this.f);
                    g.b("RewardVideoController", sb32.toString());
                    g.d("RewardVideoController", "controller 819");
                    if (!cVar.d()) {
                    }
                }
            }
            if (this.d == 0 && this.e == 0) {
                if (this.f == 0) {
                    if (this.l != null) {
                        this.l.onShowFail("can't show because load is failed");
                    }
                    if (!this.v && !this.w && this.h.b(4)) {
                        b(false);
                    }
                    return;
                }
            }
            Context context3 = this.c;
            StringBuilder sb4 = new StringBuilder();
            sb4.append(this.i);
            sb4.append("_1");
            r.a(context3, sb4.toString(), Integer.valueOf(0));
            c(str);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:28:0x0061  */
    public final boolean c(boolean z2) {
        boolean z3;
        try {
            if (d()) {
                z3 = false;
            } else {
                com.mintegral.msdk.reward.a.c cVar = new com.mintegral.msdk.reward.a.c(this.c, this.o);
                cVar.a(this.v);
                cVar.b(this.w);
                g.d("RewardVideoController", "controller 865");
                z3 = cVar.d();
                if (z3) {
                    try {
                        if (cVar.c() && !z2 && !this.w && this.h.b(1)) {
                            a(false, "");
                        }
                    } catch (Throwable th) {
                        th = th;
                        if (MIntegralConstans.DEBUG) {
                            th.printStackTrace();
                        }
                        return z3;
                    }
                }
            }
            if (!z3 && !z2 && !this.w && this.h.b(1)) {
                a(false, "");
            }
        } catch (Throwable th2) {
            th = th2;
            z3 = false;
            if (MIntegralConstans.DEBUG) {
            }
            return z3;
        }
        return z3;
    }

    public final void a(String str, String str2) {
        try {
            this.p = str;
            this.n = str2;
            if (this.m != null && this.m.b == 1) {
                if (this.l != null) {
                    this.l.onShowFail("campaing is loading");
                }
            } else if (this.c == null) {
                if (this.l != null) {
                    this.l.onShowFail("context is null");
                }
            } else if (this.v && !k.b(this.c)) {
                if (this.l != null) {
                    this.l.onShowFail("network exception");
                }
            } else if (d()) {
                if (this.l != null) {
                    this.l.onShowFail("Play more than limit");
                }
                if (!this.w && this.h.b(4)) {
                    a(this.j, this.y, true, false, "");
                }
            } else {
                if (TextUtils.isEmpty(this.n)) {
                    this.n = com.mintegral.msdk.base.utils.c.k();
                }
                String format = new SimpleDateFormat("dd").format(new Date());
                String str3 = (String) r.b(this.c, "reward_date", "0");
                if (!TextUtils.isEmpty(str3) && !TextUtils.isEmpty(format) && !str3.equals(format)) {
                    r.a(this.c, "reward_date", format);
                    Context context = this.c;
                    StringBuilder sb = new StringBuilder();
                    sb.append(this.i);
                    sb.append("_1");
                    r.a(context, sb.toString(), Integer.valueOf(0));
                }
                c(str);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    private boolean d() {
        try {
            com.mintegral.msdk.base.b.j a2 = com.mintegral.msdk.base.b.j.a((h) this.A);
            if (this.h == null) {
                com.mintegral.msdk.videocommon.e.b.a();
                this.h = com.mintegral.msdk.videocommon.e.b.a(com.mintegral.msdk.base.controller.a.d().j(), this.o);
            }
            int c2 = this.h.c();
            if (a2 == null || !a2.a(this.o, c2)) {
                return false;
            }
            return true;
        } catch (Throwable unused) {
            g.d("RewardVideoController", "cap check error");
            return false;
        }
    }

    public static void a(boolean z2, boolean z3) {
        try {
            if (b != null) {
                b.clear();
            }
            com.mintegral.msdk.base.common.net.a.b();
            if (z2) {
                if (z3) {
                    com.mintegral.msdk.videocommon.a.a(287);
                } else {
                    com.mintegral.msdk.videocommon.a.b(287);
                }
            } else if (z3) {
                com.mintegral.msdk.videocommon.a.a(94);
            } else {
                com.mintegral.msdk.videocommon.a.b(94);
            }
        } catch (Throwable unused) {
            g.d("RewardVideoController", "destory failed");
        }
    }
}
