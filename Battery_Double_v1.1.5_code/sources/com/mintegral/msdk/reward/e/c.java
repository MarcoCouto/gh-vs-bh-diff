package com.mintegral.msdk.reward.e;

import android.support.v4.app.NotificationCompat;
import com.mintegral.msdk.base.common.net.a.b;
import com.mintegral.msdk.base.entity.CampaignUnit;
import com.mintegral.msdk.base.utils.g;
import org.json.JSONObject;

/* compiled from: RewardResponseHandler */
public abstract class c extends b {

    /* renamed from: a reason: collision with root package name */
    private int f2932a;
    private long b;
    private String c;

    public abstract void b(int i, String str);

    public abstract void b(CampaignUnit campaignUnit);

    public final /* synthetic */ void a(Object obj) {
        JSONObject jSONObject = (JSONObject) obj;
        if (this.f2932a == 0) {
            int optInt = jSONObject.optInt("status");
            if (1 == optInt) {
                System.currentTimeMillis();
                CampaignUnit parseCampaignUnit = CampaignUnit.parseCampaignUnit(jSONObject.optJSONObject("data"), this.c);
                if (parseCampaignUnit == null || parseCampaignUnit.getAds() == null || parseCampaignUnit.getAds().size() <= 0) {
                    b(optInt, jSONObject.optString(NotificationCompat.CATEGORY_MESSAGE));
                } else {
                    b(parseCampaignUnit);
                }
            } else {
                b(optInt, jSONObject.optString(NotificationCompat.CATEGORY_MESSAGE));
            }
        } else {
            if (this.f2932a == 1) {
                int optInt2 = jSONObject.optInt("status");
                if (1 == optInt2) {
                    System.currentTimeMillis();
                    CampaignUnit parseCampaignUnit2 = CampaignUnit.parseCampaignUnit(jSONObject.optJSONObject("data"), this.c);
                    if (parseCampaignUnit2 == null || parseCampaignUnit2.getListFrames() == null || parseCampaignUnit2.getListFrames().size() <= 0) {
                        b(optInt2, jSONObject.optString(NotificationCompat.CATEGORY_MESSAGE));
                    } else {
                        parseCampaignUnit2.getListFrames();
                    }
                } else {
                    b(optInt2, jSONObject.optString(NotificationCompat.CATEGORY_MESSAGE));
                }
            }
        }
    }

    public final void a(String str) {
        this.c = str;
    }

    public final void a() {
        super.a();
        this.b = System.currentTimeMillis();
    }

    public final void a(int i) {
        StringBuilder sb = new StringBuilder("errorCode = ");
        sb.append(i);
        g.d("", sb.toString());
        b(i, c(i));
    }
}
