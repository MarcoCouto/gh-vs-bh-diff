package com.mintegral.msdk.reward.e;

import com.mintegral.msdk.base.entity.CampaignUnit;

/* compiled from: RewarLoadVideoResponseHandler */
public abstract class a extends c {
    public abstract void a(int i, String str);

    public abstract void a(CampaignUnit campaignUnit);

    public final void b(CampaignUnit campaignUnit) {
        a(campaignUnit);
    }

    public final void b(int i, String str) {
        a(i, str);
    }
}
