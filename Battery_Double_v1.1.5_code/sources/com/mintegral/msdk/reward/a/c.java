package com.mintegral.msdk.reward.a;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.TextUtils;
import android.webkit.WebView;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.source.chunk.ChunkedTrackBlacklistUtil;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.base.b.l;
import com.mintegral.msdk.base.b.m;
import com.mintegral.msdk.base.b.w;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.entity.CampaignUnit;
import com.mintegral.msdk.base.entity.p;
import com.mintegral.msdk.base.utils.CommonMD5;
import com.mintegral.msdk.base.utils.k;
import com.mintegral.msdk.base.utils.n;
import com.mintegral.msdk.base.utils.s;
import com.mintegral.msdk.mtgjscommon.windvane.WindVaneWebView;
import com.mintegral.msdk.reward.player.MTGRewardVideoActivity;
import com.mintegral.msdk.videocommon.a.C0068a;
import io.fabric.sdk.android.services.events.EventsFilesManager;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;

/* compiled from: RewardMVVideoAdapter */
public final class c implements a {

    /* renamed from: a reason: collision with root package name */
    public List<CampaignEx> f2907a = new ArrayList();
    /* access modifiers changed from: private */
    public Context b;
    /* access modifiers changed from: private */
    public String c;
    private int d;
    private int e;
    private int f;
    private boolean g;
    /* access modifiers changed from: private */
    public String h;
    /* access modifiers changed from: private */
    public String i;
    private d j;
    /* access modifiers changed from: private */
    public b k;
    private com.mintegral.msdk.videocommon.e.c l;
    /* access modifiers changed from: private */
    public i m;
    /* access modifiers changed from: private */
    public boolean n = false;
    /* access modifiers changed from: private */
    public boolean o = false;
    private int p = 2;
    /* access modifiers changed from: private */
    public boolean q;
    private boolean r;
    /* access modifiers changed from: private */
    public Handler s = new Handler(Looper.getMainLooper()) {
        public final void handleMessage(Message message) {
            try {
                int i = message.what;
                if (i != 16) {
                    String str = null;
                    switch (i) {
                        case 1:
                            StringBuilder sb = new StringBuilder("handler id获取成功 开始load mTtcIds:");
                            sb.append(c.this.h);
                            sb.append("  mExcludes:");
                            sb.append(c.this.i);
                            com.mintegral.msdk.base.utils.g.b("RewardMVVideoAdapter", sb.toString());
                            if (message.obj != null) {
                                str = message.obj.toString();
                            }
                            c.this.a(str);
                            return;
                        case 2:
                            StringBuilder sb2 = new StringBuilder("handler id获取超时  开始load mTtcIds:");
                            sb2.append(c.this.h);
                            sb2.append("  mExcludes:");
                            sb2.append(c.this.i);
                            com.mintegral.msdk.base.utils.g.b("RewardMVVideoAdapter", sb2.toString());
                            if (message.obj != null) {
                                str = message.obj.toString();
                            }
                            c.this.a(str);
                            return;
                        case 3:
                            if (c.this.k != null) {
                                com.mintegral.msdk.base.utils.g.b("RewardMVVideoAdapter", "handler 数据load成功");
                                c.this.k.b();
                            }
                            sendEmptyMessageDelayed(5, ChunkedTrackBlacklistUtil.DEFAULT_TRACK_BLACKLIST_MS);
                            return;
                        case 4:
                            if (c.this.k != null) {
                                com.mintegral.msdk.base.utils.g.b("RewardMVVideoAdapter", "handler 数据load失败");
                                try {
                                    if (message.obj == null) {
                                        c.this.k.a("data load failed");
                                        return;
                                    }
                                    String str2 = (String) message.obj;
                                    if (TextUtils.isEmpty(str2)) {
                                        c.this.k.a("data load failed, errorMsg null");
                                        return;
                                    }
                                    b c = c.this.k;
                                    StringBuilder sb3 = new StringBuilder("data load failed, errorMsg is ");
                                    sb3.append(str2);
                                    c.a(sb3.toString());
                                    return;
                                } catch (Exception e) {
                                    b c2 = c.this.k;
                                    StringBuilder sb4 = new StringBuilder("data load failed, exception is ");
                                    sb4.append(e.getMessage());
                                    c2.a(sb4.toString());
                                    return;
                                }
                            }
                            break;
                        case 5:
                            if (c.this.k != null) {
                                com.mintegral.msdk.base.utils.g.b("RewardMVVideoAdapter", "资源请求超时");
                                com.mintegral.msdk.base.utils.g.d("RewardMVVideoAdapter", "adapter 177");
                                if (c.this.d()) {
                                    c.this.k.a();
                                    return;
                                } else {
                                    c.this.k.a("resource load timeout");
                                    return;
                                }
                            }
                            break;
                        case 6:
                            com.mintegral.msdk.base.utils.g.b("RewardMVVideoAdapter", "handler res数据load成功0");
                            if (c.this.k != null) {
                                com.mintegral.msdk.base.utils.g.b("RewardMVVideoAdapter", "handler res数据load成功1");
                                c.this.k.a();
                                CampaignEx campaignEx = (CampaignEx) message.obj;
                                if (campaignEx != null && !TextUtils.isEmpty(c.this.c)) {
                                    com.mintegral.msdk.reward.d.a.a(c.this.b, campaignEx, c.this.c);
                                    com.mintegral.msdk.base.utils.g.d("RewardMVVideoAdapter", "2dwOnLoadSuccess");
                                }
                                return;
                            }
                            break;
                        default:
                            switch (i) {
                                case 8:
                                    if (c.this.k != null) {
                                        com.mintegral.msdk.base.utils.g.d("RewardMVVideoAdapter", "adapter 202");
                                        if (c.this.d()) {
                                            com.mintegral.msdk.base.utils.g.b("RewardMVVideoAdapter", "handler endcard源码下载成功 isready为true");
                                            c.this.k.a();
                                            CampaignEx campaignEx2 = (CampaignEx) message.obj;
                                            if (campaignEx2 != null && !TextUtils.isEmpty(c.this.c)) {
                                                com.mintegral.msdk.reward.d.a.a(c.this.b, campaignEx2, c.this.c);
                                                com.mintegral.msdk.base.utils.g.d("RewardMVVideoAdapter", "3dwOnLoadSuccess");
                                            }
                                            return;
                                        }
                                    }
                                    break;
                                case 9:
                                    break;
                            }
                    }
                }
                try {
                    int i2 = message.what;
                    Object[] objArr = (Object[]) message.obj;
                    CampaignEx campaignEx3 = (CampaignEx) objArr[0];
                    String str3 = (String) objArr[1];
                    String str4 = (String) objArr[2];
                    com.mintegral.msdk.videocommon.e.c cVar = (com.mintegral.msdk.videocommon.e.c) objArr[3];
                    if (campaignEx3 != null && !TextUtils.isEmpty(str3)) {
                        c.a(c.this, campaignEx3, str3, i2, str4, cVar);
                    }
                } catch (Exception unused) {
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    };

    /* compiled from: RewardMVVideoAdapter */
    private static final class a implements com.mintegral.msdk.videocommon.listener.a {

        /* renamed from: a reason: collision with root package name */
        private c f2911a;
        private CampaignEx b;

        public a(c cVar, CampaignEx campaignEx) {
            if (cVar != null) {
                this.f2911a = cVar;
            }
            this.b = campaignEx;
        }

        public final void a(String str) {
            try {
                com.mintegral.msdk.videocommon.download.i.a().a(str);
                com.mintegral.msdk.base.utils.g.d("RewardMVVideoAdapter", "CommonVideoDownloadListener VIDEO SUCCESS");
                if (this.f2911a != null) {
                    synchronized (this.f2911a) {
                        com.mintegral.msdk.base.utils.g.d("RewardMVVideoAdapter", "adapter 1613");
                        if (this.f2911a != null && this.f2911a.d()) {
                            com.mintegral.msdk.base.utils.g.d("RewardMVVideoAdapter", "CommonVideoDownloadListener VIDEO SUCCESS callback success");
                            if (this.f2911a.s != null) {
                                Message obtain = Message.obtain();
                                obtain.what = 6;
                                obtain.obj = this.b;
                                this.f2911a.s.sendMessage(obtain);
                                this.f2911a.s.removeMessages(5);
                                this.f2911a = null;
                            }
                        }
                    }
                }
            } catch (Throwable th) {
                com.mintegral.msdk.base.utils.g.c("RewardMVVideoAdapter", th.getMessage(), th);
            }
        }

        public final void a(String str, String str2) {
            try {
                if (this.f2911a != null) {
                    synchronized (this.f2911a) {
                        com.mintegral.msdk.base.utils.g.d("RewardMVVideoAdapter", "CommonVideoDownloadListener VIDEO failed");
                        c.a(this.f2911a, str, str2);
                        this.f2911a = null;
                    }
                }
            } catch (Throwable th) {
                com.mintegral.msdk.base.utils.g.c("RewardMVVideoAdapter", th.getMessage(), th);
            }
        }
    }

    /* compiled from: RewardMVVideoAdapter */
    private static final class b implements com.mintegral.msdk.base.common.c.c {

        /* renamed from: a reason: collision with root package name */
        private c f2912a;
        private CampaignEx b;
        private String c;

        public final void onSuccessLoad(Bitmap bitmap, String str) {
        }

        public b(c cVar, CampaignEx campaignEx, String str) {
            if (cVar != null) {
                this.f2912a = cVar;
            }
            this.b = campaignEx;
            this.c = str;
        }

        public final void onFailedLoad(String str, String str2) {
            if (this.f2912a != null) {
                c.a(this.c, this.b, str);
            }
        }
    }

    /* renamed from: com.mintegral.msdk.reward.a.c$c reason: collision with other inner class name */
    /* compiled from: RewardMVVideoAdapter */
    private static class C0064c implements com.mintegral.msdk.videocommon.download.g.a {

        /* renamed from: a reason: collision with root package name */
        private CampaignEx f2913a;
        private String b;
        private com.mintegral.msdk.videocommon.e.c c;
        private c d;

        public C0064c(c cVar, CampaignEx campaignEx, String str, com.mintegral.msdk.videocommon.e.c cVar2) {
            this.f2913a = campaignEx;
            this.b = str;
            this.c = cVar2;
            this.d = cVar;
        }

        public final void a(String str) {
            if (this.d != null) {
                this.d.a(this.f2913a, str, this.b, this.c);
            }
        }

        public final void a(String str, String str2) {
            if (this.d != null) {
                c.a(this.d, "TemplateUrl source download failed", str);
            }
        }
    }

    /* compiled from: RewardMVVideoAdapter */
    private static final class d implements com.mintegral.msdk.base.common.c.c {

        /* renamed from: a reason: collision with root package name */
        private c f2914a;
        private CampaignEx b;
        private String c;

        public d(c cVar, CampaignEx campaignEx, String str) {
            if (cVar != null) {
                this.f2914a = cVar;
            }
            this.b = campaignEx;
            this.c = str;
        }

        public final void onSuccessLoad(Bitmap bitmap, String str) {
            try {
                com.mintegral.msdk.videocommon.download.i.a();
                com.mintegral.msdk.videocommon.download.i.c(str);
                StringBuilder sb = new StringBuilder("DownTemplateImgCommonImageLoaderListener IMAGE SUCCESS");
                sb.append(str);
                com.mintegral.msdk.base.utils.g.d("RewardMVVideoAdapter", sb.toString());
                if (this.f2914a != null) {
                    synchronized (this.f2914a) {
                        com.mintegral.msdk.base.utils.g.d("RewardMVVideoAdapter", "adapter 1433");
                        if (this.f2914a.d() && this.f2914a.s != null) {
                            Message obtain = Message.obtain();
                            obtain.what = 6;
                            obtain.obj = this.b;
                            this.f2914a.s.sendMessage(obtain);
                            this.f2914a.s.removeMessages(5);
                            this.f2914a = null;
                        }
                    }
                }
            } catch (Throwable th) {
                com.mintegral.msdk.base.utils.g.c("RewardMVVideoAdapter", th.getMessage(), th);
            }
        }

        public final void onFailedLoad(String str, String str2) {
            try {
                if (this.f2914a != null) {
                    synchronized (this.f2914a) {
                        com.mintegral.msdk.base.utils.g.d("RewardMVVideoAdapter", "DownTemplateImgCommonImageLoaderListener IMAGE failed");
                        c.a(this.c, this.b, str);
                        c.a(this.f2914a, str, str2);
                        this.f2914a = null;
                    }
                }
            } catch (Throwable th) {
                com.mintegral.msdk.base.utils.g.c("RewardMVVideoAdapter", th.getMessage(), th);
            }
        }
    }

    /* compiled from: RewardMVVideoAdapter */
    private static final class e implements com.mintegral.msdk.videocommon.download.g.a {

        /* renamed from: a reason: collision with root package name */
        private c f2915a;
        private CampaignEx b;
        private long c = System.currentTimeMillis();
        private String d;
        private boolean e = true;

        public e(c cVar, CampaignEx campaignEx, String str, boolean z) {
            this.d = str;
            this.f2915a = cVar;
            this.b = campaignEx;
            this.e = z;
        }

        public final void a(String str) {
            try {
                if (this.f2915a.s != null) {
                    StringBuilder sb = new StringBuilder("H5SourceDownloadListener 源码下载成功 cid:");
                    sb.append(this.b.getId());
                    sb.append("  url:");
                    sb.append(str);
                    com.mintegral.msdk.base.utils.g.d("RewardMVVideoAdapter", sb.toString());
                    this.f2915a.s.removeMessages(5);
                    Message obtain = Message.obtain();
                    obtain.what = 8;
                    obtain.obj = this.b;
                    this.f2915a.s.sendMessage(obtain);
                }
                if (this.e) {
                    long currentTimeMillis = System.currentTimeMillis() - this.c;
                    w.a((com.mintegral.msdk.base.b.h) com.mintegral.msdk.base.b.i.a(com.mintegral.msdk.base.controller.a.d().h()));
                    p pVar = new p("2000043", 20, String.valueOf(currentTimeMillis), str, this.b.getId(), this.d, "", "2");
                    pVar.k(this.b.getRequestIdNotice());
                    pVar.m(this.b.getId());
                    if (this.b.getAdType() == 287) {
                        pVar.h("3");
                    } else if (this.b.getAdType() == 94) {
                        pVar.h("1");
                    }
                    com.mintegral.msdk.base.common.d.a.a(pVar, com.mintegral.msdk.base.controller.a.d().h(), this.d);
                }
            } catch (Throwable th) {
                com.mintegral.msdk.base.utils.g.c("RewardMVVideoAdapter", th.getMessage(), th);
            }
        }

        public final void a(String str, String str2) {
            String str3 = "RewardMVVideoAdapter";
            try {
                StringBuilder sb = new StringBuilder("H5SourceDownloadListener 源码下载失败 cid:");
                sb.append(this.b.getId());
                sb.append("  url:");
                sb.append(str);
                com.mintegral.msdk.base.utils.g.d(str3, sb.toString());
                if (this.b != null) {
                    com.mintegral.msdk.videocommon.a.a.a().b(this.b);
                }
                if (this.f2915a != null) {
                    c.a(this.f2915a, "H5 code resource download failed ", str);
                }
                if (this.e) {
                    long currentTimeMillis = System.currentTimeMillis() - this.c;
                    w.a((com.mintegral.msdk.base.b.h) com.mintegral.msdk.base.b.i.a(com.mintegral.msdk.base.controller.a.d().h()));
                    p pVar = new p("2000043", 21, String.valueOf(currentTimeMillis), str, this.b.getId(), this.d, "url download failed", "2");
                    pVar.k(this.b.getRequestIdNotice());
                    pVar.m(this.b.getId());
                    if (this.b.getAdType() == 287) {
                        pVar.h("3");
                    } else if (this.b.getAdType() == 94) {
                        pVar.h("1");
                    }
                    com.mintegral.msdk.base.common.d.a.a(pVar, com.mintegral.msdk.base.controller.a.d().h(), this.d);
                }
            } catch (Throwable th) {
                com.mintegral.msdk.base.utils.g.c("RewardMVVideoAdapter", th.getMessage(), th);
            }
        }
    }

    /* compiled from: RewardMVVideoAdapter */
    private static class f implements Runnable {

        /* renamed from: a reason: collision with root package name */
        private CampaignEx f2916a;
        private String b;
        private String c;
        private com.mintegral.msdk.videocommon.e.c d;
        private int e;
        private int f;
        private c g;

        public f(CampaignEx campaignEx, String str, String str2, com.mintegral.msdk.videocommon.e.c cVar, int i, int i2, c cVar2) {
            this.f2916a = campaignEx;
            this.b = str;
            this.c = str2;
            this.d = cVar;
            this.e = i;
            this.f = i2;
            this.g = cVar2;
        }

        public final void run() {
            String str = "RewardMVVideoAdapter";
            try {
                StringBuilder sb = new StringBuilder("retry load template url = ");
                sb.append(this.b);
                com.mintegral.msdk.base.utils.g.b(str, sb.toString());
                C0068a aVar = new C0068a();
                WindVaneWebView windVaneWebView = new WindVaneWebView(com.mintegral.msdk.base.controller.a.d().h());
                aVar.a(windVaneWebView);
                com.mintegral.msdk.video.js.a.h hVar = new com.mintegral.msdk.video.js.a.h(null, this.f2916a);
                hVar.a(this.e);
                hVar.a(this.c);
                hVar.a(this.d);
                j jVar = new j(aVar, this.f2916a, this.g, null, null);
                windVaneWebView.setWebViewListener(jVar);
                windVaneWebView.setObject(hVar);
                int i = this.f;
                if (i != 9) {
                    if (i == 16) {
                        windVaneWebView.loadUrl(this.b);
                    }
                    return;
                }
                windVaneWebView.loadDataWithBaseURL(this.f2916a.getRewardTemplateMode().d(), this.b, WebRequest.CONTENT_TYPE_HTML, "utf-8", null);
            } catch (Exception unused) {
            }
        }
    }

    /* compiled from: RewardMVVideoAdapter */
    private static final class g implements com.mintegral.msdk.videocommon.download.g.c {

        /* renamed from: a reason: collision with root package name */
        private CampaignEx f2917a;
        private c b;
        private long c = System.currentTimeMillis();
        private String d;
        private int e = 0;
        private com.mintegral.msdk.videocommon.e.c f;
        private boolean g = true;

        public g(CampaignEx campaignEx, c cVar, String str, int i, com.mintegral.msdk.videocommon.e.c cVar2, boolean z) {
            this.d = str;
            this.f2917a = campaignEx;
            this.e = i;
            this.f = cVar2;
            if (cVar != null) {
                this.b = cVar;
            }
            this.g = z;
        }

        /* JADX WARNING: Removed duplicated region for block: B:29:0x00df A[Catch:{ Throwable -> 0x0148 }] */
        /* JADX WARNING: Removed duplicated region for block: B:30:0x00ed A[Catch:{ Throwable -> 0x0148 }] */
        /* JADX WARNING: Removed duplicated region for block: B:33:0x010e A[Catch:{ Throwable -> 0x0148 }] */
        public final void a(String str) {
            p pVar;
            String str2 = str;
            try {
                long currentTimeMillis = System.currentTimeMillis() - this.c;
                w a2 = w.a((com.mintegral.msdk.base.b.h) com.mintegral.msdk.base.b.i.a(com.mintegral.msdk.base.controller.a.d().h()));
                if (this.e == 497) {
                    if (this.g) {
                        pVar = new p("2000043", 1, String.valueOf(currentTimeMillis), str, this.f2917a.getId(), this.d, "", "1");
                        pVar.k(this.f2917a.getRequestIdNotice());
                        pVar.m(this.f2917a.getId());
                        if (this.f2917a.getAdType() == 287) {
                            pVar.h("3");
                        } else if (this.f2917a.getAdType() == 94) {
                            pVar.h("1");
                        }
                        if (this.e == 497) {
                            com.mintegral.msdk.base.common.d.a.a(pVar, com.mintegral.msdk.base.controller.a.d().h(), this.d);
                        } else {
                            a2.a(pVar);
                        }
                        com.mintegral.msdk.videocommon.download.i.a().b(str2);
                        StringBuilder sb = new StringBuilder("RewardZipDownloadListener ZIP SUCCESS:");
                        sb.append(str2);
                        com.mintegral.msdk.base.utils.g.d("RewardMVVideoAdapter", sb.toString());
                        if (this.b == null && this.e != 859) {
                            synchronized (this.b) {
                                com.mintegral.msdk.base.utils.g.d("RewardMVVideoAdapter", "adapter 1286");
                                if (this.b.d() && this.b.s != null) {
                                    this.b.s.sendEmptyMessage(6);
                                    this.b.s.removeMessages(5);
                                    this.b = null;
                                }
                            }
                            return;
                        }
                    }
                } else if (this.e == 859) {
                    p pVar2 = new p();
                    pVar2.n("2000045");
                    if (this.b != null) {
                        this.b.a(this.f2917a, str2, this.d, this.f);
                        Context e2 = this.b.b;
                        if (e2 != null) {
                            pVar2.b(com.mintegral.msdk.base.utils.c.p(e2.getApplicationContext()));
                        }
                    }
                    pVar2.c(1);
                    if (this.f2917a != null) {
                        pVar2.m(this.f2917a.getId());
                        pVar2.k(this.f2917a.getRequestIdNotice());
                    }
                    pVar2.i(str2);
                    pVar2.o("");
                    pVar2.l(this.d);
                    pVar = pVar2;
                    if (this.e == 497) {
                    }
                    com.mintegral.msdk.videocommon.download.i.a().b(str2);
                    StringBuilder sb2 = new StringBuilder("RewardZipDownloadListener ZIP SUCCESS:");
                    sb2.append(str2);
                    com.mintegral.msdk.base.utils.g.d("RewardMVVideoAdapter", sb2.toString());
                    if (this.b == null) {
                    }
                } else if (this.e == 313) {
                    return;
                }
                pVar = null;
                if (this.e == 497) {
                }
                com.mintegral.msdk.videocommon.download.i.a().b(str2);
                StringBuilder sb22 = new StringBuilder("RewardZipDownloadListener ZIP SUCCESS:");
                sb22.append(str2);
                com.mintegral.msdk.base.utils.g.d("RewardMVVideoAdapter", sb22.toString());
                if (this.b == null) {
                }
            } catch (Throwable th) {
                com.mintegral.msdk.base.utils.g.c("RewardMVVideoAdapter", th.getMessage(), th);
            }
        }

        /* JADX WARNING: Removed duplicated region for block: B:29:0x00e6 A[Catch:{ Exception -> 0x0121, Throwable -> 0x0133 }] */
        /* JADX WARNING: Removed duplicated region for block: B:30:0x00f4 A[Catch:{ Exception -> 0x0121, Throwable -> 0x0133 }] */
        /* JADX WARNING: Removed duplicated region for block: B:33:0x00fb A[Catch:{ Exception -> 0x0121, Throwable -> 0x0133 }] */
        /* JADX WARNING: Removed duplicated region for block: B:36:0x0108 A[Catch:{ Exception -> 0x0121, Throwable -> 0x0133 }] */
        /* JADX WARNING: Removed duplicated region for block: B:48:0x014a  */
        public final void a(String str, String str2) {
            Exception exc;
            p pVar;
            String str3 = str;
            String str4 = str2;
            StringBuilder sb = new StringBuilder("RewardZipDownloadListener ZIP failed:");
            sb.append(str4);
            com.mintegral.msdk.base.utils.g.d("RewardMVVideoAdapter", sb.toString());
            try {
                long currentTimeMillis = System.currentTimeMillis() - this.c;
                w a2 = w.a((com.mintegral.msdk.base.b.h) com.mintegral.msdk.base.b.i.a(com.mintegral.msdk.base.controller.a.d().h()));
                if (this.e == 497) {
                    if (this.g) {
                        pVar = new p("2000043", 3, String.valueOf(currentTimeMillis), str2, this.f2917a.getId(), this.d, "zip download failed", "1");
                        pVar.k(this.f2917a.getRequestIdNotice());
                        pVar.m(this.f2917a.getId());
                        if (this.f2917a.getAdType() == 287) {
                            pVar.h("3");
                        } else if (this.f2917a.getAdType() == 94) {
                            pVar.h("1");
                        }
                        if (this.e == 497) {
                            com.mintegral.msdk.base.common.d.a.a(pVar, com.mintegral.msdk.base.controller.a.d().h(), this.d);
                        } else {
                            a2.a(pVar);
                        }
                        if (this.f2917a != null) {
                            com.mintegral.msdk.videocommon.a.a.a().b(this.f2917a);
                        }
                        if (this.b != null) {
                            StringBuilder sb2 = new StringBuilder("RewardZipDownloadListener ZIP failed:");
                            sb2.append(str4);
                            com.mintegral.msdk.base.utils.g.d("RewardMVVideoAdapter", sb2.toString());
                            c.a(this.b, str3, str4);
                        }
                        if (this.b != null) {
                            this.b = null;
                        }
                    }
                } else if (this.e == 859) {
                    p pVar2 = new p();
                    pVar2.n("2000045");
                    if (this.b != null) {
                        Context e2 = this.b.b;
                        if (e2 != null) {
                            pVar2.b(com.mintegral.msdk.base.utils.c.p(e2.getApplicationContext()));
                        }
                    }
                    pVar2.c(3);
                    if (this.f2917a != null) {
                        pVar2.m(this.f2917a.getId());
                        pVar2.k(this.f2917a.getRequestIdNotice());
                    }
                    pVar2.i(str4);
                    pVar2.o(str3);
                    pVar2.l(this.d);
                    pVar = pVar2;
                    if (this.e == 497) {
                    }
                    if (this.f2917a != null) {
                    }
                    if (this.b != null) {
                    }
                    if (this.b != null) {
                    }
                } else if (this.e == 313) {
                    return;
                }
                pVar = null;
                if (this.e == 497) {
                }
                if (this.f2917a != null) {
                }
                if (this.b != null) {
                }
            } catch (Exception e3) {
                exc = e3;
                if (this.b != null) {
                    this.b.k.a("clear error info failed");
                }
            } catch (Throwable th) {
                com.mintegral.msdk.base.utils.g.c("RewardMVVideoAdapter", th.getMessage(), th);
            }
            if (this.b != null) {
            }
            com.mintegral.msdk.base.utils.g.c("RewardMVVideoAdapter", exc.getMessage(), exc);
            if (this.b != null) {
            }
        }
    }

    /* compiled from: RewardMVVideoAdapter */
    public class h implements Runnable {
        private String b;

        public h(String str) {
            this.b = str;
        }

        public final void run() {
            String str = "RewardMVVideoAdapter";
            try {
                StringBuilder sb = new StringBuilder("=====getTtcRunnable 开始获取 mTtcIds:");
                sb.append(c.this.h);
                sb.append("  mExcludes:");
                sb.append(c.this.i);
                com.mintegral.msdk.base.utils.g.b(str, sb.toString());
                if (c.this.b != null) {
                    com.mintegral.msdk.base.b.i a2 = com.mintegral.msdk.base.b.i.a(c.this.b);
                    if (a2 != null) {
                        com.mintegral.msdk.base.b.d a3 = com.mintegral.msdk.base.b.d.a((com.mintegral.msdk.base.b.h) a2);
                        a3.c();
                        c.this.h = a3.a(c.this.c);
                    }
                }
                c.this.i = c.this.i();
                StringBuilder sb2 = new StringBuilder("=====getTtcRunnable 获取完毕 mTtcIds:");
                sb2.append(c.this.h);
                sb2.append("  mExcludes:");
                sb2.append(c.this.i);
                com.mintegral.msdk.base.utils.g.b("RewardMVVideoAdapter", sb2.toString());
                if (c.this.o) {
                    StringBuilder sb3 = new StringBuilder("=====getTtcRunnable 获取ttcid和excludeids超时 mIsGetTtcExcIdsTimeout：");
                    sb3.append(c.this.o);
                    sb3.append(" mIsGetTtcExcIdsSuccess:");
                    sb3.append(c.this.n);
                    com.mintegral.msdk.base.utils.g.b("RewardMVVideoAdapter", sb3.toString());
                    return;
                }
                StringBuilder sb4 = new StringBuilder("=====getTtcRunnable 获取ttcid和excludeids没有超时 mIsGetTtcExcIdsTimeout:");
                sb4.append(c.this.o);
                sb4.append(" mIsGetTtcExcIdsSuccess:");
                sb4.append(c.this.n);
                com.mintegral.msdk.base.utils.g.b("RewardMVVideoAdapter", sb4.toString());
                if (c.this.m != null) {
                    com.mintegral.msdk.base.utils.g.b("RewardMVVideoAdapter", "=====getTtcRunnable 删除 获取ttcid的超时任务");
                    c.this.s.removeCallbacks(c.this.m);
                }
                c.this.n = true;
                StringBuilder sb5 = new StringBuilder("=====getTtcRunnable 给handler发送消息 mTtcIds:");
                sb5.append(c.this.h);
                sb5.append("  mExcludes:");
                sb5.append(c.this.i);
                com.mintegral.msdk.base.utils.g.b("RewardMVVideoAdapter", sb5.toString());
                if (c.this.s != null) {
                    Message obtainMessage = c.this.s.obtainMessage();
                    obtainMessage.obj = this.b;
                    obtainMessage.what = 1;
                    c.this.s.sendMessage(obtainMessage);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /* compiled from: RewardMVVideoAdapter */
    public class i implements Runnable {
        private String b;

        public i(String str) {
            this.b = str;
        }

        public final void run() {
            String str = "RewardMVVideoAdapter";
            try {
                StringBuilder sb = new StringBuilder("=====超时task 开始执行 mTtcIds:");
                sb.append(c.this.h);
                sb.append("  RewardMVVideoAdapter.this.mExcludes:");
                sb.append(c.this.i);
                com.mintegral.msdk.base.utils.g.b(str, sb.toString());
                if (c.this.n) {
                    StringBuilder sb2 = new StringBuilder("超时task 已经成功获取ttcid excludeids mIsGetTtcExcIdsTimeout:");
                    sb2.append(c.this.o);
                    sb2.append(" mIsGetTtcExcIdsSuccess:");
                    sb2.append(c.this.n);
                    sb2.append("超时task不做处理");
                    com.mintegral.msdk.base.utils.g.b("RewardMVVideoAdapter", sb2.toString());
                    return;
                }
                StringBuilder sb3 = new StringBuilder("获取ttcid excludeids超时 mIsGetTtcExcIdsTimeout:");
                sb3.append(c.this.o);
                sb3.append(" mIsGetTtcExcIdsSuccess:");
                sb3.append(c.this.n);
                com.mintegral.msdk.base.utils.g.b("RewardMVVideoAdapter", sb3.toString());
                c.this.o = true;
                if (c.this.s != null) {
                    Message obtainMessage = c.this.s.obtainMessage();
                    obtainMessage.obj = this.b;
                    obtainMessage.what = 2;
                    c.this.s.sendMessage(obtainMessage);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /* compiled from: RewardMVVideoAdapter */
    private static class j extends com.mintegral.msdk.mtgjscommon.b.a {

        /* renamed from: a reason: collision with root package name */
        private c f2920a;
        private C0068a b;
        private CampaignEx c;
        private boolean d;
        private boolean e;
        private f f;
        private Handler g;

        public j(C0068a aVar, CampaignEx campaignEx, c cVar, f fVar, Handler handler) {
            this.b = aVar;
            if (cVar != null) {
                this.f2920a = cVar;
            }
            this.c = campaignEx;
            this.f = fVar;
            this.g = handler;
        }

        public final void a(int i) {
            if (!this.e) {
                if (!(this.f == null || this.g == null)) {
                    this.g.removeCallbacks(this.f);
                }
                if (this.b != null) {
                    this.b.c();
                }
                StringBuilder sb = new StringBuilder("templete preload readyState state = ");
                sb.append(i);
                com.mintegral.msdk.base.utils.g.a("WindVaneWebView", sb.toString());
                if (this.f2920a == null || !this.f2920a.q) {
                    if (this.c.isBidCampaign()) {
                        com.mintegral.msdk.base.utils.g.a("WindVaneWebView", "put templeteCache in bidRVCache ");
                        com.mintegral.msdk.videocommon.a.a(94, this.c.getRequestIdNotice(), this.b);
                    } else {
                        com.mintegral.msdk.base.utils.g.a("WindVaneWebView", "put templeteCache in rVCache ");
                        com.mintegral.msdk.videocommon.a.b(94, this.c.getRequestIdNotice(), this.b);
                    }
                } else if (this.c.isBidCampaign()) {
                    com.mintegral.msdk.base.utils.g.a("WindVaneWebView", "put templeteCache in bidIVCache ");
                    com.mintegral.msdk.videocommon.a.a(287, this.c.getRequestIdNotice(), this.b);
                } else {
                    com.mintegral.msdk.base.utils.g.a("WindVaneWebView", "put templeteCache in iVCache ");
                    com.mintegral.msdk.videocommon.a.b(287, this.c.getRequestIdNotice(), this.b);
                }
                try {
                    com.mintegral.msdk.base.utils.g.d("WindVaneWebView", "TempalteWindVaneWebviewClient tempalte load SUCCESS");
                    if (this.f2920a != null) {
                        synchronized (this.f2920a) {
                            com.mintegral.msdk.base.utils.g.d("WindVaneWebView", "adapter 341");
                            if (this.f2920a != null && this.f2920a.d()) {
                                com.mintegral.msdk.base.utils.g.d("WindVaneWebView", "TempalteWindVaneWebviewClient tempalte load  callback success");
                                if (this.f2920a.s != null) {
                                    Message obtain = Message.obtain();
                                    obtain.what = 6;
                                    obtain.obj = this.c;
                                    this.f2920a.s.sendMessage(obtain);
                                    this.f2920a.s.removeMessages(5);
                                    this.f2920a = null;
                                }
                            }
                        }
                    } else {
                        com.mintegral.msdk.base.utils.g.d("WindVaneWebView", "TempalteWindVaneWebviewClient tempalte load SUCCESS  mRewardMVVideoAdapter is null");
                    }
                } catch (Throwable th) {
                    com.mintegral.msdk.base.utils.g.c("WindVaneWebView", th.getMessage(), th);
                }
                this.e = true;
            }
        }

        public final void a(WebView webView, String str) {
            super.a(webView, str);
            if (!this.d) {
                com.mintegral.msdk.mtgjscommon.windvane.g.a();
                com.mintegral.msdk.mtgjscommon.windvane.g.a(webView, "onJSBridgeConnected", "");
                this.d = true;
            }
        }

        public final void a(WebView webView, int i, String str, String str2) {
            super.a(webView, i, str, str2);
            try {
                com.mintegral.msdk.base.utils.g.d("WindVaneWebView", "TempalteWindVaneWebviewClient tempalte load failed");
                if (this.f2920a != null) {
                    synchronized (this.f2920a) {
                        com.mintegral.msdk.base.utils.g.d("WindVaneWebView", "TempalteWindVaneWebviewClient tempalte load callback failed");
                        c.a(this.f2920a, str, str2);
                        this.f2920a = null;
                    }
                }
            } catch (Throwable th) {
                com.mintegral.msdk.base.utils.g.c("WindVaneWebView", th.getMessage(), th);
            }
        }
    }

    public final void a(boolean z) {
        this.q = z;
    }

    public final void b(boolean z) {
        this.r = z;
    }

    public final void a(int i2) {
        this.p = i2;
    }

    public final String a() {
        return this.c;
    }

    public c(Context context, String str) {
        try {
            this.b = context.getApplicationContext();
            this.c = str;
            b();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public final void b() {
        try {
            String j2 = com.mintegral.msdk.base.controller.a.d().j();
            com.mintegral.msdk.videocommon.e.b.a();
            this.l = com.mintegral.msdk.videocommon.e.b.a(j2, this.c);
            if (this.l == null) {
                this.l = com.mintegral.msdk.videocommon.e.b.d();
            }
        } catch (Throwable th) {
            com.mintegral.msdk.base.utils.g.c("RewardMVVideoAdapter", th.getMessage(), th);
        }
    }

    public final boolean c() {
        com.mintegral.msdk.videocommon.e.b.a();
        return com.mintegral.msdk.videocommon.a.a.a().a(this.c, com.mintegral.msdk.videocommon.e.b.b().e());
    }

    public final boolean d() {
        try {
            com.mintegral.msdk.base.utils.g.b("RewardMVVideoAdapter", "进来 isReady");
            com.mintegral.msdk.videocommon.download.c instance = com.mintegral.msdk.videocommon.download.c.getInstance();
            if (s.b(this.c) && instance != null) {
                j();
                List a2 = com.mintegral.msdk.videocommon.a.a.a().a(this.c, 1, this.r);
                String str = "RewardMVVideoAdapter";
                StringBuilder sb = new StringBuilder("camapignList.size() = ");
                sb.append(a2 != null ? a2.size() : 0);
                sb.append(" isBidCampaign = ");
                sb.append(this.r);
                com.mintegral.msdk.base.utils.g.a(str, sb.toString());
                if (a2 != null && a2.size() > 0) {
                    instance.createUnitCache(this.b, this.c, a2, 3, new a(this, (CampaignEx) a2.get(0)));
                    return instance.b(this.q ? 287 : 94, this.c, this.r);
                }
            }
        } catch (Exception e2) {
            StringBuilder sb2 = new StringBuilder();
            sb2.append(e2.getMessage());
            sb2.append("isReady 出错");
            com.mintegral.msdk.base.utils.g.d("RewardMVVideoAdapter", sb2.toString());
        }
        return false;
    }

    public final boolean e() {
        try {
            com.mintegral.msdk.base.utils.g.b("RewardMVVideoAdapter", "进来 isSpareOfferReady");
            com.mintegral.msdk.videocommon.download.c instance = com.mintegral.msdk.videocommon.download.c.getInstance();
            if (s.b(this.c) && instance != null) {
                j();
                List a2 = com.mintegral.msdk.videocommon.a.a.a().a(this.c, this.r);
                if (a2 != null && a2.size() > 0) {
                    instance.createUnitCache(this.b, this.c, a2, 3, new a(this, (CampaignEx) a2.get(0)));
                    return instance.b(this.q ? 287 : 94, this.c, this.r);
                }
            }
        } catch (Exception e2) {
            StringBuilder sb = new StringBuilder();
            sb.append(e2.getMessage());
            sb.append("isSpareOfferReady 出错");
            com.mintegral.msdk.base.utils.g.d("RewardMVVideoAdapter", sb.toString());
        }
        return false;
    }

    private boolean g() {
        boolean z = false;
        try {
            com.mintegral.msdk.base.utils.g.b("RewardMVVideoAdapter", "进来 isReadyDoSuccessful");
            com.mintegral.msdk.videocommon.download.c instance = com.mintegral.msdk.videocommon.download.c.getInstance();
            if (s.b(this.c) && instance != null) {
                j();
                List a2 = com.mintegral.msdk.videocommon.a.a.a().a(this.c, 1, this.r);
                if (a2 != null && a2.size() > 0) {
                    instance.createUnitCache(this.b, this.c, a2, 3, new a(this, (CampaignEx) a2.get(0)));
                    boolean b2 = instance.b(this.q ? 287 : 94, this.c, this.r);
                    if (b2) {
                        try {
                            com.mintegral.msdk.reward.d.a.a(this.b, (CampaignEx) a2.get(0), this.c);
                        } catch (Exception e2) {
                            boolean z2 = b2;
                            e = e2;
                            z = z2;
                        }
                    }
                    z = b2;
                }
            }
        } catch (Exception e3) {
            e = e3;
            e.printStackTrace();
            com.mintegral.msdk.base.utils.g.d("RewardMVVideoAdapter", "isReadyDoSuccessful 出错");
            StringBuilder sb = new StringBuilder("结果 isReadyDoSuccessful：");
            sb.append(z);
            com.mintegral.msdk.base.utils.g.b("RewardMVVideoAdapter", sb.toString());
            return z;
        }
        StringBuilder sb2 = new StringBuilder("结果 isReadyDoSuccessful：");
        sb2.append(z);
        com.mintegral.msdk.base.utils.g.b("RewardMVVideoAdapter", sb2.toString());
        return z;
    }

    public final void a(d dVar, String str, String str2, int i2) {
        try {
            this.j = dVar;
            com.mintegral.msdk.base.utils.g.b("RewardMVVideoAdapter", "show 进来");
            if (this.b != null) {
                if (!s.a(this.c)) {
                    com.mintegral.msdk.base.utils.g.b("RewardMVVideoAdapter", "show isReady true 打开播放器页面");
                    Intent intent = new Intent(this.b, MTGRewardVideoActivity.class);
                    intent.addFlags(268435456);
                    intent.putExtra("unitId", this.c);
                    intent.putExtra(MTGRewardVideoActivity.INTENT_REWARD, str);
                    intent.putExtra("mute", i2);
                    intent.putExtra(MTGRewardVideoActivity.INTENT_ISIV, this.q);
                    intent.putExtra(MTGRewardVideoActivity.INTENT_ISBID, this.r);
                    if (!TextUtils.isEmpty(str2)) {
                        intent.putExtra("userId", str2);
                    }
                    this.b.startActivity(intent);
                    return;
                }
            }
            if (this.j != null) {
                this.j.a("context or unitid is null");
            }
            com.mintegral.msdk.base.utils.g.b("RewardMVVideoAdapter", "show context munitid null");
        } catch (Exception e2) {
            e2.printStackTrace();
            if (this.j != null) {
                d dVar2 = this.j;
                StringBuilder sb = new StringBuilder("show failed, exception is ");
                sb.append(e2.getMessage());
                dVar2.a(sb.toString());
            }
        }
    }

    public final void f() {
        a(1, 8000, false, "");
    }

    public final void a(int i2, int i3, boolean z, String str) {
        this.e = i2;
        this.f = i3;
        this.g = z;
        if (this.b == null) {
            b("Context is null");
        } else if (s.a(this.c)) {
            b("UnitId is null");
        } else if (this.l == null) {
            b("RewardUnitSetting is null");
        } else {
            try {
                if (com.mintegral.msdk.base.common.a.c.f2510a != null && com.mintegral.msdk.base.common.a.c.f2510a.size() > 0) {
                    com.mintegral.msdk.base.common.a.c.f2510a.clear();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
            try {
                l.a((com.mintegral.msdk.base.b.h) com.mintegral.msdk.base.b.i.a(this.b)).b(this.c);
            } catch (Exception e3) {
                e3.printStackTrace();
            }
            com.mintegral.msdk.base.utils.g.b("RewardMVVideoAdapter", "load 开始清除过期数据");
            j();
            if (!this.r) {
                List k2 = k();
                if (k2 != null && k2.size() > 0) {
                    com.mintegral.msdk.base.utils.g.a("RewardMVVideoAdapter", "==本地campaign条数 大于0");
                    e(k2);
                    d(k2);
                    if (k2.size() >= this.l.z()) {
                        a(k2);
                        StringBuilder sb = new StringBuilder("load 本地已有缓存 返回load成功 vcn：");
                        sb.append(this.l.z());
                        com.mintegral.msdk.base.utils.g.b("RewardMVVideoAdapter", sb.toString());
                        h();
                        return;
                    }
                    a(k2, this.c, this.l);
                    if (z && !this.l.b(3)) {
                        return;
                    }
                }
            } else {
                CampaignEx a2 = a(this.c, str);
                if (a2 != null && !a(a2)) {
                    c(a2);
                    com.mintegral.msdk.base.utils.g.b("RewardMVVideoAdapter", "bid load 本地已有缓存 返回load成功 ");
                    h();
                    return;
                }
            }
            new Thread(new h(str)).start();
            if (this.s != null) {
                this.m = new i(str);
                this.s.postDelayed(this.m, 90000);
                return;
            }
            com.mintegral.msdk.base.utils.g.b("RewardMVVideoAdapter", "handler 为空 直接load");
            a(str);
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(2:42|43) */
    /* JADX WARNING: Code restructure failed: missing block: B:43:?, code lost:
        r0 = com.mintegral.msdk.base.common.a.i;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:42:0x0157 */
    public final void a(String str) {
        int i2;
        String str2;
        String str3 = str;
        try {
            if (this.b == null) {
                b("Context is null");
            } else if (s.a(this.c)) {
                b("UnitId is null");
            } else if (this.l == null) {
                b("RewardUnitSetting is null");
            } else {
                com.mintegral.msdk.base.utils.g.b("RewardMVVideoAdapter", "load 开始准备请求参数");
                String j2 = com.mintegral.msdk.base.controller.a.d().j();
                StringBuilder sb = new StringBuilder();
                sb.append(com.mintegral.msdk.base.controller.a.d().j());
                sb.append(com.mintegral.msdk.base.controller.a.d().k());
                String md5 = CommonMD5.getMD5(sb.toString());
                int i3 = this.g ? 2 : 3;
                int i4 = 0;
                if (this.l != null) {
                    i4 = this.l.v();
                    i2 = this.l.x();
                } else {
                    i2 = 0;
                }
                String str4 = "1";
                String str5 = "1";
                String str6 = this.i;
                String str7 = this.h;
                String o2 = o();
                String p2 = p();
                this.d = l();
                String n2 = n();
                int i5 = this.e;
                int i6 = this.q ? 287 : 94;
                int i7 = i2;
                com.mintegral.msdk.base.common.net.l lVar = new com.mintegral.msdk.base.common.net.l();
                n.a(lVar, "app_id", j2);
                n.a(lVar, MIntegralConstans.PROPERTIES_UNIT_ID, this.c);
                n.a(lVar, "sign", md5);
                n.a(lVar, "req_type", String.valueOf(i3));
                n.a(lVar, "ad_num", String.valueOf(i4));
                String str8 = "tnum";
                StringBuilder sb2 = new StringBuilder();
                sb2.append(this.q ? 1 : i7);
                n.a(lVar, str8, sb2.toString());
                n.a(lVar, "only_impression", str4);
                n.a(lVar, "ping_mode", str5);
                n.a(lVar, "ttc_ids", str7);
                n.a(lVar, "display_cids", o2);
                n.a(lVar, "exclude_ids", str6);
                n.a(lVar, "install_ids", p2);
                n.a(lVar, CampaignEx.JSON_KEY_AD_SOURCE_ID, String.valueOf(i5));
                n.a(lVar, "session_id", n2);
                n.a(lVar, "ad_type", String.valueOf(i6));
                StringBuilder sb3 = new StringBuilder();
                sb3.append(this.d);
                n.a(lVar, "offset", sb3.toString());
                String str9 = com.mintegral.msdk.base.common.a.k;
                if (!TextUtils.isEmpty(str)) {
                    str2 = str;
                    lVar.a("token", str2);
                    String[] split = str2.split(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
                    if (split == null || split.length <= 1) {
                        str9 = com.mintegral.msdk.base.common.a.i;
                    } else {
                        StringBuilder sb4 = new StringBuilder("https://");
                        sb4.append(split[1]);
                        sb4.append("-hb.rayjump.com/load");
                        str9 = sb4.toString();
                    }
                } else {
                    str2 = str;
                }
                com.mintegral.msdk.reward.e.b bVar = new com.mintegral.msdk.reward.e.b(this.b);
                AnonymousClass2 r4 = new com.mintegral.msdk.reward.e.a() {
                    public final void a(CampaignUnit campaignUnit) {
                        try {
                            com.mintegral.msdk.base.utils.g.b("RewardMVVideoAdapter", "onLoadCompaginSuccess 数据刚请求回来");
                            c.a(c.this, campaignUnit);
                        } catch (Exception e) {
                            e.printStackTrace();
                            com.mintegral.msdk.base.utils.g.b("RewardMVVideoAdapter", "onLoadCompaginSuccess 数据刚请求失败");
                            c.this.b("Exception after load success");
                            c.this.m();
                        }
                    }

                    public final void a(int i, String str) {
                        com.mintegral.msdk.base.utils.g.d("RewardMVVideoAdapter", str);
                        StringBuilder sb = new StringBuilder("onLoadCompaginFailed load failed errorCode:");
                        sb.append(i);
                        sb.append(" msg:");
                        sb.append(str);
                        com.mintegral.msdk.base.utils.g.b("RewardMVVideoAdapter", sb.toString());
                        c.this.b(str);
                        c.this.m();
                    }
                };
                r4.a(str2);
                bVar.a(str9, lVar, (com.mintegral.msdk.base.common.net.d<?>) r4);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            b("Load exception");
            m();
        }
    }

    private void h() {
        if (this.s != null) {
            this.s.sendEmptyMessage(3);
        }
    }

    /* access modifiers changed from: private */
    public void b(String str) {
        if (this.s != null) {
            if (TextUtils.isEmpty(str)) {
                this.s.sendEmptyMessage(4);
                return;
            }
            Message obtain = Message.obtain();
            obtain.what = 4;
            obtain.obj = str;
            this.s.sendMessage(obtain);
        }
    }

    /* access modifiers changed from: private */
    public String i() {
        String str = "";
        try {
            com.mintegral.msdk.b.b.a();
            com.mintegral.msdk.b.a b2 = com.mintegral.msdk.b.b.b(com.mintegral.msdk.base.controller.a.d().j());
            JSONArray jSONArray = new JSONArray();
            if (b2 != null && b2.aK() == 1) {
                StringBuilder sb = new StringBuilder("excludes cfc:");
                sb.append(b2.aK());
                com.mintegral.msdk.base.utils.g.b("RewardMVVideoAdapter", sb.toString());
                long[] c2 = m.a((com.mintegral.msdk.base.b.h) com.mintegral.msdk.base.b.i.a(com.mintegral.msdk.base.controller.a.d().h())).c();
                if (c2 != null) {
                    for (long j2 : c2) {
                        StringBuilder sb2 = new StringBuilder("excludes campaignIds:");
                        sb2.append(c2);
                        com.mintegral.msdk.base.utils.g.b("RewardMVVideoAdapter", sb2.toString());
                        jSONArray.put(j2);
                    }
                }
            }
            List q2 = q();
            if (q2 != null && q2.size() > 0) {
                for (int i2 = 0; i2 < q2.size(); i2++) {
                    String str2 = (String) q2.get(i2);
                    if (s.b(str2)) {
                        try {
                            jSONArray.put(Long.parseLong(str2));
                        } catch (Exception e2) {
                            e2.printStackTrace();
                        }
                    }
                }
            }
            if (jSONArray.length() > 0) {
                str = k.a(jSONArray);
            }
            StringBuilder sb3 = new StringBuilder("get excludes:");
            sb3.append(str);
            com.mintegral.msdk.base.utils.g.b("RewardMVVideoAdapter", sb3.toString());
        } catch (Exception e3) {
            e3.printStackTrace();
        }
        return str;
    }

    private void j() {
        if (com.mintegral.msdk.videocommon.a.a.a() != null) {
            com.mintegral.msdk.b.b.a();
            com.mintegral.msdk.b.a b2 = com.mintegral.msdk.b.b.b(com.mintegral.msdk.base.controller.a.d().j());
            if (b2 == null) {
                com.mintegral.msdk.b.b.a();
                b2 = com.mintegral.msdk.b.b.b();
            }
            com.mintegral.msdk.videocommon.a.a.a().b(b2.ak() * 1000, this.c);
        }
    }

    private List<CampaignEx> k() {
        try {
            if (com.mintegral.msdk.videocommon.a.a.a() != null) {
                return com.mintegral.msdk.videocommon.a.a.a().a(this.c, this.e, this.r);
            }
            return null;
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }

    private static boolean a(CampaignEx campaignEx) {
        try {
            if (com.mintegral.msdk.videocommon.a.a.a() != null) {
                com.mintegral.msdk.videocommon.a.a.a();
                return com.mintegral.msdk.videocommon.a.a.a(campaignEx);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return true;
    }

    private static CampaignEx a(String str, String str2) {
        try {
            if (com.mintegral.msdk.videocommon.a.a.a() != null) {
                return com.mintegral.msdk.videocommon.a.a.a().a(str, str2);
            }
            return null;
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public final void a(b bVar) {
        if (bVar != null) {
            com.mintegral.msdk.base.utils.g.b("RewardMVVideoAdapter", "======set listener is not null");
        } else {
            com.mintegral.msdk.base.utils.g.b("RewardMVVideoAdapter", "======set listener is  null");
        }
        this.k = bVar;
    }

    private static void b(CampaignEx campaignEx) {
        try {
            com.mintegral.msdk.videocommon.a.a a2 = com.mintegral.msdk.videocommon.a.a.a();
            if (a2 != null) {
                a2.b(campaignEx);
            }
        } catch (Throwable th) {
            com.mintegral.msdk.base.utils.g.c("RewardMVVideoAdapter", th.getMessage(), th);
        }
    }

    private void c(CampaignEx campaignEx) {
        if (campaignEx != null) {
            if (!TextUtils.isEmpty(campaignEx.getEndScreenUrl())) {
                com.mintegral.msdk.videocommon.download.g.a().b(campaignEx.getEndScreenUrl());
            }
            if (campaignEx.getRewardTemplateMode() != null) {
                com.mintegral.msdk.base.entity.CampaignEx.c rewardTemplateMode = campaignEx.getRewardTemplateMode();
                if (!TextUtils.isEmpty(rewardTemplateMode.c())) {
                    if (rewardTemplateMode.c().contains(".zip")) {
                        g gVar = new g(campaignEx, this, this.c, 313, this.l, false);
                        com.mintegral.msdk.videocommon.download.g.a().a(rewardTemplateMode.c(), (com.mintegral.msdk.videocommon.download.g.c) gVar);
                    } else {
                        com.mintegral.msdk.videocommon.download.g.a().b(rewardTemplateMode.c());
                    }
                }
                if (!TextUtils.isEmpty(rewardTemplateMode.d())) {
                    if (rewardTemplateMode.d().contains(".zip")) {
                        g gVar2 = new g(campaignEx, this, this.c, 859, this.l, false);
                        com.mintegral.msdk.videocommon.download.g.a().a(rewardTemplateMode.d(), (com.mintegral.msdk.videocommon.download.g.c) gVar2);
                        return;
                    }
                    com.mintegral.msdk.videocommon.download.g.a().a(rewardTemplateMode.d(), (com.mintegral.msdk.videocommon.download.g.a) new C0064c(this, campaignEx, this.c, this.l));
                }
            }
        }
    }

    private void a(List<CampaignEx> list) {
        if (list != null && list.size() > 0) {
            for (CampaignEx c2 : list) {
                c(c2);
            }
        }
    }

    private void b(List<CampaignEx> list) {
        if (list != null && list.size() > 0) {
            for (CampaignEx campaignEx : list) {
                String str = campaignEx.getendcard_url();
                if (!TextUtils.isEmpty(str) && !campaignEx.isMraid()) {
                    if (!str.contains(".zip") || !str.contains("md5filename")) {
                        com.mintegral.msdk.videocommon.download.g.a().a(str, (com.mintegral.msdk.videocommon.download.g.a) new e(this, campaignEx, this.c, TextUtils.isEmpty(com.mintegral.msdk.videocommon.download.h.a().a(str))));
                    } else {
                        g gVar = new g(campaignEx, this, this.c, 497, this.l, TextUtils.isEmpty(com.mintegral.msdk.videocommon.download.j.a().a(str)));
                        com.mintegral.msdk.videocommon.download.g.a().a(str, (com.mintegral.msdk.videocommon.download.g.c) gVar);
                    }
                }
            }
        }
    }

    private void a(List<CampaignEx> list, String str, com.mintegral.msdk.videocommon.e.c cVar) {
        C0068a aVar;
        if (list != null) {
            try {
                for (CampaignEx campaignEx : list) {
                    if (campaignEx.getRewardTemplateMode() != null && !TextUtils.isEmpty(campaignEx.getRewardTemplateMode().d())) {
                        if (this.q) {
                            aVar = com.mintegral.msdk.videocommon.a.a(287, campaignEx);
                        } else {
                            aVar = com.mintegral.msdk.videocommon.a.a(94, campaignEx);
                        }
                        if (aVar != null) {
                            if (!(aVar.a() == null || aVar.a().getParent() == null)) {
                                if (this.q) {
                                    com.mintegral.msdk.videocommon.a.b(287, campaignEx);
                                } else {
                                    com.mintegral.msdk.videocommon.a.b(94, campaignEx);
                                }
                            }
                        }
                        a(campaignEx, campaignEx.getRewardTemplateMode().d(), str, cVar);
                    }
                }
            } catch (Exception e2) {
                if (MIntegralConstans.DEBUG) {
                    e2.printStackTrace();
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(CampaignEx campaignEx, String str, String str2, com.mintegral.msdk.videocommon.e.c cVar) {
        try {
            if (!TextUtils.isEmpty(str)) {
                Object[] objArr = new Object[4];
                int i2 = 16;
                if (str.contains("zip")) {
                    str = com.mintegral.msdk.videocommon.download.j.a().a(str);
                } else {
                    String a2 = com.mintegral.msdk.videocommon.download.h.a().a(str);
                    if (!TextUtils.isEmpty(a2)) {
                        i2 = 9;
                        str = a2;
                    }
                }
                Message obtain = Message.obtain();
                obtain.what = i2;
                objArr[0] = campaignEx;
                objArr[1] = str;
                objArr[2] = str2;
                objArr[3] = cVar;
                obtain.obj = objArr;
                this.s.sendMessage(obtain);
            }
        } catch (Exception unused) {
        }
    }

    private void c(List<CampaignEx> list) {
        if (list != null && list.size() > 0) {
            for (CampaignEx campaignEx : list) {
                if (!TextUtils.isEmpty(campaignEx.getIconUrl())) {
                    com.mintegral.msdk.base.common.c.b.a(com.mintegral.msdk.base.controller.a.d().h()).a(campaignEx.getIconUrl(), (com.mintegral.msdk.base.common.c.c) new b(this, campaignEx, this.c));
                }
                if (!TextUtils.isEmpty(campaignEx.getImageUrl())) {
                    com.mintegral.msdk.base.common.c.b.a(com.mintegral.msdk.base.controller.a.d().h()).a(campaignEx.getImageUrl(), (com.mintegral.msdk.base.common.c.c) new b(this, campaignEx, this.c));
                }
            }
        }
    }

    private void d(List<CampaignEx> list) {
        if (list != null) {
            try {
                if (list.size() > 0) {
                    for (CampaignEx campaignEx : list) {
                        if (!(campaignEx.getRewardTemplateMode() == null || campaignEx.getRewardTemplateMode().e() == null)) {
                            List<com.mintegral.msdk.base.entity.CampaignEx.c.a> e2 = campaignEx.getRewardTemplateMode().e();
                            if (e2 != null) {
                                for (com.mintegral.msdk.base.entity.CampaignEx.c.a aVar : e2) {
                                    if (!(aVar == null || aVar.b == null)) {
                                        for (String str : aVar.b) {
                                            if (s.b(str)) {
                                                com.mintegral.msdk.base.common.c.b.a(com.mintegral.msdk.base.controller.a.d().h()).a(str, (com.mintegral.msdk.base.common.c.c) new d(this, campaignEx, this.c));
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            } catch (Throwable th) {
                if (MIntegralConstans.DEBUG) {
                    th.printStackTrace();
                }
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0017, code lost:
        if (r1 <= 0) goto L_0x0019;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x014f, code lost:
        if (com.mintegral.msdk.base.utils.s.a(r5.getVideoUrlEncode()) != false) goto L_0x0151;
     */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x00f8 A[SYNTHETIC, Splitter:B:47:0x00f8] */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x011e A[Catch:{ Exception -> 0x0191 }] */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x0156 A[Catch:{ Exception -> 0x0191 }] */
    /* JADX WARNING: Removed duplicated region for block: B:94:0x0175 A[SYNTHETIC] */
    private List<CampaignEx> a(CampaignUnit campaignUnit) {
        int i2;
        boolean z;
        ArrayList arrayList = new ArrayList();
        try {
            k.a((List<CampaignEx>) campaignUnit.getAds());
            if (this.l != null) {
                i2 = this.l.x();
            }
            i2 = 1;
            if (!(campaignUnit == null || campaignUnit.getAds() == null || campaignUnit.getAds().size() <= 0)) {
                ArrayList ads = campaignUnit.getAds();
                StringBuilder sb = new StringBuilder("onload 总共返回 的compaign有：");
                sb.append(ads.size());
                com.mintegral.msdk.base.utils.g.b("RewardMVVideoAdapter", sb.toString());
                int i3 = 0;
                while (i3 < ads.size() && i3 < i2) {
                    CampaignEx campaignEx = (CampaignEx) ads.get(i3);
                    if (campaignEx.isMraid()) {
                        if (!TextUtils.isEmpty(campaignEx.getMraid().trim())) {
                            FileOutputStream fileOutputStream = null;
                            String str = "";
                            if (campaignEx.getAdType() == 287) {
                                str = "3";
                            } else if (campaignEx.getAdType() == 94) {
                                str = "1";
                            } else if (campaignEx.getAdType() == 42) {
                                str = "2";
                            }
                            try {
                                String b2 = com.mintegral.msdk.base.common.b.e.b(com.mintegral.msdk.base.common.b.c.MINTEGRAL_700_HTML);
                                String md5 = CommonMD5.getMD5(campaignEx.getMraid());
                                if (TextUtils.isEmpty(md5)) {
                                    md5 = String.valueOf(System.currentTimeMillis());
                                }
                                File file = new File(b2, md5.concat(".html"));
                                FileOutputStream fileOutputStream2 = new FileOutputStream(file);
                                try {
                                    fileOutputStream2.write(campaignEx.getMraid().getBytes());
                                    fileOutputStream2.flush();
                                    campaignEx.setMraid(file.getAbsolutePath());
                                    com.mintegral.msdk.base.common.d.a.a(campaignEx, "", this.c, str);
                                    fileOutputStream2.close();
                                } catch (Exception e2) {
                                    FileOutputStream fileOutputStream3 = fileOutputStream2;
                                    e = e2;
                                    fileOutputStream = fileOutputStream3;
                                    try {
                                        e.printStackTrace();
                                        campaignEx.setMraid("");
                                        com.mintegral.msdk.base.common.d.a.a(campaignEx, e.getMessage(), this.c, str);
                                        if (fileOutputStream != null) {
                                            fileOutputStream.close();
                                        }
                                        File file2 = new File(campaignEx.getMraid());
                                        b("mraid resource write fail");
                                        i3++;
                                    } catch (Throwable th) {
                                        th = th;
                                        if (fileOutputStream != null) {
                                            fileOutputStream.close();
                                        }
                                        throw th;
                                    }
                                } catch (Throwable th2) {
                                    th = th2;
                                    fileOutputStream = fileOutputStream2;
                                    if (fileOutputStream != null) {
                                    }
                                    throw th;
                                }
                            } catch (Exception e3) {
                                e = e3;
                                e.printStackTrace();
                                campaignEx.setMraid("");
                                com.mintegral.msdk.base.common.d.a.a(campaignEx, e.getMessage(), this.c, str);
                                if (fileOutputStream != null) {
                                }
                                File file22 = new File(campaignEx.getMraid());
                                b("mraid resource write fail");
                                i3++;
                            }
                            File file222 = new File(campaignEx.getMraid());
                            if (!file222.exists() || !file222.isFile() || !file222.canRead()) {
                                b("mraid resource write fail");
                            }
                        }
                        i3++;
                    }
                    if (!(campaignEx == null || campaignEx.getOfferType() == 99)) {
                        if (d(campaignEx)) {
                            if (s.a(campaignEx.getendcard_url()) && TextUtils.isEmpty(campaignEx.getMraid())) {
                            }
                            z = true;
                            if (z) {
                                if (!k.a(this.b, campaignEx.getPackageName())) {
                                    arrayList.add(campaignEx);
                                } else if (k.b(campaignEx) || k.a(campaignEx)) {
                                    arrayList.add(campaignEx);
                                }
                            }
                        }
                        z = false;
                        if (z) {
                        }
                    }
                    i3++;
                }
                StringBuilder sb2 = new StringBuilder("onload 返回有以下带有视频素材的compaign：");
                sb2.append(arrayList.size());
                com.mintegral.msdk.base.utils.g.b("RewardMVVideoAdapter", sb2.toString());
            }
        } catch (Exception e4) {
            e4.printStackTrace();
        }
        return arrayList;
    }

    private void e(List<CampaignEx> list) {
        try {
            com.mintegral.msdk.base.utils.g.b("RewardMVVideoAdapter", "===准备下载");
            if (list == null || list.size() <= 0) {
                com.mintegral.msdk.base.utils.g.b("RewardMVVideoAdapter", "onload 不用下载视频素材 size为0");
            } else {
                StringBuilder sb = new StringBuilder("onload 开始下载视频素材 size:");
                sb.append(list.size());
                com.mintegral.msdk.base.utils.g.b("RewardMVVideoAdapter", sb.toString());
                this.f2907a.clear();
                this.f2907a.addAll(list);
                com.mintegral.msdk.videocommon.download.i.a().a(list);
                if (com.mintegral.msdk.videocommon.download.c.getInstance() != null) {
                    com.mintegral.msdk.videocommon.download.c.getInstance().createUnitCache(this.b, this.c, list, 3, new a(this, (CampaignEx) list.get(0)));
                    com.mintegral.msdk.videocommon.download.c.getInstance().load(this.c);
                }
            }
        } catch (Exception e2) {
            com.mintegral.msdk.base.utils.g.c("RewardMVVideoAdapter", e2.getMessage(), e2);
        }
    }

    private int l() {
        try {
            int a2 = s.b(this.c) ? com.mintegral.msdk.reward.b.a.a(this.c) : 0;
            if (this.l == null || a2 > this.l.E()) {
                return 0;
            }
            return a2;
        } catch (Exception e2) {
            e2.printStackTrace();
            return 0;
        }
    }

    /* access modifiers changed from: private */
    public void m() {
        try {
            if (s.b(this.c)) {
                com.mintegral.msdk.reward.b.a.a(this.c, 0);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    private static String n() {
        String str = "";
        try {
            if (s.b(com.mintegral.msdk.reward.b.a.f2921a)) {
                return com.mintegral.msdk.reward.b.a.f2921a;
            }
            return str;
        } catch (Exception e2) {
            e2.printStackTrace();
            return str;
        }
    }

    private String o() {
        String str = "";
        try {
            if (com.mintegral.msdk.base.common.a.c.b == null) {
                return str;
            }
            Map<String, List<String>> map = com.mintegral.msdk.base.common.a.c.b;
            if (!s.b(this.c) || !map.containsKey(this.c)) {
                return str;
            }
            List list = (List) map.get(this.c);
            if (list == null || list.size() <= 0) {
                return str;
            }
            return list.toString();
        } catch (Exception e2) {
            e2.printStackTrace();
            return str;
        }
    }

    private static String p() {
        String str = "";
        try {
            JSONArray jSONArray = new JSONArray();
            com.mintegral.msdk.base.controller.a.d();
            List<Long> g2 = com.mintegral.msdk.base.controller.a.g();
            if (g2 != null && g2.size() > 0) {
                for (Long longValue : g2) {
                    jSONArray.put(longValue.longValue());
                }
            }
            if (jSONArray.length() > 0) {
                return k.a(jSONArray);
            }
            return str;
        } catch (Exception e2) {
            e2.printStackTrace();
            return str;
        }
    }

    private List<String> q() {
        try {
            return l.a((com.mintegral.msdk.base.b.h) com.mintegral.msdk.base.b.i.a(this.b)).a(this.c);
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }

    private static boolean d(CampaignEx campaignEx) {
        if (campaignEx != null) {
            try {
                if (campaignEx.getPlayable_ads_without_video() == 2) {
                    return true;
                }
            } catch (Throwable th) {
                if (MIntegralConstans.DEBUG) {
                    th.printStackTrace();
                }
            }
        }
        return false;
    }

    static /* synthetic */ void a(c cVar, CampaignEx campaignEx, String str, int i2, String str2, com.mintegral.msdk.videocommon.e.c cVar2) {
        c cVar3 = cVar;
        int i3 = i2;
        try {
            C0068a aVar = new C0068a();
            WindVaneWebView windVaneWebView = new WindVaneWebView(com.mintegral.msdk.base.controller.a.d().h());
            aVar.a(windVaneWebView);
            com.mintegral.msdk.video.js.a.h hVar = new com.mintegral.msdk.video.js.a.h(null, campaignEx);
            hVar.a(cVar3.p);
            hVar.a(str2);
            hVar.a(cVar2);
            f fVar = new f(campaignEx, str, str2, cVar2, cVar3.p, i2, cVar);
            j jVar = new j(aVar, campaignEx, cVar, fVar, cVar3.s);
            windVaneWebView.setWebViewListener(jVar);
            windVaneWebView.setObject(hVar);
            if (i3 == 9) {
                String str3 = str;
                windVaneWebView.loadDataWithBaseURL(campaignEx.getRewardTemplateMode().d(), str, WebRequest.CONTENT_TYPE_HTML, "utf-8", null);
            } else if (i3 == 16) {
                windVaneWebView.loadUrl(str);
            }
            cVar3.s.postDelayed(fVar, DefaultRenderersFactory.DEFAULT_ALLOWED_VIDEO_JOINING_TIME_MS);
        } catch (Exception unused) {
        }
    }

    static /* synthetic */ void a(c cVar, String str, String str2) {
        try {
            com.mintegral.msdk.base.utils.g.d("RewardMVVideoAdapter", "====delCampaignFromDownLoadCampaignListByUrld");
            if (cVar.f2907a != null && !TextUtils.isEmpty(str2)) {
                Iterator it = cVar.f2907a.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        break;
                    }
                    CampaignEx campaignEx = (CampaignEx) it.next();
                    if (campaignEx != null) {
                        String videoUrlEncode = campaignEx.getVideoUrlEncode();
                        if (!TextUtils.isEmpty(videoUrlEncode) && str2.equals(videoUrlEncode)) {
                            cVar.f2907a.remove(campaignEx);
                            b(campaignEx);
                            break;
                        }
                        String str3 = campaignEx.getendcard_url();
                        if (!TextUtils.isEmpty(str3) && str2.equals(str3)) {
                            cVar.f2907a.remove(campaignEx);
                            b(campaignEx);
                            break;
                        }
                        com.mintegral.msdk.base.entity.CampaignEx.c rewardTemplateMode = campaignEx.getRewardTemplateMode();
                        if (rewardTemplateMode != null) {
                            List e2 = rewardTemplateMode.e();
                            if (e2 != null) {
                                Iterator it2 = e2.iterator();
                                while (true) {
                                    if (!it2.hasNext()) {
                                        break;
                                    }
                                    com.mintegral.msdk.base.entity.CampaignEx.c.a aVar = (com.mintegral.msdk.base.entity.CampaignEx.c.a) it2.next();
                                    if (aVar != null && aVar.b != null && aVar.b.contains(str2)) {
                                        cVar.f2907a.remove(campaignEx);
                                        b(campaignEx);
                                        break;
                                    }
                                }
                            }
                            String d2 = rewardTemplateMode.d();
                            if (!TextUtils.isEmpty(d2) && str2.equals(d2)) {
                                cVar.f2907a.remove(campaignEx);
                                b(campaignEx);
                                break;
                            }
                        } else {
                            continue;
                        }
                    }
                }
                if (!(cVar.k == null || cVar.f2907a == null || cVar.f2907a.size() != 0)) {
                    if (cVar.s != null) {
                        cVar.s.removeMessages(5);
                    }
                    com.mintegral.msdk.base.utils.g.d("RewardMVVideoAdapter", "====del campaign and callback failed");
                    cVar.k.a(str);
                }
            } else if (cVar.k != null) {
                if (cVar.s != null) {
                    cVar.s.removeMessages(5);
                }
                com.mintegral.msdk.base.utils.g.d("RewardMVVideoAdapter", "====del campaign and callback failed");
                cVar.k.a(str);
            }
        } catch (Throwable th) {
            com.mintegral.msdk.base.utils.g.c("RewardMVVideoAdapter", th.getMessage(), th);
        }
    }

    static /* synthetic */ void a(c cVar, final CampaignUnit campaignUnit) {
        final List a2 = cVar.a(campaignUnit);
        if (a2.size() > 0) {
            StringBuilder sb = new StringBuilder("onload load成功 size:");
            sb.append(a2.size());
            com.mintegral.msdk.base.utils.g.b("RewardMVVideoAdapter", sb.toString());
            cVar.h();
        } else {
            com.mintegral.msdk.base.utils.g.b("RewardMVVideoAdapter", "onload load失败 返回的compaign 没有带视频素材");
            cVar.b("No video campaign");
        }
        if (campaignUnit != null) {
            String sessionId = campaignUnit.getSessionId();
            if (s.b(sessionId)) {
                StringBuilder sb2 = new StringBuilder("onload sessionId:");
                sb2.append(sessionId);
                com.mintegral.msdk.base.utils.g.b("RewardMVVideoAdapter", sb2.toString());
                com.mintegral.msdk.reward.b.a.f2921a = sessionId;
            }
        }
        try {
            if (a2.size() > 0) {
                cVar.d += a2.size();
            }
            if (cVar.l == null || cVar.d > cVar.l.E()) {
                com.mintegral.msdk.base.utils.g.b("RewardMVVideoAdapter", "onload 重置offset为0");
                cVar.d = 0;
            }
            StringBuilder sb3 = new StringBuilder("onload 算出 下次的offset是:");
            sb3.append(cVar.d);
            com.mintegral.msdk.base.utils.g.b("RewardMVVideoAdapter", sb3.toString());
            if (s.b(cVar.c)) {
                com.mintegral.msdk.reward.b.a.a(cVar.c, cVar.d);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        new Thread(new Runnable() {
            public final void run() {
                com.mintegral.msdk.base.utils.g.b("RewardMVVideoAdapter", "在子线程处理业务逻辑 开始");
                if (a2 != null && a2.size() > 0) {
                    StringBuilder sb = new StringBuilder("onload 把广告存在本地 size:");
                    sb.append(a2.size());
                    com.mintegral.msdk.base.utils.g.b("RewardMVVideoAdapter", sb.toString());
                    String d = c.this.c;
                    List list = a2;
                    if (com.mintegral.msdk.videocommon.a.a.a() != null) {
                        com.mintegral.msdk.videocommon.a.a.a().a(d, list);
                    }
                }
                m.a((com.mintegral.msdk.base.b.h) com.mintegral.msdk.base.b.i.a(c.this.b)).d();
                if (!(campaignUnit == null || campaignUnit.getAds() == null || campaignUnit.getAds().size() <= 0)) {
                    c.a(c.this, (List) campaignUnit.getAds());
                }
                com.mintegral.msdk.base.utils.g.b("RewardMVVideoAdapter", "在子线程处理业务逻辑 完成");
            }
        }).start();
        cVar.e(a2);
        cVar.b(a2);
        cVar.c(a2);
        cVar.a(a2);
        cVar.d(a2);
        com.mintegral.msdk.base.utils.g.d("RewardMVVideoAdapter", "adapter 905");
        if (cVar.g() && cVar.k != null) {
            cVar.k.a();
        }
    }

    static /* synthetic */ void a(String str, CampaignEx campaignEx, String str2) {
        try {
            w a2 = w.a((com.mintegral.msdk.base.b.h) com.mintegral.msdk.base.b.i.a(com.mintegral.msdk.base.controller.a.d().h()));
            if (campaignEx == null) {
                com.mintegral.msdk.base.utils.g.a("RewardMVVideoAdapter", "campaign is null");
                return;
            }
            p pVar = new p();
            pVar.n("2000044");
            pVar.b(com.mintegral.msdk.base.utils.c.p(com.mintegral.msdk.base.controller.a.d().h()));
            pVar.m(campaignEx.getId());
            pVar.d(campaignEx.getImageUrl());
            pVar.k(campaignEx.getRequestIdNotice());
            pVar.l(str);
            pVar.o(str2);
            a2.a(pVar);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    static /* synthetic */ void a(c cVar, List list) {
        com.mintegral.msdk.base.utils.g.b("RewardMVVideoAdapter", "onload 开始 更新本机已安装广告列表");
        if (cVar.b == null || list == null || list.size() == 0) {
            com.mintegral.msdk.base.utils.g.b("RewardMVVideoAdapter", "onload 列表为空 不做更新本机已安装广告列表");
            return;
        }
        m a2 = m.a((com.mintegral.msdk.base.b.h) com.mintegral.msdk.base.b.i.a(cVar.b));
        boolean z = false;
        for (int i2 = 0; i2 < list.size(); i2++) {
            CampaignEx campaignEx = (CampaignEx) list.get(i2);
            if (campaignEx != null) {
                if (k.a(cVar.b, campaignEx.getPackageName())) {
                    if (com.mintegral.msdk.base.controller.a.c() != null) {
                        com.mintegral.msdk.base.controller.a.c().add(new com.mintegral.msdk.base.entity.g(campaignEx.getId(), campaignEx.getPackageName()));
                        z = true;
                    }
                } else if (a2 != null && !a2.a(campaignEx.getId())) {
                    com.mintegral.msdk.base.entity.f fVar = new com.mintegral.msdk.base.entity.f();
                    fVar.a(campaignEx.getId());
                    fVar.a(campaignEx.getFca());
                    fVar.b(campaignEx.getFcb());
                    fVar.g();
                    fVar.e();
                    fVar.a(System.currentTimeMillis());
                    a2.a(fVar);
                }
            }
        }
        if (z) {
            com.mintegral.msdk.base.utils.g.b("RewardMVVideoAdapter", "更新安装列表");
            com.mintegral.msdk.base.controller.a.d().f();
        }
    }
}
