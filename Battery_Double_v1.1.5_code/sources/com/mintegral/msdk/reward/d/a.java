package com.mintegral.msdk.reward.d;

import android.content.Context;
import android.text.TextUtils;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.mintegral.msdk.base.b.h;
import com.mintegral.msdk.base.b.i;
import com.mintegral.msdk.base.b.w;
import com.mintegral.msdk.base.common.d.c;
import com.mintegral.msdk.base.common.d.c.b;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.entity.p;
import com.mintegral.msdk.base.utils.g;
import java.util.List;

/* compiled from: RewardReport */
public class a {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public static final String f2931a = "com.mintegral.msdk.reward.d.a";

    public static void a(Context context, String str) {
        if (context != null) {
            try {
                w a2 = w.a((h) i.a(context));
                if (!TextUtils.isEmpty(str) && a2 != null && a2.c() > 0) {
                    List a3 = a2.a("2000022");
                    List a4 = a2.a("2000021");
                    List a5 = a2.a("2000039");
                    List a6 = a2.a("2000043");
                    List a7 = a2.a("2000045");
                    List a8 = a2.a("2000044");
                    String b = p.b(a4);
                    String c = p.c(a3);
                    String g = p.g(a5);
                    String d = p.d(a6);
                    String e = p.e(a7);
                    String f = p.f(a8);
                    StringBuilder sb = new StringBuilder();
                    if (!TextUtils.isEmpty(b)) {
                        sb.append(b);
                    }
                    if (!TextUtils.isEmpty(c)) {
                        sb.append(c);
                    }
                    if (!TextUtils.isEmpty(g)) {
                        sb.append(g);
                    }
                    if (!TextUtils.isEmpty(d)) {
                        sb.append(d);
                    }
                    if (!TextUtils.isEmpty(e)) {
                        sb.append(e);
                    }
                    if (!TextUtils.isEmpty(f)) {
                        sb.append(f);
                    }
                    String str2 = f2931a;
                    StringBuilder sb2 = new StringBuilder("reward 批量上报：");
                    sb2.append(sb);
                    g.b(str2, sb2.toString());
                    if (!TextUtils.isEmpty(sb.toString())) {
                        String sb3 = sb.toString();
                        if (context != null && !TextUtils.isEmpty(sb3) && !TextUtils.isEmpty(str)) {
                            try {
                                com.mintegral.msdk.base.common.d.c.a aVar = new com.mintegral.msdk.base.common.d.c.a(context);
                                aVar.c();
                                aVar.b(com.mintegral.msdk.base.common.a.f, c.a(sb3, context, str), new b() {
                                    public final void a(String str) {
                                        g.d(a.f2931a, str);
                                    }

                                    public final void b(String str) {
                                        g.d(a.f2931a, str);
                                    }
                                });
                            } catch (Exception e2) {
                                e2.printStackTrace();
                                g.d(f2931a, e2.getMessage());
                            }
                        }
                    }
                }
            } catch (Exception e3) {
                e3.printStackTrace();
            }
        }
    }

    private static void a(Context context, String str, String str2) {
        if (context != null && !TextUtils.isEmpty(str) && !TextUtils.isEmpty(str2)) {
            try {
                com.mintegral.msdk.base.common.d.c.a aVar = new com.mintegral.msdk.base.common.d.c.a(context);
                aVar.c();
                aVar.b(com.mintegral.msdk.base.common.a.f, c.a(str, context, str2), new b() {
                    public final void a(String str) {
                        g.d(a.f2931a, str);
                    }

                    public final void b(String str) {
                        g.d(a.f2931a, str);
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
                g.d(f2931a, e.getMessage());
            }
        }
    }

    public static void a(Context context, CampaignEx campaignEx, String str) {
        if (!(context == null || campaignEx == null)) {
            try {
                if (!TextUtils.isEmpty(str)) {
                    StringBuffer stringBuffer = new StringBuffer("key=2000048&");
                    if (campaignEx != null) {
                        StringBuilder sb = new StringBuilder("cid=");
                        sb.append(campaignEx.getId());
                        sb.append(RequestParameters.AMPERSAND);
                        stringBuffer.append(sb.toString());
                    }
                    StringBuilder sb2 = new StringBuilder("network_type=");
                    sb2.append(com.mintegral.msdk.base.utils.c.p(context));
                    sb2.append(RequestParameters.AMPERSAND);
                    stringBuffer.append(sb2.toString());
                    StringBuilder sb3 = new StringBuilder("unit_id=");
                    sb3.append(str);
                    sb3.append(RequestParameters.AMPERSAND);
                    stringBuffer.append(sb3.toString());
                    if (campaignEx != null) {
                        StringBuilder sb4 = new StringBuilder("rid_n=");
                        sb4.append(campaignEx.getRequestIdNotice());
                        stringBuffer.append(sb4.toString());
                    }
                    a(context, stringBuffer.toString(), str);
                }
            } catch (Throwable th) {
                th.printStackTrace();
            }
        }
    }

    public static void a(Context context, String str, String str2, boolean z) {
        if (context != null) {
            try {
                if (!TextUtils.isEmpty(str) && !TextUtils.isEmpty(str2)) {
                    StringBuffer stringBuffer = new StringBuffer("key=2000047&");
                    StringBuilder sb = new StringBuilder("network_type=");
                    sb.append(com.mintegral.msdk.base.utils.c.p(context));
                    sb.append(RequestParameters.AMPERSAND);
                    stringBuffer.append(sb.toString());
                    StringBuilder sb2 = new StringBuilder("unit_id=");
                    sb2.append(str2);
                    sb2.append(RequestParameters.AMPERSAND);
                    stringBuffer.append(sb2.toString());
                    if (!TextUtils.isEmpty(com.mintegral.msdk.base.common.a.D)) {
                        StringBuilder sb3 = new StringBuilder("sys_id=");
                        sb3.append(com.mintegral.msdk.base.common.a.D);
                        sb3.append(RequestParameters.AMPERSAND);
                        stringBuffer.append(sb3.toString());
                    }
                    if (!TextUtils.isEmpty(com.mintegral.msdk.base.common.a.E)) {
                        StringBuilder sb4 = new StringBuilder("bkup_id=");
                        sb4.append(com.mintegral.msdk.base.common.a.E);
                        sb4.append(RequestParameters.AMPERSAND);
                        stringBuffer.append(sb4.toString());
                    }
                    if (z) {
                        stringBuffer.append("hb=1&");
                    }
                    StringBuilder sb5 = new StringBuilder("reason=");
                    sb5.append(str);
                    stringBuffer.append(sb5.toString());
                    String stringBuffer2 = stringBuffer.toString();
                    if (context != null && !TextUtils.isEmpty(stringBuffer2)) {
                        com.mintegral.msdk.base.common.d.c.a aVar = new com.mintegral.msdk.base.common.d.c.a(context);
                        aVar.c();
                        aVar.b(com.mintegral.msdk.base.common.a.f, c.b(stringBuffer2, context), new b() {
                            public final void a(String str) {
                                g.d(a.f2931a, str);
                            }

                            public final void b(String str) {
                                g.d(a.f2931a, str);
                            }
                        });
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                g.d(f2931a, e.getMessage());
            } catch (Throwable th) {
                th.printStackTrace();
            }
        }
    }

    public static void b(Context context, CampaignEx campaignEx, String str) {
        if (!(context == null || campaignEx == null)) {
            try {
                if (!TextUtils.isEmpty(str)) {
                    StringBuffer stringBuffer = new StringBuffer("key=2000054&");
                    if (campaignEx != null) {
                        StringBuilder sb = new StringBuilder("cid=");
                        sb.append(campaignEx.getId());
                        sb.append(RequestParameters.AMPERSAND);
                        stringBuffer.append(sb.toString());
                    }
                    StringBuilder sb2 = new StringBuilder("network_type=");
                    sb2.append(com.mintegral.msdk.base.utils.c.p(context));
                    sb2.append(RequestParameters.AMPERSAND);
                    stringBuffer.append(sb2.toString());
                    StringBuilder sb3 = new StringBuilder("unit_id=");
                    sb3.append(str);
                    sb3.append(RequestParameters.AMPERSAND);
                    stringBuffer.append(sb3.toString());
                    StringBuilder sb4 = new StringBuilder("cid=");
                    sb4.append(campaignEx.getId());
                    sb4.append(RequestParameters.AMPERSAND);
                    stringBuffer.append(sb4.toString());
                    stringBuffer.append("reason=&");
                    stringBuffer.append("result=2&");
                    if (campaignEx != null && campaignEx.getAdType() == 287) {
                        stringBuffer.append("ad_type=3&");
                    } else if (campaignEx == null || campaignEx.getAdType() != 94) {
                        stringBuffer.append("ad_type=1&");
                    } else {
                        stringBuffer.append("ad_type=1&");
                    }
                    StringBuilder sb5 = new StringBuilder("creative=");
                    sb5.append(campaignEx.getendcard_url());
                    sb5.append(RequestParameters.AMPERSAND);
                    stringBuffer.append(sb5.toString());
                    StringBuilder sb6 = new StringBuilder("devid=");
                    sb6.append(com.mintegral.msdk.base.utils.c.k());
                    sb6.append(RequestParameters.AMPERSAND);
                    stringBuffer.append(sb6.toString());
                    if (campaignEx != null) {
                        StringBuilder sb7 = new StringBuilder("rid_n=");
                        sb7.append(campaignEx.getRequestIdNotice());
                        stringBuffer.append(sb7.toString());
                    }
                    a(context, stringBuffer.toString(), str);
                }
            } catch (Throwable th) {
                th.printStackTrace();
            }
        }
    }

    public static void a(Context context, CampaignEx campaignEx, String str, String str2) {
        if (!(context == null || campaignEx == null)) {
            try {
                if ((!TextUtils.isEmpty(str)) && (!TextUtils.isEmpty(str2))) {
                    StringBuffer stringBuffer = new StringBuffer("key=2000054&");
                    if (campaignEx != null) {
                        StringBuilder sb = new StringBuilder("cid=");
                        sb.append(campaignEx.getId());
                        sb.append(RequestParameters.AMPERSAND);
                        stringBuffer.append(sb.toString());
                    }
                    StringBuilder sb2 = new StringBuilder("network_type=");
                    sb2.append(com.mintegral.msdk.base.utils.c.p(context));
                    sb2.append(RequestParameters.AMPERSAND);
                    stringBuffer.append(sb2.toString());
                    StringBuilder sb3 = new StringBuilder("unit_id=");
                    sb3.append(str);
                    sb3.append(RequestParameters.AMPERSAND);
                    stringBuffer.append(sb3.toString());
                    StringBuilder sb4 = new StringBuilder("cid=");
                    sb4.append(campaignEx.getId());
                    sb4.append(RequestParameters.AMPERSAND);
                    stringBuffer.append(sb4.toString());
                    StringBuilder sb5 = new StringBuilder("reason=");
                    sb5.append(str2);
                    sb5.append(RequestParameters.AMPERSAND);
                    stringBuffer.append(sb5.toString());
                    stringBuffer.append("result=1&");
                    StringBuilder sb6 = new StringBuilder("creative=");
                    sb6.append(campaignEx.getendcard_url());
                    sb6.append(RequestParameters.AMPERSAND);
                    stringBuffer.append(sb6.toString());
                    StringBuilder sb7 = new StringBuilder("devid=");
                    sb7.append(com.mintegral.msdk.base.utils.c.k());
                    sb7.append(RequestParameters.AMPERSAND);
                    stringBuffer.append(sb7.toString());
                    if (campaignEx != null && campaignEx.getAdType() == 287) {
                        stringBuffer.append("ad_type=3&");
                    } else if (campaignEx == null || campaignEx.getAdType() != 94) {
                        stringBuffer.append("ad_type=1&");
                    } else {
                        stringBuffer.append("ad_type=1&");
                    }
                    if (campaignEx != null) {
                        StringBuilder sb8 = new StringBuilder("rid_n=");
                        sb8.append(campaignEx.getRequestIdNotice());
                        stringBuffer.append(sb8.toString());
                    }
                    a(context, stringBuffer.toString(), str);
                }
            } catch (Throwable th) {
                th.printStackTrace();
            }
        }
    }
}
