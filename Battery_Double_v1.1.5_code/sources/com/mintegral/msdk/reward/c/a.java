package com.mintegral.msdk.reward.c;

import com.mintegral.msdk.out.RewardVideoListener;
import com.mintegral.msdk.videocommon.listener.InterVideoOutListener;

/* compiled from: DecoratorRewardVideoListener */
public final class a implements InterVideoOutListener {

    /* renamed from: a reason: collision with root package name */
    private RewardVideoListener f2928a;

    public a(RewardVideoListener rewardVideoListener) {
        this.f2928a = rewardVideoListener;
    }

    public final void onAdShow() {
        if (this.f2928a != null) {
            this.f2928a.onAdShow();
        }
    }

    public final void onAdClose(boolean z, String str, float f) {
        if (this.f2928a != null) {
            this.f2928a.onAdClose(z, str, f);
        }
    }

    public final void onShowFail(String str) {
        if (this.f2928a != null) {
            this.f2928a.onShowFail(str);
        }
    }

    public final void onVideoAdClicked(String str) {
        if (this.f2928a != null) {
            this.f2928a.onVideoAdClicked(str);
        }
    }

    public final void onVideoComplete(String str) {
        if (this.f2928a != null) {
            this.f2928a.onVideoComplete(str);
        }
    }

    public final void onEndcardShow(String str) {
        if (this.f2928a != null) {
            this.f2928a.onEndcardShow(str);
        }
    }

    public final void onVideoLoadFail(String str) {
        if (this.f2928a != null) {
            this.f2928a.onVideoLoadFail(str);
        }
    }

    public final void onVideoLoadSuccess(String str) {
        if (this.f2928a != null) {
            this.f2928a.onVideoLoadSuccess(str);
        }
    }

    public final void onLoadSuccess(String str) {
        if (this.f2928a != null) {
            this.f2928a.onLoadSuccess(str);
        }
    }
}
