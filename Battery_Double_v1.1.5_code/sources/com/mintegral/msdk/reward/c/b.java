package com.mintegral.msdk.reward.c;

import android.content.Context;
import android.text.TextUtils;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.reward.a.d;
import com.mintegral.msdk.videocommon.e.a;
import com.mintegral.msdk.videocommon.e.c;
import java.util.List;
import java.util.Random;

/* compiled from: DeductionShowRewardListener */
public final class b extends c {

    /* renamed from: a reason: collision with root package name */
    private d f2929a;
    private c b;
    private String c;
    private boolean d;
    private Context e;
    private CampaignEx f;
    private boolean g = false;
    private boolean h = false;
    private boolean i = false;
    private boolean j = false;
    private boolean k = false;

    public b(Context context, boolean z, c cVar, CampaignEx campaignEx, d dVar, String str) {
        this.f2929a = dVar;
        this.g = a(cVar, campaignEx);
        this.b = cVar;
        this.c = str;
        this.d = z;
        this.e = context;
        this.f = campaignEx;
    }

    public final void a() {
        super.a();
        if (this.f2929a != null && !this.h) {
            if (!this.g || (this.g && !a(1))) {
                this.f2929a.a();
                if (b(1)) {
                    c();
                }
            }
            this.f2929a.e(this.c);
            this.h = true;
        }
    }

    public final void a(boolean z, com.mintegral.msdk.videocommon.b.d dVar) {
        int i2;
        super.a(z, dVar);
        if (this.f2929a != null && !this.j) {
            if (!this.g) {
                this.f2929a.a(z, dVar);
                this.j = true;
                return;
            }
            if (!a(5)) {
                if (this.b != null) {
                    com.mintegral.msdk.videocommon.b.c L = this.b.L();
                    if (L != null) {
                        i2 = L.a();
                        if (i2 == 0 && a(1)) {
                            this.f2929a.a("mediaplayer cannot play");
                        }
                        this.f2929a.a(z, dVar);
                    }
                }
                i2 = 1;
                this.f2929a.a("mediaplayer cannot play");
                this.f2929a.a(z, dVar);
            } else {
                this.k = true;
            }
            this.j = true;
        }
    }

    public final void a(String str) {
        super.a(str);
        if (this.f2929a != null && !this.i) {
            if (!this.g || (this.g && !a(1))) {
                this.f2929a.a(str);
            }
            this.i = true;
        }
    }

    public final void b(String str) {
        super.b(str);
        if (this.f2929a == null) {
            return;
        }
        if (!this.g || (this.g && !a(4))) {
            this.f2929a.b(str);
        }
    }

    public final void d(String str) {
        super.d(str);
        if (this.f2929a == null) {
            return;
        }
        if (!this.g || (this.g && !a(3))) {
            this.f2929a.d(str);
            if (b(3)) {
                c();
            }
        }
    }

    public final void c(String str) {
        super.c(str);
        if (this.f2929a == null) {
            return;
        }
        if (!this.g || (this.g && !a(2))) {
            this.f2929a.c(str);
            if (b(2)) {
                c();
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x002d A[Catch:{ Exception -> 0x0069 }] */
    private static boolean a(c cVar, CampaignEx campaignEx) {
        long j2;
        a b2;
        try {
            String j3 = com.mintegral.msdk.base.controller.a.d().j();
            long j4 = 0;
            if (!TextUtils.isEmpty(j3)) {
                com.mintegral.msdk.b.b.a();
                com.mintegral.msdk.b.a b3 = com.mintegral.msdk.b.b.b(j3);
                if (b3 != null) {
                    j2 = b3.ak() * 1000;
                    com.mintegral.msdk.videocommon.e.b.a();
                    b2 = com.mintegral.msdk.videocommon.e.b.b();
                    if (b2 != null) {
                        j4 = b2.e();
                    }
                    if (campaignEx != null || !campaignEx.isSpareOffer(j4, j2)) {
                        campaignEx.setSpareOfferFlag(0);
                        if (campaignEx != null || campaignEx.isBidCampaign() || cVar == null || cVar.C() == 1.0d) {
                            return false;
                        }
                        if (new Random().nextDouble() > cVar.C()) {
                            return true;
                        }
                        return false;
                    }
                    campaignEx.setSpareOfferFlag(1);
                    return true;
                }
            }
            j2 = 0;
            com.mintegral.msdk.videocommon.e.b.a();
            b2 = com.mintegral.msdk.videocommon.e.b.b();
            if (b2 != null) {
            }
            if (campaignEx != null) {
            }
            campaignEx.setSpareOfferFlag(0);
            if (campaignEx != null) {
            }
        } catch (Exception unused) {
        }
        return false;
    }

    private boolean a(int i2) {
        if (this.b != null) {
            com.mintegral.msdk.videocommon.b.c L = this.b.L();
            if (L != null) {
                if (L.a() == 0) {
                    return i2 <= 4;
                }
                List b2 = L.b();
                if (b2 != null) {
                    return b2.contains(Integer.valueOf(i2));
                }
            }
        }
        return i2 <= 4;
    }

    private boolean b(int i2) {
        return this.b != null ? this.d ? i2 == this.b.a(287) : i2 == this.b.a(94) : this.d ? i2 == 3 : i2 == 2;
    }

    private void c() {
        try {
            if (!(this.f == null || this.f.getNativeVideoTracking() == null)) {
                String[] r = this.f.getNativeVideoTracking().r();
                if (r != null) {
                    for (String str : r) {
                        if (!TextUtils.isEmpty(str)) {
                            com.mintegral.msdk.click.a.a(this.e, this.f, this.c, str, false, true);
                        }
                    }
                }
            }
        } catch (Exception e2) {
            if (MIntegralConstans.DEBUG) {
                e2.printStackTrace();
            }
        }
    }

    public final boolean b() {
        return this.k;
    }
}
