package com.mintegral.msdk.reward.c;

import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.reward.a.d;

/* compiled from: DefaultShowRewardListener */
public class c implements d {
    public boolean b() {
        return false;
    }

    public void a() {
        g.a("ShowRewardListener", "onAdShow");
    }

    public void a(boolean z, com.mintegral.msdk.videocommon.b.d dVar) {
        StringBuilder sb = new StringBuilder("onAdClose:isCompleteView:");
        sb.append(z);
        sb.append(",reward:");
        sb.append(dVar);
        g.a("ShowRewardListener", sb.toString());
    }

    public void a(String str) {
        StringBuilder sb = new StringBuilder("onShowFail:");
        sb.append(str);
        g.a("ShowRewardListener", sb.toString());
    }

    public void b(String str) {
        StringBuilder sb = new StringBuilder("onVideoAdClicked:");
        sb.append(str);
        g.a("ShowRewardListener", sb.toString());
    }

    public void c(String str) {
        StringBuilder sb = new StringBuilder("onVideoComplete: ");
        sb.append(str);
        g.a("ShowRewardListener", sb.toString());
    }

    public void d(String str) {
        StringBuilder sb = new StringBuilder("onEndcardShow: ");
        sb.append(str);
        g.a("ShowRewardListener", sb.toString());
    }

    public final void e(String str) {
        StringBuilder sb = new StringBuilder("onAutoLoad: ");
        sb.append(str);
        g.a("ShowRewardListener", sb.toString());
    }
}
