package com.mintegral.msdk.out;

import android.content.Context;
import com.mintegral.msdk.videofeeds.b.a;
import com.mintegral.msdk.videofeeds.vfplayer.VideoFeedsAdView;
import java.util.Map;

public class MTGVideoFeedsHandler {

    /* renamed from: a reason: collision with root package name */
    private a f2862a;

    public MTGVideoFeedsHandler(Context context, Map<String, Object> map) {
        if (this.f2862a == null) {
            this.f2862a = new a();
        }
        this.f2862a.a(context, map);
        if (com.mintegral.msdk.base.controller.a.d().h() == null && context != null) {
            com.mintegral.msdk.base.controller.a.d().a(context.getApplicationContext());
        }
    }

    public void load() {
        if (this.f2862a != null) {
            this.f2862a.a();
        }
    }

    public VideoFeedsAdView show() {
        try {
            if (this.f2862a != null) {
                return this.f2862a.b();
            }
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public void setVideoFeedsListener(VideoFeedsListener videoFeedsListener) {
        if (this.f2862a != null) {
            this.f2862a.a(videoFeedsListener);
        }
    }

    public void clearVideoCache() {
        try {
            if (this.f2862a != null) {
                a.c();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
