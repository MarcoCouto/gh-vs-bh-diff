package com.mintegral.msdk.out;

import android.content.Context;
import java.util.Map;

public abstract class MtgBidCommonHandler {

    /* renamed from: a reason: collision with root package name */
    protected Map<String, Object> f2863a;
    protected Context b;

    public abstract void bidLoad(String str);

    public abstract void bidRelease();

    public MtgBidCommonHandler() {
    }

    public MtgBidCommonHandler(Map<String, Object> map, Context context) {
        this.f2863a = map;
        this.b = context;
    }
}
