package com.mintegral.msdk.out;

import com.mintegral.msdk.system.a;

public class MIntegralSDKFactory {

    /* renamed from: a reason: collision with root package name */
    private static a f2857a;

    private MIntegralSDKFactory() {
    }

    public static a getMIntegralSDK() {
        if (f2857a == null) {
            synchronized (MIntegralSDKFactory.class) {
                if (f2857a == null) {
                    f2857a = new a();
                }
            }
        }
        return f2857a;
    }
}
