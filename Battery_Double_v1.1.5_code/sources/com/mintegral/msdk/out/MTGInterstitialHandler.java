package com.mintegral.msdk.out;

import android.content.Context;
import com.mintegral.msdk.interstitial.c.a;
import java.util.Map;

public class MTGInterstitialHandler {

    /* renamed from: a reason: collision with root package name */
    private a f2859a;

    public MTGInterstitialHandler(Context context, Map<String, Object> map) {
        if (this.f2859a == null) {
            this.f2859a = new a();
        }
        this.f2859a.a(context, map);
        if (com.mintegral.msdk.base.controller.a.d().h() == null && context != null) {
            com.mintegral.msdk.base.controller.a.d().a(context);
        }
    }

    public void preload() {
        try {
            if (this.f2859a != null) {
                this.f2859a.a();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void show() {
        try {
            this.f2859a.b();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setInterstitialListener(InterstitialListener interstitialListener) {
        try {
            if (this.f2859a != null) {
                this.f2859a.a(interstitialListener);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
