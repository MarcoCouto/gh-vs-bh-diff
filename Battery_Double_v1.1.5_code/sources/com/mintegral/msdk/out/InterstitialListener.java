package com.mintegral.msdk.out;

public interface InterstitialListener {
    void onInterstitialAdClick();

    void onInterstitialClosed();

    void onInterstitialLoadFail(String str);

    void onInterstitialLoadSuccess();

    void onInterstitialShowFail(String str);

    void onInterstitialShowSuccess();
}
