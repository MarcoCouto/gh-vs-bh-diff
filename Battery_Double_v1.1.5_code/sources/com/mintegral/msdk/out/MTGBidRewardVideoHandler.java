package com.mintegral.msdk.out;

import android.content.Context;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.reward.b.a;
import com.mintegral.msdk.videocommon.listener.InterVideoOutListener;

public class MTGBidRewardVideoHandler {

    /* renamed from: a reason: collision with root package name */
    private a f2858a;

    public MTGBidRewardVideoHandler(Context context, String str) {
        if (com.mintegral.msdk.base.controller.a.d().h() == null && context != null) {
            com.mintegral.msdk.base.controller.a.d().a(context);
        }
        a(str);
    }

    public MTGBidRewardVideoHandler(String str) {
        a(str);
    }

    private void a(String str) {
        try {
            if (this.f2858a == null) {
                this.f2858a = new a();
                this.f2858a.a(false);
                this.f2858a.a();
            }
            this.f2858a.b(str);
        } catch (Throwable th) {
            g.c("MTGBidRewardVideoHandler", th.getMessage(), th);
        }
    }

    public void loadFromBid(String str) {
        if (this.f2858a != null) {
            this.f2858a.a(true, str);
        }
    }

    public boolean isBidReady() {
        if (this.f2858a != null) {
            return this.f2858a.c(false);
        }
        return false;
    }

    public void showFromBid(String str) {
        if (this.f2858a != null) {
            this.f2858a.a(str, (String) null);
        }
    }

    public void showFromBid(String str, String str2) {
        if (this.f2858a != null) {
            this.f2858a.a(str, str2);
        }
    }

    public void setRewardVideoListener(RewardVideoListener rewardVideoListener) {
        if (this.f2858a != null) {
            this.f2858a.a((InterVideoOutListener) new com.mintegral.msdk.reward.c.a(rewardVideoListener));
        }
    }

    public void clearVideoCache() {
        try {
            if (this.f2858a != null) {
                a.b();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void playVideoMute(int i) {
        if (this.f2858a != null) {
            this.f2858a.a(i);
        }
    }
}
