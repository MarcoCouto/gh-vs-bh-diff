package com.mintegral.msdk.out;

import com.mintegral.msdk.base.entity.CampaignEx;
import java.util.List;

public class Frame {

    /* renamed from: a reason: collision with root package name */
    private int f2853a;
    private List<CampaignEx> b;
    private String c;
    private String d;

    public int getTemplate() {
        return this.f2853a;
    }

    public void setTemplate(int i) {
        this.f2853a = i;
    }

    public List<CampaignEx> getCampaigns() {
        return this.b;
    }

    public void setCampaigns(List<CampaignEx> list) {
        this.b = list;
    }

    public String getSessionId() {
        return this.c;
    }

    public void setSessionId(String str) {
        this.c = str;
    }

    public String getParentSessionId() {
        return this.d;
    }

    public void setParentSessionId(String str) {
        this.d = str;
    }
}
