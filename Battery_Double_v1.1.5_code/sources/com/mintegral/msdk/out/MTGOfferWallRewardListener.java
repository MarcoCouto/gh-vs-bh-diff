package com.mintegral.msdk.out;

import java.util.List;

public interface MTGOfferWallRewardListener {
    void responseRewardInfo(List<OfferWallRewardInfo> list);
}
