package com.mintegral.msdk.out;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.base.b.f;
import com.mintegral.msdk.base.b.h;
import com.mintegral.msdk.base.b.i;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.base.utils.j;
import com.mintegral.msdk.base.utils.s;
import com.mintegral.msdk.mtgnative.d.a;
import com.mintegral.msdk.out.NativeListener.NativeAdListener;
import com.mintegral.msdk.out.NativeListener.NativeTrackingListener;
import com.mintegral.msdk.out.NativeListener.Template;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;

public class MtgBidNativeHandler extends MtgBidCommonHandler {
    public static final String TEMPLATE_ID = "id";
    public static final String TEMPLATE_NUM = "ad_num";
    private static final String g = "com.mintegral.msdk.out.MtgBidNativeHandler";
    private static String k;
    private a c = new a();
    private NativeTrackingListener d;
    private com.mintegral.msdk.mtgnative.f.a e;
    private List<Template> f;
    private com.mintegral.msdk.click.a h;
    private Context i;
    private Map<String, Object> j;

    public static class KeyWordInfo {

        /* renamed from: a reason: collision with root package name */
        String f2864a;
        String b;

        public String getKeyWordPN() {
            return this.f2864a;
        }

        public void setKeyWordPN(String str) {
            this.f2864a = str;
        }

        public String getKeyWorkVN() {
            return this.b;
        }

        public void setKeyWorkVN(String str) {
            this.b = str;
        }

        public KeyWordInfo(String str, String str2) {
            this.f2864a = str;
            this.b = str2;
        }
    }

    public void setMustBrowser(boolean z) {
        j.f2615a = z;
    }

    public MtgBidNativeHandler(Context context) {
        this.i = context;
        if (com.mintegral.msdk.base.controller.a.d().h() == null && context != null) {
            com.mintegral.msdk.base.controller.a.d().a(context);
        }
    }

    public MtgBidNativeHandler(Map<String, Object> map, Context context) {
        super(map, context);
        this.i = context;
        this.j = map;
        if (com.mintegral.msdk.base.controller.a.d().h() == null && context != null) {
            com.mintegral.msdk.base.controller.a.d().a(context);
        }
        if (map != null) {
            try {
                if (map.containsKey(MIntegralConstans.PROPERTIES_UNIT_ID) && map.get(MIntegralConstans.PROPERTIES_UNIT_ID) != null && (map.get(MIntegralConstans.PROPERTIES_UNIT_ID) instanceof String) && map.containsKey(MIntegralConstans.NATIVE_VIDEO_WIDTH) && map.get(MIntegralConstans.NATIVE_VIDEO_WIDTH) != null && (map.get(MIntegralConstans.NATIVE_VIDEO_WIDTH) instanceof Integer) && map.containsKey(MIntegralConstans.NATIVE_VIDEO_HEIGHT) && map.get(MIntegralConstans.NATIVE_VIDEO_HEIGHT) != null) {
                    map.get(MIntegralConstans.NATIVE_VIDEO_HEIGHT);
                }
            } catch (Throwable th) {
                th.printStackTrace();
            }
        }
    }

    public void bidLoad(String str) {
        if (this.f2863a == null || !this.f2863a.containsKey(MIntegralConstans.PROPERTIES_UNIT_ID)) {
            g.c("", "no unit id.");
        } else if (TextUtils.isEmpty(str) && this.c != null) {
            this.c.onAdLoadError("bidToken can not be null, bid first");
        } else if (this.f2863a == null || !this.f2863a.containsKey(MIntegralConstans.PROPERTIES_UNIT_ID)) {
            g.c("", "no unit id.");
        } else {
            if (this.f != null && this.f.size() > 0) {
                try {
                    this.f2863a.put(MIntegralConstans.NATIVE_INFO, buildTemplateString());
                } catch (Exception unused) {
                    g.c("com.mintegral.msdk", "MTGSDK set template error");
                }
            }
            try {
                this.f2863a.put(MIntegralConstans.PROPERTIES_HANDLER_CONTROLLER, this);
                if (this.e == null) {
                    this.e = new com.mintegral.msdk.mtgnative.f.a(this.c, this.d);
                    this.e.a(this.i, this.f2863a);
                }
                this.e.a(str);
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    public void bidRelease() {
        if (this.e != null) {
            this.e.d();
        }
    }

    public static Map<String, Object> getNativeProperties(String str) {
        HashMap hashMap = new HashMap();
        k = str;
        hashMap.put(MIntegralConstans.PROPERTIES_UNIT_ID, str);
        hashMap.put(MIntegralConstans.PLUGIN_NAME, new String[]{MIntegralConstans.PLUGIN_NATIVE});
        hashMap.put(MIntegralConstans.PROPERTIES_LAYOUT_TYPE, Integer.valueOf(0));
        return hashMap;
    }

    public void addTemplate(Template template) {
        if (template != null) {
            if (this.f == null) {
                this.f = new ArrayList();
            }
            this.f.add(template);
        }
    }

    public void handleResult(Campaign campaign, String str) {
        if (this.h == null) {
            String str2 = null;
            if (this.j != null) {
                str2 = (String) this.j.get(MIntegralConstans.PROPERTIES_UNIT_ID);
            }
            this.h = new com.mintegral.msdk.click.a(this.i, str2);
        }
        this.h.a(campaign, str);
    }

    public String buildTemplateString() {
        try {
            if (this.f != null && this.f.size() > 0) {
                String str = "[{$native_info}]";
                StringBuffer stringBuffer = new StringBuffer();
                for (Template template : this.f) {
                    stringBuffer.append("{\"id\":");
                    StringBuilder sb = new StringBuilder();
                    sb.append(template.getId());
                    sb.append(",");
                    stringBuffer.append(sb.toString());
                    stringBuffer.append("\"ad_num\":");
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append(template.getAdNum());
                    sb2.append("},");
                    stringBuffer.append(sb2.toString());
                }
                return str.replace("{$native_info}", stringBuffer.subSequence(0, stringBuffer.lastIndexOf(",")));
            }
        } catch (Exception unused) {
        }
        return null;
    }

    public static String getTemplateString(List<Template> list) {
        if (list != null) {
            try {
                if (list.size() > 0) {
                    String str = "[{$native_info}]";
                    StringBuffer stringBuffer = new StringBuffer();
                    for (Template template : list) {
                        stringBuffer.append("{\"id\":");
                        StringBuilder sb = new StringBuilder();
                        sb.append(template.getId());
                        sb.append(",");
                        stringBuffer.append(sb.toString());
                        stringBuffer.append("\"ad_num\":");
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append(template.getAdNum());
                        sb2.append("},");
                        stringBuffer.append(sb2.toString());
                    }
                    return str.replace("{$native_info}", stringBuffer.subSequence(0, stringBuffer.lastIndexOf(",")));
                }
            } catch (Exception unused) {
            }
        }
        return null;
    }

    public static String parseKeyWordInfoListStr(List<KeyWordInfo> list) {
        if (list != null && list.size() > 0) {
            try {
                JSONArray jSONArray = new JSONArray();
                for (int i2 = 0; i2 < list.size(); i2++) {
                    JSONObject jSONObject = new JSONObject();
                    KeyWordInfo keyWordInfo = (KeyWordInfo) list.get(i2);
                    if (!TextUtils.isEmpty(keyWordInfo.getKeyWordPN()) && !TextUtils.isEmpty(keyWordInfo.getKeyWorkVN())) {
                        jSONObject.put(TtmlNode.TAG_P, keyWordInfo.getKeyWordPN());
                        jSONObject.put("v", keyWordInfo.getKeyWorkVN());
                        com.mintegral.msdk.base.controller.a d2 = com.mintegral.msdk.base.controller.a.d();
                        if (d2 != null) {
                            List l = d2.l();
                            if (l == null) {
                                jSONObject.put("i", 2);
                            } else if (l.contains(keyWordInfo.getKeyWordPN())) {
                                jSONObject.put("i", 1);
                            } else {
                                jSONObject.put("i", 0);
                            }
                        }
                        jSONArray.put(jSONObject);
                    }
                }
                return jSONArray.toString();
            } catch (Exception e2) {
                e2.printStackTrace();
                g.d(g, e2.getMessage());
            }
        }
        return null;
    }

    public void registerView(View view, Campaign campaign) {
        if (this.e == null) {
            this.e = new com.mintegral.msdk.mtgnative.f.a(this.c, this.d);
            if (this.f2863a != null) {
                this.f2863a.put(MIntegralConstans.PROPERTIES_HANDLER_CONTROLLER, this);
            }
            this.e.a(this.i, this.f2863a);
        }
        this.e.a(view, campaign);
    }

    public void registerView(View view, List<View> list, Campaign campaign) {
        if (this.e == null) {
            this.e = new com.mintegral.msdk.mtgnative.f.a(this.c, this.d);
            if (this.f2863a != null) {
                this.f2863a.put(MIntegralConstans.PROPERTIES_HANDLER_CONTROLLER, this);
            }
            this.e.a(this.i, this.f2863a);
        }
        this.e.a(view, list, campaign);
    }

    public void unregisterView(View view, Campaign campaign) {
        if (this.e == null) {
            this.e = new com.mintegral.msdk.mtgnative.f.a(this.c, this.d);
            if (this.f2863a != null) {
                this.f2863a.put(MIntegralConstans.PROPERTIES_HANDLER_CONTROLLER, this);
            }
            this.e.a(this.i, this.f2863a);
        }
        this.e.b(view, campaign);
    }

    public void unregisterView(View view, List<View> list, Campaign campaign) {
        if (this.e == null) {
            this.e = new com.mintegral.msdk.mtgnative.f.a(this.c, this.d);
            if (this.f2863a != null) {
                this.f2863a.put(MIntegralConstans.PROPERTIES_HANDLER_CONTROLLER, this);
            }
            this.e.a(this.i, this.f2863a);
        }
        this.e.b(view, list, campaign);
    }

    public a getAdListener() {
        return this.c;
    }

    public void setAdListener(NativeAdListener nativeAdListener) {
        this.c = new a(nativeAdListener);
        if (this.e != null) {
            this.e.a(this.c);
        }
    }

    public NativeTrackingListener getTrackingListener() {
        return this.d;
    }

    public void setTrackingListener(NativeTrackingListener nativeTrackingListener) {
        this.d = nativeTrackingListener;
        if (this.e != null) {
            this.e.a(nativeTrackingListener);
        }
    }

    public void clearVideoCache() {
        try {
            if (this.e != null) {
                com.mintegral.msdk.mtgnative.f.a.c();
            }
        } catch (Exception e2) {
            g.a(g, e2.getMessage());
        }
    }

    public void clearCacheByUnitid(String str) {
        try {
            if (!s.a(str)) {
                f.a((h) i.a(com.mintegral.msdk.base.controller.a.d().h())).b(str);
                com.mintegral.msdk.mtgnative.a.f.a(3).a(str);
                com.mintegral.msdk.mtgnative.a.f.a(6).a(str);
                com.mintegral.msdk.mtgnative.a.f.a(7).a(str);
            }
        } catch (Exception e2) {
            g.a(g, e2.getMessage());
        }
    }
}
