package com.mintegral.msdk.out;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import com.mintegral.msdk.base.common.c.b;
import com.mintegral.msdk.base.common.c.c;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.utils.k;
import com.mintegral.msdk.widget.MTGImageView;

public class LoadingActivity extends Activity {

    /* renamed from: a reason: collision with root package name */
    BroadcastReceiver f2854a = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            LoadingActivity.this.finish();
        }
    };
    private RelativeLayout b;
    /* access modifiers changed from: private */
    public ImageView c;
    /* access modifiers changed from: private */
    public Bitmap d;
    private String e;
    private c f = new c() {
        public void onFailedLoad(String str, String str2) {
        }

        public void onSuccessLoad(Bitmap bitmap, String str) {
            if (LoadingActivity.this.c != null && bitmap != null && !bitmap.isRecycled() && ((String) LoadingActivity.this.c.getTag()).equals(str)) {
                LoadingActivity.this.c.setImageBitmap(bitmap);
                LoadingActivity.this.d = bitmap;
            }
        }
    };
    private Drawable g;
    private RelativeLayout h;

    public interface OnLoadingDialogCallback {
        void onCancel(CampaignEx campaignEx);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("ExitApp");
        if (this.f2854a != null) {
            registerReceiver(this.f2854a, intentFilter);
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (this.f2854a != null) {
            unregisterReceiver(this.f2854a);
        }
        if (this.c != null) {
            this.c.setImageBitmap(null);
        }
        this.c = null;
        this.b = null;
        this.f = null;
        this.g = null;
        if (this.h != null) {
            this.h.setBackgroundDrawable(null);
        }
        this.h = null;
        if (this.d != null && !this.d.isRecycled()) {
            this.d = null;
        }
        b.a(getApplicationContext()).c();
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (getIntent().hasExtra(CampaignEx.JSON_KEY_ICON_URL)) {
            this.e = getIntent().getStringExtra(CampaignEx.JSON_KEY_ICON_URL);
        }
        if (this.b == null) {
            this.b = new RelativeLayout(this);
            this.h = new RelativeLayout(this);
            int b2 = k.b(this, 15.0f);
            this.h.setPadding(b2, b2, b2, b2);
            this.h.setBackgroundResource(getResources().getIdentifier("mintegral_native_bg_loading_camera", "drawable", getPackageName()));
            this.h.addView(new TextView(this), new LayoutParams(k.b(this, 140.0f), k.b(this, 31.5f)));
            this.c = new MTGImageView(this);
            this.c.setId(k.b());
            this.c.setTag(this.e);
            if (!TextUtils.isEmpty(this.e)) {
                b.a(getApplicationContext()).a(this.e, this.f);
            }
            int b3 = k.b(this, 64.0f);
            LayoutParams layoutParams = new LayoutParams(b3, b3);
            layoutParams.addRule(13, -1);
            this.h.addView(this.c, layoutParams);
            TextView textView = new TextView(this);
            textView.setSingleLine();
            textView.setTextColor(-1);
            textView.setTextSize(16.0f);
            textView.setText("Relax while loading....");
            LayoutParams layoutParams2 = new LayoutParams(-2, -2);
            layoutParams2.addRule(3, this.c.getId());
            layoutParams2.addRule(14, -1);
            this.h.addView(textView, layoutParams2);
            this.b.addView(this.h, new LayoutParams(-1, -1));
        }
        setContentView(this.b);
    }
}
