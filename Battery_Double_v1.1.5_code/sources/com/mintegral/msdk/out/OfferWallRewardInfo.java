package com.mintegral.msdk.out;

public class OfferWallRewardInfo {

    /* renamed from: a reason: collision with root package name */
    private String f2868a;
    private int b;

    public OfferWallRewardInfo(String str, int i) {
        this.f2868a = str;
        this.b = i;
    }

    public String getRewardName() {
        return this.f2868a;
    }

    public void setRewardName(String str) {
        this.f2868a = str;
    }

    public int getRewardAmount() {
        return this.b;
    }

    public void setRewardAmount(int i) {
        this.b = i;
    }
}
