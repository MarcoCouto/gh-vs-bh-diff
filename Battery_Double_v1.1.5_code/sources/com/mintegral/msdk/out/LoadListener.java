package com.mintegral.msdk.out;

import java.io.Serializable;

public abstract class LoadListener implements Serializable {
    public abstract void onLoadFaild(String str);

    public abstract void onLoadSucceed();
}
