package com.mintegral.msdk.out;

import android.content.Context;
import com.mintegral.msdk.offerwall.c.a;
import java.util.Map;

public class MTGOfferWallHandler {

    /* renamed from: a reason: collision with root package name */
    private a f2860a;

    public MTGOfferWallHandler(Context context, Map<String, Object> map) {
        if (this.f2860a == null) {
            this.f2860a = new a();
        }
        this.f2860a.a(context, map);
        if (com.mintegral.msdk.base.controller.a.d().h() == null && context != null) {
            com.mintegral.msdk.base.controller.a.d().a(context.getApplicationContext());
        }
    }

    public void load() {
        try {
            if (this.f2860a != null) {
                this.f2860a.a();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void show() {
        try {
            if (this.f2860a != null) {
                this.f2860a.b();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setOfferWallListener(OfferWallListener offerWallListener) {
        try {
            if (this.f2860a != null) {
                this.f2860a.a(offerWallListener);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void queryOfferWallRewards(MTGOfferWallRewardListener mTGOfferWallRewardListener) {
        try {
            if (this.f2860a != null) {
                this.f2860a.a(mTGOfferWallRewardListener);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
