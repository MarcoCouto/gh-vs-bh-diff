package com.mintegral.msdk.out;

import android.content.Context;
import java.util.Map;

public abstract class MtgCommonHandler {

    /* renamed from: a reason: collision with root package name */
    protected Map<String, Object> f2865a;
    protected Context b;

    public abstract boolean load();

    public abstract void release();

    public MtgCommonHandler() {
    }

    public MtgCommonHandler(Map<String, Object> map, Context context) {
        this.f2865a = map;
        this.b = context;
    }
}
