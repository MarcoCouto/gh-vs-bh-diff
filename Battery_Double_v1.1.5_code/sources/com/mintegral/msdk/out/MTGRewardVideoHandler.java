package com.mintegral.msdk.out;

import android.content.Context;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.reward.b.a;
import com.mintegral.msdk.videocommon.listener.InterVideoOutListener;

public class MTGRewardVideoHandler {

    /* renamed from: a reason: collision with root package name */
    private a f2861a;

    public MTGRewardVideoHandler(Context context, String str) {
        if (com.mintegral.msdk.base.controller.a.d().h() == null && context != null) {
            com.mintegral.msdk.base.controller.a.d().a(context);
        }
        a(str);
    }

    public MTGRewardVideoHandler(String str) {
        a(str);
    }

    private void a(String str) {
        try {
            if (this.f2861a == null) {
                this.f2861a = new a();
                this.f2861a.a(false);
            }
            this.f2861a.b(str);
        } catch (Throwable th) {
            g.c("MTGRewardVideoHandler", th.getMessage(), th);
        }
    }

    public void load() {
        if (this.f2861a != null) {
            this.f2861a.b(true);
        }
    }

    public void loadFormSelfFilling() {
        if (this.f2861a != null) {
            this.f2861a.b(false);
        }
    }

    public boolean isReady() {
        if (this.f2861a != null) {
            return this.f2861a.c(false);
        }
        return false;
    }

    public void show(String str) {
        if (this.f2861a != null) {
            this.f2861a.a(str, (String) null);
        }
    }

    public void show(String str, String str2) {
        if (this.f2861a != null) {
            this.f2861a.a(str, str2);
        }
    }

    public void setRewardVideoListener(RewardVideoListener rewardVideoListener) {
        if (this.f2861a != null) {
            this.f2861a.a((InterVideoOutListener) new com.mintegral.msdk.reward.c.a(rewardVideoListener));
        }
    }

    public void clearVideoCache() {
        try {
            if (this.f2861a != null) {
                a.b();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void playVideoMute(int i) {
        if (this.f2861a != null) {
            this.f2861a.a(i);
        }
    }

    public void setAlertDialogText(String str, String str2, String str3, String str4) {
        if (this.f2861a != null) {
            this.f2861a.a(str, str2, str3, str4);
        }
    }
}
