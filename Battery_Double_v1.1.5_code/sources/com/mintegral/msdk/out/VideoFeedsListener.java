package com.mintegral.msdk.out;

public interface VideoFeedsListener {
    void onAdClicked();

    void onAdShowSuccess(int i);

    void onShowFail(String str);

    void onVideoLoadFail(String str);

    void onVideoLoadSuccess();
}
