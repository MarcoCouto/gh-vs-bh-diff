package com.mintegral.msdk.out;

public interface OfferWallListener {
    void onOfferWallAdClick();

    void onOfferWallClose();

    void onOfferWallCreditsEarned(String str, int i);

    void onOfferWallLoadFail(String str);

    void onOfferWallLoadSuccess();

    void onOfferWallOpen();

    void onOfferWallShowFail(String str);
}
