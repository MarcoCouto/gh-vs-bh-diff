package com.mintegral.msdk.out;

import java.util.List;

public class NativeListener {

    public interface FilpListener {
        void filpEvent(int i);
    }

    public interface NativeAdListener {
        void onAdClick(Campaign campaign);

        void onAdFramesLoaded(List<Frame> list);

        void onAdLoadError(String str);

        void onAdLoaded(List<Campaign> list, int i);

        void onLoggingImpression(int i);
    }

    public interface NativeTrackingListener {
        void onDismissLoading(Campaign campaign);

        void onDownloadFinish(Campaign campaign);

        void onDownloadProgress(int i);

        void onDownloadStart(Campaign campaign);

        void onFinishRedirection(Campaign campaign, String str);

        boolean onInterceptDefaultLoadingDialog();

        void onRedirectionFailed(Campaign campaign, String str);

        void onShowLoading(Campaign campaign);

        void onStartRedirection(Campaign campaign, String str);
    }

    public static class Template {

        /* renamed from: a reason: collision with root package name */
        private int f2867a;
        private int b;

        public Template(int i, int i2) {
            this.f2867a = i;
            this.b = i2;
        }

        public int getId() {
            return this.f2867a;
        }

        public void setId(int i) {
            this.f2867a = i;
        }

        public int getAdNum() {
            return this.b;
        }

        public void setAdNum(int i) {
            this.b = i;
        }
    }
}
