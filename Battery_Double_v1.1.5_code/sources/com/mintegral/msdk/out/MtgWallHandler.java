package com.mintegral.msdk.out;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.appwall.service.HandlerProvider;
import com.mintegral.msdk.base.utils.g;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

public class MtgWallHandler extends MtgCommonHandler {
    private ViewGroup c;
    private View d;
    private HandlerProvider e;
    private Context f;

    public interface AppWallViewCampaignClickListener {
        void onEndJump();

        void onStartJump();
    }

    public interface AppWallViewLoadingEndListener {
        void onLoadEnd();
    }

    public interface AppWallViewNoMoreDateListener {
        void onNoMoreData();
    }

    public interface WallViewBackClickListener {
        void onBackClick();
    }

    public MtgWallHandler(Map<String, Object> map, Context context, ViewGroup viewGroup) {
        super(map, context);
        this.f = context;
        setHandlerContainer(viewGroup);
    }

    public MtgWallHandler(Map<String, Object> map, Context context) {
        super(map, context);
        this.f = context;
    }

    public void refreshUI(View view) {
        try {
            Class cls = Class.forName("com.mintegral.msdk.appwallex.WallView");
            if (view != null && cls.isInstance(view)) {
                cls.getMethod("refresh", new Class[0]).invoke(cls.cast(view), new Object[0]);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public static Map<String, Object> getWallProperties(String str) {
        HashMap hashMap = new HashMap();
        hashMap.put(MIntegralConstans.PROPERTIES_UNIT_ID, str);
        hashMap.put(MIntegralConstans.PLUGIN_NAME, new String[]{MIntegralConstans.PLUGIN_WALL});
        hashMap.put(MIntegralConstans.PROPERTIES_LAYOUT_TYPE, Integer.valueOf(3));
        return hashMap;
    }

    public boolean load() {
        if (this.f2865a == null || !this.f2865a.containsKey(MIntegralConstans.PROPERTIES_UNIT_ID)) {
            g.c("", "no unit id.");
        } else {
            a();
        }
        return true;
    }

    private boolean a() {
        try {
            if (this.d != null) {
                this.f2865a.put(MIntegralConstans.PROPERTIES_WALL_ENTRY, this.d);
            }
            if (this.e == null) {
                this.e = new HandlerProvider();
                this.e.insetView(this.c, null, this.f2865a);
            }
            this.e.load();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return true;
    }

    public void release() {
        if (this.e != null) {
            this.e.release();
        }
        if (this.c != null) {
            this.c = null;
        }
        if (this.d != null) {
            this.d = null;
        }
    }

    public void setWallViewBackClickListener(View view, WallViewBackClickListener wallViewBackClickListener) {
        try {
            Class cls = Class.forName("com.mintegral.msdk.appwallex.WallView");
            if (view != null && cls.isInstance(view)) {
                Object cast = cls.cast(view);
                cls.getMethod("setmWallViewClickListener", new Class[]{WallViewBackClickListener.class}).invoke(cast, new Object[]{wallViewBackClickListener});
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public View getWallView(Context context, AppWallTrackingListener appWallTrackingListener) {
        Object obj;
        Bundle bundle = null;
        if (this.f2865a == null || this.f2865a.size() <= 0) {
            return null;
        }
        try {
            Class cls = Class.forName("com.mintegral.msdk.appwallex.WallView");
            obj = cls.getConstructor(new Class[]{Context.class, AppWallTrackingListener.class}).newInstance(new Object[]{context, appWallTrackingListener});
            try {
                Method method = cls.getMethod("setParamsIntent", new Class[]{Bundle.class});
                Object[] objArr = new Object[1];
                Map map = this.f2865a;
                if (map != null) {
                    if (map.containsKey(MIntegralConstans.PROPERTIES_UNIT_ID)) {
                        bundle = new Bundle();
                        bundle.putString(MIntegralConstans.PROPERTIES_UNIT_ID, (String) map.get(MIntegralConstans.PROPERTIES_UNIT_ID));
                        if (map.containsKey(MIntegralConstans.PROPERTIES_WALL_TITLE_BACKGROUND_ID)) {
                            bundle.putInt(MIntegralConstans.PROPERTIES_WALL_TITLE_BACKGROUND_ID, ((Integer) map.get(MIntegralConstans.PROPERTIES_WALL_TITLE_BACKGROUND_ID)).intValue());
                        }
                        if (map.containsKey(MIntegralConstans.PROPERTIES_WALL_TITLE_LOGO_ID)) {
                            bundle.putInt(MIntegralConstans.PROPERTIES_WALL_TITLE_LOGO_ID, ((Integer) map.get(MIntegralConstans.PROPERTIES_WALL_TITLE_LOGO_ID)).intValue());
                        }
                        if (map.containsKey(MIntegralConstans.PROPERTIES_WALL_MAIN_BACKGROUND_ID)) {
                            bundle.putInt(MIntegralConstans.PROPERTIES_WALL_MAIN_BACKGROUND_ID, ((Integer) map.get(MIntegralConstans.PROPERTIES_WALL_MAIN_BACKGROUND_ID)).intValue());
                        }
                        if (map.containsKey(MIntegralConstans.PROPERTIES_WALL_TAB_BACKGROUND_ID)) {
                            bundle.putInt(MIntegralConstans.PROPERTIES_WALL_TAB_BACKGROUND_ID, ((Integer) map.get(MIntegralConstans.PROPERTIES_WALL_TAB_BACKGROUND_ID)).intValue());
                        }
                        if (map.containsKey(MIntegralConstans.PROPERTIES_WALL_TAB_INDICATE_LINE_BACKGROUND_ID)) {
                            bundle.putInt(MIntegralConstans.PROPERTIES_WALL_TAB_INDICATE_LINE_BACKGROUND_ID, ((Integer) map.get(MIntegralConstans.PROPERTIES_WALL_TAB_INDICATE_LINE_BACKGROUND_ID)).intValue());
                        }
                        if (map.containsKey(MIntegralConstans.PROPERTIES_WALL_BUTTON_BACKGROUND_ID)) {
                            bundle.putInt(MIntegralConstans.PROPERTIES_WALL_BUTTON_BACKGROUND_ID, ((Integer) map.get(MIntegralConstans.PROPERTIES_WALL_BUTTON_BACKGROUND_ID)).intValue());
                        }
                        if (map.containsKey(MIntegralConstans.PROPERTIES_WALL_LOAD_ID)) {
                            bundle.putInt(MIntegralConstans.PROPERTIES_WALL_LOAD_ID, ((Integer) map.get(MIntegralConstans.PROPERTIES_WALL_LOAD_ID)).intValue());
                        }
                        if (map.containsKey(MIntegralConstans.PROPERTIES_WALL_STATUS_COLOR)) {
                            int intValue = ((Integer) map.get(MIntegralConstans.PROPERTIES_WALL_STATUS_COLOR)).intValue();
                            if (intValue > 0) {
                                bundle.putInt(MIntegralConstans.PROPERTIES_WALL_STATUS_COLOR, intValue);
                            }
                        }
                        if (map.containsKey(MIntegralConstans.PROPERTIES_WALL_NAVIGATION_COLOR)) {
                            int intValue2 = ((Integer) map.get(MIntegralConstans.PROPERTIES_WALL_NAVIGATION_COLOR)).intValue();
                            if (intValue2 > 0) {
                                bundle.putInt(MIntegralConstans.PROPERTIES_WALL_NAVIGATION_COLOR, intValue2);
                            }
                        }
                        if (map.containsKey(MIntegralConstans.PROPERTIES_WALL_TITLE_BACKGROUND_COLOR)) {
                            int intValue3 = ((Integer) map.get(MIntegralConstans.PROPERTIES_WALL_TITLE_BACKGROUND_COLOR)).intValue();
                            if (intValue3 > 0) {
                                bundle.putInt(MIntegralConstans.PROPERTIES_WALL_TITLE_BACKGROUND_COLOR, intValue3);
                            }
                        }
                        if (map.containsKey(MIntegralConstans.PROPERTIES_WALL_TITLE_LOGO_TEXT)) {
                            String str = (String) map.get(MIntegralConstans.PROPERTIES_WALL_TITLE_LOGO_TEXT);
                            if (!TextUtils.isEmpty(str)) {
                                bundle.putString(MIntegralConstans.PROPERTIES_WALL_TITLE_LOGO_TEXT, str);
                            }
                        }
                        if (map.containsKey(MIntegralConstans.PROPERTIES_WALL_TITLE_LOGO_TEXT_COLOR)) {
                            int intValue4 = ((Integer) map.get(MIntegralConstans.PROPERTIES_WALL_TITLE_LOGO_TEXT_COLOR)).intValue();
                            if (intValue4 > 0) {
                                bundle.putInt(MIntegralConstans.PROPERTIES_WALL_TITLE_LOGO_TEXT_COLOR, intValue4);
                            }
                        }
                        if (map.containsKey(MIntegralConstans.PROPERTIES_WALL_TITLE_LOGO_TEXT_SIZE)) {
                            int intValue5 = ((Integer) map.get(MIntegralConstans.PROPERTIES_WALL_TITLE_LOGO_TEXT_SIZE)).intValue();
                            if (intValue5 > 0) {
                                bundle.putInt(MIntegralConstans.PROPERTIES_WALL_TITLE_LOGO_TEXT_SIZE, intValue5);
                            }
                        }
                        if (map.containsKey(MIntegralConstans.PROPERTIES_WALL_TITLE_LOGO_TEXT_TYPEFACE)) {
                            int intValue6 = ((Integer) map.get(MIntegralConstans.PROPERTIES_WALL_TITLE_LOGO_TEXT_TYPEFACE)).intValue();
                            if (intValue6 > 0) {
                                bundle.putInt(MIntegralConstans.PROPERTIES_WALL_TITLE_LOGO_TEXT_TYPEFACE, intValue6);
                            }
                        }
                        if (map.containsKey(MIntegralConstans.PROPERTIES_WALL_CURRENT_TAB_ID)) {
                            int intValue7 = ((Integer) map.get(MIntegralConstans.PROPERTIES_WALL_CURRENT_TAB_ID)).intValue();
                            if (intValue7 >= 0) {
                                bundle.putInt(MIntegralConstans.PROPERTIES_WALL_CURRENT_TAB_ID, intValue7);
                            }
                        }
                        if (map.containsKey(MIntegralConstans.PROPERTIES_WALL_TAB_SHAPE_COLOR)) {
                            int intValue8 = ((Integer) map.get(MIntegralConstans.PROPERTIES_WALL_TAB_SHAPE_COLOR)).intValue();
                            if (intValue8 >= 0) {
                                bundle.putInt(MIntegralConstans.PROPERTIES_WALL_TAB_SHAPE_COLOR, intValue8);
                            }
                        }
                        if (map.containsKey(MIntegralConstans.PROPERTIES_WALL_TAB_SHAPE_HEIGHT)) {
                            int intValue9 = ((Integer) map.get(MIntegralConstans.PROPERTIES_WALL_TAB_SHAPE_HEIGHT)).intValue();
                            if (intValue9 >= 0) {
                                bundle.putInt(MIntegralConstans.PROPERTIES_WALL_TAB_SHAPE_HEIGHT, intValue9);
                            }
                        }
                        if (map.containsKey(MIntegralConstans.PROPERTIES_WALL_CONFIGCHANGES)) {
                            bundle.putInt(MIntegralConstans.PROPERTIES_WALL_CONFIGCHANGES, ((Integer) map.get(MIntegralConstans.PROPERTIES_WALL_CONFIGCHANGES)).intValue());
                        }
                        if (map.containsKey(MIntegralConstans.WALL_VIEW_VIEWPAGER_NOSCROLL)) {
                            Object obj2 = map.get(MIntegralConstans.WALL_VIEW_VIEWPAGER_NOSCROLL);
                            if (obj2 instanceof Boolean) {
                                bundle.putBoolean(MIntegralConstans.WALL_VIEW_VIEWPAGER_NOSCROLL, ((Boolean) obj2).booleanValue());
                            }
                        }
                        if (map.containsKey(MIntegralConstans.WALL_VIEW_VIEWPAGER_NOSCROLL)) {
                            Object obj3 = map.get(MIntegralConstans.WALL_VIEW_VIEWPAGER_NOSCROLL);
                            if (obj3 instanceof Boolean) {
                                bundle.putBoolean(MIntegralConstans.WALL_VIEW_VIEWPAGER_NOSCROLL, ((Boolean) obj3).booleanValue());
                            }
                        }
                        if (map.containsKey(MIntegralConstans.APPWALL_VIEW_LOAD_RESULT_LISTENER)) {
                            LoadListener loadListener = (LoadListener) map.get(MIntegralConstans.APPWALL_VIEW_LOAD_RESULT_LISTENER);
                            if (loadListener != null) {
                                bundle.putSerializable(MIntegralConstans.APPWALL_VIEW_LOAD_RESULT_LISTENER, loadListener);
                            }
                        }
                    }
                }
                objArr[0] = bundle;
                method.invoke(obj, objArr);
            } catch (Exception e2) {
                e = e2;
                e.printStackTrace();
                return (View) obj;
            }
        } catch (Exception e3) {
            e = e3;
            obj = null;
            e.printStackTrace();
            return (View) obj;
        }
        return (View) obj;
    }

    public void releaseWallView(View view) {
        try {
            Class cls = Class.forName("com.mintegral.msdk.appwallex.WallView");
            if (view != null && cls.isInstance(view)) {
                cls.getMethod("destory", new Class[0]).invoke(cls.cast(view), new Object[0]);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        if (this.f != null) {
            this.f = null;
        }
    }

    public ViewGroup getHandlerContainer() {
        return this.c;
    }

    public void setHandlerContainer(ViewGroup viewGroup) {
        this.c = viewGroup;
    }

    public View getHandlerCustomerLayout() {
        return this.d;
    }

    public void setHandlerCustomerLayout(View view) {
        this.d = view;
    }

    public void setAppWallViewCampaignClickListener(View view, AppWallViewCampaignClickListener appWallViewCampaignClickListener) {
        try {
            Class cls = Class.forName("com.mintegral.msdk.appwallex.WallView");
            if (view != null && cls.isInstance(view)) {
                Object cast = cls.cast(view);
                cls.getMethod("setAppWallViewCampaignClickListener", new Class[]{AppWallViewCampaignClickListener.class}).invoke(cast, new Object[]{appWallViewCampaignClickListener});
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public void setAppWallViewNoMoreDateListener(View view, AppWallViewNoMoreDateListener appWallViewNoMoreDateListener) {
        try {
            Class cls = Class.forName("com.mintegral.msdk.appwallex.WallView");
            if (view != null && cls.isInstance(view)) {
                Object cast = cls.cast(view);
                cls.getMethod("setAppWallViewNoMoreDateListener", new Class[]{AppWallViewNoMoreDateListener.class}).invoke(cast, new Object[]{appWallViewNoMoreDateListener});
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public void setAppWallViewLoadingEnd(View view, AppWallViewLoadingEndListener appWallViewLoadingEndListener) {
        try {
            Class cls = Class.forName("com.mintegral.msdk.appwallex.WallView");
            if (view != null && cls.isInstance(view)) {
                Object cast = cls.cast(view);
                cls.getMethod("setAppWallViewLoadingEndListener", new Class[]{AppWallViewLoadingEndListener.class}).invoke(cast, new Object[]{appWallViewLoadingEndListener});
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public void startWall() {
        if (this.f2865a == null || !this.f2865a.containsKey(MIntegralConstans.PROPERTIES_UNIT_ID)) {
            g.c("", "no unit id.");
            return;
        }
        if (this.e == null) {
            this.e = new HandlerProvider();
        }
        this.e.startShuffleOrAppwall(this.f, this.f2865a);
    }
}
