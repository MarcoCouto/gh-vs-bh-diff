package com.mintegral.msdk.nativex.adapter;

import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import com.mintegral.msdk.nativex.view.MTGNativeRollView.a;
import com.mintegral.msdk.out.Frame;
import java.util.ArrayList;
import java.util.List;

public class RollingAdapter extends PagerAdapter {

    /* renamed from: a reason: collision with root package name */
    private List<Frame> f2803a = new ArrayList();
    private List<View> b = new ArrayList();
    private a c;

    public boolean isViewFromObject(View view, Object obj) {
        return view == obj;
    }

    public final void a(a aVar) {
        this.c = aVar;
    }

    public RollingAdapter(List<Frame> list) {
        this.f2803a = list;
    }

    public int getCount() {
        return this.f2803a.size();
    }

    public Object instantiateItem(ViewGroup viewGroup, int i) {
        if (!(this.c == null || this.f2803a == null || this.f2803a.size() <= 0)) {
            a aVar = this.c;
            this.f2803a.get(i);
            View a2 = aVar.a();
            if (a2 != null) {
                this.b.add(a2);
                viewGroup.addView(a2);
                return a2;
            }
        }
        return null;
    }

    public void destroyItem(ViewGroup viewGroup, int i, Object obj) {
        viewGroup.removeView((View) this.b.get(i));
    }
}
