package com.mintegral.msdk.nativex.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import com.mintegral.msdk.base.utils.k;
import com.mintegral.msdk.out.Frame;
import com.mintegral.msdk.out.NativeListener.FilpListener;
import java.util.List;

public class MTGNativeRollView extends LinearLayout {

    /* renamed from: a reason: collision with root package name */
    private RollingBCView f2832a;
    private Context b;
    private FilpListener c;

    public interface a {
        View a();
    }

    public void setFilpListening(FilpListener filpListener) {
        if (filpListener != null) {
            this.c = filpListener;
            this.f2832a.setFilpListening(filpListener);
        }
    }

    public MTGNativeRollView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public MTGNativeRollView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.b = context;
        this.f2832a = new RollingBCView(context);
        addView(this.f2832a);
        double f = (double) k.f(context);
        Double.isNaN(f);
        this.f2832a.setLayoutParams(new LayoutParams((int) (f * 0.9d), -2));
        setClipChildren(false);
    }

    public MTGNativeRollView(Context context) {
        this(context, null);
    }

    public void setData(List<Frame> list, Context context, String str, a aVar) {
        this.f2832a.setData(list, context, str, aVar);
    }

    public void setFrameWidth(int i) {
        this.f2832a.setLayoutParams(new LayoutParams(i, -2));
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        return this.f2832a.dispatchTouchEvent(motionEvent);
    }
}
