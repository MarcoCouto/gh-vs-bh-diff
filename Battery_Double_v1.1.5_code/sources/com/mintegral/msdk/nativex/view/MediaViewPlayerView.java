package com.mintegral.msdk.nativex.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.SurfaceTexture;
import android.graphics.drawable.AnimationDrawable;
import android.os.Build.VERSION;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.TextureView;
import android.view.TextureView.SurfaceTextureListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.DecelerateInterpolator;
import android.webkit.URLUtil;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.base.utils.k;
import com.mintegral.msdk.base.utils.p;
import com.mintegral.msdk.base.utils.s;
import com.mintegral.msdk.out.Campaign;
import com.mintegral.msdk.playercommon.c;
import com.mintegral.msdk.playercommon.d;
import com.mintegral.msdk.videocommon.view.MyImageView;
import java.io.File;
import java.util.Timer;
import java.util.TimerTask;

public class MediaViewPlayerView extends LinearLayout implements c {
    /* access modifiers changed from: private */
    public CampaignEx A;
    /* access modifiers changed from: private */
    public com.mintegral.msdk.nativex.listener.a B;
    private Timer C;
    private Handler D;
    private com.mintegral.msdk.videocommon.download.a E;
    /* access modifiers changed from: private */
    public d F;
    private a G;

    /* renamed from: a reason: collision with root package name */
    private boolean f2833a = false;
    /* access modifiers changed from: private */
    public boolean b = false;
    /* access modifiers changed from: private */
    public boolean c = false;
    /* access modifiers changed from: private */
    public boolean d = false;
    /* access modifiers changed from: private */
    public boolean e = false;
    /* access modifiers changed from: private */
    public boolean f = false;
    /* access modifiers changed from: private */
    public boolean g = true;
    /* access modifiers changed from: private */
    public boolean h = false;
    /* access modifiers changed from: private */
    public boolean i = true;
    /* access modifiers changed from: private */
    public boolean j = false;
    private boolean k = true;
    private boolean l = true;
    private TextureView m;
    private LinearLayout n;
    /* access modifiers changed from: private */
    public Surface o;
    private ProgressBar p;
    /* access modifiers changed from: private */
    public MyImageView q;
    /* access modifiers changed from: private */
    public ImageView r;
    /* access modifiers changed from: private */
    public ImageView s;
    private ImageView t;
    private ImageView u;
    private View v;
    private AnimationDrawable w;
    private AlphaAnimation x;
    private String y;
    /* access modifiers changed from: private */
    public String z;

    public static class a {

        /* renamed from: a reason: collision with root package name */
        private MediaViewPlayerView f2842a;

        public a(MediaViewPlayerView mediaViewPlayerView) {
            this.f2842a = mediaViewPlayerView;
        }

        public void a() {
            try {
                g.b("MediaViewPlayerView", "=========onPlayCompleted");
                if (this.f2842a != null) {
                    if (this.f2842a.i) {
                        g.b("MediaViewPlayerView", "播放结束 调用onClickPlayButton");
                        this.f2842a.onClickPlayButton();
                        return;
                    }
                    g.b("MediaViewPlayerView", "播放结束 不能循环播放 显示endcardView");
                    this.f2842a.b();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private class b implements SurfaceTextureListener {
        public final void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {
        }

        private b() {
        }

        /* synthetic */ b(MediaViewPlayerView mediaViewPlayerView, byte b) {
            this();
        }

        public final void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int i, int i2) {
            String str = "MediaViewPlayerView";
            try {
                StringBuilder sb = new StringBuilder("onSurfaceTextureAvailable 进来:");
                sb.append(MediaViewPlayerView.this.A == null ? "appname" : MediaViewPlayerView.this.A.getAppName());
                sb.append(" url:");
                sb.append(MediaViewPlayerView.this.z);
                g.d(str, sb.toString());
                MediaViewPlayerView.this.o = new Surface(surfaceTexture);
                MediaViewPlayerView.this.f = true;
                MediaViewPlayerView.this.h = true;
                if (MediaViewPlayerView.this.d) {
                    g.b("MediaViewPlayerView", "onSurfaceTextureAvailable 在全屏");
                    if (MediaViewPlayerView.this.g) {
                        MediaViewPlayerView.this.c = false;
                        MediaViewPlayerView.this.g = false;
                        g.b("MediaViewPlayerView", "onSurfaceTextureAvailable 在全屏 mIsNeedToRepeatPrepare置为false");
                    }
                    if (!MediaViewPlayerView.this.isComplete()) {
                        if (!MediaViewPlayerView.this.e) {
                            g.b("MediaViewPlayerView", "onSurfaceTextureAvailable 在全屏 startOrPlayVideo");
                            MediaViewPlayerView.this.startOrPlayVideo();
                            return;
                        }
                    }
                    g.b("MediaViewPlayerView", "onSurfaceTextureAvailable 在全屏 showPlayEndView");
                    MediaViewPlayerView.this.b();
                    return;
                }
                g.b("MediaViewPlayerView", "onSurfaceTextureAvailable 在半屏");
                if (MediaViewPlayerView.this.b) {
                    if (MediaViewPlayerView.this.hasPrepare()) {
                        if (MediaViewPlayerView.this.isComplete()) {
                            StringBuilder sb2 = new StringBuilder("onSurfaceTextureAvailable 在半屏 自动播放showPlayEndView hasPrepare():");
                            sb2.append(MediaViewPlayerView.this.hasPrepare());
                            sb2.append(" isComplete:");
                            sb2.append(MediaViewPlayerView.this.isComplete());
                            g.b("MediaViewPlayerView", sb2.toString());
                            MediaViewPlayerView.this.b();
                            return;
                        }
                    }
                    StringBuilder sb3 = new StringBuilder("onSurfaceTextureAvailable 在半屏 自动播放startOrPlayVideo hasPrepare():");
                    sb3.append(MediaViewPlayerView.this.hasPrepare());
                    sb3.append(" isComplete:");
                    sb3.append(MediaViewPlayerView.this.isComplete());
                    g.b("MediaViewPlayerView", sb3.toString());
                    MediaViewPlayerView.this.startOrPlayVideo();
                    return;
                }
                if (MediaViewPlayerView.this.hasPrepare()) {
                    if (!MediaViewPlayerView.this.isComplete()) {
                        StringBuilder sb4 = new StringBuilder("onSurfaceTextureAvailable 在半屏 startOrPlayVideo hasPrepare():");
                        sb4.append(MediaViewPlayerView.this.hasPrepare());
                        sb4.append(" isComplete:");
                        sb4.append(MediaViewPlayerView.this.isComplete());
                        g.b("MediaViewPlayerView", sb4.toString());
                        MediaViewPlayerView.this.startOrPlayVideo();
                        return;
                    }
                }
                StringBuilder sb5 = new StringBuilder("onSurfaceTextureAvailable 在半屏 点击播放showPlayEndView hasPrepare():");
                sb5.append(MediaViewPlayerView.this.hasPrepare());
                sb5.append(" isComplete:");
                sb5.append(MediaViewPlayerView.this.isComplete());
                g.b("MediaViewPlayerView", sb5.toString());
                MediaViewPlayerView.this.b();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public final void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int i, int i2) {
            g.b("MediaViewPlayerView", "onSurfaceTextureSizeChanged ");
        }

        public final boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {
            String str = "MediaViewPlayerView";
            try {
                StringBuilder sb = new StringBuilder("onSurfaceTextureDestroyed:");
                sb.append(MediaViewPlayerView.this.A == null ? "appname" : MediaViewPlayerView.this.A.getAppName());
                g.d(str, sb.toString());
                if (MediaViewPlayerView.this.F != null && MediaViewPlayerView.this.F.f()) {
                    MediaViewPlayerView.this.pause();
                }
                if (!MediaViewPlayerView.this.d && MediaViewPlayerView.this.F != null) {
                    MediaViewPlayerView.this.F.c();
                }
                MediaViewPlayerView.this.c = true;
                MediaViewPlayerView.this.f = false;
            } catch (Throwable th) {
                th.printStackTrace();
            }
            return true;
        }
    }

    public void OnBufferingEnd() {
    }

    public void OnBufferingStart(String str) {
    }

    public void setMediaViewPlayListener(a aVar) {
        this.G = aVar;
    }

    public MediaViewPlayerView(Context context) {
        super(context);
        a();
    }

    public MediaViewPlayerView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a();
    }

    public boolean initPlayerViewData(String str, CampaignEx campaignEx, boolean z2, c cVar, com.mintegral.msdk.videocommon.download.a aVar, String str2) {
        try {
            if (TextUtils.isEmpty(str)) {
                g.b("MediaViewPlayerView", "playUrl==null return ");
                return false;
            } else if (campaignEx == null) {
                g.b("MediaViewPlayerView", "campaign ==null return ");
                return false;
            } else {
                this.z = str;
                this.b = z2;
                this.A = campaignEx;
                this.E = aVar;
                this.y = str2;
                this.F.a(this.A.getVideoUrlEncode(), (View) this.q, cVar);
                try {
                    if (this.A != null) {
                        String imageUrl = this.A.getImageUrl();
                        if (s.a(imageUrl)) {
                            g.b("MediaViewPlayerView", "imageUrl isnull initPlayEndPic return");
                        } else if (getContext() != null) {
                            if (com.mintegral.msdk.base.common.c.b.a(getContext()).b(imageUrl)) {
                                Bitmap a2 = com.mintegral.msdk.base.common.c.b.a(com.mintegral.msdk.base.controller.a.d().h()).a(imageUrl);
                                if (!(this.q == null || a2 == null)) {
                                    this.q.setImageUrl(imageUrl);
                                    this.q.setImageBitmap(a2);
                                    this.q.setVisibility(0);
                                }
                            } else {
                                com.mintegral.msdk.base.common.c.b.a(getContext()).a(imageUrl, (com.mintegral.msdk.base.common.c.c) new com.mintegral.msdk.base.common.c.c() {
                                    public final void onFailedLoad(String str, String str2) {
                                    }

                                    public final void onSuccessLoad(Bitmap bitmap, String str) {
                                        if (MediaViewPlayerView.this.q != null && bitmap != null) {
                                            MediaViewPlayerView.this.q.setImageUrl(str);
                                            MediaViewPlayerView.this.q.setImageBitmap(bitmap);
                                        }
                                    }
                                });
                            }
                        }
                    }
                } catch (Throwable th) {
                    th.printStackTrace();
                }
                this.f2833a = true;
                return true;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            this.f2833a = false;
        }
    }

    public void playVideo() {
        try {
            if (!this.f2833a) {
                g.b("MediaViewPlayerView", "playVideo() init failed 播放失败");
            } else if (this.F == null) {
                g.b("MediaViewPlayerView", "playVideo() mVideoFeedsPlayer is null 播放失败");
            } else if (!this.f) {
                b();
                g.b("MediaViewPlayerView", "playVideo() mSurfaceTextureAvailable no init return");
            } else {
                if ((!TextUtils.isEmpty(this.z) && this.z.startsWith("http")) || this.z.startsWith("https")) {
                    this.z = j();
                }
                g.b("MediaViewPlayerView", "playVideo() play");
                c();
                this.F.a(this.z, this.o);
                if (this.j) {
                    this.F.e();
                } else {
                    this.F.d();
                }
                this.c = false;
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public void startOrPlayVideo() {
        String str = "MediaViewPlayerView";
        try {
            StringBuilder sb = new StringBuilder("startOrPlayVideo() mIsNeedToRepeatPrepare:");
            sb.append(this.c);
            sb.append(" mhasprepare:");
            sb.append(hasPrepare());
            g.b(str, sb.toString());
            if (!this.f) {
                b();
                g.b("MediaViewPlayerView", "startOrPlayVideo() mSurfaceTextureAvailable no init return");
                return;
            }
            if (!this.c) {
                if (hasPrepare()) {
                    g.b("MediaViewPlayerView", "startOrPlayVideo() start");
                    try {
                        if (this.F == null) {
                            g.b("MediaViewPlayerView", "start() mVideoFeedsPlayer is null return");
                            return;
                        }
                        c();
                        if (this.h) {
                            g.d("MediaViewPlayerView", "start() startOrPlayVideo need setSurface final");
                            this.F.a(this.o);
                            this.h = false;
                            return;
                        }
                        g.b("MediaViewPlayerView", "start() startOrPlayVideo final");
                        this.F.a((Surface) null);
                        return;
                    } catch (Throwable th) {
                        g.c("MediaViewPlayerView", th.getMessage(), th);
                        return;
                    }
                }
            }
            g.b("MediaViewPlayerView", "startOrPlayVideo() playVideo");
            playVideo();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public void pause() {
        try {
            if (this.F != null) {
                this.F.a();
            }
            e();
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public void stop() {
        try {
            if (this.F != null) {
                this.F.b();
            }
            e();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public void release() {
        try {
            if (this.F != null) {
                this.F.c();
                this.F = null;
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public boolean hasPrepare() {
        try {
            if (this.F != null) {
                return this.F.g();
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
        return false;
    }

    public boolean isComplete() {
        try {
            if (this.F != null) {
                return this.F.h();
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
        return false;
    }

    public boolean isPlaying() {
        try {
            if (this.F != null) {
                return this.F.i();
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
        return false;
    }

    public void openSound() {
        this.j = true;
        try {
            if (this.F != null) {
                this.t.setImageResource(p.a(getContext(), "mintegral_nativex_sound_open", "drawable"));
                this.F.e();
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public void closeSound() {
        this.j = false;
        try {
            if (this.F != null) {
                this.t.setImageResource(p.a(getContext(), "mintegral_nativex_sound_close", "drawable"));
                this.F.d();
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public void setIsFrontDesk(boolean z2) {
        try {
            if (this.F != null) {
                this.F.a(z2);
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public boolean halfLoadingViewisVisible() {
        try {
            if (this.F != null && this.F.j()) {
                return true;
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
        return false;
    }

    public void setIsComplete(boolean z2) {
        try {
            if (this.F != null) {
                this.F.b(z2);
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public void setEnterFullScreen() {
        try {
            g.b("MediaViewPlayerView", "setEnterFullScreen");
            this.d = true;
            this.g = true;
            this.t.setVisibility(0);
            e();
        } catch (Throwable th) {
            g.c("MediaViewPlayerView", th.getMessage(), th);
        }
    }

    public void setExitFullScreen() {
        try {
            g.b("MediaViewPlayerView", "setExitFullScreen");
            this.d = false;
            this.c = false;
            g.b("MediaViewPlayerView", "setExitFullScreen mIsNeedToRepeatPrepare=false");
            this.t.setVisibility(8);
            d();
            c();
        } catch (Throwable th) {
            g.c("MediaViewPlayerView", th.getMessage(), th);
        }
    }

    public boolean curIsFullScreen() {
        return this.d;
    }

    public void setIsActivePause(boolean z2) {
        this.e = z2;
    }

    public boolean getIsActiviePause() {
        return this.e;
    }

    public Campaign getCampaign() {
        return this.A;
    }

    public void setAllowLoopPlay(boolean z2) {
        this.i = z2;
    }

    public void setOnMediaViewPlayerViewListener(com.mintegral.msdk.nativex.listener.a aVar) {
        this.B = aVar;
    }

    public void onClickPlayButton() {
        try {
            c();
            g();
            setIsComplete(false);
            if (!hasPrepare() || this.c) {
                g.b("MediaViewPlayerView", "点击播放 playVideo()");
                playVideo();
            } else {
                StringBuilder sb = new StringBuilder("startOrPlayVideo() hasPrepare():");
                sb.append(hasPrepare());
                sb.append(" mIsNeedToRepeatPrepare:");
                sb.append(this.c);
                g.b("MediaViewPlayerView", sb.toString());
                startOrPlayVideo();
            }
            if (this.e && this.B != null) {
                this.B.d();
            }
            this.e = false;
        } catch (Throwable th) {
            g.c("MediaViewPlayerView", th.getMessage(), th);
        }
    }

    public void onClickPlayerView() {
        try {
            if (this.q != null && this.q.getVisibility() == 0) {
                g.b("MediaViewPlayerView", "playend is visibility return");
            } else if (!isPlaying()) {
                g.b("MediaViewPlayerView", "isplaying return");
            } else if (this.s == null) {
                g.b("MediaViewPlayerView", "pause id is null return");
            } else if (this.s.getVisibility() == 0) {
                g.b("MediaViewPlayerView", "gone durview");
                gonePauseView();
                i();
            } else {
                g.b("MediaViewPlayerView", "show durview");
                if (this.x != null) {
                    this.x.cancel();
                }
                this.x = new AlphaAnimation(0.0f, 1.0f);
                this.x.setDuration(300);
                this.x.setInterpolator(new DecelerateInterpolator());
                this.x.setAnimationListener(new AnimationListener() {
                    public final void onAnimationRepeat(Animation animation) {
                    }

                    public final void onAnimationStart(Animation animation) {
                    }

                    public final void onAnimationEnd(Animation animation) {
                        MediaViewPlayerView.this.s.setVisibility(0);
                        MediaViewPlayerView.h(MediaViewPlayerView.this);
                    }
                });
                f();
                this.v.startAnimation(this.x);
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public void showPlayView() {
        this.r.setVisibility(0);
    }

    public void gonePauseView() {
        this.D.post(new Runnable() {
            public final void run() {
                try {
                    MediaViewPlayerView.this.h();
                    MediaViewPlayerView.this.g();
                    g.b("MediaViewPlayerView", "隐藏进度条");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void b() {
        try {
            this.q.setVisibility(0);
            this.r.setVisibility(0);
            f();
            h();
            this.p.setVisibility(8);
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    private void c() {
        try {
            this.q.setVisibility(8);
            this.r.setVisibility(8);
            g();
            showProgressView(this.l);
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    private void d() {
        if (!this.d && isPlaying() && this.u != null && this.u.getVisibility() != 0 && this.k) {
            this.u.setVisibility(0);
        }
    }

    private void e() {
        if (this.u.getVisibility() == 0) {
            this.u.setVisibility(8);
        }
    }

    public void showProgressView(boolean z2) {
        this.l = z2;
        if (this.p != null) {
            this.p.setVisibility(z2 ? 0 : 4);
        }
    }

    public void showSoundIndicator(boolean z2) {
        this.k = z2;
        if (this.k) {
            d();
        } else {
            e();
        }
    }

    /* access modifiers changed from: private */
    public void f() {
        this.v.setVisibility(0);
    }

    /* access modifiers changed from: private */
    public void g() {
        if (this.r.getVisibility() != 0) {
            this.v.setVisibility(8);
        }
    }

    /* access modifiers changed from: private */
    public void h() {
        this.s.setVisibility(8);
    }

    public void onPlayStarted(int i2) {
        if (this.B != null && !TextUtils.isEmpty(this.z)) {
            this.B.a(this.z);
        }
    }

    public void onPlayProgress(int i2, int i3) {
        try {
            c();
            d();
            this.e = false;
            this.c = false;
        } catch (Throwable th) {
            g.c("MediaViewPlayerView", th.getMessage(), th);
        }
    }

    public void onPlayCompleted() {
        g.b("MediaViewPlayerView", "=========onPlayCompleted");
        if (this.G != null) {
            this.G.a();
            return;
        }
        try {
            if (this.i) {
                g.b("MediaViewPlayerView", "播放结束 调用onClickPlayButton");
                onClickPlayButton();
                return;
            }
            g.b("MediaViewPlayerView", "播放结束 不能循环播放 显示endcardView");
            b();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public void onPlayError(String str) {
        String str2 = "MediaViewPlayerView";
        try {
            StringBuilder sb = new StringBuilder("onPlayError:");
            sb.append(str);
            g.b(str2, sb.toString());
            this.c = true;
            b();
            k();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public void onPlaySetDataSourceError(String str) {
        String str2 = "MediaViewPlayerView";
        try {
            StringBuilder sb = new StringBuilder("onPlaySetDataSourceError:");
            sb.append(str);
            g.b(str2, sb.toString());
            this.c = true;
            k();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    private void i() {
        try {
            if (this.C != null) {
                this.C.cancel();
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    private String j() {
        try {
            if (this.A == null) {
                return null;
            }
            try {
                if (this.E == null) {
                    StringBuilder sb = new StringBuilder();
                    sb.append(this.A.getId());
                    sb.append(this.A.getVideoUrlEncode());
                    sb.append(this.A.getBidToken());
                    this.E = com.mintegral.msdk.videocommon.download.c.getInstance().a(this.y, sb.toString());
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
            if (this.E != null) {
                int h2 = this.E.h();
                if (h2 == 5) {
                    String c2 = this.E.c();
                    if (new File(c2).exists() && this.E.d() == k.a(new File(c2))) {
                        StringBuilder sb2 = new StringBuilder("本地已下载完 拿本地播放地址：");
                        sb2.append(c2);
                        sb2.append(" state：");
                        sb2.append(h2);
                        g.b("MediaViewPlayerView", sb2.toString());
                        return c2;
                    }
                }
            }
            String videoUrlEncode = this.A.getVideoUrlEncode();
            if (s.b(videoUrlEncode)) {
                StringBuilder sb3 = new StringBuilder("本地尚未下载完 拿网络地址：");
                sb3.append(videoUrlEncode);
                g.b("MediaViewPlayerView", sb3.toString());
                return videoUrlEncode;
            }
            return null;
        } catch (Exception e3) {
            e3.printStackTrace();
        }
    }

    private void k() {
        try {
            if (URLUtil.isNetworkUrl(this.z)) {
                g.b("MediaViewPlayerView", "playerview  dwLocalAddressplayError playurl is network return");
                return;
            }
            String videoUrlEncode = this.A.getVideoUrlEncode();
            if (s.b(videoUrlEncode)) {
                this.z = videoUrlEncode;
                StringBuilder sb = new StringBuilder("playerview dwLocalAddressplayError 用网络地址抄底播放");
                sb.append(videoUrlEncode);
                g.b("MediaViewPlayerView", sb.toString());
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    private void a() {
        try {
            View inflate = LayoutInflater.from(getContext()).inflate(p.a(getContext(), "mintegral_nativex_playerview", TtmlNode.TAG_LAYOUT), null);
            if (inflate != null) {
                this.n = (LinearLayout) inflate.findViewById(p.a(getContext(), "mintegral_ll_loading", "id"));
                if (VERSION.SDK_INT >= 14) {
                    this.m = (TextureView) inflate.findViewById(p.a(getContext(), "mintegral_textureview", "id"));
                    this.m.setKeepScreenOn(true);
                    this.m.setSurfaceTextureListener(new b(this, 0));
                }
                this.p = (ProgressBar) inflate.findViewById(p.a(getContext(), "mintegral_progress", "id"));
                this.q = (MyImageView) inflate.findViewById(p.a(getContext(), "mintegral_iv_playend_pic", "id"));
                this.r = (ImageView) inflate.findViewById(p.a(getContext(), "mintegral_iv_play", "id"));
                this.s = (ImageView) inflate.findViewById(p.a(getContext(), "mintegral_iv_pause", "id"));
                this.t = (ImageView) inflate.findViewById(p.a(getContext(), "mintegral_iv_sound", "id"));
                this.v = inflate.findViewById(p.a(getContext(), "mobivsta_view_cover", "id"));
                this.u = (ImageView) inflate.findViewById(p.a(getContext(), "mintegral_iv_sound_animation", "id"));
                this.w = (AnimationDrawable) this.u.getDrawable();
                this.w.start();
                this.t.setOnClickListener(new OnClickListener() {
                    public final void onClick(View view) {
                        try {
                            if (MediaViewPlayerView.this.j) {
                                MediaViewPlayerView.this.closeSound();
                                if (MediaViewPlayerView.this.B != null) {
                                    MediaViewPlayerView.this.B.a();
                                }
                            } else {
                                MediaViewPlayerView.this.openSound();
                                if (MediaViewPlayerView.this.B != null) {
                                    MediaViewPlayerView.this.B.b();
                                }
                            }
                        } catch (Throwable th) {
                            th.printStackTrace();
                        }
                    }
                });
                this.s.setOnClickListener(new OnClickListener() {
                    public final void onClick(View view) {
                        try {
                            MediaViewPlayerView.this.pause();
                            MediaViewPlayerView.this.r.setVisibility(0);
                            MediaViewPlayerView.this.f();
                            MediaViewPlayerView.this.h();
                            if (MediaViewPlayerView.this.B != null) {
                                MediaViewPlayerView.this.B.c();
                            }
                            MediaViewPlayerView.this.e = true;
                        } catch (Throwable th) {
                            g.c("MediaViewPlayerView", th.getMessage(), th);
                        }
                    }
                });
                this.r.setOnClickListener(new OnClickListener() {
                    public final void onClick(View view) {
                        MediaViewPlayerView.this.onClickPlayButton();
                    }
                });
                addView(inflate, -1, -1);
            }
        } catch (Throwable th) {
            try {
                th.printStackTrace();
            } catch (Exception e2) {
                e2.printStackTrace();
                return;
            }
        }
        this.F = new d();
        this.F.a((c) this);
        this.D = new Handler() {
            public final void handleMessage(Message message) {
            }
        };
    }

    public void onPlayProgressMS(int i2, int i3) {
        try {
            if (this.p != null && this.p.getVisibility() == 0) {
                if (i3 > 0) {
                    this.p.setMax(i3);
                }
                if (i2 >= 0) {
                    this.p.setProgress(i2 + 1);
                }
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    static /* synthetic */ void h(MediaViewPlayerView mediaViewPlayerView) {
        mediaViewPlayerView.i();
        mediaViewPlayerView.C = new Timer();
        mediaViewPlayerView.C.schedule(new TimerTask() {
            public final void run() {
                try {
                    MediaViewPlayerView.this.gonePauseView();
                } catch (Throwable th) {
                    g.c("MediaViewPlayerView", th.getMessage(), th);
                }
            }
        }, 2000);
    }
}
