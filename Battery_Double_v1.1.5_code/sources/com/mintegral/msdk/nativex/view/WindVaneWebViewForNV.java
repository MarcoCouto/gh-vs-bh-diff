package com.mintegral.msdk.nativex.view;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Base64;
import android.view.KeyEvent;
import android.view.MotionEvent;
import com.mintegral.msdk.base.common.d.a;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.entity.p;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.mtgjscommon.windvane.WindVaneWebView;
import com.mintegral.msdk.nativex.listener.b;
import org.json.JSONObject;

public class WindVaneWebViewForNV extends WindVaneWebView {
    private static String e = "WindVaneWebViewForNV";
    private b f;
    private boolean g = false;

    public void setInterceptTouch(boolean z) {
        this.g = z;
    }

    public void setBackListener(b bVar) {
        this.f = bVar;
    }

    public WindVaneWebViewForNV(Context context) {
        super(context);
    }

    public WindVaneWebViewForNV(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public WindVaneWebViewForNV(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (this.g) {
            return false;
        }
        return super.onTouchEvent(motionEvent);
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        String str = e;
        StringBuilder sb = new StringBuilder("====webview 返回keyCode:");
        sb.append(i);
        g.a(str, sb.toString());
        if (i == 4) {
            g.a(e, "====webview 返回");
            if (this.f != null) {
                this.f.a();
                return true;
            }
        }
        return super.onKeyDown(i, keyEvent);
    }

    public void webViewShow(CampaignEx campaignEx, String str) {
        try {
            g.a(e, "webviewshow");
            com.mintegral.msdk.mtgjscommon.windvane.g.a();
            com.mintegral.msdk.mtgjscommon.windvane.g.a(this, "webviewshow", "");
            p pVar = new p();
            pVar.k(campaignEx.getRequestIdNotice());
            if (getContext() != null) {
                if (getContext().getApplicationContext() != null) {
                    pVar.m(campaignEx.getId());
                    pVar.a(campaignEx.isMraid() ? p.f2605a : p.b);
                    a.b(pVar, getContext().getApplicationContext(), str);
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public void orientation(boolean z) {
        try {
            JSONObject jSONObject = new JSONObject();
            if (z) {
                jSONObject.put("orientation", "landscape");
            } else {
                jSONObject.put("orientation", "portrait");
            }
            String encodeToString = Base64.encodeToString(jSONObject.toString().getBytes(), 2);
            com.mintegral.msdk.mtgjscommon.windvane.g.a();
            com.mintegral.msdk.mtgjscommon.windvane.g.a(this, "orientation", encodeToString);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }
}
