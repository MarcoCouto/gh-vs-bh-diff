package com.mintegral.msdk.nativex.view.mtgfullview;

import android.content.Context;
import android.view.View;
import java.lang.ref.WeakReference;

/* compiled from: FullViewFactory */
public class a extends c {
    private static volatile a b;

    /* renamed from: a reason: collision with root package name */
    private WeakReference<Context> f2847a;

    /* renamed from: com.mintegral.msdk.nativex.view.mtgfullview.a$1 reason: invalid class name */
    /* compiled from: FullViewFactory */
    static /* synthetic */ class AnonymousClass1 {

        /* renamed from: a reason: collision with root package name */
        static final /* synthetic */ int[] f2848a = new int[com.mintegral.msdk.nativex.view.mtgfullview.BaseView.a.a().length];

        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0011 */
        static {
            f2848a[com.mintegral.msdk.nativex.view.mtgfullview.BaseView.a.f2846a - 1] = 1;
            try {
                f2848a[com.mintegral.msdk.nativex.view.mtgfullview.BaseView.a.b - 1] = 2;
            } catch (NoSuchFieldError unused) {
            }
        }
    }

    public static a a(Context context) {
        if (b == null) {
            synchronized (a.class) {
                if (b == null) {
                    b = new a(context);
                }
            }
        }
        return b;
    }

    private a(Context context) {
        this.f2847a = new WeakReference<>(context);
    }

    public final <T extends View> T a(int i) {
        Context context = (Context) this.f2847a.get();
        T t = null;
        if (context == null) {
            return null;
        }
        switch (AnonymousClass1.f2848a[i - 1]) {
            case 1:
                t = new MIntegralTopFullView(context);
                break;
            case 2:
                t = new MIntegralFullView(context);
                break;
        }
        if (t != null) {
            t.setStytle$64568c2d(i);
        }
        return t;
    }
}
