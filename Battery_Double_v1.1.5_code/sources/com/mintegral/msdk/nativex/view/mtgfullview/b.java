package com.mintegral.msdk.nativex.view.mtgfullview;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Build.VERSION;
import android.os.Handler;
import android.support.v4.view.InputDeviceCompat;
import android.support.v7.widget.helper.ItemTouchHelper.Callback;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.LinearInterpolator;
import android.widget.RelativeLayout.LayoutParams;
import com.mintegral.msdk.base.common.c.c;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.utils.k;
import com.mintegral.msdk.base.utils.p;
import com.mintegral.msdk.nativex.view.mtgfullview.BaseView.a;
import java.lang.ref.WeakReference;

/* compiled from: FullViewManager */
public class b {
    private static volatile b b;

    /* renamed from: a reason: collision with root package name */
    private WeakReference<Context> f2849a;

    /* renamed from: com.mintegral.msdk.nativex.view.mtgfullview.b$3 reason: invalid class name */
    /* compiled from: FullViewManager */
    static /* synthetic */ class AnonymousClass3 {

        /* renamed from: a reason: collision with root package name */
        static final /* synthetic */ int[] f2852a = new int[a.a().length];

        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0011 */
        static {
            f2852a[a.f2846a - 1] = 1;
            try {
                f2852a[a.b - 1] = 2;
            } catch (NoSuchFieldError unused) {
            }
        }
    }

    private b(Context context) {
        this.f2849a = new WeakReference<>(context);
    }

    public static b a(Context context) {
        if (b == null) {
            synchronized (b.class) {
                if (b == null) {
                    b = new b(context);
                }
            }
        }
        return b;
    }

    public final void a(int i, CampaignEx campaignEx, BaseView baseView) {
        switch (AnonymousClass3.f2852a[i - 1]) {
            case 1:
                final MIntegralTopFullView mIntegralTopFullView = (MIntegralTopFullView) baseView;
                if (mIntegralTopFullView != null) {
                    Context context = (Context) this.f2849a.get();
                    if (context != null) {
                        com.mintegral.msdk.base.common.c.b.a(context).a(campaignEx.getIconUrl(), (c) new c() {
                            public final void onFailedLoad(String str, String str2) {
                            }

                            public final void onSuccessLoad(Bitmap bitmap, String str) {
                                mIntegralTopFullView.getMintegralFullViewDisplayIcon().setImageBitmap(b.a(bitmap));
                            }
                        });
                    }
                    mIntegralTopFullView.getMintegralFullViewDisplayTitle().setText(campaignEx.getAppName());
                    mIntegralTopFullView.getMintegralFullViewDisplayDscription().setText(campaignEx.getAppDesc());
                    mIntegralTopFullView.getMintegralFullTvInstall().setText(campaignEx.getAdCall());
                    mIntegralTopFullView.getStarLevelLayoutView().setRating((int) campaignEx.getRating());
                    return;
                }
                break;
            case 2:
                baseView.getMintegralFullTvInstall().setText(campaignEx.getAdCall());
                break;
        }
    }

    public static void a(boolean z, BaseView baseView) {
        if (baseView instanceof MIntegralTopFullView) {
            MIntegralTopFullView mIntegralTopFullView = (MIntegralTopFullView) baseView;
            int i = z ? 0 : 8;
            mIntegralTopFullView.getMintegralFullViewDisplayIcon().setVisibility(i);
            mIntegralTopFullView.getMintegralFullViewDisplayTitle().setVisibility(i);
            mIntegralTopFullView.getMintegralFullViewDisplayDscription().setVisibility(i);
            mIntegralTopFullView.getStarLevelLayoutView().setVisibility(i);
        }
    }

    public final void a(boolean z, BaseView baseView, int i) {
        LayoutParams layoutParams;
        Context context = (Context) this.f2849a.get();
        if (context != null) {
            if (z) {
                layoutParams = new LayoutParams((int) (a(z) / 3.0f), k.b(context, 45.0f));
                layoutParams.addRule(11);
                layoutParams.addRule(12);
                layoutParams.bottomMargin = k.b(context, 10.0f);
                layoutParams.rightMargin = k.m(context) && i == 0 ? k.l(context) + k.b(context, 8.0f) : k.b(context, 8.0f);
            } else {
                layoutParams = new LayoutParams(-1, k.b(context, 45.0f));
                layoutParams.addRule(12);
            }
            baseView.getmAnimationContent().setLayoutParams(layoutParams);
        }
    }

    private float a(boolean z) {
        float f = 0.0f;
        try {
            Context context = (Context) this.f2849a.get();
            if (context != null) {
                float j = (float) k.j(context);
                f = z ? j + ((float) k.l(context)) : j;
            }
            return f;
        } catch (Throwable th) {
            th.printStackTrace();
            return 0.0f;
        }
    }

    public final void a(final boolean z, boolean z2, final BaseView baseView) {
        int parseColor = Color.parseColor("#ff264870");
        baseView.getmAnimationPlayer().setBackgroundColor(parseColor);
        Context context = (Context) this.f2849a.get();
        if (z) {
            if (baseView.style$161ffeb8 == a.f2846a && context != null) {
                baseView.getmAnimationContent().setBackgroundResource(p.a(context, "mintegral_nativex_fullview_background", "drawable"));
                baseView.getmAnimationPlayer().setBackgroundColor(parseColor);
            }
            if (z2) {
                baseView.getmAnimationPlayer().getBackground().setAlpha(80);
            } else {
                baseView.getmAnimationPlayer().setBackgroundColor(Color.parseColor("#ff4c8fdf"));
                baseView.getmAnimationPlayer().getBackground().setAlpha(Callback.DEFAULT_DRAG_ANIMATION_DURATION);
            }
        } else if (baseView.style$161ffeb8 != a.b) {
            if (context != null) {
                baseView.getmAnimationContent().setBackgroundResource(p.a(context, "mintegral_nativex_cta_por_pre", "drawable"));
                baseView.getmAnimationPlayer().setBackgroundResource(p.a(context, "mintegral_nativex_cta_por_pre", "drawable"));
                return;
            }
        }
        if (z2) {
            new Handler().postDelayed(new Runnable() {
                public final void run() {
                    baseView.getmAnimationPlayer().setBackgroundColor(Color.parseColor("#ff4c8fdf"));
                    baseView.getmAnimationPlayer().getBackground().setAlpha(z ? Callback.DEFAULT_DRAG_ANIMATION_DURATION : 255);
                    b.a((View) baseView.getmAnimationPlayer());
                }
            }, 1000);
        }
    }

    public static void a(BaseView baseView) {
        baseView.getmAnimationPlayer().clearAnimation();
    }

    public static void a(View view, BaseView baseView) {
        if (view != null && baseView != null) {
            view.setLayoutParams(new LayoutParams(-1, -1));
            baseView.i.addView(view);
        }
    }

    public static void a(BaseView baseView, boolean z) {
        if (baseView != null && VERSION.SDK_INT >= 11) {
            baseView.setSystemUiVisibility(z ? 0 : InputDeviceCompat.SOURCE_TOUCHSCREEN);
        }
    }

    static /* synthetic */ Bitmap a(Bitmap bitmap) {
        Bitmap createBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Config.ARGB_8888);
        Canvas canvas = new Canvas(createBitmap);
        Paint paint = new Paint();
        Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        RectF rectF = new RectF(rect);
        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(-12434878);
        bitmap.getWidth();
        canvas.drawRoundRect(rectF, 25.0f, 25.0f, paint);
        paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);
        return createBitmap;
    }

    static /* synthetic */ void a(View view) {
        if (view != null) {
            AlphaAnimation alphaAnimation = new AlphaAnimation(0.2f, 1.0f);
            alphaAnimation.setDuration(800);
            alphaAnimation.setInterpolator(new LinearInterpolator());
            alphaAnimation.setRepeatCount(2);
            alphaAnimation.setRepeatMode(1);
            view.startAnimation(alphaAnimation);
        }
    }
}
