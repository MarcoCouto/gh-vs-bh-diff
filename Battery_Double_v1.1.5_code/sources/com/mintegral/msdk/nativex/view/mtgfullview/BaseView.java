package com.mintegral.msdk.nativex.view.mtgfullview;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import com.mintegral.msdk.base.utils.p;

public class BaseView extends RelativeLayout {

    /* renamed from: a reason: collision with root package name */
    protected RelativeLayout f2845a;
    protected RelativeLayout b;
    protected RelativeLayout c;
    protected ImageView d;
    protected TextView e;
    protected ProgressBar f;
    protected FrameLayout g;
    protected LinearLayout h;
    protected RelativeLayout i;
    public int style$161ffeb8;

    public enum a {
        ;

        static {
            c = new int[]{f2846a, b};
        }

        public static int[] a() {
            return (int[]) c.clone();
        }
    }

    public FrameLayout getmAnimationContent() {
        return this.g;
    }

    public LinearLayout getmAnimationPlayer() {
        return this.h;
    }

    public BaseView(Context context) {
        super(context);
        View inflate = LayoutInflater.from(getContext()).inflate(p.a(getContext(), "mintegral_nativex_fullbasescreen", TtmlNode.TAG_LAYOUT), this);
        this.i = (RelativeLayout) inflate;
        if (inflate != null) {
            this.f2845a = (RelativeLayout) inflate.findViewById(p.a(getContext(), "mintegral_full_rl_playcontainer", "id"));
            this.b = (RelativeLayout) inflate.findViewById(p.a(getContext(), "mintegral_full_player_parent", "id"));
            this.c = (RelativeLayout) inflate.findViewById(p.a(getContext(), "mintegral_full_rl_close", "id"));
            this.d = (ImageView) inflate.findViewById(p.a(getContext(), "mintegral_full_iv_close", "id"));
            this.e = (TextView) inflate.findViewById(p.a(getContext(), "mintegral_full_tv_install", "id"));
            this.f = (ProgressBar) inflate.findViewById(p.a(getContext(), "mintegral_full_pb_loading", "id"));
            this.g = (FrameLayout) inflate.findViewById(p.a(getContext(), "mintegral_full_animation_content", "id"));
            this.h = (LinearLayout) inflate.findViewById(p.a(getContext(), "mintegral_full_animation_player", "id"));
            inflate.setLayoutParams(new LayoutParams(-1, -1));
        }
    }

    public RelativeLayout getMintegralFullPlayContainer() {
        return this.f2845a;
    }

    public RelativeLayout getMintegralFullPlayerParent() {
        return this.b;
    }

    public RelativeLayout getMintegralFullClose() {
        return this.c;
    }

    public ImageView getMintegralFullIvClose() {
        return this.d;
    }

    public TextView getMintegralFullTvInstall() {
        return this.e;
    }

    public ProgressBar getMintegralFullPb() {
        return this.f;
    }

    public int getStytle$21377bb9() {
        return this.style$161ffeb8;
    }

    public void setStytle$64568c2d(int i2) {
        this.style$161ffeb8 = i2;
    }
}
