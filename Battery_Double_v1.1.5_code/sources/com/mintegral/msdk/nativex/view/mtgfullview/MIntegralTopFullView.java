package com.mintegral.msdk.nativex.view.mtgfullview;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import com.mintegral.msdk.base.utils.p;

public class MIntegralTopFullView extends BaseView {
    public static final String INTERFACE_RESULT;
    protected ImageView j;
    protected TextView k;
    protected TextView l;
    protected StarLevelLayoutView m;

    static {
        StringBuilder sb = new StringBuilder();
        sb.append(MIntegralTopFullView.class.getName());
        sb.append("WithResault");
        INTERFACE_RESULT = sb.toString();
    }

    public ImageView getMintegralFullViewDisplayIcon() {
        return this.j;
    }

    public TextView getMintegralFullViewDisplayTitle() {
        return this.k;
    }

    public TextView getMintegralFullViewDisplayDscription() {
        return this.l;
    }

    public StarLevelLayoutView getStarLevelLayoutView() {
        return this.m;
    }

    public MIntegralTopFullView(Context context) {
        super(context);
        View inflate = LayoutInflater.from(getContext()).inflate(p.a(getContext(), "mintegral_nativex_fullscreen_top", TtmlNode.TAG_LAYOUT), this.i);
        if (inflate != null) {
            this.j = (ImageView) inflate.findViewById(p.a(getContext(), "mintegral_full_tv_display_icon", "id"));
            this.k = (TextView) inflate.findViewById(p.a(getContext(), "mintegral_full_tv_display_title", "id"));
            this.l = (TextView) inflate.findViewById(p.a(getContext(), "mintegral_full_tv_display_description", "id"));
            this.m = (StarLevelLayoutView) inflate.findViewById(p.a(getContext(), "mintegral_full_tv_feeds_star", "id"));
            this.l.setTextColor(-7829368);
            inflate.setLayoutParams(new LayoutParams(-1, -1));
            updateLayoutParams();
        }
    }

    public void updateLayoutParams() {
        LayoutParams layoutParams = new LayoutParams(-1, -1);
        layoutParams.addRule(10);
        this.f2845a.setLayoutParams(layoutParams);
        LayoutParams layoutParams2 = new LayoutParams(-1, -2);
        layoutParams2.addRule(10);
        this.b.setLayoutParams(layoutParams2);
    }
}
