package com.mintegral.msdk.nativex.view;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.View.MeasureSpec;
import com.mintegral.msdk.nativex.adapter.RollingAdapter;
import com.mintegral.msdk.nativex.listener.RollingPagerListenrt;
import com.mintegral.msdk.nativex.view.MTGNativeRollView.a;
import com.mintegral.msdk.out.Frame;
import com.mintegral.msdk.out.NativeListener.FilpListener;
import java.util.List;

public class RollingBCView extends ViewPager {

    /* renamed from: a reason: collision with root package name */
    private boolean f2844a = true;
    private RollingPagerListenrt b = new RollingPagerListenrt();
    private FilpListener c;

    public void setFilpListening(FilpListener filpListener) {
        this.c = filpListener;
    }

    public RollingBCView(Context context) {
        super(context);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        if (MeasureSpec.getMode(i2) == Integer.MIN_VALUE) {
            int i3 = 0;
            for (int i4 = 0; i4 < getChildCount(); i4++) {
                View childAt = getChildAt(i4);
                childAt.measure(i, MeasureSpec.makeMeasureSpec(0, 0));
                int measuredHeight = childAt.getMeasuredHeight();
                if (measuredHeight > i3) {
                    i3 = measuredHeight;
                }
                if (measuredHeight > 10 && this.f2844a) {
                    this.b.a(0);
                    this.f2844a = false;
                }
            }
            i2 = MeasureSpec.makeMeasureSpec(i3, 1073741824);
        }
        super.onMeasure(i, i2);
    }

    /* access modifiers changed from: protected */
    public void onWindowVisibilityChanged(int i) {
        super.onWindowVisibilityChanged(i);
    }

    public void setData(List<Frame> list, Context context, String str, a aVar) {
        if (list == null || list.size() == 0) {
            throw new NegativeArraySizeException("ad date is null or size is 0");
        }
        RollingAdapter rollingAdapter = new RollingAdapter(list);
        if (aVar != null) {
            rollingAdapter.a(aVar);
        }
        setAdapter(rollingAdapter);
        this.b.a(list, context, str);
        if (this.c != null) {
            this.b.a(this.c);
        }
        setOnPageChangeListener(this.b);
        if (this.f2844a) {
            this.b.a(0);
            this.f2844a = false;
        }
    }
}
