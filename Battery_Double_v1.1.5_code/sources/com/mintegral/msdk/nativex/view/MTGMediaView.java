package com.mintegral.msdk.nativex.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Build.VERSION;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.ViewCompat;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Base64;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.webkit.URLUtil;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.github.mikephil.charting.utils.Utils;
import com.integralads.avid.library.inmobi.utils.AvidJSONUtil;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.base.b.h;
import com.mintegral.msdk.base.b.i;
import com.mintegral.msdk.base.b.w;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.entity.p;
import com.mintegral.msdk.base.utils.j;
import com.mintegral.msdk.base.utils.k;
import com.mintegral.msdk.base.utils.s;
import com.mintegral.msdk.base.webview.BrowserView.MTGDownloadListener;
import com.mintegral.msdk.nativex.view.mtgfullview.BaseView;
import com.mintegral.msdk.nativex.view.mtgfullview.MIntegralTopFullView;
import com.mintegral.msdk.out.Campaign;
import com.mintegral.msdk.out.NativeListener.NativeTrackingListener;
import com.mintegral.msdk.out.OnMTGMediaViewListener;
import com.mintegral.msdk.video.js.bridge.IRewardBridge;
import com.mintegral.msdk.videocommon.view.MyImageView;
import java.io.File;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Timer;
import java.util.TimerTask;
import org.json.JSONArray;
import org.json.JSONObject;

public class MTGMediaView extends LinearLayout implements com.mintegral.msdk.playercommon.c, IRewardBridge {
    public static final int OPEN_FULLSCREEN_ON_VIDEO_TYPE_1_LANDING_PAGE = 1;
    public static final int OPEN_FULLSCREEN_ON_VIDEO_TYPE_2_NORMAL_FULLSCREEN = 2;
    public static final int OPEN_FULLSCREEN_ON_VIDEO_TYPE_3_NORMAL_FULLSCREEN_ENDCARD = 3;
    public static final int OPEN_FULLSCREEN_ON_VIDEO_TYPE_4_NORMAL_FULLSCREEN_LP = 4;
    public static final int OPEN_FULLSCREEN_ON_VIDEO_TYPE_6_SMALLVIDEO = 6;
    public static final String TAG = "MTGMediaView";
    private static int n = 2;
    private static int o = 1;
    /* access modifiers changed from: private */
    public View A;
    /* access modifiers changed from: private */
    public BaseView B;
    private RelativeLayout C;
    private RelativeLayout D;
    private ImageView E;
    /* access modifiers changed from: private */
    public TextView F;
    private ProgressBar G;
    private RelativeLayout H;
    private int I;
    /* access modifiers changed from: private */
    public Handler J;
    /* access modifiers changed from: private */
    public CampaignEx K;
    private Timer L;
    private int M;
    private int N;
    private double O;
    private double P;
    /* access modifiers changed from: private */
    public int Q;
    /* access modifiers changed from: private */
    public int R;
    private e S;
    private SensorManager T;
    private Sensor U;
    private com.mintegral.msdk.videocommon.download.a V = null;
    /* access modifiers changed from: private */
    public a W = null;
    private OnMTGMediaViewListener Z;

    /* renamed from: a reason: collision with root package name */
    private boolean f2805a = true;
    private int aa;
    /* access modifiers changed from: private */
    public boolean ab = false;
    private boolean ac = false;
    private RelativeLayout ad;
    private ImageView ae;
    private boolean b = true;
    private boolean c = true;
    /* access modifiers changed from: private */
    public boolean d = true;
    private boolean e = true;
    private boolean f = false;
    private boolean g = true;
    /* access modifiers changed from: private */
    public boolean h = false;
    /* access modifiers changed from: private */
    public boolean i = false;
    /* access modifiers changed from: private */
    public boolean j = false;
    private boolean k = true;
    private boolean l = true;
    private ArrayList<String> m = new ArrayList<>();
    private boolean p = false;
    /* access modifiers changed from: private */
    public boolean q = false;
    /* access modifiers changed from: private */
    public MediaViewPlayerView r;
    private RelativeLayout s;
    private RelativeLayout t;
    private RelativeLayout u;
    private WindVaneWebViewForNV v;
    /* access modifiers changed from: private */
    public WindVaneWebViewForNV w;
    private RelativeLayout x;
    /* access modifiers changed from: private */
    public MyImageView y;
    private ProgressBar z;

    /* renamed from: com.mintegral.msdk.nativex.view.MTGMediaView$8 reason: invalid class name */
    static /* synthetic */ class AnonymousClass8 {

        /* renamed from: a reason: collision with root package name */
        static final /* synthetic */ int[] f2822a = new int[com.mintegral.msdk.nativex.view.mtgfullview.BaseView.a.a().length];

        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0011 */
        static {
            f2822a[com.mintegral.msdk.nativex.view.mtgfullview.BaseView.a.b - 1] = 1;
            try {
                f2822a[com.mintegral.msdk.nativex.view.mtgfullview.BaseView.a.f2846a - 1] = 2;
            } catch (NoSuchFieldError unused) {
            }
        }
    }

    private enum a {
        BIG_IMAGE,
        VIDEO,
        FB,
        GIF
    }

    private static final class b implements com.mintegral.msdk.nativex.listener.a {

        /* renamed from: a reason: collision with root package name */
        WeakReference<MTGMediaView> f2825a;

        public b(MTGMediaView mTGMediaView) {
            this.f2825a = new WeakReference<>(mTGMediaView);
        }

        public final void a() {
            MTGMediaView mTGMediaView = (MTGMediaView) this.f2825a.get();
            if (mTGMediaView != null) {
                MTGMediaView.M(mTGMediaView);
            }
        }

        public final void b() {
            MTGMediaView mTGMediaView = (MTGMediaView) this.f2825a.get();
            if (mTGMediaView != null) {
                MTGMediaView.N(mTGMediaView);
            }
        }

        public final void c() {
            MTGMediaView mTGMediaView = (MTGMediaView) this.f2825a.get();
            if (mTGMediaView != null) {
                MTGMediaView.O(mTGMediaView);
            }
        }

        public final void d() {
            MTGMediaView mTGMediaView = (MTGMediaView) this.f2825a.get();
            if (mTGMediaView != null) {
                MTGMediaView.P(mTGMediaView);
            }
        }

        public final void a(String str) {
            MTGMediaView mTGMediaView = (MTGMediaView) this.f2825a.get();
            if (mTGMediaView != null) {
                mTGMediaView.a(str);
            }
        }
    }

    private static final class c implements Runnable {

        /* renamed from: a reason: collision with root package name */
        WeakReference<MTGMediaView> f2826a;

        public c(MTGMediaView mTGMediaView) {
            this.f2826a = new WeakReference<>(mTGMediaView);
        }

        public final void run() {
            MTGMediaView mTGMediaView = (MTGMediaView) this.f2826a.get();
            if (mTGMediaView != null) {
                try {
                    if (mTGMediaView.W != null && mTGMediaView.W == a.BIG_IMAGE) {
                        mTGMediaView.c();
                        mTGMediaView.W = a.VIDEO;
                        mTGMediaView.changeNoticeURL();
                    }
                } catch (Throwable th) {
                    com.mintegral.msdk.base.utils.g.c(MTGMediaView.TAG, th.getMessage(), th);
                }
            }
        }
    }

    private static final class d implements com.mintegral.msdk.mtgjscommon.base.a {
        private d() {
        }

        /* synthetic */ d(byte b) {
            this();
        }

        public final boolean a(String str) {
            try {
                if (!TextUtils.isEmpty(str)) {
                    if (com.mintegral.msdk.base.utils.j.a.a(str)) {
                        j.a(com.mintegral.msdk.base.controller.a.d().h(), str);
                        return true;
                    } else if (URLUtil.isNetworkUrl(str)) {
                        return false;
                    } else {
                        j.b(com.mintegral.msdk.base.controller.a.d().h(), str);
                        return true;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return false;
        }
    }

    private class e implements SensorEventListener {
        public final void onAccuracyChanged(Sensor sensor, int i) {
        }

        private e() {
        }

        /* synthetic */ e(MTGMediaView mTGMediaView, byte b) {
            this();
        }

        public final void onSensorChanged(SensorEvent sensorEvent) {
            int i;
            try {
                float[] fArr = sensorEvent.values;
                float f = -fArr[0];
                float f2 = -fArr[1];
                float f3 = -fArr[2];
                if (((f * f) + (f2 * f2)) * 4.0f >= f3 * f3) {
                    i = 90 - Math.round(((float) Math.atan2((double) (-f2), (double) f)) * 57.29578f);
                    while (i >= 360) {
                        i -= 360;
                    }
                    while (i < 0) {
                        i += 360;
                    }
                } else {
                    i = -1;
                }
                float D = MTGMediaView.this.u();
                int k = k.k(MTGMediaView.this.getContext());
                if ((i <= 45 || i >= 135) && (i <= 225 || i >= 315)) {
                    if (((i > 135 && i < 225) || ((i > 315 && i < 360) || ((i >= 0 && i <= 45) || i == -1))) && D <= ((float) k)) {
                        MTGMediaView.this.J.postDelayed(new Runnable() {
                            public final void run() {
                                try {
                                    if (MTGMediaView.this.j) {
                                        com.mintegral.msdk.base.utils.g.d(MTGMediaView.TAG, "|||||||||||||||||");
                                        MTGMediaView.this.i = false;
                                        MTGMediaView.this.j = false;
                                        com.mintegral.msdk.nativex.view.mtgfullview.b.a(MTGMediaView.this.getContext());
                                        com.mintegral.msdk.nativex.view.mtgfullview.b.a(MTGMediaView.this.B, MTGMediaView.this.i);
                                        MTGMediaView.this.w();
                                        MTGMediaView.this.g();
                                        MTGMediaView.this.e();
                                        MTGMediaView.this.f();
                                        if (MTGMediaView.this.w != null) {
                                            com.mintegral.msdk.base.utils.g.a(MTGMediaView.TAG, "=====orientation|||||");
                                            MTGMediaView.this.w.orientation(MTGMediaView.this.i);
                                        }
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }, 200);
                    }
                } else if (D >= ((float) k)) {
                    MTGMediaView.this.J.postDelayed(new Runnable() {
                        public final void run() {
                            try {
                                if (!MTGMediaView.this.j) {
                                    com.mintegral.msdk.base.utils.g.d(MTGMediaView.TAG, "--------------");
                                    MTGMediaView.this.i = true;
                                    MTGMediaView.this.j = true;
                                    com.mintegral.msdk.nativex.view.mtgfullview.b.a(MTGMediaView.this.getContext());
                                    com.mintegral.msdk.nativex.view.mtgfullview.b.a(MTGMediaView.this.B, MTGMediaView.this.i);
                                    MTGMediaView.this.x();
                                    MTGMediaView.this.g();
                                    MTGMediaView.this.e();
                                    MTGMediaView.this.f();
                                    if (MTGMediaView.this.w != null) {
                                        com.mintegral.msdk.base.utils.g.a(MTGMediaView.TAG, "=====orientation----");
                                        MTGMediaView.this.w.orientation(MTGMediaView.this.i);
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }, 200);
                }
            } catch (Throwable th) {
                th.printStackTrace();
            }
        }
    }

    private static final class f implements com.mintegral.msdk.videocommon.listener.a {

        /* renamed from: a reason: collision with root package name */
        WeakReference<MTGMediaView> f2830a;

        public final void a(String str, String str2) {
        }

        public f(MTGMediaView mTGMediaView) {
            this.f2830a = new WeakReference<>(mTGMediaView);
        }

        public final void a(String str) {
            MTGMediaView mTGMediaView = (MTGMediaView) this.f2830a.get();
            if (mTGMediaView != null) {
                mTGMediaView.J.post(new c(mTGMediaView));
            }
        }
    }

    private static final class g extends WebViewClient {

        /* renamed from: a reason: collision with root package name */
        WeakReference<MTGMediaView> f2831a;

        public g(MTGMediaView mTGMediaView) {
            this.f2831a = new WeakReference<>(mTGMediaView);
        }

        public final void onPageFinished(WebView webView, String str) {
            super.onPageFinished(webView, str);
            try {
                MTGMediaView mTGMediaView = (MTGMediaView) this.f2831a.get();
                if (!(mTGMediaView == null || mTGMediaView.W == null || mTGMediaView.W != a.BIG_IMAGE)) {
                    mTGMediaView.d();
                    mTGMediaView.W = a.GIF;
                    mTGMediaView.changeNoticeURL();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void destory() {
    }

    public void handlerPlayableException(Object obj, String str) {
    }

    public void notifyCloseBtn(Object obj, String str) {
    }

    public void onPlayCompleted() {
    }

    public void onPlayProgressMS(int i2, int i3) {
    }

    public void onPlayStarted(int i2) {
    }

    public void setOrientation(Object obj, String str) {
    }

    public void getEndScreenInfo(Object obj, String str) {
        try {
            ArrayList arrayList = new ArrayList();
            arrayList.add(this.K);
            String a2 = a((List<CampaignEx>) arrayList, k(), "MAL_9.13.51,3.0.1");
            String encodeToString = !TextUtils.isEmpty(a2) ? Base64.encodeToString(a2.getBytes(), 2) : "";
            String str2 = TAG;
            StringBuilder sb = new StringBuilder("====getEndScreenInfo-mCampaign.name:");
            sb.append(this.K.getAppName());
            com.mintegral.msdk.base.utils.g.d(str2, sb.toString());
            com.mintegral.msdk.mtgjscommon.windvane.g.a();
            com.mintegral.msdk.mtgjscommon.windvane.g.a(obj, encodeToString);
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public void install(Object obj, String str) {
        j();
    }

    public void toggleCloseBtn(Object obj, String str) {
        try {
            if (!TextUtils.isEmpty(str)) {
                try {
                    new JSONObject(str).optInt("state", 1);
                } catch (Exception unused) {
                }
            }
            com.mintegral.msdk.base.utils.g.d(TAG, "SHOW CLOSE BTN ");
            showEndCardWebViewCloseBtn();
            com.mintegral.msdk.mtgjscommon.windvane.g.a();
            com.mintegral.msdk.mtgjscommon.windvane.g.a(obj, a(0));
        } catch (Exception e2) {
            e2.printStackTrace();
            com.mintegral.msdk.mtgjscommon.windvane.g.a();
            com.mintegral.msdk.mtgjscommon.windvane.g.b(obj, a(1));
        }
    }

    public void triggerCloseBtn(Object obj, String str) {
        try {
            exitFullScreen();
            com.mintegral.msdk.mtgjscommon.windvane.g.a();
            com.mintegral.msdk.mtgjscommon.windvane.g.a(obj, a(0));
        } catch (Exception e2) {
            e2.printStackTrace();
            com.mintegral.msdk.mtgjscommon.windvane.g.a();
            com.mintegral.msdk.mtgjscommon.windvane.g.b(obj, a(1));
        }
    }

    private static String a(int i2) {
        String str = "";
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("code", i2);
            String jSONObject2 = jSONObject.toString();
            if (!TextUtils.isEmpty(jSONObject2)) {
                return Base64.encodeToString(jSONObject2.getBytes(), 2);
            }
            return str;
        } catch (Throwable unused) {
            com.mintegral.msdk.base.utils.g.d(TAG, "code to string is error");
            return str;
        }
    }

    private static String a(List<CampaignEx> list, String str, String str2) {
        try {
            if (list.size() <= 0) {
                return null;
            }
            JSONArray parseCamplistToJson = CampaignEx.parseCamplistToJson(list);
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("campaignList", parseCamplistToJson);
            jSONObject.put(MIntegralConstans.PROPERTIES_UNIT_ID, str);
            jSONObject.put("sdk_info", str2);
            return jSONObject.toString();
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public MTGMediaView(Context context) {
        super(context);
        a();
    }

    public MTGMediaView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a();
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        String str = TAG;
        try {
            StringBuilder sb = new StringBuilder("onAttachedToWindow start:");
            sb.append(this.K == null ? "" : this.K.getAppName());
            com.mintegral.msdk.base.utils.g.b(str, sb.toString());
            if (this.c) {
                this.T = (SensorManager) getContext().getSystemService("sensor");
                this.U = this.T.getDefaultSensor(1);
                this.S = new e(this, 0);
                this.T.registerListener(this.S, this.U, 2);
                com.mintegral.msdk.base.utils.g.b(TAG, "register sensorlistener");
            }
        } catch (Throwable th) {
            th.printStackTrace();
            return;
        }
        if (VERSION.SDK_INT >= 11) {
            this.g = isHardwareAccelerated();
        }
        b();
        try {
            n();
            this.L = new Timer();
            this.L.schedule(new TimerTask() {
                public final void run() {
                    MTGMediaView.this.J.post(new Runnable() {
                        public final void run() {
                            try {
                                if (MTGMediaView.this.A != null && MTGMediaView.this.A.getParent() == null) {
                                    MTGMediaView.this.h = false;
                                    if (MTGMediaView.this.r != null && MTGMediaView.this.r.curIsFullScreen()) {
                                        com.mintegral.msdk.base.utils.g.b(MTGMediaView.TAG, "handler mPlayerView.setExitFullScreen();");
                                        MTGMediaView.this.r.setExitFullScreen();
                                    }
                                }
                                Message obtain = Message.obtain();
                                if (MTGMediaView.this.h) {
                                    obtain.what = 2;
                                } else {
                                    obtain.what = 1;
                                }
                                MTGMediaView.this.J.sendMessage(obtain);
                            } catch (Throwable th) {
                                th.printStackTrace();
                            }
                        }
                    });
                }
            }, 0, 300);
        } catch (Throwable th2) {
            th2.printStackTrace();
        }
        String str2 = TAG;
        StringBuilder sb2 = new StringBuilder("onAttachedToWindow setDisplay finalmCurDisplayMode:");
        sb2.append(this.W);
        com.mintegral.msdk.base.utils.g.b(str2, sb2.toString());
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        String str = TAG;
        try {
            StringBuilder sb = new StringBuilder("onDetachedFromWindow appname:");
            sb.append(this.K);
            com.mintegral.msdk.base.utils.g.b(str, sb.toString() == null ? "" : this.K.getAppName());
            n();
            if (!(this.T == null || this.S == null)) {
                this.T.unregisterListener(this.S);
                com.mintegral.msdk.base.utils.g.b(TAG, "unRegister sensorlistener");
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public void onWindowFocusChanged(boolean z2) {
        super.onWindowFocusChanged(z2);
        String str = TAG;
        try {
            StringBuilder sb = new StringBuilder("hasWindowFocus:");
            sb.append(z2);
            com.mintegral.msdk.base.utils.g.d(str, sb.toString());
            this.e = z2;
            if (this.W == a.VIDEO && this.r != null) {
                this.r.setIsFrontDesk(z2);
            }
            if (!this.h) {
                com.mintegral.msdk.base.utils.g.b(TAG, "initFullPlayeron WindowFocuse 在半屏 return");
            } else if (this.r == null) {
                com.mintegral.msdk.base.utils.g.d(TAG, "fullscreen playerview is null return");
            } else if (!this.e) {
                com.mintegral.msdk.base.utils.g.b(TAG, "fullscreen windowfocuse false pasue======");
                this.r.pause();
            } else if (this.r.isPlaying()) {
                com.mintegral.msdk.base.utils.g.b(TAG, "fullscreen windowfocuse true isPlaying do nothing return");
            } else if (this.r != null && !this.r.isComplete() && !this.r.getIsActiviePause()) {
                com.mintegral.msdk.base.utils.g.d(TAG, "fullscreen windowfocuse true startOrPlayVideo");
                this.r.onClickPlayButton();
            }
        } catch (Throwable th) {
            th.printStackTrace();
            return;
        }
        requestLayout();
        com.mintegral.msdk.base.utils.g.b(TAG, "onWindowFocusChanged reqeusetlaytout");
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        super.onMeasure(i2, i3);
        try {
            this.M = getHeight();
            this.N = getWidth();
            if (this.N == 0) {
                this.N = getMeasuredWidth();
                String str = TAG;
                StringBuilder sb = new StringBuilder("onMeasure 宽度为0 调用getMeasuredWidth mDevWidth:");
                sb.append(this.N);
                com.mintegral.msdk.base.utils.g.c(str, sb.toString());
            }
            if (this.M == 0) {
                this.M = getMeasuredHeight();
                String str2 = TAG;
                StringBuilder sb2 = new StringBuilder("onMeasure 高度为0 调用getMeasuredHeight mDevWidth:");
                sb2.append(this.M);
                com.mintegral.msdk.base.utils.g.c(str2, sb2.toString());
            }
            String str3 = TAG;
            StringBuilder sb3 = new StringBuilder("onMeasure pre mDevWidth ");
            sb3.append(this.N);
            sb3.append(" mDevHeight:");
            sb3.append(this.M);
            sb3.append(" mCurDisplayMode:");
            sb3.append(this.W);
            sb3.append(" mCurIsLandScape:");
            sb3.append(this.i);
            com.mintegral.msdk.base.utils.g.b(str3, sb3.toString());
            if (this.N == 0 && this.M == 0) {
                this.N = (int) u();
                String str4 = TAG;
                StringBuilder sb4 = new StringBuilder("onMeasure 宽度和高度都为0 宽度取屏幕宽度mDevWidth:");
                sb4.append(this.N);
                com.mintegral.msdk.base.utils.g.d(str4, sb4.toString());
            }
            if (this.W == a.VIDEO && !this.h) {
                LayoutParams layoutParams = getLayoutParams();
                if (this.M == 0 || ((layoutParams != null && layoutParams.height == -2) || (layoutParams != null && layoutParams.height == -1))) {
                    double d2 = (double) this.N;
                    double d3 = this.P;
                    Double.isNaN(d2);
                    this.M = (int) ((d2 * d3) / this.O);
                    String str5 = TAG;
                    StringBuilder sb5 = new StringBuilder("onMeasure mDevHeight为0并且设置WRAP_CONTENT 拿视频宽高算高度mDevHeight:");
                    sb5.append(this.M);
                    com.mintegral.msdk.base.utils.g.b(str5, sb5.toString());
                }
                String str6 = TAG;
                StringBuilder sb6 = new StringBuilder("onMeasure after mDevWidth ");
                sb6.append(this.N);
                sb6.append(" * mDevHeight *****");
                sb6.append(this.M);
                com.mintegral.msdk.base.utils.g.b(str6, sb6.toString());
                g();
            } else if (this.W != a.BIG_IMAGE || this.h) {
                if (this.W == a.GIF && !this.h) {
                    f();
                }
            } else {
                e();
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public void setNativeAd(Campaign campaign) {
        if (campaign == null) {
            try {
                com.mintegral.msdk.base.utils.g.d(TAG, "setNativeAd campaign is null return");
            } catch (Throwable th) {
                th.printStackTrace();
            }
        } else if (this.K == null || this.K != campaign) {
            this.e = true;
            this.g = true;
            this.h = false;
            this.i = false;
            this.j = false;
            this.q = false;
            t();
            this.K = (CampaignEx) campaign;
            if (this.K.getMediaViewHolder() == null) {
                com.mintegral.msdk.base.entity.CampaignEx.b bVar = new com.mintegral.msdk.base.entity.CampaignEx.b();
                bVar.l = this.K.getAdvImpList();
                this.K.setMediaViewHolder(bVar);
                String str = TAG;
                StringBuilder sb = new StringBuilder("setNativeAd mediaViewHolder appname:");
                sb.append(this.K.getAppName());
                com.mintegral.msdk.base.utils.g.b(str, sb.toString());
            }
            StringBuilder sb2 = new StringBuilder();
            sb2.append(this.K.getId());
            sb2.append(this.K.getVideoUrlEncode());
            sb2.append(this.K.getBidToken());
            this.V = com.mintegral.msdk.videocommon.download.c.getInstance().a(k(), sb2.toString());
            String str2 = TAG;
            StringBuilder sb3 = new StringBuilder("setNativeAd cid");
            sb3.append(this.K.getId());
            sb3.append(" appname:");
            sb3.append(this.K.getAppName());
            com.mintegral.msdk.base.utils.g.d(str2, sb3.toString());
            if (this.p) {
                String str3 = TAG;
                StringBuilder sb4 = new StringBuilder("setNativeAd setDisplay appname:");
                sb4.append(this.K.getAppName());
                com.mintegral.msdk.base.utils.g.b(str3, sb4.toString());
                b();
            }
        } else {
            com.mintegral.msdk.base.utils.g.b(TAG, "setNativeAd has init return");
        }
    }

    public void setOnMediaViewListener(OnMTGMediaViewListener onMTGMediaViewListener) {
        this.Z = onMTGMediaViewListener;
    }

    public void setIsAllowFullScreen(boolean z2) {
        this.d = z2;
    }

    public void setAllowVideoRefresh(boolean z2) {
        this.f2805a = z2;
    }

    public void setAllowLoopPlay(boolean z2) {
        this.b = z2;
    }

    public void setAllowScreenChange(boolean z2) {
        this.c = z2;
    }

    private void b() {
        try {
            this.W = a(true);
            String str = TAG;
            StringBuilder sb = new StringBuilder("setDisplay mCurDisplayMode:");
            sb.append(this.W);
            com.mintegral.msdk.base.utils.g.d(str, sb.toString());
            changeNoticeURL();
            if (this.W == a.FB) {
                this.x.setVisibility(0);
                this.s.setVisibility(8);
                this.y.setVisibility(8);
                if (this.K != null) {
                    this.K.getNativead();
                }
            } else if (this.W == a.BIG_IMAGE) {
                try {
                    k.a((ImageView) this.y);
                    this.y.setVisibility(0);
                    this.x.setVisibility(8);
                    this.s.setVisibility(8);
                    this.u.setVisibility(8);
                } catch (Throwable th) {
                    th.printStackTrace();
                }
                try {
                    if (this.K != null) {
                        String imageUrl = this.K.getImageUrl();
                        if (!s.a(imageUrl)) {
                            if (getContext() != null) {
                                com.mintegral.msdk.base.utils.g.b(TAG, "fillBigimage startOrPlayVideo");
                                com.mintegral.msdk.base.common.c.b.a(getContext()).a(imageUrl, (com.mintegral.msdk.base.common.c.c) new com.mintegral.msdk.base.common.c.c() {
                                    public final void onFailedLoad(String str, String str2) {
                                        com.mintegral.msdk.base.utils.g.c(MTGMediaView.TAG, "load image fail in mtgmediaview");
                                    }

                                    public final void onSuccessLoad(Bitmap bitmap, String str) {
                                        String str2 = MTGMediaView.TAG;
                                        StringBuilder sb = new StringBuilder("fillBigimage onSuccessLoad mCurDisplayMode:");
                                        sb.append(MTGMediaView.this.W);
                                        com.mintegral.msdk.base.utils.g.b(str2, sb.toString());
                                        if (MTGMediaView.this.y != null && MTGMediaView.this.W == a.BIG_IMAGE) {
                                            if (bitmap != null) {
                                                com.mintegral.msdk.base.utils.g.b(MTGMediaView.TAG, "setimgeBitmap=======");
                                                MTGMediaView.this.Q = bitmap.getWidth();
                                                MTGMediaView.this.R = bitmap.getHeight();
                                                MTGMediaView.this.y.setImageUrl(str);
                                                MTGMediaView.this.y.setImageBitmap(bitmap);
                                            }
                                            MTGMediaView.this.y.setOnClickListener(new com.mintegral.msdk.widget.a() {
                                                /* access modifiers changed from: protected */
                                                public final void a() {
                                                    MTGMediaView.this.j();
                                                }
                                            });
                                        }
                                    }
                                });
                            }
                        }
                    }
                } catch (Throwable th2) {
                    th2.printStackTrace();
                }
            } else if (this.W == a.VIDEO) {
                c();
            } else if (this.W == a.GIF) {
                i();
                d();
            }
        } catch (Throwable th3) {
            com.mintegral.msdk.base.utils.g.c(TAG, th3.getMessage(), th3);
            return;
        }
        this.p = true;
    }

    /* access modifiers changed from: private */
    public void e() {
        try {
            if (!(this.W != a.BIG_IMAGE || this.N == 0 || this.R == 0 || this.Q == 0)) {
                int i2 = (this.N * this.R) / this.Q;
                if (!(this.y == null || i2 == 0)) {
                    RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) this.y.getLayoutParams();
                    layoutParams.width = this.N;
                    layoutParams.height = i2;
                    this.y.setLayoutParams(layoutParams);
                    String str = TAG;
                    StringBuilder sb = new StringBuilder("initBitImageViewWHByDevWh onMeasure mdevWidth:");
                    sb.append(this.N);
                    sb.append(" mDevHeight:");
                    sb.append(this.M);
                    sb.append(" finalHeigt:");
                    sb.append(i2);
                    com.mintegral.msdk.base.utils.g.d(str, sb.toString());
                }
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    public void f() {
        try {
            if (this.W == a.GIF) {
                if (this.N != 0 && this.R != 0 && this.Q != 0) {
                    int i2 = (this.N * this.R) / this.Q;
                    if (!(this.u == null || i2 == 0)) {
                        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) this.u.getLayoutParams();
                        layoutParams.width = this.N;
                        layoutParams.height = i2;
                        layoutParams.addRule(13);
                        this.u.setLayoutParams(layoutParams);
                        String str = TAG;
                        StringBuilder sb = new StringBuilder("initGifImageViewWHByDevWh onMeasure mdevWidth:");
                        sb.append(this.N);
                        sb.append(" mDevHeight:");
                        sb.append(this.M);
                        sb.append(" finalHeigt:");
                        sb.append(i2);
                        sb.append(this.K.getAppName());
                        com.mintegral.msdk.base.utils.g.d(str, sb.toString());
                    }
                } else if (!(this.N == 0 || this.u == null)) {
                    RelativeLayout.LayoutParams layoutParams2 = (RelativeLayout.LayoutParams) this.u.getLayoutParams();
                    layoutParams2.width = this.N;
                    layoutParams2.height = (this.N * 627) / IronSourceConstants.RV_INSTANCE_LOAD_FAILED;
                    layoutParams2.addRule(13);
                    this.u.setLayoutParams(layoutParams2);
                    String str2 = TAG;
                    StringBuilder sb2 = new StringBuilder("initGifImageViewWHByDevWh onMeasure mdevWidth:");
                    sb2.append(this.N);
                    sb2.append(" mDevHeight:");
                    sb2.append(this.M);
                    sb2.append(this.K.getAppName());
                    com.mintegral.msdk.base.utils.g.d(str2, sb2.toString());
                }
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    public void g() {
        try {
            if (this.W == a.VIDEO && this.s != null) {
                int u2 = (int) u();
                int k2 = k.k(getContext());
                if (!this.h) {
                    RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) this.s.getLayoutParams();
                    layoutParams.width = this.N;
                    layoutParams.height = this.M;
                    layoutParams.addRule(13);
                    this.s.setLayoutParams(layoutParams);
                } else {
                    RelativeLayout.LayoutParams layoutParams2 = (RelativeLayout.LayoutParams) this.C.getLayoutParams();
                    layoutParams2.width = u2;
                    layoutParams2.height = k2;
                    layoutParams2.addRule(13);
                    this.C.setLayoutParams(layoutParams2);
                }
                if (!this.h) {
                    a((View) this.r, (float) this.N, (float) this.M);
                    return;
                }
                a((View) this.r, (float) u2, (float) k2);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public final void a(String str) {
        try {
            if (!TextUtils.isEmpty(str) && !this.m.contains(str)) {
                this.m.add(str);
                if (this.Z != null) {
                    this.Z.onVideoStart();
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    private void h() {
        try {
            this.s.setVisibility(0);
            this.y.setVisibility(8);
            this.x.setVisibility(8);
            this.u.setVisibility(8);
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    private void i() {
        try {
            if (this.K != null) {
                String gifUrl = this.K.getGifUrl();
                if (!s.a(gifUrl) && getContext() != null) {
                    com.mintegral.msdk.base.utils.g.b(TAG, "fillGifimage");
                    this.v.loadDataWithBaseURL(null, "<!DOCTYPE html><html lang=\"en\"><head>  <meta charset=\"UTF-8\">  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"><meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">  <title>Document</title>  <style>  *{    margin: 0;    padding: 0;  }  html, body{    width: 100%;    height: 100%;  }  body{    background-image: url('gifUrl');    background-position: center;    background-size: contain;    background-repeat: no-repeat;  }  </style></head><body></body></html>".replace("gifUrl", gifUrl), WebRequest.CONTENT_TYPE_HTML, "utf-8", null);
                    this.v.setInterceptTouch(true);
                    this.u.setOnClickListener(new com.mintegral.msdk.widget.a() {
                        /* access modifiers changed from: protected */
                        public final void a() {
                            MTGMediaView.this.j();
                            com.mintegral.msdk.base.utils.g.b(MTGMediaView.TAG, "CLICK WEBVIEW LAYOUT ");
                        }
                    });
                }
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    public String k() {
        try {
            if (this.K != null && s.b(this.K.getCampaignUnitId())) {
                return this.K.getCampaignUnitId();
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return null;
    }

    private a a(boolean z2) {
        a aVar = null;
        try {
            if (this.K != null) {
                String str = TAG;
                StringBuilder sb = new StringBuilder("initCurDisplayMode appname:");
                sb.append(this.K.getAppName());
                com.mintegral.msdk.base.utils.g.b(str, sb.toString());
                if (s.a(this.K.getVideoUrlEncode())) {
                    if (s.b(this.K.getImageUrl())) {
                        aVar = a.BIG_IMAGE;
                        com.mintegral.msdk.base.utils.g.b(TAG, "没有视频 只有大图 显示大图");
                    }
                    if (s.b(this.K.getGifUrl())) {
                        com.mintegral.msdk.base.utils.g.b(TAG, "没有视频 有gif图 显示gif图");
                        if (s.a(this.K.getImageUrl())) {
                            com.mintegral.msdk.base.utils.g.b(TAG, "没有视频 没有大图 有gif图 显示gif图");
                            aVar = a.GIF;
                        }
                        this.v.setWebViewClient(new g(this));
                        i();
                    }
                } else if (VERSION.SDK_INT < 14 || !this.g || this.V == null) {
                    String str2 = TAG;
                    StringBuilder sb2 = new StringBuilder("版本小于4.0或者没有开启硬件加速 显示大图 isHard:");
                    sb2.append(this.g);
                    sb2.append(" downloadtask:");
                    sb2.append(this.V != null);
                    com.mintegral.msdk.base.utils.g.d(str2, sb2.toString());
                    aVar = a.BIG_IMAGE;
                } else if (s.b(this.K.getVideoUrlEncode()) && s.a(this.K.getImageUrl())) {
                    aVar = a.VIDEO;
                    com.mintegral.msdk.base.utils.g.b(TAG, "只有视频 没有大图 显示视频");
                } else if (s.b(this.K.getVideoUrlEncode()) && s.b(this.K.getImageUrl())) {
                    com.mintegral.msdk.base.utils.g.b(TAG, "有视频 又有大图 进入判断逻辑");
                    int i2 = 100;
                    if (o() != null) {
                        i2 = o().d();
                    }
                    String str3 = TAG;
                    StringBuilder sb3 = new StringBuilder("readyRate:");
                    sb3.append(i2);
                    com.mintegral.msdk.base.utils.g.b(str3, sb3.toString());
                    if (com.mintegral.msdk.videocommon.download.k.a(this.V, i2)) {
                        aVar = a.VIDEO;
                        com.mintegral.msdk.base.utils.g.b(TAG, "满足readyrate 显示视频");
                    } else {
                        aVar = a.BIG_IMAGE;
                        com.mintegral.msdk.base.utils.g.b(TAG, "没有满足readyrate 暂时显示大图 判断是否监听下载");
                        if (z2) {
                            if (this.f2805a) {
                                com.mintegral.msdk.base.utils.g.b(TAG, "可以监听下载 下载满足readyrate之后 显示大图");
                                this.V.b((com.mintegral.msdk.videocommon.listener.a) new f(this));
                            } else {
                                com.mintegral.msdk.base.utils.g.b(TAG, "开发者禁止监听下载 一直显示大图");
                            }
                        }
                    }
                }
            }
            return aVar;
        } catch (Throwable th) {
            th.printStackTrace();
            com.mintegral.msdk.base.utils.g.b(TAG, "默认显示大图");
            return a.BIG_IMAGE;
        }
    }

    private void l() {
        try {
            if (this.K == null) {
                com.mintegral.msdk.base.utils.g.b(TAG, "campaign is null addPlayerView return");
            }
            com.mintegral.msdk.base.utils.g.b(TAG, "specSize addPlayerView");
            if (!(this.r == null || this.r.getParent() == null)) {
                ((ViewGroup) this.r.getParent()).removeView(this.r);
            }
            h();
            this.r = new MediaViewPlayerView(getContext());
            this.r.showProgressView(this.k);
            this.r.showSoundIndicator(this.l);
            if (this.f) {
                this.r.openSound();
            } else {
                this.r.closeSound();
            }
            this.r.setAllowLoopPlay(this.b);
            this.r.initPlayerViewData(m(), this.K, p(), this, this.V, k());
            this.r.setOnMediaViewPlayerViewListener(new b(this));
            this.s.addView(this.r, -1, -1);
            if (this.r == null) {
                com.mintegral.msdk.base.utils.g.b(TAG, "setPlayerViewListener playerview is null return");
            } else {
                this.r.setOnClickListener(new com.mintegral.msdk.widget.a() {
                    /* access modifiers changed from: protected */
                    public final void a() {
                        try {
                            if (!MTGMediaView.this.h) {
                                MTGMediaView.d(MTGMediaView.this);
                            }
                            MTGMediaView.this.r.showSoundIndicator(true);
                            MTGMediaView.this.r.showProgressView(true);
                            if (MTGMediaView.this.d && !MTGMediaView.this.h && (MTGMediaView.this.A == null || MTGMediaView.this.A.getParent() == null)) {
                                if (!MTGMediaView.this.r.halfLoadingViewisVisible()) {
                                    if (MTGMediaView.this.r.isPlaying()) {
                                        MTGMediaView.h(MTGMediaView.this);
                                        return;
                                    }
                                }
                                com.mintegral.msdk.base.utils.g.b(MTGMediaView.TAG, "is loading or no playing return;");
                            } else if (MTGMediaView.this.h) {
                                com.mintegral.msdk.base.utils.g.b(MTGMediaView.TAG, "fullScreenShowUI");
                                MTGMediaView.i(MTGMediaView.this);
                            } else {
                                MTGMediaView.this.j();
                                com.mintegral.msdk.base.utils.g.d(MTGMediaView.TAG, "不允许全屏 跳gp");
                            }
                        } catch (Throwable th) {
                            com.mintegral.msdk.base.utils.g.c(MTGMediaView.TAG, th.getMessage(), th);
                        }
                    }
                });
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    private String m() {
        try {
            if (this.K == null) {
                return null;
            }
            String str = TAG;
            StringBuilder sb = new StringBuilder("getPlayUrl curDisplay:");
            sb.append(this.W);
            com.mintegral.msdk.base.utils.g.b(str, sb.toString());
            if (this.V != null) {
                int h2 = this.V.h();
                String str2 = TAG;
                StringBuilder sb2 = new StringBuilder("downloadState:");
                sb2.append(h2);
                com.mintegral.msdk.base.utils.g.b(str2, sb2.toString());
                if (h2 == 5) {
                    String c2 = this.V.c();
                    if (new File(c2).exists() && this.V.d() == k.a(new File(c2))) {
                        String str3 = TAG;
                        StringBuilder sb3 = new StringBuilder("本地已下载完 拿本地播放地址：");
                        sb3.append(c2);
                        sb3.append(" state：");
                        sb3.append(h2);
                        com.mintegral.msdk.base.utils.g.b(str3, sb3.toString());
                        return c2;
                    }
                }
            }
            String videoUrlEncode = this.K.getVideoUrlEncode();
            if (s.b(videoUrlEncode)) {
                String str4 = TAG;
                StringBuilder sb4 = new StringBuilder("本地尚未下载完 拿网络地址：");
                sb4.append(videoUrlEncode);
                com.mintegral.msdk.base.utils.g.b(str4, sb4.toString());
                return videoUrlEncode;
            }
            return null;
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    private void n() {
        try {
            if (this.L != null) {
                this.L.cancel();
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    public boolean a(View view) {
        if (view != null) {
            try {
                if (view.getVisibility() == 0) {
                    Rect rect = new Rect();
                    if (!view.getLocalVisibleRect(rect)) {
                        return false;
                    }
                    long height = (long) (rect.height() * rect.width());
                    long height2 = (long) (view.getHeight() * view.getWidth());
                    long j2 = (long) (((float) height2) * 0.5f);
                    if (height2 > 0 && k.n(getContext()) && this.e && height >= j2 && isShown()) {
                        return true;
                    }
                    return false;
                }
            } catch (Throwable th) {
                th.printStackTrace();
            }
        }
        return false;
    }

    private com.mintegral.msdk.b.d o() {
        try {
            if (this.K != null) {
                if (!s.a(this.K.getCampaignUnitId())) {
                    String campaignUnitId = this.K.getCampaignUnitId();
                    String j2 = com.mintegral.msdk.base.controller.a.d().j();
                    if (!s.a(campaignUnitId)) {
                        if (!s.a(j2)) {
                            com.mintegral.msdk.b.b.a();
                            com.mintegral.msdk.b.d c2 = com.mintegral.msdk.b.b.c(j2, campaignUnitId);
                            if (c2 != null) {
                                return c2;
                            }
                            return com.mintegral.msdk.b.d.b(campaignUnitId);
                        }
                    }
                    return com.mintegral.msdk.b.d.b(campaignUnitId);
                }
            }
            return null;
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }

    private boolean p() {
        try {
            com.mintegral.msdk.b.d o2 = o();
            if (o2 == null) {
                return false;
            }
            int k2 = o2.k();
            String str = TAG;
            StringBuilder sb = new StringBuilder("========autoPlayType：");
            sb.append(k2);
            com.mintegral.msdk.base.utils.g.b(str, sb.toString());
            if (k2 == 1) {
                if (k.a(getContext())) {
                    com.mintegral.msdk.base.utils.g.b(TAG, "========wifi下自动播放");
                    return true;
                }
                com.mintegral.msdk.base.utils.g.b(TAG, "========wifi下自动播放 但目前不是wifi环境 现在为点击播放");
                return false;
            } else if (k2 == 2) {
                com.mintegral.msdk.base.utils.g.b(TAG, "========点击播放");
                return false;
            } else if (k2 == 3) {
                com.mintegral.msdk.base.utils.g.b(TAG, "========有网自动播放");
                if (k.b(getContext())) {
                    return true;
                }
                return false;
            } else if (k.a(getContext())) {
                com.mintegral.msdk.base.utils.g.b(TAG, "========else wifi下自动播放");
                return true;
            } else {
                com.mintegral.msdk.base.utils.g.b(TAG, "========else wifi下自动播放 但目前不是wifi环境 现在为点击播放");
                return false;
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public void onPlayError(String str) {
        try {
            com.mintegral.msdk.base.utils.g.b("error", str);
            if (!(this.K == null || this.K.getMediaViewHolder() == null || this.K.getMediaViewHolder().d || TextUtils.isEmpty(this.K.getCampaignUnitId()) || this.K.getNativeVideoTracking() == null || this.K.getNativeVideoTracking().m() == null)) {
                this.K.getMediaViewHolder().d = true;
                com.mintegral.msdk.click.a.a(getContext(), this.K, this.K.getCampaignUnitId(), this.K.getNativeVideoTracking().m(), false);
            }
            w a2 = w.a((h) i.a(getContext()));
            p pVar = null;
            if (!TextUtils.isEmpty(this.K.getNoticeUrl())) {
                int p2 = com.mintegral.msdk.base.utils.c.p(getContext());
                pVar = new p("2000021", p2, this.K.getNoticeUrl(), str, com.mintegral.msdk.base.utils.c.a(getContext(), p2));
            } else if (!TextUtils.isEmpty(this.K.getClickURL())) {
                int p3 = com.mintegral.msdk.base.utils.c.p(getContext());
                pVar = new p("2000021", p3, this.K.getClickURL(), str, com.mintegral.msdk.base.utils.c.a(getContext(), p3));
            }
            if (pVar != null) {
                pVar.m(this.K.getId());
                pVar.e(this.K.getVideoUrlEncode());
                pVar.o(str);
                pVar.k(this.K.getRequestIdNotice());
                pVar.l(k());
                a2.a(pVar);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        } catch (Throwable th) {
            th.printStackTrace();
            return;
        }
        com.mintegral.msdk.mtgnative.c.b.a(this.K.getCampaignUnitId(), this.K);
    }

    public void OnBufferingStart(String str) {
        com.mintegral.msdk.base.utils.g.b("bufferMsg", str);
    }

    public void OnBufferingEnd() {
        com.mintegral.msdk.base.utils.g.b("bufferend", "bufferend");
    }

    public void onPlaySetDataSourceError(String str) {
        com.mintegral.msdk.base.utils.g.b("errorstr", str);
    }

    /* access modifiers changed from: private */
    public View q() {
        try {
            this.ad = new RelativeLayout(getContext());
            this.ad.setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
            this.ae = new ImageView(getContext());
            this.ae.setScaleType(ScaleType.FIT_XY);
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(k.b(getContext(), 30.0f), k.b(getContext(), 30.0f));
            layoutParams.addRule(11);
            layoutParams.addRule(10);
            layoutParams.topMargin = k.b(getContext(), 8.0f);
            layoutParams.rightMargin = k.b(getContext(), 8.0f);
            this.ae.setLayoutParams(layoutParams);
            this.ae.setBackgroundResource(com.mintegral.msdk.base.utils.p.a(getContext(), "mintegral_nativex_close", "drawable"));
            this.ae.setOnClickListener(new OnClickListener() {
                public final void onClick(View view) {
                    MTGMediaView.this.exitFullScreen();
                }
            });
            this.w.setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
            this.ad.addView(this.w);
            this.ad.addView(this.ae);
            return this.ad;
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public void exitFullScreen() {
        try {
            r();
            String str = TAG;
            StringBuilder sb = new StringBuilder("=========webview close mAllowLoopPlay:");
            sb.append(this.b);
            com.mintegral.msdk.base.utils.g.b(str, sb.toString());
            if (this.b) {
                com.mintegral.msdk.base.utils.g.b(TAG, "播放结束 调用onClickPlayButton");
                this.r.onClickPlayButton();
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public void showEndCardWebViewCloseBtn() {
        if (this.ae != null && this.ae.getVisibility() != 0) {
            this.ae.setVisibility(0);
        }
    }

    public void hideEndCardWebViewCloseBtn() {
        if (this.ae != null && this.ae.getVisibility() == 0) {
            this.ae.setVisibility(8);
        }
    }

    /* access modifiers changed from: private */
    public void r() {
        try {
            FrameLayout frameLayout = (FrameLayout) getRootView().findViewById(16908290);
            FrameLayout frameLayout2 = (FrameLayout) getRootView().findViewById(100);
            RelativeLayout relativeLayout = (RelativeLayout) getRootView().findViewById(101);
            RelativeLayout relativeLayout2 = (RelativeLayout) getRootView().findViewById(103);
            if (relativeLayout2 == null && relativeLayout != null) {
                relativeLayout2 = (RelativeLayout) relativeLayout.findViewById(103);
            }
            RelativeLayout relativeLayout3 = (RelativeLayout) getRootView().findViewById(com.mintegral.msdk.base.utils.p.a(getContext(), "mintegral_full_rl_playcontainer", "id"));
            if (relativeLayout3 == null && relativeLayout2 != null) {
                relativeLayout3 = (RelativeLayout) relativeLayout2.findViewById(com.mintegral.msdk.base.utils.p.a(getContext(), "mintegral_full_rl_playcontainer", "id"));
            }
            RelativeLayout relativeLayout4 = (RelativeLayout) getRootView().findViewById(com.mintegral.msdk.base.utils.p.a(getContext(), "mintegral_full_player_parent", "id"));
            if (relativeLayout4 == null && relativeLayout3 != null) {
                relativeLayout4 = (RelativeLayout) relativeLayout3.findViewById(com.mintegral.msdk.base.utils.p.a(getContext(), "mintegral_full_player_parent", "id"));
            }
            ProgressBar progressBar = (ProgressBar) getRootView().findViewById(com.mintegral.msdk.base.utils.p.a(getContext(), "mintegral_full_pb_loading", "id"));
            RelativeLayout relativeLayout5 = (RelativeLayout) getRootView().findViewById(com.mintegral.msdk.base.utils.p.a(getContext(), "mintegral_full_rl_install", "id"));
            LinearLayout linearLayout = (LinearLayout) getRootView().findViewById(com.mintegral.msdk.base.utils.p.a(getContext(), "mintegral_full_ll_pro_dur", "id"));
            ViewGroup viewGroup = frameLayout2 != null ? (ViewGroup) frameLayout2.getParent() : null;
            if (relativeLayout2 != null) {
                relativeLayout2.removeView(linearLayout);
            } else if (!(linearLayout == null || linearLayout.getParent() == null)) {
                ((ViewGroup) linearLayout.getParent()).removeView(linearLayout);
            }
            if (this.ad != null) {
                if (relativeLayout2 != null) {
                    relativeLayout2.removeView(this.ad);
                } else if (this.ad.getParent() != null) {
                    ((ViewGroup) this.ad.getParent()).removeView(this.ad);
                }
                this.w.setBackListener(null);
                this.w.setObject(null);
                this.w = null;
                this.ad = null;
            }
            if (relativeLayout2 != null) {
                relativeLayout2.removeView(relativeLayout5);
            } else if (!(relativeLayout5 == null || relativeLayout5.getParent() == null)) {
                ((ViewGroup) relativeLayout5.getParent()).removeView(relativeLayout5);
            }
            if (relativeLayout2 != null) {
                relativeLayout2.removeView(progressBar);
            } else if (!(progressBar == null || progressBar.getParent() == null)) {
                ((ViewGroup) progressBar.getParent()).removeView(progressBar);
            }
            if (relativeLayout4 != null) {
                relativeLayout4.removeView(this.r);
            }
            if (relativeLayout3 != null) {
                relativeLayout3.removeView(relativeLayout4);
            } else if (!(relativeLayout4 == null || relativeLayout4.getParent() == null)) {
                ((ViewGroup) relativeLayout4.getParent()).removeView(relativeLayout4);
            }
            if (relativeLayout2 != null) {
                relativeLayout2.removeView(relativeLayout3);
            } else if (relativeLayout3 != null && relativeLayout3.getParent() != null) {
                ((ViewGroup) relativeLayout3.getParent()).removeView(relativeLayout3);
            } else if (this.B != null) {
                this.B.removeView(this.C);
            }
            if (relativeLayout != null) {
                relativeLayout.removeView(relativeLayout2);
            } else if (relativeLayout2 != null && relativeLayout2.getParent() != null) {
                ((ViewGroup) relativeLayout2.getParent()).removeView(relativeLayout2);
            } else if (this.A != null) {
                ((ViewGroup) this.A).removeView(this.B);
                ((ViewGroup) this.A.getParent()).removeView(this.A);
            }
            if (frameLayout != null) {
                if (relativeLayout != null) {
                    frameLayout.removeView(relativeLayout);
                } else {
                    frameLayout.removeView(this.A);
                    if (this.A.getParent() != null) {
                        ((ViewGroup) this.A.getParent()).removeView(this.A);
                        this.A.setVisibility(8);
                    }
                }
            }
            setVisibility(0);
            requestLayout();
            if (viewGroup != null) {
                if (!(this.r.getParent() == null || this.r.getParent() == viewGroup)) {
                    ((ViewGroup) this.r.getParent()).removeView(this.r);
                }
                viewGroup.addView(this.r, this.I);
                viewGroup.removeView(frameLayout2);
                viewGroup.invalidate();
            }
            try {
                this.r.showSoundIndicator(this.l);
                this.r.showProgressView(this.k);
                if (this.Z != null) {
                    this.Z.onExitFullscreen();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
            this.h = false;
            if (this.r != null) {
                this.r.setExitFullScreen();
                if (this.f) {
                    this.r.openSound();
                } else {
                    this.r.closeSound();
                }
                this.r.gonePauseView();
            }
        } catch (Exception e3) {
            e3.printStackTrace();
        }
    }

    private boolean s() {
        try {
            this.C = this.B.getMintegralFullPlayContainer();
            this.H = this.B.getMintegralFullPlayerParent();
            this.D = this.B.getMintegralFullClose();
            this.E = this.B.getMintegralFullIvClose();
            this.F = this.B.getMintegralFullTvInstall();
            this.G = this.B.getMintegralFullPb();
            return true;
        } catch (Throwable th) {
            th.printStackTrace();
            return false;
        }
    }

    private void t() {
        try {
            this.i = k.j(getContext()) >= k.k(getContext());
            this.j = this.i;
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    public float u() {
        try {
            float j2 = (float) k.j(getContext());
            if (this.i) {
                j2 += (float) k.l(getContext());
            }
            return j2;
        } catch (Throwable th) {
            th.printStackTrace();
            return 0.0f;
        }
    }

    private float v() {
        try {
            float k2 = (float) k.k(getContext());
            if (!this.i) {
                k2 += (float) k.l(getContext());
            }
            return k2;
        } catch (Throwable th) {
            th.printStackTrace();
            return 0.0f;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:55:0x014e, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:?, code lost:
        r0.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x017f, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x0180, code lost:
        r0.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x0183, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0012, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [B:3:0x000e, B:52:0x014a] */
    private void a(View view, float f2, float f3) {
        View view2 = view;
        float f4 = f2;
        float f5 = f3;
        if (view2 == null) {
            com.mintegral.msdk.base.utils.g.b(TAG, "setPlayViewParamsByVidoWH view is null");
            return;
        }
        double d2 = this.O;
        double d3 = Utils.DOUBLE_EPSILON;
        int i2 = -1;
        if (d2 > Utils.DOUBLE_EPSILON) {
            if (this.P > Utils.DOUBLE_EPSILON) {
                double d4 = this.O / this.P;
                if (f4 > 0.0f && f5 > 0.0f) {
                    d3 = (double) (f4 / f5);
                }
                double a2 = k.a(Double.valueOf(d4));
                double a3 = k.a(Double.valueOf(d3));
                RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) this.s.getLayoutParams();
                RelativeLayout.LayoutParams layoutParams2 = (RelativeLayout.LayoutParams) view.getLayoutParams();
                boolean z2 = false;
                if (((getContext().getResources().getConfiguration().screenLayout & 15) >= 3) && this.i) {
                    z2 = true;
                }
                if (a2 > a3) {
                    double d5 = (double) f4;
                    double d6 = this.P;
                    Double.isNaN(d5);
                    double d7 = (d5 * d6) / this.O;
                    String str = TAG;
                    StringBuilder sb = new StringBuilder(" setPlayView 宽铺满 playerViewHeight:");
                    sb.append(d7);
                    sb.append(" onMeasure mDevWidth ");
                    sb.append(this.N);
                    sb.append(" * mDevHeight *****");
                    sb.append(this.M);
                    com.mintegral.msdk.base.utils.g.b(str, sb.toString());
                    layoutParams2.width = -1;
                    if (!z2) {
                        i2 = (int) d7;
                    }
                    layoutParams2.height = i2;
                    layoutParams2.addRule(13);
                    layoutParams.width = this.N;
                    layoutParams.height = (int) d7;
                    layoutParams.addRule(13);
                } else if (a2 < a3) {
                    double d8 = (double) f5;
                    Double.isNaN(d8);
                    double d9 = d8 * d4;
                    layoutParams2.width = z2 ? -1 : (int) d9;
                    layoutParams2.height = -1;
                    layoutParams2.addRule(13);
                    layoutParams.width = (int) d9;
                    layoutParams.height = this.M;
                    layoutParams.addRule(13);
                    String str2 = TAG;
                    StringBuilder sb2 = new StringBuilder("setPlayView 高铺满 playerViewWidth:");
                    sb2.append(d9);
                    sb2.append(" mDevWidth ");
                    sb2.append(this.N);
                    sb2.append(" * mDevHeight *****");
                    sb2.append(this.M);
                    com.mintegral.msdk.base.utils.g.b(str2, sb2.toString());
                } else {
                    layoutParams2.width = -1;
                    layoutParams2.height = -1;
                    layoutParams.width = this.N;
                    layoutParams.height = this.M;
                    layoutParams.addRule(13);
                    String str3 = TAG;
                    StringBuilder sb3 = new StringBuilder("setPlayView 铺满父布局  videoWHDivide_final：");
                    sb3.append(a2);
                    sb3.append("  screenWHDivide_final：");
                    sb3.append(a3);
                    com.mintegral.msdk.base.utils.g.b(str3, sb3.toString());
                }
                if (!this.h) {
                    this.s.setLayoutParams(layoutParams);
                }
                view2.setLayoutParams(layoutParams2);
                return;
            }
        }
        if (view2 == null) {
            com.mintegral.msdk.base.utils.g.b(TAG, "setPlayViewParamsDefault view is null");
        } else if (this.i) {
            RelativeLayout.LayoutParams layoutParams3 = (RelativeLayout.LayoutParams) view.getLayoutParams();
            layoutParams3.width = -1;
            layoutParams3.height = -1;
            view2.setLayoutParams(layoutParams3);
        } else {
            RelativeLayout.LayoutParams layoutParams4 = (RelativeLayout.LayoutParams) view.getLayoutParams();
            float u2 = u();
            layoutParams4.width = -1;
            layoutParams4.height = (((int) u2) * 9) / 16;
            layoutParams4.addRule(13);
            view2.setLayoutParams(layoutParams4);
        }
    }

    /* access modifiers changed from: private */
    public void w() {
        try {
            if (!(this.D == null && this.ae == null)) {
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(k.b(getContext(), 30.0f), k.b(getContext(), 30.0f));
                layoutParams.addRule(11);
                layoutParams.addRule(10);
                layoutParams.topMargin = k.b(getContext(), 8.0f);
                layoutParams.rightMargin = k.b(getContext(), 8.0f);
                if (this.D != null) {
                    this.D.setLayoutParams(layoutParams);
                }
                if (this.ae != null) {
                    this.ae.setLayoutParams(layoutParams);
                }
                updateViewManger(false);
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    public void x() {
        try {
            if (!(this.D == null && this.ae == null)) {
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(k.b(getContext(), 30.0f), k.b(getContext(), 30.0f));
                layoutParams.addRule(11);
                layoutParams.addRule(10);
                if (this.aa != 0 || !k.m(getContext())) {
                    layoutParams.rightMargin = k.b(getContext(), 8.0f);
                } else {
                    layoutParams.rightMargin = k.l(getContext()) + k.b(getContext(), 8.0f);
                }
                layoutParams.topMargin = k.b(getContext(), 8.0f);
                if (this.D != null) {
                    this.D.setLayoutParams(layoutParams);
                }
                if (this.ae != null) {
                    this.ae.setLayoutParams(layoutParams);
                }
                updateViewManger(true);
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public void updateViewManger(boolean z2) {
        com.mintegral.msdk.nativex.view.mtgfullview.b a2 = com.mintegral.msdk.nativex.view.mtgfullview.b.a(getContext());
        if (a2 != null) {
            try {
                com.mintegral.msdk.nativex.view.mtgfullview.b.a(this.B);
                a2.a(z2, !this.r.isComplete(), this.B);
                a2.a(z2, this.B, this.aa);
            } catch (NullPointerException e2) {
                e2.printStackTrace();
                return;
            }
        }
        if ((this.B instanceof MIntegralTopFullView) && a2 != null) {
            com.mintegral.msdk.nativex.view.mtgfullview.b.a(!z2, this.B);
        }
    }

    private void a(int i2, int i3) {
        try {
            if (this.K != null) {
                com.mintegral.msdk.base.entity.CampaignEx.b mediaViewHolder = this.K.getMediaViewHolder();
                if (!(mediaViewHolder == null || mediaViewHolder.i || i3 == 0)) {
                    List g2 = this.K.getNativeVideoTracking().g();
                    int i4 = ((i2 + 1) * 100) / i3;
                    if (g2 != null) {
                        int i5 = 0;
                        while (i5 < g2.size()) {
                            Map map = (Map) g2.get(i5);
                            if (map != null && map.size() > 0) {
                                Iterator it = map.entrySet().iterator();
                                while (it.hasNext()) {
                                    Entry entry = (Entry) it.next();
                                    int intValue = ((Integer) entry.getKey()).intValue();
                                    String str = (String) entry.getValue();
                                    if (intValue <= i4 && !TextUtils.isEmpty(str)) {
                                        com.mintegral.msdk.click.a.a(getContext(), this.K, this.K.getCampaignUnitId(), new String[]{str}, true);
                                        it.remove();
                                        g2.remove(i5);
                                        i5--;
                                    }
                                }
                            }
                            i5++;
                        }
                        if (g2.size() <= 0) {
                            mediaViewHolder.i = true;
                        }
                    }
                }
            }
        } catch (Throwable unused) {
            com.mintegral.msdk.base.utils.g.d("", "reportPlayPercentageData error");
        }
    }

    private void y() {
        int i2;
        try {
            if (!(this.K == null || this.K.getNativeVideoTracking() == null)) {
                String[] f2 = this.K.getNativeVideoTracking().f();
                if (this.i) {
                    i2 = n;
                } else {
                    i2 = o;
                }
                for (String str : f2) {
                    if (!TextUtils.isEmpty(str)) {
                        StringBuilder sb = new StringBuilder();
                        sb.append(str);
                        sb.append("&orienation=");
                        sb.append(i2);
                        com.mintegral.msdk.click.a.a(getContext(), this.K, this.K.getCampaignUnitId(), sb.toString(), false, false);
                    }
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public void changeNoticeURL() {
        if (this.K != null) {
            String noticeUrl = this.K.getNoticeUrl();
            if (!TextUtils.isEmpty(noticeUrl)) {
                if (!noticeUrl.contains("is_video")) {
                    String str = "";
                    if (this.W == a.VIDEO) {
                        str = "1";
                    } else if (this.W == a.BIG_IMAGE) {
                        str = "2";
                    }
                    StringBuilder sb = new StringBuilder(noticeUrl);
                    if (noticeUrl.contains("?")) {
                        sb.append("&is_video=");
                        sb.append(str);
                    } else {
                        sb.append("?is_video=");
                        sb.append(str);
                    }
                    noticeUrl = sb.toString();
                } else if (this.W == a.VIDEO) {
                    if (noticeUrl.contains("is_video=2")) {
                        noticeUrl = noticeUrl.replace("is_video=2", "is_video=1");
                    }
                } else if (this.W == a.BIG_IMAGE && noticeUrl.contains("is_video=1")) {
                    noticeUrl = noticeUrl.replace("is_video=1", "is_video=2");
                }
                this.K.setNoticeUrl(noticeUrl);
            }
        }
    }

    public String getAddNVT2ToNoticeURL() {
        if (this.K == null) {
            return null;
        }
        String noticeUrl = this.K.getNoticeUrl();
        if (!TextUtils.isEmpty(noticeUrl) && !noticeUrl.contains(CampaignEx.JSON_KEY_NV_T2)) {
            StringBuilder sb = new StringBuilder(noticeUrl);
            sb.append("&nv_t2=");
            sb.append(this.K.getNvT2());
            noticeUrl = sb.toString();
        }
        return noticeUrl;
    }

    public boolean canShowVideo() {
        a a2 = a(false);
        if (a2 == a.VIDEO || a2 == a.FB) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: private */
    public WindVaneWebViewForNV z() {
        String str = TAG;
        try {
            StringBuilder sb = new StringBuilder("getEndCardWebview hadStarLoad:");
            sb.append(this.ac);
            sb.append("-endCardWebview:");
            sb.append(this.w);
            com.mintegral.msdk.base.utils.g.d(str, sb.toString());
            if (this.w != null && this.ac) {
                return this.w;
            }
            if (!this.ac) {
                A();
            }
            return null;
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    private void A() {
        try {
            int nvT2 = this.K.getNvT2();
            if (this.w == null) {
                this.w = new WindVaneWebViewForNV(getContext());
                this.w.setObject(this);
                this.w.setBackListener(new com.mintegral.msdk.nativex.listener.b() {
                    public final void a() {
                        MTGMediaView.this.exitFullScreen();
                    }
                });
                this.w.setWebViewListener(new com.mintegral.msdk.mtgjscommon.b.a() {
                    public final void a(WebView webView, String str) {
                        super.a(webView, str);
                        MTGMediaView.this.ab = true;
                    }

                    public final void a(WebView webView, int i, String str, String str2) {
                        super.a(webView, i, str, str2);
                        MTGMediaView.this.ab = false;
                    }
                });
            }
            if (nvT2 == 3) {
                String str = this.K.getendcard_url();
                if (!TextUtils.isEmpty(str)) {
                    this.K.getMediaViewHolder();
                    if (!str.contains(".zip") || !str.contains("md5filename")) {
                        String a2 = com.mintegral.msdk.videocommon.download.h.a().a(str);
                        if (s.b(a2)) {
                            com.mintegral.msdk.base.utils.g.a(TAG, "load html...");
                            this.ac = true;
                            this.w.loadDataWithBaseURL(str, a2, WebRequest.CONTENT_TYPE_HTML, "UTF-8", null);
                        }
                    } else {
                        String a3 = com.mintegral.msdk.videocommon.download.g.a().a(str);
                        if (s.b(a3)) {
                            this.ac = true;
                            this.w.loadUrl(a3);
                        }
                        return;
                    }
                }
                return;
            }
            if (nvT2 == 4) {
                this.ac = true;
                if (this.K != null) {
                    MTGDownloadListener mTGDownloadListener = new MTGDownloadListener(this.K);
                    mTGDownloadListener.setTitle(this.K.getAppName());
                    this.w.setDownloadListener(mTGDownloadListener);
                    this.w.setFilter(new d(0));
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public int handleViewStyleResult$21377bb9() {
        int nvT2 = this.K.getNvT2();
        if (nvT2 == 6) {
            return com.mintegral.msdk.nativex.view.mtgfullview.BaseView.a.f2846a;
        }
        switch (nvT2) {
            case 1:
                j();
                break;
            case 2:
                return com.mintegral.msdk.nativex.view.mtgfullview.BaseView.a.b;
            case 3:
                return com.mintegral.msdk.nativex.view.mtgfullview.BaseView.a.b;
            case 4:
                return com.mintegral.msdk.nativex.view.mtgfullview.BaseView.a.b;
        }
        return 0;
    }

    public void setProgressVisibility(boolean z2) {
        this.k = z2;
        if (this.r != null) {
            this.r.showProgressView(this.k);
        }
    }

    public void setSoundIndicatorVisibility(boolean z2) {
        this.l = z2;
        if (this.r != null) {
            this.r.showSoundIndicator(this.l);
        }
    }

    public void setVideoSoundOnOff(boolean z2) {
        this.f = z2;
        if (this.r != null) {
            if (this.f) {
                this.r.openSound();
                return;
            }
            this.r.closeSound();
        }
    }

    private void a() {
        try {
            this.J = new Handler() {
                public final void handleMessage(Message message) {
                    if (message != null) {
                        try {
                            switch (message.what) {
                                case 1:
                                    MTGMediaView.a(MTGMediaView.this);
                                    return;
                                case 2:
                                    return;
                                case 3:
                                    Object obj = message.obj;
                                    if (obj != null && (obj instanceof View)) {
                                        if (MTGMediaView.this.a((View) obj)) {
                                            MTGMediaView.b(MTGMediaView.this);
                                            break;
                                        }
                                    }
                                    break;
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            };
            com.mintegral.msdk.base.utils.g.d(TAG, "initView");
            int a2 = com.mintegral.msdk.base.utils.p.a(getContext(), "mintegral_nativex_mtgmediaview", TtmlNode.TAG_LAYOUT);
            if (a2 == -1) {
                com.mintegral.msdk.base.utils.g.d(TAG, "can not find mediaview resource");
                return;
            }
            View inflate = LayoutInflater.from(getContext()).inflate(a2, null);
            this.t = (RelativeLayout) inflate.findViewById(com.mintegral.msdk.base.utils.p.a(getContext(), "mintegral_rl_mediaview_root", "id"));
            this.s = (RelativeLayout) inflate.findViewById(com.mintegral.msdk.base.utils.p.a(getContext(), "mintegral_ll_playerview_container", "id"));
            this.y = (MyImageView) inflate.findViewById(com.mintegral.msdk.base.utils.p.a(getContext(), "mintegral_my_big_img", "id"));
            this.x = (RelativeLayout) inflate.findViewById(com.mintegral.msdk.base.utils.p.a(getContext(), "mintegral_fb_mediaview_layout", "id"));
            this.z = (ProgressBar) inflate.findViewById(com.mintegral.msdk.base.utils.p.a(getContext(), "mintegral_native_pb", "id"));
            this.u = (RelativeLayout) inflate.findViewById(com.mintegral.msdk.base.utils.p.a(getContext(), "mintegral_nativex_webview_layout", "id"));
            this.v = (WindVaneWebViewForNV) inflate.findViewById(com.mintegral.msdk.base.utils.p.a(getContext(), "mintegral_nativex_webview_layout_webview", "id"));
            this.t.setClickable(true);
            addView(inflate, -1, -1);
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    public void c() {
        try {
            if (this.K != null) {
                if (!s.a(this.K.getVideoResolution())) {
                    String videoResolution = this.K.getVideoResolution();
                    String str = TAG;
                    StringBuilder sb = new StringBuilder("videoResolution:");
                    sb.append(videoResolution);
                    com.mintegral.msdk.base.utils.g.d(str, sb.toString());
                    String[] split = videoResolution.split(AvidJSONUtil.KEY_X);
                    if (split != null && split.length == 2) {
                        String str2 = split[0];
                        String str3 = split[1];
                        double c2 = k.c(str2);
                        double c3 = k.c(str3);
                        if (c2 > Utils.DOUBLE_EPSILON && c3 > Utils.DOUBLE_EPSILON) {
                            this.O = c2;
                            this.P = c3;
                        }
                    }
                    h();
                }
            }
            com.mintegral.msdk.base.utils.g.b(TAG, "campaign is null initVideoWH return");
        } catch (Throwable th) {
            th.printStackTrace();
        }
        h();
    }

    /* access modifiers changed from: private */
    public void d() {
        try {
            this.s.setVisibility(8);
            this.y.setVisibility(8);
            this.x.setVisibility(8);
            this.u.setVisibility(0);
            this.v.setVisibility(0);
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    public void j() {
        try {
            if (this.Z != null) {
                this.Z.onVideoAdClicked(this.K);
            }
        } catch (Exception e2) {
            try {
                e2.printStackTrace();
            } catch (Exception e3) {
                e3.printStackTrace();
                return;
            }
        }
        if (this.K != null) {
            if (!s.a(k())) {
                if (!this.K.isReportClick()) {
                    this.K.setReportClick(true);
                    if (!(this.K == null || this.K.getNativeVideoTracking() == null || this.K.getNativeVideoTracking().j() == null)) {
                        com.mintegral.msdk.click.a.a(getContext(), this.K, this.K.getCampaignUnitId(), this.K.getNativeVideoTracking().j(), false);
                    }
                }
                com.mintegral.msdk.click.a aVar = new com.mintegral.msdk.click.a(getContext(), k());
                aVar.a((NativeTrackingListener) new NativeTrackingListener() {
                    public final void onDismissLoading(Campaign campaign) {
                    }

                    public final void onDownloadFinish(Campaign campaign) {
                    }

                    public final void onDownloadProgress(int i) {
                    }

                    public final void onDownloadStart(Campaign campaign) {
                    }

                    public final boolean onInterceptDefaultLoadingDialog() {
                        return true;
                    }

                    public final void onShowLoading(Campaign campaign) {
                    }

                    public final void onStartRedirection(Campaign campaign, String str) {
                        try {
                            MTGMediaView.m(MTGMediaView.this);
                            MTGMediaView.n(MTGMediaView.this);
                            MTGMediaView.a(MTGMediaView.this, campaign, str);
                            com.mintegral.msdk.base.utils.g.d(MTGMediaView.TAG, "=====showloading");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    public final void onRedirectionFailed(Campaign campaign, String str) {
                        try {
                            MTGMediaView.o(MTGMediaView.this);
                            MTGMediaView.p(MTGMediaView.this);
                            MTGMediaView.b(MTGMediaView.this, campaign, str);
                            com.mintegral.msdk.base.utils.g.d(MTGMediaView.TAG, "=====hideloading");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    public final void onFinishRedirection(Campaign campaign, String str) {
                        try {
                            MTGMediaView.o(MTGMediaView.this);
                            MTGMediaView.p(MTGMediaView.this);
                            MTGMediaView.c(MTGMediaView.this, campaign, str);
                            com.mintegral.msdk.base.utils.g.d(MTGMediaView.TAG, "=====hideloading");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
                aVar.b(this.K);
            }
        }
    }

    public void onPlayProgress(int i2, int i3) {
        try {
            if (this.K != null) {
                com.mintegral.msdk.base.entity.CampaignEx.b mediaViewHolder = this.K.getMediaViewHolder();
                if (mediaViewHolder != null && !mediaViewHolder.h && mediaViewHolder.l != null && mediaViewHolder.l.size() > 0) {
                    Map<Integer, String> map = mediaViewHolder.l;
                    String str = TAG;
                    StringBuilder sb = new StringBuilder("reportAdvImp pre advImpMap.size:");
                    sb.append(map.size());
                    com.mintegral.msdk.base.utils.g.b(str, sb.toString());
                    Iterator it = map.entrySet().iterator();
                    while (it.hasNext()) {
                        Entry entry = (Entry) it.next();
                        Integer num = (Integer) entry.getKey();
                        String str2 = (String) entry.getValue();
                        if (i2 >= num.intValue() && !TextUtils.isEmpty(str2)) {
                            com.mintegral.msdk.click.a.a(getContext(), this.K, this.K.getCampaignUnitId(), str2, false, false);
                            it.remove();
                            String str3 = TAG;
                            StringBuilder sb2 = new StringBuilder("reportAdvImp remove value:");
                            sb2.append(str2);
                            com.mintegral.msdk.base.utils.g.b(str3, sb2.toString());
                        }
                    }
                    String str4 = TAG;
                    StringBuilder sb3 = new StringBuilder("reportAdvImp advImpMap after size:");
                    sb3.append(map.size());
                    com.mintegral.msdk.base.utils.g.b(str4, sb3.toString());
                    if (map.size() <= 0) {
                        mediaViewHolder.h = true;
                    }
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        a(i2, i3);
    }

    static /* synthetic */ void a(MTGMediaView mTGMediaView) {
        try {
            if (mTGMediaView.getVisibility() == 0) {
                boolean a2 = mTGMediaView.a((View) mTGMediaView);
                if (a2) {
                    int i2 = 0;
                    com.mintegral.msdk.b.d o2 = mTGMediaView.o();
                    if (o2 != null) {
                        i2 = o2.a();
                    }
                    Message obtainMessage = mTGMediaView.J.obtainMessage();
                    obtainMessage.what = 3;
                    obtainMessage.obj = mTGMediaView;
                    mTGMediaView.J.sendMessageDelayed(obtainMessage, (long) (i2 * 1000));
                }
                if (mTGMediaView.W == a.VIDEO) {
                    if (!a2) {
                        try {
                            if (mTGMediaView.r != null && mTGMediaView.r.hasPrepare() && mTGMediaView.r.isPlaying()) {
                                com.mintegral.msdk.base.utils.g.b(TAG, "isPlaying pasue======");
                                if (mTGMediaView.r != null) {
                                    mTGMediaView.r.pause();
                                }
                            }
                        } catch (Throwable th) {
                            th.printStackTrace();
                        }
                    } else if (mTGMediaView.r == null) {
                        com.mintegral.msdk.base.utils.g.b(TAG, "onpreDraw addPlayerView");
                        mTGMediaView.l();
                    } else if (mTGMediaView.K != mTGMediaView.r.getCampaign()) {
                        mTGMediaView.r.release();
                        mTGMediaView.l();
                        if (mTGMediaView.getParent() != null) {
                            ((View) mTGMediaView.getParent()).invalidate();
                        }
                        mTGMediaView.requestLayout();
                        com.mintegral.msdk.base.utils.g.b(TAG, "playerview realese and addplayerview");
                    } else {
                        if (mTGMediaView.r != null && mTGMediaView.r.hasPrepare() && !mTGMediaView.r.isPlaying() && !mTGMediaView.r.isComplete() && mTGMediaView.r.hasPrepare()) {
                            mTGMediaView.r.startOrPlayVideo();
                        }
                    }
                }
            }
        } catch (Throwable th2) {
            th2.printStackTrace();
        }
    }

    static /* synthetic */ void b(MTGMediaView mTGMediaView) {
        if (mTGMediaView.K != null && mTGMediaView.K.getMediaViewHolder() != null) {
            com.mintegral.msdk.mtgnative.e.b.a(mTGMediaView.K, mTGMediaView.getContext(), mTGMediaView.k(), null);
            com.mintegral.msdk.base.entity.CampaignEx.b mediaViewHolder = mTGMediaView.K.getMediaViewHolder();
            if (!mediaViewHolder.f2588a && mTGMediaView.W == a.VIDEO && s.b(mTGMediaView.K.getImpressionURL())) {
                mediaViewHolder.f2588a = true;
                String impressionURL = mTGMediaView.K.getImpressionURL();
                if (!impressionURL.contains("is_video=1")) {
                    StringBuilder sb = new StringBuilder(impressionURL);
                    if (impressionURL.contains("?")) {
                        sb.append("&is_video=1");
                    } else {
                        sb.append("?is_video=1");
                    }
                    impressionURL = sb.toString();
                }
                String str = impressionURL;
                String str2 = TAG;
                StringBuilder sb2 = new StringBuilder("change impressionurl:");
                sb2.append(str);
                com.mintegral.msdk.base.utils.g.b(str2, sb2.toString());
                com.mintegral.msdk.click.a.a(mTGMediaView.getContext(), mTGMediaView.K, mTGMediaView.K.getCampaignUnitId(), str, false, true);
            }
        }
    }

    static /* synthetic */ void d(MTGMediaView mTGMediaView) {
        if (mTGMediaView.K != null) {
            com.mintegral.msdk.base.entity.CampaignEx.b mediaViewHolder = mTGMediaView.K.getMediaViewHolder();
            if (mediaViewHolder != null && !mediaViewHolder.g && mTGMediaView.K.getNativeVideoTracking() != null && mTGMediaView.K.getNativeVideoTracking().q() != null) {
                mediaViewHolder.g = true;
                com.mintegral.msdk.click.a.a(mTGMediaView.getContext(), mTGMediaView.K, mTGMediaView.K.getCampaignUnitId(), mTGMediaView.K.getNativeVideoTracking().q(), false);
            }
        }
    }

    static /* synthetic */ void h(MTGMediaView mTGMediaView) {
        try {
            if (mTGMediaView.getRootView() != null) {
                if (mTGMediaView.getRootView() instanceof ViewGroup) {
                    int handleViewStyleResult$21377bb9 = mTGMediaView.handleViewStyleResult$21377bb9();
                    if (handleViewStyleResult$21377bb9 != 0) {
                        mTGMediaView.B = (BaseView) com.mintegral.msdk.nativex.view.mtgfullview.a.a(mTGMediaView.getContext()).a(handleViewStyleResult$21377bb9);
                        if (mTGMediaView.B == null) {
                            com.mintegral.msdk.base.utils.g.b(TAG, "mFullScreenViewUI is null");
                            return;
                        } else if (!mTGMediaView.s()) {
                            com.mintegral.msdk.base.utils.g.d(TAG, "fullViewFailed return");
                            return;
                        } else {
                            com.mintegral.msdk.nativex.view.mtgfullview.b.a(mTGMediaView.getContext()).a(mTGMediaView.B.style$161ffeb8, mTGMediaView.K, mTGMediaView.B);
                            mTGMediaView.h = true;
                            mTGMediaView.q = false;
                            if (mTGMediaView.r != null) {
                                mTGMediaView.r.setEnterFullScreen();
                                mTGMediaView.r.setIsActivePause(false);
                            }
                            try {
                                if (mTGMediaView.Z != null) {
                                    mTGMediaView.Z.onEnterFullscreen();
                                }
                            } catch (Exception e2) {
                                e2.printStackTrace();
                            }
                            FrameLayout frameLayout = (FrameLayout) mTGMediaView.getRootView().findViewById(16908290);
                            RelativeLayout relativeLayout = new RelativeLayout(mTGMediaView.getContext());
                            mTGMediaView.A = relativeLayout;
                            mTGMediaView.A.setClickable(true);
                            ViewGroup viewGroup = (ViewGroup) mTGMediaView.r.getParent();
                            int childCount = viewGroup.getChildCount();
                            int i2 = 0;
                            while (i2 < childCount && viewGroup.getChildAt(i2) != mTGMediaView.r) {
                                i2++;
                            }
                            mTGMediaView.I = i2;
                            FrameLayout frameLayout2 = new FrameLayout(mTGMediaView.getContext());
                            frameLayout2.setId(100);
                            viewGroup.addView(frameLayout2, i2, new LayoutParams(mTGMediaView.getWidth(), mTGMediaView.getHeight()));
                            viewGroup.removeView(mTGMediaView.r);
                            FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-1, -1);
                            relativeLayout.setId(101);
                            new RelativeLayout.LayoutParams(-1, -1);
                            mTGMediaView.H.addView(mTGMediaView.r, new RelativeLayout.LayoutParams(-1, -1));
                            frameLayout.addView(relativeLayout, layoutParams);
                            int i3 = ViewCompat.MEASURED_STATE_MASK;
                            switch (AnonymousClass8.f2822a[mTGMediaView.B.style$161ffeb8 - 1]) {
                                case 1:
                                    break;
                                case 2:
                                    i3 = -1;
                                    break;
                            }
                            relativeLayout.setBackgroundColor(i3);
                            mTGMediaView.B.setId(103);
                            RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-1, -1);
                            int[] iArr = new int[2];
                            frameLayout.getLocationInWindow(iArr);
                            mTGMediaView.aa = iArr[1];
                            String str = TAG;
                            StringBuilder sb = new StringBuilder("mFullViewStartY:");
                            sb.append(mTGMediaView.aa);
                            com.mintegral.msdk.base.utils.g.b(str, sb.toString());
                            if (mTGMediaView.aa == 0) {
                                com.mintegral.msdk.base.utils.g.b(TAG, "addfullview 增加状态栏高度 沉浸式时contentview的高度和屏幕高度一样");
                                layoutParams2.setMargins(0, k.e(mTGMediaView.getContext()), 0, 0);
                            }
                            relativeLayout.addView(mTGMediaView.B, layoutParams2);
                            try {
                                mTGMediaView.t();
                                mTGMediaView.a((View) mTGMediaView.C, mTGMediaView.u(), mTGMediaView.v());
                                com.mintegral.msdk.nativex.view.mtgfullview.b.a(mTGMediaView.getContext());
                                com.mintegral.msdk.nativex.view.mtgfullview.b.a(mTGMediaView.B, mTGMediaView.i);
                                if (mTGMediaView.i) {
                                    mTGMediaView.x();
                                } else {
                                    mTGMediaView.w();
                                }
                                mTGMediaView.J.postDelayed(new Runnable() {
                                    public final void run() {
                                        MTGMediaView.this.q = true;
                                        if (MTGMediaView.this.h) {
                                            MTGMediaView.this.F;
                                        }
                                    }
                                }, 3000);
                            } catch (Throwable th) {
                                th.printStackTrace();
                            }
                            try {
                                mTGMediaView.A.setFocusableInTouchMode(true);
                                mTGMediaView.A.requestFocus();
                                mTGMediaView.A.setOnKeyListener(new OnKeyListener() {
                                    public final boolean onKey(View view, int i, KeyEvent keyEvent) {
                                        try {
                                            if (keyEvent.getKeyCode() == 4) {
                                                MTGMediaView.this.r();
                                                return true;
                                            }
                                        } catch (Throwable th) {
                                            th.printStackTrace();
                                        }
                                        return false;
                                    }
                                });
                                mTGMediaView.A.setOnClickListener(new OnClickListener() {
                                    public final void onClick(View view) {
                                        MTGMediaView.i(MTGMediaView.this);
                                    }
                                });
                                mTGMediaView.D.setOnClickListener(new OnClickListener() {
                                    public final void onClick(View view) {
                                        MTGMediaView.this.r();
                                    }
                                });
                                mTGMediaView.F.setOnClickListener(new com.mintegral.msdk.widget.a() {
                                    /* access modifiers changed from: protected */
                                    public final void a() {
                                        try {
                                            com.mintegral.msdk.base.utils.g.b(MTGMediaView.TAG, "点击安装 click");
                                            MTGMediaView.this.j();
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                });
                            } catch (Exception e3) {
                                e3.printStackTrace();
                            }
                            if (mTGMediaView.r != null) {
                                mTGMediaView.r.openSound();
                            }
                            mTGMediaView.r.setMediaViewPlayListener(new com.mintegral.msdk.nativex.view.MediaViewPlayerView.a(mTGMediaView.r) {
                                public final void a() {
                                    String[] o;
                                    int nvT2 = MTGMediaView.this.K.getNvT2();
                                    if (!MTGMediaView.this.h || !(nvT2 == 3 || nvT2 == 4)) {
                                        super.a();
                                        return;
                                    }
                                    WindVaneWebViewForNV t = MTGMediaView.this.z();
                                    if (t != null) {
                                        View u = MTGMediaView.this.q();
                                        if (u == null) {
                                            super.a();
                                            return;
                                        }
                                        if (nvT2 == 3 && MTGMediaView.this.ab) {
                                            com.mintegral.msdk.nativex.view.mtgfullview.b.a(MTGMediaView.this.getContext());
                                            com.mintegral.msdk.nativex.view.mtgfullview.b.a(u, MTGMediaView.this.B);
                                            Context context = MTGMediaView.this.getContext();
                                            CampaignEx s = MTGMediaView.this.K;
                                            String campaignUnitId = MTGMediaView.this.K.getCampaignUnitId();
                                            if (s != null) {
                                                try {
                                                    if (!(s.getNativeVideoTracking() == null || s.getNativeVideoTracking().o() == null)) {
                                                        for (String str : s.getNativeVideoTracking().o()) {
                                                            if (!TextUtils.isEmpty(str)) {
                                                                com.mintegral.msdk.click.a.a(context, s, campaignUnitId, str, false, true);
                                                            }
                                                        }
                                                    }
                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        } else if (nvT2 == 4) {
                                            com.mintegral.msdk.nativex.view.mtgfullview.b.a(MTGMediaView.this.getContext());
                                            com.mintegral.msdk.nativex.view.mtgfullview.b.a(u, MTGMediaView.this.B);
                                            String clickURL = MTGMediaView.this.K.getClickURL();
                                            if (!TextUtils.isEmpty(clickURL)) {
                                                com.mintegral.msdk.click.a.a(MTGMediaView.this.getContext(), MTGMediaView.this.K, MTGMediaView.this.k(), MTGMediaView.this.getAddNVT2ToNoticeURL(), true, false);
                                                MTGMediaView.this.w.loadUrl(clickURL);
                                            }
                                        } else {
                                            super.a();
                                            return;
                                        }
                                        t.webViewShow(MTGMediaView.this.K, MTGMediaView.this.k());
                                        t.orientation(MTGMediaView.this.i);
                                        return;
                                    }
                                    super.a();
                                }
                            });
                            mTGMediaView.A();
                            if (!(mTGMediaView.K == null || mTGMediaView.K.getMediaViewHolder() == null || mTGMediaView.K.getMediaViewHolder().k || TextUtils.isEmpty(mTGMediaView.K.getCampaignUnitId()) || mTGMediaView.K.getNativeVideoTracking() == null || mTGMediaView.K.getNativeVideoTracking().f() == null)) {
                                mTGMediaView.K.getMediaViewHolder().k = true;
                                mTGMediaView.y();
                            }
                            com.mintegral.msdk.base.utils.g.b(TAG, "mediaview add to full screen");
                            return;
                        }
                    } else {
                        return;
                    }
                }
            }
            com.mintegral.msdk.base.utils.g.b(TAG, "rootView is null");
        } catch (Exception e4) {
            if (MIntegralConstans.DEBUG) {
                e4.printStackTrace();
            }
        }
    }

    static /* synthetic */ void i(MTGMediaView mTGMediaView) {
        try {
            if (mTGMediaView.r == null) {
                com.mintegral.msdk.base.utils.g.b(TAG, "playerview is null return");
            } else {
                mTGMediaView.r.onClickPlayerView();
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    static /* synthetic */ void m(MTGMediaView mTGMediaView) {
        try {
            if (mTGMediaView.z != null) {
                mTGMediaView.z.setVisibility(0);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    static /* synthetic */ void n(MTGMediaView mTGMediaView) {
        try {
            if (mTGMediaView.G != null) {
                mTGMediaView.G.setVisibility(0);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    static /* synthetic */ void a(MTGMediaView mTGMediaView, Campaign campaign, String str) {
        try {
            if (mTGMediaView.Z != null) {
                mTGMediaView.Z.onStartRedirection(campaign, str);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    static /* synthetic */ void o(MTGMediaView mTGMediaView) {
        try {
            if (mTGMediaView.z != null) {
                mTGMediaView.z.setVisibility(8);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    static /* synthetic */ void p(MTGMediaView mTGMediaView) {
        try {
            if (mTGMediaView.G != null) {
                mTGMediaView.G.setVisibility(8);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    static /* synthetic */ void b(MTGMediaView mTGMediaView, Campaign campaign, String str) {
        try {
            if (mTGMediaView.Z != null) {
                mTGMediaView.Z.onRedirectionFailed(campaign, str);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    static /* synthetic */ void c(MTGMediaView mTGMediaView, Campaign campaign, String str) {
        try {
            if (mTGMediaView.Z != null) {
                mTGMediaView.Z.onFinishRedirection(campaign, str);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    static /* synthetic */ void M(MTGMediaView mTGMediaView) {
        if (mTGMediaView.K != null) {
            com.mintegral.msdk.base.entity.CampaignEx.b mediaViewHolder = mTGMediaView.K.getMediaViewHolder();
            if (mediaViewHolder != null && !mediaViewHolder.b && mTGMediaView.K.getNativeVideoTracking() != null && mTGMediaView.K.getNativeVideoTracking().h() != null) {
                mediaViewHolder.b = true;
                com.mintegral.msdk.click.a.a(mTGMediaView.getContext(), mTGMediaView.K, mTGMediaView.K.getCampaignUnitId(), mTGMediaView.K.getNativeVideoTracking().h(), false);
            }
        }
    }

    static /* synthetic */ void N(MTGMediaView mTGMediaView) {
        if (mTGMediaView.K != null) {
            com.mintegral.msdk.base.entity.CampaignEx.b mediaViewHolder = mTGMediaView.K.getMediaViewHolder();
            if (mediaViewHolder != null && !mediaViewHolder.c && mTGMediaView.K.getNativeVideoTracking() != null && mTGMediaView.K.getNativeVideoTracking().i() != null) {
                mediaViewHolder.c = true;
                com.mintegral.msdk.click.a.a(mTGMediaView.getContext(), mTGMediaView.K, mTGMediaView.K.getCampaignUnitId(), mTGMediaView.K.getNativeVideoTracking().i(), false);
            }
        }
    }

    static /* synthetic */ void O(MTGMediaView mTGMediaView) {
        if (mTGMediaView.K != null) {
            com.mintegral.msdk.base.entity.CampaignEx.b mediaViewHolder = mTGMediaView.K.getMediaViewHolder();
            if (mediaViewHolder != null && !mediaViewHolder.e && mTGMediaView.K.getNativeVideoTracking() != null && mTGMediaView.K.getNativeVideoTracking().k() != null) {
                mediaViewHolder.e = true;
                com.mintegral.msdk.click.a.a(mTGMediaView.getContext(), mTGMediaView.K, mTGMediaView.K.getCampaignUnitId(), mTGMediaView.K.getNativeVideoTracking().k(), false);
            }
        }
    }

    static /* synthetic */ void P(MTGMediaView mTGMediaView) {
        if (mTGMediaView.K != null) {
            com.mintegral.msdk.base.entity.CampaignEx.b mediaViewHolder = mTGMediaView.K.getMediaViewHolder();
            if (mediaViewHolder != null && !mediaViewHolder.f && mTGMediaView.K.getNativeVideoTracking() != null && mTGMediaView.K.getNativeVideoTracking().l() != null) {
                mediaViewHolder.f = true;
                com.mintegral.msdk.click.a.a(mTGMediaView.getContext(), mTGMediaView.K, mTGMediaView.K.getCampaignUnitId(), mTGMediaView.K.getNativeVideoTracking().l(), false);
            }
        }
    }
}
