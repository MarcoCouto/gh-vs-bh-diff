package com.mintegral.msdk.nativex.listener;

import android.content.Context;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.base.common.d.b;
import com.mintegral.msdk.base.common.d.c;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.out.Frame;
import com.mintegral.msdk.out.NativeListener.FilpListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RollingPagerListenrt implements OnPageChangeListener {

    /* renamed from: a reason: collision with root package name */
    private List<Frame> f2804a;
    private b b;
    private String c;
    private List<Integer> d = new ArrayList();
    private FilpListener e;

    public void onPageScrollStateChanged(int i) {
    }

    public void onPageScrolled(int i, float f, int i2) {
    }

    public final void a(FilpListener filpListener) {
        this.e = filpListener;
    }

    public final void a(List<Frame> list, Context context, String str) {
        this.f2804a = list;
        this.b = new b(context, 2);
        this.c = str;
        this.d.clear();
        a(0);
    }

    public void onPageSelected(int i) {
        a(i);
    }

    public final void a(int i) {
        if (this.e != null) {
            this.e.filpEvent(i);
        }
        if (!this.d.contains(Integer.valueOf(i))) {
            this.d.add(Integer.valueOf(i));
            Frame frame = (Frame) this.f2804a.get(i);
            List campaigns = frame.getCampaigns();
            if (campaigns != null && !campaigns.isEmpty()) {
                CampaignEx campaignEx = (CampaignEx) campaigns.get(0);
                StringBuilder sb = new StringBuilder();
                for (int i2 = 0; i2 < campaigns.size(); i2++) {
                    campaignEx = (CampaignEx) campaigns.get(i2);
                    if (i2 == campaigns.size() - 1) {
                        sb.append(campaignEx.getId());
                    } else {
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append(campaignEx.getId());
                        sb2.append(",");
                        sb.append(sb2.toString());
                    }
                }
                HashMap hashMap = new HashMap();
                hashMap.put("rid_n", campaignEx.getRequestId());
                StringBuilder sb3 = new StringBuilder();
                sb3.append(i + 1);
                hashMap.put("frame_id", sb3.toString());
                hashMap.put("template", Integer.valueOf(frame.getTemplate()));
                hashMap.put("cids", sb.toString());
                hashMap.put(MIntegralConstans.PROPERTIES_UNIT_ID, this.c);
                this.b.a("native_rollbc", c.a("2000005", (Map<String, Object>) hashMap), this.c, frame);
            }
        }
    }
}
