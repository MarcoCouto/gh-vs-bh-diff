package com.mintegral.msdk.interstitialvideo.a;

import com.mintegral.msdk.interstitialvideo.out.InterstitialVideoListener;
import com.mintegral.msdk.videocommon.listener.InterVideoOutListener;

/* compiled from: DecoratorInterstitialListener */
public final class a implements InterVideoOutListener {

    /* renamed from: a reason: collision with root package name */
    private InterstitialVideoListener f2702a;

    public a(InterstitialVideoListener interstitialVideoListener) {
        this.f2702a = interstitialVideoListener;
    }

    public final void onAdShow() {
        if (this.f2702a != null) {
            this.f2702a.onAdShow();
        }
    }

    public final void onAdClose(boolean z, String str, float f) {
        if (this.f2702a != null) {
            this.f2702a.onAdClose(z);
        }
    }

    public final void onShowFail(String str) {
        if (this.f2702a != null) {
            this.f2702a.onShowFail(str);
        }
    }

    public final void onVideoAdClicked(String str) {
        if (this.f2702a != null) {
            this.f2702a.onVideoAdClicked(str);
        }
    }

    public final void onVideoComplete(String str) {
        if (this.f2702a != null) {
            this.f2702a.onVideoComplete(str);
        }
    }

    public final void onEndcardShow(String str) {
        if (this.f2702a != null) {
            this.f2702a.onEndcardShow(str);
        }
    }

    public final void onVideoLoadFail(String str) {
        if (this.f2702a != null) {
            this.f2702a.onVideoLoadFail(str);
        }
    }

    public final void onVideoLoadSuccess(String str) {
        if (this.f2702a != null) {
            this.f2702a.onVideoLoadSuccess(str);
        }
    }

    public final void onLoadSuccess(String str) {
        if (this.f2702a != null) {
            this.f2702a.onLoadSuccess(str);
        }
    }
}
