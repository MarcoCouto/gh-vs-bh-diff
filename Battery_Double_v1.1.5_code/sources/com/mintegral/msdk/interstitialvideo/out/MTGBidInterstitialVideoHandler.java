package com.mintegral.msdk.interstitialvideo.out;

import android.content.Context;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.reward.b.a;
import com.mintegral.msdk.videocommon.listener.InterVideoOutListener;

public class MTGBidInterstitialVideoHandler {

    /* renamed from: a reason: collision with root package name */
    private a f2703a;

    public MTGBidInterstitialVideoHandler(Context context, String str) {
        if (com.mintegral.msdk.base.controller.a.d().h() == null && context != null) {
            com.mintegral.msdk.base.controller.a.d().a(context);
        }
        a(str);
    }

    public MTGBidInterstitialVideoHandler(String str) {
        a(str);
    }

    private void a(String str) {
        try {
            if (this.f2703a == null) {
                this.f2703a = new a();
                this.f2703a.a(true);
                this.f2703a.a();
            }
            this.f2703a.b(str);
        } catch (Throwable th) {
            g.c("MTGBidRewardVideoHandler", th.getMessage(), th);
        }
    }

    public void loadFromBid(String str) {
        if (this.f2703a != null) {
            this.f2703a.a(true, str);
        }
    }

    public void loadFormSelfFilling() {
        if (this.f2703a != null) {
            this.f2703a.b(false);
        }
    }

    public boolean isBidReady() {
        if (this.f2703a != null) {
            return this.f2703a.c(true);
        }
        return false;
    }

    public void showFromBid() {
        if (this.f2703a != null) {
            this.f2703a.a((String) null, (String) null);
        }
    }

    public void setRewardVideoListener(InterstitialVideoListener interstitialVideoListener) {
        if (this.f2703a != null) {
            this.f2703a.a((InterVideoOutListener) new com.mintegral.msdk.interstitialvideo.a.a(interstitialVideoListener));
        }
    }

    public void setInterstitialVideoListener(InterstitialVideoListener interstitialVideoListener) {
        if (this.f2703a != null) {
            this.f2703a.a((InterVideoOutListener) new com.mintegral.msdk.interstitialvideo.a.a(interstitialVideoListener));
        }
    }

    public void clearVideoCache() {
        try {
            if (this.f2703a != null) {
                a.b();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void playVideoMute(int i) {
        if (this.f2703a != null) {
            this.f2703a.a(i);
        }
    }
}
