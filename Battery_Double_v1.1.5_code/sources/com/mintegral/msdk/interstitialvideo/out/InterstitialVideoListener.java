package com.mintegral.msdk.interstitialvideo.out;

public interface InterstitialVideoListener {
    void onAdClose(boolean z);

    void onAdShow();

    void onEndcardShow(String str);

    void onLoadSuccess(String str);

    void onShowFail(String str);

    void onVideoAdClicked(String str);

    void onVideoComplete(String str);

    void onVideoLoadFail(String str);

    void onVideoLoadSuccess(String str);
}
