package com.mintegral.msdk.interstitialvideo.out;

import android.content.Context;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.reward.b.a;
import com.mintegral.msdk.videocommon.listener.InterVideoOutListener;

public class MTGInterstitialVideoHandler {

    /* renamed from: a reason: collision with root package name */
    private a f2704a;

    public MTGInterstitialVideoHandler(Context context, String str) {
        if (com.mintegral.msdk.base.controller.a.d().h() == null && context != null) {
            com.mintegral.msdk.base.controller.a.d().a(context);
        }
        a(str);
    }

    public MTGInterstitialVideoHandler(String str) {
        a(str);
    }

    private void a(String str) {
        try {
            if (this.f2704a == null) {
                this.f2704a = new a();
                this.f2704a.a(true);
            }
            this.f2704a.b(str);
        } catch (Throwable th) {
            g.c("MTGRewardVideoHandler", th.getMessage(), th);
        }
    }

    public void load() {
        if (this.f2704a != null) {
            this.f2704a.b(true);
        }
    }

    public void loadFormSelfFilling() {
        if (this.f2704a != null) {
            this.f2704a.b(false);
        }
    }

    public boolean isReady() {
        if (this.f2704a != null) {
            return this.f2704a.c(true);
        }
        return false;
    }

    public void show() {
        if (this.f2704a != null) {
            this.f2704a.a((String) null, (String) null);
        }
    }

    public void setRewardVideoListener(InterstitialVideoListener interstitialVideoListener) {
        if (this.f2704a != null) {
            this.f2704a.a((InterVideoOutListener) new com.mintegral.msdk.interstitialvideo.a.a(interstitialVideoListener));
        }
    }

    public void setInterstitialVideoListener(InterstitialVideoListener interstitialVideoListener) {
        if (this.f2704a != null) {
            this.f2704a.a((InterVideoOutListener) new com.mintegral.msdk.interstitialvideo.a.a(interstitialVideoListener));
        }
    }

    public void clearVideoCache() {
        try {
            if (this.f2704a != null) {
                a.b();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void playVideoMute(int i) {
        if (this.f2704a != null) {
            this.f2704a.a(i);
        }
    }
}
