package com.mintegral.msdk.mtgdownload;

import android.content.Context;
import com.mintegral.msdk.MIntegralConstans;

/* compiled from: MTGDownloadAgent */
public class g extends b {
    public g(Context context, String str) {
        super(context, "mtg", str);
        if (MIntegralConstans.RICH_NOTIFICATION) {
            setRichNotification(true);
        } else {
            setRichNotification(false);
        }
        if (MIntegralConstans.SLIENT_DOWNLOAD) {
            setSilentDownload(true);
        } else {
            setSilentDownload(false);
        }
        setDownloadClz("com.mintegral.msdk.shell.MTGService");
    }

    public g(Context context, String str, String str2, boolean z) {
        super(context, str, str2);
        setRichNotification(z);
        setDownloadClz("com.mintegral.msdk.shell.MTGService");
    }
}
