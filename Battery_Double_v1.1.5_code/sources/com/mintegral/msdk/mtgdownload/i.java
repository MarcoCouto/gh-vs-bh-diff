package com.mintegral.msdk.mtgdownload;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.mintegral.msdk.base.utils.g;
import java.util.ArrayList;
import java.util.List;

/* compiled from: NotificationRuntimeCache */
public class i {

    /* renamed from: a reason: collision with root package name */
    private static final String f2721a = "com.mintegral.msdk.mtgdownload.i";
    private final Context b;

    public i(Context context) {
        this.b = context;
    }

    public final void a(int i) {
        try {
            SharedPreferences sharedPreferences = this.b.getSharedPreferences("MTG_RUNTIME_CACHE", 0);
            Editor edit = sharedPreferences.edit();
            synchronized (sharedPreferences) {
                edit.putString(String.valueOf(i), "");
                edit.apply();
            }
            String str = f2721a;
            StringBuilder sb = new StringBuilder("add nid [");
            sb.append(i);
            sb.append("] to runtime cache.");
            g.b(str, sb.toString());
        } catch (Exception unused) {
        }
    }

    public final List<Integer> a() {
        ArrayList arrayList = new ArrayList();
        try {
            SharedPreferences sharedPreferences = this.b.getSharedPreferences("MTG_RUNTIME_CACHE", 0);
            for (String parseInt : sharedPreferences.getAll().keySet()) {
                try {
                    int parseInt2 = Integer.parseInt(parseInt);
                    arrayList.add(Integer.valueOf(parseInt2));
                    String str = f2721a;
                    StringBuilder sb = new StringBuilder("get nid [");
                    sb.append(parseInt2);
                    sb.append(RequestParameters.RIGHT_BRACKETS);
                    g.b(str, sb.toString());
                } catch (NumberFormatException unused) {
                }
            }
            sharedPreferences.edit().clear().apply();
        } catch (Exception unused2) {
        }
        return arrayList;
    }

    public final boolean b() {
        if (this.b.getSharedPreferences("MTG_RUNTIME_CACHE", 0).getAll().size() > 0) {
            return true;
        }
        return false;
    }

    public final void b(int i) {
        try {
            SharedPreferences sharedPreferences = this.b.getSharedPreferences("MTG_RUNTIME_CACHE", 0);
            if (sharedPreferences.contains(String.valueOf(i))) {
                Editor edit = sharedPreferences.edit();
                edit.remove(String.valueOf(i));
                edit.apply();
            }
            String str = f2721a;
            StringBuilder sb = new StringBuilder("remove nid [");
            sb.append(i);
            sb.append("] to runtime cache.");
            g.b(str, sb.toString());
        } catch (Exception unused) {
        }
    }
}
