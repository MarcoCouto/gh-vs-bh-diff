package com.mintegral.msdk.mtgdownload;

import android.content.Context;

/* compiled from: DeltaUpdate */
public final class a {

    /* renamed from: a reason: collision with root package name */
    private static boolean f2705a;

    public static String a(Context context) {
        return context.getApplicationInfo().sourceDir;
    }

    static {
        try {
            System.loadLibrary("bspatch");
            f2705a = true;
        } catch (UnsatisfiedLinkError unused) {
            f2705a = false;
        }
    }
}
