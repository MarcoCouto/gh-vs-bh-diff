package com.mintegral.msdk.mtgdownload;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.out.IDownloadListener;

/* compiled from: DownloadAgent */
public class b {
    /* access modifiers changed from: private */
    public static final String b = "com.mintegral.msdk.mtgdownload.b";

    /* renamed from: a reason: collision with root package name */
    final Messenger f2706a = new Messenger(new C0057b());
    /* access modifiers changed from: private */
    public Context c;
    /* access modifiers changed from: private */
    public IDownloadListener d;
    /* access modifiers changed from: private */
    public Messenger e;
    /* access modifiers changed from: private */
    public String f = "none";
    /* access modifiers changed from: private */
    public String g = "";
    /* access modifiers changed from: private */
    public String h;
    /* access modifiers changed from: private */
    public String i;
    /* access modifiers changed from: private */
    public String j;
    /* access modifiers changed from: private */
    public String k;
    private String l;
    /* access modifiers changed from: private */
    public String[] m;
    /* access modifiers changed from: private */
    public String[] n;
    /* access modifiers changed from: private */
    public String[] o;
    /* access modifiers changed from: private */
    public String[] p;
    /* access modifiers changed from: private */
    public String[] q;
    /* access modifiers changed from: private */
    public String[] r;
    /* access modifiers changed from: private */
    public boolean s = false;
    /* access modifiers changed from: private */
    public boolean t = false;
    /* access modifiers changed from: private */
    public boolean u = false;
    /* access modifiers changed from: private */
    public ServiceConnection v = new ServiceConnection() {
        public final void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            g.a(b.b, "ServiceConnection.onServiceConnected");
            b.this.e = new Messenger(iBinder);
            try {
                Message obtain = Message.obtain(null, 4);
                a aVar = new a(b.this.f, b.this.g, b.this.h);
                aVar.e = b.this.i;
                aVar.f = b.this.j;
                aVar.f2708a = b.this.k;
                aVar.g = b.this.m;
                aVar.i = b.this.q;
                aVar.j = b.this.n;
                aVar.k = b.this.o;
                aVar.l = b.this.p;
                aVar.h = b.this.r;
                aVar.m = b.this.s;
                aVar.n = b.this.t;
                aVar.o = b.this.u;
                Bundle bundle = new Bundle();
                bundle.putString("mComponentName", aVar.b);
                bundle.putString("mTitle", aVar.c);
                bundle.putString("mUrl", aVar.d);
                bundle.putString("mMd5", aVar.e);
                bundle.putString("mTargetMd5", aVar.f);
                bundle.putString("mReqClz", aVar.f2708a);
                bundle.putStringArray("succUrls", aVar.g);
                bundle.putStringArray("faiUrls", aVar.i);
                bundle.putStringArray("startUrls", aVar.j);
                bundle.putStringArray("pauseUrls", aVar.k);
                bundle.putStringArray("cancelUrls", aVar.l);
                bundle.putStringArray("carryonUrls", aVar.h);
                bundle.putBoolean("rich_notification", aVar.m);
                bundle.putBoolean("mSilent", aVar.n);
                bundle.putBoolean("mWifiOnly", aVar.o);
                obtain.setData(bundle);
                obtain.replyTo = b.this.f2706a;
                b.this.e.send(obtain);
            } catch (RemoteException unused) {
            }
        }

        public final void onServiceDisconnected(ComponentName componentName) {
            g.a(b.b, "ServiceConnection.onServiceDisconnected");
            b.this.e = null;
        }
    };

    /* compiled from: DownloadAgent */
    static class a {

        /* renamed from: a reason: collision with root package name */
        public String f2708a;
        public String b;
        public String c;
        public String d;
        public String e;
        public String f;
        public String[] g = null;
        public String[] h = null;
        public String[] i = null;
        public String[] j = null;
        public String[] k = null;
        public String[] l = null;
        public boolean m = false;
        public boolean n = false;
        public boolean o = false;

        public a(String str, String str2, String str3) {
            this.b = str;
            this.c = str2;
            this.d = str3;
        }
    }

    /* renamed from: com.mintegral.msdk.mtgdownload.b$b reason: collision with other inner class name */
    /* compiled from: DownloadAgent */
    class C0057b extends Handler {
        C0057b() {
        }

        public final void handleMessage(Message message) {
            try {
                String a2 = b.b;
                StringBuilder sb = new StringBuilder("DownloadAgent.handleMessage(");
                sb.append(message.what);
                sb.append("): ");
                g.a(a2, sb.toString());
                int i = message.what;
                if (i != 5) {
                    switch (i) {
                        case 1:
                            if (b.this.d != null) {
                                b.this.d.onStart();
                                return;
                            }
                            break;
                        case 2:
                            if (b.this.d != null) {
                                b.this.d.onStatus(message.arg1);
                                return;
                            }
                            break;
                        case 3:
                            if (b.this.d != null) {
                                b.this.d.onProgressUpdate(message.arg1);
                                return;
                            }
                            break;
                        default:
                            super.handleMessage(message);
                            break;
                    }
                } else {
                    b.this.c.unbindService(b.this.v);
                    if (b.this.d != null) {
                        if (!(message.arg1 == 1 || message.arg1 == 3)) {
                            if (message.arg1 != 5) {
                                b.this.d.onEnd(0, 0, null);
                                g.a(b.b, "DownloadAgent.handleMessage(DownloadingService.DOWNLOAD_COMPLETE_FAIL): ");
                                return;
                            }
                        }
                        b.this.d.onEnd(message.arg1, message.arg2, message.getData().getString("filename"));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                String a3 = b.b;
                StringBuilder sb2 = new StringBuilder("DownloadAgent.handleMessage(");
                sb2.append(message.what);
                sb2.append("): ");
                sb2.append(e.getMessage());
                g.a(a3, sb2.toString());
            }
        }
    }

    public void setMd5(String str) {
        this.i = str;
    }

    public void setTargetMd5(String str) {
        this.j = str;
    }

    public void setReportClz(String str) {
        this.k = str;
    }

    public void setSuccUrls(String... strArr) {
        this.m = strArr;
    }

    public void setFaiUrls(String... strArr) {
        this.q = strArr;
    }

    public void setStartUrls(String... strArr) {
        this.n = strArr;
    }

    public void setPauseUrls(String... strArr) {
        this.o = strArr;
    }

    public void setCancelUrls(String... strArr) {
        this.p = strArr;
    }

    public void setCarryOnUrls(String... strArr) {
        this.r = strArr;
    }

    public void setRichNotification(boolean z) {
        this.s = z;
    }

    public void setSilentDownload(boolean z) {
        this.t = z;
    }

    public void setWifiOnly(boolean z) {
        this.u = z;
    }

    public b(Context context, String str, String str2) {
        this.c = context.getApplicationContext();
        this.f = str;
        this.h = str2;
    }

    public b setTitle(String str) {
        this.g = str;
        return this;
    }

    public void setDownloadListener(IDownloadListener iDownloadListener) {
        this.d = iDownloadListener;
    }

    public void setDownloadClz(String str) {
        this.l = str;
    }

    public void start() {
        if (this.l != null) {
            try {
                Class cls = Class.forName(this.l);
                this.c.bindService(new Intent(this.c, cls), this.v, 1);
                this.c.startService(new Intent(this.c, cls));
            } catch (ClassNotFoundException e2) {
                throw new IllegalArgumentException(e2);
            }
        } else {
            throw new IllegalArgumentException("cannot find MTGService");
        }
    }
}
