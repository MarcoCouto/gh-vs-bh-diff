package com.mintegral.msdk.mtgdownload;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.widget.Toast;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.base.utils.g;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/* compiled from: WorkThread */
class k extends Thread {

    /* renamed from: a reason: collision with root package name */
    private static final String f2722a = "k";
    /* access modifiers changed from: private */
    public static Boolean n = Boolean.valueOf(false);
    private c b;
    /* access modifiers changed from: private */
    public Context c;
    private boolean d;
    private File e;
    private int f = 0;
    private long g = -1;
    private long h = -1;
    private int i = -1;
    private int j;
    private Handler k;
    private a l;
    private NotificationManager m;

    public k(c cVar, a aVar, int i2, int i3) {
        String str;
        this.b = cVar;
        this.c = cVar.e().getApplicationContext();
        this.l = aVar;
        this.f = i3;
        this.m = (NotificationManager) cVar.e().getSystemService("notification");
        j.a(this.m);
        this.k = new Handler(this.b.e().getMainLooper());
        try {
            if (c.d.indexOfKey(i2) >= 0) {
                long[] jArr = ((b) c.d.get(i2)).f;
                if (jArr != null && jArr.length > 1) {
                    this.g = jArr[0];
                    this.h = jArr[1];
                }
            }
            this.j = i2;
            boolean[] zArr = new boolean[1];
            this.e = e.a("/apk", this.c, zArr);
            this.d = zArr[0];
            a aVar2 = this.l;
            if (aVar2.f != null) {
                StringBuilder sb = new StringBuilder();
                sb.append(aVar2.f);
                sb.append(".apk.tmp");
                str = sb.toString();
            } else {
                StringBuilder sb2 = new StringBuilder();
                sb2.append(e.a(aVar2.d));
                sb2.append(".apk.tmp");
                str = sb2.toString();
            }
            if (aVar2.b.equalsIgnoreCase("delta_update")) {
                str = str.replace(".apk", ".patch");
            }
            this.e = new File(this.e, str);
        } catch (Exception e2) {
            g.c(f2722a, e2.getMessage(), e2);
            this.b.b(this.j);
        }
    }

    public void run() {
        boolean z = false;
        this.f = 0;
        try {
            if (this.b != null) {
                this.b.a(this.j);
            }
            if (this.g > 0) {
                z = true;
            }
            a(z);
            if (c.c.size() <= 0) {
                this.b.e().stopSelf();
            }
        } catch (Exception e2) {
            if (MIntegralConstans.DEBUG) {
                e2.printStackTrace();
            }
        } catch (Throwable th) {
            if (MIntegralConstans.DEBUG) {
                th.printStackTrace();
            }
        }
    }

    public final void a(int i2) {
        this.i = i2;
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(2:73|74) */
    /* JADX WARNING: Code restructure failed: missing block: B:161:?, code lost:
        r1.b.d().a(r1.c, r1.j);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:?, code lost:
        com.mintegral.msdk.base.utils.g.d(f2722a, java.lang.String.format("Service Client for downloading %1$15s is dead. Removing messenger from the service", new java.lang.Object[]{r1.l.c}));
        com.mintegral.msdk.mtgdownload.c.c.put(r1.l, null);
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* JADX WARNING: Missing exception handler attribute for start block: B:160:0x03fb */
    /* JADX WARNING: Missing exception handler attribute for start block: B:205:0x049b */
    /* JADX WARNING: Missing exception handler attribute for start block: B:27:0x00f1 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:73:0x0226 */
    /* JADX WARNING: Removed duplicated region for block: B:208:0x04b1 A[SYNTHETIC, Splitter:B:208:0x04b1] */
    /* JADX WARNING: Removed duplicated region for block: B:230:0x04d8 A[SYNTHETIC, Splitter:B:230:0x04d8] */
    /* JADX WARNING: Removed duplicated region for block: B:243:0x04fa A[Catch:{ all -> 0x0535, all -> 0x0675 }] */
    /* JADX WARNING: Removed duplicated region for block: B:264:0x058b A[Catch:{ all -> 0x062b, InterruptedException -> 0x062f }] */
    /* JADX WARNING: Removed duplicated region for block: B:268:0x05a2 A[Catch:{ all -> 0x062b, InterruptedException -> 0x062f }] */
    /* JADX WARNING: Removed duplicated region for block: B:288:0x0643 A[SYNTHETIC, Splitter:B:288:0x0643] */
    /* JADX WARNING: Removed duplicated region for block: B:310:0x066a A[SYNTHETIC, Splitter:B:310:0x066a] */
    /* JADX WARNING: Removed duplicated region for block: B:319:0x0679 A[SYNTHETIC, Splitter:B:319:0x0679] */
    /* JADX WARNING: Removed duplicated region for block: B:337:0x0699 A[SYNTHETIC, Splitter:B:337:0x0699] */
    private void a(boolean z) {
        InputStream inputStream;
        FileOutputStream fileOutputStream;
        InputStream inputStream2;
        Throwable th;
        Exception exc;
        int i2;
        Handler handler;
        AnonymousClass1 r3;
        boolean z2;
        int i3;
        String name = this.e.getName();
        inputStream = null;
        try {
            fileOutputStream = new FileOutputStream(this.e, true);
            if (!this.d && !e.b(this.e.getAbsolutePath())) {
                fileOutputStream.close();
                FileOutputStream openFileOutput = this.c.openFileOutput(name, 32771);
                try {
                    this.e = this.c.getFileStreamPath(name);
                    fileOutputStream = openFileOutput;
                } catch (IOException e2) {
                    exc = e2;
                    inputStream2 = null;
                    fileOutputStream = openFileOutput;
                    try {
                        g.d(f2722a, "下载发生错误。");
                        i2 = this.f + 1;
                        this.f = i2;
                        if (i2 > 3) {
                        }
                        String str = f2722a;
                        StringBuilder sb = new StringBuilder("wait for repeating Test network repeat count=");
                        sb.append(this.f);
                        g.a(str, sb.toString());
                        try {
                            if (this.l.m) {
                            }
                        } catch (InterruptedException e3) {
                            a((Exception) e3);
                            this.b.d().a(this.c, this.j);
                        } catch (Throwable th2) {
                            while (true) {
                            }
                            throw th2;
                        }
                        if (inputStream2 != null) {
                        }
                        if (fileOutputStream != null) {
                        }
                    } catch (Throwable th3) {
                        th = th3;
                        if (inputStream2 != null) {
                            try {
                                inputStream2.close();
                            } catch (IOException e4) {
                                e4.printStackTrace();
                                if (fileOutputStream != null) {
                                    try {
                                        fileOutputStream.close();
                                    } catch (IOException e5) {
                                        e5.printStackTrace();
                                    }
                                }
                                throw th;
                            } catch (Throwable th4) {
                                Throwable th5 = th4;
                                if (fileOutputStream != null) {
                                    try {
                                        fileOutputStream.close();
                                    } catch (IOException e6) {
                                        e6.printStackTrace();
                                    }
                                }
                                throw th5;
                            }
                        }
                        if (fileOutputStream != null) {
                            fileOutputStream.close();
                        }
                        throw th;
                    }
                } catch (Exception unused) {
                    fileOutputStream = openFileOutput;
                    try {
                        this.b.d().a(this.c, this.j);
                        g.d(f2722a, "Download failed.");
                        if (inputStream != null) {
                        }
                        if (fileOutputStream != null) {
                        }
                    } catch (Throwable th6) {
                        th = th6;
                        inputStream2 = inputStream;
                        if (inputStream2 != null) {
                        }
                        if (fileOutputStream != null) {
                        }
                        throw th;
                    }
                } catch (Throwable th7) {
                    th = th7;
                    inputStream2 = null;
                    fileOutputStream = openFileOutput;
                    if (inputStream2 != null) {
                    }
                    if (fileOutputStream != null) {
                    }
                    throw th;
                }
            }
            try {
                g.a(f2722a, String.format("saveAPK: url = %1$15s\t|\tfilename = %2$15s", new Object[]{this.l.d, this.e.getAbsolutePath()}));
                URL url = new URL(this.l.d);
                File file = this.e;
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod(HttpRequest.METHOD_GET);
                httpURLConnection.setRequestProperty(HttpRequest.HEADER_ACCEPT_ENCODING, "identity");
                httpURLConnection.addRequestProperty("Connection", "keep-alive");
                httpURLConnection.setConnectTimeout(5000);
                httpURLConnection.setReadTimeout(10000);
                long j2 = 0;
                if (file.exists() && file.length() > 0) {
                    String str2 = f2722a;
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append(this.l.c);
                    sb2.append(" getFileLength: %1$15s");
                    g.a(str2, String.format(sb2.toString(), new Object[]{Long.valueOf(file.length())}));
                    StringBuilder sb3 = new StringBuilder("bytes=");
                    sb3.append(file.length());
                    sb3.append("-");
                    httpURLConnection.setRequestProperty("Range", sb3.toString());
                }
                httpURLConnection.setRequestMethod(HttpRequest.METHOD_GET);
                httpURLConnection.connect();
                this.e.delete();
                inputStream2 = httpURLConnection.getInputStream();
                if (!z) {
                    try {
                        long length = (!this.e.exists() || this.e.length() <= 0) ? 0 : this.e.length() + 0;
                        this.g = length;
                        this.h = length + ((long) httpURLConnection.getContentLength());
                        g.a(f2722a, String.format("getFileLength: %1$15s", new Object[]{Long.valueOf(this.g)}));
                        g.a(f2722a, String.format("getConnectionLength: %1$15s", new Object[]{Integer.valueOf(httpURLConnection.getContentLength())}));
                        g.a(f2722a, String.format("getContentLength: %1$15s", new Object[]{Long.valueOf(this.h)}));
                    } catch (IOException e7) {
                        exc = e7;
                        g.d(f2722a, "下载发生错误。");
                        i2 = this.f + 1;
                        this.f = i2;
                        if (i2 > 3 || this.l.m) {
                            String str3 = f2722a;
                            StringBuilder sb4 = new StringBuilder("wait for repeating Test network repeat count=");
                            sb4.append(this.f);
                            g.a(str3, sb4.toString());
                            if (this.l.m) {
                                Thread.sleep(8000);
                                if (this.h < 1) {
                                    a(false);
                                } else {
                                    a(true);
                                }
                            } else {
                                b bVar = (b) c.d.get(this.j);
                                bVar.f[0] = this.g;
                                bVar.f[1] = this.h;
                                bVar.f[2] = (long) this.f;
                                String a2 = j.a(this.j, "continue");
                                Intent intent = new Intent(this.c, c.class);
                                intent.putExtra("com.mintegral.msdk.broadcast.download.msg", a2);
                                this.b.d().a(this.b, intent);
                                final String str4 = f.n;
                                synchronized (n) {
                                    if (!n.booleanValue()) {
                                        String str5 = f2722a;
                                        StringBuilder sb5 = new StringBuilder("show single toast.[");
                                        sb5.append(str4);
                                        sb5.append(RequestParameters.RIGHT_BRACKETS);
                                        g.a(str5, sb5.toString());
                                        n = Boolean.valueOf(true);
                                        this.k.post(new Runnable() {
                                            public final void run() {
                                                Toast.makeText(k.this.c, str4, 0).show();
                                            }
                                        });
                                        this.k.postDelayed(new Runnable() {
                                            public final void run() {
                                                k.n = Boolean.valueOf(false);
                                            }
                                        }, 1200);
                                    }
                                }
                                g.a(f2722a, "changed play state button on op-notification.");
                            }
                        } else {
                            try {
                                g.a(f2722a, "Download Fail out of max repeat count");
                                ((Messenger) c.c.get(this.l)).send(Message.obtain(null, 5, 0, 0));
                                this.b.d().a(this.c, this.j);
                                a(exc);
                                handler = this.k;
                                r3 = new Runnable() {
                                    public final void run() {
                                        Toast.makeText(k.this.c, f.m, 0).show();
                                    }
                                };
                            } catch (RemoteException e8) {
                                e8.printStackTrace();
                                this.b.d().a(this.c, this.j);
                                a(exc);
                                handler = this.k;
                                r3 = new Runnable() {
                                    public final void run() {
                                        Toast.makeText(k.this.c, f.m, 0).show();
                                    }
                                };
                                handler.post(r3);
                                if (inputStream2 != null) {
                                }
                                if (fileOutputStream != null) {
                                }
                            }
                            handler.post(r3);
                        }
                        if (inputStream2 != null) {
                            try {
                                inputStream2.close();
                            } catch (IOException e9) {
                                e9.printStackTrace();
                                if (fileOutputStream != null) {
                                    try {
                                        fileOutputStream.close();
                                    } catch (IOException e10) {
                                        e10.printStackTrace();
                                        return;
                                    }
                                }
                                return;
                            } catch (Throwable th8) {
                                Throwable th9 = th8;
                                if (fileOutputStream != null) {
                                    try {
                                        fileOutputStream.close();
                                    } catch (IOException e11) {
                                        e11.printStackTrace();
                                    }
                                }
                                throw th9;
                            }
                        }
                        if (fileOutputStream != null) {
                            try {
                                fileOutputStream.close();
                            } catch (IOException e12) {
                                e12.printStackTrace();
                                return;
                            }
                        }
                    } catch (Exception unused2) {
                        inputStream = inputStream2;
                        this.b.d().a(this.c, this.j);
                        g.d(f2722a, "Download failed.");
                        if (inputStream != null) {
                            try {
                                inputStream.close();
                            } catch (IOException e13) {
                                e13.printStackTrace();
                                if (fileOutputStream != null) {
                                    try {
                                        fileOutputStream.close();
                                    } catch (IOException e14) {
                                        e14.printStackTrace();
                                        return;
                                    }
                                }
                                return;
                            } catch (Throwable th10) {
                                Throwable th11 = th10;
                                if (fileOutputStream != null) {
                                    try {
                                        fileOutputStream.close();
                                    } catch (IOException e15) {
                                        e15.printStackTrace();
                                    }
                                }
                                throw th11;
                            }
                        }
                        if (fileOutputStream != null) {
                            try {
                                fileOutputStream.close();
                            } catch (IOException e16) {
                                e16.printStackTrace();
                                return;
                            }
                        }
                    }
                }
                byte[] bArr = new byte[4096];
                String str6 = f2722a;
                StringBuilder sb6 = new StringBuilder();
                sb6.append(this.l.c);
                sb6.append("saveAPK getContentLength ");
                sb6.append(String.valueOf(this.h));
                g.a(str6, sb6.toString());
                d.a(this.c).a(this.l.b, this.l.d);
                int i4 = 0;
                while (true) {
                    if (this.i >= 0) {
                        break;
                    }
                    int read = inputStream2.read(bArr);
                    if (read <= 0) {
                        break;
                    }
                    fileOutputStream.write(bArr, 0, read);
                    this.g += (long) read;
                    int i5 = i4 + 1;
                    if (i4 % 50 == 0) {
                        if (!e.b(this.c)) {
                            z2 = false;
                            break;
                        }
                        if (!e.c(this.c)) {
                            if (this.l.o) {
                                g.d(f2722a, "no wifi");
                                throw new IOException("no wifi");
                            }
                        }
                        if (this.h != j2) {
                            i3 = (int) ((((float) this.g) * 100.0f) / ((float) this.h));
                            if (i3 < 0 || i3 > 100) {
                                i3 = 99;
                            }
                        } else {
                            i3 = 0;
                        }
                        if (this.b != null) {
                            this.b.a(this.j, i3);
                        }
                        if (c.c.get(this.l) != null) {
                            ((Messenger) c.c.get(this.l)).send(Message.obtain(null, 3, i3, 0));
                        }
                        d.a(this.c).a(this.l.b, this.l.d, i3);
                    }
                    i4 = i5;
                    j2 = 0;
                }
                z2 = true;
                inputStream2.close();
                fileOutputStream.close();
                e d2 = this.b.d();
                if (this.i == 1) {
                    b bVar2 = (b) c.d.get(this.j);
                    if (bVar2 != null) {
                        bVar2.f[0] = this.g;
                        bVar2.f[1] = this.h;
                        bVar2.f[2] = (long) this.f;
                    }
                    if (inputStream2 != null) {
                        try {
                            inputStream2.close();
                        } catch (IOException e17) {
                            e17.printStackTrace();
                            if (fileOutputStream != null) {
                                try {
                                    fileOutputStream.close();
                                } catch (IOException e18) {
                                    e18.printStackTrace();
                                    return;
                                }
                            }
                            return;
                        } catch (Throwable th12) {
                            Throwable th13 = th12;
                            if (fileOutputStream != null) {
                                try {
                                    fileOutputStream.close();
                                } catch (IOException e19) {
                                    e19.printStackTrace();
                                }
                            }
                            throw th13;
                        }
                    }
                    if (fileOutputStream != null) {
                        try {
                            fileOutputStream.close();
                        } catch (IOException e20) {
                            e20.printStackTrace();
                        }
                    }
                } else if (this.i == 2) {
                    this.m.cancel(this.j);
                    if (inputStream2 != null) {
                        try {
                            inputStream2.close();
                        } catch (IOException e21) {
                            e21.printStackTrace();
                            if (fileOutputStream != null) {
                                try {
                                    fileOutputStream.close();
                                } catch (IOException e22) {
                                    e22.printStackTrace();
                                    return;
                                }
                            }
                            return;
                        } catch (Throwable th14) {
                            Throwable th15 = th14;
                            if (fileOutputStream != null) {
                                try {
                                    fileOutputStream.close();
                                } catch (IOException e23) {
                                    e23.printStackTrace();
                                }
                            }
                            throw th15;
                        }
                    }
                    if (fileOutputStream != null) {
                        try {
                            fileOutputStream.close();
                        } catch (IOException e24) {
                            e24.printStackTrace();
                        }
                    }
                } else {
                    if (!z2) {
                        String str7 = f2722a;
                        StringBuilder sb7 = new StringBuilder("Download Fail repeat count=");
                        sb7.append(this.f);
                        g.d(str7, sb7.toString());
                        ((Messenger) c.c.get(this.l)).send(Message.obtain(null, 5, 0, 0));
                        d2.a(this.c, this.j);
                        StringBuilder sb8 = new StringBuilder("Download Fail repeat count=");
                        sb8.append(this.f);
                        a(new Exception(sb8.toString()));
                    } else {
                        File file2 = new File(this.e.getParent(), this.e.getName().replace(".tmp", ""));
                        this.e.renameTo(file2);
                        String absolutePath = file2.getAbsolutePath();
                        String str8 = f2722a;
                        StringBuilder sb9 = new StringBuilder("itemMd5 ");
                        sb9.append(this.l.e);
                        g.a(str8, sb9.toString());
                        String str9 = f2722a;
                        StringBuilder sb10 = new StringBuilder("fileMd5 ");
                        sb10.append(e.a(file2));
                        g.a(str9, sb10.toString());
                        if (this.l.e != null && !this.l.e.equalsIgnoreCase(e.a(file2))) {
                            if (this.l.b.equalsIgnoreCase("delta_update")) {
                                this.m.cancel(this.j);
                                Bundle bundle = new Bundle();
                                bundle.putString("filename", absolutePath);
                                Message obtain = Message.obtain();
                                obtain.what = 5;
                                obtain.arg1 = 3;
                                obtain.arg2 = this.j;
                                obtain.setData(bundle);
                                if (c.c.get(this.l) != null) {
                                    ((Messenger) c.c.get(this.l)).send(obtain);
                                }
                                this.b.d().a(this.c, this.j);
                            } else {
                                ((Messenger) c.c.get(this.l)).send(Message.obtain(null, 5, 0, 0));
                                if (!this.l.n) {
                                    this.b.d().a(this.c, this.j);
                                    Notification a3 = e.a(this.c, f.m, PendingIntent.getActivity(this.c, 0, new Intent(), 0));
                                    a3.flags |= 16;
                                    this.m.notify(this.j, a3);
                                }
                            }
                        }
                        if (this.b != null) {
                            this.b.a(this.j, absolutePath);
                        }
                    }
                    if (inputStream2 != null) {
                        try {
                            inputStream2.close();
                        } catch (IOException e25) {
                            e25.printStackTrace();
                            if (fileOutputStream != null) {
                                try {
                                    fileOutputStream.close();
                                } catch (IOException e26) {
                                    e26.printStackTrace();
                                    return;
                                }
                            }
                            return;
                        } catch (Throwable th16) {
                            Throwable th17 = th16;
                            if (fileOutputStream != null) {
                                try {
                                    fileOutputStream.close();
                                } catch (IOException e27) {
                                    e27.printStackTrace();
                                }
                            }
                            throw th17;
                        }
                    }
                    if (fileOutputStream != null) {
                        try {
                            fileOutputStream.close();
                        } catch (IOException e28) {
                            e28.printStackTrace();
                        }
                    }
                }
            } catch (IOException e29) {
                exc = e29;
                inputStream2 = null;
                g.d(f2722a, "下载发生错误。");
                i2 = this.f + 1;
                this.f = i2;
                if (i2 > 3) {
                }
                String str32 = f2722a;
                StringBuilder sb42 = new StringBuilder("wait for repeating Test network repeat count=");
                sb42.append(this.f);
                g.a(str32, sb42.toString());
                if (this.l.m) {
                }
                if (inputStream2 != null) {
                }
                if (fileOutputStream != null) {
                }
            } catch (Exception ) {
                this.b.d().a(this.c, this.j);
                g.d(f2722a, "Download failed.");
                if (inputStream != null) {
                }
                if (fileOutputStream != null) {
                }
            }
        } catch (IOException e30) {
            exc = e30;
            inputStream2 = null;
            fileOutputStream = null;
            g.d(f2722a, "下载发生错误。");
            i2 = this.f + 1;
            this.f = i2;
            if (i2 > 3) {
            }
            String str322 = f2722a;
            StringBuilder sb422 = new StringBuilder("wait for repeating Test network repeat count=");
            sb422.append(this.f);
            g.a(str322, sb422.toString());
            if (this.l.m) {
            }
            if (inputStream2 != null) {
            }
            if (fileOutputStream != null) {
            }
        } catch (Exception unused3) {
            fileOutputStream = null;
        } catch (Throwable th18) {
            th = th18;
            inputStream2 = null;
            fileOutputStream = null;
            if (inputStream2 != null) {
            }
            if (fileOutputStream != null) {
            }
            throw th;
        }
    }

    private void a(Exception exc) {
        String str = f2722a;
        StringBuilder sb = new StringBuilder("can not install. ");
        sb.append(exc.getMessage());
        g.d(str, sb.toString());
        if (this.b != null) {
            this.b.b(this.j);
        }
    }
}
