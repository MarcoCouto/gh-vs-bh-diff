package com.mintegral.msdk.mtgdownload;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Debug;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.os.SystemClock;
import android.support.v4.app.NotificationCompat;
import android.util.SparseArray;
import android.widget.Toast;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.mintegral.msdk.base.utils.MTGFileProvider;
import com.mintegral.msdk.base.utils.g;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

/* compiled from: DownloadProvider */
public class c {

    /* renamed from: a reason: collision with root package name */
    public static final String f2710a = "c";
    public static boolean b = false;
    protected static Map<a, Messenger> c = new HashMap();
    protected static SparseArray<b> d = new SparseArray<>();
    final Messenger e = new Messenger(new b());
    SparseArray<Long> f = new SparseArray<>();
    /* access modifiers changed from: private */
    public NotificationManager g;
    /* access modifiers changed from: private */
    public e h;
    private i i;
    private Handler j;
    private boolean k = true;
    /* access modifiers changed from: private */
    public Service l;

    /* compiled from: DownloadProvider */
    class a extends Handler {
        a() {
        }

        /* JADX WARNING: Code restructure failed: missing block: B:16:?, code lost:
            r6.setDataAndType(android.net.Uri.fromFile(new java.io.File(r13)), "application/vnd.android.package-archive");
         */
        /* JADX WARNING: Code restructure failed: missing block: B:28:0x0190, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:15:0x00f7 */
        /* JADX WARNING: Removed duplicated region for block: B:19:0x0113 A[Catch:{ Exception -> 0x0191, Throwable -> 0x0190 }] */
        /* JADX WARNING: Removed duplicated region for block: B:20:0x0120 A[Catch:{ Exception -> 0x0191, Throwable -> 0x0190 }] */
        /* JADX WARNING: Removed duplicated region for block: B:29:? A[ExcHandler: Throwable (unused java.lang.Throwable), SYNTHETIC, Splitter:B:4:0x0088] */
        public final void handleMessage(Message message) {
            Notification notification;
            switch (message.what) {
                case 5:
                    a aVar = (a) message.obj;
                    int i = message.arg2;
                    try {
                        String string = message.getData().getString("filename");
                        g.a(c.f2710a, "Cancel old notification....");
                        Intent intent = new Intent("android.intent.action.VIEW");
                        intent.addFlags(268435456);
                        Context applicationContext = c.this.l.getApplicationContext();
                        if (com.mintegral.msdk.base.utils.c.k(applicationContext) < 24 || VERSION.SDK_INT < 24) {
                            intent.setDataAndType(Uri.fromFile(new File(string)), "application/vnd.android.package-archive");
                            PendingIntent activity = PendingIntent.getActivity(c.this.l, 0, intent, 134217728);
                            if (!aVar.n) {
                                notification = e.a((Context) c.this.l, f.k, activity);
                            } else {
                                notification = e.a((Context) c.this.l, f.j, activity);
                            }
                            notification.flags = 16;
                            int i2 = i + 1;
                            c.this.g.notify(i2, notification);
                            g.a(c.f2710a, "Show new  notification....");
                            c.this.h;
                            boolean a2 = e.a((Context) c.this.l);
                            g.a(c.f2710a, String.format("isAppOnForeground = %1$B", new Object[]{Boolean.valueOf(a2)}));
                            if (a2 && !aVar.n) {
                                c.this.g.cancel(i2);
                                c.this.l.startActivity(intent);
                            }
                            g.b(c.f2710a, String.format("%1$10s downloaded. Saved to: %2$s", new Object[]{aVar.c, string}));
                            return;
                        }
                        StringBuilder sb = new StringBuilder();
                        sb.append(applicationContext.getApplicationContext().getPackageName());
                        sb.append(".mtgFileProvider");
                        Uri uriForFile = MTGFileProvider.getUriForFile(applicationContext, sb.toString(), new File(string));
                        if (uriForFile != null) {
                            intent.setDataAndType(uriForFile, "application/vnd.android.package-archive");
                            intent.addFlags(1);
                        }
                        PendingIntent activity2 = PendingIntent.getActivity(c.this.l, 0, intent, 134217728);
                        if (!aVar.n) {
                        }
                        notification.flags = 16;
                        int i22 = i + 1;
                        c.this.g.notify(i22, notification);
                        g.a(c.f2710a, "Show new  notification....");
                        c.this.h;
                        boolean a22 = e.a((Context) c.this.l);
                        g.a(c.f2710a, String.format("isAppOnForeground = %1$B", new Object[]{Boolean.valueOf(a22)}));
                        c.this.g.cancel(i22);
                        c.this.l.startActivity(intent);
                        g.b(c.f2710a, String.format("%1$10s downloaded. Saved to: %2$s", new Object[]{aVar.c, string}));
                        return;
                    } catch (Exception e) {
                        String str = c.f2710a;
                        StringBuilder sb2 = new StringBuilder("can not install. ");
                        sb2.append(e.getMessage());
                        g.d(str, sb2.toString());
                        c.this.g.cancel(i + 1);
                        return;
                    } catch (Throwable unused) {
                    }
                    break;
                case 6:
                    a aVar2 = (a) message.obj;
                    int i3 = message.arg2;
                    String string2 = message.getData().getString("filename");
                    c.this.g.cancel(i3);
                    c.this.g.notify(i3 + 1, e.a((Context) c.this.l, f.l, PendingIntent.getActivity(c.this.l, 0, new Intent(), 134217728)));
                    String replace = string2.replace(".patch", ".apk");
                    String a3 = a.a(c.this.l);
                    e a4 = c.this.h;
                    a4.getClass();
                    c cVar = new c(c.this.l, i3, aVar2, replace);
                    cVar.execute(new String[]{a3, replace, string2});
                    break;
            }
        }
    }

    /* compiled from: DownloadProvider */
    class b extends Handler {
        b() {
        }

        public final void handleMessage(Message message) {
            try {
                String str = c.f2710a;
                StringBuilder sb = new StringBuilder("IncomingHandler(msg.what:");
                sb.append(message.what);
                sb.append(" msg.arg1:");
                sb.append(message.arg1);
                sb.append(" msg.arg2:");
                sb.append(message.arg2);
                sb.append(" msg.replyTo:");
                sb.append(message.replyTo);
                g.a(str, sb.toString());
                if (message.what != 4) {
                    super.handleMessage(message);
                    return;
                }
                Bundle data = message.getData();
                String str2 = c.f2710a;
                StringBuilder sb2 = new StringBuilder("IncomingHandler(msg.getData():");
                sb2.append(data);
                g.a(str2, sb2.toString());
                a aVar = new a(data.getString("mComponentName"), data.getString("mTitle"), data.getString("mUrl"));
                aVar.e = data.getString("mMd5");
                aVar.f = data.getString("mTargetMd5");
                aVar.f2708a = data.getString("mReqClz");
                aVar.g = data.getStringArray("succUrls");
                aVar.i = data.getStringArray("faiUrls");
                aVar.j = data.getStringArray("startUrls");
                aVar.k = data.getStringArray("pauseUrls");
                aVar.l = data.getStringArray("cancelUrls");
                aVar.h = data.getStringArray("carryonUrls");
                aVar.m = data.getBoolean("rich_notification");
                aVar.n = data.getBoolean("mSilent");
                aVar.o = data.getBoolean("mWifiOnly");
                if (c.this.h.a(aVar, c.b, message.replyTo)) {
                    String str3 = c.f2710a;
                    StringBuilder sb3 = new StringBuilder();
                    sb3.append(aVar.c);
                    sb3.append(" is already in downloading list. ");
                    g.b(str3, sb3.toString());
                    int b = c.this.h.b(aVar);
                    if (b == -1 || ((b) c.d.get(b)).f2718a != null) {
                        Toast.makeText(c.this.l, f.i, 0).show();
                        Message obtain = Message.obtain();
                        obtain.what = 2;
                        obtain.arg1 = 2;
                        obtain.arg2 = 0;
                        try {
                            message.replyTo.send(obtain);
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    } else {
                        String a2 = j.a(b, "continue");
                        Intent intent = new Intent(c.this.l, c.class);
                        intent.putExtra("com.mintegral.msdk.broadcast.download.msg", a2);
                        c.this.h.a(c.this, intent);
                    }
                } else if (e.b((Context) c.this.l)) {
                    c.c.put(aVar, message.replyTo);
                    Message obtain2 = Message.obtain();
                    obtain2.what = 1;
                    obtain2.arg1 = 1;
                    obtain2.arg2 = 0;
                    try {
                        message.replyTo.send(obtain2);
                    } catch (RemoteException e2) {
                        e2.printStackTrace();
                    }
                    c.a(c.this, aVar);
                } else {
                    Toast.makeText(c.this.l, f.d, 0).show();
                    Message obtain3 = Message.obtain();
                    obtain3.what = 2;
                    obtain3.arg1 = 4;
                    obtain3.arg2 = 0;
                    try {
                        message.replyTo.send(obtain3);
                    } catch (RemoteException e3) {
                        e3.printStackTrace();
                    }
                }
            } catch (Exception e4) {
                e4.printStackTrace();
            }
        }
    }

    public final IBinder a() {
        g.a(f2710a, "onBind ");
        return this.e.getBinder();
    }

    public final int a(Intent intent) {
        if (!(intent == null || intent.getExtras() == null)) {
            this.h.a(this, intent);
        }
        if (VERSION.SDK_INT >= 19 && (this.i.b() || this.k)) {
            try {
                Intent intent2 = new Intent(this.l.getApplicationContext(), getClass());
                intent2.setPackage(this.l.getPackageName());
                ((AlarmManager) this.l.getApplicationContext().getSystemService(NotificationCompat.CATEGORY_ALARM)).set(3, SystemClock.elapsedRealtime() + DefaultRenderersFactory.DEFAULT_ALLOWED_VIDEO_JOINING_TIME_MS, PendingIntent.getService(this.l.getApplicationContext(), 1, intent2, 1073741824));
            } catch (Exception unused) {
            }
        }
        if (this.k) {
            f();
            this.k = false;
        }
        return 1;
    }

    public final void b() {
        if (b) {
            Debug.waitForDebugger();
        }
        this.g = (NotificationManager) this.l.getSystemService("notification");
        j.a(this.g);
        this.i = new i(this.l);
        this.h = new e(d, c, this.i);
        this.j = new a();
    }

    public final void c() {
        try {
            d.a(this.l.getApplicationContext()).a();
            d.a(this.l.getApplicationContext()).finalize();
        } catch (Exception e2) {
            g.d(f2710a, e2.getMessage());
        }
    }

    private void f() {
        for (Integer intValue : this.i.a()) {
            this.g.cancel(intValue.intValue());
        }
    }

    public final void a(int i2) {
        int i3;
        if (d.indexOfKey(i2) >= 0) {
            b bVar = (b) d.get(i2);
            long[] jArr = bVar.f;
            if (jArr == null || jArr[1] <= 0) {
                i3 = 0;
            } else {
                i3 = (int) ((((float) jArr[0]) / ((float) jArr[1])) * 100.0f);
                if (i3 > 100) {
                    i3 = 99;
                }
            }
            if (!bVar.e.n) {
                this.f.put(i2, Long.valueOf(-1));
                Service service = this.l;
                a aVar = bVar.e;
                Context applicationContext = service.getApplicationContext();
                a aVar2 = new a(applicationContext);
                PendingIntent activity = PendingIntent.getActivity(applicationContext, 0, new Intent(), 134217728);
                if (aVar.m) {
                    aVar2.b();
                    e.a(applicationContext, aVar2, i2, 2);
                }
                aVar2.c(f.e).d().a(activity).a(System.currentTimeMillis());
                StringBuilder sb = new StringBuilder();
                sb.append(f.f);
                sb.append(aVar.c);
                a b2 = aVar2.b(sb.toString());
                StringBuilder sb2 = new StringBuilder();
                sb2.append(i3);
                sb2.append("%");
                b2.a((CharSequence) sb2.toString()).a(i3);
                aVar2.c().a(false);
                bVar.b = aVar2;
                this.g.notify(i2, aVar2.a());
            }
        }
    }

    public final void a(int i2, int i3) {
        if (d.indexOfKey(i2) >= 0) {
            b bVar = (b) d.get(i2);
            a aVar = bVar.e;
            long currentTimeMillis = System.currentTimeMillis();
            if (!aVar.n && currentTimeMillis - ((Long) this.f.get(i2)).longValue() > 500) {
                this.f.put(i2, Long.valueOf(currentTimeMillis));
                a aVar2 = bVar.b;
                a a2 = aVar2.a(i3);
                StringBuilder sb = new StringBuilder();
                sb.append(String.valueOf(i3));
                sb.append("%");
                a2.a((CharSequence) sb.toString());
                this.g.notify(i2, aVar2.a());
            }
            g.a(f2710a, String.format("%3$10s Notification: mNotificationId = %1$15s\t|\tprogress = %2$15s", new Object[]{Integer.valueOf(i2), Integer.valueOf(i3), aVar.c}));
        }
    }

    public final void a(int i2, String str) {
        if (d.indexOfKey(i2) >= 0) {
            b bVar = (b) d.get(i2);
            if (bVar != null) {
                a aVar = bVar.e;
                d.a(this.l).a(aVar.b, aVar.d, 100);
                Bundle bundle = new Bundle();
                bundle.putString("filename", str);
                if (aVar.b.equalsIgnoreCase("delta_update")) {
                    Message obtain = Message.obtain();
                    obtain.what = 6;
                    obtain.arg1 = 1;
                    obtain.obj = aVar;
                    obtain.arg2 = i2;
                    obtain.setData(bundle);
                    this.j.sendMessage(obtain);
                    return;
                }
                Message obtain2 = Message.obtain();
                obtain2.what = 5;
                obtain2.arg1 = 1;
                obtain2.obj = aVar;
                obtain2.arg2 = i2;
                obtain2.setData(bundle);
                this.j.sendMessage(obtain2);
                Message obtain3 = Message.obtain();
                obtain3.what = 5;
                obtain3.arg1 = 1;
                obtain3.arg2 = i2;
                obtain3.setData(bundle);
                try {
                    if (c.get(aVar) != null) {
                        ((Messenger) c.get(aVar)).send(obtain3);
                    }
                    this.h.a((Context) this.l, i2);
                } catch (RemoteException unused) {
                    this.h.a((Context) this.l, i2);
                }
            }
        }
    }

    public final void b(int i2) {
        if (d.indexOfKey(i2) >= 0) {
            this.h.a((Context) this.l, i2);
        }
    }

    public final e d() {
        return this.h;
    }

    public final void a(Service service) {
        this.l = service;
    }

    public final Service e() {
        return this.l;
    }

    static /* synthetic */ void a(c cVar, a aVar) {
        String str = f2710a;
        StringBuilder sb = new StringBuilder("startDownload([mComponentName:");
        sb.append(aVar.b);
        sb.append(" mTitle:");
        sb.append(aVar.c);
        sb.append(" mUrl:");
        sb.append(aVar.d);
        sb.append("])");
        g.a(str, sb.toString());
        int a2 = e.a(aVar);
        k kVar = new k(cVar, aVar, a2, 0);
        b bVar = new b(aVar, a2);
        cVar.i.a(a2);
        d.put(bVar.c, bVar);
        bVar.f2718a = kVar;
        kVar.start();
        if (b) {
            int size = c.size();
            int size2 = d.size();
            String str2 = f2710a;
            StringBuilder sb2 = new StringBuilder("Client size =");
            sb2.append(size);
            sb2.append("   cacheSize = ");
            sb2.append(size2);
            g.b(str2, sb2.toString());
            if (size != size2) {
                StringBuilder sb3 = new StringBuilder("Client size =");
                sb3.append(size);
                sb3.append("   cacheSize = ");
                sb3.append(size2);
                throw new RuntimeException(sb3.toString());
            }
        }
        if (b) {
            for (int i2 = 0; i2 < d.size(); i2++) {
                b bVar2 = (b) d.valueAt(i2);
                String str3 = f2710a;
                StringBuilder sb4 = new StringBuilder("Running task ");
                sb4.append(bVar2.e.c);
                g.a(str3, sb4.toString());
            }
        }
    }
}
