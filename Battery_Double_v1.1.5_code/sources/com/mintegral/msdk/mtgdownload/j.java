package com.mintegral.msdk.mtgdownload;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build.VERSION;
import com.mintegral.msdk.base.controller.a;
import com.mintegral.msdk.base.utils.c;
import com.mintegral.msdk.shell.MTGService;
import com.vungle.warren.ui.JavascriptBridge.MraidHandler;

/* compiled from: NotificationUtils */
public final class j {
    public static String a(int i, String str) {
        if (i == 0) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(i);
        StringBuilder sb2 = new StringBuilder(sb.toString());
        sb2.append(":");
        sb2.append(str);
        return sb2.toString();
    }

    public static PendingIntent a(Context context, String str) {
        Intent intent = new Intent(context, MTGService.class);
        intent.putExtra("com.mintegral.msdk.broadcast.download.msg", str);
        return PendingIntent.getService(context, str.hashCode(), intent, 134217728);
    }

    public static void a(NotificationManager notificationManager) {
        Context h = a.d().h();
        if ((h != null ? c.k(h) : 0) >= 26 && VERSION.SDK_INT >= 26) {
            notificationManager.createNotificationChannel(new NotificationChannel(MraidHandler.DOWNLOAD_ACTION, "下载进度", 2));
        }
    }
}
