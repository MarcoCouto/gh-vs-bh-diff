package com.mintegral.msdk.mtgdownload;

import android.app.Notification;
import android.app.Notification.Builder;
import android.app.PendingIntent;
import android.content.Context;
import android.os.Build.VERSION;
import android.support.v4.app.NotificationCompat;
import com.vungle.warren.ui.JavascriptBridge.MraidHandler;
import java.lang.reflect.Field;

/* compiled from: NotificationBuilder */
public class h {
    protected Context e;
    protected Notification f;
    protected Builder g;
    protected NotificationCompat.Builder h;
    protected PendingIntent i;

    public h(Context context) {
        this.e = context.getApplicationContext();
        if ((context != null ? this.e.getApplicationInfo().targetSdkVersion : 0) >= 26 && VERSION.SDK_INT >= 26) {
            this.h = new NotificationCompat.Builder(this.e, MraidHandler.DOWNLOAD_ACTION);
        } else if (VERSION.SDK_INT >= 16) {
            this.g = new Builder(context);
        } else {
            this.f = new Notification();
        }
    }

    public final void b() {
        if ((this.e != null ? this.e.getApplicationInfo().targetSdkVersion : 0) < 26 || VERSION.SDK_INT < 26) {
            if (VERSION.SDK_INT >= 16) {
                try {
                    Field declaredField = Builder.class.getDeclaredField("mActions");
                    declaredField.setAccessible(true);
                    declaredField.set(this.g, declaredField.get(this.g).getClass().newInstance());
                    return;
                } catch (Exception unused) {
                }
            }
            return;
        }
        try {
            Field declaredField2 = NotificationCompat.Builder.class.getDeclaredField("mActions");
            declaredField2.setAccessible(true);
            declaredField2.set(this.h, declaredField2.get(this.h).getClass().newInstance());
        } catch (Exception unused2) {
        }
    }

    public final h a(PendingIntent pendingIntent) {
        if ((this.e != null ? this.e.getApplicationInfo().targetSdkVersion : 0) >= 26 && VERSION.SDK_INT >= 26) {
            this.h.setContentIntent(pendingIntent);
        } else if (VERSION.SDK_INT >= 16) {
            this.g.setContentIntent(pendingIntent);
        } else {
            this.i = pendingIntent;
        }
        return this;
    }

    public final h c() {
        if ((this.e != null ? this.e.getApplicationInfo().targetSdkVersion : 0) >= 26 && VERSION.SDK_INT >= 26) {
            this.h.setOngoing(true);
        } else if (VERSION.SDK_INT >= 16) {
            this.g.setOngoing(true);
        } else {
            this.f.flags |= 2;
        }
        return this;
    }

    public final h a(boolean z) {
        if ((this.e != null ? this.e.getApplicationInfo().targetSdkVersion : 0) >= 26 && VERSION.SDK_INT >= 26) {
            this.h.setAutoCancel(z);
        } else if (VERSION.SDK_INT >= 16) {
            this.g.setAutoCancel(z);
        } else if (z) {
            this.f.flags |= 16;
        } else {
            this.f.flags &= -17;
        }
        return this;
    }

    public final h d() {
        if ((this.e != null ? this.e.getApplicationInfo().targetSdkVersion : 0) >= 26 && VERSION.SDK_INT >= 26) {
            this.h.setSmallIcon(17301633);
        } else if (VERSION.SDK_INT >= 16) {
            this.g.setSmallIcon(17301633);
        } else {
            this.f.icon = 17301633;
        }
        return this;
    }

    public final h c(CharSequence charSequence) {
        if ((this.e != null ? this.e.getApplicationInfo().targetSdkVersion : 0) >= 26 && VERSION.SDK_INT >= 26) {
            this.h.setTicker(charSequence);
        } else if (VERSION.SDK_INT >= 16) {
            this.g.setTicker(charSequence);
        } else {
            this.f.tickerText = charSequence;
        }
        return this;
    }

    public final h a(long j) {
        if ((this.e != null ? this.e.getApplicationInfo().targetSdkVersion : 0) >= 26 && VERSION.SDK_INT >= 26) {
            this.h.setWhen(j);
        } else if (VERSION.SDK_INT >= 16) {
            this.g.setWhen(j);
        } else {
            this.f.when = j;
        }
        return this;
    }
}
