package com.mintegral.msdk.mtgdownload;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.NetworkInfo.State;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Environment;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.v4.app.NotificationCompat.Builder;
import android.text.TextUtils;
import android.util.SparseArray;
import android.widget.Toast;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.mintegral.msdk.base.utils.CommonMD5;
import com.mintegral.msdk.base.utils.MTGFileProvider;
import com.mintegral.msdk.base.utils.g;
import com.vungle.warren.ui.JavascriptBridge.MraidHandler;
import io.fabric.sdk.android.services.events.EventsFilesManager;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;

/* compiled from: DownloadTool */
class e {

    /* renamed from: a reason: collision with root package name */
    private static final String f2716a = "com.mintegral.msdk.mtgdownload.e";
    private SparseArray<b> b;
    /* access modifiers changed from: private */
    public Map<a, Messenger> c;
    private i d;

    /* compiled from: DownloadTool */
    static class a extends h {

        /* renamed from: a reason: collision with root package name */
        String f2717a;
        String b;
        String c;
        Context d;

        public a(Context context) {
            super(context);
            this.d = context;
        }

        public final a a(CharSequence charSequence) {
            if ((this.d != null ? com.mintegral.msdk.base.utils.c.k(this.d) : 0) >= 26 && VERSION.SDK_INT >= 26) {
                this.h.setContentText(charSequence);
            } else if (VERSION.SDK_INT >= 16) {
                this.g.setContentText(charSequence);
            } else {
                this.b = charSequence.toString();
            }
            return this;
        }

        public final a b(CharSequence charSequence) {
            if ((this.d != null ? com.mintegral.msdk.base.utils.c.k(this.d) : 0) >= 26 && VERSION.SDK_INT >= 26) {
                this.h.setContentTitle(charSequence);
            } else if (VERSION.SDK_INT >= 16) {
                this.g.setContentTitle(charSequence);
            } else {
                this.f2717a = charSequence.toString();
            }
            return this;
        }

        public final a a(int i) {
            if ((this.d != null ? com.mintegral.msdk.base.utils.c.k(this.d) : 0) >= 26 && VERSION.SDK_INT >= 26) {
                this.h.setProgress(100, i, false);
            } else if (VERSION.SDK_INT >= 16) {
                this.g.setProgress(100, i, false);
            } else {
                StringBuilder sb = new StringBuilder();
                sb.append(i);
                sb.append("%");
                this.c = sb.toString();
            }
            return this;
        }

        public final Notification a() {
            if ((this.d != null ? com.mintegral.msdk.base.utils.c.k(this.d) : 0) >= 26 && VERSION.SDK_INT >= 26) {
                return this.h.build();
            }
            if (VERSION.SDK_INT >= 16) {
                return this.g.build();
            }
            if (VERSION.SDK_INT < 16) {
                this.f = new Builder(this.e).setTicker(this.f2717a).setContentIntent(this.i).build();
            } else {
                this.f = new Notification.Builder(this.e).setTicker(this.f2717a).setContentIntent(this.i).build();
            }
            return this.f;
        }

        public final void a(int i, String str, PendingIntent pendingIntent) {
            if ((this.d != null ? com.mintegral.msdk.base.utils.c.k(this.d) : 0) < 26 || VERSION.SDK_INT < 26) {
                if (VERSION.SDK_INT >= 16) {
                    this.g.addAction(i, str, pendingIntent);
                }
                return;
            }
            this.h.addAction(i, str, pendingIntent);
        }
    }

    /* compiled from: DownloadTool */
    static class b {

        /* renamed from: a reason: collision with root package name */
        k f2718a;
        a b;
        int c;
        int d;
        a e;
        long[] f = new long[3];

        public b(a aVar, int i) {
            this.c = i;
            this.e = aVar;
        }
    }

    /* compiled from: DownloadTool */
    class c extends AsyncTask<String, Void, Integer> {

        /* renamed from: a reason: collision with root package name */
        public int f2719a;
        public String b;
        private a d;
        private Context e;
        private NotificationManager f = ((NotificationManager) this.e.getSystemService("notification"));

        /* access modifiers changed from: protected */
        /* JADX WARNING: Can't wrap try/catch for region: R(2:14|15) */
        /* JADX WARNING: Can't wrap try/catch for region: R(3:26|27|28) */
        /* JADX WARNING: Can't wrap try/catch for region: R(3:36|37|38) */
        /* JADX WARNING: Code restructure failed: missing block: B:15:?, code lost:
            r7.setDataAndType(android.net.Uri.fromFile(new java.io.File(r6.b)), "application/vnd.android.package-archive");
         */
        /* JADX WARNING: Code restructure failed: missing block: B:27:?, code lost:
            r6.c.a(r6.e, r6.f2719a);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:28:0x00f7, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:37:?, code lost:
            r6.c.a(r6.e, r6.f2719a);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:38:0x014e, code lost:
            return;
         */
        /* JADX WARNING: Missing exception handler attribute for start block: B:14:0x0068 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:26:0x00ee */
        /* JADX WARNING: Missing exception handler attribute for start block: B:36:0x0145 */
        /* JADX WARNING: Removed duplicated region for block: B:18:0x009d A[Catch:{ Throwable -> 0x014f }] */
        /* JADX WARNING: Removed duplicated region for block: B:23:0x00d3 A[Catch:{ RemoteException -> 0x00ee }] */
        /* JADX WARNING: Unknown top exception splitter block from list: {B:20:0x00c5=Splitter:B:20:0x00c5, B:5:0x0016=Splitter:B:5:0x0016, B:30:0x011c=Splitter:B:30:0x011c} */
        public final /* synthetic */ void onPostExecute(Object obj) {
            try {
                if (((Integer) obj).intValue() == 1) {
                    Intent intent = new Intent("android.intent.action.VIEW");
                    intent.addFlags(268435456);
                    if (com.mintegral.msdk.base.utils.c.k(this.e) < 24 || VERSION.SDK_INT < 24) {
                        intent.setDataAndType(Uri.fromFile(new File(this.b)), "application/vnd.android.package-archive");
                        Notification a2 = e.a(this.e, f.h, PendingIntent.getActivity(this.e, 0, intent, 134217728));
                        a2.flags = 16;
                        this.f.notify(this.f2719a + 1, a2);
                        if (e.a(this.e)) {
                            this.f.cancel(this.f2719a + 1);
                            this.e.startActivity(intent);
                        }
                        Bundle bundle = new Bundle();
                        bundle.putString("filename", this.b);
                        Message obtain = Message.obtain();
                        obtain.what = 5;
                        obtain.arg1 = 1;
                        obtain.arg2 = this.f2719a;
                        obtain.setData(bundle);
                        if (e.this.c.get(this.d) != null) {
                            ((Messenger) e.this.c.get(this.d)).send(obtain);
                        }
                        e.this.a(this.e, this.f2719a);
                        return;
                    }
                    Context context = this.e;
                    StringBuilder sb = new StringBuilder();
                    sb.append(this.e.getApplicationContext().getPackageName());
                    sb.append(".mtgFileProvider");
                    Uri uriForFile = MTGFileProvider.getUriForFile(context, sb.toString(), new File(this.b));
                    if (uriForFile != null) {
                        intent.setDataAndType(uriForFile, "application/vnd.android.package-archive");
                        intent.addFlags(1);
                    }
                    Notification a22 = e.a(this.e, f.h, PendingIntent.getActivity(this.e, 0, intent, 134217728));
                    a22.flags = 16;
                    this.f.notify(this.f2719a + 1, a22);
                    if (e.a(this.e)) {
                    }
                    Bundle bundle2 = new Bundle();
                    bundle2.putString("filename", this.b);
                    Message obtain2 = Message.obtain();
                    obtain2.what = 5;
                    obtain2.arg1 = 1;
                    obtain2.arg2 = this.f2719a;
                    obtain2.setData(bundle2);
                    if (e.this.c.get(this.d) != null) {
                    }
                    e.this.a(this.e, this.f2719a);
                    return;
                }
                this.f.cancel(this.f2719a + 1);
                Bundle bundle3 = new Bundle();
                bundle3.putString("filename", this.b);
                Message obtain3 = Message.obtain();
                obtain3.what = 5;
                obtain3.arg1 = 3;
                obtain3.arg2 = this.f2719a;
                obtain3.setData(bundle3);
                if (e.this.c.get(this.d) != null) {
                    ((Messenger) e.this.c.get(this.d)).send(obtain3);
                }
                e.this.a(this.e, this.f2719a);
            } catch (Throwable unused) {
            }
        }

        public c(Context context, int i, a aVar, String str) {
            this.e = context.getApplicationContext();
            j.a(this.f);
            this.f2719a = i;
            this.d = aVar;
            this.b = str;
        }

        /* access modifiers changed from: protected */
        public final void onPreExecute() {
            super.onPreExecute();
        }

        /* access modifiers changed from: protected */
        public final /* synthetic */ Object doInBackground(Object[] objArr) {
            return Integer.valueOf(1);
        }
    }

    public e(SparseArray<b> sparseArray, Map<a, Messenger> map, i iVar) {
        this.b = sparseArray;
        this.c = map;
        this.d = iVar;
    }

    static int a(a aVar) {
        return Math.abs((int) (((long) ((aVar.c.hashCode() >> 2) + (aVar.d.hashCode() >> 3))) + System.currentTimeMillis()));
    }

    static void a(Context context, a aVar, int i, int i2) {
        if (VERSION.SDK_INT >= 16) {
            PendingIntent a2 = j.a(context, j.a(i, "continue"));
            PendingIntent a3 = j.a(context, j.a(i, "cancel"));
            switch (i2) {
                case 1:
                    aVar.a(17301540, f.f2720a, a2);
                    break;
                case 2:
                    aVar.a(17301539, f.b, a2);
                    break;
            }
            aVar.a(17301560, f.c, a3);
        }
    }

    static boolean a(Context context) {
        List<RunningAppProcessInfo> runningAppProcesses = ((ActivityManager) context.getSystemService("activity")).getRunningAppProcesses();
        if (runningAppProcesses == null) {
            return false;
        }
        String packageName = context.getPackageName();
        for (RunningAppProcessInfo runningAppProcessInfo : runningAppProcesses) {
            if (runningAppProcessInfo.importance == 100 && runningAppProcessInfo.processName.equals(packageName)) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: 0000 */
    public final boolean a(a aVar, boolean z, Messenger messenger) {
        if (z) {
            int nextInt = new Random().nextInt(1000);
            if (this.c != null) {
                for (a aVar2 : this.c.keySet()) {
                    String str = f2716a;
                    StringBuilder sb = new StringBuilder(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
                    sb.append(nextInt);
                    sb.append(" downling  ");
                    sb.append(aVar2.c);
                    sb.append("   ");
                    sb.append(aVar2.d);
                    g.a(str, sb.toString());
                }
            } else {
                String str2 = f2716a;
                StringBuilder sb2 = new StringBuilder(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
                sb2.append(nextInt);
                sb2.append("downling  null");
                g.a(str2, sb2.toString());
            }
        }
        if (this.c == null) {
            return false;
        }
        for (a aVar3 : this.c.keySet()) {
            if (aVar.f != null && aVar.f.equals(aVar3.f)) {
                this.c.put(aVar3, messenger);
                return true;
            } else if (aVar3.d.equals(aVar.d)) {
                this.c.put(aVar3, messenger);
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: 0000 */
    public final int b(a aVar) {
        for (int i = 0; i < this.b.size(); i++) {
            int keyAt = this.b.keyAt(i);
            if (aVar.f != null && aVar.f.equals(((b) this.b.get(keyAt)).e.f)) {
                return ((b) this.b.get(keyAt)).c;
            }
            if (((b) this.b.get(keyAt)).e.d.equals(aVar.d)) {
                return ((b) this.b.get(keyAt)).c;
            }
        }
        return -1;
    }

    /* access modifiers changed from: 0000 */
    public final void a(Context context, int i) {
        NotificationManager notificationManager = (NotificationManager) context.getApplicationContext().getSystemService("notification");
        b bVar = (b) this.b.get(i);
        if (bVar != null) {
            String str = f2716a;
            StringBuilder sb = new StringBuilder("download service clear cache ");
            sb.append(bVar.e.c);
            g.a(str, sb.toString());
            if (bVar.f2718a != null) {
                bVar.f2718a.a(2);
            }
            notificationManager.cancel(bVar.c);
            if (this.c.containsKey(bVar.e)) {
                this.c.remove(bVar.e);
            }
            SparseArray<b> sparseArray = this.b;
            if (sparseArray.indexOfKey(bVar.c) >= 0) {
                sparseArray.remove(bVar.c);
            }
            this.d.b(i);
        }
    }

    /* access modifiers changed from: 0000 */
    /* JADX WARNING: Can't wrap try/catch for region: R(2:51|52) */
    /* JADX WARNING: Can't wrap try/catch for region: R(2:61|62) */
    /* JADX WARNING: Code restructure failed: missing block: B:52:?, code lost:
        a(r1, r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:?, code lost:
        a(r1, r2);
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:51:0x0160 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:61:0x0189 */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:46:0x0145=Splitter:B:46:0x0145, B:61:0x0189=Splitter:B:61:0x0189} */
    public final boolean a(c cVar, Intent intent) {
        Context applicationContext;
        int parseInt;
        b bVar;
        try {
            applicationContext = cVar.e().getApplicationContext();
            String string = intent.getExtras().getString("com.mintegral.msdk.broadcast.download.msg");
            if (string == null) {
                return false;
            }
            String[] split = string.split(":");
            parseInt = Integer.parseInt(split[0]);
            String trim = split[1].trim();
            if (parseInt != 0 && !TextUtils.isEmpty(trim) && this.b.indexOfKey(parseInt) >= 0) {
                bVar = (b) this.b.get(parseInt);
                k kVar = bVar.f2718a;
                if ("continue".equals(trim)) {
                    if (kVar == null) {
                        g.a(f2716a, "Receive action do play click.");
                        if (!b(applicationContext)) {
                            Toast.makeText(applicationContext, f.d, 1).show();
                            return false;
                        }
                        k kVar2 = new k(cVar, bVar.e, parseInt, bVar.d);
                        bVar.f2718a = kVar2;
                        kVar2.start();
                        Message obtain = Message.obtain();
                        obtain.what = 2;
                        obtain.arg1 = 7;
                        obtain.arg2 = parseInt;
                        try {
                            if (this.c.get(bVar.e) != null) {
                                ((Messenger) this.c.get(bVar.e)).send(obtain);
                            }
                        } catch (RemoteException e) {
                            g.c(f2716a, "", e);
                        }
                        return true;
                    }
                    g.a(f2716a, "Receive action do play click.");
                    kVar.a(1);
                    bVar.f2718a = null;
                    Context applicationContext2 = applicationContext.getApplicationContext();
                    NotificationManager notificationManager = (NotificationManager) applicationContext2.getSystemService("notification");
                    j.a(notificationManager);
                    b bVar2 = (b) this.b.get(parseInt);
                    bVar2.b.b();
                    a(applicationContext2, bVar2.b, parseInt, 1);
                    a aVar = bVar2.b;
                    StringBuilder sb = new StringBuilder();
                    sb.append(f.g);
                    sb.append(bVar2.e.c);
                    aVar.b(sb.toString()).c().a(true);
                    notificationManager.notify(parseInt, bVar2.b.a());
                    Message obtain2 = Message.obtain();
                    obtain2.what = 2;
                    obtain2.arg1 = 6;
                    obtain2.arg2 = parseInt;
                    try {
                        if (this.c.get(bVar.e) != null) {
                            ((Messenger) this.c.get(bVar.e)).send(obtain2);
                        }
                    } catch (RemoteException e2) {
                        g.c(f2716a, "", e2);
                    }
                    return true;
                } else if ("cancel".equals(trim)) {
                    g.a(f2716a, "Receive action do stop click.");
                    if (kVar != null) {
                        kVar.a(2);
                    }
                    Message obtain3 = Message.obtain();
                    obtain3.what = 5;
                    obtain3.arg1 = 5;
                    obtain3.arg2 = parseInt;
                    if (this.c.get(bVar.e) != null) {
                        ((Messenger) this.c.get(bVar.e)).send(obtain3);
                    }
                    a(applicationContext, parseInt);
                    return true;
                }
            }
            return false;
        } catch (Exception unused) {
            Message obtain4 = Message.obtain();
            obtain4.what = 5;
            obtain4.arg1 = 5;
            obtain4.arg2 = parseInt;
            if (this.c.get(bVar.e) != null) {
                ((Messenger) this.c.get(bVar.e)).send(obtain4);
            }
            a(applicationContext, parseInt);
        } catch (Exception e3) {
            e3.printStackTrace();
        } catch (Throwable th) {
            Message obtain5 = Message.obtain();
            obtain5.what = 5;
            obtain5.arg1 = 5;
            obtain5.arg2 = parseInt;
            if (this.c.get(bVar.e) != null) {
                ((Messenger) this.c.get(bVar.e)).send(obtain5);
            }
            a(applicationContext, parseInt);
            throw th;
        }
    }

    public static boolean b(Context context) {
        try {
            int checkPermission = context.getPackageManager().checkPermission("android.permission.ACCESS_NETWORK_STATE", context.getPackageName());
            boolean isConnectedOrConnecting = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo().isConnectedOrConnecting();
            if (checkPermission != 0 || !isConnectedOrConnecting) {
                return false;
            }
            return true;
        } catch (Exception unused) {
            return false;
        }
    }

    public static String a(File file) {
        byte[] bArr = new byte[1024];
        try {
            if (!file.isFile()) {
                return "";
            }
            MessageDigest instance = MessageDigest.getInstance(CommonMD5.TAG);
            FileInputStream fileInputStream = new FileInputStream(file);
            while (true) {
                int read = fileInputStream.read(bArr, 0, 1024);
                if (read != -1) {
                    instance.update(bArr, 0, read);
                } else {
                    fileInputStream.close();
                    return String.format("%1$032x", new Object[]{new BigInteger(1, instance.digest())});
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String a(String str) {
        if (str == null) {
            return null;
        }
        try {
            byte[] bytes = str.getBytes();
            MessageDigest instance = MessageDigest.getInstance(CommonMD5.TAG);
            instance.reset();
            instance.update(bytes);
            byte[] digest = instance.digest();
            StringBuffer stringBuffer = new StringBuffer();
            for (byte valueOf : digest) {
                stringBuffer.append(String.format("%02X", new Object[]{Byte.valueOf(valueOf)}));
            }
            return stringBuffer.toString();
        } catch (Exception unused) {
            return str.replaceAll("[^[a-z][A-Z][0-9][.][_]]", "");
        }
    }

    public static String a() {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US).format(new Date());
    }

    private static boolean a(String str, int i) {
        try {
            Class.forName("android.os.FileUtils").getMethod("setPermissions", new Class[]{String.class, Integer.TYPE, Integer.TYPE, Integer.TYPE}).invoke(null, new Object[]{str, Integer.valueOf(i), Integer.valueOf(-1), Integer.valueOf(-1)});
            return true;
        } catch (ClassNotFoundException e) {
            g.a(f2716a, "error when set permissions:", e);
            return false;
        } catch (NoSuchMethodException e2) {
            g.a(f2716a, "error when set permissions:", e2);
            return false;
        } catch (IllegalArgumentException e3) {
            g.a(f2716a, "error when set permissions:", e3);
            return false;
        } catch (IllegalAccessException e4) {
            g.a(f2716a, "error when set permissions:", e4);
            return false;
        } catch (InvocationTargetException e5) {
            g.a(f2716a, "error when set permissions:", e5);
            return false;
        }
    }

    protected static boolean b(String str) {
        return a(str, 438);
    }

    private static String[] d(Context context) {
        String[] strArr = {"Unknown", "Unknown"};
        try {
            if (context.getPackageManager().checkPermission("android.permission.ACCESS_NETWORK_STATE", context.getPackageName()) != 0) {
                strArr[0] = "Unknown";
                return strArr;
            }
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
            if (connectivityManager == null) {
                strArr[0] = "Unknown";
                return strArr;
            } else if (connectivityManager.getNetworkInfo(1).getState() == State.CONNECTED) {
                strArr[0] = "Wi-Fi";
                return strArr;
            } else {
                NetworkInfo networkInfo = connectivityManager.getNetworkInfo(0);
                if (networkInfo.getState() == State.CONNECTED) {
                    strArr[0] = "2G/3G";
                    strArr[1] = networkInfo.getSubtypeName();
                    return strArr;
                }
                return strArr;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean c(Context context) {
        return "Wi-Fi".equals(d(context)[0]);
    }

    public static Notification a(Context context, String str, PendingIntent pendingIntent) {
        if ((context != null ? com.mintegral.msdk.base.utils.c.k(context) : 0) >= 26 && VERSION.SDK_INT >= 26) {
            return new Builder(context, MraidHandler.DOWNLOAD_ACTION).setSmallIcon(17301634).setLargeIcon(BitmapFactory.decodeResource(context.getResources(), 17301634)).setTicker(str).setWhen(System.currentTimeMillis()).setContentIntent(pendingIntent).build();
        }
        if (VERSION.SDK_INT < 16) {
            return new Builder(context).setSmallIcon(17301633).setTicker(str).setWhen(System.currentTimeMillis()).setContentIntent(pendingIntent).build();
        }
        return new Notification.Builder(context).setSmallIcon(17301633).setTicker(str).setWhen(System.currentTimeMillis()).setContentIntent(pendingIntent).build();
    }

    public static File a(String str, Context context, boolean[] zArr) throws IOException {
        if (Environment.getExternalStorageState().equals("mounted")) {
            String b2 = com.mintegral.msdk.base.common.b.e.b(com.mintegral.msdk.base.common.b.c.MINTEGRAL_700_APK);
            StringBuilder sb = new StringBuilder();
            sb.append(b2);
            sb.append("/download/.mtg");
            sb.append(str);
            File file = new File(sb.toString());
            file.mkdirs();
            if (file.exists()) {
                zArr[0] = true;
                return file;
            }
        }
        String absolutePath = context.getCacheDir().getAbsolutePath();
        new File(absolutePath).mkdir();
        a(absolutePath, (int) IronSourceError.ERROR_CODE_KEY_NOT_SET);
        StringBuilder sb2 = new StringBuilder();
        sb2.append(absolutePath);
        sb2.append("/mtgdownload");
        String sb3 = sb2.toString();
        new File(sb3).mkdir();
        a(sb3, (int) IronSourceError.ERROR_CODE_KEY_NOT_SET);
        File file2 = new File(sb3);
        zArr[0] = false;
        return file2;
    }
}
