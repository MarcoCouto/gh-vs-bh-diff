package com.mintegral.msdk.mtgdownload;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.v4.app.NotificationCompat;
import com.mintegral.msdk.base.utils.g;
import java.text.SimpleDateFormat;
import java.util.Date;

/* compiled from: DownloadTaskList */
public class d {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public static final String f2713a = "com.mintegral.msdk.mtgdownload.d";
    private static Context b;
    private a c;

    /* compiled from: DownloadTaskList */
    class a extends SQLiteOpenHelper {
        public final void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        }

        a(Context context) {
            super(context, "MTG_DATA", null, 2);
        }

        public final void onCreate(SQLiteDatabase sQLiteDatabase) {
            g.a(d.f2713a, "CREATE TABLE mtg_download_task_list (cp TEXT, url TEXT, progress INTEGER, extra TEXT, last_modified TEXT, UNIQUE (cp,url) ON CONFLICT ABORT);");
            sQLiteDatabase.execSQL("CREATE TABLE mtg_download_task_list (cp TEXT, url TEXT, progress INTEGER, extra TEXT, last_modified TEXT, UNIQUE (cp,url) ON CONFLICT ABORT);");
        }
    }

    /* compiled from: DownloadTaskList */
    private static class b {

        /* renamed from: a reason: collision with root package name */
        public static final d f2715a = new d(0);
    }

    /* synthetic */ d(byte b2) {
        this();
    }

    private d() {
        this.c = new a(b);
    }

    public static d a(Context context) {
        if (b == null && context == null) {
            throw new NullPointerException();
        }
        if (b == null) {
            b = context;
        }
        return b.f2715a;
    }

    public final boolean a(String str, String str2) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("cp", str);
        boolean z = false;
        contentValues.put(NotificationCompat.CATEGORY_PROGRESS, Integer.valueOf(0));
        contentValues.put("last_modified", e.a());
        try {
            String[] strArr = {NotificationCompat.CATEGORY_PROGRESS};
            Cursor query = this.c.getReadableDatabase().query("mtg_download_task_list", strArr, "cp=? and url=?", new String[]{str, str2}, null, null, null, "1");
            if (query.getCount() > 0) {
                String str3 = f2713a;
                StringBuilder sb = new StringBuilder("insert(");
                sb.append(str);
                sb.append(", ");
                sb.append(str2);
                sb.append("):  already exists in the db. Insert is cancelled.");
                g.a(str3, sb.toString());
            } else {
                long insert = this.c.getWritableDatabase().insert("mtg_download_task_list", null, contentValues);
                if (insert != -1) {
                    z = true;
                }
                String str4 = f2713a;
                StringBuilder sb2 = new StringBuilder("insert(");
                sb2.append(str);
                sb2.append(", ");
                sb2.append(str2);
                sb2.append("): rowid=");
                sb2.append(insert);
                g.a(str4, sb2.toString());
            }
            query.close();
        } catch (Exception e) {
            String str5 = f2713a;
            StringBuilder sb3 = new StringBuilder("insert(");
            sb3.append(str);
            sb3.append(", ");
            sb3.append(str2);
            sb3.append("): ");
            sb3.append(e.getMessage());
            g.c(str5, sb3.toString(), e);
        }
        return z;
    }

    public final void a(String str, String str2, int i) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(NotificationCompat.CATEGORY_PROGRESS, Integer.valueOf(i));
        contentValues.put("last_modified", e.a());
        String[] strArr = {str, str2};
        this.c.getWritableDatabase().update("mtg_download_task_list", contentValues, "cp=? and url=?", strArr);
        String str3 = f2713a;
        StringBuilder sb = new StringBuilder("updateProgress(");
        sb.append(str);
        sb.append(", ");
        sb.append(str2);
        sb.append(", ");
        sb.append(i);
        sb.append(")");
        g.a(str3, sb.toString());
    }

    public final void a() {
        try {
            Date date = new Date(new Date().getTime() - 259200000);
            String format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date);
            StringBuilder sb = new StringBuilder(" DELETE FROM mtg_download_task_list WHERE strftime('yyyy-MM-dd HH:mm:ss', last_modified)<=strftime('yyyy-MM-dd HH:mm:ss', '");
            sb.append(format);
            sb.append("')");
            this.c.getWritableDatabase().execSQL(sb.toString());
            String str = f2713a;
            StringBuilder sb2 = new StringBuilder("clearOverdueTasks(259200) remove all tasks before ");
            sb2.append(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date));
            g.a(str, sb2.toString());
        } catch (Exception e) {
            g.d(f2713a, e.getMessage());
        }
    }

    public void finalize() {
        this.c.close();
    }
}
