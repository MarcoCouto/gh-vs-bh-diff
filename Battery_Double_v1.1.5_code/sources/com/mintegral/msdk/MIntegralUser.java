package com.mintegral.msdk;

import android.text.TextUtils;
import com.ironsource.mediationsdk.IronSourceSegment;
import org.json.JSONException;
import org.json.JSONObject;

public class MIntegralUser {

    /* renamed from: a reason: collision with root package name */
    private Integer f2482a;
    private Integer b;
    private Integer c;
    private a d;
    private String e;

    public static class a {

        /* renamed from: a reason: collision with root package name */
        private double f2483a = -1000.0d;
        private double b = -1000.0d;

        public final double a() {
            return this.f2483a;
        }

        public final void a(double d) {
            this.f2483a = d;
        }

        public final double b() {
            return this.b;
        }

        public final void b(double d) {
            this.b = d;
        }

        public final boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            a aVar = (a) obj;
            return Double.compare(aVar.f2483a, this.f2483a) == 0 && Double.compare(aVar.b, this.b) == 0;
        }

        public final int hashCode() {
            long doubleToLongBits = Double.doubleToLongBits(this.f2483a);
            int i = (int) (doubleToLongBits ^ (doubleToLongBits >>> 32));
            long doubleToLongBits2 = Double.doubleToLongBits(this.b);
            return (i * 31) + ((int) ((doubleToLongBits2 >>> 32) ^ doubleToLongBits2));
        }

        public final String toString() {
            StringBuilder sb = new StringBuilder("GPS{lat=");
            sb.append(this.f2483a);
            sb.append(", lng=");
            sb.append(this.b);
            sb.append('}');
            return sb.toString();
        }
    }

    public void setGender(int i) {
        this.f2482a = Integer.valueOf(i);
    }

    public void setAge(int i) {
        this.b = Integer.valueOf(i);
    }

    public void setPay(int i) {
        this.c = Integer.valueOf(i);
    }

    public void setLat(double d2) {
        if (this.d == null) {
            this.d = new a();
        }
        this.d.a(d2);
    }

    public void setLng(double d2) {
        if (this.d == null) {
            this.d = new a();
        }
        this.d.b(d2);
    }

    public void setCustom(String str) {
        this.e = str;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        MIntegralUser mIntegralUser = (MIntegralUser) obj;
        if (this.f2482a.equals(mIntegralUser.f2482a) && this.b.equals(mIntegralUser.b) && this.c.equals(mIntegralUser.c) && this.d.equals(mIntegralUser.d)) {
            return this.e.equals(mIntegralUser.e);
        }
        return false;
    }

    public int hashCode() {
        return (((((((this.f2482a.hashCode() * 31) + this.b.hashCode()) * 31) + this.c.hashCode()) * 31) + this.d.hashCode()) * 31) + this.e.hashCode();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("MIntegralUser{gender=");
        sb.append(this.f2482a);
        sb.append(", age=");
        sb.append(this.b);
        sb.append(", pay=");
        sb.append(this.c);
        sb.append(", gps=");
        sb.append(this.d);
        sb.append(", custom='");
        sb.append(this.e);
        sb.append('\'');
        sb.append('}');
        return sb.toString();
    }

    public static String toJSON(MIntegralUser mIntegralUser) {
        if (mIntegralUser == null) {
            return null;
        }
        JSONObject jSONObject = new JSONObject();
        try {
            if (mIntegralUser.f2482a != null) {
                jSONObject.put("gender", mIntegralUser.f2482a);
            }
            if (mIntegralUser.b != null) {
                jSONObject.put(IronSourceSegment.AGE, mIntegralUser.b);
            }
            if (mIntegralUser.c != null) {
                jSONObject.put(IronSourceSegment.PAYING, mIntegralUser.c);
            }
            if (mIntegralUser.d != null) {
                JSONObject jSONObject2 = new JSONObject();
                if (mIntegralUser.d.a() != -1000.0d) {
                    jSONObject2.put("lat", mIntegralUser.d.a());
                }
                if (mIntegralUser.d.b() != -1000.0d) {
                    jSONObject2.put("lng", mIntegralUser.d.b());
                }
                jSONObject.put("gps", jSONObject2);
            }
            if (!TextUtils.isEmpty(mIntegralUser.e)) {
                jSONObject.put("custom", mIntegralUser.e);
            }
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
        if (jSONObject.length() == 0) {
            return null;
        }
        return jSONObject.toString();
    }

    public static MIntegralUser getMintegralUser(String str) {
        MIntegralUser mIntegralUser = null;
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            JSONObject jSONObject = new JSONObject(str);
            MIntegralUser mIntegralUser2 = new MIntegralUser();
            try {
                if (jSONObject.has(IronSourceSegment.AGE)) {
                    mIntegralUser2.setAge(jSONObject.optInt(IronSourceSegment.AGE));
                }
                if (jSONObject.has("gender")) {
                    mIntegralUser2.setGender(jSONObject.optInt("gender"));
                }
                if (jSONObject.has(IronSourceSegment.PAYING)) {
                    mIntegralUser2.setPay(jSONObject.optInt(IronSourceSegment.PAYING));
                }
                if (jSONObject.has("custom")) {
                    mIntegralUser2.setCustom(jSONObject.optString("custom"));
                }
                JSONObject optJSONObject = jSONObject.optJSONObject("gps");
                if (optJSONObject != null) {
                    a aVar = new a();
                    aVar.a(optJSONObject.optDouble("lat", -1000.0d));
                    aVar.b(optJSONObject.optDouble("lng", -1000.0d));
                    mIntegralUser2.d = aVar;
                }
                return mIntegralUser2;
            } catch (JSONException e2) {
                e = e2;
                mIntegralUser = mIntegralUser2;
                e.printStackTrace();
                return mIntegralUser;
            }
        } catch (JSONException e3) {
            e = e3;
            e.printStackTrace();
            return mIntegralUser;
        }
    }
}
