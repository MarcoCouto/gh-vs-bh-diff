package com.mintegral.msdk.b;

import android.content.Context;
import android.text.TextUtils;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.base.b.h;
import com.mintegral.msdk.base.b.i;
import com.mintegral.msdk.base.b.q;
import com.mintegral.msdk.base.b.t;
import com.mintegral.msdk.base.controller.a;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.entity.m;
import com.mintegral.msdk.base.entity.n;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.base.utils.r;
import io.fabric.sdk.android.services.events.EventsFilesManager;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: SettingManager */
public class b {

    /* renamed from: a reason: collision with root package name */
    public static final String f2491a = "b";
    private static b b;
    private static HashMap<String, d> c = new HashMap<>();
    private static a d = null;

    private b() {
    }

    public static b a() {
        if (b == null) {
            synchronized (b.class) {
                if (b == null) {
                    b = new b();
                }
            }
        }
        return b;
    }

    public static boolean a(String str) {
        a b2 = b(str);
        if (b2 != null) {
            long aM = b2.aM() * 1000;
            long currentTimeMillis = System.currentTimeMillis();
            long aQ = b2.aQ() + aM;
            if (aQ > currentTimeMillis) {
                String str2 = f2491a;
                StringBuilder sb = new StringBuilder("app setting nexttime is not ready  [settingNextRequestTime= ");
                sb.append(aQ);
                sb.append(" currentTime = ");
                sb.append(currentTimeMillis);
                sb.append(RequestParameters.RIGHT_BRACKETS);
                g.b(str2, sb.toString());
                return false;
            }
        }
        g.b(f2491a, "app setting timeout or not exists");
        return true;
    }

    public static boolean a(String str, int i, String str2) {
        try {
            a b2 = b(str);
            if (b2 == null) {
                a();
                b2 = b();
            }
            Context h = a.d().h();
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
            sb.append(i);
            sb.append(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
            sb.append(str2);
            String sb2 = sb.toString();
            long longValue = ((Long) r.b(h, sb2, Long.valueOf(0))).longValue();
            long aq = b2.aq() * 1000;
            long currentTimeMillis = System.currentTimeMillis();
            if (longValue + aq > currentTimeMillis) {
                return false;
            }
            r.a(h, sb2, Long.valueOf(currentTimeMillis));
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static long a(String str, String str2) {
        StringBuilder sb = new StringBuilder("wall_style_");
        sb.append(str);
        sb.append(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
        sb.append(str2);
        String sb2 = sb.toString();
        if (TextUtils.isEmpty(str2)) {
            List b2 = com.mintegral.msdk.base.a.a.a.a().b();
            if (b2 != null && b2.size() > 0) {
                int i = 0;
                while (true) {
                    if (i >= b2.size()) {
                        break;
                    }
                    String str3 = (String) b2.get(i);
                    if (str3.contains(sb2.replace("null", ""))) {
                        sb2 = str3;
                        break;
                    }
                    i++;
                }
            }
        }
        String a2 = com.mintegral.msdk.base.a.a.a.a().a(sb2);
        try {
            if (!TextUtils.isEmpty(a2)) {
                long optLong = new JSONObject(a2).optLong("current_time");
                String str4 = f2491a;
                StringBuilder sb3 = new StringBuilder("lastGetTime");
                sb3.append(optLong);
                g.b(str4, sb3.toString());
                return optLong;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static void b(String str, String str2) {
        com.mintegral.msdk.base.a.a.a.a().a(str, str2);
        a f = a.f(str2);
        d = f;
        if (f != null) {
            d.bb();
        }
        a aVar = d;
        if (aVar != null) {
            try {
                if (aVar.O() <= 0) {
                    t.a((h) i.a(a.d().h())).d();
                } else if (aVar.S() != null && aVar.S().size() > 0) {
                    if (aVar.T() == 1) {
                        List a2 = q.a((h) i.a(a.d().h())).a(aVar.S().size());
                        ArrayList arrayList = new ArrayList();
                        if (a2 == null || a2.size() <= 0) {
                            a(aVar.S(), 0);
                            return;
                        }
                        int i = 0;
                        boolean z = false;
                        while (i < aVar.S().size()) {
                            int i2 = i + 1;
                            if (a2.size() >= i2) {
                                m mVar = (m) a2.get(i);
                                CampaignEx campaignEx = (CampaignEx) aVar.S().get(i);
                                campaignEx.setPackageName(mVar.a());
                                campaignEx.setIex(1);
                                campaignEx.setTs(mVar.c());
                                String str3 = null;
                                switch (mVar.b()) {
                                    case 1:
                                        str3 = "im";
                                        break;
                                    case 2:
                                        str3 = "if";
                                        break;
                                    case 3:
                                        str3 = "is";
                                        break;
                                }
                                campaignEx.setLabel(str3);
                                campaignEx.setPkgSource(mVar.d());
                                arrayList.add(campaignEx);
                                mVar.a(0);
                                z = true;
                            } else {
                                CampaignEx campaignEx2 = (CampaignEx) aVar.S().get(i);
                                campaignEx2.setIex(0);
                                campaignEx2.setTs(0);
                                campaignEx2.setLabel("im");
                                arrayList.add(campaignEx2);
                            }
                            i = i2;
                        }
                        if (z) {
                            a((List<CampaignEx>) arrayList, 1);
                            q.a((h) i.a(a.d().h())).a(a2);
                            return;
                        }
                        a((List<CampaignEx>) arrayList, 0);
                        return;
                    }
                    a(aVar.S(), 0);
                }
            } catch (Exception e) {
                if (MIntegralConstans.DEBUG) {
                    e.printStackTrace();
                }
            }
        }
    }

    private static n a(List<CampaignEx> list, int i) {
        n nVar = new n();
        try {
            nVar.b(b(list, i));
            nVar.a(String.valueOf(i));
            if (t.a((h) i.a(a.d().h())).c() > 500) {
                t.a((h) i.a(a.d().h())).b("0");
            }
            t.a((h) i.a(a.d().h())).a(nVar);
        } catch (Exception unused) {
        }
        return nVar;
    }

    private static String b(List<CampaignEx> list, int i) {
        StringBuilder sb = new StringBuilder("key=2000041&iex=");
        sb.append(i);
        sb.append("&cal=");
        String sb2 = sb.toString();
        try {
            JSONArray jSONArray = new JSONArray();
            for (CampaignEx a2 : list) {
                jSONArray.put(a(a2));
            }
            String b2 = com.mintegral.msdk.base.utils.a.b(jSONArray.toString());
            StringBuilder sb3 = new StringBuilder();
            sb3.append(sb2);
            sb3.append(b2);
            return sb3.toString();
        } catch (Exception e) {
            if (MIntegralConstans.DEBUG) {
                e.printStackTrace();
            }
            return sb2;
        }
    }

    private static JSONObject a(CampaignEx campaignEx) {
        JSONObject jSONObject;
        if (campaignEx == null) {
            return null;
        }
        try {
            jSONObject = new JSONObject();
            try {
                jSONObject.put("campaignid", campaignEx.getId());
                jSONObject.put("packageName", campaignEx.getPackageName());
                jSONObject.put("title", campaignEx.getAppName());
                jSONObject.put("cta", campaignEx.getAdCall());
                jSONObject.put(CampaignEx.JSON_KEY_DESC, campaignEx.getAppDesc());
                jSONObject.put("image_url", campaignEx.getImageUrl());
                jSONObject.put(CampaignEx.JSON_KEY_IMPRESSION_URL, campaignEx.getImpressionURL());
                jSONObject.put(CampaignEx.JSON_KEY_ST_IEX, campaignEx.getIex());
                jSONObject.put(CampaignEx.JSON_KEY_ST_TS, campaignEx.getTs());
                jSONObject.put("label", campaignEx.getLabel());
                jSONObject.put("pkg_source", campaignEx.getPkgSource());
                return jSONObject;
            } catch (Exception e) {
                e = e;
            }
        } catch (Exception e2) {
            e = e2;
            jSONObject = null;
            if (!MIntegralConstans.DEBUG) {
                return jSONObject;
            }
            e.printStackTrace();
            return jSONObject;
        }
    }

    public static a b(String str) {
        if (d == null) {
            try {
                a f = a.f(com.mintegral.msdk.base.a.a.a.a().a(str));
                d = f;
                if (f != null) {
                    d.bb();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return d;
    }

    public static String c(String str) {
        if (str == null) {
            return "";
        }
        try {
            String a2 = com.mintegral.msdk.base.a.a.a.a().a(str);
            if (a2 == null) {
                a2 = "";
            }
            return a2;
        } catch (Exception e) {
            if (MIntegralConstans.DEBUG) {
                e.printStackTrace();
            }
            return "";
        }
    }

    public static void a(Context context, String str) {
        try {
            Map all = context.getSharedPreferences("mintegral", 0).getAll();
            for (String str2 : all.keySet()) {
                StringBuilder sb = new StringBuilder();
                sb.append(str);
                sb.append(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
                if (str2.startsWith(sb.toString())) {
                    c.put(str2, d.a((String) all.get(str2)));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static d c(String str, String str2) {
        if (TextUtils.isEmpty(str)) {
            str = a.d().j();
        }
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
        sb.append(str2);
        String sb2 = sb.toString();
        d dVar = null;
        if (c.containsKey(sb2)) {
            return (d) c.get(sb2);
        }
        try {
            d a2 = d.a(com.mintegral.msdk.base.a.a.a.a().a(sb2));
            try {
                c.put(sb2, a2);
                return a2;
            } catch (Exception e) {
                e = e;
                dVar = a2;
                e.printStackTrace();
                return dVar;
            }
        } catch (Exception e2) {
            e = e2;
            e.printStackTrace();
            return dVar;
        }
    }

    public static void a(String str, String str2, String str3) {
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
        sb.append(str2);
        String sb2 = sb.toString();
        com.mintegral.msdk.base.a.a.a.a().a(sb2, str3);
        c.put(sb2, d.a(str3));
    }

    public static a b() {
        a aVar = new a();
        aVar.e("US");
        aVar.aJ();
        aVar.aL();
        aVar.aO();
        aVar.aR();
        aVar.aS();
        aVar.aG();
        aVar.aI();
        aVar.aE();
        aVar.aB();
        aVar.al();
        aVar.ax();
        aVar.az();
        aVar.ah();
        aVar.aj();
        aVar.d("正在下载中，请去通知栏查看下载进度");
        aVar.b("mintegral");
        aVar.y();
        aVar.ar();
        aVar.af();
        aVar.ab();
        aVar.X();
        aVar.Z();
        aVar.av();
        aVar.an();
        aVar.A();
        aVar.U();
        aVar.P();
        aVar.R();
        aVar.ap();
        aVar.C();
        aVar.E();
        aVar.H();
        aVar.c("pid");
        aVar.J();
        aVar.L();
        aVar.N();
        aVar.at();
        aVar.n();
        aVar.l();
        aVar.p();
        aVar.r();
        aVar.t();
        aVar.w();
        aVar.a(com.mintegral.msdk.b.a.b.f2490a);
        aVar.i();
        aVar.g();
        aVar.c();
        return aVar;
    }
}
