package com.mintegral.msdk.b;

import android.content.Context;
import android.content.SharedPreferences.Editor;
import android.text.TextUtils;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.base.common.a;
import com.mintegral.msdk.base.common.net.d;
import com.mintegral.msdk.base.common.net.l;
import com.mintegral.msdk.base.utils.CommonMD5;
import com.mintegral.msdk.base.utils.g;
import com.mintegral.msdk.c.b;
import org.json.JSONArray;
import org.json.JSONObject;

/* compiled from: SettingRequestController */
public class c {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public static final String f2492a = "c";

    public final void a(final Context context, final String str, final String str2) {
        if (context != null) {
            com.mintegral.msdk.b.a.c cVar = new com.mintegral.msdk.b.a.c(context);
            l lVar = new l();
            lVar.a("app_id", str);
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append(str2);
            lVar.a("sign", CommonMD5.getMD5(sb.toString()));
            lVar.a("jm_a", b.a(context).c());
            StringBuilder sb2 = new StringBuilder();
            sb2.append(b.a(context).a());
            lVar.a("jm_n", sb2.toString());
            lVar.a("launcher", b.a(context).b());
            cVar.a(a.n, lVar, (d<?>) new com.mintegral.msdk.b.a.d() {
                public final void a(JSONObject jSONObject) {
                    if (jSONObject != null) {
                        try {
                            MIntegralConstans.ALLOW_APK_DOWNLOAD = jSONObject.optBoolean("aa");
                            Editor edit = com.mintegral.msdk.base.controller.a.d().h().getApplicationContext().getSharedPreferences("cv", 0).edit();
                            edit.clear();
                            edit.commit();
                            jSONObject.put("current_time", System.currentTimeMillis());
                            b.a();
                            b.b(str, jSONObject.toString());
                            new Thread(new Runnable() {
                                public final void run() {
                                    com.mintegral.msdk.base.controller.a.e();
                                }
                            }).start();
                        } catch (Exception e) {
                            e.printStackTrace();
                            return;
                        }
                    } else {
                        g.d(c.f2492a, "app setting is null");
                    }
                    new com.mintegral.msdk.base.common.d.b(context).a();
                    c.this.b(context, str, str2);
                }

                public final void a(String str) {
                    new com.mintegral.msdk.base.common.d.b(context).a();
                    c.this.b(context, str, str2);
                    String a2 = c.f2492a;
                    StringBuilder sb = new StringBuilder("get app setting error");
                    sb.append(str);
                    g.d(a2, sb.toString());
                }
            });
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x0090  */
    /* JADX WARNING: Removed duplicated region for block: B:22:? A[RETURN, SYNTHETIC] */
    public final void a(Context context, final String str, String str2, final String str3) {
        if (TextUtils.isEmpty(str) || TextUtils.isEmpty(str2)) {
            str = com.mintegral.msdk.base.controller.a.d().j();
            str2 = com.mintegral.msdk.base.controller.a.d().k();
        }
        b.a();
        a b = b.b(str);
        boolean z = true;
        if (b.a(str) && b.a(str, 1, str3)) {
            new c().a(com.mintegral.msdk.base.controller.a.d().h(), str, com.mintegral.msdk.base.controller.a.d().k());
        }
        d c = b.c(str, str3);
        if (!(b == null || c == null)) {
            long aC = b.aC() * 1000;
            long currentTimeMillis = System.currentTimeMillis();
            long w = c.w() + aC;
            if (w > currentTimeMillis) {
                String str4 = b.f2491a;
                StringBuilder sb = new StringBuilder("unit setting  nexttime is not ready  [settingNextRequestTime= ");
                sb.append(w);
                sb.append(" currentTime = ");
                sb.append(currentTimeMillis);
                sb.append(RequestParameters.RIGHT_BRACKETS);
                g.b(str4, sb.toString());
                z = false;
                if (!z) {
                    b.a();
                    if (b.a(str, 2, str3)) {
                        com.mintegral.msdk.b.a.c cVar = new com.mintegral.msdk.b.a.c(context);
                        l lVar = new l();
                        StringBuilder sb2 = new StringBuilder(RequestParameters.LEFT_BRACKETS);
                        sb2.append(str3);
                        sb2.append(RequestParameters.RIGHT_BRACKETS);
                        lVar.a("unit_ids", sb2.toString());
                        lVar.a("app_id", str);
                        StringBuilder sb3 = new StringBuilder();
                        sb3.append(str);
                        sb3.append(str2);
                        lVar.a("sign", CommonMD5.getMD5(sb3.toString()));
                        cVar.a(a.n, lVar, (d<?>) new com.mintegral.msdk.b.a.d() {
                            public final void a(JSONObject jSONObject) {
                                if (jSONObject != null) {
                                    try {
                                        JSONArray optJSONArray = jSONObject.optJSONArray("unitSetting");
                                        if (optJSONArray != null && optJSONArray.length() > 0) {
                                            JSONObject optJSONObject = optJSONArray.optJSONObject(0);
                                            optJSONObject.put("current_time", System.currentTimeMillis());
                                            b.a();
                                            b.a(str, str3, optJSONObject.toString());
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                } else {
                                    g.d(c.f2492a, "UNIT SETTING IS NULL");
                                }
                            }

                            public final void a(String str) {
                                String a2 = c.f2492a;
                                StringBuilder sb = new StringBuilder("GET UNITID SETTING ERROR");
                                sb.append(str);
                                g.d(a2, sb.toString());
                            }
                        });
                        return;
                    }
                    return;
                }
                return;
            }
        }
        g.b(b.f2491a, "unit setting timeout or not exists");
        if (!z) {
        }
    }

    private void c(Context context, String str, String str2) {
        if (context != null) {
            com.mintegral.msdk.b.a.a aVar = new com.mintegral.msdk.b.a.a(context);
            l lVar = new l();
            lVar.a("app_id", str);
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append(str2);
            lVar.a("sign", CommonMD5.getMD5(sb.toString()));
            aVar.a(a.o, lVar, (d<?>) new com.mintegral.msdk.b.a.d() {
                public final void a(JSONObject jSONObject) {
                    if (jSONObject != null) {
                        try {
                            String optString = jSONObject.optString("system_id");
                            if (!TextUtils.isEmpty(optString) && !TextUtils.equals(a.D, optString)) {
                                a.D = optString;
                                com.mintegral.msdk.base.a.a.a.a().a("sys_id", a.D);
                            }
                            String optString2 = jSONObject.optString("sysbkup_id");
                            if (!TextUtils.isEmpty(optString2) && !TextUtils.equals(a.E, optString2)) {
                                a.E = optString2;
                                com.mintegral.msdk.base.a.a.a.a().a("bkup_id", a.E);
                            }
                        } catch (Exception e) {
                            if (MIntegralConstans.DEBUG) {
                                e.printStackTrace();
                            }
                        }
                    } else {
                        g.d(c.f2492a, "custom id return data null");
                    }
                }

                public final void a(String str) {
                    String a2 = c.f2492a;
                    StringBuilder sb = new StringBuilder("get custom id error");
                    sb.append(str);
                    g.d(a2, sb.toString());
                }
            });
        }
    }

    public final void b(Context context, String str, String str2) {
        if (b.a() != null) {
            a b = b.b(str);
            if (b != null && b.bg() == 1) {
                c(context, str, str2);
                return;
            }
        }
        if (TextUtils.isEmpty(a.D)) {
            String a2 = com.mintegral.msdk.base.a.a.a.a().a("sys_id");
            a.D = a2;
            if (TextUtils.isEmpty(a2)) {
                c(context, str, str2);
            }
        }
    }
}
