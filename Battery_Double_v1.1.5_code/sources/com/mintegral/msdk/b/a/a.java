package com.mintegral.msdk.b.a;

import android.content.Context;
import android.os.Build.VERSION;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.mintegral.msdk.base.common.net.a.d;
import com.mintegral.msdk.base.common.net.l;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.utils.c;
import com.mintegral.msdk.out.MTGConfiguration;
import com.tapjoy.TapjoyConstants;

/* compiled from: CustomIdRequest */
public final class a extends com.mintegral.msdk.base.common.net.a {
    public a(Context context) {
        super(context);
    }

    /* access modifiers changed from: protected */
    public final void a(l lVar) {
        super.a(lVar);
        lVar.a(TapjoyConstants.TJC_PLATFORM, "1");
        lVar.a(TapjoyConstants.TJC_DEVICE_OS_VERSION_NAME, VERSION.RELEASE);
        lVar.a(CampaignEx.JSON_KEY_PACKAGE_NAME, c.n(this.b));
        lVar.a("app_version_name", c.j(this.b));
        StringBuilder sb = new StringBuilder();
        sb.append(c.i(this.b));
        lVar.a("app_version_code", sb.toString());
        lVar.a("model", c.c());
        lVar.a("brand", c.e());
        lVar.a("gaid", c.k());
        lVar.a(RequestParameters.NETWORK_MNC, c.b());
        lVar.a(RequestParameters.NETWORK_MCC, c.a());
        int p = c.p(this.b);
        lVar.a("network_type", String.valueOf(p));
        lVar.a("network_str", c.a(this.b, p));
        lVar.a(TapjoyConstants.TJC_DEVICE_TIMEZONE, c.h());
        lVar.a("useragent", c.f());
        lVar.a("sdk_version", MTGConfiguration.SDK_VERSION);
        lVar.a("gp_version", c.q(this.b));
        d.b(lVar);
    }
}
