package com.mintegral.msdk.b;

import android.text.TextUtils;
import com.mintegral.msdk.base.entity.CampaignEx;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import org.json.JSONArray;
import org.json.JSONObject;

/* compiled from: UnitSetting */
public final class d {
    private int A;
    private int B;
    private String C;
    private String D;

    /* renamed from: a reason: collision with root package name */
    private int f2497a;
    private int b;
    private int c;
    private int d = 30;
    private String e;
    private String f;
    private List<Integer> g;
    private long h;
    private int i = 1;
    private int j;
    private List<Integer> k;
    private int l;
    private int m;
    private int n;
    private int o;
    private double p;
    private String q;
    private int r;
    private int s;
    private int t;
    private long u;
    private String v;
    private int w;
    private long x;
    private long y;
    private int z;

    public final int a() {
        return this.B;
    }

    public final double b() {
        return this.p;
    }

    public final String c() {
        return this.q;
    }

    public final int d() {
        return this.z;
    }

    public final int e() {
        return this.A;
    }

    public final long f() {
        return this.x;
    }

    public final long g() {
        return this.y;
    }

    public final String h() {
        return this.v;
    }

    public final int i() {
        return this.w;
    }

    public final long j() {
        return this.u;
    }

    public final int k() {
        return this.s;
    }

    public final int l() {
        return this.t;
    }

    public final int m() {
        if (this.d <= 0 || this.d > 100) {
            this.d = 30;
        }
        return this.d;
    }

    public final int n() {
        return this.o;
    }

    public final String o() {
        return this.D;
    }

    public final String p() {
        return this.C;
    }

    public final int q() {
        return this.m;
    }

    public final int r() {
        return this.n;
    }

    public final String toString() {
        String str = "";
        if (this.g != null && this.g.size() > 0) {
            for (Integer num : this.g) {
                StringBuilder sb = new StringBuilder();
                sb.append(str);
                sb.append(num);
                sb.append(",");
                str = sb.toString();
            }
        }
        StringBuilder sb2 = new StringBuilder("offset = ");
        sb2.append(this.i);
        sb2.append(" unitId = ");
        sb2.append(this.e);
        sb2.append(" fbPlacementId = ");
        sb2.append(this.f);
        sb2.append(str);
        return sb2.toString();
    }

    public final String s() {
        return this.f;
    }

    public final List<Integer> t() {
        return this.g;
    }

    public final List<Integer> u() {
        return this.k;
    }

    public static d a(String str) {
        d dVar = null;
        try {
            if (TextUtils.isEmpty(str)) {
                return null;
            }
            d dVar2 = new d();
            try {
                JSONObject jSONObject = new JSONObject(str);
                dVar2.e = jSONObject.optString("unitId");
                JSONArray optJSONArray = jSONObject.optJSONArray("adSourceList");
                if (optJSONArray != null && optJSONArray.length() > 0) {
                    ArrayList arrayList = new ArrayList();
                    for (int i2 = 0; i2 < optJSONArray.length(); i2++) {
                        arrayList.add(Integer.valueOf(optJSONArray.optInt(i2)));
                    }
                    dVar2.g = arrayList;
                }
                JSONArray optJSONArray2 = jSONObject.optJSONArray("ad_source_timeout");
                if (optJSONArray2 != null && optJSONArray2.length() > 0) {
                    ArrayList arrayList2 = new ArrayList();
                    for (int i3 = 0; i3 < optJSONArray2.length(); i3++) {
                        arrayList2.add(Integer.valueOf(optJSONArray2.optInt(i3)));
                    }
                    dVar2.k = arrayList2;
                }
                dVar2.l = jSONObject.optInt("tpqn");
                dVar2.m = jSONObject.optInt("aqn");
                dVar2.n = jSONObject.optInt("acn");
                dVar2.o = jSONObject.optInt("wt");
                dVar2.j = jSONObject.optInt(CampaignEx.JSON_KEY_TTC_TYPE);
                dVar2.f = jSONObject.optString("fbPlacementId");
                dVar2.h = jSONObject.optLong("current_time");
                dVar2.i = jSONObject.optInt("offset");
                dVar2.C = jSONObject.optString("admobUnitId");
                dVar2.D = jSONObject.optString("myTargetSlotId");
                dVar2.u = jSONObject.optLong("dlct", 3600);
                dVar2.s = jSONObject.optInt("autoplay", 1);
                dVar2.t = jSONObject.optInt("dlnet", 2);
                dVar2.v = jSONObject.optString("no_offer");
                dVar2.w = jSONObject.optInt("cb_type");
                dVar2.x = jSONObject.optLong("clct", 86400);
                dVar2.y = jSONObject.optLong("clcq", 300);
                dVar2.z = jSONObject.optInt("ready_rate", 100);
                dVar2.A = jSONObject.optInt("content", 1);
                dVar2.B = jSONObject.optInt("impt", 0);
                dVar2.p = jSONObject.optDouble("cbp", 1.0d);
                dVar2.r = jSONObject.optInt("icon_type", 1);
                dVar2.q = jSONObject.optString("no_ads_url", "");
                dVar2.f2497a = jSONObject.optInt("playclosebtn_tm", -1);
                dVar2.b = jSONObject.optInt("play_ctdown", 0);
                dVar2.c = jSONObject.optInt("close_alert", 0);
                dVar2.d = jSONObject.optInt("intershowlimit", 30);
                return dVar2;
            } catch (Exception e2) {
                e = e2;
                dVar = dVar2;
                e.printStackTrace();
                return dVar;
            }
        } catch (Exception e3) {
            e = e3;
            e.printStackTrace();
            return dVar;
        }
    }

    public final JSONObject v() {
        JSONObject jSONObject = new JSONObject();
        try {
            if (this.g != null && this.g.size() > 0) {
                int size = this.g.size();
                JSONArray jSONArray = new JSONArray();
                for (int i2 = 0; i2 < size; i2++) {
                    jSONArray.put(this.g.get(i2));
                }
                jSONObject.put("adSourceList", jSONArray);
            }
            if (this.k != null && this.k.size() > 0) {
                int size2 = this.k.size();
                JSONArray jSONArray2 = new JSONArray();
                for (int i3 = 0; i3 < size2; i3++) {
                    jSONArray2.put(this.g.get(i3));
                }
                jSONObject.put("ad_source_timeout", jSONArray2);
            }
            jSONObject.put("tpqn", this.l);
            jSONObject.put("aqn", this.m);
            jSONObject.put("acn", this.n);
            jSONObject.put("wt", this.o);
            jSONObject.put(CampaignEx.JSON_KEY_TTC_TYPE, this.j);
            jSONObject.put("fbPlacementId", this.f);
            jSONObject.put("current_time", this.h);
            jSONObject.put("offset", this.i);
            jSONObject.put("admobUnitId", this.C);
            jSONObject.put("myTargetSlotId", this.D);
            jSONObject.put("dlct", this.u);
            jSONObject.put("autoplay", this.s);
            jSONObject.put("dlnet", this.t);
            jSONObject.put("no_offer", this.v);
            jSONObject.put("cb_type", this.w);
            jSONObject.put("clct", this.x);
            jSONObject.put("clcq", this.y);
            jSONObject.put("ready_rate", this.z);
            jSONObject.put("content", this.A);
            jSONObject.put("impt", this.B);
            jSONObject.put("cbp", this.p);
            jSONObject.put("icon_type", this.r);
            jSONObject.put("no_ads_url", this.q);
            jSONObject.put("playclosebtn_tm", this.f2497a);
            jSONObject.put("play_ctdown", this.b);
            jSONObject.put("close_alert", this.c);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return jSONObject;
    }

    public final long w() {
        return this.h;
    }

    public final int x() {
        return this.i;
    }

    public static d b(String str) {
        d dVar = new d();
        ArrayList arrayList = new ArrayList();
        arrayList.add(Integer.valueOf(1));
        ArrayList arrayList2 = new ArrayList();
        arrayList2.add(Integer.valueOf(8));
        arrayList2.add(Integer.valueOf(8));
        dVar.j = 2;
        dVar.i = 1;
        dVar.e = str;
        dVar.g = arrayList;
        dVar.k = arrayList2;
        dVar.l = 1;
        dVar.n = -2;
        dVar.m = -2;
        dVar.o = 5;
        dVar.u = 3600;
        dVar.t = 2;
        dVar.s = 1;
        dVar.z = 100;
        dVar.A = 1;
        dVar.B = 0;
        return dVar;
    }

    public static d c(String str) {
        d dVar = new d();
        try {
            ArrayList arrayList = new ArrayList();
            arrayList.add(Integer.valueOf(1));
            dVar.g = arrayList;
            ArrayList arrayList2 = new ArrayList();
            arrayList2.add(Integer.valueOf(30));
            dVar.k = arrayList2;
            dVar.e = str;
            dVar.j = 2;
            dVar.i = 1;
            dVar.l = 1;
            dVar.n = -2;
            dVar.m = -2;
            dVar.o = 5;
            dVar.u = 3600;
            dVar.t = 2;
            dVar.s = 3;
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return dVar;
    }

    public static d d(String str) {
        d dVar = new d();
        try {
            dVar.e = str;
            dVar.j = 2;
            dVar.i = 1;
            dVar.l = 1;
            dVar.n = 10;
            dVar.m = 20;
            dVar.o = 5;
            dVar.w = 3;
            dVar.x = 86400;
            dVar.y = 300;
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return dVar;
    }

    public static d e(String str) {
        d dVar = new d();
        try {
            dVar.e = str;
            dVar.j = 2;
            dVar.i = 1;
            dVar.l = 1;
            dVar.o = 5;
            dVar.j = 2;
            dVar.n = 1;
            dVar.m = 1;
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return dVar;
    }

    public static d f(String str) {
        d b2 = b(str);
        try {
            b2.p = 1.0d;
            b2.r = 1;
            b2.q = "";
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return b2;
    }

    public static Queue<Integer> a(List<Integer> list) {
        LinkedList linkedList = new LinkedList();
        if (list != null) {
            try {
                if (list.size() > 0) {
                    for (Integer num : list) {
                        if (num != null) {
                            linkedList.add(num);
                        }
                    }
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        return linkedList;
    }

    public final Queue<Integer> b(List<Integer> list) {
        LinkedList linkedList = new LinkedList();
        try {
            if (this.g != null && this.g.size() > 0) {
                for (Integer num : list) {
                    if (num != null) {
                        linkedList.add(Integer.valueOf(num.intValue() * 1000));
                    }
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return linkedList;
    }
}
