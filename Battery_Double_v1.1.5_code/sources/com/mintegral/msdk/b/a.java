package com.mintegral.msdk.b;

import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;
import com.explorestack.iab.vast.VastError;
import com.facebook.share.internal.MessengerShareContentUtility;
import com.integralads.avid.library.inmobi.utils.AvidJSONUtil;
import com.mintegral.msdk.MIntegralConstans;
import com.mintegral.msdk.b.a.b;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.utils.c;
import com.mintegral.msdk.base.utils.m;
import com.mintegral.msdk.base.utils.s;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: Setting */
public final class a {
    private long A;
    private long B;
    private int C;
    private int D;
    private int E;
    private String F;
    private String G;
    private int H;
    private List<CampaignEx> I;
    private List<com.mintegral.msdk.base.entity.a> J;
    private LinkedList<String> K;
    private int L = 0;
    private int M = 3600;
    private int N;
    private List<String> O;
    private int P;
    private int Q;
    private int R;
    private int S;
    private String T;
    private String U;
    private int V = 1;
    private int W = 1;
    private int X = 1;
    private int Y = 0;
    private int Z = 1;

    /* renamed from: a reason: collision with root package name */
    private int f2488a = 0;
    private int aA = CampaignEx.TTC_CT_DEFAULT_VALUE;
    private int aB = 0;
    private String aC = "";
    private String aD = "";
    private String aE = "";
    private String aF = "";
    private String aG = "";
    private Map<String, C0048a> aH;
    private int aI = 0;
    private String aa = "";
    private int ab = 0;
    private int ac = 2;
    private String ad;
    private int ae = 86400;
    private String af = "LdxThdi1WBK\\/WgfPhbxQYkeXHBPwHZKAJ7eXHM==";
    private String ag = "LdxThdi1WBK\\/WgfPhbxQYkeXHBPwHZKsYFh=";
    private int ah;
    private int ai = 1;
    private long aj;
    private long ak;
    private int al;
    private int am;
    private long an;
    private int ao = 3;
    private int ap;
    private String aq = "";
    private String ar = "";
    private String as = "";
    private String at = "";
    private String au = "";
    private int av = 0;
    private int aw = 21600;
    private int ax = 2;
    private int ay = 0;
    private int az = 0;
    private String b;
    private long c;
    private int d;
    private long e = 86400;
    private int f;
    private boolean g;
    private Map<String, String> h;
    private boolean i;
    private long j;
    private boolean k;
    private long l;
    private long m;
    private long n;
    private boolean o;
    private int p;
    private int q;
    private int r;
    private long s;
    private int t;
    private int u;
    private int v;
    private int w;
    private String x;
    private long y;
    private int z;

    /* renamed from: com.mintegral.msdk.b.a$a reason: collision with other inner class name */
    /* compiled from: Setting */
    public static class C0048a {

        /* renamed from: a reason: collision with root package name */
        private List<String> f2489a;
        private List<String> b;
        private List<String> c;
        private List<String> d;

        public final List<String> a() {
            return this.f2489a;
        }

        public final List<String> b() {
            return this.b;
        }

        public final List<String> c() {
            return this.c;
        }

        public final List<String> d() {
            return this.d;
        }

        public final void a(JSONObject jSONObject) {
            try {
                JSONArray optJSONArray = jSONObject.optJSONArray(AvidJSONUtil.KEY_X);
                if (optJSONArray != null) {
                    this.f2489a = m.a(optJSONArray);
                }
                JSONArray optJSONArray2 = jSONObject.optJSONArray(AvidJSONUtil.KEY_Y);
                if (optJSONArray2 != null) {
                    this.b = m.a(optJSONArray2);
                }
                JSONArray optJSONArray3 = jSONObject.optJSONArray("width");
                if (optJSONArray3 != null) {
                    this.c = m.a(optJSONArray3);
                }
                JSONArray optJSONArray4 = jSONObject.optJSONArray("height");
                if (optJSONArray4 != null) {
                    this.d = m.a(optJSONArray4);
                }
            } catch (Exception e) {
                if (MIntegralConstans.DEBUG) {
                    e.printStackTrace();
                }
            }
        }
    }

    public final List<com.mintegral.msdk.base.entity.a> a() {
        return this.J;
    }

    public final int b() {
        return this.f2488a;
    }

    public final void c() {
        this.f2488a = 0;
    }

    public final String d() {
        return this.af;
    }

    public final String e() {
        return this.ag;
    }

    public final int f() {
        return this.ae;
    }

    public final void g() {
        this.ae = 86400;
    }

    public final int h() {
        return this.ac;
    }

    public final void i() {
        this.ac = 2;
    }

    public final String j() {
        return this.ad;
    }

    public final int k() {
        return this.V;
    }

    public final void l() {
        this.V = 1;
    }

    public final int m() {
        return this.W;
    }

    public final void n() {
        this.W = 1;
    }

    public final int o() {
        return this.X;
    }

    public final void p() {
        this.X = 1;
    }

    public final int q() {
        return this.Y;
    }

    public final void r() {
        this.Y = 0;
    }

    public final int s() {
        return this.Z;
    }

    public final void t() {
        this.Z = 1;
    }

    public final String u() {
        return this.aa;
    }

    public final void a(String str) {
        this.aa = str;
    }

    public final int v() {
        return this.ab;
    }

    public final void w() {
        this.ab = -1;
    }

    public final int x() {
        return this.H;
    }

    public final void y() {
        this.H = 1;
    }

    public final String z() {
        return this.G;
    }

    public final void b(String str) {
        this.G = str;
    }

    public final void A() {
        this.ai = 1;
    }

    public final int B() {
        return this.ah;
    }

    public final void C() {
        this.ah = 1;
    }

    public final int D() {
        return this.N;
    }

    public final void E() {
        this.N = VastError.ERROR_CODE_UNKNOWN;
    }

    public final List<String> F() {
        return this.O;
    }

    public final int G() {
        return this.P;
    }

    public final void H() {
        this.P = 20;
    }

    public final int I() {
        return this.Q;
    }

    public final void J() {
        this.Q = 1;
    }

    public final int K() {
        return this.R;
    }

    public final void L() {
        this.R = 1;
    }

    public final int M() {
        return this.S;
    }

    public final void N() {
        this.S = 1;
    }

    public final void c(String str) {
        this.T = str;
    }

    public final int O() {
        return this.C;
    }

    public final void P() {
        this.C = 1;
    }

    public final int Q() {
        return this.D;
    }

    public final void R() {
        this.D = 86400;
    }

    public final List<CampaignEx> S() {
        return this.I;
    }

    public final int T() {
        return this.E;
    }

    public final void U() {
        this.E = 1;
    }

    public final String V() {
        return this.F;
    }

    public final void d(String str) {
        this.F = str;
    }

    public final int W() {
        return this.z;
    }

    public final void X() {
        this.z = 1;
    }

    public final long Y() {
        return this.A;
    }

    public final void Z() {
        this.A = 604800;
    }

    public final long aa() {
        return this.B;
    }

    public final void ab() {
        this.B = 43200;
    }

    public final long ac() {
        return this.y * 1000;
    }

    public final int ad() {
        return this.w;
    }

    public final int ae() {
        return this.t;
    }

    public final void af() {
        this.t = 100;
    }

    public final int ag() {
        return this.u;
    }

    public final void ah() {
        this.u = 1;
    }

    public final int ai() {
        return this.v;
    }

    public final void aj() {
        this.v = 1;
    }

    public final long ak() {
        return this.s;
    }

    public final void al() {
        this.s = 7200;
    }

    public final int am() {
        return this.ao;
    }

    public final void an() {
        this.ao = 3;
    }

    public final int ao() {
        return this.ap;
    }

    public final void ap() {
        this.ap = 259200;
    }

    public final long aq() {
        return this.an;
    }

    public final void ar() {
        this.an = 1800;
    }

    public final int as() {
        return this.al;
    }

    public final void at() {
        this.al = 1;
    }

    public final int au() {
        return this.am;
    }

    public final void av() {
        this.am = 1;
    }

    public final long aw() {
        return this.aj * 1000;
    }

    public final void ax() {
        this.aj = 20;
    }

    public final long ay() {
        return this.ak * 1000;
    }

    public final void az() {
        this.ak = 10;
    }

    public final boolean aA() {
        return this.o;
    }

    public final void aB() {
        this.o = false;
    }

    public final long aC() {
        return this.n;
    }

    public final boolean aD() {
        return this.k;
    }

    public final void aE() {
        this.k = false;
    }

    public final long aF() {
        return this.l;
    }

    public final void aG() {
        this.l = 3600;
    }

    public final long aH() {
        return this.m;
    }

    public final void aI() {
        this.m = 0;
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder("cc=");
        sb.append(this.b);
        sb.append(" upal=");
        sb.append(this.c);
        sb.append(" cfc=");
        sb.append(this.d);
        sb.append(" getpf=");
        sb.append(this.e);
        sb.append(" uplc=");
        sb.append(this.f);
        sb.append(" rurl=");
        sb.append(this.k);
        return sb.toString();
    }

    public final void e(String str) {
        this.b = str;
    }

    public final void aJ() {
        this.c = 86400;
    }

    public final int aK() {
        return this.d;
    }

    public final void aL() {
        this.d = 1;
    }

    public final long aM() {
        return this.e;
    }

    public final int aN() {
        return this.f;
    }

    public final void aO() {
        this.f = 1;
    }

    public final String aP() {
        return this.U;
    }

    public static a f(String str) {
        a aVar;
        Exception e2;
        long j2;
        long j3;
        List<CampaignEx> list;
        ArrayList arrayList = null;
        try {
            if (TextUtils.isEmpty(str)) {
                return null;
            }
            JSONObject jSONObject = new JSONObject(str);
            aVar = new a();
            try {
                aVar.b = jSONObject.optString("cc");
                aVar.F = jSONObject.optString("apk_toast", "正在下载中，请去通知栏查看下载进度");
                aVar.G = jSONObject.optString("mv_wildcard", "mintegral");
                aVar.c = jSONObject.optLong("upal");
                aVar.d = jSONObject.optInt("cfc");
                aVar.e = jSONObject.optLong("getpf");
                aVar.f = jSONObject.optInt("uplc");
                aVar.g = jSONObject.optBoolean("aa");
                aVar.j = jSONObject.optLong("current_time");
                aVar.i = jSONObject.optBoolean("cfb");
                aVar.m = jSONObject.optLong("awct");
                if (jSONObject.optLong(CampaignEx.JSON_KEY_PLCT) == 0) {
                    j2 = 3600;
                } else {
                    j2 = jSONObject.optLong(CampaignEx.JSON_KEY_PLCT);
                }
                aVar.l = j2;
                aVar.k = jSONObject.optBoolean("rurl");
                aVar.n = jSONObject.optLong("uct");
                aVar.o = jSONObject.optBoolean("ujds");
                aVar.p = jSONObject.optInt("n2");
                aVar.q = jSONObject.optInt("n3");
                aVar.H = jSONObject.optInt("is_startup_crashsystem");
                aVar.r = jSONObject.optInt("n4", 1800);
                aVar.t = jSONObject.optInt("pcrn");
                if (jSONObject.optLong(CampaignEx.JSON_KEY_PLCTB) == 0) {
                    j3 = 7200;
                } else {
                    j3 = jSONObject.optLong(CampaignEx.JSON_KEY_PLCTB);
                }
                aVar.s = j3;
                aVar.al = jSONObject.optInt("upmi");
                aVar.am = jSONObject.optInt("upaid");
                aVar.t = jSONObject.optInt("pcrn", 100);
                aVar.u = jSONObject.optInt("wicon", 2);
                aVar.v = jSONObject.optInt("wreq", 2);
                aVar.w = jSONObject.optInt("opent", 1);
                aVar.an = jSONObject.optLong("sfct", 1800);
                aVar.x = jSONObject.optString("t_vba");
                aVar.y = jSONObject.optLong("tcct", 21600000);
                aVar.z = jSONObject.optInt("dlrf", 1);
                aVar.A = (long) jSONObject.optInt("dlrfct", CampaignEx.TTC_CT_DEFAULT_VALUE);
                aVar.B = (long) jSONObject.optInt("pcct", 43200);
                aVar.ao = jSONObject.optInt("pctn", 3);
                aVar.ai = jSONObject.optInt("ilrf", 1);
                aVar.U = jSONObject.optString("pw", "");
                aVar.ah = jSONObject.optInt("dlapk", 1);
                aVar.W = jSONObject.optInt("upgd", 1);
                aVar.V = jSONObject.optInt("upsrl", 1);
                aVar.X = jSONObject.optInt("updevid", 1);
                aVar.Y = jSONObject.optInt("sc", 0);
                aVar.Z = jSONObject.optInt("up_tips", 1);
                aVar.ab = jSONObject.optInt("iseu", -1);
                aVar.aa = jSONObject.optString("up_tips_url", b.f2490a);
                aVar.ac = jSONObject.optInt("jmc", 2);
                aVar.ae = jSONObject.optInt("jmct", 86400);
                aVar.ad = jSONObject.optString("jm_unit");
                aVar.ag = jSONObject.optString("cdai");
                aVar.af = jSONObject.optString("csdai");
                aVar.M = jSONObject.optInt("clptm", 3600);
                aVar.L = jSONObject.optInt("clptype", 0);
                try {
                    JSONArray optJSONArray = jSONObject.optJSONArray("clpcode");
                    if (optJSONArray != null && optJSONArray.length() > 0) {
                        LinkedList<String> linkedList = new LinkedList<>();
                        for (int i2 = 0; i2 < optJSONArray.length(); i2++) {
                            linkedList.add(optJSONArray.optString(i2));
                        }
                        aVar.K = linkedList;
                    }
                } catch (Throwable th) {
                    th.printStackTrace();
                }
                long optLong = jSONObject.optLong("pcto");
                if (optLong == 0) {
                    aVar.aj = 20;
                } else {
                    aVar.aj = optLong;
                }
                long optLong2 = jSONObject.optLong("tcto");
                if (optLong2 == 0) {
                    aVar.ak = 10;
                } else {
                    aVar.ak = optLong2;
                    JSONArray optJSONArray2 = jSONObject.optJSONArray("jt");
                    if (optJSONArray2 != null && optJSONArray2.length() > 0) {
                        HashMap hashMap = new HashMap();
                        for (int i3 = 0; i3 < optJSONArray2.length(); i3++) {
                            JSONObject optJSONObject = optJSONArray2.optJSONObject(i3);
                            hashMap.put(optJSONObject.optString("domain"), optJSONObject.optString("format"));
                        }
                        aVar.h = hashMap;
                    }
                }
                aVar.C = jSONObject.optInt("plc", 3);
                aVar.D = jSONObject.optInt("dut", 86400);
                aVar.E = jSONObject.optInt(CampaignEx.JSON_KEY_ST_IEX, 1);
                JSONArray optJSONArray3 = jSONObject.optJSONArray("cal");
                if (optJSONArray3 == null || optJSONArray3.length() <= 0) {
                    list = null;
                } else {
                    list = new ArrayList<>();
                    for (int i4 = 0; i4 < optJSONArray3.length(); i4++) {
                        String optString = optJSONArray3.optString(i4);
                        if (s.b(optString)) {
                            CampaignEx parseSettingCampaign = CampaignEx.parseSettingCampaign(new JSONObject(optString));
                            if (parseSettingCampaign != null) {
                                list.add(parseSettingCampaign);
                            }
                        }
                    }
                }
                if (list != null) {
                    aVar.I = list;
                }
                try {
                    JSONArray optJSONArray4 = jSONObject.optJSONArray("atf");
                    if (optJSONArray4 != null && optJSONArray4.length() > 0) {
                        arrayList = new ArrayList();
                        for (int i5 = 0; i5 < optJSONArray4.length(); i5++) {
                            String optString2 = optJSONArray4.optString(i5);
                            if (s.b(optString2)) {
                                JSONObject jSONObject2 = new JSONObject(optString2);
                                arrayList.add(new com.mintegral.msdk.base.entity.a(jSONObject2.optInt("adtype"), jSONObject2.optString("unitid")));
                            }
                        }
                    }
                    if (arrayList != null) {
                        aVar.J = arrayList;
                    }
                } catch (Exception e3) {
                    e3.printStackTrace();
                }
                aVar.ap = jSONObject.optInt("adct", 259200);
                aVar.N = jSONObject.optInt("pf", VastError.ERROR_CODE_UNKNOWN);
                aVar.P = jSONObject.optInt("pmax", 20);
                aVar.T = jSONObject.optString("pid");
                JSONArray optJSONArray5 = jSONObject.optJSONArray("pb");
                if (optJSONArray5 != null && optJSONArray5.length() > 0) {
                    ArrayList arrayList2 = new ArrayList();
                    for (int i6 = 0; i6 < optJSONArray5.length(); i6++) {
                        arrayList2.add(optJSONArray5.optString(i6));
                    }
                    aVar.O = arrayList2;
                }
                JSONObject optJSONObject2 = jSONObject.optJSONObject("pctrl");
                if (optJSONObject2 != null) {
                    aVar.Q = optJSONObject2.optInt(MessengerShareContentUtility.WEBVIEW_RATIO_FULL, 1);
                    aVar.R = optJSONObject2.optInt("add", 1);
                    aVar.S = optJSONObject2.optInt("delete", 1);
                }
                aVar.aq = jSONObject.optString("confirm_title", "");
                aVar.ar = jSONObject.optString("confirm_description", "");
                aVar.as = jSONObject.optString("confirm_t", "");
                aVar.at = jSONObject.optString("confirm_c_rv", "");
                aVar.au = jSONObject.optString("confirm_c_play", "");
                aVar.av = jSONObject.optInt("offercacheRate", 0);
                aVar.aw = jSONObject.optInt("offercachepacing", 21600);
                aVar.ax = jSONObject.optInt("useexpriedcacheoffer", 2);
                aVar.ay = jSONObject.optInt("retryoffer", 0);
                aVar.az = jSONObject.optInt("mapping_cache_rate", 0);
                aVar.aA = jSONObject.optInt("tokencachetime", CampaignEx.TTC_CT_DEFAULT_VALUE);
                aVar.aB = jSONObject.optInt("protect", 0);
                aVar.aC = jSONObject.optString("adchoice_icon", "");
                aVar.aE = jSONObject.optString("adchoice_link", "");
                aVar.aD = jSONObject.optString("adchoice_size", "");
                aVar.aG = jSONObject.optString("platform_logo", "");
                aVar.aF = jSONObject.optString("platform_name", "");
                aVar.aH = g(jSONObject.optString("cdnate_cfg", ""));
                aVar.f2488a = jSONObject.optInt("atrqt", 0);
                aVar.aI = jSONObject.optInt("iupdid", 0);
                return aVar;
            } catch (Exception e4) {
                e2 = e4;
                e2.printStackTrace();
                return aVar;
            }
        } catch (Exception e5) {
            e2 = e5;
            aVar = null;
            e2.printStackTrace();
            return aVar;
        }
    }

    public final long aQ() {
        return this.j;
    }

    public final void aR() {
        this.g = true;
    }

    public static String a(Context context, String str) {
        try {
            b.a();
            a b2 = b.b(com.mintegral.msdk.base.controller.a.d().j());
            if (b2 != null) {
                if (b2.h != null) {
                    String str2 = "";
                    String host = Uri.parse(str).getHost();
                    Iterator it = b2.h.entrySet().iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            break;
                        }
                        String str3 = (String) ((Entry) it.next()).getKey();
                        if (!TextUtils.isEmpty(host) && host.contains(str3)) {
                            String str4 = (String) b2.h.get(str3);
                            if (TextUtils.isEmpty(str4)) {
                                return "";
                            }
                            str2 = str4.replace("{gaid}", c.k());
                            if (str2.contains("{android_id}")) {
                                if (c.d(context) != null) {
                                    str2 = str2.replace("{android_id}", c.d(context));
                                }
                            } else if (str2.contains("{android_id_md5_upper}") && c.e(context) != null) {
                                str2 = str2.replace("{android_id_md5_upper}", c.e(context));
                            }
                        }
                    }
                    return str2;
                }
            }
            return "";
        } catch (Throwable unused) {
            return "";
        }
    }

    public final void aS() {
        this.i = true;
    }

    public final int aT() {
        return this.p;
    }

    public final int aU() {
        return this.q;
    }

    public static boolean aV() {
        try {
            b.a();
            a b2 = b.b(com.mintegral.msdk.base.controller.a.d().j());
            if (b2 != null) {
                return b2.g;
            }
            return true;
        } catch (Exception e2) {
            e2.printStackTrace();
            return true;
        }
    }

    public final String aW() {
        return this.aq;
    }

    public final String aX() {
        return this.ar;
    }

    public final String aY() {
        return this.as;
    }

    public final String aZ() {
        return this.at;
    }

    public final String ba() {
        return this.au;
    }

    public final void bb() {
        String language = Locale.getDefault().getLanguage();
        boolean z2 = true;
        if (!(!TextUtils.isEmpty(this.aq) && !TextUtils.isEmpty(this.ar) && !TextUtils.isEmpty(this.as) && !TextUtils.isEmpty(this.at))) {
            if (TextUtils.isEmpty(language) || !language.equals("zh")) {
                this.aq = "Confirm to close? ";
                this.ar = "You will not be rewarded after closing the window";
                this.as = "Close it";
                this.at = "Continue";
            } else {
                this.aq = "确认关闭？";
                this.ar = "关闭后您将不会获得任何奖励噢~ ";
                this.as = "确认关闭";
                this.at = "继续观看";
            }
        }
        if (TextUtils.isEmpty(this.aq) || TextUtils.isEmpty(this.ar) || TextUtils.isEmpty(this.as) || TextUtils.isEmpty(this.au)) {
            z2 = false;
        }
        if (!z2) {
            if (TextUtils.isEmpty(language) || !language.equals("zh")) {
                this.aq = "Confirm to close? ";
                this.ar = "You will not be rewarded after closing the window";
                this.as = "Close it";
                this.au = "Continue";
            } else {
                this.aq = "确认关闭？";
                this.ar = "关闭后您将不会获得任何奖励噢~ ";
                this.as = "确认关闭";
                this.au = "继续试玩";
            }
        }
    }

    public final String bc() {
        return this.aC;
    }

    public final String bd() {
        return this.aD;
    }

    public final String be() {
        return this.aE;
    }

    public final Map<String, C0048a> bf() {
        return this.aH;
    }

    private static Map<String, C0048a> g(String str) {
        try {
            if (TextUtils.isEmpty(str)) {
                return null;
            }
            HashMap hashMap = new HashMap();
            JSONObject jSONObject = new JSONObject(str);
            Iterator keys = jSONObject.keys();
            while (keys.hasNext()) {
                String str2 = (String) keys.next();
                C0048a aVar = new C0048a();
                JSONObject optJSONObject = jSONObject.optJSONObject(str2);
                if (optJSONObject != null) {
                    aVar.a(optJSONObject);
                }
                hashMap.put(str2, aVar);
            }
            return hashMap;
        } catch (JSONException e2) {
            if (MIntegralConstans.DEBUG) {
                e2.printStackTrace();
            }
            return null;
        } catch (Exception e3) {
            if (MIntegralConstans.DEBUG) {
                e3.printStackTrace();
            }
            return null;
        }
    }

    public final int bg() {
        return this.aI;
    }
}
