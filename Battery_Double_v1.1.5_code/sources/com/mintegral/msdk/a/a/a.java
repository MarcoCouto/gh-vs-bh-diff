package com.mintegral.msdk.a.a;

import com.mintegral.msdk.out.PreloadListener;
import java.lang.ref.WeakReference;

/* compiled from: PreloadListenerEx */
public final class a implements PreloadListener {

    /* renamed from: a reason: collision with root package name */
    WeakReference<PreloadListener> f2485a;
    private boolean b = false;
    private int c = 0;

    public final boolean a() {
        return this.b;
    }

    public final void b() {
        this.b = true;
    }

    public a(PreloadListener preloadListener) {
        if (preloadListener != null) {
            this.f2485a = new WeakReference<>(preloadListener);
        }
    }

    public final void onPreloadSucceed() {
        if (this.f2485a != null && this.f2485a.get() != null) {
            ((PreloadListener) this.f2485a.get()).onPreloadSucceed();
        }
    }

    public final void onPreloadFaild(String str) {
        if (this.f2485a != null && this.f2485a.get() != null) {
            ((PreloadListener) this.f2485a.get()).onPreloadFaild(str);
        }
    }
}
