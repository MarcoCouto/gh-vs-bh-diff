package com.mintegral.msdk.a;

import com.mintegral.msdk.out.AdMobClickListener;
import java.util.Map;

/* compiled from: PreloadController */
public class a {

    /* renamed from: a reason: collision with root package name */
    public static final String f2484a = "a";

    public static void a() {
    }

    public static void b() {
    }

    public static void a(Map<String, Object> map, int i, AdMobClickListener adMobClickListener) {
        try {
            Class cls = Class.forName("com.mintegral.msdk.mtgnative.f.a");
            Object newInstance = cls.newInstance();
            cls.getMethod("preload", new Class[]{Map.class, Integer.TYPE, AdMobClickListener.class}).invoke(newInstance, new Object[]{map, Integer.valueOf(i), adMobClickListener});
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void a(Map<String, Object> map) {
        try {
            Class cls = Class.forName("com.mintegral.msdk.appwall.service.HandlerProvider");
            Object newInstance = cls.newInstance();
            cls.getMethod("preload", new Class[]{Map.class}).invoke(newInstance, new Object[]{map});
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
