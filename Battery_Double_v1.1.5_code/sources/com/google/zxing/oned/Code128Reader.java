package com.google.zxing.oned;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.ChecksumException;
import com.google.zxing.DecodeHintType;
import com.google.zxing.FormatException;
import com.google.zxing.NotFoundException;
import com.google.zxing.Result;
import com.google.zxing.ResultPoint;
import com.google.zxing.common.BitArray;
import java.util.ArrayList;
import java.util.Map;

public final class Code128Reader extends OneDReader {
    private static final int CODE_CODE_A = 101;
    private static final int CODE_CODE_B = 100;
    private static final int CODE_CODE_C = 99;
    private static final int CODE_FNC_1 = 102;
    private static final int CODE_FNC_2 = 97;
    private static final int CODE_FNC_3 = 96;
    private static final int CODE_FNC_4_A = 101;
    private static final int CODE_FNC_4_B = 100;
    static final int[][] CODE_PATTERNS = {new int[]{2, 1, 2, 2, 2, 2}, new int[]{2, 2, 2, 1, 2, 2}, new int[]{2, 2, 2, 2, 2, 1}, new int[]{1, 2, 1, 2, 2, 3}, new int[]{1, 2, 1, 3, 2, 2}, new int[]{1, 3, 1, 2, 2, 2}, new int[]{1, 2, 2, 2, 1, 3}, new int[]{1, 2, 2, 3, 1, 2}, new int[]{1, 3, 2, 2, 1, 2}, new int[]{2, 2, 1, 2, 1, 3}, new int[]{2, 2, 1, 3, 1, 2}, new int[]{2, 3, 1, 2, 1, 2}, new int[]{1, 1, 2, 2, 3, 2}, new int[]{1, 2, 2, 1, 3, 2}, new int[]{1, 2, 2, 2, 3, 1}, new int[]{1, 1, 3, 2, 2, 2}, new int[]{1, 2, 3, 1, 2, 2}, new int[]{1, 2, 3, 2, 2, 1}, new int[]{2, 2, 3, 2, 1, 1}, new int[]{2, 2, 1, 1, 3, 2}, new int[]{2, 2, 1, 2, 3, 1}, new int[]{2, 1, 3, 2, 1, 2}, new int[]{2, 2, 3, 1, 1, 2}, new int[]{3, 1, 2, 1, 3, 1}, new int[]{3, 1, 1, 2, 2, 2}, new int[]{3, 2, 1, 1, 2, 2}, new int[]{3, 2, 1, 2, 2, 1}, new int[]{3, 1, 2, 2, 1, 2}, new int[]{3, 2, 2, 1, 1, 2}, new int[]{3, 2, 2, 2, 1, 1}, new int[]{2, 1, 2, 1, 2, 3}, new int[]{2, 1, 2, 3, 2, 1}, new int[]{2, 3, 2, 1, 2, 1}, new int[]{1, 1, 1, 3, 2, 3}, new int[]{1, 3, 1, 1, 2, 3}, new int[]{1, 3, 1, 3, 2, 1}, new int[]{1, 1, 2, 3, 1, 3}, new int[]{1, 3, 2, 1, 1, 3}, new int[]{1, 3, 2, 3, 1, 1}, new int[]{2, 1, 1, 3, 1, 3}, new int[]{2, 3, 1, 1, 1, 3}, new int[]{2, 3, 1, 3, 1, 1}, new int[]{1, 1, 2, 1, 3, 3}, new int[]{1, 1, 2, 3, 3, 1}, new int[]{1, 3, 2, 1, 3, 1}, new int[]{1, 1, 3, 1, 2, 3}, new int[]{1, 1, 3, 3, 2, 1}, new int[]{1, 3, 3, 1, 2, 1}, new int[]{3, 1, 3, 1, 2, 1}, new int[]{2, 1, 1, 3, 3, 1}, new int[]{2, 3, 1, 1, 3, 1}, new int[]{2, 1, 3, 1, 1, 3}, new int[]{2, 1, 3, 3, 1, 1}, new int[]{2, 1, 3, 1, 3, 1}, new int[]{3, 1, 1, 1, 2, 3}, new int[]{3, 1, 1, 3, 2, 1}, new int[]{3, 3, 1, 1, 2, 1}, new int[]{3, 1, 2, 1, 1, 3}, new int[]{3, 1, 2, 3, 1, 1}, new int[]{3, 3, 2, 1, 1, 1}, new int[]{3, 1, 4, 1, 1, 1}, new int[]{2, 2, 1, 4, 1, 1}, new int[]{4, 3, 1, 1, 1, 1}, new int[]{1, 1, 1, 2, 2, 4}, new int[]{1, 1, 1, 4, 2, 2}, new int[]{1, 2, 1, 1, 2, 4}, new int[]{1, 2, 1, 4, 2, 1}, new int[]{1, 4, 1, 1, 2, 2}, new int[]{1, 4, 1, 2, 2, 1}, new int[]{1, 1, 2, 2, 1, 4}, new int[]{1, 1, 2, 4, 1, 2}, new int[]{1, 2, 2, 1, 1, 4}, new int[]{1, 2, 2, 4, 1, 1}, new int[]{1, 4, 2, 1, 1, 2}, new int[]{1, 4, 2, 2, 1, 1}, new int[]{2, 4, 1, 2, 1, 1}, new int[]{2, 2, 1, 1, 1, 4}, new int[]{4, 1, 3, 1, 1, 1}, new int[]{2, 4, 1, 1, 1, 2}, new int[]{1, 3, 4, 1, 1, 1}, new int[]{1, 1, 1, 2, 4, 2}, new int[]{1, 2, 1, 1, 4, 2}, new int[]{1, 2, 1, 2, 4, 1}, new int[]{1, 1, 4, 2, 1, 2}, new int[]{1, 2, 4, 1, 1, 2}, new int[]{1, 2, 4, 2, 1, 1}, new int[]{4, 1, 1, 2, 1, 2}, new int[]{4, 2, 1, 1, 1, 2}, new int[]{4, 2, 1, 2, 1, 1}, new int[]{2, 1, 2, 1, 4, 1}, new int[]{2, 1, 4, 1, 2, 1}, new int[]{4, 1, 2, 1, 2, 1}, new int[]{1, 1, 1, 1, 4, 3}, new int[]{1, 1, 1, 3, 4, 1}, new int[]{1, 3, 1, 1, 4, 1}, new int[]{1, 1, 4, 1, 1, 3}, new int[]{1, 1, 4, 3, 1, 1}, new int[]{4, 1, 1, 1, 1, 3}, new int[]{4, 1, 1, 3, 1, 1}, new int[]{1, 1, 3, 1, 4, 1}, new int[]{1, 1, 4, 1, 3, 1}, new int[]{3, 1, 1, 1, 4, 1}, new int[]{4, 1, 1, 1, 3, 1}, new int[]{2, 1, 1, 4, 1, 2}, new int[]{2, 1, 1, 2, 1, 4}, new int[]{2, 1, 1, 2, 3, 2}, new int[]{2, 3, 3, 1, 1, 1, 2}};
    private static final int CODE_SHIFT = 98;
    private static final int CODE_START_A = 103;
    private static final int CODE_START_B = 104;
    private static final int CODE_START_C = 105;
    private static final int CODE_STOP = 106;
    private static final float MAX_AVG_VARIANCE = 0.25f;
    private static final float MAX_INDIVIDUAL_VARIANCE = 0.7f;

    private static int[] findStartPattern(BitArray bitArray) throws NotFoundException {
        int size = bitArray.getSize();
        int nextSet = bitArray.getNextSet(0);
        int[] iArr = new int[6];
        int i = nextSet;
        boolean z = false;
        int i2 = 0;
        while (nextSet < size) {
            if (bitArray.get(nextSet) ^ z) {
                iArr[i2] = iArr[i2] + 1;
            } else {
                if (i2 == 5) {
                    float f = MAX_AVG_VARIANCE;
                    int i3 = -1;
                    for (int i4 = 103; i4 <= 105; i4++) {
                        float patternMatchVariance = patternMatchVariance(iArr, CODE_PATTERNS[i4], MAX_INDIVIDUAL_VARIANCE);
                        if (patternMatchVariance < f) {
                            i3 = i4;
                            f = patternMatchVariance;
                        }
                    }
                    if (i3 < 0 || !bitArray.isRange(Math.max(0, i - ((nextSet - i) / 2)), i, false)) {
                        i += iArr[0] + iArr[1];
                        System.arraycopy(iArr, 2, iArr, 0, 4);
                        iArr[4] = 0;
                        iArr[5] = 0;
                        i2--;
                    } else {
                        return new int[]{i, nextSet, i3};
                    }
                } else {
                    i2++;
                }
                iArr[i2] = 1;
                z = !z;
            }
            nextSet++;
        }
        throw NotFoundException.getNotFoundInstance();
    }

    private static int decodeCode(BitArray bitArray, int[] iArr, int i) throws NotFoundException {
        recordPattern(bitArray, i, iArr);
        float f = MAX_AVG_VARIANCE;
        int i2 = -1;
        for (int i3 = 0; i3 < CODE_PATTERNS.length; i3++) {
            float patternMatchVariance = patternMatchVariance(iArr, CODE_PATTERNS[i3], MAX_INDIVIDUAL_VARIANCE);
            if (patternMatchVariance < f) {
                i2 = i3;
                f = patternMatchVariance;
            }
        }
        if (i2 >= 0) {
            return i2;
        }
        throw NotFoundException.getNotFoundInstance();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:104:0x018d, code lost:
        r15 = r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:105:0x018e, code lost:
        r5 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:106:0x018f, code lost:
        if (r8 == false) goto L_0x019b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:108:0x0193, code lost:
        if (r3 != 'e') goto L_0x0198;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:109:0x0195, code lost:
        r3 = 'd';
     */
    /* JADX WARNING: Code restructure failed: missing block: B:110:0x0198, code lost:
        r3 = 'e';
     */
    /* JADX WARNING: Code restructure failed: missing block: B:112:0x019d, code lost:
        r8 = r5;
        r5 = r15;
        r15 = 6;
        r21 = r12;
        r12 = r9;
        r9 = r14;
        r14 = r18;
        r18 = r21;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x00e0, code lost:
        if (r5 != false) goto L_0x0137;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x0100, code lost:
        r5 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x0103, code lost:
        r15 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x012e, code lost:
        r5 = false;
        r11 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:75:0x0135, code lost:
        if (r5 != false) goto L_0x0137;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:0x0137, code lost:
        r5 = false;
        r11 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x013c, code lost:
        r5 = false;
        r15 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:78:0x0141, code lost:
        r15 = r5;
        r3 = 'c';
     */
    /* JADX WARNING: Code restructure failed: missing block: B:80:0x0148, code lost:
        r5 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:0x014a, code lost:
        r6 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:0x014b, code lost:
        r15 = r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:0x014c, code lost:
        r5 = false;
     */
    public Result decodeRow(int i, BitArray bitArray, Map<DecodeHintType, ?> map) throws NotFoundException, FormatException, ChecksumException {
        char c;
        boolean z;
        boolean z2;
        boolean z3;
        BitArray bitArray2 = bitArray;
        Map<DecodeHintType, ?> map2 = map;
        boolean z4 = map2 != null && map2.containsKey(DecodeHintType.ASSUME_GS1);
        int[] findStartPattern = findStartPattern(bitArray);
        int i2 = findStartPattern[2];
        ArrayList arrayList = new ArrayList(20);
        arrayList.add(Byte.valueOf((byte) i2));
        switch (i2) {
            case 103:
                c = 'e';
                break;
            case 104:
                c = 'd';
                break;
            case 105:
                c = 'c';
                break;
            default:
                throw FormatException.getFormatInstance();
        }
        StringBuilder sb = new StringBuilder(20);
        int i3 = findStartPattern[0];
        int i4 = findStartPattern[1];
        int i5 = 6;
        int[] iArr = new int[6];
        int i6 = i2;
        int i7 = i3;
        char c2 = c;
        boolean z5 = false;
        boolean z6 = false;
        boolean z7 = false;
        boolean z8 = false;
        int i8 = 0;
        int i9 = 0;
        int i10 = 0;
        boolean z9 = true;
        while (!z6) {
            int decodeCode = decodeCode(bitArray2, iArr, i4);
            arrayList.add(Byte.valueOf((byte) decodeCode));
            if (decodeCode != 106) {
                z9 = true;
            }
            if (decodeCode != 106) {
                i9++;
                i6 += i9 * decodeCode;
            }
            int i11 = i4;
            for (int i12 = 0; i12 < i5; i12++) {
                i11 += iArr[i12];
            }
            switch (decodeCode) {
                case 103:
                case 104:
                case 105:
                    throw FormatException.getFormatInstance();
                default:
                    switch (c2) {
                        case 'c':
                            if (decodeCode >= 100) {
                                if (decodeCode != 106) {
                                    z9 = false;
                                }
                                if (decodeCode == 106) {
                                    z = z5;
                                    boolean z10 = false;
                                    z6 = true;
                                    break;
                                } else {
                                    switch (decodeCode) {
                                        case 100:
                                            z2 = z5;
                                            c2 = 'd';
                                            break;
                                        case 101:
                                            z2 = z5;
                                            c2 = 'e';
                                            break;
                                        case 102:
                                            if (z4) {
                                                if (sb.length() != 0) {
                                                    sb.append(29);
                                                    break;
                                                } else {
                                                    sb.append("]C1");
                                                    break;
                                                }
                                            }
                                            break;
                                    }
                                }
                            } else {
                                if (decodeCode < 10) {
                                    sb.append('0');
                                }
                                sb.append(decodeCode);
                                break;
                            }
                            break;
                        case 'd':
                            if (decodeCode < 96) {
                                if (z5 != z8) {
                                    sb.append((char) (decodeCode + 32 + 128));
                                    break;
                                } else {
                                    sb.append((char) (decodeCode + 32));
                                    break;
                                }
                            } else {
                                if (decodeCode != 106) {
                                    z9 = false;
                                }
                                if (decodeCode != 106) {
                                    switch (decodeCode) {
                                        case 98:
                                            z = z5;
                                            c2 = 'e';
                                            break;
                                        case 99:
                                            break;
                                        case 100:
                                            if (z8 || !z5) {
                                                if (z8) {
                                                }
                                            }
                                            break;
                                        case 101:
                                            z3 = z5;
                                            c2 = 'e';
                                            break;
                                        case 102:
                                            if (z4) {
                                                if (sb.length() != 0) {
                                                    sb.append(29);
                                                    break;
                                                } else {
                                                    sb.append("]C1");
                                                    break;
                                                }
                                            }
                                            break;
                                    }
                                }
                            }
                            break;
                        case 'e':
                            if (decodeCode >= 64) {
                                if (decodeCode < 96) {
                                    if (z5 != z8) {
                                        sb.append((char) (decodeCode + 64));
                                        break;
                                    } else {
                                        sb.append((char) (decodeCode - 64));
                                        break;
                                    }
                                } else {
                                    if (decodeCode != 106) {
                                        z9 = false;
                                    }
                                    if (decodeCode != 106) {
                                        switch (decodeCode) {
                                            case 98:
                                                z = z5;
                                                c2 = 'd';
                                                break;
                                            case 99:
                                                break;
                                            case 100:
                                                z3 = z5;
                                                c2 = 'd';
                                                break;
                                            case 101:
                                                if (z8 || !z5) {
                                                    if (z8) {
                                                    }
                                                }
                                                break;
                                            case 102:
                                                if (z4) {
                                                    if (sb.length() != 0) {
                                                        sb.append(29);
                                                        break;
                                                    } else {
                                                        sb.append("]C1");
                                                        break;
                                                    }
                                                }
                                                break;
                                        }
                                    }
                                }
                            } else if (z5 != z8) {
                                sb.append((char) (decodeCode + 32 + 128));
                                break;
                            } else {
                                sb.append((char) (decodeCode + 32));
                                break;
                            }
                            break;
                    }
            }
        }
        int i13 = i4 - i7;
        int nextUnset = bitArray2.getNextUnset(i4);
        if (bitArray2.isRange(nextUnset, Math.min(bitArray.getSize(), ((nextUnset - i7) / 2) + nextUnset), false)) {
            int i14 = i10;
            if ((i6 - (i9 * i14)) % 103 == i14) {
                int length = sb.length();
                if (length != 0) {
                    if (length > 0 && z9) {
                        if (c2 == 'c') {
                            sb.delete(length - 2, length);
                        } else {
                            sb.delete(length - 1, length);
                        }
                    }
                    float f = ((float) (findStartPattern[1] + findStartPattern[0])) / 2.0f;
                    float f2 = ((float) i7) + (((float) i13) / 2.0f);
                    int size = arrayList.size();
                    byte[] bArr = new byte[size];
                    for (int i15 = 0; i15 < size; i15++) {
                        bArr[i15] = ((Byte) arrayList.get(i15)).byteValue();
                    }
                    float f3 = (float) i;
                    return new Result(sb.toString(), bArr, new ResultPoint[]{new ResultPoint(f, f3), new ResultPoint(f2, f3)}, BarcodeFormat.CODE_128);
                }
                throw NotFoundException.getNotFoundInstance();
            }
            throw ChecksumException.getChecksumInstance();
        }
        throw NotFoundException.getNotFoundInstance();
    }
}
