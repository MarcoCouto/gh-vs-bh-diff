package com.google.android.gms.ads.impl;

public final class R {

    public static final class string {
        public static final int s1 = 2131624208;
        public static final int s2 = 2131624209;
        public static final int s3 = 2131624210;
        public static final int s4 = 2131624211;
        public static final int s5 = 2131624212;
        public static final int s6 = 2131624213;
        public static final int s7 = 2131624214;

        private string() {
        }
    }

    private R() {
    }
}
