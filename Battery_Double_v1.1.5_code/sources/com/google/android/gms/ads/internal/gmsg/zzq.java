package com.google.android.gms.ads.internal.gmsg;

import com.google.android.gms.internal.ads.zzbgg;
import com.mintegral.msdk.base.entity.CampaignEx;
import java.util.Map;

final class zzq implements zzu<zzbgg> {
    zzq() {
    }

    public final /* synthetic */ void zza(Object obj, Map map) {
        zzbgg zzbgg = (zzbgg) obj;
        String str = (String) map.get("action");
        if (CampaignEx.JSON_NATIVE_VIDEO_PAUSE.equals(str)) {
            zzbgg.zzjf();
            return;
        }
        if (CampaignEx.JSON_NATIVE_VIDEO_RESUME.equals(str)) {
            zzbgg.zzjg();
        }
    }
}
