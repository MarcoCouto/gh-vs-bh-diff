package com.google.android.gms.ads.internal;

import android.app.Activity;
import android.content.Context;
import android.os.RemoteException;
import android.support.annotation.Keep;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.FrameLayout;
import com.google.android.gms.ads.internal.overlay.AdOverlayInfoParcel;
import com.google.android.gms.ads.internal.overlay.zzq;
import com.google.android.gms.ads.internal.overlay.zzr;
import com.google.android.gms.ads.internal.overlay.zzs;
import com.google.android.gms.ads.internal.overlay.zzx;
import com.google.android.gms.ads.internal.overlay.zzy;
import com.google.android.gms.common.annotation.KeepForSdkWithMembers;
import com.google.android.gms.common.util.DynamiteApi;
import com.google.android.gms.common.util.RetainForClient;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.ObjectWrapper;
import com.google.android.gms.internal.ads.zzaan;
import com.google.android.gms.internal.ads.zzacr;
import com.google.android.gms.internal.ads.zzact;
import com.google.android.gms.internal.ads.zzadf;
import com.google.android.gms.internal.ads.zzadk;
import com.google.android.gms.internal.ads.zzahr;
import com.google.android.gms.internal.ads.zzalg;
import com.google.android.gms.internal.ads.zzaop;
import com.google.android.gms.internal.ads.zzaoz;
import com.google.android.gms.internal.ads.zzark;
import com.google.android.gms.internal.ads.zzaun;
import com.google.android.gms.internal.ads.zzauw;
import com.google.android.gms.internal.ads.zzayh;
import com.google.android.gms.internal.ads.zzbbi;
import com.google.android.gms.internal.ads.zzwf;
import com.google.android.gms.internal.ads.zzxg;
import com.google.android.gms.internal.ads.zzxl;
import com.google.android.gms.internal.ads.zzxx;
import com.google.android.gms.internal.ads.zzyc;
import java.util.HashMap;
import javax.annotation.ParametersAreNonnullByDefault;

@Keep
@zzark
@KeepForSdkWithMembers
@DynamiteApi
@RetainForClient
@ParametersAreNonnullByDefault
public class ClientApi extends zzxx {
    public zzaoz createInAppPurchaseManager(IObjectWrapper iObjectWrapper) {
        return null;
    }

    @Nullable
    public zzauw createRewardedVideoAdSku(IObjectWrapper iObjectWrapper, int i) {
        return null;
    }

    @Nullable
    public zzyc getMobileAdsSettingsManager(IObjectWrapper iObjectWrapper) {
        return null;
    }

    public zzxl createBannerAdManager(IObjectWrapper iObjectWrapper, zzwf zzwf, String str, zzalg zzalg, int i) throws RemoteException {
        Context context = (Context) ObjectWrapper.unwrap(iObjectWrapper);
        zzbv.zzlf();
        zzx zzx = new zzx(context, zzwf, str, zzalg, new zzbbi(14300000, i, true, zzayh.zzav(context)), zzv.zzd(context));
        return zzx;
    }

    public zzxl createSearchAdManager(IObjectWrapper iObjectWrapper, zzwf zzwf, String str, int i) throws RemoteException {
        Context context = (Context) ObjectWrapper.unwrap(iObjectWrapper);
        zzbv.zzlf();
        return new zzbp(context, zzwf, str, new zzbbi(14300000, i, true, zzayh.zzav(context)));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0034, code lost:
        if (((java.lang.Boolean) com.google.android.gms.internal.ads.zzwu.zzpz().zzd(com.google.android.gms.internal.ads.zzaan.zzcrz)).booleanValue() == false) goto L_0x0036;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0048, code lost:
        if (((java.lang.Boolean) com.google.android.gms.internal.ads.zzwu.zzpz().zzd(com.google.android.gms.internal.ads.zzaan.zzcsa)).booleanValue() != false) goto L_0x004c;
     */
    public zzxl createInterstitialAdManager(IObjectWrapper iObjectWrapper, zzwf zzwf, String str, zzalg zzalg, int i) throws RemoteException {
        Context context = (Context) ObjectWrapper.unwrap(iObjectWrapper);
        zzaan.initialize(context);
        zzbv.zzlf();
        boolean z = true;
        zzbbi zzbbi = new zzbbi(14300000, i, true, zzayh.zzav(context));
        boolean equals = "reward_mb".equals(zzwf.zzckk);
        if (!equals) {
        }
        if (equals) {
        }
        z = false;
        if (z) {
            zzahr zzahr = new zzahr(context, str, zzalg, zzbbi, zzv.zzd(context));
            return zzahr;
        }
        zzal zzal = new zzal(context, zzwf, str, zzalg, zzbbi, zzv.zzd(context));
        return zzal;
    }

    public zzxg createAdLoaderBuilder(IObjectWrapper iObjectWrapper, String str, zzalg zzalg, int i) {
        Context context = (Context) ObjectWrapper.unwrap(iObjectWrapper);
        zzbv.zzlf();
        zzak zzak = new zzak(context, str, zzalg, new zzbbi(14300000, i, true, zzayh.zzav(context)), zzv.zzd(context));
        return zzak;
    }

    public zzyc getMobileAdsSettingsManagerWithClientJarVersion(IObjectWrapper iObjectWrapper, int i) {
        Context context = (Context) ObjectWrapper.unwrap(iObjectWrapper);
        zzbv.zzlf();
        return zzay.zza(context, new zzbbi(14300000, i, true, zzayh.zzav(context)));
    }

    public zzadf createNativeAdViewDelegate(IObjectWrapper iObjectWrapper, IObjectWrapper iObjectWrapper2) {
        return new zzacr((FrameLayout) ObjectWrapper.unwrap(iObjectWrapper), (FrameLayout) ObjectWrapper.unwrap(iObjectWrapper2));
    }

    public zzadk createNativeAdViewHolderDelegate(IObjectWrapper iObjectWrapper, IObjectWrapper iObjectWrapper2, IObjectWrapper iObjectWrapper3) {
        return new zzact((View) ObjectWrapper.unwrap(iObjectWrapper), (HashMap) ObjectWrapper.unwrap(iObjectWrapper2), (HashMap) ObjectWrapper.unwrap(iObjectWrapper3));
    }

    public zzauw createRewardedVideoAd(IObjectWrapper iObjectWrapper, zzalg zzalg, int i) {
        Context context = (Context) ObjectWrapper.unwrap(iObjectWrapper);
        zzbv.zzlf();
        return new zzaun(context, zzv.zzd(context), zzalg, new zzbbi(14300000, i, true, zzayh.zzav(context)));
    }

    public zzaop createAdOverlay(IObjectWrapper iObjectWrapper) {
        Activity activity = (Activity) ObjectWrapper.unwrap(iObjectWrapper);
        AdOverlayInfoParcel zzc = AdOverlayInfoParcel.zzc(activity.getIntent());
        if (zzc == null) {
            return new zzr(activity);
        }
        switch (zzc.zzdsa) {
            case 1:
                return new zzq(activity);
            case 2:
                return new zzx(activity);
            case 3:
                return new zzy(activity);
            case 4:
                return new zzs(activity, zzc);
            default:
                return new zzr(activity);
        }
    }
}
