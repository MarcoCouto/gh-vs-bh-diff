package com.google.android.gms.ads.internal;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import android.util.DisplayMetrics;
import android.view.View;
import com.google.android.gms.ads.internal.overlay.zzn;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.util.VisibleForTesting;
import com.google.android.gms.common.wrappers.Wrappers;
import com.google.android.gms.internal.ads.zzaan;
import com.google.android.gms.internal.ads.zzaba;
import com.google.android.gms.internal.ads.zzacp;
import com.google.android.gms.internal.ads.zzadx;
import com.google.android.gms.internal.ads.zzaeh;
import com.google.android.gms.internal.ads.zzahq;
import com.google.android.gms.internal.ads.zzaks;
import com.google.android.gms.internal.ads.zzakz;
import com.google.android.gms.internal.ads.zzalg;
import com.google.android.gms.internal.ads.zzark;
import com.google.android.gms.internal.ads.zzarn;
import com.google.android.gms.internal.ads.zzasj;
import com.google.android.gms.internal.ads.zzatd;
import com.google.android.gms.internal.ads.zzaxf;
import com.google.android.gms.internal.ads.zzaxj;
import com.google.android.gms.internal.ads.zzaxq;
import com.google.android.gms.internal.ads.zzaxv;
import com.google.android.gms.internal.ads.zzaxz;
import com.google.android.gms.internal.ads.zzayf;
import com.google.android.gms.internal.ads.zzayh;
import com.google.android.gms.internal.ads.zzayp;
import com.google.android.gms.internal.ads.zzbbi;
import com.google.android.gms.internal.ads.zzbbq;
import com.google.android.gms.internal.ads.zzbcb;
import com.google.android.gms.internal.ads.zzbcg;
import com.google.android.gms.internal.ads.zzbgg;
import com.google.android.gms.internal.ads.zzsx;
import com.google.android.gms.internal.ads.zzuo.zza.zzb;
import com.google.android.gms.internal.ads.zzur;
import com.google.android.gms.internal.ads.zzwb;
import com.google.android.gms.internal.ads.zzwf;
import com.google.android.gms.internal.ads.zzwu;
import com.google.android.gms.internal.ads.zzyv;
import com.integralads.avid.library.inmobi.utils.AvidJSONUtil;
import com.tapjoy.TJAdUnitConstants.String;
import com.tapjoy.TapjoyConstants;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.Executor;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import javax.annotation.ParametersAreNonnullByDefault;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@zzark
@ParametersAreNonnullByDefault
public abstract class zzc extends zza implements zzn, zzbo, zzaks {
    protected final zzalg zzbma;
    private transient boolean zzbmb;

    public zzc(Context context, zzwf zzwf, String str, zzalg zzalg, zzbbi zzbbi, zzv zzv) {
        this(new zzbw(context, zzwf, str, zzbbi), zzalg, null, zzv);
    }

    @VisibleForTesting
    private zzc(zzbw zzbw, zzalg zzalg, @Nullable zzbl zzbl, zzv zzv) {
        super(zzbw, null, zzv);
        this.zzbma = zzalg;
        this.zzbmb = false;
    }

    public final boolean zza(zzwb zzwb, zzaba zzaba, int i) {
        Bundle bundle;
        zzaxj zzaxj;
        if (!zziu()) {
            return false;
        }
        zzbv.zzlf();
        zzsx zzzo = zzbv.zzlj().zzyq().zzzo();
        String str = null;
        if (zzzo == null) {
            bundle = null;
        } else {
            bundle = zzayh.zza(zzzo);
        }
        this.zzblr.cancel();
        this.zzbls.zzbtw = 0;
        if (((Boolean) zzwu.zzpz().zzd(zzaan.zzcuz)).booleanValue()) {
            zzaxj = zzbv.zzlj().zzyq().zzzi();
            zzad zzln = zzbv.zzln();
            Context context = this.zzbls.zzsp;
            zzbbi zzbbi = this.zzbls.zzbsp;
            String str2 = this.zzbls.zzbsn;
            if (zzaxj != null) {
                str = zzaxj.zzyf();
            }
            zzln.zza(context, zzbbi, false, zzaxj, str, str2, null);
        } else {
            zzaxj = null;
        }
        return zza(zza(zzwb, bundle, zzaxj, i), zzaba);
    }

    public final boolean zza(zzasj zzasj, zzaba zzaba) {
        zzaxv zzaxv;
        this.zzbln = zzaba;
        zzaba.zzg("seq_num", zzasj.zzdwj);
        zzaba.zzg("request_id", zzasj.zzdws);
        zzaba.zzg("session_id", zzasj.zzclm);
        if (zzasj.zzdwh != null) {
            zzaba.zzg(TapjoyConstants.TJC_APP_VERSION_NAME, String.valueOf(zzasj.zzdwh.versionCode));
        }
        zzbw zzbw = this.zzbls;
        zzbv.zzlb();
        Context context = this.zzbls.zzsp;
        zzur zzur = this.zzbly.zzbmv;
        if (zzasj.zzdwg.extras.getBundle("sdk_less_server_data") != null) {
            zzaxv = new zzatd(context, zzasj, this, zzur);
        } else {
            zzaxv = new zzarn(context, zzasj, this, zzur);
        }
        zzaxv.zzyz();
        zzbw.zzbsr = zzaxv;
        return true;
    }

    public boolean zza(zzwb zzwb, zzaba zzaba) {
        return zza(zzwb, zzaba, 1);
    }

    public final void zzb(zzaxf zzaxf) {
        super.zzb(zzaxf);
        if (zzaxf.zzdnb != null) {
            zzaxz.zzdn("Disable the debug gesture detector on the mediation ad frame.");
            if (this.zzbls.zzbsq != null) {
                this.zzbls.zzbsq.zzmp();
            }
            zzaxz.zzdn("Pinging network fill URLs.");
            zzbv.zzlz();
            zzakz.zza(this.zzbls.zzsp, this.zzbls.zzbsp.zzdp, zzaxf, this.zzbls.zzbsn, false, zzaxf.zzdnb.zzdld);
            if (!(zzaxf.zzehj == null || zzaxf.zzehj.zzdlu == null || zzaxf.zzehj.zzdlu.size() <= 0)) {
                zzaxz.zzdn("Pinging urls remotely");
                zzbv.zzlf().zza(this.zzbls.zzsp, zzaxf.zzehj.zzdlu);
            }
        } else {
            zzaxz.zzdn("Enable the debug gesture detector on the admob ad frame.");
            if (this.zzbls.zzbsq != null) {
                this.zzbls.zzbsq.zzmo();
            }
        }
        if (zzaxf.errorCode == 3 && zzaxf.zzehj != null && zzaxf.zzehj.zzdlt != null) {
            zzaxz.zzdn("Pinging no fill URLs.");
            zzbv.zzlz();
            zzakz.zza(this.zzbls.zzsp, this.zzbls.zzbsp.zzdp, zzaxf, this.zzbls.zzbsn, false, zzaxf.zzehj.zzdlt);
        }
    }

    /* access modifiers changed from: protected */
    public boolean zza(@Nullable zzaxf zzaxf, zzaxf zzaxf2) {
        int i;
        if (!(zzaxf == null || zzaxf.zzdne == null)) {
            zzaxf.zzdne.zza((zzaks) null);
        }
        if (zzaxf2.zzdne != null) {
            zzaxf2.zzdne.zza((zzaks) this);
        }
        int i2 = 0;
        if (zzaxf2.zzehj != null) {
            i2 = zzaxf2.zzehj.zzdmg;
            i = zzaxf2.zzehj.zzdmh;
        } else {
            i = 0;
        }
        this.zzbls.zzbtu.zzl(i2, i);
        return true;
    }

    public void onAdClicked() {
        if (this.zzbls.zzbsu == null) {
            zzaxz.zzeo("Ad state was null when trying to ping click URLs.");
            return;
        }
        if (!(this.zzbls.zzbsu.zzehj == null || this.zzbls.zzbsu.zzehj.zzdlq == null)) {
            zzbv.zzlz();
            zzakz.zza(this.zzbls.zzsp, this.zzbls.zzbsp.zzdp, this.zzbls.zzbsu, this.zzbls.zzbsn, false, zza(this.zzbls.zzbsu.zzehj.zzdlq, this.zzbls.zzbsu.zzdzf));
        }
        if (!(this.zzbls.zzbsu.zzdnb == null || this.zzbls.zzbsu.zzdnb.zzdkz == null)) {
            zzbv.zzlz();
            zzakz.zza(this.zzbls.zzsp, this.zzbls.zzbsp.zzdp, this.zzbls.zzbsu, this.zzbls.zzbsn, false, this.zzbls.zzbsu.zzdnb.zzdkz);
        }
        super.onAdClicked();
    }

    /* access modifiers changed from: 0000 */
    public final boolean zza(zzaxf zzaxf) {
        zzwb zzwb;
        boolean z = false;
        if (this.zzblt != null) {
            zzwb = this.zzblt;
            this.zzblt = null;
        } else {
            zzwb = zzaxf.zzdwg;
            if (zzwb.extras != null) {
                z = zzwb.extras.getBoolean("_noRefresh", false);
            }
        }
        return zza(zzwb, zzaxf, z);
    }

    /* access modifiers changed from: protected */
    public boolean zza(zzwb zzwb, zzaxf zzaxf, boolean z) {
        if (!z && this.zzbls.zzmj()) {
            if (zzaxf.zzdlx > 0) {
                this.zzblr.zza(zzwb, zzaxf.zzdlx);
            } else if (zzaxf.zzehj != null && zzaxf.zzehj.zzdlx > 0) {
                this.zzblr.zza(zzwb, zzaxf.zzehj.zzdlx);
            } else if (!zzaxf.zzdyd && zzaxf.errorCode == 2) {
                this.zzblr.zzg(zzwb);
            }
        }
        return this.zzblr.zzkv();
    }

    public void pause() {
        Preconditions.checkMainThread("pause must be called on the main UI thread.");
        if (!(this.zzbls.zzbsu == null || this.zzbls.zzbsu.zzdrv == null || !this.zzbls.zzmj())) {
            zzbv.zzlh();
            zzayp.zzi(this.zzbls.zzbsu.zzdrv);
        }
        if (!(this.zzbls.zzbsu == null || this.zzbls.zzbsu.zzdnc == null)) {
            try {
                this.zzbls.zzbsu.zzdnc.pause();
            } catch (RemoteException unused) {
                zzaxz.zzeo("Could not pause mediation adapter.");
            }
        }
        this.zzblu.zzj(this.zzbls.zzbsu);
        this.zzblr.pause();
    }

    public void resume() {
        Preconditions.checkMainThread("resume must be called on the main UI thread.");
        zzbgg zzbgg = (this.zzbls.zzbsu == null || this.zzbls.zzbsu.zzdrv == null) ? null : this.zzbls.zzbsu.zzdrv;
        if (zzbgg != null && this.zzbls.zzmj()) {
            zzbv.zzlh();
            zzayp.zzj(this.zzbls.zzbsu.zzdrv);
        }
        if (!(this.zzbls.zzbsu == null || this.zzbls.zzbsu.zzdnc == null)) {
            try {
                this.zzbls.zzbsu.zzdnc.resume();
            } catch (RemoteException unused) {
                zzaxz.zzeo("Could not resume mediation adapter.");
            }
        }
        if (zzbgg == null || !zzbgg.zzadt()) {
            this.zzblr.resume();
        }
        this.zzblu.zzk(this.zzbls.zzbsu);
    }

    /* access modifiers changed from: protected */
    public final boolean zzc(zzwb zzwb) {
        return super.zzc(zzwb) && !this.zzbmb;
    }

    /* access modifiers changed from: protected */
    public boolean zziu() {
        zzbv.zzlf();
        if (zzayh.zzn(this.zzbls.zzsp, "android.permission.INTERNET")) {
            zzbv.zzlf();
            if (zzayh.zzah(this.zzbls.zzsp)) {
                return true;
            }
        }
        return false;
    }

    public void zziv() {
        this.zzbmb = false;
        zzii();
        this.zzbls.zzbsw.zzxx();
    }

    public void zziw() {
        this.zzbmb = true;
        zzik();
    }

    public final void onPause() {
        this.zzblu.zzj(this.zzbls.zzbsu);
    }

    public final void onResume() {
        this.zzblu.zzk(this.zzbls.zzbsu);
    }

    public void zzix() {
        zzaxz.zzeo("Mediated ad does not support onVideoEnd callback");
    }

    public void zziy() {
        onAdClicked();
    }

    public final void zziz() {
        zziv();
    }

    public final void zzja() {
        zzij();
    }

    public final void zzjb() {
        zziw();
    }

    public final void zzjc() {
        if (this.zzbls.zzbsu != null) {
            String str = this.zzbls.zzbsu.zzdnd;
            StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 74);
            sb.append("Mediation adapter ");
            sb.append(str);
            sb.append(" refreshed, but mediation adapters should never refresh.");
            zzaxz.zzeo(sb.toString());
        }
        zza(this.zzbls.zzbsu, true);
        zzb(this.zzbls.zzbsu, true);
        zzil();
    }

    public void zzjd() {
        recordImpression();
    }

    public final void zzd(String str, String str2) {
        onAppEvent(str, str2);
    }

    public final void zza(zzadx zzadx, String str) {
        Object obj;
        zzaeh zzaeh = null;
        if (zzadx != null) {
            try {
                obj = zzadx.getCustomTemplateId();
            } catch (RemoteException e) {
                zzaxz.zzc("Unable to call onCustomClick.", e);
                return;
            }
        } else {
            obj = null;
        }
        if (!(this.zzbls.zzbtg == null || obj == null)) {
            zzaeh = (zzaeh) this.zzbls.zzbtg.get(obj);
        }
        if (zzaeh == null) {
            zzaxz.zzeo("Mediation adapter invoked onCustomClick but no listeners were set.");
        } else {
            zzaeh.zzb(zzadx, str);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:34:0x0110  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x014b  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x0152  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x0166  */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x0176  */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x018d  */
    private final zzasj zza(zzwb zzwb, Bundle bundle, zzaxj zzaxj, int i) {
        PackageInfo packageInfo;
        Bundle bundle2;
        long j;
        int i2;
        String str;
        ApplicationInfo applicationInfo = this.zzbls.zzsp.getApplicationInfo();
        int i3 = 0;
        try {
            packageInfo = Wrappers.packageManager(this.zzbls.zzsp).getPackageInfo(applicationInfo.packageName, 0);
        } catch (NameNotFoundException unused) {
            packageInfo = null;
        }
        DisplayMetrics displayMetrics = this.zzbls.zzsp.getResources().getDisplayMetrics();
        if (this.zzbls.zzbsq == null || this.zzbls.zzbsq.getParent() == null) {
            bundle2 = null;
        } else {
            int[] iArr = new int[2];
            this.zzbls.zzbsq.getLocationOnScreen(iArr);
            int i4 = iArr[0];
            int i5 = 1;
            int i6 = iArr[1];
            int width = this.zzbls.zzbsq.getWidth();
            int height = this.zzbls.zzbsq.getHeight();
            if (!this.zzbls.zzbsq.isShown() || i4 + width <= 0 || i6 + height <= 0 || i4 > displayMetrics.widthPixels || i6 > displayMetrics.heightPixels) {
                i5 = 0;
            }
            Bundle bundle3 = new Bundle(5);
            bundle3.putInt(AvidJSONUtil.KEY_X, i4);
            bundle3.putInt(AvidJSONUtil.KEY_Y, i6);
            bundle3.putInt("width", width);
            bundle3.putInt("height", height);
            bundle3.putInt(String.VISIBLE, i5);
            bundle2 = bundle3;
        }
        this.zzbls.zzbsw = zzbv.zzlj().zzys().zza(zzbv.zzlm(), this.zzbls.zzbsn);
        this.zzbls.zzbsw.zzn(zzwb);
        zzbv.zzlf();
        String zza = zzayh.zza(this.zzbls.zzsp, (View) this.zzbls.zzbsq, this.zzbls.zzbst);
        if (this.zzbls.zzbtb != null) {
            try {
                j = this.zzbls.zzbtb.getValue();
            } catch (RemoteException unused2) {
                zzaxz.zzeo("Cannot get correlation id, default to 0.");
            }
            String uuid = UUID.randomUUID().toString();
            Bundle zza2 = zzbv.zzlj().zzys().zza(this.zzbls.zzsp, (zzaxq) this);
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            for (i2 = 0; i2 < this.zzbls.zzbth.size(); i2++) {
                String str2 = (String) this.zzbls.zzbth.keyAt(i2);
                arrayList.add(str2);
                if (this.zzbls.zzbtg.containsKey(str2) && this.zzbls.zzbtg.get(str2) != null) {
                    arrayList2.add(str2);
                }
            }
            zzbcb zza3 = zzayf.zza(new zzf(this));
            zzbcb zza4 = zzayf.zza(new zzg(this));
            String zzye = zzaxj == null ? zzaxj.zzye() : null;
            if (this.zzbls.zzbtt != null && this.zzbls.zzbtt.size() > 0) {
                if (packageInfo != null) {
                    i3 = packageInfo.versionCode;
                }
                if (i3 <= zzbv.zzlj().zzyq().zzzh()) {
                    zzbv.zzlj().zzyq().zzzn();
                    zzbv.zzlj().zzyq().zzcv(i3);
                } else {
                    JSONObject zzzm = zzbv.zzlj().zzyq().zzzm();
                    if (zzzm != null) {
                        JSONArray optJSONArray = zzzm.optJSONArray(this.zzbls.zzbsn);
                        if (optJSONArray != null) {
                            str = optJSONArray.toString();
                            zzwf zzwf = this.zzbls.zzbst;
                            String str3 = this.zzbls.zzbsn;
                            String zzxy = this.zzbls.zzbsw.zzxy();
                            String zzqa = zzwu.zzqa();
                            ArrayList arrayList3 = arrayList2;
                            zzbbi zzbbi = this.zzbls.zzbsp;
                            ArrayList arrayList4 = arrayList;
                            List<String> list = this.zzbls.zzbtt;
                            boolean zzzb = zzbv.zzlj().zzyq().zzzb();
                            int i7 = displayMetrics.widthPixels;
                            int i8 = displayMetrics.heightPixels;
                            float f = displayMetrics.density;
                            List zzqw = zzaan.zzqw();
                            String str4 = this.zzbls.zzbsm;
                            zzacp zzacp = this.zzbls.zzbti;
                            String zzml = this.zzbls.zzml();
                            float zzkj = zzbv.zzlk().zzkj();
                            boolean zzkk = zzbv.zzlk().zzkk();
                            zzbv.zzlf();
                            int zzas = zzayh.zzas(this.zzbls.zzsp);
                            zzbv.zzlf();
                            int zzy = zzayh.zzy(this.zzbls.zzbsq);
                            boolean z = this.zzbls.zzsp instanceof Activity;
                            boolean zzzg = zzbv.zzlj().zzyq().zzzg();
                            boolean zzyj = zzbv.zzlj().zzyj();
                            int zzada = zzbv.zzmd().zzada();
                            zzbv.zzlf();
                            Bundle zzzv = zzayh.zzzv();
                            String zzaag = zzbv.zzlp().zzaag();
                            zzyv zzyv = this.zzbls.zzbtl;
                            boolean zzaah = zzbv.zzlp().zzaah();
                            Bundle zztx = zzahq.zzto().zztx();
                            boolean zzdu = zzbv.zzlj().zzyq().zzdu(this.zzbls.zzbsn);
                            List<Integer> list2 = this.zzbls.zzbtn;
                            boolean isCallerInstantApp = Wrappers.packageManager(this.zzbls.zzsp).isCallerInstantApp();
                            boolean zzyk = zzbv.zzlj().zzyk();
                            zzbv.zzlh();
                            zzasj zzasj = r2;
                            zzasj zzasj2 = new zzasj(bundle2, zzwb, zzwf, str3, applicationInfo, packageInfo, zzxy, zzqa, zzbbi, zza2, list, arrayList4, bundle, zzzb, i7, i8, f, zza, j, uuid, zzqw, str4, zzacp, zzml, zzkj, zzkk, zzas, zzy, z, zzzg, zza3, zzye, zzyj, zzada, zzzv, zzaag, zzyv, zzaah, zztx, zzdu, zza4, list2, str, arrayList3, i, isCallerInstantApp, zzyk, zzayp.zzaaa(), (ArrayList) zzbbq.zza((Future<T>) zzbv.zzlj().zzyr(), null, 1000, TimeUnit.MILLISECONDS), zzbv.zzlf().zzaw(this.zzbls.zzsp), this.zzbls.zzbtk, zzbv.zzlf().zzax(this.zzbls.zzsp));
                            return zzasj;
                        }
                    }
                }
            }
            str = null;
            zzwf zzwf2 = this.zzbls.zzbst;
            String str32 = this.zzbls.zzbsn;
            String zzxy2 = this.zzbls.zzbsw.zzxy();
            String zzqa2 = zzwu.zzqa();
            ArrayList arrayList32 = arrayList2;
            zzbbi zzbbi2 = this.zzbls.zzbsp;
            ArrayList arrayList42 = arrayList;
            List<String> list3 = this.zzbls.zzbtt;
            boolean zzzb2 = zzbv.zzlj().zzyq().zzzb();
            int i72 = displayMetrics.widthPixels;
            int i82 = displayMetrics.heightPixels;
            float f2 = displayMetrics.density;
            List zzqw2 = zzaan.zzqw();
            String str42 = this.zzbls.zzbsm;
            zzacp zzacp2 = this.zzbls.zzbti;
            String zzml2 = this.zzbls.zzml();
            float zzkj2 = zzbv.zzlk().zzkj();
            boolean zzkk2 = zzbv.zzlk().zzkk();
            zzbv.zzlf();
            int zzas2 = zzayh.zzas(this.zzbls.zzsp);
            zzbv.zzlf();
            int zzy2 = zzayh.zzy(this.zzbls.zzbsq);
            boolean z2 = this.zzbls.zzsp instanceof Activity;
            boolean zzzg2 = zzbv.zzlj().zzyq().zzzg();
            boolean zzyj2 = zzbv.zzlj().zzyj();
            int zzada2 = zzbv.zzmd().zzada();
            zzbv.zzlf();
            Bundle zzzv2 = zzayh.zzzv();
            String zzaag2 = zzbv.zzlp().zzaag();
            zzyv zzyv2 = this.zzbls.zzbtl;
            boolean zzaah2 = zzbv.zzlp().zzaah();
            Bundle zztx2 = zzahq.zzto().zztx();
            boolean zzdu2 = zzbv.zzlj().zzyq().zzdu(this.zzbls.zzbsn);
            List<Integer> list22 = this.zzbls.zzbtn;
            boolean isCallerInstantApp2 = Wrappers.packageManager(this.zzbls.zzsp).isCallerInstantApp();
            boolean zzyk2 = zzbv.zzlj().zzyk();
            zzbv.zzlh();
            zzasj zzasj3 = zzasj2;
            zzasj zzasj22 = new zzasj(bundle2, zzwb, zzwf2, str32, applicationInfo, packageInfo, zzxy2, zzqa2, zzbbi2, zza2, list3, arrayList42, bundle, zzzb2, i72, i82, f2, zza, j, uuid, zzqw2, str42, zzacp2, zzml2, zzkj2, zzkk2, zzas2, zzy2, z2, zzzg2, zza3, zzye, zzyj2, zzada2, zzzv2, zzaag2, zzyv2, zzaah2, zztx2, zzdu2, zza4, list22, str, arrayList32, i, isCallerInstantApp2, zzyk2, zzayp.zzaaa(), (ArrayList) zzbbq.zza((Future<T>) zzbv.zzlj().zzyr(), null, 1000, TimeUnit.MILLISECONDS), zzbv.zzlf().zzaw(this.zzbls.zzsp), this.zzbls.zzbtk, zzbv.zzlf().zzax(this.zzbls.zzsp));
            return zzasj3;
        }
        j = 0;
        String uuid2 = UUID.randomUUID().toString();
        Bundle zza22 = zzbv.zzlj().zzys().zza(this.zzbls.zzsp, (zzaxq) this);
        ArrayList arrayList5 = new ArrayList();
        ArrayList arrayList22 = new ArrayList();
        while (i2 < this.zzbls.zzbth.size()) {
        }
        zzbcb zza32 = zzayf.zza(new zzf(this));
        zzbcb zza42 = zzayf.zza(new zzg(this));
        if (zzaxj == null) {
        }
        if (packageInfo != null) {
        }
        if (i3 <= zzbv.zzlj().zzyq().zzzh()) {
        }
        str = null;
        zzwf zzwf22 = this.zzbls.zzbst;
        String str322 = this.zzbls.zzbsn;
        String zzxy22 = this.zzbls.zzbsw.zzxy();
        String zzqa22 = zzwu.zzqa();
        ArrayList arrayList322 = arrayList22;
        zzbbi zzbbi22 = this.zzbls.zzbsp;
        ArrayList arrayList422 = arrayList5;
        List<String> list32 = this.zzbls.zzbtt;
        boolean zzzb22 = zzbv.zzlj().zzyq().zzzb();
        int i722 = displayMetrics.widthPixels;
        int i822 = displayMetrics.heightPixels;
        float f22 = displayMetrics.density;
        List zzqw22 = zzaan.zzqw();
        String str422 = this.zzbls.zzbsm;
        zzacp zzacp22 = this.zzbls.zzbti;
        String zzml22 = this.zzbls.zzml();
        float zzkj22 = zzbv.zzlk().zzkj();
        boolean zzkk22 = zzbv.zzlk().zzkk();
        zzbv.zzlf();
        int zzas22 = zzayh.zzas(this.zzbls.zzsp);
        zzbv.zzlf();
        int zzy22 = zzayh.zzy(this.zzbls.zzbsq);
        boolean z22 = this.zzbls.zzsp instanceof Activity;
        boolean zzzg22 = zzbv.zzlj().zzyq().zzzg();
        boolean zzyj22 = zzbv.zzlj().zzyj();
        int zzada22 = zzbv.zzmd().zzada();
        zzbv.zzlf();
        Bundle zzzv22 = zzayh.zzzv();
        String zzaag22 = zzbv.zzlp().zzaag();
        zzyv zzyv22 = this.zzbls.zzbtl;
        boolean zzaah22 = zzbv.zzlp().zzaah();
        Bundle zztx22 = zzahq.zzto().zztx();
        boolean zzdu22 = zzbv.zzlj().zzyq().zzdu(this.zzbls.zzbsn);
        List<Integer> list222 = this.zzbls.zzbtn;
        boolean isCallerInstantApp22 = Wrappers.packageManager(this.zzbls.zzsp).isCallerInstantApp();
        boolean zzyk22 = zzbv.zzlj().zzyk();
        zzbv.zzlh();
        zzasj zzasj32 = zzasj22;
        zzasj zzasj222 = new zzasj(bundle2, zzwb, zzwf22, str322, applicationInfo, packageInfo, zzxy22, zzqa22, zzbbi22, zza22, list32, arrayList422, bundle, zzzb22, i722, i822, f22, zza, j, uuid2, zzqw22, str422, zzacp22, zzml22, zzkj22, zzkk22, zzas22, zzy22, z22, zzzg22, zza32, zzye, zzyj22, zzada22, zzzv22, zzaag22, zzyv22, zzaah22, zztx22, zzdu22, zza42, list222, str, arrayList322, i, isCallerInstantApp22, zzyk22, zzayp.zzaaa(), (ArrayList) zzbbq.zza((Future<T>) zzbv.zzlj().zzyr(), null, 1000, TimeUnit.MILLISECONDS), zzbv.zzlf().zzaw(this.zzbls.zzsp), this.zzbls.zzbtk, zzbv.zzlf().zzax(this.zzbls.zzsp));
        return zzasj32;
    }

    public final void recordImpression() {
        zza(this.zzbls.zzbsu, false);
    }

    /* access modifiers changed from: protected */
    public void zza(@Nullable zzaxf zzaxf, boolean z) {
        if (zzaxf == null) {
            zzaxz.zzeo("Ad state was null when trying to ping impression URLs.");
            return;
        }
        if (zzaxf == null) {
            zzaxz.zzeo("Ad state was null when trying to ping impression URLs.");
        } else {
            zzaxz.zzdn("Pinging Impression URLs.");
            if (this.zzbls.zzbsw != null) {
                this.zzbls.zzbsw.zzxv();
            }
            zzaxf.zzehw.zza(zzb.AD_IMPRESSION);
            if (zzaxf.zzdlr != null && !zzaxf.zzehq) {
                zzbv.zzlf();
                zzayh.zza(this.zzbls.zzsp, this.zzbls.zzbsp.zzdp, zza(zzaxf.zzdlr, zzaxf.zzdzf));
                zzaxf.zzehq = true;
            }
        }
        if (!zzaxf.zzehs || z) {
            if (!(zzaxf.zzehj == null || zzaxf.zzehj.zzdlr == null)) {
                zzbv.zzlz();
                zzakz.zza(this.zzbls.zzsp, this.zzbls.zzbsp.zzdp, zzaxf, this.zzbls.zzbsn, z, zza(zzaxf.zzehj.zzdlr, zzaxf.zzdzf));
            }
            if (!(zzaxf.zzdnb == null || zzaxf.zzdnb.zzdla == null)) {
                zzbv.zzlz();
                zzakz.zza(this.zzbls.zzsp, this.zzbls.zzbsp.zzdp, zzaxf, this.zzbls.zzbsn, z, zzaxf.zzdnb.zzdla);
            }
            zzaxf.zzehs = true;
        }
    }

    /* access modifiers changed from: protected */
    public final void zzb(@Nullable zzaxf zzaxf, boolean z) {
        if (zzaxf != null) {
            if (!(zzaxf == null || zzaxf.zzdls == null || zzaxf.zzehr)) {
                zzbv.zzlf();
                zzayh.zza(this.zzbls.zzsp, this.zzbls.zzbsp.zzdp, zzc(zzaxf.zzdls));
                zzaxf.zzehr = true;
            }
            if (!zzaxf.zzeht || z) {
                if (!(zzaxf.zzehj == null || zzaxf.zzehj.zzdls == null)) {
                    zzbv.zzlz();
                    zzakz.zza(this.zzbls.zzsp, this.zzbls.zzbsp.zzdp, zzaxf, this.zzbls.zzbsn, z, zzc(zzaxf.zzehj.zzdls));
                }
                if (!(zzaxf.zzdnb == null || zzaxf.zzdnb.zzdlb == null)) {
                    zzbv.zzlz();
                    zzakz.zza(this.zzbls.zzsp, this.zzbls.zzbsp.zzdp, zzaxf, this.zzbls.zzbsn, z, zzaxf.zzdnb.zzdlb);
                }
                zzaxf.zzeht = true;
            }
        }
    }

    @Nullable
    public final String getMediationAdapterClassName() {
        if (this.zzbls.zzbsu == null) {
            return null;
        }
        return this.zzbls.zzbsu.zzdnd;
    }

    @Nullable
    public final String zzje() {
        if (this.zzbls.zzbsu == null) {
            return null;
        }
        return zzc(this.zzbls.zzbsu);
    }

    @Nullable
    static String zzc(zzaxf zzaxf) {
        if (zzaxf == null) {
            return null;
        }
        String str = zzaxf.zzdnd;
        if (("com.google.android.gms.ads.mediation.customevent.CustomEventAdapter".equals(str) || "com.google.ads.mediation.customevent.CustomEventAdapter".equals(str)) && zzaxf.zzdnb != null) {
            try {
                return new JSONObject(zzaxf.zzdnb.zzdle).getString("class_name");
            } catch (NullPointerException | JSONException unused) {
            }
        }
        return str;
    }

    public void showInterstitial() {
        zzaxz.zzeo("showInterstitial is not supported for current ad type");
    }

    public final void zzjf() {
        Executor executor = zzbcg.zzepo;
        zzbl zzbl = this.zzblr;
        zzbl.getClass();
        executor.execute(zzd.zza(zzbl));
    }

    public final void zzjg() {
        Executor executor = zzbcg.zzepo;
        zzbl zzbl = this.zzblr;
        zzbl.getClass();
        executor.execute(zze.zza(zzbl));
    }
}
