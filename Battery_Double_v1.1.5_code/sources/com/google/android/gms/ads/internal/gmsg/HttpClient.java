package com.google.android.gms.ads.internal.gmsg;

import android.content.Context;
import android.support.annotation.Keep;
import android.text.TextUtils;
import com.google.android.gms.ads.internal.zzbv;
import com.google.android.gms.common.annotation.KeepName;
import com.google.android.gms.common.util.VisibleForTesting;
import com.google.android.gms.internal.ads.zzahu;
import com.google.android.gms.internal.ads.zzark;
import com.google.android.gms.internal.ads.zzaxz;
import com.google.android.gms.internal.ads.zzayf;
import com.google.android.gms.internal.ads.zzayh;
import com.google.android.gms.internal.ads.zzbax;
import com.google.android.gms.internal.ads.zzbbi;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.ironsource.mediationsdk.utils.ServerResponseWrapper;
import java.io.BufferedOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@Keep
@zzark
@KeepName
public class HttpClient implements zzu<zzahu> {
    private final Context mContext;
    private final zzbbi zzbob;

    @zzark
    @VisibleForTesting
    static class zza {
        private final String mKey;
        private final String mValue;

        public zza(String str, String str2) {
            this.mKey = str;
            this.mValue = str2;
        }

        public final String getKey() {
            return this.mKey;
        }

        public final String getValue() {
            return this.mValue;
        }
    }

    @zzark
    @VisibleForTesting
    static class zzb {
        private final String zzdfr;
        private final URL zzdfs;
        private final ArrayList<zza> zzdft;
        private final String zzdfu;

        zzb(String str, URL url, ArrayList<zza> arrayList, String str2) {
            this.zzdfr = str;
            this.zzdfs = url;
            this.zzdft = arrayList;
            this.zzdfu = str2;
        }

        public final String zzsy() {
            return this.zzdfr;
        }

        public final URL zzsz() {
            return this.zzdfs;
        }

        public final ArrayList<zza> zzta() {
            return this.zzdft;
        }

        public final String zztb() {
            return this.zzdfu;
        }
    }

    @zzark
    @VisibleForTesting
    class zzc {
        private final zzd zzdfv;
        private final boolean zzdfw;
        private final String zzdfx;

        public zzc(HttpClient httpClient, boolean z, zzd zzd, String str) {
            this.zzdfw = z;
            this.zzdfv = zzd;
            this.zzdfx = str;
        }

        public final String getReason() {
            return this.zzdfx;
        }

        public final zzd zztc() {
            return this.zzdfv;
        }

        public final boolean isSuccess() {
            return this.zzdfw;
        }
    }

    @zzark
    @VisibleForTesting
    static class zzd {
        private final List<zza> zzcf;
        private final String zzday;
        private final String zzdfr;
        private final int zzdfy;

        zzd(String str, int i, List<zza> list, String str2) {
            this.zzdfr = str;
            this.zzdfy = i;
            this.zzcf = list;
            this.zzday = str2;
        }

        public final String zzsy() {
            return this.zzdfr;
        }

        public final int getResponseCode() {
            return this.zzdfy;
        }

        public final Iterable<zza> zztd() {
            return this.zzcf;
        }

        public final String getBody() {
            return this.zzday;
        }
    }

    public HttpClient(Context context, zzbbi zzbbi) {
        this.mContext = context;
        this.zzbob = zzbbi;
    }

    private static zzb zzc(JSONObject jSONObject) {
        String optString = jSONObject.optString("http_request_id");
        String optString2 = jSONObject.optString("url");
        URL url = null;
        String optString3 = jSONObject.optString("post_body", null);
        try {
            url = new URL(optString2);
        } catch (MalformedURLException e) {
            zzaxz.zzb("Error constructing http request.", e);
        }
        ArrayList arrayList = new ArrayList();
        JSONArray optJSONArray = jSONObject.optJSONArray("headers");
        if (optJSONArray == null) {
            optJSONArray = new JSONArray();
        }
        for (int i = 0; i < optJSONArray.length(); i++) {
            JSONObject optJSONObject = optJSONArray.optJSONObject(i);
            if (optJSONObject != null) {
                arrayList.add(new zza(optJSONObject.optString("key"), optJSONObject.optString("value")));
            }
        }
        return new zzb(optString, url, arrayList, optString3);
    }

    private static JSONObject zza(zzd zzd2) {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("http_request_id", zzd2.zzsy());
            if (zzd2.getBody() != null) {
                jSONObject.put(TtmlNode.TAG_BODY, zzd2.getBody());
            }
            JSONArray jSONArray = new JSONArray();
            for (zza zza2 : zzd2.zztd()) {
                jSONArray.put(new JSONObject().put("key", zza2.getKey()).put("value", zza2.getValue()));
            }
            jSONObject.put("headers", jSONArray);
            jSONObject.put("response_code", zzd2.getResponseCode());
        } catch (JSONException e) {
            zzaxz.zzb("Error constructing JSON for http response.", e);
        }
        return jSONObject;
    }

    /* JADX WARNING: Removed duplicated region for block: B:32:0x00fd  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x0104  */
    private final zzc zza(zzb zzb2) {
        HttpURLConnection httpURLConnection;
        byte[] bArr;
        try {
            httpURLConnection = (HttpURLConnection) zzb2.zzsz().openConnection();
            try {
                zzbv.zzlf().zza(this.mContext, this.zzbob.zzdp, false, httpURLConnection);
                ArrayList zzta = zzb2.zzta();
                int size = zzta.size();
                int i = 0;
                while (i < size) {
                    Object obj = zzta.get(i);
                    i++;
                    zza zza2 = (zza) obj;
                    httpURLConnection.addRequestProperty(zza2.getKey(), zza2.getValue());
                }
                if (!TextUtils.isEmpty(zzb2.zztb())) {
                    httpURLConnection.setDoOutput(true);
                    bArr = zzb2.zztb().getBytes();
                    httpURLConnection.setFixedLengthStreamingMode(bArr.length);
                    BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(httpURLConnection.getOutputStream());
                    bufferedOutputStream.write(bArr);
                    bufferedOutputStream.close();
                } else {
                    bArr = null;
                }
                zzbax zzbax = new zzbax();
                zzbax.zza(httpURLConnection, bArr);
                ArrayList arrayList = new ArrayList();
                if (httpURLConnection.getHeaderFields() != null) {
                    for (Entry entry : httpURLConnection.getHeaderFields().entrySet()) {
                        for (String zza3 : (List) entry.getValue()) {
                            arrayList.add(new zza((String) entry.getKey(), zza3));
                        }
                    }
                }
                String zzsy = zzb2.zzsy();
                int responseCode = httpURLConnection.getResponseCode();
                zzbv.zzlf();
                zzd zzd2 = new zzd(zzsy, responseCode, arrayList, zzayh.zza(new InputStreamReader(httpURLConnection.getInputStream())));
                zzbax.zza(httpURLConnection, zzd2.getResponseCode());
                zzbax.zzek(zzd2.getBody());
                zzc zzc2 = new zzc(this, true, zzd2, null);
                if (httpURLConnection != null) {
                    httpURLConnection.disconnect();
                }
                return zzc2;
            } catch (Exception e) {
                e = e;
                try {
                    zzc zzc3 = new zzc(this, false, null, e.toString());
                    if (httpURLConnection != null) {
                        httpURLConnection.disconnect();
                    }
                    return zzc3;
                } catch (Throwable th) {
                    th = th;
                    if (httpURLConnection != null) {
                        httpURLConnection.disconnect();
                    }
                    throw th;
                }
            }
        } catch (Exception e2) {
            e = e2;
            httpURLConnection = null;
            zzc zzc32 = new zzc(this, false, null, e.toString());
            if (httpURLConnection != null) {
            }
            return zzc32;
        } catch (Throwable th2) {
            th = th2;
            httpURLConnection = null;
            if (httpURLConnection != null) {
            }
            throw th;
        }
    }

    @Keep
    @KeepName
    public JSONObject send(JSONObject jSONObject) {
        String str;
        JSONObject jSONObject2 = new JSONObject();
        try {
            str = jSONObject.optString("http_request_id");
            try {
                zzc zza2 = zza(zzc(jSONObject));
                if (zza2.isSuccess()) {
                    jSONObject2.put(ServerResponseWrapper.RESPONSE_FIELD, zza(zza2.zztc()));
                    jSONObject2.put("success", true);
                } else {
                    jSONObject2.put(ServerResponseWrapper.RESPONSE_FIELD, new JSONObject().put("http_request_id", str));
                    jSONObject2.put("success", false);
                    jSONObject2.put(IronSourceConstants.EVENTS_ERROR_REASON, zza2.getReason());
                }
            } catch (Exception e) {
                e = e;
                zzaxz.zzb("Error executing http request.", e);
                try {
                    jSONObject2.put(ServerResponseWrapper.RESPONSE_FIELD, new JSONObject().put("http_request_id", str));
                    jSONObject2.put("success", false);
                    jSONObject2.put(IronSourceConstants.EVENTS_ERROR_REASON, e.toString());
                } catch (JSONException e2) {
                    zzaxz.zzb("Error executing http request.", e2);
                }
                return jSONObject2;
            }
        } catch (Exception e3) {
            e = e3;
            str = "";
            zzaxz.zzb("Error executing http request.", e);
            jSONObject2.put(ServerResponseWrapper.RESPONSE_FIELD, new JSONObject().put("http_request_id", str));
            jSONObject2.put("success", false);
            jSONObject2.put(IronSourceConstants.EVENTS_ERROR_REASON, e.toString());
            return jSONObject2;
        }
        return jSONObject2;
    }

    public final /* synthetic */ void zza(Object obj, Map map) {
        zzayf.zzc(new zzv(this, map, (zzahu) obj));
    }
}
