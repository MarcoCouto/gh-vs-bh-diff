package com.google.android.gms.internal.ads;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build.VERSION;
import android.support.annotation.VisibleForTesting;
import android.util.Log;
import android.util.Pair;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.common.GoogleApiAvailabilityLight;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import dalvik.system.DexClassLoader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class zzdl {
    private static final String TAG = "zzdl";
    private volatile boolean zzrp = false;
    protected Context zzsp;
    private ExecutorService zzsq;
    private DexClassLoader zzsr;
    private zzcw zzss;
    private byte[] zzst;
    private volatile AdvertisingIdClient zzsu = null;
    private Future zzsv = null;
    private boolean zzsw;
    /* access modifiers changed from: private */
    public volatile zzbl zzsx = null;
    private Future zzsy = null;
    private zzco zzsz;
    private boolean zzta = false;
    private boolean zztb = false;
    private Map<Pair<String, String>, zzes> zztc;
    private boolean zztd = false;
    /* access modifiers changed from: private */
    public boolean zzte;
    private boolean zztf;

    final class zza extends BroadcastReceiver {
        private zza() {
        }

        public final void onReceive(Context context, Intent intent) {
            if ("android.intent.action.USER_PRESENT".equals(intent.getAction())) {
                zzdl.this.zzte = true;
                return;
            }
            if ("android.intent.action.SCREEN_OFF".equals(intent.getAction())) {
                zzdl.this.zzte = false;
            }
        }

        /* synthetic */ zza(zzdl zzdl, zzdm zzdm) {
            this();
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(31:1|2|(1:4)|5|6|7|8|(1:10)(1:11)|12|(1:14)(1:15)|16|17|18|(2:20|(1:22)(2:23|24))|25|26|27|28|29|(2:31|(1:33)(2:34|35))|36|(1:38)|39|40|41|42|43|44|45|(1:47)|48) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:17:0x004b */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0054 A[Catch:{ zzcx -> 0x0152, zzdi -> 0x0159 }] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0087 A[Catch:{ all -> 0x011f, FileNotFoundException -> 0x014b, IOException -> 0x0144, zzcx -> 0x013d, NullPointerException -> 0x0136 }] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00b2 A[Catch:{ all -> 0x011f, FileNotFoundException -> 0x014b, IOException -> 0x0144, zzcx -> 0x013d, NullPointerException -> 0x0136 }] */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x00fa A[Catch:{ zzcx -> 0x0152, zzdi -> 0x0159 }] */
    public static zzdl zza(Context context, String str, String str2, boolean z) {
        File cacheDir;
        String str3;
        File file;
        zzdl zzdl = new zzdl(context);
        try {
            zzdl.zzsq = Executors.newCachedThreadPool(new zzdm());
            zzdl.zzrp = z;
            if (z) {
                zzdl.zzsv = zzdl.zzsq.submit(new zzdn(zzdl));
            }
            zzdl.zzsq.execute(new zzdp(zzdl));
            GoogleApiAvailabilityLight instance = GoogleApiAvailabilityLight.getInstance();
            zzdl.zzta = instance.getApkVersion(zzdl.zzsp) > 0;
            zzdl.zztb = instance.isGooglePlayServicesAvailable(zzdl.zzsp) == 0;
            zzdl.zza(0, true);
            if (zzds.isMainThread()) {
                if (((Boolean) zzwu.zzpz().zzd(zzaan.zzctr)).booleanValue()) {
                    throw new IllegalStateException("Task Context initialization must not be called from the UI thread.");
                }
            }
            zzdl.zzss = new zzcw(null);
            zzdl.zzst = zzdl.zzss.zzl(str);
            try {
                cacheDir = zzdl.zzsp.getCacheDir();
                if (cacheDir == null) {
                    cacheDir = zzdl.zzsp.getDir("dex", 0);
                    if (cacheDir == null) {
                        throw new zzdi();
                    }
                }
                str3 = "1529567361524";
                file = new File(String.format("%s/%s.jar", new Object[]{cacheDir, str3}));
                if (!file.exists()) {
                    byte[] zza2 = zzdl.zzss.zza(zzdl.zzst, str2);
                    file.createNewFile();
                    FileOutputStream fileOutputStream = new FileOutputStream(file);
                    fileOutputStream.write(zza2, 0, zza2.length);
                    fileOutputStream.close();
                }
                zzdl.zzb(cacheDir, str3);
                zzdl.zzsr = new DexClassLoader(file.getAbsolutePath(), cacheDir.getAbsolutePath(), null, zzdl.zzsp.getClassLoader());
                zzb(file);
                zzdl.zza(cacheDir, str3);
                zzm(String.format("%s/%s.dex", new Object[]{cacheDir, str3}));
                if (!zzdl.zztf) {
                    IntentFilter intentFilter = new IntentFilter();
                    intentFilter.addAction("android.intent.action.USER_PRESENT");
                    intentFilter.addAction("android.intent.action.SCREEN_OFF");
                    zzdl.zzsp.registerReceiver(new zza(zzdl, null), intentFilter);
                    zzdl.zztf = true;
                }
                zzdl.zzsz = new zzco(zzdl);
                zzdl.zztd = true;
            } catch (FileNotFoundException e) {
                throw new zzdi(e);
            } catch (IOException e2) {
                throw new zzdi(e2);
            } catch (zzcx e3) {
                throw new zzdi(e3);
            } catch (NullPointerException e4) {
                throw new zzdi(e4);
            } catch (Throwable th) {
                zzb(file);
                zzdl.zza(cacheDir, str3);
                zzm(String.format("%s/%s.dex", new Object[]{cacheDir, str3}));
                throw th;
            }
        } catch (zzcx e5) {
            throw new zzdi(e5);
        } catch (zzdi unused) {
        }
        return zzdl;
    }

    public final Context getContext() {
        return this.zzsp;
    }

    public final boolean isInitialized() {
        return this.zztd;
    }

    public final ExecutorService zzac() {
        return this.zzsq;
    }

    public final DexClassLoader zzad() {
        return this.zzsr;
    }

    public final zzcw zzae() {
        return this.zzss;
    }

    public final byte[] zzaf() {
        return this.zzst;
    }

    public final boolean zzag() {
        return this.zzta;
    }

    public final zzco zzah() {
        return this.zzsz;
    }

    public final boolean zzai() {
        return this.zztb;
    }

    public final boolean zzaj() {
        return this.zzte;
    }

    public final zzbl zzak() {
        return this.zzsx;
    }

    public final Future zzal() {
        return this.zzsy;
    }

    private zzdl(Context context) {
        boolean z = true;
        this.zzte = true;
        this.zztf = false;
        Context applicationContext = context.getApplicationContext();
        if (applicationContext == null) {
            z = false;
        }
        this.zzsw = z;
        if (this.zzsw) {
            context = applicationContext;
        }
        this.zzsp = context;
        this.zztc = new HashMap();
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(10:20|21|22|23|24|25|26|27|28|30) */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:26:0x0091 */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00a3 A[SYNTHETIC, Splitter:B:39:0x00a3] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x00a8 A[SYNTHETIC, Splitter:B:43:0x00a8] */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x00b2 A[SYNTHETIC, Splitter:B:52:0x00b2] */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x00b7 A[SYNTHETIC, Splitter:B:56:0x00b7] */
    private final void zza(File file, String str) {
        FileInputStream fileInputStream;
        File file2 = new File(String.format("%s/%s.tmp", new Object[]{file, str}));
        if (!file2.exists()) {
            File file3 = new File(String.format("%s/%s.dex", new Object[]{file, str}));
            if (file3.exists()) {
                long length = file3.length();
                if (length > 0) {
                    byte[] bArr = new byte[((int) length)];
                    FileOutputStream fileOutputStream = null;
                    try {
                        fileInputStream = new FileInputStream(file3);
                        try {
                            if (fileInputStream.read(bArr) <= 0) {
                                try {
                                    fileInputStream.close();
                                } catch (IOException unused) {
                                }
                                zzb(file3);
                                return;
                            }
                            zzbp zzbp = new zzbp();
                            zzbp.zzho = VERSION.SDK.getBytes();
                            zzbp.zzhn = str.getBytes();
                            byte[] bytes = this.zzss.zzb(this.zzst, bArr).getBytes();
                            zzbp.data = bytes;
                            zzbp.zzhm = zzbw.zzb(bytes);
                            file2.createNewFile();
                            FileOutputStream fileOutputStream2 = new FileOutputStream(file2);
                            try {
                                byte[] zzb = zzbuz.zzb(zzbp);
                                fileOutputStream2.write(zzb, 0, zzb.length);
                                fileOutputStream2.close();
                                fileInputStream.close();
                                fileOutputStream2.close();
                                zzb(file3);
                            } catch (zzcx | IOException | NoSuchAlgorithmException unused2) {
                                fileOutputStream = fileOutputStream2;
                                if (fileInputStream != null) {
                                }
                                if (fileOutputStream != null) {
                                }
                                zzb(file3);
                            } catch (Throwable th) {
                                th = th;
                                fileOutputStream = fileOutputStream2;
                                if (fileInputStream != null) {
                                }
                                if (fileOutputStream != null) {
                                }
                                zzb(file3);
                                throw th;
                            }
                        } catch (zzcx | IOException | NoSuchAlgorithmException unused3) {
                            if (fileInputStream != null) {
                                try {
                                    fileInputStream.close();
                                } catch (IOException unused4) {
                                }
                            }
                            if (fileOutputStream != null) {
                                try {
                                    fileOutputStream.close();
                                } catch (IOException unused5) {
                                }
                            }
                            zzb(file3);
                        } catch (Throwable th2) {
                            th = th2;
                            if (fileInputStream != null) {
                                try {
                                    fileInputStream.close();
                                } catch (IOException unused6) {
                                }
                            }
                            if (fileOutputStream != null) {
                                try {
                                    fileOutputStream.close();
                                } catch (IOException unused7) {
                                }
                            }
                            zzb(file3);
                            throw th;
                        }
                    } catch (zzcx | IOException | NoSuchAlgorithmException unused8) {
                        fileInputStream = null;
                        if (fileInputStream != null) {
                        }
                        if (fileOutputStream != null) {
                        }
                        zzb(file3);
                    } catch (Throwable th3) {
                        th = th3;
                        fileInputStream = null;
                        if (fileInputStream != null) {
                        }
                        if (fileOutputStream != null) {
                        }
                        zzb(file3);
                        throw th;
                    }
                }
            }
        }
    }

    private static void zzm(String str) {
        zzb(new File(str));
    }

    private static void zzb(File file) {
        if (!file.exists()) {
            Log.d(TAG, String.format("File %s not found. No need for deletion", new Object[]{file.getAbsolutePath()}));
            return;
        }
        file.delete();
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(8:29|30|31|32|33|34|35|36) */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:34:0x00b1 */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x00c7 A[SYNTHETIC, Splitter:B:52:0x00c7] */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x00cc A[SYNTHETIC, Splitter:B:56:0x00cc] */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x00d3 A[SYNTHETIC, Splitter:B:64:0x00d3] */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x00d8 A[SYNTHETIC, Splitter:B:68:0x00d8] */
    private final boolean zzb(File file, String str) {
        FileInputStream fileInputStream;
        File file2 = new File(String.format("%s/%s.tmp", new Object[]{file, str}));
        if (!file2.exists()) {
            return false;
        }
        File file3 = new File(String.format("%s/%s.dex", new Object[]{file, str}));
        if (file3.exists()) {
            return false;
        }
        FileOutputStream fileOutputStream = null;
        try {
            long length = file2.length();
            if (length <= 0) {
                zzb(file2);
                return false;
            }
            byte[] bArr = new byte[((int) length)];
            fileInputStream = new FileInputStream(file2);
            try {
                if (fileInputStream.read(bArr) <= 0) {
                    Log.d(TAG, "Cannot read the cache data.");
                    zzb(file2);
                    try {
                        fileInputStream.close();
                    } catch (IOException unused) {
                    }
                    return false;
                }
                zzbp zzbp = (zzbp) zzbuz.zza(new zzbp(), bArr);
                if (str.equals(new String(zzbp.zzhn)) && Arrays.equals(zzbp.zzhm, zzbw.zzb(zzbp.data))) {
                    if (Arrays.equals(zzbp.zzho, VERSION.SDK.getBytes())) {
                        byte[] zza2 = this.zzss.zza(this.zzst, new String(zzbp.data));
                        file3.createNewFile();
                        FileOutputStream fileOutputStream2 = new FileOutputStream(file3);
                        try {
                            fileOutputStream2.write(zza2, 0, zza2.length);
                            fileInputStream.close();
                            fileOutputStream2.close();
                            return true;
                        } catch (zzcx | IOException | NoSuchAlgorithmException unused2) {
                            fileOutputStream = fileOutputStream2;
                            if (fileInputStream != null) {
                                try {
                                    fileInputStream.close();
                                } catch (IOException unused3) {
                                }
                            }
                            if (fileOutputStream != null) {
                                try {
                                    fileOutputStream.close();
                                } catch (IOException unused4) {
                                }
                            }
                            return false;
                        } catch (Throwable th) {
                            th = th;
                            fileOutputStream = fileOutputStream2;
                            if (fileInputStream != null) {
                                try {
                                    fileInputStream.close();
                                } catch (IOException unused5) {
                                }
                            }
                            if (fileOutputStream != null) {
                                try {
                                    fileOutputStream.close();
                                } catch (IOException unused6) {
                                }
                            }
                            throw th;
                        }
                    }
                }
                zzb(file2);
                try {
                    fileInputStream.close();
                } catch (IOException unused7) {
                }
                return false;
            } catch (zzcx | IOException | NoSuchAlgorithmException unused8) {
                if (fileInputStream != null) {
                }
                if (fileOutputStream != null) {
                }
                return false;
            } catch (Throwable th2) {
                th = th2;
                if (fileInputStream != null) {
                }
                if (fileOutputStream != null) {
                }
                throw th;
            }
        } catch (zzcx | IOException | NoSuchAlgorithmException unused9) {
            fileInputStream = null;
            if (fileInputStream != null) {
            }
            if (fileOutputStream != null) {
            }
            return false;
        } catch (Throwable th3) {
            th = th3;
            fileInputStream = null;
            if (fileInputStream != null) {
            }
            if (fileOutputStream != null) {
            }
            throw th;
        }
    }

    public final boolean zza(String str, String str2, Class<?>... clsArr) {
        if (this.zztc.containsKey(new Pair(str, str2))) {
            return false;
        }
        this.zztc.put(new Pair(str, str2), new zzes(this, str, str2, clsArr));
        return true;
    }

    public final Method zza(String str, String str2) {
        zzes zzes = (zzes) this.zztc.get(new Pair(str, str2));
        if (zzes == null) {
            return null;
        }
        return zzes.zzax();
    }

    /* access modifiers changed from: private */
    public final void zzam() {
        try {
            if (this.zzsu == null && this.zzsw) {
                AdvertisingIdClient advertisingIdClient = new AdvertisingIdClient(this.zzsp);
                advertisingIdClient.start();
                this.zzsu = advertisingIdClient;
            }
        } catch (GooglePlayServicesNotAvailableException | GooglePlayServicesRepairableException | IOException unused) {
            this.zzsu = null;
        }
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public final void zza(int i, boolean z) {
        if (this.zztb) {
            Future submit = this.zzsq.submit(new zzdo(this, i, z));
            if (i == 0) {
                this.zzsy = submit;
            }
        }
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public final zzbl zzb(int i, boolean z) {
        if (i > 0 && z) {
            try {
                Thread.sleep((long) (i * 1000));
            } catch (InterruptedException unused) {
            }
        }
        return zzan();
    }

    /* access modifiers changed from: private */
    public static boolean zza(int i, zzbl zzbl) {
        if (i < 4) {
            if (zzbl == null) {
                return true;
            }
            if (((Boolean) zzwu.zzpz().zzd(zzaan.zzctu)).booleanValue() && (zzbl.zzdq == null || zzbl.zzdq.equals("0000000000000000000000000000000000000000000000000000000000000000"))) {
                return true;
            }
            if (((Boolean) zzwu.zzpz().zzd(zzaan.zzctv)).booleanValue() && (zzbl.zzgj == null || zzbl.zzgj.zzhh == null || zzbl.zzgj.zzhh.longValue() == -2)) {
                return true;
            }
        }
        return false;
    }

    @VisibleForTesting
    private final zzbl zzan() {
        try {
            return zzbjd.zzk(this.zzsp, this.zzsp.getPackageName(), Integer.toString(this.zzsp.getPackageManager().getPackageInfo(this.zzsp.getPackageName(), 0).versionCode));
        } catch (Throwable unused) {
            return null;
        }
    }

    public final AdvertisingIdClient zzao() {
        if (!this.zzrp) {
            return null;
        }
        if (this.zzsu != null) {
            return this.zzsu;
        }
        if (this.zzsv != null) {
            try {
                this.zzsv.get(2000, TimeUnit.MILLISECONDS);
                this.zzsv = null;
            } catch (InterruptedException | ExecutionException unused) {
            } catch (TimeoutException unused2) {
                this.zzsv.cancel(true);
            }
        }
        return this.zzsu;
    }

    public final int zzy() {
        if (this.zzsz != null) {
            return zzco.zzy();
        }
        return Integer.MIN_VALUE;
    }
}
