package com.google.android.gms.internal.ads;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.Nullable;
import com.google.android.gms.ads.internal.zzbv;
import com.google.android.gms.common.util.IOUtils;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

@zzark
public final class zzbep implements zzov {
    private boolean isOpen;
    private Uri uri;
    private InputStream zzevf;
    private final zzov zzevg;
    @Nullable
    private final zzpn<zzov> zzevh;
    private final zzbeq zzevi;
    private final Context zzsp;

    public zzbep(Context context, zzov zzov, zzpn<zzov> zzpn, zzbeq zzbeq) {
        this.zzsp = context;
        this.zzevg = zzov;
        this.zzevh = zzpn;
        this.zzevi = zzbeq;
    }

    public final void close() throws IOException {
        if (this.isOpen) {
            this.isOpen = false;
            this.uri = null;
            if (this.zzevf != null) {
                IOUtils.closeQuietly((Closeable) this.zzevf);
                this.zzevf = null;
            } else {
                this.zzevg.close();
            }
            if (this.zzevh != null) {
                this.zzevh.zze(this);
                return;
            }
            return;
        }
        throw new IOException("Attempt to close an already closed CacheDataSource.");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x009f, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:?, code lost:
        r4.cancel(true);
        java.lang.Thread.currentThread().interrupt();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:?, code lost:
        r4.cancel(true);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x00d6, code lost:
        r4 = com.google.android.gms.ads.internal.zzbv.zzlm().elapsedRealtime() - r9;
        r1.zzevi.zzb(false, r4);
        r2 = new java.lang.StringBuilder(44);
        r2.append("Cache connection took ");
        r2.append(r4);
        r2.append("ms");
        com.google.android.gms.internal.ads.zzaxz.v(r2.toString());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x00fe, code lost:
        r2 = com.google.android.gms.ads.internal.zzbv.zzlm().elapsedRealtime() - r9;
        r1.zzevi.zzb(false, r2);
        r4 = new java.lang.StringBuilder(44);
        r4.append("Cache connection took ");
        r4.append(r2);
        r4.append("ms");
        com.google.android.gms.internal.ads.zzaxz.v(r4.toString());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0125, code lost:
        throw r0;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [B:18:0x00a1, B:21:0x00d3] */
    /* JADX WARNING: Missing exception handler attribute for start block: B:18:0x00a1 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:21:0x00d3 */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:18:0x00a1=Splitter:B:18:0x00a1, B:21:0x00d3=Splitter:B:21:0x00d3} */
    public final long zza(zzoz zzoz) throws IOException {
        Long l;
        zzoz zzoz2 = zzoz;
        if (!this.isOpen) {
            this.isOpen = true;
            this.uri = zzoz2.uri;
            if (this.zzevh != null) {
                this.zzevh.zza(this, zzoz2);
            }
            zzty zzd = zzty.zzd(zzoz2.uri);
            if (!((Boolean) zzwu.zzpz().zzd(zzaan.zzcvv)).booleanValue()) {
                zztv zztv = null;
                if (zzd != null) {
                    zzd.zzcab = zzoz2.zzaha;
                    zztv = zzbv.zzll().zza(zzd);
                }
                if (zztv != null && zztv.zzoe()) {
                    this.zzevf = zztv.zzof();
                    return -1;
                }
            } else if (zzd != null) {
                zzd.zzcab = zzoz2.zzaha;
                if (zzd.zzcaa) {
                    l = (Long) zzwu.zzpz().zzd(zzaan.zzcvx);
                } else {
                    l = (Long) zzwu.zzpz().zzd(zzaan.zzcvw);
                }
                long longValue = l.longValue();
                long elapsedRealtime = zzbv.zzlm().elapsedRealtime();
                zzbv.zzmb();
                Future zza = zzul.zza(this.zzsp, zzd);
                this.zzevf = (InputStream) zza.get(longValue, TimeUnit.MILLISECONDS);
                long elapsedRealtime2 = zzbv.zzlm().elapsedRealtime() - elapsedRealtime;
                this.zzevi.zzb(true, elapsedRealtime2);
                StringBuilder sb = new StringBuilder(44);
                sb.append("Cache connection took ");
                sb.append(elapsedRealtime2);
                sb.append("ms");
                zzaxz.v(sb.toString());
                return -1;
            }
            if (zzd != null) {
                zzoz zzoz3 = new zzoz(Uri.parse(zzd.url), zzoz2.zzbft, zzoz2.zzbfu, zzoz2.zzaha, zzoz2.zzcc, zzoz2.zzcb, zzoz2.flags);
                zzoz2 = zzoz3;
            }
            return this.zzevg.zza(zzoz2);
        }
        throw new IOException("Attempt to open an already open CacheDataSource.");
    }

    public final int read(byte[] bArr, int i, int i2) throws IOException {
        int i3;
        if (this.isOpen) {
            if (this.zzevf != null) {
                i3 = this.zzevf.read(bArr, i, i2);
            } else {
                i3 = this.zzevg.read(bArr, i, i2);
            }
            if (this.zzevh != null) {
                this.zzevh.zzc(this, i3);
            }
            return i3;
        }
        throw new IOException("Attempt to read closed CacheDataSource.");
    }

    public final Uri getUri() {
        return this.uri;
    }
}
