package com.google.android.gms.internal.ads;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.facebook.share.internal.MessengerShareContentUtility;
import com.google.android.exoplayer2.util.MimeTypes;
import com.google.android.gms.ads.internal.gmsg.zzu;
import com.google.android.gms.ads.internal.zzbb;
import com.google.android.gms.ads.internal.zzbv;
import com.google.android.gms.common.util.VisibleForTesting;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.mintegral.msdk.base.entity.CampaignUnit;
import com.vungle.warren.model.ReportDBAdapter.ReportColumns;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicInteger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@zzark
public final class zzapw implements Callable<zzaxf> {
    @VisibleForTesting
    private static long zzdtd = 10;
    private final Context mContext;
    private int mErrorCode;
    private final Object mLock = new Object();
    private final zzaba zzbln;
    private final zzaqp zzbqa;
    private final zzcu zzdcf;
    private final zzaxg zzdsk;
    private final zzazs zzdte;
    /* access modifiers changed from: private */
    public final zzbb zzdtf;
    private boolean zzdtg;
    private List<String> zzdth;
    private JSONObject zzdti;
    private String zzdtj;
    @Nullable
    private String zzdtk;

    public zzapw(Context context, zzbb zzbb, zzazs zzazs, zzcu zzcu, zzaxg zzaxg, zzaba zzaba) {
        this.mContext = context;
        this.zzdtf = zzbb;
        this.zzdte = zzazs;
        this.zzdsk = zzaxg;
        this.zzdcf = zzcu;
        this.zzbln = zzaba;
        this.zzbqa = zzbb.zzkn();
        this.zzdtg = false;
        this.mErrorCode = -2;
        this.zzdth = null;
        this.zzdtj = null;
        this.zzdtk = null;
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x003e, code lost:
        if (r3.length() != 0) goto L_0x0044;
     */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0071 A[Catch:{ InterruptedException | CancellationException | ExecutionException | JSONException -> 0x0217, TimeoutException -> 0x0210, Exception -> 0x0209 }] */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0078 A[Catch:{ InterruptedException | CancellationException | ExecutionException | JSONException -> 0x0217, TimeoutException -> 0x0210, Exception -> 0x0209 }] */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x007b A[Catch:{ InterruptedException | CancellationException | ExecutionException | JSONException -> 0x0217, TimeoutException -> 0x0210, Exception -> 0x0209 }] */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0081 A[Catch:{ InterruptedException | CancellationException | ExecutionException | JSONException -> 0x0217, TimeoutException -> 0x0210, Exception -> 0x0209 }] */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x00b5 A[Catch:{ InterruptedException | CancellationException | ExecutionException | JSONException -> 0x0217, TimeoutException -> 0x0210, Exception -> 0x0209 }] */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x0178 A[Catch:{ InterruptedException | CancellationException | ExecutionException | JSONException -> 0x0217, TimeoutException -> 0x0210, Exception -> 0x0209 }] */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x017a A[Catch:{ InterruptedException | CancellationException | ExecutionException | JSONException -> 0x0217, TimeoutException -> 0x0210, Exception -> 0x0209 }] */
    /* JADX WARNING: Removed duplicated region for block: B:85:0x01ec A[Catch:{ InterruptedException | CancellationException | ExecutionException | JSONException -> 0x0217, TimeoutException -> 0x0210, Exception -> 0x0209 }] */
    /* JADX WARNING: Removed duplicated region for block: B:96:0x0221  */
    /* renamed from: zzwd */
    public final zzaxf call() {
        JSONObject jSONObject;
        boolean optBoolean;
        zzbcb zza;
        zzaqe zzaqe;
        zzacf zzacf;
        Object[] objArr;
        List<String> list;
        try {
            String uuid = this.zzdtf.getUuid();
            if (!zzwe()) {
                JSONObject jSONObject2 = new JSONObject(this.zzdsk.zzehy.zzdyb);
                JSONObject jSONObject3 = new JSONObject(this.zzdsk.zzehy.zzdyb);
                if (jSONObject3.length() != 0) {
                    JSONArray optJSONArray = jSONObject3.optJSONArray(CampaignUnit.JSON_KEY_ADS);
                    JSONObject optJSONObject = optJSONArray != null ? optJSONArray.optJSONObject(0) : null;
                    if (optJSONObject != null) {
                    }
                }
                zzcs(3);
                JSONObject jSONObject4 = (JSONObject) this.zzbqa.zzh(jSONObject2).get(zzdtd, TimeUnit.SECONDS);
                if (jSONObject4.optBoolean("success", false)) {
                    jSONObject = jSONObject4.getJSONObject("json").optJSONArray(CampaignUnit.JSON_KEY_ADS).getJSONObject(0);
                    optBoolean = jSONObject == null ? jSONObject.optBoolean("enable_omid") : false;
                    if (optBoolean) {
                        zza = zzbbq.zzm(null);
                    } else {
                        JSONObject optJSONObject2 = jSONObject.optJSONObject("omid_settings");
                        if (optJSONObject2 == null) {
                            zza = zzbbq.zzm(null);
                        } else {
                            String optString = optJSONObject2.optString("omid_html");
                            if (TextUtils.isEmpty(optString)) {
                                zza = zzbbq.zzm(null);
                            } else {
                                zza = zzbbq.zza((zzbcb<A>) zzbbq.zzm(null), (zzbbl<? super A, ? extends B>) new zzapx<Object,Object>(this, optString), zzbcg.zzepo);
                            }
                        }
                    }
                    zzbcb zzbcb = zza;
                    if (!zzwe()) {
                        if (jSONObject != null) {
                            String string = jSONObject.getString(ReportColumns.COLUMN_TEMPATE_ID);
                            boolean equals = "instream".equals(jSONObject.optString("type"));
                            boolean z = this.zzdsk.zzeag.zzbti != null ? this.zzdsk.zzeag.zzbti.zzdcs : false;
                            boolean z2 = this.zzdsk.zzeag.zzbti != null ? this.zzdsk.zzeag.zzbti.zzdcu : false;
                            if (equals) {
                                zzaqe = new zzapv();
                            } else if ("2".equals(string)) {
                                zzaqe = new zzaqq(z, z2, this.zzdsk.zzehx);
                            } else if ("1".equals(string)) {
                                zzaqe = new zzaqr(z, z2, this.zzdsk.zzehx);
                            } else if ("3".equals(string)) {
                                String string2 = jSONObject.getString("custom_template_id");
                                zzbcl zzbcl = new zzbcl();
                                zzayh.zzelc.post(new zzapz(this, zzbcl, string2));
                                if (zzbcl.get(zzdtd, TimeUnit.SECONDS) != null) {
                                    zzaqe = new zzaqs(z);
                                } else {
                                    String str = "No handler for custom template: ";
                                    String valueOf = String.valueOf(jSONObject.getString("custom_template_id"));
                                    zzaxz.e(valueOf.length() != 0 ? str.concat(valueOf) : new String(str));
                                }
                            } else {
                                zzcs(0);
                            }
                            if (!zzwe() && zzaqe != null) {
                                if (jSONObject != null) {
                                    JSONObject jSONObject5 = jSONObject.getJSONObject("tracking_urls_and_actions");
                                    JSONArray optJSONArray2 = jSONObject5.optJSONArray("impression_tracking_urls");
                                    if (optJSONArray2 == null) {
                                        objArr = null;
                                    } else {
                                        objArr = new String[optJSONArray2.length()];
                                        for (int i = 0; i < optJSONArray2.length(); i++) {
                                            objArr[i] = optJSONArray2.getString(i);
                                        }
                                    }
                                    if (objArr == null) {
                                        list = null;
                                    } else {
                                        list = Arrays.asList(objArr);
                                    }
                                    this.zzdth = list;
                                    this.zzdti = jSONObject5.optJSONObject("active_view");
                                    this.zzdtj = jSONObject.optString("debug_signals");
                                    this.zzdtk = jSONObject.optString("omid_settings");
                                    zzacf = zzaqe.zza(this, jSONObject);
                                    if (zzacf == null) {
                                        zzaxz.e("Failed to retrieve ad assets.");
                                    } else {
                                        zzach zzach = new zzach(this.mContext, this.zzdtf, this.zzbqa, this.zzdcf, jSONObject, zzacf, this.zzdsk.zzeag.zzbsp, uuid);
                                        zzacf.zzb(zzach);
                                        if (zzacf instanceof zzabw) {
                                            this.zzbqa.zza("/nativeAdCustomClick", (zzu<? super T>) new zzaqa<Object>(this, (zzabw) zzacf));
                                        }
                                        zzaxf zza2 = zza(zzacf, optBoolean);
                                        this.zzdtf.zzg(zzb(zzbcb));
                                        return zza2;
                                    }
                                }
                            }
                            zzacf = null;
                            if (zzacf instanceof zzabw) {
                            }
                            zzaxf zza22 = zza(zzacf, optBoolean);
                            this.zzdtf.zzg(zzb(zzbcb));
                            return zza22;
                        }
                    }
                    zzaqe = null;
                    if (jSONObject != null) {
                    }
                    zzacf = null;
                    if (zzacf instanceof zzabw) {
                    }
                    zzaxf zza222 = zza(zzacf, optBoolean);
                    this.zzdtf.zzg(zzb(zzbcb));
                    return zza222;
                }
            }
            jSONObject = null;
            if (jSONObject == null) {
            }
            if (optBoolean) {
            }
            zzbcb zzbcb2 = zza;
            if (!zzwe()) {
            }
            zzaqe = null;
            if (jSONObject != null) {
            }
            zzacf = null;
            if (zzacf instanceof zzabw) {
            }
            zzaxf zza2222 = zza(zzacf, optBoolean);
            this.zzdtf.zzg(zzb(zzbcb2));
            return zza2222;
        } catch (InterruptedException | CancellationException | ExecutionException | JSONException e) {
            zzaxz.zzc("Malformed native JSON response.", e);
            if (!this.zzdtg) {
                zzcs(0);
            }
            return zza((zzacf) null, false);
        } catch (TimeoutException e2) {
            zzaxz.zzc("Timeout when loading native ad.", e2);
            if (!this.zzdtg) {
            }
            return zza((zzacf) null, false);
        } catch (Exception e3) {
            zzaxz.zzc("Error occured while doing native ads initialization.", e3);
            if (!this.zzdtg) {
            }
            return zza((zzacf) null, false);
        }
    }

    private static zzbgg zzb(zzbcb<zzbgg> zzbcb) {
        try {
            return (zzbgg) zzbcb.get((long) ((Integer) zzwu.zzpz().zzd(zzaan.zzcui)).intValue(), TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            zzbbd.zzc("", e);
            Thread.currentThread().interrupt();
            return null;
        } catch (CancellationException | ExecutionException | TimeoutException e2) {
            zzbbd.zzc("", e2);
            return null;
        }
    }

    /* access modifiers changed from: private */
    public final void zzc(zzadx zzadx, String str) {
        try {
            zzaeh zzar = this.zzdtf.zzar(zzadx.getCustomTemplateId());
            if (zzar != null) {
                zzar.zzb(zzadx, str);
            }
        } catch (RemoteException e) {
            StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 40);
            sb.append("Failed to call onCustomClick for asset ");
            sb.append(str);
            sb.append(".");
            zzaxz.zzc(sb.toString(), e);
        }
    }

    private final zzaxf zza(zzacf zzacf, boolean z) {
        int i;
        synchronized (this.mLock) {
            i = (zzacf == null && this.mErrorCode == -2) ? 0 : this.mErrorCode;
        }
        zzaxf zzaxf = new zzaxf(this.zzdsk.zzeag.zzdwg, null, this.zzdsk.zzehy.zzdlq, i, this.zzdsk.zzehy.zzdlr, this.zzdth, this.zzdsk.zzehy.orientation, this.zzdsk.zzehy.zzdlx, this.zzdsk.zzeag.zzdwj, false, null, null, null, null, null, 0, this.zzdsk.zzbst, this.zzdsk.zzehy.zzdyc, this.zzdsk.zzehn, this.zzdsk.zzeho, this.zzdsk.zzehy.zzdyi, this.zzdti, i != -2 ? null : zzacf, null, null, null, this.zzdsk.zzehy.zzdyu, this.zzdsk.zzehy.zzdyv, null, this.zzdsk.zzehy.zzdlu, this.zzdtj, this.zzdsk.zzehw, this.zzdsk.zzehy.zzbph, this.zzdsk.zzehx, z, this.zzdsk.zzehy.zzdls, this.zzdsk.zzehy.zzbpi, this.zzdtk, this.zzdsk.zzehy.zzdzf);
        return zzaxf;
    }

    public final zzbcb<zzabm> zzg(JSONObject jSONObject) throws JSONException {
        JSONObject optJSONObject = jSONObject.optJSONObject("attribution");
        if (optJSONObject == null) {
            return zzbbq.zzm(null);
        }
        String optString = optJSONObject.optString(MimeTypes.BASE_TYPE_TEXT);
        int optInt = optJSONObject.optInt("text_size", -1);
        Integer zzb = zzb(optJSONObject, "text_color");
        Integer zzb2 = zzb(optJSONObject, "bg_color");
        int optInt2 = optJSONObject.optInt("animation_ms", 1000);
        int optInt3 = optJSONObject.optInt("presentation_ms", 4000);
        int i = (this.zzdsk.zzeag.zzbti == null || this.zzdsk.zzeag.zzbti.versionCode < 2) ? 1 : this.zzdsk.zzeag.zzbti.zzdcv;
        boolean optBoolean = optJSONObject.optBoolean("allow_pub_rendering");
        List<zzbcb> arrayList = new ArrayList<>();
        if (optJSONObject.optJSONArray("images") != null) {
            arrayList = zza(optJSONObject, "images", false, false, true);
        } else {
            arrayList.add(zza(optJSONObject, MessengerShareContentUtility.MEDIA_IMAGE, false, false));
        }
        zzbcl zzbcl = new zzbcl();
        int size = arrayList.size();
        AtomicInteger atomicInteger = new AtomicInteger(0);
        for (zzbcb zza : arrayList) {
            List list = arrayList;
            zza.zza(new zzaqd(atomicInteger, size, zzbcl, arrayList), zzayf.zzeky);
            arrayList = list;
        }
        zzbcl zzbcl2 = zzbcl;
        zzaqb zzaqb = new zzaqb(this, optString, zzb2, zzb, optInt, optInt3, optInt2, i, optBoolean);
        return zzbbq.zza((zzbcb<A>) zzbcl2, (zzbbm<A, B>) zzaqb, (Executor) zzayf.zzeky);
    }

    private static Integer zzb(JSONObject jSONObject, String str) {
        try {
            JSONObject jSONObject2 = jSONObject.getJSONObject(str);
            return Integer.valueOf(Color.rgb(jSONObject2.getInt(CampaignEx.JSON_KEY_AD_R), jSONObject2.getInt("g"), jSONObject2.getInt("b")));
        } catch (JSONException unused) {
            return null;
        }
    }

    public final Future<zzabr> zza(JSONObject jSONObject, String str, boolean z) throws JSONException {
        JSONObject jSONObject2 = jSONObject.getJSONObject(str);
        boolean optBoolean = jSONObject2.optBoolean("require", true);
        if (jSONObject2 == null) {
            jSONObject2 = new JSONObject();
        }
        return zza(jSONObject2, optBoolean, z);
    }

    public final zzbcb<zzabr> zza(JSONObject jSONObject, String str, boolean z, boolean z2) throws JSONException {
        JSONObject jSONObject2 = z ? jSONObject.getJSONObject(str) : jSONObject.optJSONObject(str);
        if (jSONObject2 == null) {
            jSONObject2 = new JSONObject();
        }
        return zza(jSONObject2, z, z2);
    }

    public final List<zzbcb<zzabr>> zza(JSONObject jSONObject, String str, boolean z, boolean z2, boolean z3) throws JSONException {
        JSONArray optJSONArray = jSONObject.optJSONArray(str);
        ArrayList arrayList = new ArrayList();
        if (optJSONArray == null || optJSONArray.length() == 0) {
            zzh(0, false);
            return arrayList;
        }
        int length = z3 ? optJSONArray.length() : 1;
        for (int i = 0; i < length; i++) {
            JSONObject jSONObject2 = optJSONArray.getJSONObject(i);
            if (jSONObject2 == null) {
                jSONObject2 = new JSONObject();
            }
            arrayList.add(zza(jSONObject2, false, z2));
        }
        return arrayList;
    }

    private final zzbcb<zzabr> zza(JSONObject jSONObject, boolean z, boolean z2) throws JSONException {
        String str;
        if (z) {
            str = jSONObject.getString("url");
        } else {
            str = jSONObject.optString("url");
        }
        double optDouble = jSONObject.optDouble("scale", 1.0d);
        boolean optBoolean = jSONObject.optBoolean("is_transparent", true);
        if (TextUtils.isEmpty(str)) {
            zzh(0, z);
            return zzbbq.zzm(null);
        } else if (z2) {
            return zzbbq.zzm(new zzabr(null, Uri.parse(str), optDouble));
        } else {
            zzazs zzazs = this.zzdte;
            zzaqc zzaqc = new zzaqc(this, z, optDouble, optBoolean, str);
            return zzazs.zza(str, zzaqc);
        }
    }

    public final zzbcb<zzbgg> zzc(JSONObject jSONObject, String str) throws JSONException {
        JSONObject optJSONObject = jSONObject.optJSONObject(str);
        if (optJSONObject == null) {
            return zzbbq.zzm(null);
        }
        if (TextUtils.isEmpty(optJSONObject.optString("vast_xml"))) {
            zzaxz.zzeo("Required field 'vast_xml' is missing");
            return zzbbq.zzm(null);
        }
        zzaqf zza = zza(this.mContext, this.zzdcf, this.zzdsk, this.zzbln, this.zzdtf);
        boolean equals = "instream".equals(jSONObject.optString("type"));
        zzbcl zzbcl = new zzbcl();
        zzbcg.zzepo.execute(new zzaqg(zza, equals, optJSONObject, zzbcl));
        return zzbcl;
    }

    public final zzbcb<zzbgg> zza(String str, String str2, boolean z) {
        if (TextUtils.isEmpty(str) || TextUtils.isEmpty(str2)) {
            return zzbbq.zzm(null);
        }
        zzaqf zza = zza(this.mContext, this.zzdcf, this.zzdsk, this.zzbln, this.zzdtf);
        zzbcl zzbcl = new zzbcl();
        Executor executor = zzbcg.zzepo;
        zzaqh zzaqh = new zzaqh(zza, true, zzbcl, str, str2);
        executor.execute(zzaqh);
        return zzbcl;
    }

    private final boolean zzwe() {
        boolean z;
        synchronized (this.mLock) {
            z = this.zzdtg;
        }
        return z;
    }

    private final void zzcs(int i) {
        synchronized (this.mLock) {
            this.zzdtg = true;
            this.mErrorCode = i;
        }
    }

    public final void zzh(int i, boolean z) {
        if (z) {
            zzcs(i);
        }
    }

    static zzbgg zzc(zzbcb<zzbgg> zzbcb) {
        try {
            return (zzbgg) zzbcb.get((long) ((Integer) zzwu.zzpz().zzd(zzaan.zzcuh)).intValue(), TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            zzaxz.zzc("InterruptedException occurred while waiting for video to load", e);
            Thread.currentThread().interrupt();
            return null;
        } catch (CancellationException | ExecutionException | TimeoutException e2) {
            zzaxz.zzc("Exception occurred while waiting for video to load", e2);
            return null;
        }
    }

    /* access modifiers changed from: private */
    public static <V> List<V> zzk(List<zzbcb<V>> list) throws ExecutionException, InterruptedException {
        ArrayList arrayList = new ArrayList();
        for (zzbcb zzbcb : list) {
            Object obj = zzbcb.get();
            if (obj != null) {
                arrayList.add(obj);
            }
        }
        return arrayList;
    }

    @VisibleForTesting
    private static zzaqf zza(Context context, zzcu zzcu, zzaxg zzaxg, zzaba zzaba, zzbb zzbb) {
        zzaqf zzaqf = new zzaqf(context, zzcu, zzaxg, zzaba, zzbb);
        return zzaqf;
    }

    /* access modifiers changed from: 0000 */
    public final /* synthetic */ zzbcb zza(String str, Object obj) throws Exception {
        zzbv.zzlg();
        zzbgg zza = zzbgm.zza(this.mContext, zzbht.zzaey(), "native-omid", false, false, this.zzdcf, this.zzdsk.zzeag.zzbsp, this.zzbln, null, this.zzdtf.zzid(), this.zzdsk.zzehw);
        zzbck zzn = zzbck.zzn(zza);
        zza.zzadl().zza((zzbho) new zzapy(zzn));
        zza.loadData(str, WebRequest.CONTENT_TYPE_HTML, "UTF-8");
        return zzn;
    }
}
