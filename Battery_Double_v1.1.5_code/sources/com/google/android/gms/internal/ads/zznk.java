package com.google.android.gms.internal.ads;

import android.net.Uri;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.util.Pair;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.util.MimeTypes;
import com.google.android.gms.internal.ads.zzhp.zza;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.ironsource.sdk.constants.Constants.ParametersKeys;
import com.mintegral.msdk.base.b.d;
import com.mintegral.msdk.base.entity.CampaignEx;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.xml.sax.helpers.DefaultHandler;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

public final class zznk extends DefaultHandler implements zzpm<zznj> {
    private static final Pattern zzbcz = Pattern.compile("(\\d+)(?:/(\\d+))?");
    private static final Pattern zzbda = Pattern.compile("CC([1-4])=.*");
    private static final Pattern zzbdb = Pattern.compile("([1-9]|[1-5][0-9]|6[0-3])=.*");
    private final String zzbdc;
    private final XmlPullParserFactory zzbdd;

    public zznk() {
        this(null);
    }

    private zznk(String str) {
        this.zzbdc = null;
        try {
            this.zzbdd = XmlPullParserFactory.newInstance();
        } catch (XmlPullParserException e) {
            throw new RuntimeException("Couldn't create XmlPullParserFactory instance", e);
        }
    }

    /* JADX WARNING: type inference failed for: r7v0 */
    /* JADX WARNING: type inference failed for: r7v1 */
    /* JADX WARNING: type inference failed for: r33v0 */
    /* JADX WARNING: type inference failed for: r7v4 */
    /* JADX WARNING: type inference failed for: r7v8 */
    /* JADX WARNING: type inference failed for: r7v9 */
    /* JADX WARNING: type inference failed for: r4v25, types: [com.google.android.gms.internal.ads.zznw] */
    /* JADX WARNING: type inference failed for: r4v26, types: [com.google.android.gms.internal.ads.zznv] */
    /* JADX WARNING: type inference failed for: r4v27 */
    /* JADX WARNING: type inference failed for: r7v12 */
    /* JADX WARNING: type inference failed for: r4v28, types: [com.google.android.gms.internal.ads.zzny] */
    /* JADX WARNING: type inference failed for: r37v0 */
    /* JADX WARNING: type inference failed for: r37v2 */
    /* JADX WARNING: type inference failed for: r11v8, types: [java.lang.Object] */
    /* JADX WARNING: type inference failed for: r56v0, types: [com.google.android.gms.internal.ads.zznr] */
    /* JADX WARNING: type inference failed for: r67v2, types: [com.google.android.gms.internal.ads.zzns] */
    /* JADX WARNING: type inference failed for: r37v4 */
    /* JADX WARNING: type inference failed for: r37v5 */
    /* JADX WARNING: type inference failed for: r2v61, types: [com.google.android.gms.internal.ads.zznw] */
    /* JADX WARNING: type inference failed for: r2v64, types: [com.google.android.gms.internal.ads.zznv] */
    /* JADX WARNING: type inference failed for: r2v65 */
    /* JADX WARNING: type inference failed for: r37v6 */
    /* JADX WARNING: type inference failed for: r2v68, types: [com.google.android.gms.internal.ads.zzny] */
    /* JADX WARNING: type inference failed for: r7v25 */
    /* JADX WARNING: type inference failed for: r7v26 */
    /* JADX WARNING: type inference failed for: r7v27 */
    /* JADX WARNING: type inference failed for: r7v28 */
    /* JADX WARNING: type inference failed for: r4v54 */
    /* JADX WARNING: type inference failed for: r4v55 */
    /* JADX WARNING: type inference failed for: r4v56 */
    /* JADX WARNING: type inference failed for: r37v7 */
    /* JADX WARNING: type inference failed for: r56v2, types: [com.google.android.gms.internal.ads.zznr] */
    /* JADX WARNING: type inference failed for: r67v4, types: [com.google.android.gms.internal.ads.zzns] */
    /* JADX WARNING: type inference failed for: r37v8 */
    /* JADX WARNING: type inference failed for: r37v9 */
    /* JADX WARNING: type inference failed for: r2v105 */
    /* JADX WARNING: type inference failed for: r2v106 */
    /* JADX WARNING: type inference failed for: r2v107 */
    /* access modifiers changed from: private */
    /* JADX WARNING: Multi-variable type inference failed. Error: jadx.core.utils.exceptions.JadxRuntimeException: No candidate types for var: r7v4
  assigns: []
  uses: []
  mth insns count: 859
    	at jadx.core.dex.visitors.typeinference.TypeSearch.fillTypeCandidates(TypeSearch.java:237)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.typeinference.TypeSearch.run(TypeSearch.java:53)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.runMultiVariableSearch(TypeInferenceVisitor.java:99)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.visit(TypeInferenceVisitor.java:92)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:27)
    	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$1(DepthTraversal.java:14)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
    	at jadx.core.ProcessClass.process(ProcessClass.java:30)
    	at jadx.core.ProcessClass.lambda$processDependencies$0(ProcessClass.java:49)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:49)
    	at jadx.core.ProcessClass.process(ProcessClass.java:35)
    	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
    	at jadx.api.JavaClass.decompile(JavaClass.java:62)
    	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
     */
    /* JADX WARNING: Removed duplicated region for block: B:174:0x0464 A[Catch:{ XmlPullParserException -> 0x0809 }] */
    /* JADX WARNING: Removed duplicated region for block: B:227:0x0587 A[Catch:{ XmlPullParserException -> 0x0809 }] */
    /* JADX WARNING: Removed duplicated region for block: B:228:0x058a A[Catch:{ XmlPullParserException -> 0x0809 }] */
    /* JADX WARNING: Removed duplicated region for block: B:231:0x05a8 A[Catch:{ XmlPullParserException -> 0x0809 }] */
    /* JADX WARNING: Removed duplicated region for block: B:277:0x06cc A[Catch:{ XmlPullParserException -> 0x0809 }, LOOP:2: B:56:0x01e4->B:277:0x06cc, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:303:0x0796 A[Catch:{ XmlPullParserException -> 0x0809 }, LOOP:1: B:47:0x0122->B:303:0x0796, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:320:0x07d7 A[Catch:{ XmlPullParserException -> 0x0809 }] */
    /* JADX WARNING: Removed duplicated region for block: B:322:0x07e7 A[Catch:{ XmlPullParserException -> 0x0809 }] */
    /* JADX WARNING: Removed duplicated region for block: B:324:0x07ef A[Catch:{ XmlPullParserException -> 0x0809 }, LOOP:0: B:30:0x0092->B:324:0x07ef, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:332:0x07b5 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:333:0x072a A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:334:0x063c A[EDGE_INSN: B:334:0x063c->B:261:0x063c ?: BREAK  , SYNTHETIC] */
    /* JADX WARNING: Unknown variable types count: 17 */
    /* renamed from: zzc */
    public final zznj zzb(Uri uri, InputStream inputStream) throws IOException {
        long j;
        long j2;
        long j3;
        zzob zzob;
        boolean z;
        ArrayList arrayList;
        String str;
        long j4;
        String str2;
        long j5;
        ? r7;
        String str3;
        long j6;
        ArrayList arrayList2;
        long j7;
        ? r72;
        ArrayList arrayList3;
        long j8;
        long j9;
        zzob zzob2;
        String str4;
        ? r4;
        ? r37;
        ArrayList arrayList4;
        int i;
        int i2;
        float f;
        String str5;
        int i3;
        String str6;
        boolean z2;
        ArrayList arrayList5;
        ArrayList arrayList6;
        ? r11;
        String str7;
        ? r2;
        String str8;
        zzfs zzfs;
        zznt zznt;
        String str9;
        int i4;
        zzfs zzfs2;
        int i5;
        int parseInt;
        zzfs zza;
        String zzae;
        zznt zza2;
        try {
            XmlPullParser newPullParser = this.zzbdd.newPullParser();
            String str10 = null;
            newPullParser.setInput(inputStream, null);
            if (newPullParser.next() != 2 || !"MPD".equals(newPullParser.getName())) {
                throw new zzfx("inputStream does not contain a valid media presentation description");
            }
            String uri2 = uri.toString();
            String attributeValue = newPullParser.getAttributeValue(null, "availabilityStartTime");
            long j10 = C.TIME_UNSET;
            if (attributeValue == null) {
                j = -9223372036854775807L;
            } else {
                j = zzqe.zzal(attributeValue);
            }
            long zza3 = zza(newPullParser, "mediaPresentationDuration", (long) C.TIME_UNSET);
            long zza4 = zza(newPullParser, "minBufferTime", (long) C.TIME_UNSET);
            String attributeValue2 = newPullParser.getAttributeValue(null, "type");
            boolean z3 = attributeValue2 != null && attributeValue2.equals("dynamic");
            long zza5 = z3 ? zza(newPullParser, "minimumUpdatePeriod", (long) C.TIME_UNSET) : -9223372036854775807L;
            long zza6 = z3 ? zza(newPullParser, "timeShiftBufferDepth", (long) C.TIME_UNSET) : -9223372036854775807L;
            long zza7 = z3 ? zza(newPullParser, "suggestedPresentationDelay", (long) C.TIME_UNSET) : -9223372036854775807L;
            ArrayList arrayList7 = new ArrayList();
            String str11 = uri2;
            long j11 = j;
            long j12 = z3 ? -9223372036854775807L : 0;
            boolean z4 = false;
            boolean z5 = false;
            zzob zzob3 = null;
            Uri uri3 = null;
            while (true) {
                newPullParser.next();
                if (zzqg.zzd(newPullParser, "BaseURL")) {
                    if (!z4) {
                        str11 = zzb(newPullParser, str11);
                        j2 = zza3;
                        zzob = zzob3;
                        j5 = zza4;
                        z = true;
                    }
                    z = z4;
                    j2 = zza3;
                    zzob = zzob3;
                    str2 = str11;
                    j3 = zza4;
                    str = str10;
                    arrayList = arrayList7;
                    j12 = j12;
                    str11 = str2;
                    if (!zzqg.zzc(newPullParser, "MPD")) {
                        if (j2 == C.TIME_UNSET) {
                            if (j12 != C.TIME_UNSET) {
                                j4 = j12;
                                if (arrayList.isEmpty()) {
                                    zznj zznj = new zznj(j11, j4, j3, z3, zza5, zza6, zza7, zzob, uri3, arrayList);
                                    return zznj;
                                }
                                throw new zzfx("No periods found.");
                            } else if (!z3) {
                                throw new zzfx("Unable to determine duration of static manifest.");
                            }
                        }
                        j4 = j2;
                        if (arrayList.isEmpty()) {
                        }
                    } else {
                        str10 = str;
                        arrayList7 = arrayList;
                        z4 = z;
                        zzob3 = zzob;
                        zza4 = j3;
                        zza3 = j2;
                        j10 = C.TIME_UNSET;
                    }
                } else {
                    if (zzqg.zzd(newPullParser, "UTCTiming")) {
                        z = z4;
                        j2 = zza3;
                        zzob = new zzob(newPullParser.getAttributeValue(str10, "schemeIdUri"), newPullParser.getAttributeValue(str10, "value"));
                    } else if (zzqg.zzd(newPullParser, "Location")) {
                        z = z4;
                        uri3 = Uri.parse(newPullParser.nextText());
                        j2 = zza3;
                        zzob = zzob3;
                    } else {
                        if (zzqg.zzd(newPullParser, "Period") && !z5) {
                            String attributeValue3 = newPullParser.getAttributeValue(str10, "id");
                            z = z4;
                            long zza8 = zza(newPullParser, "start", j12);
                            long zza9 = zza(newPullParser, IronSourceConstants.EVENTS_DURATION, j10);
                            ArrayList arrayList8 = new ArrayList();
                            String str12 = str11;
                            boolean z6 = false;
                            ? r73 = 0;
                            while (true) {
                                newPullParser.next();
                                ? r33 = r73;
                                if (zzqg.zzd(newPullParser, "BaseURL")) {
                                    if (!z6) {
                                        str12 = zzb(newPullParser, str12);
                                        j6 = zza8;
                                        str3 = attributeValue3;
                                        arrayList2 = arrayList7;
                                        j9 = zza3;
                                        j7 = j12;
                                        zzob2 = zzob3;
                                        arrayList3 = arrayList8;
                                        str2 = str11;
                                        j3 = zza4;
                                        r72 = r33;
                                        str = null;
                                        z6 = true;
                                        if (!zzqg.zzc(newPullParser, "Period")) {
                                            Pair create = Pair.create(new zznn(str3, j6, arrayList3), Long.valueOf(zza9));
                                            zznn zznn = (zznn) create.first;
                                            if (zznn.zzbdj != C.TIME_UNSET) {
                                                long longValue = ((Long) create.second).longValue();
                                                if (longValue == C.TIME_UNSET) {
                                                    arrayList = arrayList2;
                                                    j8 = C.TIME_UNSET;
                                                } else {
                                                    j8 = longValue + zznn.zzbdj;
                                                    arrayList = arrayList2;
                                                }
                                                arrayList.add(zznn);
                                                j12 = j8;
                                            } else if (z3) {
                                                j12 = j7;
                                                str11 = str2;
                                                arrayList = arrayList2;
                                                z5 = true;
                                            } else {
                                                int size = arrayList2.size();
                                                StringBuilder sb = new StringBuilder(47);
                                                sb.append("Unable to determine start of period ");
                                                sb.append(size);
                                                throw new zzfx(sb.toString());
                                            }
                                        } else {
                                            arrayList8 = arrayList3;
                                            j12 = j7;
                                            zzob3 = zzob;
                                            str11 = str2;
                                            zza4 = j3;
                                            zza3 = j2;
                                            arrayList7 = arrayList2;
                                            zza8 = j6;
                                            attributeValue3 = str3;
                                            r73 = r72;
                                        }
                                    } else {
                                        j6 = zza8;
                                        str3 = attributeValue3;
                                        arrayList2 = arrayList7;
                                        str4 = str12;
                                        j9 = zza3;
                                        j7 = j12;
                                        zzob2 = zzob3;
                                        arrayList3 = arrayList8;
                                        str2 = str11;
                                        j3 = zza4;
                                    }
                                } else if (zzqg.zzd(newPullParser, "AdaptationSet")) {
                                    str4 = str12;
                                    int zza10 = zza(newPullParser, "id", -1);
                                    int zza11 = zza(newPullParser);
                                    String attributeValue4 = newPullParser.getAttributeValue(null, "mimeType");
                                    j7 = j12;
                                    String attributeValue5 = newPullParser.getAttributeValue(null, "codecs");
                                    int zza12 = zza(newPullParser, "width", -1);
                                    zzob2 = zzob3;
                                    int zza13 = zza(newPullParser, "height", -1);
                                    float zza14 = zza(newPullParser, -1.0f);
                                    str2 = str11;
                                    j3 = zza4;
                                    int zza15 = zza(newPullParser, "audioSamplingRate", -1);
                                    j9 = zza3;
                                    String attributeValue6 = newPullParser.getAttributeValue(null, "lang");
                                    ArrayList arrayList9 = new ArrayList();
                                    ArrayList arrayList10 = new ArrayList();
                                    String str13 = attributeValue6;
                                    ArrayList arrayList11 = new ArrayList();
                                    arrayList2 = arrayList7;
                                    ArrayList arrayList12 = new ArrayList();
                                    j6 = zza8;
                                    ArrayList arrayList13 = new ArrayList();
                                    str3 = attributeValue3;
                                    ArrayList arrayList14 = arrayList8;
                                    ArrayList arrayList15 = arrayList10;
                                    ? r372 = r33;
                                    String str14 = str4;
                                    int i6 = zza11;
                                    String str15 = str13;
                                    boolean z7 = false;
                                    int i7 = 0;
                                    int i8 = -1;
                                    while (true) {
                                        newPullParser.next();
                                        ArrayList arrayList16 = arrayList13;
                                        if (zzqg.zzd(newPullParser, "BaseURL")) {
                                            if (!z7) {
                                                str14 = zzb(newPullParser, str14);
                                                arrayList4 = arrayList12;
                                                str6 = attributeValue4;
                                                i3 = zza12;
                                                str5 = attributeValue5;
                                                f = zza14;
                                                i2 = zza13;
                                                i = zza15;
                                                arrayList6 = arrayList15;
                                                arrayList5 = arrayList16;
                                                z2 = true;
                                                if (!zzqg.zzc(newPullParser, "AdaptationSet")) {
                                                    break;
                                                }
                                                arrayList15 = arrayList6;
                                                arrayList13 = arrayList5;
                                                z7 = z2;
                                                attributeValue4 = str6;
                                                zza12 = i3;
                                                attributeValue5 = str5;
                                                zza14 = f;
                                                zza13 = i2;
                                                zza15 = i;
                                                arrayList12 = arrayList4;
                                                r372 = r37;
                                            }
                                        } else if (zzqg.zzd(newPullParser, "ContentProtection")) {
                                            zza zzb = zzb(newPullParser);
                                            if (zzb != null) {
                                                arrayList9.add(zzb);
                                            }
                                        } else if (zzqg.zzd(newPullParser, "ContentComponent")) {
                                            z2 = z7;
                                            String attributeValue7 = newPullParser.getAttributeValue(null, "lang");
                                            if (str15 == null) {
                                                str15 = attributeValue7;
                                            } else if (attributeValue7 != null) {
                                                zzpo.checkState(str15.equals(attributeValue7));
                                            }
                                            i6 = zzd(i6, zza(newPullParser));
                                            arrayList4 = arrayList12;
                                            str6 = attributeValue4;
                                            i3 = zza12;
                                            str5 = attributeValue5;
                                            f = zza14;
                                            i2 = zza13;
                                            i = zza15;
                                            arrayList6 = arrayList15;
                                            arrayList5 = arrayList16;
                                            if (!zzqg.zzc(newPullParser, "AdaptationSet")) {
                                            }
                                        } else {
                                            z2 = z7;
                                            if (zzqg.zzd(newPullParser, "Role")) {
                                                String zzb2 = zzb(newPullParser, "schemeIdUri", (String) null);
                                                str7 = str14;
                                                String zzb3 = zzb(newPullParser, "value", (String) null);
                                                do {
                                                    newPullParser.next();
                                                } while (!zzqg.zzc(newPullParser, "Role"));
                                                i7 |= (!"urn:mpeg:dash:role:2011".equals(zzb2) || !ParametersKeys.MAIN.equals(zzb3)) ? 0 : 1;
                                            } else {
                                                str7 = str14;
                                                if (zzqg.zzd(newPullParser, "AudioChannelConfiguration")) {
                                                    i8 = zze(newPullParser);
                                                } else if (zzqg.zzd(newPullParser, "Accessibility")) {
                                                    arrayList11.add(zza(newPullParser, "Accessibility"));
                                                } else if (zzqg.zzd(newPullParser, "SupplementalProperty")) {
                                                    arrayList12.add(zza(newPullParser, "SupplementalProperty"));
                                                } else {
                                                    if (zzqg.zzd(newPullParser, "Representation")) {
                                                        String attributeValue8 = newPullParser.getAttributeValue(null, "id");
                                                        int zza16 = zza(newPullParser, "bandwidth", -1);
                                                        String zzb4 = zzb(newPullParser, "mimeType", attributeValue4);
                                                        String zzb5 = zzb(newPullParser, "codecs", attributeValue5);
                                                        int zza17 = zza(newPullParser, "width", zza12);
                                                        int zza18 = zza(newPullParser, "height", zza13);
                                                        float zza19 = zza(newPullParser, zza14);
                                                        int zza20 = zza(newPullParser, "audioSamplingRate", zza15);
                                                        str6 = attributeValue4;
                                                        ArrayList arrayList17 = new ArrayList();
                                                        i3 = zza12;
                                                        ArrayList arrayList18 = new ArrayList();
                                                        str5 = attributeValue5;
                                                        int i9 = i8;
                                                        zznt zznt2 = r37;
                                                        String str16 = str7;
                                                        boolean z8 = false;
                                                        while (true) {
                                                            newPullParser.next();
                                                            f = zza14;
                                                            if (zzqg.zzd(newPullParser, "BaseURL")) {
                                                                if (!z8) {
                                                                    str16 = zzb(newPullParser, str16);
                                                                    z8 = true;
                                                                }
                                                            } else if (zzqg.zzd(newPullParser, "AudioChannelConfiguration")) {
                                                                i9 = zze(newPullParser);
                                                            } else {
                                                                if (zzqg.zzd(newPullParser, "SegmentBase")) {
                                                                    zza2 = zza(newPullParser, (zzny) zznt2);
                                                                } else if (zzqg.zzd(newPullParser, "SegmentList")) {
                                                                    zza2 = zza(newPullParser, (zznv) zznt2);
                                                                } else if (zzqg.zzd(newPullParser, "SegmentTemplate")) {
                                                                    zza2 = zza(newPullParser, (zznw) zznt2);
                                                                } else if (zzqg.zzd(newPullParser, "ContentProtection")) {
                                                                    zza zzb6 = zzb(newPullParser);
                                                                    if (zzb6 != null) {
                                                                        arrayList17.add(zzb6);
                                                                    }
                                                                } else if (zzqg.zzd(newPullParser, "InbandEventStream")) {
                                                                    arrayList18.add(zza(newPullParser, "InbandEventStream"));
                                                                }
                                                                zznt2 = zza2;
                                                            }
                                                            if (zzqg.zzc(newPullParser, "Representation")) {
                                                                break;
                                                            }
                                                            ArrayList arrayList19 = arrayList12;
                                                            zza14 = f;
                                                        }
                                                        if (zzpt.zzab(zzb4)) {
                                                            if (zzb5 != null) {
                                                                String[] split = zzb5.split(",");
                                                                i2 = zza13;
                                                                int length = split.length;
                                                                i = zza15;
                                                                int i10 = 0;
                                                                while (true) {
                                                                    if (i10 >= length) {
                                                                        break;
                                                                    }
                                                                    zzae = zzpt.zzae(split[i10]);
                                                                    if (zzae != null && zzpt.zzab(zzae)) {
                                                                        break;
                                                                    }
                                                                    i10++;
                                                                }
                                                            } else {
                                                                i2 = zza13;
                                                                i = zza15;
                                                            }
                                                            str8 = null;
                                                            if (str8 != null) {
                                                                if (zzpt.zzac(str8)) {
                                                                    zza = zzfs.zza(attributeValue8, zzb4, str8, zzb5, zza16, zza17, zza18, zza19, null, i7);
                                                                } else if (zzpt.zzab(str8)) {
                                                                    zza = zzfs.zza(attributeValue8, zzb4, str8, zzb5, zza16, i9, zza20, null, i7, str15);
                                                                } else if (zzx(str8)) {
                                                                    if (MimeTypes.APPLICATION_CEA608.equals(str8)) {
                                                                        int i11 = 0;
                                                                        while (i11 < arrayList11.size()) {
                                                                            zznm zznm = (zznm) arrayList11.get(i11);
                                                                            arrayList4 = arrayList12;
                                                                            if ("urn:scte:dash:cc:cea-608:2015".equals(zznm.zzbdi) && zznm.value != null) {
                                                                                Matcher matcher = zzbda.matcher(zznm.value);
                                                                                if (matcher.matches()) {
                                                                                    parseInt = Integer.parseInt(matcher.group(1));
                                                                                    break;
                                                                                }
                                                                                String str17 = "MpdParser";
                                                                                String str18 = "Unable to parse CEA-608 channel number from: ";
                                                                                String valueOf = String.valueOf(zznm.value);
                                                                                Log.w(str17, valueOf.length() != 0 ? str18.concat(valueOf) : new String(str18));
                                                                            }
                                                                            i11++;
                                                                            arrayList12 = arrayList4;
                                                                        }
                                                                        arrayList4 = arrayList12;
                                                                    } else {
                                                                        arrayList4 = arrayList12;
                                                                        if (MimeTypes.APPLICATION_CEA708.equals(str8)) {
                                                                            int i12 = 0;
                                                                            while (true) {
                                                                                if (i12 >= arrayList11.size()) {
                                                                                    break;
                                                                                }
                                                                                zznm zznm2 = (zznm) arrayList11.get(i12);
                                                                                if ("urn:scte:dash:cc:cea-708:2015".equals(zznm2.zzbdi) && zznm2.value != null) {
                                                                                    Matcher matcher2 = zzbdb.matcher(zznm2.value);
                                                                                    if (matcher2.matches()) {
                                                                                        parseInt = Integer.parseInt(matcher2.group(1));
                                                                                        break;
                                                                                    }
                                                                                    String str19 = "MpdParser";
                                                                                    String str20 = "Unable to parse CEA-708 service block number from: ";
                                                                                    String valueOf2 = String.valueOf(zznm2.value);
                                                                                    Log.w(str19, valueOf2.length() != 0 ? str20.concat(valueOf2) : new String(str20));
                                                                                }
                                                                                i12++;
                                                                            }
                                                                            i5 = parseInt;
                                                                            zzfs2 = zzfs.zza(attributeValue8, zzb4, str8, zzb5, zza16, i7, str15, i5);
                                                                            zzfs = zzfs2;
                                                                            if (zznt2 == null) {
                                                                                zznt = zznt2;
                                                                            } else {
                                                                                zznt = new zzny();
                                                                            }
                                                                            zznl zznl = new zznl(zzfs, str16, zznt, arrayList17, arrayList18);
                                                                            str9 = zznl.zzaad.zzzj;
                                                                            if (!TextUtils.isEmpty(str9)) {
                                                                                if (zzpt.zzac(str9)) {
                                                                                    i4 = 2;
                                                                                } else if (zzpt.zzab(str9)) {
                                                                                    i4 = 1;
                                                                                } else if (zzx(str9)) {
                                                                                    i4 = 3;
                                                                                }
                                                                                int zzd = zzd(i6, i4);
                                                                                arrayList5 = arrayList16;
                                                                                arrayList5.add(zznl);
                                                                                i6 = zzd;
                                                                            }
                                                                            i4 = -1;
                                                                            int zzd2 = zzd(i6, i4);
                                                                            arrayList5 = arrayList16;
                                                                            arrayList5.add(zznl);
                                                                            i6 = zzd2;
                                                                        } else {
                                                                            i5 = -1;
                                                                            zzfs2 = zzfs.zza(attributeValue8, zzb4, str8, zzb5, zza16, i7, str15, i5);
                                                                            zzfs = zzfs2;
                                                                            if (zznt2 == null) {
                                                                            }
                                                                            zznl zznl2 = new zznl(zzfs, str16, zznt, arrayList17, arrayList18);
                                                                            str9 = zznl2.zzaad.zzzj;
                                                                            if (!TextUtils.isEmpty(str9)) {
                                                                            }
                                                                            i4 = -1;
                                                                            int zzd22 = zzd(i6, i4);
                                                                            arrayList5 = arrayList16;
                                                                            arrayList5.add(zznl2);
                                                                            i6 = zzd22;
                                                                        }
                                                                    }
                                                                    parseInt = -1;
                                                                    i5 = parseInt;
                                                                    zzfs2 = zzfs.zza(attributeValue8, zzb4, str8, zzb5, zza16, i7, str15, i5);
                                                                    zzfs = zzfs2;
                                                                    if (zznt2 == null) {
                                                                    }
                                                                    zznl zznl22 = new zznl(zzfs, str16, zznt, arrayList17, arrayList18);
                                                                    str9 = zznl22.zzaad.zzzj;
                                                                    if (!TextUtils.isEmpty(str9)) {
                                                                    }
                                                                    i4 = -1;
                                                                    int zzd222 = zzd(i6, i4);
                                                                    arrayList5 = arrayList16;
                                                                    arrayList5.add(zznl22);
                                                                    i6 = zzd222;
                                                                }
                                                                zzfs = zza;
                                                                arrayList4 = arrayList12;
                                                                if (zznt2 == null) {
                                                                }
                                                                zznl zznl222 = new zznl(zzfs, str16, zznt, arrayList17, arrayList18);
                                                                str9 = zznl222.zzaad.zzzj;
                                                                if (!TextUtils.isEmpty(str9)) {
                                                                }
                                                                i4 = -1;
                                                                int zzd2222 = zzd(i6, i4);
                                                                arrayList5 = arrayList16;
                                                                arrayList5.add(zznl222);
                                                                i6 = zzd2222;
                                                            }
                                                            arrayList4 = arrayList12;
                                                            zzfs2 = zzfs.zza(attributeValue8, zzb4, str8, zzb5, zza16, i7, str15);
                                                            zzfs = zzfs2;
                                                            if (zznt2 == null) {
                                                            }
                                                            zznl zznl2222 = new zznl(zzfs, str16, zznt, arrayList17, arrayList18);
                                                            str9 = zznl2222.zzaad.zzzj;
                                                            if (!TextUtils.isEmpty(str9)) {
                                                            }
                                                            i4 = -1;
                                                            int zzd22222 = zzd(i6, i4);
                                                            arrayList5 = arrayList16;
                                                            arrayList5.add(zznl2222);
                                                            i6 = zzd22222;
                                                        } else {
                                                            i2 = zza13;
                                                            i = zza15;
                                                            if (!zzpt.zzac(zzb4)) {
                                                                if (zzx(zzb4)) {
                                                                    str8 = zzb4;
                                                                } else if (MimeTypes.APPLICATION_MP4.equals(zzb4)) {
                                                                    if ("stpp".equals(zzb5)) {
                                                                        str8 = MimeTypes.APPLICATION_TTML;
                                                                    } else if ("wvtt".equals(zzb5)) {
                                                                        str8 = MimeTypes.APPLICATION_MP4VTT;
                                                                    }
                                                                } else if (MimeTypes.APPLICATION_RAWCC.equals(zzb4) && zzb5 != null) {
                                                                    if (zzb5.contains("cea708")) {
                                                                        str8 = MimeTypes.APPLICATION_CEA708;
                                                                    } else if (zzb5.contains("eia608") || zzb5.contains("cea608")) {
                                                                        str8 = MimeTypes.APPLICATION_CEA608;
                                                                    }
                                                                }
                                                                if (str8 != null) {
                                                                }
                                                                arrayList4 = arrayList12;
                                                                zzfs2 = zzfs.zza(attributeValue8, zzb4, str8, zzb5, zza16, i7, str15);
                                                                zzfs = zzfs2;
                                                                if (zznt2 == null) {
                                                                }
                                                                zznl zznl22222 = new zznl(zzfs, str16, zznt, arrayList17, arrayList18);
                                                                str9 = zznl22222.zzaad.zzzj;
                                                                if (!TextUtils.isEmpty(str9)) {
                                                                }
                                                                i4 = -1;
                                                                int zzd222222 = zzd(i6, i4);
                                                                arrayList5 = arrayList16;
                                                                arrayList5.add(zznl22222);
                                                                i6 = zzd222222;
                                                            } else if (zzb5 != null) {
                                                                String[] split2 = zzb5.split(",");
                                                                int length2 = split2.length;
                                                                int i13 = 0;
                                                                while (i13 < length2) {
                                                                    zzae = zzpt.zzae(split2[i13]);
                                                                    if (zzae == null || !zzpt.zzac(zzae)) {
                                                                        i13++;
                                                                    }
                                                                }
                                                            }
                                                            str8 = null;
                                                            if (str8 != null) {
                                                            }
                                                            arrayList4 = arrayList12;
                                                            zzfs2 = zzfs.zza(attributeValue8, zzb4, str8, zzb5, zza16, i7, str15);
                                                            zzfs = zzfs2;
                                                            if (zznt2 == null) {
                                                            }
                                                            zznl zznl222222 = new zznl(zzfs, str16, zznt, arrayList17, arrayList18);
                                                            str9 = zznl222222.zzaad.zzzj;
                                                            if (!TextUtils.isEmpty(str9)) {
                                                            }
                                                            i4 = -1;
                                                            int zzd2222222 = zzd(i6, i4);
                                                            arrayList5 = arrayList16;
                                                            arrayList5.add(zznl222222);
                                                            i6 = zzd2222222;
                                                        }
                                                        str8 = zzae;
                                                        if (str8 != null) {
                                                        }
                                                        arrayList4 = arrayList12;
                                                        zzfs2 = zzfs.zza(attributeValue8, zzb4, str8, zzb5, zza16, i7, str15);
                                                        zzfs = zzfs2;
                                                        if (zznt2 == null) {
                                                        }
                                                        zznl zznl2222222 = new zznl(zzfs, str16, zznt, arrayList17, arrayList18);
                                                        str9 = zznl2222222.zzaad.zzzj;
                                                        if (!TextUtils.isEmpty(str9)) {
                                                        }
                                                        i4 = -1;
                                                        int zzd22222222 = zzd(i6, i4);
                                                        arrayList5 = arrayList16;
                                                        arrayList5.add(zznl2222222);
                                                        i6 = zzd22222222;
                                                    } else {
                                                        arrayList4 = arrayList12;
                                                        str6 = attributeValue4;
                                                        i3 = zza12;
                                                        str5 = attributeValue5;
                                                        f = zza14;
                                                        i2 = zza13;
                                                        i = zza15;
                                                        arrayList5 = arrayList16;
                                                        if (zzqg.zzd(newPullParser, "SegmentBase")) {
                                                            r2 = zza(newPullParser, (zzny) r37);
                                                        } else if (zzqg.zzd(newPullParser, "SegmentList")) {
                                                            r2 = zza(newPullParser, (zznv) r37);
                                                        } else if (zzqg.zzd(newPullParser, "SegmentTemplate")) {
                                                            r2 = zza(newPullParser, r37);
                                                        } else {
                                                            if (zzqg.zzd(newPullParser, "InbandEventStream")) {
                                                                arrayList6 = arrayList15;
                                                                arrayList6.add(zza(newPullParser, "InbandEventStream"));
                                                            } else {
                                                                arrayList6 = arrayList15;
                                                                zzqg.zzf(newPullParser);
                                                            }
                                                            str14 = str7;
                                                            r37 = r37;
                                                            if (!zzqg.zzc(newPullParser, "AdaptationSet")) {
                                                            }
                                                        }
                                                        r37 = r2;
                                                    }
                                                    arrayList6 = arrayList15;
                                                    r37 = r37;
                                                    str14 = str7;
                                                    r37 = r37;
                                                    if (!zzqg.zzc(newPullParser, "AdaptationSet")) {
                                                    }
                                                }
                                            }
                                            arrayList4 = arrayList12;
                                            str6 = attributeValue4;
                                            i3 = zza12;
                                            str5 = attributeValue5;
                                            f = zza14;
                                            i2 = zza13;
                                            i = zza15;
                                            arrayList6 = arrayList15;
                                            arrayList5 = arrayList16;
                                            str14 = str7;
                                            r37 = r37;
                                            if (!zzqg.zzc(newPullParser, "AdaptationSet")) {
                                            }
                                        }
                                        z2 = z7;
                                        str7 = str14;
                                        arrayList4 = arrayList12;
                                        str6 = attributeValue4;
                                        i3 = zza12;
                                        str5 = attributeValue5;
                                        f = zza14;
                                        i2 = zza13;
                                        i = zza15;
                                        arrayList6 = arrayList15;
                                        arrayList5 = arrayList16;
                                        str14 = str7;
                                        r37 = r37;
                                        if (!zzqg.zzc(newPullParser, "AdaptationSet")) {
                                        }
                                    }
                                    ArrayList arrayList20 = new ArrayList(arrayList5.size());
                                    for (int i14 = 0; i14 < arrayList5.size(); i14++) {
                                        zznl zznl3 = (zznl) arrayList5.get(i14);
                                        zzfs zzfs3 = zznl3.zzaad;
                                        ArrayList<zza> arrayList21 = zznl3.zzbdg;
                                        arrayList21.addAll(arrayList9);
                                        if (!arrayList21.isEmpty()) {
                                            zzfs3 = zzfs3.zza(new zzhp((List<zza>) arrayList21));
                                        }
                                        zzfs zzfs4 = zzfs3;
                                        ArrayList<zznm> arrayList22 = zznl3.zzbdh;
                                        arrayList22.addAll(arrayList6);
                                        String str21 = zznl3.zzbde;
                                        zznt zznt3 = zznl3.zzbdf;
                                        if (zznt3 instanceof zzny) {
                                            ? zzns = new zzns(null, -1, zzfs4, str21, (zzny) zznt3, arrayList22, null, -1);
                                            r11 = zzns;
                                        } else if (zznt3 instanceof zznu) {
                                            ? zznr = new zznr(null, -1, zzfs4, str21, (zznu) zznt3, arrayList22);
                                            r11 = zznr;
                                        } else {
                                            throw new IllegalArgumentException("segmentBase must be of type SingleSegmentBase or MultiSegmentBase");
                                        }
                                        arrayList20.add(r11);
                                    }
                                    zzni zzni = new zzni(zza10, i6, arrayList20, arrayList11, arrayList4);
                                    arrayList3 = arrayList14;
                                    arrayList3.add(zzni);
                                } else {
                                    j6 = zza8;
                                    str3 = attributeValue3;
                                    arrayList2 = arrayList7;
                                    str4 = str12;
                                    j9 = zza3;
                                    j7 = j12;
                                    zzob2 = zzob3;
                                    arrayList3 = arrayList8;
                                    str2 = str11;
                                    j3 = zza4;
                                    if (zzqg.zzd(newPullParser, "SegmentBase")) {
                                        str = null;
                                        r4 = zza(newPullParser, (zzny) null);
                                    } else {
                                        str = null;
                                        if (zzqg.zzd(newPullParser, "SegmentList")) {
                                            r4 = zza(newPullParser, (zznv) null);
                                        } else {
                                            if (zzqg.zzd(newPullParser, "SegmentTemplate")) {
                                                r4 = zza(newPullParser, (zznw) null);
                                            }
                                            r7 = r33;
                                            str12 = str4;
                                            r72 = r7;
                                            if (!zzqg.zzc(newPullParser, "Period")) {
                                            }
                                        }
                                    }
                                    r7 = r4;
                                    str12 = str4;
                                    r72 = r7;
                                    if (!zzqg.zzc(newPullParser, "Period")) {
                                    }
                                }
                                str = null;
                                r7 = r33;
                                str12 = str4;
                                r72 = r7;
                                if (!zzqg.zzc(newPullParser, "Period")) {
                                }
                            }
                            if (!zzqg.zzc(newPullParser, "MPD")) {
                            }
                        }
                        z = z4;
                        j2 = zza3;
                        zzob = zzob3;
                        str2 = str11;
                        j3 = zza4;
                        str = str10;
                        arrayList = arrayList7;
                        j12 = j12;
                        str11 = str2;
                        if (!zzqg.zzc(newPullParser, "MPD")) {
                        }
                    }
                    j5 = zza4;
                }
                str = str10;
                arrayList = arrayList7;
                if (!zzqg.zzc(newPullParser, "MPD")) {
                }
            }
        } catch (XmlPullParserException e) {
            throw new zzfx((Throwable) e);
        }
    }

    private static int zza(XmlPullParser xmlPullParser) {
        String attributeValue = xmlPullParser.getAttributeValue(null, "contentType");
        if (!TextUtils.isEmpty(attributeValue)) {
            if (MimeTypes.BASE_TYPE_AUDIO.equals(attributeValue)) {
                return 1;
            }
            if ("video".equals(attributeValue)) {
                return 2;
            }
            if (MimeTypes.BASE_TYPE_TEXT.equals(attributeValue)) {
                return 3;
            }
        }
        return -1;
    }

    private static zza zzb(XmlPullParser xmlPullParser) throws XmlPullParserException, IOException {
        boolean equals = "urn:uuid:9a04f079-9840-4286-ab92-e65be0885f95".equals(xmlPullParser.getAttributeValue(null, "schemeIdUri"));
        byte[] bArr = null;
        UUID uuid = null;
        boolean z = false;
        do {
            xmlPullParser.next();
            if (bArr == null && zzqg.zzd(xmlPullParser, "cenc:pssh") && xmlPullParser.next() == 4) {
                bArr = Base64.decode(xmlPullParser.getText(), 0);
                uuid = zzjq.zze(bArr);
                if (uuid == null) {
                    Log.w("MpdParser", "Skipping malformed cenc:pssh data");
                    bArr = null;
                }
            } else if (bArr == null && equals && zzqg.zzd(xmlPullParser, "mspr:pro") && xmlPullParser.next() == 4) {
                bArr = zzjq.zza(zzfe.zzwp, Base64.decode(xmlPullParser.getText(), 0));
                uuid = zzfe.zzwp;
            } else if (zzqg.zzd(xmlPullParser, "widevine:license")) {
                String attributeValue = xmlPullParser.getAttributeValue(null, "robustness_level");
                z = attributeValue != null && attributeValue.startsWith("HW");
            }
        } while (!zzqg.zzc(xmlPullParser, "ContentProtection"));
        if (bArr != null) {
            return new zza(uuid, MimeTypes.VIDEO_MP4, bArr, z);
        }
        return null;
    }

    private final zzny zza(XmlPullParser xmlPullParser, zzny zzny) throws XmlPullParserException, IOException {
        long j;
        long j2;
        XmlPullParser xmlPullParser2 = xmlPullParser;
        zzny zzny2 = zzny;
        long zzb = zzb(xmlPullParser2, "timescale", zzny2 != null ? zzny2.zzcr : 1);
        long j3 = 0;
        long zzb2 = zzb(xmlPullParser2, "presentationTimeOffset", zzny2 != null ? zzny2.zzbdw : 0);
        long j4 = zzny2 != null ? zzny2.zzbec : 0;
        if (zzny2 != null) {
            j3 = zzny2.zzbed;
        }
        zzno zzno = null;
        String attributeValue = xmlPullParser2.getAttributeValue(null, "indexRange");
        if (attributeValue != null) {
            String[] split = attributeValue.split("-");
            j2 = Long.parseLong(split[0]);
            j = (Long.parseLong(split[1]) - j2) + 1;
        } else {
            j = j3;
            j2 = j4;
        }
        if (zzny2 != null) {
            zzno = zzny2.zzbdv;
        }
        do {
            xmlPullParser.next();
            if (zzqg.zzd(xmlPullParser2, "Initialization")) {
                zzno = zzd(xmlPullParser);
            }
        } while (!zzqg.zzc(xmlPullParser2, "SegmentBase"));
        zzny zzny3 = new zzny(zzno, zzb, zzb2, j2, j);
        return zzny3;
    }

    private final zznv zza(XmlPullParser xmlPullParser, zznv zznv) throws XmlPullParserException, IOException {
        XmlPullParser xmlPullParser2 = xmlPullParser;
        zznv zznv2 = zznv;
        long zzb = zzb(xmlPullParser2, "timescale", zznv2 != null ? zznv2.zzcr : 1);
        long zzb2 = zzb(xmlPullParser2, "presentationTimeOffset", zznv2 != null ? zznv2.zzbdw : 0);
        long zzb3 = zzb(xmlPullParser2, IronSourceConstants.EVENTS_DURATION, zznv2 != null ? zznv2.zzcs : C.TIME_UNSET);
        int zza = zza(xmlPullParser2, "startNumber", zznv2 != null ? zznv2.zzbdx : 1);
        List list = null;
        zzno zzno = null;
        List list2 = null;
        do {
            xmlPullParser.next();
            if (zzqg.zzd(xmlPullParser2, "Initialization")) {
                zzno = zzd(xmlPullParser);
            } else if (zzqg.zzd(xmlPullParser2, "SegmentTimeline")) {
                list2 = zzc(xmlPullParser);
            } else if (zzqg.zzd(xmlPullParser2, "SegmentURL")) {
                if (list == null) {
                    list = new ArrayList();
                }
                list.add(zza(xmlPullParser2, "media", "mediaRange"));
            }
        } while (!zzqg.zzc(xmlPullParser2, "SegmentList"));
        if (zznv2 != null) {
            if (zzno == null) {
                zzno = zznv2.zzbdv;
            }
            if (list2 == null) {
                list2 = zznv2.zzbdy;
            }
            if (list == null) {
                list = zznv2.zzbdz;
            }
        }
        zznv zznv3 = new zznv(zzno, zzb, zzb2, zza, zzb3, list2, list);
        return zznv3;
    }

    private final zznw zza(XmlPullParser xmlPullParser, zznw zznw) throws XmlPullParserException, IOException {
        XmlPullParser xmlPullParser2 = xmlPullParser;
        zznw zznw2 = zznw;
        long zzb = zzb(xmlPullParser2, "timescale", zznw2 != null ? zznw2.zzcr : 1);
        long zzb2 = zzb(xmlPullParser2, "presentationTimeOffset", zznw2 != null ? zznw2.zzbdw : 0);
        long zzb3 = zzb(xmlPullParser2, IronSourceConstants.EVENTS_DURATION, zznw2 != null ? zznw2.zzcs : C.TIME_UNSET);
        int zza = zza(xmlPullParser2, "startNumber", zznw2 != null ? zznw2.zzbdx : 1);
        zzno zzno = null;
        zzoa zza2 = zza(xmlPullParser2, "media", zznw2 != null ? zznw2.zzbeb : null);
        zzoa zza3 = zza(xmlPullParser2, "initialization", zznw2 != null ? zznw2.zzbea : null);
        List list = null;
        do {
            xmlPullParser.next();
            if (zzqg.zzd(xmlPullParser2, "Initialization")) {
                zzno = zzd(xmlPullParser);
            } else if (zzqg.zzd(xmlPullParser2, "SegmentTimeline")) {
                list = zzc(xmlPullParser);
            }
        } while (!zzqg.zzc(xmlPullParser2, "SegmentTemplate"));
        if (zznw2 != null) {
            if (zzno == null) {
                zzno = zznw2.zzbdv;
            }
            if (list == null) {
                list = zznw2.zzbdy;
            }
        }
        zznw zznw3 = new zznw(zzno, zzb, zzb2, zza, zzb3, list, zza3, zza2);
        return zznw3;
    }

    private static List<zznx> zzc(XmlPullParser xmlPullParser) throws XmlPullParserException, IOException {
        ArrayList arrayList = new ArrayList();
        long j = 0;
        do {
            xmlPullParser.next();
            if (zzqg.zzd(xmlPullParser, "S")) {
                j = zzb(xmlPullParser, "t", j);
                long zzb = zzb(xmlPullParser, d.b, (long) C.TIME_UNSET);
                int zza = zza(xmlPullParser, CampaignEx.JSON_KEY_AD_R, 0) + 1;
                for (int i = 0; i < zza; i++) {
                    arrayList.add(new zznx(j, zzb));
                    j += zzb;
                }
            }
        } while (!zzqg.zzc(xmlPullParser, "SegmentTimeline"));
        return arrayList;
    }

    private static zzoa zza(XmlPullParser xmlPullParser, String str, zzoa zzoa) {
        String attributeValue = xmlPullParser.getAttributeValue(null, str);
        return attributeValue != null ? zzoa.zzaa(attributeValue) : zzoa;
    }

    private final zzno zzd(XmlPullParser xmlPullParser) {
        return zza(xmlPullParser, "sourceURL", "range");
    }

    private static zzno zza(XmlPullParser xmlPullParser, String str, String str2) {
        long j;
        long j2;
        String attributeValue = xmlPullParser.getAttributeValue(null, str);
        String attributeValue2 = xmlPullParser.getAttributeValue(null, str2);
        if (attributeValue2 != null) {
            String[] split = attributeValue2.split("-");
            j2 = Long.parseLong(split[0]);
            if (split.length == 2) {
                j = (Long.parseLong(split[1]) - j2) + 1;
                zzno zzno = new zzno(attributeValue, j2, j);
                return zzno;
            }
        } else {
            j2 = 0;
        }
        j = -1;
        zzno zzno2 = new zzno(attributeValue, j2, j);
        return zzno2;
    }

    private static int zze(XmlPullParser xmlPullParser) throws XmlPullParserException, IOException {
        int i = -1;
        if ("urn:mpeg:dash:23003:3:audio_channel_configuration:2011".equals(zzb(xmlPullParser, "schemeIdUri", (String) null))) {
            i = zza(xmlPullParser, "value", -1);
        }
        do {
            xmlPullParser.next();
        } while (!zzqg.zzc(xmlPullParser, "AudioChannelConfiguration"));
        return i;
    }

    private static boolean zzx(String str) {
        return zzpt.zzad(str) || MimeTypes.APPLICATION_TTML.equals(str) || MimeTypes.APPLICATION_MP4VTT.equals(str) || MimeTypes.APPLICATION_CEA708.equals(str) || MimeTypes.APPLICATION_CEA608.equals(str);
    }

    private static int zzd(int i, int i2) {
        if (i == -1) {
            return i2;
        }
        if (i2 == -1) {
            return i;
        }
        zzpo.checkState(i == i2);
        return i;
    }

    private static zznm zza(XmlPullParser xmlPullParser, String str) throws XmlPullParserException, IOException {
        String zzb = zzb(xmlPullParser, "schemeIdUri", "");
        String zzb2 = zzb(xmlPullParser, "value", (String) null);
        String zzb3 = zzb(xmlPullParser, "id", (String) null);
        do {
            xmlPullParser.next();
        } while (!zzqg.zzc(xmlPullParser, str));
        return new zznm(zzb, zzb2, zzb3);
    }

    private static float zza(XmlPullParser xmlPullParser, float f) {
        String attributeValue = xmlPullParser.getAttributeValue(null, "frameRate");
        if (attributeValue == null) {
            return f;
        }
        Matcher matcher = zzbcz.matcher(attributeValue);
        if (!matcher.matches()) {
            return f;
        }
        int parseInt = Integer.parseInt(matcher.group(1));
        String group = matcher.group(2);
        return !TextUtils.isEmpty(group) ? ((float) parseInt) / ((float) Integer.parseInt(group)) : (float) parseInt;
    }

    private static long zza(XmlPullParser xmlPullParser, String str, long j) {
        String attributeValue = xmlPullParser.getAttributeValue(null, str);
        if (attributeValue == null) {
            return j;
        }
        return zzqe.zzak(attributeValue);
    }

    private static String zzb(XmlPullParser xmlPullParser, String str) throws XmlPullParserException, IOException {
        xmlPullParser.next();
        return zzqd.zzc(str, xmlPullParser.getText());
    }

    private static int zza(XmlPullParser xmlPullParser, String str, int i) {
        String attributeValue = xmlPullParser.getAttributeValue(null, str);
        if (attributeValue == null) {
            return i;
        }
        return Integer.parseInt(attributeValue);
    }

    private static long zzb(XmlPullParser xmlPullParser, String str, long j) {
        String attributeValue = xmlPullParser.getAttributeValue(null, str);
        if (attributeValue == null) {
            return j;
        }
        return Long.parseLong(attributeValue);
    }

    private static String zzb(XmlPullParser xmlPullParser, String str, String str2) {
        String attributeValue = xmlPullParser.getAttributeValue(null, str);
        return attributeValue == null ? str2 : attributeValue;
    }
}
