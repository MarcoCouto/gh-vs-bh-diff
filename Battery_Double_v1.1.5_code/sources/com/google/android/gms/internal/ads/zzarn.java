package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.webkit.CookieManager;
import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.gms.ads.internal.zzbv;
import com.google.android.gms.common.GooglePlayServicesUtilLight;
import com.google.android.gms.common.util.VisibleForTesting;
import com.google.android.gms.internal.ads.zzuo.zza.zzb;
import com.integralads.avid.library.inmobi.utils.AvidJSONUtil;
import javax.annotation.concurrent.GuardedBy;
import org.json.JSONException;
import org.json.JSONObject;

@zzark
public final class zzarn extends zzaxv implements zzasa {
    private final Context mContext;
    @VisibleForTesting
    private zzakr zzdmn;
    @VisibleForTesting
    private zzasi zzdnh;
    @VisibleForTesting
    private zzasm zzdsl;
    private Runnable zzdsm;
    private final Object zzdsn = new Object();
    private final zzarm zzdvp;
    private final zzasj zzdvq;
    private final zzum zzdvr;
    private final zzur zzdvs;
    @GuardedBy("mCancelLock")
    @VisibleForTesting
    private boolean zzdvt;
    @GuardedBy("mCancelLock")
    @VisibleForTesting
    private zzazb zzdvu;

    public zzarn(Context context, zzasj zzasj, zzarm zzarm, zzur zzur) {
        this.zzdvp = zzarm;
        this.mContext = context;
        this.zzdvq = zzasj;
        this.zzdvs = zzur;
        this.zzdvr = new zzum(this.zzdvs);
        this.zzdvr.zza((zzun) new zzaro(this));
        zzvq zzvq = new zzvq();
        zzvq.zzchy = Integer.valueOf(this.zzdvq.zzbsp.zzeou);
        zzvq.zzchz = Integer.valueOf(this.zzdvq.zzbsp.zzeov);
        zzvq.zzcia = Integer.valueOf(this.zzdvq.zzbsp.zzeow ? 0 : 2);
        this.zzdvr.zza((zzun) new zzarp(zzvq));
        if (this.zzdvq.zzdwh != null) {
            this.zzdvr.zza((zzun) new zzarq(this));
        }
        zzwf zzwf = this.zzdvq.zzbst;
        if (zzwf.zzckl && "interstitial_mb".equals(zzwf.zzckk)) {
            this.zzdvr.zza(zzarr.zzdvx);
        } else if (zzwf.zzckl && "reward_mb".equals(zzwf.zzckk)) {
            this.zzdvr.zza(zzars.zzdvx);
        } else if (zzwf.zzckn || zzwf.zzckl) {
            this.zzdvr.zza(zzaru.zzdvx);
        } else {
            this.zzdvr.zza(zzart.zzdvx);
        }
        this.zzdvr.zza(zzb.AD_REQUEST);
    }

    public final void zzki() {
        zzaxz.zzdn("AdLoaderBackgroundTask started.");
        this.zzdsm = new zzarv(this);
        zzayh.zzelc.postDelayed(this.zzdsm, ((Long) zzwu.zzpz().zzd(zzaan.zzcte)).longValue());
        long elapsedRealtime = zzbv.zzlm().elapsedRealtime();
        if (this.zzdvq.zzdwg.extras != null) {
            String string = this.zzdvq.zzdwg.extras.getString("_ad");
            if (string != null) {
                zzasi zzasi = new zzasi(this.zzdvq, elapsedRealtime, null, null, null, null);
                this.zzdnh = zzasi;
                zza(zzatv.zza(this.mContext, this.zzdnh, string));
                return;
            }
        }
        zzbcr zzbcr = new zzbcr();
        zzayf.zzc(new zzarw(this, zzbcr));
        zzasi zzasi2 = new zzasi(this.zzdvq, elapsedRealtime, zzbv.zzmf().zzx(this.mContext), zzbv.zzmf().zzy(this.mContext), zzbv.zzmf().zzz(this.mContext), zzbv.zzmf().zzaa(this.mContext));
        this.zzdnh = zzasi2;
        zzbcr.zzo(this.zzdnh);
    }

    private final void zzd(int i, String str) {
        zzasi zzasi;
        int i2 = i;
        if (i2 == 3 || i2 == -1) {
            zzaxz.zzen(str);
        } else {
            zzaxz.zzeo(str);
        }
        if (this.zzdsl == null) {
            this.zzdsl = new zzasm(i2);
        } else {
            this.zzdsl = new zzasm(i2, this.zzdsl.zzdlx);
        }
        if (this.zzdnh != null) {
            zzasi = this.zzdnh;
        } else {
            zzasi = new zzasi(this.zzdvq, -1, null, null, null, null);
        }
        zzaxg zzaxg = new zzaxg(zzasi, this.zzdsl, this.zzdmn, null, i, -1, this.zzdsl.zzdyh, null, this.zzdvr, null);
        this.zzdvp.zza(zzaxg);
    }

    /* JADX WARNING: Removed duplicated region for block: B:63:0x0195  */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x01ba  */
    /* JADX WARNING: Removed duplicated region for block: B:76:0x01da  */
    public final void zza(@NonNull zzasm zzasm) {
        Boolean bool;
        JSONObject jSONObject;
        zzaxz.zzdn("Received ad response.");
        this.zzdsl = zzasm;
        String str = this.zzdsl.zzdze;
        if (!TextUtils.isEmpty(str)) {
            zzbv.zzmf().zzh(this.mContext, str);
        }
        long elapsedRealtime = zzbv.zzlm().elapsedRealtime();
        synchronized (this.zzdsn) {
            bool = null;
            this.zzdvu = null;
        }
        zzbv.zzlj().zzyq().zzap(this.zzdsl.zzdxb);
        if (((Boolean) zzwu.zzpz().zzd(zzaan.zzcrz)).booleanValue()) {
            if (this.zzdsl.zzdxn) {
                zzbv.zzlj().zzyq().zzds(this.zzdnh.zzbsn);
            } else {
                zzbv.zzlj().zzyq().zzdt(this.zzdnh.zzbsn);
            }
        }
        try {
            if (this.zzdsl.errorCode != -2) {
                if (this.zzdsl.errorCode != -3) {
                    int i = this.zzdsl.errorCode;
                    StringBuilder sb = new StringBuilder(66);
                    sb.append("There was a problem getting an ad response. ErrorCode: ");
                    sb.append(i);
                    throw new zzarx(sb.toString(), this.zzdsl.errorCode);
                }
            }
            if (this.zzdsl.errorCode != -3) {
                if (!TextUtils.isEmpty(this.zzdsl.zzdyb)) {
                    zzbv.zzlj().zzyq().zzam(this.zzdsl.zzdwn);
                    if (this.zzdsl.zzdyd) {
                        this.zzdmn = new zzakr(this.zzdsl.zzdyb);
                        zzbv.zzlj().zzal(this.zzdmn.zzdlv);
                    } else {
                        zzbv.zzlj().zzal(this.zzdsl.zzdlv);
                    }
                    if (!TextUtils.isEmpty(this.zzdsl.zzdxc)) {
                        zzaxz.zzdn("Received cookie from server. Setting webview cookie in CookieManager.");
                        CookieManager zzba = zzbv.zzlh().zzba(this.mContext);
                        if (zzba != null) {
                            zzba.setCookie("googleads.g.doubleclick.net", this.zzdsl.zzdxc);
                        }
                    }
                } else {
                    throw new zzarx("No fill from ad server.", 3);
                }
            }
            zzwf zza = this.zzdnh.zzbst.zzckm != null ? zza(this.zzdnh) : null;
            zzbv.zzlj().zzyq().zzan(this.zzdsl.zzdyn);
            zzbv.zzlj().zzyq().zzao(this.zzdsl.zzdyz);
            if (!TextUtils.isEmpty(this.zzdsl.zzdyl)) {
                try {
                    jSONObject = new JSONObject(this.zzdsl.zzdyl);
                } catch (Exception e) {
                    zzaxz.zzb("Error parsing the JSON for Active View.", e);
                }
                Bundle bundle = this.zzdnh.zzdwg.zzcjl;
                if (this.zzdsl.zzdzb != 2) {
                    bool = Boolean.valueOf(true);
                    Bundle bundle2 = bundle.getBundle(AdMobAdapter.class.getName());
                    if (bundle2 == null) {
                        bundle2 = new Bundle();
                        bundle.putBundle(AdMobAdapter.class.getName(), bundle2);
                    }
                    bundle2.putBoolean("render_test_ad_label", true);
                } else if (this.zzdsl.zzdzb == 1) {
                    bool = Boolean.valueOf(false);
                } else if (this.zzdsl.zzdzb == 0) {
                    bool = Boolean.valueOf(zzbal.zzf(bundle));
                }
                Boolean bool2 = bool;
                if (this.zzdsl.zzdzf) {
                    Bundle bundle3 = bundle.getBundle(AdMobAdapter.class.getName());
                    if (bundle3 == null) {
                        bundle3 = new Bundle();
                        bundle.putBundle(AdMobAdapter.class.getName(), bundle3);
                    }
                    bundle3.putBoolean("is_analytics_logging_enabled", true);
                }
                zzaxg zzaxg = new zzaxg(this.zzdnh, this.zzdsl, this.zzdmn, zza, -2, elapsedRealtime, this.zzdsl.zzdyh, jSONObject, this.zzdvr, bool2);
                this.zzdvp.zza(zzaxg);
                zzayh.zzelc.removeCallbacks(this.zzdsm);
            }
            jSONObject = null;
            Bundle bundle4 = this.zzdnh.zzdwg.zzcjl;
            if (this.zzdsl.zzdzb != 2) {
            }
            Boolean bool22 = bool;
            if (this.zzdsl.zzdzf) {
            }
            zzaxg zzaxg2 = new zzaxg(this.zzdnh, this.zzdsl, this.zzdmn, zza, -2, elapsedRealtime, this.zzdsl.zzdyh, jSONObject, this.zzdvr, bool22);
            this.zzdvp.zza(zzaxg2);
            zzayh.zzelc.removeCallbacks(this.zzdsm);
        } catch (JSONException e2) {
            zzaxz.zzb("Could not parse mediation config.", e2);
            String str2 = "Could not parse mediation config: ";
            String valueOf = String.valueOf(this.zzdsl.zzdyb);
            throw new zzarx(valueOf.length() != 0 ? str2.concat(valueOf) : new String(str2), 0);
        } catch (zzarx e3) {
            zzd(e3.getErrorCode(), e3.getMessage());
            zzayh.zzelc.removeCallbacks(this.zzdsm);
        }
    }

    public final void onStop() {
        synchronized (this.zzdsn) {
            if (this.zzdvu != null) {
                this.zzdvu.cancel();
            }
        }
    }

    @VisibleForTesting
    private final zzwf zza(zzasi zzasi) throws zzarx {
        int i;
        zzwf[] zzwfArr;
        if (((this.zzdnh == null || this.zzdnh.zzbtn == null || this.zzdnh.zzbtn.size() <= 1) ? false : true) && this.zzdmn != null && !this.zzdmn.zzdmi) {
            return null;
        }
        if (this.zzdsl.zzcko) {
            for (zzwf zzwf : zzasi.zzbst.zzckm) {
                if (zzwf.zzcko) {
                    return new zzwf(zzwf, zzasi.zzbst.zzckm);
                }
            }
        }
        if (this.zzdsl.zzdyg != null) {
            String[] split = this.zzdsl.zzdyg.split(AvidJSONUtil.KEY_X);
            if (split.length != 2) {
                String str = "Invalid ad size format from the ad response: ";
                String valueOf = String.valueOf(this.zzdsl.zzdyg);
                throw new zzarx(valueOf.length() != 0 ? str.concat(valueOf) : new String(str), 0);
            }
            try {
                int parseInt = Integer.parseInt(split[0]);
                int parseInt2 = Integer.parseInt(split[1]);
                zzwf[] zzwfArr2 = zzasi.zzbst.zzckm;
                int length = zzwfArr2.length;
                for (int i2 = 0; i2 < length; i2++) {
                    zzwf zzwf2 = zzwfArr2[i2];
                    float f = this.mContext.getResources().getDisplayMetrics().density;
                    int i3 = zzwf2.width == -1 ? (int) (((float) zzwf2.widthPixels) / f) : zzwf2.width;
                    if (zzwf2.height == -2) {
                        i = (int) (((float) zzwf2.heightPixels) / f);
                    } else {
                        i = zzwf2.height;
                    }
                    if (parseInt == i3 && parseInt2 == i && !zzwf2.zzcko) {
                        return new zzwf(zzwf2, zzasi.zzbst.zzckm);
                    }
                }
                String str2 = "The ad size from the ad response was not one of the requested sizes: ";
                String valueOf2 = String.valueOf(this.zzdsl.zzdyg);
                throw new zzarx(valueOf2.length() != 0 ? str2.concat(valueOf2) : new String(str2), 0);
            } catch (NumberFormatException unused) {
                String str3 = "Invalid ad size number from the ad response: ";
                String valueOf3 = String.valueOf(this.zzdsl.zzdyg);
                throw new zzarx(valueOf3.length() != 0 ? str3.concat(valueOf3) : new String(str3), 0);
            }
        } else {
            throw new zzarx("The ad response must specify one of the supported ad sizes.", 0);
        }
    }

    /* access modifiers changed from: 0000 */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x005f, code lost:
        return;
     */
    public final /* synthetic */ void zza(zzbcn zzbcn) {
        zzasc zzasc;
        synchronized (this.zzdsn) {
            if (this.zzdvt) {
                zzaxz.zzeo("Request task was already canceled");
                return;
            }
            zzbbi zzbbi = this.zzdvq.zzbsp;
            Context context = this.mContext;
            if (new zzarz(context).zza(zzbbi)) {
                zzaxz.zzdn("Fetching ad response from local ad request service.");
                zzasc = new zzasf(context, zzbcn, this);
                zzasc.zzwa();
            } else {
                zzaxz.zzdn("Fetching ad response from remote ad request service.");
                zzwu.zzpv();
                if (!zzbat.zzc(context, GooglePlayServicesUtilLight.GOOGLE_PLAY_SERVICES_VERSION_CODE)) {
                    zzaxz.zzeo("Failed to connect to remote ad request service.");
                    zzasc = null;
                } else {
                    zzasc = new zzasg(context, zzbbi, zzbcn, this);
                }
            }
            this.zzdvu = zzasc;
            if (this.zzdvu == null) {
                zzd(0, "Could not start the ad request service.");
                zzayh.zzelc.removeCallbacks(this.zzdsm);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public final /* synthetic */ void zzwh() {
        synchronized (this.zzdsn) {
            this.zzdvt = true;
            if (this.zzdvu != null) {
                onStop();
            }
            zzd(2, "Timed out waiting for ad response.");
        }
    }

    /* access modifiers changed from: 0000 */
    public final /* synthetic */ void zzb(zzvp zzvp) {
        zzvp.zzchu.zzchc = this.zzdvq.zzdwh.packageName;
    }

    /* access modifiers changed from: 0000 */
    public final /* synthetic */ void zzc(zzvp zzvp) {
        zzvp.zzchp = this.zzdvq.zzdws;
    }
}
