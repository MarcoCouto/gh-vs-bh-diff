package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.util.DisplayMetrics;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.zzc;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.SafeParcelWriter;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Class;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Constructor;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Field;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Param;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable.Reserved;
import com.integralads.avid.library.inmobi.utils.AvidJSONUtil;

@zzark
@Class(creator = "AdSizeParcelCreator")
@Reserved({1})
public class zzwf extends AbstractSafeParcelable {
    public static final Creator<zzwf> CREATOR = new zzwg();
    @Field(id = 3)
    public final int height;
    @Field(id = 4)
    public final int heightPixels;
    @Field(id = 6)
    public final int width;
    @Field(id = 7)
    public final int widthPixels;
    @Field(id = 2)
    public final String zzckk;
    @Field(id = 5)
    public final boolean zzckl;
    @Field(id = 8)
    public final zzwf[] zzckm;
    @Field(id = 9)
    public final boolean zzckn;
    @Field(id = 10)
    public final boolean zzcko;
    @Field(id = 11)
    public boolean zzckp;

    public static int zzb(DisplayMetrics displayMetrics) {
        return displayMetrics.widthPixels;
    }

    public static int zzc(DisplayMetrics displayMetrics) {
        return (int) (((float) zzd(displayMetrics)) * displayMetrics.density);
    }

    private static int zzd(DisplayMetrics displayMetrics) {
        int i = (int) (((float) displayMetrics.heightPixels) / displayMetrics.density);
        if (i <= 400) {
            return 32;
        }
        return i <= 720 ? 50 : 90;
    }

    public static zzwf zzg(Context context) {
        zzwf zzwf = new zzwf("320x50_mb", 0, 0, false, 0, 0, null, true, false, false);
        return zzwf;
    }

    public static zzwf zzpo() {
        zzwf zzwf = new zzwf("reward_mb", 0, 0, true, 0, 0, null, false, false, false);
        return zzwf;
    }

    public zzwf() {
        this("interstitial_mb", 0, 0, true, 0, 0, null, false, false, false);
    }

    public zzwf(Context context, AdSize adSize) {
        this(context, new AdSize[]{adSize});
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x0086  */
    public zzwf(Context context, AdSize[] adSizeArr) {
        int i;
        int i2;
        double d;
        double d2;
        AdSize adSize = adSizeArr[0];
        this.zzckl = false;
        this.zzcko = adSize.isFluid();
        if (this.zzcko) {
            this.width = AdSize.BANNER.getWidth();
            this.height = AdSize.BANNER.getHeight();
        } else {
            this.width = adSize.getWidth();
            this.height = adSize.getHeight();
        }
        boolean z = this.width == -1;
        boolean z2 = this.height == -2;
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        if (z) {
            zzwu.zzpv();
            if (zzbat.zzbi(context)) {
                zzwu.zzpv();
                if (zzbat.zzbj(context)) {
                    int i3 = displayMetrics.widthPixels;
                    zzwu.zzpv();
                    this.widthPixels = i3 - zzbat.zzbk(context);
                    d = (double) (((float) this.widthPixels) / displayMetrics.density);
                    i = (int) d;
                    d2 = (double) i;
                    Double.isNaN(d);
                    Double.isNaN(d2);
                    if (d - d2 >= 0.01d) {
                        i++;
                    }
                }
            }
            this.widthPixels = displayMetrics.widthPixels;
            d = (double) (((float) this.widthPixels) / displayMetrics.density);
            i = (int) d;
            d2 = (double) i;
            Double.isNaN(d);
            Double.isNaN(d2);
            if (d - d2 >= 0.01d) {
            }
        } else {
            i = this.width;
            zzwu.zzpv();
            this.widthPixels = zzbat.zza(displayMetrics, this.width);
        }
        if (z2) {
            i2 = zzd(displayMetrics);
        } else {
            i2 = this.height;
        }
        zzwu.zzpv();
        this.heightPixels = zzbat.zza(displayMetrics, i2);
        if (z || z2) {
            StringBuilder sb = new StringBuilder(26);
            sb.append(i);
            sb.append(AvidJSONUtil.KEY_X);
            sb.append(i2);
            sb.append("_as");
            this.zzckk = sb.toString();
        } else if (this.zzcko) {
            this.zzckk = "320x50_mb";
        } else {
            this.zzckk = adSize.toString();
        }
        if (adSizeArr.length > 1) {
            this.zzckm = new zzwf[adSizeArr.length];
            for (int i4 = 0; i4 < adSizeArr.length; i4++) {
                this.zzckm[i4] = new zzwf(context, adSizeArr[i4]);
            }
        } else {
            this.zzckm = null;
        }
        this.zzckn = false;
        this.zzckp = false;
    }

    public zzwf(zzwf zzwf, zzwf[] zzwfArr) {
        this(zzwf.zzckk, zzwf.height, zzwf.heightPixels, zzwf.zzckl, zzwf.width, zzwf.widthPixels, zzwfArr, zzwf.zzckn, zzwf.zzcko, zzwf.zzckp);
    }

    @Constructor
    zzwf(@Param(id = 2) String str, @Param(id = 3) int i, @Param(id = 4) int i2, @Param(id = 5) boolean z, @Param(id = 6) int i3, @Param(id = 7) int i4, @Param(id = 8) zzwf[] zzwfArr, @Param(id = 9) boolean z2, @Param(id = 10) boolean z3, @Param(id = 11) boolean z4) {
        this.zzckk = str;
        this.height = i;
        this.heightPixels = i2;
        this.zzckl = z;
        this.width = i3;
        this.widthPixels = i4;
        this.zzckm = zzwfArr;
        this.zzckn = z2;
        this.zzcko = z3;
        this.zzckp = z4;
    }

    public final AdSize zzpp() {
        return zzc.zza(this.width, this.height, this.zzckk);
    }

    public void writeToParcel(Parcel parcel, int i) {
        int beginObjectHeader = SafeParcelWriter.beginObjectHeader(parcel);
        SafeParcelWriter.writeString(parcel, 2, this.zzckk, false);
        SafeParcelWriter.writeInt(parcel, 3, this.height);
        SafeParcelWriter.writeInt(parcel, 4, this.heightPixels);
        SafeParcelWriter.writeBoolean(parcel, 5, this.zzckl);
        SafeParcelWriter.writeInt(parcel, 6, this.width);
        SafeParcelWriter.writeInt(parcel, 7, this.widthPixels);
        SafeParcelWriter.writeTypedArray(parcel, 8, this.zzckm, i, false);
        SafeParcelWriter.writeBoolean(parcel, 9, this.zzckn);
        SafeParcelWriter.writeBoolean(parcel, 10, this.zzcko);
        SafeParcelWriter.writeBoolean(parcel, 11, this.zzckp);
        SafeParcelWriter.finishObjectHeader(parcel, beginObjectHeader);
    }
}
