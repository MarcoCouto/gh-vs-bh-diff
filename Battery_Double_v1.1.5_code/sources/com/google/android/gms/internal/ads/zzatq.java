package com.google.android.gms.internal.ads;

import android.content.Context;
import android.location.Location;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.text.TextUtils;
import com.google.android.exoplayer2.source.chunk.ChunkedTrackBlacklistUtil;
import com.google.android.gms.ads.identifier.AdvertisingIdClient.Info;
import com.google.android.gms.ads.internal.zzbv;
import com.google.android.gms.common.util.IOUtils;
import java.io.BufferedOutputStream;
import java.io.Closeable;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import javax.annotation.concurrent.GuardedBy;
import org.json.JSONException;
import org.json.JSONObject;

@zzark
public final class zzatq extends zzasr {
    private static final Object sLock = new Object();
    @GuardedBy("sLock")
    private static zzatq zzeau;
    private final Context mContext;
    private final zzatp zzeav;
    private final ScheduledExecutorService zzeaw = Executors.newSingleThreadScheduledExecutor();

    public static zzatq zza(Context context, zzatp zzatp) {
        zzatq zzatq;
        synchronized (sLock) {
            if (zzeau == null) {
                if (context.getApplicationContext() != null) {
                    context = context.getApplicationContext();
                }
                zzaan.initialize(context);
                zzeau = new zzatq(context, zzatp);
                if (context.getApplicationContext() != null) {
                    zzbv.zzlf().zzaj(context);
                }
                zzaxx.zzag(context);
            }
            zzatq = zzeau;
        }
        return zzatq;
    }

    public final void zza(zzatb zzatb, zzasw zzasw) {
        zzaxz.v("Nonagon code path entered in octagon");
        throw new IllegalArgumentException();
    }

    public final void zzb(zzatb zzatb, zzasw zzasw) {
        zzaxz.v("Nonagon code path entered in octagon");
        throw new IllegalArgumentException();
    }

    private static zzasm zza(Context context, zzatp zzatp, zzasi zzasi, ScheduledExecutorService scheduledExecutorService) {
        String str;
        char c;
        Context context2 = context;
        zzatp zzatp2 = zzatp;
        zzasi zzasi2 = zzasi;
        ScheduledExecutorService scheduledExecutorService2 = scheduledExecutorService;
        zzaxz.zzdn("Starting ad request from service using: google.afma.request.getAdDictionary");
        zzaba zzaba = new zzaba(((Boolean) zzwu.zzpz().zzd(zzaan.zzcpw)).booleanValue(), "load_ad", zzasi2.zzbst.zzckk);
        if (zzasi2.versionCode > 10 && zzasi2.zzdwv != -1) {
            zzaba.zza(zzaba.zzao(zzasi2.zzdwv), "cts");
        }
        zzaay zzrg = zzaba.zzrg();
        zzbcb zza = zzbbq.zza(zzatp2.zzear.zzm(context2), ((Long) zzwu.zzpz().zzd(zzaan.zzcvj)).longValue(), TimeUnit.MILLISECONDS, scheduledExecutorService2);
        Future zzm = zzbbq.zzm(null);
        if (((Boolean) zzwu.zzpz().zzd(zzaan.zzcxz)).booleanValue()) {
            zzm = zzatp2.zzeam.zzdo(zzasi2.zzdwh.packageName);
        }
        zzbcb zzdp = zzatp2.zzeam.zzdp(zzasi2.zzdwh.packageName);
        zzbcb zza2 = zzatp2.zzeas.zza(zzasi2.zzdwi, zzasi2.zzdwh);
        Future zzt = zzbv.zzlq().zzt(context2);
        zzbcb zzm2 = zzbbq.zzm(null);
        Bundle bundle = zzasi2.zzdwg.extras;
        boolean z = (bundle == null || bundle.getString("_ad") == null) ? false : true;
        if (zzasi2.zzdxb && !z) {
            zzm2 = zzatp2.zzeap.zza(zzasi2.applicationInfo);
        }
        zzaay zzaay = zzrg;
        zzbcb zza3 = zzbbq.zza(zzm2, ((Long) zzwu.zzpz().zzd(zzaan.zzcuu)).longValue(), TimeUnit.MILLISECONDS, scheduledExecutorService2);
        Future zzm3 = zzbbq.zzm(null);
        if (((Boolean) zzwu.zzpz().zzd(zzaan.zzcro)).booleanValue()) {
            zzm3 = zzbbq.zza(zzatp2.zzeas.zzad(context2), ((Long) zzwu.zzpz().zzd(zzaan.zzcrp)).longValue(), TimeUnit.MILLISECONDS, scheduledExecutorService2);
        }
        Bundle bundle2 = (zzasi2.versionCode < 4 || zzasi2.zzdwm == null) ? null : zzasi2.zzdwm;
        zzbv.zzlf();
        if (zzayh.zzn(context2, "android.permission.ACCESS_NETWORK_STATE") && ((ConnectivityManager) context2.getSystemService("connectivity")).getActiveNetworkInfo() == null) {
            zzaxz.zzdn("Device is offline.");
        }
        if (zzasi2.versionCode >= 7) {
            str = zzasi2.zzdws;
        } else {
            str = UUID.randomUUID().toString();
        }
        if (zzasi2.zzdwg.extras != null) {
            String string = zzasi2.zzdwg.extras.getString("_ad");
            if (string != null) {
                return zzatv.zza(context2, zzasi2, string);
            }
        }
        List<String> zzf = zzatp2.zzean.zzf(zzasi2.zzdwt);
        Object obj = str;
        zzaba zzaba2 = zzaba;
        Bundle bundle3 = (Bundle) zzbbq.zza((Future<T>) zza, null, ((Long) zzwu.zzpz().zzd(zzaan.zzcvj)).longValue(), TimeUnit.MILLISECONDS);
        Location location = (Location) zzbbq.zza((Future<T>) zza3, null);
        Info info = (Info) zzbbq.zza(zzm3, null);
        String str2 = (String) zzbbq.zza((Future<T>) zza2, null);
        String str3 = (String) zzbbq.zza(zzm, null);
        String str4 = (String) zzbbq.zza((Future<T>) zzdp, null);
        zzatz zzatz = (zzatz) zzbbq.zza(zzt, null);
        if (zzatz == null) {
            zzaxz.zzeo("Error fetching device info. This is not recoverable.");
            return new zzasm(0);
        }
        zzato zzato = new zzato();
        zzato.zzeag = zzasi2;
        zzato.zzeah = zzatz;
        zzato.zzcjj = location;
        zzato.zzeac = bundle3;
        zzato.zzdwi = str2;
        zzato.zzeaf = info;
        if (zzf == null) {
            zzato.zzdwt.clear();
        }
        zzato.zzdwt = zzf;
        zzato.zzdwm = bundle2;
        zzato.zzead = str3;
        zzato.zzeae = str4;
        zzato.zzeai = zzatp2.zzeal.zzf(context2);
        zzato.zzeaj = zzatp2.zzeaj;
        JSONObject zza4 = zzatv.zza(context2, zzato);
        if (zza4 == null) {
            return new zzasm(0);
        }
        if (zzasi2.versionCode < 7) {
            try {
                zza4.put("request_id", obj);
            } catch (JSONException unused) {
            }
        }
        zzaay zzaay2 = zzaay;
        zzaba zzaba3 = zzaba2;
        zzaba3.zza(zzaay2, "arc");
        ScheduledExecutorService scheduledExecutorService3 = scheduledExecutorService;
        zzbcb zza5 = zzbbq.zza(zzbbq.zza(zzatp2.zzeat.zzwo().zzj(zza4), zzatr.zzbni, (Executor) scheduledExecutorService3), 10, TimeUnit.SECONDS, scheduledExecutorService3);
        zzbcb zzwy = zzatp2.zzeao.zzwy();
        if (zzwy != null) {
            zzbbo.zza(zzwy, "AdRequestServiceImpl.loadAd.flags");
        }
        zzasm zzasm = null;
        zzaty zzaty = (zzaty) zzbbq.zza((Future<T>) zza5, null);
        if (zzaty == null) {
            return new zzasm(0);
        }
        if (zzaty.getErrorCode() != -2) {
            return new zzasm(zzaty.getErrorCode());
        }
        zzaba3.zzrj();
        if (!TextUtils.isEmpty(zzaty.zzwt())) {
            zzasm = zzatv.zza(context2, zzasi2, zzaty.zzwt());
        }
        if (zzasm == null && !TextUtils.isEmpty(zzaty.getUrl())) {
            zzasm = zza(zzasi, context, zzasi2.zzbsp.zzdp, zzaty.getUrl(), str3, str4, zzaty, zzaba3, zzatp);
        }
        if (zzasm == null) {
            c = 0;
            zzasm = new zzasm(0);
        } else {
            c = 0;
        }
        String[] strArr = new String[1];
        strArr[c] = "tts";
        zzaba3.zza(zzaay2, strArr);
        zzasm.zzdyq = zzaba3.zzrh();
        zzasm.zzdze = zzaty.zzwv();
        return zzasm;
    }

    private static void zza(String str, Map<String, List<String>> map, String str2, int i) {
        if (zzaxz.isLoggable(2)) {
            StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 39);
            sb.append("Http Response: {\n  URL:\n    ");
            sb.append(str);
            sb.append("\n  Headers:");
            zzaxz.v(sb.toString());
            if (map != null) {
                for (String str3 : map.keySet()) {
                    StringBuilder sb2 = new StringBuilder(String.valueOf(str3).length() + 5);
                    sb2.append("    ");
                    sb2.append(str3);
                    sb2.append(":");
                    zzaxz.v(sb2.toString());
                    for (String valueOf : (List) map.get(str3)) {
                        String str4 = "      ";
                        String valueOf2 = String.valueOf(valueOf);
                        zzaxz.v(valueOf2.length() != 0 ? str4.concat(valueOf2) : new String(str4));
                    }
                }
            }
            zzaxz.v("  Body:");
            if (str2 != null) {
                int i2 = 0;
                while (i2 < Math.min(str2.length(), DefaultOggSeeker.MATCH_BYTE_RANGE)) {
                    int i3 = i2 + 1000;
                    zzaxz.v(str2.substring(i2, Math.min(str2.length(), i3)));
                    i2 = i3;
                }
            } else {
                zzaxz.v("    null");
            }
            StringBuilder sb3 = new StringBuilder(34);
            sb3.append("  Response Code:\n    ");
            sb3.append(i);
            sb3.append("\n}");
            zzaxz.v(sb3.toString());
        }
    }

    /* JADX INFO: used method not loaded: com.google.android.gms.internal.ads.zzatw.zza(long, com.google.android.gms.internal.ads.zzaty):null, types can be incorrect */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x00eb, code lost:
        r0 = r7.toString();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:?, code lost:
        r7 = new java.io.InputStreamReader(r12.getInputStream());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:?, code lost:
        com.google.android.gms.ads.internal.zzbv.zzlf();
        r11 = com.google.android.gms.internal.ads.zzayh.zza(r7);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:?, code lost:
        com.google.android.gms.common.util.IOUtils.closeQuietly((java.io.Closeable) r7);
        r4.zzek(r11);
        zza(r0, r13, r11, r10);
        r6.zza(r0, r13, r11);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x010b, code lost:
        if (r2 == null) goto L_0x0118;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x010d, code lost:
        r2.zza(r5, "ufe");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x0118, code lost:
        r0 = r6.zza(r8, r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:?, code lost:
        r12.disconnect();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x011f, code lost:
        if (r3 == null) goto L_0x0126;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x0121, code lost:
        r3.zzeaq.zzxa();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x0126, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x0127, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x0128, code lost:
        r16 = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x012b, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x012c, code lost:
        r16 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:?, code lost:
        com.google.android.gms.common.util.IOUtils.closeQuietly((java.io.Closeable) r16);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x0131, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x014c, code lost:
        com.google.android.gms.internal.ads.zzaxz.zzeo("No location header to follow redirect.");
        r0 = new com.google.android.gms.internal.ads.zzasm(0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:79:?, code lost:
        r12.disconnect();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:80:0x015a, code lost:
        if (r3 == null) goto L_0x0161;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:0x015c, code lost:
        r3.zzeaq.zzxa();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:0x0161, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:86:0x017b, code lost:
        com.google.android.gms.internal.ads.zzaxz.zzeo("Too many redirects.");
        r0 = new com.google.android.gms.internal.ads.zzasm(0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:88:?, code lost:
        r12.disconnect();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:89:0x0189, code lost:
        if (r3 == null) goto L_0x0190;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:90:0x018b, code lost:
        r3.zzeaq.zzxa();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:91:0x0190, code lost:
        return r0;
     */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x008c A[Catch:{ all -> 0x00c2, all -> 0x01ca }] */
    public static zzasm zza(zzasi zzasi, Context context, String str, String str2, String str3, String str4, zzaty zzaty, zzaba zzaba, zzatp zzatp) {
        HttpURLConnection httpURLConnection;
        String str5;
        byte[] bArr;
        int responseCode;
        Map headerFields;
        BufferedOutputStream bufferedOutputStream;
        BufferedOutputStream bufferedOutputStream2;
        zzasi zzasi2 = zzasi;
        zzaty zzaty2 = zzaty;
        zzaba zzaba2 = zzaba;
        zzatp zzatp2 = zzatp;
        zzaay zzrg = zzaba2 != null ? zzaba.zzrg() : null;
        try {
            zzatw zzatw = new zzatw(zzasi2, zzaty.zzwq());
            String str6 = "AdRequestServiceImpl: Sending request: ";
            String valueOf = String.valueOf(str2);
            zzaxz.zzdn(valueOf.length() != 0 ? str6.concat(valueOf) : new String(str6));
            URL url = new URL(str2);
            long elapsedRealtime = zzbv.zzlm().elapsedRealtime();
            boolean z = false;
            int i = 0;
            while (true) {
                if (zzatp2 != null) {
                    zzatp2.zzeaq.zzwz();
                }
                httpURLConnection = (HttpURLConnection) url.openConnection();
                try {
                    zzbv.zzlf().zza(context, str, z, httpURLConnection);
                    if (zzaty.zzws()) {
                        if (!TextUtils.isEmpty(str3)) {
                            httpURLConnection.addRequestProperty("x-afma-drt-cookie", str3);
                        } else {
                            String str7 = str3;
                        }
                        if (!TextUtils.isEmpty(str4)) {
                            httpURLConnection.addRequestProperty("x-afma-drt-v2-cookie", str4);
                            str5 = zzasi2.zzdxc;
                            if (!TextUtils.isEmpty(str5)) {
                                zzaxz.zzdn("Sending webview cookie in ad request header.");
                                httpURLConnection.addRequestProperty("Cookie", str5);
                            }
                            if (zzaty2 != null || TextUtils.isEmpty(zzaty.zzwr())) {
                                bArr = null;
                            } else {
                                httpURLConnection.setDoOutput(true);
                                bArr = zzaty.zzwr().getBytes();
                                httpURLConnection.setFixedLengthStreamingMode(bArr.length);
                                try {
                                    bufferedOutputStream2 = new BufferedOutputStream(httpURLConnection.getOutputStream());
                                    bufferedOutputStream2.write(bArr);
                                    IOUtils.closeQuietly((Closeable) bufferedOutputStream2);
                                } catch (Throwable th) {
                                    th = th;
                                    bufferedOutputStream = null;
                                    IOUtils.closeQuietly((Closeable) bufferedOutputStream);
                                    throw th;
                                }
                            }
                            zzbax zzbax = new zzbax(zzasi2.zzdws);
                            zzbax.zza(httpURLConnection, bArr);
                            responseCode = httpURLConnection.getResponseCode();
                            headerFields = httpURLConnection.getHeaderFields();
                            zzbax.zza(httpURLConnection, responseCode);
                            if (responseCode < 200 && responseCode < 300) {
                                break;
                            }
                            zza(url.toString(), headerFields, (String) null, responseCode);
                            if (responseCode >= 300 && responseCode < 400) {
                                String headerField = httpURLConnection.getHeaderField("Location");
                                if (TextUtils.isEmpty(headerField)) {
                                    break;
                                }
                                url = new URL(headerField);
                                i++;
                                if (i > ((Integer) zzwu.zzpz().zzd(zzaan.zzcwx)).intValue()) {
                                    break;
                                }
                                zzatw.zzl(headerFields);
                                httpURLConnection.disconnect();
                                if (zzatp2 != null) {
                                    zzatp2.zzeaq.zzxa();
                                }
                                zzasi2 = zzasi;
                                z = false;
                            }
                        }
                    } else {
                        String str8 = str3;
                    }
                    String str9 = str4;
                    str5 = zzasi2.zzdxc;
                    if (!TextUtils.isEmpty(str5)) {
                    }
                    if (zzaty2 != null) {
                    }
                    bArr = null;
                    zzbax zzbax2 = new zzbax(zzasi2.zzdws);
                    zzbax2.zza(httpURLConnection, bArr);
                    responseCode = httpURLConnection.getResponseCode();
                    headerFields = httpURLConnection.getHeaderFields();
                    zzbax2.zza(httpURLConnection, responseCode);
                    if (responseCode < 200) {
                    }
                    zza(url.toString(), headerFields, (String) null, responseCode);
                    if (responseCode >= 300) {
                        break;
                    }
                    break;
                } catch (Throwable th2) {
                    httpURLConnection.disconnect();
                    if (zzatp2 != null) {
                        zzatp2.zzeaq.zzxa();
                    }
                    throw th2;
                }
            }
            StringBuilder sb = new StringBuilder(46);
            sb.append("Received error HTTP response code: ");
            sb.append(responseCode);
            zzaxz.zzeo(sb.toString());
            zzasm zzasm = new zzasm(0);
            httpURLConnection.disconnect();
            if (zzatp2 != null) {
                zzatp2.zzeaq.zzxa();
            }
            return zzasm;
        } catch (IOException e) {
            String str10 = "Error while connecting to ad server: ";
            String valueOf2 = String.valueOf(e.getMessage());
            zzaxz.zzeo(valueOf2.length() != 0 ? str10.concat(valueOf2) : new String(str10));
            return new zzasm(2);
        }
    }

    private zzatq(Context context, zzatp zzatp) {
        this.mContext = context;
        this.zzeav = zzatp;
    }

    public final zzasm zzb(zzasi zzasi) {
        return zza(this.mContext, this.zzeav, zzasi, this.zzeaw);
    }

    public final void zza(zzasi zzasi, zzast zzast) {
        zzbv.zzlj().zzd(this.mContext, zzasi.zzbsp);
        zzbcb zzc = zzayf.zzc(new zzats(this, zzasi, zzast));
        zzbv.zzlv().zzaak();
        zzbv.zzlv().getHandler().postDelayed(new zzatt(this, zzc), ChunkedTrackBlacklistUtil.DEFAULT_TRACK_BLACKLIST_MS);
    }
}
