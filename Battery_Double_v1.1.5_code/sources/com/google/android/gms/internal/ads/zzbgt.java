package com.google.android.gms.internal.ads;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.net.Uri;
import android.os.Build.VERSION;
import android.support.annotation.Nullable;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.ViewTreeObserver.OnScrollChangedListener;
import android.view.WindowManager;
import android.webkit.DownloadListener;
import android.webkit.ValueCallback;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.google.android.gms.ads.internal.gmsg.zzu;
import com.google.android.gms.ads.internal.overlay.zzc;
import com.google.android.gms.ads.internal.overlay.zzd;
import com.google.android.gms.ads.internal.zzbo;
import com.google.android.gms.ads.internal.zzbv;
import com.google.android.gms.ads.internal.zzv;
import com.google.android.gms.common.util.PlatformVersion;
import com.google.android.gms.common.util.Predicate;
import com.google.android.gms.common.util.VisibleForTesting;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.internal.ads.zzuo.zza.zzb;
import com.integralads.avid.library.inmobi.utils.AvidJSONUtil;
import com.integralads.avid.library.inmobi.video.AvidVideoPlaybackListenerImpl;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.ironsource.sdk.ISNAdView.ISNAdViewConstants;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.ParametersAreNonnullByDefault;
import javax.annotation.concurrent.GuardedBy;
import org.json.JSONException;
import org.json.JSONObject;

@zzark
@ParametersAreNonnullByDefault
@VisibleForTesting
final class zzbgt extends WebView implements OnGlobalLayoutListener, DownloadListener, zzbgg {
    @GuardedBy("this")
    private boolean mIsDestroyed;
    private final zzv zzbly;
    private final zzbbi zzbob;
    private zzbas zzbuf;
    private final WindowManager zzbuv;
    private final DisplayMetrics zzbwk;
    @Nullable
    private final zzcu zzdcf;
    private int zzdqf = -1;
    private int zzdqg = -1;
    private int zzdqi = -1;
    private int zzdqj = -1;
    private final zzum zzdvr;
    @GuardedBy("this")
    private Boolean zzeiy;
    @GuardedBy("this")
    private String zzetu = "";
    private zzaay zzetv;
    private final zzbhs zzezf;
    private final zzbo zzezg;
    private final float zzezh;
    private boolean zzezi = false;
    private boolean zzezj = false;
    private zzbgh zzezk;
    @GuardedBy("this")
    private zzd zzezl;
    @GuardedBy("this")
    private IObjectWrapper zzezm;
    @GuardedBy("this")
    private zzbht zzezn;
    @GuardedBy("this")
    private boolean zzezo;
    @GuardedBy("this")
    private boolean zzezp;
    @GuardedBy("this")
    private boolean zzezq;
    @GuardedBy("this")
    private int zzezr;
    @GuardedBy("this")
    private boolean zzezs = true;
    @GuardedBy("this")
    private boolean zzezt = false;
    @GuardedBy("this")
    private zzbgw zzezu;
    @GuardedBy("this")
    private boolean zzezv;
    @GuardedBy("this")
    private boolean zzezw;
    @GuardedBy("this")
    private zzacb zzezx;
    @GuardedBy("this")
    private int zzezy;
    /* access modifiers changed from: private */
    @GuardedBy("this")
    public int zzezz;
    private zzaay zzfaa;
    private zzaay zzfab;
    private zzaaz zzfac;
    private WeakReference<OnClickListener> zzfad;
    @GuardedBy("this")
    private zzd zzfae;
    @GuardedBy("this")
    private boolean zzfaf;
    private Map<String, zzbfk> zzfag;
    @GuardedBy("this")
    private String zzvv;

    static zzbgt zzb(Context context, zzbht zzbht, String str, boolean z, boolean z2, @Nullable zzcu zzcu, zzbbi zzbbi, zzaba zzaba, zzbo zzbo, zzv zzv, zzum zzum) {
        Context context2 = context;
        zzbgt zzbgt = new zzbgt(new zzbhs(context), zzbht, str, z, z2, zzcu, zzbbi, zzaba, zzbo, zzv, zzum);
        return zzbgt;
    }

    public final View getView() {
        return this;
    }

    public final WebView getWebView() {
        return this;
    }

    public final zzbdq zzabt() {
        return null;
    }

    @VisibleForTesting
    private zzbgt(zzbhs zzbhs, zzbht zzbht, String str, boolean z, boolean z2, @Nullable zzcu zzcu, zzbbi zzbbi, zzaba zzaba, zzbo zzbo, zzv zzv, zzum zzum) {
        super(zzbhs);
        this.zzezf = zzbhs;
        this.zzezn = zzbht;
        this.zzvv = str;
        this.zzezp = z;
        this.zzezr = -1;
        this.zzdcf = zzcu;
        this.zzbob = zzbbi;
        this.zzezg = zzbo;
        this.zzbly = zzv;
        this.zzbuv = (WindowManager) getContext().getSystemService("window");
        zzbv.zzlf();
        this.zzbwk = zzayh.zza(this.zzbuv);
        this.zzezh = this.zzbwk.density;
        this.zzdvr = zzum;
        setBackgroundColor(0);
        WebSettings settings = getSettings();
        settings.setAllowFileAccess(false);
        try {
            settings.setJavaScriptEnabled(true);
        } catch (NullPointerException e) {
            zzaxz.zzb("Unable to enable Javascript.", e);
        }
        settings.setSavePassword(false);
        settings.setSupportMultipleWindows(true);
        settings.setJavaScriptCanOpenWindowsAutomatically(true);
        if (VERSION.SDK_INT >= 21) {
            settings.setMixedContentMode(2);
        }
        zzbv.zzlf().zza((Context) zzbhs, zzbbi.zzdp, settings);
        zzbv.zzlh().zza(getContext(), settings);
        setDownloadListener(this);
        zzaer();
        if (PlatformVersion.isAtLeastJellyBeanMR1()) {
            addJavascriptInterface(zzbgz.zzk(this), "googleAdsJsInterface");
        }
        removeJavascriptInterface("accessibility");
        removeJavascriptInterface("accessibilityTraversal");
        this.zzbuf = new zzbas(this.zzezf.zzabw(), this, this, null);
        zzaev();
        this.zzfac = new zzaaz(new zzaba(true, "make_wv", this.zzvv));
        this.zzfac.zzrf().zzc(zzaba);
        this.zzetv = zzaat.zzb(this.zzfac.zzrf());
        this.zzfac.zza("native:view_create", this.zzetv);
        this.zzfab = null;
        this.zzfaa = null;
        zzbv.zzlh().zzaz(zzbhs);
        zzbv.zzlj().zzyn();
    }

    public final void setWebViewClient(WebViewClient webViewClient) {
        super.setWebViewClient(webViewClient);
        if (webViewClient instanceof zzbgh) {
            this.zzezk = (zzbgh) webViewClient;
        }
    }

    public final zzv zzid() {
        return this.zzbly;
    }

    private final boolean zzaeo() {
        int i;
        int i2;
        boolean z = false;
        if (!this.zzezk.zzmu() && !this.zzezk.zzaeb()) {
            return false;
        }
        zzwu.zzpv();
        int zzb = zzbat.zzb(this.zzbwk, this.zzbwk.widthPixels);
        zzwu.zzpv();
        int zzb2 = zzbat.zzb(this.zzbwk, this.zzbwk.heightPixels);
        Activity zzabw = this.zzezf.zzabw();
        if (zzabw == null || zzabw.getWindow() == null) {
            i2 = zzb;
            i = zzb2;
        } else {
            zzbv.zzlf();
            int[] zzg = zzayh.zzg(zzabw);
            zzwu.zzpv();
            int zzb3 = zzbat.zzb(this.zzbwk, zzg[0]);
            zzwu.zzpv();
            i = zzbat.zzb(this.zzbwk, zzg[1]);
            i2 = zzb3;
        }
        if (this.zzdqf == zzb && this.zzdqg == zzb2 && this.zzdqi == i2 && this.zzdqj == i) {
            return false;
        }
        if (!(this.zzdqf == zzb && this.zzdqg == zzb2)) {
            z = true;
        }
        this.zzdqf = zzb;
        this.zzdqg = zzb2;
        this.zzdqi = i2;
        this.zzdqj = i;
        new zzaok(this).zza(zzb, zzb2, i2, i, this.zzbwk.density, this.zzbuv.getDefaultDisplay().getRotation());
        return z;
    }

    public final void zza(String str, Map<String, ?> map) {
        try {
            zza(str, zzbv.zzlf().zzn(map));
        } catch (JSONException unused) {
            zzaxz.zzeo("Could not convert parameters to JSON.");
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0013, code lost:
        return;
     */
    @TargetApi(19)
    public final synchronized void evaluateJavascript(String str, ValueCallback<String> valueCallback) {
        if (isDestroyed()) {
            zzaxz.zzeq("#004 The webview is destroyed. Ignoring action.");
            if (valueCallback != null) {
                valueCallback.onReceiveValue(null);
            }
        } else {
            super.evaluateJavascript(str, valueCallback);
        }
    }

    private final synchronized void zzfc(String str) {
        if (!isDestroyed()) {
            loadUrl(str);
        } else {
            zzaxz.zzeo("#004 The webview is destroyed. Ignoring action.");
        }
    }

    public final synchronized void loadUrl(String str) {
        if (!isDestroyed()) {
            try {
                super.loadUrl(str);
            } catch (Exception | IncompatibleClassChangeError | NoClassDefFoundError e) {
                zzbv.zzlj().zza(e, "AdWebViewImpl.loadUrl");
                zzaxz.zzc("Could not call loadUrl. ", e);
            }
        } else {
            zzaxz.zzeo("#004 The webview is destroyed. Ignoring action.");
        }
    }

    private final synchronized void zzfd(String str) {
        try {
            super.loadUrl(str);
        } catch (Exception | IncompatibleClassChangeError | NoClassDefFoundError | UnsatisfiedLinkError e) {
            zzbv.zzlj().zza(e, "AdWebViewImpl.loadUrlUnsafe");
            zzaxz.zzc("Could not call loadUrl. ", e);
        }
    }

    public final synchronized void loadData(String str, String str2, String str3) {
        if (!isDestroyed()) {
            super.loadData(str, str2, str3);
        } else {
            zzaxz.zzeo("#004 The webview is destroyed. Ignoring action.");
        }
    }

    public final synchronized void loadDataWithBaseURL(String str, String str2, String str3, String str4, String str5) {
        if (!isDestroyed()) {
            super.loadDataWithBaseURL(str, str2, str3, str4, str5);
        } else {
            zzaxz.zzeo("#004 The webview is destroyed. Ignoring action.");
        }
    }

    public final synchronized void zzc(String str, String str2, @Nullable String str3) {
        if (!isDestroyed()) {
            if (((Boolean) zzwu.zzpz().zzd(zzaan.zzcre)).booleanValue()) {
                str2 = zzbhi.zzc(str2, zzbhi.zzaex());
            }
            super.loadDataWithBaseURL(str, str2, WebRequest.CONTENT_TYPE_HTML, "UTF-8", str3);
            return;
        }
        zzaxz.zzeo("#004 The webview is destroyed. Ignoring action.");
    }

    @TargetApi(19)
    private final synchronized void zza(String str, ValueCallback<String> valueCallback) {
        if (!isDestroyed()) {
            evaluateJavascript(str, null);
        } else {
            zzaxz.zzeo("#004 The webview is destroyed. Ignoring action.");
        }
    }

    private final void zzfe(String str) {
        if (PlatformVersion.isAtLeastKitKat()) {
            if (zzyi() == null) {
                zzaep();
            }
            if (zzyi().booleanValue()) {
                zza(str, null);
                return;
            }
            String str2 = "javascript:";
            String valueOf = String.valueOf(str);
            zzfc(valueOf.length() != 0 ? str2.concat(valueOf) : new String(str2));
            return;
        }
        String str3 = "javascript:";
        String valueOf2 = String.valueOf(str);
        zzfc(valueOf2.length() != 0 ? str3.concat(valueOf2) : new String(str3));
    }

    public final void zzcg(String str) {
        zzfe(str);
    }

    private final synchronized void zzaep() {
        this.zzeiy = zzbv.zzlj().zzyi();
        if (this.zzeiy == null) {
            try {
                evaluateJavascript("(function(){})()", null);
                zza(Boolean.valueOf(true));
            } catch (IllegalStateException unused) {
                zza(Boolean.valueOf(false));
            }
        }
    }

    @VisibleForTesting
    private final void zza(Boolean bool) {
        synchronized (this) {
            this.zzeiy = bool;
        }
        zzbv.zzlj().zza(bool);
    }

    @VisibleForTesting
    private final synchronized Boolean zzyi() {
        return this.zzeiy;
    }

    public final void zzb(String str, JSONObject jSONObject) {
        if (jSONObject == null) {
            jSONObject = new JSONObject();
        }
        String jSONObject2 = jSONObject.toString();
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 3 + String.valueOf(jSONObject2).length());
        sb.append(str);
        sb.append("(");
        sb.append(jSONObject2);
        sb.append(");");
        zzfe(sb.toString());
    }

    public final void zza(String str, JSONObject jSONObject) {
        if (jSONObject == null) {
            jSONObject = new JSONObject();
        }
        String jSONObject2 = jSONObject.toString();
        StringBuilder sb = new StringBuilder();
        sb.append("(window.AFMA_ReceiveMessage || function() {})('");
        sb.append(str);
        sb.append("'");
        sb.append(",");
        sb.append(jSONObject2);
        sb.append(");");
        String str2 = "Dispatching AFMA event: ";
        String valueOf = String.valueOf(sb.toString());
        zzaxz.zzdn(valueOf.length() != 0 ? str2.concat(valueOf) : new String(str2));
        zzfe(sb.toString());
    }

    public final void zzade() {
        zzaeq();
        HashMap hashMap = new HashMap(1);
        hashMap.put("version", this.zzbob.zzdp);
        zza("onhide", (Map<String, ?>) hashMap);
    }

    public final void zzdh(int i) {
        if (i == 0) {
            zzaat.zza(this.zzfac.zzrf(), this.zzetv, "aebb2");
        }
        zzaeq();
        if (this.zzfac.zzrf() != null) {
            this.zzfac.zzrf().zzg("close_type", String.valueOf(i));
        }
        HashMap hashMap = new HashMap(2);
        hashMap.put("closetype", String.valueOf(i));
        hashMap.put("version", this.zzbob.zzdp);
        zza("onhide", (Map<String, ?>) hashMap);
    }

    private final void zzaeq() {
        zzaat.zza(this.zzfac.zzrf(), this.zzetv, "aeh2");
    }

    public final void zzvv() {
        if (this.zzfaa == null) {
            zzaat.zza(this.zzfac.zzrf(), this.zzetv, "aes2");
            this.zzfaa = zzaat.zzb(this.zzfac.zzrf());
            this.zzfac.zza("native:view_show", this.zzfaa);
        }
        HashMap hashMap = new HashMap(1);
        hashMap.put("version", this.zzbob.zzdp);
        zza("onshow", (Map<String, ?>) hashMap);
    }

    public final void zzadf() {
        HashMap hashMap = new HashMap(3);
        hashMap.put("app_muted", String.valueOf(zzbv.zzlk().zzkk()));
        hashMap.put("app_volume", String.valueOf(zzbv.zzlk().zzkj()));
        hashMap.put("device_volume", String.valueOf(zzaza.zzbb(getContext())));
        zza(AvidVideoPlaybackListenerImpl.VOLUME, (Map<String, ?>) hashMap);
    }

    public final void zza(boolean z, long j) {
        HashMap hashMap = new HashMap(2);
        hashMap.put("success", z ? "1" : "0");
        hashMap.put(IronSourceConstants.EVENTS_DURATION, Long.toString(j));
        zza("onCacheAccessComplete", (Map<String, ?>) hashMap);
    }

    public final synchronized zzd zzadh() {
        return this.zzezl;
    }

    public final synchronized IObjectWrapper zzadp() {
        return this.zzezm;
    }

    public final synchronized zzd zzadi() {
        return this.zzfae;
    }

    public final synchronized zzbht zzadj() {
        return this.zzezn;
    }

    public final synchronized String zzadk() {
        return this.zzvv;
    }

    public final WebViewClient zzadm() {
        return this.zzezk;
    }

    public final synchronized boolean zzadn() {
        return this.zzezo;
    }

    public final zzcu zzado() {
        return this.zzdcf;
    }

    public final zzbbi zzabz() {
        return this.zzbob;
    }

    public final synchronized boolean zzadq() {
        return this.zzezp;
    }

    public final void onDownloadStart(String str, String str2, String str3, String str4, long j) {
        try {
            Intent intent = new Intent("android.intent.action.VIEW");
            intent.setDataAndType(Uri.parse(str), str4);
            zzbv.zzlf();
            zzayh.zza(getContext(), intent);
        } catch (ActivityNotFoundException unused) {
            StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 51 + String.valueOf(str4).length());
            sb.append("Couldn't find an Activity to view url/mimetype: ");
            sb.append(str);
            sb.append(" / ");
            sb.append(str4);
            zzaxz.zzdn(sb.toString());
        }
    }

    public final boolean onTouchEvent(MotionEvent motionEvent) {
        if (this.zzezk.zzaeb()) {
            synchronized (this) {
                if (this.zzezx != null) {
                    this.zzezx.zzc(motionEvent);
                }
            }
        } else if (this.zzdcf != null) {
            this.zzdcf.zza(motionEvent);
        }
        if (isDestroyed()) {
            return false;
        }
        return super.onTouchEvent(motionEvent);
    }

    public final boolean onGenericMotionEvent(MotionEvent motionEvent) {
        float axisValue = motionEvent.getAxisValue(9);
        float axisValue2 = motionEvent.getAxisValue(10);
        if (motionEvent.getActionMasked() != 8 || ((axisValue <= 0.0f || canScrollVertically(-1)) && ((axisValue >= 0.0f || canScrollVertically(1)) && ((axisValue2 <= 0.0f || canScrollHorizontally(-1)) && (axisValue2 >= 0.0f || canScrollHorizontally(1)))))) {
            return super.onGenericMotionEvent(motionEvent);
        }
        return false;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:121:0x01f9, code lost:
        return;
     */
    /* JADX WARNING: Removed duplicated region for block: B:104:0x016c  */
    /* JADX WARNING: Removed duplicated region for block: B:112:0x01d7 A[SYNTHETIC, Splitter:B:112:0x01d7] */
    /* JADX WARNING: Removed duplicated region for block: B:86:0x0125  */
    /* JADX WARNING: Removed duplicated region for block: B:93:0x0141  */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:122:0x01fa=Splitter:B:122:0x01fa, B:64:0x00de=Splitter:B:64:0x00de} */
    @SuppressLint({"DrawAllocation"})
    public final synchronized void onMeasure(int i, int i2) {
        int i3;
        boolean z;
        boolean z2;
        int i4;
        if (isDestroyed()) {
            setMeasuredDimension(0, 0);
            return;
        }
        if (!isInEditMode() && !this.zzezp) {
            if (!this.zzezn.zzafc()) {
                if (this.zzezn.zzafe()) {
                    super.onMeasure(i, i2);
                    return;
                } else if (this.zzezn.zzafd()) {
                    if (((Boolean) zzwu.zzpz().zzd(zzaan.zzcum)).booleanValue()) {
                        super.onMeasure(i, i2);
                        return;
                    }
                    zzbgw zzabu = zzabu();
                    float aspectRatio = zzabu != null ? zzabu.getAspectRatio() : 0.0f;
                    if (aspectRatio == 0.0f) {
                        super.onMeasure(i, i2);
                        return;
                    }
                    int size = MeasureSpec.getSize(i);
                    int size2 = MeasureSpec.getSize(i2);
                    int i5 = (int) (((float) size2) * aspectRatio);
                    int i6 = (int) (((float) size) / aspectRatio);
                    if (size2 == 0 && i6 != 0) {
                        i5 = (int) (((float) i6) * aspectRatio);
                        size2 = i6;
                    } else if (size == 0 && i5 != 0) {
                        i6 = (int) (((float) i5) / aspectRatio);
                        size = i5;
                    }
                    setMeasuredDimension(Math.min(i5, size), Math.min(i6, size2));
                    return;
                } else if (this.zzezn.isFluid()) {
                    if (!((Boolean) zzwu.zzpz().zzd(zzaan.zzcur)).booleanValue()) {
                        if (PlatformVersion.isAtLeastJellyBeanMR1()) {
                            zza("/contentHeight", (zzu<? super zzbgg>) new zzbgu<Object>(this));
                            zzfe("(function() {  var height = -1;  if (document.body) {    height = document.body.offsetHeight;  } else if (document.documentElement) {    height = document.documentElement.offsetHeight;  }  var url = 'gmsg://mobileads.google.com/contentHeight?';  url += 'height=' + height;  try {    window.googleAdsJsInterface.notify(url);  } catch (e) {    var frame = document.getElementById('afma-notify-fluid');    if (!frame) {      frame = document.createElement('IFRAME');      frame.id = 'afma-notify-fluid';      frame.style.display = 'none';      var body = document.body || document.documentElement;      body.appendChild(frame);    }    frame.src = url;  }})();");
                            float f = this.zzbwk.density;
                            int size3 = MeasureSpec.getSize(i);
                            if (this.zzezz != -1) {
                                i4 = (int) (((float) this.zzezz) * f);
                            } else {
                                i4 = MeasureSpec.getSize(i2);
                            }
                            setMeasuredDimension(size3, i4);
                            return;
                        }
                    }
                    super.onMeasure(i, i2);
                    return;
                } else if (this.zzezn.zzafb()) {
                    setMeasuredDimension(this.zzbwk.widthPixels, this.zzbwk.heightPixels);
                    return;
                } else {
                    int mode = MeasureSpec.getMode(i);
                    int size4 = MeasureSpec.getSize(i);
                    int mode2 = MeasureSpec.getMode(i2);
                    int size5 = MeasureSpec.getSize(i2);
                    int i7 = Integer.MAX_VALUE;
                    if (mode != Integer.MIN_VALUE) {
                        if (mode != 1073741824) {
                            i3 = Integer.MAX_VALUE;
                            if (mode2 == Integer.MIN_VALUE || mode2 == 1073741824) {
                                i7 = size5;
                            }
                            if (this.zzezn.widthPixels <= i3) {
                                if (this.zzezn.heightPixels <= i7) {
                                    z = false;
                                    if (((Boolean) zzwu.zzpz().zzd(zzaan.zzcxk)).booleanValue()) {
                                        z2 = ((float) this.zzezn.widthPixels) / this.zzezh <= ((float) i3) / this.zzezh && ((float) this.zzezn.heightPixels) / this.zzezh <= ((float) i7) / this.zzezh;
                                        if (z) {
                                            if (!z2) {
                                                int i8 = (int) (((float) this.zzezn.widthPixels) / this.zzezh);
                                                int i9 = (int) (((float) this.zzezn.heightPixels) / this.zzezh);
                                                int i10 = (int) (((float) size4) / this.zzezh);
                                                int i11 = (int) (((float) size5) / this.zzezh);
                                                StringBuilder sb = new StringBuilder(103);
                                                sb.append("Not enough space to show ad. Needs ");
                                                sb.append(i8);
                                                sb.append(AvidJSONUtil.KEY_X);
                                                sb.append(i9);
                                                sb.append(" dp, but only has ");
                                                sb.append(i10);
                                                sb.append(AvidJSONUtil.KEY_X);
                                                sb.append(i11);
                                                sb.append(" dp.");
                                                zzaxz.zzeo(sb.toString());
                                                if (getVisibility() != 8) {
                                                    setVisibility(4);
                                                }
                                                setMeasuredDimension(0, 0);
                                                if (!this.zzezi) {
                                                    this.zzdvr.zza(zzb.BANNER_SIZE_INVALID);
                                                    this.zzezi = true;
                                                    return;
                                                }
                                            } else {
                                                if (getVisibility() != 8) {
                                                    setVisibility(0);
                                                }
                                                if (!this.zzezj) {
                                                    this.zzdvr.zza(zzb.BANNER_SIZE_VALID);
                                                    this.zzezj = true;
                                                }
                                                setMeasuredDimension(this.zzezn.widthPixels, this.zzezn.heightPixels);
                                            }
                                        }
                                    }
                                    z2 = z;
                                    if (!z2) {
                                    }
                                }
                            }
                            z = true;
                            if (((Boolean) zzwu.zzpz().zzd(zzaan.zzcxk)).booleanValue()) {
                            }
                            z2 = z;
                            if (!z2) {
                            }
                        }
                    }
                    i3 = size4;
                    i7 = size5;
                    if (this.zzezn.widthPixels <= i3) {
                    }
                    z = true;
                    if (((Boolean) zzwu.zzpz().zzd(zzaan.zzcxk)).booleanValue()) {
                    }
                    z2 = z;
                    if (!z2) {
                    }
                }
            }
        }
        super.onMeasure(i, i2);
    }

    public final void onGlobalLayout() {
        boolean zzaeo = zzaeo();
        zzd zzadh = zzadh();
        if (zzadh != null && zzaeo) {
            zzadh.zzvu();
        }
    }

    public final synchronized void zza(zzd zzd) {
        this.zzezl = zzd;
    }

    public final synchronized void zzaa(IObjectWrapper iObjectWrapper) {
        this.zzezm = iObjectWrapper;
    }

    public final synchronized void zzb(zzd zzd) {
        this.zzfae = zzd;
    }

    public final synchronized void zza(zzbht zzbht) {
        this.zzezn = zzbht;
        requestLayout();
    }

    public final synchronized void zzav(boolean z) {
        boolean z2 = z != this.zzezp;
        this.zzezp = z;
        zzaer();
        if (z2) {
            new zzaok(this).zzdc(z ? "expanded" : "default");
        }
    }

    public final void zzadv() {
        this.zzbuf.zzaam();
    }

    /* access modifiers changed from: protected */
    public final synchronized void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (!isDestroyed()) {
            this.zzbuf.onAttachedToWindow();
        }
        boolean z = this.zzezv;
        if (this.zzezk != null && this.zzezk.zzaeb()) {
            if (!this.zzezw) {
                OnGlobalLayoutListener zzaec = this.zzezk.zzaec();
                if (zzaec != null) {
                    zzbv.zzme();
                    zzbct.zza((View) this, zzaec);
                }
                OnScrollChangedListener zzaed = this.zzezk.zzaed();
                if (zzaed != null) {
                    zzbv.zzme();
                    zzbct.zza((View) this, zzaed);
                }
                this.zzezw = true;
            }
            zzaeo();
            z = true;
        }
        zzaz(z);
    }

    /* access modifiers changed from: protected */
    public final void onDetachedFromWindow() {
        synchronized (this) {
            if (!isDestroyed()) {
                this.zzbuf.onDetachedFromWindow();
            }
            super.onDetachedFromWindow();
            if (this.zzezw && this.zzezk != null && this.zzezk.zzaeb() && getViewTreeObserver() != null && getViewTreeObserver().isAlive()) {
                OnGlobalLayoutListener zzaec = this.zzezk.zzaec();
                if (zzaec != null) {
                    zzbv.zzlh().zza(getViewTreeObserver(), zzaec);
                }
                OnScrollChangedListener zzaed = this.zzezk.zzaed();
                if (zzaed != null) {
                    getViewTreeObserver().removeOnScrollChangedListener(zzaed);
                }
                this.zzezw = false;
            }
        }
        zzaz(false);
    }

    public final void zzbo(Context context) {
        this.zzezf.setBaseContext(context);
        this.zzbuf.zzj(this.zzezf.zzabw());
    }

    public final synchronized void zzaf(boolean z) {
        if (this.zzezl != null) {
            this.zzezl.zza(this.zzezk.zzmu(), z);
        } else {
            this.zzezo = z;
        }
    }

    public final synchronized int getRequestedOrientation() {
        return this.zzezr;
    }

    public final synchronized void setRequestedOrientation(int i) {
        this.zzezr = i;
        if (this.zzezl != null) {
            this.zzezl.setRequestedOrientation(this.zzezr);
        }
    }

    public final Activity zzabw() {
        return this.zzezf.zzabw();
    }

    public final Context zzadg() {
        return this.zzezf.zzadg();
    }

    private final synchronized void zzaer() {
        if (!this.zzezp) {
            if (!this.zzezn.zzafb()) {
                if (VERSION.SDK_INT < 18) {
                    zzaxz.zzdn("Disabling hardware acceleration on an AdView.");
                    zzaes();
                    return;
                }
                zzaxz.zzdn("Enabling hardware acceleration on an AdView.");
                zzaet();
                return;
            }
        }
        zzaxz.zzdn("Enabling hardware acceleration on an overlay.");
        zzaet();
    }

    private final synchronized void zzaes() {
        if (!this.zzezq) {
            zzbv.zzlh().zzaa(this);
        }
        this.zzezq = true;
    }

    private final synchronized void zzaet() {
        if (this.zzezq) {
            zzbv.zzlh().zzz(this);
        }
        this.zzezq = false;
    }

    public final synchronized void destroy() {
        zzaev();
        this.zzbuf.zzaan();
        if (this.zzezl != null) {
            this.zzezl.close();
            this.zzezl.onDestroy();
            this.zzezl = null;
        }
        this.zzezm = null;
        this.zzezk.reset();
        if (!this.mIsDestroyed) {
            zzbv.zzmd();
            zzbfj.zzc(this);
            zzaeu();
            this.mIsDestroyed = true;
            zzaxz.v("Initiating WebView self destruct sequence in 3...");
            zzaxz.v("Loading blank page in WebView, 2...");
            zzfd("about:blank");
        }
    }

    /* access modifiers changed from: protected */
    public final void finalize() throws Throwable {
        try {
            synchronized (this) {
                if (!this.mIsDestroyed) {
                    this.zzezk.reset();
                    zzbv.zzmd();
                    zzbfj.zzc(this);
                    zzaeu();
                    zzyo();
                }
            }
            super.finalize();
        } catch (Throwable th) {
            super.finalize();
            throw th;
        }
    }

    public final synchronized void zzadr() {
        zzaxz.v("Destroying WebView!");
        zzyo();
        zzayh.zzelc.post(new zzbgv(this));
    }

    private final synchronized void zzyo() {
        if (!this.zzfaf) {
            this.zzfaf = true;
            zzbv.zzlj().zzyo();
        }
    }

    public final synchronized boolean isDestroyed() {
        return this.mIsDestroyed;
    }

    /* access modifiers changed from: protected */
    @TargetApi(21)
    public final void onDraw(Canvas canvas) {
        if (!isDestroyed()) {
            if (VERSION.SDK_INT != 21 || !canvas.isHardwareAccelerated() || isAttachedToWindow()) {
                super.onDraw(canvas);
                if (!(this.zzezk == null || this.zzezk.zzael() == null)) {
                    this.zzezk.zzael().zzjw();
                }
            }
        }
    }

    public final void zzadw() {
        if (this.zzfab == null) {
            this.zzfab = zzaat.zzb(this.zzfac.zzrf());
            this.zzfac.zza("native:view_load", this.zzfab);
        }
    }

    public final void onPause() {
        if (!isDestroyed()) {
            try {
                super.onPause();
            } catch (Exception e) {
                zzaxz.zzb("Could not pause webview.", e);
            }
        }
    }

    public final void onResume() {
        if (!isDestroyed()) {
            try {
                super.onResume();
            } catch (Exception e) {
                zzaxz.zzb("Could not resume webview.", e);
            }
        }
    }

    public final void zzadz() {
        zzaxz.v("Cannot add text view to inner AdWebView");
    }

    public final void zzay(boolean z) {
        this.zzezk.zzay(z);
    }

    public final void stopLoading() {
        if (!isDestroyed()) {
            try {
                super.stopLoading();
            } catch (Exception e) {
                zzaxz.zzb("Could not stop loading webview.", e);
            }
        }
    }

    public final synchronized void zzaw(boolean z) {
        this.zzezs = z;
    }

    public final synchronized boolean zzads() {
        return this.zzezs;
    }

    public final synchronized boolean zzadt() {
        return this.zzezt;
    }

    public final synchronized void zzjf() {
        this.zzezt = true;
        if (this.zzezg != null) {
            this.zzezg.zzjf();
        }
    }

    public final synchronized void zzjg() {
        this.zzezt = false;
        if (this.zzezg != null) {
            this.zzezg.zzjg();
        }
    }

    private final synchronized void zzaeu() {
        if (this.zzfag != null) {
            for (zzbfk release : this.zzfag.values()) {
                release.release();
            }
        }
        this.zzfag = null;
    }

    public final synchronized void zza(String str, zzbfk zzbfk) {
        if (this.zzfag == null) {
            this.zzfag = new HashMap();
        }
        this.zzfag.put(str, zzbfk);
    }

    public final synchronized zzbfk zzet(String str) {
        if (this.zzfag == null) {
            return null;
        }
        return (zzbfk) this.zzfag.get(str);
    }

    public final synchronized void zzfb(String str) {
        if (str == null) {
            str = "";
        }
        this.zzetu = str;
    }

    public final synchronized String zzabx() {
        return this.zzetu;
    }

    public final synchronized void zzacc() {
    }

    public final zzaay zzabv() {
        return this.zzetv;
    }

    public final zzaaz zzaby() {
        return this.zzfac;
    }

    public final void setOnClickListener(OnClickListener onClickListener) {
        this.zzfad = new WeakReference<>(onClickListener);
        super.setOnClickListener(onClickListener);
    }

    public final OnClickListener getOnClickListener() {
        return (OnClickListener) this.zzfad.get();
    }

    public final synchronized void zzb(zzacb zzacb) {
        this.zzezx = zzacb;
    }

    public final synchronized zzacb zzadx() {
        return this.zzezx;
    }

    public final synchronized zzbgw zzabu() {
        return this.zzezu;
    }

    public final synchronized void zza(zzbgw zzbgw) {
        if (this.zzezu != null) {
            zzaxz.e("Attempt to create multiple AdWebViewVideoControllers.");
        } else {
            this.zzezu = zzbgw;
        }
    }

    public final synchronized boolean zzadu() {
        return this.zzezy > 0;
    }

    public final synchronized void zzax(boolean z) {
        this.zzezy += z ? 1 : -1;
        if (this.zzezy <= 0 && this.zzezl != null) {
            this.zzezl.zzvx();
        }
    }

    private final void zzaev() {
        if (this.zzfac != null) {
            zzaba zzrf = this.zzfac.zzrf();
            if (!(zzrf == null || zzbv.zzlj().zzyh() == null)) {
                zzbv.zzlj().zzyh().zza(zzrf);
            }
        }
    }

    public final void zzady() {
        setBackgroundColor(0);
    }

    public final void zzat(boolean z) {
        this.zzezk.zzat(z);
    }

    public final void zzvw() {
        zzd zzadh = zzadh();
        if (zzadh != null) {
            zzadh.zzvw();
        }
    }

    public final int zzaca() {
        return getMeasuredHeight();
    }

    public final int zzacb() {
        return getMeasuredWidth();
    }

    public final void zza(zzc zzc) {
        this.zzezk.zza(zzc);
    }

    public final void zzb(boolean z, int i) {
        this.zzezk.zzb(z, i);
    }

    public final void zza(boolean z, int i, String str) {
        this.zzezk.zza(z, i, str);
    }

    public final void zza(boolean z, int i, String str, String str2) {
        this.zzezk.zza(z, i, str, str2);
    }

    public final void zza(zzsf zzsf) {
        synchronized (this) {
            this.zzezv = zzsf.zzuc;
        }
        zzaz(zzsf.zzuc);
    }

    private final void zzaz(boolean z) {
        HashMap hashMap = new HashMap();
        hashMap.put(ISNAdViewConstants.IS_VISIBLE_KEY, z ? "1" : "0");
        zza("onAdVisibilityChanged", (Map<String, ?>) hashMap);
    }

    public final void zza(String str, zzu<? super zzbgg> zzu) {
        if (this.zzezk != null) {
            this.zzezk.zza(str, zzu);
        }
    }

    public final void zzb(String str, zzu<? super zzbgg> zzu) {
        if (this.zzezk != null) {
            this.zzezk.zzb(str, zzu);
        }
    }

    public final void zza(String str, Predicate<zzu<? super zzbgg>> predicate) {
        if (this.zzezk != null) {
            this.zzezk.zza(str, predicate);
        }
    }

    public final /* synthetic */ zzbhn zzadl() {
        return this.zzezk;
    }
}
