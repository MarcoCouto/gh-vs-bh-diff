package com.google.android.gms.internal.ads;

import android.support.v7.widget.helper.ItemTouchHelper.Callback;
import com.facebook.internal.FacebookRequestErrorClassification;
import com.google.android.gms.dynamite.descriptors.com.google.android.gms.ads.dynamite.ModuleDescriptor;
import com.ironsource.mediationsdk.logger.IronSourceError;
import java.io.IOException;

public final class zzbl extends zzbut<zzbl> {
    public String zzdp;
    public String zzdq;
    public String zzds;
    public String zzdt;
    public String zzdu;
    public String zzdw;
    public Long zzdx;
    private Long zzdy;
    public Long zzdz;
    public Long zzea;
    private Long zzeb;
    private Long zzec;
    private Long zzed;
    private Long zzee;
    private Long zzef;
    public Long zzeg;
    private String zzeh;
    public Long zzei;
    public Long zzej;
    public Long zzek;
    public Long zzel;
    private Long zzem;
    private Long zzen;
    public Long zzeo;
    public Long zzep;
    public Long zzeq;
    public String zzer;
    public Long zzes;
    public Long zzet;
    public Long zzeu;
    public Long zzev;
    public Long zzew;
    public Long zzex;
    private zzbo zzey;
    public Long zzez;
    public Long zzfa;
    public Long zzfb;
    public Long zzfc;
    public Long zzfd;
    public Long zzfe;
    public Integer zzff;
    public Integer zzfg;
    public Long zzfh;
    public Long zzfi;
    public Long zzfj;
    private Long zzfk;
    private Long zzfl;
    public Integer zzfm;
    public zzbm zzfn;
    public zzbm[] zzfo;
    public zzbn zzfp;
    private Long zzfq;
    public Long zzfr;
    public Long zzfs;
    public Long zzft;
    public Long zzfu;
    public Long zzfv;
    public String zzfw;
    private Long zzfx;
    private Integer zzfy;
    private Integer zzfz;
    private Integer zzga;
    public zzbr zzgb;
    private Long zzgc;
    private Long zzgd;
    public String zzge;
    public Integer zzgf;
    public Boolean zzgg;
    private String zzgh;
    public Long zzgi;
    public zzbq zzgj;

    public zzbl() {
        this.zzdw = null;
        this.zzdp = null;
        this.zzdx = null;
        this.zzdy = null;
        this.zzdz = null;
        this.zzea = null;
        this.zzeb = null;
        this.zzec = null;
        this.zzed = null;
        this.zzee = null;
        this.zzef = null;
        this.zzeg = null;
        this.zzeh = null;
        this.zzei = null;
        this.zzej = null;
        this.zzek = null;
        this.zzel = null;
        this.zzem = null;
        this.zzen = null;
        this.zzeo = null;
        this.zzep = null;
        this.zzeq = null;
        this.zzdq = null;
        this.zzer = null;
        this.zzes = null;
        this.zzet = null;
        this.zzeu = null;
        this.zzds = null;
        this.zzev = null;
        this.zzew = null;
        this.zzex = null;
        this.zzey = null;
        this.zzez = null;
        this.zzfa = null;
        this.zzfb = null;
        this.zzfc = null;
        this.zzfd = null;
        this.zzfe = null;
        this.zzdt = null;
        this.zzdu = null;
        this.zzfh = null;
        this.zzfi = null;
        this.zzfj = null;
        this.zzfk = null;
        this.zzfl = null;
        this.zzfn = null;
        this.zzfo = zzbm.zzu();
        this.zzfp = null;
        this.zzfq = null;
        this.zzfr = null;
        this.zzfs = null;
        this.zzft = null;
        this.zzfu = null;
        this.zzfv = null;
        this.zzfw = null;
        this.zzfx = null;
        this.zzgb = null;
        this.zzgc = null;
        this.zzgd = null;
        this.zzge = null;
        this.zzgg = null;
        this.zzgh = null;
        this.zzgi = null;
        this.zzgj = null;
        this.zzfwt = -1;
    }

    public final void zza(zzbur zzbur) throws IOException {
        if (this.zzdw != null) {
            zzbur.zzf(1, this.zzdw);
        }
        if (this.zzdp != null) {
            zzbur.zzf(2, this.zzdp);
        }
        if (this.zzdx != null) {
            zzbur.zzr(3, this.zzdx.longValue());
        }
        if (this.zzdy != null) {
            zzbur.zzr(4, this.zzdy.longValue());
        }
        if (this.zzdz != null) {
            zzbur.zzr(5, this.zzdz.longValue());
        }
        if (this.zzea != null) {
            zzbur.zzr(6, this.zzea.longValue());
        }
        if (this.zzeb != null) {
            zzbur.zzr(7, this.zzeb.longValue());
        }
        if (this.zzec != null) {
            zzbur.zzr(8, this.zzec.longValue());
        }
        if (this.zzed != null) {
            zzbur.zzr(9, this.zzed.longValue());
        }
        if (this.zzee != null) {
            zzbur.zzr(10, this.zzee.longValue());
        }
        if (this.zzef != null) {
            zzbur.zzr(11, this.zzef.longValue());
        }
        if (this.zzeg != null) {
            zzbur.zzr(12, this.zzeg.longValue());
        }
        if (this.zzeh != null) {
            zzbur.zzf(13, this.zzeh);
        }
        if (this.zzei != null) {
            zzbur.zzr(14, this.zzei.longValue());
        }
        if (this.zzej != null) {
            zzbur.zzr(15, this.zzej.longValue());
        }
        if (this.zzek != null) {
            zzbur.zzr(16, this.zzek.longValue());
        }
        if (this.zzel != null) {
            zzbur.zzr(17, this.zzel.longValue());
        }
        if (this.zzem != null) {
            zzbur.zzr(18, this.zzem.longValue());
        }
        if (this.zzen != null) {
            zzbur.zzr(19, this.zzen.longValue());
        }
        if (this.zzeo != null) {
            zzbur.zzr(20, this.zzeo.longValue());
        }
        if (this.zzgd != null) {
            zzbur.zzr(21, this.zzgd.longValue());
        }
        if (this.zzep != null) {
            zzbur.zzr(22, this.zzep.longValue());
        }
        if (this.zzeq != null) {
            zzbur.zzr(23, this.zzeq.longValue());
        }
        if (this.zzge != null) {
            zzbur.zzf(24, this.zzge);
        }
        if (this.zzgi != null) {
            zzbur.zzr(25, this.zzgi.longValue());
        }
        if (this.zzgf != null) {
            zzbur.zzv(26, this.zzgf.intValue());
        }
        if (this.zzdq != null) {
            zzbur.zzf(27, this.zzdq);
        }
        if (this.zzgg != null) {
            zzbur.zzj(28, this.zzgg.booleanValue());
        }
        if (this.zzer != null) {
            zzbur.zzf(29, this.zzer);
        }
        if (this.zzgh != null) {
            zzbur.zzf(30, this.zzgh);
        }
        if (this.zzes != null) {
            zzbur.zzr(31, this.zzes.longValue());
        }
        if (this.zzet != null) {
            zzbur.zzr(32, this.zzet.longValue());
        }
        if (this.zzeu != null) {
            zzbur.zzr(33, this.zzeu.longValue());
        }
        if (this.zzds != null) {
            zzbur.zzf(34, this.zzds);
        }
        if (this.zzev != null) {
            zzbur.zzr(35, this.zzev.longValue());
        }
        if (this.zzew != null) {
            zzbur.zzr(36, this.zzew.longValue());
        }
        if (this.zzex != null) {
            zzbur.zzr(37, this.zzex.longValue());
        }
        if (this.zzey != null) {
            zzbur.zza(38, (zzbuz) this.zzey);
        }
        if (this.zzez != null) {
            zzbur.zzr(39, this.zzez.longValue());
        }
        if (this.zzfa != null) {
            zzbur.zzr(40, this.zzfa.longValue());
        }
        if (this.zzfb != null) {
            zzbur.zzr(41, this.zzfb.longValue());
        }
        if (this.zzfc != null) {
            zzbur.zzr(42, this.zzfc.longValue());
        }
        if (this.zzfo != null && this.zzfo.length > 0) {
            for (zzbm zzbm : this.zzfo) {
                if (zzbm != null) {
                    zzbur.zza(43, (zzbuz) zzbm);
                }
            }
        }
        if (this.zzfd != null) {
            zzbur.zzr(44, this.zzfd.longValue());
        }
        if (this.zzfe != null) {
            zzbur.zzr(45, this.zzfe.longValue());
        }
        if (this.zzdt != null) {
            zzbur.zzf(46, this.zzdt);
        }
        if (this.zzdu != null) {
            zzbur.zzf(47, this.zzdu);
        }
        if (this.zzff != null) {
            zzbur.zzv(48, this.zzff.intValue());
        }
        if (this.zzfg != null) {
            zzbur.zzv(49, this.zzfg.intValue());
        }
        if (this.zzfn != null) {
            zzbur.zza(50, (zzbuz) this.zzfn);
        }
        if (this.zzfh != null) {
            zzbur.zzr(51, this.zzfh.longValue());
        }
        if (this.zzfi != null) {
            zzbur.zzr(52, this.zzfi.longValue());
        }
        if (this.zzfj != null) {
            zzbur.zzr(53, this.zzfj.longValue());
        }
        if (this.zzfk != null) {
            zzbur.zzr(54, this.zzfk.longValue());
        }
        if (this.zzfl != null) {
            zzbur.zzr(55, this.zzfl.longValue());
        }
        if (this.zzfm != null) {
            zzbur.zzv(56, this.zzfm.intValue());
        }
        if (this.zzfp != null) {
            zzbur.zza(57, (zzbuz) this.zzfp);
        }
        if (this.zzfq != null) {
            zzbur.zzr(58, this.zzfq.longValue());
        }
        if (this.zzfr != null) {
            zzbur.zzr(59, this.zzfr.longValue());
        }
        if (this.zzfs != null) {
            zzbur.zzr(60, this.zzfs.longValue());
        }
        if (this.zzft != null) {
            zzbur.zzr(61, this.zzft.longValue());
        }
        if (this.zzfu != null) {
            zzbur.zzr(62, this.zzfu.longValue());
        }
        if (this.zzfv != null) {
            zzbur.zzr(63, this.zzfv.longValue());
        }
        if (this.zzfx != null) {
            zzbur.zzr(64, this.zzfx.longValue());
        }
        if (this.zzfy != null) {
            zzbur.zzv(65, this.zzfy.intValue());
        }
        if (this.zzfz != null) {
            zzbur.zzv(66, this.zzfz.intValue());
        }
        if (this.zzfw != null) {
            zzbur.zzf(67, this.zzfw);
        }
        if (this.zzga != null) {
            zzbur.zzv(68, this.zzga.intValue());
        }
        if (this.zzgb != null) {
            zzbur.zza(69, (zzbuz) this.zzgb);
        }
        if (this.zzgc != null) {
            zzbur.zzr(70, this.zzgc.longValue());
        }
        if (this.zzgj != null) {
            zzbur.zza(201, (zzbuz) this.zzgj);
        }
        super.zza(zzbur);
    }

    /* access modifiers changed from: protected */
    public final int zzt() {
        int zzt = super.zzt();
        if (this.zzdw != null) {
            zzt += zzbur.zzg(1, this.zzdw);
        }
        if (this.zzdp != null) {
            zzt += zzbur.zzg(2, this.zzdp);
        }
        if (this.zzdx != null) {
            zzt += zzbur.zzm(3, this.zzdx.longValue());
        }
        if (this.zzdy != null) {
            zzt += zzbur.zzm(4, this.zzdy.longValue());
        }
        if (this.zzdz != null) {
            zzt += zzbur.zzm(5, this.zzdz.longValue());
        }
        if (this.zzea != null) {
            zzt += zzbur.zzm(6, this.zzea.longValue());
        }
        if (this.zzeb != null) {
            zzt += zzbur.zzm(7, this.zzeb.longValue());
        }
        if (this.zzec != null) {
            zzt += zzbur.zzm(8, this.zzec.longValue());
        }
        if (this.zzed != null) {
            zzt += zzbur.zzm(9, this.zzed.longValue());
        }
        if (this.zzee != null) {
            zzt += zzbur.zzm(10, this.zzee.longValue());
        }
        if (this.zzef != null) {
            zzt += zzbur.zzm(11, this.zzef.longValue());
        }
        if (this.zzeg != null) {
            zzt += zzbur.zzm(12, this.zzeg.longValue());
        }
        if (this.zzeh != null) {
            zzt += zzbur.zzg(13, this.zzeh);
        }
        if (this.zzei != null) {
            zzt += zzbur.zzm(14, this.zzei.longValue());
        }
        if (this.zzej != null) {
            zzt += zzbur.zzm(15, this.zzej.longValue());
        }
        if (this.zzek != null) {
            zzt += zzbur.zzm(16, this.zzek.longValue());
        }
        if (this.zzel != null) {
            zzt += zzbur.zzm(17, this.zzel.longValue());
        }
        if (this.zzem != null) {
            zzt += zzbur.zzm(18, this.zzem.longValue());
        }
        if (this.zzen != null) {
            zzt += zzbur.zzm(19, this.zzen.longValue());
        }
        if (this.zzeo != null) {
            zzt += zzbur.zzm(20, this.zzeo.longValue());
        }
        if (this.zzgd != null) {
            zzt += zzbur.zzm(21, this.zzgd.longValue());
        }
        if (this.zzep != null) {
            zzt += zzbur.zzm(22, this.zzep.longValue());
        }
        if (this.zzeq != null) {
            zzt += zzbur.zzm(23, this.zzeq.longValue());
        }
        if (this.zzge != null) {
            zzt += zzbur.zzg(24, this.zzge);
        }
        if (this.zzgi != null) {
            zzt += zzbur.zzm(25, this.zzgi.longValue());
        }
        if (this.zzgf != null) {
            zzt += zzbur.zzz(26, this.zzgf.intValue());
        }
        if (this.zzdq != null) {
            zzt += zzbur.zzg(27, this.zzdq);
        }
        if (this.zzgg != null) {
            this.zzgg.booleanValue();
            zzt += zzbur.zzfd(28) + 1;
        }
        if (this.zzer != null) {
            zzt += zzbur.zzg(29, this.zzer);
        }
        if (this.zzgh != null) {
            zzt += zzbur.zzg(30, this.zzgh);
        }
        if (this.zzes != null) {
            zzt += zzbur.zzm(31, this.zzes.longValue());
        }
        if (this.zzet != null) {
            zzt += zzbur.zzm(32, this.zzet.longValue());
        }
        if (this.zzeu != null) {
            zzt += zzbur.zzm(33, this.zzeu.longValue());
        }
        if (this.zzds != null) {
            zzt += zzbur.zzg(34, this.zzds);
        }
        if (this.zzev != null) {
            zzt += zzbur.zzm(35, this.zzev.longValue());
        }
        if (this.zzew != null) {
            zzt += zzbur.zzm(36, this.zzew.longValue());
        }
        if (this.zzex != null) {
            zzt += zzbur.zzm(37, this.zzex.longValue());
        }
        if (this.zzey != null) {
            zzt += zzbur.zzb(38, (zzbuz) this.zzey);
        }
        if (this.zzez != null) {
            zzt += zzbur.zzm(39, this.zzez.longValue());
        }
        if (this.zzfa != null) {
            zzt += zzbur.zzm(40, this.zzfa.longValue());
        }
        if (this.zzfb != null) {
            zzt += zzbur.zzm(41, this.zzfb.longValue());
        }
        if (this.zzfc != null) {
            zzt += zzbur.zzm(42, this.zzfc.longValue());
        }
        if (this.zzfo != null && this.zzfo.length > 0) {
            for (zzbm zzbm : this.zzfo) {
                if (zzbm != null) {
                    zzt += zzbur.zzb(43, (zzbuz) zzbm);
                }
            }
        }
        if (this.zzfd != null) {
            zzt += zzbur.zzm(44, this.zzfd.longValue());
        }
        if (this.zzfe != null) {
            zzt += zzbur.zzm(45, this.zzfe.longValue());
        }
        if (this.zzdt != null) {
            zzt += zzbur.zzg(46, this.zzdt);
        }
        if (this.zzdu != null) {
            zzt += zzbur.zzg(47, this.zzdu);
        }
        if (this.zzff != null) {
            zzt += zzbur.zzz(48, this.zzff.intValue());
        }
        if (this.zzfg != null) {
            zzt += zzbur.zzz(49, this.zzfg.intValue());
        }
        if (this.zzfn != null) {
            zzt += zzbur.zzb(50, (zzbuz) this.zzfn);
        }
        if (this.zzfh != null) {
            zzt += zzbur.zzm(51, this.zzfh.longValue());
        }
        if (this.zzfi != null) {
            zzt += zzbur.zzm(52, this.zzfi.longValue());
        }
        if (this.zzfj != null) {
            zzt += zzbur.zzm(53, this.zzfj.longValue());
        }
        if (this.zzfk != null) {
            zzt += zzbur.zzm(54, this.zzfk.longValue());
        }
        if (this.zzfl != null) {
            zzt += zzbur.zzm(55, this.zzfl.longValue());
        }
        if (this.zzfm != null) {
            zzt += zzbur.zzz(56, this.zzfm.intValue());
        }
        if (this.zzfp != null) {
            zzt += zzbur.zzb(57, (zzbuz) this.zzfp);
        }
        if (this.zzfq != null) {
            zzt += zzbur.zzm(58, this.zzfq.longValue());
        }
        if (this.zzfr != null) {
            zzt += zzbur.zzm(59, this.zzfr.longValue());
        }
        if (this.zzfs != null) {
            zzt += zzbur.zzm(60, this.zzfs.longValue());
        }
        if (this.zzft != null) {
            zzt += zzbur.zzm(61, this.zzft.longValue());
        }
        if (this.zzfu != null) {
            zzt += zzbur.zzm(62, this.zzfu.longValue());
        }
        if (this.zzfv != null) {
            zzt += zzbur.zzm(63, this.zzfv.longValue());
        }
        if (this.zzfx != null) {
            zzt += zzbur.zzm(64, this.zzfx.longValue());
        }
        if (this.zzfy != null) {
            zzt += zzbur.zzz(65, this.zzfy.intValue());
        }
        if (this.zzfz != null) {
            zzt += zzbur.zzz(66, this.zzfz.intValue());
        }
        if (this.zzfw != null) {
            zzt += zzbur.zzg(67, this.zzfw);
        }
        if (this.zzga != null) {
            zzt += zzbur.zzz(68, this.zzga.intValue());
        }
        if (this.zzgb != null) {
            zzt += zzbur.zzb(69, (zzbuz) this.zzgb);
        }
        if (this.zzgc != null) {
            zzt += zzbur.zzm(70, this.zzgc.longValue());
        }
        return this.zzgj != null ? zzt + zzbur.zzb(201, (zzbuz) this.zzgj) : zzt;
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0067, code lost:
        throw new java.lang.IllegalArgumentException(r5.toString());
     */
    /* renamed from: zzb */
    public final zzbl zza(zzbuq zzbuq) throws IOException {
        int zzalm;
        while (true) {
            int zzaku = zzbuq.zzaku();
            switch (zzaku) {
                case 0:
                    return this;
                case 10:
                    this.zzdw = zzbuq.readString();
                    break;
                case 18:
                    this.zzdp = zzbuq.readString();
                    break;
                case 24:
                    this.zzdx = Long.valueOf(zzbuq.zzaln());
                    break;
                case 32:
                    this.zzdy = Long.valueOf(zzbuq.zzaln());
                    break;
                case 40:
                    this.zzdz = Long.valueOf(zzbuq.zzaln());
                    break;
                case 48:
                    this.zzea = Long.valueOf(zzbuq.zzaln());
                    break;
                case 56:
                    this.zzeb = Long.valueOf(zzbuq.zzaln());
                    break;
                case 64:
                    this.zzec = Long.valueOf(zzbuq.zzaln());
                    break;
                case 72:
                    this.zzed = Long.valueOf(zzbuq.zzaln());
                    break;
                case 80:
                    this.zzee = Long.valueOf(zzbuq.zzaln());
                    break;
                case 88:
                    this.zzef = Long.valueOf(zzbuq.zzaln());
                    break;
                case 96:
                    this.zzeg = Long.valueOf(zzbuq.zzaln());
                    break;
                case 106:
                    this.zzeh = zzbuq.readString();
                    break;
                case 112:
                    this.zzei = Long.valueOf(zzbuq.zzaln());
                    break;
                case 120:
                    this.zzej = Long.valueOf(zzbuq.zzaln());
                    break;
                case 128:
                    this.zzek = Long.valueOf(zzbuq.zzaln());
                    break;
                case 136:
                    this.zzel = Long.valueOf(zzbuq.zzaln());
                    break;
                case 144:
                    this.zzem = Long.valueOf(zzbuq.zzaln());
                    break;
                case 152:
                    this.zzen = Long.valueOf(zzbuq.zzaln());
                    break;
                case 160:
                    this.zzeo = Long.valueOf(zzbuq.zzaln());
                    break;
                case 168:
                    this.zzgd = Long.valueOf(zzbuq.zzaln());
                    break;
                case 176:
                    this.zzep = Long.valueOf(zzbuq.zzaln());
                    break;
                case 184:
                    this.zzeq = Long.valueOf(zzbuq.zzaln());
                    break;
                case 194:
                    this.zzge = zzbuq.readString();
                    break;
                case Callback.DEFAULT_DRAG_ANIMATION_DURATION /*200*/:
                    this.zzgi = Long.valueOf(zzbuq.zzaln());
                    break;
                case 208:
                    try {
                        zzalm = zzbuq.zzalm();
                        if (zzalm >= 0 && zzalm <= 6) {
                            this.zzgf = Integer.valueOf(zzalm);
                            break;
                        } else {
                            StringBuilder sb = new StringBuilder(44);
                            sb.append(zzalm);
                            sb.append(" is not a valid enum DeviceIdType");
                            break;
                        }
                    } catch (IllegalArgumentException unused) {
                        zzbuq.zzgc(zzbuq.getPosition());
                        zza(zzbuq, zzaku);
                        break;
                    }
                case 218:
                    this.zzdq = zzbuq.readString();
                    break;
                case 224:
                    this.zzgg = Boolean.valueOf(zzbuq.zzala());
                    break;
                case 234:
                    this.zzer = zzbuq.readString();
                    break;
                case 242:
                    this.zzgh = zzbuq.readString();
                    break;
                case 248:
                    this.zzes = Long.valueOf(zzbuq.zzaln());
                    break;
                case 256:
                    this.zzet = Long.valueOf(zzbuq.zzaln());
                    break;
                case 264:
                    this.zzeu = Long.valueOf(zzbuq.zzaln());
                    break;
                case 274:
                    this.zzds = zzbuq.readString();
                    break;
                case 280:
                    this.zzev = Long.valueOf(zzbuq.zzaln());
                    break;
                case 288:
                    this.zzew = Long.valueOf(zzbuq.zzaln());
                    break;
                case 296:
                    this.zzex = Long.valueOf(zzbuq.zzaln());
                    break;
                case 306:
                    if (this.zzey == null) {
                        this.zzey = new zzbo();
                    }
                    zzbuq.zza((zzbuz) this.zzey);
                    break;
                case 312:
                    this.zzez = Long.valueOf(zzbuq.zzaln());
                    break;
                case ModuleDescriptor.MODULE_VERSION /*320*/:
                    this.zzfa = Long.valueOf(zzbuq.zzaln());
                    break;
                case 328:
                    this.zzfb = Long.valueOf(zzbuq.zzaln());
                    break;
                case 336:
                    this.zzfc = Long.valueOf(zzbuq.zzaln());
                    break;
                case 346:
                    int zzb = zzbvc.zzb(zzbuq, 346);
                    int length = this.zzfo == null ? 0 : this.zzfo.length;
                    zzbm[] zzbmArr = new zzbm[(zzb + length)];
                    if (length != 0) {
                        System.arraycopy(this.zzfo, 0, zzbmArr, 0, length);
                    }
                    while (length < zzbmArr.length - 1) {
                        zzbmArr[length] = new zzbm();
                        zzbuq.zza((zzbuz) zzbmArr[length]);
                        zzbuq.zzaku();
                        length++;
                    }
                    zzbmArr[length] = new zzbm();
                    zzbuq.zza((zzbuz) zzbmArr[length]);
                    this.zzfo = zzbmArr;
                    break;
                case 352:
                    this.zzfd = Long.valueOf(zzbuq.zzaln());
                    break;
                case 360:
                    this.zzfe = Long.valueOf(zzbuq.zzaln());
                    break;
                case 370:
                    this.zzdt = zzbuq.readString();
                    break;
                case 378:
                    this.zzdu = zzbuq.readString();
                    break;
                case 384:
                    int position = zzbuq.getPosition();
                    try {
                        this.zzff = Integer.valueOf(zzbk.zzd(zzbuq.zzalm()));
                        break;
                    } catch (IllegalArgumentException unused2) {
                        zzbuq.zzgc(position);
                        zza(zzbuq, zzaku);
                        break;
                    }
                case 392:
                    int position2 = zzbuq.getPosition();
                    try {
                        this.zzfg = Integer.valueOf(zzbk.zzd(zzbuq.zzalm()));
                        break;
                    } catch (IllegalArgumentException unused3) {
                        zzbuq.zzgc(position2);
                        zza(zzbuq, zzaku);
                        break;
                    }
                case 402:
                    if (this.zzfn == null) {
                        this.zzfn = new zzbm();
                    }
                    zzbuq.zza((zzbuz) this.zzfn);
                    break;
                case 408:
                    this.zzfh = Long.valueOf(zzbuq.zzaln());
                    break;
                case 416:
                    this.zzfi = Long.valueOf(zzbuq.zzaln());
                    break;
                case 424:
                    this.zzfj = Long.valueOf(zzbuq.zzaln());
                    break;
                case 432:
                    this.zzfk = Long.valueOf(zzbuq.zzaln());
                    break;
                case 440:
                    this.zzfl = Long.valueOf(zzbuq.zzaln());
                    break;
                case 448:
                    int position3 = zzbuq.getPosition();
                    try {
                        this.zzfm = Integer.valueOf(zzbk.zzd(zzbuq.zzalm()));
                        break;
                    } catch (IllegalArgumentException unused4) {
                        zzbuq.zzgc(position3);
                        zza(zzbuq, zzaku);
                        break;
                    }
                case FacebookRequestErrorClassification.ESC_APP_NOT_INSTALLED /*458*/:
                    if (this.zzfp == null) {
                        this.zzfp = new zzbn();
                    }
                    zzbuq.zza((zzbuz) this.zzfp);
                    break;
                case 464:
                    this.zzfq = Long.valueOf(zzbuq.zzaln());
                    break;
                case 472:
                    this.zzfr = Long.valueOf(zzbuq.zzaln());
                    break;
                case 480:
                    this.zzfs = Long.valueOf(zzbuq.zzaln());
                    break;
                case 488:
                    this.zzft = Long.valueOf(zzbuq.zzaln());
                    break;
                case 496:
                    this.zzfu = Long.valueOf(zzbuq.zzaln());
                    break;
                case 504:
                    this.zzfv = Long.valueOf(zzbuq.zzaln());
                    break;
                case 512:
                    this.zzfx = Long.valueOf(zzbuq.zzaln());
                    break;
                case IronSourceError.ERROR_NO_INTERNET_CONNECTION /*520*/:
                    int position4 = zzbuq.getPosition();
                    try {
                        this.zzfy = Integer.valueOf(zzbk.zzf(zzbuq.zzalm()));
                        break;
                    } catch (IllegalArgumentException unused5) {
                        zzbuq.zzgc(position4);
                        zza(zzbuq, zzaku);
                        break;
                    }
                case 528:
                    int position5 = zzbuq.getPosition();
                    try {
                        this.zzfz = Integer.valueOf(zzbk.zze(zzbuq.zzalm()));
                        break;
                    } catch (IllegalArgumentException unused6) {
                        zzbuq.zzgc(position5);
                        zza(zzbuq, zzaku);
                        break;
                    }
                case 538:
                    this.zzfw = zzbuq.readString();
                    break;
                case 544:
                    try {
                        int zzalm2 = zzbuq.zzalm();
                        if (zzalm2 >= 0 && zzalm2 <= 3) {
                            this.zzga = Integer.valueOf(zzalm2);
                            break;
                        } else {
                            StringBuilder sb2 = new StringBuilder(45);
                            sb2.append(zzalm2);
                            sb2.append(" is not a valid enum DebuggerState");
                            break;
                        }
                    } catch (IllegalArgumentException unused7) {
                        zzbuq.zzgc(zzbuq.getPosition());
                        zza(zzbuq, zzaku);
                        break;
                    }
                    break;
                case 554:
                    if (this.zzgb == null) {
                        this.zzgb = new zzbr();
                    }
                    zzbuq.zza((zzbuz) this.zzgb);
                    break;
                case 560:
                    this.zzgc = Long.valueOf(zzbuq.zzaln());
                    break;
                case 1610:
                    if (this.zzgj == null) {
                        this.zzgj = new zzbq();
                    }
                    zzbuq.zza((zzbuz) this.zzgj);
                    break;
                default:
                    if (super.zza(zzbuq, zzaku)) {
                        break;
                    } else {
                        return this;
                    }
            }
        }
        StringBuilder sb3 = new StringBuilder(44);
        sb3.append(zzalm);
        sb3.append(" is not a valid enum DeviceIdType");
        throw new IllegalArgumentException(sb3.toString());
    }
}
