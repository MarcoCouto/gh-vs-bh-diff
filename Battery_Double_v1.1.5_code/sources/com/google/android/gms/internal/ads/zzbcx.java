package com.google.android.gms.internal.ads;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.SurfaceTexture;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnBufferingUpdateListener;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnInfoListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.media.MediaPlayer.OnVideoSizeChangedListener;
import android.net.Uri;
import android.os.Build.VERSION;
import android.view.Surface;
import android.view.TextureView.SurfaceTextureListener;
import android.view.View.MeasureSpec;
import com.google.android.gms.ads.internal.zzbv;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@zzark
@TargetApi(14)
public final class zzbcx extends zzbdi implements OnBufferingUpdateListener, OnCompletionListener, OnErrorListener, OnInfoListener, OnPreparedListener, OnVideoSizeChangedListener, SurfaceTextureListener {
    private static final Map<Integer, String> zzeqe = new HashMap();
    private final zzbea zzeqf;
    private final boolean zzeqg;
    private int zzeqh = 0;
    private int zzeqi = 0;
    private MediaPlayer zzeqj;
    private Uri zzeqk;
    private int zzeql;
    private int zzeqm;
    private int zzeqn;
    private int zzeqo;
    private int zzeqp;
    private zzbdx zzeqq;
    private boolean zzeqr;
    private int zzeqs;
    /* access modifiers changed from: private */
    public zzbdh zzeqt;

    public zzbcx(Context context, boolean z, boolean z2, zzbdy zzbdy, zzbea zzbea) {
        super(context);
        setSurfaceTextureListener(this);
        this.zzeqf = zzbea;
        this.zzeqr = z;
        this.zzeqg = z2;
        this.zzeqf.zzb(this);
    }

    public final String zzaaz() {
        String str = "MediaPlayer";
        String valueOf = String.valueOf(this.zzeqr ? " spherical" : "");
        return valueOf.length() != 0 ? str.concat(valueOf) : new String(str);
    }

    public final void zza(zzbdh zzbdh) {
        this.zzeqt = zzbdh;
    }

    public final void setVideoPath(String str) {
        Uri parse = Uri.parse(str);
        zzty zzd = zzty.zzd(parse);
        if (zzd != null) {
            parse = Uri.parse(zzd.url);
        }
        this.zzeqk = parse;
        this.zzeqs = 0;
        zzaba();
        requestLayout();
        invalidate();
    }

    public final void stop() {
        zzaxz.v("AdMediaPlayerView stop");
        if (this.zzeqj != null) {
            this.zzeqj.stop();
            this.zzeqj.release();
            this.zzeqj = null;
            zzcx(0);
            this.zzeqi = 0;
        }
        this.zzeqf.onStop();
    }

    public final void onVideoSizeChanged(MediaPlayer mediaPlayer, int i, int i2) {
        StringBuilder sb = new StringBuilder(57);
        sb.append("AdMediaPlayerView size changed: ");
        sb.append(i);
        sb.append(" x ");
        sb.append(i2);
        zzaxz.v(sb.toString());
        this.zzeql = mediaPlayer.getVideoWidth();
        this.zzeqm = mediaPlayer.getVideoHeight();
        if (this.zzeql != 0 && this.zzeqm != 0) {
            requestLayout();
        }
    }

    public final void onPrepared(MediaPlayer mediaPlayer) {
        zzaxz.v("AdMediaPlayerView prepared");
        zzcx(2);
        this.zzeqf.zzcg();
        zzayh.zzelc.post(new zzbcz(this));
        this.zzeql = mediaPlayer.getVideoWidth();
        this.zzeqm = mediaPlayer.getVideoHeight();
        if (this.zzeqs != 0) {
            seekTo(this.zzeqs);
        }
        zzabb();
        int i = this.zzeql;
        int i2 = this.zzeqm;
        StringBuilder sb = new StringBuilder(62);
        sb.append("AdMediaPlayerView stream dimensions: ");
        sb.append(i);
        sb.append(" x ");
        sb.append(i2);
        zzaxz.zzen(sb.toString());
        if (this.zzeqi == 3) {
            play();
        }
        zzabd();
    }

    public final void onCompletion(MediaPlayer mediaPlayer) {
        zzaxz.v("AdMediaPlayerView completion");
        zzcx(5);
        this.zzeqi = 5;
        zzayh.zzelc.post(new zzbda(this));
    }

    public final boolean onInfo(MediaPlayer mediaPlayer, int i, int i2) {
        String str = (String) zzeqe.get(Integer.valueOf(i));
        String str2 = (String) zzeqe.get(Integer.valueOf(i2));
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 37 + String.valueOf(str2).length());
        sb.append("AdMediaPlayerView MediaPlayer info: ");
        sb.append(str);
        sb.append(":");
        sb.append(str2);
        zzaxz.v(sb.toString());
        return true;
    }

    public final boolean onError(MediaPlayer mediaPlayer, int i, int i2) {
        String str = (String) zzeqe.get(Integer.valueOf(i));
        String str2 = (String) zzeqe.get(Integer.valueOf(i2));
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 38 + String.valueOf(str2).length());
        sb.append("AdMediaPlayerView MediaPlayer error: ");
        sb.append(str);
        sb.append(":");
        sb.append(str2);
        zzaxz.zzeo(sb.toString());
        zzcx(-1);
        this.zzeqi = -1;
        zzayh.zzelc.post(new zzbdb(this, str, str2));
        return true;
    }

    public final void onBufferingUpdate(MediaPlayer mediaPlayer, int i) {
        this.zzeqn = i;
    }

    public final void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int i, int i2) {
        zzaxz.v("AdMediaPlayerView surface created");
        zzaba();
        zzayh.zzelc.post(new zzbdc(this));
    }

    public final void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int i, int i2) {
        zzaxz.v("AdMediaPlayerView surface changed");
        boolean z = false;
        boolean z2 = this.zzeqi == 3;
        if (this.zzeql == i && this.zzeqm == i2) {
            z = true;
        }
        if (this.zzeqj != null && z2 && z) {
            if (this.zzeqs != 0) {
                seekTo(this.zzeqs);
            }
            play();
        }
        if (this.zzeqq != null) {
            this.zzeqq.zzo(i, i2);
        }
        zzayh.zzelc.post(new zzbdd(this, i, i2));
    }

    public final boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {
        zzaxz.v("AdMediaPlayerView surface destroyed");
        if (this.zzeqj != null && this.zzeqs == 0) {
            this.zzeqs = this.zzeqj.getCurrentPosition();
        }
        if (this.zzeqq != null) {
            this.zzeqq.zzabq();
        }
        zzayh.zzelc.post(new zzbde(this));
        zzar(true);
        return true;
    }

    public final void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {
        this.zzeqf.zzc(this);
        this.zzera.zza(surfaceTexture, this.zzeqt);
    }

    /* access modifiers changed from: protected */
    public final void onWindowVisibilityChanged(int i) {
        StringBuilder sb = new StringBuilder(58);
        sb.append("AdMediaPlayerView window visibility changed to ");
        sb.append(i);
        zzaxz.v(sb.toString());
        zzayh.zzelc.post(new zzbcy(this, i));
        super.onWindowVisibilityChanged(i);
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0072, code lost:
        if (r1 > r6) goto L_0x0098;
     */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x009f  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x00aa  */
    /* JADX WARNING: Removed duplicated region for block: B:52:? A[RETURN, SYNTHETIC] */
    public final void onMeasure(int i, int i2) {
        int i3;
        int i4;
        int i5;
        int defaultSize = getDefaultSize(this.zzeql, i);
        int defaultSize2 = getDefaultSize(this.zzeqm, i2);
        if (this.zzeql <= 0 || this.zzeqm <= 0 || this.zzeqq != null) {
            i4 = defaultSize;
        } else {
            int mode = MeasureSpec.getMode(i);
            i4 = MeasureSpec.getSize(i);
            int mode2 = MeasureSpec.getMode(i2);
            i3 = MeasureSpec.getSize(i2);
            if (mode == 1073741824 && mode2 == 1073741824) {
                if (this.zzeql * i3 < this.zzeqm * i4) {
                    i4 = (this.zzeql * i3) / this.zzeqm;
                } else if (this.zzeql * i3 > this.zzeqm * i4) {
                    defaultSize2 = (this.zzeqm * i4) / this.zzeql;
                }
                setMeasuredDimension(i4, i3);
                if (this.zzeqq != null) {
                }
                if (VERSION.SDK_INT == 16) {
                }
            } else {
                if (mode == 1073741824) {
                    int i6 = (this.zzeqm * i4) / this.zzeql;
                    if (mode2 != Integer.MIN_VALUE || i6 <= i3) {
                        i3 = i6;
                    }
                } else {
                    if (mode2 == 1073741824) {
                        i5 = (this.zzeql * i3) / this.zzeqm;
                        if (mode == Integer.MIN_VALUE) {
                        }
                    } else {
                        int i7 = this.zzeql;
                        int i8 = this.zzeqm;
                        if (mode2 != Integer.MIN_VALUE || i8 <= i3) {
                            i5 = i7;
                            i3 = i8;
                        } else {
                            i5 = (this.zzeql * i3) / this.zzeqm;
                        }
                        if (mode == Integer.MIN_VALUE && i5 > i4) {
                            defaultSize2 = (this.zzeqm * i4) / this.zzeql;
                        }
                    }
                    i4 = i5;
                }
                setMeasuredDimension(i4, i3);
                if (this.zzeqq != null) {
                    this.zzeqq.zzo(i4, i3);
                }
                if (VERSION.SDK_INT == 16) {
                    if ((this.zzeqo > 0 && this.zzeqo != i4) || (this.zzeqp > 0 && this.zzeqp != i3)) {
                        zzabb();
                    }
                    this.zzeqo = i4;
                    this.zzeqp = i3;
                    return;
                }
                return;
            }
        }
        i3 = defaultSize2;
        setMeasuredDimension(i4, i3);
        if (this.zzeqq != null) {
        }
        if (VERSION.SDK_INT == 16) {
        }
    }

    public final String toString() {
        String name = getClass().getName();
        String hexString = Integer.toHexString(hashCode());
        StringBuilder sb = new StringBuilder(String.valueOf(name).length() + 1 + String.valueOf(hexString).length());
        sb.append(name);
        sb.append("@");
        sb.append(hexString);
        return sb.toString();
    }

    private final void zzaba() {
        zzaxz.v("AdMediaPlayerView init MediaPlayer");
        SurfaceTexture surfaceTexture = getSurfaceTexture();
        if (this.zzeqk != null && surfaceTexture != null) {
            zzar(false);
            try {
                zzbv.zzlx();
                this.zzeqj = new MediaPlayer();
                this.zzeqj.setOnBufferingUpdateListener(this);
                this.zzeqj.setOnCompletionListener(this);
                this.zzeqj.setOnErrorListener(this);
                this.zzeqj.setOnInfoListener(this);
                this.zzeqj.setOnPreparedListener(this);
                this.zzeqj.setOnVideoSizeChangedListener(this);
                this.zzeqn = 0;
                if (this.zzeqr) {
                    this.zzeqq = new zzbdx(getContext());
                    this.zzeqq.zza(surfaceTexture, getWidth(), getHeight());
                    this.zzeqq.start();
                    SurfaceTexture zzabr = this.zzeqq.zzabr();
                    if (zzabr != null) {
                        surfaceTexture = zzabr;
                    } else {
                        this.zzeqq.zzabq();
                        this.zzeqq = null;
                    }
                }
                this.zzeqj.setDataSource(getContext(), this.zzeqk);
                zzbv.zzly();
                this.zzeqj.setSurface(new Surface(surfaceTexture));
                this.zzeqj.setAudioStreamType(3);
                this.zzeqj.setScreenOnWhilePlaying(true);
                this.zzeqj.prepareAsync();
                zzcx(1);
            } catch (IOException | IllegalArgumentException | IllegalStateException e) {
                String valueOf = String.valueOf(this.zzeqk);
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 36);
                sb.append("Failed to initialize MediaPlayer at ");
                sb.append(valueOf);
                zzaxz.zzc(sb.toString(), e);
                onError(this.zzeqj, 1, 0);
            }
        }
    }

    private final void zzabb() {
        if (this.zzeqg && zzabc() && this.zzeqj.getCurrentPosition() > 0 && this.zzeqi != 3) {
            zzaxz.v("AdMediaPlayerView nudging MediaPlayer");
            zzd(0.0f);
            this.zzeqj.start();
            int currentPosition = this.zzeqj.getCurrentPosition();
            long currentTimeMillis = zzbv.zzlm().currentTimeMillis();
            while (zzabc() && this.zzeqj.getCurrentPosition() == currentPosition) {
                if (zzbv.zzlm().currentTimeMillis() - currentTimeMillis > 250) {
                    break;
                }
            }
            this.zzeqj.pause();
            zzabd();
        }
    }

    private final void zzar(boolean z) {
        zzaxz.v("AdMediaPlayerView release");
        if (this.zzeqq != null) {
            this.zzeqq.zzabq();
            this.zzeqq = null;
        }
        if (this.zzeqj != null) {
            this.zzeqj.reset();
            this.zzeqj.release();
            this.zzeqj = null;
            zzcx(0);
            if (z) {
                this.zzeqi = 0;
                this.zzeqi = 0;
            }
        }
    }

    public final void play() {
        zzaxz.v("AdMediaPlayerView play");
        if (zzabc()) {
            this.zzeqj.start();
            zzcx(3);
            this.zzera.zzabf();
            zzayh.zzelc.post(new zzbdf(this));
        }
        this.zzeqi = 3;
    }

    public final void pause() {
        zzaxz.v("AdMediaPlayerView pause");
        if (zzabc() && this.zzeqj.isPlaying()) {
            this.zzeqj.pause();
            zzcx(4);
            zzayh.zzelc.post(new zzbdg(this));
        }
        this.zzeqi = 4;
    }

    public final int getDuration() {
        if (zzabc()) {
            return this.zzeqj.getDuration();
        }
        return -1;
    }

    public final int getCurrentPosition() {
        if (zzabc()) {
            return this.zzeqj.getCurrentPosition();
        }
        return 0;
    }

    public final void seekTo(int i) {
        StringBuilder sb = new StringBuilder(34);
        sb.append("AdMediaPlayerView seek ");
        sb.append(i);
        zzaxz.v(sb.toString());
        if (zzabc()) {
            this.zzeqj.seekTo(i);
            this.zzeqs = 0;
            return;
        }
        this.zzeqs = i;
    }

    private final boolean zzabc() {
        return (this.zzeqj == null || this.zzeqh == -1 || this.zzeqh == 0 || this.zzeqh == 1) ? false : true;
    }

    public final void zza(float f, float f2) {
        if (this.zzeqq != null) {
            this.zzeqq.zzb(f, f2);
        }
    }

    public final int getVideoWidth() {
        if (this.zzeqj != null) {
            return this.zzeqj.getVideoWidth();
        }
        return 0;
    }

    public final int getVideoHeight() {
        if (this.zzeqj != null) {
            return this.zzeqj.getVideoHeight();
        }
        return 0;
    }

    public final void zzabd() {
        zzd(this.zzerb.getVolume());
    }

    private final void zzd(float f) {
        if (this.zzeqj != null) {
            try {
                this.zzeqj.setVolume(f, f);
            } catch (IllegalStateException unused) {
            }
        } else {
            zzaxz.zzeo("AdMediaPlayerView setMediaPlayerVolume() called before onPrepared().");
        }
    }

    private final void zzcx(int i) {
        if (i == 3) {
            this.zzeqf.zzacd();
            this.zzerb.zzacd();
        } else if (this.zzeqh == 3) {
            this.zzeqf.zzace();
            this.zzerb.zzace();
        }
        this.zzeqh = i;
    }

    /* access modifiers changed from: 0000 */
    public final /* synthetic */ void zzcy(int i) {
        if (this.zzeqt != null) {
            this.zzeqt.onWindowVisibilityChanged(i);
        }
    }

    static {
        if (VERSION.SDK_INT >= 17) {
            zzeqe.put(Integer.valueOf(-1004), "MEDIA_ERROR_IO");
            zzeqe.put(Integer.valueOf(-1007), "MEDIA_ERROR_MALFORMED");
            zzeqe.put(Integer.valueOf(-1010), "MEDIA_ERROR_UNSUPPORTED");
            zzeqe.put(Integer.valueOf(-110), "MEDIA_ERROR_TIMED_OUT");
            zzeqe.put(Integer.valueOf(3), "MEDIA_INFO_VIDEO_RENDERING_START");
        }
        zzeqe.put(Integer.valueOf(100), "MEDIA_ERROR_SERVER_DIED");
        zzeqe.put(Integer.valueOf(1), "MEDIA_ERROR_UNKNOWN");
        zzeqe.put(Integer.valueOf(1), "MEDIA_INFO_UNKNOWN");
        zzeqe.put(Integer.valueOf(700), "MEDIA_INFO_VIDEO_TRACK_LAGGING");
        zzeqe.put(Integer.valueOf(701), "MEDIA_INFO_BUFFERING_START");
        zzeqe.put(Integer.valueOf(702), "MEDIA_INFO_BUFFERING_END");
        zzeqe.put(Integer.valueOf(800), "MEDIA_INFO_BAD_INTERLEAVING");
        zzeqe.put(Integer.valueOf(801), "MEDIA_INFO_NOT_SEEKABLE");
        zzeqe.put(Integer.valueOf(802), "MEDIA_INFO_METADATA_UPDATE");
        if (VERSION.SDK_INT >= 19) {
            zzeqe.put(Integer.valueOf(901), "MEDIA_INFO_UNSUPPORTED_SUBTITLE");
            zzeqe.put(Integer.valueOf(902), "MEDIA_INFO_SUBTITLE_TIMED_OUT");
        }
    }
}
