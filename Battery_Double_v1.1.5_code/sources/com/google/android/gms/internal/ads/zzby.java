package com.google.android.gms.internal.ads;

import com.mintegral.msdk.base.utils.CommonMD5;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

final class zzby implements Runnable {
    private zzby() {
    }

    public final void run() {
        try {
            zzbw.zziv = MessageDigest.getInstance(CommonMD5.TAG);
        } catch (NoSuchAlgorithmException unused) {
        } finally {
            zzbw.zziy.countDown();
        }
    }
}
