package com.google.android.gms.internal.ads;

import android.annotation.TargetApi;
import android.net.Uri;
import com.google.android.gms.ads.internal.zzbv;
import com.google.android.gms.common.util.Clock;
import java.io.IOException;

@zzark
@TargetApi(16)
public final class zzbfw extends zzbfk implements zzbez {
    private String url;
    private boolean zzexf;
    private zzbes zzexl;
    private Exception zzexm;
    private boolean zzexn;

    public zzbfw(zzbdz zzbdz, zzbdy zzbdy) {
        super(zzbdz);
        this.zzexl = new zzbes(zzbdz.getContext(), zzbdy);
        this.zzexl.zza((zzbez) this);
    }

    public final void zzdd(int i) {
    }

    public final void zzp(int i, int i2) {
    }

    public final void zzb(boolean z, long j) {
        zzbdz zzbdz = (zzbdz) this.zzewo.get();
        if (zzbdz != null) {
            zzbcg.zzepo.execute(new zzbfx(zzbdz, z, j));
        }
    }

    public final void zza(String str, Exception exc) {
        this.zzexm = exc;
        zzaxz.zzc("Precache error", exc);
        zzfa(str);
    }

    public final void zzda(int i) {
        this.zzexl.zzacz().zzdg(i);
    }

    public final void zzcz(int i) {
        this.zzexl.zzacz().zzdf(i);
    }

    public final void zzdb(int i) {
        this.zzexl.zzacz().zzdb(i);
    }

    public final void zzdc(int i) {
        this.zzexl.zzacz().zzdc(i);
    }

    public final void release() {
        if (this.zzexl != null) {
            this.zzexl.zza((zzbez) null);
            this.zzexl.release();
        }
        super.release();
    }

    /* access modifiers changed from: protected */
    public final String zzey(String str) {
        String valueOf = String.valueOf("cache:");
        String valueOf2 = String.valueOf(super.zzey(str));
        return valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0079, code lost:
        if (r11.zzexm == null) goto L_0x0080;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x007b, code lost:
        r1 = "badUrl";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x007f, code lost:
        throw r11.zzexm;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0080, code lost:
        r1 = "externalAbort";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0089, code lost:
        throw new java.io.IOException("Abort requested before buffering finished. ");
     */
    public final boolean zzex(String str) {
        String str2;
        String str3;
        long j;
        long j2;
        long j3;
        long j4;
        long j5;
        String str4 = str;
        this.url = str4;
        String zzey = zzey(str);
        String str5 = "error";
        try {
            this.zzexl.zza(Uri.parse(str), this.zzeiz);
            zzbdz zzbdz = (zzbdz) this.zzewo.get();
            if (zzbdz != null) {
                try {
                    zzbdz.zza(zzey, (zzbfk) this);
                } catch (Exception e) {
                    e = e;
                }
            }
            Clock zzlm = zzbv.zzlm();
            long currentTimeMillis = zzlm.currentTimeMillis();
            long longValue = ((Long) zzwu.zzpz().zzd(zzaan.zzcox)).longValue();
            long longValue2 = ((Long) zzwu.zzpz().zzd(zzaan.zzcow)).longValue() * 1000;
            long intValue = (long) ((Integer) zzwu.zzpz().zzd(zzaan.zzcov)).intValue();
            long j6 = -1;
            while (true) {
                synchronized (this) {
                    try {
                        if (zzlm.currentTimeMillis() - currentTimeMillis > longValue2) {
                            long j7 = longValue2;
                            String str6 = str5;
                            str2 = "downloadTimeout";
                            StringBuilder sb = new StringBuilder(47);
                            sb.append("Timeout reached. Limit: ");
                            sb.append(j7);
                            sb.append(" ms");
                            throw new IOException(sb.toString());
                        } else if (this.zzexf) {
                            try {
                                break;
                            } catch (Throwable th) {
                                th = th;
                            }
                        } else if (this.zzexn) {
                            break;
                        } else {
                            zzfg zzacw = this.zzexl.zzacw();
                            if (zzacw != null) {
                                str3 = str5;
                                try {
                                    long duration = zzacw.getDuration();
                                    if (duration > 0) {
                                        long bufferedPosition = zzacw.getBufferedPosition();
                                        if (bufferedPosition != j6) {
                                            j2 = intValue;
                                            j = longValue2;
                                            j5 = longValue;
                                            zza(str, zzey, bufferedPosition, duration, bufferedPosition > 0, zzbes.zzacx(), zzbes.zzacy());
                                            j6 = bufferedPosition;
                                        } else {
                                            j2 = intValue;
                                            j = longValue2;
                                            j5 = longValue;
                                        }
                                        if (bufferedPosition >= duration) {
                                            zzc(str4, zzey, duration);
                                        } else if (this.zzexl.getBytesTransferred() < j2 || bufferedPosition <= 0) {
                                            j3 = j6;
                                            j4 = j5;
                                        }
                                    } else {
                                        j2 = intValue;
                                        j = longValue2;
                                        j3 = j6;
                                        j4 = longValue;
                                    }
                                    wait(j4);
                                } catch (InterruptedException unused) {
                                    String str7 = "interrupted";
                                    throw new IOException("Wait interrupted.");
                                } catch (Throwable th2) {
                                    th = th2;
                                    str5 = str2;
                                    throw th;
                                }
                            } else {
                                String str8 = str5;
                                str2 = "exoPlayerReleased";
                                throw new IOException("ExoPlayer was released during preloading.");
                            }
                        }
                    } catch (Throwable th3) {
                        th = th3;
                        String str9 = str5;
                        throw th;
                    }
                }
                longValue = j4;
                j6 = j3;
                str5 = str3;
                intValue = j2;
                longValue2 = j;
            }
            return true;
        } catch (Exception e2) {
            e = e2;
            String str10 = str5;
            String message = e.getMessage();
            StringBuilder sb2 = new StringBuilder(String.valueOf(str).length() + 34 + String.valueOf(message).length());
            sb2.append("Failed to preload url ");
            sb2.append(str4);
            sb2.append(" Exception: ");
            sb2.append(message);
            zzaxz.zzeo(sb2.toString());
            release();
            zza(str4, zzey, str5, zzb(str5, e));
            return false;
        }
    }

    public final void abort() {
        zzfa(null);
    }

    private final void zzfa(String str) {
        synchronized (this) {
            this.zzexf = true;
            notify();
            release();
        }
        if (this.url != null) {
            String zzey = zzey(this.url);
            if (this.zzexm != null) {
                zza(this.url, zzey, "badUrl", zzb(str, this.zzexm));
                return;
            }
            zza(this.url, zzey, "externalAbort", "Programmatic precache abort.");
        }
    }

    public final zzbes zzadd() {
        synchronized (this) {
            this.zzexn = true;
            notify();
        }
        this.zzexl.zza((zzbez) null);
        zzbes zzbes = this.zzexl;
        this.zzexl = null;
        return zzbes;
    }

    private static String zzb(String str, Exception exc) {
        String canonicalName = exc.getClass().getCanonicalName();
        String message = exc.getMessage();
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 2 + String.valueOf(canonicalName).length() + String.valueOf(message).length());
        sb.append(str);
        sb.append("/");
        sb.append(canonicalName);
        sb.append(":");
        sb.append(message);
        return sb.toString();
    }
}
