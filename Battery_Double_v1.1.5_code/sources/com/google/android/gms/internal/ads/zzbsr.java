package com.google.android.gms.internal.ads;

import java.io.IOException;
import java.util.Iterator;
import java.util.Map.Entry;

final class zzbsr<T> implements zzbtc<T> {
    private final zzbsl zzfsm;
    private final boolean zzfsn;
    private final zzbtu<?, ?> zzfsw;
    private final zzbqr<?> zzfsx;

    private zzbsr(zzbtu<?, ?> zzbtu, zzbqr<?> zzbqr, zzbsl zzbsl) {
        this.zzfsw = zzbtu;
        this.zzfsn = zzbqr.zzh(zzbsl);
        this.zzfsx = zzbqr;
        this.zzfsm = zzbsl;
    }

    static <T> zzbsr<T> zza(zzbtu<?, ?> zzbtu, zzbqr<?> zzbqr, zzbsl zzbsl) {
        return new zzbsr<>(zzbtu, zzbqr, zzbsl);
    }

    public final T newInstance() {
        return this.zzfsm.zzamu().zzamz();
    }

    public final boolean equals(T t, T t2) {
        if (!this.zzfsw.zzag(t).equals(this.zzfsw.zzag(t2))) {
            return false;
        }
        if (this.zzfsn) {
            return this.zzfsx.zzq(t).equals(this.zzfsx.zzq(t2));
        }
        return true;
    }

    public final int hashCode(T t) {
        int hashCode = this.zzfsw.zzag(t).hashCode();
        return this.zzfsn ? (hashCode * 53) + this.zzfsx.zzq(t).hashCode() : hashCode;
    }

    public final void zzd(T t, T t2) {
        zzbte.zza(this.zzfsw, t, t2);
        if (this.zzfsn) {
            zzbte.zza(this.zzfsx, t, t2);
        }
    }

    public final void zza(T t, zzbup zzbup) throws IOException {
        Iterator it = this.zzfsx.zzq(t).iterator();
        while (it.hasNext()) {
            Entry entry = (Entry) it.next();
            zzbqw zzbqw = (zzbqw) entry.getKey();
            if (zzbqw.zzamm() != zzbuo.MESSAGE || zzbqw.zzamn() || zzbqw.zzamo()) {
                throw new IllegalStateException("Found invalid MessageSet item.");
            } else if (entry instanceof zzbrq) {
                zzbup.zzb(zzbqw.zzom(), (Object) ((zzbrq) entry).zzann().zzakf());
            } else {
                zzbup.zzb(zzbqw.zzom(), entry.getValue());
            }
        }
        zzbtu<?, ?> zzbtu = this.zzfsw;
        zzbtu.zzc(zzbtu.zzag(t), zzbup);
    }

    public final void zza(T t, byte[] bArr, int i, int i2, zzbpr zzbpr) throws IOException {
        zzbrd zzbrd = (zzbrd) t;
        zzbtv zzbtv = zzbrd.zzfpu;
        if (zzbtv == zzbtv.zzaoz()) {
            zzbtv = zzbtv.zzapa();
            zzbrd.zzfpu = zzbtv;
        }
        zzbtv zzbtv2 = zzbtv;
        while (i < i2) {
            int zza = zzbpq.zza(bArr, i, zzbpr);
            int i3 = zzbpr.zzfld;
            if (i3 == 11) {
                int i4 = 0;
                zzbpu zzbpu = null;
                while (zza < i2) {
                    zza = zzbpq.zza(bArr, zza, zzbpr);
                    int i5 = zzbpr.zzfld;
                    int i6 = i5 & 7;
                    switch (i5 >>> 3) {
                        case 2:
                            if (i6 == 0) {
                                zza = zzbpq.zza(bArr, zza, zzbpr);
                                i4 = zzbpr.zzfld;
                                break;
                            }
                        case 3:
                            if (i6 == 2) {
                                zza = zzbpq.zze(bArr, zza, zzbpr);
                                zzbpu = (zzbpu) zzbpr.zzflf;
                                break;
                            }
                        default:
                            if (i5 == 12) {
                                break;
                            } else {
                                zza = zzbpq.zza(i5, bArr, zza, i2, zzbpr);
                                break;
                            }
                    }
                }
                if (zzbpu != null) {
                    zzbtv2.zzc((i4 << 3) | 2, zzbpu);
                }
                i = zza;
            } else if ((i3 & 7) == 2) {
                i = zzbpq.zza(i3, bArr, zza, i2, zzbtv2, zzbpr);
            } else {
                i = zzbpq.zza(i3, bArr, zza, i2, zzbpr);
            }
        }
        if (i != i2) {
            throw zzbrl.zzanj();
        }
    }

    public final void zza(T t, zzbtb zzbtb, zzbqq zzbqq) throws IOException {
        boolean z;
        zzbtu<?, ?> zzbtu = this.zzfsw;
        zzbqr<?> zzbqr = this.zzfsx;
        Object zzah = zzbtu.zzah(t);
        zzbqu zzr = zzbqr.zzr(t);
        do {
            try {
                if (zzbtb.zzals() == Integer.MAX_VALUE) {
                    zzbtu.zzg(t, zzah);
                    return;
                }
                int tag = zzbtb.getTag();
                if (tag == 11) {
                    Object obj = null;
                    zzbpu zzbpu = null;
                    int i = 0;
                    while (zzbtb.zzals() != Integer.MAX_VALUE) {
                        int tag2 = zzbtb.getTag();
                        if (tag2 == 16) {
                            i = zzbtb.zzald();
                            obj = zzbqr.zza(zzbqq, this.zzfsm, i);
                        } else if (tag2 == 26) {
                            if (obj != null) {
                                zzbqr.zza(zzbtb, obj, zzbqq, zzr);
                            } else {
                                zzbpu = zzbtb.zzalc();
                            }
                        } else if (!zzbtb.zzalt()) {
                            break;
                        }
                    }
                    if (zzbtb.getTag() != 12) {
                        throw zzbrl.zzang();
                    } else if (zzbpu != null) {
                        if (obj != null) {
                            zzbqr.zza(zzbpu, obj, zzbqq, zzr);
                        } else {
                            zzbtu.zza(zzah, i, zzbpu);
                        }
                    }
                } else if ((tag & 7) == 2) {
                    Object zza = zzbqr.zza(zzbqq, this.zzfsm, tag >>> 3);
                    if (zza != null) {
                        zzbqr.zza(zzbtb, zza, zzbqq, zzr);
                    } else {
                        z = zzbtu.zza(zzah, zzbtb);
                        continue;
                    }
                } else {
                    z = zzbtb.zzalt();
                    continue;
                }
                z = true;
                continue;
            } finally {
                zzbtu.zzg(t, zzah);
            }
        } while (z);
    }

    public final void zzs(T t) {
        this.zzfsw.zzs(t);
        this.zzfsx.zzs(t);
    }

    public final boolean zzae(T t) {
        return this.zzfsx.zzq(t).isInitialized();
    }

    public final int zzac(T t) {
        zzbtu<?, ?> zzbtu = this.zzfsw;
        int zzai = zzbtu.zzai(zzbtu.zzag(t)) + 0;
        return this.zzfsn ? zzai + this.zzfsx.zzq(t).zzamk() : zzai;
    }
}
