package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.RemoteException;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.google.android.gms.ads.internal.gmsg.zzb;
import com.google.android.gms.ads.internal.zzbv;
import com.google.android.gms.ads.internal.zzbw;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.dynamic.ObjectWrapper;
import java.util.HashMap;
import java.util.Map;

@zzark
public final class zzauk {
    private static final zzalf zzedx = new zzalf();
    private final zzalg zzedy;
    private final zzbw zzedz;
    private final Map<String, zzavy> zzeea = new HashMap();
    private final zzavr zzeeb;
    private final zzb zzeec;
    private final zzapm zzeed;

    public zzauk(zzbw zzbw, zzalg zzalg, zzavr zzavr, zzb zzb, zzapm zzapm) {
        this.zzedz = zzbw;
        this.zzedy = zzalg;
        this.zzeeb = zzavr;
        this.zzeec = zzb;
        this.zzeed = zzapm;
    }

    public static boolean zza(zzaxf zzaxf, zzaxf zzaxf2) {
        return true;
    }

    public final zzb zzxb() {
        return this.zzeec;
    }

    public final zzapm zzxc() {
        return this.zzeed;
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x0038  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x003d  */
    @Nullable
    public final zzavy zzdd(String str) {
        zzavy zzavy = (zzavy) this.zzeea.get(str);
        if (zzavy != null) {
            return zzavy;
        }
        try {
            zzalg zzalg = this.zzedy;
            if ("com.google.ads.mediation.admob.AdMobAdapter".equals(str)) {
                zzalg = zzedx;
            }
            zzavy zzavy2 = new zzavy(zzalg.zzcp(str), this.zzeeb);
            try {
                this.zzeea.put(str, zzavy2);
                return zzavy2;
            } catch (Exception e) {
                e = e;
                zzavy = zzavy2;
                String str2 = "Fail to instantiate adapter ";
                String valueOf = String.valueOf(str);
                zzaxz.zzc(valueOf.length() == 0 ? str2.concat(valueOf) : new String(str2), e);
                return zzavy;
            }
        } catch (Exception e2) {
            e = e2;
            String str22 = "Fail to instantiate adapter ";
            String valueOf2 = String.valueOf(str);
            zzaxz.zzc(valueOf2.length() == 0 ? str22.concat(valueOf2) : new String(str22), e);
            return zzavy;
        }
    }

    public final void zzxd() {
        this.zzedz.zzbtw = 0;
        zzbw zzbw = this.zzedz;
        zzbv.zzle();
        zzavu zzavu = new zzavu(this.zzedz.zzsp, this.zzedz.zzbsv, this);
        String str = "AdRenderer: ";
        String valueOf = String.valueOf(zzavu.getClass().getName());
        zzaxz.zzdn(valueOf.length() != 0 ? str.concat(valueOf) : new String(str));
        zzavu.zzwa();
        zzbw.zzbss = zzavu;
    }

    public final void zzah(boolean z) {
        zzavy zzdd = zzdd(this.zzedz.zzbsu.zzdnd);
        if (!(zzdd == null || zzdd.zzxn() == null)) {
            try {
                zzdd.zzxn().setImmersiveMode(z);
                zzdd.zzxn().showVideo();
            } catch (RemoteException e) {
                zzaxz.zzd("#007 Could not call remote method.", e);
            }
        }
    }

    public final void pause() {
        Preconditions.checkMainThread("pause must be called on the main UI thread.");
        for (String str : this.zzeea.keySet()) {
            try {
                zzavy zzavy = (zzavy) this.zzeea.get(str);
                if (!(zzavy == null || zzavy.zzxn() == null)) {
                    zzavy.zzxn().pause();
                }
            } catch (RemoteException e) {
                zzaxz.zzd("#007 Could not call remote method.", e);
            }
        }
    }

    public final void resume() {
        Preconditions.checkMainThread("resume must be called on the main UI thread.");
        for (String str : this.zzeea.keySet()) {
            try {
                zzavy zzavy = (zzavy) this.zzeea.get(str);
                if (!(zzavy == null || zzavy.zzxn() == null)) {
                    zzavy.zzxn().resume();
                }
            } catch (RemoteException e) {
                zzaxz.zzd("#007 Could not call remote method.", e);
            }
        }
    }

    public final void destroy() {
        Preconditions.checkMainThread("destroy must be called on the main UI thread.");
        for (String str : this.zzeea.keySet()) {
            try {
                zzavy zzavy = (zzavy) this.zzeea.get(str);
                if (!(zzavy == null || zzavy.zzxn() == null)) {
                    zzavy.zzxn().destroy();
                }
            } catch (RemoteException e) {
                zzaxz.zzd("#007 Could not call remote method.", e);
            }
        }
    }

    public final void onContextChanged(@NonNull Context context) {
        for (zzavy zzxn : this.zzeea.values()) {
            try {
                zzxn.zzxn().zzj(ObjectWrapper.wrap(context));
            } catch (RemoteException e) {
                zzaxz.zzb("Unable to call Adapter.onContextChanged.", e);
            }
        }
    }

    public final void zzxe() {
        if (this.zzedz.zzbsu != null && this.zzedz.zzbsu.zzdnb != null) {
            zzbv.zzlz();
            zzakz.zza(this.zzedz.zzsp, this.zzedz.zzbsp.zzdp, this.zzedz.zzbsu, this.zzedz.zzbsn, false, this.zzedz.zzbsu.zzdnb.zzdlf);
        }
    }

    public final void zzxf() {
        if (this.zzedz.zzbsu != null && this.zzedz.zzbsu.zzdnb != null) {
            zzbv.zzlz();
            zzakz.zza(this.zzedz.zzsp, this.zzedz.zzbsp.zzdp, this.zzedz.zzbsu, this.zzedz.zzbsn, false, this.zzedz.zzbsu.zzdnb.zzdlh);
        }
    }

    public final zzawd zzd(zzawd zzawd) {
        if (!(this.zzedz.zzbsu == null || this.zzedz.zzbsu.zzehj == null || TextUtils.isEmpty(this.zzedz.zzbsu.zzehj.zzdly))) {
            zzawd = new zzawd(this.zzedz.zzbsu.zzehj.zzdly, this.zzedz.zzbsu.zzehj.zzdlz);
        }
        if (!(this.zzedz.zzbsu == null || this.zzedz.zzbsu.zzdnb == null)) {
            zzbv.zzlz();
            zzakz.zza(this.zzedz.zzsp, this.zzedz.zzbsp.zzdp, this.zzedz.zzbsu.zzdnb.zzdlg, this.zzedz.zzbtr, this.zzedz.zzbts, zzawd);
        }
        return zzawd;
    }
}
