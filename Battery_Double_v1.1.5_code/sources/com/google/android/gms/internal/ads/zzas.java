package com.google.android.gms.internal.ads;

import android.support.annotation.VisibleForTesting;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import javax.net.ssl.SSLSocketFactory;

public final class zzas extends zzai {
    private final zzau zzci;
    private final SSLSocketFactory zzcj;

    public zzas() {
        this(null);
    }

    private zzas(zzau zzau) {
        this(null, null);
    }

    private zzas(zzau zzau, SSLSocketFactory sSLSocketFactory) {
        this.zzci = null;
        this.zzcj = null;
    }

    /* JADX WARNING: Removed duplicated region for block: B:58:0x012a  */
    public final zzaq zza(zzr<?> zzr, Map<String, String> map) throws IOException, zza {
        String str;
        String url = zzr.getUrl();
        HashMap hashMap = new HashMap();
        hashMap.putAll(map);
        hashMap.putAll(zzr.getHeaders());
        if (this.zzci != null) {
            str = this.zzci.zzg(url);
            if (str == null) {
                String str2 = "URL blocked by rewriter: ";
                String valueOf = String.valueOf(url);
                throw new IOException(valueOf.length() != 0 ? str2.concat(valueOf) : new String(str2));
            }
        } else {
            str = url;
        }
        URL url2 = new URL(str);
        HttpURLConnection httpURLConnection = (HttpURLConnection) url2.openConnection();
        httpURLConnection.setInstanceFollowRedirects(HttpURLConnection.getFollowRedirects());
        int zzj = zzr.zzj();
        httpURLConnection.setConnectTimeout(zzj);
        httpURLConnection.setReadTimeout(zzj);
        boolean z = false;
        httpURLConnection.setUseCaches(false);
        httpURLConnection.setDoInput(true);
        "https".equals(url2.getProtocol());
        try {
            for (String str3 : hashMap.keySet()) {
                httpURLConnection.setRequestProperty(str3, (String) hashMap.get(str3));
            }
            switch (zzr.getMethod()) {
                case -1:
                    break;
                case 0:
                    httpURLConnection.setRequestMethod(HttpRequest.METHOD_GET);
                    break;
                case 1:
                    httpURLConnection.setRequestMethod(HttpRequest.METHOD_POST);
                    zza(httpURLConnection, zzr);
                    break;
                case 2:
                    httpURLConnection.setRequestMethod(HttpRequest.METHOD_PUT);
                    zza(httpURLConnection, zzr);
                    break;
                case 3:
                    httpURLConnection.setRequestMethod(HttpRequest.METHOD_DELETE);
                    break;
                case 4:
                    httpURLConnection.setRequestMethod(HttpRequest.METHOD_HEAD);
                    break;
                case 5:
                    httpURLConnection.setRequestMethod(HttpRequest.METHOD_OPTIONS);
                    break;
                case 6:
                    httpURLConnection.setRequestMethod(HttpRequest.METHOD_TRACE);
                    break;
                case 7:
                    httpURLConnection.setRequestMethod("PATCH");
                    zza(httpURLConnection, zzr);
                    break;
                default:
                    throw new IllegalStateException("Unknown method type.");
            }
            int responseCode = httpURLConnection.getResponseCode();
            if (responseCode != -1) {
                if (!((zzr.getMethod() == 4 || (100 <= responseCode && responseCode < 200) || responseCode == 204 || responseCode == 304) ? false : true)) {
                    zzaq zzaq = new zzaq(responseCode, zza(httpURLConnection.getHeaderFields()));
                    httpURLConnection.disconnect();
                    return zzaq;
                }
                try {
                    return new zzaq(responseCode, zza(httpURLConnection.getHeaderFields()), httpURLConnection.getContentLength(), new zzat(httpURLConnection));
                } catch (Throwable th) {
                    th = th;
                    z = true;
                    if (!z) {
                        httpURLConnection.disconnect();
                    }
                    throw th;
                }
            } else {
                throw new IOException("Could not retrieve response code from HttpUrlConnection.");
            }
        } catch (Throwable th2) {
            th = th2;
            if (!z) {
            }
            throw th;
        }
    }

    @VisibleForTesting
    private static List<zzl> zza(Map<String, List<String>> map) {
        ArrayList arrayList = new ArrayList(map.size());
        for (Entry entry : map.entrySet()) {
            if (entry.getKey() != null) {
                for (String zzl : (List) entry.getValue()) {
                    arrayList.add(new zzl((String) entry.getKey(), zzl));
                }
            }
        }
        return arrayList;
    }

    /* access modifiers changed from: private */
    public static InputStream zza(HttpURLConnection httpURLConnection) {
        try {
            return httpURLConnection.getInputStream();
        } catch (IOException unused) {
            return httpURLConnection.getErrorStream();
        }
    }

    private static void zza(HttpURLConnection httpURLConnection, zzr<?> zzr) throws IOException, zza {
        byte[] zzh = zzr.zzh();
        if (zzh != null) {
            httpURLConnection.setDoOutput(true);
            if (!httpURLConnection.getRequestProperties().containsKey("Content-Type")) {
                String str = "Content-Type";
                String str2 = "application/x-www-form-urlencoded; charset=";
                String valueOf = String.valueOf("UTF-8");
                httpURLConnection.setRequestProperty(str, valueOf.length() != 0 ? str2.concat(valueOf) : new String(str2));
            }
            DataOutputStream dataOutputStream = new DataOutputStream(httpURLConnection.getOutputStream());
            dataOutputStream.write(zzh);
            dataOutputStream.close();
        }
    }
}
