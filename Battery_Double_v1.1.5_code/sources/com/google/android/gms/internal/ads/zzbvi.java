package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzbvd.zzb.zzf.C0026zzb;

final class zzbvi implements zzbri {
    static final zzbri zzccw = new zzbvi();

    private zzbvi() {
    }

    public final boolean zzcb(int i) {
        return C0026zzb.zzgj(i) != null;
    }
}
