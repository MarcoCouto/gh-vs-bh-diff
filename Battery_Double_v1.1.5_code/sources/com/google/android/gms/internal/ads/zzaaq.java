package com.google.android.gms.internal.ads;

import android.content.Context;
import android.net.Uri;
import android.net.Uri.Builder;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.google.android.gms.ads.internal.zzbv;
import com.google.android.gms.common.util.VisibleForTesting;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.annotation.ParametersAreNonnullByDefault;

@zzark
@ParametersAreNonnullByDefault
public final class zzaaq {
    @VisibleForTesting
    private Context mContext;
    @VisibleForTesting
    private String zzbuk;
    @VisibleForTesting
    private String zzcyt;
    @VisibleForTesting
    private BlockingQueue<zzaba> zzcyv = new ArrayBlockingQueue(100);
    @VisibleForTesting
    private ExecutorService zzcyw;
    @VisibleForTesting
    private LinkedHashMap<String, String> zzcyx = new LinkedHashMap<>();
    @VisibleForTesting
    private Map<String, zzaau> zzcyy = new HashMap();
    private AtomicBoolean zzcyz;
    private File zzcza;

    public final void zza(Context context, String str, String str2, Map<String, String> map) {
        this.mContext = context;
        this.zzbuk = str;
        this.zzcyt = str2;
        this.zzcyz = new AtomicBoolean(false);
        this.zzcyz.set(((Boolean) zzwu.zzpz().zzd(zzaan.zzcpy)).booleanValue());
        if (this.zzcyz.get()) {
            File externalStorageDirectory = Environment.getExternalStorageDirectory();
            if (externalStorageDirectory != null) {
                this.zzcza = new File(externalStorageDirectory, "sdk_csi_data.txt");
            }
        }
        for (Entry entry : map.entrySet()) {
            this.zzcyx.put((String) entry.getKey(), (String) entry.getValue());
        }
        this.zzcyw = Executors.newSingleThreadExecutor();
        this.zzcyw.execute(new zzaar(this));
        this.zzcyy.put("action", zzaau.zzczd);
        this.zzcyy.put("ad_format", zzaau.zzczd);
        this.zzcyy.put("e", zzaau.zzcze);
    }

    public final void zzg(@Nullable List<String> list) {
        if (list != null && !list.isEmpty()) {
            this.zzcyx.put("e", TextUtils.join(",", list));
        }
    }

    public final boolean zza(zzaba zzaba) {
        return this.zzcyv.offer(zzaba);
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x009f A[SYNTHETIC, Splitter:B:31:0x009f] */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x00ae A[SYNTHETIC, Splitter:B:36:0x00ae] */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x0000 A[SYNTHETIC] */
    public final void zzrc() {
        while (true) {
            try {
                zzaba zzaba = (zzaba) this.zzcyv.take();
                String zzrh = zzaba.zzrh();
                if (!TextUtils.isEmpty(zzrh)) {
                    Map zza = zza(this.zzcyx, zzaba.zzri());
                    Builder buildUpon = Uri.parse(this.zzcyt).buildUpon();
                    for (Entry entry : zza.entrySet()) {
                        buildUpon.appendQueryParameter((String) entry.getKey(), (String) entry.getValue());
                    }
                    StringBuilder sb = new StringBuilder(buildUpon.build().toString());
                    sb.append("&it=");
                    sb.append(zzrh);
                    String sb2 = sb.toString();
                    if (this.zzcyz.get()) {
                        File file = this.zzcza;
                        if (file != null) {
                            FileOutputStream fileOutputStream = null;
                            try {
                                FileOutputStream fileOutputStream2 = new FileOutputStream(file, true);
                                try {
                                    fileOutputStream2.write(sb2.getBytes());
                                    fileOutputStream2.write(10);
                                    try {
                                        fileOutputStream2.close();
                                    } catch (IOException e) {
                                        zzaxz.zzc("CsiReporter: Cannot close file: sdk_csi_data.txt.", e);
                                    }
                                } catch (IOException e2) {
                                    e = e2;
                                    fileOutputStream = fileOutputStream2;
                                    try {
                                        zzaxz.zzc("CsiReporter: Cannot write to file: sdk_csi_data.txt.", e);
                                        if (fileOutputStream == null) {
                                        }
                                    } catch (Throwable th) {
                                        th = th;
                                        if (fileOutputStream != null) {
                                            try {
                                                fileOutputStream.close();
                                            } catch (IOException e3) {
                                                zzaxz.zzc("CsiReporter: Cannot close file: sdk_csi_data.txt.", e3);
                                            }
                                        }
                                        throw th;
                                    }
                                } catch (Throwable th2) {
                                    th = th2;
                                    fileOutputStream = fileOutputStream2;
                                    if (fileOutputStream != null) {
                                    }
                                    throw th;
                                }
                            } catch (IOException e4) {
                                e = e4;
                                zzaxz.zzc("CsiReporter: Cannot write to file: sdk_csi_data.txt.", e);
                                if (fileOutputStream == null) {
                                    try {
                                        fileOutputStream.close();
                                    } catch (IOException e5) {
                                        zzaxz.zzc("CsiReporter: Cannot close file: sdk_csi_data.txt.", e5);
                                    }
                                }
                            }
                        } else {
                            zzaxz.zzeo("CsiReporter: File doesn't exists. Cannot write CSI data to file.");
                        }
                    } else {
                        zzbv.zzlf();
                        zzayh.zzc(this.mContext, this.zzbuk, sb2);
                    }
                }
            } catch (InterruptedException e6) {
                zzaxz.zzc("CsiReporter:reporter interrupted", e6);
                return;
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public final Map<String, String> zza(Map<String, String> map, @Nullable Map<String, String> map2) {
        LinkedHashMap linkedHashMap = new LinkedHashMap(map);
        if (map2 == null) {
            return linkedHashMap;
        }
        for (Entry entry : map2.entrySet()) {
            String str = (String) entry.getKey();
            String str2 = (String) linkedHashMap.get(str);
            linkedHashMap.put(str, zzbn(str).zzf(str2, (String) entry.getValue()));
        }
        return linkedHashMap;
    }

    public final zzaau zzbn(String str) {
        zzaau zzaau = (zzaau) this.zzcyy.get(str);
        if (zzaau != null) {
            return zzaau;
        }
        return zzaau.zzczc;
    }
}
