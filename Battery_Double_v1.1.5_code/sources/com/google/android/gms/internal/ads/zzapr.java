package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.gms.common.util.VisibleForTesting;
import io.fabric.sdk.android.services.events.EventsFilesManager;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import org.json.JSONObject;

@zzark
public final class zzapr extends zzaph {
    private final zzaba zzbln;
    private zzalg zzbma;
    /* access modifiers changed from: private */
    public final zzbgg zzdin;
    private zzakr zzdmn;
    @VisibleForTesting
    private zzakp zzdsw;
    protected zzakx zzdsx;
    /* access modifiers changed from: private */
    public boolean zzdsy;

    zzapr(Context context, zzaxg zzaxg, zzalg zzalg, zzapm zzapm, zzaba zzaba, zzbgg zzbgg) {
        super(context, zzaxg, zzapm);
        this.zzbma = zzalg;
        this.zzdmn = zzaxg.zzehj;
        this.zzbln = zzaba;
        this.zzdin = zzbgg;
    }

    /* access modifiers changed from: protected */
    public final zzaxf zzcr(int i) {
        String name;
        String str;
        boolean z;
        zzakr zzakr;
        long j;
        zzaso zzaso;
        String str2;
        String str3;
        boolean z2;
        zzakr zzakr2;
        long j2;
        Iterator it;
        zzakr zzakr3;
        int i2;
        zzasi zzasi = this.zzdsk.zzeag;
        zzwb zzwb = zzasi.zzdwg;
        zzbgg zzbgg = this.zzdin;
        List<String> list = this.zzdsl.zzdlq;
        List<String> list2 = this.zzdsl.zzdlr;
        List<String> list3 = this.zzdsl.zzdyf;
        int i3 = this.zzdsl.orientation;
        long j3 = this.zzdsl.zzdlx;
        String str4 = zzasi.zzdwj;
        boolean z3 = this.zzdsl.zzdyd;
        zzakq zzakq = this.zzdsx != null ? this.zzdsx.zzdnb : null;
        zzalj zzalj = this.zzdsx != null ? this.zzdsx.zzdnc : null;
        if (this.zzdsx != null) {
            name = this.zzdsx.zzdnd;
        } else {
            name = AdMobAdapter.class.getName();
        }
        String str5 = name;
        zzakr zzakr4 = this.zzdmn;
        zzakt zzakt = this.zzdsx != null ? this.zzdsx.zzdne : null;
        zzakq zzakq2 = zzakq;
        zzalj zzalj2 = zzalj;
        long j4 = this.zzdsl.zzdye;
        zzwf zzwf = this.zzdsk.zzbst;
        long j5 = j4;
        long j6 = this.zzdsl.zzdyc;
        long j7 = this.zzdsk.zzehn;
        long j8 = this.zzdsl.zzdyh;
        String str6 = this.zzdsl.zzdyi;
        JSONObject jSONObject = this.zzdsk.zzehh;
        zzawd zzawd = this.zzdsl.zzdyr;
        List<String> list4 = this.zzdsl.zzdys;
        List<String> list5 = this.zzdsl.zzdyt;
        zzwf zzwf2 = zzwf;
        boolean z4 = this.zzdmn != null ? this.zzdmn.zzdmc : false;
        zzaso zzaso2 = this.zzdsl.zzdyv;
        if (this.zzdsw != null) {
            List zzui = this.zzdsw.zzui();
            String str7 = "";
            if (zzui == null) {
                zzakr = zzakr4;
                zzaso = zzaso2;
                str2 = str7.toString();
                str = str4;
                z = z3;
                j = j8;
            } else {
                Iterator it2 = zzui.iterator();
                String str8 = str7;
                while (it2.hasNext()) {
                    zzaso zzaso3 = zzaso2;
                    zzakx zzakx = (zzakx) it2.next();
                    if (zzakx != null) {
                        it = it2;
                        if (zzakx.zzdnb == null || TextUtils.isEmpty(zzakx.zzdnb.zzdkx)) {
                            zzakr3 = zzakr4;
                        } else {
                            String valueOf = String.valueOf(str8);
                            j2 = j8;
                            String str9 = zzakx.zzdnb.zzdkx;
                            switch (zzakx.zzdna) {
                                case -1:
                                    i2 = 4;
                                    break;
                                case 0:
                                    zzakr2 = zzakr4;
                                    i2 = 0;
                                    break;
                                case 1:
                                    zzakr2 = zzakr4;
                                    i2 = 1;
                                    break;
                                case 3:
                                    i2 = 2;
                                    break;
                                case 4:
                                    i2 = 3;
                                    break;
                                case 5:
                                    i2 = 5;
                                    break;
                                default:
                                    i2 = 6;
                                    break;
                            }
                            zzakr2 = zzakr4;
                            long j9 = zzakx.zzdng;
                            z2 = z3;
                            str3 = str4;
                            StringBuilder sb = new StringBuilder(String.valueOf(str9).length() + 33);
                            sb.append(str9);
                            sb.append(".");
                            sb.append(i2);
                            sb.append(".");
                            sb.append(j9);
                            String sb2 = sb.toString();
                            StringBuilder sb3 = new StringBuilder(String.valueOf(valueOf).length() + 1 + String.valueOf(sb2).length());
                            sb3.append(valueOf);
                            sb3.append(sb2);
                            sb3.append(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
                            str8 = sb3.toString();
                            zzaso2 = zzaso3;
                            it2 = it;
                            j8 = j2;
                            zzakr4 = zzakr2;
                            z3 = z2;
                            str4 = str3;
                        }
                    } else {
                        zzakr3 = zzakr4;
                        it = it2;
                    }
                    str3 = str4;
                    z2 = z3;
                    j2 = j8;
                    zzaso2 = zzaso3;
                    it2 = it;
                    j8 = j2;
                    zzakr4 = zzakr2;
                    z3 = z2;
                    str4 = str3;
                }
                zzakr = zzakr4;
                zzaso = zzaso2;
                str = str4;
                z = z3;
                j = j8;
                str2 = str8.substring(0, Math.max(0, str8.length() - 1));
            }
        } else {
            zzakr = zzakr4;
            zzaso = zzaso2;
            str = str4;
            z = z3;
            j = j8;
            str2 = null;
        }
        zzaxf zzaxf = new zzaxf(zzwb, zzbgg, list, i, list2, list3, i3, j3, str, z, zzakq2, zzalj2, str5, zzakr, zzakt, j5, zzwf2, j6, j7, j, str6, jSONObject, null, zzawd, list4, list5, z4, zzaso, str2, this.zzdsl.zzdlu, this.zzdsl.zzdyy, this.zzdsk.zzehw, this.zzdsl.zzbph, this.zzdsk.zzehx, this.zzdsl.zzdzc, this.zzdsl.zzdls, this.zzdsl.zzbpi, this.zzdsl.zzdzd, this.zzdsl.zzdzf);
        return zzaxf;
    }

    /* JADX WARNING: type inference failed for: r0v3, types: [com.google.android.gms.internal.ads.zzakp] */
    /* JADX WARNING: type inference failed for: r17v0, types: [com.google.android.gms.internal.ads.zzald] */
    /* JADX WARNING: type inference failed for: r4v4, types: [com.google.android.gms.internal.ads.zzala] */
    /* JADX WARNING: type inference failed for: r17v2, types: [com.google.android.gms.internal.ads.zzald] */
    /* JADX WARNING: type inference failed for: r4v5, types: [com.google.android.gms.internal.ads.zzala] */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Multi-variable type inference failed. Error: jadx.core.utils.exceptions.JadxRuntimeException: No candidate types for var: r17v2, types: [com.google.android.gms.internal.ads.zzald]
  assigns: [com.google.android.gms.internal.ads.zzald, com.google.android.gms.internal.ads.zzala]
  uses: [com.google.android.gms.internal.ads.zzald, com.google.android.gms.internal.ads.zzakp, com.google.android.gms.internal.ads.zzala]
  mth insns count: 151
    	at jadx.core.dex.visitors.typeinference.TypeSearch.fillTypeCandidates(TypeSearch.java:237)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.typeinference.TypeSearch.run(TypeSearch.java:53)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.runMultiVariableSearch(TypeInferenceVisitor.java:99)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.visit(TypeInferenceVisitor.java:92)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:27)
    	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$1(DepthTraversal.java:14)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
    	at jadx.core.ProcessClass.process(ProcessClass.java:30)
    	at jadx.core.ProcessClass.lambda$processDependencies$0(ProcessClass.java:49)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:49)
    	at jadx.core.ProcessClass.process(ProcessClass.java:35)
    	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
    	at jadx.api.JavaClass.decompile(JavaClass.java:62)
    	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
     */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x00b3  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x00de  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00fb  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0104  */
    /* JADX WARNING: Unknown variable types count: 3 */
    public final void zzap(long j) throws zzapk {
        ? r0;
        boolean z;
        synchronized (this.zzdsn) {
            if (this.zzdmn.zzdma != -1) {
                long j2 = j;
                ? zzala = new zzala(this.mContext, this.zzdsk.zzeag, this.zzbma, this.zzdmn, this.zzdsl.zzckn, this.zzdsl.zzckp, this.zzdsl.zzdyw, j2, ((Long) zzwu.zzpz().zzd(zzaan.zzctf)).longValue(), 2, this.zzdsk.zzehx);
                r0 = zzala;
            } else {
                ? zzald = new zzald(this.mContext, this.zzdsk.zzeag, this.zzbma, this.zzdmn, this.zzdsl.zzckn, this.zzdsl.zzckp, this.zzdsl.zzdyw, j, ((Long) zzwu.zzpz().zzd(zzaan.zzctf)).longValue(), this.zzbln, this.zzdsk.zzehx);
                r0 = zzald;
            }
            this.zzdsw = r0;
        }
        ArrayList arrayList = new ArrayList(this.zzdmn.zzdlp);
        Bundle bundle = this.zzdsk.zzeag.zzdwg.zzcjl;
        String str = "com.google.ads.mediation.admob.AdMobAdapter";
        if (bundle != null) {
            Bundle bundle2 = bundle.getBundle(str);
            if (bundle2 != null) {
                z = bundle2.getBoolean("_skipMediation");
                if (z) {
                    ListIterator listIterator = arrayList.listIterator();
                    while (listIterator.hasNext()) {
                        if (!((zzakq) listIterator.next()).zzdkw.contains(str)) {
                            listIterator.remove();
                        }
                    }
                }
                this.zzdsx = this.zzdsw.zzh(arrayList);
                switch (this.zzdsx.zzdna) {
                    case 0:
                        if (this.zzdsx.zzdnb != null && this.zzdsx.zzdnb.zzdli != null) {
                            CountDownLatch countDownLatch = new CountDownLatch(1);
                            zzayh.zzelc.post(new zzaps(this, countDownLatch));
                            try {
                                countDownLatch.await(10, TimeUnit.SECONDS);
                                synchronized (this.zzdsn) {
                                    if (!this.zzdsy) {
                                        throw new zzapk("View could not be prepared", 0);
                                    } else if (this.zzdin.isDestroyed()) {
                                        throw new zzapk("Assets not loaded, web view is destroyed", 0);
                                    }
                                }
                                return;
                            } catch (InterruptedException e) {
                                String valueOf = String.valueOf(e);
                                StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 38);
                                sb.append("Interrupted while waiting for latch : ");
                                sb.append(valueOf);
                                throw new zzapk(sb.toString(), 0);
                            }
                        } else {
                            return;
                        }
                    case 1:
                        throw new zzapk("No fill from any mediation ad networks.", 3);
                    default:
                        int i = this.zzdsx.zzdna;
                        StringBuilder sb2 = new StringBuilder(40);
                        sb2.append("Unexpected mediation result: ");
                        sb2.append(i);
                        throw new zzapk(sb2.toString(), 0);
                }
            }
        }
        z = false;
        if (z) {
        }
        this.zzdsx = this.zzdsw.zzh(arrayList);
        switch (this.zzdsx.zzdna) {
            case 0:
                break;
            case 1:
                break;
        }
    }

    public final void onStop() {
        synchronized (this.zzdsn) {
            super.onStop();
            if (this.zzdsw != null) {
                this.zzdsw.cancel();
            }
        }
    }
}
