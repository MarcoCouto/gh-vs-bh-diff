package com.google.android.gms.internal.ads;

import android.content.Context;
import android.os.Bundle;
import android.os.RemoteException;
import com.google.android.gms.ads.mediation.zza;
import com.google.android.gms.ads.mediation.zzb;
import com.google.android.gms.ads.zzc;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.dynamic.ObjectWrapper;
import com.smaato.sdk.core.api.VideoType;
import java.util.ArrayList;
import java.util.Iterator;
import org.json.JSONException;
import org.json.JSONObject;

public final class zzanm extends zzanh {
    private final zzbit zzdon;

    public zzanm(zzbit zzbit) {
        this.zzdon = zzbit;
    }

    public final void zza(String str, String str2, Bundle bundle, IObjectWrapper iObjectWrapper, zzamy zzamy, zzalm zzalm, zzwf zzwf) throws RemoteException {
        try {
            zzann zzann = new zzann(this, zzamy, zzalm);
            zzbit zzbit = this.zzdon;
            new zzbis((Context) ObjectWrapper.unwrap(iObjectWrapper), str, zzcx(str2), bundle);
            zzc.zza(zzwf.width, zzwf.height, zzwf.zzckk);
            zzann.zzbw(String.valueOf(zzbit.getClass().getSimpleName()).concat(" does not support banner ads."));
        } catch (Throwable th) {
            zzbbd.zzb("Adapter failed to render banner ad.", th);
            throw new RemoteException();
        }
    }

    public final void zza(String str, String str2, Bundle bundle, IObjectWrapper iObjectWrapper, zzana zzana, zzalm zzalm) throws RemoteException {
        try {
            zzano zzano = new zzano(this, zzana, zzalm);
            zzbit zzbit = this.zzdon;
            new zzbis((Context) ObjectWrapper.unwrap(iObjectWrapper), str, zzcx(str2), bundle);
            zzano.zzbw(String.valueOf(zzbit.getClass().getSimpleName()).concat(" does not support interstitial ads."));
        } catch (Throwable th) {
            zzbbd.zzb("Adapter failed to render interstitial ad.", th);
            throw new RemoteException();
        }
    }

    public final void zza(String str, String str2, Bundle bundle, IObjectWrapper iObjectWrapper, zzane zzane, zzalm zzalm) throws RemoteException {
        try {
            zzanp zzanp = new zzanp(this, zzane, zzalm);
            zzbit zzbit = this.zzdon;
            new zzbis((Context) ObjectWrapper.unwrap(iObjectWrapper), str, zzcx(str2), bundle);
            zzanp.zzbw(String.valueOf(zzbit.getClass().getSimpleName()).concat(" does not support rewarded ads."));
        } catch (Throwable th) {
            zzbbd.zzb("Adapter failed to render rewarded ad.", th);
            throw new RemoteException();
        }
    }

    public final void zza(String str, String str2, Bundle bundle, IObjectWrapper iObjectWrapper, zzanc zzanc, zzalm zzalm) throws RemoteException {
        try {
            zzanq zzanq = new zzanq(this, zzanc, zzalm);
            zzbit zzbit = this.zzdon;
            new zzbis((Context) ObjectWrapper.unwrap(iObjectWrapper), str, zzcx(str2), bundle);
            zzanq.zzbw(String.valueOf(zzbit.getClass().getSimpleName()).concat(" does not support native ads."));
        } catch (Throwable th) {
            zzbbd.zzb("Adapter failed to render rewarded ad.", th);
            throw new RemoteException();
        }
    }

    public final void showInterstitial() throws RemoteException {
        zzbiq zzbiq = null;
        try {
            zzbiq.zzxh();
        } catch (Throwable th) {
            zzbbd.zzb("", th);
            throw new RemoteException();
        }
    }

    public final void zzvk() throws RemoteException {
        zzbir zzbir = null;
        try {
            zzbir.zzxh();
        } catch (Throwable th) {
            zzbbd.zzb("", th);
            throw new RemoteException();
        }
    }

    public final zzyp getVideoController() {
        if (!(this.zzdon instanceof zzb)) {
            return null;
        }
        try {
            return ((zzb) this.zzdon).getVideoController();
        } catch (Throwable th) {
            zzbbd.zzb("", th);
            return null;
        }
    }

    /* JADX WARNING: type inference failed for: r0v0, types: [com.google.android.gms.internal.ads.zzbiv, com.google.android.gms.internal.ads.zzanr] */
    /* JADX WARNING: Multi-variable type inference failed. Error: jadx.core.utils.exceptions.JadxRuntimeException: No candidate types for var: r0v0, types: [com.google.android.gms.internal.ads.zzbiv, com.google.android.gms.internal.ads.zzanr]
  assigns: [com.google.android.gms.internal.ads.zzanr]
  uses: [com.google.android.gms.internal.ads.zzbiv]
  mth insns count: 18
    	at jadx.core.dex.visitors.typeinference.TypeSearch.fillTypeCandidates(TypeSearch.java:237)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.typeinference.TypeSearch.run(TypeSearch.java:53)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.runMultiVariableSearch(TypeInferenceVisitor.java:99)
    	at jadx.core.dex.visitors.typeinference.TypeInferenceVisitor.visit(TypeInferenceVisitor.java:92)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:27)
    	at jadx.core.dex.visitors.DepthTraversal.lambda$visit$1(DepthTraversal.java:14)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.dex.visitors.DepthTraversal.visit(DepthTraversal.java:14)
    	at jadx.core.ProcessClass.process(ProcessClass.java:30)
    	at jadx.core.ProcessClass.lambda$processDependencies$0(ProcessClass.java:49)
    	at java.util.ArrayList.forEach(Unknown Source)
    	at jadx.core.ProcessClass.processDependencies(ProcessClass.java:49)
    	at jadx.core.ProcessClass.process(ProcessClass.java:35)
    	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:311)
    	at jadx.api.JavaClass.decompile(JavaClass.java:62)
    	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:217)
     */
    /* JADX WARNING: Unknown variable types count: 1 */
    public final void zza(IObjectWrapper iObjectWrapper, String str, Bundle bundle, Bundle bundle2, zzwf zzwf, zzanj zzanj) throws RemoteException {
        try {
            ? zzanr = new zzanr(this, zzanj);
            this.zzdon.zza(new zzbiu((Context) ObjectWrapper.unwrap(iObjectWrapper), new zza(zzcw(str), bundle2), bundle, zzc.zza(zzwf.width, zzwf.height, zzwf.zzckk)), zzanr);
        } catch (Throwable th) {
            zzbbd.zzb("Error generating signals for RTB", th);
            throw new RemoteException();
        }
    }

    public final zzans zzvi() throws RemoteException {
        return zzans.zza(this.zzdon.zzafj());
    }

    public final zzans zzvj() throws RemoteException {
        return zzans.zza(this.zzdon.zzafi());
    }

    public final void zzn(IObjectWrapper iObjectWrapper) throws RemoteException {
        this.zzdon.initialize((Context) ObjectWrapper.unwrap(iObjectWrapper));
    }

    public final void zza(String[] strArr, Bundle[] bundleArr) throws RemoteException {
        ArrayList arrayList = new ArrayList();
        int i = 0;
        while (i < strArr.length) {
            try {
                arrayList.add(new zza(zzcw(strArr[i]), bundleArr[i]));
                i++;
            } catch (IndexOutOfBoundsException unused) {
                throw new RemoteException();
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x0045  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x004d  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0050  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0053  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0056  */
    private static int zzcw(String str) {
        char c;
        int hashCode = str.hashCode();
        if (hashCode == -1396342996) {
            if (str.equals("banner")) {
                c = 0;
                switch (c) {
                    case 0:
                        break;
                    case 1:
                        break;
                    case 2:
                        break;
                    case 3:
                        break;
                }
            }
        } else if (hashCode == -1052618729) {
            if (str.equals("native")) {
                c = 3;
                switch (c) {
                    case 0:
                        break;
                    case 1:
                        break;
                    case 2:
                        break;
                    case 3:
                        break;
                }
            }
        } else if (hashCode == -239580146) {
            if (str.equals("rewarded")) {
                c = 2;
                switch (c) {
                    case 0:
                        break;
                    case 1:
                        break;
                    case 2:
                        break;
                    case 3:
                        break;
                }
            }
        } else if (hashCode == 604727084 && str.equals(VideoType.INTERSTITIAL)) {
            c = 1;
            switch (c) {
                case 0:
                    return com.google.android.gms.ads.zza.zzvi;
                case 1:
                    return com.google.android.gms.ads.zza.zzvj;
                case 2:
                    return com.google.android.gms.ads.zza.zzvk;
                case 3:
                    return com.google.android.gms.ads.zza.zzvl;
                default:
                    throw new IllegalArgumentException("Internal Error");
            }
        }
        c = 65535;
        switch (c) {
            case 0:
                break;
            case 1:
                break;
            case 2:
                break;
            case 3:
                break;
        }
    }

    private static Bundle zzcx(String str) throws RemoteException {
        String str2 = "Server parameters: ";
        String valueOf = String.valueOf(str);
        zzbbd.zzeo(valueOf.length() != 0 ? str2.concat(valueOf) : new String(str2));
        try {
            Bundle bundle = new Bundle();
            if (str == null) {
                return bundle;
            }
            JSONObject jSONObject = new JSONObject(str);
            Bundle bundle2 = new Bundle();
            Iterator keys = jSONObject.keys();
            while (keys.hasNext()) {
                String str3 = (String) keys.next();
                bundle2.putString(str3, jSONObject.getString(str3));
            }
            return bundle2;
        } catch (JSONException e) {
            zzbbd.zzb("", e);
            throw new RemoteException();
        }
    }
}
