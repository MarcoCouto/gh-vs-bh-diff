package com.google.android.gms.internal.ads;

import com.google.android.gms.internal.ads.zzuw.zza.C0029zza;

final class zzuz implements zzbri {
    static final zzbri zzccw = new zzuz();

    private zzuz() {
    }

    public final boolean zzcb(int i) {
        return C0029zza.zzca(i) != null;
    }
}
