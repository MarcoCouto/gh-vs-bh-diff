package com.google.android.gms.internal.measurement;

import com.google.android.exoplayer2.extractor.ts.PsExtractor;
import java.nio.ByteBuffer;

final class zzxq extends zzxn {
    zzxq() {
    }

    /* access modifiers changed from: 0000 */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0065, code lost:
        return -1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x00bb, code lost:
        return -1;
     */
    public final int zzb(int i, byte[] bArr, int i2, int i3) {
        int i4;
        byte[] bArr2 = bArr;
        int i5 = i2;
        int i6 = i3;
        if ((i5 | i6 | (bArr2.length - i6)) >= 0) {
            long j = (long) i5;
            int i7 = (int) (((long) i6) - j);
            if (i7 >= 16) {
                long j2 = j;
                i4 = 0;
                while (true) {
                    if (i4 >= i7) {
                        i4 = i7;
                        break;
                    }
                    long j3 = j2 + 1;
                    if (zzxj.zza(bArr2, j2) < 0) {
                        break;
                    }
                    i4++;
                    j2 = j3;
                }
            } else {
                i4 = 0;
            }
            int i8 = i7 - i4;
            long j4 = j + ((long) i4);
            while (true) {
                int i9 = 0;
                while (true) {
                    if (i8 <= 0) {
                        break;
                    }
                    long j5 = j4 + 1;
                    byte zza = zzxj.zza(bArr2, j4);
                    if (zza < 0) {
                        long j6 = j5;
                        i9 = zza;
                        j4 = j6;
                        break;
                    }
                    i8--;
                    long j7 = j5;
                    i9 = zza;
                    j4 = j7;
                }
                if (i8 != 0) {
                    int i10 = i8 - 1;
                    if (i9 >= -32) {
                        if (i9 >= -16) {
                            if (i10 >= 3) {
                                i8 = i10 - 3;
                                long j8 = j4 + 1;
                                byte zza2 = zzxj.zza(bArr2, j4);
                                if (zza2 > -65 || (((i9 << 28) + (zza2 + 112)) >> 30) != 0) {
                                    break;
                                }
                                long j9 = j8 + 1;
                                if (zzxj.zza(bArr2, j8) > -65) {
                                    break;
                                }
                                j4 = j9 + 1;
                                if (zzxj.zza(bArr2, j9) > -65) {
                                    break;
                                }
                            } else {
                                return zza(bArr2, i9, j4, i10);
                            }
                        } else if (i10 >= 2) {
                            i8 = i10 - 2;
                            long j10 = j4 + 1;
                            byte zza3 = zzxj.zza(bArr2, j4);
                            if (zza3 > -65 || ((i9 == -32 && zza3 < -96) || (i9 == -19 && zza3 >= -96))) {
                                break;
                            }
                            j4 = j10 + 1;
                            if (zzxj.zza(bArr2, j10) > -65) {
                                break;
                            }
                        } else {
                            return zza(bArr2, i9, j4, i10);
                        }
                    } else if (i10 != 0) {
                        i8 = i10 - 1;
                        if (i9 < -62) {
                            break;
                        }
                        long j11 = j4 + 1;
                        if (zzxj.zza(bArr2, j4) > -65) {
                            break;
                        }
                        j4 = j11;
                    } else {
                        return i9;
                    }
                } else {
                    return 0;
                }
            }
            return -1;
        }
        throw new ArrayIndexOutOfBoundsException(String.format("Array length=%d, index=%d, limit=%d", new Object[]{Integer.valueOf(bArr2.length), Integer.valueOf(i2), Integer.valueOf(i3)}));
    }

    /* access modifiers changed from: 0000 */
    public final String zzh(byte[] bArr, int i, int i2) throws zzuv {
        if ((i | i2 | ((bArr.length - i) - i2)) >= 0) {
            int i3 = i + i2;
            char[] cArr = new char[i2];
            int i4 = 0;
            while (r13 < i3) {
                byte zza = zzxj.zza(bArr, (long) r13);
                if (!zzxm.zzd(zza)) {
                    break;
                }
                i = r13 + 1;
                int i5 = i4 + 1;
                zzxm.zza(zza, cArr, i4);
                i4 = i5;
            }
            int i6 = i4;
            while (r13 < i3) {
                int i7 = r13 + 1;
                byte zza2 = zzxj.zza(bArr, (long) r13);
                if (zzxm.zzd(zza2)) {
                    int i8 = i6 + 1;
                    zzxm.zza(zza2, cArr, i6);
                    while (i7 < i3) {
                        byte zza3 = zzxj.zza(bArr, (long) i7);
                        if (!zzxm.zzd(zza3)) {
                            break;
                        }
                        i7++;
                        int i9 = i8 + 1;
                        zzxm.zza(zza3, cArr, i8);
                        i8 = i9;
                    }
                    r13 = i7;
                    i6 = i8;
                } else if (zzxm.zze(zza2)) {
                    if (i7 < i3) {
                        int i10 = i7 + 1;
                        int i11 = i6 + 1;
                        zzxm.zza(zza2, zzxj.zza(bArr, (long) i7), cArr, i6);
                        r13 = i10;
                        i6 = i11;
                    } else {
                        throw zzuv.zzwx();
                    }
                } else if (zzxm.zzf(zza2)) {
                    if (i7 < i3 - 1) {
                        int i12 = i7 + 1;
                        int i13 = i12 + 1;
                        int i14 = i6 + 1;
                        zzxm.zza(zza2, zzxj.zza(bArr, (long) i7), zzxj.zza(bArr, (long) i12), cArr, i6);
                        r13 = i13;
                        i6 = i14;
                    } else {
                        throw zzuv.zzwx();
                    }
                } else if (i7 < i3 - 2) {
                    int i15 = i7 + 1;
                    byte zza4 = zzxj.zza(bArr, (long) i7);
                    int i16 = i15 + 1;
                    int i17 = i16 + 1;
                    int i18 = i6 + 1;
                    zzxm.zza(zza2, zza4, zzxj.zza(bArr, (long) i15), zzxj.zza(bArr, (long) i16), cArr, i6);
                    r13 = i17;
                    i6 = i18 + 1;
                } else {
                    throw zzuv.zzwx();
                }
            }
            return new String(cArr, 0, i6);
        }
        throw new ArrayIndexOutOfBoundsException(String.format("buffer length=%d, index=%d, size=%d", new Object[]{Integer.valueOf(bArr.length), Integer.valueOf(i), Integer.valueOf(i2)}));
    }

    /* access modifiers changed from: 0000 */
    public final int zzb(CharSequence charSequence, byte[] bArr, int i, int i2) {
        long j;
        long j2;
        CharSequence charSequence2 = charSequence;
        byte[] bArr2 = bArr;
        int i3 = i;
        int i4 = i2;
        long j3 = (long) i3;
        long j4 = ((long) i4) + j3;
        int length = charSequence.length();
        if (length > i4 || bArr2.length - i4 < i3) {
            char charAt = charSequence2.charAt(length - 1);
            int i5 = i3 + i4;
            StringBuilder sb = new StringBuilder(37);
            sb.append("Failed writing ");
            sb.append(charAt);
            sb.append(" at index ");
            sb.append(i5);
            throw new ArrayIndexOutOfBoundsException(sb.toString());
        }
        int i6 = 0;
        while (i6 < length) {
            char charAt2 = charSequence2.charAt(i6);
            if (charAt2 >= 128) {
                break;
            }
            long j5 = 1 + j;
            zzxj.zza(bArr2, j, (byte) charAt2);
            i6++;
            j3 = j5;
        }
        if (i6 == length) {
            return (int) j;
        }
        while (i6 < length) {
            char charAt3 = charSequence2.charAt(i6);
            if (charAt3 < 128 && j < j4) {
                j2 = j + 1;
                zzxj.zza(bArr2, j, (byte) charAt3);
            } else if (charAt3 < 2048 && j <= j4 - 2) {
                long j6 = j + 1;
                zzxj.zza(bArr2, j, (byte) ((charAt3 >>> 6) | 960));
                j = j6 + 1;
                zzxj.zza(bArr2, j6, (byte) ((charAt3 & '?') | 128));
                i6++;
            } else if ((charAt3 < 55296 || 57343 < charAt3) && j <= j4 - 3) {
                long j7 = j + 1;
                zzxj.zza(bArr2, j, (byte) ((charAt3 >>> 12) | 480));
                long j8 = j7 + 1;
                zzxj.zza(bArr2, j7, (byte) (((charAt3 >>> 6) & 63) | 128));
                j2 = j8 + 1;
                zzxj.zza(bArr2, j8, (byte) ((charAt3 & '?') | 128));
            } else if (j <= j4 - 4) {
                int i7 = i6 + 1;
                if (i7 != length) {
                    char charAt4 = charSequence2.charAt(i7);
                    if (Character.isSurrogatePair(charAt3, charAt4)) {
                        int codePoint = Character.toCodePoint(charAt3, charAt4);
                        long j9 = j + 1;
                        zzxj.zza(bArr2, j, (byte) ((codePoint >>> 18) | PsExtractor.VIDEO_STREAM_MASK));
                        long j10 = j9 + 1;
                        zzxj.zza(bArr2, j9, (byte) (((codePoint >>> 12) & 63) | 128));
                        long j11 = j10 + 1;
                        zzxj.zza(bArr2, j10, (byte) (((codePoint >>> 6) & 63) | 128));
                        j = j11 + 1;
                        zzxj.zza(bArr2, j11, (byte) ((codePoint & 63) | 128));
                        i6 = i7;
                        i6++;
                    }
                } else {
                    i7 = i6;
                }
                throw new zzxp(i7 - 1, length);
            } else {
                if (55296 <= charAt3 && charAt3 <= 57343) {
                    int i8 = i6 + 1;
                    if (i8 == length || !Character.isSurrogatePair(charAt3, charSequence2.charAt(i8))) {
                        throw new zzxp(i6, length);
                    }
                }
                StringBuilder sb2 = new StringBuilder(46);
                sb2.append("Failed writing ");
                sb2.append(charAt3);
                sb2.append(" at index ");
                sb2.append(j);
                throw new ArrayIndexOutOfBoundsException(sb2.toString());
            }
            j = j2;
            i6++;
        }
        return (int) j;
    }

    /* access modifiers changed from: 0000 */
    public final void zzb(CharSequence charSequence, ByteBuffer byteBuffer) {
        char c;
        long j;
        long j2;
        long j3;
        CharSequence charSequence2 = charSequence;
        ByteBuffer byteBuffer2 = byteBuffer;
        long zzb = zzxj.zzb(byteBuffer);
        long position = ((long) byteBuffer.position()) + zzb;
        long limit = ((long) byteBuffer.limit()) + zzb;
        int length = charSequence.length();
        if (((long) length) <= limit - position) {
            int i = 0;
            while (true) {
                c = 128;
                j = 1;
                if (i >= length) {
                    break;
                }
                char charAt = charSequence2.charAt(i);
                if (charAt >= 128) {
                    break;
                }
                long j4 = 1 + position;
                zzxj.zza(position, (byte) charAt);
                i++;
                position = j4;
            }
            if (i == length) {
                byteBuffer2.position((int) (position - zzb));
                return;
            }
            while (i < length) {
                char charAt2 = charSequence2.charAt(i);
                if (charAt2 < c && position < limit) {
                    j2 = position + j;
                    zzxj.zza(position, (byte) charAt2);
                } else if (charAt2 < 2048 && position <= limit - 2) {
                    long j5 = position + j;
                    zzxj.zza(position, (byte) ((charAt2 >>> 6) | 960));
                    long j6 = j5 + j;
                    zzxj.zza(j5, (byte) ((charAt2 & '?') | 128));
                    j2 = j6;
                } else if ((charAt2 < 55296 || 57343 < charAt2) && position <= limit - 3) {
                    long j7 = position + j;
                    zzxj.zza(position, (byte) ((charAt2 >>> 12) | 480));
                    long j8 = j7 + j;
                    zzxj.zza(j7, (byte) (((charAt2 >>> 6) & 63) | 128));
                    long j9 = j8 + 1;
                    zzxj.zza(j8, (byte) ((charAt2 & '?') | 128));
                    j2 = j9;
                    j3 = 1;
                    i++;
                    j = j3;
                    position = j2;
                    c = 128;
                } else if (position <= limit - 4) {
                    int i2 = i + 1;
                    if (i2 != length) {
                        char charAt3 = charSequence2.charAt(i2);
                        if (Character.isSurrogatePair(charAt2, charAt3)) {
                            int codePoint = Character.toCodePoint(charAt2, charAt3);
                            long j10 = position + 1;
                            zzxj.zza(position, (byte) ((codePoint >>> 18) | PsExtractor.VIDEO_STREAM_MASK));
                            long j11 = j10 + 1;
                            zzxj.zza(j10, (byte) (((codePoint >>> 12) & 63) | 128));
                            long j12 = j11 + 1;
                            zzxj.zza(j11, (byte) (((codePoint >>> 6) & 63) | 128));
                            j3 = 1;
                            long j13 = j12 + 1;
                            zzxj.zza(j12, (byte) ((codePoint & 63) | 128));
                            i = i2;
                            j2 = j13;
                            i++;
                            j = j3;
                            position = j2;
                            c = 128;
                        } else {
                            i = i2;
                        }
                    }
                    throw new zzxp(i - 1, length);
                } else {
                    if (55296 <= charAt2 && charAt2 <= 57343) {
                        int i3 = i + 1;
                        if (i3 == length || !Character.isSurrogatePair(charAt2, charSequence2.charAt(i3))) {
                            throw new zzxp(i, length);
                        }
                    }
                    StringBuilder sb = new StringBuilder(46);
                    sb.append("Failed writing ");
                    sb.append(charAt2);
                    sb.append(" at index ");
                    sb.append(position);
                    throw new ArrayIndexOutOfBoundsException(sb.toString());
                }
                j3 = j;
                i++;
                j = j3;
                position = j2;
                c = 128;
            }
            byteBuffer2.position((int) (position - zzb));
            return;
        }
        char charAt4 = charSequence2.charAt(length - 1);
        int limit2 = byteBuffer.limit();
        StringBuilder sb2 = new StringBuilder(37);
        sb2.append("Failed writing ");
        sb2.append(charAt4);
        sb2.append(" at index ");
        sb2.append(limit2);
        throw new ArrayIndexOutOfBoundsException(sb2.toString());
    }

    private static int zza(byte[] bArr, int i, long j, int i2) {
        switch (i2) {
            case 0:
                return zzxl.zzbz(i);
            case 1:
                return zzxl.zzq(i, zzxj.zza(bArr, j));
            case 2:
                return zzxl.zzc(i, zzxj.zza(bArr, j), zzxj.zza(bArr, j + 1));
            default:
                throw new AssertionError();
        }
    }
}
