package com.google.android.gms.internal.measurement;

import com.google.android.gms.internal.measurement.zzfe.zzb.C0031zzb;

final class zzfh implements zzut {
    static final zzut zzoc = new zzfh();

    private zzfh() {
    }

    public final boolean zzb(int i) {
        return C0031zzb.zzt(i) != null;
    }
}
