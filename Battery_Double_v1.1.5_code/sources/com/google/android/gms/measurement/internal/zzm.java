package com.google.android.gms.measurement.internal;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.support.annotation.WorkerThread;
import android.support.v4.util.ArrayMap;
import android.text.TextUtils;
import android.util.Pair;
import com.github.mikephil.charting.utils.Utils;
import com.google.android.gms.common.internal.Preconditions;
import com.google.android.gms.common.util.VisibleForTesting;
import com.google.android.gms.internal.measurement.zzfj;
import com.google.android.gms.internal.measurement.zzfk;
import com.google.android.gms.internal.measurement.zzfl;
import com.google.android.gms.internal.measurement.zzfm;
import com.google.android.gms.internal.measurement.zzfn;
import com.google.android.gms.internal.measurement.zzfr;
import com.google.android.gms.internal.measurement.zzfs;
import com.google.android.gms.internal.measurement.zzft;
import com.google.android.gms.internal.measurement.zzfu;
import com.google.android.gms.internal.measurement.zzfx;
import com.google.android.gms.internal.measurement.zzfy;
import com.google.android.gms.internal.measurement.zzfz;
import com.google.android.gms.internal.measurement.zzya;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

final class zzm extends zzfn {
    zzm(zzfo zzfo) {
        super(zzfo);
    }

    /* access modifiers changed from: protected */
    public final boolean zzgy() {
        return false;
    }

    /* access modifiers changed from: 0000 */
    /* JADX WARNING: Removed duplicated region for block: B:106:0x02f3  */
    /* JADX WARNING: Removed duplicated region for block: B:110:0x0330  */
    /* JADX WARNING: Removed duplicated region for block: B:124:0x0393  */
    /* JADX WARNING: Removed duplicated region for block: B:125:0x03e5  */
    /* JADX WARNING: Removed duplicated region for block: B:128:0x0439  */
    /* JADX WARNING: Removed duplicated region for block: B:135:0x045a  */
    /* JADX WARNING: Removed duplicated region for block: B:245:0x08c8  */
    /* JADX WARNING: Removed duplicated region for block: B:394:0x0aa2 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x01d8  */
    /* JADX WARNING: Removed duplicated region for block: B:93:0x02ba  */
    /* JADX WARNING: Removed duplicated region for block: B:99:0x02d7  */
    @WorkerThread
    public final zzfr[] zza(String str, zzft[] zzftArr, zzfz[] zzfzArr) {
        ArrayMap arrayMap;
        Iterator it;
        ArrayMap arrayMap2;
        ArrayMap arrayMap3;
        zzfy[] zzfyArr;
        ArrayMap arrayMap4;
        int i;
        Map map;
        ArrayMap arrayMap5;
        ArrayMap arrayMap6;
        ArrayMap arrayMap7;
        Map map2;
        ArrayMap arrayMap8;
        Map map3;
        Map map4;
        Iterator it2;
        ArrayMap arrayMap9;
        ArrayMap arrayMap10;
        ArrayMap arrayMap11;
        ArrayMap arrayMap12;
        ArrayMap arrayMap13;
        Map map5;
        ArrayMap arrayMap14;
        Object obj;
        ArrayMap arrayMap15;
        ArrayMap arrayMap16;
        ArrayMap arrayMap17;
        ArrayMap arrayMap18;
        ArrayMap arrayMap19;
        ArrayMap arrayMap20;
        int i2;
        int i3;
        HashSet hashSet;
        zzft zzft;
        ArrayMap arrayMap21;
        zzfu[] zzfuArr;
        Long l;
        boolean z;
        zzft zzft2;
        String str2;
        zzac zzg;
        ArrayMap arrayMap22;
        ArrayMap arrayMap23;
        HashSet hashSet2;
        ArrayMap arrayMap24;
        ArrayMap arrayMap25;
        ArrayMap arrayMap26;
        String str3;
        zzft zzft3;
        zzac zzac;
        Map map6;
        Iterator it3;
        Map map7;
        Iterator it4;
        BitSet bitSet;
        ArrayMap arrayMap27;
        ArrayMap arrayMap28;
        Map map8;
        Map map9;
        ArrayMap arrayMap29;
        Map map10;
        BitSet bitSet2;
        HashSet hashSet3;
        ArrayMap arrayMap30;
        ArrayMap arrayMap31;
        String str4;
        ArrayMap arrayMap32;
        ArrayMap arrayMap33;
        ArrayMap arrayMap34;
        zzft zzft4;
        Map map11;
        BitSet bitSet3;
        BitSet bitSet4;
        BitSet bitSet5;
        ArrayMap arrayMap35;
        int i4;
        int i5;
        boolean z2;
        Long l2;
        ArrayMap arrayMap36;
        Long l3;
        zzft zzft5;
        long j;
        zzft zzft6;
        int length;
        int i6;
        int i7;
        zzft zzft7;
        HashSet hashSet4;
        BitSet bitSet6;
        Iterator it5;
        Map map12;
        ArrayMap arrayMap37;
        BitSet bitSet7;
        ArrayMap arrayMap38;
        ArrayMap arrayMap39;
        ArrayMap arrayMap40;
        boolean z3;
        String str5 = str;
        zzft[] zzftArr2 = zzftArr;
        zzfz[] zzfzArr2 = zzfzArr;
        Preconditions.checkNotEmpty(str);
        HashSet hashSet5 = new HashSet();
        ArrayMap arrayMap41 = new ArrayMap();
        ArrayMap arrayMap42 = new ArrayMap();
        ArrayMap arrayMap43 = new ArrayMap();
        ArrayMap arrayMap44 = new ArrayMap();
        ArrayMap arrayMap45 = new ArrayMap();
        boolean zzbb = zzgv().zzbb(str5);
        Map zzbp = zzjt().zzbp(str5);
        if (zzbp != null) {
            Iterator it6 = zzbp.keySet().iterator();
            while (it6.hasNext()) {
                int intValue = ((Integer) it6.next()).intValue();
                zzfx zzfx = (zzfx) zzbp.get(Integer.valueOf(intValue));
                BitSet bitSet8 = (BitSet) arrayMap42.get(Integer.valueOf(intValue));
                BitSet bitSet9 = (BitSet) arrayMap43.get(Integer.valueOf(intValue));
                if (zzbb) {
                    map12 = zzbp;
                    arrayMap37 = new ArrayMap();
                    if (zzfx != null) {
                        it5 = it6;
                        if (zzfx.zzayp != null) {
                            zzfs[] zzfsArr = zzfx.zzayp;
                            bitSet6 = bitSet9;
                            int length2 = zzfsArr.length;
                            hashSet4 = hashSet5;
                            int i8 = 0;
                            while (i8 < length2) {
                                int i9 = length2;
                                zzfs zzfs = zzfsArr[i8];
                                zzfs[] zzfsArr2 = zzfsArr;
                                if (zzfs.zzawx != null) {
                                    arrayMap37.put(zzfs.zzawx, zzfs.zzawy);
                                }
                                i8++;
                                length2 = i9;
                                zzfsArr = zzfsArr2;
                            }
                            arrayMap44.put(Integer.valueOf(intValue), arrayMap37);
                        }
                    } else {
                        it5 = it6;
                    }
                    bitSet6 = bitSet9;
                    hashSet4 = hashSet5;
                    arrayMap44.put(Integer.valueOf(intValue), arrayMap37);
                } else {
                    map12 = zzbp;
                    it5 = it6;
                    bitSet6 = bitSet9;
                    hashSet4 = hashSet5;
                    arrayMap37 = null;
                }
                if (bitSet8 == null) {
                    bitSet8 = new BitSet();
                    arrayMap42.put(Integer.valueOf(intValue), bitSet8);
                    bitSet7 = new BitSet();
                    arrayMap43.put(Integer.valueOf(intValue), bitSet7);
                } else {
                    bitSet7 = bitSet6;
                }
                int i10 = 0;
                while (i10 < (zzfx.zzayn.length << 6)) {
                    if (zzfu.zza(zzfx.zzayn, i10)) {
                        arrayMap40 = arrayMap44;
                        arrayMap39 = arrayMap43;
                        arrayMap38 = arrayMap42;
                        zzgt().zzjo().zze("Filter already evaluated. audience ID, filter ID", Integer.valueOf(intValue), Integer.valueOf(i10));
                        bitSet7.set(i10);
                        if (zzfu.zza(zzfx.zzayo, i10)) {
                            bitSet8.set(i10);
                            z3 = true;
                            if (arrayMap37 != null && !z3) {
                                arrayMap37.remove(Integer.valueOf(i10));
                            }
                            i10++;
                            arrayMap44 = arrayMap40;
                            arrayMap43 = arrayMap39;
                            arrayMap42 = arrayMap38;
                        }
                    } else {
                        arrayMap40 = arrayMap44;
                        arrayMap39 = arrayMap43;
                        arrayMap38 = arrayMap42;
                    }
                    z3 = false;
                    arrayMap37.remove(Integer.valueOf(i10));
                    i10++;
                    arrayMap44 = arrayMap40;
                    arrayMap43 = arrayMap39;
                    arrayMap42 = arrayMap38;
                }
                ArrayMap arrayMap46 = arrayMap44;
                ArrayMap arrayMap47 = arrayMap43;
                ArrayMap arrayMap48 = arrayMap42;
                zzfr zzfr = new zzfr();
                arrayMap41.put(Integer.valueOf(intValue), zzfr);
                zzfr.zzawv = Boolean.valueOf(false);
                zzfr.zzawu = zzfx;
                zzfr.zzawt = new zzfx();
                zzfr.zzawt.zzayo = zzfu.zza(bitSet8);
                zzfr.zzawt.zzayn = zzfu.zza(bitSet7);
                if (zzbb) {
                    zzfr.zzawt.zzayp = zzb(arrayMap37);
                    arrayMap45.put(Integer.valueOf(intValue), new ArrayMap());
                }
                zzbp = map12;
                it6 = it5;
                hashSet5 = hashSet4;
                arrayMap44 = arrayMap46;
                arrayMap43 = arrayMap47;
                arrayMap42 = arrayMap48;
            }
        }
        ArrayMap arrayMap49 = arrayMap44;
        ArrayMap arrayMap50 = arrayMap43;
        ArrayMap arrayMap51 = arrayMap42;
        HashSet hashSet6 = hashSet5;
        if (zzftArr2 != null) {
            ArrayMap arrayMap52 = new ArrayMap();
            int length3 = zzftArr2.length;
            long j2 = 0;
            zzft zzft8 = null;
            Long l4 = null;
            int i11 = 0;
            while (i11 < length3) {
                zzft zzft9 = zzftArr2[i11];
                String str6 = zzft9.name;
                zzfu[] zzfuArr2 = zzft9.zzaxa;
                long j3 = j2;
                if (zzgv().zzd(str5, zzai.zzaki)) {
                    zzjr();
                    Long l5 = (Long) zzfu.zzb(zzft9, "_eid");
                    boolean z4 = l5 != null;
                    if (z4) {
                        i5 = i11;
                        if (str6.equals("_ep")) {
                            z2 = true;
                            if (!z2) {
                                zzjr();
                                String str7 = (String) zzfu.zzb(zzft9, "_en");
                                if (TextUtils.isEmpty(str7)) {
                                    zzgt().zzjg().zzg("Extra parameter without an event name. eventId", l5);
                                    arrayMap36 = arrayMap45;
                                    i3 = i5;
                                } else {
                                    if (zzft8 == null || l4 == null || l5.longValue() != l4.longValue()) {
                                        Pair zza = zzjt().zza(str5, l5);
                                        if (zza == null || zza.first == null) {
                                            arrayMap36 = arrayMap45;
                                            i3 = i5;
                                            zzgt().zzjg().zze("Extra parameter without existing main event. eventName, eventId", str7, l5);
                                        } else {
                                            zzft zzft10 = (zzft) zza.first;
                                            j = ((Long) zza.second).longValue();
                                            zzjr();
                                            l3 = (Long) zzfu.zzb(zzft10, "_eid");
                                            zzft5 = zzft10;
                                        }
                                    } else {
                                        zzft5 = zzft8;
                                        l3 = l4;
                                        j = j3;
                                    }
                                    long j4 = j - 1;
                                    if (j4 <= 0) {
                                        zzt zzjt = zzjt();
                                        zzjt.zzaf();
                                        zzjt.zzgt().zzjo().zzg("Clearing complex main event info. appId", str5);
                                        try {
                                            SQLiteDatabase writableDatabase = zzjt.getWritableDatabase();
                                            String str8 = "delete from main_event_params where app_id=?";
                                            zzft7 = zzft5;
                                            try {
                                                String[] strArr = new String[1];
                                                try {
                                                    strArr[0] = str5;
                                                    writableDatabase.execSQL(str8, strArr);
                                                } catch (SQLiteException e) {
                                                    e = e;
                                                }
                                            } catch (SQLiteException e2) {
                                                e = e2;
                                                zzjt.zzgt().zzjg().zzg("Error clearing complex main event", e);
                                                zzft2 = zzft9;
                                                arrayMap21 = arrayMap45;
                                                i4 = i5;
                                                zzft6 = zzft7;
                                                z = true;
                                                zzfu[] zzfuArr3 = new zzfu[(zzft6.zzaxa.length + zzfuArr2.length)];
                                                zzfu[] zzfuArr4 = zzft6.zzaxa;
                                                length = zzfuArr4.length;
                                                i6 = 0;
                                                i7 = 0;
                                                while (i6 < length) {
                                                }
                                                zzft = zzft6;
                                                if (i7 > 0) {
                                                }
                                                l = l3;
                                                j3 = j4;
                                                zzg = zzjt().zzg(str5, zzft2.name);
                                                if (zzg == null) {
                                                }
                                                zzjt().zza(zzac);
                                                long j5 = zzac.zzahv;
                                                ArrayMap arrayMap53 = arrayMap25;
                                                map6 = (Map) arrayMap53.get(str2);
                                                if (map6 == null) {
                                                }
                                                Map map13 = map6;
                                                it3 = map13.keySet().iterator();
                                                while (it3.hasNext()) {
                                                }
                                                arrayMap20 = arrayMap53;
                                                arrayMap19 = arrayMap21;
                                                arrayMap17 = arrayMap26;
                                                arrayMap15 = arrayMap24;
                                                hashSet = hashSet2;
                                                arrayMap16 = arrayMap23;
                                                arrayMap18 = arrayMap22;
                                                l4 = l;
                                                j2 = j3;
                                                zzft8 = zzft;
                                                i11 = i3 + 1;
                                                zzftArr2 = zzftArr;
                                                hashSet6 = hashSet;
                                                length3 = i2;
                                                arrayMap52 = arrayMap20;
                                                arrayMap45 = arrayMap19;
                                                arrayMap49 = arrayMap18;
                                                arrayMap50 = arrayMap17;
                                                arrayMap41 = arrayMap16;
                                                arrayMap51 = arrayMap15;
                                                zzfzArr2 = zzfzArr;
                                                str5 = str;
                                            }
                                        } catch (SQLiteException e3) {
                                            e = e3;
                                            zzft7 = zzft5;
                                            zzjt.zzgt().zzjg().zzg("Error clearing complex main event", e);
                                            zzft2 = zzft9;
                                            arrayMap21 = arrayMap45;
                                            i4 = i5;
                                            zzft6 = zzft7;
                                            z = true;
                                            zzfu[] zzfuArr32 = new zzfu[(zzft6.zzaxa.length + zzfuArr2.length)];
                                            zzfu[] zzfuArr42 = zzft6.zzaxa;
                                            length = zzfuArr42.length;
                                            i6 = 0;
                                            i7 = 0;
                                            while (i6 < length) {
                                            }
                                            zzft = zzft6;
                                            if (i7 > 0) {
                                            }
                                            l = l3;
                                            j3 = j4;
                                            zzg = zzjt().zzg(str5, zzft2.name);
                                            if (zzg == null) {
                                            }
                                            zzjt().zza(zzac);
                                            long j52 = zzac.zzahv;
                                            ArrayMap arrayMap532 = arrayMap25;
                                            map6 = (Map) arrayMap532.get(str2);
                                            if (map6 == null) {
                                            }
                                            Map map132 = map6;
                                            it3 = map132.keySet().iterator();
                                            while (it3.hasNext()) {
                                            }
                                            arrayMap20 = arrayMap532;
                                            arrayMap19 = arrayMap21;
                                            arrayMap17 = arrayMap26;
                                            arrayMap15 = arrayMap24;
                                            hashSet = hashSet2;
                                            arrayMap16 = arrayMap23;
                                            arrayMap18 = arrayMap22;
                                            l4 = l;
                                            j2 = j3;
                                            zzft8 = zzft;
                                            i11 = i3 + 1;
                                            zzftArr2 = zzftArr;
                                            hashSet6 = hashSet;
                                            length3 = i2;
                                            arrayMap52 = arrayMap20;
                                            arrayMap45 = arrayMap19;
                                            arrayMap49 = arrayMap18;
                                            arrayMap50 = arrayMap17;
                                            arrayMap41 = arrayMap16;
                                            arrayMap51 = arrayMap15;
                                            zzfzArr2 = zzfzArr;
                                            str5 = str;
                                        }
                                        zzft2 = zzft9;
                                        arrayMap21 = arrayMap45;
                                        i4 = i5;
                                        zzft6 = zzft7;
                                        z = true;
                                    } else {
                                        zzft2 = zzft9;
                                        i4 = i5;
                                        zzft zzft11 = zzft5;
                                        z = true;
                                        arrayMap21 = arrayMap45;
                                        zzjt().zza(str, l5, j4, zzft11);
                                        zzft6 = zzft11;
                                    }
                                    zzfu[] zzfuArr322 = new zzfu[(zzft6.zzaxa.length + zzfuArr2.length)];
                                    zzfu[] zzfuArr422 = zzft6.zzaxa;
                                    length = zzfuArr422.length;
                                    i6 = 0;
                                    i7 = 0;
                                    while (i6 < length) {
                                        zzfu zzfu = zzfuArr422[i6];
                                        zzjr();
                                        zzft zzft12 = zzft6;
                                        if (zzfu.zza(zzft2, zzfu.name) == null) {
                                            int i12 = i7 + 1;
                                            zzfuArr322[i7] = zzfu;
                                            i7 = i12;
                                        }
                                        i6++;
                                        zzft6 = zzft12;
                                    }
                                    zzft = zzft6;
                                    if (i7 > 0) {
                                        int length4 = zzfuArr2.length;
                                        int i13 = 0;
                                        while (i13 < length4) {
                                            int i14 = i7 + 1;
                                            zzfuArr322[i7] = zzfuArr2[i13];
                                            i13++;
                                            i7 = i14;
                                        }
                                        if (i7 != zzfuArr322.length) {
                                            zzfuArr322 = (zzfu[]) Arrays.copyOf(zzfuArr322, i7);
                                        }
                                        zzfuArr = zzfuArr322;
                                        str2 = str7;
                                    } else {
                                        zzgt().zzjj().zzg("No unique parameters in main event. eventName", str7);
                                        str2 = str7;
                                        zzfuArr = zzfuArr2;
                                    }
                                    l = l3;
                                    j3 = j4;
                                }
                                i2 = length3;
                                arrayMap20 = arrayMap52;
                                arrayMap16 = arrayMap41;
                                hashSet = hashSet6;
                                arrayMap18 = arrayMap49;
                                arrayMap17 = arrayMap50;
                                arrayMap15 = arrayMap51;
                                j2 = j3;
                                arrayMap19 = arrayMap36;
                                i11 = i3 + 1;
                                zzftArr2 = zzftArr;
                                hashSet6 = hashSet;
                                length3 = i2;
                                arrayMap52 = arrayMap20;
                                arrayMap45 = arrayMap19;
                                arrayMap49 = arrayMap18;
                                arrayMap50 = arrayMap17;
                                arrayMap41 = arrayMap16;
                                arrayMap51 = arrayMap15;
                                zzfzArr2 = zzfzArr;
                                str5 = str;
                            } else {
                                zzft2 = zzft9;
                                arrayMap21 = arrayMap45;
                                i4 = i5;
                                z = true;
                                if (z4) {
                                    zzjr();
                                    Object valueOf = Long.valueOf(0);
                                    Object zzb = zzfu.zzb(zzft2, "_epc");
                                    if (zzb == null) {
                                        zzb = valueOf;
                                    }
                                    long longValue = ((Long) zzb).longValue();
                                    if (longValue <= 0) {
                                        zzgt().zzjj().zzg("Complex event with zero extra param count. eventName", str6);
                                        l2 = l5;
                                    } else {
                                        l2 = l5;
                                        zzjt().zza(str, l5, longValue, zzft2);
                                    }
                                    l = l2;
                                    str2 = str6;
                                    zzfuArr = zzfuArr2;
                                    zzft = zzft2;
                                    j3 = longValue;
                                }
                            }
                            zzg = zzjt().zzg(str5, zzft2.name);
                            if (zzg == null) {
                                zzgt().zzjj().zze("Event aggregate wasn't created during raw event logging. appId, event", zzas.zzbw(str), zzgq().zzbt(str2));
                                i2 = length3;
                                arrayMap25 = arrayMap52;
                                arrayMap26 = arrayMap50;
                                arrayMap24 = arrayMap51;
                                arrayMap22 = arrayMap49;
                                arrayMap23 = arrayMap41;
                                hashSet2 = hashSet6;
                                zzft3 = zzft2;
                                zzfz[] zzfzArr3 = zzfzArr2;
                                str3 = str5;
                                zzac = new zzac(str, zzft2.name, 1, 1, zzft2.zzaxb.longValue(), 0, null, null, null, null);
                            } else {
                                i2 = length3;
                                arrayMap25 = arrayMap52;
                                arrayMap23 = arrayMap41;
                                zzft3 = zzft2;
                                zzfz[] zzfzArr4 = zzfzArr2;
                                str3 = str5;
                                hashSet2 = hashSet6;
                                arrayMap22 = arrayMap49;
                                arrayMap26 = arrayMap50;
                                arrayMap24 = arrayMap51;
                                zzac zzac2 = new zzac(zzg.zztt, zzg.name, zzg.zzahv + 1, zzg.zzahw + 1, zzg.zzahx, zzg.zzahy, zzg.zzahz, zzg.zzaia, zzg.zzaib, zzg.zzaic);
                                zzac = zzac2;
                            }
                            zzjt().zza(zzac);
                            long j522 = zzac.zzahv;
                            ArrayMap arrayMap5322 = arrayMap25;
                            map6 = (Map) arrayMap5322.get(str2);
                            if (map6 == null) {
                                map6 = zzjt().zzl(str3, str2);
                                if (map6 == null) {
                                    map6 = new ArrayMap();
                                }
                                arrayMap5322.put(str2, map6);
                            }
                            Map map1322 = map6;
                            it3 = map1322.keySet().iterator();
                            while (it3.hasNext()) {
                                int intValue2 = ((Integer) it3.next()).intValue();
                                HashSet hashSet7 = hashSet2;
                                if (hashSet7.contains(Integer.valueOf(intValue2))) {
                                    zzgt().zzjo().zzg("Skipping failed audience ID", Integer.valueOf(intValue2));
                                    hashSet3 = hashSet7;
                                } else {
                                    ArrayMap arrayMap54 = arrayMap23;
                                    zzfr zzfr2 = (zzfr) arrayMap54.get(Integer.valueOf(intValue2));
                                    ArrayMap arrayMap55 = arrayMap24;
                                    BitSet bitSet10 = (BitSet) arrayMap55.get(Integer.valueOf(intValue2));
                                    zzft zzft13 = zzft3;
                                    ArrayMap arrayMap56 = arrayMap5322;
                                    ArrayMap arrayMap57 = arrayMap26;
                                    BitSet bitSet11 = (BitSet) arrayMap57.get(Integer.valueOf(intValue2));
                                    if (zzbb) {
                                        bitSet = bitSet11;
                                        it4 = it3;
                                        arrayMap27 = arrayMap22;
                                        map7 = (Map) arrayMap27.get(Integer.valueOf(intValue2));
                                        arrayMap28 = arrayMap21;
                                        map8 = (Map) arrayMap28.get(Integer.valueOf(intValue2));
                                    } else {
                                        bitSet = bitSet11;
                                        it4 = it3;
                                        arrayMap28 = arrayMap21;
                                        arrayMap27 = arrayMap22;
                                        map8 = null;
                                        map7 = null;
                                    }
                                    if (zzfr2 == null) {
                                        zzfr zzfr3 = new zzfr();
                                        arrayMap54.put(Integer.valueOf(intValue2), zzfr3);
                                        zzfr3.zzawv = Boolean.valueOf(z);
                                        bitSet10 = new BitSet();
                                        arrayMap55.put(Integer.valueOf(intValue2), bitSet10);
                                        BitSet bitSet12 = new BitSet();
                                        Map map14 = map8;
                                        arrayMap57.put(Integer.valueOf(intValue2), bitSet12);
                                        if (zzbb) {
                                            ArrayMap arrayMap58 = new ArrayMap();
                                            BitSet bitSet13 = bitSet12;
                                            arrayMap27.put(Integer.valueOf(intValue2), arrayMap58);
                                            ArrayMap arrayMap59 = new ArrayMap();
                                            Map map15 = arrayMap58;
                                            arrayMap28.put(Integer.valueOf(intValue2), arrayMap59);
                                            arrayMap29 = arrayMap28;
                                            bitSet2 = bitSet13;
                                            map9 = map15;
                                            map10 = arrayMap59;
                                        } else {
                                            arrayMap29 = arrayMap28;
                                            map9 = map7;
                                            map10 = map14;
                                            bitSet2 = bitSet12;
                                        }
                                    } else {
                                        Map map16 = map8;
                                        arrayMap29 = arrayMap28;
                                        bitSet2 = bitSet;
                                        map9 = map7;
                                        map10 = map16;
                                    }
                                    for (zzfj zzfj : (List) map1322.get(Integer.valueOf(intValue2))) {
                                        BitSet bitSet14 = bitSet2;
                                        Map map17 = map1322;
                                        if (zzgt().isLoggable(2)) {
                                            arrayMap31 = arrayMap55;
                                            arrayMap30 = arrayMap27;
                                            zzgt().zzjo().zzd("Evaluating filter. audience, filter, event", Integer.valueOf(intValue2), zzfj.zzavk, zzgq().zzbt(zzfj.zzavl));
                                            zzgt().zzjo().zzg("Filter definition", zzjr().zza(zzfj));
                                        } else {
                                            arrayMap31 = arrayMap55;
                                            arrayMap30 = arrayMap27;
                                        }
                                        if (zzfj.zzavk == null || zzfj.zzavk.intValue() > 256) {
                                            str4 = str2;
                                            arrayMap34 = arrayMap57;
                                            arrayMap33 = arrayMap54;
                                            zzft4 = zzft13;
                                            map11 = map9;
                                            bitSet3 = bitSet14;
                                            arrayMap32 = arrayMap31;
                                            bitSet4 = bitSet10;
                                            zzgt().zzjj().zze("Invalid event filter ID. appId, id", zzas.zzbw(str), String.valueOf(zzfj.zzavk));
                                        } else if (zzbb) {
                                            boolean z5 = (zzfj == null || zzfj.zzavh == null || !zzfj.zzavh.booleanValue()) ? false : true;
                                            boolean z6 = (zzfj == null || zzfj.zzavi == null || !zzfj.zzavi.booleanValue()) ? false : true;
                                            if (!bitSet10.get(zzfj.zzavk.intValue()) || z5 || z6) {
                                                zzfj zzfj2 = zzfj;
                                                bitSet3 = bitSet14;
                                                arrayMap34 = arrayMap57;
                                                arrayMap33 = arrayMap54;
                                                zzfz[] zzfzArr5 = zzfzArr;
                                                zzft4 = zzft13;
                                                bitSet4 = bitSet10;
                                                str4 = str2;
                                                zzfj zzfj3 = zzfj2;
                                                Map map18 = map9;
                                                arrayMap32 = arrayMap31;
                                                Boolean zza2 = zza(zzfj2, str2, zzfuArr, j522);
                                                zzgt().zzjo().zzg("Event filter result", zza2 == 0 ? "null" : zza2);
                                                if (zza2 == 0) {
                                                    hashSet7.add(Integer.valueOf(intValue2));
                                                } else {
                                                    bitSet3.set(zzfj3.zzavk.intValue());
                                                    if (zza2.booleanValue()) {
                                                        bitSet4.set(zzfj3.zzavk.intValue());
                                                        if ((z5 || z6) && zzft4.zzaxb != null) {
                                                            if (z6) {
                                                                zzb(map10, zzfj3.zzavk.intValue(), zzft4.zzaxb.longValue());
                                                            } else {
                                                                map11 = map18;
                                                                zza(map11, zzfj3.zzavk.intValue(), zzft4.zzaxb.longValue());
                                                            }
                                                        }
                                                    }
                                                }
                                                bitSet10 = bitSet4;
                                                bitSet5 = bitSet3;
                                                zzft13 = zzft4;
                                                map1322 = map17;
                                                arrayMap35 = arrayMap30;
                                                arrayMap57 = arrayMap34;
                                                arrayMap54 = arrayMap33;
                                                arrayMap55 = arrayMap32;
                                                str2 = str4;
                                                map9 = map18;
                                                zzfz[] zzfzArr6 = zzfzArr;
                                                String str9 = str;
                                            } else {
                                                zzgt().zzjo().zze("Event filter already evaluated true and it is not associated with a dynamic audience. audience ID, filter ID", Integer.valueOf(intValue2), zzfj.zzavk);
                                                bitSet5 = bitSet14;
                                                map1322 = map17;
                                                arrayMap55 = arrayMap31;
                                                arrayMap35 = arrayMap30;
                                                zzfz[] zzfzArr62 = zzfzArr;
                                                String str92 = str;
                                            }
                                        } else {
                                            str4 = str2;
                                            zzfj zzfj4 = zzfj;
                                            arrayMap34 = arrayMap57;
                                            arrayMap33 = arrayMap54;
                                            zzft4 = zzft13;
                                            map11 = map9;
                                            bitSet3 = bitSet14;
                                            arrayMap32 = arrayMap31;
                                            bitSet4 = bitSet10;
                                            if (bitSet4.get(zzfj4.zzavk.intValue())) {
                                                zzgt().zzjo().zze("Event filter already evaluated true. audience ID, filter ID", Integer.valueOf(intValue2), zzfj4.zzavk);
                                            } else {
                                                Boolean zza3 = zza(zzfj4, str4, zzfuArr, j522);
                                                zzgt().zzjo().zzg("Event filter result", zza3 == 0 ? "null" : zza3);
                                                if (zza3 == 0) {
                                                    hashSet7.add(Integer.valueOf(intValue2));
                                                } else {
                                                    bitSet3.set(zzfj4.zzavk.intValue());
                                                    if (zza3.booleanValue()) {
                                                        bitSet4.set(zzfj4.zzavk.intValue());
                                                    }
                                                }
                                            }
                                        }
                                        bitSet10 = bitSet4;
                                        bitSet5 = bitSet3;
                                        map9 = map11;
                                        zzft13 = zzft4;
                                        map1322 = map17;
                                        arrayMap35 = arrayMap30;
                                        arrayMap57 = arrayMap34;
                                        arrayMap54 = arrayMap33;
                                        arrayMap55 = arrayMap32;
                                        str2 = str4;
                                        zzfz[] zzfzArr622 = zzfzArr;
                                        String str922 = str;
                                    }
                                    arrayMap24 = arrayMap55;
                                    arrayMap26 = arrayMap57;
                                    arrayMap22 = arrayMap27;
                                    hashSet3 = hashSet7;
                                    zzft3 = zzft13;
                                    arrayMap5322 = arrayMap56;
                                    it3 = it4;
                                    arrayMap21 = arrayMap29;
                                    arrayMap23 = arrayMap54;
                                    zzfz[] zzfzArr7 = zzfzArr;
                                    String str10 = str;
                                }
                            }
                            arrayMap20 = arrayMap5322;
                            arrayMap19 = arrayMap21;
                            arrayMap17 = arrayMap26;
                            arrayMap15 = arrayMap24;
                            hashSet = hashSet2;
                            arrayMap16 = arrayMap23;
                            arrayMap18 = arrayMap22;
                            l4 = l;
                            j2 = j3;
                            zzft8 = zzft;
                            i11 = i3 + 1;
                            zzftArr2 = zzftArr;
                            hashSet6 = hashSet;
                            length3 = i2;
                            arrayMap52 = arrayMap20;
                            arrayMap45 = arrayMap19;
                            arrayMap49 = arrayMap18;
                            arrayMap50 = arrayMap17;
                            arrayMap41 = arrayMap16;
                            arrayMap51 = arrayMap15;
                            zzfzArr2 = zzfzArr;
                            str5 = str;
                        }
                    } else {
                        i5 = i11;
                    }
                    z2 = false;
                    if (!z2) {
                    }
                    zzg = zzjt().zzg(str5, zzft2.name);
                    if (zzg == null) {
                    }
                    zzjt().zza(zzac);
                    long j5222 = zzac.zzahv;
                    ArrayMap arrayMap53222 = arrayMap25;
                    map6 = (Map) arrayMap53222.get(str2);
                    if (map6 == null) {
                    }
                    Map map13222 = map6;
                    it3 = map13222.keySet().iterator();
                    while (it3.hasNext()) {
                    }
                    arrayMap20 = arrayMap53222;
                    arrayMap19 = arrayMap21;
                    arrayMap17 = arrayMap26;
                    arrayMap15 = arrayMap24;
                    hashSet = hashSet2;
                    arrayMap16 = arrayMap23;
                    arrayMap18 = arrayMap22;
                    l4 = l;
                    j2 = j3;
                    zzft8 = zzft;
                    i11 = i3 + 1;
                    zzftArr2 = zzftArr;
                    hashSet6 = hashSet;
                    length3 = i2;
                    arrayMap52 = arrayMap20;
                    arrayMap45 = arrayMap19;
                    arrayMap49 = arrayMap18;
                    arrayMap50 = arrayMap17;
                    arrayMap41 = arrayMap16;
                    arrayMap51 = arrayMap15;
                    zzfzArr2 = zzfzArr;
                    str5 = str;
                } else {
                    i4 = i11;
                    zzft2 = zzft9;
                    arrayMap21 = arrayMap45;
                    z = true;
                }
                zzft = zzft8;
                l = l4;
                str2 = str6;
                zzfuArr = zzfuArr2;
                zzg = zzjt().zzg(str5, zzft2.name);
                if (zzg == null) {
                }
                zzjt().zza(zzac);
                long j52222 = zzac.zzahv;
                ArrayMap arrayMap532222 = arrayMap25;
                map6 = (Map) arrayMap532222.get(str2);
                if (map6 == null) {
                }
                Map map132222 = map6;
                it3 = map132222.keySet().iterator();
                while (it3.hasNext()) {
                }
                arrayMap20 = arrayMap532222;
                arrayMap19 = arrayMap21;
                arrayMap17 = arrayMap26;
                arrayMap15 = arrayMap24;
                hashSet = hashSet2;
                arrayMap16 = arrayMap23;
                arrayMap18 = arrayMap22;
                l4 = l;
                j2 = j3;
                zzft8 = zzft;
                i11 = i3 + 1;
                zzftArr2 = zzftArr;
                hashSet6 = hashSet;
                length3 = i2;
                arrayMap52 = arrayMap20;
                arrayMap45 = arrayMap19;
                arrayMap49 = arrayMap18;
                arrayMap50 = arrayMap17;
                arrayMap41 = arrayMap16;
                arrayMap51 = arrayMap15;
                zzfzArr2 = zzfzArr;
                str5 = str;
            }
        }
        ArrayMap arrayMap60 = arrayMap45;
        ArrayMap arrayMap61 = arrayMap41;
        HashSet hashSet8 = hashSet6;
        ArrayMap arrayMap62 = arrayMap49;
        ArrayMap arrayMap63 = arrayMap50;
        ArrayMap arrayMap64 = arrayMap51;
        zzfz[] zzfzArr8 = zzfzArr;
        if (zzfzArr8 != null) {
            ArrayMap arrayMap65 = new ArrayMap();
            int length5 = zzfzArr8.length;
            int i15 = 0;
            while (i15 < length5) {
                zzfz zzfz = zzfzArr8[i15];
                Map map19 = (Map) arrayMap65.get(zzfz.name);
                if (map19 == null) {
                    map19 = zzjt().zzm(str, zzfz.name);
                    if (map19 == null) {
                        map19 = new ArrayMap();
                    }
                    arrayMap65.put(zzfz.name, map19);
                } else {
                    String str11 = str;
                }
                Iterator it7 = map19.keySet().iterator();
                while (it7.hasNext()) {
                    int intValue3 = ((Integer) it7.next()).intValue();
                    if (hashSet8.contains(Integer.valueOf(intValue3))) {
                        zzgt().zzjo().zzg("Skipping failed audience ID", Integer.valueOf(intValue3));
                    } else {
                        ArrayMap arrayMap66 = arrayMap61;
                        zzfr zzfr4 = (zzfr) arrayMap66.get(Integer.valueOf(intValue3));
                        ArrayMap arrayMap67 = arrayMap64;
                        BitSet bitSet15 = (BitSet) arrayMap67.get(Integer.valueOf(intValue3));
                        ArrayMap arrayMap68 = arrayMap63;
                        BitSet bitSet16 = (BitSet) arrayMap68.get(Integer.valueOf(intValue3));
                        if (zzbb) {
                            arrayMap5 = arrayMap65;
                            arrayMap7 = arrayMap62;
                            map = (Map) arrayMap7.get(Integer.valueOf(intValue3));
                            i = length5;
                            arrayMap6 = arrayMap60;
                            map2 = (Map) arrayMap6.get(Integer.valueOf(intValue3));
                        } else {
                            arrayMap5 = arrayMap65;
                            i = length5;
                            arrayMap6 = arrayMap60;
                            arrayMap7 = arrayMap62;
                            map2 = null;
                            map = null;
                        }
                        if (zzfr4 == null) {
                            zzfr zzfr5 = new zzfr();
                            arrayMap66.put(Integer.valueOf(intValue3), zzfr5);
                            zzfr5.zzawv = Boolean.valueOf(true);
                            bitSet15 = new BitSet();
                            arrayMap67.put(Integer.valueOf(intValue3), bitSet15);
                            bitSet16 = new BitSet();
                            arrayMap68.put(Integer.valueOf(intValue3), bitSet16);
                            if (zzbb) {
                                ArrayMap arrayMap69 = new ArrayMap();
                                arrayMap7.put(Integer.valueOf(intValue3), arrayMap69);
                                map3 = new ArrayMap();
                                Map map20 = arrayMap69;
                                arrayMap6.put(Integer.valueOf(intValue3), map3);
                                arrayMap8 = arrayMap6;
                                map4 = map20;
                                it2 = ((List) map19.get(Integer.valueOf(intValue3))).iterator();
                                while (true) {
                                    if (it2.hasNext()) {
                                        arrayMap62 = arrayMap7;
                                        arrayMap61 = arrayMap66;
                                        arrayMap64 = arrayMap67;
                                        arrayMap63 = arrayMap68;
                                        arrayMap65 = arrayMap5;
                                        length5 = i;
                                        arrayMap60 = arrayMap8;
                                        break;
                                    }
                                    Iterator it8 = it2;
                                    zzfm zzfm = (zzfm) it2.next();
                                    Map map21 = map19;
                                    Iterator it9 = it7;
                                    if (zzgt().isLoggable(2)) {
                                        arrayMap11 = arrayMap7;
                                        arrayMap10 = arrayMap68;
                                        arrayMap9 = arrayMap66;
                                        zzgt().zzjo().zzd("Evaluating filter. audience, filter, property", Integer.valueOf(intValue3), zzfm.zzavk, zzgq().zzbv(zzfm.zzawa));
                                        zzgt().zzjo().zzg("Filter definition", zzjr().zza(zzfm));
                                    } else {
                                        arrayMap11 = arrayMap7;
                                        arrayMap9 = arrayMap66;
                                        arrayMap10 = arrayMap68;
                                    }
                                    if (zzfm.zzavk == null || zzfm.zzavk.intValue() > 256) {
                                        ArrayMap arrayMap70 = arrayMap67;
                                        ArrayMap arrayMap71 = arrayMap8;
                                        zzgt().zzjj().zze("Invalid property filter ID. appId, id", zzas.zzbw(str), String.valueOf(zzfm.zzavk));
                                        hashSet8.add(Integer.valueOf(intValue3));
                                        arrayMap65 = arrayMap5;
                                        length5 = i;
                                        map19 = map21;
                                        it7 = it9;
                                        arrayMap62 = arrayMap11;
                                        arrayMap63 = arrayMap10;
                                        arrayMap61 = arrayMap9;
                                        arrayMap60 = arrayMap71;
                                        arrayMap64 = arrayMap70;
                                    } else {
                                        if (zzbb) {
                                            boolean z7 = (zzfm == null || zzfm.zzavh == null || !zzfm.zzavh.booleanValue()) ? false : true;
                                            boolean z8 = (zzfm == null || zzfm.zzavi == null || !zzfm.zzavi.booleanValue()) ? false : true;
                                            if (!bitSet15.get(zzfm.zzavk.intValue()) || z7 || z8) {
                                                ArrayMap arrayMap72 = arrayMap8;
                                                Boolean zza4 = zza(zzfm, zzfz);
                                                zzau zzjo = zzgt().zzjo();
                                                arrayMap13 = arrayMap72;
                                                String str12 = "Property filter result";
                                                if (zza4 == 0) {
                                                    arrayMap12 = arrayMap67;
                                                    obj = "null";
                                                } else {
                                                    arrayMap12 = arrayMap67;
                                                    obj = zza4;
                                                }
                                                zzjo.zzg(str12, obj);
                                                if (zza4 == 0) {
                                                    hashSet8.add(Integer.valueOf(intValue3));
                                                } else {
                                                    bitSet16.set(zzfm.zzavk.intValue());
                                                    bitSet15.set(zzfm.zzavk.intValue(), zza4.booleanValue());
                                                    if (zza4.booleanValue() && ((z7 || z8) && zzfz.zzayu != null)) {
                                                        if (z8) {
                                                            zzb(map3, zzfm.zzavk.intValue(), zzfz.zzayu.longValue());
                                                        } else {
                                                            zza(map4, zzfm.zzavk.intValue(), zzfz.zzayu.longValue());
                                                        }
                                                    }
                                                }
                                            } else {
                                                zzgt().zzjo().zze("Property filter already evaluated true and it is not associated with a dynamic audience. audience ID, filter ID", Integer.valueOf(intValue3), zzfm.zzavk);
                                                it2 = it8;
                                                map5 = map21;
                                                it7 = it9;
                                                arrayMap14 = arrayMap11;
                                                arrayMap68 = arrayMap10;
                                                arrayMap66 = arrayMap9;
                                                String str13 = str;
                                            }
                                        } else {
                                            arrayMap12 = arrayMap67;
                                            arrayMap13 = arrayMap8;
                                            if (bitSet15.get(zzfm.zzavk.intValue())) {
                                                zzgt().zzjo().zze("Property filter already evaluated true. audience ID, filter ID", Integer.valueOf(intValue3), zzfm.zzavk);
                                            } else {
                                                Boolean zza5 = zza(zzfm, zzfz);
                                                zzgt().zzjo().zzg("Property filter result", zza5 == 0 ? "null" : zza5);
                                                if (zza5 == 0) {
                                                    hashSet8.add(Integer.valueOf(intValue3));
                                                } else {
                                                    bitSet16.set(zzfm.zzavk.intValue());
                                                    if (zza5.booleanValue()) {
                                                        bitSet15.set(zzfm.zzavk.intValue());
                                                    }
                                                }
                                            }
                                        }
                                        it2 = it8;
                                        map5 = map21;
                                        it7 = it9;
                                        arrayMap14 = arrayMap11;
                                        arrayMap68 = arrayMap10;
                                        arrayMap66 = arrayMap9;
                                        arrayMap8 = arrayMap13;
                                        arrayMap67 = arrayMap12;
                                        String str132 = str;
                                    }
                                }
                                zzfz[] zzfzArr9 = zzfzArr;
                                String str14 = str;
                            }
                        }
                        map3 = map2;
                        arrayMap8 = arrayMap6;
                        map4 = map;
                        it2 = ((List) map19.get(Integer.valueOf(intValue3))).iterator();
                        while (true) {
                            if (it2.hasNext()) {
                            }
                            String str1322 = str;
                        }
                        zzfz[] zzfzArr92 = zzfzArr;
                        String str142 = str;
                    }
                }
                ArrayMap arrayMap73 = arrayMap65;
                int i16 = length5;
                ArrayMap arrayMap74 = arrayMap60;
                ArrayMap arrayMap75 = arrayMap62;
                ArrayMap arrayMap76 = arrayMap63;
                ArrayMap arrayMap77 = arrayMap61;
                ArrayMap arrayMap78 = arrayMap64;
                i15++;
                zzfzArr8 = zzfzArr;
            }
        }
        ArrayMap arrayMap79 = arrayMap60;
        ArrayMap arrayMap80 = arrayMap62;
        ArrayMap arrayMap81 = arrayMap63;
        ArrayMap arrayMap82 = arrayMap61;
        ArrayMap arrayMap83 = arrayMap64;
        zzfr[] zzfrArr = new zzfr[arrayMap83.size()];
        Iterator it10 = arrayMap83.keySet().iterator();
        int i17 = 0;
        while (it10.hasNext()) {
            int intValue4 = ((Integer) it10.next()).intValue();
            if (!hashSet8.contains(Integer.valueOf(intValue4))) {
                ArrayMap arrayMap84 = arrayMap82;
                zzfr zzfr6 = (zzfr) arrayMap84.get(Integer.valueOf(intValue4));
                if (zzfr6 == null) {
                    zzfr6 = new zzfr();
                }
                int i18 = i17 + 1;
                zzfrArr[i17] = zzfr6;
                zzfr6.zzave = Integer.valueOf(intValue4);
                zzfr6.zzawt = new zzfx();
                ArrayMap arrayMap85 = arrayMap83;
                zzfr6.zzawt.zzayo = zzfu.zza((BitSet) arrayMap85.get(Integer.valueOf(intValue4)));
                ArrayMap arrayMap86 = arrayMap81;
                zzfr6.zzawt.zzayn = zzfu.zza((BitSet) arrayMap86.get(Integer.valueOf(intValue4)));
                if (zzbb) {
                    arrayMap3 = arrayMap80;
                    zzfr6.zzawt.zzayp = zzb((Map) arrayMap3.get(Integer.valueOf(intValue4)));
                    zzfx zzfx2 = zzfr6.zzawt;
                    arrayMap2 = arrayMap79;
                    Map map22 = (Map) arrayMap2.get(Integer.valueOf(intValue4));
                    if (map22 == null) {
                        it = it10;
                        arrayMap = arrayMap84;
                        zzfyArr = new zzfy[0];
                    } else {
                        zzfyArr = new zzfy[map22.size()];
                        int i19 = 0;
                        for (Integer num : map22.keySet()) {
                            Iterator it11 = it10;
                            zzfy zzfy = new zzfy();
                            zzfy.zzawx = num;
                            List<Long> list = (List) map22.get(num);
                            if (list != null) {
                                Collections.sort(list);
                                arrayMap4 = arrayMap84;
                                long[] jArr = new long[list.size()];
                                int i20 = 0;
                                for (Long longValue2 : list) {
                                    int i21 = i20 + 1;
                                    jArr[i20] = longValue2.longValue();
                                    i20 = i21;
                                }
                                zzfy.zzays = jArr;
                            } else {
                                arrayMap4 = arrayMap84;
                            }
                            int i22 = i19 + 1;
                            zzfyArr[i19] = zzfy;
                            i19 = i22;
                            it10 = it11;
                            arrayMap84 = arrayMap4;
                        }
                        it = it10;
                        arrayMap = arrayMap84;
                    }
                    zzfx2.zzayq = zzfyArr;
                } else {
                    it = it10;
                    arrayMap = arrayMap84;
                    arrayMap3 = arrayMap80;
                    arrayMap2 = arrayMap79;
                }
                zzt zzjt2 = zzjt();
                zzfx zzfx3 = zzfr6.zzawt;
                zzjt2.zzcl();
                zzjt2.zzaf();
                Preconditions.checkNotEmpty(str);
                Preconditions.checkNotNull(zzfx3);
                try {
                    byte[] bArr = new byte[zzfx3.zzvx()];
                    try {
                        zzya zzk = zzya.zzk(bArr, 0, bArr.length);
                        zzfx3.zza(zzk);
                        zzk.zzza();
                        ContentValues contentValues = new ContentValues();
                        contentValues.put("app_id", str);
                        contentValues.put("audience_id", Integer.valueOf(intValue4));
                        contentValues.put("current_results", bArr);
                        try {
                            try {
                                if (zzjt2.getWritableDatabase().insertWithOnConflict("audience_filter_values", null, contentValues, 5) == -1) {
                                    zzjt2.zzgt().zzjg().zzg("Failed to insert filter results (got -1). appId", zzas.zzbw(str));
                                }
                            } catch (SQLiteException e4) {
                                e = e4;
                                zzjt2.zzgt().zzjg().zze("Error storing filter results. appId", zzas.zzbw(str), e);
                                i17 = i18;
                                arrayMap83 = arrayMap85;
                                arrayMap81 = arrayMap86;
                                arrayMap80 = arrayMap3;
                                arrayMap79 = arrayMap2;
                                it10 = it;
                                arrayMap82 = arrayMap;
                            }
                        } catch (SQLiteException e5) {
                            e = e5;
                            zzjt2.zzgt().zzjg().zze("Error storing filter results. appId", zzas.zzbw(str), e);
                            i17 = i18;
                            arrayMap83 = arrayMap85;
                            arrayMap81 = arrayMap86;
                            arrayMap80 = arrayMap3;
                            arrayMap79 = arrayMap2;
                            it10 = it;
                            arrayMap82 = arrayMap;
                        }
                    } catch (IOException e6) {
                        e = e6;
                        String str15 = str;
                        zzjt2.zzgt().zzjg().zze("Configuration loss. Failed to serialize filter results. appId", zzas.zzbw(str), e);
                        i17 = i18;
                        arrayMap83 = arrayMap85;
                        arrayMap81 = arrayMap86;
                        arrayMap80 = arrayMap3;
                        arrayMap79 = arrayMap2;
                        it10 = it;
                        arrayMap82 = arrayMap;
                    }
                } catch (IOException e7) {
                    e = e7;
                    String str152 = str;
                    zzjt2.zzgt().zzjg().zze("Configuration loss. Failed to serialize filter results. appId", zzas.zzbw(str), e);
                    i17 = i18;
                    arrayMap83 = arrayMap85;
                    arrayMap81 = arrayMap86;
                    arrayMap80 = arrayMap3;
                    arrayMap79 = arrayMap2;
                    it10 = it;
                    arrayMap82 = arrayMap;
                }
                i17 = i18;
                arrayMap83 = arrayMap85;
                arrayMap81 = arrayMap86;
                arrayMap80 = arrayMap3;
                arrayMap79 = arrayMap2;
                it10 = it;
                arrayMap82 = arrayMap;
            } else {
                String str16 = str;
            }
        }
        return (zzfr[]) Arrays.copyOf(zzfrArr, i17);
    }

    private final Boolean zza(zzfj zzfj, String str, zzfu[] zzfuArr, long j) {
        zzfk[] zzfkArr;
        zzfk[] zzfkArr2;
        Boolean bool;
        if (zzfj.zzavo != null) {
            Boolean zza = zza(j, zzfj.zzavo);
            if (zza == null) {
                return null;
            }
            if (!zza.booleanValue()) {
                return Boolean.valueOf(false);
            }
        }
        HashSet hashSet = new HashSet();
        for (zzfk zzfk : zzfj.zzavm) {
            if (TextUtils.isEmpty(zzfk.zzavt)) {
                zzgt().zzjj().zzg("null or empty param name in filter. event", zzgq().zzbt(str));
                return null;
            }
            hashSet.add(zzfk.zzavt);
        }
        ArrayMap arrayMap = new ArrayMap();
        for (zzfu zzfu : zzfuArr) {
            if (hashSet.contains(zzfu.name)) {
                if (zzfu.zzaxe != null) {
                    arrayMap.put(zzfu.name, zzfu.zzaxe);
                } else if (zzfu.zzaun != null) {
                    arrayMap.put(zzfu.name, zzfu.zzaun);
                } else if (zzfu.zzaml != null) {
                    arrayMap.put(zzfu.name, zzfu.zzaml);
                } else {
                    zzgt().zzjj().zze("Unknown value for param. event, param", zzgq().zzbt(str), zzgq().zzbu(zzfu.name));
                    return null;
                }
            }
        }
        for (zzfk zzfk2 : zzfj.zzavm) {
            boolean equals = Boolean.TRUE.equals(zzfk2.zzavs);
            String str2 = zzfk2.zzavt;
            if (TextUtils.isEmpty(str2)) {
                zzgt().zzjj().zzg("Event has empty param name. event", zzgq().zzbt(str));
                return null;
            }
            Object obj = arrayMap.get(str2);
            if (obj instanceof Long) {
                if (zzfk2.zzavr == null) {
                    zzgt().zzjj().zze("No number filter for long param. event, param", zzgq().zzbt(str), zzgq().zzbu(str2));
                    return null;
                }
                Boolean zza2 = zza(((Long) obj).longValue(), zzfk2.zzavr);
                if (zza2 == null) {
                    return null;
                }
                if ((true ^ zza2.booleanValue()) ^ equals) {
                    return Boolean.valueOf(false);
                }
            } else if (obj instanceof Double) {
                if (zzfk2.zzavr == null) {
                    zzgt().zzjj().zze("No number filter for double param. event, param", zzgq().zzbt(str), zzgq().zzbu(str2));
                    return null;
                }
                Boolean zza3 = zza(((Double) obj).doubleValue(), zzfk2.zzavr);
                if (zza3 == null) {
                    return null;
                }
                if ((true ^ zza3.booleanValue()) ^ equals) {
                    return Boolean.valueOf(false);
                }
            } else if (obj instanceof String) {
                if (zzfk2.zzavq != null) {
                    bool = zza((String) obj, zzfk2.zzavq);
                } else if (zzfk2.zzavr != null) {
                    String str3 = (String) obj;
                    if (zzfu.zzcs(str3)) {
                        bool = zza(str3, zzfk2.zzavr);
                    } else {
                        zzgt().zzjj().zze("Invalid param value for number filter. event, param", zzgq().zzbt(str), zzgq().zzbu(str2));
                        return null;
                    }
                } else {
                    zzgt().zzjj().zze("No filter for String param. event, param", zzgq().zzbt(str), zzgq().zzbu(str2));
                    return null;
                }
                if (bool == null) {
                    return null;
                }
                if ((true ^ bool.booleanValue()) ^ equals) {
                    return Boolean.valueOf(false);
                }
            } else if (obj == null) {
                zzgt().zzjo().zze("Missing param for filter. event, param", zzgq().zzbt(str), zzgq().zzbu(str2));
                return Boolean.valueOf(false);
            } else {
                zzgt().zzjj().zze("Unknown param type. event, param", zzgq().zzbt(str), zzgq().zzbu(str2));
                return null;
            }
        }
        return Boolean.valueOf(true);
    }

    private final Boolean zza(zzfm zzfm, zzfz zzfz) {
        zzfk zzfk = zzfm.zzawb;
        if (zzfk == null) {
            zzgt().zzjj().zzg("Missing property filter. property", zzgq().zzbv(zzfz.name));
            return null;
        }
        boolean equals = Boolean.TRUE.equals(zzfk.zzavs);
        if (zzfz.zzaxe != null) {
            if (zzfk.zzavr != null) {
                return zza(zza(zzfz.zzaxe.longValue(), zzfk.zzavr), equals);
            }
            zzgt().zzjj().zzg("No number filter for long property. property", zzgq().zzbv(zzfz.name));
            return null;
        } else if (zzfz.zzaun != null) {
            if (zzfk.zzavr != null) {
                return zza(zza(zzfz.zzaun.doubleValue(), zzfk.zzavr), equals);
            }
            zzgt().zzjj().zzg("No number filter for double property. property", zzgq().zzbv(zzfz.name));
            return null;
        } else if (zzfz.zzaml == null) {
            zzgt().zzjj().zzg("User property has no value, property", zzgq().zzbv(zzfz.name));
            return null;
        } else if (zzfk.zzavq != null) {
            return zza(zza(zzfz.zzaml, zzfk.zzavq), equals);
        } else {
            if (zzfk.zzavr == null) {
                zzgt().zzjj().zzg("No string or number filter defined. property", zzgq().zzbv(zzfz.name));
            } else if (zzfu.zzcs(zzfz.zzaml)) {
                return zza(zza(zzfz.zzaml, zzfk.zzavr), equals);
            } else {
                zzgt().zzjj().zze("Invalid user property value for Numeric number filter. property, value", zzgq().zzbv(zzfz.name), zzfz.zzaml);
            }
            return null;
        }
    }

    @VisibleForTesting
    private static Boolean zza(Boolean bool, boolean z) {
        if (bool == null) {
            return null;
        }
        return Boolean.valueOf(bool.booleanValue() ^ z);
    }

    @VisibleForTesting
    private final Boolean zza(String str, zzfn zzfn) {
        String str2;
        List list;
        Preconditions.checkNotNull(zzfn);
        if (str == null || zzfn.zzawc == null || zzfn.zzawc.intValue() == 0) {
            return null;
        }
        if (zzfn.zzawc.intValue() == 6) {
            if (zzfn.zzawf == null || zzfn.zzawf.length == 0) {
                return null;
            }
        } else if (zzfn.zzawd == null) {
            return null;
        }
        int intValue = zzfn.zzawc.intValue();
        boolean z = zzfn.zzawe != null && zzfn.zzawe.booleanValue();
        if (z || intValue == 1 || intValue == 6) {
            str2 = zzfn.zzawd;
        } else {
            str2 = zzfn.zzawd.toUpperCase(Locale.ENGLISH);
        }
        String str3 = str2;
        if (zzfn.zzawf == null) {
            list = null;
        } else {
            String[] strArr = zzfn.zzawf;
            if (z) {
                list = Arrays.asList(strArr);
            } else {
                ArrayList arrayList = new ArrayList();
                for (String upperCase : strArr) {
                    arrayList.add(upperCase.toUpperCase(Locale.ENGLISH));
                }
                list = arrayList;
            }
        }
        return zza(str, intValue, z, str3, list, intValue == 1 ? str3 : null);
    }

    private final Boolean zza(String str, int i, boolean z, String str2, List<String> list, String str3) {
        if (str == null) {
            return null;
        }
        if (i == 6) {
            if (list == null || list.size() == 0) {
                return null;
            }
        } else if (str2 == null) {
            return null;
        }
        if (!z && i != 1) {
            str = str.toUpperCase(Locale.ENGLISH);
        }
        switch (i) {
            case 1:
                try {
                    return Boolean.valueOf(Pattern.compile(str3, z ? 0 : 66).matcher(str).matches());
                } catch (PatternSyntaxException unused) {
                    zzgt().zzjj().zzg("Invalid regular expression in REGEXP audience filter. expression", str3);
                    return null;
                }
            case 2:
                return Boolean.valueOf(str.startsWith(str2));
            case 3:
                return Boolean.valueOf(str.endsWith(str2));
            case 4:
                return Boolean.valueOf(str.contains(str2));
            case 5:
                return Boolean.valueOf(str.equals(str2));
            case 6:
                return Boolean.valueOf(list.contains(str));
            default:
                return null;
        }
    }

    private final Boolean zza(long j, zzfl zzfl) {
        try {
            return zza(new BigDecimal(j), zzfl, (double) Utils.DOUBLE_EPSILON);
        } catch (NumberFormatException unused) {
            return null;
        }
    }

    private final Boolean zza(double d, zzfl zzfl) {
        try {
            return zza(new BigDecimal(d), zzfl, Math.ulp(d));
        } catch (NumberFormatException unused) {
            return null;
        }
    }

    private final Boolean zza(String str, zzfl zzfl) {
        if (!zzfu.zzcs(str)) {
            return null;
        }
        try {
            return zza(new BigDecimal(str), zzfl, (double) Utils.DOUBLE_EPSILON);
        } catch (NumberFormatException unused) {
            return null;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0072, code lost:
        if (r3 != null) goto L_0x0074;
     */
    @VisibleForTesting
    private static Boolean zza(BigDecimal bigDecimal, zzfl zzfl, double d) {
        BigDecimal bigDecimal2;
        BigDecimal bigDecimal3;
        BigDecimal bigDecimal4;
        Preconditions.checkNotNull(zzfl);
        if (zzfl.zzavu == null || zzfl.zzavu.intValue() == 0) {
            return null;
        }
        if (zzfl.zzavu.intValue() == 4) {
            if (zzfl.zzavx == null || zzfl.zzavy == null) {
                return null;
            }
        } else if (zzfl.zzavw == null) {
            return null;
        }
        int intValue = zzfl.zzavu.intValue();
        if (zzfl.zzavu.intValue() == 4) {
            if (!zzfu.zzcs(zzfl.zzavx) || !zzfu.zzcs(zzfl.zzavy)) {
                return null;
            }
            try {
                BigDecimal bigDecimal5 = new BigDecimal(zzfl.zzavx);
                bigDecimal3 = new BigDecimal(zzfl.zzavy);
                bigDecimal2 = bigDecimal5;
                bigDecimal4 = null;
            } catch (NumberFormatException unused) {
                return null;
            }
        } else if (!zzfu.zzcs(zzfl.zzavw)) {
            return null;
        } else {
            try {
                bigDecimal4 = new BigDecimal(zzfl.zzavw);
                bigDecimal2 = null;
                bigDecimal3 = null;
            } catch (NumberFormatException unused2) {
                return null;
            }
        }
        if (intValue == 4) {
            if (bigDecimal2 == null) {
                return null;
            }
        }
        boolean z = false;
        switch (intValue) {
            case 1:
                if (bigDecimal.compareTo(bigDecimal4) == -1) {
                    z = true;
                }
                return Boolean.valueOf(z);
            case 2:
                if (bigDecimal.compareTo(bigDecimal4) == 1) {
                    z = true;
                }
                return Boolean.valueOf(z);
            case 3:
                if (d != Utils.DOUBLE_EPSILON) {
                    if (bigDecimal.compareTo(bigDecimal4.subtract(new BigDecimal(d).multiply(new BigDecimal(2)))) == 1 && bigDecimal.compareTo(bigDecimal4.add(new BigDecimal(d).multiply(new BigDecimal(2)))) == -1) {
                        z = true;
                    }
                    return Boolean.valueOf(z);
                }
                if (bigDecimal.compareTo(bigDecimal4) == 0) {
                    z = true;
                }
                return Boolean.valueOf(z);
            case 4:
                if (!(bigDecimal.compareTo(bigDecimal2) == -1 || bigDecimal.compareTo(bigDecimal3) == 1)) {
                    z = true;
                }
                return Boolean.valueOf(z);
        }
        return null;
    }

    private static zzfs[] zzb(Map<Integer, Long> map) {
        if (map == null) {
            return null;
        }
        int i = 0;
        zzfs[] zzfsArr = new zzfs[map.size()];
        for (Integer num : map.keySet()) {
            zzfs zzfs = new zzfs();
            zzfs.zzawx = num;
            zzfs.zzawy = (Long) map.get(num);
            int i2 = i + 1;
            zzfsArr[i] = zzfs;
            i = i2;
        }
        return zzfsArr;
    }

    private static void zza(Map<Integer, Long> map, int i, long j) {
        Long l = (Long) map.get(Integer.valueOf(i));
        long j2 = j / 1000;
        if (l == null || j2 > l.longValue()) {
            map.put(Integer.valueOf(i), Long.valueOf(j2));
        }
    }

    private static void zzb(Map<Integer, List<Long>> map, int i, long j) {
        List list = (List) map.get(Integer.valueOf(i));
        if (list == null) {
            list = new ArrayList();
            map.put(Integer.valueOf(i), list);
        }
        list.add(Long.valueOf(j / 1000));
    }
}
