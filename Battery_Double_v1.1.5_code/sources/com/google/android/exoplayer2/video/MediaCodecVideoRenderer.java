package com.google.android.exoplayer2.video;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Point;
import android.media.MediaCodec;
import android.media.MediaCodec.OnFrameRenderedListener;
import android.media.MediaCrypto;
import android.media.MediaFormat;
import android.os.Handler;
import android.os.SystemClock;
import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.Surface;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.decoder.DecoderInputBuffer;
import com.google.android.exoplayer2.drm.DrmInitData;
import com.google.android.exoplayer2.drm.DrmSessionManager;
import com.google.android.exoplayer2.drm.FrameworkMediaCrypto;
import com.google.android.exoplayer2.mediacodec.MediaCodecInfo;
import com.google.android.exoplayer2.mediacodec.MediaCodecRenderer;
import com.google.android.exoplayer2.mediacodec.MediaCodecSelector;
import com.google.android.exoplayer2.mediacodec.MediaCodecUtil;
import com.google.android.exoplayer2.mediacodec.MediaCodecUtil.DecoderQueryException;
import com.google.android.exoplayer2.mediacodec.MediaFormatUtil;
import com.google.android.exoplayer2.util.Assertions;
import com.google.android.exoplayer2.util.MimeTypes;
import com.google.android.exoplayer2.util.TraceUtil;
import com.google.android.exoplayer2.util.Util;
import com.google.android.exoplayer2.video.VideoRendererEventListener.EventDispatcher;
import com.integralads.avid.library.inmobi.utils.AvidJSONUtil;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import java.nio.ByteBuffer;

@TargetApi(16)
public class MediaCodecVideoRenderer extends MediaCodecRenderer {
    private static final String KEY_CROP_BOTTOM = "crop-bottom";
    private static final String KEY_CROP_LEFT = "crop-left";
    private static final String KEY_CROP_RIGHT = "crop-right";
    private static final String KEY_CROP_TOP = "crop-top";
    private static final int MAX_PENDING_OUTPUT_STREAM_OFFSET_COUNT = 10;
    private static final int[] STANDARD_LONG_EDGE_VIDEO_PX = {1920, 1600, 1440, 1280, 960, 854, 640, 540, 480};
    private static final String TAG = "MediaCodecVideoRenderer";
    private static boolean deviceNeedsSetOutputSurfaceWorkaround;
    private static boolean evaluatedDeviceNeedsSetOutputSurfaceWorkaround;
    private final long allowedJoiningTimeMs;
    private int buffersInCodecCount;
    private CodecMaxValues codecMaxValues;
    private boolean codecNeedsSetOutputSurfaceWorkaround;
    private int consecutiveDroppedFrameCount;
    private final Context context;
    private int currentHeight;
    private float currentPixelWidthHeightRatio;
    private int currentUnappliedRotationDegrees;
    private int currentWidth;
    private final boolean deviceNeedsAutoFrcWorkaround;
    private long droppedFrameAccumulationStartTimeMs;
    private int droppedFrames;
    private Surface dummySurface;
    private final EventDispatcher eventDispatcher;
    private final VideoFrameReleaseTimeHelper frameReleaseTimeHelper;
    private long initialPositionUs;
    private long joiningDeadlineMs;
    private long lastInputTimeUs;
    private long lastRenderTimeUs;
    private final int maxDroppedFramesToNotify;
    private long outputStreamOffsetUs;
    private int pendingOutputStreamOffsetCount;
    private final long[] pendingOutputStreamOffsetsUs;
    private final long[] pendingOutputStreamSwitchTimesUs;
    private float pendingPixelWidthHeightRatio;
    private int pendingRotationDegrees;
    private boolean renderedFirstFrame;
    private int reportedHeight;
    private float reportedPixelWidthHeightRatio;
    private int reportedUnappliedRotationDegrees;
    private int reportedWidth;
    private int scalingMode;
    private Surface surface;
    private boolean tunneling;
    private int tunnelingAudioSessionId;
    OnFrameRenderedListenerV23 tunnelingOnFrameRenderedListener;

    protected static final class CodecMaxValues {
        public final int height;
        public final int inputSize;
        public final int width;

        public CodecMaxValues(int i, int i2, int i3) {
            this.width = i;
            this.height = i2;
            this.inputSize = i3;
        }
    }

    @TargetApi(23)
    private final class OnFrameRenderedListenerV23 implements OnFrameRenderedListener {
        private OnFrameRenderedListenerV23(MediaCodec mediaCodec) {
            mediaCodec.setOnFrameRenderedListener(this, new Handler());
        }

        public void onFrameRendered(@NonNull MediaCodec mediaCodec, long j, long j2) {
            if (this == MediaCodecVideoRenderer.this.tunnelingOnFrameRenderedListener) {
                MediaCodecVideoRenderer.this.maybeNotifyRenderedFirstFrame();
            }
        }
    }

    private static boolean isBufferLate(long j) {
        return j < -30000;
    }

    private static boolean isBufferVeryLate(long j) {
        return j < -500000;
    }

    public MediaCodecVideoRenderer(Context context2, MediaCodecSelector mediaCodecSelector) {
        this(context2, mediaCodecSelector, 0);
    }

    public MediaCodecVideoRenderer(Context context2, MediaCodecSelector mediaCodecSelector, long j) {
        this(context2, mediaCodecSelector, j, null, null, -1);
    }

    public MediaCodecVideoRenderer(Context context2, MediaCodecSelector mediaCodecSelector, long j, @Nullable Handler handler, @Nullable VideoRendererEventListener videoRendererEventListener, int i) {
        this(context2, mediaCodecSelector, j, null, false, handler, videoRendererEventListener, i);
    }

    public MediaCodecVideoRenderer(Context context2, MediaCodecSelector mediaCodecSelector, long j, @Nullable DrmSessionManager<FrameworkMediaCrypto> drmSessionManager, boolean z, @Nullable Handler handler, @Nullable VideoRendererEventListener videoRendererEventListener, int i) {
        super(2, mediaCodecSelector, drmSessionManager, z);
        this.allowedJoiningTimeMs = j;
        this.maxDroppedFramesToNotify = i;
        this.context = context2.getApplicationContext();
        this.frameReleaseTimeHelper = new VideoFrameReleaseTimeHelper(this.context);
        this.eventDispatcher = new EventDispatcher(handler, videoRendererEventListener);
        this.deviceNeedsAutoFrcWorkaround = deviceNeedsAutoFrcWorkaround();
        this.pendingOutputStreamOffsetsUs = new long[10];
        this.pendingOutputStreamSwitchTimesUs = new long[10];
        this.outputStreamOffsetUs = C.TIME_UNSET;
        this.lastInputTimeUs = C.TIME_UNSET;
        this.joiningDeadlineMs = C.TIME_UNSET;
        this.currentWidth = -1;
        this.currentHeight = -1;
        this.currentPixelWidthHeightRatio = -1.0f;
        this.pendingPixelWidthHeightRatio = -1.0f;
        this.scalingMode = 1;
        clearReportedVideoSize();
    }

    /* access modifiers changed from: protected */
    public int supportsFormat(MediaCodecSelector mediaCodecSelector, DrmSessionManager<FrameworkMediaCrypto> drmSessionManager, Format format) throws DecoderQueryException {
        boolean z;
        String str = format.sampleMimeType;
        int i = 0;
        if (!MimeTypes.isVideo(str)) {
            return 0;
        }
        DrmInitData drmInitData = format.drmInitData;
        if (drmInitData != null) {
            z = false;
            for (int i2 = 0; i2 < drmInitData.schemeDataCount; i2++) {
                z |= drmInitData.get(i2).requiresSecureDecryption;
            }
        } else {
            z = false;
        }
        MediaCodecInfo decoderInfo = mediaCodecSelector.getDecoderInfo(str, z);
        int i3 = 2;
        if (decoderInfo == null) {
            if (!z || mediaCodecSelector.getDecoderInfo(str, false) == null) {
                i3 = 1;
            }
            return i3;
        } else if (!supportsFormatDrm(drmSessionManager, drmInitData)) {
            return 2;
        } else {
            boolean isCodecSupported = decoderInfo.isCodecSupported(format.codecs);
            if (isCodecSupported && format.width > 0 && format.height > 0) {
                if (Util.SDK_INT >= 21) {
                    isCodecSupported = decoderInfo.isVideoSizeAndRateSupportedV21(format.width, format.height, (double) format.frameRate);
                } else {
                    isCodecSupported = format.width * format.height <= MediaCodecUtil.maxH264DecodableFrameSize();
                    if (!isCodecSupported) {
                        String str2 = TAG;
                        StringBuilder sb = new StringBuilder();
                        sb.append("FalseCheck [legacyFrameSize, ");
                        sb.append(format.width);
                        sb.append(AvidJSONUtil.KEY_X);
                        sb.append(format.height);
                        sb.append("] [");
                        sb.append(Util.DEVICE_DEBUG_INFO);
                        sb.append(RequestParameters.RIGHT_BRACKETS);
                        Log.d(str2, sb.toString());
                    }
                }
            }
            int i4 = decoderInfo.adaptive ? 16 : 8;
            if (decoderInfo.tunneling) {
                i = 32;
            }
            return (isCodecSupported ? 4 : 3) | i4 | i;
        }
    }

    /* access modifiers changed from: protected */
    public void onEnabled(boolean z) throws ExoPlaybackException {
        super.onEnabled(z);
        this.tunnelingAudioSessionId = getConfiguration().tunnelingAudioSessionId;
        this.tunneling = this.tunnelingAudioSessionId != 0;
        this.eventDispatcher.enabled(this.decoderCounters);
        this.frameReleaseTimeHelper.enable();
    }

    /* access modifiers changed from: protected */
    public void onStreamChanged(Format[] formatArr, long j) throws ExoPlaybackException {
        if (this.outputStreamOffsetUs == C.TIME_UNSET) {
            this.outputStreamOffsetUs = j;
        } else {
            if (this.pendingOutputStreamOffsetCount == this.pendingOutputStreamOffsetsUs.length) {
                String str = TAG;
                StringBuilder sb = new StringBuilder();
                sb.append("Too many stream changes, so dropping offset: ");
                sb.append(this.pendingOutputStreamOffsetsUs[this.pendingOutputStreamOffsetCount - 1]);
                Log.w(str, sb.toString());
            } else {
                this.pendingOutputStreamOffsetCount++;
            }
            this.pendingOutputStreamOffsetsUs[this.pendingOutputStreamOffsetCount - 1] = j;
            this.pendingOutputStreamSwitchTimesUs[this.pendingOutputStreamOffsetCount - 1] = this.lastInputTimeUs;
        }
        super.onStreamChanged(formatArr, j);
    }

    /* access modifiers changed from: protected */
    public void onPositionReset(long j, boolean z) throws ExoPlaybackException {
        super.onPositionReset(j, z);
        clearRenderedFirstFrame();
        this.initialPositionUs = C.TIME_UNSET;
        this.consecutiveDroppedFrameCount = 0;
        this.lastInputTimeUs = C.TIME_UNSET;
        if (this.pendingOutputStreamOffsetCount != 0) {
            this.outputStreamOffsetUs = this.pendingOutputStreamOffsetsUs[this.pendingOutputStreamOffsetCount - 1];
            this.pendingOutputStreamOffsetCount = 0;
        }
        if (z) {
            setJoiningDeadlineMs();
        } else {
            this.joiningDeadlineMs = C.TIME_UNSET;
        }
    }

    public boolean isReady() {
        if (super.isReady() && (this.renderedFirstFrame || ((this.dummySurface != null && this.surface == this.dummySurface) || getCodec() == null || this.tunneling))) {
            this.joiningDeadlineMs = C.TIME_UNSET;
            return true;
        } else if (this.joiningDeadlineMs == C.TIME_UNSET) {
            return false;
        } else {
            if (SystemClock.elapsedRealtime() < this.joiningDeadlineMs) {
                return true;
            }
            this.joiningDeadlineMs = C.TIME_UNSET;
            return false;
        }
    }

    /* access modifiers changed from: protected */
    public void onStarted() {
        super.onStarted();
        this.droppedFrames = 0;
        this.droppedFrameAccumulationStartTimeMs = SystemClock.elapsedRealtime();
        this.lastRenderTimeUs = SystemClock.elapsedRealtime() * 1000;
    }

    /* access modifiers changed from: protected */
    public void onStopped() {
        this.joiningDeadlineMs = C.TIME_UNSET;
        maybeNotifyDroppedFrames();
        super.onStopped();
    }

    /* access modifiers changed from: protected */
    public void onDisabled() {
        this.currentWidth = -1;
        this.currentHeight = -1;
        this.currentPixelWidthHeightRatio = -1.0f;
        this.pendingPixelWidthHeightRatio = -1.0f;
        this.outputStreamOffsetUs = C.TIME_UNSET;
        this.lastInputTimeUs = C.TIME_UNSET;
        this.pendingOutputStreamOffsetCount = 0;
        clearReportedVideoSize();
        clearRenderedFirstFrame();
        this.frameReleaseTimeHelper.disable();
        this.tunnelingOnFrameRenderedListener = null;
        this.tunneling = false;
        try {
            super.onDisabled();
        } finally {
            this.decoderCounters.ensureUpdated();
            this.eventDispatcher.disabled(this.decoderCounters);
        }
    }

    public void handleMessage(int i, Object obj) throws ExoPlaybackException {
        if (i == 1) {
            setSurface((Surface) obj);
        } else if (i == 4) {
            this.scalingMode = ((Integer) obj).intValue();
            MediaCodec codec = getCodec();
            if (codec != null) {
                codec.setVideoScalingMode(this.scalingMode);
            }
        } else {
            super.handleMessage(i, obj);
        }
    }

    private void setSurface(Surface surface2) throws ExoPlaybackException {
        if (surface2 == null) {
            if (this.dummySurface != null) {
                surface2 = this.dummySurface;
            } else {
                MediaCodecInfo codecInfo = getCodecInfo();
                if (codecInfo != null && shouldUseDummySurface(codecInfo)) {
                    this.dummySurface = DummySurface.newInstanceV17(this.context, codecInfo.secure);
                    surface2 = this.dummySurface;
                }
            }
        }
        if (this.surface != surface2) {
            this.surface = surface2;
            int state = getState();
            if (state == 1 || state == 2) {
                MediaCodec codec = getCodec();
                if (Util.SDK_INT < 23 || codec == null || surface2 == null || this.codecNeedsSetOutputSurfaceWorkaround) {
                    releaseCodec();
                    maybeInitCodec();
                } else {
                    setOutputSurfaceV23(codec, surface2);
                }
            }
            if (surface2 == null || surface2 == this.dummySurface) {
                clearReportedVideoSize();
                clearRenderedFirstFrame();
                return;
            }
            maybeRenotifyVideoSizeChanged();
            clearRenderedFirstFrame();
            if (state == 2) {
                setJoiningDeadlineMs();
            }
        } else if (surface2 != null && surface2 != this.dummySurface) {
            maybeRenotifyVideoSizeChanged();
            maybeRenotifyRenderedFirstFrame();
        }
    }

    /* access modifiers changed from: protected */
    public boolean shouldInitCodec(MediaCodecInfo mediaCodecInfo) {
        return this.surface != null || shouldUseDummySurface(mediaCodecInfo);
    }

    /* access modifiers changed from: protected */
    public void configureCodec(MediaCodecInfo mediaCodecInfo, MediaCodec mediaCodec, Format format, MediaCrypto mediaCrypto) throws DecoderQueryException {
        this.codecMaxValues = getCodecMaxValues(mediaCodecInfo, format, getStreamFormats());
        MediaFormat mediaFormat = getMediaFormat(format, this.codecMaxValues, this.deviceNeedsAutoFrcWorkaround, this.tunnelingAudioSessionId);
        if (this.surface == null) {
            Assertions.checkState(shouldUseDummySurface(mediaCodecInfo));
            if (this.dummySurface == null) {
                this.dummySurface = DummySurface.newInstanceV17(this.context, mediaCodecInfo.secure);
            }
            this.surface = this.dummySurface;
        }
        mediaCodec.configure(mediaFormat, this.surface, mediaCrypto, 0);
        if (Util.SDK_INT >= 23 && this.tunneling) {
            this.tunnelingOnFrameRenderedListener = new OnFrameRenderedListenerV23(mediaCodec);
        }
    }

    /* access modifiers changed from: protected */
    public int canKeepCodec(MediaCodec mediaCodec, MediaCodecInfo mediaCodecInfo, Format format, Format format2) {
        if (!areAdaptationCompatible(mediaCodecInfo.adaptive, format, format2) || format2.width > this.codecMaxValues.width || format2.height > this.codecMaxValues.height || getMaxInputSize(mediaCodecInfo, format2) > this.codecMaxValues.inputSize) {
            return 0;
        }
        return format.initializationDataEquals(format2) ? 1 : 3;
    }

    /* access modifiers changed from: protected */
    @CallSuper
    public void releaseCodec() {
        try {
            super.releaseCodec();
        } finally {
            this.buffersInCodecCount = 0;
            if (this.dummySurface != null) {
                if (this.surface == this.dummySurface) {
                    this.surface = null;
                }
                this.dummySurface.release();
                this.dummySurface = null;
            }
        }
    }

    /* access modifiers changed from: protected */
    @CallSuper
    public void flushCodec() throws ExoPlaybackException {
        super.flushCodec();
        this.buffersInCodecCount = 0;
    }

    /* access modifiers changed from: protected */
    public void onCodecInitialized(String str, long j, long j2) {
        this.eventDispatcher.decoderInitialized(str, j, j2);
        this.codecNeedsSetOutputSurfaceWorkaround = codecNeedsSetOutputSurfaceWorkaround(str);
    }

    /* access modifiers changed from: protected */
    public void onInputFormatChanged(Format format) throws ExoPlaybackException {
        super.onInputFormatChanged(format);
        this.eventDispatcher.inputFormatChanged(format);
        this.pendingPixelWidthHeightRatio = format.pixelWidthHeightRatio;
        this.pendingRotationDegrees = format.rotationDegrees;
    }

    /* access modifiers changed from: protected */
    @CallSuper
    public void onQueueInputBuffer(DecoderInputBuffer decoderInputBuffer) {
        this.buffersInCodecCount++;
        this.lastInputTimeUs = Math.max(decoderInputBuffer.timeUs, this.lastInputTimeUs);
        if (Util.SDK_INT < 23 && this.tunneling) {
            maybeNotifyRenderedFirstFrame();
        }
    }

    /* access modifiers changed from: protected */
    public void onOutputFormatChanged(MediaCodec mediaCodec, MediaFormat mediaFormat) {
        int i;
        int i2;
        boolean z = mediaFormat.containsKey(KEY_CROP_RIGHT) && mediaFormat.containsKey(KEY_CROP_LEFT) && mediaFormat.containsKey(KEY_CROP_BOTTOM) && mediaFormat.containsKey(KEY_CROP_TOP);
        if (z) {
            i = (mediaFormat.getInteger(KEY_CROP_RIGHT) - mediaFormat.getInteger(KEY_CROP_LEFT)) + 1;
        } else {
            i = mediaFormat.getInteger("width");
        }
        this.currentWidth = i;
        if (z) {
            i2 = (mediaFormat.getInteger(KEY_CROP_BOTTOM) - mediaFormat.getInteger(KEY_CROP_TOP)) + 1;
        } else {
            i2 = mediaFormat.getInteger("height");
        }
        this.currentHeight = i2;
        this.currentPixelWidthHeightRatio = this.pendingPixelWidthHeightRatio;
        if (Util.SDK_INT < 21) {
            this.currentUnappliedRotationDegrees = this.pendingRotationDegrees;
        } else if (this.pendingRotationDegrees == 90 || this.pendingRotationDegrees == 270) {
            int i3 = this.currentWidth;
            this.currentWidth = this.currentHeight;
            this.currentHeight = i3;
            this.currentPixelWidthHeightRatio = 1.0f / this.currentPixelWidthHeightRatio;
        }
        mediaCodec.setVideoScalingMode(this.scalingMode);
    }

    /* access modifiers changed from: protected */
    public boolean processOutputBuffer(long j, long j2, MediaCodec mediaCodec, ByteBuffer byteBuffer, int i, int i2, long j3, boolean z) throws ExoPlaybackException {
        long j4 = j;
        long j5 = j2;
        MediaCodec mediaCodec2 = mediaCodec;
        int i3 = i;
        long j6 = j3;
        if (this.initialPositionUs == C.TIME_UNSET) {
            this.initialPositionUs = j4;
        }
        long j7 = j6 - this.outputStreamOffsetUs;
        if (z) {
            skipOutputBuffer(mediaCodec2, i3, j7);
            return true;
        }
        long j8 = j6 - j4;
        if (this.surface != this.dummySurface) {
            long elapsedRealtime = SystemClock.elapsedRealtime() * 1000;
            boolean z2 = getState() == 2;
            if (!this.renderedFirstFrame || (z2 && shouldForceRenderOutputBuffer(j8, elapsedRealtime - this.lastRenderTimeUs))) {
                if (Util.SDK_INT >= 21) {
                    renderOutputBufferV21(mediaCodec, i, j7, System.nanoTime());
                } else {
                    renderOutputBuffer(mediaCodec2, i3, j7);
                }
                return true;
            } else if (!z2 || j4 == this.initialPositionUs) {
                return false;
            } else {
                long j9 = j8 - (elapsedRealtime - j5);
                long nanoTime = System.nanoTime();
                long adjustReleaseTime = this.frameReleaseTimeHelper.adjustReleaseTime(j6, (j9 * 1000) + nanoTime);
                long j10 = (adjustReleaseTime - nanoTime) / 1000;
                if (shouldDropBuffersToKeyframe(j10, j5) && maybeDropBuffersToKeyframe(mediaCodec, i, j7, j)) {
                    return false;
                }
                if (shouldDropOutputBuffer(j10, j5)) {
                    dropOutputBuffer(mediaCodec2, i3, j7);
                    return true;
                }
                if (Util.SDK_INT >= 21) {
                    if (j10 < 50000) {
                        renderOutputBufferV21(mediaCodec, i, j7, adjustReleaseTime);
                        return true;
                    }
                } else if (j10 < 30000) {
                    if (j10 > 11000) {
                        try {
                            Thread.sleep((j10 - 10000) / 1000);
                        } catch (InterruptedException unused) {
                            Thread.currentThread().interrupt();
                            return false;
                        }
                    }
                    renderOutputBuffer(mediaCodec2, i3, j7);
                    return true;
                }
                return false;
            }
        } else if (!isBufferLate(j8)) {
            return false;
        } else {
            skipOutputBuffer(mediaCodec2, i3, j7);
            return true;
        }
    }

    /* access modifiers changed from: protected */
    @CallSuper
    public void onProcessedOutputBuffer(long j) {
        this.buffersInCodecCount--;
        while (this.pendingOutputStreamOffsetCount != 0 && j >= this.pendingOutputStreamSwitchTimesUs[0]) {
            this.outputStreamOffsetUs = this.pendingOutputStreamOffsetsUs[0];
            this.pendingOutputStreamOffsetCount--;
            System.arraycopy(this.pendingOutputStreamOffsetsUs, 1, this.pendingOutputStreamOffsetsUs, 0, this.pendingOutputStreamOffsetCount);
            System.arraycopy(this.pendingOutputStreamSwitchTimesUs, 1, this.pendingOutputStreamSwitchTimesUs, 0, this.pendingOutputStreamOffsetCount);
        }
    }

    /* access modifiers changed from: protected */
    public boolean shouldDropOutputBuffer(long j, long j2) {
        return isBufferLate(j);
    }

    /* access modifiers changed from: protected */
    public boolean shouldDropBuffersToKeyframe(long j, long j2) {
        return isBufferVeryLate(j);
    }

    /* access modifiers changed from: protected */
    public boolean shouldForceRenderOutputBuffer(long j, long j2) {
        return isBufferLate(j) && j2 > 100000;
    }

    /* access modifiers changed from: protected */
    public void skipOutputBuffer(MediaCodec mediaCodec, int i, long j) {
        TraceUtil.beginSection("skipVideoBuffer");
        mediaCodec.releaseOutputBuffer(i, false);
        TraceUtil.endSection();
        this.decoderCounters.skippedOutputBufferCount++;
    }

    /* access modifiers changed from: protected */
    public void dropOutputBuffer(MediaCodec mediaCodec, int i, long j) {
        TraceUtil.beginSection("dropVideoBuffer");
        mediaCodec.releaseOutputBuffer(i, false);
        TraceUtil.endSection();
        updateDroppedBufferCounters(1);
    }

    /* access modifiers changed from: protected */
    public boolean maybeDropBuffersToKeyframe(MediaCodec mediaCodec, int i, long j, long j2) throws ExoPlaybackException {
        int skipSource = skipSource(j2);
        if (skipSource == 0) {
            return false;
        }
        this.decoderCounters.droppedToKeyframeCount++;
        updateDroppedBufferCounters(this.buffersInCodecCount + skipSource);
        flushCodec();
        return true;
    }

    /* access modifiers changed from: protected */
    public void updateDroppedBufferCounters(int i) {
        this.decoderCounters.droppedBufferCount += i;
        this.droppedFrames += i;
        this.consecutiveDroppedFrameCount += i;
        this.decoderCounters.maxConsecutiveDroppedBufferCount = Math.max(this.consecutiveDroppedFrameCount, this.decoderCounters.maxConsecutiveDroppedBufferCount);
        if (this.droppedFrames >= this.maxDroppedFramesToNotify) {
            maybeNotifyDroppedFrames();
        }
    }

    /* access modifiers changed from: protected */
    public void renderOutputBuffer(MediaCodec mediaCodec, int i, long j) {
        maybeNotifyVideoSizeChanged();
        TraceUtil.beginSection("releaseOutputBuffer");
        mediaCodec.releaseOutputBuffer(i, true);
        TraceUtil.endSection();
        this.lastRenderTimeUs = SystemClock.elapsedRealtime() * 1000;
        this.decoderCounters.renderedOutputBufferCount++;
        this.consecutiveDroppedFrameCount = 0;
        maybeNotifyRenderedFirstFrame();
    }

    /* access modifiers changed from: protected */
    @TargetApi(21)
    public void renderOutputBufferV21(MediaCodec mediaCodec, int i, long j, long j2) {
        maybeNotifyVideoSizeChanged();
        TraceUtil.beginSection("releaseOutputBuffer");
        mediaCodec.releaseOutputBuffer(i, j2);
        TraceUtil.endSection();
        this.lastRenderTimeUs = SystemClock.elapsedRealtime() * 1000;
        this.decoderCounters.renderedOutputBufferCount++;
        this.consecutiveDroppedFrameCount = 0;
        maybeNotifyRenderedFirstFrame();
    }

    private boolean shouldUseDummySurface(MediaCodecInfo mediaCodecInfo) {
        return Util.SDK_INT >= 23 && !this.tunneling && !codecNeedsSetOutputSurfaceWorkaround(mediaCodecInfo.name) && (!mediaCodecInfo.secure || DummySurface.isSecureSupported(this.context));
    }

    private void setJoiningDeadlineMs() {
        this.joiningDeadlineMs = this.allowedJoiningTimeMs > 0 ? SystemClock.elapsedRealtime() + this.allowedJoiningTimeMs : C.TIME_UNSET;
    }

    private void clearRenderedFirstFrame() {
        this.renderedFirstFrame = false;
        if (Util.SDK_INT >= 23 && this.tunneling) {
            MediaCodec codec = getCodec();
            if (codec != null) {
                this.tunnelingOnFrameRenderedListener = new OnFrameRenderedListenerV23(codec);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void maybeNotifyRenderedFirstFrame() {
        if (!this.renderedFirstFrame) {
            this.renderedFirstFrame = true;
            this.eventDispatcher.renderedFirstFrame(this.surface);
        }
    }

    private void maybeRenotifyRenderedFirstFrame() {
        if (this.renderedFirstFrame) {
            this.eventDispatcher.renderedFirstFrame(this.surface);
        }
    }

    private void clearReportedVideoSize() {
        this.reportedWidth = -1;
        this.reportedHeight = -1;
        this.reportedPixelWidthHeightRatio = -1.0f;
        this.reportedUnappliedRotationDegrees = -1;
    }

    private void maybeNotifyVideoSizeChanged() {
        if (this.currentWidth != -1 || this.currentHeight != -1) {
            if (this.reportedWidth != this.currentWidth || this.reportedHeight != this.currentHeight || this.reportedUnappliedRotationDegrees != this.currentUnappliedRotationDegrees || this.reportedPixelWidthHeightRatio != this.currentPixelWidthHeightRatio) {
                this.eventDispatcher.videoSizeChanged(this.currentWidth, this.currentHeight, this.currentUnappliedRotationDegrees, this.currentPixelWidthHeightRatio);
                this.reportedWidth = this.currentWidth;
                this.reportedHeight = this.currentHeight;
                this.reportedUnappliedRotationDegrees = this.currentUnappliedRotationDegrees;
                this.reportedPixelWidthHeightRatio = this.currentPixelWidthHeightRatio;
            }
        }
    }

    private void maybeRenotifyVideoSizeChanged() {
        if (this.reportedWidth != -1 || this.reportedHeight != -1) {
            this.eventDispatcher.videoSizeChanged(this.reportedWidth, this.reportedHeight, this.reportedUnappliedRotationDegrees, this.reportedPixelWidthHeightRatio);
        }
    }

    private void maybeNotifyDroppedFrames() {
        if (this.droppedFrames > 0) {
            long elapsedRealtime = SystemClock.elapsedRealtime();
            this.eventDispatcher.droppedFrames(this.droppedFrames, elapsedRealtime - this.droppedFrameAccumulationStartTimeMs);
            this.droppedFrames = 0;
            this.droppedFrameAccumulationStartTimeMs = elapsedRealtime;
        }
    }

    @TargetApi(23)
    private static void setOutputSurfaceV23(MediaCodec mediaCodec, Surface surface2) {
        mediaCodec.setOutputSurface(surface2);
    }

    @TargetApi(21)
    private static void configureTunnelingV21(MediaFormat mediaFormat, int i) {
        mediaFormat.setFeatureEnabled("tunneled-playback", true);
        mediaFormat.setInteger("audio-session-id", i);
    }

    /* access modifiers changed from: protected */
    @SuppressLint({"InlinedApi"})
    public MediaFormat getMediaFormat(Format format, CodecMaxValues codecMaxValues2, boolean z, int i) {
        MediaFormat mediaFormat = new MediaFormat();
        mediaFormat.setString("mime", format.sampleMimeType);
        mediaFormat.setInteger("width", format.width);
        mediaFormat.setInteger("height", format.height);
        MediaFormatUtil.setCsdBuffers(mediaFormat, format.initializationData);
        MediaFormatUtil.maybeSetFloat(mediaFormat, "frame-rate", format.frameRate);
        MediaFormatUtil.maybeSetInteger(mediaFormat, "rotation-degrees", format.rotationDegrees);
        MediaFormatUtil.maybeSetColorInfo(mediaFormat, format.colorInfo);
        mediaFormat.setInteger("max-width", codecMaxValues2.width);
        mediaFormat.setInteger("max-height", codecMaxValues2.height);
        MediaFormatUtil.maybeSetInteger(mediaFormat, "max-input-size", codecMaxValues2.inputSize);
        if (Util.SDK_INT >= 23) {
            mediaFormat.setInteger("priority", 0);
        }
        if (z) {
            mediaFormat.setInteger("auto-frc", 0);
        }
        if (i != 0) {
            configureTunnelingV21(mediaFormat, i);
        }
        return mediaFormat;
    }

    /* access modifiers changed from: protected */
    public CodecMaxValues getCodecMaxValues(MediaCodecInfo mediaCodecInfo, Format format, Format[] formatArr) throws DecoderQueryException {
        int i = format.width;
        int i2 = format.height;
        int maxInputSize = getMaxInputSize(mediaCodecInfo, format);
        if (formatArr.length == 1) {
            return new CodecMaxValues(i, i2, maxInputSize);
        }
        int i3 = i2;
        int i4 = maxInputSize;
        boolean z = false;
        int i5 = i;
        for (Format format2 : formatArr) {
            if (areAdaptationCompatible(mediaCodecInfo.adaptive, format, format2)) {
                z |= format2.width == -1 || format2.height == -1;
                i5 = Math.max(i5, format2.width);
                i3 = Math.max(i3, format2.height);
                i4 = Math.max(i4, getMaxInputSize(mediaCodecInfo, format2));
            }
        }
        if (z) {
            String str = TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("Resolutions unknown. Codec max resolution: ");
            sb.append(i5);
            sb.append(AvidJSONUtil.KEY_X);
            sb.append(i3);
            Log.w(str, sb.toString());
            Point codecMaxSize = getCodecMaxSize(mediaCodecInfo, format);
            if (codecMaxSize != null) {
                i5 = Math.max(i5, codecMaxSize.x);
                i3 = Math.max(i3, codecMaxSize.y);
                i4 = Math.max(i4, getMaxInputSize(mediaCodecInfo, format.sampleMimeType, i5, i3));
                String str2 = TAG;
                StringBuilder sb2 = new StringBuilder();
                sb2.append("Codec max resolution adjusted to: ");
                sb2.append(i5);
                sb2.append(AvidJSONUtil.KEY_X);
                sb2.append(i3);
                Log.w(str2, sb2.toString());
            }
        }
        return new CodecMaxValues(i5, i3, i4);
    }

    private static Point getCodecMaxSize(MediaCodecInfo mediaCodecInfo, Format format) throws DecoderQueryException {
        int[] iArr;
        boolean z = format.height > format.width;
        int i = z ? format.height : format.width;
        int i2 = z ? format.width : format.height;
        float f = ((float) i2) / ((float) i);
        for (int i3 : STANDARD_LONG_EDGE_VIDEO_PX) {
            int i4 = (int) (((float) i3) * f);
            if (i3 <= i || i4 <= i2) {
                return null;
            }
            if (Util.SDK_INT >= 21) {
                int i5 = z ? i4 : i3;
                if (!z) {
                    i3 = i4;
                }
                Point alignVideoSizeV21 = mediaCodecInfo.alignVideoSizeV21(i5, i3);
                if (mediaCodecInfo.isVideoSizeAndRateSupportedV21(alignVideoSizeV21.x, alignVideoSizeV21.y, (double) format.frameRate)) {
                    return alignVideoSizeV21;
                }
            } else {
                int ceilDivide = Util.ceilDivide(i3, 16) * 16;
                int ceilDivide2 = Util.ceilDivide(i4, 16) * 16;
                if (ceilDivide * ceilDivide2 <= MediaCodecUtil.maxH264DecodableFrameSize()) {
                    int i6 = z ? ceilDivide2 : ceilDivide;
                    if (z) {
                        ceilDivide2 = ceilDivide;
                    }
                    return new Point(i6, ceilDivide2);
                }
            }
        }
        return null;
    }

    private static int getMaxInputSize(MediaCodecInfo mediaCodecInfo, Format format) {
        if (format.maxInputSize == -1) {
            return getMaxInputSize(mediaCodecInfo, format.sampleMimeType, format.width, format.height);
        }
        int i = 0;
        for (int i2 = 0; i2 < format.initializationData.size(); i2++) {
            i += ((byte[]) format.initializationData.get(i2)).length;
        }
        return format.maxInputSize + i;
    }

    private static int getMaxInputSize(MediaCodecInfo mediaCodecInfo, String str, int i, int i2) {
        char c;
        int i3;
        if (i == -1 || i2 == -1) {
            return -1;
        }
        int i4 = 4;
        switch (str.hashCode()) {
            case -1664118616:
                if (str.equals(MimeTypes.VIDEO_H263)) {
                    c = 0;
                    break;
                }
            case -1662541442:
                if (str.equals(MimeTypes.VIDEO_H265)) {
                    c = 4;
                    break;
                }
            case 1187890754:
                if (str.equals(MimeTypes.VIDEO_MP4V)) {
                    c = 1;
                    break;
                }
            case 1331836730:
                if (str.equals(MimeTypes.VIDEO_H264)) {
                    c = 2;
                    break;
                }
            case 1599127256:
                if (str.equals(MimeTypes.VIDEO_VP8)) {
                    c = 3;
                    break;
                }
            case 1599127257:
                if (str.equals(MimeTypes.VIDEO_VP9)) {
                    c = 5;
                    break;
                }
            default:
                c = 65535;
                break;
        }
        switch (c) {
            case 0:
            case 1:
                i3 = i * i2;
                break;
            case 2:
                if (!"BRAVIA 4K 2015".equals(Util.MODEL) && (!"Amazon".equals(Util.MANUFACTURER) || (!"KFSOWI".equals(Util.MODEL) && (!"AFTS".equals(Util.MODEL) || !mediaCodecInfo.secure)))) {
                    i3 = Util.ceilDivide(i, 16) * Util.ceilDivide(i2, 16) * 16 * 16;
                    break;
                } else {
                    return -1;
                }
                break;
            case 3:
                i3 = i * i2;
                break;
            case 4:
            case 5:
                i3 = i * i2;
                break;
            default:
                return -1;
        }
        i4 = 2;
        return (i3 * 3) / (i4 * 2);
    }

    private static boolean areAdaptationCompatible(boolean z, Format format, Format format2) {
        return format.sampleMimeType.equals(format2.sampleMimeType) && format.rotationDegrees == format2.rotationDegrees && (z || (format.width == format2.width && format.height == format2.height)) && Util.areEqual(format.colorInfo, format2.colorInfo);
    }

    private static boolean deviceNeedsAutoFrcWorkaround() {
        return Util.SDK_INT <= 22 && "foster".equals(Util.DEVICE) && "NVIDIA".equals(Util.MANUFACTURER);
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:366:0x059a, code lost:
        r1 = 65535;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:367:0x059b, code lost:
        switch(r1) {
            case 0: goto L_0x059f;
            case 1: goto L_0x059f;
            case 2: goto L_0x059f;
            case 3: goto L_0x059f;
            case 4: goto L_0x059f;
            case 5: goto L_0x059f;
            case 6: goto L_0x059f;
            case 7: goto L_0x059f;
            case 8: goto L_0x059f;
            case 9: goto L_0x059f;
            case 10: goto L_0x059f;
            case 11: goto L_0x059f;
            case 12: goto L_0x059f;
            case 13: goto L_0x059f;
            case 14: goto L_0x059f;
            case 15: goto L_0x059f;
            case 16: goto L_0x059f;
            case 17: goto L_0x059f;
            case 18: goto L_0x059f;
            case 19: goto L_0x059f;
            case 20: goto L_0x059f;
            case 21: goto L_0x059f;
            case 22: goto L_0x059f;
            case 23: goto L_0x059f;
            case 24: goto L_0x059f;
            case 25: goto L_0x059f;
            case 26: goto L_0x059f;
            case 27: goto L_0x059f;
            case 28: goto L_0x059f;
            case 29: goto L_0x059f;
            case 30: goto L_0x059f;
            case 31: goto L_0x059f;
            case 32: goto L_0x059f;
            case 33: goto L_0x059f;
            case 34: goto L_0x059f;
            case 35: goto L_0x059f;
            case 36: goto L_0x059f;
            case 37: goto L_0x059f;
            case 38: goto L_0x059f;
            case 39: goto L_0x059f;
            case 40: goto L_0x059f;
            case 41: goto L_0x059f;
            case 42: goto L_0x059f;
            case 43: goto L_0x059f;
            case 44: goto L_0x059f;
            case 45: goto L_0x059f;
            case 46: goto L_0x059f;
            case 47: goto L_0x059f;
            case 48: goto L_0x059f;
            case 49: goto L_0x059f;
            case 50: goto L_0x059f;
            case 51: goto L_0x059f;
            case 52: goto L_0x059f;
            case 53: goto L_0x059f;
            case 54: goto L_0x059f;
            case 55: goto L_0x059f;
            case 56: goto L_0x059f;
            case 57: goto L_0x059f;
            case 58: goto L_0x059f;
            case 59: goto L_0x059f;
            case 60: goto L_0x059f;
            case 61: goto L_0x059f;
            case 62: goto L_0x059f;
            case 63: goto L_0x059f;
            case 64: goto L_0x059f;
            case 65: goto L_0x059f;
            case 66: goto L_0x059f;
            case 67: goto L_0x059f;
            case 68: goto L_0x059f;
            case 69: goto L_0x059f;
            case 70: goto L_0x059f;
            case 71: goto L_0x059f;
            case 72: goto L_0x059f;
            case 73: goto L_0x059f;
            case 74: goto L_0x059f;
            case 75: goto L_0x059f;
            case 76: goto L_0x059f;
            case 77: goto L_0x059f;
            case 78: goto L_0x059f;
            case 79: goto L_0x059f;
            case 80: goto L_0x059f;
            case 81: goto L_0x059f;
            case 82: goto L_0x059f;
            case 83: goto L_0x059f;
            case 84: goto L_0x059f;
            case 85: goto L_0x059f;
            case 86: goto L_0x059f;
            case 87: goto L_0x059f;
            case 88: goto L_0x059f;
            case 89: goto L_0x059f;
            case 90: goto L_0x059f;
            case 91: goto L_0x059f;
            case 92: goto L_0x059f;
            case 93: goto L_0x059f;
            case 94: goto L_0x059f;
            case 95: goto L_0x059f;
            case 96: goto L_0x059f;
            case 97: goto L_0x059f;
            case 98: goto L_0x059f;
            case 99: goto L_0x059f;
            case 100: goto L_0x059f;
            case 101: goto L_0x059f;
            case 102: goto L_0x059f;
            case 103: goto L_0x059f;
            case 104: goto L_0x059f;
            case 105: goto L_0x059f;
            case 106: goto L_0x059f;
            case 107: goto L_0x059f;
            case 108: goto L_0x059f;
            case 109: goto L_0x059f;
            case 110: goto L_0x059f;
            case 111: goto L_0x059f;
            case 112: goto L_0x059f;
            case 113: goto L_0x059f;
            case 114: goto L_0x059f;
            case 115: goto L_0x059f;
            case 116: goto L_0x059f;
            case 117: goto L_0x059f;
            default: goto L_0x059e;
        };
     */
    /* JADX WARNING: Code restructure failed: missing block: B:369:0x059f, code lost:
        deviceNeedsSetOutputSurfaceWorkaround = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:370:0x05a1, code lost:
        r0 = com.google.android.exoplayer2.util.Util.MODEL;
        r1 = r0.hashCode();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:371:0x05aa, code lost:
        if (r1 == 2006354) goto L_0x05bc;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:373:0x05af, code lost:
        if (r1 == 2006367) goto L_0x05b2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:376:0x05b8, code lost:
        if (r0.equals("AFTN") == false) goto L_0x05c5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:377:0x05ba, code lost:
        r2 = 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:379:0x05c2, code lost:
        if (r0.equals("AFTA") == false) goto L_0x05c5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:381:0x05c5, code lost:
        r2 = 65535;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:382:0x05c6, code lost:
        switch(r2) {
            case 0: goto L_0x05ca;
            case 1: goto L_0x05ca;
            default: goto L_0x05c9;
        };
     */
    /* JADX WARNING: Code restructure failed: missing block: B:384:0x05ca, code lost:
        deviceNeedsSetOutputSurfaceWorkaround = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:385:0x05cc, code lost:
        evaluatedDeviceNeedsSetOutputSurfaceWorkaround = true;
     */
    public boolean codecNeedsSetOutputSurfaceWorkaround(String str) {
        char c = 27;
        char c2 = 0;
        if (Util.SDK_INT >= 27 || str.startsWith("OMX.google")) {
            return false;
        }
        synchronized (MediaCodecVideoRenderer.class) {
            if (!evaluatedDeviceNeedsSetOutputSurfaceWorkaround) {
                String str2 = Util.DEVICE;
                switch (str2.hashCode()) {
                    case -2144781245:
                        if (str2.equals("GIONEE_SWW1609")) {
                            c = '\'';
                            break;
                        }
                    case -2144781185:
                        if (str2.equals("GIONEE_SWW1627")) {
                            c = '(';
                            break;
                        }
                    case -2144781160:
                        if (str2.equals("GIONEE_SWW1631")) {
                            c = ')';
                            break;
                        }
                    case -2097309513:
                        if (str2.equals("K50a40")) {
                            c = '9';
                            break;
                        }
                    case -2022874474:
                        if (str2.equals("CP8676_I02")) {
                            c = 16;
                            break;
                        }
                    case -1978993182:
                        if (str2.equals("NX541J")) {
                            c = 'E';
                            break;
                        }
                    case -1978990237:
                        if (str2.equals("NX573J")) {
                            c = 'F';
                            break;
                        }
                    case -1936688988:
                        if (str2.equals("PGN528")) {
                            c = 'P';
                            break;
                        }
                    case -1936688066:
                        if (str2.equals("PGN610")) {
                            c = 'Q';
                            break;
                        }
                    case -1936688065:
                        if (str2.equals("PGN611")) {
                            c = 'R';
                            break;
                        }
                    case -1931988508:
                        if (str2.equals("AquaPowerM")) {
                            c = 10;
                            break;
                        }
                    case -1696512866:
                        if (str2.equals("XT1663")) {
                            c = 's';
                            break;
                        }
                    case -1680025915:
                        if (str2.equals("ComioS1")) {
                            c = 15;
                            break;
                        }
                    case -1615810839:
                        if (str2.equals("Phantom6")) {
                            c = 'S';
                            break;
                        }
                    case -1554255044:
                        if (str2.equals("vernee_M5")) {
                            c = 'l';
                            break;
                        }
                    case -1481772737:
                        if (str2.equals("panell_dl")) {
                            c = 'L';
                            break;
                        }
                    case -1481772730:
                        if (str2.equals("panell_ds")) {
                            c = 'M';
                            break;
                        }
                    case -1481772729:
                        if (str2.equals("panell_dt")) {
                            c = 'N';
                            break;
                        }
                    case -1320080169:
                        if (str2.equals("GiONEE_GBL7319")) {
                            c = '%';
                            break;
                        }
                    case -1217592143:
                        if (str2.equals("BRAVIA_ATV2")) {
                            c = 13;
                            break;
                        }
                    case -1180384755:
                        if (str2.equals("iris60")) {
                            c = '5';
                            break;
                        }
                    case -1139198265:
                        if (str2.equals("Slate_Pro")) {
                            c = '`';
                            break;
                        }
                    case -1052835013:
                        if (str2.equals("namath")) {
                            c = 'C';
                            break;
                        }
                    case -993250464:
                        if (str2.equals("A10-70F")) {
                            c = 3;
                            break;
                        }
                    case -965403638:
                        if (str2.equals("s905x018")) {
                            c = 'b';
                            break;
                        }
                    case -958336948:
                        if (str2.equals("ELUGA_Ray_X")) {
                            c = 26;
                            break;
                        }
                    case -879245230:
                        if (str2.equals("tcl_eu")) {
                            c = 'h';
                            break;
                        }
                    case -842500323:
                        if (str2.equals("nicklaus_f")) {
                            c = 'D';
                            break;
                        }
                    case -821392978:
                        if (str2.equals("A7000-a")) {
                            c = 6;
                            break;
                        }
                    case -797483286:
                        if (str2.equals("SVP-DTV15")) {
                            c = 'a';
                            break;
                        }
                    case -794946968:
                        if (str2.equals("watson")) {
                            c = 'm';
                            break;
                        }
                    case -788334647:
                        if (str2.equals("whyred")) {
                            c = 'n';
                            break;
                        }
                    case -782144577:
                        if (str2.equals("OnePlus5T")) {
                            c = 'G';
                            break;
                        }
                    case -575125681:
                        if (str2.equals("GiONEE_CBL7513")) {
                            c = '$';
                            break;
                        }
                    case -521118391:
                        if (str2.equals("GIONEE_GBL7360")) {
                            c = '&';
                            break;
                        }
                    case -430914369:
                        if (str2.equals("Pixi4-7_3G")) {
                            c = 'T';
                            break;
                        }
                    case -290434366:
                        if (str2.equals("taido_row")) {
                            c = 'c';
                            break;
                        }
                    case -282781963:
                        if (str2.equals("BLACK-1X")) {
                            c = 12;
                            break;
                        }
                    case -277133239:
                        if (str2.equals("Z12_PRO")) {
                            c = 't';
                            break;
                        }
                    case -173639913:
                        if (str2.equals("ELUGA_A3_Pro")) {
                            c = 23;
                            break;
                        }
                    case -56598463:
                        if (str2.equals("woods_fn")) {
                            c = 'p';
                            break;
                        }
                    case 2126:
                        if (str2.equals("C1")) {
                            c = 14;
                            break;
                        }
                    case 2564:
                        if (str2.equals("Q5")) {
                            c = '\\';
                            break;
                        }
                    case 2715:
                        if (str2.equals("V1")) {
                            c = 'i';
                            break;
                        }
                    case 2719:
                        if (str2.equals("V5")) {
                            c = 'k';
                            break;
                        }
                    case 3483:
                        if (str2.equals("mh")) {
                            c = '@';
                            break;
                        }
                    case 73405:
                        if (str2.equals("JGZ")) {
                            c = '8';
                            break;
                        }
                    case 75739:
                        if (str2.equals("M5c")) {
                            c = '<';
                            break;
                        }
                    case 76779:
                        if (str2.equals("MX6")) {
                            c = 'B';
                            break;
                        }
                    case 78669:
                        if (str2.equals("P85")) {
                            c = 'J';
                            break;
                        }
                    case 79305:
                        if (str2.equals("PLE")) {
                            c = 'V';
                            break;
                        }
                    case 80618:
                        if (str2.equals("QX1")) {
                            c = '^';
                            break;
                        }
                    case 88274:
                        if (str2.equals("Z80")) {
                            c = 'u';
                            break;
                        }
                    case 98846:
                        if (str2.equals("cv1")) {
                            c = 19;
                            break;
                        }
                    case 98848:
                        if (str2.equals("cv3")) {
                            c = 20;
                            break;
                        }
                    case 99329:
                        if (str2.equals("deb")) {
                            c = 21;
                            break;
                        }
                    case 101481:
                        if (str2.equals("flo")) {
                            c = '#';
                            break;
                        }
                    case 1513190:
                        if (str2.equals("1601")) {
                            c = 0;
                            break;
                        }
                    case 1514184:
                        if (str2.equals("1713")) {
                            c = 1;
                            break;
                        }
                    case 1514185:
                        if (str2.equals("1714")) {
                            c = 2;
                            break;
                        }
                    case 2436959:
                        if (str2.equals("P681")) {
                            c = 'I';
                            break;
                        }
                    case 2463773:
                        if (str2.equals("Q350")) {
                            c = 'X';
                            break;
                        }
                    case 2464648:
                        if (str2.equals("Q427")) {
                            c = 'Z';
                            break;
                        }
                    case 2689555:
                        if (str2.equals("XE2X")) {
                            c = 'r';
                            break;
                        }
                    case 3351335:
                        if (str2.equals("mido")) {
                            c = 'A';
                            break;
                        }
                    case 3386211:
                        if (str2.equals("p212")) {
                            c = 'H';
                            break;
                        }
                    case 41325051:
                        if (str2.equals("MEIZU_M5")) {
                            c = '?';
                            break;
                        }
                    case 55178625:
                        if (str2.equals("Aura_Note_2")) {
                            c = 11;
                            break;
                        }
                    case 61542055:
                        if (str2.equals("A1601")) {
                            c = 4;
                            break;
                        }
                    case 65355429:
                        if (str2.equals("E5643")) {
                            c = 22;
                            break;
                        }
                    case 66214468:
                        if (str2.equals("F3111")) {
                            c = 28;
                            break;
                        }
                    case 66214470:
                        if (str2.equals("F3113")) {
                            c = 29;
                            break;
                        }
                    case 66214473:
                        if (str2.equals("F3116")) {
                            c = 30;
                            break;
                        }
                    case 66215429:
                        if (str2.equals("F3211")) {
                            c = 31;
                            break;
                        }
                    case 66215431:
                        if (str2.equals("F3213")) {
                            c = ' ';
                            break;
                        }
                    case 66215433:
                        if (str2.equals("F3215")) {
                            c = '!';
                            break;
                        }
                    case 66216390:
                        if (str2.equals("F3311")) {
                            c = '\"';
                            break;
                        }
                    case 76402249:
                        if (str2.equals("PRO7S")) {
                            c = 'W';
                            break;
                        }
                    case 76404105:
                        if (str2.equals("Q4260")) {
                            c = 'Y';
                            break;
                        }
                    case 76404911:
                        if (str2.equals("Q4310")) {
                            c = '[';
                            break;
                        }
                    case 80963634:
                        if (str2.equals("V23GB")) {
                            c = 'j';
                            break;
                        }
                    case 82882791:
                        if (str2.equals("X3_HK")) {
                            c = 'q';
                            break;
                        }
                    case 102844228:
                        if (str2.equals("le_x6")) {
                            c = ':';
                            break;
                        }
                    case 165221241:
                        if (str2.equals("A2016a40")) {
                            c = 5;
                            break;
                        }
                    case 182191441:
                        if (str2.equals("CPY83_I00")) {
                            c = 18;
                            break;
                        }
                    case 245388979:
                        if (str2.equals("marino_f")) {
                            c = '>';
                            break;
                        }
                    case 287431619:
                        if (str2.equals("griffin")) {
                            c = '-';
                            break;
                        }
                    case 307593612:
                        if (str2.equals("A7010a48")) {
                            c = 8;
                            break;
                        }
                    case 308517133:
                        if (str2.equals("A7020a48")) {
                            c = 9;
                            break;
                        }
                    case 316215098:
                        if (str2.equals("TB3-730F")) {
                            c = 'd';
                            break;
                        }
                    case 316215116:
                        if (str2.equals("TB3-730X")) {
                            c = 'e';
                            break;
                        }
                    case 316246811:
                        if (str2.equals("TB3-850F")) {
                            c = 'f';
                            break;
                        }
                    case 316246818:
                        if (str2.equals("TB3-850M")) {
                            c = 'g';
                            break;
                        }
                    case 407160593:
                        if (str2.equals("Pixi5-10_4G")) {
                            c = 'U';
                            break;
                        }
                    case 507412548:
                        if (str2.equals("QM16XE_U")) {
                            c = ']';
                            break;
                        }
                    case 793982701:
                        if (str2.equals("GIONEE_WBL5708")) {
                            c = '*';
                            break;
                        }
                    case 794038622:
                        if (str2.equals("GIONEE_WBL7365")) {
                            c = '+';
                            break;
                        }
                    case 794040393:
                        if (str2.equals("GIONEE_WBL7519")) {
                            c = ',';
                            break;
                        }
                    case 835649806:
                        if (str2.equals("manning")) {
                            c = '=';
                            break;
                        }
                    case 917340916:
                        if (str2.equals("A7000plus")) {
                            c = 7;
                            break;
                        }
                    case 958008161:
                        if (str2.equals("j2xlteins")) {
                            c = '7';
                            break;
                        }
                    case 1060579533:
                        if (str2.equals("panell_d")) {
                            c = 'K';
                            break;
                        }
                    case 1150207623:
                        if (str2.equals("LS-5017")) {
                            c = ';';
                            break;
                        }
                    case 1176899427:
                        if (str2.equals("itel_S41")) {
                            c = '6';
                            break;
                        }
                    case 1280332038:
                        if (str2.equals("hwALE-H")) {
                            c = '/';
                            break;
                        }
                    case 1306947716:
                        if (str2.equals("EverStar_S")) {
                            break;
                        }
                    case 1349174697:
                        if (str2.equals("htc_e56ml_dtul")) {
                            c = '.';
                            break;
                        }
                    case 1522194893:
                        if (str2.equals("woods_f")) {
                            c = 'o';
                            break;
                        }
                    case 1691543273:
                        if (str2.equals("CPH1609")) {
                            c = 17;
                            break;
                        }
                    case 1709443163:
                        if (str2.equals("iball8735_9806")) {
                            c = '3';
                            break;
                        }
                    case 1865889110:
                        if (str2.equals("santoni")) {
                            c = '_';
                            break;
                        }
                    case 1906253259:
                        if (str2.equals("PB2-670M")) {
                            c = 'O';
                            break;
                        }
                    case 1977196784:
                        if (str2.equals("Infinix-X572")) {
                            c = '4';
                            break;
                        }
                    case 2029784656:
                        if (str2.equals("HWBLN-H")) {
                            c = '0';
                            break;
                        }
                    case 2030379515:
                        if (str2.equals("HWCAM-H")) {
                            c = '1';
                            break;
                        }
                    case 2047190025:
                        if (str2.equals("ELUGA_Note")) {
                            c = 24;
                            break;
                        }
                    case 2047252157:
                        if (str2.equals("ELUGA_Prim")) {
                            c = 25;
                            break;
                        }
                    case 2048319463:
                        if (str2.equals("HWVNS-H")) {
                            c = '2';
                            break;
                        }
                }
            }
        }
        return deviceNeedsSetOutputSurfaceWorkaround;
    }
}
