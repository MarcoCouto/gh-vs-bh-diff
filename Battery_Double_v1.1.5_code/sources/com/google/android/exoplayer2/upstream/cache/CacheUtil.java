package com.google.android.exoplayer2.upstream.cache;

import android.net.Uri;
import android.support.annotation.Nullable;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DataSpec;
import com.google.android.exoplayer2.upstream.cache.Cache.CacheException;
import com.google.android.exoplayer2.util.Assertions;
import com.google.android.exoplayer2.util.PriorityTaskManager;
import com.google.android.exoplayer2.util.PriorityTaskManager.PriorityTooLowException;
import com.google.android.exoplayer2.util.Util;
import java.io.EOFException;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicBoolean;

public final class CacheUtil {
    public static final int DEFAULT_BUFFER_SIZE_BYTES = 131072;

    public static class CachingCounters {
        public volatile long alreadyCachedBytes;
        public volatile long contentLength = -1;
        public volatile long newlyCachedBytes;

        public long totalCachedBytes() {
            return this.alreadyCachedBytes + this.newlyCachedBytes;
        }
    }

    public static String generateKey(Uri uri) {
        return uri.toString();
    }

    public static String getKey(DataSpec dataSpec) {
        return dataSpec.key != null ? dataSpec.key : generateKey(dataSpec.uri);
    }

    public static void getCached(DataSpec dataSpec, Cache cache, CachingCounters cachingCounters) {
        long j;
        DataSpec dataSpec2 = dataSpec;
        CachingCounters cachingCounters2 = cachingCounters;
        String key = getKey(dataSpec);
        long j2 = dataSpec2.absoluteStreamPosition;
        if (dataSpec2.length != -1) {
            j = dataSpec2.length;
            Cache cache2 = cache;
        } else {
            j = cache.getContentLength(key);
        }
        cachingCounters2.contentLength = j;
        cachingCounters2.alreadyCachedBytes = 0;
        cachingCounters2.newlyCachedBytes = 0;
        long j3 = j2;
        long j4 = j;
        while (j4 != 0) {
            long cachedLength = cache.getCachedLength(key, j3, j4 != -1 ? j4 : Long.MAX_VALUE);
            if (cachedLength > 0) {
                cachingCounters2.alreadyCachedBytes += cachedLength;
            } else {
                cachedLength = -cachedLength;
                if (cachedLength == Long.MAX_VALUE) {
                    return;
                }
            }
            j3 += cachedLength;
            if (j4 == -1) {
                cachedLength = 0;
            }
            j4 -= cachedLength;
        }
    }

    public static void cache(DataSpec dataSpec, Cache cache, DataSource dataSource, @Nullable CachingCounters cachingCounters, @Nullable AtomicBoolean atomicBoolean) throws IOException, InterruptedException {
        cache(dataSpec, cache, new CacheDataSource(cache, dataSource), new byte[131072], null, 0, cachingCounters, atomicBoolean, false);
    }

    public static void cache(DataSpec dataSpec, Cache cache, CacheDataSource cacheDataSource, byte[] bArr, PriorityTaskManager priorityTaskManager, int i, @Nullable CachingCounters cachingCounters, @Nullable AtomicBoolean atomicBoolean, boolean z) throws IOException, InterruptedException {
        long j;
        DataSpec dataSpec2 = dataSpec;
        Cache cache2 = cache;
        CachingCounters cachingCounters2 = cachingCounters;
        Assertions.checkNotNull(cacheDataSource);
        Assertions.checkNotNull(bArr);
        if (cachingCounters2 != null) {
            getCached(dataSpec2, cache2, cachingCounters2);
        } else {
            cachingCounters2 = new CachingCounters();
        }
        CachingCounters cachingCounters3 = cachingCounters2;
        String key = getKey(dataSpec);
        long j2 = dataSpec2.absoluteStreamPosition;
        long contentLength = dataSpec2.length != -1 ? dataSpec2.length : cache2.getContentLength(key);
        while (contentLength != 0) {
            if (atomicBoolean == null || !atomicBoolean.get()) {
                long cachedLength = cache.getCachedLength(key, j2, contentLength != -1 ? contentLength : Long.MAX_VALUE);
                if (cachedLength > 0) {
                    j = cachedLength;
                } else {
                    long j3 = -cachedLength;
                    j = j3;
                    if (readAndDiscard(dataSpec, j2, j3, cacheDataSource, bArr, priorityTaskManager, i, cachingCounters3) < j) {
                        if (z && contentLength != -1) {
                            throw new EOFException();
                        }
                        return;
                    }
                }
                j2 += j;
                if (contentLength == -1) {
                    j = 0;
                }
                contentLength -= j;
            } else {
                throw new InterruptedException();
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0093, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x0094, code lost:
        com.google.android.exoplayer2.util.Util.closeQuietly(r22);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x0097, code lost:
        throw r0;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0093 A[ExcHandler: all (r0v0 'th' java.lang.Throwable A[CUSTOM_DECLARE]), Splitter:B:3:0x000d] */
    private static long readAndDiscard(DataSpec dataSpec, long j, long j2, DataSource dataSource, byte[] bArr, PriorityTaskManager priorityTaskManager, int i, CachingCounters cachingCounters) throws IOException, InterruptedException {
        DataSpec dataSpec2;
        DataSource dataSource2 = dataSource;
        byte[] bArr2 = bArr;
        CachingCounters cachingCounters2 = cachingCounters;
        DataSpec dataSpec3 = dataSpec;
        while (true) {
            if (priorityTaskManager != null) {
                priorityTaskManager.proceed(i);
            }
            try {
                if (!Thread.interrupted()) {
                    dataSpec2 = new DataSpec(dataSpec3.uri, dataSpec3.postBody, j, (dataSpec3.position + j) - dataSpec3.absoluteStreamPosition, -1, dataSpec3.key, dataSpec3.flags | 2);
                    long open = dataSource2.open(dataSpec2);
                    if (cachingCounters2.contentLength == -1 && open != -1) {
                        cachingCounters2.contentLength = dataSpec2.absoluteStreamPosition + open;
                    }
                    long j3 = 0;
                    while (true) {
                        if (j3 == j2) {
                            break;
                        } else if (!Thread.interrupted()) {
                            int read = dataSource2.read(bArr2, 0, j2 != -1 ? (int) Math.min((long) bArr2.length, j2 - j3) : bArr2.length);
                            if (read != -1) {
                                long j4 = (long) read;
                                j3 += j4;
                                cachingCounters2.newlyCachedBytes += j4;
                            } else if (cachingCounters2.contentLength == -1) {
                                cachingCounters2.contentLength = dataSpec2.absoluteStreamPosition + j3;
                            }
                        } else {
                            throw new InterruptedException();
                        }
                    }
                    Util.closeQuietly(dataSource);
                    return j3;
                }
                throw new InterruptedException();
            } catch (PriorityTooLowException unused) {
                dataSpec3 = dataSpec2;
            } catch (Throwable th) {
            }
            Util.closeQuietly(dataSource);
        }
    }

    public static void remove(Cache cache, String str) {
        for (CacheSpan removeSpan : cache.getCachedSpans(str)) {
            try {
                cache.removeSpan(removeSpan);
            } catch (CacheException unused) {
            }
        }
    }

    private CacheUtil() {
    }
}
