package com.truenet.android;

import a.a.d.b.g;
import com.startapp.common.c.e;
import java.util.ArrayList;
import java.util.List;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/* compiled from: StartAppSDK */
public final class ValidationResults {
    @NotNull
    @e(b = ArrayList.class, c = ValidationResult.class)
    private final List<ValidationResult> results;

    public ValidationResults(@NotNull List<ValidationResult> list) {
        g.b(list, "results");
        this.results = list;
    }

    /* JADX WARNING: Incorrect type for immutable var: ssa=java.util.List, code=java.util.List<com.truenet.android.ValidationResult>, for r1v0, types: [java.util.List] */
    @NotNull
    public static /* synthetic */ ValidationResults copy$default(ValidationResults validationResults, List<ValidationResult> list, int i, Object obj) {
        if ((i & 1) != 0) {
            list = validationResults.results;
        }
        return validationResults.copy(list);
    }

    @NotNull
    public final List<ValidationResult> component1() {
        return this.results;
    }

    @NotNull
    public final ValidationResults copy(@NotNull List<ValidationResult> list) {
        g.b(list, "results");
        return new ValidationResults(list);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:4:0x0010, code lost:
        if (a.a.d.b.g.a((java.lang.Object) r1.results, (java.lang.Object) ((com.truenet.android.ValidationResults) r2).results) == false) goto L_0x0012;
     */
    public final boolean equals(@Nullable Object obj) {
        if (this != obj) {
            if (obj instanceof ValidationResults) {
            }
            return false;
        }
        return true;
    }

    @NotNull
    public final List<ValidationResult> getResults() {
        return this.results;
    }

    public final int hashCode() {
        List<ValidationResult> list = this.results;
        if (list != null) {
            return list.hashCode();
        }
        return 0;
    }

    @NotNull
    public final String toString() {
        StringBuilder sb = new StringBuilder("ValidationResults(results=");
        sb.append(this.results);
        sb.append(")");
        return sb.toString();
    }
}
