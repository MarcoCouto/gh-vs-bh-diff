package com.truenet.android;

import a.a.d.b.g;
import com.ironsource.mediationsdk.utils.GeneralPropertiesWorker;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.ironsource.sdk.constants.LocationConst;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/* compiled from: StartAppSDK */
public final class DeviceInfo {
    @NotNull
    private final String advertisingId;
    @NotNull
    private final String cellId;
    @NotNull
    private final String deviceManufacturer;
    @NotNull
    private final String deviceModel;
    @NotNull
    private final String deviceType;
    @NotNull
    private final String deviceVersion;
    @NotNull
    private final String isp;
    @NotNull
    private final String ispName;
    @NotNull
    private final String latitude;
    @NotNull
    private final String locale;
    @NotNull
    private final String locationAreaCode;
    @NotNull
    private final String longitude;
    @NotNull
    private final String networkOperName;
    @NotNull
    private final String networkType;
    @NotNull
    private final String osId;
    @NotNull
    private final String osVer;
    @NotNull
    private final String packageName;
    @NotNull
    private String publisherId;
    @NotNull
    private final String sdkVersion;
    @NotNull
    private final String signalLevel;
    @NotNull
    private final String userAgent;

    public DeviceInfo(@NotNull String str, @NotNull String str2, @NotNull String str3, @NotNull String str4, @NotNull String str5, @NotNull String str6, @NotNull String str7, @NotNull String str8, @NotNull String str9, @NotNull String str10, @NotNull String str11, @NotNull String str12, @NotNull String str13, @NotNull String str14, @NotNull String str15, @NotNull String str16, @NotNull String str17, @NotNull String str18, @NotNull String str19, @NotNull String str20, @NotNull String str21) {
        String str22 = str;
        String str23 = str2;
        String str24 = str3;
        String str25 = str4;
        String str26 = str5;
        String str27 = str6;
        String str28 = str7;
        String str29 = str8;
        String str30 = str9;
        String str31 = str10;
        String str32 = str11;
        String str33 = str12;
        String str34 = str13;
        String str35 = str14;
        String str36 = str15;
        String str37 = str16;
        g.b(str22, LocationConst.LATITUDE);
        g.b(str23, LocationConst.LONGITUDE);
        g.b(str24, "userAgent");
        g.b(str25, "locale");
        g.b(str26, "advertisingId");
        g.b(str27, "osId");
        g.b(str28, "osVer");
        g.b(str29, RequestParameters.DEVICE_MODEL);
        g.b(str30, "deviceManufacturer");
        g.b(str31, "deviceVersion");
        g.b(str32, "packageName");
        g.b(str33, "networkOperName");
        g.b(str34, "isp");
        g.b(str35, "ispName");
        g.b(str36, "cellId");
        g.b(str16, "locationAreaCode");
        g.b(str17, "networkType");
        g.b(str18, "signalLevel");
        g.b(str19, "deviceType");
        g.b(str20, GeneralPropertiesWorker.SDK_VERSION);
        g.b(str21, "publisherId");
        String str38 = str16;
        this.latitude = str22;
        this.longitude = str23;
        this.userAgent = str24;
        this.locale = str25;
        this.advertisingId = str26;
        this.osId = str27;
        this.osVer = str28;
        this.deviceModel = str29;
        this.deviceManufacturer = str30;
        this.deviceVersion = str31;
        this.packageName = str32;
        this.networkOperName = str33;
        this.isp = str34;
        this.ispName = str35;
        this.cellId = str15;
        this.locationAreaCode = str38;
        String str39 = str18;
        this.networkType = str17;
        this.signalLevel = str39;
        String str40 = str20;
        this.deviceType = str19;
        this.sdkVersion = str40;
        this.publisherId = str21;
    }

    @NotNull
    public static /* synthetic */ DeviceInfo copy$default(DeviceInfo deviceInfo, String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, String str9, String str10, String str11, String str12, String str13, String str14, String str15, String str16, String str17, String str18, String str19, String str20, String str21, int i, Object obj) {
        String str22;
        String str23;
        String str24;
        String str25;
        String str26;
        String str27;
        String str28;
        String str29;
        String str30;
        String str31;
        DeviceInfo deviceInfo2 = deviceInfo;
        int i2 = i;
        String str32 = (i2 & 1) != 0 ? deviceInfo2.latitude : str;
        String str33 = (i2 & 2) != 0 ? deviceInfo2.longitude : str2;
        String str34 = (i2 & 4) != 0 ? deviceInfo2.userAgent : str3;
        String str35 = (i2 & 8) != 0 ? deviceInfo2.locale : str4;
        String str36 = (i2 & 16) != 0 ? deviceInfo2.advertisingId : str5;
        String str37 = (i2 & 32) != 0 ? deviceInfo2.osId : str6;
        String str38 = (i2 & 64) != 0 ? deviceInfo2.osVer : str7;
        String str39 = (i2 & 128) != 0 ? deviceInfo2.deviceModel : str8;
        String str40 = (i2 & 256) != 0 ? deviceInfo2.deviceManufacturer : str9;
        String str41 = (i2 & 512) != 0 ? deviceInfo2.deviceVersion : str10;
        String str42 = (i2 & 1024) != 0 ? deviceInfo2.packageName : str11;
        String str43 = (i2 & 2048) != 0 ? deviceInfo2.networkOperName : str12;
        String str44 = (i2 & 4096) != 0 ? deviceInfo2.isp : str13;
        String str45 = (i2 & 8192) != 0 ? deviceInfo2.ispName : str14;
        String str46 = (i2 & 16384) != 0 ? deviceInfo2.cellId : str15;
        if ((i2 & 32768) != 0) {
            str22 = str46;
            str23 = deviceInfo2.locationAreaCode;
        } else {
            str22 = str46;
            str23 = str16;
        }
        if ((i2 & 65536) != 0) {
            str24 = str23;
            str25 = deviceInfo2.networkType;
        } else {
            str24 = str23;
            str25 = str17;
        }
        if ((i2 & 131072) != 0) {
            str26 = str25;
            str27 = deviceInfo2.signalLevel;
        } else {
            str26 = str25;
            str27 = str18;
        }
        if ((i2 & 262144) != 0) {
            str28 = str27;
            str29 = deviceInfo2.deviceType;
        } else {
            str28 = str27;
            str29 = str19;
        }
        if ((i2 & 524288) != 0) {
            str30 = str29;
            str31 = deviceInfo2.sdkVersion;
        } else {
            str30 = str29;
            str31 = str20;
        }
        return deviceInfo.copy(str32, str33, str34, str35, str36, str37, str38, str39, str40, str41, str42, str43, str44, str45, str22, str24, str26, str28, str30, str31, (i2 & 1048576) != 0 ? deviceInfo2.publisherId : str21);
    }

    @NotNull
    public final String component1() {
        return this.latitude;
    }

    @NotNull
    public final String component10() {
        return this.deviceVersion;
    }

    @NotNull
    public final String component11() {
        return this.packageName;
    }

    @NotNull
    public final String component12() {
        return this.networkOperName;
    }

    @NotNull
    public final String component13() {
        return this.isp;
    }

    @NotNull
    public final String component14() {
        return this.ispName;
    }

    @NotNull
    public final String component15() {
        return this.cellId;
    }

    @NotNull
    public final String component16() {
        return this.locationAreaCode;
    }

    @NotNull
    public final String component17() {
        return this.networkType;
    }

    @NotNull
    public final String component18() {
        return this.signalLevel;
    }

    @NotNull
    public final String component19() {
        return this.deviceType;
    }

    @NotNull
    public final String component2() {
        return this.longitude;
    }

    @NotNull
    public final String component20() {
        return this.sdkVersion;
    }

    @NotNull
    public final String component21() {
        return this.publisherId;
    }

    @NotNull
    public final String component3() {
        return this.userAgent;
    }

    @NotNull
    public final String component4() {
        return this.locale;
    }

    @NotNull
    public final String component5() {
        return this.advertisingId;
    }

    @NotNull
    public final String component6() {
        return this.osId;
    }

    @NotNull
    public final String component7() {
        return this.osVer;
    }

    @NotNull
    public final String component8() {
        return this.deviceModel;
    }

    @NotNull
    public final String component9() {
        return this.deviceManufacturer;
    }

    @NotNull
    public final DeviceInfo copy(@NotNull String str, @NotNull String str2, @NotNull String str3, @NotNull String str4, @NotNull String str5, @NotNull String str6, @NotNull String str7, @NotNull String str8, @NotNull String str9, @NotNull String str10, @NotNull String str11, @NotNull String str12, @NotNull String str13, @NotNull String str14, @NotNull String str15, @NotNull String str16, @NotNull String str17, @NotNull String str18, @NotNull String str19, @NotNull String str20, @NotNull String str21) {
        String str22 = str;
        String str23 = str2;
        String str24 = str3;
        String str25 = str4;
        String str26 = str5;
        String str27 = str6;
        String str28 = str7;
        String str29 = str8;
        String str30 = str9;
        String str31 = str10;
        String str32 = str11;
        String str33 = str12;
        String str34 = str13;
        String str35 = str14;
        String str36 = str15;
        String str37 = str16;
        String str38 = str17;
        String str39 = str18;
        String str40 = str19;
        String str41 = str20;
        String str42 = str21;
        String str43 = str22;
        g.b(str22, LocationConst.LATITUDE);
        g.b(str2, LocationConst.LONGITUDE);
        g.b(str3, "userAgent");
        g.b(str4, "locale");
        g.b(str5, "advertisingId");
        g.b(str6, "osId");
        g.b(str7, "osVer");
        g.b(str8, RequestParameters.DEVICE_MODEL);
        g.b(str9, "deviceManufacturer");
        g.b(str10, "deviceVersion");
        g.b(str11, "packageName");
        g.b(str12, "networkOperName");
        g.b(str13, "isp");
        g.b(str14, "ispName");
        g.b(str15, "cellId");
        g.b(str16, "locationAreaCode");
        g.b(str17, "networkType");
        g.b(str18, "signalLevel");
        g.b(str19, "deviceType");
        g.b(str20, GeneralPropertiesWorker.SDK_VERSION);
        g.b(str21, "publisherId");
        DeviceInfo deviceInfo = new DeviceInfo(str43, str23, str24, str25, str26, str27, str28, str29, str30, str31, str32, str33, str34, str35, str36, str37, str38, str39, str40, str41, str42);
        return deviceInfo;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:44:0x00d8, code lost:
        if (a.a.d.b.g.a((java.lang.Object) r2.publisherId, (java.lang.Object) r3.publisherId) != false) goto L_0x00dc;
     */
    public final boolean equals(@Nullable Object obj) {
        if (this != obj) {
            if (obj instanceof DeviceInfo) {
                DeviceInfo deviceInfo = (DeviceInfo) obj;
                if (g.a((Object) this.latitude, (Object) deviceInfo.latitude)) {
                    if (g.a((Object) this.longitude, (Object) deviceInfo.longitude)) {
                        if (g.a((Object) this.userAgent, (Object) deviceInfo.userAgent)) {
                            if (g.a((Object) this.locale, (Object) deviceInfo.locale)) {
                                if (g.a((Object) this.advertisingId, (Object) deviceInfo.advertisingId)) {
                                    if (g.a((Object) this.osId, (Object) deviceInfo.osId)) {
                                        if (g.a((Object) this.osVer, (Object) deviceInfo.osVer)) {
                                            if (g.a((Object) this.deviceModel, (Object) deviceInfo.deviceModel)) {
                                                if (g.a((Object) this.deviceManufacturer, (Object) deviceInfo.deviceManufacturer)) {
                                                    if (g.a((Object) this.deviceVersion, (Object) deviceInfo.deviceVersion)) {
                                                        if (g.a((Object) this.packageName, (Object) deviceInfo.packageName)) {
                                                            if (g.a((Object) this.networkOperName, (Object) deviceInfo.networkOperName)) {
                                                                if (g.a((Object) this.isp, (Object) deviceInfo.isp)) {
                                                                    if (g.a((Object) this.ispName, (Object) deviceInfo.ispName)) {
                                                                        if (g.a((Object) this.cellId, (Object) deviceInfo.cellId)) {
                                                                            if (g.a((Object) this.locationAreaCode, (Object) deviceInfo.locationAreaCode)) {
                                                                                if (g.a((Object) this.networkType, (Object) deviceInfo.networkType)) {
                                                                                    if (g.a((Object) this.signalLevel, (Object) deviceInfo.signalLevel)) {
                                                                                        if (g.a((Object) this.deviceType, (Object) deviceInfo.deviceType)) {
                                                                                            if (g.a((Object) this.sdkVersion, (Object) deviceInfo.sdkVersion)) {
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return false;
        }
        return true;
    }

    @NotNull
    public final String getAdvertisingId() {
        return this.advertisingId;
    }

    @NotNull
    public final String getCellId() {
        return this.cellId;
    }

    @NotNull
    public final String getDeviceManufacturer() {
        return this.deviceManufacturer;
    }

    @NotNull
    public final String getDeviceModel() {
        return this.deviceModel;
    }

    @NotNull
    public final String getDeviceType() {
        return this.deviceType;
    }

    @NotNull
    public final String getDeviceVersion() {
        return this.deviceVersion;
    }

    @NotNull
    public final String getIsp() {
        return this.isp;
    }

    @NotNull
    public final String getIspName() {
        return this.ispName;
    }

    @NotNull
    public final String getLatitude() {
        return this.latitude;
    }

    @NotNull
    public final String getLocale() {
        return this.locale;
    }

    @NotNull
    public final String getLocationAreaCode() {
        return this.locationAreaCode;
    }

    @NotNull
    public final String getLongitude() {
        return this.longitude;
    }

    @NotNull
    public final String getNetworkOperName() {
        return this.networkOperName;
    }

    @NotNull
    public final String getNetworkType() {
        return this.networkType;
    }

    @NotNull
    public final String getOsId() {
        return this.osId;
    }

    @NotNull
    public final String getOsVer() {
        return this.osVer;
    }

    @NotNull
    public final String getPackageName() {
        return this.packageName;
    }

    @NotNull
    public final String getPublisherId() {
        return this.publisherId;
    }

    @NotNull
    public final String getSdkVersion() {
        return this.sdkVersion;
    }

    @NotNull
    public final String getSignalLevel() {
        return this.signalLevel;
    }

    @NotNull
    public final String getUserAgent() {
        return this.userAgent;
    }

    public final int hashCode() {
        String str = this.latitude;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.longitude;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.userAgent;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.locale;
        int hashCode4 = (hashCode3 + (str4 != null ? str4.hashCode() : 0)) * 31;
        String str5 = this.advertisingId;
        int hashCode5 = (hashCode4 + (str5 != null ? str5.hashCode() : 0)) * 31;
        String str6 = this.osId;
        int hashCode6 = (hashCode5 + (str6 != null ? str6.hashCode() : 0)) * 31;
        String str7 = this.osVer;
        int hashCode7 = (hashCode6 + (str7 != null ? str7.hashCode() : 0)) * 31;
        String str8 = this.deviceModel;
        int hashCode8 = (hashCode7 + (str8 != null ? str8.hashCode() : 0)) * 31;
        String str9 = this.deviceManufacturer;
        int hashCode9 = (hashCode8 + (str9 != null ? str9.hashCode() : 0)) * 31;
        String str10 = this.deviceVersion;
        int hashCode10 = (hashCode9 + (str10 != null ? str10.hashCode() : 0)) * 31;
        String str11 = this.packageName;
        int hashCode11 = (hashCode10 + (str11 != null ? str11.hashCode() : 0)) * 31;
        String str12 = this.networkOperName;
        int hashCode12 = (hashCode11 + (str12 != null ? str12.hashCode() : 0)) * 31;
        String str13 = this.isp;
        int hashCode13 = (hashCode12 + (str13 != null ? str13.hashCode() : 0)) * 31;
        String str14 = this.ispName;
        int hashCode14 = (hashCode13 + (str14 != null ? str14.hashCode() : 0)) * 31;
        String str15 = this.cellId;
        int hashCode15 = (hashCode14 + (str15 != null ? str15.hashCode() : 0)) * 31;
        String str16 = this.locationAreaCode;
        int hashCode16 = (hashCode15 + (str16 != null ? str16.hashCode() : 0)) * 31;
        String str17 = this.networkType;
        int hashCode17 = (hashCode16 + (str17 != null ? str17.hashCode() : 0)) * 31;
        String str18 = this.signalLevel;
        int hashCode18 = (hashCode17 + (str18 != null ? str18.hashCode() : 0)) * 31;
        String str19 = this.deviceType;
        int hashCode19 = (hashCode18 + (str19 != null ? str19.hashCode() : 0)) * 31;
        String str20 = this.sdkVersion;
        int hashCode20 = (hashCode19 + (str20 != null ? str20.hashCode() : 0)) * 31;
        String str21 = this.publisherId;
        if (str21 != null) {
            i = str21.hashCode();
        }
        return hashCode20 + i;
    }

    public final void setPublisherId(@NotNull String str) {
        g.b(str, "<set-?>");
        this.publisherId = str;
    }

    @NotNull
    public final String toString() {
        StringBuilder sb = new StringBuilder("DeviceInfo(latitude=");
        sb.append(this.latitude);
        sb.append(", longitude=");
        sb.append(this.longitude);
        sb.append(", userAgent=");
        sb.append(this.userAgent);
        sb.append(", locale=");
        sb.append(this.locale);
        sb.append(", advertisingId=");
        sb.append(this.advertisingId);
        sb.append(", osId=");
        sb.append(this.osId);
        sb.append(", osVer=");
        sb.append(this.osVer);
        sb.append(", deviceModel=");
        sb.append(this.deviceModel);
        sb.append(", deviceManufacturer=");
        sb.append(this.deviceManufacturer);
        sb.append(", deviceVersion=");
        sb.append(this.deviceVersion);
        sb.append(", packageName=");
        sb.append(this.packageName);
        sb.append(", networkOperName=");
        sb.append(this.networkOperName);
        sb.append(", isp=");
        sb.append(this.isp);
        sb.append(", ispName=");
        sb.append(this.ispName);
        sb.append(", cellId=");
        sb.append(this.cellId);
        sb.append(", locationAreaCode=");
        sb.append(this.locationAreaCode);
        sb.append(", networkType=");
        sb.append(this.networkType);
        sb.append(", signalLevel=");
        sb.append(this.signalLevel);
        sb.append(", deviceType=");
        sb.append(this.deviceType);
        sb.append(", sdkVersion=");
        sb.append(this.sdkVersion);
        sb.append(", publisherId=");
        sb.append(this.publisherId);
        sb.append(")");
        return sb.toString();
    }
}
