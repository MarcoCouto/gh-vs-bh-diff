package com.truenet.android;

import a.a.a.a;
import a.a.d.b.g;
import com.ironsource.sdk.constants.Constants;
import com.startapp.common.c.e;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/* compiled from: StartAppSDK */
public final class Link {
    @NotNull
    private final String body;
    @NotNull
    @e(b = HashMap.class)
    private final Map<String, Set<String>> headers;
    @NotNull
    private final String htmlStorage;
    @NotNull
    private final String imageStorage;
    @NotNull
    private final String instanceId;
    @NotNull
    @e(a = true)
    private final b javascript;
    @NotNull
    private final String metaData;
    @NotNull
    private final String method;
    @NotNull
    private final String validationUrl;
    private final long waitSec;
    private final boolean webview;

    public Link() {
        this(null, null, null, null, null, null, null, 0, false, null, null, 2047, null);
    }

    public Link(@NotNull String str, @NotNull String str2, @NotNull Map<String, ? extends Set<String>> map, @NotNull String str3, @NotNull String str4, @NotNull String str5, @NotNull String str6, long j, boolean z, @NotNull b bVar, @NotNull String str7) {
        g.b(str, Constants.CONVERT_INSTANCE_ID);
        g.b(str2, "validationUrl");
        g.b(map, "headers");
        g.b(str3, "method");
        g.b(str4, TtmlNode.TAG_BODY);
        g.b(str5, "imageStorage");
        g.b(str6, "htmlStorage");
        g.b(bVar, "javascript");
        g.b(str7, com.startapp.android.publish.common.metaData.e.KEY_METADATA);
        this.instanceId = str;
        this.validationUrl = str2;
        this.headers = map;
        this.method = str3;
        this.body = str4;
        this.imageStorage = str5;
        this.htmlStorage = str6;
        this.waitSec = j;
        this.webview = z;
        this.javascript = bVar;
        this.metaData = str7;
    }

    public /* synthetic */ Link(String str, String str2, Map map, String str3, String str4, String str5, String str6, long j, boolean z, b bVar, String str7, int i, a aVar) {
        int i2 = i;
        String str8 = (i2 & 1) != 0 ? "" : str;
        this(str8, (i2 & 2) != 0 ? "" : str2, (i2 & 4) != 0 ? a.a() : map, (i2 & 8) != 0 ? HttpRequest.METHOD_GET : str3, (i2 & 16) != 0 ? "" : str4, (i2 & 32) != 0 ? "" : str5, (i2 & 64) != 0 ? "" : str6, (i2 & 128) != 0 ? 1 : j, (i2 & 256) != 0 ? false : z, (i2 & 512) != 0 ? new b(0) : bVar, (i2 & 1024) != 0 ? "" : str7);
    }

    @NotNull
    public static /* synthetic */ Link copy$default(Link link, String str, String str2, Map map, String str3, String str4, String str5, String str6, long j, boolean z, b bVar, String str7, int i, Object obj) {
        Link link2 = link;
        int i2 = i;
        return link.copy((i2 & 1) != 0 ? link2.instanceId : str, (i2 & 2) != 0 ? link2.validationUrl : str2, (i2 & 4) != 0 ? link2.headers : map, (i2 & 8) != 0 ? link2.method : str3, (i2 & 16) != 0 ? link2.body : str4, (i2 & 32) != 0 ? link2.imageStorage : str5, (i2 & 64) != 0 ? link2.htmlStorage : str6, (i2 & 128) != 0 ? link2.waitSec : j, (i2 & 256) != 0 ? link2.webview : z, (i2 & 512) != 0 ? link2.javascript : bVar, (i2 & 1024) != 0 ? link2.metaData : str7);
    }

    @NotNull
    public final String component1() {
        return this.instanceId;
    }

    @NotNull
    public final b component10() {
        return this.javascript;
    }

    @NotNull
    public final String component11() {
        return this.metaData;
    }

    @NotNull
    public final String component2() {
        return this.validationUrl;
    }

    @NotNull
    public final Map<String, Set<String>> component3() {
        return this.headers;
    }

    @NotNull
    public final String component4() {
        return this.method;
    }

    @NotNull
    public final String component5() {
        return this.body;
    }

    @NotNull
    public final String component6() {
        return this.imageStorage;
    }

    @NotNull
    public final String component7() {
        return this.htmlStorage;
    }

    public final long component8() {
        return this.waitSec;
    }

    public final boolean component9() {
        return this.webview;
    }

    @NotNull
    public final Link copy(@NotNull String str, @NotNull String str2, @NotNull Map<String, ? extends Set<String>> map, @NotNull String str3, @NotNull String str4, @NotNull String str5, @NotNull String str6, long j, boolean z, @NotNull b bVar, @NotNull String str7) {
        String str8 = str;
        g.b(str, Constants.CONVERT_INSTANCE_ID);
        String str9 = str2;
        g.b(str9, "validationUrl");
        Map<String, ? extends Set<String>> map2 = map;
        g.b(map2, "headers");
        String str10 = str3;
        g.b(str10, "method");
        String str11 = str4;
        g.b(str11, TtmlNode.TAG_BODY);
        String str12 = str5;
        g.b(str12, "imageStorage");
        String str13 = str6;
        g.b(str13, "htmlStorage");
        b bVar2 = bVar;
        g.b(bVar2, "javascript");
        String str14 = str7;
        g.b(str14, com.startapp.android.publish.common.metaData.e.KEY_METADATA);
        Link link = new Link(str8, str9, map2, str10, str11, str12, str13, j, z, bVar2, str14);
        return link;
    }

    public final boolean equals(@Nullable Object obj) {
        if (this != obj) {
            if (obj instanceof Link) {
                Link link = (Link) obj;
                if (g.a((Object) this.instanceId, (Object) link.instanceId) && g.a((Object) this.validationUrl, (Object) link.validationUrl) && g.a((Object) this.headers, (Object) link.headers) && g.a((Object) this.method, (Object) link.method) && g.a((Object) this.body, (Object) link.body) && g.a((Object) this.imageStorage, (Object) link.imageStorage) && g.a((Object) this.htmlStorage, (Object) link.htmlStorage)) {
                    if (this.waitSec == link.waitSec) {
                        if (!(this.webview == link.webview) || !g.a((Object) this.javascript, (Object) link.javascript) || !g.a((Object) this.metaData, (Object) link.metaData)) {
                            return false;
                        }
                    }
                }
            }
            return false;
        }
        return true;
    }

    @NotNull
    public final String getBody() {
        return this.body;
    }

    @NotNull
    public final Map<String, Set<String>> getHeaders() {
        return this.headers;
    }

    @NotNull
    public final String getHtmlStorage() {
        return this.htmlStorage;
    }

    @NotNull
    public final String getImageStorage() {
        return this.imageStorage;
    }

    @NotNull
    public final String getInstanceId() {
        return this.instanceId;
    }

    @NotNull
    public final b getJavascript() {
        return this.javascript;
    }

    @NotNull
    public final String getMetaData() {
        return this.metaData;
    }

    @NotNull
    public final String getMethod() {
        return this.method;
    }

    @NotNull
    public final String getValidationUrl() {
        return this.validationUrl;
    }

    public final long getWaitSec() {
        return this.waitSec;
    }

    public final boolean getWebview() {
        return this.webview;
    }

    public final int hashCode() {
        String str = this.instanceId;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.validationUrl;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        Map<String, Set<String>> map = this.headers;
        int hashCode3 = (hashCode2 + (map != null ? map.hashCode() : 0)) * 31;
        String str3 = this.method;
        int hashCode4 = (hashCode3 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.body;
        int hashCode5 = (hashCode4 + (str4 != null ? str4.hashCode() : 0)) * 31;
        String str5 = this.imageStorage;
        int hashCode6 = (hashCode5 + (str5 != null ? str5.hashCode() : 0)) * 31;
        String str6 = this.htmlStorage;
        int hashCode7 = (hashCode6 + (str6 != null ? str6.hashCode() : 0)) * 31;
        long j = this.waitSec;
        int i2 = (hashCode7 + ((int) (j ^ (j >>> 32)))) * 31;
        boolean z = this.webview;
        if (z) {
            z = true;
        }
        int i3 = (i2 + (z ? 1 : 0)) * 31;
        b bVar = this.javascript;
        int hashCode8 = (i3 + (bVar != null ? bVar.hashCode() : 0)) * 31;
        String str7 = this.metaData;
        if (str7 != null) {
            i = str7.hashCode();
        }
        return hashCode8 + i;
    }

    @NotNull
    public final String toString() {
        StringBuilder sb = new StringBuilder("Link(instanceId=");
        sb.append(this.instanceId);
        sb.append(", validationUrl=");
        sb.append(this.validationUrl);
        sb.append(", headers=");
        sb.append(this.headers);
        sb.append(", method=");
        sb.append(this.method);
        sb.append(", body=");
        sb.append(this.body);
        sb.append(", imageStorage=");
        sb.append(this.imageStorage);
        sb.append(", htmlStorage=");
        sb.append(this.htmlStorage);
        sb.append(", waitSec=");
        sb.append(this.waitSec);
        sb.append(", webview=");
        sb.append(this.webview);
        sb.append(", javascript=");
        sb.append(this.javascript);
        sb.append(", metaData=");
        sb.append(this.metaData);
        sb.append(")");
        return sb.toString();
    }
}
