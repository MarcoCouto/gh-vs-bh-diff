package com.truenet.android;

import a.a.d.b.f;
import a.a.d.b.g;
import a.a.d.b.h;
import a.a.d.b.m;
import a.a.e.a.C0001a;
import a.a.j;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import com.facebook.share.internal.MessengerShareContentUtility;
import com.ironsource.mediationsdk.utils.ServerResponseWrapper;
import com.startapp.common.b.a.b.C0095b;
import com.tapjoy.TJAdUnitConstants.String;
import com.truenet.android.c.C0100c;
import java.io.ByteArrayOutputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/* compiled from: StartAppSDK */
public final class TrueNetSDK implements com.startapp.common.b.a.a {
    private static final String BASE_INIT_URL = new String("https://validation-engine.truenet.ai");
    private static final String BASE_RESULT_URL = new String("https://result-api.truenet.ai");
    public static final a Companion = new a(0);
    private static final String INIT_URL;
    private static final int JOB_ID = 97764;
    @NotNull
    public static final String JOB_TAG = "TruenetCheckLinksJob";
    private static final String PREFS_ENABLED = "PrefsEnabled";
    private static final String PREFS_PUBLISHER_ID = "PrefsPublisherId";
    @NotNull
    public static final String PREFS_TAG = "TruenetJobKey";
    private static final String RESULT_URL;
    /* access modifiers changed from: private */
    public static final URL initUrl = new URL(INIT_URL);
    /* access modifiers changed from: private */
    public static int intervalPosition;
    /* access modifiers changed from: private */
    public static final List<Long> intervals;
    /* access modifiers changed from: private */
    public static long requestDelay;
    /* access modifiers changed from: private */
    public static final URL resultUrl = new URL(RESULT_URL);
    /* access modifiers changed from: private */
    public static ThreadFactory threadFactory = b.f4556a;
    /* access modifiers changed from: private */
    public static boolean wasInitCalled;

    /* compiled from: StartAppSDK */
    public static final class a {

        /* renamed from: com.truenet.android.TrueNetSDK$a$a reason: collision with other inner class name */
        /* compiled from: StartAppSDK */
        static final class C0097a extends h implements a.a.d.a.a<j> {

            /* renamed from: a reason: collision with root package name */
            final /* synthetic */ LinksData f4550a;
            final /* synthetic */ ConcurrentLinkedQueue b;
            final /* synthetic */ Context c;
            final /* synthetic */ a.a.d.a.a d;

            C0097a(LinksData linksData, ConcurrentLinkedQueue concurrentLinkedQueue, Context context, a.a.d.a.a aVar) {
                this.f4550a = linksData;
                this.b = concurrentLinkedQueue;
                this.c = context;
                this.d = aVar;
                super(0);
            }

            public final /* synthetic */ Object a() {
                if (this.f4550a.getBulkResponse()) {
                    String a2 = com.startapp.common.c.b.a((Object) new ValidationResults(a.a.a.a.b((Iterable<? extends T>) this.b)));
                    URL access$getResultUrl$cp = TrueNetSDK.resultUrl;
                    g.a((Object) a2, "json");
                    com.truenet.android.a.a.b(access$getResultUrl$cp, a2, this.c);
                }
                a aVar = TrueNetSDK.Companion;
                a.b(this.c, this.f4550a.getSleep());
                if (this.f4550a.getSleep() != 0) {
                    this.d.a();
                }
                return j.f696a;
            }
        }

        /* compiled from: StartAppSDK */
        static final class b extends h implements a.a.d.a.b<c, Integer, j> {

            /* renamed from: a reason: collision with root package name */
            final /* synthetic */ a.a.d.b.l.a f4551a;
            final /* synthetic */ LinksData b;
            final /* synthetic */ Context c;
            final /* synthetic */ ConcurrentLinkedQueue d;

            b(a.a.d.b.l.a aVar, LinksData linksData, Context context, ConcurrentLinkedQueue concurrentLinkedQueue) {
                this.f4551a = aVar;
                this.b = linksData;
                this.c = context;
                this.d = concurrentLinkedQueue;
                super(2);
            }

            /* JADX WARNING: Removed duplicated region for block: B:27:0x00cc  */
            /* JADX WARNING: Removed duplicated region for block: B:28:0x00d2  */
            public final /* synthetic */ Object a(Object obj, Object obj2) {
                String str;
                boolean z;
                c cVar = (c) obj;
                int intValue = ((Number) obj2).intValue();
                g.b(cVar, String.VIDEO_INFO);
                List<com.truenet.android.c.b> f = cVar.f();
                ArrayList arrayList = new ArrayList(a.a.a.a.a((Iterable<? extends T>) f));
                for (com.truenet.android.c.b bVar : f) {
                    T f2 = bVar.f();
                    if (f2 != null) {
                        this.f4551a.element = f2;
                    }
                    String a2 = bVar.a();
                    long b2 = bVar.b();
                    int c2 = bVar.c();
                    List d2 = bVar.d();
                    if (d2 == null) {
                        d2 = a.a.a.c.f678a;
                    }
                    RedirectsResult redirectsResult = new RedirectsResult(a2, b2, c2, d2);
                    arrayList.add(redirectsResult);
                }
                Link link = (Link) this.b.getValidation().get(intValue);
                String instanceId = link.getInstanceId();
                int d3 = cVar.d();
                long e = cVar.e();
                String g = cVar.g();
                String h = cVar.h();
                if (h != null) {
                    try {
                        z = com.truenet.android.a.a.a(new URL(link.getHtmlStorage()), h, this.c);
                    } catch (Exception unused) {
                        z = false;
                    }
                    if (z) {
                        str = link.getHtmlStorage();
                        String str2 = str;
                        ByteArrayOutputStream c3 = cVar.c();
                        String imageStorage = (c3 == null && com.startapp.a.a.a.a.a(c3, link.getImageStorage())) ? link.getImageStorage() : "";
                        a aVar = TrueNetSDK.Companion;
                        ValidationResult validationResult = new ValidationResult(instanceId, d3, e, arrayList, g, str2, imageStorage, a.d(this.c), (String) this.f4551a.element, link.getMetaData());
                        if (!this.b.getBulkResponse()) {
                            this.d.add(validationResult);
                        } else {
                            String a3 = com.startapp.common.c.b.a((Object) new ValidationResults(a.a.a.a.a(validationResult)));
                            URL access$getResultUrl$cp = TrueNetSDK.resultUrl;
                            g.a((Object) a3, "json");
                            com.truenet.android.a.a.b(access$getResultUrl$cp, a3, this.c);
                        }
                        return j.f696a;
                    }
                }
                str = "";
                String str22 = str;
                ByteArrayOutputStream c32 = cVar.c();
                String imageStorage2 = (c32 == null && com.startapp.a.a.a.a.a(c32, link.getImageStorage())) ? link.getImageStorage() : "";
                a aVar2 = TrueNetSDK.Companion;
                ValidationResult validationResult2 = new ValidationResult(instanceId, d3, e, arrayList, g, str22, imageStorage2, a.d(this.c), (String) this.f4551a.element, link.getMetaData());
                if (!this.b.getBulkResponse()) {
                }
                return j.f696a;
            }
        }

        /* compiled from: StartAppSDK */
        static final class c implements Runnable {

            /* renamed from: a reason: collision with root package name */
            private /* synthetic */ Context f4552a;
            private /* synthetic */ a.a.d.a.a b;

            c(Context context, a.a.d.a.a aVar) {
                this.f4552a = context;
                this.b = aVar;
            }

            public final void run() {
                boolean z = TrueNetSDK.requestDelay != 0;
                Log.d("JobManager", "sending initial request");
                URL access$getInitUrl$cp = TrueNetSDK.initUrl;
                a aVar = TrueNetSDK.Companion;
                String b2 = com.truenet.android.a.a.b(access$getInitUrl$cp, a.a(this.f4552a), this.f4552a);
                if (b2 != null) {
                    a aVar2 = TrueNetSDK.Companion;
                    a.a(this.f4552a, b2, this.b);
                    return;
                }
                a aVar3 = TrueNetSDK.Companion;
                a.a(z ? TrueNetSDK.intervalPosition : 1 + TrueNetSDK.intervalPosition, 0);
                this.b.a();
            }
        }

        /* compiled from: StartAppSDK */
        static final class d implements Runnable {

            /* renamed from: a reason: collision with root package name */
            final /* synthetic */ Context f4553a;
            private /* synthetic */ long b;

            /* renamed from: com.truenet.android.TrueNetSDK$a$d$a reason: collision with other inner class name */
            /* compiled from: StartAppSDK */
            static final class C0098a extends h implements a.a.d.a.a<String> {

                /* renamed from: a reason: collision with root package name */
                final /* synthetic */ d f4554a;
                final /* synthetic */ a.a.d.b.l.a b;

                C0098a(d dVar, a.a.d.b.l.a aVar) {
                    this.f4554a = dVar;
                    this.b = aVar;
                    super(0);
                }

                public final /* synthetic */ Object a() {
                    a.a.d.b.l.a aVar = this.b;
                    URL access$getInitUrl$cp = TrueNetSDK.initUrl;
                    a aVar2 = TrueNetSDK.Companion;
                    aVar.element = com.truenet.android.a.a.b(access$getInitUrl$cp, a.a(this.f4554a.f4553a), this.f4554a.f4553a);
                    return (String) this.b.element;
                }
            }

            /* compiled from: StartAppSDK */
            static final class b extends h implements a.a.d.a.a<j> {

                /* renamed from: a reason: collision with root package name */
                public static final b f4555a = new b();

                b() {
                    super(0);
                }

                public final /* bridge */ /* synthetic */ Object a() {
                    return j.f696a;
                }
            }

            d(long j, Context context) {
                this.b = j;
                this.f4553a = context;
            }

            public final void run() {
                a.a.d.b.l.a aVar = new a.a.d.b.l.a();
                aVar.element = null;
                if (this.b != 0 || new C0098a(this, aVar).a() == null) {
                    a aVar2 = TrueNetSDK.Companion;
                    a.a(0, this.b);
                    return;
                }
                a aVar3 = TrueNetSDK.Companion;
                Context context = this.f4553a;
                String str = (String) aVar.element;
                if (str == null) {
                    g.a();
                }
                a.a(context, str, b.f4555a);
            }
        }

        private a() {
        }

        public /* synthetic */ a(byte b2) {
            this();
        }

        public static void a(@NotNull Context context, @NotNull a.a.d.a.a<j> aVar) {
            g.b(context, "context");
            g.b(aVar, "finish");
            try {
                if (!context.getSharedPreferences(TrueNetSDK.PREFS_TAG, 0).getBoolean(TrueNetSDK.PREFS_ENABLED, true)) {
                    com.startapp.common.b.a.a((int) TrueNetSDK.JOB_ID);
                    aVar.a();
                    return;
                }
                Executors.newSingleThreadExecutor(TrueNetSDK.threadFactory).execute(new c(context, aVar));
            } catch (Exception e) {
                Thread currentThread = Thread.currentThread();
                g.a((Object) currentThread, "Thread.currentThread()");
                b(currentThread, (Throwable) e);
            }
        }

        public static void a(@NotNull Context context, @NotNull String str) {
            g.b(context, "context");
            g.b(str, "publisherID");
            try {
                SharedPreferences sharedPreferences = context.getSharedPreferences(TrueNetSDK.PREFS_TAG, 0);
                sharedPreferences.edit().putString(TrueNetSDK.PREFS_PUBLISHER_ID, str).apply();
                if (sharedPreferences.getBoolean(TrueNetSDK.PREFS_ENABLED, true) && !TrueNetSDK.wasInitCalled) {
                    c(context);
                    TrueNetSDK.wasInitCalled = true;
                }
            } catch (Exception e) {
                Thread currentThread = Thread.currentThread();
                g.a((Object) currentThread, "Thread.currentThread()");
                b(currentThread, (Throwable) e);
            }
        }

        public static void a(@NotNull Context context, boolean z) {
            g.b(context, "context");
            try {
                context.getSharedPreferences(TrueNetSDK.PREFS_TAG, 0).edit().putBoolean(TrueNetSDK.PREFS_ENABLED, z).apply();
                if (z && !TrueNetSDK.wasInitCalled) {
                    c(context);
                    TrueNetSDK.wasInitCalled = true;
                }
            } catch (Exception e) {
                Thread currentThread = Thread.currentThread();
                g.a((Object) currentThread, "Thread.currentThread()");
                b(currentThread, (Throwable) e);
            }
        }

        /* access modifiers changed from: private */
        public static void b(Context context, long j) {
            Executors.newSingleThreadExecutor(TrueNetSDK.threadFactory).execute(new d(j, context));
        }

        /* access modifiers changed from: private */
        public static void b(Thread thread, Throwable th) {
            StringBuilder sb = new StringBuilder("Something went wrong in thread: ");
            sb.append(String.valueOf(thread.getId()));
            Log.e("TrueNetSDK", sb.toString(), th);
        }

        /* access modifiers changed from: private */
        public static String d(Context context) {
            String string = context.getSharedPreferences(TrueNetSDK.PREFS_TAG, 0).getString(TrueNetSDK.PREFS_PUBLISHER_ID, "Undefined");
            return string == null ? "Undefined" : string;
        }

        private static void c(Context context) {
            com.startapp.common.b.a.a(context);
            com.startapp.common.b.a.a((com.startapp.common.b.a.a) new TrueNetSDK());
            b(context, 0);
        }

        public static final /* synthetic */ String a(Context context) {
            DeviceInfo a2 = new a(context).a();
            a2.setPublisherId(d(context));
            String a3 = com.startapp.common.c.b.a((Object) a2);
            g.a((Object) a3, "JSONParser.toJson(info)");
            return a3;
        }

        public static final /* synthetic */ void a(Context context, String str, a.a.d.a.a aVar) {
            C0100c cVar;
            Context context2 = context;
            TrueNetSDK.intervalPosition = 0;
            TrueNetSDK.requestDelay = 0;
            LinksData linksData = (LinksData) com.startapp.common.c.b.a(str, LinksData.class);
            if (linksData.getValidation().size() != 0) {
                g.a((Object) linksData, ServerResponseWrapper.RESPONSE_FIELD);
                a.a.d.b.l.a aVar2 = new a.a.d.b.l.a();
                aVar2.element = "";
                List<Link> validation = linksData.getValidation();
                ArrayList arrayList = new ArrayList(a.a.a.a.a((Iterable<? extends T>) validation));
                for (Link link : validation) {
                    link.getJavascript();
                    link.getJavascript();
                    link.getJavascript();
                    StringBuilder sb = new StringBuilder();
                    sb.append("");
                    sb.append("");
                    sb.append("");
                    String sb2 = sb.toString();
                    C0100c cVar2 = C0100c.GET;
                    try {
                        cVar = C0100c.valueOf(link.getMethod());
                    } catch (IllegalArgumentException e) {
                        StringBuilder sb3 = new StringBuilder("Illegal HTTP method: ");
                        sb3.append(e.getMessage());
                        aVar2.element = sb3.toString();
                        cVar = cVar2;
                    }
                    String validationUrl = link.getValidationUrl();
                    Map headers = link.getHeaders();
                    String body = link.getBody();
                    if (!(sb2.length() > 0)) {
                        sb2 = null;
                    }
                    com.truenet.android.e.b bVar = new com.truenet.android.e.b(validationUrl, headers, cVar, body, sb2, link.getWaitSec(), link.getWebview());
                    arrayList.add(bVar);
                }
                e eVar = new e(context, arrayList, TrueNetSDK.threadFactory, linksData.getMaxRedirectTime(), linksData.getNumOfRedirect(), linksData.getValidateParallel());
                ConcurrentLinkedQueue concurrentLinkedQueue = new ConcurrentLinkedQueue();
                eVar.a((a.a.d.a.a<j>) new C0097a<j>(linksData, concurrentLinkedQueue, context2, aVar));
                eVar.a((a.a.d.a.b<? super c, ? super Integer, j>) new b<Object,Object,j>(aVar2, linksData, context2, concurrentLinkedQueue));
                return;
            }
            a.a.d.a.a aVar3 = aVar;
            b(context2, linksData.getSleep());
            if (linksData.getSleep() != 0) {
                aVar.a();
            }
        }

        public static final /* synthetic */ void a(int i, long j) {
            TrueNetSDK.requestDelay = j;
            boolean z = true;
            TrueNetSDK.intervalPosition = C0001a.a(i, TrueNetSDK.intervals.size() - 1);
            if (j == 0) {
                z = false;
            }
            if (!z) {
                j = TimeUnit.MINUTES.toMillis(((Number) TrueNetSDK.intervals.get(TrueNetSDK.intervalPosition)).longValue());
            }
            StringBuilder sb = new StringBuilder("scheduled millis: ");
            sb.append(String.valueOf(j));
            Log.d("JobManager", sb.toString());
            com.startapp.common.b.a.a((int) TrueNetSDK.JOB_ID);
            com.startapp.common.b.a.a(new com.startapp.common.b.b.a(TrueNetSDK.JOB_ID).a(j).a(false).a(TrueNetSDK.JOB_TAG, TrueNetSDK.PREFS_TAG).a().b());
        }
    }

    /* compiled from: StartAppSDK */
    static final class b implements ThreadFactory {

        /* renamed from: a reason: collision with root package name */
        public static final b f4556a = new b();

        /* compiled from: StartAppSDK */
        static final /* synthetic */ class a extends f implements a.a.d.a.b<Thread, Throwable, j> {
            a(a aVar) {
                super(aVar);
            }

            public final a.a.f.b a() {
                return m.a(a.class);
            }

            public final String b() {
                return "uncaughtExceptionHandler";
            }

            public final String c() {
                return "uncaughtExceptionHandler(Ljava/lang/Thread;Ljava/lang/Throwable;)V";
            }

            public final /* synthetic */ Object a(Object obj, Object obj2) {
                Thread thread = (Thread) obj;
                Throwable th = (Throwable) obj2;
                g.b(thread, "p1");
                g.b(th, "p2");
                a.b(thread, th);
                return j.f696a;
            }
        }

        b() {
        }

        @NotNull
        public final Thread newThread(Runnable runnable) {
            Thread thread = new Thread(runnable);
            thread.setUncaughtExceptionHandler(new f(new a(TrueNetSDK.Companion)));
            return thread;
        }
    }

    /* compiled from: StartAppSDK */
    static final class c implements com.startapp.common.b.a.b {

        /* renamed from: a reason: collision with root package name */
        private /* synthetic */ TrueNetSDK f4557a;

        /* compiled from: StartAppSDK */
        static final class a extends h implements a.a.d.a.a<j> {

            /* renamed from: a reason: collision with root package name */
            final /* synthetic */ Map f4558a;
            final /* synthetic */ Context b;
            final /* synthetic */ int c;
            final /* synthetic */ C0095b d;

            a(Map map, Context context, int i, C0095b bVar) {
                this.f4558a = map;
                this.b = context;
                this.c = i;
                this.d = bVar;
                super(0);
            }

            public final /* synthetic */ Object a() {
                StringBuilder sb = new StringBuilder("finished ");
                sb.append(String.valueOf(this.c));
                Log.d("JobManager", sb.toString());
                this.d.a(com.startapp.common.b.a.b.a.SUCCESS);
                return j.f696a;
            }
        }

        c(TrueNetSDK trueNetSDK) {
            this.f4557a = trueNetSDK;
        }

        public final void a(Context context, int i, Map<String, String> map, C0095b bVar) {
            synchronized (this.f4557a) {
                if (g.a((Object) (String) map.get(TrueNetSDK.JOB_TAG), (Object) TrueNetSDK.PREFS_TAG)) {
                    a aVar = TrueNetSDK.Companion;
                    g.a((Object) context, "context");
                    a.a(context, (a.a.d.a.a<j>) new a<j>(map, context, i, bVar));
                }
                j jVar = j.f696a;
            }
        }
    }

    public static final void enable(@NotNull Context context, boolean z) {
        a.a(context, z);
    }

    public static final void with(@NotNull Context context, @NotNull String str) {
        a.a(context, str);
    }

    @Nullable
    public final com.startapp.common.b.a.b create(int i) {
        if (i != JOB_ID) {
            return null;
        }
        Log.d("JobManager", "addJobCreator");
        return new c(this);
    }

    static {
        StringBuilder sb = new StringBuilder();
        sb.append(BASE_INIT_URL);
        sb.append("/api/initial");
        INIT_URL = sb.toString();
        StringBuilder sb2 = new StringBuilder();
        sb2.append(BASE_RESULT_URL);
        sb2.append("/api/result");
        RESULT_URL = sb2.toString();
        Long[] lArr = {Long.valueOf(15), Long.valueOf(30), Long.valueOf(60), Long.valueOf(120), Long.valueOf(240), Long.valueOf(480)};
        g.b(lArr, MessengerShareContentUtility.ELEMENTS);
        intervals = a.a.a.a.a((T[]) lArr);
    }
}
