package com.truenet.android;

import a.a.d.b.g;
import a.a.d.b.h;
import a.a.j;
import android.content.Context;
import android.os.Build.VERSION;
import android.webkit.CookieManager;
import com.truenet.android.c.C0100c;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/* compiled from: StartAppSDK */
public final class e {

    /* renamed from: a reason: collision with root package name */
    private final ExecutorService f4580a;
    @NotNull
    private a.a.d.a.a<j> b = d.f4583a;
    private int c;
    private final Context d;
    private final List<b> e;
    private final long f;
    private final int g;

    /* compiled from: StartAppSDK */
    public static final class a {
        private a() {
        }

        public /* synthetic */ a(byte b) {
            this();
        }
    }

    /* compiled from: StartAppSDK */
    public static final class b {
        @NotNull

        /* renamed from: a reason: collision with root package name */
        private final String f4581a;
        @Nullable
        private final Map<String, Set<String>> b;
        @NotNull
        private final C0100c c;
        @Nullable
        private final String d;
        @Nullable
        private final String e;
        private final long f;
        private final boolean g;

        public b(@NotNull String str, @Nullable Map<String, ? extends Set<String>> map, @NotNull C0100c cVar, @Nullable String str2, @Nullable String str3, long j, boolean z) {
            g.b(str, "url");
            g.b(cVar, "httpMethod");
            this.f4581a = str;
            this.b = map;
            this.c = cVar;
            this.d = str2;
            this.e = str3;
            this.f = j;
            this.g = z;
        }

        @NotNull
        public final String a() {
            return this.f4581a;
        }

        @Nullable
        public final Map<String, Set<String>> b() {
            return this.b;
        }

        @NotNull
        public final C0100c c() {
            return this.c;
        }

        @Nullable
        public final String d() {
            return this.d;
        }

        @Nullable
        public final String e() {
            return this.e;
        }

        public final boolean equals(@Nullable Object obj) {
            if (this != obj) {
                if (obj instanceof b) {
                    b bVar = (b) obj;
                    if (g.a((Object) this.f4581a, (Object) bVar.f4581a) && g.a((Object) this.b, (Object) bVar.b) && g.a((Object) this.c, (Object) bVar.c) && g.a((Object) this.d, (Object) bVar.d) && g.a((Object) this.e, (Object) bVar.e)) {
                        if (this.f == bVar.f) {
                            if (!(this.g == bVar.g)) {
                                return false;
                            }
                        }
                    }
                }
                return false;
            }
            return true;
        }

        public final long f() {
            return this.f;
        }

        public final boolean g() {
            return this.g;
        }

        public final int hashCode() {
            String str = this.f4581a;
            int i = 0;
            int hashCode = (str != null ? str.hashCode() : 0) * 31;
            Map<String, Set<String>> map = this.b;
            int hashCode2 = (hashCode + (map != null ? map.hashCode() : 0)) * 31;
            C0100c cVar = this.c;
            int hashCode3 = (hashCode2 + (cVar != null ? cVar.hashCode() : 0)) * 31;
            String str2 = this.d;
            int hashCode4 = (hashCode3 + (str2 != null ? str2.hashCode() : 0)) * 31;
            String str3 = this.e;
            if (str3 != null) {
                i = str3.hashCode();
            }
            int i2 = (hashCode4 + i) * 31;
            long j = this.f;
            int i3 = (i2 + ((int) (j ^ (j >>> 32)))) * 31;
            boolean z = this.g;
            if (z) {
                z = true;
            }
            return i3 + (z ? 1 : 0);
        }

        @NotNull
        public final String toString() {
            StringBuilder sb = new StringBuilder("Link(url=");
            sb.append(this.f4581a);
            sb.append(", headers=");
            sb.append(this.b);
            sb.append(", httpMethod=");
            sb.append(this.c);
            sb.append(", httpBody=");
            sb.append(this.d);
            sb.append(", javaScript=");
            sb.append(this.e);
            sb.append(", lastPageWebViewInterval=");
            sb.append(this.f);
            sb.append(", webviewOnly=");
            sb.append(this.g);
            sb.append(")");
            return sb.toString();
        }
    }

    /* compiled from: StartAppSDK */
    static final class c implements Runnable {

        /* renamed from: a reason: collision with root package name */
        private /* synthetic */ c f4582a;
        private /* synthetic */ int b;
        private /* synthetic */ e c;
        private /* synthetic */ a.a.d.a.b d;

        c(c cVar, int i, e eVar, a.a.d.a.b bVar) {
            this.f4582a = cVar;
            this.b = i;
            this.c = eVar;
            this.d = bVar;
        }

        public final void run() {
            this.f4582a.i();
            this.d.a(this.f4582a, Integer.valueOf(this.b));
            e.a(this.c);
        }
    }

    /* compiled from: StartAppSDK */
    static final class d extends h implements a.a.d.a.a<j> {

        /* renamed from: a reason: collision with root package name */
        public static final d f4583a = new d();

        d() {
            super(0);
        }

        public final /* bridge */ /* synthetic */ Object a() {
            return j.f696a;
        }
    }

    static {
        new a(0);
    }

    public e(@NotNull Context context, @NotNull List<b> list, @NotNull ThreadFactory threadFactory, long j, int i, int i2) {
        g.b(context, "context");
        g.b(list, "links");
        g.b(threadFactory, "threadFactory");
        this.d = context;
        this.e = list;
        this.f = j;
        this.g = i;
        this.f4580a = Executors.newFixedThreadPool(i2, threadFactory);
    }

    private final int a() {
        int i;
        synchronized (this) {
            this.c++;
            i = this.c;
        }
        return i;
    }

    public final void a(@NotNull a.a.d.a.a<j> aVar) {
        g.b(aVar, "<set-?>");
        this.b = aVar;
    }

    public static final /* synthetic */ void a(e eVar) {
        synchronized (eVar) {
            eVar.c--;
            if (eVar.c <= 0) {
                eVar.b.a();
            }
            j jVar = j.f696a;
        }
    }

    public final void a(@NotNull a.a.d.a.b<? super c, ? super Integer, j> bVar) {
        g.b(bVar, "block");
        if (VERSION.SDK_INT >= 21) {
            CookieManager.getInstance().removeAllCookies(null);
        }
        if (VERSION.SDK_INT <= 19) {
            CookieManager.getInstance().removeAllCookie();
        }
        int i = 0;
        for (Object next : this.e) {
            int i2 = i + 1;
            if (i >= 0) {
                b bVar2 = (b) next;
                a();
                c cVar = new c(this.d, bVar2.a(), bVar2.b());
                cVar.a(this.g);
                cVar.a(this.f);
                cVar.a(bVar2.e());
                cVar.a(bVar2.g());
                cVar.a(bVar2.c());
                cVar.b(bVar2.d());
                cVar.b(bVar2.f());
                this.f4580a.execute(new c(cVar, i, this, bVar));
                i = i2;
            } else {
                throw new ArithmeticException("Index overflow has happened.");
            }
        }
    }
}
