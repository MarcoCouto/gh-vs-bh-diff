package com.truenet.android;

import a.a.d.b.g;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.location.Location;
import android.os.Build;
import android.os.Build.VERSION;
import android.telephony.CellIdentityCdma;
import android.telephony.CellIdentityGsm;
import android.telephony.CellInfo;
import android.telephony.CellInfoCdma;
import android.telephony.CellInfoGsm;
import android.telephony.CellLocation;
import android.telephony.TelephonyManager;
import android.telephony.cdma.CdmaCellLocation;
import android.telephony.gsm.GsmCellLocation;
import android.util.DisplayMetrics;
import android.util.Log;
import com.facebook.places.model.PlaceFields;
import com.startapp.common.a.b;
import com.startapp.common.a.b.C0094b;
import com.startapp.common.c;
import com.truenet.android.a.d;
import java.util.List;
import java.util.Locale;
import org.jetbrains.annotations.NotNull;

/* compiled from: StartAppSDK */
public final class a {
    @NotNull

    /* renamed from: a reason: collision with root package name */
    private final Context f4559a;
    @NotNull
    private final TelephonyManager b;

    private a(@NotNull Context context, @NotNull TelephonyManager telephonyManager) {
        g.b(context, "context");
        g.b(telephonyManager, "telephonyManager");
        this.f4559a = context;
        this.b = telephonyManager;
        try {
            c.b(this.f4559a);
        } catch (Exception e) {
            Log.e("TrueNetSDK", "NetworkStats.init failed!", e);
        }
    }

    public /* synthetic */ a(Context context) {
        g.b(context, "$this$telephonyManager");
        Object systemService = context.getSystemService(PlaceFields.PHONE);
        if (systemService != null) {
            this(context, (TelephonyManager) systemService);
            return;
        }
        throw new a.a.g("null cannot be cast to non-null type android.telephony.TelephonyManager");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0081, code lost:
        if (r0 == null) goto L_0x0083;
     */
    @NotNull
    public final DeviceInfo a() {
        String str;
        String str2;
        String str3;
        String str4;
        int i;
        List list = a.a.a.c.f678a;
        String str5 = "";
        String str6 = "";
        if (!list.isEmpty()) {
            str5 = String.valueOf(((Location) a.a.a.a.a(list)).getLatitude());
            str6 = String.valueOf(((Location) a.a.a.a.a(list)).getLongitude());
        }
        String str7 = str5;
        String str8 = str6;
        Resources resources = this.f4559a.getResources();
        g.a((Object) resources, "context.resources");
        Configuration configuration = resources.getConfiguration();
        g.a((Object) configuration, "context.resources.configuration");
        g.b(configuration, "$this$currentLocale");
        boolean z = false;
        Locale locale = VERSION.SDK_INT >= 24 ? configuration.getLocales().get(0) : configuration.locale;
        C0094b a2 = b.a().a(this.f4559a);
        g.a((Object) a2, "AdvertisingIdSingleton.g…getAdvertisingId(context)");
        String a3 = a2.a();
        int phoneType = this.b.getPhoneType();
        if (!(phoneType == 0 || phoneType == 2)) {
            str = this.b.getNetworkOperatorName();
        }
        str = "";
        String str9 = str;
        TelephonyManager telephonyManager = this.b;
        String simOperator = telephonyManager.getSimState() == 5 ? telephonyManager.getSimOperator() : "";
        TelephonyManager telephonyManager2 = this.b;
        String simOperatorName = telephonyManager2.getSimState() == 5 ? telephonyManager2.getSimOperatorName() : "";
        boolean z2 = com.startapp.a.a.a.a.a(this.f4559a, "android.permission.ACCESS_FINE_LOCATION") || com.startapp.a.a.a.a.a(this.f4559a, "android.permission.ACCESS_COARSE_LOCATION");
        int i2 = -1;
        CellInfo cellInfo = null;
        if (z2) {
            TelephonyManager telephonyManager3 = this.b;
            g.b(telephonyManager3, "$this$cellId");
            if (VERSION.SDK_INT >= 26) {
                List allCellInfo = telephonyManager3.getAllCellInfo();
                CellInfo cellInfo2 = allCellInfo != null ? (CellInfo) a.a.a.a.b(allCellInfo) : null;
                if (cellInfo2 instanceof CellInfoGsm) {
                    CellIdentityGsm cellIdentity = ((CellInfoGsm) cellInfo2).getCellIdentity();
                    g.a((Object) cellIdentity, "info.cellIdentity");
                    i = cellIdentity.getCid();
                } else if (cellInfo2 instanceof CellInfoCdma) {
                    CellIdentityCdma cellIdentity2 = ((CellInfoCdma) cellInfo2).getCellIdentity();
                    g.a((Object) cellIdentity2, "info.cellIdentity");
                    i = cellIdentity2.getBasestationId();
                }
                str2 = String.valueOf(i);
            }
            CellLocation cellLocation = telephonyManager3.getCellLocation();
            i = cellLocation instanceof GsmCellLocation ? ((GsmCellLocation) cellLocation).getCid() : cellLocation instanceof CdmaCellLocation ? ((CdmaCellLocation) cellLocation).getBaseStationId() : -1;
            str2 = String.valueOf(i);
        } else {
            str2 = "";
        }
        String str10 = str2;
        if (z2) {
            TelephonyManager telephonyManager4 = this.b;
            g.b(telephonyManager4, "$this$cellLac");
            if (VERSION.SDK_INT >= 26) {
                List allCellInfo2 = telephonyManager4.getAllCellInfo();
                if (allCellInfo2 != null) {
                    cellInfo = (CellInfo) a.a.a.a.b(allCellInfo2);
                }
                if (cellInfo instanceof CellInfoGsm) {
                    CellIdentityGsm cellIdentity3 = ((CellInfoGsm) cellInfo).getCellIdentity();
                    g.a((Object) cellIdentity3, "info.cellIdentity");
                    i2 = cellIdentity3.getLac();
                    str3 = String.valueOf(i2);
                }
            }
            CellLocation cellLocation2 = telephonyManager4.getCellLocation();
            if (cellLocation2 instanceof GsmCellLocation) {
                i2 = ((GsmCellLocation) cellLocation2).getLac();
            }
            str3 = String.valueOf(i2);
        } else {
            str3 = "";
        }
        String str11 = str3;
        try {
            c a4 = c.a();
            g.a((Object) a4, "NetworkStats.get()");
            str4 = a4.b();
        } catch (Exception e) {
            str4 = e.getMessage();
            if (str4 == null) {
                str4 = "";
            }
        }
        com.truenet.android.a.d.a aVar = d.f4563a;
        String a5 = com.truenet.android.a.d.a.a(this.f4559a);
        Resources system = Resources.getSystem();
        g.a((Object) system, "Resources.getSystem()");
        DisplayMetrics displayMetrics = system.getDisplayMetrics();
        double d = (double) (((float) displayMetrics.widthPixels) / displayMetrics.xdpi);
        double d2 = (double) (((float) displayMetrics.heightPixels) / displayMetrics.ydpi);
        Double.isNaN(d);
        Double.isNaN(d);
        double d3 = d * d;
        Double.isNaN(d2);
        Double.isNaN(d2);
        if (Math.sqrt(d3 + (d2 * d2)) > 6.5d) {
            z = true;
        }
        String str12 = z ? "tablet" : PlaceFields.PHONE;
        String locale2 = locale.toString();
        String str13 = locale2;
        g.a((Object) locale2, "locale.toString()");
        g.a((Object) a3, "advertisingId");
        String valueOf = String.valueOf(VERSION.SDK_INT);
        String str14 = Build.MODEL;
        String str15 = str14;
        g.a((Object) str14, "Build.MODEL");
        String str16 = Build.MANUFACTURER;
        String str17 = str16;
        g.a((Object) str16, "Build.MANUFACTURER");
        String str18 = VERSION.RELEASE;
        String str19 = str18;
        g.a((Object) str18, "Build.VERSION.RELEASE");
        String packageName = this.f4559a.getPackageName();
        String str20 = packageName;
        g.a((Object) packageName, "context.packageName");
        g.a((Object) simOperator, "ips");
        g.a((Object) simOperatorName, "ipsName");
        Context context = this.f4559a;
        g.b(context, "$this$network");
        String a6 = new com.truenet.android.a.b(context).a();
        g.a((Object) str4, "signalLevel");
        DeviceInfo deviceInfo = new DeviceInfo(str7, str8, a5, str13, a3, "android", valueOf, str15, str17, str19, str20, str9, simOperator, simOperatorName, str10, str11, a6, str4, str12, "1.2.1", "");
        return deviceInfo;
    }
}
