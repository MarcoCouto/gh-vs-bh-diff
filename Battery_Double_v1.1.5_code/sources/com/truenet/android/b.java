package com.truenet.android;

import a.a.d.b.g;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/* compiled from: StartAppSDK */
public final class b {
    @Nullable

    /* renamed from: a reason: collision with root package name */
    private final String f4565a;
    @Nullable
    private final String b;
    @Nullable
    private final String c;

    private b() {
        this.f4565a = null;
        this.b = null;
        this.c = null;
    }

    public /* synthetic */ b(byte b2) {
        this();
    }

    public final boolean equals(@Nullable Object obj) {
        return this == obj || ((obj instanceof b) && g.a((Object) null, (Object) null) && g.a((Object) null, (Object) null) && g.a((Object) null, (Object) null));
    }

    public final int hashCode() {
        return 0;
    }

    @NotNull
    public final String toString() {
        StringBuilder sb = new StringBuilder("JavaScript(script=");
        sb.append(null);
        sb.append(", url=");
        sb.append(null);
        sb.append(", method=");
        sb.append(null);
        sb.append(")");
        return sb.toString();
    }
}
