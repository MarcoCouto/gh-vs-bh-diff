package com.truenet.android;

import a.a.d.b.m;
import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Build.VERSION;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.widget.helper.ItemTouchHelper.Callback;
import android.util.Log;
import android.webkit.CookieSyncManager;
import android.webkit.JavascriptInterface;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.ironsource.sdk.precache.DownloadManager;
import com.tapjoy.TJAdUnitConstants.String;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.net.CookieManager;
import java.net.CookieStore;
import java.net.HttpCookie;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.TimeUnit;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/* compiled from: StartAppSDK */
public final class c {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public static final String f4566a = new a(0).getClass().getSimpleName();
    /* access modifiers changed from: private */
    public static final a.a.h.c b = new a.a.h.c("^\\w+(://){1}.+$");
    private int c = -1;
    private long d = 30;
    private boolean e;
    @Nullable
    private String f;
    private long g = 1;
    @NotNull
    private C0100c h = C0100c.GET;
    @Nullable
    private String i;
    private final Map<String, Set<String>> j;
    @Nullable
    private ByteArrayOutputStream k;
    private long l;
    @NotNull
    private String m;
    @Nullable
    private String n;
    private ScheduledExecutorService o;
    private ScheduledFuture<?> p;
    private final CookieManager q;
    private final List<b> r;
    private final a.a.d s;
    private final a.a.d t;
    /* access modifiers changed from: private */
    public final Context u;
    private final String v;

    /* compiled from: StartAppSDK */
    public static final class a {
        private a() {
        }

        public /* synthetic */ a(byte b) {
            this();
        }

        public static final /* synthetic */ boolean a(String str) {
            return a.a.h.d.a(str, "http://play.google.com") || a.a.h.d.a(str, "https://play.google.com") || a.a.h.d.a(str, "http://itunes.apple.com") || a.a.h.d.a(str, "https://itunes.apple.com") || (!a.a.h.d.a(str, "http://") && !a.a.h.d.a(str, "https://") && c.b.a(str));
        }
    }

    /* compiled from: StartAppSDK */
    public static final class b {
        @NotNull

        /* renamed from: a reason: collision with root package name */
        private final String f4567a;
        private final long b;
        private final int c;
        @Nullable
        private final List<String> d;
        @Nullable
        private final String e;
        @Nullable
        private final String f;

        public /* synthetic */ b(String str, long j, int i, List list, String str2, int i2) {
            if ((i2 & 2) != 0) {
                j = 0;
            }
            this(str, j, (i2 & 4) != 0 ? Callback.DEFAULT_DRAG_ANIMATION_DURATION : i, (i2 & 8) != 0 ? null : list, (i2 & 16) != 0 ? null : str2, (String) null);
        }

        public b(@NotNull String str, long j, int i, @Nullable List<String> list, @Nullable String str2, @Nullable String str3) {
            a.a.d.b.g.b(str, "url");
            this.f4567a = str;
            this.b = j;
            this.c = i;
            this.d = list;
            this.e = str2;
            this.f = str3;
        }

        @NotNull
        public final String a() {
            return this.f4567a;
        }

        public final long b() {
            return this.b;
        }

        public final int c() {
            return this.c;
        }

        @Nullable
        public final List<String> d() {
            return this.d;
        }

        @Nullable
        public final String e() {
            return this.e;
        }

        public final boolean equals(@Nullable Object obj) {
            if (this != obj) {
                if (obj instanceof b) {
                    b bVar = (b) obj;
                    if (a.a.d.b.g.a((Object) this.f4567a, (Object) bVar.f4567a)) {
                        if (this.b == bVar.b) {
                            if (!(this.c == bVar.c) || !a.a.d.b.g.a((Object) this.d, (Object) bVar.d) || !a.a.d.b.g.a((Object) this.e, (Object) bVar.e) || !a.a.d.b.g.a((Object) this.f, (Object) bVar.f)) {
                                return false;
                            }
                        }
                    }
                }
                return false;
            }
            return true;
        }

        @Nullable
        public final String f() {
            return this.f;
        }

        public final int hashCode() {
            String str = this.f4567a;
            int i = 0;
            int hashCode = (str != null ? str.hashCode() : 0) * 31;
            long j = this.b;
            int i2 = (((hashCode + ((int) (j ^ (j >>> 32)))) * 31) + this.c) * 31;
            List<String> list = this.d;
            int hashCode2 = (i2 + (list != null ? list.hashCode() : 0)) * 31;
            String str2 = this.e;
            int hashCode3 = (hashCode2 + (str2 != null ? str2.hashCode() : 0)) * 31;
            String str3 = this.f;
            if (str3 != null) {
                i = str3.hashCode();
            }
            return hashCode3 + i;
        }

        @NotNull
        public final String toString() {
            StringBuilder sb = new StringBuilder("ConnectionInfo(url=");
            sb.append(this.f4567a);
            sb.append(", loadTime=");
            sb.append(this.b);
            sb.append(", httpCode=");
            sb.append(this.c);
            sb.append(", cookie=");
            sb.append(this.d);
            sb.append(", redirectUrl=");
            sb.append(this.e);
            sb.append(", error=");
            sb.append(this.f);
            sb.append(")");
            return sb.toString();
        }

        @NotNull
        public static /* synthetic */ b a(b bVar, String str, long j, int i, List list, String str2, String str3, int i2) {
            b bVar2 = bVar;
            String str4 = (i2 & 1) != 0 ? bVar2.f4567a : str;
            long j2 = (i2 & 2) != 0 ? bVar2.b : j;
            int i3 = (i2 & 4) != 0 ? bVar2.c : i;
            List list2 = (i2 & 8) != 0 ? bVar2.d : list;
            String str5 = (i2 & 16) != 0 ? bVar2.e : str2;
            String str6 = (i2 & 32) != 0 ? bVar2.f : str3;
            a.a.d.b.g.b(str4, "url");
            b bVar3 = new b(str4, j2, i3, list2, str5, str6);
            return bVar3;
        }
    }

    /* renamed from: com.truenet.android.c$c reason: collision with other inner class name */
    /* compiled from: StartAppSDK */
    public enum C0100c {
        GET,
        POST,
        PUT
    }

    /* compiled from: StartAppSDK */
    public final class d {
        public d() {
        }

        @JavascriptInterface
        public final void onFinish() {
            c.a(c.this);
        }

        @JavascriptInterface
        public final void onHtmlLoad(@NotNull String str) {
            a.a.d.b.g.b(str, String.HTML);
            c.this.j().offer(str);
        }
    }

    /* compiled from: StartAppSDK */
    public final class e extends WebViewClient {
        public e() {
        }

        private final void a(WebView webView, String str) {
            c.this.m();
            if (str != null) {
                if (webView != null) {
                    webView.stopLoading();
                }
                c.this.j().offer(str);
            }
        }

        public final void onPageFinished(@Nullable WebView webView, @Nullable String str) {
            c.a(c.this);
            super.onPageFinished(webView, str);
        }

        public final void onReceivedError(@Nullable WebView webView, @Nullable WebResourceRequest webResourceRequest, @Nullable WebResourceError webResourceError) {
            c.this.m();
            if (webView != null) {
                webView.stopLoading();
            }
            c.this.j().offer("");
            super.onReceivedError(webView, webResourceRequest, webResourceError);
        }

        @TargetApi(24)
        public final boolean shouldOverrideUrlLoading(@Nullable WebView webView, @Nullable WebResourceRequest webResourceRequest) {
            a(webView, String.valueOf(webResourceRequest != null ? webResourceRequest.getUrl() : null));
            return true;
        }

        public final boolean shouldOverrideUrlLoading(@Nullable WebView webView, @Nullable String str) {
            a(webView, str);
            return true;
        }
    }

    /* compiled from: StartAppSDK */
    static final class f implements Runnable {

        /* renamed from: a reason: collision with root package name */
        private /* synthetic */ c f4571a;
        private /* synthetic */ String b;
        private /* synthetic */ a.a.d.b.l.a c;

        f(c cVar, String str, a.a.d.b.l.a aVar) {
            this.f4571a = cVar;
            this.b = str;
            this.c = aVar;
        }

        public final void run() {
            WebView d = this.f4571a.k();
            if (d != null) {
                d.loadDataWithBaseURL(this.b, this.f4571a.h(), ((HttpURLConnection) this.c.element).getContentType(), ((HttpURLConnection) this.c.element).getContentEncoding(), null);
            }
        }
    }

    /* compiled from: StartAppSDK */
    static final class g implements Runnable {

        /* renamed from: a reason: collision with root package name */
        private /* synthetic */ c f4572a;
        private /* synthetic */ String b;
        private /* synthetic */ Map c;

        g(c cVar, String str, Map map) {
            this.f4572a = cVar;
            this.b = str;
            this.c = map;
        }

        public final void run() {
            WebView d = this.f4572a.k();
            if (d != null) {
                d.loadUrl(this.b, this.c);
            }
        }
    }

    /* compiled from: StartAppSDK */
    static final class h extends a.a.d.b.h implements a.a.d.a.a<SynchronousQueue<String>> {

        /* renamed from: a reason: collision with root package name */
        public static final h f4573a = new h();

        h() {
            super(0);
        }

        public final /* synthetic */ Object a() {
            return new SynchronousQueue();
        }
    }

    /* compiled from: StartAppSDK */
    static final class i implements Runnable {

        /* renamed from: a reason: collision with root package name */
        final /* synthetic */ c f4574a;
        private /* synthetic */ String b;

        /* compiled from: StartAppSDK */
        static final class a<T> implements ValueCallback<String> {

            /* renamed from: a reason: collision with root package name */
            private /* synthetic */ i f4575a;

            a(i iVar) {
                this.f4575a = iVar;
            }

            public final /* synthetic */ void onReceiveValue(Object obj) {
                c.a(this.f4575a.f4574a);
            }
        }

        i(String str, c cVar) {
            this.b = str;
            this.f4574a = cVar;
        }

        public final void run() {
            if (VERSION.SDK_INT >= 19) {
                WebView d = this.f4574a.k();
                if (d != null) {
                    d.evaluateJavascript(this.b, new a(this));
                }
            }
            if (VERSION.SDK_INT <= 18) {
                WebView d2 = this.f4574a.k();
                if (d2 != null) {
                    StringBuilder sb = new StringBuilder("javaScript:try { ");
                    sb.append(this.b);
                    sb.append(" } catch(e) {} finally { window.JSInterface.onFinish(); }");
                    d2.loadUrl(sb.toString());
                }
            }
        }
    }

    /* compiled from: StartAppSDK */
    static final class j implements Runnable {

        /* renamed from: a reason: collision with root package name */
        private /* synthetic */ c f4576a;

        j(c cVar) {
            this.f4576a = cVar;
        }

        public final void run() {
            this.f4576a.j().offer("");
        }
    }

    /* compiled from: StartAppSDK */
    static final class k implements Runnable {

        /* renamed from: a reason: collision with root package name */
        private /* synthetic */ c f4577a;
        private /* synthetic */ String b;

        k(c cVar, String str) {
            this.f4577a = cVar;
            this.b = str;
        }

        public final void run() {
            if (VERSION.SDK_INT >= 19) {
                WebView d = this.f4577a.k();
                if (d != null) {
                    d.evaluateJavascript(this.b, null);
                }
            }
            if (VERSION.SDK_INT <= 18) {
                WebView d2 = this.f4577a.k();
                if (d2 != null) {
                    StringBuilder sb = new StringBuilder("javaScript:");
                    sb.append(this.b);
                    d2.loadUrl(sb.toString());
                }
            }
        }
    }

    /* compiled from: StartAppSDK */
    static final class l extends a.a.d.b.h implements a.a.d.a.a<WebView> {

        /* renamed from: a reason: collision with root package name */
        final /* synthetic */ c f4578a;

        l(c cVar) {
            this.f4578a = cVar;
            super(0);
        }

        /* access modifiers changed from: private */
        @Nullable
        /* renamed from: b */
        public WebView a() {
            try {
                WebView webView = new WebView(this.f4578a.u);
                if (VERSION.SDK_INT >= 11) {
                    webView.setLayerType(1, null);
                }
                if (VERSION.SDK_INT >= 21) {
                    android.webkit.CookieManager.getInstance().acceptThirdPartyCookies(webView);
                }
                android.webkit.CookieManager.getInstance().setAcceptCookie(true);
                WebSettings settings = webView.getSettings();
                a.a.d.b.g.a((Object) settings, DownloadManager.SETTINGS);
                settings.setJavaScriptEnabled(true);
                webView.addJavascriptInterface(new d(), "JSInterface");
                webView.setWebChromeClient(new WebChromeClient());
                webView.setWebViewClient(new e());
                return webView;
            } catch (Exception e) {
                Log.e(c.f4566a, e.getMessage());
                return null;
            }
        }
    }

    static {
        a.a.f.d[] dVarArr = {m.a((a.a.d.b.j) new a.a.d.b.k(m.a(c.class), "queue", "getQueue()Ljava/util/concurrent/SynchronousQueue;")), m.a((a.a.d.b.j) new a.a.d.b.k(m.a(c.class), "webView", "getWebView()Landroid/webkit/WebView;"))};
    }

    private final b d(String str) {
        String str2;
        Throwable th;
        OutputStreamWriter outputStreamWriter;
        Throwable th2;
        String str3 = str;
        this.n = null;
        if (a.a(str)) {
            b bVar = new b(str, 0, 0, (List) null, (String) null, 62);
            return bVar;
        }
        a.a.d.b.l.a aVar = new a.a.d.b.l.a();
        aVar.element = null;
        try {
            URL url = new URL(str3);
            T openConnection = url.openConnection();
            if (openConnection != null) {
                T t2 = (HttpURLConnection) openConnection;
                boolean z = false;
                t2.setInstanceFollowRedirects(false);
                t2.setConnectTimeout(((int) this.d) * 1000);
                t2.setReadTimeout(((int) this.d) * 1000);
                String str4 = "User-Agent";
                com.truenet.android.a.d.a aVar2 = com.truenet.android.a.d.f4563a;
                t2.setRequestProperty(str4, com.truenet.android.a.d.a.a(this.u));
                Map<String, Set<String>> map = this.j;
                if (map != null) {
                    for (Entry entry : map.entrySet()) {
                        for (String addRequestProperty : (Iterable) entry.getValue()) {
                            t2.addRequestProperty((String) entry.getKey(), addRequestProperty);
                        }
                    }
                }
                CookieStore cookieStore = this.q.getCookieStore();
                a.a.d.b.g.a((Object) cookieStore, "cookieManager.cookieStore");
                List<HttpCookie> cookies = cookieStore.getCookies();
                a.a.d.b.g.a((Object) cookies, "cookies");
                if (!cookies.isEmpty()) {
                    String str5 = "Cookie";
                    ArrayList arrayList = new ArrayList(a.a.a.a.a((Iterable<? extends T>) cookies));
                    for (HttpCookie httpCookie : cookies) {
                        arrayList.add(httpCookie.toString());
                    }
                    Iterator it = arrayList.iterator();
                    if (it.hasNext()) {
                        Object next = it.next();
                        while (it.hasNext()) {
                            String str6 = (String) it.next();
                            String str7 = (String) next;
                            StringBuilder sb = new StringBuilder();
                            sb.append(str7);
                            sb.append("; ");
                            sb.append(str6);
                            next = sb.toString();
                        }
                        t2.setRequestProperty(str5, (String) next);
                    } else {
                        throw new UnsupportedOperationException("Empty collection can't be reduced.");
                    }
                }
                t2.setRequestMethod(this.h.name());
                int i2 = d.f4579a[this.h.ordinal()];
                if (i2 == 1 || i2 == 2) {
                    t2.setDoOutput(true);
                    this.h = C0100c.GET;
                    OutputStream outputStream = t2.getOutputStream();
                    try {
                        outputStreamWriter = new OutputStreamWriter(outputStream, "UTF-8");
                        try {
                            outputStreamWriter.write(this.i);
                            a.a.j jVar = a.a.j.f696a;
                            a.a.c.a.a(outputStreamWriter, null);
                            a.a.j jVar2 = a.a.j.f696a;
                            a.a.c.a.a(outputStream, null);
                        } catch (Throwable th3) {
                            th = th3;
                        }
                    } catch (Throwable th4) {
                        a.a.c.a.a(outputStream, th);
                        throw th4;
                    }
                }
                aVar.element = t2;
                long currentTimeMillis = System.currentTimeMillis();
                ((HttpURLConnection) aVar.element).connect();
                long currentTimeMillis2 = System.currentTimeMillis() - currentTimeMillis;
                String headerField = ((HttpURLConnection) aVar.element).getHeaderField("Location");
                if (headerField != null) {
                    a.a.h.c cVar = b;
                    a.a.d.b.g.a((Object) headerField, "nextUrl");
                    if (!cVar.a(headerField)) {
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append(url.getProtocol());
                        sb2.append("://");
                        sb2.append(url.getHost());
                        a.a.d.b.g.a((Object) headerField, "nextUrl");
                        if (!a.a.h.d.a(headerField, "/")) {
                            StringBuilder sb3 = new StringBuilder();
                            sb3.append('/');
                            sb3.append(headerField);
                            headerField = sb3.toString();
                        }
                        sb2.append(headerField);
                        str2 = sb2.toString();
                    } else {
                        str2 = headerField;
                    }
                } else {
                    str2 = null;
                }
                b bVar2 = new b(str, currentTimeMillis2, ((HttpURLConnection) aVar.element).getResponseCode(), (List) ((HttpURLConnection) aVar.element).getHeaderFields().get("Set-Cookie"), str2, 32);
                int responseCode = ((HttpURLConnection) aVar.element).getResponseCode();
                if (200 <= responseCode && 299 >= responseCode) {
                    InputStream inputStream = ((HttpURLConnection) aVar.element).getInputStream();
                    a.a.d.b.g.a((Object) inputStream, "connection.inputStream");
                    this.n = com.startapp.a.a.a.a.a((Reader) new BufferedReader(new InputStreamReader(inputStream, a.a.h.a.f693a), 8192));
                    long currentTimeMillis3 = System.currentTimeMillis() - currentTimeMillis;
                    new Handler(Looper.getMainLooper()).post(new f(this, str3, aVar));
                    String str8 = (String) j().take();
                    a.a.d.b.g.a((Object) str8, "jsRedirectUrl");
                    if (str8.length() == 0) {
                        z = true;
                    }
                    b a2 = z ? b.a(bVar2, null, currentTimeMillis3, 0, null, null, null, 61) : b.a(bVar2, null, currentTimeMillis3, 0, null, str8, null, 45);
                    HttpURLConnection httpURLConnection = (HttpURLConnection) aVar.element;
                    if (httpURLConnection != null) {
                        httpURLConnection.disconnect();
                    }
                    return a2;
                } else if (300 > responseCode || 399 < responseCode) {
                    b a3 = b.a(bVar2, null, 0, 0, null, null, null, 47);
                    HttpURLConnection httpURLConnection2 = (HttpURLConnection) aVar.element;
                    if (httpURLConnection2 != null) {
                        httpURLConnection2.disconnect();
                    }
                    return a3;
                } else {
                    HttpURLConnection httpURLConnection3 = (HttpURLConnection) aVar.element;
                    if (httpURLConnection3 != null) {
                        httpURLConnection3.disconnect();
                    }
                    return bVar2;
                }
            } else {
                throw new a.a.g("null cannot be cast to non-null type java.net.HttpURLConnection");
            }
        } catch (Exception e2) {
            try {
                b bVar3 = new b(str, 0, -1, null, (String) null, e2.getMessage());
                return bVar3;
            } finally {
                HttpURLConnection httpURLConnection4 = (HttpURLConnection) aVar.element;
                if (httpURLConnection4 != null) {
                    httpURLConnection4.disconnect();
                }
            }
        }
        a.a.c.a.a(outputStreamWriter, th2);
        throw th;
    }

    /* access modifiers changed from: private */
    public final SynchronousQueue<String> j() {
        return (SynchronousQueue) this.s.a();
    }

    /* access modifiers changed from: private */
    public final WebView k() {
        return (WebView) this.t.a();
    }

    /* access modifiers changed from: private */
    public final void m() {
        ScheduledFuture<?> scheduledFuture = this.p;
        if (scheduledFuture != null) {
            scheduledFuture.cancel(false);
        }
        this.p = null;
    }

    public final void a(int i2) {
        this.c = i2;
    }

    public final void a(long j2) {
        this.d = j2;
    }

    public final void a(@NotNull C0100c cVar) {
        a.a.d.b.g.b(cVar, "<set-?>");
        this.h = cVar;
    }

    public final void a(@Nullable String str) {
        this.f = str;
    }

    public final void a(boolean z) {
        this.e = z;
    }

    public final void b(long j2) {
        this.g = j2;
    }

    public final void b(@Nullable String str) {
        this.i = str;
    }

    @Nullable
    public final ByteArrayOutputStream c() {
        return this.k;
    }

    public final int d() {
        return this.r.size();
    }

    public final long e() {
        return this.l;
    }

    @NotNull
    public final List<b> f() {
        return a.a.a.a.b((Iterable<? extends T>) this.r);
    }

    @NotNull
    public final String g() {
        return this.m;
    }

    @Nullable
    public final String h() {
        return this.n;
    }

    private final b c(String str) {
        Map map;
        ArrayList arrayList = null;
        this.n = null;
        b bVar = new b(str, 0, 0, (List) null, (String) null, 62);
        if (a.a(str)) {
            return bVar;
        }
        Map<String, Set<String>> map2 = this.j;
        boolean z = true;
        if (map2 != null) {
            int size = map2.size();
            int i2 = size < 3 ? size + 1 : size < 1073741824 ? size + (size / 3) : Integer.MAX_VALUE;
            map = new LinkedHashMap(i2);
            for (Entry entry : map2.entrySet()) {
                Object key = entry.getKey();
                Iterator it = ((Iterable) entry.getValue()).iterator();
                if (it.hasNext()) {
                    Object next = it.next();
                    while (it.hasNext()) {
                        String str2 = (String) it.next();
                        String str3 = (String) next;
                        StringBuilder sb = new StringBuilder();
                        sb.append(str3);
                        sb.append("; ");
                        sb.append(str2);
                        next = sb.toString();
                    }
                    map.put(key, (String) next);
                } else {
                    throw new UnsupportedOperationException("Empty collection can't be reduced.");
                }
            }
        } else {
            map = null;
        }
        long currentTimeMillis = System.currentTimeMillis();
        new Handler(Looper.getMainLooper()).post(new g(this, str, map));
        String str4 = (String) j().take();
        String cookie = android.webkit.CookieManager.getInstance().getCookie(this.v);
        if (cookie != null) {
            List<String> a2 = a.a.h.d.a(cookie, new String[]{";"});
            if (a2 != null) {
                arrayList = new ArrayList(a.a.a.a.a((Iterable<? extends T>) a2));
                for (String a3 : a2) {
                    arrayList.add(new a.a.h.c("\\s+").a(a3, ""));
                }
            }
        }
        b a4 = b.a(bVar, null, System.currentTimeMillis() - currentTimeMillis, 0, arrayList, null, null, 53);
        a.a.d.b.g.a((Object) str4, "jsRedirectUrl");
        if (str4.length() != 0) {
            z = false;
        }
        if (!z) {
            a4 = b.a(a4, null, 0, 0, null, str4, null, 47);
        }
        return a4;
    }

    private final ByteArrayOutputStream l() {
        WebView k2 = k();
        if (k2 != null) {
            Bitmap a2 = com.startapp.a.a.a.a.a(k2);
            if (a2 != null) {
                ByteArrayOutputStream byteArrayOutputStream = null;
                Bitmap bitmap = a2;
                for (int i2 = 0; i2 < 10; i2++) {
                    byteArrayOutputStream = com.truenet.android.a.a.a(bitmap);
                    Thread.sleep(200);
                    WebView k3 = k();
                    if (k3 != null) {
                        Bitmap a3 = com.startapp.a.a.a.a.a(k3);
                        if (a3 != null) {
                            ByteArrayOutputStream a4 = com.truenet.android.a.a.a(a3);
                            a.a.d.b.g.b(a3, "$this$isSolidColor");
                            int[] iArr = new int[(a3.getWidth() * a3.getHeight())];
                            a3.getPixels(iArr, 0, a3.getWidth(), 0, 0, a3.getWidth(), a3.getHeight());
                            a.a.d.b.g.b(iArr, "$this$first");
                            boolean z = true;
                            if (!(iArr.length == 0)) {
                                int i3 = iArr[0];
                                int length = iArr.length;
                                int i4 = 1;
                                while (true) {
                                    if (i4 >= length) {
                                        break;
                                    } else if (i3 != iArr[i4]) {
                                        z = false;
                                        break;
                                    } else {
                                        i4++;
                                    }
                                }
                                if (!z && byteArrayOutputStream.size() >= a4.size()) {
                                    return a4;
                                }
                                bitmap = a3;
                            } else {
                                throw new NoSuchElementException("Array is empty.");
                            }
                        } else {
                            continue;
                        }
                    }
                }
                if (byteArrayOutputStream == null) {
                    a.a.d.b.g.a("curr");
                }
                return byteArrayOutputStream;
            }
        }
        return null;
    }

    public c(@NotNull Context context, @NotNull String str, @Nullable Map<String, ? extends Set<String>> map) {
        Map<String, Set<String>> map2;
        a.a.d.b.g.b(context, "context");
        a.a.d.b.g.b(str, "url");
        this.u = context;
        this.v = str;
        if (map != null) {
            a.a.d.b.g.b(map, "$this$toMutableMap");
            map2 = new LinkedHashMap<>(map);
        } else {
            map2 = null;
        }
        this.j = map2;
        this.m = this.v;
        this.o = Executors.newScheduledThreadPool(1);
        this.q = new CookieManager();
        this.r = new ArrayList();
        this.s = a.a.a.a((a.a.d.a.a<? extends T>) h.f4573a);
        this.t = a.a.a.a((a.a.d.a.a<? extends T>) new l<Object>(this));
    }

    public static final /* synthetic */ void a(c cVar) {
        cVar.m();
        cVar.p = cVar.o.schedule(new j(cVar), cVar.g, TimeUnit.SECONDS);
    }

    /* JADX WARNING: Removed duplicated region for block: B:34:0x00c5  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x00ef  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x00f6  */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x0115 A[EDGE_INSN: B:72:0x0115->B:51:0x0115 ?: BREAK  , SYNTHETIC] */
    public final void i() {
        long j2;
        boolean z;
        boolean z2;
        Map<String, Set<String>> map = this.j;
        if (map != null) {
            Set<String> set = (Set) map.remove("Set-Cookie");
            if (set != null) {
                if (VERSION.SDK_INT <= 19) {
                    CookieSyncManager.createInstance(this.u);
                }
                for (String str : set) {
                    CookieStore cookieStore = this.q.getCookieStore();
                    URI uri = new URI(this.v);
                    List parse = HttpCookie.parse(str);
                    a.a.d.b.g.a((Object) parse, "HttpCookie.parse(cookie)");
                    cookieStore.add(uri, (HttpCookie) a.a.a.a.a(parse));
                    android.webkit.CookieManager.getInstance().setCookie(this.v, str);
                }
                if (VERSION.SDK_INT <= 19) {
                    CookieSyncManager.getInstance().sync();
                }
            }
        }
        long currentTimeMillis = System.currentTimeMillis();
        b bVar = new b("", 0, 0, (List) null, this.v, 46);
        do {
            String e2 = bVar.e();
            if (e2 == null) {
                a.a.d.b.g.a();
            }
            b c2 = this.e ? c(e2) : d(e2);
            this.r.add(c2);
            j2 = 0;
            z = false;
            if (!(c2.e() != null || c2.b() == 0 || c2.c() == -1)) {
                String str2 = this.f;
                if (str2 != null) {
                    new Handler(Looper.getMainLooper()).post(new i(str2, this));
                    z2 = true;
                    if (!z2) {
                        String str3 = (String) j().take();
                        a.a.d.b.g.a((Object) str3, "newUrl");
                        if (!(str3.length() > 0)) {
                            str3 = null;
                        }
                        bVar = b.a(c2, null, 0, 0, null, str3, null, 47);
                    } else {
                        bVar = c2;
                    }
                    if (bVar.e() != null) {
                        break;
                    }
                    int size = this.r.size();
                    int i2 = this.c;
                    if ((size < i2 || i2 == -1) && System.currentTimeMillis() - currentTimeMillis < this.d * 1000) {
                        z = true;
                        continue;
                    }
                }
            }
            z2 = false;
            if (!z2) {
            }
            if (bVar.e() != null) {
            }
        } while (z);
        int c3 = bVar.c();
        if (200 <= c3 && 299 >= c3) {
            if (this.e) {
                new Handler(Looper.getMainLooper()).post(new k(this, "try { window.JSInterface.onHtmlLoad('<html>'+document.getElementsByTagName('html')[0].innerHTML+'</html>'); } catch(e) { window.JSInterface.onHtmlLoad(''); }"));
                Object take = j().take();
                a.a.d.b.g.a(take, "queue.take()");
                this.n = (String) take;
            }
            this.k = l();
        }
        for (b b2 : this.r) {
            j2 += b2.b();
        }
        this.l = j2;
        List<b> list = this.r;
        a.a.d.b.g.b(list, "$this$last");
        if (!list.isEmpty()) {
            a.a.d.b.g.b(list, "$this$lastIndex");
            this.m = ((b) list.get(list.size() - 1)).a();
            if (this.r.isEmpty()) {
                this.l = System.currentTimeMillis() - currentTimeMillis;
                return;
            }
            return;
        }
        throw new NoSuchElementException("List is empty.");
    }
}
