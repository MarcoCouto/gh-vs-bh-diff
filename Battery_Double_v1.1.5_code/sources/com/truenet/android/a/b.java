package com.truenet.android.a;

import a.a.d.b.g;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.startapp.a.a.a.a;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/* compiled from: StartAppSDK */
public final class b {
    @Nullable

    /* renamed from: a reason: collision with root package name */
    private final NetworkInfo f4561a;
    private final boolean b;
    private final boolean c;
    private final boolean d;
    @NotNull
    private final String e;
    @NotNull
    private final Context f;

    @NotNull
    public final String a() {
        return this.e;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0073, code lost:
        if (r4 != null) goto L_0x0086;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x0082, code lost:
        if (r4 == null) goto L_0x0084;
     */
    public b(@NotNull Context context) {
        NetworkInfo networkInfo;
        String str;
        g.b(context, "context");
        this.f = context;
        if (a.a(this.f, "android.permission.ACCESS_NETWORK_STATE")) {
            Context context2 = this.f;
            g.b(context2, "$this$connectivityManager");
            Object systemService = context2.getSystemService("connectivity");
            if (systemService != null) {
                networkInfo = ((ConnectivityManager) systemService).getActiveNetworkInfo();
            } else {
                throw new a.a.g("null cannot be cast to non-null type android.net.ConnectivityManager");
            }
        } else {
            networkInfo = null;
        }
        this.f4561a = networkInfo;
        NetworkInfo networkInfo2 = this.f4561a;
        boolean z = false;
        this.b = networkInfo2 != null ? networkInfo2.isConnected() : false;
        NetworkInfo networkInfo3 = this.f4561a;
        this.c = networkInfo3 != null && this.b && networkInfo3.getType() == 1;
        NetworkInfo networkInfo4 = this.f4561a;
        if (networkInfo4 != null && this.b && networkInfo4.getType() == 0) {
            z = true;
        }
        this.d = z;
        if (this.d) {
            NetworkInfo networkInfo5 = this.f4561a;
            if (networkInfo5 != null) {
                str = networkInfo5.getSubtypeName();
            }
        } else if (this.c) {
            NetworkInfo networkInfo6 = this.f4561a;
            if (networkInfo6 != null) {
                str = networkInfo6.getTypeName();
            }
        }
        str = "";
        this.e = str;
    }
}
