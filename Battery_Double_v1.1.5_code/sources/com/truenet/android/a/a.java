package com.truenet.android.a;

import a.a.d.b.g;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.util.Log;
import com.startapp.a.a.c.d;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.zip.GZIPOutputStream;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/* compiled from: StartAppSDK */
public final class a implements com.startapp.a.a.d.a {

    /* renamed from: a reason: collision with root package name */
    private final com.startapp.a.a.d.a f4560a;

    @NotNull
    public static final ByteArrayOutputStream a(@NotNull Bitmap bitmap) {
        g.b(bitmap, "$this$toJpegByteStream");
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(CompressFormat.JPEG, 100, byteArrayOutputStream);
        return byteArrayOutputStream;
    }

    public a(com.startapp.a.a.d.a aVar) {
        this.f4560a = aVar;
    }

    public final String a(String str) {
        GZIPOutputStream gZIPOutputStream;
        GZIPOutputStream gZIPOutputStream2 = null;
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            gZIPOutputStream = new GZIPOutputStream(byteArrayOutputStream);
            try {
                gZIPOutputStream.write(str.getBytes());
                d.a(gZIPOutputStream);
                String a2 = this.f4560a.a(com.startapp.a.a.c.a.a(byteArrayOutputStream.toByteArray()));
                d.a(gZIPOutputStream);
                return a2;
            } catch (Exception unused) {
                gZIPOutputStream2 = gZIPOutputStream;
                String str2 = "";
                d.a(gZIPOutputStream2);
                return str2;
            } catch (Throwable th) {
                th = th;
                d.a(gZIPOutputStream);
                throw th;
            }
        } catch (Exception unused2) {
            String str22 = "";
            d.a(gZIPOutputStream2);
            return str22;
        } catch (Throwable th2) {
            th = th2;
            gZIPOutputStream = null;
            d.a(gZIPOutputStream);
            throw th;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:48:0x0095 A[SYNTHETIC, Splitter:B:48:0x0095] */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x00a9  */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x00b0 A[SYNTHETIC, Splitter:B:59:0x00b0] */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x00c4  */
    public static boolean a(URL url, String str, Context context) {
        HttpURLConnection httpURLConnection;
        g.b(url, "$this$put");
        g.b(str, "data");
        BufferedOutputStream bufferedOutputStream = null;
        try {
            URLConnection openConnection = url.openConnection();
            if (openConnection != null) {
                httpURLConnection = (HttpURLConnection) openConnection;
                try {
                    httpURLConnection.setRequestProperty(HttpRequest.HEADER_CACHE_CONTROL, "no-cache");
                    if (context != null) {
                        String str2 = "User-Agent";
                        com.truenet.android.a.d.a aVar = d.f4563a;
                        httpURLConnection.setRequestProperty(str2, com.truenet.android.a.d.a.a(context));
                    }
                    httpURLConnection.setRequestMethod(HttpRequest.METHOD_PUT);
                    boolean z = true;
                    httpURLConnection.setDoOutput(true);
                    httpURLConnection.setRequestProperty("Content-Type", WebRequest.CONTENT_TYPE_HTML);
                    byte[] bytes = str.getBytes(a.a.h.a.f693a);
                    g.a((Object) bytes, "(this as java.lang.String).getBytes(charset)");
                    httpURLConnection.setFixedLengthStreamingMode(bytes.length);
                    httpURLConnection.setConnectTimeout(50000);
                    BufferedOutputStream bufferedOutputStream2 = new BufferedOutputStream(httpURLConnection.getOutputStream());
                    try {
                        bufferedOutputStream2.write(bytes);
                        bufferedOutputStream2.flush();
                        int responseCode = httpURLConnection.getResponseCode();
                        if (200 > responseCode || 299 < responseCode) {
                            z = false;
                        }
                        try {
                            bufferedOutputStream2.close();
                        } catch (Exception e) {
                            Log.e(url.getClass().getCanonicalName(), "stream closed with error!", e);
                        }
                        if (httpURLConnection != null) {
                            httpURLConnection.disconnect();
                        }
                        return z;
                    } catch (Exception unused) {
                        bufferedOutputStream = bufferedOutputStream2;
                        if (bufferedOutputStream != null) {
                        }
                        if (httpURLConnection != null) {
                        }
                        return false;
                    } catch (Throwable th) {
                        th = th;
                        bufferedOutputStream = bufferedOutputStream2;
                        if (bufferedOutputStream != null) {
                        }
                        if (httpURLConnection != null) {
                        }
                        throw th;
                    }
                } catch (Exception unused2) {
                    if (bufferedOutputStream != null) {
                    }
                    if (httpURLConnection != null) {
                    }
                    return false;
                } catch (Throwable th2) {
                    th = th2;
                    if (bufferedOutputStream != null) {
                    }
                    if (httpURLConnection != null) {
                    }
                    throw th;
                }
            } else {
                throw new a.a.g("null cannot be cast to non-null type java.net.HttpURLConnection");
            }
        } catch (Exception unused3) {
            httpURLConnection = null;
            if (bufferedOutputStream != null) {
                try {
                    bufferedOutputStream.close();
                } catch (Exception e2) {
                    Log.e(url.getClass().getCanonicalName(), "stream closed with error!", e2);
                }
            }
            if (httpURLConnection != null) {
                httpURLConnection.disconnect();
            }
            return false;
        } catch (Throwable th3) {
            th = th3;
            httpURLConnection = null;
            if (bufferedOutputStream != null) {
                try {
                    bufferedOutputStream.close();
                } catch (Exception e3) {
                    Log.e(url.getClass().getCanonicalName(), "stream closed with error!", e3);
                }
            }
            if (httpURLConnection != null) {
                httpURLConnection.disconnect();
            }
            throw th;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:60:0x00d6 A[SYNTHETIC, Splitter:B:60:0x00d6] */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x00de A[Catch:{ Exception -> 0x00da }] */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x00f1  */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x00fa A[SYNTHETIC, Splitter:B:74:0x00fa] */
    /* JADX WARNING: Removed duplicated region for block: B:79:0x0102 A[Catch:{ Exception -> 0x00fe }] */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x0115  */
    @Nullable
    public static String b(URL url, String str, Context context) {
        BufferedOutputStream bufferedOutputStream;
        BufferedInputStream bufferedInputStream;
        HttpURLConnection httpURLConnection;
        String str2;
        g.b(url, "$this$post");
        g.b(str, "data");
        BufferedInputStream bufferedInputStream2 = null;
        try {
            URLConnection openConnection = url.openConnection();
            if (openConnection != null) {
                httpURLConnection = (HttpURLConnection) openConnection;
                try {
                    httpURLConnection.setRequestProperty(HttpRequest.HEADER_CACHE_CONTROL, "no-cache");
                    if (context != null) {
                        String str3 = "User-Agent";
                        com.truenet.android.a.d.a aVar = d.f4563a;
                        httpURLConnection.setRequestProperty(str3, com.truenet.android.a.d.a.a(context));
                    }
                    httpURLConnection.setRequestMethod(HttpRequest.METHOD_POST);
                    httpURLConnection.setDoOutput(true);
                    httpURLConnection.setDoInput(true);
                    byte[] bytes = str.getBytes(a.a.h.a.f693a);
                    g.a((Object) bytes, "(this as java.lang.String).getBytes(charset)");
                    httpURLConnection.setFixedLengthStreamingMode(bytes.length);
                    httpURLConnection.setRequestProperty("Content-Type", "application/json");
                    httpURLConnection.setConnectTimeout(50000);
                    bufferedOutputStream = new BufferedOutputStream(httpURLConnection.getOutputStream());
                    try {
                        bufferedOutputStream.write(bytes);
                        bufferedOutputStream.flush();
                        if (httpURLConnection.getResponseCode() == 200) {
                            StringBuilder sb = new StringBuilder();
                            bufferedInputStream = new BufferedInputStream(httpURLConnection.getInputStream());
                            try {
                                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(bufferedInputStream));
                                a.a.d.b.l.a aVar2 = new a.a.d.b.l.a();
                                aVar2.element = null;
                                while (new c(aVar2, bufferedReader).a() != null) {
                                    sb.append((String) aVar2.element);
                                }
                                str2 = sb.toString();
                                bufferedInputStream2 = bufferedInputStream;
                            } catch (Exception unused) {
                                if (bufferedOutputStream != null) {
                                    try {
                                        bufferedOutputStream.close();
                                    } catch (Exception e) {
                                        Log.e(url.getClass().getCanonicalName(), "stream closed with error!", e);
                                        if (httpURLConnection != null) {
                                        }
                                        return null;
                                    }
                                }
                                if (bufferedInputStream != null) {
                                    bufferedInputStream.close();
                                }
                                if (httpURLConnection != null) {
                                    httpURLConnection.disconnect();
                                }
                                return null;
                            } catch (Throwable th) {
                                th = th;
                                bufferedInputStream2 = bufferedInputStream;
                                if (bufferedOutputStream != null) {
                                    try {
                                        bufferedOutputStream.close();
                                    } catch (Exception e2) {
                                        Log.e(url.getClass().getCanonicalName(), "stream closed with error!", e2);
                                        if (httpURLConnection != null) {
                                        }
                                        throw th;
                                    }
                                }
                                if (bufferedInputStream2 != null) {
                                    bufferedInputStream2.close();
                                }
                                if (httpURLConnection != null) {
                                    httpURLConnection.disconnect();
                                }
                                throw th;
                            }
                        } else {
                            str2 = null;
                        }
                        try {
                            bufferedOutputStream.close();
                            if (bufferedInputStream2 != null) {
                                bufferedInputStream2.close();
                            }
                        } catch (Exception e3) {
                            Log.e(url.getClass().getCanonicalName(), "stream closed with error!", e3);
                        }
                        if (httpURLConnection != null) {
                            httpURLConnection.disconnect();
                        }
                        return str2;
                    } catch (Exception unused2) {
                        bufferedInputStream = null;
                        if (bufferedOutputStream != null) {
                        }
                        if (bufferedInputStream != null) {
                        }
                        if (httpURLConnection != null) {
                        }
                        return null;
                    } catch (Throwable th2) {
                        th = th2;
                        if (bufferedOutputStream != null) {
                        }
                        if (bufferedInputStream2 != null) {
                        }
                        if (httpURLConnection != null) {
                        }
                        throw th;
                    }
                } catch (Exception unused3) {
                    bufferedOutputStream = null;
                    bufferedInputStream = null;
                    if (bufferedOutputStream != null) {
                    }
                    if (bufferedInputStream != null) {
                    }
                    if (httpURLConnection != null) {
                    }
                    return null;
                } catch (Throwable th3) {
                    th = th3;
                    bufferedOutputStream = null;
                    if (bufferedOutputStream != null) {
                    }
                    if (bufferedInputStream2 != null) {
                    }
                    if (httpURLConnection != null) {
                    }
                    throw th;
                }
            } else {
                throw new a.a.g("null cannot be cast to non-null type java.net.HttpURLConnection");
            }
        } catch (Exception unused4) {
            bufferedOutputStream = null;
            httpURLConnection = null;
            bufferedInputStream = null;
            if (bufferedOutputStream != null) {
            }
            if (bufferedInputStream != null) {
            }
            if (httpURLConnection != null) {
            }
            return null;
        } catch (Throwable th4) {
            th = th4;
            bufferedOutputStream = null;
            httpURLConnection = null;
            if (bufferedOutputStream != null) {
            }
            if (bufferedInputStream2 != null) {
            }
            if (httpURLConnection != null) {
            }
            throw th;
        }
    }
}
