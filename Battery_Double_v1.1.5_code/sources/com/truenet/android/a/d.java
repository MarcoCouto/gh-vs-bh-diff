package com.truenet.android.a;

import a.a.d.b.g;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.webkit.WebSettings;
import android.webkit.WebView;
import java.util.concurrent.SynchronousQueue;
import org.jetbrains.annotations.NotNull;

/* compiled from: StartAppSDK */
public final class d {

    /* renamed from: a reason: collision with root package name */
    public static final a f4563a;
    /* access modifiers changed from: private */
    public static final String b;
    /* access modifiers changed from: private */
    public static String c;

    /* compiled from: StartAppSDK */
    public static final class a {

        /* renamed from: com.truenet.android.a.d$a$a reason: collision with other inner class name */
        /* compiled from: StartAppSDK */
        static final class C0099a implements Runnable {

            /* renamed from: a reason: collision with root package name */
            private /* synthetic */ Context f4564a;
            private /* synthetic */ SynchronousQueue b;

            C0099a(Context context, SynchronousQueue synchronousQueue) {
                this.f4564a = context;
                this.b = synchronousQueue;
            }

            public final void run() {
                try {
                    WebSettings settings = new WebView(this.f4564a).getSettings();
                    g.a((Object) settings, "WebView(context).settings");
                    this.b.offer(settings.getUserAgentString());
                } catch (Exception e) {
                    Log.e(d.b, e.getMessage());
                    this.b.offer("undefined");
                }
            }
        }

        private a() {
        }

        public /* synthetic */ a(byte b) {
            this();
        }

        @NotNull
        public static String a(@NotNull Context context) {
            g.b(context, "context");
            if (d.c != null) {
                String a2 = d.c;
                if (a2 == null) {
                    g.a();
                }
                return a2;
            }
            SynchronousQueue synchronousQueue = new SynchronousQueue();
            new Handler(Looper.getMainLooper()).post(new C0099a(context, synchronousQueue));
            d.c = (String) synchronousQueue.take();
            String a3 = d.c;
            if (a3 == null) {
                g.a();
            }
            return a3;
        }
    }

    static {
        a aVar = new a(0);
        f4563a = aVar;
        b = aVar.getClass().getSimpleName();
    }
}
