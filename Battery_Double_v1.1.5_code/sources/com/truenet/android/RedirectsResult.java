package com.truenet.android;

import a.a.d.b.g;
import java.util.List;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/* compiled from: StartAppSDK */
public final class RedirectsResult {
    @NotNull
    private final List<String> cookies;
    private final long loadTime;
    private final int response;
    @NotNull
    private final String url;

    public RedirectsResult(@NotNull String str, long j, int i, @NotNull List<String> list) {
        g.b(str, "url");
        g.b(list, "cookies");
        this.url = str;
        this.loadTime = j;
        this.response = i;
        this.cookies = list;
    }

    /* JADX WARNING: Incorrect type for immutable var: ssa=java.util.List, code=java.util.List<java.lang.String>, for r8v0, types: [java.util.List] */
    @NotNull
    public static /* synthetic */ RedirectsResult copy$default(RedirectsResult redirectsResult, String str, long j, int i, List<String> list, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            str = redirectsResult.url;
        }
        if ((i2 & 2) != 0) {
            j = redirectsResult.loadTime;
        }
        long j2 = j;
        if ((i2 & 4) != 0) {
            i = redirectsResult.response;
        }
        int i3 = i;
        if ((i2 & 8) != 0) {
            list = redirectsResult.cookies;
        }
        return redirectsResult.copy(str, j2, i3, list);
    }

    @NotNull
    public final String component1() {
        return this.url;
    }

    public final long component2() {
        return this.loadTime;
    }

    public final int component3() {
        return this.response;
    }

    @NotNull
    public final List<String> component4() {
        return this.cookies;
    }

    @NotNull
    public final RedirectsResult copy(@NotNull String str, long j, int i, @NotNull List<String> list) {
        g.b(str, "url");
        g.b(list, "cookies");
        RedirectsResult redirectsResult = new RedirectsResult(str, j, i, list);
        return redirectsResult;
    }

    public final boolean equals(@Nullable Object obj) {
        if (this != obj) {
            if (obj instanceof RedirectsResult) {
                RedirectsResult redirectsResult = (RedirectsResult) obj;
                if (g.a((Object) this.url, (Object) redirectsResult.url)) {
                    if (this.loadTime == redirectsResult.loadTime) {
                        if (!(this.response == redirectsResult.response) || !g.a((Object) this.cookies, (Object) redirectsResult.cookies)) {
                            return false;
                        }
                    }
                }
            }
            return false;
        }
        return true;
    }

    @NotNull
    public final List<String> getCookies() {
        return this.cookies;
    }

    public final long getLoadTime() {
        return this.loadTime;
    }

    public final int getResponse() {
        return this.response;
    }

    @NotNull
    public final String getUrl() {
        return this.url;
    }

    public final int hashCode() {
        String str = this.url;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        long j = this.loadTime;
        int i2 = (((hashCode + ((int) (j ^ (j >>> 32)))) * 31) + this.response) * 31;
        List<String> list = this.cookies;
        if (list != null) {
            i = list.hashCode();
        }
        return i2 + i;
    }

    @NotNull
    public final String toString() {
        StringBuilder sb = new StringBuilder("RedirectsResult(url=");
        sb.append(this.url);
        sb.append(", loadTime=");
        sb.append(this.loadTime);
        sb.append(", response=");
        sb.append(this.response);
        sb.append(", cookies=");
        sb.append(this.cookies);
        sb.append(")");
        return sb.toString();
    }
}
