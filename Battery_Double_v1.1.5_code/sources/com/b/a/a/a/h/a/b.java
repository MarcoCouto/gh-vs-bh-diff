package com.b.a.a.a.h.a;

import android.os.AsyncTask;
import java.util.concurrent.ThreadPoolExecutor;
import org.json.JSONObject;

public abstract class b extends AsyncTask<Object, Void, String> {

    /* renamed from: a reason: collision with root package name */
    private a f1802a;
    protected final C0018b d;

    public interface a {
        void a();
    }

    /* renamed from: com.b.a.a.a.h.a.b$b reason: collision with other inner class name */
    public interface C0018b {
        JSONObject a();

        void a(JSONObject jSONObject);
    }

    public b(C0018b bVar) {
        this.d = bVar;
    }

    public final void a(a aVar) {
        this.f1802a = aVar;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onPostExecute(String str) {
        if (this.f1802a != null) {
            this.f1802a.a();
        }
    }

    public final void a(ThreadPoolExecutor threadPoolExecutor) {
        executeOnExecutor(threadPoolExecutor, new Object[0]);
    }
}
