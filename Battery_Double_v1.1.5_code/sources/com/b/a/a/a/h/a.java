package com.b.a.a.a.h;

import android.os.Handler;
import android.os.Looper;
import android.view.View;
import com.b.a.a.a.d.a.C0016a;
import com.b.a.a.a.e.b;
import com.b.a.a.a.h.a.c;
import com.startapp.android.publish.adsCommon.g.f;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.json.JSONObject;

public final class a implements C0016a {

    /* renamed from: a reason: collision with root package name */
    private static a f1799a = new a();
    private static Handler b = new Handler(Looper.getMainLooper());
    /* access modifiers changed from: private */
    public static Handler c = null;
    /* access modifiers changed from: private */
    public static final Runnable j = new Runnable() {
        public final void run() {
            a.b(a.a());
        }
    };
    /* access modifiers changed from: private */
    public static final Runnable k = new Runnable() {
        public final void run() {
            if (a.c != null) {
                a.c.post(a.j);
                a.c.postDelayed(a.k, 200);
            }
        }
    };
    private List<Object> d = new ArrayList();
    private int e;
    private f f = new f();
    private com.startapp.android.publish.adsCommon.j.a g = new com.startapp.android.publish.adsCommon.j.a();
    /* access modifiers changed from: private */
    public b h = new b(new c());
    private double i;

    a() {
    }

    public static a a() {
        return f1799a;
    }

    private void a(View view, com.b.a.a.a.d.a aVar, JSONObject jSONObject, int i2) {
        aVar.a(view, jSONObject, this, i2 == c.f1805a);
    }

    public static void d() {
        h();
    }

    private static void h() {
        if (c != null) {
            c.removeCallbacks(k);
            c = null;
        }
    }

    public static void b() {
        if (c == null) {
            Handler handler = new Handler(Looper.getMainLooper());
            c = handler;
            handler.post(j);
            c.postDelayed(k, 200);
        }
    }

    public final void c() {
        h();
        this.d.clear();
        b.post(new Runnable() {
            public final void run() {
                a.this.h.b();
            }
        });
    }

    public final void a(View view, com.b.a.a.a.d.a aVar, JSONObject jSONObject) {
        boolean z;
        if (com.b.a.a.a.e.c.c(view)) {
            int c2 = this.g.c(view);
            if (c2 != c.c) {
                JSONObject a2 = aVar.a(view);
                b.a(jSONObject, a2);
                String a3 = this.g.a(view);
                if (a3 != null) {
                    b.a(a2, a3);
                    this.g.f();
                    z = true;
                } else {
                    z = false;
                }
                if (!z) {
                    ArrayList b2 = this.g.b(view);
                    if (b2 != null) {
                        b.a(a2, (List<String>) b2);
                    }
                    a(view, aVar, a2, c2);
                }
                this.e++;
            }
        }
    }

    static /* synthetic */ void b(a aVar) {
        aVar.e = 0;
        aVar.i = com.b.a.a.a.b.b();
        aVar.g.d();
        double b2 = com.b.a.a.a.b.b();
        com.b.a.a.a.d.a a2 = aVar.f.a();
        if (aVar.g.c().size() > 0) {
            aVar.h.b(a2.a(null), aVar.g.c(), b2);
        }
        if (aVar.g.b().size() > 0) {
            JSONObject a3 = a2.a(null);
            aVar.a(null, a2, a3, c.f1805a);
            b.a(a3);
            aVar.h.a(a3, aVar.g.b(), b2);
        } else {
            aVar.h.b();
        }
        aVar.g.e();
        com.b.a.a.a.b.b();
        if (aVar.d.size() > 0) {
            Iterator it = aVar.d.iterator();
            while (it.hasNext()) {
                it.next();
            }
        }
    }
}
