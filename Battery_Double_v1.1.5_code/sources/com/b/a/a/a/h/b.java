package com.b.a.a.a.h;

import android.support.annotation.VisibleForTesting;
import com.b.a.a.a.h.a.b.C0018b;
import com.b.a.a.a.h.a.c;
import com.b.a.a.a.h.a.d;
import com.b.a.a.a.h.a.e;
import com.b.a.a.a.h.a.f;
import java.util.HashSet;
import org.json.JSONObject;

public final class b implements C0018b {

    /* renamed from: a reason: collision with root package name */
    private JSONObject f1804a;
    private final c b;

    public b(c cVar) {
        this.b = cVar;
    }

    @VisibleForTesting
    public final JSONObject a() {
        return this.f1804a;
    }

    @VisibleForTesting
    public final void a(JSONObject jSONObject) {
        this.f1804a = jSONObject;
    }

    public final void a(JSONObject jSONObject, HashSet<String> hashSet, double d) {
        c cVar = this.b;
        f fVar = new f(this, hashSet, jSONObject, d);
        cVar.a(fVar);
    }

    public final void b() {
        this.b.a(new d(this));
    }

    public final void b(JSONObject jSONObject, HashSet<String> hashSet, double d) {
        c cVar = this.b;
        e eVar = new e(this, hashSet, jSONObject, d);
        cVar.a(eVar);
    }
}
