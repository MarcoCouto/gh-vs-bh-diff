package com.b.a.a.a.h.a;

import com.b.a.a.a.b.b;
import com.b.a.a.a.c.a;
import com.b.a.a.a.h.a.b.C0018b;
import java.util.HashSet;
import org.json.JSONObject;

public final class e extends a {
    public e(C0018b bVar, HashSet<String> hashSet, JSONObject jSONObject, double d) {
        super(bVar, hashSet, jSONObject, d);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public final void onPostExecute(String str) {
        a a2 = a.a();
        if (a2 != null) {
            for (b bVar : a2.b()) {
                if (this.f1801a.contains(bVar.f())) {
                    bVar.e().b(str, this.c);
                }
            }
        }
        super.onPostExecute(str);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object doInBackground(Object[] objArr) {
        return this.b.toString();
    }
}
