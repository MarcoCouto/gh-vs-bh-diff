package com.b.a.a.a.h.a;

import com.b.a.a.a.h.a.b.a;
import java.util.ArrayDeque;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public final class c implements a {

    /* renamed from: a reason: collision with root package name */
    private final BlockingQueue<Runnable> f1803a = new LinkedBlockingQueue();
    private final ThreadPoolExecutor b;
    private final ArrayDeque<b> c = new ArrayDeque<>();
    private b d = null;

    public c() {
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(1, 1, 1, TimeUnit.SECONDS, this.f1803a);
        this.b = threadPoolExecutor;
    }

    private void b() {
        this.d = (b) this.c.poll();
        if (this.d != null) {
            this.d.a(this.b);
        }
    }

    public final void a() {
        this.d = null;
        b();
    }

    public final void a(b bVar) {
        bVar.a((a) this);
        this.c.add(bVar);
        if (this.d == null) {
            b();
        }
    }
}
