package com.b.a.a.a.h.a;

import com.b.a.a.a.h.a.b.C0018b;
import java.util.HashSet;
import org.json.JSONObject;

public abstract class a extends b {

    /* renamed from: a reason: collision with root package name */
    protected final HashSet<String> f1801a;
    protected final JSONObject b;
    protected final double c;

    public a(C0018b bVar, HashSet<String> hashSet, JSONObject jSONObject, double d) {
        super(bVar);
        this.f1801a = new HashSet<>(hashSet);
        this.b = jSONObject;
        this.c = d;
    }
}
