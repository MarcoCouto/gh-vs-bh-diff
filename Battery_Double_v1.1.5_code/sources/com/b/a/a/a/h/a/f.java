package com.b.a.a.a.h.a;

import android.text.TextUtils;
import com.b.a.a.a.b.b;
import com.b.a.a.a.c.a;
import com.b.a.a.a.h.a.b.C0018b;
import java.util.HashSet;
import org.json.JSONObject;

public final class f extends a {
    public f(C0018b bVar, HashSet<String> hashSet, JSONObject jSONObject, double d) {
        super(bVar, hashSet, jSONObject, d);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public final void onPostExecute(String str) {
        if (!TextUtils.isEmpty(str)) {
            a a2 = a.a();
            if (a2 != null) {
                for (b bVar : a2.b()) {
                    if (this.f1801a.contains(bVar.f())) {
                        bVar.e().a(str, this.c);
                    }
                }
            }
        }
        super.onPostExecute(str);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object doInBackground(Object[] objArr) {
        if (com.b.a.a.a.e.b.b(this.b, this.d.a())) {
            return null;
        }
        this.d.a(this.b);
        return this.b.toString();
    }
}
