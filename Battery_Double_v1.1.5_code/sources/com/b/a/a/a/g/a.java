package com.b.a.a.a.g;

import android.webkit.WebView;
import com.b.a.a.a.b;
import com.b.a.a.a.c.d;
import com.startapp.android.publish.adsCommon.a.i;
import org.json.JSONObject;

public abstract class a {

    /* renamed from: a reason: collision with root package name */
    private com.b.a.a.a.f.a f1795a = new com.b.a.a.a.f.a((WebView) null);
    private com.b.a.a.a.b.a b;
    private i c;
    private int d;
    private double e;

    /* renamed from: com.b.a.a.a.g.a$a reason: collision with other inner class name */
    enum C0017a {
        ;
        

        /* renamed from: a reason: collision with root package name */
        public static final int f1796a = 1;
        public static final int b = 2;
        public static final int c = 3;

        static {
            int[] iArr = {1, 2, 3};
        }
    }

    public a() {
        f();
    }

    public void a() {
    }

    public final void a(float f) {
        d.a().a(c(), f);
    }

    /* access modifiers changed from: 0000 */
    public final void a(WebView webView) {
        this.f1795a = new com.b.a.a.a.f.a(webView);
    }

    public final void a(com.b.a.a.a.b.a aVar) {
        this.b = aVar;
    }

    public final void a(i iVar) {
        this.c = iVar;
    }

    public final void a(String str) {
        d.a().a(c(), str, (JSONObject) null);
    }

    public final void a(String str, double d2) {
        if (d2 > this.e) {
            this.d = C0017a.b;
            d.a().b(c(), str);
        }
    }

    public final void a(String str, JSONObject jSONObject) {
        d.a().a(c(), str, jSONObject);
    }

    public void b() {
        this.f1795a.clear();
    }

    public final void b(String str, double d2) {
        if (d2 > this.e && this.d != C0017a.c) {
            this.d = C0017a.c;
            d.a().b(c(), str);
        }
    }

    public final WebView c() {
        return (WebView) this.f1795a.get();
    }

    public final com.b.a.a.a.b.a d() {
        return this.b;
    }

    public final i e() {
        return this.c;
    }

    public final void f() {
        this.e = b.b();
        this.d = C0017a.f1796a;
    }

    public final void a(boolean z) {
        if (this.f1795a.get() != null) {
            d.a().c(c(), z ? "foregrounded" : "backgrounded");
        }
    }
}
