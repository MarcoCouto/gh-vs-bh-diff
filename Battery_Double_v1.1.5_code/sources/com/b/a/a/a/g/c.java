package com.b.a.a.a.g;

import android.os.Handler;
import android.webkit.WebView;
import com.b.a.a.a.c.d;
import java.util.List;

public final class c extends a {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public WebView f1797a;
    private List<com.startapp.android.publish.adsCommon.c> b;
    private final String c;

    public c(List<com.startapp.android.publish.adsCommon.c> list, String str) {
        this.b = list;
        this.c = str;
    }

    public final void b() {
        super.b();
        new Handler().postDelayed(new Runnable() {

            /* renamed from: a reason: collision with root package name */
            private WebView f1798a = c.this.f1797a;

            public final void run() {
                this.f1798a.destroy();
            }
        }, 2000);
        this.f1797a = null;
    }

    public final void a() {
        super.a();
        this.f1797a = new WebView(com.b.a.a.a.c.c.a().b());
        this.f1797a.getSettings().setJavaScriptEnabled(true);
        a(this.f1797a);
        d.a();
        d.a(this.f1797a, this.c);
        for (com.startapp.android.publish.adsCommon.c d : this.b) {
            String externalForm = d.d().toExternalForm();
            d.a();
            WebView webView = this.f1797a;
            if (externalForm != null) {
                d.a(webView, "var script=document.createElement('script');script.setAttribute(\"type\",\"text/javascript\");script.setAttribute(\"src\",\"%SCRIPT_SRC%\");document.body.appendChild(script);".replace("%SCRIPT_SRC%", externalForm));
            }
        }
    }
}
