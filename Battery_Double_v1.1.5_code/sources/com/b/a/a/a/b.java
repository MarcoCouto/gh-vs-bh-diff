package com.b.a.a.a;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;
import android.webkit.WebView;
import com.b.a.a.a.c.c;
import com.b.a.a.a.c.e;
import com.integralads.avid.library.inmobi.utils.AvidJSONUtil;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.startapp.android.publish.GeneratedConstants;
import com.startapp.android.publish.adsCommon.a.i;
import com.startapp.android.publish.adsCommon.a.j;
import com.startapp.android.publish.adsCommon.c.a;
import com.startapp.android.publish.adsCommon.g.d;
import com.startapp.android.publish.adsCommon.g.f;
import com.startapp.android.publish.adsCommon.l;
import com.startapp.android.publish.common.model.AdDetails;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public final class b {

    /* renamed from: a reason: collision with root package name */
    private boolean f1777a;

    public static String a(String str, String str2) {
        return c.a(str, str2);
    }

    private static int a(String str) {
        b(str);
        return Integer.parseInt(str.split("\\.", 2)[0]);
    }

    private static void b(String str) {
        a((Object) str, "Version cannot be null");
        if (!str.matches("[0-9]+\\.[0-9]+\\.[0-9]+.*")) {
            throw new IllegalArgumentException("Invalid version format : ".concat(String.valueOf(str)));
        }
    }

    /* access modifiers changed from: 0000 */
    public final boolean a() {
        return this.f1777a;
    }

    /* access modifiers changed from: 0000 */
    public final boolean a(String str, Context context) {
        b(str);
        a((Object) context, "Application Context cannot be null");
        if (!(a("1.2.0-Startapp") == a(str))) {
            return false;
        }
        if (!this.f1777a) {
            this.f1777a = true;
            e.a().a(context);
            com.b.a.a.a.c.b.a().a(context);
            com.b.a.a.a.e.b.a(context);
            c.a().a(context);
        }
        return true;
    }

    public static void a(String str, Exception exc) {
        Log.e("OMIDLIB", str, exc);
    }

    public static double b() {
        return (double) TimeUnit.NANOSECONDS.toMillis(System.nanoTime());
    }

    public static void a(Object obj, String str) {
        if (obj == null) {
            throw new IllegalArgumentException(str);
        }
    }

    public static void b(String str, String str2) {
        if (TextUtils.isEmpty(str)) {
            throw new IllegalArgumentException(str2);
        }
    }

    public static void c(String str, String str2) {
        if (str.length() > 256) {
            throw new IllegalArgumentException(str2);
        }
    }

    public static void a(com.b.a.a.a.b.b bVar) {
        if (bVar.j()) {
            throw new IllegalStateException("AdSession is finished");
        }
    }

    public static void b(com.b.a.a.a.b.b bVar) {
        if (bVar.i()) {
            a(bVar);
            return;
        }
        throw new IllegalStateException("AdSession is not started");
    }

    public static List<AdDetails> a(Context context, List<AdDetails> list, int i, Set<String> set) {
        return a(context, list, i, set, true);
    }

    public static List<AdDetails> a(Context context, List<AdDetails> list, int i, Set<String> set, boolean z) {
        Context context2 = context;
        int i2 = i;
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        ArrayList arrayList3 = new ArrayList();
        ArrayList arrayList4 = new ArrayList();
        boolean z2 = false;
        for (AdDetails adDetails : list) {
            a aVar = new a(adDetails.getTrackingUrl(), adDetails.getAppPresencePackage(), i2, adDetails.getMinAppVersion());
            boolean z3 = adDetails.getAppPresencePackage() != null && adDetails.getAppPresencePackage().startsWith("!");
            boolean a2 = com.startapp.common.a.c.a(context, z3 ? adDetails.getAppPresencePackage().substring(1) : adDetails.getAppPresencePackage(), adDetails.getMinAppVersion());
            boolean z4 = com.startapp.android.publish.adsCommon.b.a().E() && ((a2 && !z3) || (!a2 && z3));
            arrayList3.add(aVar);
            if (z4) {
                aVar.b(a2);
                aVar.a(false);
                if (!z3) {
                    arrayList2.add(adDetails);
                    arrayList4.add(aVar);
                }
                set.add(adDetails.getPackageName());
                StringBuilder sb = new StringBuilder("App Presence:[");
                sb.append(adDetails.getPackageName());
                sb.append(RequestParameters.RIGHT_BRACKETS);
                z2 = true;
            } else {
                Set<String> set2 = set;
                arrayList.add(adDetails);
            }
            StringBuilder sb2 = new StringBuilder("App Not Presence:[");
            sb2.append(adDetails.getPackageName());
            sb2.append(RequestParameters.RIGHT_BRACKETS);
        }
        if (arrayList.size() < 5 && (list.size() != 1 || i2 > 0)) {
            int min = Math.min(5 - arrayList.size(), arrayList2.size());
            arrayList.addAll(arrayList2.subList(0, min));
            for (a a3 : arrayList4.subList(0, min)) {
                a3.a(true);
            }
        }
        if (z2) {
            l.c(context);
            if (z) {
                new com.startapp.android.publish.adsCommon.c.b(context, arrayList3).a();
            }
        }
        return arrayList;
    }

    public static List<a> a(String str, int i) {
        ArrayList arrayList = new ArrayList();
        String[] strArr = new String[0];
        String a2 = j.a(str, "@tracking@", "@tracking@");
        if (a2 != null) {
            strArr = a2.split(",");
        }
        String[] strArr2 = new String[0];
        String a3 = j.a(str, "@appPresencePackage@", "@appPresencePackage@");
        if (a3 != null) {
            strArr2 = a3.split(",");
        }
        String[] strArr3 = new String[0];
        String a4 = j.a(str, "@minAppVersion@", "@minAppVersion@");
        if (a4 != null) {
            strArr3 = a4.split(",");
        }
        int i2 = 0;
        while (i2 < strArr2.length) {
            arrayList.add(new a(strArr.length > i2 ? strArr[i2] : null, strArr2[i2], i, strArr3.length > i2 ? Integer.valueOf(strArr3[i2]).intValue() : 0));
            i2++;
        }
        while (i2 < strArr.length) {
            arrayList.add(new a(strArr[i2], "", i, strArr3.length > i2 ? Integer.valueOf(strArr3[i2]).intValue() : 0));
            i2++;
        }
        return arrayList;
    }

    public static Boolean a(Context context, List<a> list, int i, Set<String> set, List<a> list2) {
        boolean z = false;
        for (a aVar : list) {
            boolean startsWith = aVar.b().startsWith("!");
            boolean z2 = true;
            boolean a2 = com.startapp.common.a.c.a(context, startsWith ? aVar.b().substring(1) : aVar.b(), aVar.e());
            if ((!startsWith && a2) || (startsWith && !a2)) {
                aVar.b(a2);
                if (i != 0) {
                    z2 = false;
                }
                if (z2 && !startsWith) {
                    set.add(aVar.b());
                } else if (!z2 && aVar.a() != null) {
                    StringBuilder sb = new StringBuilder();
                    sb.append(aVar.a());
                    sb.append("&isShown=");
                    sb.append(aVar.c());
                    sb.append("&appPresence=");
                    sb.append(aVar.d());
                    aVar.a(sb.toString());
                }
                z = z2;
            }
            list2.add(aVar);
        }
        if (z) {
            for (int i2 = 0; i2 < list2.size(); i2++) {
                ((a) list2.get(i2)).a(false);
            }
        }
        return Boolean.valueOf(z);
    }

    public static void a(String str, WebView webView) {
        j.a(webView, "mraid.setPlacementType", str);
    }

    public static void a(com.startapp.android.publish.adsCommon.h.a.c cVar, WebView webView) {
        new StringBuilder("fireStateChangeEvent: ").append(cVar);
        j.a(webView, "mraid.fireStateChangeEvent", cVar.toString());
    }

    public static void a(Context context, int i, int i2, WebView webView) {
        StringBuilder sb = new StringBuilder("setScreenSize ");
        sb.append(i);
        sb.append(AvidJSONUtil.KEY_X);
        sb.append(i2);
        j.a(webView, "mraid.setScreenSize", Integer.valueOf(i.b(context, i)), Integer.valueOf(i.b(context, i2)));
    }

    public static void b(Context context, int i, int i2, WebView webView) {
        StringBuilder sb = new StringBuilder("setMaxSize ");
        sb.append(i);
        sb.append(AvidJSONUtil.KEY_X);
        sb.append(i2);
        j.a(webView, "mraid.setMaxSize", Integer.valueOf(i.b(context, i)), Integer.valueOf(i.b(context, i2)));
    }

    public static void a(Context context, int i, int i2, int i3, int i4, WebView webView) {
        StringBuilder sb = new StringBuilder("setCurrentPosition [");
        sb.append(i);
        sb.append(",");
        sb.append(i2);
        sb.append("] (");
        sb.append(i3);
        sb.append(AvidJSONUtil.KEY_X);
        sb.append(i4);
        sb.append(")");
        j.a(webView, "mraid.setCurrentPosition", Integer.valueOf(i.b(context, i)), Integer.valueOf(i.b(context, i2)), Integer.valueOf(i.b(context, i3)), Integer.valueOf(i.b(context, i4)));
    }

    public static void b(Context context, int i, int i2, int i3, int i4, WebView webView) {
        StringBuilder sb = new StringBuilder("setDefaultPosition [");
        sb.append(i);
        sb.append(",");
        sb.append(i2);
        sb.append("] (");
        sb.append(i3);
        sb.append(AvidJSONUtil.KEY_X);
        sb.append(i4);
        sb.append(")");
        j.a(webView, "mraid.setDefaultPosition", Integer.valueOf(i.b(context, i)), Integer.valueOf(i.b(context, i2)), Integer.valueOf(i.b(context, i3)), Integer.valueOf(i.b(context, i4)));
    }

    public static void a(WebView webView) {
        j.a(webView, "mraid.fireReadyEvent", new Object[0]);
    }

    public static void a(WebView webView, boolean z) {
        j.a(webView, "mraid.fireViewableChangeEvent", Boolean.valueOf(z));
    }

    public static void a(WebView webView, String str, String str2) {
        StringBuilder sb = new StringBuilder("fireErrorEvent message: ");
        sb.append(str);
        sb.append(", action:");
        sb.append(str2);
        j.a(webView, "mraid.fireErrorEvent", str, str2);
    }

    private static void a(WebView webView, String str, boolean z) {
        StringBuilder sb = new StringBuilder("setSupports feature: ");
        sb.append(str);
        sb.append(", isSupported:");
        sb.append(z);
        j.a(webView, false, "mraid.setSupports", str, Boolean.valueOf(z));
    }

    public static void a(Context context, WebView webView, com.startapp.android.publish.adsCommon.h.b.a aVar) {
        if (aVar == null) {
            aVar = new com.startapp.android.publish.adsCommon.h.b.a(context);
        }
        a(webView, "mraid.SUPPORTED_FEATURES.CALENDAR", aVar.a());
        a(webView, "mraid.SUPPORTED_FEATURES.INLINEVIDEO", aVar.b());
        a(webView, "mraid.SUPPORTED_FEATURES.SMS", aVar.c());
        a(webView, "mraid.SUPPORTED_FEATURES.STOREPICTURE", aVar.d());
        a(webView, "mraid.SUPPORTED_FEATURES.TEL", aVar.e());
    }

    public static com.b.a.a.a.b.b b(WebView webView) {
        if (!a(webView.getContext())) {
            return null;
        }
        return a(com.startapp.android.publish.ads.splash.i.a(c(), webView, ""), false);
    }

    public static com.b.a.a.a.b.b a(Context context, com.startapp.android.publish.b.a aVar) {
        if (!a(context)) {
            return null;
        }
        if (aVar == null) {
            f.a(context, d.EXCEPTION, "OMSDK: Verification details can't be null!", "", "");
            return null;
        }
        String a2 = com.startapp.android.publish.b.b.a();
        List<com.startapp.android.publish.b.c> adVerification = aVar.getAdVerification();
        ArrayList arrayList = new ArrayList(adVerification.size());
        for (com.startapp.android.publish.b.c cVar : adVerification) {
            URL a3 = a(context, cVar.getVerificationScriptUrl());
            if (a3 != null) {
                arrayList.add(com.startapp.android.publish.adsCommon.c.a(cVar.getVendorKey(), a3, cVar.getVerificationParameters()));
            }
        }
        return a(com.startapp.android.publish.ads.splash.i.a(c(), a2, arrayList, ""), true);
    }

    private static com.b.a.a.a.b.b a(com.startapp.android.publish.ads.splash.i iVar, boolean z) {
        return com.b.a.a.a.b.b.a(com.startapp.android.publish.ads.banner.bannerstandard.c.a(com.b.a.a.a.b.d.NATIVE, z ? com.b.a.a.a.b.d.NATIVE : com.b.a.a.a.b.d.NONE), iVar);
    }

    @NonNull
    private static com.startapp.android.publish.ads.video.j c() {
        return com.startapp.android.publish.ads.video.j.a("StartApp", GeneratedConstants.INAPP_VERSION);
    }

    private static URL a(Context context, String str) {
        try {
            return new URL(str);
        } catch (MalformedURLException e) {
            f.a(context, d.EXCEPTION, "OMSDK: can't create URL - ".concat(String.valueOf(str)), e.getMessage(), "");
            return null;
        }
    }

    private static boolean a(Context context) {
        try {
            if (a.b() || a.a(a.a(), context)) {
                return true;
            }
            f.a(context, d.EXCEPTION, "OMSDK: Failed to activate sdk.", "", "");
            return false;
        } catch (Exception e) {
            f.a(context, d.EXCEPTION, "OMSDK: Failed to activate sdk.", e.getMessage(), "");
            return false;
        }
    }

    public static String a(List<String> list) {
        return new com.startapp.a.a.b.a().a(list);
    }

    public static String a(Field field) {
        Annotation[] declaredAnnotations = field.getDeclaredAnnotations();
        if (declaredAnnotations != null && declaredAnnotations.length > 0) {
            Annotation annotation = field.getDeclaredAnnotations()[0];
            if (annotation.annotationType().equals(com.startapp.common.c.e.class)) {
                com.startapp.common.c.e eVar = (com.startapp.common.c.e) annotation;
                if (!"".equals(eVar.f())) {
                    return eVar.f();
                }
            }
        }
        return field.getName();
    }

    public static boolean a(Object obj) {
        Class cls = obj.getClass();
        return cls.equals(Boolean.class) || cls.equals(Integer.class) || cls.equals(Character.class) || cls.equals(Byte.class) || cls.equals(Short.class) || cls.equals(Double.class) || cls.equals(Long.class) || cls.equals(Float.class) || cls.equals(String.class);
    }
}
