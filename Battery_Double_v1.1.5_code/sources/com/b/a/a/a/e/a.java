package com.b.a.a.a.e;

import android.content.Context;
import android.os.Build;
import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;

public final class a {

    /* renamed from: a reason: collision with root package name */
    private static com.startapp.android.b.a f1792a;

    public static float a(int i, int i2) {
        if (i2 <= 0 || i <= 0) {
            return 0.0f;
        }
        float f = ((float) i) / ((float) i2);
        if (f > 1.0f) {
            f = 1.0f;
        }
        return f;
    }

    /* JADX WARNING: Removed duplicated region for block: B:29:0x0060  */
    public static boolean a(Context context) {
        boolean z;
        boolean z2;
        if (f1792a == null) {
            f1792a = new com.startapp.android.b.a(context.getApplicationContext());
        }
        com.startapp.android.b.a aVar = f1792a;
        if (!aVar.a() && !aVar.b() && !com.startapp.android.b.a.a("su") && !com.startapp.android.b.a.a("busybox") && !com.startapp.android.b.a.c() && !com.startapp.android.b.a.d()) {
            String str = Build.TAGS;
            if (!(str != null && str.contains("test-keys")) && !com.startapp.android.b.a.e() && !com.startapp.android.b.a.a("magisk")) {
                z = false;
                if (!z) {
                    String str2 = Build.TAGS;
                    if (!(str2 != null && str2.contains("test-keys")) && !a() && !a() && !b() && !c()) {
                        String[] strArr = {"com.noshufou.android.su", "com.thirdparty.superuser", "eu.chainfire.supersu", "com.koushikdutta.superuser", "com.zachspong.temprootremovejb", "com.ramdroid.appquarantine"};
                        int i = 0;
                        while (true) {
                            if (i >= 6) {
                                z2 = false;
                                break;
                            } else if (a(context, strArr[i])) {
                                z2 = true;
                                break;
                            } else {
                                i++;
                            }
                        }
                        return z2;
                    }
                }
            }
        }
        z = true;
        if (!z) {
        }
    }

    private static boolean a() {
        String[] strArr = {"/system/app/Superuser.apk", "/sbin/su", "/system/bin/su", "/system/xbin/su", "/data/local/xbin/su", "/data/local/bin/su", "/system/sd/xbin/su", "/system/bin/failsafe/su", "/data/local/su", "/su/bin/su"};
        for (int i = 0; i < 10; i++) {
            if (new File(strArr[i]).exists()) {
                return true;
            }
        }
        return false;
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x003f  */
    private static boolean b() {
        boolean z = false;
        Process process = null;
        try {
            Process exec = Runtime.getRuntime().exec(new String[]{"/system/xbin/which", "su"});
            try {
                if (new BufferedReader(new InputStreamReader(exec.getInputStream())).readLine() != null) {
                    z = true;
                }
                if (exec != null) {
                    exec.destroy();
                }
                return z;
            } catch (Throwable th) {
                th = th;
                process = exec;
                if (process != null) {
                    process.destroy();
                }
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            if (process != null) {
            }
            throw th;
        }
    }

    private static boolean c() {
        try {
            return new File("/system/app/Superuser.apk").exists();
        } catch (Throwable unused) {
            return false;
        }
    }

    private static boolean a(Context context, String str) {
        try {
            context.getPackageManager().getPackageInfo(str, 1);
            return true;
        } catch (Throwable unused) {
            return false;
        }
    }
}
