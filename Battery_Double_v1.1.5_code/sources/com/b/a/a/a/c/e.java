package com.b.a.a.a.c;

import android.content.Context;
import android.os.Handler;
import com.b.a.a.a.a.a;
import com.b.a.a.a.a.b;
import com.b.a.a.a.e.c;
import com.startapp.android.publish.ads.banner.banner3d.Banner3DSize;

public final class e implements a, b.a {

    /* renamed from: a reason: collision with root package name */
    private static e f1789a;
    private float b = 0.0f;
    private final Banner3DSize c;
    private final c d;
    private b e;
    private a f;

    private e(Banner3DSize banner3DSize, c cVar) {
        this.c = banner3DSize;
        this.d = cVar;
    }

    public static e a() {
        if (f1789a == null) {
            f1789a = new e(new Banner3DSize(), new c());
        }
        return f1789a;
    }

    public final void a(boolean z) {
        if (z) {
            com.b.a.a.a.h.a.a();
            com.b.a.a.a.h.a.b();
            return;
        }
        com.b.a.a.a.h.a.a();
        com.b.a.a.a.h.a.d();
    }

    public final void b() {
        b.a().a((b.a) this);
        b.a().b();
        if (b.a().d()) {
            com.b.a.a.a.h.a.a();
            com.b.a.a.a.h.a.b();
        }
        this.e.a();
    }

    public final void c() {
        com.b.a.a.a.h.a.a().c();
        b.a().c();
        this.e.b();
    }

    public final float d() {
        return this.b;
    }

    public final void a(Context context) {
        this.e = new b(new Handler(), context, new com.b.a.a.a.e.a(), this);
    }

    public final void a(float f2) {
        this.b = f2;
        if (this.f == null) {
            this.f = a.a();
        }
        for (com.b.a.a.a.b.b e2 : this.f.c()) {
            e2.e().a(f2);
        }
    }
}
