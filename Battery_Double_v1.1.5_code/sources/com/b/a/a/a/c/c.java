package com.b.a.a.a.c;

import android.annotation.SuppressLint;
import android.content.Context;

public final class c {
    @SuppressLint({"StaticFieldLeak"})

    /* renamed from: a reason: collision with root package name */
    private static c f1786a = new c();
    private Context b;

    private c() {
    }

    public static c a() {
        return f1786a;
    }

    public final void a(Context context) {
        this.b = context != null ? context.getApplicationContext() : null;
    }

    public final Context b() {
        return this.b;
    }
}
