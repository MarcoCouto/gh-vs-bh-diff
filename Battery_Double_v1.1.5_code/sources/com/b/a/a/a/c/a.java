package com.b.a.a.a.c;

import com.b.a.a.a.b.b;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public final class a {

    /* renamed from: a reason: collision with root package name */
    private static a f1783a = new a();
    private final ArrayList<b> b = new ArrayList<>();
    private final ArrayList<b> c = new ArrayList<>();

    private a() {
    }

    public static a a() {
        return f1783a;
    }

    private boolean d() {
        return this.c.size() > 0;
    }

    public final void a(b bVar) {
        this.b.add(bVar);
    }

    public final Collection<b> b() {
        return Collections.unmodifiableCollection(this.b);
    }

    public final void b(b bVar) {
        boolean d = d();
        this.c.add(bVar);
        if (!d) {
            e.a().b();
        }
    }

    public final Collection<b> c() {
        return Collections.unmodifiableCollection(this.c);
    }

    public final void c(b bVar) {
        boolean d = d();
        this.b.remove(bVar);
        this.c.remove(bVar);
        if (d && !d()) {
            e.a().c();
        }
    }
}
