package com.b.a.a.a.c;

import android.annotation.SuppressLint;
import android.app.KeyguardManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.annotation.NonNull;

public final class b {
    @SuppressLint({"StaticFieldLeak"})

    /* renamed from: a reason: collision with root package name */
    private static b f1784a = new b();
    private Context b;
    private BroadcastReceiver c;
    private boolean d;
    private boolean e;
    private a f;

    public interface a {
        void a(boolean z);
    }

    private b() {
    }

    public static b a() {
        return f1784a;
    }

    private void e() {
        boolean z = !this.e;
        for (com.b.a.a.a.b.b e2 : a.a().b()) {
            e2.e().a(z);
        }
    }

    public final void a(@NonNull Context context) {
        this.b = context.getApplicationContext();
    }

    public final void a(a aVar) {
        this.f = aVar;
    }

    public final boolean d() {
        return !this.e;
    }

    public final void b() {
        this.c = new BroadcastReceiver() {
            public final void onReceive(Context context, Intent intent) {
                if (intent != null) {
                    if ("android.intent.action.SCREEN_OFF".equals(intent.getAction())) {
                        b.a(b.this, true);
                    } else if ("android.intent.action.USER_PRESENT".equals(intent.getAction())) {
                        b.a(b.this, false);
                    } else {
                        if ("android.intent.action.SCREEN_ON".equals(intent.getAction())) {
                            KeyguardManager keyguardManager = (KeyguardManager) context.getSystemService("keyguard");
                            if (keyguardManager != null && !keyguardManager.inKeyguardRestrictedInputMode()) {
                                b.a(b.this, false);
                            }
                        }
                    }
                }
            }
        };
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.SCREEN_OFF");
        intentFilter.addAction("android.intent.action.SCREEN_ON");
        intentFilter.addAction("android.intent.action.USER_PRESENT");
        this.b.registerReceiver(this.c, intentFilter);
        this.d = true;
        e();
    }

    public final void c() {
        if (!(this.b == null || this.c == null)) {
            this.b.unregisterReceiver(this.c);
            this.c = null;
        }
        this.d = false;
        this.e = false;
        this.f = null;
    }

    static /* synthetic */ void a(b bVar, boolean z) {
        if (bVar.e != z) {
            bVar.e = z;
            if (bVar.d) {
                bVar.e();
                if (bVar.f != null) {
                    bVar.f.a(bVar.d());
                }
            }
        }
    }
}
