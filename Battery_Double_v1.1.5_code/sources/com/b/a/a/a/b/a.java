package com.b.a.a.a.b;

import com.b.a.a.a.b;

public final class a {

    /* renamed from: a reason: collision with root package name */
    private final b f1778a;

    private a(b bVar) {
        this.f1778a = bVar;
    }

    public static a a(b bVar) {
        b bVar2 = bVar;
        b.a((Object) bVar, "AdSession is null");
        if (bVar2.e().d() == null) {
            b.a(bVar2);
            a aVar = new a(bVar2);
            bVar2.e().a(aVar);
            return aVar;
        }
        throw new IllegalStateException("AdEvents already exists for AdSession");
    }

    public final void a() {
        b.a(this.f1778a);
        if (this.f1778a.k()) {
            if (!this.f1778a.h()) {
                try {
                    this.f1778a.a();
                } catch (Exception unused) {
                }
            }
            if (this.f1778a.h()) {
                this.f1778a.d();
                return;
            }
            return;
        }
        throw new IllegalStateException("Impression event is not expected from the Native AdSession");
    }
}
