package com.b.a.a.a.b;

import com.tapjoy.TJAdUnitConstants.String;

public enum c {
    HTML(String.HTML),
    NATIVE("native");
    
    private final String typeString;

    private c(String str) {
        this.typeString = str;
    }

    public final String toString() {
        return this.typeString;
    }
}
