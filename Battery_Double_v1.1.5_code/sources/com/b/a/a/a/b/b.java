package com.b.a.a.a.b;

import android.os.Build;
import android.os.Build.VERSION;
import android.view.View;
import com.b.a.a.a.c.d;
import com.b.a.a.a.c.e;
import com.b.a.a.a.f.a;
import com.ironsource.sdk.constants.Constants;
import com.startapp.android.publish.ads.banner.bannerstandard.c;
import com.startapp.android.publish.ads.splash.i;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;
import org.json.JSONArray;
import org.json.JSONObject;

public final class b {

    /* renamed from: a reason: collision with root package name */
    private final i f1779a;
    private final c b;
    private final List<a> c;
    private a d;
    private com.b.a.a.a.g.a e;
    private boolean f;
    private boolean g;
    private String h;
    private boolean i;

    public b() {
    }

    public final void a() {
        if (!this.f) {
            this.f = true;
            com.b.a.a.a.c.a.a().b(this);
            this.e.a(e.a().d());
            com.b.a.a.a.g.a aVar = this.e;
            i iVar = this.f1779a;
            String str = this.h;
            JSONObject jSONObject = new JSONObject();
            com.b.a.a.a.e.b.a(jSONObject, "environment", "app");
            com.b.a.a.a.e.b.a(jSONObject, "adSessionType", iVar.f());
            JSONObject jSONObject2 = new JSONObject();
            StringBuilder sb = new StringBuilder();
            sb.append(Build.MANUFACTURER);
            sb.append("; ");
            sb.append(Build.MODEL);
            com.b.a.a.a.e.b.a(jSONObject2, "deviceType", sb.toString());
            com.b.a.a.a.e.b.a(jSONObject2, "osVersion", Integer.toString(VERSION.SDK_INT));
            com.b.a.a.a.e.b.a(jSONObject2, "os", Constants.JAVASCRIPT_INTERFACE_NAME);
            com.b.a.a.a.e.b.a(jSONObject, "deviceInfo", jSONObject2);
            JSONArray jSONArray = new JSONArray();
            jSONArray.put("clid");
            jSONArray.put("vlid");
            com.b.a.a.a.e.b.a(jSONObject, "supports", jSONArray);
            JSONObject jSONObject3 = new JSONObject();
            com.b.a.a.a.e.b.a(jSONObject3, "partnerName", iVar.a().a());
            com.b.a.a.a.e.b.a(jSONObject3, "partnerVersion", iVar.a().b());
            com.b.a.a.a.e.b.a(jSONObject, "omidNativeInfo", jSONObject3);
            JSONObject jSONObject4 = new JSONObject();
            com.b.a.a.a.e.b.a(jSONObject4, "libraryVersion", "1.2.0-Startapp");
            com.b.a.a.a.e.b.a(jSONObject4, "appId", com.b.a.a.a.c.c.a().b().getApplicationContext().getPackageName());
            com.b.a.a.a.e.b.a(jSONObject, "app", jSONObject4);
            if (iVar.d() != null) {
                com.b.a.a.a.e.b.a(jSONObject, "customReferenceData", iVar.d());
            }
            JSONObject jSONObject5 = new JSONObject();
            for (com.startapp.android.publish.adsCommon.c cVar : iVar.b()) {
                com.b.a.a.a.e.b.a(jSONObject5, cVar.c(), cVar.e());
            }
            d.a().a(aVar.c(), str, jSONObject, jSONObject5);
        }
    }

    public final void a(View view) {
        if (!this.g) {
            com.b.a.a.a.b.a((Object) view, "AdView is null");
            if (g() != view) {
                d(view);
                this.e.f();
                Collection<b> b2 = com.b.a.a.a.c.a.a().b();
                if (b2 != null && b2.size() > 0) {
                    for (b bVar : b2) {
                        if (bVar != this && bVar.g() == view) {
                            bVar.d.clear();
                        }
                    }
                }
            }
        }
    }

    public final void b() {
        if (!this.g) {
            this.d.clear();
            if (!this.g) {
                this.c.clear();
            }
            this.g = true;
            d.a().a(this.e.c());
            com.b.a.a.a.c.a.a().c(this);
            this.e.b();
            this.e = null;
        }
    }

    public final void b(View view) {
        if (!this.g) {
            if (view != null) {
                if (c(view) == null) {
                    this.c.add(new a(view));
                }
                return;
            }
            throw new IllegalArgumentException("FriendlyObstruction is null");
        }
    }

    public static b a(c cVar, i iVar) {
        if (com.b.a.a.a.a.b()) {
            com.b.a.a.a.b.a((Object) cVar, "AdSessionConfiguration is null");
            com.b.a.a.a.b.a((Object) iVar, "AdSessionContext is null");
            return new b(cVar, iVar);
        }
        throw new IllegalStateException("Method called before OMID activation");
    }

    private b(c cVar, i iVar) {
        this();
        this.c = new ArrayList();
        this.f = false;
        this.g = false;
        this.b = cVar;
        this.f1779a = iVar;
        this.h = UUID.randomUUID().toString();
        d(null);
        this.e = iVar.f() == c.HTML ? new com.b.a.a.a.g.b(iVar.c()) : new com.b.a.a.a.g.c(iVar.b(), iVar.e());
        this.e.a();
        com.b.a.a.a.c.a.a().a(this);
        d.a().a(this.e.c(), cVar.c());
    }

    private a c(View view) {
        for (a aVar : this.c) {
            if (aVar.get() == view) {
                return aVar;
            }
        }
        return null;
    }

    public final List<a> c() {
        return this.c;
    }

    /* access modifiers changed from: 0000 */
    public final void d() {
        if (!this.i) {
            d.a().b(this.e.c());
            this.i = true;
            return;
        }
        throw new IllegalStateException("Impression event can only be sent once");
    }

    public final com.b.a.a.a.g.a e() {
        return this.e;
    }

    public final String f() {
        return this.h;
    }

    public final View g() {
        return (View) this.d.get();
    }

    private void d(View view) {
        this.d = new a(view);
    }

    public final boolean h() {
        return this.f && !this.g;
    }

    public final boolean i() {
        return this.f;
    }

    public final boolean j() {
        return this.g;
    }

    public final boolean k() {
        return this.b.a();
    }

    public final boolean l() {
        return this.b.b();
    }
}
