package com.b.a.a.a.b;

public enum d {
    NATIVE("native"),
    JAVASCRIPT("javascript"),
    NONE("none");
    
    private final String owner;

    private d(String str) {
        this.owner = str;
    }

    public final String toString() {
        return this.owner;
    }
}
