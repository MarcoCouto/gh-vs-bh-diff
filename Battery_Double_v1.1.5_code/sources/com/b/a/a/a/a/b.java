package com.b.a.a.a.a;

import android.content.Context;
import android.database.ContentObserver;
import android.media.AudioManager;
import android.os.Handler;
import android.provider.Settings.System;
import com.b.a.a.a.e.a;
import com.google.android.exoplayer2.util.MimeTypes;

public final class b extends ContentObserver {

    /* renamed from: a reason: collision with root package name */
    private final Context f1776a;
    private final AudioManager b;
    private final a c;
    private final a d;
    private float e;

    public b(Handler handler, Context context, a aVar, a aVar2) {
        super(handler);
        this.f1776a = context;
        this.b = (AudioManager) context.getSystemService(MimeTypes.BASE_TYPE_AUDIO);
        this.c = aVar;
        this.d = aVar2;
    }

    private float c() {
        return a.a(this.b.getStreamVolume(3), this.b.getStreamMaxVolume(3));
    }

    private void d() {
        this.d.a(this.e);
    }

    public final void a() {
        this.e = c();
        d();
        this.f1776a.getContentResolver().registerContentObserver(System.CONTENT_URI, true, this);
    }

    public final void b() {
        this.f1776a.getContentResolver().unregisterContentObserver(this);
    }

    public final void onChange(boolean z) {
        super.onChange(z);
        float c2 = c();
        if (c2 != this.e) {
            this.e = c2;
            d();
        }
    }
}
