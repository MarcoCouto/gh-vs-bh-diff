package com.github.mikephil.charting.renderer;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.Path.Direction;
import android.graphics.RectF;
import android.os.Build.VERSION;
import android.support.v4.view.ViewCompat;
import android.text.Layout.Alignment;
import android.text.StaticLayout;
import android.text.TextPaint;
import com.github.mikephil.charting.animation.ChartAnimator;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet.ValuePosition;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.IValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.IPieDataSet;
import com.github.mikephil.charting.utils.MPPointF;
import com.github.mikephil.charting.utils.Utils;
import com.github.mikephil.charting.utils.ViewPortHandler;
import java.lang.ref.WeakReference;
import java.util.List;

public class PieChartRenderer extends DataRenderer {
    protected Canvas mBitmapCanvas;
    private RectF mCenterTextLastBounds = new RectF();
    private CharSequence mCenterTextLastValue;
    private StaticLayout mCenterTextLayout;
    private TextPaint mCenterTextPaint;
    protected PieChart mChart;
    protected WeakReference<Bitmap> mDrawBitmap;
    protected Path mDrawCenterTextPathBuffer = new Path();
    protected RectF mDrawHighlightedRectF = new RectF();
    private Paint mEntryLabelsPaint;
    private Path mHoleCirclePath = new Path();
    protected Paint mHolePaint;
    private RectF mInnerRectBuffer = new RectF();
    private Path mPathBuffer = new Path();
    private RectF[] mRectBuffer = {new RectF(), new RectF(), new RectF()};
    protected Paint mTransparentCirclePaint;
    protected Paint mValueLinePaint;

    public void initBuffers() {
    }

    public PieChartRenderer(PieChart pieChart, ChartAnimator chartAnimator, ViewPortHandler viewPortHandler) {
        super(chartAnimator, viewPortHandler);
        this.mChart = pieChart;
        this.mHolePaint = new Paint(1);
        this.mHolePaint.setColor(-1);
        this.mHolePaint.setStyle(Style.FILL);
        this.mTransparentCirclePaint = new Paint(1);
        this.mTransparentCirclePaint.setColor(-1);
        this.mTransparentCirclePaint.setStyle(Style.FILL);
        this.mTransparentCirclePaint.setAlpha(105);
        this.mCenterTextPaint = new TextPaint(1);
        this.mCenterTextPaint.setColor(ViewCompat.MEASURED_STATE_MASK);
        this.mCenterTextPaint.setTextSize(Utils.convertDpToPixel(12.0f));
        this.mValuePaint.setTextSize(Utils.convertDpToPixel(13.0f));
        this.mValuePaint.setColor(-1);
        this.mValuePaint.setTextAlign(Align.CENTER);
        this.mEntryLabelsPaint = new Paint(1);
        this.mEntryLabelsPaint.setColor(-1);
        this.mEntryLabelsPaint.setTextAlign(Align.CENTER);
        this.mEntryLabelsPaint.setTextSize(Utils.convertDpToPixel(13.0f));
        this.mValueLinePaint = new Paint(1);
        this.mValueLinePaint.setStyle(Style.STROKE);
    }

    public Paint getPaintHole() {
        return this.mHolePaint;
    }

    public Paint getPaintTransparentCircle() {
        return this.mTransparentCirclePaint;
    }

    public TextPaint getPaintCenterText() {
        return this.mCenterTextPaint;
    }

    public Paint getPaintEntryLabels() {
        return this.mEntryLabelsPaint;
    }

    public void drawData(Canvas canvas) {
        int chartWidth = (int) this.mViewPortHandler.getChartWidth();
        int chartHeight = (int) this.mViewPortHandler.getChartHeight();
        if (!(this.mDrawBitmap != null && ((Bitmap) this.mDrawBitmap.get()).getWidth() == chartWidth && ((Bitmap) this.mDrawBitmap.get()).getHeight() == chartHeight)) {
            if (chartWidth > 0 && chartHeight > 0) {
                this.mDrawBitmap = new WeakReference<>(Bitmap.createBitmap(chartWidth, chartHeight, Config.ARGB_4444));
                this.mBitmapCanvas = new Canvas((Bitmap) this.mDrawBitmap.get());
            } else {
                return;
            }
        }
        ((Bitmap) this.mDrawBitmap.get()).eraseColor(0);
        for (IPieDataSet iPieDataSet : ((PieData) this.mChart.getData()).getDataSets()) {
            if (iPieDataSet.isVisible() && iPieDataSet.getEntryCount() > 0) {
                drawDataSet(canvas, iPieDataSet);
            }
        }
    }

    /* access modifiers changed from: protected */
    public float calculateMinimumRadiusForSpacedSlice(MPPointF mPPointF, float f, float f2, float f3, float f4, float f5, float f6) {
        MPPointF mPPointF2 = mPPointF;
        double d = (double) ((f5 + f6) * 0.017453292f);
        float cos = mPPointF2.x + (((float) Math.cos(d)) * f);
        float sin = mPPointF2.y + (((float) Math.sin(d)) * f);
        double d2 = (double) ((f5 + (f6 / 2.0f)) * 0.017453292f);
        float cos2 = mPPointF2.x + (((float) Math.cos(d2)) * f);
        float sin2 = mPPointF2.y + (((float) Math.sin(d2)) * f);
        double sqrt = Math.sqrt(Math.pow((double) (cos - f3), 2.0d) + Math.pow((double) (sin - f4), 2.0d)) / 2.0d;
        double d3 = (double) f2;
        Double.isNaN(d3);
        double tan = (double) (f - ((float) (sqrt * Math.tan(((180.0d - d3) / 2.0d) * 0.017453292519943295d))));
        double sqrt2 = Math.sqrt(Math.pow((double) (cos2 - ((cos + f3) / 2.0f)), 2.0d) + Math.pow((double) (sin2 - ((sin + f4) / 2.0f)), 2.0d));
        Double.isNaN(tan);
        return (float) (tan - sqrt2);
    }

    /* access modifiers changed from: protected */
    public float getSliceSpace(IPieDataSet iPieDataSet) {
        float f;
        if (!iPieDataSet.isAutomaticallyDisableSliceSpacingEnabled()) {
            return iPieDataSet.getSliceSpace();
        }
        if (iPieDataSet.getSliceSpace() / this.mViewPortHandler.getSmallestContentExtension() > (iPieDataSet.getYMin() / ((PieData) this.mChart.getData()).getYValueSum()) * 2.0f) {
            f = 0.0f;
        } else {
            f = iPieDataSet.getSliceSpace();
        }
        return f;
    }

    /* access modifiers changed from: protected */
    public void drawDataSet(Canvas canvas, IPieDataSet iPieDataSet) {
        float f;
        float f2;
        int i;
        RectF rectF;
        float f3;
        float[] fArr;
        int i2;
        float f4;
        float f5;
        int i3;
        MPPointF mPPointF;
        float f6;
        float f7;
        float f8;
        float f9;
        float f10;
        RectF rectF2;
        int i4;
        int i5;
        float f11;
        MPPointF mPPointF2;
        int i6;
        PieChartRenderer pieChartRenderer = this;
        IPieDataSet iPieDataSet2 = iPieDataSet;
        float rotationAngle = pieChartRenderer.mChart.getRotationAngle();
        float phaseX = pieChartRenderer.mAnimator.getPhaseX();
        float phaseY = pieChartRenderer.mAnimator.getPhaseY();
        RectF circleBox = pieChartRenderer.mChart.getCircleBox();
        int entryCount = iPieDataSet.getEntryCount();
        float[] drawAngles = pieChartRenderer.mChart.getDrawAngles();
        MPPointF centerCircleBox = pieChartRenderer.mChart.getCenterCircleBox();
        float radius = pieChartRenderer.mChart.getRadius();
        boolean z = pieChartRenderer.mChart.isDrawHoleEnabled() && !pieChartRenderer.mChart.isDrawSlicesUnderHoleEnabled();
        float holeRadius = z ? (pieChartRenderer.mChart.getHoleRadius() / 100.0f) * radius : 0.0f;
        int i7 = 0;
        for (int i8 = 0; i8 < entryCount; i8++) {
            if (Math.abs(((PieEntry) iPieDataSet2.getEntryForIndex(i8)).getY()) > Utils.FLOAT_EPSILON) {
                i7++;
            }
        }
        if (i7 <= 1) {
            f = 0.0f;
        } else {
            f = pieChartRenderer.getSliceSpace(iPieDataSet2);
        }
        int i9 = 0;
        float f12 = 0.0f;
        while (i9 < entryCount) {
            float f13 = drawAngles[i9];
            if (Math.abs(iPieDataSet2.getEntryForIndex(i9).getY()) <= Utils.FLOAT_EPSILON || pieChartRenderer.mChart.needsHighlight(i9)) {
                i = i9;
                f4 = radius;
                f2 = rotationAngle;
                f3 = phaseX;
                rectF = circleBox;
                i2 = entryCount;
                fArr = drawAngles;
                i3 = i7;
                f5 = holeRadius;
                mPPointF = centerCircleBox;
            } else {
                boolean z2 = f > 0.0f && f13 <= 180.0f;
                pieChartRenderer.mRenderPaint.setColor(iPieDataSet2.getColor(i9));
                float f14 = i7 == 1 ? 0.0f : f / (radius * 0.017453292f);
                float f15 = rotationAngle + ((f12 + (f14 / 2.0f)) * phaseY);
                float f16 = (f13 - f14) * phaseY;
                if (f16 < 0.0f) {
                    f16 = 0.0f;
                }
                pieChartRenderer.mPathBuffer.reset();
                int i10 = i9;
                int i11 = i7;
                double d = (double) (f15 * 0.017453292f);
                i2 = entryCount;
                fArr = drawAngles;
                float cos = centerCircleBox.x + (((float) Math.cos(d)) * radius);
                float sin = centerCircleBox.y + (((float) Math.sin(d)) * radius);
                if (f16 < 360.0f || f16 % 360.0f > Utils.FLOAT_EPSILON) {
                    f3 = phaseX;
                    pieChartRenderer.mPathBuffer.moveTo(cos, sin);
                    pieChartRenderer.mPathBuffer.arcTo(circleBox, f15, f16);
                } else {
                    f3 = phaseX;
                    pieChartRenderer.mPathBuffer.addCircle(centerCircleBox.x, centerCircleBox.y, radius, Direction.CW);
                }
                float f17 = f16;
                pieChartRenderer.mInnerRectBuffer.set(centerCircleBox.x - holeRadius, centerCircleBox.y - holeRadius, centerCircleBox.x + holeRadius, centerCircleBox.y + holeRadius);
                if (!z) {
                    f8 = holeRadius;
                    f9 = radius;
                    f10 = rotationAngle;
                    rectF2 = circleBox;
                    i4 = i10;
                    i5 = i11;
                    f6 = f17;
                    mPPointF = centerCircleBox;
                    f7 = 360.0f;
                } else if (holeRadius > 0.0f || z2) {
                    if (z2) {
                        f11 = f17;
                        float f18 = radius;
                        rectF = circleBox;
                        i3 = i11;
                        i = i10;
                        f5 = holeRadius;
                        i6 = 1;
                        f4 = radius;
                        float f19 = f15;
                        mPPointF2 = centerCircleBox;
                        float calculateMinimumRadiusForSpacedSlice = calculateMinimumRadiusForSpacedSlice(centerCircleBox, f18, f13 * phaseY, cos, sin, f19, f11);
                        if (calculateMinimumRadiusForSpacedSlice < 0.0f) {
                            calculateMinimumRadiusForSpacedSlice = -calculateMinimumRadiusForSpacedSlice;
                        }
                        holeRadius = Math.max(f5, calculateMinimumRadiusForSpacedSlice);
                    } else {
                        f5 = holeRadius;
                        f4 = radius;
                        mPPointF2 = centerCircleBox;
                        rectF = circleBox;
                        i = i10;
                        i3 = i11;
                        f11 = f17;
                        i6 = 1;
                    }
                    float f20 = (i3 == i6 || holeRadius == 0.0f) ? 0.0f : f / (holeRadius * 0.017453292f);
                    float f21 = ((f12 + (f20 / 2.0f)) * phaseY) + rotationAngle;
                    float f22 = (f13 - f20) * phaseY;
                    if (f22 < 0.0f) {
                        f22 = 0.0f;
                    }
                    float f23 = f21 + f22;
                    if (f11 < 360.0f || f11 % 360.0f > Utils.FLOAT_EPSILON) {
                        pieChartRenderer = this;
                        double d2 = (double) (f23 * 0.017453292f);
                        f2 = rotationAngle;
                        pieChartRenderer.mPathBuffer.lineTo(mPPointF2.x + (((float) Math.cos(d2)) * holeRadius), mPPointF2.y + (holeRadius * ((float) Math.sin(d2))));
                        pieChartRenderer.mPathBuffer.arcTo(pieChartRenderer.mInnerRectBuffer, f23, -f22);
                    } else {
                        pieChartRenderer = this;
                        pieChartRenderer.mPathBuffer.addCircle(mPPointF2.x, mPPointF2.y, holeRadius, Direction.CCW);
                        f2 = rotationAngle;
                    }
                    mPPointF = mPPointF2;
                    pieChartRenderer.mPathBuffer.close();
                    pieChartRenderer.mBitmapCanvas.drawPath(pieChartRenderer.mPathBuffer, pieChartRenderer.mRenderPaint);
                } else {
                    f8 = holeRadius;
                    f9 = radius;
                    f10 = rotationAngle;
                    rectF2 = circleBox;
                    i4 = i10;
                    i5 = i11;
                    f6 = f17;
                    f7 = 360.0f;
                    mPPointF = centerCircleBox;
                }
                if (f6 % f7 > Utils.FLOAT_EPSILON) {
                    if (z2) {
                        float f24 = f15 + (f6 / 2.0f);
                        float calculateMinimumRadiusForSpacedSlice2 = calculateMinimumRadiusForSpacedSlice(mPPointF, f4, f13 * phaseY, cos, sin, f15, f6);
                        double d3 = (double) (f24 * 0.017453292f);
                        pieChartRenderer.mPathBuffer.lineTo(mPPointF.x + (((float) Math.cos(d3)) * calculateMinimumRadiusForSpacedSlice2), mPPointF.y + (calculateMinimumRadiusForSpacedSlice2 * ((float) Math.sin(d3))));
                    } else {
                        pieChartRenderer.mPathBuffer.lineTo(mPPointF.x, mPPointF.y);
                    }
                }
                pieChartRenderer.mPathBuffer.close();
                pieChartRenderer.mBitmapCanvas.drawPath(pieChartRenderer.mPathBuffer, pieChartRenderer.mRenderPaint);
            }
            f12 += f13 * f3;
            i9 = i + 1;
            centerCircleBox = mPPointF;
            i7 = i3;
            holeRadius = f5;
            radius = f4;
            entryCount = i2;
            drawAngles = fArr;
            phaseX = f3;
            circleBox = rectF;
            rotationAngle = f2;
            iPieDataSet2 = iPieDataSet;
        }
        MPPointF.recycleInstance(centerCircleBox);
    }

    /* JADX WARNING: Removed duplicated region for block: B:100:0x0312  */
    /* JADX WARNING: Removed duplicated region for block: B:105:0x0329  */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x018a  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x0193  */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x019b  */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x01a9  */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x0209  */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x021c  */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x0242  */
    /* JADX WARNING: Removed duplicated region for block: B:80:0x028e  */
    /* JADX WARNING: Removed duplicated region for block: B:85:0x02a6  */
    public void drawValues(Canvas canvas) {
        float[] fArr;
        float f;
        float[] fArr2;
        float f2;
        int i;
        List list;
        float f3;
        float y;
        int i2;
        boolean z;
        float f4;
        ValuePosition valuePosition;
        float f5;
        ValuePosition valuePosition2;
        int i3;
        IPieDataSet iPieDataSet;
        int i4;
        float[] fArr3;
        float f6;
        float f7;
        float abs;
        float f8;
        double d;
        float f9;
        float f10;
        float f11;
        IPieDataSet iPieDataSet2;
        Canvas canvas2 = canvas;
        MPPointF centerCircleBox = this.mChart.getCenterCircleBox();
        float radius = this.mChart.getRadius();
        float rotationAngle = this.mChart.getRotationAngle();
        float[] drawAngles = this.mChart.getDrawAngles();
        float[] absoluteAngles = this.mChart.getAbsoluteAngles();
        float phaseX = this.mAnimator.getPhaseX();
        float phaseY = this.mAnimator.getPhaseY();
        float holeRadius = this.mChart.getHoleRadius() / 100.0f;
        float f12 = (radius / 10.0f) * 3.6f;
        if (this.mChart.isDrawHoleEnabled()) {
            f12 = (radius - (radius * holeRadius)) / 2.0f;
        }
        float f13 = radius - f12;
        PieData pieData = (PieData) this.mChart.getData();
        List dataSets = pieData.getDataSets();
        float yValueSum = pieData.getYValueSum();
        boolean isDrawEntryLabelsEnabled = this.mChart.isDrawEntryLabelsEnabled();
        canvas.save();
        float convertDpToPixel = Utils.convertDpToPixel(5.0f);
        int i5 = 0;
        int i6 = 0;
        while (i6 < dataSets.size()) {
            IPieDataSet iPieDataSet3 = (IPieDataSet) dataSets.get(i6);
            boolean isDrawValuesEnabled = iPieDataSet3.isDrawValuesEnabled();
            if (isDrawValuesEnabled || isDrawEntryLabelsEnabled) {
                ValuePosition xValuePosition = iPieDataSet3.getXValuePosition();
                ValuePosition yValuePosition = iPieDataSet3.getYValuePosition();
                applyValueTextStyle(iPieDataSet3);
                float calcTextHeight = ((float) Utils.calcTextHeight(this.mValuePaint, "Q")) + Utils.convertDpToPixel(4.0f);
                IValueFormatter valueFormatter = iPieDataSet3.getValueFormatter();
                int entryCount = iPieDataSet3.getEntryCount();
                this.mValueLinePaint.setColor(iPieDataSet3.getValueLineColor());
                this.mValueLinePaint.setStrokeWidth(Utils.convertDpToPixel(iPieDataSet3.getValueLineWidth()));
                float sliceSpace = getSliceSpace(iPieDataSet3);
                int i7 = i5;
                int i8 = 0;
                while (i8 < entryCount) {
                    PieEntry pieEntry = (PieEntry) iPieDataSet3.getEntryForIndex(i8);
                    if (i7 == 0) {
                        f3 = 0.0f;
                    } else {
                        f3 = absoluteAngles[i7 - 1] * phaseX;
                    }
                    float f14 = ((f3 + ((drawAngles[i7] - ((sliceSpace / (f13 * 0.017453292f)) / 2.0f)) / 2.0f)) * phaseY) + rotationAngle;
                    if (this.mChart.isUsePercentValuesEnabled()) {
                        y = (pieEntry.getY() / yValueSum) * 100.0f;
                    } else {
                        y = pieEntry.getY();
                    }
                    float f15 = y;
                    int i9 = i8;
                    double d2 = (double) (f14 * 0.017453292f);
                    int i10 = i6;
                    List list2 = dataSets;
                    float cos = (float) Math.cos(d2);
                    float f16 = rotationAngle;
                    float[] fArr4 = drawAngles;
                    float sin = (float) Math.sin(d2);
                    boolean z2 = isDrawEntryLabelsEnabled && xValuePosition == ValuePosition.OUTSIDE_SLICE;
                    boolean z3 = isDrawValuesEnabled && yValuePosition == ValuePosition.OUTSIDE_SLICE;
                    if (isDrawEntryLabelsEnabled) {
                        i2 = entryCount;
                        if (xValuePosition == ValuePosition.INSIDE_SLICE) {
                            z = true;
                            boolean z4 = !isDrawValuesEnabled && yValuePosition == ValuePosition.INSIDE_SLICE;
                            if (!z2 || z3) {
                                float valueLinePart1Length = iPieDataSet3.getValueLinePart1Length();
                                float valueLinePart2Length = iPieDataSet3.getValueLinePart2Length();
                                float valueLinePart1OffsetPercentage = iPieDataSet3.getValueLinePart1OffsetPercentage() / 100.0f;
                                ValuePosition valuePosition3 = yValuePosition;
                                if (!this.mChart.isDrawHoleEnabled()) {
                                    float f17 = radius * holeRadius;
                                    f7 = ((radius - f17) * valueLinePart1OffsetPercentage) + f17;
                                } else {
                                    f7 = radius * valueLinePart1OffsetPercentage;
                                }
                                abs = !iPieDataSet3.isValueLineVariableLength() ? valueLinePart2Length * f13 * ((float) Math.abs(Math.sin(d2))) : valueLinePart2Length * f13;
                                float f18 = (f7 * cos) + centerCircleBox.x;
                                float f19 = (f7 * sin) + centerCircleBox.y;
                                float f20 = (valueLinePart1Length + 1.0f) * f13;
                                valuePosition = xValuePosition;
                                f8 = (f20 * cos) + centerCircleBox.x;
                                float f21 = (f20 * sin) + centerCircleBox.y;
                                double d3 = (double) f14;
                                Double.isNaN(d3);
                                d = d3 % 360.0d;
                                if (d >= 90.0d || d > 270.0d) {
                                    f9 = f8 + abs;
                                    this.mValuePaint.setTextAlign(Align.LEFT);
                                    if (z2) {
                                        this.mEntryLabelsPaint.setTextAlign(Align.LEFT);
                                    }
                                    f10 = f9 + convertDpToPixel;
                                } else {
                                    float f22 = f8 - abs;
                                    this.mValuePaint.setTextAlign(Align.RIGHT);
                                    if (z2) {
                                        this.mEntryLabelsPaint.setTextAlign(Align.RIGHT);
                                    }
                                    f9 = f22;
                                    f10 = f22 - convertDpToPixel;
                                }
                                if (iPieDataSet3.getValueLineColor() == 1122867) {
                                    Canvas canvas3 = canvas;
                                    int i11 = i9;
                                    f4 = radius;
                                    i3 = i2;
                                    i4 = i11;
                                    valuePosition2 = valuePosition3;
                                    float f23 = f21;
                                    f11 = f10;
                                    canvas3.drawLine(f18, f19, f8, f23, this.mValueLinePaint);
                                    canvas3.drawLine(f8, f21, f9, f23, this.mValueLinePaint);
                                } else {
                                    f11 = f10;
                                    f4 = radius;
                                    i4 = i9;
                                    i3 = i2;
                                    valuePosition2 = valuePosition3;
                                }
                                if (z2 || !z3) {
                                    iPieDataSet2 = iPieDataSet3;
                                    f5 = cos;
                                    float f24 = f11;
                                    if (!z2) {
                                        if (i4 < pieData.getEntryCount() && pieEntry.getLabel() != null) {
                                            drawEntryLabel(canvas2, pieEntry.getLabel(), f24, f21 + (calcTextHeight / 2.0f));
                                        }
                                    } else if (z3) {
                                        iPieDataSet = iPieDataSet2;
                                        drawValue(canvas, valueFormatter, f15, pieEntry, 0, f24, f21 + (calcTextHeight / 2.0f), iPieDataSet.getValueTextColor(i4));
                                    }
                                } else {
                                    iPieDataSet2 = iPieDataSet3;
                                    f5 = cos;
                                    drawValue(canvas, valueFormatter, f15, pieEntry, 0, f11, f21, iPieDataSet3.getValueTextColor(i4));
                                    if (i4 < pieData.getEntryCount() && pieEntry.getLabel() != null) {
                                        drawEntryLabel(canvas2, pieEntry.getLabel(), f11, f21 + calcTextHeight);
                                    }
                                }
                                iPieDataSet = iPieDataSet2;
                            } else {
                                valuePosition = xValuePosition;
                                iPieDataSet = iPieDataSet3;
                                f5 = cos;
                                f4 = radius;
                                i4 = i9;
                                i3 = i2;
                                valuePosition2 = yValuePosition;
                            }
                            if (!z || z4) {
                                f6 = (f13 * f5) + centerCircleBox.x;
                                float f25 = (sin * f13) + centerCircleBox.y;
                                this.mValuePaint.setTextAlign(Align.CENTER);
                                if (z || !z4) {
                                    fArr3 = absoluteAngles;
                                    float f26 = f6;
                                    if (!z) {
                                        if (i4 < pieData.getEntryCount() && pieEntry.getLabel() != null) {
                                            drawEntryLabel(canvas2, pieEntry.getLabel(), f26, f25 + (calcTextHeight / 2.0f));
                                        }
                                    } else if (z4) {
                                        drawValue(canvas, valueFormatter, f15, pieEntry, 0, f26, f25 + (calcTextHeight / 2.0f), iPieDataSet.getValueTextColor(i4));
                                    }
                                } else {
                                    fArr3 = absoluteAngles;
                                    float f27 = f6;
                                    drawValue(canvas, valueFormatter, f15, pieEntry, 0, f6, f25, iPieDataSet.getValueTextColor(i4));
                                    if (i4 < pieData.getEntryCount() && pieEntry.getLabel() != null) {
                                        drawEntryLabel(canvas2, pieEntry.getLabel(), f27, f25 + calcTextHeight);
                                    }
                                }
                            } else {
                                fArr3 = absoluteAngles;
                            }
                            i7++;
                            i8 = i4 + 1;
                            iPieDataSet3 = iPieDataSet;
                            entryCount = i3;
                            dataSets = list2;
                            i6 = i10;
                            rotationAngle = f16;
                            drawAngles = fArr4;
                            yValuePosition = valuePosition2;
                            xValuePosition = valuePosition;
                            radius = f4;
                            absoluteAngles = fArr3;
                        }
                    } else {
                        i2 = entryCount;
                    }
                    z = false;
                    if (!isDrawValuesEnabled) {
                    }
                    if (!z2) {
                    }
                    float valueLinePart1Length2 = iPieDataSet3.getValueLinePart1Length();
                    float valueLinePart2Length2 = iPieDataSet3.getValueLinePart2Length();
                    float valueLinePart1OffsetPercentage2 = iPieDataSet3.getValueLinePart1OffsetPercentage() / 100.0f;
                    ValuePosition valuePosition32 = yValuePosition;
                    if (!this.mChart.isDrawHoleEnabled()) {
                    }
                    if (!iPieDataSet3.isValueLineVariableLength()) {
                    }
                    float f182 = (f7 * cos) + centerCircleBox.x;
                    float f192 = (f7 * sin) + centerCircleBox.y;
                    float f202 = (valueLinePart1Length2 + 1.0f) * f13;
                    valuePosition = xValuePosition;
                    f8 = (f202 * cos) + centerCircleBox.x;
                    float f212 = (f202 * sin) + centerCircleBox.y;
                    double d32 = (double) f14;
                    Double.isNaN(d32);
                    d = d32 % 360.0d;
                    if (d >= 90.0d) {
                    }
                    f9 = f8 + abs;
                    this.mValuePaint.setTextAlign(Align.LEFT);
                    if (z2) {
                    }
                    f10 = f9 + convertDpToPixel;
                    if (iPieDataSet3.getValueLineColor() == 1122867) {
                    }
                    if (z2) {
                    }
                    iPieDataSet2 = iPieDataSet3;
                    f5 = cos;
                    float f242 = f11;
                    if (!z2) {
                    }
                    iPieDataSet = iPieDataSet2;
                    if (!z) {
                    }
                    f6 = (f13 * f5) + centerCircleBox.x;
                    float f252 = (sin * f13) + centerCircleBox.y;
                    this.mValuePaint.setTextAlign(Align.CENTER);
                    if (z) {
                    }
                    fArr3 = absoluteAngles;
                    float f262 = f6;
                    if (!z) {
                    }
                    i7++;
                    i8 = i4 + 1;
                    iPieDataSet3 = iPieDataSet;
                    entryCount = i3;
                    dataSets = list2;
                    i6 = i10;
                    rotationAngle = f16;
                    drawAngles = fArr4;
                    yValuePosition = valuePosition2;
                    xValuePosition = valuePosition;
                    radius = f4;
                    absoluteAngles = fArr3;
                }
                i = i6;
                list = dataSets;
                f = radius;
                f2 = rotationAngle;
                fArr2 = drawAngles;
                fArr = absoluteAngles;
                i5 = i7;
            } else {
                i = i6;
                list = dataSets;
                f = radius;
                f2 = rotationAngle;
                fArr2 = drawAngles;
                fArr = absoluteAngles;
            }
            i6 = i + 1;
            dataSets = list;
            rotationAngle = f2;
            drawAngles = fArr2;
            radius = f;
            absoluteAngles = fArr;
        }
        MPPointF.recycleInstance(centerCircleBox);
        canvas.restore();
    }

    /* access modifiers changed from: protected */
    public void drawEntryLabel(Canvas canvas, String str, float f, float f2) {
        canvas.drawText(str, f, f2, this.mEntryLabelsPaint);
    }

    public void drawExtras(Canvas canvas) {
        drawHole(canvas);
        canvas.drawBitmap((Bitmap) this.mDrawBitmap.get(), 0.0f, 0.0f, null);
        drawCenterText(canvas);
    }

    /* access modifiers changed from: protected */
    public void drawHole(Canvas canvas) {
        if (this.mChart.isDrawHoleEnabled() && this.mBitmapCanvas != null) {
            float radius = this.mChart.getRadius();
            float holeRadius = (this.mChart.getHoleRadius() / 100.0f) * radius;
            MPPointF centerCircleBox = this.mChart.getCenterCircleBox();
            if (Color.alpha(this.mHolePaint.getColor()) > 0) {
                this.mBitmapCanvas.drawCircle(centerCircleBox.x, centerCircleBox.y, holeRadius, this.mHolePaint);
            }
            if (Color.alpha(this.mTransparentCirclePaint.getColor()) > 0 && this.mChart.getTransparentCircleRadius() > this.mChart.getHoleRadius()) {
                int alpha = this.mTransparentCirclePaint.getAlpha();
                float transparentCircleRadius = radius * (this.mChart.getTransparentCircleRadius() / 100.0f);
                this.mTransparentCirclePaint.setAlpha((int) (((float) alpha) * this.mAnimator.getPhaseX() * this.mAnimator.getPhaseY()));
                this.mHoleCirclePath.reset();
                this.mHoleCirclePath.addCircle(centerCircleBox.x, centerCircleBox.y, transparentCircleRadius, Direction.CW);
                this.mHoleCirclePath.addCircle(centerCircleBox.x, centerCircleBox.y, holeRadius, Direction.CCW);
                this.mBitmapCanvas.drawPath(this.mHoleCirclePath, this.mTransparentCirclePaint);
                this.mTransparentCirclePaint.setAlpha(alpha);
            }
            MPPointF.recycleInstance(centerCircleBox);
        }
    }

    /* access modifiers changed from: protected */
    public void drawCenterText(Canvas canvas) {
        float f;
        MPPointF mPPointF;
        Canvas canvas2 = canvas;
        CharSequence centerText = this.mChart.getCenterText();
        if (this.mChart.isDrawCenterTextEnabled() && centerText != null) {
            MPPointF centerCircleBox = this.mChart.getCenterCircleBox();
            MPPointF centerTextOffset = this.mChart.getCenterTextOffset();
            float f2 = centerCircleBox.x + centerTextOffset.x;
            float f3 = centerCircleBox.y + centerTextOffset.y;
            if (!this.mChart.isDrawHoleEnabled() || this.mChart.isDrawSlicesUnderHoleEnabled()) {
                f = this.mChart.getRadius();
            } else {
                f = this.mChart.getRadius() * (this.mChart.getHoleRadius() / 100.0f);
            }
            RectF rectF = this.mRectBuffer[0];
            rectF.left = f2 - f;
            rectF.top = f3 - f;
            rectF.right = f2 + f;
            rectF.bottom = f3 + f;
            RectF rectF2 = this.mRectBuffer[1];
            rectF2.set(rectF);
            float centerTextRadiusPercent = this.mChart.getCenterTextRadiusPercent() / 100.0f;
            if (((double) centerTextRadiusPercent) > Utils.DOUBLE_EPSILON) {
                rectF2.inset((rectF2.width() - (rectF2.width() * centerTextRadiusPercent)) / 2.0f, (rectF2.height() - (rectF2.height() * centerTextRadiusPercent)) / 2.0f);
            }
            if (!centerText.equals(this.mCenterTextLastValue) || !rectF2.equals(this.mCenterTextLastBounds)) {
                this.mCenterTextLastBounds.set(rectF2);
                this.mCenterTextLastValue = centerText;
                mPPointF = centerTextOffset;
                StaticLayout staticLayout = r3;
                StaticLayout staticLayout2 = new StaticLayout(centerText, 0, centerText.length(), this.mCenterTextPaint, (int) Math.max(Math.ceil((double) this.mCenterTextLastBounds.width()), 1.0d), Alignment.ALIGN_CENTER, 1.0f, 0.0f, false);
                this.mCenterTextLayout = staticLayout;
            } else {
                mPPointF = centerTextOffset;
            }
            float height = (float) this.mCenterTextLayout.getHeight();
            canvas.save();
            if (VERSION.SDK_INT >= 18) {
                Path path = this.mDrawCenterTextPathBuffer;
                path.reset();
                path.addOval(rectF, Direction.CW);
                canvas2.clipPath(path);
            }
            canvas2.translate(rectF2.left, rectF2.top + ((rectF2.height() - height) / 2.0f));
            this.mCenterTextLayout.draw(canvas2);
            canvas.restore();
            MPPointF.recycleInstance(centerCircleBox);
            MPPointF.recycleInstance(mPPointF);
        }
    }

    public void drawHighlighted(Canvas canvas, Highlight[] highlightArr) {
        float f;
        RectF rectF;
        float[] fArr;
        float[] fArr2;
        float f2;
        int i;
        float f3;
        int i2;
        float f4;
        float f5;
        float f6;
        int i3;
        int i4;
        float f7;
        float f8;
        Highlight[] highlightArr2 = highlightArr;
        float phaseX = this.mAnimator.getPhaseX();
        float phaseY = this.mAnimator.getPhaseY();
        float rotationAngle = this.mChart.getRotationAngle();
        float[] drawAngles = this.mChart.getDrawAngles();
        float[] absoluteAngles = this.mChart.getAbsoluteAngles();
        MPPointF centerCircleBox = this.mChart.getCenterCircleBox();
        float radius = this.mChart.getRadius();
        boolean z = this.mChart.isDrawHoleEnabled() && !this.mChart.isDrawSlicesUnderHoleEnabled();
        float holeRadius = z ? (this.mChart.getHoleRadius() / 100.0f) * radius : 0.0f;
        RectF rectF2 = this.mDrawHighlightedRectF;
        rectF2.set(0.0f, 0.0f, 0.0f, 0.0f);
        int i5 = 0;
        while (i5 < highlightArr2.length) {
            int x = (int) highlightArr2[i5].getX();
            if (x < drawAngles.length) {
                IPieDataSet dataSetByIndex = ((PieData) this.mChart.getData()).getDataSetByIndex(highlightArr2[i5].getDataSetIndex());
                if (dataSetByIndex != null && dataSetByIndex.isHighlightEnabled()) {
                    int entryCount = dataSetByIndex.getEntryCount();
                    int i6 = 0;
                    for (int i7 = 0; i7 < entryCount; i7++) {
                        if (Math.abs(((PieEntry) dataSetByIndex.getEntryForIndex(i7)).getY()) > Utils.FLOAT_EPSILON) {
                            i6++;
                        }
                    }
                    if (x == 0) {
                        i2 = 1;
                        f3 = 0.0f;
                    } else {
                        f3 = absoluteAngles[x - 1] * phaseX;
                        i2 = 1;
                    }
                    if (i6 <= i2) {
                        f4 = 0.0f;
                    } else {
                        f4 = dataSetByIndex.getSliceSpace();
                    }
                    float f9 = drawAngles[x];
                    float selectionShift = dataSetByIndex.getSelectionShift();
                    float f10 = radius + selectionShift;
                    int i8 = i5;
                    rectF2.set(this.mChart.getCircleBox());
                    float f11 = -selectionShift;
                    rectF2.inset(f11, f11);
                    boolean z2 = f4 > 0.0f && f9 <= 180.0f;
                    this.mRenderPaint.setColor(dataSetByIndex.getColor(x));
                    float f12 = i6 == 1 ? 0.0f : f4 / (radius * 0.017453292f);
                    float f13 = i6 == 1 ? 0.0f : f4 / (f10 * 0.017453292f);
                    float f14 = rotationAngle + ((f3 + (f12 / 2.0f)) * phaseY);
                    float f15 = (f9 - f12) * phaseY;
                    float f16 = f15 < 0.0f ? 0.0f : f15;
                    float f17 = ((f3 + (f13 / 2.0f)) * phaseY) + rotationAngle;
                    float f18 = (f9 - f13) * phaseY;
                    if (f18 < 0.0f) {
                        f18 = 0.0f;
                    }
                    this.mPathBuffer.reset();
                    if (f16 < 360.0f || f16 % 360.0f > Utils.FLOAT_EPSILON) {
                        f5 = holeRadius;
                        f2 = phaseX;
                        double d = (double) (f17 * 0.017453292f);
                        fArr2 = drawAngles;
                        fArr = absoluteAngles;
                        this.mPathBuffer.moveTo(centerCircleBox.x + (((float) Math.cos(d)) * f10), centerCircleBox.y + (f10 * ((float) Math.sin(d))));
                        this.mPathBuffer.arcTo(rectF2, f17, f18);
                    } else {
                        this.mPathBuffer.addCircle(centerCircleBox.x, centerCircleBox.y, f10, Direction.CW);
                        f5 = holeRadius;
                        f2 = phaseX;
                        fArr2 = drawAngles;
                        fArr = absoluteAngles;
                    }
                    if (z2) {
                        double d2 = (double) (f14 * 0.017453292f);
                        i = i8;
                        f6 = f5;
                        i4 = i6;
                        rectF = rectF2;
                        i3 = 1;
                        f7 = calculateMinimumRadiusForSpacedSlice(centerCircleBox, radius, f9 * phaseY, (((float) Math.cos(d2)) * radius) + centerCircleBox.x, centerCircleBox.y + (((float) Math.sin(d2)) * radius), f14, f16);
                    } else {
                        rectF = rectF2;
                        i4 = i6;
                        i = i8;
                        f6 = f5;
                        i3 = 1;
                        f7 = 0.0f;
                    }
                    this.mInnerRectBuffer.set(centerCircleBox.x - f6, centerCircleBox.y - f6, centerCircleBox.x + f6, centerCircleBox.y + f6);
                    if (!z || (f6 <= 0.0f && !z2)) {
                        f = f6;
                        if (f16 % 360.0f > Utils.FLOAT_EPSILON) {
                            if (z2) {
                                double d3 = (double) ((f14 + (f16 / 2.0f)) * 0.017453292f);
                                this.mPathBuffer.lineTo(centerCircleBox.x + (((float) Math.cos(d3)) * f7), centerCircleBox.y + (f7 * ((float) Math.sin(d3))));
                            } else {
                                this.mPathBuffer.lineTo(centerCircleBox.x, centerCircleBox.y);
                            }
                        }
                    } else {
                        if (z2) {
                            if (f7 < 0.0f) {
                                f7 = -f7;
                            }
                            f8 = Math.max(f6, f7);
                        } else {
                            f8 = f6;
                        }
                        float f19 = (i4 == i3 || f8 == 0.0f) ? 0.0f : f4 / (f8 * 0.017453292f);
                        float f20 = rotationAngle + ((f3 + (f19 / 2.0f)) * phaseY);
                        float f21 = (f9 - f19) * phaseY;
                        if (f21 < 0.0f) {
                            f21 = 0.0f;
                        }
                        float f22 = f20 + f21;
                        if (f16 < 360.0f || f16 % 360.0f > Utils.FLOAT_EPSILON) {
                            double d4 = (double) (f22 * 0.017453292f);
                            f = f6;
                            this.mPathBuffer.lineTo(centerCircleBox.x + (((float) Math.cos(d4)) * f8), centerCircleBox.y + (f8 * ((float) Math.sin(d4))));
                            this.mPathBuffer.arcTo(this.mInnerRectBuffer, f22, -f21);
                        } else {
                            this.mPathBuffer.addCircle(centerCircleBox.x, centerCircleBox.y, f8, Direction.CCW);
                            f = f6;
                        }
                    }
                    this.mPathBuffer.close();
                    this.mBitmapCanvas.drawPath(this.mPathBuffer, this.mRenderPaint);
                    i5 = i + 1;
                    phaseX = f2;
                    drawAngles = fArr2;
                    absoluteAngles = fArr;
                    rectF2 = rectF;
                    holeRadius = f;
                    highlightArr2 = highlightArr;
                }
            }
            i = i5;
            rectF = rectF2;
            f = holeRadius;
            f2 = phaseX;
            fArr2 = drawAngles;
            fArr = absoluteAngles;
            i5 = i + 1;
            phaseX = f2;
            drawAngles = fArr2;
            absoluteAngles = fArr;
            rectF2 = rectF;
            holeRadius = f;
            highlightArr2 = highlightArr;
        }
        MPPointF.recycleInstance(centerCircleBox);
    }

    /* access modifiers changed from: protected */
    public void drawRoundedSlices(Canvas canvas) {
        float f;
        float[] fArr;
        float f2;
        if (this.mChart.isDrawRoundedSlicesEnabled()) {
            IPieDataSet dataSet = ((PieData) this.mChart.getData()).getDataSet();
            if (dataSet.isVisible()) {
                float phaseX = this.mAnimator.getPhaseX();
                float phaseY = this.mAnimator.getPhaseY();
                MPPointF centerCircleBox = this.mChart.getCenterCircleBox();
                float radius = this.mChart.getRadius();
                float holeRadius = (radius - ((this.mChart.getHoleRadius() * radius) / 100.0f)) / 2.0f;
                float[] drawAngles = this.mChart.getDrawAngles();
                float rotationAngle = this.mChart.getRotationAngle();
                int i = 0;
                while (i < dataSet.getEntryCount()) {
                    float f3 = drawAngles[i];
                    if (Math.abs(dataSet.getEntryForIndex(i).getY()) > Utils.FLOAT_EPSILON) {
                        double d = (double) (radius - holeRadius);
                        double d2 = (double) ((rotationAngle + f3) * phaseY);
                        double cos = Math.cos(Math.toRadians(d2));
                        Double.isNaN(d);
                        double d3 = cos * d;
                        f2 = phaseY;
                        fArr = drawAngles;
                        f = rotationAngle;
                        double d4 = (double) centerCircleBox.x;
                        Double.isNaN(d4);
                        float f4 = (float) (d4 + d3);
                        double sin = Math.sin(Math.toRadians(d2));
                        Double.isNaN(d);
                        double d5 = d * sin;
                        double d6 = (double) centerCircleBox.y;
                        Double.isNaN(d6);
                        float f5 = (float) (d5 + d6);
                        this.mRenderPaint.setColor(dataSet.getColor(i));
                        this.mBitmapCanvas.drawCircle(f4, f5, holeRadius, this.mRenderPaint);
                    } else {
                        f2 = phaseY;
                        fArr = drawAngles;
                        f = rotationAngle;
                    }
                    rotationAngle = f + (f3 * phaseX);
                    i++;
                    phaseY = f2;
                    drawAngles = fArr;
                }
                MPPointF.recycleInstance(centerCircleBox);
            }
        }
    }

    public void releaseBitmap() {
        if (this.mBitmapCanvas != null) {
            this.mBitmapCanvas.setBitmap(null);
            this.mBitmapCanvas = null;
        }
        if (this.mDrawBitmap != null) {
            ((Bitmap) this.mDrawBitmap.get()).recycle();
            this.mDrawBitmap.clear();
            this.mDrawBitmap = null;
        }
    }
}
