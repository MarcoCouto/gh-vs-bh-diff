package com.github.mikephil.charting.charts;

import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.RectF;
import android.os.Build.VERSION;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.animation.Easing.EasingOption;
import com.github.mikephil.charting.components.Legend.LegendHorizontalAlignment;
import com.github.mikephil.charting.components.Legend.LegendVerticalAlignment;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.ChartData;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.interfaces.datasets.IDataSet;
import com.github.mikephil.charting.listener.PieRadarChartTouchListener;
import com.github.mikephil.charting.utils.MPPointF;
import com.github.mikephil.charting.utils.Utils;

public abstract class PieRadarChartBase<T extends ChartData<? extends IDataSet<? extends Entry>>> extends Chart<T> {
    protected float mMinOffset = 0.0f;
    private float mRawRotationAngle = 270.0f;
    protected boolean mRotateEnabled = true;
    private float mRotationAngle = 270.0f;

    /* access modifiers changed from: protected */
    public void calcMinMax() {
    }

    public abstract int getIndexForAngle(float f);

    public abstract float getRadius();

    /* access modifiers changed from: protected */
    public abstract float getRequiredBaseOffset();

    /* access modifiers changed from: protected */
    public abstract float getRequiredLegendOffset();

    public float getYChartMax() {
        return 0.0f;
    }

    public float getYChartMin() {
        return 0.0f;
    }

    public PieRadarChartBase(Context context) {
        super(context);
    }

    public PieRadarChartBase(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public PieRadarChartBase(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    /* access modifiers changed from: protected */
    public void init() {
        super.init();
        this.mChartTouchListener = new PieRadarChartTouchListener(this);
    }

    public int getMaxVisibleCount() {
        return this.mData.getEntryCount();
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (!this.mTouchEnabled || this.mChartTouchListener == null) {
            return super.onTouchEvent(motionEvent);
        }
        return this.mChartTouchListener.onTouch(this, motionEvent);
    }

    public void computeScroll() {
        if (this.mChartTouchListener instanceof PieRadarChartTouchListener) {
            ((PieRadarChartTouchListener) this.mChartTouchListener).computeScroll();
        }
    }

    public void notifyDataSetChanged() {
        if (this.mData != null) {
            calcMinMax();
            if (this.mLegend != null) {
                this.mLegendRenderer.computeLegend(this.mData);
            }
            calculateOffsets();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0080, code lost:
        r2 = r0;
        r0 = 0.0f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0084, code lost:
        r3 = r0;
        r0 = 0.0f;
        r2 = 0.0f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x0176, code lost:
        r0 = 0.0f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x0177, code lost:
        r2 = 0.0f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x0178, code lost:
        r3 = 0.0f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x0179, code lost:
        r1 = r1 + getRequiredBaseOffset();
        r0 = r0 + getRequiredBaseOffset();
        r3 = r3 + getRequiredBaseOffset();
        r2 = r2 + getRequiredBaseOffset();
     */
    public void calculateOffsets() {
        float f;
        float f2;
        float f3;
        float f4;
        float min;
        float f5 = 0.0f;
        if (this.mLegend != null && this.mLegend.isEnabled() && !this.mLegend.isDrawInsideEnabled()) {
            float min2 = Math.min(this.mLegend.mNeededWidth, this.mViewPortHandler.getChartWidth() * this.mLegend.getMaxSizePercent());
            switch (this.mLegend.getOrientation()) {
                case VERTICAL:
                    if (this.mLegend.getHorizontalAlignment() != LegendHorizontalAlignment.LEFT && this.mLegend.getHorizontalAlignment() != LegendHorizontalAlignment.RIGHT) {
                        f4 = 0.0f;
                    } else if (this.mLegend.getVerticalAlignment() == LegendVerticalAlignment.CENTER) {
                        f4 = min2 + Utils.convertDpToPixel(13.0f);
                    } else {
                        f4 = min2 + Utils.convertDpToPixel(8.0f);
                        float f6 = this.mLegend.mNeededHeight + this.mLegend.mTextHeightMax;
                        MPPointF center = getCenter();
                        float width = this.mLegend.getHorizontalAlignment() == LegendHorizontalAlignment.RIGHT ? (((float) getWidth()) - f4) + 15.0f : f4 - 15.0f;
                        float f7 = f6 + 15.0f;
                        float distanceToCenter = distanceToCenter(width, f7);
                        MPPointF position = getPosition(center, getRadius(), getAngleForPoint(width, f7));
                        float distanceToCenter2 = distanceToCenter(position.x, position.y);
                        float convertDpToPixel = Utils.convertDpToPixel(5.0f);
                        if (f7 < center.y || ((float) getHeight()) - f4 <= ((float) getWidth())) {
                            f4 = distanceToCenter < distanceToCenter2 ? convertDpToPixel + (distanceToCenter2 - distanceToCenter) : 0.0f;
                        }
                        MPPointF.recycleInstance(center);
                        MPPointF.recycleInstance(position);
                    }
                    switch (this.mLegend.getHorizontalAlignment()) {
                        case LEFT:
                            f5 = f4;
                            break;
                        case RIGHT:
                            break;
                        case CENTER:
                            switch (this.mLegend.getVerticalAlignment()) {
                                case TOP:
                                    min = Math.min(this.mLegend.mNeededHeight, this.mViewPortHandler.getChartHeight() * this.mLegend.getMaxSizePercent());
                                    break;
                                case BOTTOM:
                                    min = Math.min(this.mLegend.mNeededHeight, this.mViewPortHandler.getChartHeight() * this.mLegend.getMaxSizePercent());
                                    break;
                            }
                    }
                case HORIZONTAL:
                    if (this.mLegend.getVerticalAlignment() == LegendVerticalAlignment.TOP || this.mLegend.getVerticalAlignment() == LegendVerticalAlignment.BOTTOM) {
                        min = Math.min(this.mLegend.mNeededHeight + getRequiredLegendOffset(), this.mViewPortHandler.getChartHeight() * this.mLegend.getMaxSizePercent());
                        switch (this.mLegend.getVerticalAlignment()) {
                            case TOP:
                                break;
                            case BOTTOM:
                                break;
                        }
                    }
                    break;
            }
        } else {
            f3 = 0.0f;
            f2 = 0.0f;
            f = 0.0f;
        }
        float convertDpToPixel2 = Utils.convertDpToPixel(this.mMinOffset);
        if (this instanceof RadarChart) {
            XAxis xAxis = getXAxis();
            if (xAxis.isEnabled() && xAxis.isDrawLabelsEnabled()) {
                convertDpToPixel2 = Math.max(convertDpToPixel2, (float) xAxis.mLabelRotatedWidth);
            }
        }
        float extraTopOffset = f + getExtraTopOffset();
        float extraRightOffset = f3 + getExtraRightOffset();
        float extraBottomOffset = f2 + getExtraBottomOffset();
        float max = Math.max(convertDpToPixel2, f5 + getExtraLeftOffset());
        float max2 = Math.max(convertDpToPixel2, extraTopOffset);
        float max3 = Math.max(convertDpToPixel2, extraRightOffset);
        float max4 = Math.max(convertDpToPixel2, Math.max(getRequiredBaseOffset(), extraBottomOffset));
        this.mViewPortHandler.restrainViewPort(max, max2, max3, max4);
        if (this.mLogEnabled) {
            String str = Chart.LOG_TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("offsetLeft: ");
            sb.append(max);
            sb.append(", offsetTop: ");
            sb.append(max2);
            sb.append(", offsetRight: ");
            sb.append(max3);
            sb.append(", offsetBottom: ");
            sb.append(max4);
            Log.i(str, sb.toString());
        }
    }

    public float getAngleForPoint(float f, float f2) {
        MPPointF centerOffsets = getCenterOffsets();
        double d = (double) (f - centerOffsets.x);
        double d2 = (double) (f2 - centerOffsets.y);
        Double.isNaN(d);
        Double.isNaN(d);
        double d3 = d * d;
        Double.isNaN(d2);
        Double.isNaN(d2);
        double sqrt = Math.sqrt(d3 + (d2 * d2));
        Double.isNaN(d2);
        float degrees = (float) Math.toDegrees(Math.acos(d2 / sqrt));
        if (f > centerOffsets.x) {
            degrees = 360.0f - degrees;
        }
        float f3 = degrees + 90.0f;
        if (f3 > 360.0f) {
            f3 -= 360.0f;
        }
        MPPointF.recycleInstance(centerOffsets);
        return f3;
    }

    public MPPointF getPosition(MPPointF mPPointF, float f, float f2) {
        MPPointF instance = MPPointF.getInstance(0.0f, 0.0f);
        getPosition(mPPointF, f, f2, instance);
        return instance;
    }

    public void getPosition(MPPointF mPPointF, float f, float f2, MPPointF mPPointF2) {
        double d = (double) mPPointF.x;
        double d2 = (double) f;
        double d3 = (double) f2;
        double cos = Math.cos(Math.toRadians(d3));
        Double.isNaN(d2);
        double d4 = cos * d2;
        Double.isNaN(d);
        mPPointF2.x = (float) (d + d4);
        double d5 = (double) mPPointF.y;
        double sin = Math.sin(Math.toRadians(d3));
        Double.isNaN(d2);
        double d6 = d2 * sin;
        Double.isNaN(d5);
        mPPointF2.y = (float) (d5 + d6);
    }

    public float distanceToCenter(float f, float f2) {
        float f3;
        float f4;
        MPPointF centerOffsets = getCenterOffsets();
        if (f > centerOffsets.x) {
            f3 = f - centerOffsets.x;
        } else {
            f3 = centerOffsets.x - f;
        }
        if (f2 > centerOffsets.y) {
            f4 = f2 - centerOffsets.y;
        } else {
            f4 = centerOffsets.y - f2;
        }
        float sqrt = (float) Math.sqrt(Math.pow((double) f3, 2.0d) + Math.pow((double) f4, 2.0d));
        MPPointF.recycleInstance(centerOffsets);
        return sqrt;
    }

    public void setRotationAngle(float f) {
        this.mRawRotationAngle = f;
        this.mRotationAngle = Utils.getNormalizedAngle(this.mRawRotationAngle);
    }

    public float getRawRotationAngle() {
        return this.mRawRotationAngle;
    }

    public float getRotationAngle() {
        return this.mRotationAngle;
    }

    public void setRotationEnabled(boolean z) {
        this.mRotateEnabled = z;
    }

    public boolean isRotationEnabled() {
        return this.mRotateEnabled;
    }

    public float getMinOffset() {
        return this.mMinOffset;
    }

    public void setMinOffset(float f) {
        this.mMinOffset = f;
    }

    public float getDiameter() {
        RectF contentRect = this.mViewPortHandler.getContentRect();
        contentRect.left += getExtraLeftOffset();
        contentRect.top += getExtraTopOffset();
        contentRect.right -= getExtraRightOffset();
        contentRect.bottom -= getExtraBottomOffset();
        return Math.min(contentRect.width(), contentRect.height());
    }

    @SuppressLint({"NewApi"})
    public void spin(int i, float f, float f2, EasingOption easingOption) {
        if (VERSION.SDK_INT >= 11) {
            setRotationAngle(f);
            ObjectAnimator ofFloat = ObjectAnimator.ofFloat(this, "rotationAngle", new float[]{f, f2});
            ofFloat.setDuration((long) i);
            ofFloat.setInterpolator(Easing.getEasingFunctionFromOption(easingOption));
            ofFloat.addUpdateListener(new AnimatorUpdateListener() {
                public void onAnimationUpdate(ValueAnimator valueAnimator) {
                    PieRadarChartBase.this.postInvalidate();
                }
            });
            ofFloat.start();
        }
    }
}
