package com.iab.omid.library.adcolony.b;

import android.annotation.SuppressLint;
import android.content.Context;

public class c {
    @SuppressLint({"StaticFieldLeak"})

    /* renamed from: a reason: collision with root package name */
    private static c f1956a = new c();
    private Context b;

    private c() {
    }

    public static c a() {
        return f1956a;
    }

    public void a(Context context) {
        this.b = context != null ? context.getApplicationContext() : null;
    }

    public Context b() {
        return this.b;
    }
}
