package com.iab.omid.library.adcolony;

import android.content.Context;

public final class Omid {

    /* renamed from: a reason: collision with root package name */
    private static a f1935a = new a();

    private Omid() {
    }

    public static boolean activateWithOmidApiVersion(String str, Context context) {
        f1935a.a(context.getApplicationContext());
        return true;
    }

    public static String getVersion() {
        return f1935a.a();
    }

    public static boolean isActive() {
        return f1935a.b();
    }

    public static boolean isCompatibleWithOmidApiVersion(String str) {
        return f1935a.a(str);
    }
}
