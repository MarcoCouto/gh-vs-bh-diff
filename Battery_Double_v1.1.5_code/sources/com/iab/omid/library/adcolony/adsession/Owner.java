package com.iab.omid.library.adcolony.adsession;

public enum Owner {
    NATIVE("native"),
    JAVASCRIPT("javascript"),
    NONE("none");
    

    /* renamed from: a reason: collision with root package name */
    private final String f1943a;

    private Owner(String str) {
        this.f1943a = str;
    }

    public String toString() {
        return this.f1943a;
    }
}
