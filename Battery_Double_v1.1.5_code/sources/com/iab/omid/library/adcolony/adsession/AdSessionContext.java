package com.iab.omid.library.adcolony.adsession;

import android.webkit.WebView;
import com.iab.omid.library.adcolony.d.e;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class AdSessionContext {

    /* renamed from: a reason: collision with root package name */
    private final Partner f1940a;
    private final WebView b;
    private final List<VerificationScriptResource> c = new ArrayList();
    private final String d;
    private final String e;
    private final AdSessionContextType f;

    private AdSessionContext(Partner partner, WebView webView, String str, List<VerificationScriptResource> list, String str2) {
        AdSessionContextType adSessionContextType;
        this.f1940a = partner;
        this.b = webView;
        this.d = str;
        if (list != null) {
            this.c.addAll(list);
            adSessionContextType = AdSessionContextType.NATIVE;
        } else {
            adSessionContextType = AdSessionContextType.HTML;
        }
        this.f = adSessionContextType;
        this.e = str2;
    }

    public static AdSessionContext createHtmlAdSessionContext(Partner partner, WebView webView, String str) {
        e.a((Object) partner, "Partner is null");
        e.a((Object) webView, "WebView is null");
        if (str != null) {
            e.a(str, 256, "CustomReferenceData is greater than 256 characters");
        }
        AdSessionContext adSessionContext = new AdSessionContext(partner, webView, null, null, str);
        return adSessionContext;
    }

    public static AdSessionContext createNativeAdSessionContext(Partner partner, String str, List<VerificationScriptResource> list, String str2) {
        e.a((Object) partner, "Partner is null");
        e.a((Object) str, "OM SDK JS script content is null");
        e.a((Object) list, "VerificationScriptResources is null");
        if (str2 != null) {
            e.a(str2, 256, "CustomReferenceData is greater than 256 characters");
        }
        AdSessionContext adSessionContext = new AdSessionContext(partner, null, str, list, str2);
        return adSessionContext;
    }

    public AdSessionContextType getAdSessionContextType() {
        return this.f;
    }

    public String getCustomReferenceData() {
        return this.e;
    }

    public String getOmidJsScriptContent() {
        return this.d;
    }

    public Partner getPartner() {
        return this.f1940a;
    }

    public List<VerificationScriptResource> getVerificationScriptResources() {
        return Collections.unmodifiableList(this.c);
    }

    public WebView getWebView() {
        return this.b;
    }
}
