package com.iab.omid.library.adcolony.adsession;

import com.tapjoy.TJAdUnitConstants.String;

public enum AdSessionContextType {
    HTML(String.HTML),
    NATIVE("native");
    

    /* renamed from: a reason: collision with root package name */
    private final String f1941a;

    private AdSessionContextType(String str) {
        this.f1941a = str;
    }

    public String toString() {
        return this.f1941a;
    }
}
