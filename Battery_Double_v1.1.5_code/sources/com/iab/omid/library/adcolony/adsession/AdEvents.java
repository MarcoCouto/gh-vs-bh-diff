package com.iab.omid.library.adcolony.adsession;

import com.iab.omid.library.adcolony.d.e;

public final class AdEvents {

    /* renamed from: a reason: collision with root package name */
    private final a f1938a;

    private AdEvents(a aVar) {
        this.f1938a = aVar;
    }

    public static AdEvents createAdEvents(AdSession adSession) {
        a aVar = (a) adSession;
        e.a((Object) adSession, "AdSession is null");
        e.d(aVar);
        e.b(aVar);
        AdEvents adEvents = new AdEvents(aVar);
        aVar.getAdSessionStatePublisher().a(adEvents);
        return adEvents;
    }

    public void impressionOccurred() {
        e.b(this.f1938a);
        e.f(this.f1938a);
        if (!this.f1938a.d()) {
            try {
                this.f1938a.start();
            } catch (Exception unused) {
            }
        }
        if (this.f1938a.d()) {
            this.f1938a.b();
        }
    }
}
