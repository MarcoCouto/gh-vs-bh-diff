package com.iab.omid.library.adcolony.adsession;

import com.facebook.share.internal.MessengerShareContentUtility;

public enum ErrorType {
    GENERIC(MessengerShareContentUtility.TEMPLATE_GENERIC_TYPE),
    VIDEO("video");
    

    /* renamed from: a reason: collision with root package name */
    private final String f1942a;

    private ErrorType(String str) {
        this.f1942a = str;
    }

    public String toString() {
        return this.f1942a;
    }
}
