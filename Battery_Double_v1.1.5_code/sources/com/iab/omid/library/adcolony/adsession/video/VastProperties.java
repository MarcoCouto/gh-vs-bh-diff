package com.iab.omid.library.adcolony.adsession.video;

import com.iab.omid.library.adcolony.d.c;
import com.iab.omid.library.adcolony.d.e;
import com.ironsource.sdk.constants.Constants.ParametersKeys;
import org.json.JSONException;
import org.json.JSONObject;

public final class VastProperties {

    /* renamed from: a reason: collision with root package name */
    private static final String f1950a = "VastProperties: ";
    private final boolean b;
    private final Float c;
    private final boolean d;
    private final Position e;

    private VastProperties(boolean z, Float f, boolean z2, Position position) {
        this.b = z;
        this.c = f;
        this.d = z2;
        this.e = position;
    }

    public static VastProperties createVastPropertiesForNonSkippableVideo(boolean z, Position position) {
        e.a((Object) position, "Position is null");
        return new VastProperties(false, null, z, position);
    }

    public static VastProperties createVastPropertiesForSkippableVideo(float f, boolean z, Position position) {
        e.a((Object) position, "Position is null");
        return new VastProperties(true, Float.valueOf(f), z, position);
    }

    /* access modifiers changed from: 0000 */
    public JSONObject a() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("skippable", this.b);
            if (this.b) {
                jSONObject.put("skipOffset", this.c);
            }
            jSONObject.put("autoPlay", this.d);
            jSONObject.put(ParametersKeys.POSITION, this.e);
        } catch (JSONException e2) {
            c.a("VastProperties: JSON error", e2);
        }
        return jSONObject;
    }
}
