package com.iab.omid.library.adcolony.adsession.video;

import com.iab.omid.library.adcolony.adsession.AdSession;
import com.iab.omid.library.adcolony.adsession.a;
import com.iab.omid.library.adcolony.d.b;
import com.iab.omid.library.adcolony.d.e;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.ironsource.sdk.constants.Constants.ParametersKeys;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.tapjoy.TJAdUnitConstants.String;
import org.json.JSONObject;

public final class VideoEvents {

    /* renamed from: a reason: collision with root package name */
    private final a f1951a;

    private VideoEvents(a aVar) {
        this.f1951a = aVar;
    }

    private void a(float f) {
        if (f <= 0.0f) {
            throw new IllegalArgumentException("Invalid Video duration");
        }
    }

    private void b(float f) {
        if (f < 0.0f || f > 1.0f) {
            throw new IllegalArgumentException("Invalid Video volume");
        }
    }

    public static VideoEvents createVideoEvents(AdSession adSession) {
        a aVar = (a) adSession;
        e.a((Object) adSession, "AdSession is null");
        e.g(aVar);
        e.a(aVar);
        e.b(aVar);
        e.e(aVar);
        VideoEvents videoEvents = new VideoEvents(aVar);
        aVar.getAdSessionStatePublisher().a(videoEvents);
        return videoEvents;
    }

    public void adUserInteraction(InteractionType interactionType) {
        e.a((Object) interactionType, "InteractionType is null");
        e.c(this.f1951a);
        JSONObject jSONObject = new JSONObject();
        b.a(jSONObject, "interactionType", interactionType);
        this.f1951a.getAdSessionStatePublisher().a("adUserInteraction", jSONObject);
    }

    public void bufferFinish() {
        e.c(this.f1951a);
        this.f1951a.getAdSessionStatePublisher().a("bufferFinish");
    }

    public void bufferStart() {
        e.c(this.f1951a);
        this.f1951a.getAdSessionStatePublisher().a(String.VIDEO_BUFFER_START);
    }

    public void complete() {
        e.c(this.f1951a);
        this.f1951a.getAdSessionStatePublisher().a("complete");
    }

    public void firstQuartile() {
        e.c(this.f1951a);
        this.f1951a.getAdSessionStatePublisher().a("firstQuartile");
    }

    public void loaded(VastProperties vastProperties) {
        e.a((Object) vastProperties, "VastProperties is null");
        e.b(this.f1951a);
        this.f1951a.getAdSessionStatePublisher().a(ParametersKeys.LOADED, vastProperties.a());
    }

    public void midpoint() {
        e.c(this.f1951a);
        this.f1951a.getAdSessionStatePublisher().a("midpoint");
    }

    public void pause() {
        e.c(this.f1951a);
        this.f1951a.getAdSessionStatePublisher().a(CampaignEx.JSON_NATIVE_VIDEO_PAUSE);
    }

    public void playerStateChange(PlayerState playerState) {
        e.a((Object) playerState, "PlayerState is null");
        e.c(this.f1951a);
        JSONObject jSONObject = new JSONObject();
        b.a(jSONObject, "state", playerState);
        this.f1951a.getAdSessionStatePublisher().a("playerStateChange", jSONObject);
    }

    public void resume() {
        e.c(this.f1951a);
        this.f1951a.getAdSessionStatePublisher().a(CampaignEx.JSON_NATIVE_VIDEO_RESUME);
    }

    public void skipped() {
        e.c(this.f1951a);
        this.f1951a.getAdSessionStatePublisher().a(String.VIDEO_SKIPPED);
    }

    public void start(float f, float f2) {
        a(f);
        b(f2);
        e.c(this.f1951a);
        JSONObject jSONObject = new JSONObject();
        b.a(jSONObject, IronSourceConstants.EVENTS_DURATION, Float.valueOf(f));
        b.a(jSONObject, "videoPlayerVolume", Float.valueOf(f2));
        b.a(jSONObject, RequestParameters.DEVICE_VOLUME, Float.valueOf(com.iab.omid.library.adcolony.b.e.a().d()));
        this.f1951a.getAdSessionStatePublisher().a("start", jSONObject);
    }

    public void thirdQuartile() {
        e.c(this.f1951a);
        this.f1951a.getAdSessionStatePublisher().a("thirdQuartile");
    }

    public void volumeChange(float f) {
        b(f);
        e.c(this.f1951a);
        JSONObject jSONObject = new JSONObject();
        b.a(jSONObject, "videoPlayerVolume", Float.valueOf(f));
        b.a(jSONObject, RequestParameters.DEVICE_VOLUME, Float.valueOf(com.iab.omid.library.adcolony.b.e.a().d()));
        this.f1951a.getAdSessionStatePublisher().a("volumeChange", jSONObject);
    }
}
