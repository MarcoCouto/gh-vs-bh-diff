package com.iab.omid.library.adcolony.adsession.video;

public enum InteractionType {
    CLICK("click"),
    INVITATION_ACCEPTED("invitationAccept");
    

    /* renamed from: a reason: collision with root package name */
    String f1947a;

    private InteractionType(String str) {
        this.f1947a = str;
    }

    public String toString() {
        return this.f1947a;
    }
}
