package com.iab.omid.library.adcolony.adsession.video;

import com.yandex.mobile.ads.video.tracking.Tracker.Events;

public enum PlayerState {
    MINIMIZED("minimized"),
    COLLAPSED("collapsed"),
    NORMAL("normal"),
    EXPANDED("expanded"),
    FULLSCREEN(Events.CREATIVE_FULLSCREEN);
    

    /* renamed from: a reason: collision with root package name */
    private final String f1948a;

    private PlayerState(String str) {
        this.f1948a = str;
    }

    public String toString() {
        return this.f1948a;
    }
}
