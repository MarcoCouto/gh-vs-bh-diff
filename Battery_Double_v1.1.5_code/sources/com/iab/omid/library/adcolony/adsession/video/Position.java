package com.iab.omid.library.adcolony.adsession.video;

import com.yandex.mobile.ads.video.models.vmap.AdBreak.BreakId;

public enum Position {
    PREROLL(BreakId.PREROLL),
    MIDROLL(BreakId.MIDROLL),
    POSTROLL("postroll"),
    STANDALONE("standalone");
    

    /* renamed from: a reason: collision with root package name */
    private final String f1949a;

    private Position(String str) {
        this.f1949a = str;
    }

    public String toString() {
        return this.f1949a;
    }
}
