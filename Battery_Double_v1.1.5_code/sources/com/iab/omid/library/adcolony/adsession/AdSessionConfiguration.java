package com.iab.omid.library.adcolony.adsession;

import com.iab.omid.library.adcolony.d.b;
import com.iab.omid.library.adcolony.d.e;
import org.json.JSONObject;

public class AdSessionConfiguration {

    /* renamed from: a reason: collision with root package name */
    private final Owner f1939a;
    private final Owner b;
    private final boolean c;

    private AdSessionConfiguration(Owner owner, Owner owner2, boolean z) {
        this.f1939a = owner;
        if (owner2 == null) {
            this.b = Owner.NONE;
        } else {
            this.b = owner2;
        }
        this.c = z;
    }

    @Deprecated
    public static AdSessionConfiguration createAdSessionConfiguration(Owner owner, Owner owner2) {
        return createAdSessionConfiguration(owner, owner2, true);
    }

    public static AdSessionConfiguration createAdSessionConfiguration(Owner owner, Owner owner2, boolean z) {
        e.a((Object) owner, "Impression owner is null");
        e.a(owner);
        return new AdSessionConfiguration(owner, owner2, z);
    }

    public boolean isNativeImpressionOwner() {
        return Owner.NATIVE == this.f1939a;
    }

    public boolean isNativeVideoEventsOwner() {
        return Owner.NATIVE == this.b;
    }

    public JSONObject toJsonObject() {
        JSONObject jSONObject = new JSONObject();
        b.a(jSONObject, "impressionOwner", this.f1939a);
        b.a(jSONObject, "videoEventsOwner", this.b);
        b.a(jSONObject, "isolateVerificationScripts", Boolean.valueOf(this.c));
        return jSONObject;
    }
}
