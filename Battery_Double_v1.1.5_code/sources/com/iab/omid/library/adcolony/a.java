package com.iab.omid.library.adcolony;

import android.content.Context;
import com.iab.omid.library.adcolony.b.b;
import com.iab.omid.library.adcolony.b.c;
import com.iab.omid.library.adcolony.d.e;

public class a {

    /* renamed from: a reason: collision with root package name */
    private boolean f1936a;

    private void b(Context context) {
        e.a((Object) context, "Application Context cannot be null");
    }

    /* access modifiers changed from: 0000 */
    public String a() {
        return "1.2.17-Adcolony";
    }

    /* access modifiers changed from: 0000 */
    public void a(Context context) {
        b(context);
        if (!b()) {
            a(true);
            com.iab.omid.library.adcolony.b.e.a().a(context);
            b.a().a(context);
            com.iab.omid.library.adcolony.d.b.a(context);
            c.a().a(context);
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(boolean z) {
        this.f1936a = z;
    }

    /* access modifiers changed from: 0000 */
    public boolean a(String str) {
        return true;
    }

    /* access modifiers changed from: 0000 */
    public boolean b() {
        return this.f1936a;
    }
}
