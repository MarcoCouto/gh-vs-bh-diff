package com.iab.omid.library.adcolony.walking.a;

import com.iab.omid.library.adcolony.walking.a.b.C0033b;
import java.util.HashSet;
import org.json.JSONObject;

public abstract class a extends b {

    /* renamed from: a reason: collision with root package name */
    protected final HashSet<String> f1972a;
    protected final JSONObject b;
    protected final long c;

    public a(C0033b bVar, HashSet<String> hashSet, JSONObject jSONObject, long j) {
        super(bVar);
        this.f1972a = new HashSet<>(hashSet);
        this.b = jSONObject;
        this.c = j;
    }
}
