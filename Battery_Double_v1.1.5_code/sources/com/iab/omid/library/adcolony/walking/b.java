package com.iab.omid.library.adcolony.walking;

import android.support.annotation.VisibleForTesting;
import com.iab.omid.library.adcolony.walking.a.b.C0033b;
import com.iab.omid.library.adcolony.walking.a.c;
import com.iab.omid.library.adcolony.walking.a.d;
import com.iab.omid.library.adcolony.walking.a.e;
import com.iab.omid.library.adcolony.walking.a.f;
import java.util.HashSet;
import org.json.JSONObject;

public class b implements C0033b {

    /* renamed from: a reason: collision with root package name */
    private JSONObject f1975a;
    private final c b;

    public b(c cVar) {
        this.b = cVar;
    }

    public void a() {
        this.b.b(new d(this));
    }

    @VisibleForTesting
    public void a(JSONObject jSONObject) {
        this.f1975a = jSONObject;
    }

    public void a(JSONObject jSONObject, HashSet<String> hashSet, long j) {
        c cVar = this.b;
        f fVar = new f(this, hashSet, jSONObject, j);
        cVar.b(fVar);
    }

    @VisibleForTesting
    public JSONObject b() {
        return this.f1975a;
    }

    public void b(JSONObject jSONObject, HashSet<String> hashSet, long j) {
        c cVar = this.b;
        e eVar = new e(this, hashSet, jSONObject, j);
        cVar.b(eVar);
    }
}
