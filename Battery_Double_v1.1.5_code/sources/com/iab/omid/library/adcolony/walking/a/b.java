package com.iab.omid.library.adcolony.walking.a;

import android.os.AsyncTask;
import java.util.concurrent.ThreadPoolExecutor;
import org.json.JSONObject;

public abstract class b extends AsyncTask<Object, Void, String> {

    /* renamed from: a reason: collision with root package name */
    private a f1973a;
    protected final C0033b d;

    public interface a {
        void a(b bVar);
    }

    /* renamed from: com.iab.omid.library.adcolony.walking.a.b$b reason: collision with other inner class name */
    public interface C0033b {
        void a(JSONObject jSONObject);

        JSONObject b();
    }

    public b(C0033b bVar) {
        this.d = bVar;
    }

    public void a(a aVar) {
        this.f1973a = aVar;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onPostExecute(String str) {
        if (this.f1973a != null) {
            this.f1973a.a(this);
        }
    }

    public void a(ThreadPoolExecutor threadPoolExecutor) {
        executeOnExecutor(threadPoolExecutor, new Object[0]);
    }
}
