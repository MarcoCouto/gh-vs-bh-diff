package com.iab.omid.library.adcolony.walking.a;

import com.iab.omid.library.adcolony.walking.a.b.a;
import java.util.ArrayDeque;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class c implements a {

    /* renamed from: a reason: collision with root package name */
    private final BlockingQueue<Runnable> f1974a = new LinkedBlockingQueue();
    private final ThreadPoolExecutor b;
    private final ArrayDeque<b> c = new ArrayDeque<>();
    private b d = null;

    public c() {
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(1, 1, 1, TimeUnit.SECONDS, this.f1974a);
        this.b = threadPoolExecutor;
    }

    private void a() {
        this.d = (b) this.c.poll();
        if (this.d != null) {
            this.d.a(this.b);
        }
    }

    public void a(b bVar) {
        this.d = null;
        a();
    }

    public void b(b bVar) {
        bVar.a((a) this);
        this.c.add(bVar);
        if (this.d == null) {
            a();
        }
    }
}
