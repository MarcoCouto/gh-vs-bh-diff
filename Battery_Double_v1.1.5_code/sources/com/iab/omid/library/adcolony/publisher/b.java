package com.iab.omid.library.adcolony.publisher;

import android.annotation.SuppressLint;
import android.os.Handler;
import android.webkit.WebView;
import com.iab.omid.library.adcolony.adsession.VerificationScriptResource;
import com.iab.omid.library.adcolony.b.c;
import com.iab.omid.library.adcolony.b.d;
import java.util.List;

public class b extends AdSessionStatePublisher {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public WebView f1967a;
    private List<VerificationScriptResource> b;
    private final String c;

    public b(List<VerificationScriptResource> list, String str) {
        this.b = list;
        this.c = str;
    }

    public void a() {
        super.a();
        i();
    }

    public void b() {
        super.b();
        new Handler().postDelayed(new Runnable() {
            private WebView b = b.this.f1967a;

            public void run() {
                this.b.destroy();
            }
        }, 2000);
        this.f1967a = null;
    }

    /* access modifiers changed from: 0000 */
    @SuppressLint({"SetJavaScriptEnabled"})
    public void i() {
        this.f1967a = new WebView(c.a().b());
        this.f1967a.getSettings().setJavaScriptEnabled(true);
        a(this.f1967a);
        d.a().a(this.f1967a, this.c);
        for (VerificationScriptResource resourceUrl : this.b) {
            d.a().b(this.f1967a, resourceUrl.getResourceUrl().toExternalForm());
        }
    }
}
