package com.iab.omid.library.oguryco.walking;

import android.view.View;
import android.view.ViewParent;
import com.iab.omid.library.oguryco.d.f;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;

public class a {

    /* renamed from: a reason: collision with root package name */
    private final HashMap<View, String> f1999a = new HashMap<>();
    private final HashMap<View, ArrayList<String>> b = new HashMap<>();
    private final HashSet<View> c = new HashSet<>();
    private final HashSet<String> d = new HashSet<>();
    private final HashSet<String> e = new HashSet<>();
    private boolean f;

    private void a(View view, com.iab.omid.library.oguryco.adsession.a aVar) {
        ArrayList arrayList = (ArrayList) this.b.get(view);
        if (arrayList == null) {
            arrayList = new ArrayList();
            this.b.put(view, arrayList);
        }
        arrayList.add(aVar.getAdSessionId());
    }

    private void a(com.iab.omid.library.oguryco.adsession.a aVar) {
        for (com.iab.omid.library.oguryco.e.a aVar2 : aVar.a()) {
            View view = (View) aVar2.get();
            if (view != null) {
                a(view, aVar);
            }
        }
    }

    private boolean d(View view) {
        if (!view.hasWindowFocus()) {
            return false;
        }
        HashSet hashSet = new HashSet();
        while (view != null) {
            if (!f.d(view)) {
                return false;
            }
            hashSet.add(view);
            ViewParent parent = view.getParent();
            view = parent instanceof View ? (View) parent : null;
        }
        this.c.addAll(hashSet);
        return true;
    }

    public String a(View view) {
        if (this.f1999a.size() == 0) {
            return null;
        }
        String str = (String) this.f1999a.get(view);
        if (str != null) {
            this.f1999a.remove(view);
        }
        return str;
    }

    public HashSet<String> a() {
        return this.d;
    }

    public ArrayList<String> b(View view) {
        if (this.b.size() == 0) {
            return null;
        }
        ArrayList<String> arrayList = (ArrayList) this.b.get(view);
        if (arrayList != null) {
            this.b.remove(view);
            Collections.sort(arrayList);
        }
        return arrayList;
    }

    public HashSet<String> b() {
        return this.e;
    }

    public c c(View view) {
        return this.c.contains(view) ? c.PARENT_VIEW : this.f ? c.OBSTRUCTION_VIEW : c.UNDERLYING_VIEW;
    }

    public void c() {
        com.iab.omid.library.oguryco.b.a a2 = com.iab.omid.library.oguryco.b.a.a();
        if (a2 != null) {
            for (com.iab.omid.library.oguryco.adsession.a aVar : a2.c()) {
                View c2 = aVar.c();
                if (aVar.d()) {
                    if (c2 == null || !d(c2)) {
                        this.e.add(aVar.getAdSessionId());
                    } else {
                        this.d.add(aVar.getAdSessionId());
                        this.f1999a.put(c2, aVar.getAdSessionId());
                        a(aVar);
                    }
                }
            }
        }
    }

    public void d() {
        this.f1999a.clear();
        this.b.clear();
        this.c.clear();
        this.d.clear();
        this.e.clear();
        this.f = false;
    }

    public void e() {
        this.f = true;
    }
}
