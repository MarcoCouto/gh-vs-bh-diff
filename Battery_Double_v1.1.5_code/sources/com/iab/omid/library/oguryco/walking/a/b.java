package com.iab.omid.library.oguryco.walking.a;

import android.os.AsyncTask;
import java.util.concurrent.ThreadPoolExecutor;
import org.json.JSONObject;

public abstract class b extends AsyncTask<Object, Void, String> {

    /* renamed from: a reason: collision with root package name */
    private a f2001a;
    protected final C0035b d;

    public interface a {
        void a(b bVar);
    }

    /* renamed from: com.iab.omid.library.oguryco.walking.a.b$b reason: collision with other inner class name */
    public interface C0035b {
        void a(JSONObject jSONObject);

        JSONObject b();
    }

    public b(C0035b bVar) {
        this.d = bVar;
    }

    public void a(a aVar) {
        this.f2001a = aVar;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onPostExecute(String str) {
        if (this.f2001a != null) {
            this.f2001a.a(this);
        }
    }

    public void a(ThreadPoolExecutor threadPoolExecutor) {
        executeOnExecutor(threadPoolExecutor, new Object[0]);
    }
}
