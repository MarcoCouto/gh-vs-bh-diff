package com.iab.omid.library.oguryco.walking.a;

import com.iab.omid.library.oguryco.walking.a.b.C0035b;
import java.util.HashSet;
import org.json.JSONObject;

public abstract class a extends b {

    /* renamed from: a reason: collision with root package name */
    protected final HashSet<String> f2000a;
    protected final JSONObject b;
    protected final double c;

    public a(C0035b bVar, HashSet<String> hashSet, JSONObject jSONObject, double d) {
        super(bVar);
        this.f2000a = new HashSet<>(hashSet);
        this.b = jSONObject;
        this.c = d;
    }
}
