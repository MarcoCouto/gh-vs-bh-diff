package com.iab.omid.library.oguryco.walking.a;

import com.iab.omid.library.oguryco.b.a;
import com.iab.omid.library.oguryco.walking.a.b.C0035b;
import java.util.HashSet;
import org.json.JSONObject;

public class e extends a {
    public e(C0035b bVar, HashSet<String> hashSet, JSONObject jSONObject, double d) {
        super(bVar, hashSet, jSONObject, d);
    }

    private void b(String str) {
        a a2 = a.a();
        if (a2 != null) {
            for (com.iab.omid.library.oguryco.adsession.a aVar : a2.b()) {
                if (this.f2000a.contains(aVar.getAdSessionId())) {
                    aVar.getAdSessionStatePublisher().b(str, this.c);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public String doInBackground(Object... objArr) {
        return this.b.toString();
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onPostExecute(String str) {
        b(str);
        super.onPostExecute(str);
    }
}
