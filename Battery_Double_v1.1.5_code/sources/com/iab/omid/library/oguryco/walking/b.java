package com.iab.omid.library.oguryco.walking;

import android.support.annotation.VisibleForTesting;
import com.iab.omid.library.oguryco.walking.a.b.C0035b;
import com.iab.omid.library.oguryco.walking.a.c;
import com.iab.omid.library.oguryco.walking.a.d;
import com.iab.omid.library.oguryco.walking.a.e;
import com.iab.omid.library.oguryco.walking.a.f;
import java.util.HashSet;
import org.json.JSONObject;

public class b implements C0035b {

    /* renamed from: a reason: collision with root package name */
    private JSONObject f2003a;
    private final c b;

    public b(c cVar) {
        this.b = cVar;
    }

    public void a() {
        this.b.b(new d(this));
    }

    @VisibleForTesting
    public void a(JSONObject jSONObject) {
        this.f2003a = jSONObject;
    }

    public void a(JSONObject jSONObject, HashSet<String> hashSet, double d) {
        c cVar = this.b;
        f fVar = new f(this, hashSet, jSONObject, d);
        cVar.b(fVar);
    }

    @VisibleForTesting
    public JSONObject b() {
        return this.f2003a;
    }

    public void b(JSONObject jSONObject, HashSet<String> hashSet, double d) {
        c cVar = this.b;
        e eVar = new e(this, hashSet, jSONObject, d);
        cVar.b(eVar);
    }
}
