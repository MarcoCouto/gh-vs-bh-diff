package com.iab.omid.library.oguryco;

import android.content.Context;
import com.iab.omid.library.oguryco.b.b;
import com.iab.omid.library.oguryco.b.c;
import com.iab.omid.library.oguryco.d.e;

public class a {

    /* renamed from: a reason: collision with root package name */
    private boolean f1977a;

    private void b(Context context) {
        e.a((Object) context, "Application Context cannot be null");
    }

    /* access modifiers changed from: 0000 */
    public String a() {
        return "1.2.8-Oguryco";
    }

    /* access modifiers changed from: 0000 */
    public void a(Context context) {
        b(context);
        if (!b()) {
            a(true);
            com.iab.omid.library.oguryco.b.e.a().a(context);
            b.a().a(context);
            com.iab.omid.library.oguryco.d.b.a(context);
            c.a().a(context);
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(boolean z) {
        this.f1977a = z;
    }

    /* access modifiers changed from: 0000 */
    public boolean a(String str) {
        return true;
    }

    /* access modifiers changed from: 0000 */
    public boolean b() {
        return this.f1977a;
    }
}
