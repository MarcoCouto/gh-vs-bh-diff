package com.iab.omid.library.oguryco.c;

import android.support.annotation.NonNull;
import android.view.View;
import com.iab.omid.library.oguryco.b.a;
import com.iab.omid.library.oguryco.c.a.C0034a;
import com.iab.omid.library.oguryco.d.b;
import com.iab.omid.library.oguryco.d.f;
import java.util.ArrayList;
import java.util.Collection;
import java.util.IdentityHashMap;
import java.util.Iterator;
import org.json.JSONObject;

public class c implements a {

    /* renamed from: a reason: collision with root package name */
    private final a f1989a;

    public c(a aVar) {
        this.f1989a = aVar;
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public ArrayList<View> a() {
        ArrayList<View> arrayList = new ArrayList<>();
        a a2 = a.a();
        if (a2 != null) {
            Collection<com.iab.omid.library.oguryco.adsession.a> c = a2.c();
            IdentityHashMap identityHashMap = new IdentityHashMap((c.size() * 2) + 3);
            for (com.iab.omid.library.oguryco.adsession.a c2 : c) {
                View c3 = c2.c();
                if (c3 != null && f.c(c3)) {
                    View rootView = c3.getRootView();
                    if (rootView != null && !identityHashMap.containsKey(rootView)) {
                        identityHashMap.put(rootView, rootView);
                        float a3 = f.a(rootView);
                        int size = arrayList.size();
                        while (size > 0 && f.a((View) arrayList.get(size - 1)) > a3) {
                            size--;
                        }
                        arrayList.add(size, rootView);
                    }
                }
            }
        }
        return arrayList;
    }

    public JSONObject a(View view) {
        return b.a(0, 0, 0, 0);
    }

    public void a(View view, JSONObject jSONObject, C0034a aVar, boolean z) {
        Iterator it = a().iterator();
        while (it.hasNext()) {
            aVar.a((View) it.next(), this.f1989a, jSONObject);
        }
    }
}
