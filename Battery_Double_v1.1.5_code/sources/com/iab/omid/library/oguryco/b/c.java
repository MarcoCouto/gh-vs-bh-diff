package com.iab.omid.library.oguryco.b;

import android.annotation.SuppressLint;
import android.content.Context;

public class c {
    @SuppressLint({"StaticFieldLeak"})

    /* renamed from: a reason: collision with root package name */
    private static c f1984a = new c();
    private Context b;

    private c() {
    }

    public static c a() {
        return f1984a;
    }

    public void a(Context context) {
        this.b = context != null ? context.getApplicationContext() : null;
    }

    public Context b() {
        return this.b;
    }
}
