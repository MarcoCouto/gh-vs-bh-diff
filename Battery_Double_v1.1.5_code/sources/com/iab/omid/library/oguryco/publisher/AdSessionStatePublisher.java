package com.iab.omid.library.oguryco.publisher;

import android.webkit.WebView;
import com.iab.omid.library.oguryco.adsession.AdEvents;
import com.iab.omid.library.oguryco.adsession.AdSessionConfiguration;
import com.iab.omid.library.oguryco.adsession.AdSessionContext;
import com.iab.omid.library.oguryco.adsession.ErrorType;
import com.iab.omid.library.oguryco.adsession.VerificationScriptResource;
import com.iab.omid.library.oguryco.adsession.video.VideoEvents;
import com.iab.omid.library.oguryco.b.c;
import com.iab.omid.library.oguryco.b.d;
import com.iab.omid.library.oguryco.e.b;
import org.json.JSONArray;
import org.json.JSONObject;

public abstract class AdSessionStatePublisher {

    /* renamed from: a reason: collision with root package name */
    private b f1993a = new b(null);
    private AdEvents b;
    private VideoEvents c;
    private a d;
    private double e;

    enum a {
        AD_STATE_IDLE,
        AD_STATE_VISIBLE,
        AD_STATE_HIDDEN
    }

    public AdSessionStatePublisher() {
        h();
    }

    public void a() {
    }

    public void a(float f) {
        d.a().a(getWebView(), f);
    }

    /* access modifiers changed from: 0000 */
    public void a(WebView webView) {
        this.f1993a = new b(webView);
    }

    public void a(AdEvents adEvents) {
        this.b = adEvents;
    }

    public void a(AdSessionConfiguration adSessionConfiguration) {
        d.a().a(getWebView(), adSessionConfiguration.toJsonObject());
    }

    public void a(ErrorType errorType, String str) {
        d.a().a(getWebView(), errorType, str);
    }

    public void a(com.iab.omid.library.oguryco.adsession.a aVar, AdSessionContext adSessionContext) {
        String adSessionId = aVar.getAdSessionId();
        JSONObject jSONObject = new JSONObject();
        com.iab.omid.library.oguryco.d.b.a(jSONObject, "environment", "app");
        com.iab.omid.library.oguryco.d.b.a(jSONObject, "adSessionType", adSessionContext.getAdSessionContextType());
        com.iab.omid.library.oguryco.d.b.a(jSONObject, "deviceInfo", com.iab.omid.library.oguryco.d.a.d());
        JSONArray jSONArray = new JSONArray();
        jSONArray.put("clid");
        jSONArray.put("vlid");
        com.iab.omid.library.oguryco.d.b.a(jSONObject, "supports", jSONArray);
        JSONObject jSONObject2 = new JSONObject();
        com.iab.omid.library.oguryco.d.b.a(jSONObject2, "partnerName", adSessionContext.getPartner().getName());
        com.iab.omid.library.oguryco.d.b.a(jSONObject2, "partnerVersion", adSessionContext.getPartner().getVersion());
        com.iab.omid.library.oguryco.d.b.a(jSONObject, "omidNativeInfo", jSONObject2);
        JSONObject jSONObject3 = new JSONObject();
        com.iab.omid.library.oguryco.d.b.a(jSONObject3, "libraryVersion", "1.2.8-Oguryco");
        com.iab.omid.library.oguryco.d.b.a(jSONObject3, "appId", c.a().b().getApplicationContext().getPackageName());
        com.iab.omid.library.oguryco.d.b.a(jSONObject, "app", jSONObject3);
        if (adSessionContext.getCustomReferenceData() != null) {
            com.iab.omid.library.oguryco.d.b.a(jSONObject, "customReferenceData", adSessionContext.getCustomReferenceData());
        }
        JSONObject jSONObject4 = new JSONObject();
        for (VerificationScriptResource verificationScriptResource : adSessionContext.getVerificationScriptResources()) {
            com.iab.omid.library.oguryco.d.b.a(jSONObject4, verificationScriptResource.getVendorKey(), verificationScriptResource.getVerificationParameters());
        }
        d.a().a(getWebView(), adSessionId, jSONObject, jSONObject4);
    }

    public void a(VideoEvents videoEvents) {
        this.c = videoEvents;
    }

    public void a(String str) {
        d.a().a(getWebView(), str, (JSONObject) null);
    }

    public void a(String str, double d2) {
        if (d2 > this.e) {
            this.d = a.AD_STATE_VISIBLE;
            d.a().c(getWebView(), str);
        }
    }

    public void a(String str, JSONObject jSONObject) {
        d.a().a(getWebView(), str, jSONObject);
    }

    public void a(boolean z) {
        if (e()) {
            d.a().d(getWebView(), z ? "foregrounded" : "backgrounded");
        }
    }

    public void b() {
        this.f1993a.clear();
    }

    public void b(String str, double d2) {
        if (d2 > this.e && this.d != a.AD_STATE_HIDDEN) {
            this.d = a.AD_STATE_HIDDEN;
            d.a().c(getWebView(), str);
        }
    }

    public AdEvents c() {
        return this.b;
    }

    public VideoEvents d() {
        return this.c;
    }

    public boolean e() {
        return this.f1993a.get() != null;
    }

    public void f() {
        d.a().a(getWebView());
    }

    public void g() {
        d.a().b(getWebView());
    }

    public WebView getWebView() {
        return (WebView) this.f1993a.get();
    }

    public void h() {
        this.e = com.iab.omid.library.oguryco.d.d.a();
        this.d = a.AD_STATE_IDLE;
    }
}
