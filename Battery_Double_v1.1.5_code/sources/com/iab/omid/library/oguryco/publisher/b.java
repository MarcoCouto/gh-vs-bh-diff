package com.iab.omid.library.oguryco.publisher;

import android.annotation.SuppressLint;
import android.os.Handler;
import android.webkit.WebView;
import com.iab.omid.library.oguryco.adsession.VerificationScriptResource;
import com.iab.omid.library.oguryco.b.c;
import com.iab.omid.library.oguryco.b.d;
import java.util.List;

public class b extends AdSessionStatePublisher {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public WebView f1995a;
    private List<VerificationScriptResource> b;
    private final String c;

    public b(List<VerificationScriptResource> list, String str) {
        this.b = list;
        this.c = str;
    }

    public void a() {
        super.a();
        i();
    }

    public void b() {
        super.b();
        new Handler().postDelayed(new Runnable() {
            private WebView b = b.this.f1995a;

            public void run() {
                this.b.destroy();
            }
        }, 2000);
        this.f1995a = null;
    }

    /* access modifiers changed from: 0000 */
    @SuppressLint({"SetJavaScriptEnabled"})
    public void i() {
        this.f1995a = new WebView(c.a().b());
        this.f1995a.getSettings().setJavaScriptEnabled(true);
        a(this.f1995a);
        d.a().a(this.f1995a, this.c);
        for (VerificationScriptResource resourceUrl : this.b) {
            d.a().b(this.f1995a, resourceUrl.getResourceUrl().toExternalForm());
        }
    }
}
