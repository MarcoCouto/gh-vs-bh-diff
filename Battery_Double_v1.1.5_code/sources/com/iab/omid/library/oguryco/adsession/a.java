package com.iab.omid.library.oguryco.adsession;

import android.view.View;
import com.iab.omid.library.oguryco.d.e;
import com.iab.omid.library.oguryco.publisher.AdSessionStatePublisher;
import com.iab.omid.library.oguryco.publisher.b;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

public class a extends AdSession {

    /* renamed from: a reason: collision with root package name */
    private final AdSessionContext f1979a;
    private final AdSessionConfiguration b;
    private final List<com.iab.omid.library.oguryco.e.a> c = new ArrayList();
    private com.iab.omid.library.oguryco.e.a d;
    private AdSessionStatePublisher e;
    private boolean f = false;
    private boolean g = false;
    private String h;
    private boolean i;

    a(AdSessionConfiguration adSessionConfiguration, AdSessionContext adSessionContext) {
        this.b = adSessionConfiguration;
        this.f1979a = adSessionContext;
        this.h = UUID.randomUUID().toString();
        c(null);
        this.e = adSessionContext.getAdSessionContextType() == AdSessionContextType.HTML ? new com.iab.omid.library.oguryco.publisher.a(adSessionContext.getWebView()) : new b(adSessionContext.getVerificationScriptResources(), adSessionContext.getOmidJsScriptContent());
        this.e.a();
        com.iab.omid.library.oguryco.b.a.a().a(this);
        this.e.a(adSessionConfiguration);
    }

    private com.iab.omid.library.oguryco.e.a a(View view) {
        for (com.iab.omid.library.oguryco.e.a aVar : this.c) {
            if (aVar.get() == view) {
                return aVar;
            }
        }
        return null;
    }

    private void b(View view) {
        if (view == null) {
            throw new IllegalArgumentException("FriendlyObstruction is null");
        }
    }

    private void c(View view) {
        this.d = new com.iab.omid.library.oguryco.e.a(view);
    }

    private void d(View view) {
        Collection<a> b2 = com.iab.omid.library.oguryco.b.a.a().b();
        if (b2 != null && b2.size() > 0) {
            for (a aVar : b2) {
                if (aVar != this && aVar.c() == view) {
                    aVar.d.clear();
                }
            }
        }
    }

    private void i() {
        if (this.i) {
            throw new IllegalStateException("Impression event can only be sent once");
        }
    }

    public List<com.iab.omid.library.oguryco.e.a> a() {
        return this.c;
    }

    public void addFriendlyObstruction(View view) {
        if (!this.g) {
            b(view);
            if (a(view) == null) {
                this.c.add(new com.iab.omid.library.oguryco.e.a(view));
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void b() {
        i();
        getAdSessionStatePublisher().g();
        this.i = true;
    }

    public View c() {
        return (View) this.d.get();
    }

    public boolean d() {
        return this.f && !this.g;
    }

    public boolean e() {
        return this.f;
    }

    public void error(ErrorType errorType, String str) {
        if (!this.g) {
            e.a((Object) errorType, "Error type is null");
            e.a(str, "Message is null");
            getAdSessionStatePublisher().a(errorType, str);
            return;
        }
        throw new IllegalStateException("AdSession is finished");
    }

    public boolean f() {
        return this.g;
    }

    public void finish() {
        if (!this.g) {
            this.d.clear();
            removeAllFriendlyObstructions();
            this.g = true;
            getAdSessionStatePublisher().f();
            com.iab.omid.library.oguryco.b.a.a().c(this);
            getAdSessionStatePublisher().b();
            this.e = null;
        }
    }

    public boolean g() {
        return this.b.isNativeImpressionOwner();
    }

    public String getAdSessionId() {
        return this.h;
    }

    public AdSessionStatePublisher getAdSessionStatePublisher() {
        return this.e;
    }

    public boolean h() {
        return this.b.isNativeVideoEventsOwner();
    }

    public void registerAdView(View view) {
        if (!this.g) {
            e.a((Object) view, "AdView is null");
            if (c() != view) {
                c(view);
                getAdSessionStatePublisher().h();
                d(view);
            }
        }
    }

    public void removeAllFriendlyObstructions() {
        if (!this.g) {
            this.c.clear();
        }
    }

    public void removeFriendlyObstruction(View view) {
        if (!this.g) {
            b(view);
            com.iab.omid.library.oguryco.e.a a2 = a(view);
            if (a2 != null) {
                this.c.remove(a2);
            }
        }
    }

    public void start() {
        if (!this.f) {
            this.f = true;
            com.iab.omid.library.oguryco.b.a.a().b(this);
            this.e.a(com.iab.omid.library.oguryco.b.e.a().d());
            this.e.a(this, this.f1979a);
        }
    }
}
