package com.iab.omid.library.smaato.publisher;

import android.annotation.SuppressLint;
import android.os.Handler;
import android.webkit.WebView;
import com.iab.omid.library.smaato.adsession.VerificationScriptResource;
import com.iab.omid.library.smaato.b.c;
import com.iab.omid.library.smaato.b.d;
import java.util.List;

public class b extends AdSessionStatePublisher {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public WebView f2023a;
    private List<VerificationScriptResource> b;
    private final String c;

    public b(List<VerificationScriptResource> list, String str) {
        this.b = list;
        this.c = str;
    }

    public void a() {
        super.a();
        i();
    }

    public void b() {
        super.b();
        new Handler().postDelayed(new Runnable() {
            private WebView b = b.this.f2023a;

            public void run() {
                this.b.destroy();
            }
        }, 2000);
        this.f2023a = null;
    }

    /* access modifiers changed from: 0000 */
    @SuppressLint({"SetJavaScriptEnabled"})
    public void i() {
        this.f2023a = new WebView(c.a().b());
        this.f2023a.getSettings().setJavaScriptEnabled(true);
        a(this.f2023a);
        d.a().a(this.f2023a, this.c);
        for (VerificationScriptResource resourceUrl : this.b) {
            d.a().b(this.f2023a, resourceUrl.getResourceUrl().toExternalForm());
        }
    }
}
