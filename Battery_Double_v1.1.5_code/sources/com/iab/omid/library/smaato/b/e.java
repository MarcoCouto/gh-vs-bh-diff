package com.iab.omid.library.smaato.b;

import android.content.Context;
import android.os.Handler;
import com.iab.omid.library.smaato.a.b;
import com.iab.omid.library.smaato.a.c;
import com.iab.omid.library.smaato.a.d;
import com.iab.omid.library.smaato.b.b.a;
import com.iab.omid.library.smaato.walking.TreeWalker;

public class e implements c, a {

    /* renamed from: a reason: collision with root package name */
    private static e f2015a;
    private float b = 0.0f;
    private final com.iab.omid.library.smaato.a.e c;
    private final b d;
    private d e;
    private a f;

    public e(com.iab.omid.library.smaato.a.e eVar, b bVar) {
        this.c = eVar;
        this.d = bVar;
    }

    public static e a() {
        if (f2015a == null) {
            f2015a = new e(new com.iab.omid.library.smaato.a.e(), new b());
        }
        return f2015a;
    }

    private a e() {
        if (this.f == null) {
            this.f = a.a();
        }
        return this.f;
    }

    public void a(float f2) {
        this.b = f2;
        for (com.iab.omid.library.smaato.adsession.a adSessionStatePublisher : e().c()) {
            adSessionStatePublisher.getAdSessionStatePublisher().a(f2);
        }
    }

    public void a(Context context) {
        this.e = this.c.a(new Handler(), context, this.d.a(), this);
    }

    public void a(boolean z) {
        if (z) {
            TreeWalker.getInstance().a();
        } else {
            TreeWalker.getInstance().c();
        }
    }

    public void b() {
        b.a().a((a) this);
        b.a().b();
        if (b.a().d()) {
            TreeWalker.getInstance().a();
        }
        this.e.a();
    }

    public void c() {
        TreeWalker.getInstance().b();
        b.a().c();
        this.e.b();
    }

    public float d() {
        return this.b;
    }
}
