package com.iab.omid.library.smaato.a;

import android.content.Context;
import android.database.ContentObserver;
import android.media.AudioManager;
import android.os.Handler;
import android.provider.Settings.System;
import com.google.android.exoplayer2.util.MimeTypes;

public final class d extends ContentObserver {

    /* renamed from: a reason: collision with root package name */
    private final Context f2006a;
    private final AudioManager b;
    private final a c;
    private final c d;
    private float e;

    public d(Handler handler, Context context, a aVar, c cVar) {
        super(handler);
        this.f2006a = context;
        this.b = (AudioManager) context.getSystemService(MimeTypes.BASE_TYPE_AUDIO);
        this.c = aVar;
        this.d = cVar;
    }

    private boolean a(float f) {
        return f != this.e;
    }

    private float c() {
        return this.c.a(this.b.getStreamVolume(3), this.b.getStreamMaxVolume(3));
    }

    private void d() {
        this.d.a(this.e);
    }

    public final void a() {
        this.e = c();
        d();
        this.f2006a.getContentResolver().registerContentObserver(System.CONTENT_URI, true, this);
    }

    public final void b() {
        this.f2006a.getContentResolver().unregisterContentObserver(this);
    }

    public final void onChange(boolean z) {
        super.onChange(z);
        float c2 = c();
        if (a(c2)) {
            this.e = c2;
            d();
        }
    }
}
