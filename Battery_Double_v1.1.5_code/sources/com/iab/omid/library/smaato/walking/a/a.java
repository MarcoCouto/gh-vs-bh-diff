package com.iab.omid.library.smaato.walking.a;

import com.iab.omid.library.smaato.walking.a.b.C0037b;
import java.util.HashSet;
import org.json.JSONObject;

public abstract class a extends b {

    /* renamed from: a reason: collision with root package name */
    protected final HashSet<String> f2028a;
    protected final JSONObject b;
    protected final double c;

    public a(C0037b bVar, HashSet<String> hashSet, JSONObject jSONObject, double d) {
        super(bVar);
        this.f2028a = new HashSet<>(hashSet);
        this.b = jSONObject;
        this.c = d;
    }
}
