package com.iab.omid.library.smaato.walking.a;

import android.text.TextUtils;
import com.iab.omid.library.smaato.b.a;
import com.iab.omid.library.smaato.d.b;
import com.iab.omid.library.smaato.walking.a.b.C0037b;
import java.util.HashSet;
import org.json.JSONObject;

public class f extends a {
    public f(C0037b bVar, HashSet<String> hashSet, JSONObject jSONObject, double d) {
        super(bVar, hashSet, jSONObject, d);
    }

    private void b(String str) {
        a a2 = a.a();
        if (a2 != null) {
            for (com.iab.omid.library.smaato.adsession.a aVar : a2.b()) {
                if (this.f2028a.contains(aVar.getAdSessionId())) {
                    aVar.getAdSessionStatePublisher().a(str, this.c);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public String doInBackground(Object... objArr) {
        if (b.b(this.b, this.d.b())) {
            return null;
        }
        this.d.a(this.b);
        return this.b.toString();
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onPostExecute(String str) {
        if (!TextUtils.isEmpty(str)) {
            b(str);
        }
        super.onPostExecute(str);
    }
}
