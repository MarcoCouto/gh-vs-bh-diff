package com.iab.omid.library.smaato.walking;

import android.support.annotation.VisibleForTesting;
import com.iab.omid.library.smaato.walking.a.b.C0037b;
import com.iab.omid.library.smaato.walking.a.c;
import com.iab.omid.library.smaato.walking.a.d;
import com.iab.omid.library.smaato.walking.a.e;
import com.iab.omid.library.smaato.walking.a.f;
import java.util.HashSet;
import org.json.JSONObject;

public class b implements C0037b {

    /* renamed from: a reason: collision with root package name */
    private JSONObject f2031a;
    private final c b;

    public b(c cVar) {
        this.b = cVar;
    }

    public void a() {
        this.b.b(new d(this));
    }

    @VisibleForTesting
    public void a(JSONObject jSONObject) {
        this.f2031a = jSONObject;
    }

    public void a(JSONObject jSONObject, HashSet<String> hashSet, double d) {
        c cVar = this.b;
        f fVar = new f(this, hashSet, jSONObject, d);
        cVar.b(fVar);
    }

    @VisibleForTesting
    public JSONObject b() {
        return this.f2031a;
    }

    public void b(JSONObject jSONObject, HashSet<String> hashSet, double d) {
        c cVar = this.b;
        e eVar = new e(this, hashSet, jSONObject, d);
        cVar.b(eVar);
    }
}
