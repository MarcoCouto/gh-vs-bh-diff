package com.iab.omid.library.smaato.walking.a;

import com.iab.omid.library.smaato.b.a;
import com.iab.omid.library.smaato.walking.a.b.C0037b;
import java.util.HashSet;
import org.json.JSONObject;

public class e extends a {
    public e(C0037b bVar, HashSet<String> hashSet, JSONObject jSONObject, double d) {
        super(bVar, hashSet, jSONObject, d);
    }

    private void b(String str) {
        a a2 = a.a();
        if (a2 != null) {
            for (com.iab.omid.library.smaato.adsession.a aVar : a2.b()) {
                if (this.f2028a.contains(aVar.getAdSessionId())) {
                    aVar.getAdSessionStatePublisher().b(str, this.c);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public String doInBackground(Object... objArr) {
        return this.b.toString();
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onPostExecute(String str) {
        b(str);
        super.onPostExecute(str);
    }
}
