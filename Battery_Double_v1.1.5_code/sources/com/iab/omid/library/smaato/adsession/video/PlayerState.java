package com.iab.omid.library.smaato.adsession.video;

import com.yandex.mobile.ads.video.tracking.Tracker.Events;

public enum PlayerState {
    MINIMIZED("minimized"),
    COLLAPSED("collapsed"),
    NORMAL("normal"),
    EXPANDED("expanded"),
    FULLSCREEN(Events.CREATIVE_FULLSCREEN);
    
    private final String playerState;

    private PlayerState(String str) {
        this.playerState = str;
    }

    public final String toString() {
        return this.playerState;
    }
}
