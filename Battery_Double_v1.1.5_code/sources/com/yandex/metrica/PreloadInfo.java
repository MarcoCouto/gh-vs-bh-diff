package com.yandex.metrica;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class PreloadInfo {

    /* renamed from: a reason: collision with root package name */
    private String f4600a;
    private Map<String, String> b;

    public static class Builder {
        /* access modifiers changed from: private */

        /* renamed from: a reason: collision with root package name */
        public String f4601a;
        /* access modifiers changed from: private */
        public Map<String, String> b;

        private Builder(String str) {
            this.f4601a = str;
            this.b = new HashMap();
        }

        public Builder setAdditionalParams(String str, String str2) {
            if (!(str == null || str2 == null)) {
                this.b.put(str, str2);
            }
            return this;
        }

        public PreloadInfo build() {
            return new PreloadInfo(this);
        }
    }

    private PreloadInfo(Builder builder) {
        this.f4600a = builder.f4601a;
        this.b = Collections.unmodifiableMap(builder.b);
    }

    public static Builder newBuilder(String str) {
        return new Builder(str);
    }

    public String getTrackingId() {
        return this.f4600a;
    }

    public Map<String, String> getAdditionalParams() {
        return this.b;
    }
}
