package com.yandex.metrica;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.metrica.impl.ob.cx;
import com.yandex.metrica.impl.ob.yg;
import com.yandex.metrica.impl.ob.yk;
import com.yandex.metrica.impl.ob.yl;

public class ReporterConfig {
    @NonNull
    public final String apiKey;
    @Nullable
    public final Boolean logs;
    @Nullable
    public final Integer maxReportsInDatabaseCount;
    @Nullable
    public final Integer sessionTimeout;
    @Nullable
    public final Boolean statisticsSending;

    public static class Builder {

        /* renamed from: a reason: collision with root package name */
        private static final yk<String> f4602a = new yg(new yl());
        /* access modifiers changed from: private */
        public final String b;
        /* access modifiers changed from: private */
        @Nullable
        public Integer c;
        /* access modifiers changed from: private */
        @Nullable
        public Boolean d;
        /* access modifiers changed from: private */
        @Nullable
        public Boolean e;
        /* access modifiers changed from: private */
        @Nullable
        public Integer f;

        Builder(@NonNull String str) {
            f4602a.a(str);
            this.b = str;
        }

        @NonNull
        public Builder withSessionTimeout(int i) {
            this.c = Integer.valueOf(i);
            return this;
        }

        @NonNull
        public Builder withLogs() {
            this.d = Boolean.valueOf(true);
            return this;
        }

        @NonNull
        public Builder withStatisticsSending(boolean z) {
            this.e = Boolean.valueOf(z);
            return this;
        }

        @NonNull
        public Builder withMaxReportsInDatabaseCount(int i) {
            this.f = Integer.valueOf(i);
            return this;
        }

        @NonNull
        public ReporterConfig build() {
            return new ReporterConfig(this);
        }
    }

    ReporterConfig(@NonNull Builder builder) {
        this.apiKey = builder.b;
        this.sessionTimeout = builder.c;
        this.logs = builder.d;
        this.statisticsSending = builder.e;
        this.maxReportsInDatabaseCount = builder.f;
    }

    ReporterConfig(@NonNull ReporterConfig reporterConfig) {
        this.apiKey = reporterConfig.apiKey;
        this.sessionTimeout = reporterConfig.sessionTimeout;
        this.logs = reporterConfig.logs;
        this.statisticsSending = reporterConfig.statisticsSending;
        this.maxReportsInDatabaseCount = reporterConfig.maxReportsInDatabaseCount;
    }

    public static Builder createBuilderFromConfig(@NonNull ReporterConfig reporterConfig) {
        Builder newConfigBuilder = newConfigBuilder(reporterConfig.apiKey);
        if (cx.a((Object) reporterConfig.sessionTimeout)) {
            newConfigBuilder.withSessionTimeout(reporterConfig.sessionTimeout.intValue());
        }
        if (cx.a((Object) reporterConfig.logs) && reporterConfig.logs.booleanValue()) {
            newConfigBuilder.withLogs();
        }
        if (cx.a((Object) reporterConfig.statisticsSending)) {
            newConfigBuilder.withStatisticsSending(reporterConfig.statisticsSending.booleanValue());
        }
        if (cx.a((Object) reporterConfig.maxReportsInDatabaseCount)) {
            newConfigBuilder.withMaxReportsInDatabaseCount(reporterConfig.maxReportsInDatabaseCount.intValue());
        }
        return newConfigBuilder;
    }

    @NonNull
    public static Builder newConfigBuilder(@NonNull String str) {
        return new Builder(str);
    }
}
