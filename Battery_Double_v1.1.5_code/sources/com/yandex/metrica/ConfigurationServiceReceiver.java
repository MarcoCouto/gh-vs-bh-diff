package com.yandex.metrica;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.yandex.metrica.impl.ob.cx;
import com.yandex.metrica.impl.ob.jl;

public class ConfigurationServiceReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        if (intent != null && "com.yandex.metrica.configuration.service.PLC".equals(intent.getAction()) && cx.a(26)) {
            new jl(context).b(intent.getExtras());
        }
    }
}
