package com.yandex.metrica;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.metrica.impl.ob.yf;
import com.yandex.metrica.impl.ob.yg;
import com.yandex.metrica.impl.ob.yk;
import java.util.Currency;

public class Revenue {
    @NonNull
    public final Currency currency;
    @Nullable
    public final String payload;
    @Nullable
    @Deprecated
    public final Double price;
    @Nullable
    public final Long priceMicros;
    @Nullable
    public final String productID;
    @Nullable
    public final Integer quantity;
    @Nullable
    public final Receipt receipt;

    public static class Builder {
        private static final yk<Currency> h = new yg(new yf("revenue currency"));
        @Nullable

        /* renamed from: a reason: collision with root package name */
        Double f4603a;
        @Nullable
        Long b;
        @NonNull
        Currency c;
        @Nullable
        Integer d;
        @Nullable
        String e;
        @Nullable
        String f;
        @Nullable
        Receipt g;

        Builder(double d2, @NonNull Currency currency) {
            h.a(currency);
            this.f4603a = Double.valueOf(d2);
            this.c = currency;
        }

        Builder(long j, @NonNull Currency currency) {
            h.a(currency);
            this.b = Long.valueOf(j);
            this.c = currency;
        }

        @NonNull
        public Builder withQuantity(@Nullable Integer num) {
            this.d = num;
            return this;
        }

        @NonNull
        public Builder withProductID(@Nullable String str) {
            this.e = str;
            return this;
        }

        @NonNull
        public Builder withPayload(@Nullable String str) {
            this.f = str;
            return this;
        }

        @NonNull
        public Builder withReceipt(@Nullable Receipt receipt) {
            this.g = receipt;
            return this;
        }

        @NonNull
        public Revenue build() {
            return new Revenue(this);
        }
    }

    public static class Receipt {
        @Nullable
        public final String data;
        @Nullable
        public final String signature;

        public static class Builder {
            /* access modifiers changed from: private */
            @Nullable

            /* renamed from: a reason: collision with root package name */
            public String f4604a;
            /* access modifiers changed from: private */
            @Nullable
            public String b;

            Builder() {
            }

            @NonNull
            public Builder withData(@Nullable String str) {
                this.f4604a = str;
                return this;
            }

            @NonNull
            public Builder withSignature(@Nullable String str) {
                this.b = str;
                return this;
            }

            @NonNull
            public Receipt build() {
                return new Receipt(this);
            }
        }

        private Receipt(@NonNull Builder builder) {
            this.data = builder.f4604a;
            this.signature = builder.b;
        }

        @NonNull
        public static Builder newBuilder() {
            return new Builder();
        }
    }

    private Revenue(@NonNull Builder builder) {
        this.price = builder.f4603a;
        this.priceMicros = builder.b;
        this.currency = builder.c;
        this.quantity = builder.d;
        this.productID = builder.e;
        this.payload = builder.f;
        this.receipt = builder.g;
    }

    @Deprecated
    @NonNull
    public static Builder newBuilder(double d, @NonNull Currency currency2) {
        return new Builder(d, currency2);
    }

    @NonNull
    public static Builder newBuilderWithMicros(long j, @NonNull Currency currency2) {
        return new Builder(j, currency2);
    }
}
