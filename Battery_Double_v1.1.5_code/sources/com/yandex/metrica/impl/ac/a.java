package com.yandex.metrica.impl.ac;

import android.annotation.SuppressLint;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Pair;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.yandex.metrica.impl.ob.al;
import com.yandex.metrica.impl.ob.db;
import com.yandex.metrica.impl.ob.dr;
import com.yandex.metrica.impl.ob.dt;
import com.yandex.metrica.impl.ob.du;
import com.yandex.metrica.impl.ob.dv;
import com.yandex.metrica.impl.ob.dw;
import com.yandex.metrica.impl.ob.eb;
import com.yandex.metrica.impl.ob.uk;
import com.yandex.metrica.impl.ob.xh;
import com.yandex.metrica.impl.ob.yc;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import java.util.concurrent.LinkedBlockingQueue;

public class a {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public volatile String f4615a;
    /* access modifiers changed from: private */
    public volatile Boolean b;
    /* access modifiers changed from: private */
    public final Object c;
    private volatile FutureTask<Pair<String, Boolean>> d;
    /* access modifiers changed from: private */
    public uk e;
    @NonNull
    private final f f;
    @Nullable
    private Context g;
    @NonNull
    private final yc h;
    @NonNull
    private xh i;

    /* renamed from: com.yandex.metrica.impl.ac.a$a reason: collision with other inner class name */
    private static class C0104a {
        @SuppressLint({"StaticFieldLeak"})

        /* renamed from: a reason: collision with root package name */
        static final a f4619a = new a(new b(), db.k().b());
    }

    static class b implements f {
        public boolean a(@Nullable uk ukVar) {
            return true;
        }

        b() {
        }
    }

    public static class c {

        /* renamed from: a reason: collision with root package name */
        public final String f4620a;
        public final Boolean b;

        public c(String str, Boolean bool) {
            this.f4620a = str;
            this.b = bool;
        }
    }

    private interface d extends IInterface {

        /* renamed from: com.yandex.metrica.impl.ac.a$d$a reason: collision with other inner class name */
        public static abstract class C0105a extends Binder implements d {

            /* renamed from: com.yandex.metrica.impl.ac.a$d$a$a reason: collision with other inner class name */
            private static class C0106a implements d {

                /* renamed from: a reason: collision with root package name */
                private IBinder f4621a;

                C0106a(IBinder iBinder) {
                    this.f4621a = iBinder;
                }

                public IBinder asBinder() {
                    return this.f4621a;
                }

                public String a() throws RemoteException {
                    Parcel obtain = Parcel.obtain();
                    Parcel obtain2 = Parcel.obtain();
                    try {
                        obtain.writeInterfaceToken(AdvertisingInterface.ADVERTISING_ID_SERVICE_INTERFACE_TOKEN);
                        this.f4621a.transact(1, obtain, obtain2, 0);
                        obtain2.readException();
                        return obtain2.readString();
                    } finally {
                        obtain2.recycle();
                        obtain.recycle();
                    }
                }

                public boolean a(boolean z) throws RemoteException {
                    Parcel obtain = Parcel.obtain();
                    Parcel obtain2 = Parcel.obtain();
                    try {
                        obtain.writeInterfaceToken(AdvertisingInterface.ADVERTISING_ID_SERVICE_INTERFACE_TOKEN);
                        obtain.writeInt(z ? 1 : 0);
                        boolean z2 = false;
                        this.f4621a.transact(2, obtain, obtain2, 0);
                        obtain2.readException();
                        if (obtain2.readInt() != 0) {
                            z2 = true;
                        }
                        return z2;
                    } finally {
                        obtain2.recycle();
                        obtain.recycle();
                    }
                }
            }

            public static d a(IBinder iBinder) {
                if (iBinder == null) {
                    return null;
                }
                IInterface queryLocalInterface = iBinder.queryLocalInterface(AdvertisingInterface.ADVERTISING_ID_SERVICE_INTERFACE_TOKEN);
                if (queryLocalInterface == null || !(queryLocalInterface instanceof d)) {
                    return new C0106a(iBinder);
                }
                return (d) queryLocalInterface;
            }

            public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
                switch (i) {
                    case 1:
                        parcel.enforceInterface(AdvertisingInterface.ADVERTISING_ID_SERVICE_INTERFACE_TOKEN);
                        String a2 = a();
                        parcel2.writeNoException();
                        parcel2.writeString(a2);
                        return true;
                    case 2:
                        parcel.enforceInterface(AdvertisingInterface.ADVERTISING_ID_SERVICE_INTERFACE_TOKEN);
                        boolean a3 = a(parcel.readInt() != 0);
                        parcel2.writeNoException();
                        parcel2.writeInt(a3 ? 1 : 0);
                        return true;
                    default:
                        return super.onTransact(i, parcel, parcel2, i2);
                }
            }
        }

        String a() throws RemoteException;

        boolean a(boolean z) throws RemoteException;
    }

    private class e implements ServiceConnection {
        private boolean b;
        private final BlockingQueue<IBinder> c;

        public void onServiceDisconnected(ComponentName componentName) {
        }

        private e() {
            this.b = false;
            this.c = new LinkedBlockingQueue();
        }

        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            try {
                this.c.put(iBinder);
            } catch (InterruptedException unused) {
            }
        }

        public IBinder a() throws InterruptedException {
            if (!this.b) {
                this.b = true;
                return (IBinder) this.c.take();
            }
            throw new IllegalStateException();
        }
    }

    interface f {
        boolean a(@Nullable uk ukVar);
    }

    private static class g {
        @SuppressLint({"StaticFieldLeak"})

        /* renamed from: a reason: collision with root package name */
        static final a f4623a = new a(new h(), al.a().j().i());
    }

    static class h implements f {
        h() {
        }

        public boolean a(@Nullable uk ukVar) {
            return ukVar != null && (ukVar.o.h || !ukVar.u);
        }
    }

    private interface i<T> {
        T b(Future<Pair<String, Boolean>> future) throws InterruptedException, ExecutionException;
    }

    private a(@NonNull f fVar, @NonNull xh xhVar) {
        this.f4615a = null;
        this.b = null;
        this.c = new Object();
        this.f = fVar;
        this.h = new yc();
        this.i = xhVar;
        dr.a().a(this, eb.class, dv.a((du<T>) new du<eb>() {
            public void a(eb ebVar) {
                synchronized (a.this.c) {
                    a.this.e = ebVar.b;
                }
            }
        }).a());
    }

    public static a a() {
        return g.f4623a;
    }

    public static a b() {
        return C0104a.f4619a;
    }

    public void a(@NonNull Context context) {
        this.g = context.getApplicationContext();
    }

    public void b(@NonNull final Context context) {
        this.g = context.getApplicationContext();
        if (this.d == null) {
            synchronized (this.c) {
                if (this.d == null && this.f.a(this.e)) {
                    this.d = new FutureTask<>(new Callable<Pair<String, Boolean>>() {
                        /* renamed from: a */
                        public Pair<String, Boolean> call() {
                            Context applicationContext = context.getApplicationContext();
                            if (a.this.d(applicationContext)) {
                                a.this.e(applicationContext);
                            }
                            if (!a.this.e()) {
                                a.this.f(applicationContext);
                            }
                            return new Pair<>(a.this.f4615a, a.this.b);
                        }
                    });
                    this.i.a((Runnable) this.d);
                }
            }
        }
    }

    public void a(@NonNull Context context, @NonNull uk ukVar) {
        this.e = ukVar;
        b(context);
    }

    private void a(String str) {
        dr.a().b((dt) new dw(str));
        this.f4615a = str;
    }

    private void a(Boolean bool) {
        this.b = bool;
    }

    private <T> T a(Context context, i<T> iVar) {
        b(context);
        try {
            return iVar.b(this.d);
        } catch (InterruptedException | ExecutionException unused) {
            return null;
        }
    }

    public c c(Context context) {
        if (this.f.a(this.e)) {
            return (c) a(context, (i<T>) new i<c>() {
                /* renamed from: a */
                public c b(Future<Pair<String, Boolean>> future) throws InterruptedException, ExecutionException {
                    Pair pair = (Pair) future.get();
                    return new c((String) pair.first, (Boolean) pair.second);
                }
            });
        }
        return null;
    }

    public String c() {
        f();
        return this.f4615a;
    }

    public Boolean d() {
        f();
        return this.b;
    }

    private void f() {
        if (this.g != null && !e()) {
            c(this.g);
        }
    }

    public synchronized boolean e() {
        return (this.f4615a == null || this.b == null) ? false : true;
    }

    /* access modifiers changed from: private */
    public boolean d(Context context) {
        try {
            return Class.forName("com.google.android.gms.common.GooglePlayServicesUtil").getMethod("isGooglePlayServicesAvailable", new Class[]{Context.class}).invoke(null, new Object[]{context}).equals(Integer.valueOf(0));
        } catch (Exception unused) {
            return false;
        }
    }

    /* access modifiers changed from: private */
    public void e(Context context) {
        try {
            Object invoke = Class.forName("com.google.android.gms.ads.identifier.AdvertisingIdClient").getMethod("getAdvertisingIdInfo", new Class[]{Context.class}).invoke(null, new Object[]{context});
            Class cls = Class.forName("com.google.android.gms.ads.identifier.AdvertisingIdClient$Info");
            String str = (String) cls.getMethod("getId", new Class[0]).invoke(invoke, new Object[0]);
            Boolean bool = (Boolean) cls.getMethod(RequestParameters.isLAT, new Class[0]).invoke(invoke, new Object[0]);
            synchronized (this) {
                a(str);
                a(bool);
            }
        } catch (Throwable unused) {
        }
    }

    /* access modifiers changed from: private */
    public void f(Context context) {
        e eVar = new e();
        Intent intent = new Intent(AdvertisingInfoServiceStrategy.GOOGLE_PLAY_SERVICES_INTENT);
        intent.setPackage("com.google.android.gms");
        if (this.h.c(context, intent, 0) != null && context.bindService(intent, eVar, 1)) {
            try {
                d a2 = C0105a.a(eVar.a());
                String a3 = a2.a();
                Boolean valueOf = Boolean.valueOf(a2.a(true));
                synchronized (this) {
                    a(a3);
                    a(valueOf);
                }
            } catch (Throwable th) {
                context.unbindService(eVar);
                throw th;
            }
            context.unbindService(eVar);
        }
    }
}
