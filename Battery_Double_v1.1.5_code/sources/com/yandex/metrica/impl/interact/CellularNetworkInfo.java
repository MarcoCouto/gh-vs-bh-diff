package com.yandex.metrica.impl.interact;

import android.content.Context;
import android.text.TextUtils;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.tapjoy.TapjoyConstants;
import com.yandex.metrica.impl.ob.db;
import com.yandex.metrica.impl.ob.uu;
import com.yandex.metrica.impl.ob.vd;
import com.yandex.metrica.impl.ob.vf;
import com.yandex.metrica.impl.ob.vg;
import java.util.HashMap;
import java.util.Map.Entry;

public class CellularNetworkInfo {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public String f4624a = "";

    public CellularNetworkInfo(Context context) {
        new vd(context, db.k().b()).a((vg) new vg() {
            public void a(vf vfVar) {
                uu b = vfVar.b();
                if (b != null) {
                    String g = b.g();
                    String f = b.f();
                    Integer c = b.c();
                    Integer b2 = b.b();
                    Integer e = b.e();
                    Integer d = b.d();
                    Integer a2 = b.a();
                    HashMap hashMap = new HashMap();
                    hashMap.put("network_type", g);
                    hashMap.put("operator_name", f);
                    String str = null;
                    hashMap.put(TapjoyConstants.TJC_DEVICE_COUNTRY_CODE, b2 != null ? String.valueOf(b2) : null);
                    hashMap.put("operator_id", c != null ? String.valueOf(c) : null);
                    hashMap.put("cell_id", e != null ? String.valueOf(e) : null);
                    hashMap.put("lac", d != null ? String.valueOf(d) : null);
                    String str2 = "signal_strength";
                    if (a2 != null) {
                        str = String.valueOf(a2);
                    }
                    hashMap.put(str2, str);
                    StringBuilder sb = new StringBuilder();
                    String str3 = "";
                    for (Entry entry : hashMap.entrySet()) {
                        String str4 = (String) entry.getValue();
                        if (!TextUtils.isEmpty(str4)) {
                            sb.append(str3);
                            sb.append((String) entry.getKey());
                            sb.append(RequestParameters.EQUAL);
                            sb.append(str4);
                            str3 = RequestParameters.AMPERSAND;
                        }
                    }
                    CellularNetworkInfo.this.f4624a = sb.toString();
                }
            }
        });
    }

    public String getCelluralInfo() {
        return this.f4624a;
    }
}
