package com.yandex.metrica.impl.interact;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import com.yandex.metrica.impl.ob.bt;
import com.yandex.metrica.impl.ob.dr;
import com.yandex.metrica.impl.ob.du;
import com.yandex.metrica.impl.ob.dv;
import com.yandex.metrica.impl.ob.dz;
import com.yandex.metrica.impl.ob.v;
import java.util.ArrayList;
import java.util.List;

@Deprecated
public class DeviceInfo {

    /* renamed from: a reason: collision with root package name */
    private static final Object f4626a = new Object();
    private static volatile DeviceInfo b;
    public final String appPlatform;
    public final String deviceRootStatus;
    public final List<String> deviceRootStatusMarkers;
    public final String deviceType;
    public String locale;
    public final String manufacturer;
    public final String model;
    public final String osVersion;
    public final String platform;
    public final String platformDeviceId;
    public final float scaleFactor;
    public final int screenDpi;
    public final int screenHeight;
    public final int screenWidth;

    public static DeviceInfo getInstance(@NonNull Context context) {
        if (b == null) {
            synchronized (f4626a) {
                if (b == null) {
                    b = new DeviceInfo(context, v.a(context));
                }
            }
        }
        return b;
    }

    @VisibleForTesting
    DeviceInfo(@NonNull Context context, @NonNull v vVar) {
        this.platform = vVar.f5259a;
        this.appPlatform = vVar.f5259a;
        this.platformDeviceId = vVar.a();
        this.manufacturer = vVar.b;
        this.model = vVar.c;
        this.osVersion = vVar.d;
        this.screenWidth = vVar.f.f5263a;
        this.screenHeight = vVar.f.b;
        this.screenDpi = vVar.f.c;
        this.scaleFactor = vVar.f.d;
        this.deviceType = vVar.g;
        this.deviceRootStatus = vVar.h;
        this.deviceRootStatusMarkers = new ArrayList(vVar.i);
        this.locale = bt.a(context.getResources().getConfiguration().locale);
        dr.a().a(this, dz.class, dv.a((du<T>) new du<dz>() {
            public void a(dz dzVar) {
                DeviceInfo.this.locale = dzVar.f4802a;
            }
        }).a());
    }
}
