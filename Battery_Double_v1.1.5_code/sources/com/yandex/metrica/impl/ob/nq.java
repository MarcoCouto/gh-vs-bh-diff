package com.yandex.metrica.impl.ob;

import android.support.annotation.Nullable;

@Deprecated
public class nq {
    @Nullable

    /* renamed from: a reason: collision with root package name */
    public final String f5014a;
    @Nullable
    public final String b;

    public nq(@Nullable String str, @Nullable String str2) {
        this.f5014a = str;
        this.b = str2;
    }

    public boolean equals(Object obj) {
        boolean z = true;
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        nq nqVar = (nq) obj;
        if (this.f5014a == null ? nqVar.f5014a != null : !this.f5014a.equals(nqVar.f5014a)) {
            return false;
        }
        if (this.b != null) {
            z = this.b.equals(nqVar.b);
        } else if (nqVar.b != null) {
            z = false;
        }
        return z;
    }

    public int hashCode() {
        int i = 0;
        int hashCode = (this.f5014a != null ? this.f5014a.hashCode() : 0) * 31;
        if (this.b != null) {
            i = this.b.hashCode();
        }
        return hashCode + i;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("AppMetricaDeviceIdentifiers{deviceID='");
        sb.append(this.f5014a);
        sb.append('\'');
        sb.append(", deviceIDHash='");
        sb.append(this.b);
        sb.append('\'');
        sb.append('}');
        return sb.toString();
    }
}
