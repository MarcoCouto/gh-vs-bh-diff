package com.yandex.metrica.impl.ob;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.telephony.TelephonyManager;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class uz implements ve<List<String>> {
    /* access modifiers changed from: private */
    @NonNull

    /* renamed from: a reason: collision with root package name */
    public final ux f5256a;
    /* access modifiers changed from: private */
    @NonNull
    public pr b;

    uz(@NonNull ux uxVar, @NonNull pr prVar) {
        this.f5256a = uxVar;
        this.b = prVar;
    }

    @Nullable
    /* renamed from: a */
    public List<String> d() {
        ArrayList arrayList = new ArrayList();
        if (this.f5256a.g()) {
            if (cx.a(23)) {
                arrayList.addAll(c());
            } else {
                arrayList.add(b());
            }
        }
        return arrayList;
    }

    @Nullable
    @SuppressLint({"MissingPermission", "HardwareIds"})
    private String b() {
        return (String) cx.a((wo<T, S>) new wo<TelephonyManager, String>() {
            public String a(TelephonyManager telephonyManager) throws Throwable {
                if (uz.this.b.d(uz.this.f5256a.d())) {
                    return telephonyManager.getDeviceId();
                }
                return null;
            }
        }, this.f5256a.c(), "getting imei", "TelephonyManager");
    }

    @TargetApi(23)
    @NonNull
    private List<String> c() {
        return (List) cx.a(new wo<TelephonyManager, List<String>>() {
            public List<String> a(TelephonyManager telephonyManager) throws Throwable {
                HashSet hashSet = new HashSet();
                if (uz.this.b.d(uz.this.f5256a.d())) {
                    for (int i = 0; i < 10; i++) {
                        String deviceId = telephonyManager.getDeviceId(i);
                        if (deviceId != null) {
                            hashSet.add(deviceId);
                        }
                    }
                }
                return new ArrayList(hashSet);
            }
        }, this.f5256a.c(), "getting all imeis", "TelephonyManager", new ArrayList());
    }
}
