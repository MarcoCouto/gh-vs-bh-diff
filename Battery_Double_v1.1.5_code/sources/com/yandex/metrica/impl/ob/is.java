package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;

public class is {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final iq f4887a;
    @NonNull
    private final ir b;
    @NonNull
    private final kx c;
    @NonNull
    private final String d;

    public is(@NonNull Context context, @NonNull ek ekVar) {
        this(new ir(), new iq(), ld.a(context).c(ekVar), "event_hashes");
    }

    @VisibleForTesting
    is(@NonNull ir irVar, @NonNull iq iqVar, @NonNull kx kxVar, @NonNull String str) {
        this.b = irVar;
        this.f4887a = iqVar;
        this.c = kxVar;
        this.d = str;
    }

    @NonNull
    public ip a() {
        try {
            byte[] a2 = this.c.a(this.d);
            if (cx.a(a2)) {
                return this.f4887a.a(this.b.c());
            }
            return this.f4887a.a(this.b.b(a2));
        } catch (Throwable unused) {
            return this.f4887a.a(this.b.c());
        }
    }

    public void a(@NonNull ip ipVar) {
        this.c.a(this.d, this.b.a(this.f4887a.b(ipVar)));
    }
}
