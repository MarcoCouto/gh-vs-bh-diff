package com.yandex.metrica.impl.ob;

import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.util.SparseArray;
import com.integralads.avid.library.inmobi.BuildConfig;

public final class ch {

    /* renamed from: a reason: collision with root package name */
    public static final a f4725a = new a("3.8.0");
    @VisibleForTesting
    static final SparseArray<a> b = new SparseArray<>();

    public static final class a {

        /* renamed from: a reason: collision with root package name */
        public final String f4726a;

        a(String str) {
            this.f4726a = str;
        }
    }

    static {
        b.put(1, new a("1.00"));
        b.put(2, new a("1.10"));
        b.put(3, new a("1.11"));
        b.put(4, new a("1.20"));
        b.put(5, new a("1.21"));
        b.put(6, new a("1.22"));
        b.put(7, new a("1.23"));
        b.put(8, new a("1.24"));
        b.put(9, new a("1.26"));
        b.put(10, new a("1.27"));
        b.put(11, new a("1.40"));
        b.put(12, new a("1.41"));
        b.put(13, new a("1.42"));
        b.put(14, new a("1.50"));
        b.put(15, new a("1.51"));
        b.put(16, new a("1.60"));
        b.put(17, new a("1.61"));
        b.put(18, new a("1.62"));
        b.put(19, new a("1.63"));
        b.put(20, new a("1.64"));
        b.put(21, new a("1.65"));
        b.put(22, new a("1.66"));
        b.put(23, new a("1.67"));
        b.put(24, new a("1.68"));
        b.put(25, new a("1.69"));
        b.put(26, new a("1.70"));
        b.put(27, new a("1.71"));
        b.put(28, new a("1.72"));
        b.put(29, new a("1.80"));
        b.put(30, new a("1.81"));
        b.put(31, new a("1.82"));
        b.put(32, new a("2.00"));
        b.put(33, new a("2.10"));
        b.put(34, new a("2.11"));
        b.put(35, new a("2.20"));
        b.put(36, new a("2.21"));
        b.put(37, new a("2.22"));
        b.put(38, new a("2.23"));
        b.put(39, new a("2.30"));
        b.put(40, new a("2.31"));
        b.put(41, new a("2.32"));
        b.put(42, new a("2.33"));
        b.put(43, new a("2.40"));
        b.put(44, new a("2.41"));
        b.put(45, new a("2.42"));
        b.put(46, new a("2.43"));
        b.put(47, new a("2.50"));
        b.put(48, new a("2.51"));
        b.put(49, new a("2.52"));
        b.put(50, new a("2.60"));
        b.put(51, new a("2.61"));
        b.put(52, new a("2.62"));
        b.put(53, new a("2.63"));
        b.put(54, new a("2.64"));
        b.put(55, new a("2.70"));
        b.put(56, new a("2.71"));
        b.put(57, new a("2.72"));
        b.put(58, new a("2.73"));
        b.put(59, new a("2.74"));
        b.put(60, new a("2.75"));
        b.put(61, new a("2.76"));
        b.put(62, new a("2.77"));
        b.put(63, new a("2.78"));
        b.put(64, new a("2.80"));
        b.put(65, new a("2.81-RC1"));
        b.put(66, new a("3.0.0"));
        b.put(67, new a("3.1.0"));
        b.put(68, new a("3.2.0"));
        b.put(69, new a("3.2.1"));
        b.put(70, new a("3.2.2"));
        b.put(71, new a("3.3.0"));
        b.put(72, new a("3.4.0"));
        b.put(73, new a("3.5.0"));
        b.put(74, new a("3.5.1"));
        b.put(75, new a("3.5.2"));
        b.put(76, new a("3.5.3"));
        b.put(77, new a("3.6.0"));
        b.put(78, new a("3.6.1"));
        b.put(79, new a("3.6.2"));
        b.put(80, new a("3.6.3"));
        b.put(81, new a(BuildConfig.VERSION_NAME));
        b.put(82, new a("3.7.0"));
        b.put(83, new a("3.7.1"));
        b.put(84, new a("3.7.2"));
        b.put(85, new a("3.8.0"));
    }

    @Nullable
    static a a(int i) {
        return (a) b.get(i);
    }
}
