package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public class oh {
    public final long e;
    public final float f;
    public final int g;
    public final int h;
    public final long i;
    public final int j;
    public final boolean k;
    public final long l;
    public final boolean m;

    public enum a {
        FOREGROUND("fg"),
        BACKGROUND("bg");
        
        private final String c;

        private a(String str) {
            this.c = str;
        }

        @NonNull
        public String toString() {
            return this.c;
        }

        @NonNull
        public static a a(@Nullable String str) {
            a[] values;
            a aVar = FOREGROUND;
            for (a aVar2 : values()) {
                if (aVar2.c.equals(str)) {
                    aVar = aVar2;
                }
            }
            return aVar;
        }
    }

    public oh(long j2, float f2, int i2, int i3, long j3, int i4, boolean z, long j4, boolean z2) {
        this.e = j2;
        this.f = f2;
        this.g = i2;
        this.h = i3;
        this.i = j3;
        this.j = i4;
        this.k = z;
        this.l = j4;
        this.m = z2;
    }

    @NonNull
    public a a() {
        return a.FOREGROUND;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("ForegroundCollectionConfig{updateTimeInterval=");
        sb.append(this.e);
        sb.append(", updateDistanceInterval=");
        sb.append(this.f);
        sb.append(", recordsCountToForceFlush=");
        sb.append(this.g);
        sb.append(", maxBatchSize=");
        sb.append(this.h);
        sb.append(", maxAgeToForceFlush=");
        sb.append(this.i);
        sb.append(", maxRecordsToStoreLocally=");
        sb.append(this.j);
        sb.append(", collectionEnabled=");
        sb.append(this.k);
        sb.append(", lbsUpdateTimeInterval=");
        sb.append(this.l);
        sb.append(", lbsCollectionEnabled=");
        sb.append(this.m);
        sb.append('}');
        return sb.toString();
    }
}
