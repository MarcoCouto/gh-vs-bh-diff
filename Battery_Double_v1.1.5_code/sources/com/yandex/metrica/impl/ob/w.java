package com.yandex.metrica.impl.ob;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.util.Base64;
import android.util.Pair;
import com.facebook.internal.NativeProtocol;
import com.yandex.metrica.impl.ob.af.a;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import org.json.JSONArray;
import org.json.JSONObject;

public class w implements Parcelable {
    public static final Creator<w> CREATOR = new Creator<w>() {
        /* renamed from: a */
        public w createFromParcel(Parcel parcel) {
            Bundle readBundle = parcel.readBundle(x.class.getClassLoader());
            w a2 = new w().a(readBundle.getInt("CounterReport.Type", a.EVENT_TYPE_UNDEFINED.a())).b(readBundle.getInt("CounterReport.CustomType")).c(cu.b(readBundle.getString("CounterReport.Value"), "")).a(readBundle.getString("CounterReport.UserInfo")).e(readBundle.getString("CounterReport.Environment")).b(readBundle.getString("CounterReport.Event")).a(w.d(readBundle)).c(readBundle.getInt("CounterReport.TRUNCATED")).d(readBundle.getString("CounterReport.ProfileID")).a(readBundle.getLong("CounterReport.CreationElapsedRealtime")).b(readBundle.getLong("CounterReport.CreationTimestamp")).a(aj.a(Integer.valueOf(readBundle.getInt("CounterReport.UniquenessStatus"))));
            ap apVar = (ap) readBundle.getParcelable("CounterReport.IdentifiersData");
            if (apVar != null) {
                a2.a(apVar);
            }
            return a2;
        }

        /* renamed from: a */
        public w[] newArray(int i) {
            return new w[i];
        }
    };

    /* renamed from: a reason: collision with root package name */
    String f5284a;
    String b;
    int c;
    int d;
    int e;
    private String f;
    private String g;
    @Nullable
    private Pair<String, String> h;
    private String i;
    private long j;
    private long k;
    @NonNull
    private aj l;
    @Nullable
    private ap m;

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i2) {
        Bundle bundle = new Bundle();
        bundle.putString("CounterReport.Event", this.f5284a);
        bundle.putString("CounterReport.Value", this.b);
        bundle.putInt("CounterReport.Type", this.c);
        bundle.putInt("CounterReport.CustomType", this.d);
        bundle.putInt("CounterReport.TRUNCATED", this.e);
        bundle.putString("CounterReport.ProfileID", this.i);
        bundle.putInt("CounterReport.UniquenessStatus", this.l.d);
        if (this.m != null) {
            bundle.putParcelable("CounterReport.IdentifiersData", this.m);
        }
        if (this.g != null) {
            bundle.putString("CounterReport.Environment", this.g);
        }
        if (this.f != null) {
            bundle.putString("CounterReport.UserInfo", this.f);
        }
        if (this.h != null) {
            a(bundle, this.h);
        }
        bundle.putLong("CounterReport.CreationElapsedRealtime", this.j);
        bundle.putLong("CounterReport.CreationTimestamp", this.k);
        parcel.writeBundle(bundle);
    }

    public w() {
        this("", 0);
    }

    public w(@Nullable w wVar) {
        this.l = aj.UNKNOWN;
        if (wVar != null) {
            this.f5284a = wVar.d();
            this.b = wVar.e();
            this.c = wVar.g();
            this.d = wVar.h();
            this.f = wVar.l();
            this.g = wVar.j();
            this.h = wVar.k();
            this.e = wVar.o();
            this.i = wVar.i;
            this.j = wVar.r();
            this.k = wVar.s();
            this.l = wVar.l;
            this.m = wVar.m;
        }
    }

    public w(String str, int i2) {
        this("", str, i2);
    }

    public w(String str, String str2, int i2) {
        this(str, str2, i2, new wg());
    }

    @VisibleForTesting
    public w(String str, String str2, int i2, wg wgVar) {
        this.l = aj.UNKNOWN;
        this.f5284a = str2;
        this.c = i2;
        this.b = str;
        this.j = wgVar.c();
        this.k = wgVar.a();
    }

    public String d() {
        return this.f5284a;
    }

    public w b(String str) {
        this.f5284a = str;
        return this;
    }

    public String e() {
        return this.b;
    }

    public byte[] f() {
        return Base64.decode(this.b, 0);
    }

    public w c(String str) {
        this.b = str;
        return this;
    }

    public w a(@Nullable byte[] bArr) {
        this.b = new String(Base64.encode(bArr, 0));
        return this;
    }

    public int g() {
        return this.c;
    }

    public w a(int i2) {
        this.c = i2;
        return this;
    }

    public int h() {
        return this.d;
    }

    public w b(int i2) {
        this.d = i2;
        return this;
    }

    @Nullable
    public ap i() {
        return this.m;
    }

    /* access modifiers changed from: 0000 */
    public String j() {
        return this.g;
    }

    public Pair<String, String> k() {
        return this.h;
    }

    /* access modifiers changed from: 0000 */
    public w e(String str) {
        this.g = str;
        return this;
    }

    /* access modifiers changed from: 0000 */
    public w b(String str, String str2) {
        if (this.h == null) {
            this.h = new Pair<>(str, str2);
        }
        return this;
    }

    /* access modifiers changed from: private */
    public w a(@Nullable Pair<String, String> pair) {
        this.h = pair;
        return this;
    }

    public String l() {
        return this.f;
    }

    public w a(String str) {
        this.f = str;
        return this;
    }

    /* access modifiers changed from: protected */
    public w c(int i2) {
        this.e = i2;
        return this;
    }

    /* access modifiers changed from: protected */
    public w a(long j2) {
        this.j = j2;
        return this;
    }

    /* access modifiers changed from: protected */
    public w b(long j2) {
        this.k = j2;
        return this;
    }

    /* access modifiers changed from: protected */
    public w a(@NonNull ap apVar) {
        this.m = apVar;
        return this;
    }

    public boolean m() {
        return this.f5284a == null;
    }

    public boolean n() {
        return a.EVENT_TYPE_UNDEFINED.a() == this.c;
    }

    public int o() {
        return this.e;
    }

    @Nullable
    public String p() {
        return this.i;
    }

    public w d(@Nullable String str) {
        this.i = str;
        return this;
    }

    @NonNull
    public aj q() {
        return this.l;
    }

    @NonNull
    public w a(@NonNull aj ajVar) {
        this.l = ajVar;
        return this;
    }

    public long r() {
        return this.j;
    }

    public long s() {
        return this.k;
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public Bundle a(Bundle bundle) {
        if (bundle == null) {
            bundle = new Bundle();
        }
        bundle.putParcelable("CounterReport.Object", this);
        return bundle;
    }

    public String toString() {
        return String.format(Locale.US, "[event: %s, type: %s, value: %s]", new Object[]{this.f5284a, a.a(this.c).b(), this.b});
    }

    private static void a(@NonNull Bundle bundle, @NonNull Pair<String, String> pair) {
        bundle.putString("CounterReport.AppEnvironmentDiffKey", (String) pair.first);
        bundle.putString("CounterReport.AppEnvironmentDiffValue", (String) pair.second);
    }

    /* access modifiers changed from: private */
    @Nullable
    public static Pair<String, String> d(Bundle bundle) {
        if (!bundle.containsKey("CounterReport.AppEnvironmentDiffKey") || !bundle.containsKey("CounterReport.AppEnvironmentDiffValue")) {
            return null;
        }
        return new Pair<>(bundle.getString("CounterReport.AppEnvironmentDiffKey"), bundle.getString("CounterReport.AppEnvironmentDiffValue"));
    }

    @NonNull
    public static w b(Bundle bundle) {
        if (bundle != null) {
            try {
                w wVar = (w) bundle.getParcelable("CounterReport.Object");
                if (wVar != null) {
                    return wVar;
                }
            } catch (Throwable unused) {
                return new w();
            }
        }
        return new w();
    }

    public static w a(w wVar, a aVar) {
        w a2 = a(wVar);
        a2.a(aVar.a());
        return a2;
    }

    public static w a(@NonNull w wVar) {
        w wVar2 = new w(wVar);
        wVar2.b("");
        wVar2.c("");
        return wVar2;
    }

    public static w b(w wVar) {
        return a(wVar, a.EVENT_TYPE_ALIVE);
    }

    public static w c(w wVar) {
        return a(wVar, a.EVENT_TYPE_START);
    }

    public static w d(w wVar) {
        return a(wVar, a.EVENT_TYPE_INIT);
    }

    public static w a(@NonNull Context context) {
        Integer num;
        JSONObject jSONObject = new JSONObject();
        JSONObject jSONObject2 = new JSONObject();
        try {
            if (cx.a(21)) {
                num = b(context);
                if (num == null) {
                    num = c(context);
                }
            } else {
                num = c(context);
            }
            if (num != null) {
                jSONObject2.put("battery", num);
            }
            jSONObject2.put("boot_time_seconds", wi.d());
            jSONObject.put("dfid", jSONObject2);
        } catch (Throwable unused) {
        }
        w b2 = new w().b("");
        b2.a(a.EVENT_TYPE_IDENTITY_LIGHT.a()).c(jSONObject.toString());
        return b2;
    }

    @Nullable
    @TargetApi(21)
    private static Integer b(@NonNull Context context) {
        BatteryManager batteryManager = (BatteryManager) context.getSystemService("batterymanager");
        Integer num = null;
        if (batteryManager == null) {
            return null;
        }
        int intProperty = batteryManager.getIntProperty(4);
        if (intProperty != 0) {
            num = Integer.valueOf(intProperty);
        }
        return num;
    }

    @Nullable
    private static Integer c(@NonNull Context context) {
        Intent registerReceiver = context.registerReceiver(null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
        if (registerReceiver != null) {
            int intExtra = registerReceiver.getIntExtra("level", -1);
            int intExtra2 = registerReceiver.getIntExtra("scale", -1);
            if (intExtra > 0 && intExtra2 > 0) {
                return Integer.valueOf((intExtra * 100) / intExtra2);
            }
        }
        return null;
    }

    public static w a(w wVar, en enVar) {
        ar a2 = new ar(enVar.k()).a();
        try {
            if (enVar.w()) {
                a2.e();
            }
            if (enVar.i().F()) {
                a2.a(enVar.i().G());
            }
            a2.c();
        } catch (Throwable unused) {
        }
        w a3 = a(wVar);
        a3.a(a.EVENT_TYPE_IDENTITY.a()).c(a2.g());
        return a3;
    }

    public static w a(w wVar, @NonNull Collection<pu> collection, @Nullable l lVar, @NonNull j jVar, @NonNull List<String> list) {
        String str;
        w a2 = a(wVar);
        try {
            JSONArray jSONArray = new JSONArray();
            for (pu puVar : collection) {
                jSONArray.put(new JSONObject().put("name", puVar.f5068a).put("granted", puVar.b));
            }
            JSONObject jSONObject = new JSONObject();
            if (lVar != null) {
                jSONObject.put("background_restricted", lVar.b);
                jSONObject.put("app_standby_bucket", jVar.a(lVar.f4958a));
            }
            str = new JSONObject().put(NativeProtocol.RESULT_ARGS_PERMISSIONS, jSONArray).put("background_restrictions", jSONObject).put("available_providers", new JSONArray(list)).toString();
        } catch (Throwable unused) {
            str = "";
        }
        return a2.a(a.EVENT_TYPE_PERMISSIONS.a()).c(str);
    }

    public static w a(w wVar, String str) {
        return a(wVar).a(a.EVENT_TYPE_APP_FEATURES.a()).c(str);
    }

    public static w e(w wVar) {
        return a(wVar, a.EVENT_TYPE_FIRST_ACTIVATION);
    }

    public static w f(w wVar) {
        return a(wVar, a.EVENT_TYPE_APP_UPDATE);
    }

    public static w t() {
        return new w().a(a.EVENT_TYPE_UPDATE_PRE_ACTIVATION_CONFIG.a());
    }
}
