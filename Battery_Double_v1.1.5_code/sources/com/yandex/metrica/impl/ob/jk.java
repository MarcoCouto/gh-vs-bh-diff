package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;

public class jk {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private Context f4906a;
    @NonNull
    private jj b;
    @NonNull
    private jo c;

    public jk(@NonNull Context context) {
        this(context, new jj(context), new jo(context));
    }

    @VisibleForTesting
    jk(@NonNull Context context, @NonNull jj jjVar, @NonNull jo joVar) {
        this.f4906a = context;
        this.b = jjVar;
        this.c = joVar;
    }

    public void a() {
        this.f4906a.getPackageName();
        this.c.a().a(this.b.a());
    }
}
