package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class ai {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final List<c> f4638a = new ArrayList();

    public static class a {

        /* renamed from: a reason: collision with root package name */
        private boolean f4639a;
        private long b;
        private long c;
        private long d;
        @NonNull
        private b e;

        a() {
            this(new b());
        }

        public a(@NonNull b bVar) {
            this.e = bVar;
            this.f4639a = false;
            this.d = Long.MAX_VALUE;
        }

        /* access modifiers changed from: 0000 */
        public boolean a() {
            if (this.f4639a) {
                return true;
            }
            return this.e.a(this.c, this.b, this.d);
        }

        /* access modifiers changed from: 0000 */
        public void a(long j, @NonNull TimeUnit timeUnit) {
            this.d = timeUnit.toMillis(j);
        }

        /* access modifiers changed from: 0000 */
        public void b() {
            this.f4639a = true;
        }

        /* access modifiers changed from: 0000 */
        public void a(@Nullable uk ukVar) {
            if (ukVar != null) {
                this.b = TimeUnit.SECONDS.toMillis(ukVar.D);
                this.c = TimeUnit.SECONDS.toMillis(ukVar.E);
            }
        }
    }

    public static class b {
        public boolean a(long j, long j2, long j3) {
            return j2 - j >= j3;
        }
    }

    public static class c {
        @NonNull

        /* renamed from: a reason: collision with root package name */
        private a f4640a;
        @NonNull
        private final com.yandex.metrica.impl.ob.h.a b;
        @NonNull
        private final xh c;

        private c(@NonNull xh xhVar, @NonNull com.yandex.metrica.impl.ob.h.a aVar, @NonNull a aVar2) {
            this.b = aVar;
            this.f4640a = aVar2;
            this.c = xhVar;
        }

        public void a(@Nullable uk ukVar) {
            this.f4640a.a(ukVar);
        }

        public void a(long j) {
            this.f4640a.a(j, TimeUnit.SECONDS);
        }

        public boolean a(int i) {
            if (!this.f4640a.a()) {
                return false;
            }
            this.b.a(TimeUnit.SECONDS.toMillis((long) i), this.c);
            this.f4640a.b();
            return true;
        }
    }

    public c a(@NonNull Runnable runnable, @NonNull xh xhVar) {
        return a(xhVar, new com.yandex.metrica.impl.ob.h.a(runnable), new a());
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public c a(@NonNull xh xhVar, @NonNull com.yandex.metrica.impl.ob.h.a aVar, @NonNull a aVar2) {
        c cVar = new c(xhVar, aVar, aVar2);
        this.f4638a.add(cVar);
        return cVar;
    }

    public void a(@Nullable uk ukVar) {
        for (c a2 : this.f4638a) {
            a2.a(ukVar);
        }
    }
}
