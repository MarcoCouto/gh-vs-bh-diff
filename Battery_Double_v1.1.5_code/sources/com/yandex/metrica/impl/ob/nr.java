package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import org.json.JSONException;
import org.json.JSONObject;

@Deprecated
public class nr {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final nq f5015a;
    private final nt b;
    private final long c;
    private final boolean d;
    private final long e;

    public nr(@NonNull JSONObject jSONObject, long j) throws JSONException {
        this.f5015a = new nq(jSONObject.optString("device_id", null), jSONObject.optString("device_id_hash", null));
        if (jSONObject.has("device_snapshot_key")) {
            this.b = new nt(jSONObject.optString("device_snapshot_key", null));
        } else {
            this.b = null;
        }
        this.c = jSONObject.optLong("last_elections_time", -1);
        this.d = d();
        this.e = j;
    }

    public nr(@NonNull nq nqVar, @NonNull nt ntVar, long j) {
        this.f5015a = nqVar;
        this.b = ntVar;
        this.c = j;
        this.d = d();
        this.e = -1;
    }

    public String a() throws JSONException {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("device_id", this.f5015a.f5014a);
        jSONObject.put("device_id_hash", this.f5015a.b);
        if (this.b != null) {
            jSONObject.put("device_snapshot_key", this.b.b());
        }
        jSONObject.put("last_elections_time", this.c);
        return jSONObject.toString();
    }

    @NonNull
    public nq b() {
        return this.f5015a;
    }

    @Nullable
    public nt c() {
        return this.b;
    }

    private boolean d() {
        boolean z = false;
        if (this.c <= -1) {
            return false;
        }
        if (System.currentTimeMillis() - this.c < 604800000) {
            z = true;
        }
        return z;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Credentials{mIdentifiers=");
        sb.append(this.f5015a);
        sb.append(", mDeviceSnapshot=");
        sb.append(this.b);
        sb.append(", mLastElectionsTime=");
        sb.append(this.c);
        sb.append(", mFresh=");
        sb.append(this.d);
        sb.append(", mLastModified=");
        sb.append(this.e);
        sb.append('}');
        return sb.toString();
    }
}
