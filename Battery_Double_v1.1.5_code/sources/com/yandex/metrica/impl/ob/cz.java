package com.yandex.metrica.impl.ob;

import android.location.Location;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public final class cz extends Location {
    @Nullable

    /* renamed from: a reason: collision with root package name */
    private final String f4767a;

    private cz(@NonNull Location location, @Nullable String str) {
        super(location);
        this.f4767a = str;
    }

    @Nullable
    public String a() {
        return this.f4767a;
    }

    public static cz a(@NonNull Location location) {
        Location location2 = new Location(location);
        String provider = location2.getProvider();
        location2.setProvider("");
        return new cz(location2, provider);
    }

    public static cz b(@NonNull Location location) {
        return new cz(new Location(location), "");
    }
}
