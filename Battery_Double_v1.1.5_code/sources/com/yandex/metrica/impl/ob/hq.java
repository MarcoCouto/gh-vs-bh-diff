package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;

public class hq extends hd {

    /* renamed from: a reason: collision with root package name */
    private final gs<hd, en> f4877a;

    public hq(en enVar) {
        super(enVar);
        this.f4877a = new gs<>(new gq(enVar), enVar);
    }

    public boolean a(@NonNull w wVar) {
        return this.f4877a.b(wVar);
    }
}
