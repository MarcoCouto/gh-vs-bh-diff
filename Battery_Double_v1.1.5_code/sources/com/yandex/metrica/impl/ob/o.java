package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public class o extends cf<Boolean> {
    public o(@NonNull Context context, @NonNull String str) {
        super(context, str, "bool");
    }

    /* access modifiers changed from: protected */
    @Nullable
    /* renamed from: a */
    public Boolean b(int i) {
        return Boolean.valueOf(this.f4723a.getResources().getBoolean(i));
    }
}
