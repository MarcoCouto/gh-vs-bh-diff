package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;

public class vd extends ut {

    /* renamed from: a reason: collision with root package name */
    private int f5270a;
    private ut b;

    public vd(@NonNull Context context, @NonNull xh xhVar) {
        this(context.getApplicationContext(), new yc(), xhVar);
    }

    @VisibleForTesting
    vd(Context context, @NonNull yc ycVar, @NonNull xh xhVar) {
        if (ycVar.b(context, "android.hardware.telephony")) {
            this.b = new ux(context, xhVar);
        } else {
            this.b = new uy();
        }
    }

    public synchronized void a() {
        this.f5270a++;
        if (this.f5270a == 1) {
            this.b.a();
        }
    }

    public synchronized void b() {
        this.f5270a--;
        if (this.f5270a == 0) {
            this.b.b();
        }
    }

    public synchronized void a(vg vgVar) {
        this.b.a(vgVar);
    }

    public synchronized void a(uv uvVar) {
        this.b.a(uvVar);
    }

    public void a(boolean z) {
        this.b.a(z);
    }

    public void a(@NonNull uk ukVar) {
        this.b.a(ukVar);
    }
}
