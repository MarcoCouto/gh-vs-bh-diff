package com.yandex.metrica.impl.ob;

import android.net.Uri.Builder;
import android.support.annotation.NonNull;
import io.fabric.sdk.android.services.network.HttpRequest;

public class cr extends bs<ta> {
    private final ul j;
    private boolean k;
    private ue l;
    @NonNull
    private final su m;

    public boolean o() {
        return true;
    }

    public cr(ul ulVar, su suVar) {
        this(ulVar, suVar, new ta(new ss()));
    }

    public cr(ul ulVar, su suVar, @NonNull ta taVar) {
        super(new cp(ulVar, suVar), taVar);
        this.k = false;
        this.j = ulVar;
        this.m = suVar;
        a(this.m.a());
    }

    public boolean a() {
        if (this.h >= 0) {
            return false;
        }
        b(false);
        a(HttpRequest.HEADER_ACCEPT_ENCODING, "encrypted");
        return this.j.c();
    }

    /* access modifiers changed from: protected */
    public void a(@NonNull Builder builder) {
        ((ta) this.i).a(builder, this.m);
    }

    public boolean b() {
        if (E()) {
            return true;
        }
        if (200 != this.e) {
            return false;
        }
        boolean b = super.b();
        if (b) {
            return b;
        }
        this.l = ue.PARSE;
        return b;
    }

    public void a(Throwable th) {
        this.l = ue.NETWORK;
    }

    public void g() {
        super.g();
        this.l = ue.NETWORK;
    }

    public void f() {
        if (!x() && y()) {
            if (this.l == null) {
                this.l = ue.UNKNOWN;
            }
            this.j.a(this.l);
        }
    }

    public synchronized void b(boolean z) {
        this.k = z;
    }

    /* access modifiers changed from: 0000 */
    public synchronized boolean E() {
        return this.k;
    }

    @NonNull
    public String n() {
        StringBuilder sb = new StringBuilder();
        sb.append("Startup task for component: ");
        sb.append(this.j.b().toString());
        return sb.toString();
    }
}
