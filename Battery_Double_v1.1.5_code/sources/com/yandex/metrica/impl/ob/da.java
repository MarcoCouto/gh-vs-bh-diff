package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.metrica.IIdentifierCallback;
import com.yandex.metrica.YandexMetricaConfig;
import com.yandex.metrica.impl.interact.DeviceInfo;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;

public final class da {

    /* renamed from: a reason: collision with root package name */
    static rz f4768a = new rz(db.a());

    @Deprecated
    public static void a(IIdentifierCallback iIdentifierCallback, @NonNull List<String> list) {
        f4768a.a(iIdentifierCallback, list);
    }

    public static void a(@NonNull Context context, @NonNull IIdentifierCallback iIdentifierCallback, @NonNull List<String> list) {
        f4768a.a(context, iIdentifierCallback, list);
    }

    public static boolean a() {
        return f4768a.a();
    }

    @Nullable
    public static Future<String> b() {
        return f4768a.b();
    }

    @NonNull
    public static DeviceInfo a(Context context) {
        return f4768a.a(context);
    }

    @NonNull
    public static String b(Context context) {
        return f4768a.b(context);
    }

    @Nullable
    public static Integer c(Context context) {
        return f4768a.c(context);
    }

    @Nullable
    @Deprecated
    public static String c() {
        return f4768a.d();
    }

    @Nullable
    public static String d(@NonNull Context context) {
        return f4768a.d(context);
    }

    @Nullable
    public static String e(@NonNull Context context) {
        return f4768a.e(context);
    }

    @NonNull
    public static String f(@NonNull Context context) {
        return f4768a.f(context);
    }

    public static void a(int i, String str, String str2, Map<String, String> map) {
        f4768a.a(i, str, str2, map);
    }

    @Nullable
    public static Future<Boolean> d() {
        return f4768a.c();
    }

    public static void e() {
        f4768a.e();
    }

    @NonNull
    public static String a(@Nullable String str) {
        return f4768a.a(str);
    }

    @NonNull
    public static String a(int i) {
        return f4768a.a(i);
    }

    @NonNull
    public static YandexMetricaConfig a(@NonNull YandexMetricaConfig yandexMetricaConfig, @NonNull String str) {
        return f4768a.a(yandexMetricaConfig, str);
    }

    @NonNull
    public static YandexMetricaConfig a(@NonNull YandexMetricaConfig yandexMetricaConfig, @NonNull List<String> list) {
        return f4768a.a(yandexMetricaConfig, list);
    }

    public static void a(@NonNull Context context, @NonNull Object obj) {
        f4768a.a(context, obj);
    }

    public static void b(@NonNull Context context, @NonNull Object obj) {
        f4768a.b(context, obj);
    }

    @Nullable
    public static Location g(Context context) {
        return f4768a.g(context);
    }
}
