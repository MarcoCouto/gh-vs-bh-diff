package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.util.Collections;
import java.util.List;

public class uk {
    @Nullable
    public final List<pu> A;
    @NonNull
    public final ua B;
    @Nullable
    public final ty C;
    public final long D;
    public final long E;
    public final boolean F;
    @Nullable
    public final tt G;
    @Nullable

    /* renamed from: a reason: collision with root package name */
    public final String f5236a;
    @Nullable
    public final String b;
    @Nullable
    public final String c;
    @Nullable
    public final String d;
    @Nullable
    public final List<String> e;
    @Nullable
    public final String f;
    @Nullable
    public final String g;
    @Nullable
    public final String h;
    @Nullable
    public final List<String> i;
    @Nullable
    public final List<String> j;
    @Nullable
    public final List<String> k;
    @Nullable
    public final List<String> l;
    @Nullable
    public final String m;
    @Nullable
    public final String n;
    @NonNull
    public final tv o;
    @Nullable
    public final oh p;
    @Nullable
    public final oc q;
    @Nullable
    public final ub r;
    @Nullable
    public final String s;
    public final long t;
    public final boolean u;
    public final boolean v;
    @Nullable
    public final List<com.yandex.metrica.impl.ob.cq.a> w;
    @Nullable
    public final String x;
    @Nullable
    public final um y;
    @Nullable
    public final tz z;

    public static class a {
        /* access modifiers changed from: private */
        @Nullable
        public List<com.yandex.metrica.impl.ob.cq.a> A;
        /* access modifiers changed from: private */
        @Nullable
        public String B;
        /* access modifiers changed from: private */
        @Nullable
        public List<pu> C;
        /* access modifiers changed from: private */
        @NonNull
        public ua D;
        /* access modifiers changed from: private */
        public long E;
        /* access modifiers changed from: private */
        public long F;
        /* access modifiers changed from: private */
        @Nullable
        public ty G;
        @Nullable

        /* renamed from: a reason: collision with root package name */
        String f5237a;
        @Nullable
        String b;
        @Nullable
        String c;
        @Nullable
        String d;
        @Nullable
        List<String> e;
        @Nullable
        String f;
        @Nullable
        String g;
        @Nullable
        String h;
        @Nullable
        List<String> i;
        @Nullable
        List<String> j;
        @Nullable
        List<String> k;
        @Nullable
        List<String> l;
        @Nullable
        String m;
        @Nullable
        String n;
        @NonNull
        final tv o;
        @Nullable
        oh p;
        @Nullable
        oc q;
        @Nullable
        ub r;
        @Nullable
        tz s;
        long t;
        boolean u;
        @Nullable
        String v;
        boolean w;
        @Nullable
        um x;
        boolean y;
        @Nullable
        tt z;

        public a(@NonNull tv tvVar) {
            this.o = tvVar;
        }

        public a a(@Nullable String str) {
            this.f5237a = str;
            return this;
        }

        public a b(@Nullable String str) {
            this.b = str;
            return this;
        }

        public a c(@Nullable String str) {
            this.c = str;
            return this;
        }

        public a d(@Nullable String str) {
            this.d = str;
            return this;
        }

        public a a(@Nullable List<String> list) {
            this.e = list;
            return this;
        }

        public a e(@Nullable String str) {
            this.f = str;
            return this;
        }

        public a f(@Nullable String str) {
            this.g = str;
            return this;
        }

        public a g(@Nullable String str) {
            this.h = str;
            return this;
        }

        public a b(@Nullable List<String> list) {
            this.i = list;
            return this;
        }

        public a c(@Nullable List<String> list) {
            this.j = list;
            return this;
        }

        public a d(@Nullable List<String> list) {
            this.k = list;
            return this;
        }

        public a e(@Nullable List<String> list) {
            this.l = list;
            return this;
        }

        public a h(@Nullable String str) {
            this.m = str;
            return this;
        }

        public a i(@Nullable String str) {
            this.n = str;
            return this;
        }

        public a a(@Nullable oh ohVar) {
            this.p = ohVar;
            return this;
        }

        public a a(@Nullable oc ocVar) {
            this.q = ocVar;
            return this;
        }

        public a a(@Nullable ub ubVar) {
            this.r = ubVar;
            return this;
        }

        public a a(@Nullable tz tzVar) {
            this.s = tzVar;
            return this;
        }

        public a j(@Nullable String str) {
            this.v = str;
            return this;
        }

        public a a(long j2) {
            this.t = j2;
            return this;
        }

        public a a(boolean z2) {
            this.u = z2;
            return this;
        }

        public a b(boolean z2) {
            this.w = z2;
            return this;
        }

        public a f(@Nullable List<com.yandex.metrica.impl.ob.cq.a> list) {
            this.A = list;
            return this;
        }

        public a k(@Nullable String str) {
            this.B = str;
            return this;
        }

        public a g(@Nullable List<pu> list) {
            this.C = list;
            return this;
        }

        public a a(@NonNull ua uaVar) {
            this.D = uaVar;
            return this;
        }

        public a b(long j2) {
            this.E = j2;
            return this;
        }

        public a c(long j2) {
            this.F = j2;
            return this;
        }

        public a a(um umVar) {
            this.x = umVar;
            return this;
        }

        public a c(boolean z2) {
            this.y = z2;
            return this;
        }

        public a a(@Nullable ty tyVar) {
            this.G = tyVar;
            return this;
        }

        public a a(@Nullable tt ttVar) {
            this.z = ttVar;
            return this;
        }

        @NonNull
        public uk a() {
            return new uk(this);
        }
    }

    private uk(@NonNull a aVar) {
        List<String> list;
        List<String> list2;
        List<String> list3;
        this.f5236a = aVar.f5237a;
        this.b = aVar.b;
        this.c = aVar.c;
        this.d = aVar.d;
        List<com.yandex.metrica.impl.ob.cq.a> list4 = null;
        this.e = aVar.e == null ? null : Collections.unmodifiableList(aVar.e);
        this.f = aVar.f;
        this.g = aVar.g;
        this.h = aVar.h;
        this.i = aVar.i == null ? null : Collections.unmodifiableList(aVar.i);
        if (aVar.j == null) {
            list = null;
        } else {
            list = Collections.unmodifiableList(aVar.j);
        }
        this.j = list;
        if (aVar.k == null) {
            list2 = null;
        } else {
            list2 = Collections.unmodifiableList(aVar.k);
        }
        this.k = list2;
        if (aVar.l == null) {
            list3 = null;
        } else {
            list3 = Collections.unmodifiableList(aVar.l);
        }
        this.l = list3;
        this.m = aVar.m;
        this.n = aVar.n;
        this.o = aVar.o;
        this.p = aVar.p;
        this.q = aVar.q;
        this.r = aVar.r;
        this.z = aVar.s;
        this.s = aVar.v;
        this.t = aVar.t;
        this.u = aVar.u;
        this.v = aVar.w;
        if (aVar.A != null) {
            list4 = Collections.unmodifiableList(aVar.A);
        }
        this.w = list4;
        this.x = aVar.B;
        this.A = aVar.C;
        this.B = aVar.D;
        this.y = aVar.x;
        this.D = aVar.E;
        this.E = aVar.F;
        this.F = aVar.y;
        this.C = aVar.G;
        this.G = aVar.z;
    }

    public a a() {
        return new a(this.o).a(this.f5236a).b(this.b).c(this.c).d(this.d).c(this.j).d(this.k).h(this.m).a(this.e).b(this.i).e(this.f).f(this.g).g(this.h).e(this.l).j(this.s).a(this.p).a(this.q).a(this.r).i(this.n).b(this.v).a(this.t).a(this.u).f(this.w).k(this.x).g(this.A).a(this.z).a(this.B).b(this.D).c(this.E).a(this.y).c(this.F).a(this.C).a(this.G);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("StartupState{uuid='");
        sb.append(this.f5236a);
        sb.append('\'');
        sb.append(", deviceID='");
        sb.append(this.b);
        sb.append('\'');
        sb.append(", deviceID2='");
        sb.append(this.c);
        sb.append('\'');
        sb.append(", deviceIDHash='");
        sb.append(this.d);
        sb.append('\'');
        sb.append(", reportUrls=");
        sb.append(this.e);
        sb.append(", getAdUrl='");
        sb.append(this.f);
        sb.append('\'');
        sb.append(", reportAdUrl='");
        sb.append(this.g);
        sb.append('\'');
        sb.append(", sdkListUrl='");
        sb.append(this.h);
        sb.append('\'');
        sb.append(", locationUrls=");
        sb.append(this.i);
        sb.append(", hostUrlsFromStartup=");
        sb.append(this.j);
        sb.append(", hostUrlsFromClient=");
        sb.append(this.k);
        sb.append(", diagnosticUrls=");
        sb.append(this.l);
        sb.append(", encodedClidsFromResponse='");
        sb.append(this.m);
        sb.append('\'');
        sb.append(", lastStartupRequestClids='");
        sb.append(this.n);
        sb.append('\'');
        sb.append(", collectingFlags=");
        sb.append(this.o);
        sb.append(", foregroundLocationCollectionConfig=");
        sb.append(this.p);
        sb.append(", backgroundLocationCollectionConfig=");
        sb.append(this.q);
        sb.append(", socketConfig=");
        sb.append(this.r);
        sb.append(", distributionReferrer='");
        sb.append(this.s);
        sb.append('\'');
        sb.append(", obtainTime=");
        sb.append(this.t);
        sb.append(", hadFirstStartup=");
        sb.append(this.u);
        sb.append(", startupResponseClidsMatchClientClids=");
        sb.append(this.v);
        sb.append(", requests=");
        sb.append(this.w);
        sb.append(", countryInit='");
        sb.append(this.x);
        sb.append('\'');
        sb.append(", statSending=");
        sb.append(this.y);
        sb.append(", permissionsCollectingConfig=");
        sb.append(this.z);
        sb.append(", permissions=");
        sb.append(this.A);
        sb.append(", sdkFingerprintingConfig=");
        sb.append(this.B);
        sb.append(", identityLightCollectingConfig=");
        sb.append(this.C);
        sb.append(", obtainServerTime=");
        sb.append(this.D);
        sb.append(", firstStartupServerTime=");
        sb.append(this.E);
        sb.append(", outdated=");
        sb.append(this.F);
        sb.append(", bleCollectingConfig=");
        sb.append(this.G);
        sb.append('}');
        return sb.toString();
    }
}
