package com.yandex.metrica.impl.ob;

import android.annotation.TargetApi;
import android.bluetooth.le.ScanResult;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import android.util.LruCache;
import com.yandex.metrica.d;
import java.util.Iterator;
import java.util.List;

@TargetApi(21)
public class dm {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Context f4789a;
    @NonNull
    private final dj b;
    @NonNull
    private final LruCache<String, Long> c;
    @NonNull
    private final wh d;
    private final long e;

    public dm(@NonNull Context context, long j) {
        this(context, j, new dj(), new wg());
    }

    public void a(int i) {
        try {
            tl.a(this.f4789a).reportEvent("beacon_scan_error", this.b.a(i).toString());
        } catch (Throwable unused) {
        }
    }

    public void a(@NonNull List<ScanResult> list) {
        boolean z;
        Iterator it = list.iterator();
        loop0:
        while (true) {
            z = false;
            while (true) {
                if (!it.hasNext()) {
                    break loop0;
                } else if (b((ScanResult) it.next(), null) || z) {
                    z = true;
                }
            }
        }
        if (z) {
            a().sendEventsBuffer();
        }
    }

    public void a(@NonNull ScanResult scanResult, @Nullable Integer num) {
        if (b(scanResult, num)) {
            a().sendEventsBuffer();
        }
    }

    private boolean b(@NonNull ScanResult scanResult, @Nullable Integer num) {
        try {
            if (!a(scanResult)) {
                return false;
            }
            a().reportEvent("beacon_scan_result", this.b.a(scanResult, num).toString());
            return true;
        } catch (Exception unused) {
            return false;
        }
    }

    private d a() {
        return tl.a(this.f4789a);
    }

    private boolean a(@NonNull ScanResult scanResult) {
        boolean z = false;
        if (scanResult.getDevice() != null) {
            String address = scanResult.getDevice().getAddress();
            long a2 = this.d.a();
            if (!TextUtils.isEmpty(address)) {
                Long l = (Long) this.c.get(address);
                if (l == null || a2 - l.longValue() > this.e) {
                    z = true;
                }
                if (z) {
                    this.c.put(address, Long.valueOf(a2));
                }
            }
        }
        return z;
    }

    @VisibleForTesting
    dm(@NonNull Context context, long j, @NonNull dj djVar, @NonNull wh whVar) {
        this.f4789a = context;
        this.e = j;
        this.b = djVar;
        this.d = whVar;
        this.c = new LruCache<>(1000);
    }
}
