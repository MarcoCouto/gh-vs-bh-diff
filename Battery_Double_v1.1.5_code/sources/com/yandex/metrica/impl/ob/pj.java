package com.yandex.metrica.impl.ob;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public abstract class pj {

    /* renamed from: a reason: collision with root package name */
    private final String f5061a;

    public static final class a {

        /* renamed from: a reason: collision with root package name */
        public static final int f5062a = ((int) TimeUnit.SECONDS.toMillis(30));
    }

    public abstract boolean b();

    public pj(String str) {
        this.f5061a = str;
    }

    public HttpURLConnection a() throws IOException {
        HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(this.f5061a).openConnection();
        httpURLConnection.setConnectTimeout(a.f5062a);
        httpURLConnection.setReadTimeout(a.f5062a);
        httpURLConnection.setDoInput(true);
        httpURLConnection.setRequestProperty("Accept", "application/json");
        httpURLConnection.setRequestProperty("User-Agent", ci.a("com.yandex.mobile.metrica.sdk"));
        return httpURLConnection;
    }
}
