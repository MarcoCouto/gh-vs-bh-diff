package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;

public class hn extends hd {

    /* renamed from: a reason: collision with root package name */
    private lu f4876a;

    public hn(en enVar) {
        this(enVar, enVar.u());
    }

    @VisibleForTesting
    hn(en enVar, lu luVar) {
        super(enVar);
        this.f4876a = luVar;
    }

    public boolean a(@NonNull w wVar) {
        w wVar2;
        en a2 = a();
        if (!this.f4876a.c()) {
            if (a2.i().P()) {
                wVar2 = w.f(wVar);
            } else {
                wVar2 = w.d(wVar);
            }
            a2.e().e(wVar2.c(this.f4876a.d("")));
            this.f4876a.a();
            this.f4876a.e();
        }
        return false;
    }
}
