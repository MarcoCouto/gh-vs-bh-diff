package com.yandex.metrica.impl.ob;

import android.annotation.TargetApi;
import android.telephony.SubscriptionInfo;

public final class vb {

    /* renamed from: a reason: collision with root package name */
    private final Integer f5264a;
    private final Integer b;
    private final boolean c;
    private final String d;
    private final String e;

    public vb(Integer num, Integer num2, boolean z, String str, String str2) {
        this.f5264a = num;
        this.b = num2;
        this.c = z;
        this.d = str;
        this.e = str2;
    }

    @TargetApi(23)
    public vb(SubscriptionInfo subscriptionInfo) {
        this.f5264a = Integer.valueOf(subscriptionInfo.getMcc());
        this.b = Integer.valueOf(subscriptionInfo.getMnc());
        boolean z = true;
        if (subscriptionInfo.getDataRoaming() != 1) {
            z = false;
        }
        this.c = z;
        this.d = subscriptionInfo.getCarrierName().toString();
        this.e = subscriptionInfo.getIccId();
    }

    public Integer a() {
        return this.f5264a;
    }

    public Integer b() {
        return this.b;
    }

    public boolean c() {
        return this.c;
    }

    public String d() {
        return this.d;
    }

    public String e() {
        return this.e;
    }
}
