package com.yandex.metrica.impl.ob;

import android.support.annotation.Nullable;

public class l {
    @Nullable

    /* renamed from: a reason: collision with root package name */
    public final a f4958a;
    @Nullable
    public final Boolean b;

    public enum a {
        ACTIVE,
        WORKING_SET,
        FREQUENT,
        RARE
    }

    public l(@Nullable a aVar, @Nullable Boolean bool) {
        this.f4958a = aVar;
        this.b = bool;
    }

    public boolean equals(Object obj) {
        boolean z = true;
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        l lVar = (l) obj;
        if (this.f4958a != lVar.f4958a) {
            return false;
        }
        if (this.b != null) {
            z = this.b.equals(lVar.b);
        } else if (lVar.b != null) {
            z = false;
        }
        return z;
    }

    public int hashCode() {
        int i = 0;
        int hashCode = (this.f4958a != null ? this.f4958a.hashCode() : 0) * 31;
        if (this.b != null) {
            i = this.b.hashCode();
        }
        return hashCode + i;
    }
}
