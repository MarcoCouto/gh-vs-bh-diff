package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.text.TextUtils;
import com.yandex.metrica.impl.ob.rh.c;
import com.yandex.metrica.impl.ob.rh.c.e;
import com.yandex.metrica.impl.ob.rh.c.e.b;
import com.yandex.metrica.impl.ob.rh.c.g;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class gh {

    /* renamed from: a reason: collision with root package name */
    public static final Map<Integer, Integer> f4849a = Collections.unmodifiableMap(new HashMap<Integer, Integer>() {
        {
            put(Integer.valueOf(com.yandex.metrica.impl.ob.af.a.EVENT_TYPE_DIAGNOSTIC.a()), Integer.valueOf(22));
            put(Integer.valueOf(com.yandex.metrica.impl.ob.af.a.EVENT_TYPE_DIAGNOSTIC_STATBOX.a()), Integer.valueOf(23));
            put(Integer.valueOf(com.yandex.metrica.impl.ob.af.a.EVENT_TYPE_DIAGNOSTIC_DISABLE_STAT_SENDING.a()), Integer.valueOf(24));
        }
    });
    @NonNull
    private final w b;
    @NonNull
    private final gi c;
    @NonNull
    private final gk d;
    @NonNull
    private final xy e;
    @NonNull
    private final xy f;
    @NonNull
    private final wh g;
    @NonNull
    private final er h;

    public static class a {
        public gh a(@NonNull w wVar, @NonNull gi giVar, @NonNull gk gkVar, @NonNull lw lwVar) {
            return new gh(wVar, giVar, gkVar, lwVar);
        }
    }

    public gh(@NonNull w wVar, @NonNull gi giVar, @NonNull gk gkVar, @NonNull lw lwVar) {
        this(wVar, giVar, gkVar, new er(lwVar), new xy(1024, "diagnostic event name"), new xy(204800, "diagnostic event value"), new wg());
    }

    public gh(@NonNull w wVar, @NonNull gi giVar, @NonNull gk gkVar, @NonNull er erVar, @NonNull xy xyVar, @NonNull xy xyVar2, @NonNull wh whVar) {
        this.b = wVar;
        this.c = giVar;
        this.d = gkVar;
        this.h = erVar;
        this.f = xyVar;
        this.e = xyVar2;
        this.g = whVar;
    }

    public byte[] a() {
        c cVar = new c();
        e eVar = new e();
        int i = 0;
        cVar.b = new e[]{eVar};
        com.yandex.metrica.impl.ob.gk.a a2 = this.d.a();
        eVar.b = a2.f4853a;
        eVar.c = new b();
        eVar.c.d = 2;
        eVar.c.b = new g();
        eVar.c.b.b = a2.b;
        eVar.c.b.c = wi.a(a2.b);
        eVar.c.c = this.c.A();
        com.yandex.metrica.impl.ob.rh.c.e.a aVar = new com.yandex.metrica.impl.ob.rh.c.e.a();
        eVar.d = new com.yandex.metrica.impl.ob.rh.c.e.a[]{aVar};
        aVar.b = (long) a2.c;
        aVar.q = (long) this.h.a(this.b.g());
        aVar.c = this.g.b() - a2.b;
        aVar.d = ((Integer) f4849a.get(Integer.valueOf(this.b.g()))).intValue();
        if (!TextUtils.isEmpty(this.b.d())) {
            aVar.e = this.f.a(this.b.d());
        }
        if (!TextUtils.isEmpty(this.b.e())) {
            String e2 = this.b.e();
            String a3 = this.e.a(e2);
            if (!TextUtils.isEmpty(a3)) {
                aVar.f = a3.getBytes();
            }
            int length = e2.getBytes().length;
            if (aVar.f != null) {
                i = aVar.f.length;
            }
            aVar.k = length - i;
        }
        return e.a((e) cVar);
    }
}
