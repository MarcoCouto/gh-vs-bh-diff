package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;

public class hg extends hd {

    /* renamed from: a reason: collision with root package name */
    private lu f4872a;
    private wd b;

    public hg(en enVar) {
        this(enVar, enVar.u(), wd.a());
    }

    @VisibleForTesting
    hg(en enVar, lu luVar, wd wdVar) {
        super(enVar);
        this.f4872a = luVar;
        this.b = wdVar;
    }

    public boolean a(@NonNull w wVar) {
        en a2 = a();
        st i = a2.i();
        if (!this.f4872a.d()) {
            if (!this.f4872a.c()) {
                if (i.P()) {
                    this.b.c();
                }
                String e = wVar.e();
                this.f4872a.c(e);
                a2.e().a(w.e(wVar).c(e));
                this.f4872a.a(i.Q());
                a().z().a();
            }
            this.f4872a.b();
        }
        return false;
    }
}
