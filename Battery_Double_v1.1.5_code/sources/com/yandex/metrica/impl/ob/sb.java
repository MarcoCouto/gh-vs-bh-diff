package com.yandex.metrica.impl.ob;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import com.yandex.metrica.AppMetricaDeviceIDListener;
import com.yandex.metrica.DeferredDeeplinkParametersListener;
import com.yandex.metrica.IReporter;
import com.yandex.metrica.ReporterConfig;
import com.yandex.metrica.Revenue;
import com.yandex.metrica.YandexMetricaConfig;
import com.yandex.metrica.f;
import com.yandex.metrica.g;
import com.yandex.metrica.j;
import com.yandex.metrica.profile.UserProfile;
import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public final class sb extends ru<rx<se>> {
    private final sd b;
    private final sc c;

    public sb(@NonNull xh xhVar) {
        this(new sa(), xhVar, new ry<rx<se>>(xhVar) {
            /* access modifiers changed from: protected */
            public rx<se> a(@NonNull xh xhVar, @NonNull Context context, @NonNull String str) {
                return new rx<>(xhVar, context, str, new se());
            }
        }, new sd(), new sc());
    }

    private sb(@NonNull sa saVar, @NonNull xh xhVar, @NonNull ry<rx<se>> ryVar, @NonNull sd sdVar, @NonNull sc scVar) {
        this(saVar, xhVar, ryVar, sdVar, scVar, new rt(saVar), new g(saVar));
    }

    @VisibleForTesting
    sb(@NonNull sa saVar, @NonNull xh xhVar, @NonNull ry<rx<se>> ryVar, @NonNull sd sdVar, @NonNull sc scVar, @NonNull rt rtVar, @NonNull g gVar) {
        super(saVar, xhVar, ryVar, rtVar, gVar);
        this.c = scVar;
        this.b = sdVar;
    }

    public void a(@NonNull final Context context, @NonNull YandexMetricaConfig yandexMetricaConfig) {
        this.b.a(context, yandexMetricaConfig);
        final j a2 = this.c.a(j.a(yandexMetricaConfig));
        this.f5096a.a(context, (YandexMetricaConfig) a2);
        a().a((Runnable) new Runnable() {
            public void run() {
                sb.this.c().b(context, j.a(a2));
            }
        });
        c().b();
    }

    public void e() {
        d().a();
        this.b.sendEventsBuffer();
        this.f5096a.a();
        a().a((Runnable) new Runnable() {
            public void run() {
                sb.this.c().e().sendEventsBuffer();
            }
        });
    }

    public void a(@Nullable final Activity activity) {
        d().a();
        this.b.resumeSession();
        this.f5096a.a(activity);
        a().a((Runnable) new Runnable() {
            public void run() {
                sb.this.c().e().b(activity);
            }
        });
    }

    public void b(@Nullable final Activity activity) {
        d().a();
        this.b.pauseSession();
        this.f5096a.b(activity);
        a().a((Runnable) new Runnable() {
            public void run() {
                sb.this.c().e().c(activity);
            }
        });
    }

    public void a(@NonNull final Application application) {
        d().a();
        this.b.a(application);
        this.f5096a.a(application);
        a().a((Runnable) new Runnable() {
            public void run() {
                sb.this.c().e().a(application, sb.this.a());
            }
        });
    }

    public void a(@NonNull final String str) {
        d().a();
        this.b.reportEvent(str);
        this.f5096a.a(str);
        a().a((Runnable) new Runnable() {
            public void run() {
                sb.this.c().e().reportEvent(str);
            }
        });
    }

    public void a(@NonNull final String str, @Nullable final String str2) {
        d().a();
        this.b.reportEvent(str, str2);
        this.f5096a.a(str, str2);
        a().a((Runnable) new Runnable() {
            public void run() {
                sb.this.c().e().reportEvent(str, str2);
            }
        });
    }

    public void a(@NonNull final String str, @Nullable Map<String, Object> map) {
        final List list;
        d().a();
        this.b.reportEvent(str, map);
        this.f5096a.a(str, map);
        if (map == null) {
            list = null;
        } else {
            list = a(map);
        }
        a().a((Runnable) new Runnable() {
            public void run() {
                LinkedHashMap linkedHashMap = new LinkedHashMap();
                if (list != null) {
                    for (Entry entry : list) {
                        linkedHashMap.put(entry.getKey(), entry.getValue());
                    }
                }
                sb.this.c().e().reportEvent(str, (Map<String, Object>) linkedHashMap);
            }
        });
    }

    public void a(@NonNull final String str, @Nullable final Throwable th) {
        d().a();
        this.b.reportError(str, th);
        this.f5096a.a(str, th);
        a().a((Runnable) new Runnable() {
            public void run() {
                sb.this.c().e().reportError(str, th);
            }
        });
    }

    public void a(@NonNull final Throwable th) {
        d().a();
        this.b.reportUnhandledException(th);
        this.f5096a.a(th);
        a().a((Runnable) new Runnable() {
            public void run() {
                sb.this.c().e().reportUnhandledException(th);
            }
        });
    }

    public void b(@NonNull final String str) {
        d().a();
        this.b.a(str);
        this.f5096a.c(str);
        a().a((Runnable) new Runnable() {
            public void run() {
                sb.this.c().e().c(str);
            }
        });
    }

    public void c(@NonNull final Activity activity) {
        d().a();
        this.b.a(activity);
        this.f5096a.c(activity);
        a().a((Runnable) new Runnable() {
            public void run() {
                sb.this.c().e().a(activity);
            }
        });
    }

    public void c(@NonNull final String str) {
        d().a();
        this.b.b(str);
        this.f5096a.d(str);
        a().a((Runnable) new Runnable() {
            public void run() {
                sb.this.c().e().e(str);
            }
        });
    }

    @Deprecated
    public void d(@NonNull final String str) {
        d().a();
        this.b.c(str);
        a().a((Runnable) new Runnable() {
            public void run() {
                sb.this.c().e().f(str);
            }
        });
    }

    public void a(@Nullable final Location location) {
        this.b.a(location);
        this.f5096a.a(location);
        a().a((Runnable) new Runnable() {
            public void run() {
                sb.this.c().a(location);
            }
        });
    }

    public void a(final boolean z) {
        this.b.a(z);
        this.f5096a.a(z);
        a().a((Runnable) new Runnable() {
            public void run() {
                sb.this.c().a(z);
            }
        });
    }

    public void a(@NonNull final Context context, final boolean z) {
        this.b.a(context, z);
        this.f5096a.a(context, z);
        a().a((Runnable) new Runnable() {
            public void run() {
                sb.this.c().a(context, z);
            }
        });
    }

    public void b(@NonNull final Context context, final boolean z) {
        this.b.b(context, z);
        this.f5096a.b(context, z);
        a().a((Runnable) new Runnable() {
            public void run() {
                sb.this.c().b(context, z);
            }
        });
    }

    public void e(@Nullable final String str) {
        d().a();
        this.b.setUserProfileID(str);
        this.f5096a.b(str);
        a().a((Runnable) new Runnable() {
            public void run() {
                sb.this.c().e().setUserProfileID(str);
            }
        });
    }

    public void a(@NonNull final UserProfile userProfile) {
        d().a();
        this.b.reportUserProfile(userProfile);
        this.f5096a.a(userProfile);
        a().a((Runnable) new Runnable() {
            public void run() {
                sb.this.c().e().reportUserProfile(userProfile);
            }
        });
    }

    public void a(@NonNull final Revenue revenue) {
        d().a();
        this.b.reportRevenue(revenue);
        this.f5096a.a(revenue);
        a().a((Runnable) new Runnable() {
            public void run() {
                sb.this.c().e().reportRevenue(revenue);
            }
        });
    }

    public void a(@NonNull final DeferredDeeplinkParametersListener deferredDeeplinkParametersListener) {
        d().a();
        this.b.a(deferredDeeplinkParametersListener);
        this.f5096a.a(deferredDeeplinkParametersListener);
        a().a((Runnable) new Runnable() {
            public void run() {
                sb.this.c().f().a(deferredDeeplinkParametersListener);
            }
        });
    }

    public void a(@NonNull final AppMetricaDeviceIDListener appMetricaDeviceIDListener) {
        d().a();
        this.b.a(appMetricaDeviceIDListener);
        this.f5096a.a(appMetricaDeviceIDListener);
        a().a((Runnable) new Runnable() {
            public void run() {
                sb.this.c().f().a(appMetricaDeviceIDListener);
            }
        });
    }

    @NonNull
    public IReporter a(@NonNull Context context, @NonNull String str) {
        this.b.a(context, str);
        return b().a(context, str);
    }

    public void a(@NonNull Context context, @NonNull ReporterConfig reporterConfig) {
        this.b.a(context, reporterConfig);
        f a2 = this.c.a(f.a(reporterConfig));
        this.f5096a.a(context, a2);
        b().a(context, (ReporterConfig) a2);
    }

    @NonNull
    private <K, V> List<Entry<K, V>> a(@NonNull Map<K, V> map) {
        ArrayList arrayList = new ArrayList(map.size());
        for (Entry simpleEntry : map.entrySet()) {
            arrayList.add(new SimpleEntry(simpleEntry));
        }
        return arrayList;
    }
}
