package com.yandex.metrica.impl.ob;

import android.annotation.TargetApi;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import com.yandex.metrica.impl.ob.rr.a.b;

@TargetApi(21)
public class df implements dh {
    private static final long g = new b().d;
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Context f4779a;
    @NonNull
    private final dd b;
    /* access modifiers changed from: private */
    @NonNull
    public final dk c;
    /* access modifiers changed from: private */
    @NonNull
    public final di d;
    /* access modifiers changed from: private */
    @NonNull
    public ScanCallback e;
    private long f;

    public df(@NonNull Context context) {
        this(context, new dd(context), new dk(), new di(), new dl(context, g));
    }

    public synchronized void a(@NonNull final tt ttVar) {
        BluetoothLeScanner a2 = this.b.a();
        if (a2 != null) {
            a();
            long j = ttVar.c;
            if (this.f != j) {
                this.f = j;
                this.e = new dl(this.f4779a, this.f);
            }
            cx.a((wn<T>) new wn<BluetoothLeScanner>() {
                public void a(BluetoothLeScanner bluetoothLeScanner) {
                    bluetoothLeScanner.startScan(df.this.d.a(ttVar.b), df.this.c.a(ttVar.f5209a), df.this.e);
                }
            }, a2, "startScan", "BluetoothLeScanner");
        }
    }

    public synchronized void a() {
        BluetoothLeScanner a2 = this.b.a();
        if (a2 != null) {
            cx.a((wn<T>) new wn<BluetoothLeScanner>() {
                public void a(BluetoothLeScanner bluetoothLeScanner) {
                    bluetoothLeScanner.stopScan(df.this.e);
                }
            }, a2, "stopScan", "BluetoothLeScanner");
        }
    }

    @VisibleForTesting
    public df(@NonNull Context context, @NonNull dd ddVar, @NonNull dk dkVar, @NonNull di diVar, @NonNull ScanCallback scanCallback) {
        this.f = g;
        this.f4779a = context;
        this.b = ddVar;
        this.c = dkVar;
        this.d = diVar;
        this.e = scanCallback;
    }
}
