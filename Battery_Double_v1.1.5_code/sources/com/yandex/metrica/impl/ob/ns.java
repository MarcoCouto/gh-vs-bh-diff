package com.yandex.metrica.impl.ob;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import java.io.File;
import org.json.JSONObject;

public class ns {

    /* renamed from: a reason: collision with root package name */
    private final yc f5016a = new yc();

    public void a(@NonNull Context context, @NonNull nq nqVar, @NonNull pr prVar) {
        try {
            nr b = b(context);
            String str = null;
            if (!(b == null || b.c() == null)) {
                str = b.c().a();
            }
            String a2 = new nr(nqVar, new nt(context, str, prVar), System.currentTimeMillis()).a();
            if (a()) {
                a(context, a2);
            }
            a(context, "credentials.dat", a2);
        } catch (Throwable unused) {
        }
    }

    @SuppressLint({"WorldReadableFiles"})
    private void a(Context context, String str, String str2) {
        am.a(context, str, str2);
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public void a(Context context, String str) {
        am.b(context, "credentials.dat", str);
    }

    @Nullable
    public nq a(@NonNull Context context) {
        nr b = b(context);
        if (b == null) {
            return null;
        }
        return b.b();
    }

    @Nullable
    private nr b(Context context) {
        nr b = b(context, context.getPackageName());
        if (b == null) {
            return null;
        }
        if (!a()) {
            return b;
        }
        nr c = c(context, context.getPackageName());
        if (c == null) {
            c = b;
        }
        return c;
    }

    /* access modifiers changed from: 0000 */
    public boolean a() {
        return am.a();
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public nr b(Context context, String str) {
        return a(context, str, context.getFileStreamPath("credentials.dat"));
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    @TargetApi(21)
    public nr c(Context context, String str) {
        return a(context, str, new File(context.getNoBackupFilesDir(), "credentials.dat"));
    }

    @Nullable
    private nr a(Context context, String str, File file) {
        ApplicationInfo b = this.f5016a.b(context, str, 8192);
        if (b != null) {
            return b(context, str, a(file, context.getApplicationInfo().dataDir, b.dataDir));
        }
        return null;
    }

    @Nullable
    private nr b(Context context, String str, String str2) {
        nr nrVar = null;
        try {
            File file = new File(str2);
            if (file.exists()) {
                String a2 = am.a(context, file);
                if (a2 != null) {
                    nrVar = new nr(new JSONObject(a2), file.lastModified());
                }
                return nrVar;
            }
        } catch (Throwable unused) {
        }
        return null;
    }

    @NonNull
    private String a(File file, String str, String str2) {
        return file.getAbsolutePath().replace(str, str2);
    }
}
