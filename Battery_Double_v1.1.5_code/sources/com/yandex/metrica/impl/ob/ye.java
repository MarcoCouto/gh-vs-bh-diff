package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;

public class ye implements yk<String> {

    /* renamed from: a reason: collision with root package name */
    private final String f5315a;

    public ye(@NonNull String str) {
        this.f5315a = str;
    }

    public yi a(@Nullable String str) {
        if (!TextUtils.isEmpty(str)) {
            return yi.a(this);
        }
        StringBuilder sb = new StringBuilder();
        sb.append(this.f5315a);
        sb.append(" is empty.");
        return yi.a(this, sb.toString());
    }
}
