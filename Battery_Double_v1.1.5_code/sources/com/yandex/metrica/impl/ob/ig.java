package com.yandex.metrica.impl.ob;

public class ig {

    /* renamed from: a reason: collision with root package name */
    private final hi f4883a;
    private final hm b;
    private final ho c;
    private final hq d;
    private final in e;
    private final hr f;
    private final hp g;
    private final hs h;
    private final hb i;
    private final ha j;
    private final hg k;
    private final hl l;
    private final hk m;
    private final hf n;
    private final ht o;
    private final hc p;
    private final hh q;
    private final he r;

    public ig(en enVar) {
        this.f4883a = new hi(enVar);
        this.b = new hm(enVar);
        this.c = new ho(enVar);
        this.d = new hq(enVar);
        this.e = new in(enVar);
        this.f = new hr(enVar);
        this.g = new hp(enVar);
        this.h = new hs(enVar);
        this.i = new hb(enVar);
        this.j = new ha(enVar);
        this.k = new hg(enVar);
        this.l = new hl(enVar);
        this.m = new hk(enVar, new pv());
        this.n = new hf(enVar);
        this.o = new ht(enVar);
        this.p = new hc(enVar);
        this.q = new hh(enVar);
        this.r = new he(enVar, tl.a(enVar.k()));
    }

    public hi a() {
        return this.f4883a;
    }

    public hm b() {
        return this.b;
    }

    public ho c() {
        return this.c;
    }

    public hq d() {
        return this.d;
    }

    public in e() {
        return this.e;
    }

    public hr f() {
        return this.f;
    }

    public hp g() {
        return this.g;
    }

    public hs h() {
        return this.h;
    }

    public hb i() {
        return this.i;
    }

    public ha j() {
        return this.j;
    }

    public hg k() {
        return this.k;
    }

    public hl l() {
        return this.l;
    }

    public hk m() {
        return this.m;
    }

    public hf n() {
        return this.n;
    }

    public ht o() {
        return this.o;
    }

    public hc p() {
        return this.p;
    }

    public hh q() {
        return this.q;
    }

    public he r() {
        return this.r;
    }
}
