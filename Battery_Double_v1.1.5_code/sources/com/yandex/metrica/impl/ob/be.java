package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import com.yandex.metrica.CounterConfiguration;
import com.yandex.metrica.MetricaService.c;
import java.io.File;

public class be implements bc {
    /* access modifiers changed from: private */
    @Nullable

    /* renamed from: a reason: collision with root package name */
    public uk f4669a;
    /* access modifiers changed from: private */
    @NonNull
    public final Context b;
    @NonNull
    private final c c;
    /* access modifiers changed from: private */
    @NonNull
    public final com.yandex.metrica.impl.ob.ts.a d;
    /* access modifiers changed from: private */
    @Nullable
    public ts e;
    /* access modifiers changed from: private */
    @NonNull
    public aq f;
    @NonNull
    private final xh g;
    /* access modifiers changed from: private */
    @NonNull
    public fo h;
    @NonNull
    private final fu i;
    @NonNull
    private final bf j;
    @Nullable
    private ox k;
    @NonNull
    private final ly l;
    private ku m;

    @VisibleForTesting
    final class a implements Runnable {
        private final w b;
        private final Bundle c;
        private final Context d;

        a(Context context, w wVar, Bundle bundle) {
            this.d = context.getApplicationContext();
            this.b = wVar;
            this.c = bundle;
        }

        public void run() {
            be.this.f.a();
            ed edVar = new ed(this.c);
            if (!be.this.a(edVar)) {
                fn a2 = fn.a(edVar);
                if (a2 != null) {
                    eg egVar = new eg(edVar);
                    be.this.h.a(a2, egVar).a(this.b, egVar);
                }
            }
        }
    }

    public be(@NonNull Context context, @NonNull c cVar) {
        this(context, cVar, new fu(context));
    }

    private be(@NonNull Context context, @NonNull c cVar, @NonNull fu fuVar) {
        this(context, cVar, fuVar, new fo(context, fuVar), new bf(), new com.yandex.metrica.impl.ob.ts.a(), new ly(ld.a(context).c()), new ah());
    }

    @VisibleForTesting
    be(@NonNull Context context, @NonNull c cVar, @NonNull fu fuVar, @NonNull fo foVar, @NonNull bf bfVar, @NonNull com.yandex.metrica.impl.ob.ts.a aVar, @NonNull ly lyVar, @NonNull ah ahVar) {
        this.b = context;
        this.c = cVar;
        this.h = foVar;
        this.i = fuVar;
        this.j = bfVar;
        this.d = aVar;
        this.l = lyVar;
        this.g = al.a().j().c();
        this.m = new ku(ahVar.b(this.b), new wm<File>() {
            public void a(File file) {
                be.this.a(file);
            }
        });
    }

    public void a() {
        new cm(this.b).a(this.b);
        wd.a().a(this.b);
        this.k = new ox(oo.a(this.b), al.a().k(), cy.a(this.b), this.l);
        c();
        dr.a().a(this, eb.class, dv.a((du<T>) new du<eb>() {
            public void a(eb ebVar) {
                be.this.b(ebVar.b);
            }
        }).a(new ds<eb>() {
            public boolean a(eb ebVar) {
                return !be.this.b.getPackageName().equals(ebVar.f4805a);
            }
        }).a());
        this.f4669a = (uk) com.yandex.metrica.impl.ob.np.a.a(uk.class).a(this.b).a();
        this.f = new aq(this.l, this.b, this.f4669a.C);
        com.yandex.metrica.impl.ac.a.a().a(this.b, this.f4669a);
        f();
        al.a().e().a();
        al.a().i().a();
        this.m.a();
    }

    private void c() {
        this.j.a((b) new b() {
            public void a() {
                be.this.c(be.this.f4669a);
                be.this.h();
            }
        });
        this.j.b((b) new b() {
            public void a() {
                be.this.c(be.this.f4669a);
                be.this.g();
            }
        });
        this.j.c((b) new b() {
            public void a() {
                be.this.c(be.this.f4669a);
                be.this.j();
                be.this.e = be.this.d.a(be.this.b);
            }
        });
        this.j.d((b) new b() {
            public void a() {
                be.this.d();
            }
        });
        this.j.e((b) new b() {
            public void a() {
                be.this.e();
            }
        });
    }

    public void a(Intent intent, int i2) {
        b(intent, i2);
    }

    public void a(Intent intent, int i2, int i3) {
        b(intent, i3);
    }

    public void a(Intent intent) {
        this.j.a(intent);
    }

    public void b(Intent intent) {
        this.j.b(intent);
    }

    public void c(Intent intent) {
        String str;
        this.j.c(intent);
        if (intent != null) {
            String action = intent.getAction();
            Uri data = intent.getData();
            if (data == null) {
                str = null;
            } else {
                str = data.getEncodedAuthority();
            }
            if ("com.yandex.metrica.IMetricaService".equals(action)) {
                a(data, str);
            }
        }
    }

    /* access modifiers changed from: private */
    public void b(@NonNull uk ukVar) {
        this.f4669a = ukVar;
        i();
        c(ukVar);
        al.a().a(ukVar);
        this.f.a(this.f4669a.C);
    }

    /* access modifiers changed from: private */
    public void d() {
        if (this.e != null) {
            this.e.b();
        }
    }

    /* access modifiers changed from: private */
    public void e() {
        if (this.e != null) {
            this.e.a();
        }
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public void a(@Nullable Uri uri, @Nullable String str) {
        if (uri != null && uri.getPath().equals("/client")) {
            this.h.a(str, Integer.parseInt(uri.getQueryParameter("pid")), uri.getQueryParameter("psid"));
        }
        if (this.h.a() <= 0) {
            e();
        }
    }

    public void b() {
        this.m.b();
        g();
        this.i.c();
        dr.a().a((Object) this);
    }

    public void a(String str, int i2, String str2, Bundle bundle) {
        bundle.setClassLoader(CounterConfiguration.class.getClassLoader());
        a(new w(str2, str, i2), bundle);
    }

    public void a(Bundle bundle) {
        bundle.setClassLoader(CounterConfiguration.class.getClassLoader());
        a(w.b(bundle), bundle);
    }

    public void a(@NonNull File file) {
        this.g.a((Runnable) new ke(this.b, file, new wm<kv>() {
            public void a(kv kvVar) {
                be beVar = be.this;
                fn fnVar = new fn(kvVar.h(), kvVar.g(), kvVar.e(), kvVar.f(), kvVar.i());
                beVar.a(fnVar, af.a(kvVar.b(), kvVar.a(), kvVar.c(), kvVar.d(), vr.a(kvVar.h())), new eg(new com.yandex.metrica.impl.ob.su.a(), new com.yandex.metrica.impl.ob.eg.a(), null));
            }
        }));
    }

    private void f() {
        if (this.f4669a != null) {
            a(this.f4669a);
        }
        c(this.f4669a);
    }

    /* access modifiers changed from: 0000 */
    public void a(@NonNull uk ukVar) {
        ub ubVar = ukVar.r;
        if (ubVar == null) {
            dr.a().a(ea.class);
        } else {
            dr.a().b((dt) new ea(ubVar));
        }
    }

    private void b(Intent intent, int i2) {
        if (intent != null) {
            intent.getExtras().setClassLoader(CounterConfiguration.class.getClassLoader());
            e(intent);
        }
        this.c.a(i2);
    }

    /* access modifiers changed from: private */
    public void g() {
        if (this.k != null) {
            this.k.a((Object) this);
        }
    }

    /* access modifiers changed from: private */
    public void h() {
        if (this.k != null) {
            this.k.b(this);
        }
    }

    private boolean d(Intent intent) {
        return intent == null || intent.getData() == null;
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public boolean a(@Nullable ed edVar) {
        return edVar == null || edVar.g() == null || !this.b.getPackageName().equals(edVar.g().h()) || edVar.g().g() != 85;
    }

    private void e(Intent intent) {
        if (!d(intent)) {
            ed edVar = new ed(intent.getExtras());
            if (!a(edVar)) {
                w b2 = w.b(intent.getExtras());
                if (!b2.m() && !b2.n()) {
                    try {
                        a(fn.a(edVar), b2, new eg(edVar));
                    } catch (Throwable unused) {
                    }
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(@NonNull fn fnVar, @NonNull w wVar, @NonNull eg egVar) {
        this.h.a(fnVar, egVar).a(wVar, egVar);
        this.h.a(fnVar.b(), fnVar.c().intValue(), fnVar.d());
    }

    private void a(w wVar, Bundle bundle) {
        if (!wVar.n()) {
            this.g.a((Runnable) new a(this.b, wVar, bundle));
        }
    }

    /* access modifiers changed from: private */
    public void c(@NonNull uk ukVar) {
        if (this.k != null) {
            this.k.a(ukVar, this.j.c());
        }
    }

    private void i() {
        final jk jkVar = new jk(this.b);
        al.a().j().i().a((Runnable) new Runnable() {
            public void run() {
                try {
                    jkVar.a();
                } catch (Throwable unused) {
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void j() {
        if (this.f4669a != null) {
            al.a().f().a(this.f4669a);
        }
    }
}
