package com.yandex.metrica.impl.ob;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import com.yandex.metrica.IIdentifierCallback;
import com.yandex.metrica.IParamsCallback;
import com.yandex.metrica.IParamsCallback.Reason;
import com.yandex.metrica.impl.ob.x.a;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.WeakHashMap;

public class uf implements ug {

    /* renamed from: a reason: collision with root package name */
    static final Map<ue, Reason> f5232a = Collections.unmodifiableMap(new HashMap<ue, Reason>() {
        {
            put(ue.UNKNOWN, Reason.UNKNOWN);
            put(ue.NETWORK, Reason.NETWORK);
            put(ue.PARSE, Reason.INVALID_RESPONSE);
        }
    });
    private final List<String> b;
    private final cd c;
    private final ui d;
    @NonNull
    private final Handler e;
    @Nullable
    private vz f;
    private final a g;
    private final Object h;
    private final Map<tx, List<String>> i;
    private Map<String, String> j;

    public uf(cd cdVar, lv lvVar, @NonNull Handler handler) {
        this(cdVar, new ui(lvVar), handler);
    }

    @VisibleForTesting
    uf(cd cdVar, @NonNull ui uiVar, @NonNull Handler handler) {
        this.b = Arrays.asList(new String[]{"yandex_mobile_metrica_uuid", "yandex_mobile_metrica_device_id", "appmetrica_device_id_hash", "yandex_mobile_metrica_get_ad_url", "yandex_mobile_metrica_report_ad_url", IParamsCallback.YANDEX_MOBILE_METRICA_CLIDS});
        this.h = new Object();
        this.i = new WeakHashMap();
        this.c = cdVar;
        this.d = uiVar;
        this.e = handler;
        this.g = new a() {
            public void a(int i, @NonNull Bundle bundle) {
            }
        };
    }

    public String a() {
        return this.d.e();
    }

    public String b() {
        return this.d.f();
    }

    public void a(IIdentifierCallback iIdentifierCallback, @NonNull List<String> list, @Nullable Map<String, String> map) {
        a((tx) new tu(iIdentifierCallback), list, map);
    }

    private void a(final tx txVar, @NonNull List<String> list, @Nullable Map<String, String> map) {
        synchronized (this.h) {
            this.d.a(map);
            b(txVar, list);
            if (!this.d.c()) {
                if (this.d.a(list)) {
                    a(txVar);
                }
            }
            a(list, (a) new a() {
                public void a(int i, Bundle bundle) {
                    uf.this.a(i, bundle, txVar);
                }
            }, map);
        }
    }

    public void a(int i2, @NonNull Bundle bundle) {
        a(i2, bundle, (tx) null);
    }

    public void a(int i2, @NonNull Bundle bundle, @Nullable tx txVar) {
        synchronized (this.h) {
            a(bundle, i2);
            d();
            if (txVar != null) {
                a(txVar, bundle);
            }
        }
    }

    public void a(@NonNull vz vzVar) {
        this.f = vzVar;
    }

    private void b(@Nullable Map<String, String> map) {
        a(this.b, map);
    }

    private void a(@NonNull List<String> list, @Nullable Map<String, String> map) {
        a(list, this.g, map);
    }

    private void a(@NonNull List<String> list, @NonNull a aVar, @Nullable Map<String, String> map) {
        this.c.a(list, (ResultReceiver) new x(this.e, aVar), map);
    }

    private void a(@NonNull Bundle bundle, int i2) {
        this.d.a(bundle);
        if (i2 == 1) {
            this.d.a(wi.b());
        }
        d();
    }

    public void c() {
        synchronized (this.h) {
            if (!this.d.b() || this.d.c()) {
                b(this.j);
            }
        }
    }

    public void a(List<String> list) {
        synchronized (this.h) {
            List d2 = this.d.d();
            if (cx.a((Collection) list)) {
                if (!cx.a((Collection) d2)) {
                    this.d.b(null);
                    this.c.a(null);
                }
            } else if (!cx.a((Object) list, (Object) d2)) {
                this.d.b(list);
                this.c.a(list);
            } else {
                this.c.a(d2);
            }
        }
    }

    public void a(Map<String, String> map) {
        synchronized (this.h) {
            Map<String, String> c2 = we.c(map);
            this.j = c2;
            this.c.a(c2);
            this.d.a(c2);
        }
    }

    public void a(String str) {
        synchronized (this.h) {
            this.c.b(str);
        }
    }

    private void a(@NonNull tx txVar) {
        a(txVar, new Bundle());
    }

    private void a(@NonNull tx txVar, @NonNull Bundle bundle) {
        if (this.i.containsKey(txVar)) {
            List list = (List) this.i.get(txVar);
            if (this.d.a(list)) {
                a(txVar, list);
            } else {
                ue b2 = ue.b(bundle);
                Reason reason = null;
                if (b2 == null) {
                    if (!this.d.a()) {
                        if (this.f != null) {
                            this.f.b("Clids error. Passed clids: %s, and clids from server are empty.", this.j);
                        }
                        reason = Reason.INCONSISTENT_CLIDS;
                    } else {
                        b2 = ue.UNKNOWN;
                    }
                }
                if (reason == null) {
                    reason = (Reason) cx.a(f5232a, b2, Reason.UNKNOWN);
                }
                a(txVar, list, reason);
            }
            b(txVar);
        }
    }

    private void a(@NonNull tx txVar, @NonNull List<String> list) {
        txVar.a(b(list));
    }

    private void a(@NonNull tx txVar, @NonNull List<String> list, @NonNull Reason reason) {
        txVar.a(reason, b(list));
    }

    private void d() {
        WeakHashMap weakHashMap = new WeakHashMap();
        for (Entry entry : this.i.entrySet()) {
            if (this.d.a((List) entry.getValue())) {
                weakHashMap.put(entry.getKey(), entry.getValue());
            }
        }
        for (Entry key : weakHashMap.entrySet()) {
            tx txVar = (tx) key.getKey();
            if (txVar != null) {
                a(txVar);
            }
        }
        weakHashMap.clear();
    }

    @Nullable
    private Map<String, String> b(@Nullable List<String> list) {
        if (list == null) {
            return null;
        }
        HashMap hashMap = new HashMap();
        this.d.a(list, (Map<String, String>) hashMap);
        return hashMap;
    }

    private void b(tx txVar, List<String> list) {
        if (this.i.isEmpty()) {
            this.c.c();
        }
        this.i.put(txVar, list);
    }

    private void b(tx txVar) {
        this.i.remove(txVar);
        if (this.i.isEmpty()) {
            this.c.d();
        }
    }
}
