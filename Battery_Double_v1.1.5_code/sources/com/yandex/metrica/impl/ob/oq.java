package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;

public class oq {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private or f5041a;
    /* access modifiers changed from: private */
    @NonNull
    public ou b;
    @NonNull
    private ok c;
    @NonNull
    private LocationListener d;
    private boolean e;

    static class a {
        a() {
        }

        @NonNull
        public or a(@NonNull Context context, @NonNull Looper looper, @Nullable LocationManager locationManager, @NonNull LocationListener locationListener, @NonNull pr prVar) {
            or orVar = new or(context, looper, locationManager, locationListener, prVar);
            return orVar;
        }
    }

    public oq(@NonNull Context context, @NonNull Looper looper, @Nullable LocationManager locationManager, @Nullable oh ohVar, @NonNull ow owVar, @NonNull od odVar, @NonNull pr prVar) {
        this(context, looper, locationManager, new a(), new ou(context, ohVar, owVar, odVar), new ok(context, locationManager, prVar), prVar);
    }

    public void a() {
        Location a2 = this.c.a();
        if (a2 != null) {
            this.b.a(a2);
        }
    }

    @Nullable
    public Location b() {
        return this.b.a();
    }

    @Nullable
    public Location c() {
        return this.c.a();
    }

    public void d() {
        this.e = true;
        this.f5041a.a();
    }

    public void e() {
        this.e = false;
        this.f5041a.b();
    }

    public void a(@Nullable oh ohVar) {
        this.b.a(ohVar);
        f();
    }

    private void f() {
        if (this.e) {
            e();
            d();
            a();
        }
    }

    @VisibleForTesting
    oq(@NonNull Context context, @NonNull Looper looper, @Nullable LocationManager locationManager, @NonNull a aVar, @NonNull ou ouVar, @NonNull ok okVar, @NonNull pr prVar) {
        this.e = false;
        this.d = new LocationListener() {
            public void onProviderDisabled(String str) {
            }

            public void onProviderEnabled(String str) {
            }

            public void onStatusChanged(String str, int i, Bundle bundle) {
            }

            public void onLocationChanged(@Nullable Location location) {
                if (location != null) {
                    oq.this.b.a(location);
                }
            }
        };
        this.c = okVar;
        this.f5041a = aVar.a(context, looper, locationManager, this.d, prVar);
        this.b = ouVar;
    }
}
