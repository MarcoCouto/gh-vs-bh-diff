package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.util.SparseArray;
import com.yandex.metrica.impl.ob.rl.a;
import com.yandex.metrica.impl.ob.rl.a.C0115a;
import java.util.ArrayList;
import java.util.HashMap;

public class re {

    /* renamed from: a reason: collision with root package name */
    private static final int[] f5086a = {0, 1, 2, 3};
    private final SparseArray<HashMap<String, C0115a>> b;
    private int c;

    public re() {
        this(f5086a);
    }

    @VisibleForTesting
    re(int[] iArr) {
        this.b = new SparseArray<>();
        this.c = 0;
        for (int put : iArr) {
            this.b.put(put, new HashMap());
        }
    }

    @Nullable
    public C0115a a(int i, @NonNull String str) {
        return (C0115a) ((HashMap) this.b.get(i)).get(str);
    }

    /* access modifiers changed from: 0000 */
    public void a(@NonNull C0115a aVar) {
        ((HashMap) this.b.get(aVar.c)).put(new String(aVar.b), aVar);
    }

    public int a() {
        return this.c;
    }

    public void b() {
        this.c++;
    }

    @NonNull
    public a c() {
        a aVar = new a();
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < this.b.size(); i++) {
            for (C0115a add : ((HashMap) this.b.get(this.b.keyAt(i))).values()) {
                arrayList.add(add);
            }
        }
        aVar.b = (C0115a[]) arrayList.toArray(new C0115a[arrayList.size()]);
        return aVar;
    }
}
