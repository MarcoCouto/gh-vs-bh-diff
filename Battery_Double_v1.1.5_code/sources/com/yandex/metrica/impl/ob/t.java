package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;

public class t {
    @Nullable

    /* renamed from: a reason: collision with root package name */
    private Long f5178a;
    @NonNull
    private wg b;

    public t() {
        this(new wg());
    }

    public void a() {
        this.f5178a = Long.valueOf(this.b.c());
    }

    @VisibleForTesting
    t(@NonNull wg wgVar) {
        this.b = wgVar;
    }
}
