package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import com.yandex.metrica.g;
import com.yandex.metrica.impl.ob.rx;

public class ru<S extends rx> {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    protected final g f5096a;
    @NonNull
    private final sa b;
    @NonNull
    private final xh c;
    @NonNull
    private final ry<S> d;
    @NonNull
    private final rt e;

    @VisibleForTesting
    ru(@NonNull sa saVar, @NonNull xh xhVar, @NonNull ry<S> ryVar, @NonNull rt rtVar, @NonNull g gVar) {
        this.b = saVar;
        this.c = xhVar;
        this.d = ryVar;
        this.e = rtVar;
        this.f5096a = gVar;
    }

    @NonNull
    public xh a() {
        return this.c;
    }

    @NonNull
    public ry<S> b() {
        return this.d;
    }

    @NonNull
    public sa c() {
        return this.b;
    }

    @NonNull
    public rt d() {
        return this.e;
    }
}
