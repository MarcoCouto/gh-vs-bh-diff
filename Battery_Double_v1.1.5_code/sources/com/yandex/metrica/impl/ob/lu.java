package com.yandex.metrica.impl.ob;

public class lu extends lx {

    /* renamed from: a reason: collision with root package name */
    private final qk f4983a = new qk("init_event_pref_key");
    private final qk b = new qk("first_event_pref_key");
    private final qk c = new qk("first_event_description_key");
    private final qk d = new qk("preload_info_auto_tracking_enabled");

    public lu(lf lfVar) {
        super(lfVar);
    }

    public void a() {
        b(this.f4983a.b(), "DONE").q();
    }

    public void b() {
        b(this.b.b(), "DONE").q();
    }

    public String a(String str) {
        return c(this.f4983a.b(), str);
    }

    public String b(String str) {
        return c(this.b.b(), str);
    }

    public boolean c() {
        return a((String) null) != null;
    }

    public boolean d() {
        return b((String) null) != null;
    }

    public void c(String str) {
        b(this.c.b(), str).q();
    }

    public String d(String str) {
        return c(this.c.b(), str);
    }

    public void e() {
        a(this.c);
    }

    public void a(boolean z) {
        a(this.d.b(), z).q();
    }

    public boolean b(boolean z) {
        return b(this.d.b(), z);
    }

    private void a(qk qkVar) {
        r(qkVar.b()).q();
    }
}
