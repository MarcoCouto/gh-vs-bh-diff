package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;

public class fw extends fl {
    fw(@NonNull Context context, @NonNull fc fcVar) {
        super(context, fcVar);
    }

    /* access modifiers changed from: protected */
    public void b(@NonNull w wVar, @NonNull eg egVar) {
        a(wk.a(egVar.b.e, true));
        b().a(wVar, egVar);
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public void a(boolean z) {
        c().a(z);
    }
}
