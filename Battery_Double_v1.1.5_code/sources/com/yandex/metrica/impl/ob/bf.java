package com.yandex.metrica.impl.ob;

import android.content.Intent;
import android.net.Uri;
import android.os.Process;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

public class bf implements bg {

    /* renamed from: a reason: collision with root package name */
    private final wp<String, Integer> f4681a = new wp<>();
    private final Map<b, a> b = new LinkedHashMap();
    private final Map<b, a> c = new LinkedHashMap();

    interface a {
        boolean a(@NonNull Intent intent, @NonNull bf bfVar);
    }

    interface b {
        void a();
    }

    public void a() {
    }

    public void a(Intent intent, int i) {
    }

    public void a(Intent intent, int i, int i2) {
    }

    public void b() {
    }

    public void a(Intent intent) {
        if (intent != null) {
            d(intent);
        }
    }

    public void b(Intent intent) {
        if (intent != null) {
            d(intent);
        }
    }

    private void d(@NonNull Intent intent) {
        String action = intent.getAction();
        if (!TextUtils.isEmpty(action)) {
            this.f4681a.a(action, Integer.valueOf(h(intent)));
        }
        a(intent, this.b);
    }

    private void a(@NonNull Intent intent, @NonNull Map<b, a> map) {
        for (Entry entry : map.entrySet()) {
            if (((a) entry.getValue()).a(intent, this)) {
                ((b) entry.getKey()).a();
            }
        }
    }

    public void c(Intent intent) {
        if (intent != null) {
            e(intent);
        }
    }

    private void e(@NonNull Intent intent) {
        String action = intent.getAction();
        if (!TextUtils.isEmpty(action)) {
            this.f4681a.b(action, Integer.valueOf(h(intent)));
        }
        a(intent, this.c);
    }

    public void a(@NonNull b bVar) {
        this.b.put(bVar, d());
    }

    public void b(@NonNull b bVar) {
        this.c.put(bVar, d());
    }

    public void c(@NonNull b bVar) {
        this.b.put(bVar, new a() {
            public boolean a(@NonNull Intent intent, @NonNull bf bfVar) {
                return bf.this.f(intent);
            }
        });
    }

    public void d(@NonNull b bVar) {
        this.b.put(bVar, new a() {
            public boolean a(@NonNull Intent intent, @NonNull bf bfVar) {
                return bf.this.g(intent);
            }
        });
    }

    public void e(@NonNull b bVar) {
        this.c.put(bVar, new a() {
            public boolean a(@NonNull Intent intent, @NonNull bf bfVar) {
                return bf.this.g(intent) && bf.this.g();
            }
        });
    }

    @NonNull
    private a d() {
        return new a() {
            public boolean a(@NonNull Intent intent, @NonNull bf bfVar) {
                return bf.this.a(intent.getAction());
            }
        };
    }

    public boolean c() {
        return !cx.a(this.f4681a.a("com.yandex.metrica.ACTION_C_BG_L"));
    }

    /* access modifiers changed from: private */
    public boolean a(@Nullable String str) {
        return "com.yandex.metrica.ACTION_C_BG_L".equals(str);
    }

    /* access modifiers changed from: private */
    public boolean f(@NonNull Intent intent) {
        return g(intent) && e();
    }

    private boolean b(@Nullable String str) {
        return "com.yandex.metrica.IMetricaService".equals(str);
    }

    /* access modifiers changed from: private */
    public boolean g(@NonNull Intent intent) {
        if (b(intent.getAction())) {
            return !a(h(intent));
        }
        return false;
    }

    private boolean e() {
        return f() == 1;
    }

    private int f() {
        Collection<Integer> h = h();
        int i = 0;
        if (!cx.a((Collection) h)) {
            for (Integer intValue : h) {
                if (!a(intValue.intValue())) {
                    i++;
                }
            }
        }
        return i;
    }

    /* access modifiers changed from: private */
    public boolean g() {
        return f() == 0;
    }

    private Collection<Integer> h() {
        return this.f4681a.a("com.yandex.metrica.IMetricaService");
    }

    private boolean a(int i) {
        return i == Process.myPid();
    }

    private int h(@NonNull Intent intent) {
        Uri data = intent.getData();
        if (data != null && data.getPath().equals("/client")) {
            try {
                return Integer.parseInt(data.getQueryParameter("pid"));
            } catch (Throwable unused) {
            }
        }
        return -1;
    }
}
