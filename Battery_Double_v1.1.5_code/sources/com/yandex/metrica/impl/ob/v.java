package com.yandex.metrica.impl.ob;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Point;
import android.os.Build;
import android.os.Build.VERSION;
import android.provider.Settings.Secure;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.tapjoy.TapjoyConstants;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

public class v {
    private static final Object j = new Object();
    @SuppressLint({"StaticFieldLeak"})
    private static volatile v k;
    @NonNull

    /* renamed from: a reason: collision with root package name */
    public final String f5259a = "android";
    public final String b = Build.MANUFACTURER;
    public final String c = Build.MODEL;
    public final String d = VERSION.RELEASE;
    public final int e = VERSION.SDK_INT;
    @NonNull
    public final b f;
    @NonNull
    public final String g;
    @NonNull
    public final String h;
    @NonNull
    public final List<String> i;
    @NonNull
    private final a l;

    public static class a {
        @Nullable

        /* renamed from: a reason: collision with root package name */
        private String f5261a;
        @NonNull
        private Context b;
        /* access modifiers changed from: private */
        public uk c;

        a(@NonNull Context context) {
            this.b = context;
            dr.a().b((dt) new dx(this.f5261a));
            dr.a().a(this, eb.class, dv.a((du<T>) new du<eb>() {
                public void a(eb ebVar) {
                    synchronized (a.this) {
                        a.this.c = ebVar.b;
                    }
                }
            }).a());
            this.f5261a = b(this.c) ? a(context) : null;
        }

        @Nullable
        @SuppressLint({"HardwareIds"})
        private String a(@NonNull Context context) {
            try {
                return Secure.getString(context.getContentResolver(), TapjoyConstants.TJC_ANDROID_ID);
            } catch (Throwable unused) {
                return null;
            }
        }

        @Nullable
        public String a(@NonNull uk ukVar) {
            if (TextUtils.isEmpty(this.f5261a) && b(ukVar)) {
                this.f5261a = a(this.b);
            }
            return this.f5261a;
        }

        private synchronized boolean b(@NonNull uk ukVar) {
            if (ukVar == null) {
                ukVar = this.c;
            }
            return c(ukVar);
        }

        private boolean c(@NonNull uk ukVar) {
            return ukVar != null && ukVar.o.g;
        }
    }

    public static final class b {

        /* renamed from: a reason: collision with root package name */
        public final int f5263a;
        public final int b;
        public final int c;
        public final float d;

        b(@NonNull Point point, int i, float f) {
            this.f5263a = Math.max(point.x, point.y);
            this.b = Math.min(point.x, point.y);
            this.c = i;
            this.d = f;
        }
    }

    public static v a(@NonNull Context context) {
        if (k == null) {
            synchronized (j) {
                if (k == null) {
                    k = new v(context.getApplicationContext());
                }
            }
        }
        return k;
    }

    private v(@NonNull Context context) {
        this.l = new a(context);
        this.f = new b(bt.b(context), context.getResources().getDisplayMetrics().densityDpi, context.getResources().getDisplayMetrics().density);
        this.g = bt.a(context).name().toLowerCase(Locale.US);
        this.h = String.valueOf(com.yandex.metrica.impl.ob.bt.b.c());
        this.i = Collections.unmodifiableList(new ArrayList<String>() {
            {
                if (com.yandex.metrica.impl.ob.bt.b.a()) {
                    add("Superuser.apk");
                }
                if (com.yandex.metrica.impl.ob.bt.b.b()) {
                    add("su.so");
                }
            }
        });
    }

    @Nullable
    public String a() {
        return this.l.a((uk) null);
    }

    @Nullable
    public String a(@NonNull uk ukVar) {
        return this.l.a(ukVar);
    }
}
