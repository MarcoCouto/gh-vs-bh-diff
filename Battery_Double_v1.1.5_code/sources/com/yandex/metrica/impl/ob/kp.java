package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import com.yandex.metrica.impl.ob.rg.d;
import java.util.List;

public class kp implements mw<StackTraceElement, d> {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private kq f4945a = new kq();

    @NonNull
    /* renamed from: a */
    public d[] b(@NonNull List<StackTraceElement> list) {
        d[] dVarArr = new d[list.size()];
        int i = 0;
        for (StackTraceElement a2 : list) {
            dVarArr[i] = this.f4945a.b(a2);
            i++;
        }
        return dVarArr;
    }

    @NonNull
    public List<StackTraceElement> a(d[] dVarArr) {
        throw new UnsupportedOperationException();
    }
}
