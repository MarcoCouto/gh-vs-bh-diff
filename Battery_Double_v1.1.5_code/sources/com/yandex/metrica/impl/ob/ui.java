package com.yandex.metrica.impl.ob;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.yandex.metrica.IParamsCallback;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ui {

    /* renamed from: a reason: collision with root package name */
    private final Map<String, String> f5235a = new HashMap();
    private List<String> b;
    private Map<String, String> c;
    private long d;
    private final lv e;

    public ui(lv lvVar) {
        this.e = lvVar;
        Map<String, String> map = null;
        a("yandex_mobile_metrica_device_id", this.e.b((String) null));
        a("appmetrica_device_id_hash", this.e.c((String) null));
        a("yandex_mobile_metrica_uuid", this.e.a((String) null));
        a("yandex_mobile_metrica_get_ad_url", this.e.d((String) null));
        a("yandex_mobile_metrica_report_ad_url", this.e.e((String) null));
        b(IParamsCallback.YANDEX_MOBILE_METRICA_CLIDS, this.e.f((String) null));
        this.b = this.e.b();
        String p = this.e.p(null);
        if (p != null) {
            map = we.a(p);
        }
        this.c = map;
        this.d = this.e.a(0);
        g();
    }

    public void a(@Nullable Map<String, String> map) {
        Map<String, String> map2;
        if (!cx.a((Object) map, (Object) this.c)) {
            if (map == null) {
                map2 = null;
            } else {
                map2 = new HashMap<>(map);
            }
            this.c = map2;
            this.f5235a.remove(IParamsCallback.YANDEX_MOBILE_METRICA_CLIDS);
            g();
        }
    }

    public boolean a() {
        String str = (String) this.f5235a.get(IParamsCallback.YANDEX_MOBILE_METRICA_CLIDS);
        if (str != null && str.isEmpty()) {
            return cx.a((Map) this.c);
        }
        return true;
    }

    private void a(@NonNull String str, @Nullable String str2) {
        if (!TextUtils.isEmpty(str2)) {
            this.f5235a.put(str, str2);
        }
    }

    private void b(@NonNull String str, @Nullable String str2) {
        if (str2 != null) {
            this.f5235a.put(str, str2);
        }
    }

    private void a(@Nullable String str) {
        if (TextUtils.isEmpty((CharSequence) this.f5235a.get("yandex_mobile_metrica_uuid")) && !TextUtils.isEmpty(str)) {
            a("yandex_mobile_metrica_uuid", str);
        }
    }

    /* access modifiers changed from: 0000 */
    public synchronized void a(@NonNull List<String> list, Map<String, String> map) {
        for (String str : list) {
            String str2 = (String) this.f5235a.get(str);
            if (str2 != null) {
                map.put(str, str2);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public synchronized boolean b() {
        return a(Arrays.asList(new String[]{IParamsCallback.YANDEX_MOBILE_METRICA_CLIDS, "appmetrica_device_id_hash", "yandex_mobile_metrica_device_id", "yandex_mobile_metrica_get_ad_url", "yandex_mobile_metrica_report_ad_url", "yandex_mobile_metrica_uuid"}));
    }

    /* access modifiers changed from: 0000 */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0033, code lost:
        return false;
     */
    public synchronized boolean a(@NonNull List<String> list) {
        for (String str : list) {
            if (IParamsCallback.YANDEX_MOBILE_METRICA_CLIDS.equals(str)) {
                String str2 = (String) this.f5235a.get(str);
                if (str2 == null || (str2.isEmpty() && !cx.a((Map) this.c))) {
                }
            } else {
                if (!"yandex_mobile_metrica_get_ad_url".equals(str)) {
                    if (!"yandex_mobile_metrica_report_ad_url".equals(str)) {
                        if (TextUtils.isEmpty((CharSequence) this.f5235a.get(str))) {
                            return false;
                        }
                    }
                }
                if (this.f5235a.get(str) == null) {
                    return false;
                }
            }
        }
        return true;
    }

    /* access modifiers changed from: 0000 */
    public synchronized void a(@NonNull Bundle bundle) {
        e(bundle);
        f(bundle);
        d(bundle);
        b(bundle);
        g();
    }

    private void g() {
        this.e.g((String) this.f5235a.get("yandex_mobile_metrica_uuid")).h((String) this.f5235a.get("yandex_mobile_metrica_device_id")).i((String) this.f5235a.get("appmetrica_device_id_hash")).j((String) this.f5235a.get("yandex_mobile_metrica_get_ad_url")).k((String) this.f5235a.get("yandex_mobile_metrica_report_ad_url")).d(this.d).l((String) this.f5235a.get(IParamsCallback.YANDEX_MOBILE_METRICA_CLIDS)).o(we.a(this.c)).q();
    }

    private void b(Bundle bundle) {
        if (c(bundle)) {
            this.f5235a.put(IParamsCallback.YANDEX_MOBILE_METRICA_CLIDS, bundle.getString("Clids"));
        }
    }

    private boolean c(@NonNull Bundle bundle) {
        Map a2 = we.a(bundle.getString("RequestClids"));
        if (cx.a((Map) this.c)) {
            return cx.a(a2);
        }
        return this.c.equals(a2);
    }

    /* access modifiers changed from: 0000 */
    public void a(long j) {
        this.e.e(j).q();
    }

    /* access modifiers changed from: 0000 */
    public boolean c() {
        long b2 = wi.b() - this.e.b(0);
        return b2 > 86400 || b2 < 0;
    }

    /* access modifiers changed from: 0000 */
    public List<String> d() {
        return this.b;
    }

    /* access modifiers changed from: 0000 */
    public void b(List<String> list) {
        this.b = list;
        this.e.a(this.b);
    }

    private void d(Bundle bundle) {
        b(bundle.getLong("ServerTimeOffset"));
    }

    private synchronized void e(@NonNull Bundle bundle) {
        a(bundle.getString("Uuid"));
        a("yandex_mobile_metrica_device_id", bundle.getString("DeviceId"));
        a("appmetrica_device_id_hash", bundle.getString("DeviceIdHash"));
    }

    private synchronized void f(Bundle bundle) {
        String string = bundle.getString("AdUrlGet");
        if (string != null) {
            b(string);
        }
        String string2 = bundle.getString("AdUrlReport");
        if (string2 != null) {
            c(string2);
        }
    }

    private synchronized void b(String str) {
        this.f5235a.put("yandex_mobile_metrica_get_ad_url", str);
    }

    private synchronized void c(String str) {
        this.f5235a.put("yandex_mobile_metrica_report_ad_url", str);
    }

    private synchronized void b(long j) {
        this.d = j;
    }

    /* access modifiers changed from: 0000 */
    public String e() {
        return (String) this.f5235a.get("yandex_mobile_metrica_uuid");
    }

    /* access modifiers changed from: 0000 */
    public String f() {
        return (String) this.f5235a.get("yandex_mobile_metrica_device_id");
    }
}
