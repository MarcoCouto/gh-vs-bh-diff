package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.util.Log;
import com.yandex.metrica.d;
import com.yandex.metrica.f;
import com.yandex.metrica.j;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class ca {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private Context f4712a;
    @NonNull
    private ee b;
    @NonNull
    private cd c;
    @NonNull
    private Handler d;
    @NonNull
    private uf e;
    private Map<String, d> f = new HashMap();
    private final yk<String> g = new yg(new ym(this.f));
    private final List<String> h = Arrays.asList(new String[]{"20799a27-fa80-4b36-b2db-0f8141f24180", "0e5e9c33-f8c3-4568-86c5-2e4f57523f72"});

    public ca(@NonNull Context context, @NonNull ee eeVar, @NonNull cd cdVar, @NonNull Handler handler, @NonNull uf ufVar) {
        this.f4712a = context;
        this.b = eeVar;
        this.c = cdVar;
        this.d = handler;
        this.e = ufVar;
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public ax a(@NonNull j jVar, boolean z) {
        this.g.a(jVar.apiKey);
        ax axVar = new ax(this.f4712a, this.b, jVar, this.c, this.e, new cb(this, "20799a27-fa80-4b36-b2db-0f8141f24180"), new cb(this, "0e5e9c33-f8c3-4568-86c5-2e4f57523f72"));
        a((n) axVar);
        axVar.a(jVar, z);
        axVar.a();
        this.c.a(axVar);
        this.f.put(jVar.apiKey, axVar);
        return axVar;
    }

    /* access modifiers changed from: 0000 */
    public synchronized void a(@NonNull f fVar) {
        if (this.f.containsKey(fVar.apiKey)) {
            vz a2 = vr.a(fVar.apiKey);
            if (a2.c()) {
                a2.b("Reporter with apiKey=%s already exists.", fVar.apiKey);
            }
        } else {
            b(fVar);
            StringBuilder sb = new StringBuilder();
            sb.append("Activate reporter with APIKey ");
            sb.append(cx.b(fVar.apiKey));
            Log.i("AppMetrica", sb.toString());
        }
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public synchronized d b(@NonNull f fVar) {
        d dVar;
        dVar = (d) this.f.get(fVar.apiKey);
        if (dVar == 0) {
            if (!this.h.contains(fVar.apiKey)) {
                this.e.c();
            }
            ay ayVar = new ay(this.f4712a, this.b, fVar, this.c);
            a((n) ayVar);
            ayVar.a();
            this.f.put(fVar.apiKey, ayVar);
            dVar = ayVar;
        }
        return dVar;
    }

    private void a(@NonNull n nVar) {
        nVar.a(new at(this.d, nVar));
        nVar.a((ug) this.e);
    }
}
