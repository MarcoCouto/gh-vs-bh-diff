package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import java.io.IOException;

public interface ml<T> {
    @NonNull
    byte[] a(@NonNull T t);

    @NonNull
    T b(@NonNull byte[] bArr) throws IOException;

    @NonNull
    T c();
}
