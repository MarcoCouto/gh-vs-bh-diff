package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;

public class aq {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final wh f4646a;
    @NonNull
    private final cw b;
    @NonNull
    private final ly c;
    @NonNull
    private final Context d;
    private long e;
    @Nullable
    private ty f;

    public aq(@NonNull ly lyVar, @NonNull Context context, @Nullable ty tyVar) {
        this(lyVar, context, tyVar, new wg(), new cw());
    }

    @VisibleForTesting
    aq(@NonNull ly lyVar, @NonNull Context context, @Nullable ty tyVar, @NonNull wh whVar, @NonNull cw cwVar) {
        this.c = lyVar;
        this.d = context;
        this.f = tyVar;
        this.e = this.c.h(0);
        this.f4646a = whVar;
        this.b = cwVar;
    }

    public void a() {
        if (this.f != null && this.b.a(this.e, this.f.f5224a, "should send EVENT_IDENTITY_LIGHT")) {
            b();
            this.e = this.f4646a.b();
            this.c.i(this.e);
        }
    }

    private void b() {
        tl.a(this.d).e();
    }

    public void a(@Nullable ty tyVar) {
        this.f = tyVar;
    }
}
