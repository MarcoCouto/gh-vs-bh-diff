package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import java.util.Collection;
import java.util.List;

public class jq {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final vt f4918a;
    @NonNull
    private final js b;

    public jq(@NonNull Context context) {
        this(new vt(), new js(context));
    }

    @Nullable
    public Long a(@Nullable List<oj> list) {
        long j;
        if (cx.a((Collection) list)) {
            return null;
        }
        oj ojVar = (oj) list.get(Math.min(this.b.a(), list.size()) - 1);
        if (ojVar.f5029a == ojVar.b) {
            j = ojVar.f5029a;
        } else {
            j = this.f4918a.a(ojVar.f5029a, ojVar.b);
        }
        return Long.valueOf(j);
    }

    @VisibleForTesting
    jq(@NonNull vt vtVar, @NonNull js jsVar) {
        this.f4918a = vtVar;
        this.b = jsVar;
    }
}
