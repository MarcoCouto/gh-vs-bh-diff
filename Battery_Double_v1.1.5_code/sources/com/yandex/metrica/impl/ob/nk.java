package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import com.yandex.metrica.impl.ob.rr.a.b.C0123a;
import com.yandex.metrica.impl.ob.tt.a;
import java.util.ArrayList;
import java.util.List;

public class nk implements mw<a, C0123a> {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final nj f5004a;

    public nk() {
        this(new nj());
    }

    @NonNull
    public List<a> a(@NonNull C0123a[] aVarArr) {
        ArrayList arrayList = new ArrayList(aVarArr.length);
        for (C0123a a2 : aVarArr) {
            arrayList.add(this.f5004a.a(a2));
        }
        return arrayList;
    }

    @NonNull
    /* renamed from: a */
    public C0123a[] b(@NonNull List<a> list) {
        C0123a[] aVarArr = new C0123a[list.size()];
        for (int i = 0; i < list.size(); i++) {
            aVarArr[i] = this.f5004a.b((a) list.get(i));
        }
        return aVarArr;
    }

    @VisibleForTesting
    nk(@NonNull nj njVar) {
        this.f5004a = njVar;
    }
}
