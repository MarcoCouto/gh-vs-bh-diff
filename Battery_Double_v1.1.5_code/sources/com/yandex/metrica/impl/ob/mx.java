package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import com.yandex.metrica.impl.ob.rr.a.f;
import java.util.ArrayList;
import java.util.List;

public class mx implements mw<pu, f> {
    @NonNull
    /* renamed from: a */
    public f[] b(@NonNull List<pu> list) {
        f[] fVarArr = new f[list.size()];
        for (int i = 0; i < list.size(); i++) {
            fVarArr[i] = a((pu) list.get(i));
        }
        return fVarArr;
    }

    @NonNull
    public List<pu> a(@NonNull f[] fVarArr) {
        ArrayList arrayList = new ArrayList(fVarArr.length);
        for (f a2 : fVarArr) {
            arrayList.add(a(a2));
        }
        return arrayList;
    }

    @NonNull
    private f a(@NonNull pu puVar) {
        f fVar = new f();
        fVar.b = puVar.f5068a;
        fVar.c = puVar.b;
        return fVar;
    }

    @NonNull
    private pu a(@NonNull f fVar) {
        return new pu(fVar.b, fVar.c);
    }
}
