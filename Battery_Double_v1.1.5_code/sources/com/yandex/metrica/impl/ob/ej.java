package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;

public class ej implements eq<ei> {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final bm f4813a;

    public ej() {
        this(new bm());
    }

    public ej(@NonNull bm bmVar) {
        this.f4813a = bmVar;
    }

    /* renamed from: a */
    public ei b(@NonNull Context context, @NonNull ek ekVar, @NonNull eg egVar) {
        ei eiVar = new ei(context, uc.a(), ekVar, egVar, al.a().c(), this.f4813a.a(context, ekVar));
        return eiVar;
    }
}
