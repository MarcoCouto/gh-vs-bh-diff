package com.yandex.metrica.impl.ob;

import android.annotation.TargetApi;
import android.app.usage.StorageStatsManager;
import android.content.Context;
import android.os.Environment;
import android.os.StatFs;
import android.os.storage.StorageManager;
import android.os.storage.StorageVolume;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.support.v4.media.session.PlaybackStateCompat;
import java.util.UUID;

public class ad {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Context f4632a;
    @NonNull
    private final b b;

    public static class a {

        /* renamed from: a reason: collision with root package name */
        public final long f4633a;
        public final long b;

        public a(long j, long j2) {
            this.f4633a = j;
            this.b = j2;
        }
    }

    static class b {
        b() {
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        public StatFs a() {
            return new StatFs(Environment.getDataDirectory().getAbsolutePath());
        }
    }

    public ad(@NonNull Context context) {
        this(context, new b());
    }

    @VisibleForTesting
    ad(@NonNull Context context, @NonNull b bVar) {
        this.f4632a = context;
        this.b = bVar;
    }

    @NonNull
    public a a() {
        if (cx.a(26)) {
            return b();
        }
        if (cx.a(18)) {
            return c();
        }
        return d();
    }

    @TargetApi(26)
    @NonNull
    private a b() {
        long j;
        StorageStatsManager storageStatsManager = (StorageStatsManager) this.f4632a.getSystemService("storagestats");
        StorageManager storageManager = (StorageManager) this.f4632a.getSystemService("storage");
        long j2 = 0;
        if (storageManager == null || storageStatsManager == null) {
            j = 0;
        } else {
            long j3 = 0;
            for (StorageVolume uuid : storageManager.getStorageVolumes()) {
                try {
                    String uuid2 = uuid.getUuid();
                    UUID fromString = uuid2 == null ? StorageManager.UUID_DEFAULT : UUID.fromString(uuid2);
                    j3 += storageStatsManager.getTotalBytes(fromString);
                    j2 += storageStatsManager.getFreeBytes(fromString);
                } catch (Throwable unused) {
                }
            }
            j = j2;
            j2 = j3;
        }
        return new a(j2 / PlaybackStateCompat.ACTION_PLAY_FROM_MEDIA_ID, j / PlaybackStateCompat.ACTION_PLAY_FROM_MEDIA_ID);
    }

    @TargetApi(18)
    @NonNull
    private a c() {
        try {
            StatFs a2 = this.b.a();
            long blockSizeLong = a2.getBlockSizeLong();
            return new a((a2.getBlockCountLong() * blockSizeLong) / PlaybackStateCompat.ACTION_PLAY_FROM_MEDIA_ID, (a2.getAvailableBlocksLong() * blockSizeLong) / PlaybackStateCompat.ACTION_PLAY_FROM_MEDIA_ID);
        } catch (Throwable unused) {
            return new a(0, 0);
        }
    }

    @NonNull
    private a d() {
        try {
            StatFs a2 = this.b.a();
            long blockSize = (long) a2.getBlockSize();
            return new a((((long) a2.getBlockCount()) * blockSize) / PlaybackStateCompat.ACTION_PLAY_FROM_MEDIA_ID, (((long) a2.getAvailableBlocks()) * blockSize) / PlaybackStateCompat.ACTION_PLAY_FROM_MEDIA_ID);
        } catch (Throwable unused) {
            return new a(0, 0);
        }
    }
}
