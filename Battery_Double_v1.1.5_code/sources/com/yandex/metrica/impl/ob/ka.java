package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public class ka extends jy {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final jv f4932a;
    @Nullable
    private final de b;

    public ka(@NonNull Context context, @NonNull jv jvVar, @Nullable de deVar) {
        super(context);
        this.f4932a = jvVar;
        this.b = deVar;
    }

    public void a(@Nullable Bundle bundle, @Nullable jw jwVar) {
        this.f4932a.a();
        if (this.b != null) {
            this.b.a(a());
        }
        if (jwVar != null) {
            jwVar.a();
        }
    }
}
