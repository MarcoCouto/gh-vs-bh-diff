package com.yandex.metrica.impl.ob;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

public class xm implements ThreadFactory {

    /* renamed from: a reason: collision with root package name */
    private static final AtomicInteger f5304a = new AtomicInteger(0);
    private final String b;

    public xm(String str) {
        this.b = str;
    }

    /* renamed from: a */
    public xl newThread(Runnable runnable) {
        return new xl(runnable, c());
    }

    private String c() {
        return a(this.b);
    }

    public xk a() {
        return new xk(c());
    }

    public static xl a(String str, Runnable runnable) {
        return new xm(str).newThread(runnable);
    }

    public static String a(String str) {
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append("-");
        sb.append(b());
        return sb.toString();
    }

    public static int b() {
        return f5304a.incrementAndGet();
    }
}
