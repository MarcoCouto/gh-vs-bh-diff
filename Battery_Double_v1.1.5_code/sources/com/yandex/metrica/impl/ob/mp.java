package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import java.util.Arrays;

public class mp {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final a f4997a;
    @NonNull
    private final vm b;

    public static class a {
        /* access modifiers changed from: 0000 */
        public wr a(byte[] bArr, byte[] bArr2) {
            return new wr("AES/CBC/PKCS5Padding", bArr, bArr2);
        }
    }

    public mp() {
        this(new a(), new vm());
    }

    @VisibleForTesting
    public mp(@NonNull a aVar, @NonNull vm vmVar) {
        this.f4997a = aVar;
        this.b = vmVar;
    }

    @Nullable
    public byte[] a(@Nullable byte[] bArr, @NonNull String str) {
        try {
            wr a2 = this.f4997a.a(str.getBytes(), Arrays.copyOfRange(bArr, 0, 16));
            if (cx.a(bArr)) {
                return null;
            }
            return this.b.b(a2.a(bArr, 16, bArr.length - 16));
        } catch (Throwable unused) {
            return null;
        }
    }
}
