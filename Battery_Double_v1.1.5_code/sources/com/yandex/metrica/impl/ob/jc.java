package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;

public class jc {

    /* renamed from: a reason: collision with root package name */
    private final lw f4898a;

    public jc(@NonNull lw lwVar) {
        this.f4898a = lwVar;
    }

    public long a() {
        long j = this.f4898a.j();
        long j2 = 10000000000L;
        if (j >= 10000000000L) {
            j2 = 1 + j;
        }
        this.f4898a.e(j2).q();
        return j2;
    }
}
