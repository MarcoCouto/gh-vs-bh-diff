package com.yandex.metrica.impl.ob;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build.VERSION;

public class vl {
    @SuppressLint({"ObsoleteSdkInt"})
    public static void a(Cursor cursor, ContentValues contentValues) {
        if (VERSION.SDK_INT >= 11) {
            b(cursor, contentValues);
        } else {
            DatabaseUtils.cursorRowToContentValues(cursor, contentValues);
        }
    }

    @TargetApi(11)
    public static void b(Cursor cursor, ContentValues contentValues) {
        String[] columnNames = cursor.getColumnNames();
        int length = columnNames.length;
        for (int i = 0; i < length; i++) {
            switch (cursor.getType(i)) {
                case 0:
                    contentValues.put(columnNames[i], cursor.getString(i));
                    break;
                case 1:
                    contentValues.put(columnNames[i], Long.valueOf(cursor.getLong(i)));
                    break;
                case 2:
                    contentValues.put(columnNames[i], Double.valueOf(cursor.getDouble(i)));
                    break;
                case 3:
                    contentValues.put(columnNames[i], cursor.getString(i));
                    break;
                case 4:
                    contentValues.put(columnNames[i], cursor.getBlob(i));
                    break;
                default:
                    contentValues.put(columnNames[i], cursor.getString(i));
                    break;
            }
        }
    }

    public static long a(SQLiteDatabase sQLiteDatabase, String str) {
        Cursor cursor = null;
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("SELECT count() FROM ");
            sb.append(str);
            Cursor rawQuery = sQLiteDatabase.rawQuery(sb.toString(), null);
            try {
                long j = rawQuery.moveToFirst() ? rawQuery.getLong(0) : 0;
                cx.a(rawQuery);
                return j;
            } catch (Throwable th) {
                th = th;
                cursor = rawQuery;
                cx.a(cursor);
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            cx.a(cursor);
            throw th;
        }
    }
}
