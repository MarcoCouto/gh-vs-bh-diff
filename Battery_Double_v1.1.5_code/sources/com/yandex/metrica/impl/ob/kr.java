package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import com.yandex.metrica.impl.ob.rg.e;

public class kr implements mq<kk, e> {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private kp f4946a = new kp();

    @NonNull
    /* renamed from: a */
    public e b(@NonNull kk kkVar) {
        e eVar = new e();
        eVar.f = kkVar.e == null ? -1 : kkVar.e.intValue();
        eVar.e = kkVar.d;
        eVar.c = kkVar.b;
        eVar.b = kkVar.f4941a;
        eVar.d = kkVar.c;
        eVar.g = this.f4946a.b(kkVar.f);
        return eVar;
    }

    @NonNull
    public kk a(@NonNull e eVar) {
        throw new UnsupportedOperationException();
    }
}
