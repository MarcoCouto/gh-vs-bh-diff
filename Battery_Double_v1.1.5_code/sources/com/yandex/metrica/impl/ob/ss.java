package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import com.tapjoy.TapjoyConstants;
import java.util.HashMap;
import java.util.Map;

public class ss {

    /* renamed from: a reason: collision with root package name */
    private final Map<String, String> f5169a = new HashMap();

    public ss() {
        this.f5169a.put(TapjoyConstants.TJC_ANDROID_ID, "a");
        this.f5169a.put("background_location_collection", "blc");
        this.f5169a.put("background_lbs_collection", "blbc");
        this.f5169a.put("easy_collecting", "ec");
        this.f5169a.put("access_point", "ap");
        this.f5169a.put("cells_around", "ca");
        this.f5169a.put("google_aid", "g");
        this.f5169a.put("own_macs", "om");
        this.f5169a.put("sim_imei", "sm");
        this.f5169a.put("sim_info", "si");
        this.f5169a.put("wifi_around", "wa");
        this.f5169a.put("wifi_connected", "wc");
        this.f5169a.put("features_collecting", "fc");
        this.f5169a.put("foreground_location_collection", "flc");
        this.f5169a.put("foreground_lbs_collection", "flbc");
        this.f5169a.put("package_info", "pi");
        this.f5169a.put("permissions_collecting", "pc");
        this.f5169a.put("sdk_list", "sl");
        this.f5169a.put("socket", "s");
        this.f5169a.put("telephony_restricted_to_location_tracking", "trtlt");
        this.f5169a.put("identity_light_collecting", "ilc");
        this.f5169a.put("ble_collecting", "bc");
    }

    @NonNull
    public String a(@NonNull String str) {
        return this.f5169a.containsKey(str) ? (String) this.f5169a.get(str) : str;
    }
}
