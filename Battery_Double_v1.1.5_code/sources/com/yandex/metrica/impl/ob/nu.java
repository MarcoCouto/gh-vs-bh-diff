package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import com.yandex.metrica.impl.ob.af.a;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;

public class nu implements nv, nw {

    /* renamed from: a reason: collision with root package name */
    private final Set<Integer> f5018a = new HashSet();
    private AtomicLong b;

    public nu(@NonNull kz kzVar) {
        this.f5018a.add(Integer.valueOf(a.EVENT_TYPE_FIRST_ACTIVATION.a()));
        this.f5018a.add(Integer.valueOf(a.EVENT_TYPE_APP_UPDATE.a()));
        this.f5018a.add(Integer.valueOf(a.EVENT_TYPE_INIT.a()));
        this.f5018a.add(Integer.valueOf(a.EVENT_TYPE_IDENTITY.a()));
        this.f5018a.add(Integer.valueOf(a.EVENT_TYPE_SEND_REFERRER.a()));
        kzVar.a((nw) this);
        this.b = new AtomicLong(kzVar.a(this.f5018a));
    }

    public boolean a() {
        return this.b.get() > 0;
    }

    public void a(@NonNull List<Integer> list) {
        int i = 0;
        for (Integer intValue : list) {
            if (this.f5018a.contains(Integer.valueOf(intValue.intValue()))) {
                i++;
            }
        }
        this.b.addAndGet((long) i);
    }

    public void b(@NonNull List<Integer> list) {
        int i = 0;
        for (Integer intValue : list) {
            if (this.f5018a.contains(Integer.valueOf(intValue.intValue()))) {
                i++;
            }
        }
        this.b.addAndGet((long) (-i));
    }
}
