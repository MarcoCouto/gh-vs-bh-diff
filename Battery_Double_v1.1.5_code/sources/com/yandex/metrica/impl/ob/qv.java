package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.metrica.impl.ob.rl.a.C0115a;

public class qv implements qn {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final xs f5081a;

    public qv(@NonNull xs xsVar) {
        this.f5081a = xsVar;
    }

    @Nullable
    public C0115a a(@NonNull re reVar, @NonNull C0115a aVar) {
        if (reVar.a() == this.f5081a.a()) {
            if (reVar.a(aVar.c, new String(aVar.b)) != null) {
                reVar.a(aVar);
            }
        } else if (reVar.a() < this.f5081a.a()) {
            reVar.a(aVar);
            reVar.b();
        }
        return aVar;
    }
}
