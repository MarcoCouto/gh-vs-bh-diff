package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import com.google.android.exoplayer2.offline.DownloadService;

public class iv extends it {
    iv(@NonNull en enVar, @NonNull jc jcVar) {
        this(enVar, jcVar, new jg(enVar.y(), DownloadService.KEY_FOREGROUND));
    }

    @VisibleForTesting
    iv(@NonNull en enVar, @NonNull jc jcVar, @NonNull jg jgVar) {
        super(enVar, jcVar, jgVar, jb.a(jh.FOREGROUND).a());
    }
}
