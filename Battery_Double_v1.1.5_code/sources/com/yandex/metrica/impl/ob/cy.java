package com.yandex.metrica.impl.ob;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import java.net.NetworkInterface;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import org.json.JSONArray;
import org.json.JSONObject;

public class cy implements p {
    @SuppressLint({"StaticFieldLeak"})
    private static volatile cy f;
    private static final Object g = new Object();

    /* renamed from: a reason: collision with root package name */
    private final Context f4757a;
    @Nullable
    private final WifiManager b;
    /* access modifiers changed from: private */
    @NonNull
    public pr c;
    @NonNull
    private px d;
    @NonNull
    private wf e;
    private com.yandex.metrica.impl.ob.p.a<JSONArray> h;
    private com.yandex.metrica.impl.ob.p.a<List<a>> i;
    private uk j;
    private final ps k;

    public static final class a {

        /* renamed from: a reason: collision with root package name */
        public final String f4766a;
        public final String b;

        public a(String str, String str2) {
            this.f4766a = str;
            this.b = str2;
        }
    }

    private cy(Context context) {
        this(context, (WifiManager) context.getApplicationContext().getSystemService("wifi"), new px());
    }

    private cy(Context context, @Nullable WifiManager wifiManager, px pxVar) {
        this(context, wifiManager, pxVar, new pr(pxVar.a()), new wf());
    }

    public static cy a(Context context) {
        if (f == null) {
            synchronized (g) {
                if (f == null) {
                    f = new cy(context.getApplicationContext());
                }
            }
        }
        return f;
    }

    public void a(@NonNull uk ukVar) {
        this.j = ukVar;
        this.d.a(ukVar);
        this.c.a(this.d.a());
    }

    public synchronized JSONArray a() {
        if (!f()) {
            return new JSONArray();
        }
        if (this.h.b() || this.h.c()) {
            this.h.a(e());
        }
        return (JSONArray) this.h.a();
    }

    @Nullable
    private String a(@Nullable String str) {
        if (str == null) {
            return null;
        }
        return str.toUpperCase(Locale.US).replace(":", "");
    }

    @Nullable
    private List<ScanResult> c() {
        return (List) cx.a((wo<T, S>) new wo<WifiManager, List<ScanResult>>() {
            public List<ScanResult> a(WifiManager wifiManager) throws Throwable {
                return wifiManager.getScanResults();
            }
        }, this.b, "getting scan results", "WifiManager");
    }

    @Nullable
    private WifiInfo d() {
        return (WifiInfo) cx.a((wo<T, S>) new wo<WifiManager, WifiInfo>() {
            public WifiInfo a(WifiManager wifiManager) throws Throwable {
                WifiInfo connectionInfo = wifiManager.getConnectionInfo();
                if (connectionInfo == null) {
                    return connectionInfo;
                }
                if ("00:00:00:00:00:00".equals(connectionInfo.getBSSID()) || "02:00:00:00:00:00".equals(connectionInfo.getBSSID()) || connectionInfo.getIpAddress() == 0) {
                    return null;
                }
                return connectionInfo;
            }
        }, this.b, "getting connection info", "WifiManager");
    }

    private JSONArray e() {
        String str;
        String str2 = null;
        List<ScanResult> c2 = (!j() || !this.k.a(this.f4757a)) ? null : c();
        WifiInfo d2 = (!i() || !this.c.e(this.f4757a)) ? null : d();
        if (d2 == null) {
            str = null;
        } else {
            str = d2.getBSSID();
        }
        JSONArray jSONArray = new JSONArray();
        if (c2 != null) {
            for (ScanResult scanResult : c2) {
                if (scanResult != null && !"02:00:00:00:00:00".equals(scanResult.BSSID)) {
                    JSONObject a2 = a(str, scanResult);
                    if (a2 != null) {
                        jSONArray.put(a2);
                    }
                }
            }
        } else if (d2 != null) {
            String a3 = a(str);
            String ssid = d2.getSSID();
            if (ssid != null) {
                str2 = b(ssid);
            }
            JSONObject a4 = a(a3, str2, true, d2.getRssi(), Long.valueOf(0));
            if (a4 != null) {
                jSONArray.put(a4);
            }
        }
        return jSONArray;
    }

    private JSONObject a(@Nullable String str, @NonNull ScanResult scanResult) {
        boolean z;
        String str2 = null;
        try {
            String str3 = scanResult.BSSID;
            if (str3 != null) {
                z = str3.equals(str);
                try {
                    str2 = a(str3);
                } catch (NoSuchFieldError unused) {
                }
                return a(str2, scanResult.SSID, z, scanResult.level, a(scanResult));
            }
        } catch (NoSuchFieldError unused2) {
        }
        z = false;
        return a(str2, scanResult.SSID, z, scanResult.level, a(scanResult));
    }

    @Nullable
    private Long a(@NonNull ScanResult scanResult) {
        if (VERSION.SDK_INT >= 17) {
            return b(scanResult);
        }
        return null;
    }

    @TargetApi(17)
    @NonNull
    private Long b(@NonNull ScanResult scanResult) {
        return Long.valueOf(this.e.a(scanResult.timestamp, TimeUnit.MICROSECONDS));
    }

    @NonNull
    private String b(@NonNull String str) {
        return str.replace("\"", "");
    }

    private JSONObject a(@Nullable String str, @Nullable String str2, boolean z, int i2, @Nullable Long l) {
        try {
            return new JSONObject().put("mac", str).put("ssid", str2).put("signal_strength", i2).put("is_connected", z).put("last_visible_offset_seconds", l);
        } catch (Throwable unused) {
            return null;
        }
    }

    private boolean f() {
        return ((Boolean) cx.a(new wo<WifiManager, Boolean>() {
            public Boolean a(WifiManager wifiManager) throws Throwable {
                return Boolean.valueOf(wifiManager.isWifiEnabled());
            }
        }, this.b, "getting wifi enabled state", "WifiManager", Boolean.valueOf(false))).booleanValue();
    }

    public List<a> b() {
        if (this.i.b() || this.i.c()) {
            ArrayList arrayList = new ArrayList();
            a((List<a>) arrayList);
            this.i.a(arrayList);
        }
        return (List) this.i.a();
    }

    private void a(List<a> list) {
        if (h()) {
            StringBuilder sb = new StringBuilder();
            try {
                Iterator it = Collections.list(NetworkInterface.getNetworkInterfaces()).iterator();
                while (it.hasNext()) {
                    NetworkInterface networkInterface = (NetworkInterface) it.next();
                    byte[] hardwareAddress = networkInterface.getHardwareAddress();
                    if (hardwareAddress != null) {
                        for (byte valueOf : hardwareAddress) {
                            sb.append(String.format(Locale.US, "%02X:", new Object[]{Byte.valueOf(valueOf)}));
                        }
                        if (sb.length() > 0) {
                            sb.deleteCharAt(sb.length() - 1);
                            list.add(new a(networkInterface.getName(), sb.toString()));
                            sb.setLength(0);
                        }
                    }
                }
            } catch (Throwable unused) {
            }
        }
    }

    @Nullable
    public String b(final Context context) {
        return (String) cx.a((wo<T, S>) new wo<WifiManager, String>() {
            public String a(WifiManager wifiManager) throws Throwable {
                String str = null;
                if (!cy.this.g() || !cy.this.c.e(context)) {
                    return null;
                }
                WifiConfiguration wifiConfiguration = (WifiConfiguration) wifiManager.getClass().getMethod("getWifiApConfiguration", new Class[0]).invoke(wifiManager, new Object[0]);
                if (wifiConfiguration != null) {
                    str = wifiConfiguration.SSID;
                }
                return str;
            }
        }, this.b, "getting wifi access point name", "WifiManager");
    }

    public int c(final Context context) {
        return ((Integer) cx.a(new wo<WifiManager, Integer>() {
            public Integer a(WifiManager wifiManager) throws Throwable {
                if (!cy.this.g() || !cy.this.c.e(context)) {
                    return null;
                }
                int intValue = ((Integer) wifiManager.getClass().getMethod("getWifiApState", new Class[0]).invoke(wifiManager, new Object[0])).intValue();
                if (intValue > 10) {
                    intValue -= 10;
                }
                return Integer.valueOf(intValue);
            }
        }, this.b, "getting access point state", "WifiManager", Integer.valueOf(-1))).intValue();
    }

    public void a(boolean z) {
        this.d.a(z);
        this.c.a(this.d.a());
    }

    /* access modifiers changed from: private */
    public synchronized boolean g() {
        return k() && this.j.o.l;
    }

    private synchronized boolean h() {
        return k() && this.j.o.k;
    }

    private synchronized boolean i() {
        return k() && this.j.o.j;
    }

    private synchronized boolean j() {
        return k() && this.j.o.i;
    }

    private synchronized boolean k() {
        return this.j != null;
    }

    @VisibleForTesting
    cy(Context context, @Nullable WifiManager wifiManager, @NonNull px pxVar, @NonNull pr prVar, @NonNull wf wfVar) {
        this.d = new px();
        this.h = new com.yandex.metrica.impl.ob.p.a<>();
        this.i = new com.yandex.metrica.impl.ob.p.a<>();
        this.f4757a = context;
        this.b = wifiManager;
        this.d = pxVar;
        this.c = prVar;
        this.k = l();
        this.e = wfVar;
    }

    @NonNull
    private ps l() {
        if (cx.a(28)) {
            return new ps() {
                public boolean a(@NonNull Context context) {
                    return cy.this.c.c(context) && cy.this.c.e(context);
                }
            };
        }
        if (cx.a(26)) {
            return new ps() {
                public boolean a(@NonNull Context context) {
                    return cy.this.c.c(context) || cy.this.c.f(context);
                }
            };
        }
        return new ps() {
            public boolean a(@NonNull Context context) {
                return cy.this.c.c(context);
            }
        };
    }
}
