package com.yandex.metrica.impl.ob;

import java.util.concurrent.TimeUnit;

public interface p {

    public static class a<T> {

        /* renamed from: a reason: collision with root package name */
        public static final long f5051a = TimeUnit.SECONDS.toMillis(10);
        private long b;
        private long c;
        private T d;
        private boolean e;

        public a() {
            this(f5051a);
        }

        public a(long j) {
            this.c = 0;
            this.d = null;
            this.e = true;
            this.b = j;
        }

        public T a() {
            return this.d;
        }

        public void a(T t) {
            this.d = t;
            e();
            this.e = false;
        }

        public final boolean b() {
            return this.d == null;
        }

        private void e() {
            this.c = System.currentTimeMillis();
        }

        public final boolean a(long j) {
            long currentTimeMillis = System.currentTimeMillis() - this.c;
            return currentTimeMillis > j || currentTimeMillis < 0;
        }

        public final boolean c() {
            return a(this.b);
        }

        public T d() {
            if (c()) {
                return null;
            }
            return this.d;
        }
    }
}
