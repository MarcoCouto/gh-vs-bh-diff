package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import com.yandex.metrica.impl.ob.eg.a;

public class fi extends en {
    private final String b;
    private final cs c;

    public fi(@NonNull Context context, @NonNull ek ekVar, @NonNull bl blVar, @NonNull a aVar, @NonNull cs csVar, @NonNull uk ukVar, @NonNull un unVar) {
        super(context, ukVar, blVar, ekVar, aVar, new fg(csVar), unVar);
        this.b = ekVar.a();
        this.c = csVar;
    }

    public synchronized void a(@NonNull a aVar) {
        super.a(aVar);
        this.c.a(this.b, aVar.m);
    }
}
