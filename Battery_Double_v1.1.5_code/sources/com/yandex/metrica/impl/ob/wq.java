package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;

public class wq {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final String f5290a;

    public wq(@NonNull Context context) {
        this(context.getPackageName());
    }

    @VisibleForTesting
    wq(@NonNull String str) {
        this.f5290a = str;
    }

    public byte[] a() {
        try {
            return wc.a(this.f5290a);
        } catch (Throwable unused) {
            return new byte[16];
        }
    }

    public byte[] b() {
        try {
            return wc.a(new StringBuilder(this.f5290a).reverse().toString());
        } catch (Throwable unused) {
            return new byte[16];
        }
    }
}
