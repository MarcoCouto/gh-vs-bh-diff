package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class vw<K, V> {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Map<K, V> f5281a;
    @NonNull
    private final V b;

    public vw(@NonNull V v) {
        this(new HashMap(), v);
    }

    @VisibleForTesting
    public vw(@NonNull Map<K, V> map, @NonNull V v) {
        this.f5281a = map;
        this.b = v;
    }

    public void a(@Nullable K k, @Nullable V v) {
        this.f5281a.put(k, v);
    }

    @NonNull
    public V a(@Nullable K k) {
        V v = this.f5281a.get(k);
        return v == null ? this.b : v;
    }

    @NonNull
    public Set<K> a() {
        return this.f5281a.keySet();
    }
}
