package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import java.util.concurrent.Executor;

public class bm {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private Executor f4694a;

    public bm() {
        this(al.a().j().b());
    }

    public bm(@NonNull Executor executor) {
        this.f4694a = executor;
    }

    public bl a(@NonNull Context context, @NonNull ek ekVar) {
        bl blVar = new bl(context, ekVar, this.f4694a);
        StringBuilder sb = new StringBuilder();
        sb.append("YMM-NC[");
        sb.append(ekVar.c());
        sb.append(RequestParameters.RIGHT_BRACKETS);
        blVar.setName(xm.a(sb.toString()));
        blVar.start();
        return blVar;
    }
}
