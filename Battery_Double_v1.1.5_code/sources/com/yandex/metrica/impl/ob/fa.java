package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import com.yandex.metrica.impl.ob.eg.a;
import com.yandex.metrica.impl.ob.st.d;
import java.io.File;

public class fa extends eo {
    public fa(@NonNull Context context, @NonNull ek ekVar, @NonNull a aVar, @NonNull un unVar, @NonNull uk ukVar, @NonNull d dVar, @NonNull bl blVar, @NonNull xh xhVar, int i) {
        super(context, ekVar, aVar, unVar, ukVar, dVar, blVar, xhVar, i);
    }

    @NonNull
    public ez.a a(@NonNull ez ezVar) {
        ezVar.getClass();
        return new ez.a();
    }

    @NonNull
    public bk b(@NonNull ez ezVar) {
        return new bk(ezVar);
    }

    @NonNull
    public bj a(@NonNull bk bkVar, @NonNull lw lwVar) {
        return new bj(this.f4826a, bkVar, lwVar);
    }

    @NonNull
    public ku a(@NonNull ah ahVar, @NonNull wm<File> wmVar) {
        return new ku(ahVar.c(this.f4826a), wmVar);
    }
}
