package com.yandex.metrica.impl.ob;

import java.util.Collections;
import java.util.List;

public class gn<BaseHandler> extends go<BaseHandler> {

    /* renamed from: a reason: collision with root package name */
    private final List<BaseHandler> f4857a;

    public gn(List<BaseHandler> list) {
        this.f4857a = Collections.unmodifiableList(list);
    }

    public List<? extends BaseHandler> a() {
        return this.f4857a;
    }
}
