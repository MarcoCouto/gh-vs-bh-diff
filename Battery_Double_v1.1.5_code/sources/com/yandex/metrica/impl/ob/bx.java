package com.yandex.metrica.impl.ob;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.yandex.metrica.DeferredDeeplinkParametersListener;
import com.yandex.metrica.DeferredDeeplinkParametersListener.Error;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class bx {

    /* renamed from: a reason: collision with root package name */
    private final boolean f4704a;
    /* access modifiers changed from: private */
    public final cd b;
    /* access modifiers changed from: private */
    public final lv c;
    private String d;
    private Map<String, String> e;
    private DeferredDeeplinkParametersListener f;

    public bx(cd cdVar, lv lvVar, @NonNull xh xhVar) {
        this(cdVar, lvVar, new sl(cdVar.b()), xhVar);
    }

    @VisibleForTesting
    bx(cd cdVar, lv lvVar, @NonNull final sl slVar, @NonNull xh xhVar) {
        this.b = cdVar;
        this.c = lvVar;
        this.d = lvVar.c();
        this.f4704a = lvVar.d();
        if (this.f4704a) {
            this.c.n(null);
            this.d = null;
        } else {
            e(b(this.d));
        }
        if (!this.c.e()) {
            xhVar.a((Runnable) new Runnable() {
                public void run() {
                    slVar.a(new sk() {
                        public void a(@NonNull sj sjVar) {
                            bx.this.b.a(sjVar);
                            bx.this.d(sjVar.f5159a);
                            a();
                        }

                        public void a(@NonNull Throwable th) {
                            bx.this.b.a((sj) null);
                            a();
                        }

                        private void a() {
                            bx.this.c.g();
                        }
                    });
                }
            });
        }
    }

    public void a(String str) {
        this.b.a(str);
        d(str);
    }

    /* access modifiers changed from: private */
    public void d(@Nullable String str) {
        if (!(this.f4704a || TextUtils.isEmpty(str)) && TextUtils.isEmpty(this.d)) {
            synchronized (this) {
                this.d = str;
                this.c.n(this.d);
                e(b(str));
                a();
            }
        }
    }

    private void e(String str) {
        if (!TextUtils.isEmpty(str)) {
            this.e = c(str);
        }
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public final String b(String str) {
        return (String) f(str).get("appmetrica_deep_link");
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public final Map<String, String> c(String str) {
        Map f2 = f(Uri.decode(str));
        HashMap hashMap = new HashMap(f2.size());
        for (Entry entry : f2.entrySet()) {
            hashMap.put(Uri.decode((String) entry.getKey()), Uri.decode((String) entry.getValue()));
        }
        return hashMap;
    }

    private static Map<String, String> f(String str) {
        String[] split;
        HashMap hashMap = new HashMap();
        if (str != null) {
            String g = g(str);
            if (h(g)) {
                for (String str2 : g.split(RequestParameters.AMPERSAND)) {
                    int indexOf = str2.indexOf(RequestParameters.EQUAL);
                    if (indexOf >= 0) {
                        hashMap.put(str2.substring(0, indexOf), str2.substring(indexOf + 1));
                    } else {
                        hashMap.put(str2, "");
                    }
                }
            }
        }
        return hashMap;
    }

    private static String g(String str) {
        int lastIndexOf = str.lastIndexOf(63);
        return lastIndexOf >= 0 ? str.substring(lastIndexOf + 1) : str;
    }

    private static boolean h(String str) {
        return str.contains(RequestParameters.EQUAL);
    }

    private void a() {
        if (cx.a((Map) this.e)) {
            if (this.d != null) {
                a(Error.PARSE_ERROR);
            }
        } else if (this.f != null) {
            this.f.onParametersLoaded(this.e);
            this.f = null;
        }
    }

    private void a(Error error) {
        if (this.f != null) {
            this.f.onError(error, this.d);
            this.f = null;
        }
    }

    public synchronized void a(DeferredDeeplinkParametersListener deferredDeeplinkParametersListener) {
        try {
            this.f = deferredDeeplinkParametersListener;
            if (this.f4704a) {
                a(Error.NOT_A_FIRST_LAUNCH);
            } else {
                a();
            }
            this.c.f();
        } catch (Throwable th) {
            this.c.f();
            throw th;
        }
    }
}
