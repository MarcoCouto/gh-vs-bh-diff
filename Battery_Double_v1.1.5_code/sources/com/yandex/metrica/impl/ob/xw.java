package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;

public class xw {

    /* renamed from: a reason: collision with root package name */
    private final xz f5309a;
    private final xz b;
    private final xs c;
    @NonNull
    private final vz d;
    private final String e;

    public xw(int i, int i2, int i3, @NonNull String str, @NonNull vz vzVar) {
        xs xsVar = new xs(i);
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append("map key");
        xz xzVar = new xz(i2, sb.toString(), vzVar);
        StringBuilder sb2 = new StringBuilder();
        sb2.append(str);
        sb2.append("map value");
        this(xsVar, xzVar, new xz(i3, sb2.toString(), vzVar), str, vzVar);
    }

    @VisibleForTesting
    xw(@NonNull xs xsVar, @NonNull xz xzVar, @NonNull xz xzVar2, @NonNull String str, @NonNull vz vzVar) {
        this.c = xsVar;
        this.f5309a = xzVar;
        this.b = xzVar2;
        this.e = str;
        this.d = vzVar;
    }

    public xz a() {
        return this.f5309a;
    }

    public xz b() {
        return this.b;
    }

    public xs c() {
        return this.c;
    }

    public void a(@NonNull String str) {
        if (this.d.c()) {
            this.d.b("The %s has reached the limit of %d items. Item with key %s will be ignored", this.e, Integer.valueOf(this.c.a()), str);
        }
    }
}
