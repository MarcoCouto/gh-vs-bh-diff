package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;

public class tb {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private mf<te> f5180a;
    @NonNull
    private te b;
    @NonNull
    private wg c;
    @NonNull
    private tg d;
    @NonNull
    private a e;

    interface a {
        void a();
    }

    public tb(@NonNull Context context, @NonNull mf<te> mfVar, @NonNull a aVar) {
        this(mfVar, aVar, new wg(), new tg(context, mfVar));
    }

    @VisibleForTesting
    tb(@NonNull mf<te> mfVar, @NonNull a aVar, @NonNull wg wgVar, @NonNull tg tgVar) {
        this.f5180a = mfVar;
        this.b = (te) this.f5180a.a();
        this.c = wgVar;
        this.d = tgVar;
        this.e = aVar;
    }

    public void a(@NonNull te teVar) {
        this.f5180a.a(teVar);
        this.b = teVar;
        this.d.a();
        this.e.a();
    }

    public void a() {
        te teVar = new te(this.b.f5187a, this.b.b, this.c.a(), true, true);
        this.f5180a.a(teVar);
        this.b = teVar;
        this.e.a();
    }
}
