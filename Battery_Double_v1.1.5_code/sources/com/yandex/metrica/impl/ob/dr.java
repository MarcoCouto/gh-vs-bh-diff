package com.yandex.metrica.impl.ob;

import android.support.annotation.VisibleForTesting;
import java.util.Iterator;
import java.util.List;
import java.util.WeakHashMap;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.LinkedBlockingQueue;

public class dr {

    /* renamed from: a reason: collision with root package name */
    private final xl f4792a = xm.a("YMM-BD", new Runnable() {
        public void run() {
            while (dr.this.b) {
                try {
                    ((a) dr.this.c.take()).a();
                } catch (InterruptedException unused) {
                }
            }
        }
    });
    /* access modifiers changed from: private */
    public volatile boolean b = true;
    /* access modifiers changed from: private */
    public final BlockingQueue<a> c = new LinkedBlockingQueue();
    private ConcurrentHashMap<Class, CopyOnWriteArrayList<dv<? extends dt>>> d = new ConcurrentHashMap<>();
    private WeakHashMap<Object, CopyOnWriteArrayList<c>> e = new WeakHashMap<>();
    private ConcurrentHashMap<Class, dt> f = new ConcurrentHashMap<>();

    private static class a {

        /* renamed from: a reason: collision with root package name */
        private final dt f4794a;
        private final dv<? extends dt> b;

        private a(dt dtVar, dv<? extends dt> dvVar) {
            this.f4794a = dtVar;
            this.b = dvVar;
        }

        /* access modifiers changed from: 0000 */
        public void a() {
            try {
                if (!this.b.b(this.f4794a)) {
                    this.b.a(this.f4794a);
                }
            } catch (Throwable unused) {
            }
        }
    }

    private static final class b {
        /* access modifiers changed from: private */

        /* renamed from: a reason: collision with root package name */
        public static final dr f4795a = new dr();
    }

    private static class c {

        /* renamed from: a reason: collision with root package name */
        final CopyOnWriteArrayList<dv<? extends dt>> f4796a;
        final dv<? extends dt> b;

        private c(CopyOnWriteArrayList<dv<? extends dt>> copyOnWriteArrayList, dv<? extends dt> dvVar) {
            this.f4796a = copyOnWriteArrayList;
            this.b = dvVar;
        }

        /* access modifiers changed from: protected */
        public void a() {
            this.f4796a.remove(this.b);
        }

        /* access modifiers changed from: protected */
        public void finalize() throws Throwable {
            super.finalize();
            a();
        }
    }

    public static final dr a() {
        return b.f4795a;
    }

    @VisibleForTesting
    dr() {
        this.f4792a.start();
    }

    public synchronized void a(dt dtVar) {
        CopyOnWriteArrayList copyOnWriteArrayList = (CopyOnWriteArrayList) this.d.get(dtVar.getClass());
        if (copyOnWriteArrayList != null) {
            Iterator it = copyOnWriteArrayList.iterator();
            while (it.hasNext()) {
                a(dtVar, (dv) it.next());
            }
        }
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public void a(dt dtVar, dv<? extends dt> dvVar) {
        this.c.add(new a(dtVar, dvVar));
    }

    public synchronized void b(dt dtVar) {
        a(dtVar);
        this.f.put(dtVar.getClass(), dtVar);
    }

    public synchronized void a(Class<? extends dt> cls) {
        this.f.remove(cls);
    }

    public synchronized void a(Object obj, Class cls, dv<? extends dt> dvVar) {
        CopyOnWriteArrayList copyOnWriteArrayList = (CopyOnWriteArrayList) this.d.get(cls);
        if (copyOnWriteArrayList == null) {
            copyOnWriteArrayList = new CopyOnWriteArrayList();
            this.d.put(cls, copyOnWriteArrayList);
        }
        copyOnWriteArrayList.add(dvVar);
        CopyOnWriteArrayList copyOnWriteArrayList2 = (CopyOnWriteArrayList) this.e.get(obj);
        if (copyOnWriteArrayList2 == null) {
            copyOnWriteArrayList2 = new CopyOnWriteArrayList();
            this.e.put(obj, copyOnWriteArrayList2);
        }
        copyOnWriteArrayList2.add(new c(copyOnWriteArrayList, dvVar));
        dt dtVar = (dt) this.f.get(cls);
        if (dtVar != null) {
            a(dtVar, dvVar);
        }
    }

    public synchronized void a(Object obj) {
        List<c> list = (List) this.e.remove(obj);
        if (list != null) {
            for (c a2 : list) {
                a2.a();
            }
        }
    }
}
