package com.yandex.metrica.impl.ob;

import android.annotation.TargetApi;
import android.app.ActivityManager;
import android.app.usage.UsageStatsManager;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import com.yandex.metrica.impl.ob.l.a;

public class m {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Context f4989a;
    /* access modifiers changed from: private */
    @NonNull
    public final j b;

    public m(@NonNull Context context) {
        this(context, new j());
    }

    @VisibleForTesting
    m(@NonNull Context context, @NonNull j jVar) {
        this.f4989a = context;
        this.b = jVar;
    }

    @Nullable
    public l a() {
        if (cx.a(28)) {
            return b();
        }
        return null;
    }

    @TargetApi(28)
    @NonNull
    private l b() {
        return new l((a) cx.a((wo<T, S>) new wo<UsageStatsManager, a>() {
            public a a(UsageStatsManager usageStatsManager) {
                return m.this.b.a(usageStatsManager.getAppStandbyBucket());
            }
        }, (UsageStatsManager) this.f4989a.getSystemService("usagestats"), "getting app standby bucket", "usageStatsManager"), (Boolean) cx.a((wo<T, S>) new wo<ActivityManager, Boolean>() {
            public Boolean a(ActivityManager activityManager) throws Throwable {
                return Boolean.valueOf(activityManager.isBackgroundRestricted());
            }
        }, (ActivityManager) this.f4989a.getSystemService("activity"), "getting is background restricted", "activityManager"));
    }
}
