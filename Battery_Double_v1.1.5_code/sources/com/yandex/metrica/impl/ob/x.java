package com.yandex.metrica.impl.ob;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.util.HashMap;
import java.util.Map;

@SuppressLint({"ParcelCreator"})
public class x extends ResultReceiver {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final a f5298a;

    public interface a {
        void a(int i, @NonNull Bundle bundle);
    }

    public x(Handler handler, @NonNull a aVar) {
        super(handler);
        this.f5298a = aVar;
    }

    /* access modifiers changed from: protected */
    public void onReceiveResult(int i, Bundle bundle) {
        a aVar = this.f5298a;
        if (bundle == null) {
            bundle = new Bundle();
        }
        aVar.a(i, bundle);
    }

    public static void a(@Nullable ResultReceiver resultReceiver, @NonNull uk ukVar) {
        if (resultReceiver != null) {
            Bundle bundle = new Bundle();
            a(bundle, ukVar);
            resultReceiver.send(1, bundle);
        }
    }

    public static void a(ResultReceiver resultReceiver, ue ueVar, @Nullable uk ukVar) {
        if (resultReceiver != null) {
            Bundle bundle = new Bundle();
            ueVar.a(bundle);
            if (ukVar != null) {
                a(bundle, ukVar);
            }
            resultReceiver.send(2, bundle);
        }
    }

    private static void a(@NonNull Bundle bundle, @NonNull uk ukVar) {
        bundle.putString("Uuid", ukVar.f5236a);
        bundle.putString("DeviceId", ukVar.b);
        bundle.putString("DeviceIdHash", ukVar.d);
        bundle.putString("AdUrlGet", ukVar.f);
        bundle.putString("AdUrlReport", ukVar.g);
        bundle.putLong("ServerTimeOffset", wi.c());
        Map a2 = we.a(ukVar.m);
        if (a2.isEmpty()) {
            a2 = new HashMap();
        }
        bundle.putString("Clids", vq.a(a2));
        bundle.putString("RequestClids", ukVar.n);
    }
}
