package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import com.yandex.metrica.impl.ob.rp.a.C0118a;
import com.yandex.metrica.impl.ob.rp.a.C0118a.C0119a;
import com.yandex.metrica.impl.ob.rs.a;
import com.yandex.metrica.impl.ob.rs.a.C0128a;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class na implements mv<a, rp.a> {

    /* renamed from: a reason: collision with root package name */
    private static final Map<Integer, bt.a> f4999a = Collections.unmodifiableMap(new HashMap<Integer, bt.a>() {
        {
            put(Integer.valueOf(1), bt.a.WIFI);
            put(Integer.valueOf(2), bt.a.CELL);
        }
    });
    private static final Map<bt.a, Integer> b = Collections.unmodifiableMap(new HashMap<bt.a, Integer>() {
        {
            put(bt.a.WIFI, Integer.valueOf(1));
            put(bt.a.CELL, Integer.valueOf(2));
        }
    });

    @NonNull
    /* renamed from: a */
    public rp.a b(@NonNull a aVar) {
        rp.a aVar2 = new rp.a();
        Set a2 = aVar.a();
        aVar2.c = (String[]) a2.toArray(new String[a2.size()]);
        aVar2.b = b(aVar);
        return aVar2;
    }

    @NonNull
    public a a(@NonNull rp.a aVar) {
        return new a(b(aVar), Arrays.asList(aVar.c));
    }

    @NonNull
    private List<C0128a> b(@NonNull rp.a aVar) {
        C0118a[] aVarArr;
        ArrayList arrayList = new ArrayList();
        for (C0118a aVar2 : aVar.b) {
            C0128a aVar3 = new C0128a(aVar2.b, aVar2.c, aVar2.d, a(aVar2.e), aVar2.f, a(aVar2.g));
            arrayList.add(aVar3);
        }
        return arrayList;
    }

    @NonNull
    private wp<String, String> a(@NonNull C0119a[] aVarArr) {
        wp<String, String> wpVar = new wp<>();
        for (C0119a aVar : aVarArr) {
            wpVar.a(aVar.b, aVar.c);
        }
        return wpVar;
    }

    private C0118a[] b(@NonNull a aVar) {
        List b2 = aVar.b();
        C0118a[] aVarArr = new C0118a[b2.size()];
        for (int i = 0; i < b2.size(); i++) {
            aVarArr[i] = a((C0128a) b2.get(i));
        }
        return aVarArr;
    }

    @NonNull
    private C0118a a(@NonNull C0128a aVar) {
        C0118a aVar2 = new C0118a();
        aVar2.b = aVar.f5092a;
        aVar2.c = aVar.b;
        aVar2.e = b(aVar);
        aVar2.d = aVar.c;
        aVar2.f = aVar.e;
        aVar2.g = a(aVar.f);
        return aVar2;
    }

    @NonNull
    private C0119a[] b(@NonNull C0128a aVar) {
        C0119a[] aVarArr = new C0119a[aVar.d.a()];
        int i = 0;
        for (Entry entry : aVar.d.b()) {
            for (String str : (Collection) entry.getValue()) {
                C0119a aVar2 = new C0119a();
                aVar2.b = (String) entry.getKey();
                aVar2.c = str;
                aVarArr[i] = aVar2;
                i++;
            }
        }
        return aVarArr;
    }

    @NonNull
    private List<bt.a> a(@NonNull int[] iArr) {
        ArrayList arrayList = new ArrayList(iArr.length);
        for (int valueOf : iArr) {
            arrayList.add(f4999a.get(Integer.valueOf(valueOf)));
        }
        return arrayList;
    }

    @NonNull
    private int[] a(@NonNull List<bt.a> list) {
        int[] iArr = new int[list.size()];
        for (int i = 0; i < list.size(); i++) {
            iArr[i] = ((Integer) b.get(list.get(i))).intValue();
        }
        return iArr;
    }
}
