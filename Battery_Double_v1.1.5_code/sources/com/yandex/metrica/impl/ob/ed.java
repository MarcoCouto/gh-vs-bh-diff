package com.yandex.metrica.impl.ob;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.metrica.CounterConfiguration;

public class ed {
    @Nullable

    /* renamed from: a reason: collision with root package name */
    private final ee f4807a;
    @NonNull
    private final CounterConfiguration b;

    public ed(@NonNull Bundle bundle) {
        this.f4807a = ee.a(bundle);
        this.b = CounterConfiguration.c(bundle);
    }

    public ed(@NonNull ee eeVar, @NonNull CounterConfiguration counterConfiguration) {
        this.f4807a = eeVar;
        this.b = counterConfiguration;
    }

    @NonNull
    public ee g() {
        return this.f4807a;
    }

    @NonNull
    public CounterConfiguration h() {
        return this.b;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("ClientConfiguration{mProcessConfiguration=");
        sb.append(this.f4807a);
        sb.append(", mCounterConfiguration=");
        sb.append(this.b);
        sb.append('}');
        return sb.toString();
    }
}
