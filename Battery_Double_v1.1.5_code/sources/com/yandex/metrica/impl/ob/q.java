package com.yandex.metrica.impl.ob;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.pm.Signature;
import android.content.pm.SigningInfo;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class q {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Context f5073a;
    @NonNull
    private final lw b;
    @NonNull
    private final String c;
    @NonNull
    private final yc d;

    public q(@NonNull Context context, @NonNull lw lwVar) {
        this(context, lwVar, context.getPackageName(), new yc());
    }

    @VisibleForTesting
    q(@NonNull Context context, @NonNull lw lwVar, @NonNull String str, @NonNull yc ycVar) {
        this.f5073a = context;
        this.b = lwVar;
        this.c = str;
        this.d = ycVar;
    }

    @NonNull
    public List<String> a() {
        List<String> b2 = b();
        if (b2.isEmpty()) {
            b2 = c();
            if (!b2.isEmpty()) {
                a(b2);
            }
        }
        return b2;
    }

    @NonNull
    private List<String> b() {
        return this.b.o();
    }

    @NonNull
    private List<String> c() {
        Signature[] signatureArr;
        ArrayList arrayList = new ArrayList();
        try {
            if (cx.a(28)) {
                signatureArr = d();
            } else {
                signatureArr = this.d.a(this.f5073a, this.c, 64).signatures;
            }
            if (signatureArr != null) {
                for (Signature a2 : signatureArr) {
                    String a3 = a(a2);
                    if (a3 != null) {
                        arrayList.add(a3);
                    }
                }
            }
        } catch (Throwable unused) {
        }
        Collections.sort(arrayList);
        return arrayList;
    }

    @Nullable
    @TargetApi(28)
    private Signature[] d() {
        SigningInfo signingInfo = this.d.a(this.f5073a, this.c, 134217728).signingInfo;
        if (signingInfo.hasMultipleSigners()) {
            return signingInfo.getApkContentsSigners();
        }
        return signingInfo.getSigningCertificateHistory();
    }

    @Nullable
    private String a(@NonNull Signature signature) {
        try {
            return cu.b(MessageDigest.getInstance("SHA1").digest(signature.toByteArray()));
        } catch (Throwable unused) {
            return null;
        }
    }

    private void a(@NonNull List<String> list) {
        this.b.a(list).q();
    }
}
