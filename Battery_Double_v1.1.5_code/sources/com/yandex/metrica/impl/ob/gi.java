package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.metrica.impl.ob.sn.c;
import java.util.List;

public class gi extends sq {
    @Nullable

    /* renamed from: a reason: collision with root package name */
    private List<String> f4850a;
    @NonNull
    private String b;
    private Boolean c;

    public static final class a extends com.yandex.metrica.impl.ob.sn.a<com.yandex.metrica.impl.ob.eg.a, a> {
        @NonNull

        /* renamed from: a reason: collision with root package name */
        public final String f4851a;
        public final boolean b;

        public a(@Nullable String str, @Nullable String str2, @Nullable String str3, @NonNull String str4, @Nullable Boolean bool) {
            super(str, str2, str3);
            this.f4851a = str4;
            this.b = wk.a(bool, true);
        }

        public a(@NonNull com.yandex.metrica.impl.ob.eg.a aVar) {
            this(aVar.f4810a, aVar.b, aVar.c, aVar.d, aVar.m);
        }

        @NonNull
        /* renamed from: a */
        public a b(@NonNull com.yandex.metrica.impl.ob.eg.a aVar) {
            a aVar2 = new a((String) wk.a(aVar.f4810a, this.c), (String) wk.a(aVar.b, this.d), (String) wk.a(aVar.c, this.e), wk.b(aVar.d, this.f4851a), (Boolean) wk.a(aVar.m, Boolean.valueOf(this.b)));
            return aVar2;
        }

        /* renamed from: b */
        public boolean a(@NonNull com.yandex.metrica.impl.ob.eg.a aVar) {
            boolean z = false;
            if (aVar.f4810a != null && !aVar.f4810a.equals(this.c)) {
                return false;
            }
            if (aVar.b != null && !aVar.b.equals(this.d)) {
                return false;
            }
            if (aVar.c != null && !aVar.c.equals(this.e)) {
                return false;
            }
            if (aVar.d == null || aVar.d.equals(this.f4851a)) {
                z = true;
            }
            return z;
        }
    }

    public static class b extends a<gi, a> {
        public b(@NonNull Context context, @NonNull String str) {
            super(context, str);
        }

        /* access modifiers changed from: protected */
        @NonNull
        /* renamed from: a */
        public gi b() {
            return new gi();
        }

        @NonNull
        /* renamed from: a */
        public gi c(@NonNull c<a> cVar) {
            gi giVar = (gi) super.c(cVar);
            giVar.a(cVar.f5164a.l);
            giVar.a(((a) cVar.b).f4851a);
            giVar.a(Boolean.valueOf(((a) cVar.b).b));
            return giVar;
        }
    }

    @Nullable
    public List<String> a() {
        return this.f4850a;
    }

    public void a(@Nullable List<String> list) {
        this.f4850a = list;
    }

    @NonNull
    public String b() {
        return this.b;
    }

    public void a(@NonNull String str) {
        this.b = str;
    }

    @Nullable
    public Boolean c() {
        return this.c;
    }

    public void a(Boolean bool) {
        this.c = bool;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("DiagnosticRequestConfig{mDiagnosticHosts=");
        sb.append(this.f4850a);
        sb.append(", mApiKey='");
        sb.append(this.b);
        sb.append('\'');
        sb.append(", statisticsSending=");
        sb.append(this.c);
        sb.append('}');
        return sb.toString();
    }
}
