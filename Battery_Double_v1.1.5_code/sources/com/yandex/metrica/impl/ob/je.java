package com.yandex.metrica.impl.ob;

public class je {

    /* renamed from: a reason: collision with root package name */
    private long f4902a;
    private long b;
    private long c;
    private jh d;

    public long a() {
        return this.f4902a;
    }

    public je a(long j) {
        this.f4902a = j;
        return this;
    }

    public jh b() {
        return this.d;
    }

    public je a(jh jhVar) {
        this.d = jhVar;
        return this;
    }

    public long c() {
        return this.b;
    }

    public je b(long j) {
        this.b = j;
        return this;
    }

    public long d() {
        return this.c;
    }

    public je c(long j) {
        this.c = j;
        return this;
    }
}
