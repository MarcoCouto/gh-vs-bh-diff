package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import com.yandex.metrica.IReporter;
import java.util.HashMap;
import java.util.Map;

public class he extends hd {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final IReporter f4870a;

    public he(@NonNull en enVar, @NonNull IReporter iReporter) {
        super(enVar);
        this.f4870a = iReporter;
    }

    public boolean a(@NonNull w wVar) {
        kd a2 = kd.a(wVar.g());
        HashMap hashMap = new HashMap();
        hashMap.put("type", a2.f4934a);
        hashMap.put("delivery_method", a2.b);
        this.f4870a.reportEvent("crash_saved", (Map<String, Object>) hashMap);
        return false;
    }
}
