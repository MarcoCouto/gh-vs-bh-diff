package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.metrica.impl.ob.vq.a;

public class sr {
    @Nullable

    /* renamed from: a reason: collision with root package name */
    public final String f5168a;
    @Nullable
    public final String b;
    @Nullable
    @Deprecated
    public final String c;
    @Nullable
    public final String d;
    @Nullable
    public final String e;
    @Nullable
    public final String f;
    @Nullable
    public final String g;
    @Nullable
    public final String h;
    @Nullable
    public final String i;
    @Nullable
    public final String j;
    @Nullable
    public final String k;
    @Nullable
    public final String l;
    @Nullable
    public final String m;
    @Nullable
    public final String n;
    @Nullable
    public final String o;

    public sr(@NonNull a aVar) {
        String str;
        this.f5168a = aVar.a("dId");
        this.b = aVar.a("uId");
        this.c = aVar.b("kitVer");
        this.d = aVar.a("analyticsSdkVersionName");
        this.e = aVar.a("kitBuildNumber");
        this.f = aVar.a("kitBuildType");
        this.g = aVar.a("appVer");
        this.h = aVar.optString("app_debuggable", "0");
        this.i = aVar.a("appBuild");
        this.j = aVar.a("osVer");
        this.l = aVar.a("lang");
        this.m = aVar.a("root");
        this.n = aVar.optString("app_framework", ci.b());
        int optInt = aVar.optInt("osApiLev", -1);
        String str2 = null;
        if (optInt == -1) {
            str = null;
        } else {
            str = String.valueOf(optInt);
        }
        this.k = str;
        int optInt2 = aVar.optInt("attribution_id", 0);
        if (optInt2 > 0) {
            str2 = String.valueOf(optInt2);
        }
        this.o = str2;
    }

    public sr() {
        this.f5168a = null;
        this.b = null;
        this.c = null;
        this.d = null;
        this.e = null;
        this.f = null;
        this.g = null;
        this.h = null;
        this.i = null;
        this.j = null;
        this.k = null;
        this.l = null;
        this.m = null;
        this.n = null;
        this.o = null;
    }
}
