package com.yandex.metrica.impl.ob;

import android.net.Uri.Builder;
import android.support.annotation.NonNull;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.tapjoy.TapjoyConstants;
import com.yandex.metrica.impl.ac.a.c;

public class sx extends sy<ph> {

    /* renamed from: a reason: collision with root package name */
    private long f5175a;

    public void a(@NonNull Builder builder, @NonNull ph phVar) {
        String str;
        String str2;
        super.a(builder, phVar);
        builder.appendPath("location");
        builder.appendQueryParameter("deviceid", phVar.r());
        builder.appendQueryParameter(TapjoyConstants.TJC_DEVICE_TYPE_NAME, phVar.C());
        builder.appendQueryParameter("uuid", phVar.t());
        builder.appendQueryParameter("analytics_sdk_version_name", phVar.i());
        builder.appendQueryParameter("analytics_sdk_build_number", phVar.j());
        builder.appendQueryParameter("analytics_sdk_build_type", phVar.k());
        builder.appendQueryParameter("app_version_name", phVar.q());
        builder.appendQueryParameter("app_build_number", phVar.p());
        builder.appendQueryParameter(TapjoyConstants.TJC_DEVICE_OS_VERSION_NAME, phVar.n());
        builder.appendQueryParameter("os_api_level", String.valueOf(phVar.o()));
        builder.appendQueryParameter("is_rooted", phVar.u());
        builder.appendQueryParameter("app_framework", phVar.v());
        builder.appendQueryParameter("app_id", phVar.d());
        builder.appendQueryParameter("app_platform", phVar.l());
        builder.appendQueryParameter(TapjoyConstants.TJC_ANDROID_ID, phVar.B());
        builder.appendQueryParameter("request_id", String.valueOf(this.f5175a));
        c D = phVar.D();
        if (D == null) {
            str = "";
        } else {
            str = D.f4620a;
        }
        String str3 = CampaignEx.JSON_KEY_ADV_ID;
        if (str == null) {
            str = "";
        }
        builder.appendQueryParameter(str3, str);
        String str4 = "limit_ad_tracking";
        if (D == null) {
            str2 = "";
        } else {
            str2 = a(D.b);
        }
        builder.appendQueryParameter(str4, str2);
    }

    public void a(long j) {
        this.f5175a = j;
    }
}
