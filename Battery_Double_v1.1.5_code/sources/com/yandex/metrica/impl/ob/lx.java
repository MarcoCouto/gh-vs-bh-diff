package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import java.util.Arrays;
import java.util.List;
import org.json.JSONArray;

public abstract class lx {
    public static final String q = "lx";

    /* renamed from: a reason: collision with root package name */
    private final lf f4986a;
    private final String b;

    public lx(lf lfVar) {
        this(lfVar, null);
    }

    public lx(lf lfVar, String str) {
        this.f4986a = lfVar;
        this.b = str;
    }

    public String p() {
        return this.b;
    }

    /* access modifiers changed from: protected */
    public qk q(String str) {
        return new qk(str, p());
    }

    /* access modifiers changed from: protected */
    public <T extends lx> T b(String str, String str2) {
        synchronized (this) {
            this.f4986a.b(str, str2);
        }
        return this;
    }

    public <T extends lx> T a(String str, long j) {
        synchronized (this) {
            this.f4986a.b(str, j);
        }
        return this;
    }

    /* access modifiers changed from: protected */
    @NonNull
    public <T extends lx> T a(String str, int i) {
        synchronized (this) {
            this.f4986a.b(str, i);
        }
        return this;
    }

    public <T extends lx> T a(String str, boolean z) {
        synchronized (this) {
            this.f4986a.b(str, z);
        }
        return this;
    }

    /* access modifiers changed from: protected */
    public <T extends lx> T a(String str, String[] strArr) {
        String str2;
        try {
            JSONArray jSONArray = new JSONArray();
            for (String put : strArr) {
                jSONArray.put(put);
            }
            str2 = jSONArray.toString();
        } catch (Throwable unused) {
            str2 = null;
        }
        this.f4986a.b(str, str2);
        return this;
    }

    /* access modifiers changed from: protected */
    public <T extends lx> T a(String str, List<String> list) {
        return a(str, (String[]) list.toArray(new String[list.size()]));
    }

    public <T extends lx> T r(String str) {
        synchronized (this) {
            this.f4986a.a(str);
        }
        return this;
    }

    public void q() {
        synchronized (this) {
            this.f4986a.b();
        }
    }

    public long b(String str, long j) {
        return this.f4986a.a(str, j);
    }

    /* access modifiers changed from: 0000 */
    public int b(String str, int i) {
        return this.f4986a.a(str, i);
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public String s(@NonNull String str) {
        return this.f4986a.a(str, (String) null);
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public String c(@NonNull String str, @Nullable String str2) {
        return this.f4986a.a(str, str2);
    }

    public boolean b(String str, boolean z) {
        return this.f4986a.a(str, z);
    }

    public boolean t(@NonNull String str) {
        return this.f4986a.b(str);
    }

    /* access modifiers changed from: 0000 */
    public String[] b(String str, String[] strArr) {
        String a2 = this.f4986a.a(str, (String) null);
        if (TextUtils.isEmpty(a2)) {
            return strArr;
        }
        try {
            JSONArray jSONArray = new JSONArray(a2);
            String[] strArr2 = new String[jSONArray.length()];
            int i = 0;
            while (i < jSONArray.length()) {
                try {
                    strArr2[i] = jSONArray.optString(i);
                    i++;
                } catch (Throwable unused) {
                }
            }
            return strArr2;
        } catch (Throwable unused2) {
            return strArr;
        }
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public List<String> b(@NonNull String str, @Nullable List<String> list) {
        String[] b2 = b(str, list == null ? null : (String[]) list.toArray(new String[list.size()]));
        if (b2 == null) {
            return null;
        }
        return Arrays.asList(b2);
    }
}
