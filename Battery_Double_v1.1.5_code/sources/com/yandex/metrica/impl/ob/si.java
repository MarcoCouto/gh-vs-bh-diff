package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.support.annotation.WorkerThread;
import android.text.TextUtils;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.Set;

public class si {

    /* renamed from: a reason: collision with root package name */
    private static final EnumSet<b> f5156a = EnumSet.of(b.HAS_FROM_PLAY_SERVICES, b.HAS_FROM_RECEIVER_ONLY, b.RECEIVER);
    private static final EnumSet<b> b = EnumSet.of(b.HAS_FROM_PLAY_SERVICES, b.HAS_FROM_RECEIVER_ONLY);
    private final Set<a> c;
    @Nullable
    private sj d;
    @Nullable
    private sj e;
    private boolean f;
    @NonNull
    private final ly g;
    private b h;

    public interface a {
        boolean a(@NonNull sj sjVar);
    }

    private enum b {
        EMPTY,
        RECEIVER,
        WAIT_FOR_RECEIVER_ONLY,
        HAS_FROM_PLAY_SERVICES,
        HAS_FROM_RECEIVER_ONLY
    }

    @WorkerThread
    public si(@NonNull Context context) {
        this(new ly(ld.a(context).c()));
    }

    @VisibleForTesting
    si(@NonNull ly lyVar) {
        this.c = new HashSet();
        this.h = b.EMPTY;
        this.g = lyVar;
        this.f = this.g.d();
        if (!this.f) {
            String b2 = this.g.b();
            if (!TextUtils.isEmpty(b2)) {
                sj sjVar = new sj(b2, 0, 0);
                this.d = sjVar;
            }
            this.e = this.g.c();
            this.h = b.values()[this.g.d(0)];
        }
    }

    public synchronized void a(@Nullable sj sjVar) {
        if (!b.contains(this.h)) {
            this.e = sjVar;
            this.g.a(sjVar).q();
            a(b(sjVar));
        }
    }

    public synchronized void a(@Nullable String str) {
        if (!f5156a.contains(this.h) && !TextUtils.isEmpty(str)) {
            sj sjVar = new sj(str, 0, 0);
            this.d = sjVar;
            this.g.a(str).q();
            a(a());
        }
    }

    public synchronized void a(@NonNull a aVar) {
        if (!this.f) {
            this.c.add(aVar);
            b();
        }
    }

    private b b(sj sjVar) {
        switch (this.h) {
            case EMPTY:
                return sjVar == null ? b.WAIT_FOR_RECEIVER_ONLY : b.HAS_FROM_PLAY_SERVICES;
            case RECEIVER:
                return sjVar == null ? b.HAS_FROM_RECEIVER_ONLY : b.HAS_FROM_PLAY_SERVICES;
            default:
                return this.h;
        }
    }

    private b a() {
        int i = AnonymousClass1.f5157a[this.h.ordinal()];
        if (i == 1) {
            return b.RECEIVER;
        }
        if (i != 3) {
            return this.h;
        }
        return b.HAS_FROM_RECEIVER_ONLY;
    }

    private void a(@NonNull b bVar) {
        if (bVar != this.h) {
            this.h = bVar;
            this.g.e(this.h.ordinal()).q();
            b();
        }
    }

    private void b() {
        switch (this.h) {
            case HAS_FROM_PLAY_SERVICES:
                c(this.e);
                return;
            case HAS_FROM_RECEIVER_ONLY:
                c(this.d);
                return;
            default:
                return;
        }
    }

    private synchronized void c(@Nullable sj sjVar) {
        if (sjVar != null) {
            if (!this.c.isEmpty() && !this.f) {
                boolean z = false;
                for (a a2 : this.c) {
                    if (a2.a(sjVar)) {
                        z = true;
                    }
                }
                if (z) {
                    c();
                    this.c.clear();
                }
            }
        }
    }

    private void c() {
        this.f = true;
        this.g.e().f().q();
    }
}
