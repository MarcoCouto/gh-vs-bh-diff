package com.yandex.metrica.impl.ob;

import android.annotation.TargetApi;
import android.bluetooth.le.ScanResult;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.TimeUnit;

@TargetApi(26)
public class jx extends jy {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final xh f4927a;
    @NonNull
    private dn b;
    @NonNull
    private tw c;

    public jx(@NonNull Context context, @NonNull xh xhVar) {
        this(context, xhVar, new dn(), new tw());
    }

    public void a(@Nullable Bundle bundle, @Nullable jw jwVar) {
        if (bundle == null || bundle.isEmpty()) {
            a(jwVar);
            return;
        }
        int i = bundle.getInt("android.bluetooth.le.extra.ERROR_CODE", Integer.MIN_VALUE);
        int i2 = bundle.getInt("android.bluetooth.le.extra.CALLBACK_TYPE", Integer.MIN_VALUE);
        ArrayList parcelableArrayList = bundle.getParcelableArrayList("android.bluetooth.le.extra.LIST_SCAN_RESULT");
        tt ttVar = this.c.a(a()).G;
        if (ttVar != null) {
            dm a2 = this.b.a(a(), ttVar.c);
            if (i > 0) {
                a(a2, i);
            } else if (!cx.a((Collection) parcelableArrayList)) {
                if (parcelableArrayList.size() == 1) {
                    a(a2, (ScanResult) parcelableArrayList.get(0), i2);
                } else {
                    a(a2, (List<ScanResult>) parcelableArrayList);
                }
            }
            b(jwVar);
            return;
        }
        a(jwVar);
    }

    private void a(@NonNull dm dmVar, @NonNull List<ScanResult> list) {
        dmVar.a(list);
    }

    private void a(@NonNull dm dmVar, ScanResult scanResult, int i) {
        dmVar.a(scanResult, i > 0 ? Integer.valueOf(i) : null);
    }

    private void a(dm dmVar, int i) {
        dmVar.a(i);
    }

    private void a(@Nullable jw jwVar) {
        if (jwVar != null) {
            jwVar.a();
        }
    }

    private void b(@Nullable final jw jwVar) {
        if (jwVar != null) {
            this.f4927a.a(new Runnable() {
                public void run() {
                    jwVar.a();
                }
            }, TimeUnit.SECONDS.toMillis(5));
        }
    }

    @VisibleForTesting
    public jx(@NonNull Context context, @NonNull xh xhVar, @NonNull dn dnVar, @NonNull tw twVar) {
        super(context);
        this.f4927a = xhVar;
        this.b = dnVar;
        this.c = twVar;
    }
}
