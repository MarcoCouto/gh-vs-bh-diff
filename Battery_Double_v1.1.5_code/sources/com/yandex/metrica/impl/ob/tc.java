package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class tc {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private xh f5181a;
    @NonNull
    private final mf<te> b;
    @NonNull
    private a c;
    @NonNull
    private mp d;
    @NonNull
    private final b e;
    @NonNull
    private final wg f;
    @NonNull
    private final pi g;
    /* access modifiers changed from: private */
    @Nullable
    public String h;

    public static class a {
        @NonNull

        /* renamed from: a reason: collision with root package name */
        private final ti f5183a;

        public a() {
            this(new ti());
        }

        @VisibleForTesting
        a(@NonNull ti tiVar) {
            this.f5183a = tiVar;
        }

        @NonNull
        public List<th> a(@Nullable byte[] bArr) {
            List<th> list;
            List<th> arrayList = new ArrayList<>();
            if (cx.a(bArr)) {
                return arrayList;
            }
            try {
                list = this.f5183a.a(new String(bArr, "UTF-8"));
            } catch (UnsupportedEncodingException unused) {
                list = arrayList;
            }
            return list;
        }
    }

    static class b {
        b() {
        }

        @Nullable
        public HttpURLConnection a(@NonNull String str, @NonNull String str2) {
            try {
                HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str2).openConnection();
                if (!TextUtils.isEmpty(str)) {
                    httpURLConnection.setRequestProperty(HttpRequest.HEADER_IF_NONE_MATCH, str);
                }
                httpURLConnection.setInstanceFollowRedirects(true);
                httpURLConnection.setRequestMethod(HttpRequest.METHOD_GET);
                httpURLConnection.setConnectTimeout(com.yandex.metrica.impl.ob.pj.a.f5062a);
                httpURLConnection.setReadTimeout(com.yandex.metrica.impl.ob.pj.a.f5062a);
                httpURLConnection.connect();
                return httpURLConnection;
            } catch (Throwable unused) {
                return null;
            }
        }
    }

    public tc(@NonNull Context context, @Nullable String str, @NonNull xh xhVar) {
        this(str, com.yandex.metrica.impl.ob.np.a.a(te.class).a(context), new a(), new b(), xhVar, new mp(), new wg(), new pi(context));
    }

    @VisibleForTesting
    tc(@Nullable String str, @NonNull mf mfVar, @NonNull a aVar, @NonNull b bVar, @NonNull xh xhVar, @NonNull mp mpVar, @NonNull wg wgVar, @NonNull pi piVar) {
        this.h = str;
        this.b = mfVar;
        this.c = aVar;
        this.e = bVar;
        this.f5181a = xhVar;
        this.d = mpVar;
        this.f = wgVar;
        this.g = piVar;
    }

    public void a(@NonNull final tb tbVar) {
        this.f5181a.a((Runnable) new Runnable() {
            public void run() {
                tc.this.a(tbVar, tc.this.h);
            }
        });
    }

    /* access modifiers changed from: private */
    public void a(@NonNull tb tbVar, String str) {
        te teVar = (te) this.b.a();
        te teVar2 = null;
        if (this.g.a() && str != null) {
            try {
                HttpURLConnection a2 = this.e.a(teVar.b, str);
                if (a2 != null) {
                    teVar2 = a(a2, teVar);
                }
            } catch (Throwable unused) {
            }
        }
        if (teVar2 != null) {
            tbVar.a(teVar2);
        } else {
            tbVar.a();
        }
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    public te a(@NonNull HttpURLConnection httpURLConnection, @NonNull te teVar) throws IOException {
        int responseCode = httpURLConnection.getResponseCode();
        if (responseCode == 200) {
            String a2 = cu.a(httpURLConnection.getHeaderField(HttpRequest.HEADER_ETAG));
            try {
                te teVar2 = new te(this.c.a(this.d.a(am.b(httpURLConnection.getInputStream()), "af9202nao18gswqp")), a2, this.f.a(), true, false);
                return teVar2;
            } catch (IOException unused) {
            }
        } else if (responseCode == 304) {
            te teVar3 = new te(teVar.f5187a, teVar.b, this.f.a(), true, false);
            return teVar3;
        }
        return null;
    }

    public void a(@Nullable uk ukVar) {
        if (ukVar != null) {
            this.h = ukVar.h;
        }
    }

    public boolean b(@NonNull uk ukVar) {
        boolean z = true;
        if (this.h != null) {
            return !this.h.equals(ukVar.h);
        }
        if (ukVar.h == null) {
            z = false;
        }
        return z;
    }
}
