package com.yandex.metrica.impl.ob;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.support.annotation.NonNull;
import com.facebook.internal.NativeProtocol;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class ld {
    @SuppressLint({"StaticFieldLeak"})

    /* renamed from: a reason: collision with root package name */
    private static volatile ld f4963a;
    private final Map<String, lc> b;
    private final Map<String, lf> c;
    private final Map<String, kx> d;
    @NonNull
    private final la e;
    private final Context f;
    private lc g;
    private kx h;
    private lf i;
    private lf j;
    private lf k;
    private lh l;
    private lg m;
    private li n;

    public static ld a(Context context) {
        if (f4963a == null) {
            synchronized (ld.class) {
                if (f4963a == null) {
                    f4963a = new ld(context.getApplicationContext());
                }
            }
        }
        return f4963a;
    }

    public ld(Context context) {
        this(context, lq.a());
    }

    public ld(Context context, @NonNull la laVar) {
        this.b = new HashMap();
        this.c = new HashMap();
        this.d = new HashMap();
        this.f = context;
        this.e = laVar;
    }

    public synchronized lc a(ek ekVar) {
        lc lcVar;
        String d2 = d(ekVar);
        lcVar = (lc) this.b.get(d2);
        if (lcVar == null) {
            lcVar = a(d2, this.e.a());
            this.b.put(d2, lcVar);
        }
        return lcVar;
    }

    public synchronized lc a() {
        if (this.g == null) {
            this.g = a("metrica_data.db", this.e.b());
        }
        return this.g;
    }

    public synchronized lf b(ek ekVar) {
        lf lfVar;
        String ekVar2 = ekVar.toString();
        lfVar = (lf) this.c.get(ekVar2);
        if (lfVar == null) {
            lfVar = new lf(a(ekVar), "preferences");
            this.c.put(ekVar2, lfVar);
        }
        return lfVar;
    }

    @NonNull
    public synchronized kx c(@NonNull ek ekVar) {
        kx kxVar;
        String ekVar2 = ekVar.toString();
        kxVar = (kx) this.d.get(ekVar2);
        if (kxVar == null) {
            kxVar = new kx(new lp(a(ekVar)), "binary_data");
            this.d.put(ekVar2, kxVar);
        }
        return kxVar;
    }

    public synchronized kx b() {
        if (this.h == null) {
            this.h = new kx(new lp(a()), "binary_data");
        }
        return this.h;
    }

    public synchronized lf c() {
        if (this.i == null) {
            this.i = new lf(a(), "preferences");
        }
        return this.i;
    }

    public synchronized li d() {
        if (this.n == null) {
            this.n = new li(a(), NativeProtocol.RESULT_ARGS_PERMISSIONS);
        }
        return this.n;
    }

    public synchronized lf e() {
        if (this.j == null) {
            this.j = new lf(a(), "startup");
        }
        return this.j;
    }

    public synchronized lf f() {
        if (this.k == null) {
            this.k = new lf("preferences", (lm) new lo(this.f, a("metrica_client_data.db")));
        }
        return this.k;
    }

    public synchronized lh g() {
        if (this.l == null) {
            this.l = new lh(this.f, a());
        }
        return this.l;
    }

    public synchronized lg h() {
        if (this.m == null) {
            this.m = new lg(this.f, a());
        }
        return this.m;
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public lc a(String str, lj ljVar) {
        return new lc(this.f, a(str), ljVar);
    }

    private String a(String str) {
        return cx.a(21) ? b(str) : str;
    }

    @TargetApi(21)
    private String b(String str) {
        try {
            File noBackupFilesDir = this.f.getNoBackupFilesDir();
            File file = new File(noBackupFilesDir, str);
            if (!file.exists()) {
                if (a(noBackupFilesDir, str)) {
                    StringBuilder sb = new StringBuilder();
                    sb.append(str);
                    sb.append("-journal");
                    a(noBackupFilesDir, sb.toString());
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append(str);
                    sb2.append("-shm");
                    a(noBackupFilesDir, sb2.toString());
                    StringBuilder sb3 = new StringBuilder();
                    sb3.append(str);
                    sb3.append("-wal");
                    a(noBackupFilesDir, sb3.toString());
                }
            }
            return file.getAbsolutePath();
        } catch (Throwable unused) {
            return str;
        }
    }

    private boolean a(@NonNull File file, @NonNull String str) {
        File databasePath = this.f.getDatabasePath(str);
        if (databasePath == null || !databasePath.exists()) {
            return false;
        }
        return databasePath.renameTo(new File(file, str));
    }

    private static String d(ek ekVar) {
        StringBuilder sb = new StringBuilder();
        sb.append("db_metrica_");
        sb.append(ekVar);
        return sb.toString();
    }
}
