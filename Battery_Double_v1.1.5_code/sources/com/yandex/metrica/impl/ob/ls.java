package com.yandex.metrica.impl.ob;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import com.tapjoy.TapjoyConstants;
import com.yandex.metrica.impl.ob.lq.a.C0109a;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Map.Entry;
import org.json.JSONException;
import org.json.JSONObject;

public class ls {

    public static class a extends lr {
        public void a(SQLiteDatabase sQLiteDatabase) throws SQLException {
            sQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS preferences (key TEXT PRIMARY KEY,value TEXT,type INTEGER)");
        }
    }

    public static class aa extends lr {
        public void a(@NonNull SQLiteDatabase sQLiteDatabase) throws SQLException, JSONException {
            sQLiteDatabase.delete("reports", "session_id = ?", new String[]{String.valueOf(-2)});
            sQLiteDatabase.delete("sessions", "id = ?", new String[]{String.valueOf(-2)});
        }
    }

    public static class ab extends lr {
        public void a(@NonNull SQLiteDatabase sQLiteDatabase) throws SQLException, JSONException {
            sQLiteDatabase.execSQL(String.format(Locale.US, "ALTER TABLE %s ADD COLUMN %s INTEGER DEFAULT %d", new Object[]{"reports", "global_number", Integer.valueOf(0)}));
            sQLiteDatabase.execSQL(String.format(Locale.US, "ALTER TABLE %s ADD COLUMN %s INTEGER DEFAULT %d", new Object[]{"reports", "number_of_type", Integer.valueOf(0)}));
        }
    }

    public static class b extends lr {
        public void a(SQLiteDatabase sQLiteDatabase) throws SQLException {
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS preferences");
        }
    }

    public static class c extends lr {
        public void a(SQLiteDatabase sQLiteDatabase) throws SQLException {
            sQLiteDatabase.execSQL(com.yandex.metrica.impl.ob.lq.f.b);
            sQLiteDatabase.execSQL(com.yandex.metrica.impl.ob.lq.g.b);
            sQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS preferences (key TEXT PRIMARY KEY,value TEXT,type INTEGER)");
            sQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS binary_data (data_key TEXT PRIMARY KEY,value BLOB)");
        }
    }

    public static class d extends lr {
        public void a(SQLiteDatabase sQLiteDatabase) throws SQLException {
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS reports");
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS sessions");
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS preferences");
        }
    }

    public static class e extends lr {
        public void a(SQLiteDatabase sQLiteDatabase) throws SQLException {
            sQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS preferences (key TEXT PRIMARY KEY,value TEXT,type INTEGER)");
            sQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS startup (key TEXT PRIMARY KEY,value TEXT,type INTEGER)");
            sQLiteDatabase.execSQL(com.yandex.metrica.impl.ob.lq.a.b.f4976a);
            sQLiteDatabase.execSQL(C0109a.f4975a);
            sQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS binary_data (data_key TEXT PRIMARY KEY,value BLOB)");
        }
    }

    public static class f extends lr {
        public void a(SQLiteDatabase sQLiteDatabase) throws SQLException {
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS device_id_info");
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS api_level_info");
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS preferences");
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS startup");
            sQLiteDatabase.execSQL(com.yandex.metrica.impl.ob.lq.a.b.b);
            sQLiteDatabase.execSQL(C0109a.b);
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS permissions");
        }
    }

    public static class g extends lr {
        public void a(SQLiteDatabase sQLiteDatabase) throws SQLException {
            sQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS api_level_info (API_LEVEL INT )");
            sQLiteDatabase.insert("api_level_info", "API_LEVEL", com.yandex.metrica.impl.ob.lq.e.a());
        }
    }

    public static class h extends lr {
        public void a(SQLiteDatabase sQLiteDatabase) throws SQLException {
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS api_level_info");
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS device_id_info");
        }
    }

    public static class i extends lr {
        public void a(SQLiteDatabase sQLiteDatabase) throws SQLException {
            sQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS preferences (key TEXT PRIMARY KEY,value TEXT,type INTEGER)");
            sQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS startup (key TEXT PRIMARY KEY,value TEXT,type INTEGER)");
        }
    }

    public static class j extends lr {
        public void a(SQLiteDatabase sQLiteDatabase) throws SQLException {
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS GeoLocationInfo");
        }
    }

    public static class k extends lr {
        public void a(SQLiteDatabase sQLiteDatabase) {
            sQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS permissions (name TEXT PRIMARY KEY,granted INTEGER)");
        }
    }

    public static class l extends lr {
        public void a(SQLiteDatabase sQLiteDatabase) throws SQLException, JSONException {
            sQLiteDatabase.execSQL(com.yandex.metrica.impl.ob.lq.a.b.f4976a);
        }
    }

    public static class m extends lr {
        public void a(SQLiteDatabase sQLiteDatabase) throws SQLException, JSONException {
            sQLiteDatabase.execSQL(C0109a.f4975a);
        }
    }

    public static class n extends lr {
        public void a(SQLiteDatabase sQLiteDatabase) throws SQLException, JSONException {
            sQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS binary_data (data_key TEXT PRIMARY KEY,value BLOB)");
        }
    }

    public static class o extends lr {
        public void a(SQLiteDatabase sQLiteDatabase) throws SQLException, JSONException {
            Cursor cursor;
            StringBuilder sb = new StringBuilder();
            sb.append("CREATE TABLE IF NOT EXISTS ");
            sb.append("sessions_BACKUP");
            sb.append(" (");
            sb.append("id");
            sb.append(" INTEGER");
            sb.append(",");
            sb.append("start_time");
            sb.append(" INTEGER");
            sb.append(",");
            sb.append(TapjoyConstants.TJC_CONNECTION_TYPE);
            sb.append(" INTEGER");
            sb.append(",");
            sb.append("network_type");
            sb.append(" TEXT");
            sb.append(",");
            sb.append(TapjoyConstants.TJC_DEVICE_COUNTRY_CODE);
            sb.append(" INTEGER");
            sb.append(",");
            sb.append("operator_id");
            sb.append(" INTEGER");
            sb.append(",");
            sb.append("lac");
            sb.append(" INTEGER");
            sb.append(",");
            sb.append("report_request_parameters");
            sb.append(" TEXT");
            sb.append(" );");
            sQLiteDatabase.execSQL(sb.toString());
            StringBuilder sb2 = new StringBuilder();
            sb2.append("id");
            sb2.append(",");
            sb2.append("start_time");
            sb2.append(",");
            sb2.append(TapjoyConstants.TJC_CONNECTION_TYPE);
            sb2.append(",");
            sb2.append("network_type");
            sb2.append(",");
            sb2.append(TapjoyConstants.TJC_DEVICE_COUNTRY_CODE);
            sb2.append(",");
            sb2.append("operator_id");
            sb2.append(",");
            sb2.append("lac");
            sb2.append(",");
            sb2.append("report_request_parameters");
            StringBuilder sb3 = new StringBuilder();
            sb3.append("INSERT INTO ");
            sb3.append("sessions_BACKUP");
            sb3.append(" SELECT ");
            sb3.append(sb2);
            sb3.append(" FROM ");
            sb3.append("sessions");
            sb3.append(";");
            sQLiteDatabase.execSQL(sb3.toString());
            sQLiteDatabase.execSQL("DROP TABLE sessions;");
            sQLiteDatabase.execSQL(com.yandex.metrica.impl.ob.lq.g.b);
            try {
                cursor = sQLiteDatabase.rawQuery("SELECT * FROM sessions_BACKUP", null);
                while (cursor.moveToNext()) {
                    try {
                        ContentValues contentValues = new ContentValues();
                        DatabaseUtils.cursorRowToContentValues(cursor, contentValues);
                        ArrayList<String> arrayList = new ArrayList<>();
                        arrayList.add("id");
                        arrayList.add("start_time");
                        arrayList.add("report_request_parameters");
                        ContentValues contentValues2 = new ContentValues(contentValues);
                        for (Entry entry : contentValues.valueSet()) {
                            if (!arrayList.contains(entry.getKey())) {
                                contentValues2.remove((String) entry.getKey());
                            }
                        }
                        for (String remove : arrayList) {
                            contentValues.remove(remove);
                        }
                        JSONObject jSONObject = new JSONObject();
                        jSONObject.put("conn_type", contentValues.getAsInteger(TapjoyConstants.TJC_CONNECTION_TYPE));
                        jSONObject.putOpt("net_type", contentValues.get("network_type"));
                        jSONObject.putOpt("operator_id", contentValues.get("operator_id"));
                        jSONObject.putOpt("lac", contentValues.get("lac"));
                        jSONObject.putOpt(TapjoyConstants.TJC_DEVICE_COUNTRY_CODE, contentValues.get(TapjoyConstants.TJC_DEVICE_COUNTRY_CODE));
                        contentValues2.put("network_info", jSONObject.toString());
                        sQLiteDatabase.insertOrThrow("sessions", null, contentValues2);
                    } catch (Throwable th) {
                        th = th;
                        cx.a(cursor);
                        throw th;
                    }
                }
                cx.a(cursor);
                sQLiteDatabase.execSQL("DROP TABLE sessions_BACKUP;");
                StringBuilder sb4 = new StringBuilder();
                sb4.append("ALTER TABLE ");
                sb4.append("reports");
                sb4.append(" ADD COLUMN ");
                sb4.append("wifi_network_info");
                sb4.append(" TEXT DEFAULT ''");
                sQLiteDatabase.execSQL(sb4.toString());
                StringBuilder sb5 = new StringBuilder();
                sb5.append("ALTER TABLE ");
                sb5.append("reports");
                sb5.append(" ADD COLUMN ");
                sb5.append("cell_info");
                sb5.append(" TEXT DEFAULT ''");
                sQLiteDatabase.execSQL(sb5.toString());
                StringBuilder sb6 = new StringBuilder();
                sb6.append("ALTER TABLE ");
                sb6.append("reports");
                sb6.append(" ADD COLUMN ");
                sb6.append("location_info");
                sb6.append(" TEXT DEFAULT ''");
                sQLiteDatabase.execSQL(sb6.toString());
            } catch (Throwable th2) {
                th = th2;
                cursor = null;
                cx.a(cursor);
                throw th;
            }
        }
    }

    public static class p extends lr {
        public void a(SQLiteDatabase sQLiteDatabase) throws SQLException {
            StringBuilder sb = new StringBuilder();
            sb.append("ALTER TABLE ");
            sb.append("reports");
            sb.append(" ADD COLUMN ");
            sb.append("environment");
            sb.append(" TEXT ");
            sQLiteDatabase.execSQL(sb.toString());
            StringBuilder sb2 = new StringBuilder();
            sb2.append("ALTER TABLE ");
            sb2.append("reports");
            sb2.append(" ADD COLUMN ");
            sb2.append("user_info");
            sb2.append(" TEXT ");
            sQLiteDatabase.execSQL(sb2.toString());
            StringBuilder sb3 = new StringBuilder();
            sb3.append("ALTER TABLE ");
            sb3.append("reports");
            sb3.append(" ADD COLUMN ");
            sb3.append("session_type");
            sb3.append(" INTEGER DEFAULT ");
            sb3.append(jh.FOREGROUND.a());
            sQLiteDatabase.execSQL(sb3.toString());
            StringBuilder sb4 = new StringBuilder();
            sb4.append("UPDATE ");
            sb4.append("reports");
            sb4.append(" SET ");
            sb4.append("session_type");
            sb4.append(" = ");
            sb4.append(jh.BACKGROUND.a());
            sb4.append(" WHERE ");
            sb4.append("session_id");
            sb4.append(" = ");
            sb4.append(-2);
            sQLiteDatabase.execSQL(sb4.toString());
            StringBuilder sb5 = new StringBuilder();
            sb5.append("ALTER TABLE ");
            sb5.append("sessions");
            sb5.append(" ADD COLUMN ");
            sb5.append("server_time_offset");
            sb5.append(" INTEGER ");
            sQLiteDatabase.execSQL(sb5.toString());
            StringBuilder sb6 = new StringBuilder();
            sb6.append("ALTER TABLE ");
            sb6.append("sessions");
            sb6.append(" ADD COLUMN ");
            sb6.append("type");
            sb6.append(" INTEGER DEFAULT ");
            sb6.append(jh.FOREGROUND.a());
            sQLiteDatabase.execSQL(sb6.toString());
            StringBuilder sb7 = new StringBuilder();
            sb7.append("UPDATE ");
            sb7.append("sessions");
            sb7.append(" SET ");
            sb7.append("type");
            sb7.append(" = ");
            sb7.append(jh.BACKGROUND.a());
            sb7.append(" WHERE ");
            sb7.append("id");
            sb7.append(" = ");
            sb7.append(-2);
            sQLiteDatabase.execSQL(sb7.toString());
        }
    }

    public static class q extends lr {

        /* renamed from: a reason: collision with root package name */
        private static final String f4981a;

        static {
            StringBuilder sb = new StringBuilder();
            sb.append("CREATE TABLE IF NOT EXISTS reports (id INTEGER PRIMARY KEY,name TEXT,value TEXT,number INTEGER,type INTEGER,time INTEGER,session_id TEXT,wifi_network_info TEXT DEFAULT '',cell_info TEXT DEFAULT '',location_info TEXT DEFAULT '',error_environment TEXT,user_info TEXT,session_type INTEGER DEFAULT ");
            sb.append(jh.FOREGROUND.a());
            sb.append(",");
            sb.append("app_environment");
            sb.append(" TEXT DEFAULT '");
            sb.append("{}");
            sb.append("',");
            sb.append("app_environment_revision");
            sb.append(" INTEGER DEFAULT ");
            sb.append(0);
            sb.append(" )");
            f4981a = sb.toString();
        }

        public void a(SQLiteDatabase sQLiteDatabase) throws SQLException {
            Cursor cursor;
            StringBuilder sb = new StringBuilder();
            sb.append("ALTER TABLE ");
            sb.append("reports");
            sb.append(" ADD COLUMN ");
            sb.append("app_environment");
            sb.append(" TEXT DEFAULT '{}'");
            sQLiteDatabase.execSQL(sb.toString());
            StringBuilder sb2 = new StringBuilder();
            sb2.append("ALTER TABLE ");
            sb2.append("reports");
            sb2.append(" ADD COLUMN ");
            sb2.append("app_environment_revision");
            sb2.append(" INTEGER DEFAULT 0");
            sQLiteDatabase.execSQL(sb2.toString());
            sQLiteDatabase.execSQL("ALTER TABLE reports RENAME TO reports_backup");
            sQLiteDatabase.execSQL(f4981a);
            try {
                cursor = sQLiteDatabase.rawQuery("SELECT * FROM reports_backup", null);
                while (cursor.moveToNext()) {
                    try {
                        ContentValues contentValues = new ContentValues();
                        DatabaseUtils.cursorRowToContentValues(cursor, contentValues);
                        String asString = contentValues.getAsString("environment");
                        contentValues.remove("environment");
                        contentValues.put("error_environment", asString);
                        sQLiteDatabase.insert("reports", null, contentValues);
                    } catch (Throwable th) {
                        th = th;
                        cx.a(cursor);
                        throw th;
                    }
                }
                cx.a(cursor);
                sQLiteDatabase.execSQL("DROP TABLE reports_backup");
            } catch (Throwable th2) {
                th = th2;
                cursor = null;
                cx.a(cursor);
                throw th;
            }
        }
    }

    public static class r extends lr {
        public void a(SQLiteDatabase sQLiteDatabase) throws SQLException {
            StringBuilder sb = new StringBuilder();
            sb.append("ALTER TABLE ");
            sb.append("reports");
            sb.append(" ADD COLUMN ");
            sb.append("truncated");
            sb.append(" INTEGER DEFAULT 0");
            sQLiteDatabase.execSQL(sb.toString());
        }
    }

    public static class s extends lr {
        public void a(SQLiteDatabase sQLiteDatabase) throws SQLException, JSONException {
            StringBuilder sb = new StringBuilder();
            sb.append("ALTER TABLE ");
            sb.append("reports");
            sb.append(" ADD COLUMN ");
            sb.append(TapjoyConstants.TJC_CONNECTION_TYPE);
            sb.append(" INTEGER DEFAULT 2");
            sQLiteDatabase.execSQL(sb.toString());
            StringBuilder sb2 = new StringBuilder();
            sb2.append("ALTER TABLE ");
            sb2.append("reports");
            sb2.append(" ADD COLUMN ");
            sb2.append("cellular_connection_type");
            sb2.append(" TEXT ");
            sQLiteDatabase.execSQL(sb2.toString());
        }
    }

    public static class t extends lr {
        public void a(SQLiteDatabase sQLiteDatabase) throws SQLException {
            sQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS preferences (key TEXT PRIMARY KEY,value TEXT,type INTEGER)");
            StringBuilder sb = new StringBuilder();
            sb.append("ALTER TABLE ");
            sb.append("reports");
            sb.append(" ADD COLUMN ");
            sb.append("custom_type");
            sb.append(" INTEGER DEFAULT 0");
            sQLiteDatabase.execSQL(sb.toString());
        }
    }

    public static class u extends lr {
        public void a(SQLiteDatabase sQLiteDatabase) throws SQLException {
            sQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS preferences (key TEXT PRIMARY KEY,value TEXT,type INTEGER)");
            StringBuilder sb = new StringBuilder();
            sb.append("ALTER TABLE ");
            sb.append("reports");
            sb.append(" ADD COLUMN ");
            sb.append("wifi_access_point");
            sb.append(" TEXT ");
            sQLiteDatabase.execSQL(sb.toString());
        }
    }

    public static class v extends lr {
        public void a(SQLiteDatabase sQLiteDatabase) throws SQLException {
            StringBuilder sb = new StringBuilder();
            sb.append("ALTER TABLE ");
            sb.append("sessions");
            sb.append(" ADD COLUMN ");
            sb.append("wifi_network_info");
            sb.append(" TEXT DEFAULT ''");
            sQLiteDatabase.execSQL(sb.toString());
        }
    }

    public static class w extends lr {
        public void a(SQLiteDatabase sQLiteDatabase) throws SQLException, JSONException {
            StringBuilder sb = new StringBuilder();
            sb.append("ALTER TABLE ");
            sb.append("sessions");
            sb.append(" ADD COLUMN ");
            sb.append("obtained_before_first_sync");
            sb.append(" INTEGER DEFAULT 0");
            sQLiteDatabase.execSQL(sb.toString());
        }
    }

    public static class x extends lr {
        public void a(SQLiteDatabase sQLiteDatabase) throws SQLException, JSONException {
            sQLiteDatabase.execSQL(String.format(Locale.US, "ALTER TABLE %s ADD COLUMN %s INTEGER DEFAULT %d", new Object[]{"reports", "encrypting_mode", Integer.valueOf(wz.NONE.a())}));
            sQLiteDatabase.execSQL(String.format(Locale.US, "UPDATE %s SET %s = %d where %s=%d", new Object[]{"reports", "encrypting_mode", Integer.valueOf(wz.EXTERNALLY_ENCRYPTED_EVENT_CRYPTER.a()), "type", Integer.valueOf(com.yandex.metrica.impl.ob.af.a.EVENT_TYPE_IDENTITY.a())}));
            sQLiteDatabase.execSQL("ALTER TABLE reports ADD COLUMN profile_id TEXT ");
        }
    }

    public static class y extends lr {
        public void a(@NonNull SQLiteDatabase sQLiteDatabase) throws SQLException, JSONException {
            sQLiteDatabase.execSQL(String.format(Locale.US, "ALTER TABLE %s ADD COLUMN %s INTEGER DEFAULT %d", new Object[]{"reports", "first_occurrence_status", Integer.valueOf(aj.UNKNOWN.d)}));
            sQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS binary_data (data_key TEXT PRIMARY KEY,value BLOB)");
        }
    }

    public static class z extends lr {
        public void a(SQLiteDatabase sQLiteDatabase) throws SQLException {
            StringBuilder sb = new StringBuilder();
            sb.append("ALTER TABLE ");
            sb.append("sessions");
            sb.append(" ADD COLUMN ");
            sb.append("report_request_parameters");
            sb.append(" TEXT DEFAULT ''");
            sQLiteDatabase.execSQL(sb.toString());
        }
    }
}
