package com.yandex.metrica.impl.ob;

import android.os.Handler;
import java.lang.ref.WeakReference;

class au implements Runnable {

    /* renamed from: a reason: collision with root package name */
    private final WeakReference<Handler> f4649a;
    private final WeakReference<n> b;

    au(Handler handler, n nVar) {
        this.f4649a = new WeakReference<>(handler);
        this.b = new WeakReference<>(nVar);
    }

    public void run() {
        Handler handler = (Handler) this.f4649a.get();
        n nVar = (n) this.b.get();
        if (handler != null && nVar != null && nVar.c()) {
            at.a(handler, nVar, this);
        }
    }
}
