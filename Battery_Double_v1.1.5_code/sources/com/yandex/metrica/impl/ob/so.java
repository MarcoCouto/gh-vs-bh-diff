package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import com.yandex.metrica.impl.ob.sn.a;
import com.yandex.metrica.impl.ob.sn.b;
import com.yandex.metrica.impl.ob.sq;

public abstract class so<T extends sq, A extends a<eg.a, A>, L extends b<T, A>> extends sp<T, eg.a, A, L> {
    public so(@NonNull L l, @NonNull uk ukVar, @NonNull A a2) {
        super(l, ukVar, a2);
    }

    public synchronized void a(@NonNull eg.a aVar) {
        super.a(aVar);
    }
}
