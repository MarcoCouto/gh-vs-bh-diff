package com.yandex.metrica.impl.ob;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

public class ll implements lk {

    /* renamed from: a reason: collision with root package name */
    private final String f4969a;
    private final HashMap<String, List<String>> b;

    public ll(@NonNull String str, @NonNull HashMap<String, List<String>> hashMap) {
        this.f4969a = str;
        this.b = hashMap;
    }

    public boolean a(SQLiteDatabase sQLiteDatabase) {
        Cursor cursor;
        boolean z = true;
        try {
            for (Entry entry : this.b.entrySet()) {
                try {
                    cursor = sQLiteDatabase.query((String) entry.getKey(), null, null, null, null, null, null);
                    if (cursor == null) {
                        cx.a(cursor);
                        return false;
                    }
                    z &= a(cursor, (String) entry.getKey(), (List) entry.getValue());
                    cx.a(cursor);
                } catch (Throwable th) {
                    th = th;
                    cursor = null;
                    cx.a(cursor);
                    throw th;
                }
            }
            return z;
        } catch (Throwable unused) {
            return false;
        }
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public boolean a(@NonNull Cursor cursor, @NonNull String str, @NonNull List<String> list) {
        List asList = Arrays.asList(cursor.getColumnNames());
        Collections.sort(asList);
        return list.equals(asList);
    }
}
