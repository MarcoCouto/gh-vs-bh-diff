package com.yandex.metrica.impl.ob;

import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.yandex.metrica.impl.ob.rh.c.e;
import com.yandex.metrica.impl.ob.rh.c.e.a;

public class vz extends vs {

    /* renamed from: a reason: collision with root package name */
    private static final int[] f5283a = {3, 6, 4};
    private static final vz b = new vz();

    public String f() {
        return "AppMetrica";
    }

    public vz(@Nullable String str) {
        super(str);
    }

    public vz() {
        this("");
    }

    public static vz h() {
        return b;
    }

    public void a(w wVar, String str) {
        if (af.b(wVar.g())) {
            StringBuilder sb = new StringBuilder(str);
            sb.append(": ");
            sb.append(wVar.d());
            if (af.c(wVar.g()) && !TextUtils.isEmpty(wVar.e())) {
                sb.append(" with value ");
                sb.append(wVar.e());
            }
            a(sb.toString());
        }
    }

    public void a(a aVar, String str) {
        if (a(aVar)) {
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append(": ");
            sb.append(b(aVar));
            a(sb.toString());
        }
    }

    private boolean a(a aVar) {
        for (int i : f5283a) {
            if (aVar.d == i) {
                return true;
            }
        }
        return false;
    }

    private String b(a aVar) {
        if (aVar.d == 3 && TextUtils.isEmpty(aVar.e)) {
            return "Native crash of app";
        }
        if (aVar.d != 4) {
            return aVar.e;
        }
        StringBuilder sb = new StringBuilder(aVar.e);
        if (aVar.f != null) {
            String str = new String(aVar.f);
            if (!TextUtils.isEmpty(str)) {
                sb.append(" with value ");
                sb.append(str);
            }
        }
        return sb.toString();
    }

    public void a(e eVar, String str) {
        for (a a2 : eVar.d) {
            a(a2, str);
        }
    }
}
