package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import com.yandex.metrica.impl.ob.rr.a.b.C0123a.C0124a;
import com.yandex.metrica.impl.ob.tt.a.C0130a;

public class ni implements mv<C0130a, C0124a> {
    @NonNull
    /* renamed from: a */
    public C0124a b(@NonNull C0130a aVar) {
        C0124a aVar2 = new C0124a();
        aVar2.b = aVar.f5211a;
        if (aVar.b != null) {
            aVar2.c = aVar.b;
        }
        if (aVar.c != null) {
            aVar2.d = aVar.c;
        }
        return aVar2;
    }

    @NonNull
    public C0130a a(@NonNull C0124a aVar) {
        int i = aVar.b;
        byte[] bArr = null;
        byte[] bArr2 = cx.a(aVar.c) ? null : aVar.c;
        if (!cx.a(aVar.d)) {
            bArr = aVar.d;
        }
        return new C0130a(i, bArr2, bArr);
    }
}
