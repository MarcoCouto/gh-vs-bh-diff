package com.yandex.metrica.impl.ob;

public enum wz {
    NONE(0),
    EXTERNALLY_ENCRYPTED_EVENT_CRYPTER(1),
    AES_VALUE_ENCRYPTION(2);
    
    private final int d;

    private wz(int i) {
        this.d = i;
    }

    public int a() {
        return this.d;
    }

    public static wz a(int i) {
        wz[] values;
        for (wz wzVar : values()) {
            if (wzVar.a() == i) {
                return wzVar;
            }
        }
        return null;
    }
}
