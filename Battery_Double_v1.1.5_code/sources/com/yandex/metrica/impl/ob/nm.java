package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import com.yandex.metrica.impl.ob.rr.a.b.C0123a.C0125b;
import com.yandex.metrica.impl.ob.tt.a.b;

public class nm implements mv<b, C0125b> {
    @NonNull
    /* renamed from: a */
    public C0125b b(@NonNull b bVar) {
        C0125b bVar2 = new C0125b();
        bVar2.b = bVar.f5212a.toString();
        if (bVar.b != null) {
            bVar2.c = bVar.b;
        }
        if (bVar.c != null) {
            bVar2.d = bVar.c;
        }
        return bVar2;
    }

    @NonNull
    public b a(@NonNull C0125b bVar) {
        String str = bVar.b;
        byte[] bArr = null;
        byte[] bArr2 = cx.a(bVar.c) ? null : bVar.c;
        if (!cx.a(bVar.d)) {
            bArr = bVar.d;
        }
        return new b(str, bArr2, bArr);
    }
}
