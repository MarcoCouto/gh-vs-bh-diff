package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;

public abstract class gt {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final ei f4862a;

    public abstract boolean a(@NonNull w wVar, @NonNull fs fsVar);

    gt(@NonNull ei eiVar) {
        this.f4862a = eiVar;
    }

    /* access modifiers changed from: protected */
    @NonNull
    public ei a() {
        return this.f4862a;
    }
}
