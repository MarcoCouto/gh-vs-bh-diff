package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import java.io.IOException;

public class mk<T> implements ml<T> {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final ml<T> f4993a;
    @NonNull
    private final wr b;

    public mk(@NonNull ml<T> mlVar, @NonNull wr wrVar) {
        this.f4993a = mlVar;
        this.b = wrVar;
    }

    @NonNull
    public byte[] a(@NonNull T t) {
        try {
            return this.b.a(this.f4993a.a(t));
        } catch (Throwable unused) {
            return new byte[0];
        }
    }

    @NonNull
    public T b(@NonNull byte[] bArr) throws IOException {
        try {
            return this.f4993a.b(this.b.b(bArr));
        } catch (Throwable th) {
            throw new IOException(th);
        }
    }

    @NonNull
    public T c() {
        return this.f4993a.c();
    }
}
