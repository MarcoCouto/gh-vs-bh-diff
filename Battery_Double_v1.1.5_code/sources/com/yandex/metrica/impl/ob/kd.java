package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.util.SparseArray;
import com.ironsource.sdk.constants.Constants.ParametersKeys;
import com.yandex.metrica.impl.ob.af.a;

public class kd {
    private static SparseArray<kd> c = new SparseArray<>();

    /* renamed from: a reason: collision with root package name */
    public final String f4934a;
    public final String b;

    static {
        c.put(a.EVENT_TYPE_EXCEPTION_UNHANDLED.a(), new kd("jvm", "binder"));
        c.put(a.EVENT_TYPE_EXCEPTION_UNHANDLED_PROTOBUF.a(), new kd("jvm", "binder"));
        c.put(a.EVENT_TYPE_EXCEPTION_UNHANDLED_FROM_INTENT.a(), new kd("jvm", "intent"));
        c.put(a.EVENT_TYPE_EXCEPTION_UNHANDLED_FROM_FILE.a(), new kd("jvm", ParametersKeys.FILE));
        c.put(a.EVENT_TYPE_PREV_SESSION_NATIVE_CRASH.a(), new kd("jni_native", ParametersKeys.FILE));
        c.put(a.EVENT_TYPE_CURRENT_SESSION_NATIVE_CRASH.a(), new kd("jni_native", ParametersKeys.FILE));
    }

    private kd(@NonNull String str, @NonNull String str2) {
        this.f4934a = str;
        this.b = str2;
    }

    public static kd a(int i) {
        return (kd) c.get(i);
    }
}
