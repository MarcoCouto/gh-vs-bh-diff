package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import com.yandex.metrica.CounterConfiguration;
import com.yandex.metrica.impl.ob.eg.a;

public class fz implements fq, ft<en> {
    @NonNull
    public fp a(@NonNull Context context, @NonNull fu fuVar, @NonNull fn fnVar, @NonNull eg egVar) {
        return new fy(context, fuVar.a(new ek(fnVar.b(), fnVar.a()), egVar, new fd(this)));
    }

    @NonNull
    /* renamed from: a */
    public en d(@NonNull Context context, @NonNull ek ekVar, @NonNull a aVar, @NonNull bl blVar, @NonNull ul ulVar) {
        fi fiVar = new fi(context, ekVar, blVar, aVar, al.a().h(), ulVar.e(), new uo(ulVar));
        return fiVar;
    }

    @NonNull
    /* renamed from: b */
    public ge c(@NonNull Context context, @NonNull ek ekVar, @NonNull a aVar, @NonNull bl blVar, @NonNull ul ulVar) {
        ge geVar = new ge(context, ekVar, blVar, aVar, ulVar.e(), new uo(ulVar), CounterConfiguration.a.MANUAL);
        return geVar;
    }
}
