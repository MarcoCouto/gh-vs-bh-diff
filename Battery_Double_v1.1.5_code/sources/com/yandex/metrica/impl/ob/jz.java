package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public class jz extends jy {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final jv f4930a;

    public jz(@NonNull Context context, @NonNull jv jvVar) {
        super(context);
        this.f4930a = jvVar;
    }

    public void a(@Nullable Bundle bundle, @Nullable jw jwVar) {
        this.f4930a.a(jwVar);
    }
}
