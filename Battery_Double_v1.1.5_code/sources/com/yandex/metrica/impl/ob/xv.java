package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public class xv {

    /* renamed from: a reason: collision with root package name */
    private final String f5308a;
    private final int b;
    @NonNull
    private final vz c;

    public xv(int i, @NonNull String str, @NonNull vz vzVar) {
        this.b = i;
        this.f5308a = str;
        this.c = vzVar;
    }

    public boolean a(@NonNull vx vxVar, @NonNull String str, @Nullable String str2) {
        int a2 = vxVar.a();
        if (str2 != null) {
            a2 += str2.length();
        }
        if (vxVar.containsKey(str)) {
            String str3 = (String) vxVar.get(str);
            if (str3 != null) {
                a2 -= str3.length();
            }
        } else {
            a2 += str.length();
        }
        return a2 > this.b;
    }

    public void a(@NonNull String str) {
        if (this.c.c()) {
            this.c.b("The %s has reached the total size limit that equals %d symbols. Item with key %s will be ignored", this.f5308a, Integer.valueOf(this.b), str);
        }
    }
}
