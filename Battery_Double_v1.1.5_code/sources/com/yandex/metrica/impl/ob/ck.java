package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.metrica.CounterConfiguration.a;
import java.util.HashMap;
import java.util.Map;

public class ck {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final cl f4731a;
    @NonNull
    private final Context b;
    @NonNull
    private final Map<String, cj> c = new HashMap();

    public ck(@NonNull Context context, @NonNull cl clVar) {
        this.b = context;
        this.f4731a = clVar;
    }

    @Nullable
    public synchronized cj a(@NonNull String str, @NonNull a aVar) {
        cj cjVar;
        cjVar = (cj) this.c.get(str);
        if (cjVar == null) {
            cjVar = new cj(str, this.b, aVar, this.f4731a);
            this.c.put(str, cjVar);
        }
        return cjVar;
    }
}
