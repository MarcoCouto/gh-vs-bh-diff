package com.yandex.metrica.impl.ob;

import android.os.Handler;
import android.os.SystemClock;
import com.tapjoy.TJAdUnitConstants;

class at {

    /* renamed from: a reason: collision with root package name */
    private final Handler f4648a;
    private final n b;
    private final au c;

    at(Handler handler, n nVar) {
        this.f4648a = handler;
        this.b = nVar;
        this.c = new au(handler, nVar);
    }

    /* access modifiers changed from: 0000 */
    public void a() {
        b(this.f4648a, this.b, this.c);
    }

    /* access modifiers changed from: 0000 */
    public void b() {
        a(this.f4648a, this.b, this.c);
    }

    static void a(Handler handler, n nVar, Runnable runnable) {
        b(handler, nVar, runnable);
        handler.postAtTime(runnable, b(nVar), a(nVar));
    }

    private static long a(n nVar) {
        return SystemClock.uptimeMillis() + ((long) c(nVar));
    }

    private static void b(Handler handler, n nVar, Runnable runnable) {
        handler.removeCallbacks(runnable, b(nVar));
    }

    private static String b(n nVar) {
        return nVar.d().h().e();
    }

    private static int c(n nVar) {
        return wk.a(nVar.d().h().c(), 10) * TJAdUnitConstants.DEFAULT_VOLUME_CHECK_INTERVAL;
    }
}
