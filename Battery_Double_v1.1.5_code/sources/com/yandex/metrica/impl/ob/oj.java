package com.yandex.metrica.impl.ob;

public class oj {

    /* renamed from: a reason: collision with root package name */
    public final long f5029a;
    public final long b;

    public oj(long j, long j2) {
        this.f5029a = j;
        this.b = j2;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("IntervalRange{minInterval=");
        sb.append(this.f5029a);
        sb.append(", maxInterval=");
        sb.append(this.b);
        sb.append('}');
        return sb.toString();
    }
}
