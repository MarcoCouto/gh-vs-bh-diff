package com.yandex.metrica.impl.ob;

import android.location.Location;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.metrica.impl.ob.oh.a;

public class os {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    public a f5044a;
    @Nullable
    private Long b;
    private long c;
    private long d;
    @NonNull
    private Location e;

    public os(@NonNull a aVar, long j, long j2, @NonNull Location location) {
        this(aVar, j, j2, location, null);
    }

    public os(@NonNull a aVar, long j, long j2, @NonNull Location location, @Nullable Long l) {
        this.f5044a = aVar;
        this.b = l;
        this.c = j;
        this.d = j2;
        this.e = location;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("LocationWrapper{collectionMode=");
        sb.append(this.f5044a);
        sb.append(", mIncrementalId=");
        sb.append(this.b);
        sb.append(", mReceiveTimestamp=");
        sb.append(this.c);
        sb.append(", mReceiveElapsedRealtime=");
        sb.append(this.d);
        sb.append(", mLocation=");
        sb.append(this.e);
        sb.append('}');
        return sb.toString();
    }

    @Nullable
    public Long a() {
        return this.b;
    }

    public long b() {
        return this.c;
    }

    @NonNull
    public Location c() {
        return this.e;
    }

    public long d() {
        return this.d;
    }
}
