package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import android.util.Pair;
import com.applovin.sdk.AppLovinMediationProvider;
import com.facebook.appevents.UserDataStore;
import com.facebook.internal.NativeProtocol;
import com.ironsource.sdk.constants.LocationConst;
import com.tapjoy.TJAdUnitConstants.String;
import com.tapjoy.TapjoyConstants;
import com.vungle.warren.model.ReportDBAdapter.ReportColumns;
import com.yandex.metrica.impl.ob.rr.a.C0121a;
import com.yandex.metrica.impl.ob.rr.a.C0121a.C0122a;
import com.yandex.metrica.impl.ob.rr.a.c;
import com.yandex.metrica.impl.ob.rr.a.d;
import com.yandex.metrica.impl.ob.rr.a.e;
import com.yandex.metrica.impl.ob.rr.a.g;
import com.yandex.metrica.impl.ob.rr.a.i;
import com.yandex.metrica.impl.ob.rr.a.j;
import com.yandex.metrica.impl.ob.rr.a.k;
import io.fabric.sdk.android.services.network.HttpRequest;
import io.fabric.sdk.android.services.settings.SettingsJsonConstants;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class cq {

    /* renamed from: a reason: collision with root package name */
    private static final Map<String, C0108a> f4746a = Collections.unmodifiableMap(new HashMap<String, C0108a>() {
        {
            put("wifi", C0108a.WIFI);
            put("cell", C0108a.CELL);
        }
    });
    @NonNull
    private final co b;

    public static class a {
        @Nullable

        /* renamed from: a reason: collision with root package name */
        public final String f4747a;
        @Nullable
        public final String b;
        @Nullable
        public final String c;
        @NonNull
        public final List<Pair<String, String>> d;
        @Nullable
        public final Long e;
        @NonNull
        public final List<C0108a> f;

        /* renamed from: com.yandex.metrica.impl.ob.cq$a$a reason: collision with other inner class name */
        public enum C0108a {
            WIFI,
            CELL
        }

        public a(@Nullable String str, @Nullable String str2, @Nullable String str3, @NonNull List<Pair<String, String>> list, @Nullable Long l, @NonNull List<C0108a> list2) {
            this.f4747a = str;
            this.b = str2;
            this.c = str3;
            this.d = Collections.unmodifiableList(list);
            this.e = l;
            this.f = list2;
        }
    }

    public static class b {
        @NonNull
        private tt A;
        private List<pu> B = new ArrayList();
        private boolean C;
        @Nullable
        private ty D;

        /* renamed from: a reason: collision with root package name */
        private com.yandex.metrica.impl.ob.tv.a f4749a = new com.yandex.metrica.impl.ob.tv.a();
        private a b;
        private boolean c;
        /* access modifiers changed from: private */
        public boolean d;
        /* access modifiers changed from: private */
        public boolean e;
        /* access modifiers changed from: private */
        public boolean f;
        /* access modifiers changed from: private */
        public boolean g;
        private List<String> h;
        private String i = "";
        private List<String> j;
        private String k = "";
        private List<String> l;
        private String m;
        private String n;
        private String o;
        private String p;
        private ub q = null;
        @Nullable
        private tz r = null;
        private oh s;
        private oc t;
        private Long u;
        private List<a> v;
        private String w;
        private List<String> x;
        private um y;
        @Nullable
        private ua z;

        public enum a {
            BAD,
            OK
        }

        public tv a() {
            return this.f4749a.a();
        }

        public com.yandex.metrica.impl.ob.tv.a b() {
            return this.f4749a;
        }

        /* access modifiers changed from: private */
        public void a(boolean z2) {
            this.f4749a.a(z2);
        }

        /* access modifiers changed from: private */
        public void b(boolean z2) {
            this.f4749a.b(z2);
        }

        /* access modifiers changed from: private */
        public void c(boolean z2) {
            this.d = z2;
        }

        /* access modifiers changed from: private */
        public void d(boolean z2) {
            this.e = z2;
        }

        /* access modifiers changed from: 0000 */
        public void a(@NonNull String str, boolean z2) {
            this.B.add(new pu(str, z2));
        }

        /* access modifiers changed from: private */
        public void a(List<String> list) {
            this.h = list;
        }

        public List<String> c() {
            return this.h;
        }

        /* access modifiers changed from: private */
        public void a(@Nullable String str) {
            this.m = str;
        }

        @Nullable
        public String d() {
            return this.m;
        }

        /* access modifiers changed from: private */
        public void b(String str) {
            this.i = str;
        }

        public String e() {
            return this.i;
        }

        /* access modifiers changed from: private */
        public void b(List<String> list) {
            this.j = list;
        }

        public List<String> f() {
            return this.j;
        }

        /* access modifiers changed from: private */
        public void c(String str) {
            this.k = str;
        }

        public String g() {
            return this.k;
        }

        /* access modifiers changed from: private */
        public void c(List<String> list) {
            this.l = list;
        }

        public List<String> h() {
            return this.l;
        }

        /* access modifiers changed from: private */
        public void d(String str) {
            this.n = str;
        }

        public String i() {
            return this.n;
        }

        /* access modifiers changed from: private */
        public void e(String str) {
            this.o = str;
        }

        public String j() {
            return this.o;
        }

        /* access modifiers changed from: private */
        public void f(String str) {
            this.p = str;
        }

        public String k() {
            return this.p;
        }

        /* access modifiers changed from: private */
        public void a(a aVar) {
            this.b = aVar;
        }

        public a l() {
            return this.b;
        }

        public boolean m() {
            return this.c;
        }

        /* access modifiers changed from: private */
        public void e(boolean z2) {
            this.c = z2;
        }

        /* access modifiers changed from: private */
        public void a(ub ubVar) {
            this.q = ubVar;
        }

        public ub n() {
            return this.q;
        }

        /* access modifiers changed from: private */
        public void a(@NonNull tz tzVar) {
            this.r = tzVar;
        }

        @Nullable
        public tz o() {
            return this.r;
        }

        @NonNull
        public ua p() {
            return this.z;
        }

        public List<pu> q() {
            return this.B;
        }

        public oh r() {
            return this.s;
        }

        public oc s() {
            return this.t;
        }

        /* access modifiers changed from: private */
        public void f(boolean z2) {
            this.f4749a.c(z2);
        }

        /* access modifiers changed from: private */
        public void g(boolean z2) {
            this.f4749a.d(z2);
        }

        /* access modifiers changed from: private */
        public void h(boolean z2) {
            this.f4749a.e(z2);
        }

        /* access modifiers changed from: private */
        public void i(boolean z2) {
            this.f4749a.f(z2);
        }

        /* access modifiers changed from: private */
        public void a(Long l2) {
            this.u = l2;
        }

        public Long t() {
            return this.u;
        }

        /* access modifiers changed from: private */
        public void d(List<a> list) {
            this.v = list;
        }

        public List<a> u() {
            return this.v;
        }

        public String v() {
            return this.w;
        }

        /* access modifiers changed from: private */
        public void g(String str) {
            this.w = str;
        }

        public List<String> w() {
            return this.x;
        }

        /* access modifiers changed from: private */
        public void e(List<String> list) {
            this.x = list;
        }

        public um x() {
            return this.y;
        }

        /* access modifiers changed from: private */
        public void a(um umVar) {
            this.y = umVar;
        }

        /* access modifiers changed from: private */
        public void a(@NonNull ua uaVar) {
            this.z = uaVar;
        }

        /* access modifiers changed from: private */
        public void a(@NonNull oh ohVar) {
            this.s = ohVar;
        }

        /* access modifiers changed from: private */
        public void a(@NonNull oc ocVar) {
            this.t = ocVar;
        }

        /* access modifiers changed from: private */
        public void j(boolean z2) {
            this.f = z2;
        }

        /* access modifiers changed from: private */
        public void k(boolean z2) {
            this.g = z2;
        }

        /* access modifiers changed from: private */
        public void l(boolean z2) {
            this.C = z2;
        }

        /* access modifiers changed from: private */
        public void a(@Nullable ty tyVar) {
            this.D = tyVar;
        }

        @Nullable
        public ty y() {
            return this.D;
        }

        public boolean z() {
            return this.C;
        }

        @NonNull
        public tt A() {
            return this.A;
        }

        public void a(@NonNull tt ttVar) {
            this.A = ttVar;
        }
    }

    public cq() {
        this(new co());
    }

    private String a(JSONObject jSONObject, String str) {
        try {
            return jSONObject.getJSONObject(str).getJSONArray("urls").getString(0);
        } catch (Throwable unused) {
            return "";
        }
    }

    private List<String> b(JSONObject jSONObject, String str) {
        try {
            return vq.a(jSONObject.getJSONObject(str).getJSONArray("urls"));
        } catch (Throwable unused) {
            return null;
        }
    }

    public b a(byte[] bArr) {
        b bVar = new b();
        try {
            com.yandex.metrica.impl.ob.vq.a aVar = new com.yandex.metrica.impl.ob.vq.a(new String(bArr, "UTF-8"));
            d(bVar, aVar);
            g(bVar, aVar);
            k(bVar, aVar);
            l(bVar, aVar);
            f(bVar, aVar);
            a(bVar, aVar);
            b(bVar, aVar);
            c(bVar, aVar);
            if (bVar.a().c) {
                i(bVar, aVar);
            }
            if (bVar.m()) {
                j(bVar, aVar);
            }
            if (bVar.z()) {
                c(bVar, (JSONObject) aVar);
            }
            bVar.a(a(aVar.optJSONObject("foreground_location_collection"), bVar.d, bVar.f));
            bVar.a(c(aVar.optJSONObject("background_location_collection"), bVar.e, bVar.g));
            b(bVar, (JSONObject) aVar);
            e(bVar, aVar);
            h(bVar, aVar);
            bVar.a(this.b.a((JSONObject) aVar));
            bVar.a(a.OK);
            return bVar;
        } catch (Throwable unused) {
            b bVar2 = new b();
            bVar2.a(a.BAD);
            return bVar2;
        }
    }

    private void d(@NonNull b bVar, @NonNull com.yandex.metrica.impl.ob.vq.a aVar) {
        String str = "";
        String str2 = "";
        JSONObject optJSONObject = aVar.optJSONObject("device_id");
        if (optJSONObject != null) {
            str2 = optJSONObject.optString(SettingsJsonConstants.ICON_HASH_KEY);
            str = optJSONObject.optString("value");
        }
        bVar.d(str);
        bVar.e(str2);
    }

    private static oh a(@Nullable JSONObject jSONObject, boolean z, boolean z2) {
        return new ms().a(b(jSONObject, z, z2));
    }

    private static e b(@Nullable JSONObject jSONObject, boolean z, boolean z2) {
        e eVar = new e();
        eVar.b = wk.a(vq.a(jSONObject, "min_update_interval_seconds"), TimeUnit.SECONDS, eVar.b);
        eVar.c = wk.a(vq.d(jSONObject, "min_update_distance_meters"), eVar.c);
        eVar.d = wk.a(vq.b(jSONObject, "records_count_to_force_flush"), eVar.d);
        eVar.e = wk.a(vq.b(jSONObject, "max_records_count_in_batch"), eVar.e);
        eVar.f = wk.a(vq.a(jSONObject, "max_age_seconds_to_force_flush"), TimeUnit.SECONDS, eVar.f);
        eVar.g = wk.a(vq.b(jSONObject, "max_records_to_store_locally"), eVar.g);
        eVar.h = z;
        eVar.j = wk.a(vq.a(jSONObject, "lbs_min_update_interval_seconds"), TimeUnit.SECONDS, eVar.j);
        eVar.i = z2;
        return eVar;
    }

    private static oc c(@Nullable JSONObject jSONObject, boolean z, boolean z2) {
        JSONArray jSONArray;
        C0121a aVar = new C0121a();
        aVar.b = b(jSONObject, z, z2);
        aVar.c = wk.a(vq.a(jSONObject, "collection_duration_seconds"), TimeUnit.SECONDS, aVar.c);
        aVar.d = wk.a(vq.a(jSONObject, "collection_interval_seconds"), TimeUnit.SECONDS, aVar.d);
        aVar.e = vq.a(jSONObject, "aggressive_relaunch", aVar.e);
        if (jSONObject == null) {
            jSONArray = null;
        } else {
            jSONArray = jSONObject.optJSONArray("collection_interval_ranges_seconds");
        }
        aVar.f = a(jSONArray, aVar.f);
        return new mn().a(aVar);
    }

    private static C0122a[] a(@Nullable JSONArray jSONArray, C0122a[] aVarArr) {
        if (jSONArray == null || jSONArray.length() <= 0) {
            return aVarArr;
        }
        try {
            C0122a[] aVarArr2 = new C0122a[jSONArray.length()];
            int i = 0;
            while (i < jSONArray.length()) {
                try {
                    aVarArr2[i] = new C0122a();
                    JSONObject jSONObject = jSONArray.getJSONObject(i);
                    aVarArr2[i].b = TimeUnit.SECONDS.toMillis(jSONObject.getLong("min"));
                    aVarArr2[i].c = TimeUnit.SECONDS.toMillis(jSONObject.getLong(AppLovinMediationProvider.MAX));
                    i++;
                } catch (Throwable unused) {
                }
            }
            return aVarArr2;
        } catch (Throwable unused2) {
            return aVarArr;
        }
    }

    private static void e(@NonNull b bVar, @NonNull com.yandex.metrica.impl.ob.vq.a aVar) throws JSONException {
        if (aVar.has("requests")) {
            JSONObject jSONObject = aVar.getJSONObject("requests");
            if (jSONObject.has("list")) {
                JSONArray jSONArray = jSONObject.getJSONArray("list");
                ArrayList arrayList = new ArrayList(jSONArray.length());
                for (int i = 0; i < jSONArray.length(); i++) {
                    try {
                        arrayList.add(a(jSONArray.getJSONObject(i)));
                    } catch (Throwable unused) {
                    }
                }
                if (!arrayList.isEmpty()) {
                    bVar.d((List<a>) arrayList);
                }
            }
        }
    }

    @NonNull
    private static a a(@NonNull JSONObject jSONObject) throws JSONException {
        JSONObject jSONObject2 = jSONObject.getJSONObject("headers");
        ArrayList arrayList = new ArrayList(jSONObject2.length());
        Iterator keys = jSONObject2.keys();
        while (keys.hasNext()) {
            String str = (String) keys.next();
            JSONArray jSONArray = jSONObject2.getJSONArray(str);
            for (int i = 0; i < jSONArray.length(); i++) {
                arrayList.add(new Pair(str, jSONArray.getString(i)));
            }
        }
        a aVar = new a(jSONObject.optString("id", null), jSONObject.optString("url", null), jSONObject.optString("method", null), arrayList, Long.valueOf(jSONObject.getLong("delay_seconds")), b(jSONObject));
        return aVar;
    }

    @NonNull
    private static List<C0108a> b(@NonNull JSONObject jSONObject) throws JSONException {
        ArrayList arrayList = new ArrayList();
        if (jSONObject.has("accept_network_types")) {
            JSONArray jSONArray = jSONObject.getJSONArray("accept_network_types");
            for (int i = 0; i < jSONArray.length(); i++) {
                arrayList.add(f4746a.get(jSONArray.getString(i)));
            }
        }
        return arrayList;
    }

    private static void f(b bVar, com.yandex.metrica.impl.ob.vq.a aVar) throws JSONException {
        c cVar = new c();
        e eVar = new e();
        JSONObject jSONObject = (JSONObject) aVar.a(SettingsJsonConstants.FEATURES_KEY, new JSONObject());
        if (jSONObject.has("list")) {
            JSONObject jSONObject2 = jSONObject.getJSONObject("list");
            bVar.a(a(jSONObject2, "easy_collecting", cVar.b));
            bVar.b(a(jSONObject2, "package_info", cVar.c));
            bVar.e(a(jSONObject2, "socket", false));
            bVar.f(a(jSONObject2, "permissions_collecting", cVar.d));
            bVar.g(a(jSONObject2, "features_collecting", cVar.e));
            bVar.h(a(jSONObject2, "sdk_list", cVar.o));
            bVar.c(a(jSONObject2, "foreground_location_collection", eVar.h));
            bVar.j(a(jSONObject2, "foreground_lbs_collection", eVar.i));
            bVar.d(a(jSONObject2, "background_location_collection", eVar.h));
            bVar.k(a(jSONObject2, "background_lbs_collection", eVar.i));
            bVar.l(a(jSONObject2, "identity_light_collecting", cVar.p));
            bVar.i(a(jSONObject2, "ble_collecting", cVar.q));
            bVar.b().g(a(jSONObject2, TapjoyConstants.TJC_ANDROID_ID, cVar.f)).h(a(jSONObject2, "google_aid", cVar.g)).i(a(jSONObject2, "wifi_around", cVar.h)).j(a(jSONObject2, "wifi_connected", cVar.i)).k(a(jSONObject2, "own_macs", cVar.j)).l(a(jSONObject2, "cells_around", cVar.k)).m(a(jSONObject2, "sim_info", cVar.l)).n(a(jSONObject2, "sim_imei", cVar.m)).o(a(jSONObject2, "access_point", cVar.n));
        }
    }

    @VisibleForTesting
    static void a(@NonNull b bVar, @NonNull com.yandex.metrica.impl.ob.vq.a aVar) {
        JSONObject optJSONObject = aVar.optJSONObject("locale");
        String str = "";
        if (optJSONObject != null) {
            JSONObject optJSONObject2 = optJSONObject.optJSONObject(UserDataStore.COUNTRY);
            if (optJSONObject2 != null && optJSONObject2.optBoolean("reliable", false)) {
                str = optJSONObject2.optString("value", "");
            }
        }
        bVar.g(str);
    }

    @VisibleForTesting
    static void b(@NonNull b bVar, @NonNull com.yandex.metrica.impl.ob.vq.a aVar) {
        JSONObject optJSONObject = aVar.optJSONObject(NativeProtocol.RESULT_ARGS_PERMISSIONS);
        if (optJSONObject != null) {
            JSONArray optJSONArray = optJSONObject.optJSONArray("list");
            if (optJSONArray != null) {
                for (int i = 0; i < optJSONArray.length(); i++) {
                    JSONObject optJSONObject2 = optJSONArray.optJSONObject(i);
                    if (optJSONObject2 != null) {
                        String optString = optJSONObject2.optString("name");
                        boolean optBoolean = optJSONObject2.optBoolean(String.ENABLED);
                        if (TextUtils.isEmpty(optString)) {
                            bVar.a("", false);
                        } else {
                            bVar.a(optString, optBoolean);
                        }
                    }
                }
            }
        }
    }

    static void c(@NonNull b bVar, @NonNull com.yandex.metrica.impl.ob.vq.a aVar) {
        a(bVar, aVar, new nc());
    }

    @VisibleForTesting
    static void a(@NonNull b bVar, @NonNull com.yandex.metrica.impl.ob.vq.a aVar, @NonNull nc ncVar) {
        bVar.a(ncVar.a(a(aVar)));
    }

    static i a(@NonNull com.yandex.metrica.impl.ob.vq.a aVar) {
        i iVar = new i();
        JSONObject optJSONObject = aVar.optJSONObject("sdk_list");
        if (optJSONObject == null) {
            return iVar;
        }
        iVar.b = wk.a(vq.a(optJSONObject, "min_collecting_interval_seconds"), TimeUnit.SECONDS, iVar.b);
        iVar.c = wk.a(vq.a(optJSONObject, "min_first_collecting_delay_seconds"), TimeUnit.SECONDS, iVar.c);
        iVar.d = wk.a(vq.a(optJSONObject, "min_collecting_delay_after_launch_seconds"), TimeUnit.SECONDS, iVar.d);
        iVar.e = wk.a(vq.a(optJSONObject, "min_request_retry_interval_seconds"), TimeUnit.SECONDS, iVar.e);
        return iVar;
    }

    private static void g(@NonNull b bVar, @NonNull com.yandex.metrica.impl.ob.vq.a aVar) {
        JSONObject optJSONObject = aVar.optJSONObject("queries");
        if (optJSONObject != null) {
            JSONObject optJSONObject2 = optJSONObject.optJSONObject("list");
            if (optJSONObject2 != null) {
                JSONObject optJSONObject3 = optJSONObject2.optJSONObject("sdk_list");
                if (optJSONObject3 != null) {
                    bVar.a(optJSONObject3.optString("url", null));
                }
            }
        }
    }

    private static void h(@NonNull b bVar, @NonNull com.yandex.metrica.impl.ob.vq.a aVar) {
        k kVar = new k();
        JSONObject optJSONObject = aVar.optJSONObject("stat_sending");
        if (optJSONObject != null) {
            kVar.b = wk.a(vq.a(optJSONObject, "disabled_reporting_interval_seconds"), TimeUnit.SECONDS, kVar.b);
        }
        bVar.a(new ng().a(kVar));
    }

    private static boolean a(JSONObject jSONObject, String str, boolean z) throws JSONException {
        return jSONObject.has(str) ? jSONObject.getJSONObject(str).optBoolean(String.ENABLED, z) : z;
    }

    private void i(b bVar, com.yandex.metrica.impl.ob.vq.a aVar) {
        long j;
        long j2;
        JSONObject optJSONObject = aVar.optJSONObject("permissions_collecting");
        g gVar = new g();
        if (optJSONObject != null) {
            j2 = optJSONObject.optLong("check_interval_seconds", gVar.b);
            j = optJSONObject.optLong("force_send_interval_seconds", gVar.c);
        } else {
            j2 = gVar.b;
            j = gVar.c;
        }
        bVar.a(new tz(j2, j));
    }

    private void j(b bVar, com.yandex.metrica.impl.ob.vq.a aVar) {
        JSONObject optJSONObject = aVar.optJSONObject("socket");
        if (optJSONObject != null) {
            long optLong = optJSONObject.optLong("seconds_to_live");
            long optLong2 = optJSONObject.optLong("first_delay_seconds", new j().e);
            int optInt = optJSONObject.optInt("launch_delay_seconds", new j().f);
            String optString = optJSONObject.optString("token");
            JSONArray optJSONArray = optJSONObject.optJSONArray("ports");
            if (optLong > 0 && a(optString) && optJSONArray != null && optJSONArray.length() > 0) {
                ArrayList arrayList = new ArrayList(optJSONArray.length());
                for (int i = 0; i < optJSONArray.length(); i++) {
                    int optInt2 = optJSONArray.optInt(i);
                    if (optInt2 != 0) {
                        arrayList.add(Integer.valueOf(optInt2));
                    }
                }
                if (!arrayList.isEmpty()) {
                    ub ubVar = new ub(optLong, optString, arrayList, optLong2, optInt);
                    bVar.a(ubVar);
                }
            }
        }
    }

    private void k(b bVar, com.yandex.metrica.impl.ob.vq.a aVar) throws JSONException {
        JSONObject jSONObject = (JSONObject) aVar.a("query_hosts", new JSONObject());
        if (jSONObject.has("list")) {
            JSONObject jSONObject2 = jSONObject.getJSONObject("list");
            String a2 = a(jSONObject2, "get_ad");
            if (a(a2)) {
                bVar.b(a2);
            }
            List b2 = b(jSONObject2, ReportColumns.TABLE_NAME);
            if (a(b2)) {
                bVar.b(b2);
            }
            String a3 = a(jSONObject2, "report_ad");
            if (a(a3)) {
                bVar.c(a3);
            }
            List b3 = b(jSONObject2, "location");
            if (a(b3)) {
                bVar.c(b3);
            }
            List b4 = b(jSONObject2, "startup");
            if (a(b4)) {
                bVar.a(b4);
            }
            List b5 = b(jSONObject2, "diagnostic");
            if (a(b5)) {
                bVar.e(b5);
            }
        }
    }

    private boolean a(String str) {
        return !TextUtils.isEmpty(str);
    }

    private boolean a(List<String> list) {
        return !cx.a((Collection) list);
    }

    private void l(b bVar, com.yandex.metrica.impl.ob.vq.a aVar) throws JSONException {
        JSONObject optJSONObject = ((JSONObject) aVar.a("distribution_customization", new JSONObject())).optJSONObject("clids");
        if (optJSONObject != null) {
            a(bVar, optJSONObject);
        }
    }

    private void a(b bVar, JSONObject jSONObject) throws JSONException {
        HashMap hashMap = new HashMap();
        Iterator keys = jSONObject.keys();
        while (keys.hasNext()) {
            String str = (String) keys.next();
            JSONObject optJSONObject = jSONObject.optJSONObject(str);
            if (optJSONObject != null && optJSONObject.has("value")) {
                hashMap.put(str, optJSONObject.getString("value"));
            }
        }
        bVar.f(we.a((Map<String, String>) hashMap));
    }

    private void b(b bVar, JSONObject jSONObject) {
        JSONObject optJSONObject = jSONObject.optJSONObject(LocationConst.TIME);
        if (optJSONObject != null) {
            try {
                bVar.a(Long.valueOf(optJSONObject.getLong("max_valid_difference_seconds")));
            } catch (Throwable unused) {
            }
        }
    }

    private void c(b bVar, JSONObject jSONObject) {
        bVar.a(new mt().a(c(jSONObject.optJSONObject("identity_light_collecting"))));
    }

    private d c(@Nullable JSONObject jSONObject) {
        d dVar = new d();
        if (jSONObject != null) {
            dVar.b = jSONObject.optLong("min_interval_seconds", dVar.b);
        }
        return dVar;
    }

    public static Long a(@Nullable Map<String, List<String>> map) {
        if (!cx.a((Map) map)) {
            List list = (List) map.get(HttpRequest.HEADER_DATE);
            if (!cx.a((Collection) list)) {
                try {
                    return Long.valueOf(new SimpleDateFormat("E, d MMM yyyy HH:mm:ss z", Locale.US).parse((String) list.get(0)).getTime());
                } catch (Throwable unused) {
                }
            }
        }
        return null;
    }

    @VisibleForTesting
    public cq(@NonNull co coVar) {
        this.b = coVar;
    }
}
