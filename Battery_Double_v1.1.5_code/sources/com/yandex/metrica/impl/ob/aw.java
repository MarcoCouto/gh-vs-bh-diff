package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;

public final class aw {

    /* renamed from: a reason: collision with root package name */
    private static volatile aw f4651a;
    private static final Object b = new Object();
    /* access modifiers changed from: private */
    @NonNull
    public String c;

    public static aw a(@NonNull Context context) {
        if (f4651a == null) {
            synchronized (b) {
                if (f4651a == null) {
                    f4651a = new aw(context.getApplicationContext());
                }
            }
        }
        return f4651a;
    }

    private aw(Context context) {
        this.c = bt.a(context.getResources().getConfiguration().locale);
        dr.a().a(this, dz.class, dv.a((du<T>) new du<dz>() {
            public void a(dz dzVar) {
                aw.this.c = dzVar.f4802a;
            }
        }).a());
    }

    @NonNull
    public String a() {
        return this.c;
    }
}
