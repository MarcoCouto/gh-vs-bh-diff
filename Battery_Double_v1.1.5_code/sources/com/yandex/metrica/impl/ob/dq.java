package com.yandex.metrica.impl.ob;

import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

public class dq {

    /* renamed from: a reason: collision with root package name */
    private final a f4790a;
    private final wl<Thread, StackTraceElement[], kk> b;

    interface a {
        Thread a();

        Map<Thread, StackTraceElement[]> b();
    }

    public dq() {
        this(new a() {
            public Thread a() {
                return Looper.getMainLooper().getThread();
            }

            public Map<Thread, StackTraceElement[]> b() {
                return Thread.getAllStackTraces();
            }
        }, new dp());
    }

    @VisibleForTesting
    dq(@NonNull a aVar, @NonNull wl<Thread, StackTraceElement[], kk> wlVar) {
        this.f4790a = aVar;
        this.b = wlVar;
    }

    public kg a() {
        Thread a2 = this.f4790a.a();
        return new kg(b(a2), a(a2, null));
    }

    public List<kk> a(@Nullable Thread thread) {
        Thread a2 = this.f4790a.a();
        List<kk> a3 = a(a2, thread);
        if (thread != a2) {
            a3.add(0, b(a2));
        }
        return a3;
    }

    private kk b(@NonNull Thread thread) {
        StackTraceElement[] stackTraceElementArr;
        try {
            stackTraceElementArr = thread.getStackTrace();
        } catch (SecurityException unused) {
            stackTraceElementArr = null;
        }
        return (kk) this.b.a(thread, stackTraceElementArr);
    }

    private List<kk> a(@NonNull Thread thread, @Nullable Thread thread2) {
        Map map;
        ArrayList arrayList = new ArrayList();
        TreeMap treeMap = new TreeMap(new Comparator<Thread>() {
            /* renamed from: a */
            public int compare(Thread thread, Thread thread2) {
                if (thread == thread2) {
                    return 0;
                }
                return cu.d(thread.getName(), thread2.getName());
            }
        });
        try {
            map = this.f4790a.b();
        } catch (SecurityException unused) {
            map = null;
        }
        if (map != null) {
            treeMap.putAll(map);
        }
        if (thread2 != null) {
            treeMap.remove(thread2);
        }
        for (Entry entry : treeMap.entrySet()) {
            Thread thread3 = (Thread) entry.getKey();
            if (!(thread3 == thread || thread3 == thread2)) {
                arrayList.add(this.b.a(thread3, (StackTraceElement[]) entry.getValue()));
            }
        }
        return arrayList;
    }
}
