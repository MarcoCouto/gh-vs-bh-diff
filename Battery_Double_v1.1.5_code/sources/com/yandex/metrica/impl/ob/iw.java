package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public interface iw<A> {
    @Nullable
    ix a();

    @NonNull
    ix a(@NonNull A a2);
}
