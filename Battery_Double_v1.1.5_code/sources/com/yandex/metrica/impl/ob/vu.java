package com.yandex.metrica.impl.ob;

import android.annotation.SuppressLint;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;

public class vu {

    /* renamed from: a reason: collision with root package name */
    private String f5277a;

    public String a() {
        if (this.f5277a != null) {
            return this.f5277a;
        }
        this.f5277a = VERSION.SDK_INT >= 18 ? c() : b();
        return this.f5277a;
    }

    @SuppressLint({"StaticFieldLeak"})
    private String b() {
        try {
            FutureTask futureTask = new FutureTask(new Callable<String>() {
                /* renamed from: a */
                public String call() {
                    return vu.this.d();
                }
            });
            db.k().d().post(futureTask);
            return (String) futureTask.get(5, TimeUnit.SECONDS);
        } catch (Throwable unused) {
            return null;
        }
    }

    private String c() {
        return d();
    }

    /* access modifiers changed from: private */
    @SuppressLint({"PrivateApi"})
    public String d() {
        try {
            Class cls = Class.forName("android.app.ActivityThread");
            return (String) cls.getMethod("getProcessName", new Class[0]).invoke(cls.getMethod("currentActivityThread", new Class[0]).invoke(null, new Object[0]), new Object[0]);
        } catch (Throwable th) {
            throw new RuntimeException(th);
        }
    }

    public boolean a(@NonNull String str) {
        boolean z = false;
        try {
            if (!TextUtils.isEmpty(a())) {
                String a2 = a();
                StringBuilder sb = new StringBuilder();
                sb.append(":");
                sb.append(str);
                if (a2.endsWith(sb.toString())) {
                    z = true;
                }
            }
            return z;
        } catch (Throwable unused) {
            return false;
        }
    }
}
