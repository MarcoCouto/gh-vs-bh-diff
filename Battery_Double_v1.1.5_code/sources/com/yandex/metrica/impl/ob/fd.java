package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import com.yandex.metrica.impl.ob.et;
import com.yandex.metrica.impl.ob.ew;

public class fd<COMPONENT extends ew & et> implements eq<fc> {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final ft<COMPONENT> f4835a;

    public fd(@NonNull ft<COMPONENT> ftVar) {
        this.f4835a = ftVar;
    }

    /* renamed from: a */
    public fc b(@NonNull Context context, @NonNull ek ekVar, @NonNull eg egVar) {
        return new fc(context, ekVar, egVar, this.f4835a);
    }
}
