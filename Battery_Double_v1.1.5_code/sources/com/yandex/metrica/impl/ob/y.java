package com.yandex.metrica.impl.ob;

import android.content.ContentValues;
import android.content.Context;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import com.tapjoy.TJAdUnitConstants.String;
import com.tapjoy.TapjoyConstants;
import com.yandex.metrica.impl.ob.i.a;
import org.json.JSONException;
import org.json.JSONObject;

public final class y {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private Context f5311a;
    /* access modifiers changed from: private */
    public ContentValues b;
    private st c;

    public y(Context context) {
        this.f5311a = context;
    }

    public y a(ContentValues contentValues) {
        this.b = contentValues;
        return this;
    }

    public y a(@NonNull st stVar) {
        this.c = stVar;
        return this;
    }

    public void a() {
        c();
    }

    private void c() {
        JSONObject jSONObject = new JSONObject();
        try {
            a(jSONObject);
        } catch (Throwable unused) {
            jSONObject = new JSONObject();
        }
        this.b.put("report_request_parameters", jSONObject.toString());
    }

    private void a(@NonNull JSONObject jSONObject) throws JSONException {
        jSONObject.putOpt("dId", this.c.r()).putOpt("uId", this.c.t()).putOpt("appVer", this.c.q()).putOpt("appBuild", this.c.p()).putOpt("analyticsSdkVersionName", this.c.i()).putOpt("kitBuildNumber", this.c.j()).putOpt("kitBuildType", this.c.k()).putOpt("osVer", this.c.n()).putOpt("osApiLev", Integer.valueOf(this.c.o())).putOpt("lang", this.c.A()).putOpt("root", this.c.u()).putOpt("app_debuggable", this.c.E()).putOpt("app_framework", this.c.v()).putOpt("attribution_id", Integer.valueOf(this.c.V()));
    }

    private void a(@NonNull JSONObject jSONObject, @NonNull cz czVar) throws JSONException {
        vq.a(jSONObject, czVar);
    }

    private void d() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put(String.ENABLED, this.c.N());
            cz b2 = b();
            if (b2 != null) {
                a(jSONObject, b2);
            }
            this.b.put("location_info", jSONObject.toString());
        } catch (Throwable unused) {
        }
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public cz b() {
        Location location;
        cz czVar = null;
        if (this.c.N()) {
            location = this.c.O();
            if (location == null) {
                location = oo.a(this.f5311a).a();
                if (location == null) {
                    location = oo.a(this.f5311a).b();
                }
            } else {
                czVar = cz.a(location);
            }
        } else {
            location = null;
        }
        return (czVar != null || location == null) ? czVar : cz.b(location);
    }

    private void b(@NonNull cy cyVar) {
        this.b.put("wifi_network_info", cyVar.a().toString());
    }

    private void a(ut utVar) {
        utVar.a((uv) new uv() {
            public void a(uu[] uuVarArr) {
                y.this.b.put("cell_info", vq.a(uuVarArr).toString());
            }
        });
    }

    private void a(a aVar) {
        this.b.put("app_environment", aVar.f4882a);
        this.b.put("app_environment_revision", Long.valueOf(aVar.b));
    }

    private void e() {
        vd k = al.a().k();
        k.a((vg) new vg() {
            public void a(vf vfVar) {
                uu b = vfVar.b();
                if (b != null) {
                    y.this.b.put("cellular_connection_type", b.g());
                }
            }
        });
        a((ut) k);
    }

    public void a(@NonNull ww wwVar, @NonNull a aVar) {
        w wVar = wwVar.f5295a;
        this.b.put("name", wVar.d());
        this.b.put("value", wVar.e());
        this.b.put("type", Integer.valueOf(wVar.g()));
        this.b.put("custom_type", Integer.valueOf(wVar.h()));
        this.b.put("error_environment", wVar.j());
        this.b.put("user_info", wVar.l());
        this.b.put("truncated", Integer.valueOf(wVar.o()));
        this.b.put(TapjoyConstants.TJC_CONNECTION_TYPE, Integer.valueOf(bt.e(this.f5311a)));
        this.b.put("profile_id", wVar.p());
        this.b.put("encrypting_mode", Integer.valueOf(wwVar.b.a()));
        this.b.put("first_occurrence_status", Integer.valueOf(wwVar.f5295a.q().d));
        a(aVar);
        d();
        f();
    }

    private void f() {
        e();
        cy a2 = cy.a(this.f5311a);
        b(a2);
        a(a2);
    }

    /* access modifiers changed from: 0000 */
    public void a(cy cyVar) {
        String b2 = cyVar.b(this.f5311a);
        if (!TextUtils.isEmpty(b2)) {
            int c2 = cyVar.c(this.f5311a);
            try {
                JSONObject jSONObject = new JSONObject();
                jSONObject.put("ssid", b2);
                jSONObject.put("state", c2);
                this.b.put("wifi_access_point", jSONObject.toString());
            } catch (Throwable unused) {
            }
        }
    }
}
