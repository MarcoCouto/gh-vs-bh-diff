package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import com.yandex.metrica.impl.ob.bt.a;
import java.util.EnumSet;

public class pi {

    /* renamed from: a reason: collision with root package name */
    private static final EnumSet<a> f5060a = EnumSet.of(a.OFFLINE);
    private vn b = new vk();
    private final Context c;

    public pi(@NonNull Context context) {
        this.c = context;
    }

    public boolean a() {
        return !f5060a.contains(this.b.a(this.c));
    }
}
