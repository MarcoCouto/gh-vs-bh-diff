package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.util.SparseArray;
import com.yandex.metrica.impl.ob.ls.a;
import com.yandex.metrica.impl.ob.ls.aa;
import com.yandex.metrica.impl.ob.ls.ab;
import com.yandex.metrica.impl.ob.ls.b;
import com.yandex.metrica.impl.ob.ls.c;
import com.yandex.metrica.impl.ob.ls.d;
import com.yandex.metrica.impl.ob.ls.e;
import com.yandex.metrica.impl.ob.ls.f;
import com.yandex.metrica.impl.ob.ls.g;
import com.yandex.metrica.impl.ob.ls.h;
import com.yandex.metrica.impl.ob.ls.i;
import com.yandex.metrica.impl.ob.ls.j;
import com.yandex.metrica.impl.ob.ls.k;
import com.yandex.metrica.impl.ob.ls.l;
import com.yandex.metrica.impl.ob.ls.m;
import com.yandex.metrica.impl.ob.ls.n;
import com.yandex.metrica.impl.ob.ls.o;
import com.yandex.metrica.impl.ob.ls.p;
import com.yandex.metrica.impl.ob.ls.q;
import com.yandex.metrica.impl.ob.ls.r;
import com.yandex.metrica.impl.ob.ls.s;
import com.yandex.metrica.impl.ob.ls.t;
import com.yandex.metrica.impl.ob.ls.u;
import com.yandex.metrica.impl.ob.ls.v;
import com.yandex.metrica.impl.ob.ls.w;
import com.yandex.metrica.impl.ob.ls.x;
import com.yandex.metrica.impl.ob.ls.y;
import com.yandex.metrica.impl.ob.ls.z;

public class lb {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final SparseArray<lr> f4961a = new SparseArray<>();
    @NonNull
    private final SparseArray<lr> b;
    @NonNull
    private final lr c;
    @NonNull
    private final lr d;
    @NonNull
    private final lr e;
    @NonNull
    private final lr f;
    @NonNull
    private final lr g;
    @NonNull
    private final lr h;

    public lb() {
        this.f4961a.put(6, new v());
        this.f4961a.put(7, new z());
        this.f4961a.put(14, new o());
        this.f4961a.put(29, new p());
        this.f4961a.put(37, new q());
        this.f4961a.put(39, new r());
        this.f4961a.put(45, new s());
        this.f4961a.put(47, new t());
        this.f4961a.put(50, new u());
        this.f4961a.put(60, new w());
        this.f4961a.put(66, new x());
        this.f4961a.put(67, new y());
        this.f4961a.put(73, new aa());
        this.f4961a.put(77, new ab());
        this.b = new SparseArray<>();
        this.b.put(12, new g());
        this.b.put(29, new h());
        this.b.put(47, new i());
        this.b.put(50, new j());
        this.b.put(55, new k());
        this.b.put(60, new l());
        this.b.put(63, new m());
        this.b.put(67, new n());
        this.c = new c();
        this.d = new d();
        this.e = new a();
        this.f = new b();
        this.g = new e();
        this.h = new f();
    }

    @NonNull
    public SparseArray<lr> a() {
        return this.f4961a;
    }

    @NonNull
    public SparseArray<lr> b() {
        return this.b;
    }

    @NonNull
    public lr c() {
        return this.c;
    }

    @NonNull
    public lr d() {
        return this.d;
    }

    @NonNull
    public lr e() {
        return this.e;
    }

    @NonNull
    public lr f() {
        return this.f;
    }

    @NonNull
    public lr g() {
        return this.g;
    }

    @NonNull
    public lr h() {
        return this.h;
    }
}
