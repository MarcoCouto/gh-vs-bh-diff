package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

public class fo {

    /* renamed from: a reason: collision with root package name */
    private final Object f4841a;
    private final fu b;
    private final HashMap<fn, fp> c;
    private final wp<a, fn> d;
    @NonNull
    private final Context e;
    private volatile int f;
    @NonNull
    private final fr g;

    private static final class a {
        @NonNull

        /* renamed from: a reason: collision with root package name */
        private final String f4842a;
        @Nullable
        private final Integer b;
        @Nullable
        private final String c;

        a(@NonNull String str, @Nullable Integer num, @Nullable String str2) {
            this.f4842a = str;
            this.b = num;
            this.c = str2;
        }

        a(@NonNull fn fnVar) {
            this(fnVar.b(), fnVar.c(), fnVar.d());
        }

        public int hashCode() {
            int i = 0;
            int hashCode = ((this.f4842a.hashCode() * 31) + (this.b != null ? this.b.hashCode() : 0)) * 31;
            if (this.c != null) {
                i = this.c.hashCode();
            }
            return hashCode + i;
        }

        public boolean equals(Object obj) {
            boolean z = true;
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            a aVar = (a) obj;
            if (!this.f4842a.equals(aVar.f4842a)) {
                return false;
            }
            if (this.b == null ? aVar.b != null : !this.b.equals(aVar.b)) {
                return false;
            }
            if (this.c != null) {
                z = this.c.equals(aVar.c);
            } else if (aVar.c != null) {
                z = false;
            }
            return z;
        }
    }

    public fo(@NonNull Context context, @NonNull fu fuVar) {
        this(context, fuVar, new fr());
    }

    @VisibleForTesting
    fo(@NonNull Context context, @NonNull fu fuVar, @NonNull fr frVar) {
        this.f4841a = new Object();
        this.c = new HashMap<>();
        this.d = new wp<>();
        this.f = 0;
        this.e = context.getApplicationContext();
        this.b = fuVar;
        this.g = frVar;
    }

    public fp a(@NonNull fn fnVar, @NonNull eg egVar) {
        fp fpVar;
        synchronized (this.f4841a) {
            fpVar = (fp) this.c.get(fnVar);
            if (fpVar == null) {
                fpVar = this.g.a(fnVar).a(this.e, this.b, fnVar, egVar);
                this.c.put(fnVar, fpVar);
                this.d.a(new a(fnVar), fnVar);
                this.f++;
            }
        }
        return fpVar;
    }

    public void a(@NonNull String str, int i, String str2) {
        a(str, Integer.valueOf(i), str2);
    }

    private void a(@NonNull String str, @Nullable Integer num, @Nullable String str2) {
        synchronized (this.f4841a) {
            Collection<fn> b2 = this.d.b(new a(str, num, str2));
            if (!cx.a((Collection) b2)) {
                this.f -= b2.size();
                ArrayList arrayList = new ArrayList(b2.size());
                for (fn remove : b2) {
                    arrayList.add(this.c.remove(remove));
                }
                Iterator it = arrayList.iterator();
                while (it.hasNext()) {
                    ((fp) it.next()).a();
                }
            }
        }
    }

    public int a() {
        return this.f;
    }
}
