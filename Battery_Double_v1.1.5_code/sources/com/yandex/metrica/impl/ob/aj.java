package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public enum aj {
    UNKNOWN(0),
    FIRST_OCCURRENCE(1),
    NON_FIRST_OCCURENCE(2);
    
    public final int d;

    private aj(int i) {
        this.d = i;
    }

    @NonNull
    public static aj a(@Nullable Integer num) {
        aj[] values;
        if (num != null) {
            for (aj ajVar : values()) {
                if (ajVar.d == num.intValue()) {
                    return ajVar;
                }
            }
        }
        return UNKNOWN;
    }
}
