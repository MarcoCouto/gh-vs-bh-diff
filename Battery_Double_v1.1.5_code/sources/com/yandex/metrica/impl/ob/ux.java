package com.yandex.metrica.impl.ob;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.telephony.CellInfo;
import android.telephony.PhoneStateListener;
import android.telephony.SignalStrength;
import android.telephony.TelephonyManager;
import com.facebook.places.model.PlaceFields;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

class ux extends ut implements p {
    /* access modifiers changed from: private */
    @Nullable

    /* renamed from: a reason: collision with root package name */
    public final TelephonyManager f5251a;
    /* access modifiers changed from: private */
    public PhoneStateListener b;
    /* access modifiers changed from: private */
    public boolean c;
    private uk d;
    private final com.yandex.metrica.impl.ob.p.a<vf> e;
    private final com.yandex.metrica.impl.ob.p.a<uu[]> f;
    @NonNull
    private final xh g;
    private final Context h;
    private final uw i;
    private final vc j;
    private final uz k;
    @NonNull
    private final px l;
    @NonNull
    private pr m;

    private class a extends PhoneStateListener {
        private a() {
        }

        public void onSignalStrengthsChanged(SignalStrength signalStrength) {
            super.onSignalStrengthsChanged(signalStrength);
            ux.this.c(signalStrength);
        }
    }

    protected ux(@NonNull Context context, @NonNull xh xhVar) {
        this(context, new px(), xhVar);
    }

    protected ux(@NonNull Context context, @NonNull px pxVar, @NonNull xh xhVar) {
        this(context, pxVar, new pr(pxVar.a()), xhVar);
    }

    protected ux(@NonNull Context context, @NonNull px pxVar, @NonNull pr prVar, @NonNull xh xhVar) {
        TelephonyManager telephonyManager;
        this.c = false;
        this.e = new com.yandex.metrica.impl.ob.p.a<>();
        this.f = new com.yandex.metrica.impl.ob.p.a<>();
        this.h = context;
        try {
            telephonyManager = (TelephonyManager) context.getSystemService(PlaceFields.PHONE);
        } catch (Throwable unused) {
            telephonyManager = null;
        }
        this.f5251a = telephonyManager;
        this.g = xhVar;
        this.g.a((Runnable) new Runnable() {
            public void run() {
                ux.this.b = new a();
            }
        });
        this.i = new uw(this, prVar);
        this.j = new vc(this, prVar);
        this.k = new uz(this, prVar);
        this.l = pxVar;
        this.m = prVar;
    }

    public synchronized void a() {
        this.g.a((Runnable) new Runnable() {
            public void run() {
                if (!ux.this.c) {
                    ux.this.c = true;
                    if (ux.this.b != null && ux.this.f5251a != null) {
                        try {
                            ux.this.f5251a.listen(ux.this.b, 256);
                        } catch (Throwable unused) {
                        }
                    }
                }
            }
        });
    }

    public synchronized void b() {
        this.g.a((Runnable) new Runnable() {
            public void run() {
                if (ux.this.c) {
                    ux.this.c = false;
                    dr.a().a((Object) ux.this);
                    if (ux.this.b != null && ux.this.f5251a != null) {
                        try {
                            ux.this.f5251a.listen(ux.this.b, 0);
                        } catch (Throwable unused) {
                        }
                    }
                }
            }
        });
    }

    public synchronized void a(vg vgVar) {
        if (vgVar != null) {
            vgVar.a(e());
        }
    }

    public synchronized void a(uv uvVar) {
        if (uvVar != null) {
            uvVar.a(j());
        }
    }

    @Nullable
    public TelephonyManager c() {
        return this.f5251a;
    }

    public Context d() {
        return this.h;
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public synchronized vf e() {
        vf vfVar;
        if (!this.e.b()) {
            if (!this.e.c()) {
                vfVar = (vf) this.e.a();
            }
        }
        vfVar = new vf(this.i, this.j, this.k);
        uu b2 = vfVar.b();
        if (b2 != null && b2.a() == null && !this.e.b()) {
            uu b3 = ((vf) this.e.a()).b();
            if (b3 != null) {
                vfVar.b().a(b3.a());
            }
        }
        this.e.a(vfVar);
        return vfVar;
    }

    private synchronized uu[] j() {
        uu[] uuVarArr;
        if (!this.f.b()) {
            if (!this.f.c()) {
                uuVarArr = (uu[]) this.f.a();
            }
        }
        uuVarArr = f();
        this.f.a(uuVarArr);
        return uuVarArr;
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    @SuppressLint({"MissingPermission"})
    @NonNull
    public uu[] f() {
        ArrayList arrayList = new ArrayList();
        if (cx.a(17) && this.m.a(this.h)) {
            try {
                List allCellInfo = this.f5251a.getAllCellInfo();
                if (!cx.a((Collection) allCellInfo)) {
                    for (int i2 = 0; i2 < allCellInfo.size(); i2++) {
                        uu a2 = a((CellInfo) allCellInfo.get(i2));
                        if (a2 != null) {
                            arrayList.add(a2);
                        }
                    }
                }
            } catch (Throwable unused) {
            }
        }
        if (arrayList.size() >= 1) {
            return (uu[]) arrayList.toArray(new uu[arrayList.size()]);
        }
        uu b2 = e().b();
        if (b2 == null) {
            return new uu[0];
        }
        return new uu[]{b2};
    }

    @Nullable
    @TargetApi(17)
    private uu a(CellInfo cellInfo) {
        return uu.a(cellInfo);
    }

    /* access modifiers changed from: private */
    public synchronized void c(SignalStrength signalStrength) {
        if (!this.e.b() && !this.e.c()) {
            uu b2 = ((vf) this.e.a()).b();
            if (b2 != null) {
                b2.a(Integer.valueOf(a(signalStrength)));
            }
        }
    }

    @VisibleForTesting
    static int a(SignalStrength signalStrength) {
        if (signalStrength.isGsm()) {
            return b(signalStrength);
        }
        int cdmaDbm = signalStrength.getCdmaDbm();
        int evdoDbm = signalStrength.getEvdoDbm();
        if (-120 == evdoDbm) {
            return cdmaDbm;
        }
        return -120 == cdmaDbm ? evdoDbm : Math.min(cdmaDbm, evdoDbm);
    }

    @VisibleForTesting
    static int b(SignalStrength signalStrength) {
        int gsmSignalStrength = signalStrength.getGsmSignalStrength();
        if (99 == gsmSignalStrength) {
            return -1;
        }
        return (gsmSignalStrength * 2) - 113;
    }

    private synchronized boolean k() {
        return this.d != null;
    }

    /* access modifiers changed from: 0000 */
    public synchronized boolean g() {
        return k() && this.d.o.o;
    }

    /* access modifiers changed from: 0000 */
    public synchronized boolean h() {
        return k() && this.d.o.n;
    }

    /* access modifiers changed from: 0000 */
    public synchronized boolean i() {
        return k() && this.d.o.m;
    }

    public void a(@NonNull uk ukVar) {
        this.d = ukVar;
        this.l.a(ukVar);
        this.m.a(this.l.a());
    }

    public void a(boolean z) {
        this.l.a(z);
        this.m.a(this.l.a());
    }
}
