package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import com.explorestack.iab.vast.tags.VastAttributes;
import org.json.JSONException;
import org.json.JSONObject;

public final class ob {

    /* renamed from: a reason: collision with root package name */
    private final String f5021a;
    private final int b;
    private final boolean c;

    public ob(@NonNull JSONObject jSONObject) throws JSONException {
        this.f5021a = jSONObject.getString("name");
        this.c = jSONObject.getBoolean(VastAttributes.REQUIRED);
        this.b = jSONObject.optInt("version", -1);
    }

    public ob(String str, int i, boolean z) {
        this.f5021a = str;
        this.b = i;
        this.c = z;
    }

    public ob(String str, boolean z) {
        this(str, -1, z);
    }

    public JSONObject a() throws JSONException {
        JSONObject put = new JSONObject().put("name", this.f5021a).put(VastAttributes.REQUIRED, this.c);
        if (this.b != -1) {
            put.put("version", this.b);
        }
        return put;
    }

    public boolean equals(Object obj) {
        boolean z = true;
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        ob obVar = (ob) obj;
        if (this.b != obVar.b || this.c != obVar.c) {
            return false;
        }
        if (this.f5021a != null) {
            z = this.f5021a.equals(obVar.f5021a);
        } else if (obVar.f5021a != null) {
            z = false;
        }
        return z;
    }

    public int hashCode() {
        return ((((this.f5021a != null ? this.f5021a.hashCode() : 0) * 31) + this.b) * 31) + (this.c ? 1 : 0);
    }
}
