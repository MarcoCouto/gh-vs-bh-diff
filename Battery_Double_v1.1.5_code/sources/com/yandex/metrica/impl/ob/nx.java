package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

public class nx {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final List<nv> f5019a;
    @NonNull
    private final ny b;
    private final AtomicBoolean c = new AtomicBoolean(true);

    public nx(@NonNull List<nv> list, @NonNull ny nyVar) {
        this.f5019a = list;
        this.b = nyVar;
    }

    public void a() {
        if (this.c.get()) {
            d();
        }
    }

    public void b() {
        this.c.set(true);
    }

    public void c() {
        this.c.set(false);
    }

    private void d() {
        if (this.f5019a.isEmpty()) {
            e();
            return;
        }
        boolean z = false;
        for (nv a2 : this.f5019a) {
            z |= a2.a();
        }
        if (z) {
            e();
        }
    }

    private void e() {
        this.b.g();
    }
}
