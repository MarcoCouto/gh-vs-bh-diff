package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;

public class jd {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final en f4899a;
    @NonNull
    private final jc b;
    @NonNull
    private final a c;
    @NonNull
    private final iw<iy> d;
    @NonNull
    private final iw<iy> e;
    @Nullable
    private ix f;
    @Nullable
    private b g;

    public interface a {
        void a(@NonNull w wVar, @NonNull je jeVar);
    }

    public enum b {
        EMPTY,
        BACKGROUND,
        FOREGROUND
    }

    public jd(@NonNull en enVar, @NonNull jc jcVar, @NonNull a aVar) {
        this(enVar, jcVar, aVar, new iv(enVar, jcVar), new iu(enVar, jcVar));
    }

    @VisibleForTesting
    public jd(@NonNull en enVar, @NonNull jc jcVar, @NonNull a aVar, @NonNull iw<iy> iwVar, @NonNull iw<iy> iwVar2) {
        this.g = null;
        this.f4899a = enVar;
        this.c = aVar;
        this.d = iwVar;
        this.e = iwVar2;
        this.b = jcVar;
    }

    public synchronized void a(@NonNull w wVar) {
        g(wVar);
        switch (this.g) {
            case FOREGROUND:
                if (!a(this.f, wVar)) {
                    this.f = f(wVar);
                    break;
                } else {
                    this.f.b(wVar.r());
                    break;
                }
            case BACKGROUND:
                c(this.f, wVar);
                this.f = f(wVar);
                break;
            case EMPTY:
                this.f = f(wVar);
                break;
        }
    }

    public synchronized void b(@NonNull w wVar) {
        c(wVar).a(false);
        if (this.g != b.EMPTY) {
            c(this.f, wVar);
        }
        this.g = b.EMPTY;
    }

    @NonNull
    public synchronized ix c(@NonNull w wVar) {
        g(wVar);
        if (this.g != b.EMPTY && !a(this.f, wVar)) {
            this.g = b.EMPTY;
            this.f = null;
        }
        switch (this.g) {
            case FOREGROUND:
                return this.f;
            case BACKGROUND:
                this.f.b(wVar.r());
                return this.f;
            default:
                this.f = i(wVar);
                return this.f;
        }
    }

    @NonNull
    public je d(@NonNull w wVar) {
        return a(c(wVar), wVar.r());
    }

    public synchronized long a() {
        return this.f == null ? 10000000000L : this.f.c() - 1;
    }

    @NonNull
    public je a(long j) {
        long a2 = this.b.a();
        this.f4899a.j().a(a2, jh.BACKGROUND, j);
        return new je().a(a2).a(jh.BACKGROUND).b(0).c(0);
    }

    @NonNull
    private ix f(@NonNull w wVar) {
        long r = wVar.r();
        ix a2 = this.d.a(new iy(r, wVar.s()));
        this.g = b.FOREGROUND;
        this.f4899a.C().a();
        this.c.a(w.c(wVar), a(a2, r));
        return a2;
    }

    private void g(@NonNull w wVar) {
        if (this.g == null) {
            ix a2 = this.d.a();
            if (a(a2, wVar)) {
                this.f = a2;
                this.g = b.FOREGROUND;
                return;
            }
            ix a3 = this.e.a();
            if (a(a3, wVar)) {
                this.f = a3;
                this.g = b.BACKGROUND;
                return;
            }
            this.f = null;
            this.g = b.EMPTY;
        }
    }

    @Nullable
    private ix h(@NonNull w wVar) {
        if (this.g != null) {
            return this.f;
        }
        ix a2 = this.d.a();
        if (!b(a2, wVar)) {
            return a2;
        }
        ix a3 = this.e.a();
        if (!b(a3, wVar)) {
            return a3;
        }
        return null;
    }

    private boolean a(@Nullable ix ixVar, @NonNull w wVar) {
        if (ixVar == null) {
            return false;
        }
        if (ixVar.a(wVar.r())) {
            return true;
        }
        c(ixVar, wVar);
        return false;
    }

    private boolean b(@Nullable ix ixVar, @NonNull w wVar) {
        if (ixVar == null) {
            return false;
        }
        return ixVar.a(wVar.r());
    }

    private void c(@NonNull ix ixVar, @Nullable w wVar) {
        if (ixVar.h()) {
            this.c.a(w.b(wVar), a(ixVar));
            ixVar.a(false);
        }
        ixVar.e();
    }

    @NonNull
    private ix i(@NonNull w wVar) {
        this.g = b.BACKGROUND;
        long r = wVar.r();
        ix a2 = this.e.a(new iy(r, wVar.s()));
        if (this.f4899a.u().d()) {
            this.c.a(w.c(wVar), a(a2, wVar.r()));
        } else if (wVar.g() == com.yandex.metrica.impl.ob.af.a.EVENT_TYPE_FIRST_ACTIVATION.a()) {
            this.c.a(wVar, a(a2, r));
            this.c.a(w.c(wVar), a(a2, r));
        }
        return a2;
    }

    @NonNull
    private je a(@NonNull ix ixVar) {
        return new je().a(ixVar.c()).a(ixVar.a()).b(ixVar.g()).c(ixVar.d());
    }

    @NonNull
    private je a(@NonNull ix ixVar, long j) {
        return new je().a(ixVar.c()).b(ixVar.g()).c(ixVar.c(j)).a(ixVar.a());
    }

    @NonNull
    public je e(@NonNull w wVar) {
        ix h = h(wVar);
        if (h != null) {
            return new je().a(h.c()).b(h.g()).c(h.f()).a(h.a());
        }
        return a(wVar.s());
    }
}
