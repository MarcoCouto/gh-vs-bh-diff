package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import java.io.File;

public class ez extends en {
    @NonNull
    private final si b;
    @NonNull
    private final a c;
    @NonNull
    private final cs d;
    @NonNull
    private final bj e;
    @NonNull
    private final ku f;
    @NonNull
    private final bk g;

    public class a implements com.yandex.metrica.impl.ob.si.a {
        public a() {
        }

        public boolean a(@NonNull sj sjVar) {
            ez.this.a(new w().a(sjVar.a()).a(com.yandex.metrica.impl.ob.af.a.EVENT_TYPE_SEND_REFERRER.a()));
            return true;
        }
    }

    public ez(@NonNull Context context, @NonNull uk ukVar, @NonNull bl blVar, @NonNull ek ekVar, @NonNull com.yandex.metrica.impl.ob.eg.a aVar, @NonNull si siVar, @NonNull cs csVar, @NonNull un unVar) {
        a aVar2 = new a();
        cw cwVar = new cw();
        ah ahVar = new ah();
        Context context2 = context;
        fa faVar = new fa(context, ekVar, aVar, unVar, ukVar, new ey(csVar), blVar, al.a().j().g(), cx.c(context2, ekVar.b()));
        this(context2, ekVar, aVar2, cwVar, ahVar, faVar, siVar, csVar, aVar.q);
    }

    @VisibleForTesting
    ez(@NonNull Context context, @NonNull ek ekVar, @NonNull a aVar, @NonNull cw cwVar, @NonNull ah ahVar, @NonNull fa faVar, @NonNull si siVar, @NonNull cs csVar, @Nullable Boolean bool) {
        super(context, ekVar, aVar, cwVar, faVar);
        this.b = siVar;
        gp f2 = f();
        f2.a(com.yandex.metrica.impl.ob.af.a.EVENT_TYPE_REGULAR, new ie(f2.a()));
        this.c = faVar.a(this);
        this.b.a((com.yandex.metrica.impl.ob.si.a) this.c);
        this.d = csVar;
        this.g = faVar.b(this);
        this.e = faVar.a(this.g, y());
        this.f = faVar.a(ahVar, (wm<File>) new wm<File>() {
            public void a(File file) {
                ez.this.a(file);
            }
        });
        if (Boolean.TRUE.equals(bool)) {
            this.f.a();
            this.e.a();
        }
    }

    public synchronized void a(@NonNull com.yandex.metrica.impl.ob.eg.a aVar) {
        super.a(aVar);
        D();
        this.d.a(aVar.m);
    }

    private void D() {
        this.f4823a.a(i().R()).q();
    }

    public void c() {
        this.f.b();
        super.c();
    }

    @NonNull
    public com.yandex.metrica.CounterConfiguration.a a() {
        return com.yandex.metrica.CounterConfiguration.a.MAIN;
    }

    /* access modifiers changed from: private */
    public void a(File file) {
        this.e.a(file.getAbsolutePath(), new wm<Boolean>() {
            public void a(Boolean bool) {
            }
        }, true);
    }
}
