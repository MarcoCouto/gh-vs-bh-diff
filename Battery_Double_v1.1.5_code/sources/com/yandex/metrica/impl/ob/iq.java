package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import com.yandex.metrica.impl.ob.rn.a;

class iq implements mq<ip, a> {
    iq() {
    }

    @NonNull
    /* renamed from: a */
    public a b(@NonNull ip ipVar) {
        a aVar = new a();
        aVar.e = new int[ipVar.c().size()];
        int i = 0;
        for (Integer intValue : ipVar.c()) {
            aVar.e[i] = intValue.intValue();
            i++;
        }
        aVar.d = ipVar.d();
        aVar.c = ipVar.e();
        aVar.b = ipVar.b();
        return aVar;
    }

    @NonNull
    public ip a(@NonNull a aVar) {
        return new ip(aVar.b, aVar.c, aVar.d, aVar.e);
    }
}
