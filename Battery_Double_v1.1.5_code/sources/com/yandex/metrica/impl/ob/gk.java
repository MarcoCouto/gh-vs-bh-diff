package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public class gk {
    @Nullable

    /* renamed from: a reason: collision with root package name */
    private Long f4852a;
    private int b;
    @NonNull
    private wh c;

    public static class a {

        /* renamed from: a reason: collision with root package name */
        public final long f4853a;
        public final long b;
        public final int c;

        public a(long j, long j2, int i) {
            this.f4853a = j;
            this.c = i;
            this.b = j2;
        }
    }

    public gk() {
        this(new wg());
    }

    public gk(@NonNull wh whVar) {
        this.c = whVar;
    }

    public a a() {
        if (this.f4852a == null) {
            this.f4852a = Long.valueOf(this.c.b());
        }
        a aVar = new a(this.f4852a.longValue(), this.f4852a.longValue(), this.b);
        this.b++;
        return aVar;
    }
}
