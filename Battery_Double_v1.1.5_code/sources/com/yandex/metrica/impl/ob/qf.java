package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.Nullable;
import com.yandex.metrica.impl.ob.i.a;

@Deprecated
public class qf extends qd {
    @Deprecated
    public static final qk d = new qk("APP_ENVIRONMENT");
    @Deprecated
    public static final qk e = new qk("APP_ENVIRONMENT_REVISION");
    private static final qk f = new qk("SESSION_SLEEP_START_");
    private static final qk g = new qk("SESSION_ID_");
    private static final qk h = new qk("SESSION_COUNTER_ID_");
    private static final qk i = new qk("SESSION_INIT_TIME_");
    private static final qk j = new qk("SESSION_ALIVE_TIME_");
    private static final qk k = new qk("SESSION_IS_ALIVE_REPORT_NEEDED_");
    private static final qk l = new qk("BG_SESSION_ID_");
    private static final qk m = new qk("BG_SESSION_SLEEP_START_");
    private static final qk n = new qk("BG_SESSION_COUNTER_ID_");
    private static final qk o = new qk("BG_SESSION_INIT_TIME_");
    private static final qk p = new qk("COLLECT_INSTALLED_APPS_");
    private static final qk q = new qk("IDENTITY_SEND_TIME_");
    private static final qk r = new qk("USER_INFO_");
    private static final qk s = new qk("REFERRER_");
    private static final qk t = new qk("APP_ENVIRONMENT_");
    private static final qk u = new qk("APP_ENVIRONMENT_REVISION_");
    private qk A = new qk(k.a(), i());
    private qk B = new qk(l.a(), i());
    private qk C = new qk(m.a(), i());
    private qk D = new qk(n.a(), i());
    private qk E = new qk(o.a(), i());
    private qk F = new qk(q.a(), i());
    private qk G = new qk(p.a(), i());
    private qk H = new qk(r.a(), i());
    private qk I = new qk(s.a(), i());
    private qk J = new qk(t.a(), i());
    private qk K = new qk(u.a(), i());
    private qk v = new qk(f.a(), i());
    private qk w = new qk(g.a(), i());
    private qk x = new qk(h.a(), i());
    private qk y = new qk(i.a(), i());
    private qk z = new qk(j.a(), i());

    /* access modifiers changed from: protected */
    public String f() {
        return "_boundentrypreferences";
    }

    public qf(Context context, String str) {
        super(context, str);
        a(-1);
        b(0);
        c(0);
    }

    public long a(long j2) {
        return a(this.y.b(), j2);
    }

    public long b(long j2) {
        return a(this.E.b(), j2);
    }

    public long c(long j2) {
        return a(this.F.b(), j2);
    }

    public long d(long j2) {
        return a(this.w.b(), j2);
    }

    public long e(long j2) {
        return a(this.B.b(), j2);
    }

    public long f(long j2) {
        return a(this.x.b(), j2);
    }

    private long a(String str, long j2) {
        return this.c.getLong(str, j2);
    }

    public long g(long j2) {
        return a(this.D.b(), j2);
    }

    @Nullable
    public a a() {
        synchronized (this) {
            if (!this.c.contains(this.J.b()) || !this.c.contains(this.K.b())) {
                return null;
            }
            a aVar = new a(this.c.getString(this.J.b(), "{}"), this.c.getLong(this.K.b(), 0));
            return aVar;
        }
    }

    public long h(long j2) {
        return a(this.v.b(), j2);
    }

    public long i(long j2) {
        return a(this.C.b(), j2);
    }

    public Boolean a(boolean z2) {
        return Boolean.valueOf(this.c.getBoolean(this.A.b(), z2));
    }

    public Boolean b() {
        switch (this.c.getInt(this.G.b(), -1)) {
            case 0:
                return Boolean.valueOf(false);
            case 1:
                return Boolean.valueOf(true);
            default:
                return null;
        }
    }

    public String a(String str) {
        return this.c.getString(this.H.b(), str);
    }

    public String b(String str) {
        return this.c.getString(this.I.b(), str);
    }

    public qf a(a aVar) {
        synchronized (this) {
            a(this.J.b(), aVar.f4882a);
            a(this.K.b(), Long.valueOf(aVar.b));
        }
        return this;
    }

    public qf c() {
        return (qf) h(this.I.b());
    }

    private void a(int i2) {
        ql.a(this.c, this.z.b(), i2);
    }

    private void b(int i2) {
        ql.a(this.c, this.v.b(), i2);
    }

    private void c(int i2) {
        ql.a(this.c, this.x.b(), i2);
    }

    public qf d() {
        return this.c.contains(this.G.b()) ? (qf) h(this.G.b()) : this;
    }

    public boolean e() {
        return this.c.contains(this.y.b()) || this.c.contains(this.z.b()) || this.c.contains(this.A.b()) || this.c.contains(this.v.b()) || this.c.contains(this.w.b()) || this.c.contains(this.x.b()) || this.c.contains(this.E.b()) || this.c.contains(this.C.b()) || this.c.contains(this.B.b()) || this.c.contains(this.D.b()) || this.c.contains(this.J.b()) || this.c.contains(this.H.b()) || this.c.contains(this.I.b()) || this.c.contains(this.F.b());
    }

    public void g() {
        this.c.edit().remove(this.E.b()).remove(this.D.b()).remove(this.B.b()).remove(this.C.b()).remove(this.y.b()).remove(this.x.b()).remove(this.w.b()).remove(this.v.b()).remove(this.A.b()).remove(this.z.b()).remove(this.H.b()).remove(this.J.b()).remove(this.K.b()).remove(this.I.b()).remove(this.F.b()).apply();
    }
}
