package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import com.yandex.metrica.CounterConfiguration;
import com.yandex.metrica.impl.ob.eg.a;

class fv implements fq, ft<ez> {
    fv() {
    }

    @NonNull
    public fp a(@NonNull Context context, @NonNull fu fuVar, @NonNull fn fnVar, @NonNull eg egVar) {
        return new fw(context, fuVar.a(new fb(fnVar.b(), fnVar.a()), egVar, new fd(this)));
    }

    @NonNull
    /* renamed from: a */
    public ez d(@NonNull Context context, @NonNull ek ekVar, @NonNull a aVar, @NonNull bl blVar, @NonNull ul ulVar) {
        ez ezVar = new ez(context, ulVar.e(), blVar, ekVar, aVar, al.a().c(), al.a().h(), new uo(ulVar));
        return ezVar;
    }

    @NonNull
    /* renamed from: b */
    public ge c(@NonNull Context context, @NonNull ek ekVar, @NonNull a aVar, @NonNull bl blVar, @NonNull ul ulVar) {
        ge geVar = new ge(context, ekVar, blVar, aVar, ulVar.e(), new uo(ulVar), CounterConfiguration.a.MAIN);
        return geVar;
    }
}
