package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;

public class rv extends rw implements an {
    /* access modifiers changed from: private */
    @NonNull
    public final an d = ((an) a());

    public rv(@NonNull xh xhVar, @NonNull Context context, @NonNull String str) {
        super(xhVar, context, str);
    }

    public void a(@NonNull final kl klVar) {
        this.b.a((Runnable) new Runnable() {
            public void run() {
                rv.this.d.a(klVar);
            }
        });
    }

    public void a(@NonNull final kg kgVar) {
        this.b.a((Runnable) new Runnable() {
            public void run() {
                rv.this.d.a(kgVar);
            }
        });
    }

    public void e() {
        this.b.a((Runnable) new Runnable() {
            public void run() {
                rv.this.d.e();
            }
        });
    }
}
