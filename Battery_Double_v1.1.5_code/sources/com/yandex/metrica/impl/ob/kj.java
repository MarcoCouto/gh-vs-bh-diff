package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.metrica.c;
import com.yandex.metrica.impl.ob.ki.a;

public class kj extends ki {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private cb f4940a;

    public kj(@NonNull cb cbVar, a aVar, @Nullable c cVar) {
        super(aVar, cVar);
        this.f4940a = cbVar;
    }

    /* access modifiers changed from: 0000 */
    public void b(@NonNull kl klVar) {
        this.f4940a.a().a(klVar);
    }
}
