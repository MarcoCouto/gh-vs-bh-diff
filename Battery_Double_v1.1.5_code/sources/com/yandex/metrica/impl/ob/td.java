package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import com.yandex.metrica.impl.ob.h.b;
import com.yandex.metrica.impl.ob.np.a;

public class td {
    /* access modifiers changed from: private */
    @NonNull

    /* renamed from: a reason: collision with root package name */
    public final tc f5184a;
    @NonNull
    private final mf<te> b;
    @NonNull
    private final cw c;
    @NonNull
    private final xh d;
    @NonNull
    private final b e;
    @NonNull
    private final h f;
    /* access modifiers changed from: private */
    @NonNull
    public final tb g;
    /* access modifiers changed from: private */
    public boolean h;
    @Nullable
    private ua i;
    private boolean j;
    private long k;
    private long l;
    private long m;
    private boolean n;
    private boolean o;
    /* access modifiers changed from: private */
    public boolean p;
    private final Object q;

    public td(@NonNull Context context, @NonNull xh xhVar) {
        this(context, new tc(context, null, xhVar), a.a(te.class).a(context), new cw(), xhVar, al.a().i());
    }

    @VisibleForTesting
    td(@NonNull Context context, @NonNull tc tcVar, @NonNull mf<te> mfVar, @NonNull cw cwVar, @NonNull xh xhVar, @NonNull h hVar) {
        this.p = false;
        this.q = new Object();
        this.f5184a = tcVar;
        this.b = mfVar;
        this.g = new tb(context, mfVar, new a() {
            public void a() {
                td.this.c();
                td.this.h = false;
            }
        });
        this.c = cwVar;
        this.d = xhVar;
        this.e = new b() {
            public void a() {
                td.this.p = true;
                td.this.f5184a.a(td.this.g);
            }
        };
        this.f = hVar;
    }

    public void a(@Nullable uk ukVar) {
        c();
        b(ukVar);
    }

    private void d() {
        if (this.o) {
            f();
        } else {
            g();
        }
    }

    private void e() {
        if (this.k - this.l >= this.i.b) {
            b();
        }
    }

    private void f() {
        if (this.c.b(this.m, this.i.d, "should retry sdk collecting")) {
            b();
        }
    }

    private void g() {
        if (this.c.b(this.m, this.i.f5226a, "should collect sdk as usual")) {
            b();
        }
    }

    public void a() {
        synchronized (this.q) {
            if (this.j && this.i != null) {
                if (this.n) {
                    d();
                } else {
                    e();
                }
            }
        }
    }

    public void b(@Nullable uk ukVar) {
        boolean c2 = c(ukVar);
        synchronized (this.q) {
            if (ukVar != null) {
                try {
                    this.j = ukVar.o.e;
                    this.i = ukVar.B;
                    this.k = ukVar.D;
                    this.l = ukVar.E;
                } catch (Throwable th) {
                    while (true) {
                        throw th;
                    }
                }
            }
            this.f5184a.a(ukVar);
        }
        if (c2) {
            a();
        }
    }

    /* access modifiers changed from: 0000 */
    public void b() {
        if (!this.h) {
            this.h = true;
            if (!this.p) {
                this.f.a(this.i.c, this.d, this.e);
            } else {
                this.f5184a.a(this.g);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void c() {
        te teVar = (te) this.b.a();
        this.m = teVar.c;
        this.n = teVar.d;
        this.o = teVar.e;
    }

    private boolean c(@Nullable uk ukVar) {
        boolean z = false;
        if (ukVar == null) {
            return false;
        }
        if ((!this.j && ukVar.o.e) || this.i == null || !this.i.equals(ukVar.B) || this.k != ukVar.D || this.l != ukVar.E || this.f5184a.b(ukVar)) {
            z = true;
        }
        return z;
    }
}
