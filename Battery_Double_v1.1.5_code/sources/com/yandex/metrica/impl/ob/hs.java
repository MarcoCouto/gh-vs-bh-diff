package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.text.TextUtils;

public class hs extends hd {
    public hs(en enVar) {
        super(enVar);
    }

    public boolean a(@NonNull w wVar) {
        if (b(wVar)) {
            a().a(wVar.l());
        }
        return false;
    }

    private boolean b(w wVar) {
        return !TextUtils.isEmpty(wVar.l()) && TextUtils.isEmpty(a().h());
    }
}
