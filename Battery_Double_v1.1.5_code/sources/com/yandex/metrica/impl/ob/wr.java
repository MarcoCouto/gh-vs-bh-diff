package com.yandex.metrica.impl.ob;

import android.annotation.SuppressLint;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class wr {

    /* renamed from: a reason: collision with root package name */
    private final String f5291a;
    private final byte[] b;
    private final byte[] c;

    public wr(String str, byte[] bArr, byte[] bArr2) {
        this.f5291a = str;
        this.b = bArr;
        this.c = bArr2;
    }

    @SuppressLint({"TrulyRandom"})
    public byte[] a(byte[] bArr) throws Throwable {
        SecretKeySpec secretKeySpec = new SecretKeySpec(this.b, "AES");
        Cipher instance = Cipher.getInstance(this.f5291a);
        instance.init(1, secretKeySpec, new IvParameterSpec(this.c));
        return instance.doFinal(bArr);
    }

    @SuppressLint({"TrulyRandom"})
    public byte[] b(byte[] bArr) throws Throwable {
        return a(bArr, 0, bArr.length);
    }

    public byte[] a(byte[] bArr, int i, int i2) throws Throwable {
        SecretKeySpec secretKeySpec = new SecretKeySpec(this.b, "AES");
        Cipher instance = Cipher.getInstance(this.f5291a);
        instance.init(2, secretKeySpec, new IvParameterSpec(this.c));
        return instance.doFinal(bArr, i, i2);
    }
}
