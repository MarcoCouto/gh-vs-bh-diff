package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.io.UnsupportedEncodingException;

public class og {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private Context f5025a;

    public og(@NonNull Context context) {
        this.f5025a = context;
    }

    @Nullable
    public String a(@NonNull os osVar) {
        String a2 = oy.a(osVar);
        if (a2 != null) {
            return a(a2);
        }
        return null;
    }

    @Nullable
    public os a(long j, @NonNull String str) {
        String b = b(str);
        if (b == null) {
            return null;
        }
        return oy.a(j, b);
    }

    @Nullable
    public String a(@NonNull on onVar) {
        String a2 = oy.a(onVar);
        if (a2 == null) {
            return null;
        }
        return a(a2);
    }

    @Nullable
    public on b(long j, @NonNull String str) {
        String b = b(str);
        if (b == null) {
            return null;
        }
        return oy.b(j, b);
    }

    @Nullable
    public String a(@NonNull String str) {
        try {
            return wc.a(this.f5025a, str);
        } catch (UnsupportedEncodingException unused) {
            return null;
        }
    }

    @Nullable
    public String b(@NonNull String str) {
        try {
            return wc.b(this.f5025a, str);
        } catch (UnsupportedEncodingException unused) {
            return null;
        }
    }
}
