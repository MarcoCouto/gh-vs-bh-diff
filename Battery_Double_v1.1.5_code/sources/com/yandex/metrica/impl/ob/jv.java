package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;

public class jv {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Context f4925a;
    @NonNull
    private final jt b;
    /* access modifiers changed from: private */
    @NonNull
    public final ju c;
    @NonNull
    private final a d;
    @NonNull
    private final jq e;

    public static class a {
        public oc a(@NonNull Context context) {
            return ((uk) com.yandex.metrica.impl.ob.np.a.a(uk.class).a(context).a()).q;
        }
    }

    public jv(@NonNull Context context, @NonNull xh xhVar, @NonNull jp jpVar) {
        this(context, xhVar, jpVar, new ju(context));
    }

    public void a() {
        a(this.d.a(this.f4925a));
    }

    private void a(@Nullable oc ocVar) {
        if (ocVar != null) {
            boolean z = ocVar.k;
            boolean z2 = ocVar.c;
            Long a2 = this.e.a(ocVar.d);
            if (!z || a2 == null || a2.longValue() <= 0) {
                b();
            } else {
                this.b.a(a2.longValue(), z2);
            }
        }
    }

    private void b() {
        this.b.a();
    }

    public void a(@Nullable final jw jwVar) {
        oc a2 = this.d.a(this.f4925a);
        if (a2 != null) {
            long j = a2.f5022a;
            if (j > 0) {
                this.c.a(this.f4925a.getPackageName());
                this.b.a(j, (com.yandex.metrica.impl.ob.jt.a) new com.yandex.metrica.impl.ob.jt.a() {
                    public void a() {
                        jv.this.c.a();
                        jv.this.b(jwVar);
                    }
                });
            } else {
                b(jwVar);
            }
        } else {
            b(jwVar);
        }
        a(a2);
    }

    /* access modifiers changed from: private */
    public void b(@Nullable jw jwVar) {
        if (jwVar != null) {
            jwVar.a();
        }
    }

    private jv(@NonNull Context context, @NonNull xh xhVar, @NonNull jp jpVar, @NonNull ju juVar) {
        this(context, new jt(xhVar, jpVar), juVar, new a(), new jq(context));
    }

    @VisibleForTesting
    jv(@NonNull Context context, @NonNull jt jtVar, @NonNull ju juVar, @NonNull a aVar, @NonNull jq jqVar) {
        this.f4925a = context;
        this.b = jtVar;
        this.c = juVar;
        this.d = aVar;
        this.e = jqVar;
    }
}
