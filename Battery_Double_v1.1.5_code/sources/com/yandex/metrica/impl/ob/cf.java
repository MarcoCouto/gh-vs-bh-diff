package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public abstract class cf<T> {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    protected final Context f4723a;
    @NonNull
    private final String b;
    @NonNull
    private final String c;

    /* access modifiers changed from: protected */
    @Nullable
    public abstract T b(int i);

    public cf(@NonNull Context context, @NonNull String str, @NonNull String str2) {
        this.f4723a = context;
        this.b = str;
        this.c = str2;
    }

    @Nullable
    public T a() {
        int identifier = this.f4723a.getResources().getIdentifier(this.b, this.c, this.f4723a.getPackageName());
        if (identifier != 0) {
            try {
                return b(identifier);
            } catch (Throwable unused) {
            }
        }
        return null;
    }
}
