package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class tf {
    @NonNull
    public List<th> a(@NonNull List<th> list) {
        ArrayList arrayList = new ArrayList();
        for (th thVar : list) {
            ArrayList arrayList2 = new ArrayList(thVar.b.size());
            for (String str : thVar.b) {
                if (ci.b(str)) {
                    arrayList2.add(str);
                }
            }
            if (!arrayList2.isEmpty()) {
                arrayList.add(new th(thVar.f5189a, arrayList2));
            }
        }
        return arrayList;
    }

    @NonNull
    public JSONObject b(@NonNull List<th> list) {
        JSONObject jSONObject = new JSONObject();
        for (th thVar : list) {
            try {
                jSONObject.put(thVar.f5189a, new JSONObject().put("classes", new JSONArray(thVar.b)));
            } catch (Throwable unused) {
            }
        }
        return jSONObject;
    }
}
