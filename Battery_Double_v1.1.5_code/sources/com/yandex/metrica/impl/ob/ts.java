package com.yandex.metrica.impl.ob;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.net.TrafficStats;
import android.net.Uri;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import com.facebook.share.internal.ShareConstants;
import com.yandex.metrica.MetricaService;
import com.yandex.metrica.impl.ob.ai.c;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class ts implements tr, Runnable {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public final ServiceConnection f5197a;
    private final Handler b;
    private HashMap<String, tp> c;
    /* access modifiers changed from: private */
    public final Context d;
    private boolean e;
    private ServerSocket f;
    /* access modifiers changed from: private */
    public final tn g;
    /* access modifiers changed from: private */
    public ub h;
    private xl i;
    private long j;
    private long k;
    @NonNull
    private final wh l;
    @NonNull
    private final wf m;
    @NonNull
    private final c n;

    public static class a {
        public ts a(@NonNull Context context) {
            return new ts(context);
        }
    }

    public ts(Context context) {
        this(context, al.a().g(), al.a().j().i(), new wg(), new wf());
    }

    @VisibleForTesting
    ts(@NonNull Context context, @NonNull ai aiVar, @NonNull xh xhVar, @NonNull wh whVar, @NonNull wf wfVar) {
        this.f5197a = new ServiceConnection() {
            public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            }

            public void onServiceDisconnected(ComponentName componentName) {
            }
        };
        this.b = new Handler(Looper.getMainLooper()) {
            public void handleMessage(Message message) {
                super.handleMessage(message);
                if (message.what == 100) {
                    ts.this.e();
                    try {
                        ts.this.d.unbindService(ts.this.f5197a);
                    } catch (Throwable unused) {
                        tl.a(ts.this.d).reportEvent("socket_unbind_has_thrown_exception");
                    }
                }
            }
        };
        this.c = new HashMap<String, tp>() {
            {
                put(TtmlNode.TAG_P, new tp() {
                    @NonNull
                    public to a(@NonNull Socket socket, @NonNull Uri uri) {
                        tm tmVar = new tm(socket, uri, ts.this, ts.this.h, ts.this.g);
                        return tmVar;
                    }
                });
            }
        };
        this.g = new tn();
        this.d = context;
        this.l = whVar;
        this.m = wfVar;
        this.n = aiVar.a(new Runnable() {
            public void run() {
                ts.this.h();
            }
        }, xhVar);
        g();
    }

    private void g() {
        dr.a().a(this, ec.class, dv.a((du<T>) new du<ec>() {
            public void a(ec ecVar) {
                ts.this.g.a(ecVar.f4806a);
            }
        }).a(new ds<ec>() {
            public boolean a(ec ecVar) {
                return !ts.this.d.getPackageName().equals(ecVar.b);
            }
        }).a());
        dr.a().a(this, dy.class, dv.a((du<T>) new du<dy>() {
            public void a(dy dyVar) {
                ts.this.g.b(dyVar.f4801a);
            }
        }).a());
        dr.a().a(this, dw.class, dv.a((du<T>) new du<dw>() {
            public void a(dw dwVar) {
                ts.this.g.c(dwVar.f4799a);
            }
        }).a());
        dr.a().a(this, dx.class, dv.a((du<T>) new du<dx>() {
            public void a(dx dxVar) {
                ts.this.g.d(dxVar.f4800a);
            }
        }).a());
        dr.a().a(this, ea.class, dv.a((du<T>) new du<ea>() {
            public void a(ea eaVar) {
                ts.this.a(eaVar.f4804a);
                ts.this.c();
            }
        }).a());
    }

    public void a() {
        if (this.e) {
            b();
            this.b.sendMessageDelayed(this.b.obtainMessage(100), TimeUnit.SECONDS.toMillis(this.h.f5227a));
            this.k = this.l.a();
        }
    }

    public void b() {
        this.b.removeMessages(100);
        this.k = 0;
    }

    public synchronized void c() {
        if (!(this.e || this.h == null || !this.n.a(this.h.e))) {
            this.e = true;
        }
    }

    /* access modifiers changed from: private */
    public void h() {
        d();
        this.i = al.a().j().a(this);
        this.i.start();
        this.j = this.l.a();
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public void a(ub ubVar) {
        this.h = ubVar;
        if (this.h != null) {
            this.n.a(this.h.d);
        }
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public void d() {
        Intent intent = new Intent(this.d, MetricaService.class);
        intent.setAction("com.yandex.metrica.ACTION_BIND_TO_LOCAL_SERVER");
        try {
            if (!this.d.bindService(intent, this.f5197a, 1)) {
                tl.a(this.d).reportEvent("socket_bind_has_failed");
            }
        } catch (Throwable unused) {
            tl.a(this.d).reportEvent("socket_bind_has_thrown_exception");
        }
    }

    public synchronized void e() {
        try {
            this.e = false;
            if (this.i != null) {
                this.i.b();
                this.i = null;
            }
            if (this.f != null) {
                this.f.close();
                this.f = null;
            }
        } catch (IOException unused) {
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:28:0x003f A[SYNTHETIC, Splitter:B:28:0x003f] */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x0018 A[SYNTHETIC] */
    public void run() {
        ServerSocket serverSocket;
        Socket socket;
        this.f = f();
        if (cx.a(26)) {
            TrafficStats.setThreadStatsTag(40230);
        }
        if (this.f != null) {
            while (this.e) {
                synchronized (this) {
                    serverSocket = this.f;
                }
                if (serverSocket != null) {
                    try {
                        socket = serverSocket.accept();
                        try {
                            if (cx.a(26)) {
                                TrafficStats.tagSocket(socket);
                            }
                            a(socket);
                            if (socket == null) {
                            }
                        } catch (Throwable th) {
                            th = th;
                            if (socket != null) {
                                try {
                                    socket.close();
                                } catch (IOException unused) {
                                }
                            }
                            throw th;
                        }
                    } catch (Throwable th2) {
                        th = th2;
                        socket = null;
                        if (socket != null) {
                        }
                        throw th;
                    }
                    try {
                        socket.close();
                    } catch (IOException unused2) {
                    }
                }
            }
        }
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public ServerSocket f() {
        Iterator it = this.h.c.iterator();
        ServerSocket serverSocket = null;
        Integer num = null;
        while (serverSocket == null && it.hasNext()) {
            try {
                Integer num2 = (Integer) it.next();
                if (num2 != null) {
                    try {
                        serverSocket = b(num2.intValue());
                    } catch (SocketException unused) {
                        num = num2;
                        a("port_already_in_use", num.intValue());
                    } catch (IOException unused2) {
                    }
                }
                num = num2;
            } catch (SocketException unused3) {
                a("port_already_in_use", num.intValue());
            } catch (IOException unused4) {
            }
        }
        return serverSocket;
    }

    /* access modifiers changed from: 0000 */
    public ServerSocket b(int i2) throws IOException {
        return new ServerSocket(i2);
    }

    private Map<String, Object> c(int i2) {
        HashMap hashMap = new HashMap();
        hashMap.put("port", String.valueOf(i2));
        return hashMap;
    }

    private Map<String, Object> d(int i2) {
        Map<String, Object> c2 = c(i2);
        c2.put("idle_interval", Double.valueOf(a(this.j)));
        c2.put("background_interval", Double.valueOf(a(this.k)));
        return c2;
    }

    private double a(long j2) {
        long j3 = 0;
        if (j2 != 0) {
            j3 = this.m.e(j2, TimeUnit.MILLISECONDS);
        }
        return (double) j3;
    }

    private void a(@NonNull Socket socket) {
        new tq(socket, this, this.c).a();
    }

    public void a(@NonNull String str, String str2) {
        HashMap hashMap = new HashMap();
        hashMap.put(ShareConstants.MEDIA_URI, str2);
        an a2 = tl.a(this.d);
        StringBuilder sb = new StringBuilder();
        sb.append("socket_");
        sb.append(str);
        a2.reportEvent(sb.toString(), (Map<String, Object>) hashMap);
    }

    public void a(@NonNull String str) {
        tl.a(this.d).reportEvent(b(str));
    }

    public void a(@NonNull String str, Throwable th) {
        tl.a(this.d).reportError(b(str), th);
    }

    public void a(@NonNull String str, int i2) {
        tl.a(this.d).reportEvent(b(str), c(i2));
    }

    public void a(int i2) {
        tl.a(this.d).reportEvent(b("sync_succeed"), d(i2));
    }

    private String b(@NonNull String str) {
        StringBuilder sb = new StringBuilder();
        sb.append("socket_");
        sb.append(str);
        return sb.toString();
    }
}
