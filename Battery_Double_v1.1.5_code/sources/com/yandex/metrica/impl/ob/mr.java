package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import com.yandex.metrica.impl.ob.rr.a.c;
import com.yandex.metrica.impl.ob.tv.a;

public class mr implements mv<tv, c> {
    @NonNull
    /* renamed from: a */
    public c b(@NonNull tv tvVar) {
        c cVar = new c();
        cVar.e = tvVar.d;
        cVar.d = tvVar.c;
        cVar.c = tvVar.b;
        cVar.b = tvVar.f5221a;
        cVar.o = tvVar.e;
        cVar.q = tvVar.f;
        cVar.f = tvVar.g;
        cVar.g = tvVar.h;
        cVar.k = tvVar.m;
        cVar.j = tvVar.k;
        cVar.m = tvVar.o;
        cVar.l = tvVar.n;
        cVar.h = tvVar.i;
        cVar.i = tvVar.j;
        return cVar;
    }

    @NonNull
    public tv a(@NonNull c cVar) {
        return new a().a(cVar.b).n(cVar.m).m(cVar.l).l(cVar.k).k(cVar.j).j(cVar.i).i(cVar.h).h(cVar.g).g(cVar.f).d(cVar.e).e(cVar.o).f(cVar.q).c(cVar.d).b(cVar.c).a();
    }
}
