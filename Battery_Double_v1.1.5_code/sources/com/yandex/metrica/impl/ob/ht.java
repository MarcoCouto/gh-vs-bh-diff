package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.text.TextUtils;

public class ht extends hd {
    public ht(en enVar) {
        super(enVar);
    }

    public boolean a(@NonNull w wVar) {
        String B = a().B();
        String p = wVar.p();
        a().b(p);
        if (!TextUtils.equals(B, p)) {
            a().a(r.c());
        }
        return false;
    }
}
