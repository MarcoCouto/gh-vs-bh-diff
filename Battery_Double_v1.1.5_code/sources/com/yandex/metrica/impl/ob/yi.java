package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;

public final class yi {

    /* renamed from: a reason: collision with root package name */
    private final Class<? extends yk> f5318a;
    private final boolean b;
    private final String c;

    private yi(@NonNull yk<?> ykVar, boolean z, @NonNull String str) {
        this.f5318a = ykVar.getClass();
        this.b = z;
        this.c = str;
    }

    public final boolean a() {
        return this.b;
    }

    @NonNull
    public final String b() {
        return this.c;
    }

    public static final yi a(@NonNull yk<?> ykVar) {
        return new yi(ykVar, true, "");
    }

    public static final yi a(@NonNull yk<?> ykVar, @NonNull String str) {
        return new yi(ykVar, false, str);
    }
}
