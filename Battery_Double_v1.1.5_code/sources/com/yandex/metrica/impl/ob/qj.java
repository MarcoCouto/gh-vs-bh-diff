package com.yandex.metrica.impl.ob;

import android.content.Context;

@Deprecated
public class qj extends qd {
    private qk d;

    /* access modifiers changed from: protected */
    public String f() {
        return "_serviceproviderspreferences";
    }

    public qj(Context context) {
        this(context, null);
    }

    public qj(Context context, String str) {
        super(context, str);
        this.d = new qk("LOCATION_TRACKING_ENABLED");
    }

    public boolean a() {
        return this.c.getBoolean(this.d.b(), false);
    }

    public void b() {
        h(this.d.b()).j();
    }
}
