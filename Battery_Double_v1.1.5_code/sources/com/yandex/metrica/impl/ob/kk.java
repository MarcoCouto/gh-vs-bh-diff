package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.util.Collections;
import java.util.List;

public class kk {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    public final String f4941a;
    public final int b;
    public final long c;
    @NonNull
    public final String d;
    @Nullable
    public final Integer e;
    @NonNull
    public final List<StackTraceElement> f;

    public kk(@NonNull String str, int i, long j, @NonNull String str2, @Nullable Integer num, @Nullable List<StackTraceElement> list) {
        this.f4941a = str;
        this.b = i;
        this.c = j;
        this.d = str2;
        this.e = num;
        this.f = list == null ? Collections.emptyList() : Collections.unmodifiableList(list);
    }
}
