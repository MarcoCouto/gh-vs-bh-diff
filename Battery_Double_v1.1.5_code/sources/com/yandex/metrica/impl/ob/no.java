package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import com.yandex.metrica.impl.ob.rr.a;

public class no {

    /* renamed from: a reason: collision with root package name */
    private wq f5006a;

    public no(@NonNull Context context) {
        this(new wq(context));
    }

    @VisibleForTesting
    no(wq wqVar) {
        this.f5006a = wqVar;
    }

    public me<a> a() {
        return new mc(new mj(), new wr("AES/CBC/PKCS5Padding", this.f5006a.a(), this.f5006a.b()));
    }

    @NonNull
    public me<ro.a> b() {
        return new mc(new md(), new wr("AES/CBC/PKCS5Padding", this.f5006a.a(), this.f5006a.b()));
    }

    @NonNull
    public me<rm.a> c() {
        return new mc(new ma(), new wr("AES/CBC/PKCS5Padding", this.f5006a.a(), this.f5006a.b()));
    }

    @NonNull
    public me<rq.a> d() {
        return new mc(new mi(), new wr("AES/CBC/PKCS5Padding", this.f5006a.a(), this.f5006a.b()));
    }

    public me<rp.a> e() {
        return new mc(new mh(), new wr("AES/CBC/PKCS5Padding", this.f5006a.a(), this.f5006a.b()));
    }
}
