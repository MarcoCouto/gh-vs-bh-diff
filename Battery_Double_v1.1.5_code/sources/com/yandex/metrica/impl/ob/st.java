package com.yandex.metrica.impl.ob;

import android.location.Location;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import java.util.Collection;
import java.util.List;
import java.util.Map;

public class st extends sq {

    /* renamed from: a reason: collision with root package name */
    private boolean f5170a;
    private Location b;
    private boolean c;
    private boolean d;
    private boolean e;
    private int f;
    private int g;
    private boolean h;
    private int i;
    private Boolean j;
    private d k;
    private String l;
    private boolean m;
    private boolean n;
    private boolean o;
    private boolean p;
    private String q;
    private List<String> r;
    private boolean s;
    private int t;
    private long u;
    private long v;
    private boolean w;
    private long x;
    @Nullable
    private List<String> y;

    public static final class a extends com.yandex.metrica.impl.ob.sn.a<com.yandex.metrica.impl.ob.eg.a, a> {
        @Nullable

        /* renamed from: a reason: collision with root package name */
        public final String f5171a;
        @Nullable
        public final Location b;
        public final boolean f;
        public final boolean g;
        public final boolean h;
        public final int i;
        public final int j;
        public final int k;
        public final boolean l;
        public final boolean m;
        public final boolean n;
        @Nullable
        public final Map<String, String> o;
        public final int p;

        /* access modifiers changed from: 0000 */
        public boolean a(@Nullable Location location, @Nullable Location location2) {
            boolean z = true;
            if (location == location2) {
                return true;
            }
            if (location == null || location2 == null || location.getTime() != location2.getTime()) {
                return false;
            }
            if ((cx.a(17) && location.getElapsedRealtimeNanos() != location2.getElapsedRealtimeNanos()) || Double.compare(location2.getLatitude(), location.getLatitude()) != 0 || Double.compare(location2.getLongitude(), location.getLongitude()) != 0 || Double.compare(location2.getAltitude(), location.getAltitude()) != 0 || Float.compare(location2.getSpeed(), location.getSpeed()) != 0 || Float.compare(location2.getBearing(), location.getBearing()) != 0 || Float.compare(location2.getAccuracy(), location.getAccuracy()) != 0) {
                return false;
            }
            if (cx.a(26) && (Float.compare(location2.getVerticalAccuracyMeters(), location.getVerticalAccuracyMeters()) != 0 || Float.compare(location2.getSpeedAccuracyMetersPerSecond(), location.getSpeedAccuracyMetersPerSecond()) != 0 || Float.compare(location2.getBearingAccuracyDegrees(), location.getBearingAccuracyDegrees()) != 0)) {
                return false;
            }
            if (location.getProvider() == null ? location2.getProvider() != null : !location.getProvider().equals(location2.getProvider())) {
                return false;
            }
            if (location.getExtras() != null) {
                z = location.getExtras().equals(location2.getExtras());
            } else if (location2.getExtras() != null) {
                z = false;
            }
            return z;
        }

        public a(@NonNull com.yandex.metrica.impl.ob.eg.a aVar) {
            com.yandex.metrica.impl.ob.eg.a aVar2 = aVar;
            this(aVar2.f4810a, aVar2.b, aVar2.c, aVar2.d, aVar2.e, aVar2.f, aVar2.g, aVar2.h, aVar2.n, aVar2.i, aVar2.j, aVar2.k, aVar2.l, aVar2.m, aVar2.o, aVar2.p);
        }

        a(@Nullable String str, @Nullable String str2, @Nullable String str3, @Nullable String str4, @Nullable Boolean bool, @Nullable Location location, @Nullable Boolean bool2, @Nullable Boolean bool3, @Nullable Boolean bool4, @Nullable Integer num, @Nullable Integer num2, @Nullable Integer num3, @Nullable Boolean bool5, @Nullable Boolean bool6, @Nullable Map<String, String> map, @Nullable Integer num4) {
            super(str, str2, str3);
            this.f5171a = str4;
            Boolean bool7 = bool;
            this.f = wk.a(bool, true);
            this.b = location;
            Boolean bool8 = bool2;
            this.g = wk.a(bool2, false);
            Boolean bool9 = bool3;
            this.h = wk.a(bool3, false);
            Boolean bool10 = bool4;
            this.n = wk.a(bool4, false);
            Integer num5 = num;
            this.i = Math.max(10, wk.a(num, 10));
            this.j = wk.a(num2, 7);
            this.k = wk.a(num3, 90);
            this.l = wk.a(bool5, false);
            this.m = wk.a(bool6, true);
            this.o = map;
            this.p = wk.a(num4, 1000);
        }

        @NonNull
        /* renamed from: a */
        public a b(@NonNull com.yandex.metrica.impl.ob.eg.a aVar) {
            com.yandex.metrica.impl.ob.eg.a aVar2 = aVar;
            a aVar3 = new a((String) wk.a(aVar2.f4810a, this.c), (String) wk.a(aVar2.b, this.d), (String) wk.a(aVar2.c, this.e), (String) wk.a(aVar2.d, this.f5171a), (Boolean) wk.a(aVar2.e, Boolean.valueOf(this.f)), (Location) wk.a(aVar2.f, this.b), (Boolean) wk.a(aVar2.g, Boolean.valueOf(this.g)), (Boolean) wk.a(aVar2.h, Boolean.valueOf(this.h)), aVar2.n, (Integer) wk.a(aVar2.i, Integer.valueOf(this.i)), (Integer) wk.a(aVar2.j, Integer.valueOf(this.j)), (Integer) wk.a(aVar2.k, Integer.valueOf(this.k)), (Boolean) wk.a(aVar2.l, Boolean.valueOf(this.l)), (Boolean) wk.a(aVar2.m, Boolean.valueOf(this.m)), (Map) wk.a(aVar2.o, this.o), (Integer) wk.a(aVar2.p, Integer.valueOf(this.p)));
            return aVar3;
        }

        /* renamed from: b */
        public boolean a(@NonNull com.yandex.metrica.impl.ob.eg.a aVar) {
            boolean z = false;
            if (aVar.f4810a != null && !aVar.f4810a.equals(this.c)) {
                return false;
            }
            if (aVar.b != null && !aVar.b.equals(this.d)) {
                return false;
            }
            if (aVar.c != null && !aVar.c.equals(this.e)) {
                return false;
            }
            if (aVar.e != null && this.f != aVar.e.booleanValue()) {
                return false;
            }
            if (aVar.g != null && this.g != aVar.g.booleanValue()) {
                return false;
            }
            if (aVar.h != null && this.h != aVar.h.booleanValue()) {
                return false;
            }
            if (aVar.i != null && this.i != aVar.i.intValue()) {
                return false;
            }
            if (aVar.j != null && this.j != aVar.j.intValue()) {
                return false;
            }
            if (aVar.k != null && this.k != aVar.k.intValue()) {
                return false;
            }
            if (aVar.l != null && this.l != aVar.l.booleanValue()) {
                return false;
            }
            if (aVar.m != null && this.m != aVar.m.booleanValue()) {
                return false;
            }
            if (aVar.n != null && this.n != aVar.n.booleanValue()) {
                return false;
            }
            if (aVar.d != null && (this.f5171a == null || !this.f5171a.equals(aVar.d))) {
                return false;
            }
            if (aVar.o != null && (this.o == null || !this.o.equals(aVar.o))) {
                return false;
            }
            if (aVar.p != null && this.p != aVar.p.intValue()) {
                return false;
            }
            if (aVar.f == null || a(this.b, aVar.f)) {
                z = true;
            }
            return z;
        }
    }

    public static abstract class b implements d {
        @NonNull

        /* renamed from: a reason: collision with root package name */
        protected final cs f5172a;

        public b(@NonNull cs csVar) {
            this.f5172a = csVar;
        }

        public boolean a(@Nullable Boolean bool) {
            return wk.a(bool, true);
        }
    }

    public static class c extends a<st, a> {
        @NonNull
        private final en c;
        @NonNull
        private final d d;

        public c(@NonNull en enVar, @NonNull d dVar) {
            super(enVar.k(), enVar.b().b());
            this.c = enVar;
            this.d = dVar;
        }

        /* access modifiers changed from: protected */
        @NonNull
        /* renamed from: a */
        public st b() {
            return new st();
        }

        @NonNull
        /* renamed from: a */
        public st c(@NonNull com.yandex.metrica.impl.ob.sn.c<a> cVar) {
            st stVar = (st) super.c(cVar);
            stVar.m(((a) cVar.b).f5171a);
            stVar.k(this.c.x());
            stVar.d(this.c.p());
            stVar.b(this.c.A().a());
            stVar.e(((a) cVar.b).f);
            stVar.a(((a) cVar.b).b);
            stVar.f(((a) cVar.b).g);
            stVar.g(((a) cVar.b).h);
            stVar.a(((a) cVar.b).i);
            stVar.c(((a) cVar.b).j);
            stVar.b(((a) cVar.b).k);
            stVar.i(((a) cVar.b).l);
            stVar.h(((a) cVar.b).n);
            stVar.a(Boolean.valueOf(((a) cVar.b).m), this.d);
            stVar.c((long) ((a) cVar.b).p);
            a(stVar, cVar.f5164a, ((a) cVar.b).o);
            return stVar;
        }

        /* access modifiers changed from: 0000 */
        @VisibleForTesting
        public void a(@NonNull st stVar, @NonNull uk ukVar, @Nullable Map<String, String> map) {
            a(stVar, ukVar);
            b(stVar, ukVar);
            stVar.a(ukVar.m);
            stVar.j(a(map, we.a(ukVar.n)));
        }

        /* access modifiers changed from: 0000 */
        public boolean a(@Nullable Map<String, String> map, @Nullable Map<String, String> map2) {
            return map == null || map.isEmpty() || map.equals(map2);
        }

        /* access modifiers changed from: 0000 */
        public void a(st stVar, uk ukVar) {
            stVar.a(ukVar.e);
        }

        /* access modifiers changed from: 0000 */
        public void b(st stVar, uk ukVar) {
            stVar.a(ukVar.o.f5221a);
            stVar.b(ukVar.o.b);
            stVar.c(ukVar.o.c);
            if (ukVar.z != null) {
                stVar.a(ukVar.z.f5225a);
                stVar.b(ukVar.z.b);
            }
            stVar.d(ukVar.o.d);
        }
    }

    public interface d {
        boolean a(@Nullable Boolean bool);
    }

    @VisibleForTesting
    st() {
    }

    @NonNull
    public String a() {
        return wk.b(this.q, "");
    }

    /* access modifiers changed from: 0000 */
    public void a(String str) {
        this.q = str;
    }

    public void a(List<String> list) {
        this.r = list;
    }

    public List<String> b() {
        return this.r;
    }

    public String c() {
        return this.l;
    }

    public boolean F() {
        return this.m;
    }

    public void a(boolean z) {
        this.m = z;
    }

    public boolean G() {
        return this.n;
    }

    public boolean H() {
        return this.o;
    }

    public boolean I() {
        return this.p;
    }

    public void a(long j2) {
        this.u = j2;
    }

    public long J() {
        return this.u;
    }

    public void b(long j2) {
        this.v = j2;
    }

    public long K() {
        return this.v;
    }

    public void b(boolean z) {
        this.n = z;
    }

    public void c(boolean z) {
        this.o = z;
    }

    public void d(boolean z) {
        this.p = z;
    }

    public boolean L() {
        return f() && !cx.a((Collection) b()) && Z();
    }

    /* access modifiers changed from: private */
    public void m(String str) {
        this.l = str;
    }

    public boolean M() {
        return this.s;
    }

    /* access modifiers changed from: private */
    public void k(boolean z) {
        this.s = z;
    }

    public boolean N() {
        return this.f5170a;
    }

    public void e(boolean z) {
        this.f5170a = z;
    }

    public Location O() {
        return this.b;
    }

    public void a(Location location) {
        this.b = location;
    }

    public boolean P() {
        return this.c;
    }

    public void f(boolean z) {
        this.c = z;
    }

    public boolean Q() {
        return this.d;
    }

    public void g(boolean z) {
        this.d = z;
    }

    public boolean R() {
        return this.e;
    }

    public void h(boolean z) {
        this.e = z;
    }

    public int S() {
        return this.f;
    }

    public void a(int i2) {
        this.f = i2;
    }

    public int T() {
        return this.g;
    }

    public void b(int i2) {
        this.g = i2;
    }

    public void i(boolean z) {
        this.h = z;
    }

    public int U() {
        return this.i;
    }

    public void c(int i2) {
        this.i = i2;
    }

    public int V() {
        return this.t;
    }

    public void d(int i2) {
        this.t = i2;
    }

    public long W() {
        return this.x;
    }

    public void c(long j2) {
        this.x = j2;
    }

    public boolean X() {
        return this.k.a(this.j);
    }

    @Nullable
    public List<String> Y() {
        return this.y;
    }

    public void b(@NonNull List<String> list) {
        this.y = list;
    }

    public void a(@Nullable Boolean bool, @NonNull d dVar) {
        this.j = bool;
        this.k = dVar;
    }

    public boolean Z() {
        return this.w;
    }

    public void j(boolean z) {
        this.w = z;
    }
}
