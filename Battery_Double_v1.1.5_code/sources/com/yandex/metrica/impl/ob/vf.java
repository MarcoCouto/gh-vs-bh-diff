package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.util.List;

public final class vf {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final List<vb> f5271a;
    @Nullable
    private final uu b;
    @NonNull
    private final List<String> c;

    public vf(@NonNull ve<uu> veVar, @NonNull ve<List<vb>> veVar2, @NonNull ve<List<String>> veVar3) {
        this.b = (uu) veVar.d();
        this.f5271a = (List) veVar2.d();
        this.c = (List) veVar3.d();
    }

    @NonNull
    public List<vb> a() {
        return this.f5271a;
    }

    @Nullable
    public uu b() {
        return this.b;
    }

    @NonNull
    public List<String> c() {
        return this.c;
    }
}
