package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import java.util.Locale;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingQueue;

public class bl extends xl {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Executor f4692a;
    private Executor b;
    private final BlockingQueue<a> c = new LinkedBlockingQueue();
    private final Object d = new Object();
    private final Object e = new Object();
    private volatile a f;
    @NonNull
    private pi g;
    private String h;

    private static class a {
        @NonNull

        /* renamed from: a reason: collision with root package name */
        final bo f4693a;
        @NonNull
        private final String b;

        private a(@NonNull bo boVar) {
            this.f4693a = boVar;
            this.b = boVar.n();
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            return this.b.equals(((a) obj).b);
        }

        public int hashCode() {
            return this.b.hashCode();
        }
    }

    public bl(@NonNull Context context, @NonNull ek ekVar, @NonNull Executor executor) {
        this.f4692a = executor;
        this.b = new xd();
        this.h = String.format(Locale.US, "[%s:%s]", new Object[]{"NetworkTaskQueue", ekVar.toString()});
        this.g = new pi(context);
    }

    public void a(bo boVar) {
        synchronized (this.d) {
            a aVar = new a(boVar);
            if (!a(aVar)) {
                aVar.f4693a.C();
                this.c.offer(aVar);
            }
        }
    }

    public void a() {
        synchronized (this.e) {
            a aVar = this.f;
            if (aVar != null) {
                aVar.f4693a.w();
                aVar.f4693a.D();
            }
            while (!this.c.isEmpty()) {
                try {
                    ((a) this.c.take()).f4693a.D();
                } catch (InterruptedException unused) {
                }
            }
            b();
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:35:0x0043 A[SYNTHETIC] */
    public void run() {
        bo boVar = null;
        while (c()) {
            try {
                synchronized (this.e) {
                }
                this.f = (a) this.c.take();
                bo boVar2 = this.f.f4693a;
                try {
                    c(boVar2).execute(b(boVar2));
                    synchronized (this.e) {
                        this.f = null;
                        if (boVar2 != null) {
                            boVar2.D();
                        }
                    }
                    boVar = boVar2;
                } catch (InterruptedException unused) {
                    boVar = boVar2;
                    synchronized (this.e) {
                        this.f = null;
                        if (boVar != null) {
                            boVar.D();
                        }
                    }
                } catch (Throwable th) {
                    bo boVar3 = boVar2;
                    th = th;
                    boVar = boVar3;
                    synchronized (this.e) {
                        this.f = null;
                        if (boVar != null) {
                            boVar.D();
                        }
                    }
                    throw th;
                }
            } catch (InterruptedException unused2) {
            } catch (Throwable th2) {
                th = th2;
                synchronized (this.e) {
                }
                throw th;
            }
        }
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    @NonNull
    public br b(@NonNull bo boVar) {
        return new br(this.g, boVar, this, this.h);
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public Executor c(bo boVar) {
        if (boVar.o()) {
            return this.f4692a;
        }
        return this.b;
    }

    private boolean a(a aVar) {
        return this.c.contains(aVar) || aVar.equals(this.f);
    }
}
