package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;

public class wu implements xb {

    /* renamed from: a reason: collision with root package name */
    private wt f5294a;

    public wu() {
        this(new wt());
    }

    @Nullable
    public byte[] a(@Nullable byte[] bArr) {
        return this.f5294a.a(bArr);
    }

    @NonNull
    public xc a() {
        return xc.AES_RSA;
    }

    @VisibleForTesting
    wu(wt wtVar) {
        this.f5294a = wtVar;
    }
}
