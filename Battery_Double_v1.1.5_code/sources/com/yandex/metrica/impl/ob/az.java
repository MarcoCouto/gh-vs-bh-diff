package com.yandex.metrica.impl.ob;

import android.location.Location;
import android.text.TextUtils;
import com.ironsource.sdk.constants.LocationConst;
import com.tapjoy.TapjoyConstants;
import com.yandex.metrica.PreloadInfo;
import com.yandex.metrica.YandexMetricaConfig;
import com.yandex.metrica.YandexMetricaConfig.Builder;
import java.util.HashMap;
import java.util.Map.Entry;
import org.json.JSONException;
import org.json.JSONObject;

public class az {
    public String a(YandexMetricaConfig yandexMetricaConfig) {
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("apikey", yandexMetricaConfig.apiKey);
            jSONObject.put(TapjoyConstants.TJC_APP_VERSION_NAME, yandexMetricaConfig.appVersion);
            jSONObject.put("session_timeout", yandexMetricaConfig.sessionTimeout);
            jSONObject.put("location", a(yandexMetricaConfig.location));
            jSONObject.put("preload_info", a(yandexMetricaConfig.preloadInfo));
            jSONObject.put("collect_apps", yandexMetricaConfig.installedAppCollecting);
            jSONObject.put("logs", yandexMetricaConfig.logs);
            jSONObject.put("crash_enabled", yandexMetricaConfig.crashReporting);
            jSONObject.put("crash_native_enabled", yandexMetricaConfig.nativeCrashReporting);
            jSONObject.put("location_enabled", yandexMetricaConfig.locationTracking);
            jSONObject.put("max_reports_in_db_count", yandexMetricaConfig.maxReportsInDatabaseCount);
            return jSONObject.toString();
        } catch (Throwable unused) {
            return "";
        }
    }

    public YandexMetricaConfig a(String str) {
        if (!TextUtils.isEmpty(str)) {
            try {
                JSONObject jSONObject = new JSONObject(str);
                Builder newConfigBuilder = YandexMetricaConfig.newConfigBuilder(jSONObject.getString("apikey"));
                if (jSONObject.has(TapjoyConstants.TJC_APP_VERSION_NAME)) {
                    newConfigBuilder.withAppVersion(jSONObject.optString(TapjoyConstants.TJC_APP_VERSION_NAME));
                }
                if (jSONObject.has("session_timeout")) {
                    newConfigBuilder.withSessionTimeout(jSONObject.getInt("session_timeout"));
                }
                newConfigBuilder.withLocation(c(jSONObject.optString("location")));
                newConfigBuilder.withPreloadInfo(b(jSONObject.optString("preload_info")));
                if (jSONObject.has("collect_apps")) {
                    newConfigBuilder.withInstalledAppCollecting(jSONObject.optBoolean("collect_apps"));
                }
                if (jSONObject.has("logs") && jSONObject.optBoolean("logs")) {
                    newConfigBuilder.withLogs();
                }
                if (jSONObject.has("crash_enabled")) {
                    newConfigBuilder.withCrashReporting(jSONObject.optBoolean("crash_enabled"));
                }
                if (jSONObject.has("crash_native_enabled")) {
                    newConfigBuilder.withNativeCrashReporting(jSONObject.optBoolean("crash_native_enabled"));
                }
                if (jSONObject.has("location_enabled")) {
                    newConfigBuilder.withLocationTracking(jSONObject.optBoolean("location_enabled"));
                }
                if (jSONObject.has("max_reports_in_db_count")) {
                    newConfigBuilder.withMaxReportsInDatabaseCount(jSONObject.optInt("max_reports_in_db_count"));
                }
                return newConfigBuilder.build();
            } catch (Throwable unused) {
            }
        }
        return null;
    }

    private String a(PreloadInfo preloadInfo) {
        if (preloadInfo == null) {
            return null;
        }
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("trackid", preloadInfo.getTrackingId());
            jSONObject.put("params", vq.b(preloadInfo.getAdditionalParams()));
            return jSONObject.toString();
        } catch (Throwable unused) {
            return null;
        }
    }

    private PreloadInfo b(String str) throws JSONException {
        String str2 = null;
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        JSONObject jSONObject = new JSONObject(str);
        if (jSONObject.has("trackid")) {
            str2 = jSONObject.optString("trackid");
        }
        PreloadInfo.Builder newBuilder = PreloadInfo.newBuilder(str2);
        HashMap a2 = vq.a(jSONObject.optString("params"));
        if (a2 != null && a2.size() > 0) {
            for (Entry entry : a2.entrySet()) {
                newBuilder.setAdditionalParams((String) entry.getKey(), (String) entry.getValue());
            }
        }
        return newBuilder.build();
    }

    private String a(Location location) {
        if (location == null) {
            return null;
        }
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("provider", location.getProvider());
            jSONObject.put(LocationConst.TIME, location.getTime());
            jSONObject.put(LocationConst.ACCURACY, (double) location.getAccuracy());
            jSONObject.put("alt", location.getAltitude());
            jSONObject.put("lng", location.getLongitude());
            jSONObject.put("lat", location.getLatitude());
            return jSONObject.toString();
        } catch (Throwable unused) {
            return null;
        }
    }

    private Location c(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            JSONObject jSONObject = new JSONObject(str);
            Location location = new Location(jSONObject.has("provider") ? jSONObject.optString("provider") : null);
            location.setLongitude(jSONObject.getDouble("lng"));
            location.setLatitude(jSONObject.getDouble("lat"));
            location.setTime(jSONObject.optLong(LocationConst.TIME));
            location.setAccuracy((float) jSONObject.optDouble(LocationConst.ACCURACY));
            location.setAltitude((double) ((float) jSONObject.optDouble("alt")));
            return location;
        } catch (Throwable unused) {
            return null;
        }
    }
}
