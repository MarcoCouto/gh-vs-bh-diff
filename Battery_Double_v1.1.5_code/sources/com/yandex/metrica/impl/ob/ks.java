package com.yandex.metrica.impl.ob;

import android.annotation.TargetApi;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.metrica.impl.ob.rg.f;
import java.util.Arrays;

public class ks implements mq<Throwable, f> {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private kp f4947a = new kp();

    @NonNull
    /* renamed from: a */
    public f b(@NonNull Throwable th) {
        return a(th, 1, 0);
    }

    @NonNull
    private f a(@NonNull Throwable th, int i, int i2) {
        f fVar = new f();
        fVar.b = th.getClass().getName();
        fVar.c = wk.b(th.getMessage(), "");
        fVar.d = this.f4947a.b(Arrays.asList(cx.b(th)));
        if (th.getCause() != null && i2 < i) {
            fVar.e = a(th.getCause(), 30, i2 + 1);
        }
        if (!cx.a(19) || i2 >= i) {
            fVar.f = new f[0];
        } else {
            a(fVar, new Throwable[0], i2);
        }
        return fVar;
    }

    @NonNull
    public Throwable a(@NonNull f fVar) {
        throw new UnsupportedOperationException();
    }

    @TargetApi(19)
    private void a(@NonNull f fVar, @Nullable Throwable[] thArr, int i) {
        int i2 = 0;
        if (thArr == null) {
            fVar.f = new f[0];
            return;
        }
        fVar.f = new f[thArr.length];
        int length = thArr.length;
        int i3 = 0;
        while (i2 < length) {
            int i4 = i3 + 1;
            fVar.f[i3] = a(thArr[i2], 1, i + 1);
            i2++;
            i3 = i4;
        }
    }
}
