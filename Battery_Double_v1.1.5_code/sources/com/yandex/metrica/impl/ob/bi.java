package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.util.SparseArray;
import com.yandex.metrica.YandexMetrica;

public abstract class bi {

    interface a {
        void a(Context context);
    }

    /* access modifiers changed from: protected */
    public abstract int a(qg qgVar);

    /* access modifiers changed from: 0000 */
    public abstract SparseArray<a> a();

    /* access modifiers changed from: protected */
    public abstract void a(qg qgVar, int i);

    public void a(Context context) {
        qg qgVar = new qg(context);
        int a2 = a(qgVar);
        int b = b();
        if (a2 < b) {
            if (a2 > 0) {
                a(context, a2, b);
            }
            a(qgVar, b);
            qgVar.j();
        }
    }

    /* access modifiers changed from: 0000 */
    public int b() {
        return YandexMetrica.getLibraryApiLevel();
    }

    private void a(Context context, int i, int i2) {
        SparseArray a2 = a();
        while (true) {
            i++;
            if (i <= i2) {
                a aVar = (a) a2.get(i);
                if (aVar != null) {
                    aVar.a(context);
                }
            } else {
                return;
            }
        }
    }
}
