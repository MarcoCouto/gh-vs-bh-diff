package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class sh implements InvocationHandler {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private Object f5155a;
    @NonNull
    private final sk b;

    sh(@NonNull Object obj, @NonNull sk skVar) {
        this.f5155a = obj;
        this.b = skVar;
    }

    public Object invoke(Object obj, Method method, Object[] objArr) throws Throwable {
        if (!"onInstallReferrerSetupFinished".equals(method.getName())) {
            sk skVar = this.b;
            StringBuilder sb = new StringBuilder();
            sb.append("Unexpected method called ");
            sb.append(method.getName());
            skVar.a((Throwable) new IllegalArgumentException(sb.toString()));
        } else if (objArr.length != 1) {
            this.b.a((Throwable) new IllegalArgumentException("Args size is not equal to one."));
        } else if (objArr[0].equals(Integer.valueOf(0))) {
            try {
                Object invoke = this.f5155a.getClass().getMethod("getInstallReferrer", new Class[0]).invoke(this.f5155a, new Object[0]);
                sk skVar2 = this.b;
                sj sjVar = new sj((String) invoke.getClass().getMethod("getInstallReferrer", new Class[0]).invoke(invoke, new Object[0]), ((Long) invoke.getClass().getMethod("getReferrerClickTimestampSeconds", new Class[0]).invoke(invoke, new Object[0])).longValue(), ((Long) invoke.getClass().getMethod("getInstallBeginTimestampSeconds", new Class[0]).invoke(invoke, new Object[0])).longValue());
                skVar2.a(sjVar);
            } catch (Throwable th) {
                this.b.a(th);
            }
        } else {
            sk skVar3 = this.b;
            StringBuilder sb2 = new StringBuilder();
            sb2.append("Referrer check failed with error ");
            sb2.append(objArr[0]);
            skVar3.a((Throwable) new IllegalStateException(sb2.toString()));
        }
        return null;
    }
}
