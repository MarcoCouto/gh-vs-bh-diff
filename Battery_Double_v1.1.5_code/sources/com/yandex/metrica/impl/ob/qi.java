package com.yandex.metrica.impl.ob;

import android.content.Context;

@Deprecated
public class qi extends qd {
    static final qk d = new qk("PREF_KEY_DEVICE_ID_");
    static final qk e = new qk("PREF_KEY_UID_");
    static final qk f = new qk("STARTUP_CLIDS_MATCH_WITH_APP_CLIDS_");
    static final qk g = new qk("PREF_KEY_PINNING_UPDATE_URL");
    private static final qk h = new qk("PREF_KEY_HOST_URL_");
    private static final qk i = new qk("PREF_KEY_REPORT_URL_");
    private static final qk j = new qk("PREF_KEY_GET_AD_URL");
    private static final qk k = new qk("PREF_KEY_REPORT_AD_URL");
    private static final qk l = new qk("PREF_KEY_STARTUP_OBTAIN_TIME_");
    private static final qk m = new qk("PREF_KEY_STARTUP_ENCODED_CLIDS_");
    private static final qk n = new qk("PREF_KEY_DISTRIBUTION_REFERRER_");
    private static final qk o = new qk("PREF_KEY_EASY_COLLECTING_ENABLED_");
    private qk p;
    private qk q;
    private qk r;
    private qk s;
    private qk t;
    private qk u;
    private qk v;
    private qk w;
    private qk x;
    private qk y;

    /* access modifiers changed from: protected */
    public String f() {
        return "_startupserviceinfopreferences";
    }

    public qi(Context context) {
        this(context, null);
    }

    public qi(Context context, String str) {
        super(context, str);
        this.p = new qk(d.a());
        this.q = new qk(e.a(), i());
        this.r = new qk(h.a(), i());
        this.s = new qk(i.a(), i());
        this.t = new qk(j.a(), i());
        this.u = new qk(k.a(), i());
        this.v = new qk(l.a(), i());
        this.w = new qk(m.a(), i());
        this.x = new qk(n.a(), i());
        this.y = new qk(o.a(), i());
    }

    public long a(long j2) {
        return this.c.getLong(this.v.b(), j2);
    }

    public String a(String str) {
        return this.c.getString(this.p.b(), str);
    }

    public String b(String str) {
        return this.c.getString(this.q.b(), str);
    }

    public String c(String str) {
        return this.c.getString(this.r.b(), str);
    }

    public String d(String str) {
        return this.c.getString(this.w.b(), str);
    }

    public String e(String str) {
        return this.c.getString(this.s.b(), str);
    }

    public String f(String str) {
        return this.c.getString(this.t.b(), str);
    }

    public String g(String str) {
        return this.c.getString(this.u.b(), str);
    }

    public String a() {
        return this.c.getString(this.x.a(), null);
    }

    public qi i(String str) {
        return (qi) a(this.q.b(), str);
    }

    public qi j(String str) {
        return (qi) a(this.p.b(), str);
    }

    public static void a(Context context) {
        ql.a(context, "_startupserviceinfopreferences").edit().remove(d.a()).apply();
    }

    public void b() {
        h(this.p.b()).h(this.q.b()).h(this.r.b()).h(this.s.b()).h(this.t.b()).h(this.u.b()).h(this.v.b()).h(this.y.b()).h(this.w.b()).h(this.x.a()).h(f.a()).h(g.a()).j();
    }
}
