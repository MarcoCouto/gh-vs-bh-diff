package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import java.io.File;

public class ke implements Runnable {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Context f4935a;
    @NonNull
    private final File b;
    @NonNull
    private final wm<kv> c;

    public ke(@NonNull Context context, @NonNull File file, @NonNull wm<kv> wmVar) {
        this.f4935a = context;
        this.b = file;
        this.c = wmVar;
    }

    /* JADX WARNING: Missing exception handler attribute for start block: B:4:0x0013 */
    public void run() {
        if (this.b.exists()) {
            try {
                a(am.a(this.f4935a, this.b));
            } catch (Throwable unused) {
            }
            try {
                this.b.delete();
                return;
            } catch (Throwable unused2) {
                return;
            }
        } else {
            return;
        }
        throw th;
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public void a(@Nullable String str) {
        if (!TextUtils.isEmpty(str)) {
            try {
                this.c.a(new kv(str));
            } catch (Throwable unused) {
            }
        }
    }
}
