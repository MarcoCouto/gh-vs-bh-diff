package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public class rt extends yg<Void> {
    public rt(@NonNull final sa saVar) {
        super(new yk<Void>() {
            public yi a(@Nullable Void voidR) {
                if (sa.this.c()) {
                    return yi.a(this);
                }
                return yi.a(this, "YandexMetrica isn't initialized. Use YandexMetrica#activate(android.content.Context, String) method to activate.");
            }
        });
    }

    public yi a() {
        return super.a(null);
    }
}
