package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import java.util.concurrent.TimeUnit;

public abstract class it implements iw<iy> {

    /* renamed from: a reason: collision with root package name */
    private final en f4888a;
    @NonNull
    private final jc b;
    private final jg c;
    private final jb d;

    public it(@NonNull en enVar, @NonNull jc jcVar, @NonNull jg jgVar, @NonNull jb jbVar) {
        this.f4888a = enVar;
        this.b = jcVar;
        this.c = jgVar;
        this.d = jbVar;
    }

    @Nullable
    public final ix a() {
        if (this.c.i()) {
            return new ix(this.f4888a, this.c, b());
        }
        return null;
    }

    @NonNull
    public final ix a(@NonNull iy iyVar) {
        if (this.c.i()) {
            tl.a(this.f4888a.k()).reportEvent("create session with non-empty storage");
        }
        return new ix(this.f4888a, this.c, b(iyVar));
    }

    @NonNull
    private iz b(@NonNull iy iyVar) {
        long a2 = this.b.a();
        this.c.d(a2).b(TimeUnit.MILLISECONDS.toSeconds(iyVar.f4891a)).e(iyVar.f4891a).a(0).a(true).h();
        this.f4888a.j().a(a2, this.d.a(), TimeUnit.MILLISECONDS.toSeconds(iyVar.b));
        return b();
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    @NonNull
    public iz b() {
        return iz.a(this.d).a(this.c.g()).c(this.c.d()).b(this.c.c()).a(this.c.b()).d(this.c.e()).e(this.c.f()).a();
    }
}
