package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import java.util.Random;

public class vt {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Random f5276a;

    public vt() {
        this(new Random());
    }

    public vt(@NonNull Random random) {
        this.f5276a = random;
    }

    public long a(long j, long j2) {
        if (j < j2) {
            long nextLong = this.f5276a.nextLong();
            if (nextLong == Long.MIN_VALUE) {
                nextLong = 0;
            } else if (nextLong < 0) {
                nextLong = -nextLong;
            }
            return j + (nextLong % (j2 - j));
        }
        throw new IllegalArgumentException("min should be less than max");
    }
}
