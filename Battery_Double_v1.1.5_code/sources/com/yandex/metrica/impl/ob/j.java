package com.yandex.metrica.impl.ob;

import android.annotation.TargetApi;
import android.support.annotation.Nullable;
import com.yandex.metrica.impl.ob.l.a;

public class j {
    @Nullable
    public a a(int i) {
        if (cx.a(28)) {
            return b(i);
        }
        return null;
    }

    @Nullable
    @TargetApi(28)
    private a b(int i) {
        if (i == 10) {
            return a.ACTIVE;
        }
        if (i == 20) {
            return a.WORKING_SET;
        }
        if (i == 30) {
            return a.FREQUENT;
        }
        if (i != 40) {
            return null;
        }
        return a.RARE;
    }

    @Nullable
    public String a(@Nullable a aVar) {
        if (aVar == null) {
            return null;
        }
        switch (aVar) {
            case ACTIVE:
                return "ACTIVE";
            case WORKING_SET:
                return "WORKING_SET";
            case FREQUENT:
                return "FREQUENT";
            case RARE:
                return "RARE";
            default:
                return null;
        }
    }
}
