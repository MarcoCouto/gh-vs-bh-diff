package com.yandex.metrica.impl.ob;

import android.annotation.TargetApi;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;

@TargetApi(21)
public class de {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final tw f4778a;
    @NonNull
    private final dh b;
    @NonNull
    private final dc c;
    private Boolean d;

    public de(@NonNull Context context, @NonNull dh dhVar) {
        this(dhVar, new dc(context), new tw());
    }

    public void a(@NonNull Context context) {
        uk a2 = this.f4778a.a(context);
        tt ttVar = a2.G;
        if (ttVar != null && this.c.a(a2, ttVar)) {
            if (!this.c.b(a2, ttVar)) {
                this.b.a();
                this.d = Boolean.valueOf(false);
            } else if (vi.b(this.d)) {
                this.b.a(a2.G);
                this.d = Boolean.valueOf(true);
            }
        }
    }

    @VisibleForTesting
    public de(@NonNull dh dhVar, @NonNull dc dcVar, @NonNull tw twVar) {
        this.b = dhVar;
        this.c = dcVar;
        this.f4778a = twVar;
    }
}
