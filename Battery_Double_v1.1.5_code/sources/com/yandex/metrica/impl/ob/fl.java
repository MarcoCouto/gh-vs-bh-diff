package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;

public abstract class fl implements fp {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Context f4839a;
    @NonNull
    private final fc b;
    @NonNull
    private final ox c;

    /* access modifiers changed from: protected */
    public abstract void b(@NonNull w wVar, @NonNull eg egVar);

    public fl(@NonNull Context context, @NonNull fc fcVar) {
        this(context, fcVar, new ox(oo.a(context), al.a().k(), cy.a(context), new ly(ld.a(context).c())));
    }

    public void a(@NonNull w wVar, @NonNull eg egVar) {
        b(wVar, egVar);
    }

    public void a() {
        this.b.b(this);
        this.c.a((Object) this);
    }

    @NonNull
    public fc b() {
        return this.b;
    }

    @VisibleForTesting
    fl(@NonNull Context context, @NonNull fc fcVar, @NonNull ox oxVar) {
        this.f4839a = context.getApplicationContext();
        this.b = fcVar;
        this.c = oxVar;
        this.b.a((fp) this);
        this.c.b(this);
    }

    @NonNull
    public ox c() {
        return this.c;
    }
}
