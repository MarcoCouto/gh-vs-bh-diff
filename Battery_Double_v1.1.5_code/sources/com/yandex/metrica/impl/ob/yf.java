package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public class yf<T> implements yk<T> {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final String f5316a;

    public yf(@NonNull String str) {
        this.f5316a = str;
    }

    public yi a(@Nullable T t) {
        if (t != null) {
            return yi.a(this);
        }
        StringBuilder sb = new StringBuilder();
        sb.append(this.f5316a);
        sb.append(" is null.");
        return yi.a(this, sb.toString());
    }
}
