package com.yandex.metrica.impl.ob;

public class um {

    /* renamed from: a reason: collision with root package name */
    public final long f5239a;

    public um(long j) {
        this.f5239a = j;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("StatSending{disabledReportingInterval=");
        sb.append(this.f5239a);
        sb.append('}');
        return sb.toString();
    }

    public boolean equals(Object obj) {
        boolean z = true;
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        if (this.f5239a != ((um) obj).f5239a) {
            z = false;
        }
        return z;
    }

    public int hashCode() {
        return (int) (this.f5239a ^ (this.f5239a >>> 32));
    }
}
