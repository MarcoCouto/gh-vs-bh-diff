package com.yandex.metrica.impl.ob;

import android.location.Location;
import android.support.annotation.Nullable;

abstract class of implements oe {
    @Nullable

    /* renamed from: a reason: collision with root package name */
    private oe f5024a;

    public abstract void b(@Nullable String str, @Nullable Location location, @Nullable oh ohVar);

    public of(@Nullable oe oeVar) {
        this.f5024a = oeVar;
    }

    public void a(@Nullable String str, @Nullable Location location, @Nullable oh ohVar) {
        b(str, location, ohVar);
        if (this.f5024a != null) {
            this.f5024a.a(str, location, ohVar);
        }
    }
}
