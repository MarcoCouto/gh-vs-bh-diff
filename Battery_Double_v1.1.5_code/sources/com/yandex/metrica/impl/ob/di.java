package com.yandex.metrica.impl.ob;

import android.annotation.TargetApi;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanFilter.Builder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.yandex.metrica.impl.ob.tt.a;
import com.yandex.metrica.impl.ob.tt.a.C0130a;
import com.yandex.metrica.impl.ob.tt.a.b;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@TargetApi(21)
public class di {
    @NonNull
    public List<ScanFilter> a(@NonNull List<a> list) {
        ArrayList arrayList = new ArrayList();
        for (a a2 : list) {
            ScanFilter a3 = a(a2);
            if (a3 != null) {
                arrayList.add(a3);
            }
        }
        return arrayList;
    }

    @Nullable
    private ScanFilter a(@NonNull a aVar) {
        boolean z;
        boolean z2;
        boolean z3;
        Builder builder = new Builder();
        boolean z4 = false;
        if (!TextUtils.isEmpty(aVar.b)) {
            builder.setDeviceName(aVar.b);
            z = false;
        } else {
            z = true;
        }
        if (!TextUtils.isEmpty(aVar.f5210a) && BluetoothAdapter.checkBluetoothAddress(aVar.f5210a.toUpperCase(Locale.US))) {
            builder.setDeviceAddress(aVar.f5210a);
            z = false;
        }
        if (aVar.c != null) {
            z3 = a(builder, aVar.c);
            z2 = false;
        } else {
            z2 = z;
            z3 = true;
        }
        if (aVar.d != null) {
            z3 = z3 && a(builder, aVar.d);
            z2 = false;
        }
        if (aVar.e != null) {
            builder.setServiceUuid(aVar.e.f5213a, aVar.e.b);
        } else {
            z4 = z2;
        }
        if (!z3 || z4) {
            return null;
        }
        return builder.build();
    }

    private boolean a(@NonNull Builder builder, @NonNull C0130a aVar) {
        if (aVar.f5211a < 0) {
            return false;
        }
        if ((aVar.b == null && aVar.c != null) || a(aVar.b, aVar.c)) {
            return false;
        }
        builder.setManufacturerData(aVar.f5211a, aVar.b, aVar.c);
        return true;
    }

    private boolean a(@NonNull Builder builder, @NonNull b bVar) {
        if (bVar.f5212a == null) {
            return false;
        }
        if ((bVar.b == null && bVar.c != null) || a(bVar.b, bVar.c)) {
            return false;
        }
        builder.setServiceData(bVar.f5212a, bVar.b, bVar.c);
        return true;
    }

    private boolean a(byte[] bArr, byte[] bArr2) {
        return (bArr == null || bArr2 == null || bArr.length == bArr2.length) ? false : true;
    }
}
