package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.metrica.impl.ob.oh.a;
import java.util.List;

public class oc extends oh {

    /* renamed from: a reason: collision with root package name */
    public final long f5022a;
    public final long b;
    public final boolean c;
    @Nullable
    public final List<oj> d;

    public oc(long j, float f, int i, int i2, long j2, int i3, boolean z, long j3, long j4, long j5, boolean z2, boolean z3, @Nullable List<oj> list) {
        super(j, f, i, i2, j2, i3, z, j5, z2);
        this.f5022a = j3;
        this.b = j4;
        this.c = z3;
        this.d = list;
    }

    @NonNull
    public a a() {
        return a.BACKGROUND;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("BackgroundCollectionConfig{collectionDuration=");
        sb.append(this.f5022a);
        sb.append(", collectionInterval=");
        sb.append(this.b);
        sb.append(", aggressiveRelaunch=");
        sb.append(this.c);
        sb.append(", collectionIntervalRanges=");
        sb.append(this.d);
        sb.append(", updateTimeInterval=");
        sb.append(this.e);
        sb.append(", updateDistanceInterval=");
        sb.append(this.f);
        sb.append(", recordsCountToForceFlush=");
        sb.append(this.g);
        sb.append(", maxBatchSize=");
        sb.append(this.h);
        sb.append(", maxAgeToForceFlush=");
        sb.append(this.i);
        sb.append(", maxRecordsToStoreLocally=");
        sb.append(this.j);
        sb.append(", collectionEnabled=");
        sb.append(this.k);
        sb.append(", lbsUpdateTimeInterval=");
        sb.append(this.l);
        sb.append(", lbsCollectionEnabled=");
        sb.append(this.m);
        sb.append('}');
        return sb.toString();
    }
}
