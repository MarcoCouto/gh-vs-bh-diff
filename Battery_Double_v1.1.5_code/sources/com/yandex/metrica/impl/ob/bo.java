package com.yandex.metrica.impl.ob;

import android.net.Uri;
import android.net.Uri.Builder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import com.yandex.metrica.impl.ob.sw;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public abstract class bo<T extends sw> {

    /* renamed from: a reason: collision with root package name */
    protected String f4695a;
    protected int b = 1;
    protected final Map<String, List<String>> c = new HashMap();
    protected byte[] d;
    protected int e;
    protected byte[] f;
    protected Map<String, List<String>> g;
    protected int h = -1;
    @NonNull
    protected final T i;
    private List<String> j;
    @Nullable
    private Boolean k;
    private boolean l;
    @Nullable
    private Long m;
    @Nullable
    private Integer n;

    public void C() {
    }

    public void D() {
    }

    /* access modifiers changed from: protected */
    public abstract void a(@NonNull Builder builder);

    public void a(Throwable th) {
    }

    public abstract boolean a();

    public abstract boolean b();

    /* access modifiers changed from: protected */
    public boolean b(int i2) {
        return (i2 == 400 || i2 == 500) ? false : true;
    }

    public void f() {
    }

    public boolean o() {
        return false;
    }

    public bo(@NonNull T t) {
        this.i = t;
    }

    @NonNull
    public pj c() {
        return new pl().a(h());
    }

    public void d() {
        u();
        e();
    }

    /* access modifiers changed from: protected */
    public void e() {
        Builder buildUpon = Uri.parse(r()).buildUpon();
        a(buildUpon);
        a(buildUpon.build().toString());
    }

    public void g() {
        this.k = Boolean.valueOf(false);
    }

    public String h() {
        return this.f4695a;
    }

    public void a(String str) {
        this.f4695a = str;
    }

    public int i() {
        return this.b;
    }

    public byte[] j() {
        return this.d;
    }

    public void a(byte[] bArr) {
        this.b = 2;
        this.d = bArr;
    }

    public int k() {
        return this.e;
    }

    public void a(int i2) {
        this.e = i2;
    }

    public byte[] l() {
        return this.f;
    }

    public void b(byte[] bArr) {
        this.f = bArr;
    }

    /* access modifiers changed from: 0000 */
    public Map<String, List<String>> m() {
        return this.g;
    }

    /* access modifiers changed from: 0000 */
    public void a(Map<String, List<String>> map) {
        this.g = map;
    }

    @NonNull
    public String n() {
        return getClass().getName();
    }

    public void a(List<String> list) {
        this.j = list;
    }

    /* access modifiers changed from: protected */
    public boolean p() {
        return k() == 400;
    }

    public int q() {
        return this.h;
    }

    /* access modifiers changed from: protected */
    public String r() {
        return (String) this.j.get(q());
    }

    public List<String> s() {
        return this.j;
    }

    public boolean t() {
        return !v() && q() + 1 < this.j.size();
    }

    @VisibleForTesting
    public void u() {
        this.h++;
    }

    public boolean v() {
        return this.l;
    }

    public void w() {
        this.l = true;
    }

    public void a(boolean z) {
        this.k = Boolean.valueOf(z);
    }

    public boolean x() {
        return this.k != null && this.k.booleanValue();
    }

    public boolean y() {
        return this.k != null;
    }

    @Nullable
    public Long z() {
        return this.m;
    }

    @Nullable
    public Integer A() {
        return this.n;
    }

    public void a(@Nullable Long l2) {
        this.m = l2;
    }

    public void a(@Nullable Integer num) {
        this.n = num;
    }

    public void a(@NonNull String str, @NonNull String... strArr) {
        this.c.put(str, Arrays.asList(strArr));
    }

    public Map<String, List<String>> B() {
        return this.c;
    }

    /* access modifiers changed from: protected */
    public void a(long j2) {
        a(Long.valueOf(j2));
        a(Integer.valueOf(wi.a(TimeUnit.MILLISECONDS.toSeconds(j2))));
    }
}
