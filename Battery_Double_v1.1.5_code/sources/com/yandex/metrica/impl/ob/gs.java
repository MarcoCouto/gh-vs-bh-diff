package com.yandex.metrica.impl.ob;

import com.yandex.metrica.impl.ob.en;
import com.yandex.metrica.impl.ob.gz;

public class gs<T extends gz, C extends en> extends gl<T, C> {
    public gs(gr<T> grVar, C c) {
        super(grVar, c);
    }

    public boolean b(w wVar) {
        return a(wVar, new a<T>() {
            public boolean a(T t, w wVar) {
                return t.a(wVar);
            }
        });
    }
}
