package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;

public interface xh extends xj {
    <T> Future<T> a(Callable<T> callable);

    void a(@NonNull Runnable runnable);

    void a(@NonNull Runnable runnable, long j);

    void b(@NonNull Runnable runnable);
}
