package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;

public interface mq<S, P> {
    @NonNull
    S a(@NonNull P p);

    @NonNull
    P b(@NonNull S s);
}
