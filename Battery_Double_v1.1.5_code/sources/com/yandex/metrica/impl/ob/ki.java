package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.metrica.c;

public abstract class ki {

    /* renamed from: a reason: collision with root package name */
    private final a f4939a;
    @Nullable
    private final c b;

    public interface a {
        boolean a(Throwable th);
    }

    /* access modifiers changed from: 0000 */
    public abstract void b(@NonNull kl klVar);

    ki(a aVar, @Nullable c cVar) {
        this.f4939a = aVar;
        this.b = cVar;
    }

    public void a(@NonNull kl klVar) {
        if (this.f4939a.a(klVar.a())) {
            Throwable a2 = klVar.a();
            if (!(this.b == null || a2 == null)) {
                a2 = this.b.a(a2);
                if (a2 == null) {
                    return;
                }
            }
            b(new kl(a2, klVar.c, klVar.d, klVar.e));
        }
    }
}
