package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;

public class ff {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final lw f4836a;
    @NonNull
    private jd b;
    @NonNull
    private kz c;
    @NonNull
    private final wy d;
    @NonNull
    private final i e;
    @NonNull
    private final er f;
    @NonNull
    private a g;
    @NonNull
    private final wh h;
    private final int i;
    private long j;
    private long k;
    private int l;

    public interface a {
        void a();
    }

    public ff(@NonNull lw lwVar, @NonNull jd jdVar, @NonNull kz kzVar, @NonNull i iVar, @NonNull wy wyVar, int i2, @NonNull a aVar) {
        this(lwVar, jdVar, kzVar, iVar, wyVar, i2, aVar, new er(lwVar), new wg());
    }

    @VisibleForTesting
    public ff(@NonNull lw lwVar, @NonNull jd jdVar, @NonNull kz kzVar, @NonNull i iVar, @NonNull wy wyVar, int i2, @NonNull a aVar, @NonNull er erVar, @NonNull wh whVar) {
        this.f4836a = lwVar;
        this.b = jdVar;
        this.c = kzVar;
        this.e = iVar;
        this.d = wyVar;
        this.i = i2;
        this.f = erVar;
        this.h = whVar;
        this.g = aVar;
        this.j = this.f4836a.a(0);
        this.k = this.f4836a.b();
        this.l = this.f4836a.c();
    }

    public void a(w wVar) {
        this.b.c(wVar);
    }

    public void b(w wVar) {
        e(wVar);
        f();
    }

    public void c(w wVar) {
        e(wVar);
        a();
    }

    public void d(w wVar) {
        e(wVar);
        b();
    }

    public void e(w wVar) {
        a(wVar, this.b.d(wVar));
    }

    public void f(@NonNull w wVar) {
        a(wVar, this.b.e(wVar));
    }

    @VisibleForTesting
    public void a(@NonNull w wVar, @NonNull je jeVar) {
        if (TextUtils.isEmpty(wVar.l())) {
            wVar.a(this.f4836a.f());
        }
        wVar.d(this.f4836a.h());
        je jeVar2 = jeVar;
        this.c.a(this.d.a(wVar).a(wVar), wVar.g(), jeVar2, this.e.b(), this.f);
        this.g.a();
    }

    private void f() {
        this.j = this.h.b();
        this.f4836a.b(this.j).q();
    }

    public void a() {
        this.k = this.h.b();
        this.f4836a.c(this.k).q();
    }

    public void b() {
        this.l = this.i;
        this.f4836a.c(this.l).q();
    }

    public boolean c() {
        return this.h.b() - this.j > ja.f4895a;
    }

    public long d() {
        return this.k;
    }

    public boolean e() {
        return this.l < this.i;
    }
}
