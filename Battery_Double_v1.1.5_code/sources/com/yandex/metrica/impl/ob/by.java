package com.yandex.metrica.impl.ob;

import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri.Builder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import com.ironsource.sdk.constants.LocationConst;
import com.tapjoy.TapjoyConstants;
import com.yandex.metrica.impl.ob.rh.c.C0112c;
import com.yandex.metrica.impl.ob.rh.c.d;
import com.yandex.metrica.impl.ob.rh.c.e;
import com.yandex.metrica.impl.ob.rh.c.f;
import com.yandex.metrica.impl.ob.rh.c.g;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import org.json.JSONObject;

class by extends bp<sz> {
    com.yandex.metrica.impl.ob.rh.c j;
    kz k;
    List<Long> l;
    int m;
    int n;
    @NonNull
    private final en o;
    private final Map<String, String> p;
    private sr q;
    private c r;
    private final yb<byte[]> s;
    private final vz t;
    @Nullable
    private st u;
    @NonNull
    private final lw v;
    private int w;

    static class a {
        a() {
        }

        /* access modifiers changed from: 0000 */
        public by a(en enVar) {
            return new by(enVar);
        }
    }

    static final class b {

        /* renamed from: a reason: collision with root package name */
        final e f4708a;
        final com.yandex.metrica.impl.ob.i.a b;
        final boolean c;

        b(e eVar, com.yandex.metrica.impl.ob.i.a aVar, boolean z) {
            this.f4708a = eVar;
            this.b = aVar;
            this.c = z;
        }
    }

    static final class c {

        /* renamed from: a reason: collision with root package name */
        final List<e> f4709a;
        final List<Long> b;
        final JSONObject c;

        c(List<e> list, List<Long> list2, JSONObject jSONObject) {
            this.f4709a = list;
            this.b = list2;
            this.c = jSONObject;
        }
    }

    public by(en enVar) {
        this(enVar, enVar.j(), new lw(ld.a(enVar.k()).b(enVar.b())));
    }

    by(@NonNull en enVar, @NonNull kz kzVar, @NonNull lw lwVar) {
        this(enVar, kzVar, new sz(), lwVar);
    }

    @VisibleForTesting
    by(@NonNull en enVar, @NonNull kz kzVar, @NonNull sz szVar, @NonNull lw lwVar) {
        super(szVar);
        this.p = new LinkedHashMap();
        this.m = 0;
        this.n = -1;
        this.o = enVar;
        this.k = kzVar;
        this.t = enVar.l();
        this.s = new xr(245760, "event value in ReportTask", this.t);
        this.v = lwVar;
    }

    /* access modifiers changed from: protected */
    public void a(@NonNull Builder builder) {
        ((sz) this.i).a(builder, this.u);
    }

    /* access modifiers changed from: 0000 */
    public void a(@NonNull ContentValues contentValues) {
        this.p.clear();
        for (Entry entry : contentValues.valueSet()) {
            this.p.put(entry.getKey(), entry.getValue().toString());
        }
        String asString = contentValues.getAsString("report_request_parameters");
        if (!TextUtils.isEmpty(asString)) {
            try {
                this.q = new sr(new com.yandex.metrica.impl.ob.vq.a(asString));
                ((sz) this.i).a(this.q);
            } catch (Throwable unused) {
                K();
            }
        } else {
            K();
        }
    }

    private void K() {
        this.q = new sr();
        ((sz) this.i).a(this.q);
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public com.yandex.metrica.impl.ob.rh.c a(c cVar, C0112c[] cVarArr, @NonNull List<String> list) {
        com.yandex.metrica.impl.ob.rh.c cVar2 = new com.yandex.metrica.impl.ob.rh.c();
        d dVar = new d();
        dVar.b = wk.a(this.q.b, this.u.t());
        dVar.c = wk.a(this.q.f5168a, this.u.r());
        this.m += b.b(4, (e) dVar);
        cVar2.c = dVar;
        a(cVar2);
        cVar2.b = (e[]) cVar.f4709a.toArray(new e[cVar.f4709a.size()]);
        cVar2.d = a(cVar.c);
        cVar2.e = cVarArr;
        cVar2.h = (String[]) list.toArray(new String[list.size()]);
        this.m += b.h(8);
        return cVar2;
    }

    /* access modifiers changed from: 0000 */
    public void a(final com.yandex.metrica.impl.ob.rh.c cVar) {
        al.a().k().a((vg) new vg() {
            public void a(vf vfVar) {
                b(vfVar, cVar);
                a(vfVar, cVar);
            }

            private void a(vf vfVar, com.yandex.metrica.impl.ob.rh.c cVar) {
                List a2 = vfVar.a();
                if (!cx.a((Collection) a2)) {
                    cVar.g = new f[a2.size()];
                    for (int i = 0; i < a2.size(); i++) {
                        cVar.g[i] = bv.a((vb) a2.get(i));
                        by.this.m += b.b((e) cVar.g[i]);
                        by.this.m += b.h(10);
                    }
                }
            }

            private void b(vf vfVar, com.yandex.metrica.impl.ob.rh.c cVar) {
                List c = vfVar.c();
                if (!cx.a((Collection) c)) {
                    cVar.f = new String[c.size()];
                    for (int i = 0; i < c.size(); i++) {
                        String str = (String) c.get(i);
                        if (!TextUtils.isEmpty(str)) {
                            cVar.f[i] = str;
                            by.this.m += b.b(cVar.f[i]);
                            by.this.m += b.h(9);
                        }
                    }
                }
            }
        });
    }

    public boolean a() {
        List d = this.o.j().d();
        if (d.isEmpty()) {
            return false;
        }
        a((ContentValues) d.get(0));
        this.u = this.o.i();
        List Y = this.u.Y();
        if (Y.isEmpty()) {
            return false;
        }
        a(this.u.b());
        if (!this.u.L() || cx.a((Collection) s())) {
            return false;
        }
        this.l = null;
        C0112c[] G = G();
        this.r = H();
        if (this.r.f4709a.isEmpty()) {
            return false;
        }
        this.w = this.v.l() + 1;
        ((sz) this.i).a(this.w);
        this.j = a(this.r, G, Y);
        this.l = this.r.b;
        c(e.a((e) this.j));
        return true;
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public C0112c[] G() {
        C0112c[] a2 = bv.a(this.o.k());
        if (a2 != null) {
            for (C0112c b2 : a2) {
                this.m += b.b((e) b2);
            }
        }
        return a2;
    }

    private com.yandex.metrica.impl.ob.rh.c.a[] a(JSONObject jSONObject) {
        int length = jSONObject.length();
        if (length <= 0) {
            return null;
        }
        com.yandex.metrica.impl.ob.rh.c.a[] aVarArr = new com.yandex.metrica.impl.ob.rh.c.a[length];
        Iterator keys = jSONObject.keys();
        int i = 0;
        while (keys.hasNext()) {
            String str = (String) keys.next();
            try {
                com.yandex.metrica.impl.ob.rh.c.a aVar = new com.yandex.metrica.impl.ob.rh.c.a();
                aVar.b = str;
                aVar.c = jSONObject.getString(str);
                aVarArr[i] = aVar;
            } catch (Throwable unused) {
            }
            i++;
        }
        return aVarArr;
    }

    /* access modifiers changed from: protected */
    public void F() {
        b(true);
    }

    /* access modifiers changed from: protected */
    public void E() {
        b(false);
    }

    private void b(boolean z) {
        L();
        e[] eVarArr = this.j.b;
        for (int i = 0; i < eVarArr.length; i++) {
            e eVar = eVarArr[i];
            this.k.a(((Long) this.l.get(i)).longValue(), bv.a(eVar.c.d).a(), eVar.d.length, z);
            bv.a(eVar);
        }
        this.k.a(this.o.d().a());
    }

    private void L() {
        this.v.e(this.w);
    }

    public boolean t() {
        return super.t() & (400 != k());
    }

    public void f() {
        if (x()) {
            M();
        }
        this.r = null;
    }

    private void M() {
        if (this.t.c()) {
            for (int i = 0; i < this.r.f4709a.size(); i++) {
                this.t.a((e) this.r.f4709a.get(i), "Event sent");
            }
        }
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Can't wrap try/catch for region: R(8:(1:12)(2:13|(2:37|15))|16|17|18|19|20|21|(1:35)(1:39)) */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0098, code lost:
        r0 = th;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:20:0x0093 */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0098 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:20:0x0093] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00a1 A[EDGE_INSN: B:35:0x00a1->B:30:0x00a1 ?: BREAK  , SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0014 A[SYNTHETIC] */
    public c H() {
        Cursor cursor;
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        JSONObject jSONObject = new JSONObject();
        com.yandex.metrica.impl.ob.i.a aVar = null;
        try {
            cursor = I();
            while (true) {
                try {
                    if (!cursor.moveToNext()) {
                        break;
                    }
                    ContentValues contentValues = new ContentValues();
                    vl.a(cursor, contentValues);
                    long longValue = contentValues.getAsLong("id").longValue();
                    jh a2 = jh.a(contentValues.getAsInteger("type"));
                    g a3 = bv.a(contentValues);
                    com.yandex.metrica.impl.ob.rh.c.e.b a4 = bv.a(this.u.A(), bv.a(a2), a3);
                    this.m += b.d(1, Long.MAX_VALUE);
                    this.m += b.b(2, (e) a4);
                    if (this.m >= 250880) {
                        break;
                    }
                    b a5 = a(longValue, a4);
                    if (a5 != null) {
                        if (aVar != null) {
                            if (!aVar.equals(a5.b)) {
                                break;
                            }
                        } else {
                            aVar = a5.b;
                        }
                        arrayList2.add(Long.valueOf(longValue));
                        arrayList.add(a5.f4708a);
                        jSONObject = new JSONObject(a5.b.f4882a);
                        if (!a5.c) {
                            break;
                        }
                    }
                } catch (Throwable th) {
                }
            }
        } catch (Throwable th2) {
            th = th2;
            cursor = null;
            cx.a(cursor);
            throw th;
        }
        cx.a(cursor);
        return new c(arrayList, arrayList2, jSONObject);
    }

    private com.yandex.metrica.impl.ob.i.a b(ContentValues contentValues) {
        return new com.yandex.metrica.impl.ob.i.a(contentValues.getAsString("app_environment"), contentValues.getAsLong("app_environment_revision").longValue());
    }

    private int a(com.yandex.metrica.impl.ob.i.a aVar) {
        int i = 0;
        try {
            com.yandex.metrica.impl.ob.rh.c.a[] a2 = a(new JSONObject(aVar.f4882a));
            if (a2 != null) {
                int i2 = 0;
                for (com.yandex.metrica.impl.ob.rh.c.a b2 : a2) {
                    i2 += b.b(7, (e) b2);
                }
                i = i2;
            }
            return i;
        } catch (Throwable unused) {
            return 0;
        }
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0048, code lost:
        r9 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0084, code lost:
        r8 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0086, code lost:
        r9 = null;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0084 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:3:0x0015] */
    public b a(long j2, com.yandex.metrica.impl.ob.rh.c.e.b bVar) {
        com.yandex.metrica.impl.ob.i.a aVar;
        Cursor cursor;
        e eVar = new e();
        eVar.b = j2;
        eVar.c = bVar;
        boolean z = false;
        try {
            cursor = a(j2, bv.a(bVar.d));
            try {
                ArrayList arrayList = new ArrayList();
                aVar = null;
                while (true) {
                    if (!cursor.moveToNext()) {
                        break;
                    }
                    ContentValues contentValues = new ContentValues();
                    vl.a(cursor, contentValues);
                    com.yandex.metrica.impl.ob.rh.c.e.a d = d(contentValues);
                    if (d != null) {
                        com.yandex.metrica.impl.ob.i.a b2 = b(contentValues);
                        if (aVar != null) {
                            if (!aVar.equals(b2)) {
                                z = true;
                                break;
                            }
                        } else {
                            if (this.n < 0) {
                                this.n = a(b2);
                                this.m += this.n;
                            }
                            aVar = b2;
                        }
                        a(d);
                        this.m += b.b(3, (e) d);
                        if (this.m >= 250880) {
                            break;
                        }
                        arrayList.add(d);
                    }
                }
                if (arrayList.size() > 0) {
                    eVar.d = (com.yandex.metrica.impl.ob.rh.c.e.a[]) arrayList.toArray(new com.yandex.metrica.impl.ob.rh.c.e.a[arrayList.size()]);
                    cx.a(cursor);
                    return new b(eVar, aVar, z);
                }
                cx.a(cursor);
                return null;
            } catch (Throwable th) {
            }
        } catch (Throwable th2) {
            th = th2;
            cursor = null;
            cx.a(cursor);
            throw th;
        }
    }

    private void a(com.yandex.metrica.impl.ob.rh.c.e.a aVar) {
        byte[] bArr = (byte[]) this.s.a(aVar.f);
        if (!aVar.f.equals(bArr)) {
            aVar.f = bArr;
            aVar.k += aVar.f.length - bArr.length;
        }
    }

    private c c(ContentValues contentValues) {
        return c.a(contentValues.getAsInteger("type").intValue(), this.u.M()).b(contentValues.getAsInteger("custom_type")).a(contentValues.getAsString("name")).b(contentValues.getAsString("value")).a(contentValues.getAsLong(LocationConst.TIME).longValue()).a(contentValues.getAsInteger("number").intValue()).b(contentValues.getAsInteger("global_number").intValue()).c(contentValues.getAsInteger("number_of_type").intValue()).e(contentValues.getAsString("cell_info")).c(contentValues.getAsString("location_info")).d(contentValues.getAsString("wifi_network_info")).g(contentValues.getAsString("error_environment")).h(contentValues.getAsString("user_info")).d(contentValues.getAsInteger("truncated").intValue()).e(contentValues.getAsInteger(TapjoyConstants.TJC_CONNECTION_TYPE).intValue()).i(contentValues.getAsString("cellular_connection_type")).f(contentValues.getAsString("wifi_access_point")).j(contentValues.getAsString("profile_id")).a(wz.a(contentValues.getAsInteger("encrypting_mode").intValue())).a(aj.a(contentValues.getAsInteger("first_occurrence_status")));
    }

    private com.yandex.metrica.impl.ob.rh.c.e.a d(ContentValues contentValues) {
        c c2 = c(contentValues);
        if (c2.c() != null) {
            return c2.e();
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public Cursor I() {
        return this.k.a(this.p);
    }

    /* access modifiers changed from: protected */
    public Cursor a(long j2, jh jhVar) {
        return this.k.a(j2, jhVar);
    }

    public void C() {
        this.o.C().c();
    }

    public void D() {
        this.o.j().c();
        this.o.C().b();
        if (x()) {
            this.o.C().a();
        }
    }

    public static a J() {
        return new a();
    }
}
