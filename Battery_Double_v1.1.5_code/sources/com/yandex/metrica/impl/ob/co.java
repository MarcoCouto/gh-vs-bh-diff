package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import com.applovin.sdk.AppLovinMediationProvider;
import com.facebook.share.internal.ShareConstants;
import com.tapjoy.TapjoyConstants;
import com.yandex.metrica.impl.ob.rr.a.b;
import com.yandex.metrica.impl.ob.rr.a.b.C0123a;
import com.yandex.metrica.impl.ob.rr.a.b.C0123a.C0124a;
import com.yandex.metrica.impl.ob.rr.a.b.C0123a.C0125b;
import com.yandex.metrica.impl.ob.rr.a.b.C0123a.c;
import com.yandex.metrica.impl.ob.rr.a.b.C0126b;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import org.json.JSONArray;
import org.json.JSONObject;

public class co {

    /* renamed from: a reason: collision with root package name */
    private static final Map<String, Integer> f4744a;
    private static final Map<String, Integer> b;
    private static final Map<String, Integer> c;
    private static final Map<String, Integer> d;
    @NonNull
    private final nh e;

    static {
        HashMap hashMap = new HashMap(3);
        hashMap.put("all_matches", Integer.valueOf(1));
        hashMap.put("first_match", Integer.valueOf(2));
        hashMap.put("match_lost", Integer.valueOf(3));
        f4744a = Collections.unmodifiableMap(hashMap);
        HashMap hashMap2 = new HashMap(2);
        hashMap2.put("aggressive", Integer.valueOf(1));
        hashMap2.put("sticky", Integer.valueOf(2));
        b = Collections.unmodifiableMap(hashMap2);
        HashMap hashMap3 = new HashMap(3);
        hashMap3.put("one", Integer.valueOf(1));
        hashMap3.put("few", Integer.valueOf(2));
        hashMap3.put(AppLovinMediationProvider.MAX, Integer.valueOf(3));
        c = Collections.unmodifiableMap(hashMap3);
        HashMap hashMap4 = new HashMap();
        hashMap4.put("low_power", Integer.valueOf(1));
        hashMap4.put("balanced", Integer.valueOf(2));
        hashMap4.put("low_latency", Integer.valueOf(3));
        d = Collections.unmodifiableMap(hashMap4);
    }

    public co() {
        this(new nh());
    }

    @NonNull
    public tt a(@NonNull JSONObject jSONObject) {
        return this.e.a(b(jSONObject));
    }

    @NonNull
    private b b(@NonNull JSONObject jSONObject) {
        b bVar = new b();
        JSONObject optJSONObject = jSONObject.optJSONObject("ble_collecting");
        if (optJSONObject != null) {
            bVar.b = c(optJSONObject.optJSONObject("scan_settings"));
            bVar.c = a(optJSONObject.optJSONArray(ShareConstants.WEB_DIALOG_PARAM_FILTERS));
            bVar.d = wk.a(vq.a(optJSONObject, "same_beacon_min_reporting_interval"), TimeUnit.SECONDS, bVar.d);
            bVar.e = wk.a(vq.a(optJSONObject, "first_delay_seconds"), TimeUnit.SECONDS, bVar.e);
        } else {
            bVar.b = new C0126b();
        }
        return bVar;
    }

    @NonNull
    private C0126b c(@Nullable JSONObject jSONObject) {
        C0126b bVar = new C0126b();
        if (jSONObject != null) {
            Integer a2 = a(jSONObject, "callback_type", f4744a);
            if (a2 != null) {
                bVar.b = a2.intValue();
            }
            Integer a3 = a(jSONObject, "match_mode", b);
            if (a3 != null) {
                bVar.c = a3.intValue();
            }
            Integer a4 = a(jSONObject, "num_of_matches", c);
            if (a4 != null) {
                bVar.d = a4.intValue();
            }
            Integer a5 = a(jSONObject, "scan_mode", d);
            if (a5 != null) {
                bVar.e = a5.intValue();
            }
            bVar.f = wk.a(vq.a(jSONObject, "report_delay"), TimeUnit.SECONDS, bVar.f);
        }
        return bVar;
    }

    @NonNull
    private C0123a[] a(@Nullable JSONArray jSONArray) {
        ArrayList arrayList = new ArrayList();
        if (jSONArray != null && jSONArray.length() > 0) {
            for (int i = 0; i < jSONArray.length(); i++) {
                C0123a d2 = d(jSONArray.optJSONObject(i));
                if (d2 != null) {
                    arrayList.add(d2);
                }
            }
        }
        return (C0123a[]) arrayList.toArray(new C0123a[arrayList.size()]);
    }

    @Nullable
    private C0123a d(@Nullable JSONObject jSONObject) {
        C0123a aVar;
        boolean z = true;
        if (jSONObject != null) {
            aVar = new C0123a();
            String optString = jSONObject.optString("device_address", null);
            if (optString != null) {
                aVar.b = optString;
                z = false;
            }
            String optString2 = jSONObject.optString(TapjoyConstants.TJC_DEVICE_NAME, null);
            if (optString2 != null) {
                aVar.c = optString2;
                z = false;
            }
            C0124a e2 = e(jSONObject.optJSONObject("manufacturer_data"));
            if (e2 != null) {
                aVar.d = e2;
                z = false;
            }
            C0125b f = f(jSONObject.optJSONObject("service_data"));
            if (f != null) {
                aVar.e = f;
                z = false;
            }
            c g = g(jSONObject.optJSONObject("service_uuid"));
            if (g != null) {
                aVar.f = g;
                z = false;
            }
        } else {
            aVar = null;
        }
        if (z) {
            return null;
        }
        return aVar;
    }

    @Nullable
    private C0124a e(@Nullable JSONObject jSONObject) {
        if (jSONObject != null) {
            Integer b2 = vq.b(jSONObject, "id");
            if (b2 != null) {
                C0124a aVar = new C0124a();
                aVar.b = b2.intValue();
                aVar.c = vq.a(jSONObject, "data", aVar.c);
                aVar.d = vq.a(jSONObject, "data_mask", aVar.d);
                return aVar;
            }
        }
        return null;
    }

    @Nullable
    private C0125b f(@Nullable JSONObject jSONObject) {
        if (jSONObject == null) {
            return null;
        }
        String optString = jSONObject.optString("uuid", null);
        if (TextUtils.isEmpty(optString)) {
            return null;
        }
        C0125b bVar = new C0125b();
        bVar.b = optString;
        bVar.c = vq.a(jSONObject, "data", bVar.c);
        bVar.d = vq.a(jSONObject, "data_mask", bVar.d);
        return bVar;
    }

    @Nullable
    private c g(@Nullable JSONObject jSONObject) {
        if (jSONObject == null) {
            return null;
        }
        String optString = jSONObject.optString("uuid", null);
        if (TextUtils.isEmpty(optString)) {
            return null;
        }
        c cVar = new c();
        cVar.b = optString;
        cVar.c = jSONObject.optString("data_mask", cVar.c);
        return cVar;
    }

    @Nullable
    private Integer a(@NonNull JSONObject jSONObject, @NonNull String str, Map<String, Integer> map) {
        if (jSONObject.has(str)) {
            return (Integer) map.get(jSONObject.optString(str));
        }
        return null;
    }

    @VisibleForTesting
    public co(@NonNull nh nhVar) {
        this.e = nhVar;
    }
}
