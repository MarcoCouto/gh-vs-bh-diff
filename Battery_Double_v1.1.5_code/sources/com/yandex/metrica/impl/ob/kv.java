package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Base64;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.tapjoy.TapjoyConstants;
import com.yandex.metrica.CounterConfiguration;
import com.yandex.metrica.impl.ob.r.a;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import org.json.JSONException;
import org.json.JSONObject;

public class kv {

    /* renamed from: a reason: collision with root package name */
    private final byte[] f4950a;
    private final String b;
    private final int c;
    @NonNull
    private final HashMap<a, Integer> d;
    private final String e;
    private final Integer f;
    private final String g;
    private final String h;
    @NonNull
    private final CounterConfiguration.a i;

    public kv(@NonNull String str) throws JSONException {
        JSONObject jSONObject = new JSONObject(str);
        JSONObject jSONObject2 = jSONObject.getJSONObject("event");
        this.f4950a = Base64.decode(jSONObject2.getString("jvm_crash"), 0);
        this.b = jSONObject2.getString("name");
        this.c = jSONObject2.getInt("bytes_truncated");
        String optString = jSONObject2.optString("trimmed_fields");
        this.d = new HashMap<>();
        if (optString != null) {
            try {
                HashMap a2 = vq.a(optString);
                if (a2 != null) {
                    for (Entry entry : a2.entrySet()) {
                        this.d.put(a.valueOf((String) entry.getKey()), Integer.valueOf(Integer.parseInt((String) entry.getValue())));
                    }
                }
            } catch (Throwable unused) {
            }
        }
        JSONObject jSONObject3 = jSONObject.getJSONObject("process_configuration");
        this.e = jSONObject3.getString(CampaignEx.JSON_KEY_PACKAGE_NAME);
        this.f = Integer.valueOf(jSONObject3.getInt("pid"));
        this.g = jSONObject3.getString("psid");
        JSONObject jSONObject4 = jSONObject.getJSONObject("reporter_configuration");
        this.h = jSONObject4.getString(TapjoyConstants.TJC_API_KEY);
        this.i = a(jSONObject4);
    }

    public kv(@NonNull w wVar, @NonNull ed edVar, @Nullable HashMap<a, Integer> hashMap) {
        this.f4950a = wVar.f();
        this.b = wVar.d();
        this.c = wVar.o();
        if (hashMap != null) {
            this.d = hashMap;
        } else {
            this.d = new HashMap<>();
        }
        ee g2 = edVar.g();
        this.e = g2.h();
        this.f = g2.e();
        this.g = g2.f();
        CounterConfiguration h2 = edVar.h();
        this.h = h2.e();
        this.i = h2.q();
    }

    public byte[] a() {
        return this.f4950a;
    }

    public String b() {
        return this.b;
    }

    public int c() {
        return this.c;
    }

    @NonNull
    public HashMap<a, Integer> d() {
        return this.d;
    }

    public Integer e() {
        return this.f;
    }

    public String f() {
        return this.g;
    }

    public String g() {
        return this.e;
    }

    public String h() {
        return this.h;
    }

    @NonNull
    public CounterConfiguration.a i() {
        return this.i;
    }

    public String j() throws JSONException {
        HashMap hashMap = new HashMap();
        for (Entry entry : this.d.entrySet()) {
            hashMap.put(((a) entry.getKey()).name(), entry.getValue());
        }
        return new JSONObject().put("process_configuration", new JSONObject().put("pid", this.f).put("psid", this.g).put(CampaignEx.JSON_KEY_PACKAGE_NAME, this.e)).put("reporter_configuration", new JSONObject().put(TapjoyConstants.TJC_API_KEY, this.h).put("reporter_type", this.i.a())).put("event", new JSONObject().put("jvm_crash", Base64.encodeToString(this.f4950a, 0)).put("name", this.b).put("bytes_truncated", this.c).put("trimmed_fields", vq.b((Map) hashMap))).toString();
    }

    @Deprecated
    @NonNull
    private CounterConfiguration.a a(@NonNull JSONObject jSONObject) throws JSONException {
        if (jSONObject.has("reporter_type")) {
            return CounterConfiguration.a.a(jSONObject.getString("reporter_type"));
        }
        if (jSONObject.getBoolean("is_commutation")) {
            return CounterConfiguration.a.COMMUTATION;
        }
        return CounterConfiguration.a.MAIN;
    }
}
