package com.yandex.metrica.impl.ob;

import android.annotation.TargetApi;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import java.util.ArrayList;
import java.util.List;

public class vc implements ve<List<vb>> {
    /* access modifiers changed from: private */
    @NonNull

    /* renamed from: a reason: collision with root package name */
    public final ux f5265a;
    /* access modifiers changed from: private */
    @NonNull
    public pr b;

    vc(@NonNull ux uxVar, @NonNull pr prVar) {
        this.f5265a = uxVar;
        this.b = prVar;
    }

    @Nullable
    /* renamed from: a */
    public List<vb> d() {
        ArrayList arrayList = new ArrayList();
        if (this.f5265a.h()) {
            if (cx.a(23)) {
                arrayList.addAll(g());
                if (arrayList.size() == 0) {
                    arrayList.add(b());
                }
            } else {
                arrayList.add(b());
            }
        }
        return arrayList;
    }

    private vb b() {
        vb vbVar = new vb(c(), e(), h(), f(), null);
        return vbVar;
    }

    @Nullable
    private Integer c() {
        return (Integer) cx.a((wo<T, S>) new wo<TelephonyManager, Integer>() {
            public Integer a(TelephonyManager telephonyManager) throws Throwable {
                String simOperator = telephonyManager.getSimOperator();
                String substring = !TextUtils.isEmpty(simOperator) ? simOperator.substring(0, 3) : null;
                if (TextUtils.isEmpty(substring)) {
                    return null;
                }
                return Integer.valueOf(Integer.parseInt(substring));
            }
        }, this.f5265a.c(), "getting SimMcc", "TelephonyManager");
    }

    @Nullable
    private Integer e() {
        return (Integer) cx.a((wo<T, S>) new wo<TelephonyManager, Integer>() {
            public Integer a(TelephonyManager telephonyManager) throws Throwable {
                String simOperator = telephonyManager.getSimOperator();
                String substring = !TextUtils.isEmpty(simOperator) ? simOperator.substring(3) : null;
                if (TextUtils.isEmpty(substring)) {
                    return null;
                }
                return Integer.valueOf(Integer.parseInt(substring));
            }
        }, this.f5265a.c(), "getting SimMnc", "TelephonyManager");
    }

    @Nullable
    private String f() {
        return (String) cx.a((wo<T, S>) new wo<TelephonyManager, String>() {
            public String a(TelephonyManager telephonyManager) throws Throwable {
                return telephonyManager.getSimOperatorName();
            }
        }, this.f5265a.c(), "getting SimOperatorName", "TelephonyManager");
    }

    @TargetApi(23)
    @NonNull
    private List<vb> g() {
        ArrayList arrayList = new ArrayList();
        if (this.b.d(this.f5265a.d())) {
            try {
                List<SubscriptionInfo> activeSubscriptionInfoList = SubscriptionManager.from(this.f5265a.d()).getActiveSubscriptionInfoList();
                if (activeSubscriptionInfoList != null) {
                    for (SubscriptionInfo vbVar : activeSubscriptionInfoList) {
                        arrayList.add(new vb(vbVar));
                    }
                }
            } catch (Throwable unused) {
            }
        }
        return arrayList;
    }

    private boolean h() {
        return ((Boolean) cx.a(new wo<TelephonyManager, Boolean>() {
            public Boolean a(TelephonyManager telephonyManager) throws Throwable {
                if (vc.this.b.d(vc.this.f5265a.d())) {
                    return Boolean.valueOf(telephonyManager.isNetworkRoaming());
                }
                return null;
            }
        }, this.f5265a.c(), "getting NetworkRoaming", "TelephonyManager", Boolean.valueOf(false))).booleanValue();
    }
}
