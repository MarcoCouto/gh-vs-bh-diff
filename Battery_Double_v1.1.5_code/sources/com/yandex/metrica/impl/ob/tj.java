package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import com.yandex.metrica.d;
import com.yandex.metrica.impl.ob.rs.a.C0128a;
import com.yandex.metrica.impl.ob.rs.a.b;

public class tj {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final tk f5190a;
    @NonNull
    private final d b;

    public tj(@NonNull Context context) {
        this(new tk(), tl.a(context));
    }

    public void a(@NonNull b bVar) {
        this.b.a("provided_request_result", this.f5190a.a(bVar));
    }

    public void a(@NonNull C0128a aVar) {
        this.b.a("provided_request_schedule", this.f5190a.a(aVar));
    }

    public void b(@NonNull C0128a aVar) {
        this.b.a("provided_request_send", this.f5190a.a(aVar));
    }

    @VisibleForTesting
    tj(@NonNull tk tkVar, @NonNull d dVar) {
        this.f5190a = tkVar;
        this.b = dVar;
    }
}
