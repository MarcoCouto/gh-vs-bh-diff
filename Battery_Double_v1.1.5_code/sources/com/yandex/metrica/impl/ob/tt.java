package com.yandex.metrica.impl.ob;

import android.os.ParcelUuid;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.util.Arrays;
import java.util.List;

public class tt {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    public final b f5209a;
    @NonNull
    public final List<a> b;
    public final long c;
    public final long d;

    public static class a {
        @Nullable

        /* renamed from: a reason: collision with root package name */
        public final String f5210a;
        @Nullable
        public final String b;
        @Nullable
        public final C0130a c;
        @Nullable
        public final b d;
        @Nullable
        public final c e;

        /* renamed from: com.yandex.metrica.impl.ob.tt$a$a reason: collision with other inner class name */
        public static class C0130a {

            /* renamed from: a reason: collision with root package name */
            public final int f5211a;
            @Nullable
            public final byte[] b;
            @Nullable
            public final byte[] c;

            public C0130a(int i, @Nullable byte[] bArr, @Nullable byte[] bArr2) {
                this.f5211a = i;
                this.b = bArr;
                this.c = bArr2;
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (obj == null || getClass() != obj.getClass()) {
                    return false;
                }
                C0130a aVar = (C0130a) obj;
                if (this.f5211a == aVar.f5211a && Arrays.equals(this.b, aVar.b)) {
                    return Arrays.equals(this.c, aVar.c);
                }
                return false;
            }

            public int hashCode() {
                return (((this.f5211a * 31) + Arrays.hashCode(this.b)) * 31) + Arrays.hashCode(this.c);
            }

            public String toString() {
                StringBuilder sb = new StringBuilder();
                sb.append("ManufacturerData{manufacturerId=");
                sb.append(this.f5211a);
                sb.append(", data=");
                sb.append(Arrays.toString(this.b));
                sb.append(", dataMask=");
                sb.append(Arrays.toString(this.c));
                sb.append('}');
                return sb.toString();
            }
        }

        public static class b {
            @NonNull

            /* renamed from: a reason: collision with root package name */
            public final ParcelUuid f5212a;
            @Nullable
            public final byte[] b;
            @Nullable
            public final byte[] c;

            public b(@NonNull String str, @Nullable byte[] bArr, @Nullable byte[] bArr2) {
                this.f5212a = ParcelUuid.fromString(str);
                this.b = bArr;
                this.c = bArr2;
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (obj == null || getClass() != obj.getClass()) {
                    return false;
                }
                b bVar = (b) obj;
                if (this.f5212a.equals(bVar.f5212a) && Arrays.equals(this.b, bVar.b)) {
                    return Arrays.equals(this.c, bVar.c);
                }
                return false;
            }

            public int hashCode() {
                return (((this.f5212a.hashCode() * 31) + Arrays.hashCode(this.b)) * 31) + Arrays.hashCode(this.c);
            }

            public String toString() {
                StringBuilder sb = new StringBuilder();
                sb.append("ServiceData{uuid=");
                sb.append(this.f5212a);
                sb.append(", data=");
                sb.append(Arrays.toString(this.b));
                sb.append(", dataMask=");
                sb.append(Arrays.toString(this.c));
                sb.append('}');
                return sb.toString();
            }
        }

        public static class c {
            @NonNull

            /* renamed from: a reason: collision with root package name */
            public final ParcelUuid f5213a;
            @Nullable
            public final ParcelUuid b;

            public c(@NonNull ParcelUuid parcelUuid, @Nullable ParcelUuid parcelUuid2) {
                this.f5213a = parcelUuid;
                this.b = parcelUuid2;
            }

            public boolean equals(Object obj) {
                boolean z = true;
                if (this == obj) {
                    return true;
                }
                if (obj == null || getClass() != obj.getClass()) {
                    return false;
                }
                c cVar = (c) obj;
                if (!this.f5213a.equals(cVar.f5213a)) {
                    return false;
                }
                if (this.b != null) {
                    z = this.b.equals(cVar.b);
                } else if (cVar.b != null) {
                    z = false;
                }
                return z;
            }

            public int hashCode() {
                return (this.f5213a.hashCode() * 31) + (this.b != null ? this.b.hashCode() : 0);
            }

            public String toString() {
                StringBuilder sb = new StringBuilder();
                sb.append("ServiceUuid{uuid=");
                sb.append(this.f5213a);
                sb.append(", uuidMask=");
                sb.append(this.b);
                sb.append('}');
                return sb.toString();
            }
        }

        public a(@Nullable String str, @Nullable String str2, @Nullable C0130a aVar, @Nullable b bVar, @Nullable c cVar) {
            this.f5210a = str;
            this.b = str2;
            this.c = aVar;
            this.d = bVar;
            this.e = cVar;
        }

        public boolean equals(Object obj) {
            boolean z = true;
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            a aVar = (a) obj;
            if (this.f5210a == null ? aVar.f5210a != null : !this.f5210a.equals(aVar.f5210a)) {
                return false;
            }
            if (this.b == null ? aVar.b != null : !this.b.equals(aVar.b)) {
                return false;
            }
            if (this.c == null ? aVar.c != null : !this.c.equals(aVar.c)) {
                return false;
            }
            if (this.d == null ? aVar.d != null : !this.d.equals(aVar.d)) {
                return false;
            }
            if (this.e != null) {
                z = this.e.equals(aVar.e);
            } else if (aVar.e != null) {
                z = false;
            }
            return z;
        }

        public int hashCode() {
            int i = 0;
            int hashCode = (((((((this.f5210a != null ? this.f5210a.hashCode() : 0) * 31) + (this.b != null ? this.b.hashCode() : 0)) * 31) + (this.c != null ? this.c.hashCode() : 0)) * 31) + (this.d != null ? this.d.hashCode() : 0)) * 31;
            if (this.e != null) {
                i = this.e.hashCode();
            }
            return hashCode + i;
        }

        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append("Filter{deviceAddress='");
            sb.append(this.f5210a);
            sb.append('\'');
            sb.append(", deviceName='");
            sb.append(this.b);
            sb.append('\'');
            sb.append(", data=");
            sb.append(this.c);
            sb.append(", serviceData=");
            sb.append(this.d);
            sb.append(", serviceUuid=");
            sb.append(this.e);
            sb.append('}');
            return sb.toString();
        }
    }

    public static class b {
        @NonNull

        /* renamed from: a reason: collision with root package name */
        public final a f5214a;
        @NonNull
        public final C0131b b;
        @NonNull
        public final c c;
        @NonNull
        public final d d;
        public final long e;

        public enum a {
            ALL_MATCHES,
            FIRST_MATCH,
            MATCH_LOST
        }

        /* renamed from: com.yandex.metrica.impl.ob.tt$b$b reason: collision with other inner class name */
        public enum C0131b {
            AGGRESSIVE,
            STICKY
        }

        public enum c {
            ONE_AD,
            FEW_AD,
            MAX_AD
        }

        public enum d {
            LOW_POWER,
            BALANCED,
            LOW_LATENCY
        }

        public b(@NonNull a aVar, @NonNull C0131b bVar, @NonNull c cVar, @NonNull d dVar, long j) {
            this.f5214a = aVar;
            this.b = bVar;
            this.c = cVar;
            this.d = dVar;
            this.e = j;
        }

        public boolean equals(Object obj) {
            boolean z = true;
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            b bVar = (b) obj;
            if (this.e != bVar.e || this.f5214a != bVar.f5214a || this.b != bVar.b || this.c != bVar.c) {
                return false;
            }
            if (this.d != bVar.d) {
                z = false;
            }
            return z;
        }

        public int hashCode() {
            return (((((((this.f5214a.hashCode() * 31) + this.b.hashCode()) * 31) + this.c.hashCode()) * 31) + this.d.hashCode()) * 31) + ((int) (this.e ^ (this.e >>> 32)));
        }

        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append("Settings{callbackType=");
            sb.append(this.f5214a);
            sb.append(", matchMode=");
            sb.append(this.b);
            sb.append(", numOfMatches=");
            sb.append(this.c);
            sb.append(", scanMode=");
            sb.append(this.d);
            sb.append(", reportDelay=");
            sb.append(this.e);
            sb.append('}');
            return sb.toString();
        }
    }

    public tt(@NonNull b bVar, @NonNull List<a> list, long j, long j2) {
        this.f5209a = bVar;
        this.b = list;
        this.c = j;
        this.d = j2;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        tt ttVar = (tt) obj;
        if (this.c == ttVar.c && this.d == ttVar.d && this.f5209a.equals(ttVar.f5209a)) {
            return this.b.equals(ttVar.b);
        }
        return false;
    }

    public int hashCode() {
        return (((((this.f5209a.hashCode() * 31) + this.b.hashCode()) * 31) + ((int) (this.c ^ (this.c >>> 32)))) * 31) + ((int) (this.d ^ (this.d >>> 32)));
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("BleCollectingConfig{settings=");
        sb.append(this.f5209a);
        sb.append(", scanFilters=");
        sb.append(this.b);
        sb.append(", sameBeaconMinReportingInterval=");
        sb.append(this.c);
        sb.append(", firstDelay=");
        sb.append(this.d);
        sb.append('}');
        return sb.toString();
    }
}
