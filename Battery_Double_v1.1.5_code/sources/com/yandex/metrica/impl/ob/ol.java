package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;

public class ol {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final cy f5031a;
    @NonNull
    private final vd b;
    @NonNull
    private final ow c;
    @NonNull
    private final om d;
    @NonNull
    private final od e;
    @NonNull
    private final wg f;
    @NonNull
    private final xh g;
    @Nullable
    private oh h;
    private boolean i;
    private final Runnable j;

    public ol(@NonNull Context context, @NonNull cy cyVar, @NonNull vd vdVar, @NonNull ow owVar, @NonNull od odVar, @NonNull xh xhVar, @Nullable oh ohVar) {
        Context context2 = context;
        this(cyVar, vdVar, owVar, new om(context), new wg(), odVar, xhVar, ohVar);
    }

    public void a() {
        c();
    }

    public void b() {
        f();
    }

    public void c() {
        boolean z = this.h != null && this.h.m;
        if (this.i != z) {
            this.i = z;
            if (this.i) {
                this.i = true;
                e();
                return;
            }
            this.i = false;
            f();
        }
    }

    /* access modifiers changed from: private */
    public void e() {
        if (this.h != null && this.h.l > 0) {
            this.g.a(this.j, this.h.l);
        }
    }

    private void f() {
        this.g.b(this.j);
    }

    public void a(@Nullable oh ohVar) {
        this.h = ohVar;
        c();
    }

    public void d() {
        final on onVar = new on();
        onVar.a(this.f.a());
        onVar.b(this.f.c());
        onVar.a(this.f5031a.a());
        this.b.a((uv) new uv() {
            public void a(uu[] uuVarArr) {
                onVar.b(vq.a(uuVarArr));
            }
        });
        this.d.a(onVar);
        this.c.a();
        this.e.a();
    }

    @VisibleForTesting
    ol(@NonNull cy cyVar, @NonNull vd vdVar, @NonNull ow owVar, @NonNull om omVar, @NonNull wg wgVar, @NonNull od odVar, @NonNull xh xhVar, @Nullable oh ohVar) {
        this.j = new Runnable() {
            public void run() {
                ol.this.d();
                ol.this.e();
            }
        };
        this.f5031a = cyVar;
        this.b = vdVar;
        this.c = owVar;
        this.d = omVar;
        this.f = wgVar;
        this.e = odVar;
        this.g = xhVar;
        this.h = ohVar;
    }
}
