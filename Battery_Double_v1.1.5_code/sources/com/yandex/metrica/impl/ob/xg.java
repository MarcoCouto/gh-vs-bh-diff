package com.yandex.metrica.impl.ob;

import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;

public class xg implements xi {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Looper f5301a;
    @NonNull
    private final Handler b;
    @NonNull
    private final xk c;

    public xg(@NonNull String str) {
        this(a(str));
    }

    @NonNull
    public Handler a() {
        return this.b;
    }

    @NonNull
    public Looper b() {
        return this.f5301a;
    }

    public void a(@NonNull Runnable runnable) {
        this.b.post(runnable);
    }

    public <T> Future<T> a(Callable<T> callable) {
        FutureTask futureTask = new FutureTask(callable);
        a((Runnable) futureTask);
        return futureTask;
    }

    public void a(@NonNull Runnable runnable, long j) {
        a(runnable, j, TimeUnit.MILLISECONDS);
    }

    public void a(@NonNull Runnable runnable, long j, @NonNull TimeUnit timeUnit) {
        this.b.postDelayed(runnable, timeUnit.toMillis(j));
    }

    public void b(@NonNull Runnable runnable) {
        this.b.removeCallbacks(runnable);
    }

    public boolean c() {
        return this.c.c();
    }

    private static xk a(@NonNull String str) {
        xk a2 = new xm(str).a();
        a2.start();
        return a2;
    }

    @VisibleForTesting
    xg(@NonNull xk xkVar) {
        this(xkVar, xkVar.getLooper(), new Handler(xkVar.getLooper()));
    }

    @VisibleForTesting
    public xg(@NonNull xk xkVar, @NonNull Looper looper, @NonNull Handler handler) {
        this.c = xkVar;
        this.f5301a = looper;
        this.b = handler;
    }
}
