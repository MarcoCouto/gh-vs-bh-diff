package com.yandex.metrica.impl.ob;

import android.location.Location;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

class op {
    @Nullable

    /* renamed from: a reason: collision with root package name */
    private String f5040a;
    @Nullable
    private oh b;
    @NonNull
    private oe c;
    @Nullable
    private Location d;
    private long e;
    @NonNull
    private cw f;
    @NonNull
    private ow g;
    @NonNull
    private od h;

    public op(@Nullable String str, @Nullable oh ohVar, @NonNull oe oeVar, @NonNull ow owVar, @NonNull od odVar) {
        this(str, ohVar, oeVar, null, 0, new cw(), owVar, odVar);
    }

    op(@Nullable String str, @Nullable oh ohVar, @NonNull oe oeVar, @Nullable Location location, long j, @NonNull cw cwVar, @NonNull ow owVar, @NonNull od odVar) {
        this.f5040a = str;
        this.b = ohVar;
        this.c = oeVar;
        this.d = location;
        this.e = j;
        this.f = cwVar;
        this.g = owVar;
        this.h = odVar;
    }

    public void a(@Nullable Location location) {
        if (d(location)) {
            b(location);
            c(location);
            b();
            a();
        }
    }

    private void a() {
        this.h.a();
    }

    private void b() {
        this.g.a();
    }

    private void b(@Nullable Location location) {
        this.d = location;
        this.e = System.currentTimeMillis();
    }

    private void c(@Nullable Location location) {
        this.c.a(this.f5040a, location, this.b);
    }

    private boolean d(@Nullable Location location) {
        if (!(location == null || this.b == null)) {
            if (this.d == null) {
                return true;
            }
            boolean c2 = c();
            boolean e2 = e(location);
            boolean f2 = f(location);
            if ((c2 || e2) && f2) {
                return true;
            }
        }
        return false;
    }

    private boolean c() {
        return this.f.b(this.e, this.b.e, "isSavedLocationOutdated");
    }

    private boolean e(Location location) {
        return g(location) > this.b.f;
    }

    private boolean f(@NonNull Location location) {
        return this.d == null || location.getTime() - this.d.getTime() >= 0;
    }

    private float g(Location location) {
        return location.distanceTo(this.d);
    }

    public void a(@Nullable oh ohVar) {
        this.b = ohVar;
    }
}
