package com.yandex.metrica.impl.ob;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;

public class ju {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private Context f4924a;
    @NonNull
    private ServiceConnection b;
    @NonNull
    private final yc c;

    public ju(@NonNull Context context) {
        this(context, new ServiceConnection() {
            public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            }

            public void onServiceDisconnected(ComponentName componentName) {
            }
        }, new yc());
    }

    public void a(@Nullable String str) {
        if (!TextUtils.isEmpty(str)) {
            Intent a2 = a(this.f4924a, str);
            if (a2 != null) {
                this.f4924a.bindService(a2, this.b, 1);
            }
        }
    }

    public void a() {
        this.f4924a.unbindService(this.b);
    }

    @Nullable
    private Intent a(@NonNull Context context, @NonNull String str) {
        try {
            ResolveInfo resolveService = context.getPackageManager().resolveService(a(context).setPackage(str), 0);
            if (resolveService != null) {
                return new Intent().setClassName(resolveService.serviceInfo.packageName, resolveService.serviceInfo.name).setAction("com.yandex.metrica.ACTION_C_BG_L");
            }
            return null;
        } catch (Throwable unused) {
            return null;
        }
    }

    @NonNull
    private Intent a(@NonNull Context context) {
        StringBuilder sb = new StringBuilder();
        sb.append("metrica://");
        sb.append(context.getPackageName());
        Intent intent = new Intent("com.yandex.metrica.IMetricaService", Uri.parse(sb.toString()));
        a(intent);
        return intent;
    }

    @SuppressLint({"ObsoleteSdkInt"})
    private void a(@NonNull Intent intent) {
        if (VERSION.SDK_INT >= 12) {
            b(intent);
        }
    }

    @TargetApi(12)
    private void b(@NonNull Intent intent) {
        intent.addFlags(32);
    }

    @VisibleForTesting
    ju(@NonNull Context context, @NonNull ServiceConnection serviceConnection, @NonNull yc ycVar) {
        this.f4924a = context;
        this.b = serviceConnection;
        this.c = ycVar;
    }
}
