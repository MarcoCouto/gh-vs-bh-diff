package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.support.v7.widget.helper.ItemTouchHelper.Callback;
import java.util.HashMap;

public class r extends w {
    private HashMap<a, Integer> f;
    private yb<String> g;
    private yb<String> h;
    private yb<byte[]> i;
    private yb<String> j;
    private yb<String> k;

    public enum a {
        NAME,
        VALUE,
        USER_INFO
    }

    @VisibleForTesting
    public r(@NonNull vz vzVar) {
        this.f = new HashMap<>();
        b(vzVar);
    }

    public r(String str, int i2, @NonNull vz vzVar) {
        this("", str, i2, vzVar);
    }

    public r(String str, String str2, int i2, @NonNull vz vzVar) {
        this(str, str2, i2, 0, vzVar);
    }

    public r(String str, String str2, int i2, int i3, @NonNull vz vzVar) {
        this.f = new HashMap<>();
        b(vzVar);
        this.b = g(str);
        this.f5284a = f(str2);
        this.c = i2;
        this.d = i3;
    }

    public r(byte[] bArr, String str, int i2, @NonNull vz vzVar) {
        this.f = new HashMap<>();
        b(vzVar);
        a(bArr);
        this.f5284a = f(str);
        this.c = i2;
    }

    public r a(@NonNull HashMap<a, Integer> hashMap) {
        this.f = hashMap;
        return this;
    }

    @NonNull
    public HashMap<a, Integer> a() {
        return this.f;
    }

    private void b(@NonNull vz vzVar) {
        this.g = new xz(1000, "event name", vzVar);
        this.h = new xy(245760, "event value", vzVar);
        this.i = new xr(245760, "event value bytes", vzVar);
        this.j = new xz(Callback.DEFAULT_DRAG_ANIMATION_DURATION, "user profile id", vzVar);
        this.k = new xz(10000, "UserInfo", vzVar);
    }

    private void a(String str, String str2, a aVar) {
        if (xu.a(str, str2)) {
            this.f.put(aVar, Integer.valueOf(cu.c(str).length - cu.c(str2).length));
        } else {
            this.f.remove(aVar);
        }
        u();
    }

    private void a(byte[] bArr, byte[] bArr2, a aVar) {
        if (bArr.length != bArr2.length) {
            this.f.put(aVar, Integer.valueOf(bArr.length - bArr2.length));
        } else {
            this.f.remove(aVar);
        }
        u();
    }

    private void u() {
        this.e = 0;
        for (Integer intValue : this.f.values()) {
            this.e += intValue.intValue();
        }
    }

    private String f(String str) {
        String str2 = (String) this.g.a(str);
        a(str, str2, a.NAME);
        return str2;
    }

    private String g(String str) {
        String str2 = (String) this.h.a(str);
        a(str, str2, a.VALUE);
        return str2;
    }

    private byte[] b(byte[] bArr) {
        byte[] bArr2 = (byte[]) this.i.a(bArr);
        a(bArr, bArr2, a.VALUE);
        return bArr2;
    }

    public w a(String str) {
        String str2 = (String) this.k.a(str);
        a(str, str2, a.USER_INFO);
        return super.a(str2);
    }

    public w b(String str) {
        return super.b(f(str));
    }

    public w c(String str) {
        return super.c(g(str));
    }

    public final w a(@Nullable byte[] bArr) {
        return super.a(b(bArr));
    }

    @NonNull
    public w d(@Nullable String str) {
        return super.d((String) this.j.a(str));
    }

    public static w a(String str, String str2) {
        return new w().a(com.yandex.metrica.impl.ob.af.a.EVENT_TYPE_APP_ENVIRONMENT_UPDATED.a()).b(str, str2);
    }

    public static w b() {
        return new w().a(com.yandex.metrica.impl.ob.af.a.EVENT_TYPE_APP_ENVIRONMENT_CLEARED.a());
    }

    public static w c() {
        return new w().a(com.yandex.metrica.impl.ob.af.a.EVENT_TYPE_SEND_USER_PROFILE.a());
    }

    @NonNull
    static w a(@Nullable String str, @NonNull vz vzVar) {
        return new r(vzVar).a(com.yandex.metrica.impl.ob.af.a.EVENT_TYPE_SET_USER_PROFILE_ID.a()).d(str);
    }

    @NonNull
    static w a(@NonNull vz vzVar) {
        return new r(vzVar).a(com.yandex.metrica.impl.ob.af.a.EVENT_TYPE_SEND_REVENUE_EVENT.a());
    }
}
