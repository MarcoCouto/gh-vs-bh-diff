package com.yandex.metrica.impl.ob;

import android.support.annotation.Nullable;

class yn implements yk<Integer> {
    yn() {
    }

    public yi a(@Nullable Integer num) {
        if (num == null || num.intValue() > 0) {
            return yi.a(this);
        }
        StringBuilder sb = new StringBuilder();
        sb.append("Invalid quantity value ");
        sb.append(num);
        return yi.a(this, sb.toString());
    }
}
