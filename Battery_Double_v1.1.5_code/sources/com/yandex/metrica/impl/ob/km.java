package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import com.yandex.metrica.impl.ob.rg.a;
import com.yandex.metrica.impl.ob.rg.e;

public class km implements mq<kg, a> {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private kr f4943a;

    public km(@NonNull kr krVar) {
        this.f4943a = krVar;
    }

    @NonNull
    /* renamed from: a */
    public a b(@NonNull kg kgVar) {
        a aVar = new a();
        aVar.b = this.f4943a.b(kgVar.f4937a);
        aVar.c = new e[kgVar.b.size()];
        int i = 0;
        for (kk a2 : kgVar.b) {
            aVar.c[i] = this.f4943a.b(a2);
            i++;
        }
        return aVar;
    }

    @NonNull
    public kg a(@NonNull a aVar) {
        throw new UnsupportedOperationException();
    }
}
