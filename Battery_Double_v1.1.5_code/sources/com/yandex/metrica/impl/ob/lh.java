package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;

public class lh extends kw {
    @NonNull
    public String e() {
        return "l_dat";
    }

    lh(@NonNull Context context, @NonNull lc lcVar) {
        this(lcVar, new ly(ld.a(context).c()));
    }

    @VisibleForTesting
    lh(@NonNull lc lcVar, @NonNull ly lyVar) {
        super(lcVar, lyVar);
    }

    /* access modifiers changed from: protected */
    public long c(long j) {
        return c().d(j);
    }

    /* access modifiers changed from: protected */
    @NonNull
    public ly d(long j) {
        return c().e(j);
    }
}
