package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;

public class cw {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final wh f4755a;

    private boolean a(long j, long j2, long j3, @NonNull String str) {
        boolean z = true;
        if (j < j2) {
            return true;
        }
        if (j - j2 < j3) {
            z = false;
        }
        return z;
    }

    public cw() {
        this(new wg());
    }

    @VisibleForTesting
    cw(@NonNull wh whVar) {
        this.f4755a = whVar;
    }

    public boolean a(long j, long j2, @NonNull String str) {
        return a(this.f4755a.b(), j, j2, str);
    }

    public boolean b(long j, long j2, @NonNull String str) {
        return a(this.f4755a.a(), j, j2, str);
    }
}
