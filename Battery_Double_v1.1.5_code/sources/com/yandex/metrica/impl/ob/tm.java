package com.yandex.metrica.impl.ob;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.util.Base64;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONException;

class tm extends to {
    @NonNull
    private ub c;
    @NonNull
    private tn d;

    tm(@NonNull Socket socket, @NonNull Uri uri, @NonNull tr trVar, @NonNull ub ubVar, @NonNull tn tnVar) {
        super(socket, uri, trVar);
        this.c = ubVar;
        this.d = tnVar;
    }

    public void a() {
        if (this.c.b.equals(this.b.getQueryParameter("t"))) {
            try {
                final byte[] b = b();
                a("HTTP/1.1 200 OK", (Map<String, String>) new HashMap<String, String>() {
                    {
                        put("Content-Type", "text/plain; charset=utf-8");
                        put("Access-Control-Allow-Origin", "*");
                        put("Access-Control-Allow-Methods", HttpRequest.METHOD_GET);
                        put(HttpRequest.HEADER_CONTENT_LENGTH, String.valueOf(b.length));
                    }
                }, b);
            } catch (Throwable unused) {
            }
        } else {
            this.f5195a.a("request_with_wrong_token");
        }
    }

    /* access modifiers changed from: protected */
    public byte[] b() throws JSONException {
        return Base64.encode(new wt().a(this.d.a().getBytes()), 0);
    }
}
