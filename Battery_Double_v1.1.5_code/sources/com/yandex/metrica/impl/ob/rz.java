package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.metrica.IIdentifierCallback;
import com.yandex.metrica.YandexMetricaConfig;
import com.yandex.metrica.impl.interact.CellularNetworkInfo;
import com.yandex.metrica.impl.interact.DeviceInfo;
import com.yandex.metrica.j;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;

public class rz {
    /* access modifiers changed from: private */
    @NonNull

    /* renamed from: a reason: collision with root package name */
    public final sa f5120a;
    @NonNull
    private final xh b;
    @NonNull
    private final rt c;
    @NonNull
    private final yk<Context> d;
    @NonNull
    private final yk<String> e;
    @NonNull
    private final a f;

    static class a {
        a() {
        }

        public ok a(@NonNull Context context, @Nullable LocationManager locationManager) {
            return new ok(context, locationManager, new pr(new pm()));
        }
    }

    public void a(@NonNull Context context, @NonNull Object obj) {
    }

    public void b(@NonNull Context context, @NonNull Object obj) {
    }

    public rz(@NonNull xh xhVar) {
        this(xhVar, new sa());
    }

    public rz(@NonNull xh xhVar, @NonNull sa saVar) {
        this(xhVar, saVar, new rt(saVar), new yg(new yf("Context")), new yg(new yf("Event name")), new a());
    }

    public rz(@NonNull xh xhVar, @NonNull sa saVar, @NonNull rt rtVar, @NonNull yk<Context> ykVar, @NonNull yk<String> ykVar2, @NonNull a aVar) {
        this.f5120a = saVar;
        this.b = xhVar;
        this.c = rtVar;
        this.d = ykVar;
        this.e = ykVar2;
        this.f = aVar;
    }

    @Deprecated
    public void a(final IIdentifierCallback iIdentifierCallback, @NonNull final List<String> list) {
        this.b.a((Runnable) new wb() {
            public void a() throws Exception {
                if (rz.this.f5120a.d()) {
                    rz.this.f5120a.a().a(iIdentifierCallback, list);
                }
            }
        });
    }

    public void a(@NonNull final Context context, @NonNull final IIdentifierCallback iIdentifierCallback, @NonNull final List<String> list) {
        this.d.a(context);
        this.b.a((Runnable) new wb() {
            public void a() throws Exception {
                rz.this.f5120a.a(context).a(iIdentifierCallback, list);
            }
        });
    }

    public boolean a() {
        return this.f5120a.c();
    }

    @Nullable
    public Future<String> b() {
        return this.b.a((Callable<T>) new wa<String>() {
            /* renamed from: a */
            public String b() {
                return com.yandex.metrica.impl.ac.a.b().c();
            }
        });
    }

    @Nullable
    public Future<Boolean> c() {
        return this.b.a((Callable<T>) new wa<Boolean>() {
            /* renamed from: a */
            public Boolean b() {
                return com.yandex.metrica.impl.ac.a.b().d();
            }
        });
    }

    @NonNull
    public DeviceInfo a(Context context) {
        this.d.a(context);
        return DeviceInfo.getInstance(context);
    }

    @NonNull
    public String b(Context context) {
        this.d.a(context);
        return new CellularNetworkInfo(context).getCelluralInfo();
    }

    @Nullable
    public Integer c(Context context) {
        this.d.a(context);
        return bt.c(context);
    }

    @Nullable
    @Deprecated
    public String d() {
        if (this.f5120a.d()) {
            return this.f5120a.a().j();
        }
        return null;
    }

    @Nullable
    public String d(@NonNull Context context) {
        this.d.a(context);
        return this.f5120a.a(context).j();
    }

    @Nullable
    public String e(@NonNull Context context) {
        this.d.a(context);
        return this.f5120a.a(context).i();
    }

    @NonNull
    public String f(@NonNull Context context) {
        this.d.a(context);
        return context.getPackageName();
    }

    public void a(int i, @NonNull String str, @Nullable String str2, @Nullable Map<String, String> map) {
        this.c.a();
        this.e.a(str);
        xh xhVar = this.b;
        final int i2 = i;
        final String str3 = str;
        final String str4 = str2;
        final Map<String, String> map2 = map;
        AnonymousClass5 r1 = new wb() {
            public void a() throws Exception {
                rz.this.f5120a.e().a(i2, str3, str4, map2);
            }
        };
        xhVar.a((Runnable) r1);
    }

    public void e() {
        this.c.a();
        this.b.a((Runnable) new wb() {
            public void a() throws Exception {
                rz.this.f5120a.e().sendEventsBuffer();
            }
        });
    }

    @NonNull
    public String a(@Nullable String str) {
        return ci.a(str);
    }

    @NonNull
    public String a(int i) {
        return bw.a(i);
    }

    @NonNull
    public YandexMetricaConfig a(@NonNull YandexMetricaConfig yandexMetricaConfig, @NonNull String str) {
        return j.b(yandexMetricaConfig).a(Collections.singletonList(str)).b();
    }

    @NonNull
    public YandexMetricaConfig a(@NonNull YandexMetricaConfig yandexMetricaConfig, @NonNull List<String> list) {
        return j.b(yandexMetricaConfig).a(list).b();
    }

    @Nullable
    public Location g(@NonNull Context context) {
        LocationManager locationManager;
        this.d.a(context);
        try {
            locationManager = (LocationManager) context.getSystemService("location");
        } catch (Throwable unused) {
            locationManager = null;
        }
        return this.f.a(context, locationManager).a();
    }
}
