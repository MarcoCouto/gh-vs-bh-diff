package com.yandex.metrica.impl.ob;

import android.annotation.TargetApi;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.telephony.CellIdentityGsm;
import android.telephony.CellIdentityLte;
import android.telephony.CellIdentityWcdma;
import android.telephony.CellInfo;
import android.telephony.CellInfoCdma;
import android.telephony.CellInfoGsm;
import android.telephony.CellInfoLte;
import android.telephony.CellInfoWcdma;
import android.telephony.CellSignalStrength;
import java.util.concurrent.TimeUnit;

public class uu {
    @Nullable

    /* renamed from: a reason: collision with root package name */
    private Integer f5242a;
    @Nullable
    private final Integer b;
    @Nullable
    private final Integer c;
    @Nullable
    private final Integer d;
    @Nullable
    private final Integer e;
    @Nullable
    private final String f;
    @Nullable
    private final String g;
    private final boolean h;
    private final int i;
    @Nullable
    private final Integer j;
    @Nullable
    private final Long k;

    @TargetApi(17)
    static class a extends b {
        a() {
        }

        /* access modifiers changed from: 0000 */
        public uu a(CellInfo cellInfo) {
            CellInfoCdma cellInfoCdma = (CellInfoCdma) cellInfo;
            return a(null, null, cellInfoCdma.getCellSignalStrength(), null, null, cellInfoCdma.isRegistered(), 2, null, cellInfoCdma.getTimeStamp());
        }
    }

    @TargetApi(17)
    static abstract class b {

        /* renamed from: a reason: collision with root package name */
        static final Integer f5243a = Integer.valueOf(Integer.MAX_VALUE);
        static final Integer b = Integer.valueOf(Integer.MAX_VALUE);
        static final Integer c = Integer.valueOf(Integer.MAX_VALUE);
        static final Integer d = Integer.valueOf(Integer.MAX_VALUE);
        static final Integer e = Integer.valueOf(Integer.MAX_VALUE);
        @NonNull
        private final wf f;

        /* access modifiers changed from: 0000 */
        public abstract uu a(CellInfo cellInfo);

        public b() {
            this(new wf());
        }

        /* access modifiers changed from: protected */
        @TargetApi(17)
        public uu a(@Nullable Integer num, @Nullable Integer num2, @Nullable CellSignalStrength cellSignalStrength, @Nullable Integer num3, @Nullable Integer num4, boolean z, int i, @Nullable Integer num5, long j) {
            Integer num6;
            Integer num7;
            Integer num8;
            Integer num9;
            Integer num10 = num;
            Integer num11 = num2;
            Integer num12 = num3;
            Integer num13 = num4;
            Integer num14 = num5;
            if (num10 != null) {
                if (num10 == f5243a) {
                    num10 = null;
                }
                num6 = num10;
            } else {
                num6 = null;
            }
            if (num11 != null) {
                if (num11 == b) {
                    num11 = null;
                }
                num7 = num11;
            } else {
                num7 = null;
            }
            Integer valueOf = cellSignalStrength != null ? Integer.valueOf(cellSignalStrength.getDbm()) : null;
            if (num13 != null) {
                if (num13 == c) {
                    num13 = null;
                }
                num8 = num13;
            } else {
                num8 = null;
            }
            if (num12 != null) {
                if (num12 == d) {
                    num12 = null;
                }
                num9 = num12;
            } else {
                num9 = null;
            }
            uu uuVar = new uu(num8, num9, num7, num6, null, null, valueOf, z, i, (num14 == null || num14 == e) ? null : num14, a(j));
            return uuVar;
        }

        @Nullable
        private Long a(long j) {
            Long l = null;
            if (j <= 0) {
                return null;
            }
            long d2 = this.f.d(j, TimeUnit.NANOSECONDS);
            if (d2 > 0 && d2 < TimeUnit.HOURS.toSeconds(1)) {
                l = Long.valueOf(d2);
            }
            if (l != null) {
                return l;
            }
            long a2 = this.f.a(j, TimeUnit.NANOSECONDS);
            return (a2 <= 0 || a2 >= TimeUnit.HOURS.toSeconds(1)) ? l : Long.valueOf(a2);
        }

        @VisibleForTesting
        public b(@NonNull wf wfVar) {
            this.f = wfVar;
        }
    }

    @TargetApi(17)
    static class c extends b {
        c() {
        }

        /* access modifiers changed from: 0000 */
        public uu a(CellInfo cellInfo) {
            CellInfoGsm cellInfoGsm = (CellInfoGsm) cellInfo;
            CellIdentityGsm cellIdentity = cellInfoGsm.getCellIdentity();
            return a(Integer.valueOf(cellIdentity.getCid()), Integer.valueOf(cellIdentity.getLac()), cellInfoGsm.getCellSignalStrength(), Integer.valueOf(cellIdentity.getMnc()), Integer.valueOf(cellIdentity.getMcc()), cellInfoGsm.isRegistered(), 1, null, cellInfoGsm.getTimeStamp());
        }
    }

    @TargetApi(17)
    static class d extends b {
        d() {
        }

        /* access modifiers changed from: 0000 */
        public uu a(CellInfo cellInfo) {
            CellInfoLte cellInfoLte = (CellInfoLte) cellInfo;
            CellIdentityLte cellIdentity = cellInfoLte.getCellIdentity();
            return a(Integer.valueOf(cellIdentity.getCi()), Integer.valueOf(cellIdentity.getTac()), cellInfoLte.getCellSignalStrength(), Integer.valueOf(cellIdentity.getMnc()), Integer.valueOf(cellIdentity.getMcc()), cellInfoLte.isRegistered(), 4, Integer.valueOf(cellIdentity.getPci()), cellInfoLte.getTimeStamp());
        }
    }

    @TargetApi(18)
    static class e extends b {
        e() {
        }

        /* access modifiers changed from: 0000 */
        public uu a(CellInfo cellInfo) {
            CellInfoWcdma cellInfoWcdma = (CellInfoWcdma) cellInfo;
            CellIdentityWcdma cellIdentity = cellInfoWcdma.getCellIdentity();
            return a(Integer.valueOf(cellIdentity.getCid()), Integer.valueOf(cellIdentity.getLac()), cellInfoWcdma.getCellSignalStrength(), Integer.valueOf(cellIdentity.getMnc()), Integer.valueOf(cellIdentity.getMcc()), cellInfoWcdma.isRegistered(), 3, Integer.valueOf(cellIdentity.getPsc()), cellInfoWcdma.getTimeStamp());
        }
    }

    public uu(@Nullable Integer num, @Nullable Integer num2, @Nullable Integer num3, @Nullable Integer num4, @Nullable String str, @Nullable String str2, @Nullable Integer num5, boolean z, int i2, @Nullable Integer num6, @Nullable Long l) {
        this.b = num;
        this.c = num2;
        this.d = num3;
        this.e = num4;
        this.f = str;
        this.g = str2;
        this.f5242a = num5;
        this.h = z;
        this.i = i2;
        this.j = num6;
        this.k = l;
    }

    @TargetApi(17)
    public static uu a(CellInfo cellInfo) {
        b b2 = b(cellInfo);
        if (b2 == null) {
            return null;
        }
        return b2.a(cellInfo);
    }

    @TargetApi(17)
    public static b b(CellInfo cellInfo) {
        if (cellInfo instanceof CellInfoGsm) {
            return new c();
        }
        if (cellInfo instanceof CellInfoCdma) {
            return new a();
        }
        if (cellInfo instanceof CellInfoLte) {
            return new d();
        }
        if (!cx.a(18) || !(cellInfo instanceof CellInfoWcdma)) {
            return null;
        }
        return new e();
    }

    @Nullable
    public Integer a() {
        return this.f5242a;
    }

    @Nullable
    public Integer b() {
        return this.b;
    }

    @Nullable
    public Integer c() {
        return this.c;
    }

    @Nullable
    public Integer d() {
        return this.d;
    }

    @Nullable
    public Integer e() {
        return this.e;
    }

    @Nullable
    public String f() {
        return this.f;
    }

    @Nullable
    public String g() {
        return this.g;
    }

    public boolean h() {
        return this.h;
    }

    public void a(@Nullable Integer num) {
        this.f5242a = num;
    }

    public int i() {
        return this.i;
    }

    @Nullable
    public Integer j() {
        return this.j;
    }

    @Nullable
    public Long k() {
        return this.k;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("CellDescription{mSignalStrength=");
        sb.append(this.f5242a);
        sb.append(", mMobileCountryCode=");
        sb.append(this.b);
        sb.append(", mMobileNetworkCode=");
        sb.append(this.c);
        sb.append(", mLocationAreaCode=");
        sb.append(this.d);
        sb.append(", mCellId=");
        sb.append(this.e);
        sb.append(", mOperatorName='");
        sb.append(this.f);
        sb.append('\'');
        sb.append(", mNetworkType='");
        sb.append(this.g);
        sb.append('\'');
        sb.append(", mConnected=");
        sb.append(this.h);
        sb.append(", mCellType=");
        sb.append(this.i);
        sb.append(", mPci=");
        sb.append(this.j);
        sb.append(", mLastVisibleTimeOffset=");
        sb.append(this.k);
        sb.append('}');
        return sb.toString();
    }
}
