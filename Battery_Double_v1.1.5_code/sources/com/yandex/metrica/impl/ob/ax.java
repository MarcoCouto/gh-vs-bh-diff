package com.yandex.metrica.impl.ob;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.location.Location;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import com.yandex.browser.crashreports.a.C0101a;
import com.yandex.metrica.CounterConfiguration;
import com.yandex.metrica.a;
import com.yandex.metrica.j;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

public class ax extends n implements ba {
    private static final yk<String> f = new yg(new ye("Deeplink"));
    @Deprecated
    private static final yk<String> g = new yg(new ye("Referral url"));
    @NonNull
    private final a h;
    @NonNull
    private final sg i;
    @NonNull
    private final j j;
    @NonNull
    private final uf k;
    @NonNull
    private com.yandex.browser.crashreports.a l;
    private final AtomicBoolean m;
    /* access modifiers changed from: private */
    public final dq n;

    public ax(@NonNull Context context, @NonNull ee eeVar, @NonNull j jVar, @NonNull cd cdVar, @NonNull uf ufVar, @NonNull cb cbVar, @NonNull cb cbVar2) {
        long j2;
        Context context2 = context;
        j jVar2 = jVar;
        ee eeVar2 = eeVar;
        bz bzVar = new bz(eeVar, new CounterConfiguration(jVar2));
        if (jVar2.sessionTimeout == null) {
            j2 = TimeUnit.SECONDS.toMillis(10);
        } else {
            j2 = (long) jVar2.sessionTimeout.intValue();
        }
        this(context, jVar, cdVar, bzVar, new a(j2), new sg(context), ufVar, new av(), cbVar, cbVar2, db.a(), new ag(context));
    }

    @VisibleForTesting
    ax(@NonNull Context context, @NonNull j jVar, @NonNull cd cdVar, @NonNull bz bzVar, @NonNull a aVar, @NonNull sg sgVar, @NonNull uf ufVar, @NonNull av avVar, @NonNull cb cbVar, @NonNull cb cbVar2, @NonNull xh xhVar, @NonNull ag agVar) {
        j jVar2 = jVar;
        Context context2 = context;
        super(context, cdVar, bzVar, agVar);
        this.m = new AtomicBoolean(false);
        this.n = new dq();
        this.b.a(new bu(jVar2.preloadInfo, this.c));
        this.h = aVar;
        this.i = sgVar;
        this.j = jVar2;
        this.k = ufVar;
        boolean a2 = wk.a(jVar2.nativeCrashReporting, true);
        this.e.a(a2, this.b);
        if (this.c.c()) {
            this.c.a("Set report native crashes enabled: %b", Boolean.valueOf(a2));
        }
        this.i.a(aVar, this.j, this.j.m, ufVar.b(), this.c);
        this.l = a(xhVar, avVar, cbVar, cbVar2);
        if (vi.a(jVar2.l)) {
            g();
        }
    }

    public void reportError(String str, Throwable th) {
        super.reportError(str, th);
    }

    public final void g() {
        if (this.m.compareAndSet(false, true)) {
            this.l.a();
        }
    }

    public void a(Activity activity) {
        if (activity != null) {
            if (activity.getIntent() != null) {
                String dataString = activity.getIntent().getDataString();
                if (!TextUtils.isEmpty(dataString)) {
                    this.e.a(af.g(dataString, this.c), this.b);
                }
                g(dataString);
            }
        } else if (this.c.c()) {
            this.c.b("Null activity parameter for reportAppOpen(Activity)");
        }
    }

    private void g(@Nullable String str) {
        if (this.c.c()) {
            StringBuilder sb = new StringBuilder("App opened ");
            sb.append(" via deeplink: ");
            sb.append(d(str));
            this.c.a(sb.toString());
        }
    }

    public void e(String str) {
        f.a(str);
        this.e.a(af.g(str, this.c), this.b);
        g(str);
    }

    @Deprecated
    public void f(String str) {
        g.a(str);
        this.e.a(af.h(str, this.c), this.b);
        h(str);
    }

    @Deprecated
    private void h(@Nullable String str) {
        if (this.c.c()) {
            StringBuilder sb = new StringBuilder("Referral URL received: ");
            sb.append(d(str));
            this.c.a(sb.toString());
        }
    }

    public void a(Application application, @NonNull xh xhVar) {
        if (VERSION.SDK_INT >= 14) {
            if (this.c.c()) {
                this.c.a("Enable activity auto tracking");
            }
            b(application, xhVar);
        } else if (this.c.c()) {
            this.c.b("Could not enable activity auto tracking. API level should be more than 14 (ICE_CREAM_SANDWICH)");
        }
    }

    @TargetApi(14)
    private void b(Application application, @NonNull xh xhVar) {
        application.registerActivityLifecycleCallbacks(new z(this, xhVar));
    }

    public void b(Activity activity) {
        a(d(activity));
        this.h.a();
    }

    public void c(Activity activity) {
        b(d(activity));
        this.h.b();
    }

    /* access modifiers changed from: 0000 */
    public String d(Activity activity) {
        if (activity != null) {
            return activity.getClass().getSimpleName();
        }
        return null;
    }

    /* access modifiers changed from: 0000 */
    public void a(j jVar, boolean z) {
        if (z) {
            b();
        }
        b(jVar.i);
        a(jVar.h);
    }

    public void a(Location location) {
        this.b.h().a(location);
        if (this.c.c()) {
            vz vzVar = this.c;
            StringBuilder sb = new StringBuilder();
            sb.append("Set location: %s");
            sb.append(location.toString());
            vzVar.a(sb.toString(), new Object[0]);
        }
    }

    public void a(boolean z) {
        this.b.h().a(z);
    }

    @NonNull
    private com.yandex.browser.crashreports.a a(@NonNull xh xhVar, @NonNull av avVar, @NonNull cb cbVar, @NonNull cb cbVar2) {
        final xh xhVar2 = xhVar;
        final av avVar2 = avVar;
        final cb cbVar3 = cbVar;
        final cb cbVar4 = cbVar2;
        AnonymousClass1 r1 = new C0101a() {
            public void a() {
                final kg a2 = ax.this.n.a();
                xhVar2.a((Runnable) new Runnable() {
                    public void run() {
                        ax.this.a(a2);
                        if (avVar2.a(a2.f4937a.f)) {
                            cbVar3.a().a(a2);
                        }
                        if (avVar2.b(a2.f4937a.f)) {
                            cbVar4.a().a(a2);
                        }
                    }
                });
            }
        };
        return new com.yandex.browser.crashreports.a(r1);
    }
}
