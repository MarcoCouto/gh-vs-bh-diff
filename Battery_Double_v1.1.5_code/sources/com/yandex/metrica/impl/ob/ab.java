package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import java.util.List;
import java.util.Map;
import org.json.JSONObject;

public class ab implements bn {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final a f4630a;

    static class a {

        /* renamed from: com.yandex.metrica.impl.ob.ab$a$a reason: collision with other inner class name */
        static class C0107a {
            @NonNull

            /* renamed from: a reason: collision with root package name */
            final String f4631a;

            public C0107a(@NonNull String str) {
                this.f4631a = str;
            }
        }

        a() {
        }

        public C0107a a(@Nullable byte[] bArr) {
            try {
                if (!cx.a(bArr)) {
                    return new C0107a(new JSONObject(new String(bArr, "UTF-8")).optString("status"));
                }
                return null;
            } catch (Throwable unused) {
                return null;
            }
        }
    }

    public ab() {
        this(new a());
    }

    @VisibleForTesting
    ab(@NonNull a aVar) {
        this.f4630a = aVar;
    }

    public boolean a(int i, @Nullable byte[] bArr, @Nullable Map<String, List<String>> map) {
        if (200 == i) {
            C0107a a2 = this.f4630a.a(bArr);
            if (a2 != null) {
                return "accepted".equals(a2.f4631a);
            }
        }
        return false;
    }
}
