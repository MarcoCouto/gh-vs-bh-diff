package com.yandex.metrica.impl.ob;

public enum jh {
    FOREGROUND(0),
    BACKGROUND(1);
    
    private final int c;

    private jh(int i) {
        this.c = i;
    }

    public int a() {
        return this.c;
    }

    public static jh a(Integer num) {
        jh jhVar = FOREGROUND;
        if (num == null) {
            return jhVar;
        }
        switch (num.intValue()) {
            case 0:
                return FOREGROUND;
            case 1:
                return BACKGROUND;
            default:
                return jhVar;
        }
    }
}
