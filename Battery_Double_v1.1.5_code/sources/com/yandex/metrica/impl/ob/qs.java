package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import com.yandex.metrica.impl.ob.rl.a.C0115a;

public final class qs extends qr<Double> {
    public qs(@NonNull String str, double d) {
        super(2, str, Double.valueOf(d), new qu(), new qq(new qv(new xs(100))));
    }

    /* access modifiers changed from: protected */
    public void a(@NonNull C0115a aVar) {
        aVar.e.d += ((Double) b()).doubleValue();
    }
}
