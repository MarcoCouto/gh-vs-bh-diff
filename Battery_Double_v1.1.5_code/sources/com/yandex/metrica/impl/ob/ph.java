package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.metrica.impl.ob.sn.d;
import java.util.List;

public class ph extends sn {
    @Nullable

    /* renamed from: a reason: collision with root package name */
    private final oh f5057a;

    public static class a {

        /* renamed from: a reason: collision with root package name */
        public final uk f5058a;
        public final oh b;

        public a(uk ukVar, oh ohVar) {
            this.f5058a = ukVar;
            this.b = ohVar;
        }
    }

    protected static class b implements d<ph, a> {
        @NonNull

        /* renamed from: a reason: collision with root package name */
        private final Context f5059a;

        protected b(@NonNull Context context) {
            this.f5059a = context;
        }

        @NonNull
        public ph a(a aVar) {
            ph phVar = new ph(aVar.b);
            phVar.d(cx.b(this.f5059a, this.f5059a.getPackageName()));
            phVar.c(cx.a(this.f5059a, this.f5059a.getPackageName()));
            phVar.i(wk.b(v.a(this.f5059a).a(aVar.f5058a), ""));
            phVar.a(aVar.f5058a);
            phVar.a(v.a(this.f5059a));
            phVar.b(this.f5059a.getPackageName());
            phVar.e(aVar.f5058a.f5236a);
            phVar.f(aVar.f5058a.b);
            phVar.g(aVar.f5058a.c);
            phVar.a(com.yandex.metrica.impl.ac.a.a().c(this.f5059a));
            return phVar;
        }
    }

    private ph(@Nullable oh ohVar) {
        this.f5057a = ohVar;
    }

    @Nullable
    public oh a() {
        return this.f5057a;
    }

    @Nullable
    public List<String> b() {
        return e().i;
    }
}
