package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.content.SharedPreferences;

public class ql {
    public static SharedPreferences a(Context context, String str) {
        StringBuilder sb = new StringBuilder();
        sb.append(context.getPackageName());
        sb.append(str);
        return context.getSharedPreferences(sb.toString(), 0);
    }

    public static void a(SharedPreferences sharedPreferences, String str, int i) {
        if (sharedPreferences != null && sharedPreferences.contains(str)) {
            try {
                sharedPreferences.edit().remove(str).putLong(str, (long) sharedPreferences.getInt(str, i)).apply();
            } catch (ClassCastException unused) {
            }
        }
    }
}
