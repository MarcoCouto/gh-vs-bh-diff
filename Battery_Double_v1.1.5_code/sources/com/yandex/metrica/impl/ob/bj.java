package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import java.io.File;
import java.io.FilenameFilter;

public class bj {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Context f4687a;
    @NonNull
    private final String b;
    @NonNull
    private final bk c;
    /* access modifiers changed from: private */
    @NonNull
    public final lw d;
    @NonNull
    private final ah e;

    public bj(@NonNull Context context, @NonNull bk bkVar, @NonNull lw lwVar) {
        this(context, new ah(), bkVar, lwVar);
    }

    @VisibleForTesting
    bj(@NonNull Context context, @NonNull ah ahVar, @NonNull bk bkVar, @NonNull lw lwVar) {
        this.f4687a = context;
        this.e = ahVar;
        this.b = ahVar.c(context).getAbsolutePath();
        this.c = bkVar;
        this.d = lwVar;
    }

    public synchronized void a() {
        if (am.a() && !this.d.m()) {
            StringBuilder sb = new StringBuilder();
            sb.append(this.f4687a.getFilesDir().getAbsolutePath());
            sb.append("/");
            sb.append("YandexMetricaNativeCrashes");
            a(sb.toString(), new wm<Boolean>() {
                public void a(Boolean bool) {
                    bj.this.d.n();
                }
            });
        }
        a(this.b, new wm<Boolean>() {
            public void a(Boolean bool) {
            }
        });
    }

    private void a(@NonNull String str, @NonNull wm<Boolean> wmVar) {
        String[] a2;
        for (String str2 : a(str)) {
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append("/");
            sb.append(str2);
            a(sb.toString(), wmVar, false);
        }
    }

    private String[] a(String str) {
        File a2 = this.e.a(str);
        if (!a2.mkdir() && !a2.exists()) {
            return new String[0];
        }
        String[] list = a2.list(new FilenameFilter() {
            public boolean accept(File file, String str) {
                return str.endsWith(".dmp");
            }
        });
        if (list == null) {
            list = new String[0];
        }
        return list;
    }

    public void a(@NonNull String str, @NonNull wm<Boolean> wmVar, boolean z) {
        try {
            String b2 = am.b(am.a(str));
            if (b2 != null) {
                if (z) {
                    this.c.a(b2);
                } else {
                    this.c.b(b2);
                }
            }
            wmVar.a(Boolean.valueOf(true));
        } catch (Throwable th) {
            this.e.a(str).delete();
            throw th;
        }
        this.e.a(str).delete();
    }
}
