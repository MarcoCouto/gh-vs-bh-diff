package com.yandex.metrica.impl.ob;

import android.content.Context;
import java.util.Map;

@Deprecated
public class qc extends qd {
    private final qk d = new qk("init_event_pref_key", i());
    private final qk e = new qk("init_event_pref_key");
    private final qk f = new qk("first_event_pref_key", i());
    private final qk g = new qk("fitst_event_description_key", i());

    /* access modifiers changed from: protected */
    public String f() {
        return "_initpreferences";
    }

    public qc(Context context, String str) {
        super(context, str);
    }

    public void a() {
        a(this.d.b(), "DONE").j();
    }

    @Deprecated
    public String a(String str) {
        return this.c.getString(this.e.b(), str);
    }

    public String b(String str) {
        return this.c.getString(this.d.b(), str);
    }

    public String c(String str) {
        return this.c.getString(this.f.b(), str);
    }

    @Deprecated
    public void b() {
        a(this.e);
    }

    @Deprecated
    public void d(String str) {
        a(new qk("init_event_pref_key", str));
    }

    public void c() {
        a(this.d);
    }

    public void d() {
        a(this.f);
    }

    public String e(String str) {
        return this.c.getString(this.g.b(), str);
    }

    public void e() {
        a(this.g);
    }

    private void a(qk qkVar) {
        this.c.edit().remove(qkVar.b()).apply();
    }

    /* access modifiers changed from: 0000 */
    public Map<String, ?> g() {
        return this.c.getAll();
    }

    static String f(String str) {
        return new qk("init_event_pref_key", str).b();
    }

    static String g(String str) {
        return str.replace("init_event_pref_key", "");
    }
}
