package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import com.yandex.metrica.impl.ob.h.b;

class ow {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Context f5048a;
    @NonNull
    private uk b;
    @Nullable
    private oh c;
    @NonNull
    private final bq d;
    @NonNull
    private final lh e;
    @NonNull
    private final lg f;
    @NonNull
    private final wh g;
    @NonNull
    private final pg h;
    @NonNull
    private final h i;
    @NonNull
    private final b j;
    @NonNull
    private final pi k;
    @NonNull
    private final xh l;
    /* access modifiers changed from: private */
    public boolean m;

    public ow(@NonNull Context context, @NonNull uk ukVar, @Nullable oh ohVar, @NonNull lh lhVar, @NonNull lg lgVar, @NonNull xh xhVar) {
        this(context, ukVar, ohVar, new bq(), lhVar, lgVar, xhVar, new wg(), new pg(), al.a().i(), new pi(context));
    }

    public void a() {
        if (d()) {
            b();
        }
    }

    private void b() {
        if (!this.m) {
            this.i.a(h.f4865a, this.l, this.j);
        } else {
            c();
        }
    }

    /* access modifiers changed from: private */
    public void c() {
        if (this.c != null) {
            pd a2 = this.h.a(this.f5048a, this.b, this.c);
            boolean a3 = this.k.a();
            do {
                if (this.k.a()) {
                    a3 = a2.a();
                    if (a3 && a2.c().b()) {
                        while (this.l.c() && a3) {
                            this.d.a(a2);
                            a3 = !a2.b() && a2.t();
                        }
                    }
                }
            } while (a3);
        }
    }

    private boolean d() {
        return a((kw) this.e) || a((kw) this.f);
    }

    private boolean a(kw kwVar) {
        return this.c != null && (b(kwVar) || c(kwVar));
    }

    private boolean b(kw kwVar) {
        return this.c != null && a(kwVar, (long) this.c.g);
    }

    private boolean a(kw kwVar, long j2) {
        return kwVar.a() >= j2;
    }

    private boolean c(kw kwVar) {
        return this.c != null && b(kwVar, this.c.i);
    }

    private boolean b(kw kwVar, long j2) {
        return this.g.a() - kwVar.b() > j2;
    }

    public void a(@NonNull uk ukVar, @Nullable oh ohVar) {
        this.b = ukVar;
        this.c = ohVar;
    }

    @VisibleForTesting
    ow(@NonNull Context context, @NonNull uk ukVar, @Nullable oh ohVar, @NonNull bq bqVar, @NonNull lh lhVar, @NonNull lg lgVar, @NonNull xh xhVar, @NonNull wh whVar, @NonNull pg pgVar, @NonNull h hVar, @NonNull pi piVar) {
        this.m = false;
        this.f5048a = context;
        this.c = ohVar;
        this.b = ukVar;
        this.d = bqVar;
        this.e = lhVar;
        this.f = lgVar;
        this.l = xhVar;
        this.g = whVar;
        this.h = pgVar;
        this.i = hVar;
        this.j = new b() {
            public void a() {
                ow.this.m = true;
                ow.this.c();
            }
        };
        this.k = piVar;
    }
}
