package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import com.yandex.metrica.impl.ob.cq.b;
import com.yandex.metrica.impl.ob.cq.b.a;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.util.Collection;
import java.util.List;
import java.util.Map;

public class cp implements bn {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final ul f4745a;
    @NonNull
    private final su b;
    @NonNull
    private final cq c;
    @NonNull
    private final mp d;

    public cp(@NonNull ul ulVar, @NonNull su suVar) {
        this(ulVar, suVar, new cq(), new mp());
    }

    public boolean a(int i, byte[] bArr, @NonNull Map<String, List<String>> map) {
        if (200 != i) {
            return false;
        }
        List list = (List) map.get(HttpRequest.HEADER_CONTENT_ENCODING);
        if (!cx.a((Collection) list) && "encrypted".equals(list.get(0))) {
            bArr = this.d.a(bArr, "hBnBQbZrmjPXEWVJ");
        }
        if (bArr == null) {
            return false;
        }
        b a2 = this.c.a(bArr);
        if (a.OK != a2.l()) {
            return false;
        }
        this.f4745a.a(a2, this.b, map);
        return true;
    }

    @VisibleForTesting
    cp(@NonNull ul ulVar, @NonNull su suVar, @NonNull cq cqVar, @NonNull mp mpVar) {
        this.f4745a = ulVar;
        this.b = suVar;
        this.c = cqVar;
        this.d = mpVar;
    }
}
