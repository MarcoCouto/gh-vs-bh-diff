package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import com.yandex.metrica.IParamsCallback;
import com.yandex.metrica.impl.ac.a.c;
import com.yandex.metrica.impl.ob.su.a;
import com.yandex.metrica.impl.ob.su.b;
import java.util.Collection;
import java.util.List;
import java.util.Map;

public class ul implements es {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Context f5238a;
    @NonNull
    private final ek b;
    @NonNull
    private final uj c;
    @NonNull
    private volatile mf<uk> d;
    @Nullable
    private volatile cr e;
    @NonNull
    private ud f;

    public ul(@NonNull Context context, @NonNull String str, @NonNull a aVar, @NonNull uj ujVar) {
        this(context, new eh(str), aVar, ujVar, np.a.a(uk.class).a(context), new vo());
    }

    private ul(@NonNull Context context, @NonNull ek ekVar, @NonNull a aVar, @NonNull uj ujVar, @NonNull mf<uk> mfVar, @NonNull vo voVar) {
        this(context, ekVar, aVar, ujVar, mfVar, (uk) mfVar.a(), voVar);
    }

    private ul(@NonNull Context context, @NonNull ek ekVar, @NonNull a aVar, @NonNull uj ujVar, @NonNull mf<uk> mfVar, @NonNull uk ukVar, @NonNull vo voVar) {
        ud udVar = new ud(new b(context, ekVar.b()), ukVar, aVar);
        this(context, ekVar, ujVar, mfVar, ukVar, voVar, udVar);
    }

    @VisibleForTesting
    ul(@NonNull Context context, @NonNull ek ekVar, @NonNull uj ujVar, @NonNull mf<uk> mfVar, @NonNull uk ukVar, @NonNull vo voVar, @NonNull ud udVar) {
        this.f5238a = context;
        this.b = ekVar;
        this.c = ujVar;
        this.d = mfVar;
        this.f = udVar;
        a(ukVar, voVar);
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x0047  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0057  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0065  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0070  */
    private void a(@NonNull uk ukVar, @NonNull vo voVar) {
        boolean z;
        uk.a a2 = ukVar.a();
        String str = "";
        c D = ((su) this.f.d()).D();
        if (D != null) {
            str = voVar.a(D.f4620a);
            if (!TextUtils.equals(ukVar.c, str)) {
                a2 = a2.c(str);
            } else {
                z = false;
                if (!b(ukVar.f5236a) || !c(ukVar.b)) {
                    if (!b(ukVar.f5236a)) {
                        a2 = a2.a(voVar.a());
                    }
                    if (!c(ukVar.b)) {
                        a2 = a2.b(str).d("");
                    }
                    z = true;
                }
                if (!z) {
                    uk a3 = a2.a();
                    d(a3);
                    b(a3);
                    return;
                }
                b(ukVar);
                return;
            }
        } else {
            a2 = a2.c(str);
        }
        z = true;
        if (!b(ukVar.f5236a)) {
        }
        if (!c(ukVar.b)) {
        }
        z = true;
        if (!z) {
        }
    }

    @NonNull
    public ek b() {
        return this.b;
    }

    @Nullable
    public synchronized cr a() {
        if (!c()) {
            return null;
        }
        if (this.e == null) {
            this.e = new cr(this, d());
        }
        return this.e;
    }

    public synchronized boolean a(@Nullable List<String> list, @NonNull Map<String, String> map) {
        boolean z = false;
        if (list == null) {
            return false;
        }
        uk b2 = this.f.b();
        for (String str : list) {
            z = str.equals("yandex_mobile_metrica_uuid") ? z | (!b(b2.f5236a)) : str.equals("yandex_mobile_metrica_device_id") ? z | (!c(b2.b)) : str.equals("appmetrica_device_id_hash") ? z | (!d(b2.d)) : str.equals("yandex_mobile_metrica_get_ad_url") ? z | (!e(b2.f)) : str.equals("yandex_mobile_metrica_report_ad_url") ? z | (!f(b2.g)) : str.equals(IParamsCallback.YANDEX_MOBILE_METRICA_CLIDS) ? z | (!a(map)) : true;
        }
        return z;
    }

    public synchronized boolean c() {
        boolean z;
        z = e().F;
        if (!z) {
            z = !a(wk.a(Long.valueOf(e().t), 0));
            if (!z && !a(((su) this.f.d()).G())) {
                z = true;
            }
        }
        return z;
    }

    private boolean a(@Nullable Map<String, String> map) {
        if (cx.a((Map) map)) {
            return true;
        }
        return map.equals(we.a(e().n));
    }

    /* access modifiers changed from: 0000 */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x002a, code lost:
        return false;
     */
    @VisibleForTesting
    public synchronized boolean a(long j) {
        if (!((su) this.f.d()).f()) {
            return false;
        }
        long b2 = wi.b() - j;
        if (b2 <= 86400 && b2 >= 0) {
            return true;
        }
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public void a(@NonNull uk ukVar) {
        this.d.a(ukVar);
    }

    @NonNull
    public su d() {
        return (su) this.f.d();
    }

    private synchronized void f() {
        this.e = null;
    }

    public void a(@NonNull cq.b bVar, @NonNull su suVar, @Nullable Map<String, List<String>> map) {
        uk a2;
        synchronized (this) {
            Long valueOf = Long.valueOf(wk.a(cq.a(map), 0));
            a(bVar.t(), valueOf);
            a2 = a(bVar, suVar, valueOf);
            new ns().a(this.f5238a, new nq(a2.b, a2.d), new pr(po.b().a(a2).a()));
            f();
            d(a2);
        }
        c(a2);
    }

    /* access modifiers changed from: protected */
    @VisibleForTesting
    @NonNull
    public uk a(@NonNull cq.b bVar, @NonNull su suVar, @Nullable Long l) {
        String a2 = we.a(suVar.G());
        String a3 = a(bVar.k(), e().m);
        String str = e().b;
        if (TextUtils.isEmpty(str)) {
            str = bVar.i();
        }
        return new uk.a(bVar.a()).a(wi.b()).b(str).c(e().c).d(bVar.j()).a(e().f5236a).e(bVar.e()).c(bVar.c()).d(suVar.F()).a(bVar.f()).b(bVar.h()).f(bVar.g()).g(bVar.d()).e(bVar.w()).h(a3).i(a2).b(a(suVar.G(), a3)).a(bVar.n()).a(bVar.r()).a(bVar.s()).f(bVar.u()).k(bVar.v()).a(bVar.o()).g(bVar.q()).a(bVar.p()).a(bVar.x()).a(true).b(wk.a(l, wi.b() * 1000)).c(((su) this.f.d()).b(l.longValue())).c(false).j(e().s).a(bVar.y()).a(bVar.A()).a();
    }

    private void c(@NonNull uk ukVar) {
        this.c.a(this.b.b(), ukVar);
        b(ukVar);
    }

    /* access modifiers changed from: 0000 */
    public void b(uk ukVar) {
        dr.a().b((dt) new eb(this.b.b(), ukVar));
        if (!TextUtils.isEmpty(ukVar.f5236a)) {
            dr.a().b((dt) new ec(ukVar.f5236a, this.b.b()));
        }
        if (!TextUtils.isEmpty(ukVar.b)) {
            dr.a().b((dt) new dy(ukVar.b));
        }
        if (ukVar.r == null) {
            dr.a().a(ea.class);
        } else {
            dr.a().b((dt) new ea(ukVar.r));
        }
    }

    private void a(@Nullable Long l, @NonNull Long l2) {
        wd.a().a(l2.longValue(), l);
    }

    private void d(@NonNull uk ukVar) {
        this.f.a(ukVar);
        a(ukVar);
        e(ukVar);
    }

    @Nullable
    private static String a(@Nullable String str, @Nullable String str2) {
        if (we.b(str)) {
            return str;
        }
        if (we.b(str2)) {
            return str2;
        }
        return null;
    }

    private boolean a(Map<String, String> map, @Nullable String str) {
        Map a2 = we.a(str);
        if (cx.a((Map) map)) {
            return cx.a(a2);
        }
        return a2.equals(map);
    }

    @Deprecated
    private void e(uk ukVar) {
        if (!TextUtils.isEmpty(ukVar.b)) {
            try {
                Intent intent = new Intent("com.yandex.metrica.intent.action.SYNC");
                intent.putExtra("CAUSE", "CAUSE_DEVICE_ID");
                intent.putExtra("SYNC_TO_PKG", this.b.b());
                intent.putExtra("SYNC_DATA", ukVar.b);
                intent.putExtra("SYNC_DATA_2", ukVar.f5236a);
                this.f5238a.sendBroadcast(intent);
            } catch (Throwable unused) {
            }
        }
    }

    @NonNull
    public uk e() {
        return this.f.b();
    }

    public void a(@NonNull ue ueVar) {
        f();
        this.c.a(b().b(), ueVar, e());
    }

    public synchronized void a(@NonNull a aVar) {
        this.f.a(aVar);
        a((su) this.f.d());
    }

    private void a(su suVar) {
        if (suVar.J()) {
            boolean z = false;
            List I = suVar.I();
            uk.a aVar = null;
            if (cx.a((Collection) I) && !cx.a((Collection) suVar.F())) {
                aVar = e().a().d(null);
                z = true;
            }
            if (!cx.a((Collection) I) && !cx.a((Object) I, (Object) suVar.F())) {
                aVar = e().a().d(I);
                z = true;
            }
            if (z) {
                d(aVar.a());
            }
        }
    }

    public synchronized void a(String str) {
        d(e().a().j(str).a());
    }

    private boolean b(@Nullable String str) {
        return !TextUtils.isEmpty(str);
    }

    private boolean c(@Nullable String str) {
        return !TextUtils.isEmpty(str);
    }

    private boolean d(@Nullable String str) {
        return !TextUtils.isEmpty(str);
    }

    private boolean e(@Nullable String str) {
        return !TextUtils.isEmpty(str);
    }

    private boolean f(@Nullable String str) {
        return !TextUtils.isEmpty(str);
    }
}
