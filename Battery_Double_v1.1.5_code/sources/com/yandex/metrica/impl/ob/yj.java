package com.yandex.metrica.impl.ob;

import android.support.annotation.Nullable;
import android.text.TextUtils;
import java.util.LinkedList;
import java.util.List;

public class yj implements yk<List<yi>> {
    public yi a(@Nullable List<yi> list) {
        LinkedList linkedList = new LinkedList();
        boolean z = true;
        for (yi yiVar : list) {
            if (!yiVar.a()) {
                linkedList.add(yiVar.b());
                z = false;
            }
        }
        if (z) {
            return yi.a(this);
        }
        return yi.a(this, TextUtils.join(", ", linkedList));
    }
}
