package com.yandex.metrica.impl.ob;

public class ua {

    /* renamed from: a reason: collision with root package name */
    public final long f5226a;
    public final long b;
    public final long c;
    public final long d;

    public ua(long j, long j2, long j3, long j4) {
        this.f5226a = j;
        this.b = j2;
        this.c = j3;
        this.d = j4;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("SdkFingerprintingConfig{minCollectingInterval=");
        sb.append(this.f5226a);
        sb.append(", minFirstCollectingDelay=");
        sb.append(this.b);
        sb.append(", minCollectingDelayAfterLaunch=");
        sb.append(this.c);
        sb.append(", minRequestRetryInterval=");
        sb.append(this.d);
        sb.append('}');
        return sb.toString();
    }

    public boolean equals(Object obj) {
        boolean z = true;
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        ua uaVar = (ua) obj;
        if (this.f5226a != uaVar.f5226a || this.b != uaVar.b || this.c != uaVar.c) {
            return false;
        }
        if (this.d != uaVar.d) {
            z = false;
        }
        return z;
    }

    public int hashCode() {
        return (((((((int) (this.f5226a ^ (this.f5226a >>> 32))) * 31) + ((int) (this.b ^ (this.b >>> 32)))) * 31) + ((int) (this.c ^ (this.c >>> 32)))) * 31) + ((int) (this.d ^ (this.d >>> 32)));
    }
}
