package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.util.SparseArray;
import com.ironsource.sdk.constants.Constants.ParametersKeys;
import com.yandex.metrica.impl.ob.lj.a;
import com.yandex.metrica.impl.ob.lq.b;
import com.yandex.metrica.impl.ob.lq.d;
import com.yandex.metrica.impl.ob.lq.h;
import java.util.HashMap;

public class la {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final lb f4960a;
    @NonNull
    private final le b;
    @NonNull
    private final a c;

    public la(@NonNull lb lbVar, @NonNull le leVar) {
        this(lbVar, leVar, new a());
    }

    public la(@NonNull lb lbVar, @NonNull le leVar, @NonNull a aVar) {
        this.f4960a = lbVar;
        this.b = leVar;
        this.c = aVar;
    }

    public lj a() {
        return this.c.a(ParametersKeys.MAIN, this.f4960a.c(), this.f4960a.d(), this.f4960a.a(), new ll(ParametersKeys.MAIN, this.b.a()));
    }

    public lj b() {
        HashMap hashMap = new HashMap();
        hashMap.put("preferences", d.f4978a);
        hashMap.put("binary_data", b.f4977a);
        hashMap.put("startup", h.f4978a);
        hashMap.put("l_dat", lq.a.f4974a);
        hashMap.put("lbs_dat", lq.a.f4974a);
        return this.c.a("metrica.db", this.f4960a.g(), this.f4960a.h(), this.f4960a.b(), new ll("metrica.db", hashMap));
    }

    public lj c() {
        HashMap hashMap = new HashMap();
        hashMap.put("preferences", d.f4978a);
        return this.c.a("client storage", this.f4960a.e(), this.f4960a.f(), new SparseArray(), new ll("metrica.db", hashMap));
    }
}
