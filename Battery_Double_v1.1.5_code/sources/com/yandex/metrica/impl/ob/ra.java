package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.metrica.impl.ob.rl.a.C0115a;

public class ra extends qo {
    public ra(@NonNull qn qnVar) {
        super(qnVar);
    }

    public C0115a a(@NonNull re reVar, @Nullable C0115a aVar, @NonNull qm qmVar) {
        if (!a(aVar)) {
            return null;
        }
        C0115a a2 = qmVar.a();
        a2.d.b = true;
        return a().a(reVar, a2);
    }
}
