package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.util.HashMap;
import java.util.Map;

public class fu implements ep {

    /* renamed from: a reason: collision with root package name */
    private final HashMap<String, fc> f4845a = new HashMap<>();
    private final HashMap<String, ei> b = new HashMap<>();
    private final Context c;

    public fu(@NonNull Context context) {
        this.c = context.getApplicationContext();
    }

    @Nullable
    public synchronized fc a(@NonNull ek ekVar) {
        return (fc) this.f4845a.get(ekVar.toString());
    }

    @NonNull
    public synchronized fc a(@NonNull ek ekVar, @NonNull eg egVar, @NonNull eq<fc> eqVar) {
        return (fc) a(ekVar, egVar, eqVar, this.f4845a);
    }

    @NonNull
    public synchronized ei b(@NonNull ek ekVar, @NonNull eg egVar, @NonNull eq<ei> eqVar) {
        return (ei) a(ekVar, egVar, eqVar, this.b);
    }

    @NonNull
    private <T extends ev> T a(@NonNull ek ekVar, @NonNull eg egVar, @NonNull eq<T> eqVar, @NonNull Map<String, T> map) {
        T t = (ev) map.get(ekVar.toString());
        if (t == null) {
            T b2 = eqVar.b(this.c, ekVar, egVar);
            map.put(ekVar.toString(), b2);
            return b2;
        }
        t.a(egVar);
        return t;
    }

    public synchronized void c() {
        for (ep c2 : this.f4845a.values()) {
            c2.c();
        }
        for (ep c3 : this.b.values()) {
            c3.c();
        }
        this.f4845a.clear();
        this.b.clear();
    }
}
