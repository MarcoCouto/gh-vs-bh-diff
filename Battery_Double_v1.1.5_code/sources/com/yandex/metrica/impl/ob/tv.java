package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import com.yandex.metrica.impl.ob.rr.a.c;

public class tv {

    /* renamed from: a reason: collision with root package name */
    public final boolean f5221a;
    public final boolean b;
    public final boolean c;
    public final boolean d;
    public final boolean e;
    public final boolean f;
    public final boolean g;
    public final boolean h;
    public final boolean i;
    public final boolean j;
    public final boolean k;
    public final boolean l;
    public final boolean m;
    public final boolean n;
    public final boolean o;

    public static class a {
        /* access modifiers changed from: private */

        /* renamed from: a reason: collision with root package name */
        public boolean f5222a = b.f5223a;
        /* access modifiers changed from: private */
        public boolean b = b.b;
        /* access modifiers changed from: private */
        public boolean c = b.c;
        /* access modifiers changed from: private */
        public boolean d = b.d;
        /* access modifiers changed from: private */
        public boolean e = b.e;
        /* access modifiers changed from: private */
        public boolean f = b.f;
        /* access modifiers changed from: private */
        public boolean g = b.g;
        /* access modifiers changed from: private */
        public boolean h = b.h;
        /* access modifiers changed from: private */
        public boolean i = b.i;
        /* access modifiers changed from: private */
        public boolean j = b.j;
        /* access modifiers changed from: private */
        public boolean k = b.k;
        /* access modifiers changed from: private */
        public boolean l = b.o;
        /* access modifiers changed from: private */
        public boolean m = b.l;
        /* access modifiers changed from: private */
        public boolean n = b.m;
        /* access modifiers changed from: private */
        public boolean o = b.n;

        public a a(boolean z) {
            this.f5222a = z;
            return this;
        }

        public a b(boolean z) {
            this.b = z;
            return this;
        }

        public a c(boolean z) {
            this.c = z;
            return this;
        }

        public a d(boolean z) {
            this.d = z;
            return this;
        }

        public a e(boolean z) {
            this.e = z;
            return this;
        }

        public a f(boolean z) {
            this.f = z;
            return this;
        }

        public a g(boolean z) {
            this.g = z;
            return this;
        }

        public a h(boolean z) {
            this.h = z;
            return this;
        }

        public a i(boolean z) {
            this.i = z;
            return this;
        }

        public a j(boolean z) {
            this.j = z;
            return this;
        }

        public a k(boolean z) {
            this.k = z;
            return this;
        }

        public a l(boolean z) {
            this.m = z;
            return this;
        }

        public a m(boolean z) {
            this.n = z;
            return this;
        }

        public a n(boolean z) {
            this.o = z;
            return this;
        }

        public a o(boolean z) {
            this.l = z;
            return this;
        }

        public tv a() {
            return new tv(this);
        }
    }

    public static final class b {

        /* renamed from: a reason: collision with root package name */
        public static final boolean f5223a = p.b;
        public static final boolean b = p.c;
        public static final boolean c = p.d;
        public static final boolean d = p.e;
        public static final boolean e = p.o;
        public static final boolean f = p.q;
        public static final boolean g = p.f;
        public static final boolean h = p.g;
        public static final boolean i = p.h;
        public static final boolean j = p.i;
        public static final boolean k = p.j;
        public static final boolean l = p.k;
        public static final boolean m = p.l;
        public static final boolean n = p.m;
        public static final boolean o = p.n;
        private static final c p = new c();
    }

    public tv(@NonNull a aVar) {
        this.f5221a = aVar.f5222a;
        this.b = aVar.b;
        this.c = aVar.c;
        this.d = aVar.d;
        this.e = aVar.e;
        this.f = aVar.f;
        this.g = aVar.g;
        this.h = aVar.h;
        this.i = aVar.i;
        this.j = aVar.j;
        this.k = aVar.k;
        this.l = aVar.l;
        this.m = aVar.m;
        this.n = aVar.n;
        this.o = aVar.o;
    }

    public boolean equals(Object obj) {
        boolean z = true;
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        tv tvVar = (tv) obj;
        if (this.f5221a != tvVar.f5221a || this.b != tvVar.b || this.c != tvVar.c || this.d != tvVar.d || this.e != tvVar.e || this.f != tvVar.f || this.g != tvVar.g || this.h != tvVar.h || this.i != tvVar.i || this.j != tvVar.j || this.k != tvVar.k || this.l != tvVar.l || this.m != tvVar.m || this.n != tvVar.n) {
            return false;
        }
        if (this.o != tvVar.o) {
            z = false;
        }
        return z;
    }

    public int hashCode() {
        return ((((((((((((((((((((((((((((this.f5221a ? 1 : 0) * true) + (this.b ? 1 : 0)) * 31) + (this.c ? 1 : 0)) * 31) + (this.d ? 1 : 0)) * 31) + (this.e ? 1 : 0)) * 31) + (this.f ? 1 : 0)) * 31) + (this.g ? 1 : 0)) * 31) + (this.h ? 1 : 0)) * 31) + (this.i ? 1 : 0)) * 31) + (this.j ? 1 : 0)) * 31) + (this.k ? 1 : 0)) * 31) + (this.l ? 1 : 0)) * 31) + (this.m ? 1 : 0)) * 31) + (this.n ? 1 : 0)) * 31) + (this.o ? 1 : 0);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("CollectingFlags{easyCollectingEnabled=");
        sb.append(this.f5221a);
        sb.append(", packageInfoCollectingEnabled=");
        sb.append(this.b);
        sb.append(", permissionsCollectingEnabled=");
        sb.append(this.c);
        sb.append(", featuresCollectingEnabled=");
        sb.append(this.d);
        sb.append(", sdkFingerprintingCollectingEnabled=");
        sb.append(this.e);
        sb.append(", bleCollectingEnabled=");
        sb.append(this.f);
        sb.append(", androidId=");
        sb.append(this.g);
        sb.append(", googleAid=");
        sb.append(this.h);
        sb.append(", wifiAround=");
        sb.append(this.i);
        sb.append(", wifiConnected=");
        sb.append(this.j);
        sb.append(", ownMacs=");
        sb.append(this.k);
        sb.append(", accessPoint=");
        sb.append(this.l);
        sb.append(", cellsAround=");
        sb.append(this.m);
        sb.append(", simInfo=");
        sb.append(this.n);
        sb.append(", simImei=");
        sb.append(this.o);
        sb.append('}');
        return sb.toString();
    }
}
