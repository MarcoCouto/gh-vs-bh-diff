package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;

public class wp<K, V> {

    /* renamed from: a reason: collision with root package name */
    private final HashMap<K, Collection<V>> f5289a = new HashMap<>();

    public int a() {
        int i = 0;
        for (Collection size : this.f5289a.values()) {
            i += size.size();
        }
        return i;
    }

    @Nullable
    public Collection<V> a(@Nullable K k) {
        return (Collection) this.f5289a.get(k);
    }

    @Nullable
    public Collection<V> a(@Nullable K k, @Nullable V v) {
        Collection collection;
        Collection collection2 = (Collection) this.f5289a.get(k);
        if (collection2 == null) {
            collection = c();
        } else {
            collection = a(collection2);
        }
        collection.add(v);
        return (Collection) this.f5289a.put(k, collection);
    }

    @Nullable
    public Collection<V> b(@Nullable K k) {
        return (Collection) this.f5289a.remove(k);
    }

    @Nullable
    public Collection<V> b(@Nullable K k, @Nullable V v) {
        Collection collection = (Collection) this.f5289a.get(k);
        if (collection == null || !collection.remove(v)) {
            return null;
        }
        return a(collection);
    }

    @NonNull
    private Collection<V> c() {
        return new ArrayList();
    }

    @NonNull
    private Collection<V> a(@NonNull Collection<V> collection) {
        return new ArrayList(collection);
    }

    @NonNull
    public Set<? extends Entry<K, ? extends Collection<V>>> b() {
        return this.f5289a.entrySet();
    }

    public String toString() {
        return this.f5289a.toString();
    }
}
