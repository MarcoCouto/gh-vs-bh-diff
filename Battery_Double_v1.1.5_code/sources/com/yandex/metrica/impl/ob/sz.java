package com.yandex.metrica.impl.ob;

import android.net.Uri.Builder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.tapjoy.TapjoyConstants;
import com.vungle.warren.model.ReportDBAdapter.ReportColumns;
import com.yandex.metrica.impl.ac.a.c;

public class sz extends sy<st> {
    @Nullable

    /* renamed from: a reason: collision with root package name */
    private sr f5177a;
    private int b;

    public void a(@NonNull sr srVar) {
        this.f5177a = srVar;
    }

    public void a(int i) {
        this.b = i;
    }

    public void a(@NonNull Builder builder, @NonNull st stVar) {
        super.a(builder, stVar);
        builder.path(ReportColumns.TABLE_NAME);
        b(builder, stVar);
        c(builder, stVar);
        builder.appendQueryParameter("request_id", String.valueOf(this.b));
    }

    private void b(@NonNull Builder builder, @NonNull st stVar) {
        if (this.f5177a != null) {
            a(builder, "deviceid", this.f5177a.f5168a, stVar.r());
            a(builder, "uuid", this.f5177a.b, stVar.t());
            a(builder, "analytics_sdk_version", this.f5177a.c);
            a(builder, "analytics_sdk_version_name", this.f5177a.d);
            a(builder, "app_version_name", this.f5177a.g, stVar.q());
            a(builder, "app_build_number", this.f5177a.i, stVar.p());
            a(builder, TapjoyConstants.TJC_DEVICE_OS_VERSION_NAME, this.f5177a.j, stVar.n());
            a(builder, "os_api_level", this.f5177a.k);
            a(builder, "analytics_sdk_build_number", this.f5177a.e);
            a(builder, "analytics_sdk_build_type", this.f5177a.f);
            a(builder, "app_debuggable", this.f5177a.h);
            a(builder, "locale", this.f5177a.l, stVar.A());
            a(builder, "is_rooted", this.f5177a.m, stVar.u());
            a(builder, "app_framework", this.f5177a.n, stVar.v());
            a(builder, "attribution_id", this.f5177a.o);
        }
    }

    private void c(@NonNull Builder builder, @NonNull st stVar) {
        String str;
        String str2;
        builder.appendQueryParameter("api_key_128", stVar.c());
        builder.appendQueryParameter("app_id", stVar.d());
        builder.appendQueryParameter("app_platform", stVar.l());
        builder.appendQueryParameter("model", stVar.m());
        builder.appendQueryParameter("manufacturer", stVar.g());
        builder.appendQueryParameter("screen_width", String.valueOf(stVar.w()));
        builder.appendQueryParameter("screen_height", String.valueOf(stVar.x()));
        builder.appendQueryParameter("screen_dpi", String.valueOf(stVar.y()));
        builder.appendQueryParameter("scalefactor", String.valueOf(stVar.z()));
        builder.appendQueryParameter(TapjoyConstants.TJC_DEVICE_TYPE_NAME, stVar.C());
        builder.appendQueryParameter(TapjoyConstants.TJC_ANDROID_ID, stVar.B());
        a(builder, "clids_set", stVar.a());
        c D = stVar.D();
        if (D == null) {
            str = "";
        } else {
            str = D.f4620a;
        }
        String str3 = CampaignEx.JSON_KEY_ADV_ID;
        if (str == null) {
            str = "";
        }
        builder.appendQueryParameter(str3, str);
        String str4 = "limit_ad_tracking";
        if (D == null) {
            str2 = "";
        } else {
            str2 = a(D.b);
        }
        builder.appendQueryParameter(str4, str2);
    }

    private void a(Builder builder, String str, String str2, String str3) {
        builder.appendQueryParameter(str, cu.c(str2, str3));
    }

    private void a(Builder builder, String str, String str2) {
        if (!TextUtils.isEmpty(str2)) {
            builder.appendQueryParameter(str, str2);
        }
    }
}
