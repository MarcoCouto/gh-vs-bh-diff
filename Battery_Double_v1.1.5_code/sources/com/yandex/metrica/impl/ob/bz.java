package com.yandex.metrica.impl.ob;

import android.os.Bundle;
import android.support.annotation.NonNull;
import com.yandex.metrica.CounterConfiguration;

class bz extends ed {

    /* renamed from: a reason: collision with root package name */
    protected ae f4710a;
    protected bu b;
    private ak c = new ak();

    protected bz(@NonNull ee eeVar, @NonNull CounterConfiguration counterConfiguration) {
        super(eeVar, counterConfiguration);
    }

    /* access modifiers changed from: 0000 */
    public void a(xx xxVar) {
        this.f4710a = new ae(xxVar);
    }

    /* access modifiers changed from: 0000 */
    public Bundle b() {
        Bundle bundle = new Bundle();
        h().a(bundle);
        g().b(bundle);
        return bundle;
    }

    /* access modifiers changed from: 0000 */
    public void a(ug ugVar) {
        b(ugVar);
    }

    /* access modifiers changed from: 0000 */
    public void c() {
        this.c.b();
    }

    /* access modifiers changed from: 0000 */
    public boolean d() {
        return this.c.a();
    }

    /* access modifiers changed from: 0000 */
    public boolean a() {
        return this.c.c();
    }

    /* access modifiers changed from: 0000 */
    public void b(ug ugVar) {
        if (ugVar != null) {
            h().c(ugVar.a());
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(String str, String str2) {
        this.f4710a.a(str, str2);
    }

    /* access modifiers changed from: 0000 */
    public String e() {
        return this.f4710a.a();
    }

    /* access modifiers changed from: 0000 */
    public bu f() {
        return this.b;
    }

    /* access modifiers changed from: 0000 */
    public void a(bu buVar) {
        this.b = buVar;
    }
}
