package com.yandex.metrica.impl.ob;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;

public class jo {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final c f4915a;

    @TargetApi(26)
    static class a implements c {
        @NonNull

        /* renamed from: a reason: collision with root package name */
        private final jl f4916a;

        public a(@NonNull Context context) {
            this.f4916a = new jl(context);
        }

        @NonNull
        public jm a() {
            return this.f4916a;
        }
    }

    static class b implements c {
        @NonNull

        /* renamed from: a reason: collision with root package name */
        private final jn f4917a;

        public b(@NonNull Context context) {
            this.f4917a = new jn(context);
        }

        @NonNull
        public jm a() {
            return this.f4917a;
        }
    }

    interface c {
        @NonNull
        jm a();
    }

    public jo(@NonNull Context context) {
        this(VERSION.SDK_INT >= 26 ? new a(context) : new b(context));
    }

    jo(@NonNull c cVar) {
        this.f4915a = cVar;
    }

    public jm a() {
        return this.f4915a.a();
    }
}
