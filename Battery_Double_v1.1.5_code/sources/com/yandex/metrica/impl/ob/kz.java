package com.yandex.metrica.impl.ob;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.ironsource.sdk.constants.LocationConst;
import com.yandex.metrica.impl.ob.ky.b;
import com.yandex.metrica.impl.ob.lq.g;
import java.io.Closeable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class kz implements Closeable {

    /* renamed from: a reason: collision with root package name */
    private final ReentrantReadWriteLock f4956a;
    private final Lock b;
    private final Lock c;
    private final lc d;
    private final a e;
    /* access modifiers changed from: private */
    public final Object f;
    /* access modifiers changed from: private */
    public final List<ContentValues> g;
    private final Context h;
    private final en i;
    private final AtomicLong j;
    /* access modifiers changed from: private */
    @NonNull
    public final List<nw> k;
    @NonNull
    private final ky l;

    private class a extends xl {
        @Nullable
        private en b;

        a(en enVar) {
            this.b = enVar;
        }

        public void run() {
            ArrayList arrayList;
            while (c()) {
                try {
                    synchronized (this) {
                        if (kz.this.g()) {
                            wait();
                        }
                    }
                } catch (Throwable unused) {
                    b();
                }
                synchronized (kz.this.f) {
                    arrayList = new ArrayList(kz.this.g);
                    kz.this.g.clear();
                }
                kz.this.a((List<ContentValues>) arrayList);
                a(arrayList);
            }
        }

        /* access modifiers changed from: 0000 */
        public synchronized void a() {
            b();
            this.b = null;
        }

        /* access modifiers changed from: 0000 */
        public synchronized void a(@NonNull List<ContentValues> list) {
            ArrayList arrayList = new ArrayList();
            for (ContentValues a2 : list) {
                arrayList.add(Integer.valueOf(kz.this.g(a2)));
            }
            for (nw a3 : kz.this.k) {
                a3.a(arrayList);
            }
            if (this.b != null) {
                this.b.C().a();
            }
        }
    }

    public kz(@NonNull en enVar, lc lcVar) {
        this(enVar, lcVar, new ky(enVar.k(), enVar.a()));
    }

    public kz(@NonNull en enVar, lc lcVar, @NonNull ky kyVar) {
        this.f4956a = new ReentrantReadWriteLock();
        this.b = this.f4956a.readLock();
        this.c = this.f4956a.writeLock();
        this.f = new Object();
        this.g = new ArrayList(3);
        this.j = new AtomicLong();
        this.k = new ArrayList();
        this.d = lcVar;
        this.h = enVar.k();
        this.i = enVar;
        this.l = kyVar;
        this.j.set(e());
        this.e = new a(enVar);
        this.e.setName(a((et) enVar));
    }

    public void a() {
        this.e.start();
    }

    public long b() {
        this.b.lock();
        try {
            return this.j.get();
        } finally {
            this.b.unlock();
        }
    }

    public long a(@NonNull Set<Integer> set) {
        Cursor cursor;
        this.b.lock();
        Cursor cursor2 = null;
        long j2 = 0;
        try {
            SQLiteDatabase readableDatabase = this.d.getReadableDatabase();
            if (readableDatabase != null) {
                StringBuilder sb = new StringBuilder("SELECT count() FROM reports");
                if (!set.isEmpty()) {
                    sb.append(" WHERE ");
                }
                int i2 = 0;
                for (Integer num : set) {
                    if (i2 > 0) {
                        sb.append(" OR ");
                    }
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("type == ");
                    sb2.append(num);
                    sb.append(sb2.toString());
                    i2++;
                }
                cursor = readableDatabase.rawQuery(sb.toString(), null);
                try {
                    if (cursor.moveToFirst()) {
                        j2 = cursor.getLong(0);
                    }
                } catch (Throwable th) {
                    Throwable th2 = th;
                    cursor2 = cursor;
                    th = th2;
                    cx.a(cursor2);
                    this.b.unlock();
                    throw th;
                }
            } else {
                cursor = null;
            }
            cx.a(cursor);
        } catch (Throwable th3) {
            th = th3;
            cx.a(cursor2);
            this.b.unlock();
            throw th;
        }
        this.b.unlock();
        return j2;
    }

    public void a(@NonNull nw nwVar) {
        this.k.add(nwVar);
    }

    private static String a(et etVar) {
        StringBuilder sb = new StringBuilder();
        sb.append("DatabaseWorker [");
        sb.append(etVar.b().c());
        sb.append(RequestParameters.RIGHT_BRACKETS);
        return sb.toString();
    }

    public void a(long j2, jh jhVar, long j3) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("id", Long.valueOf(j2));
        contentValues.put("start_time", Long.valueOf(j3));
        contentValues.put("server_time_offset", Long.valueOf(wi.c()));
        contentValues.put("obtained_before_first_sync", Boolean.valueOf(wd.a().d()));
        contentValues.put("type", Integer.valueOf(jhVar.a()));
        new y(this.h).a(this.i.i()).a(contentValues).a();
        a(contentValues);
    }

    public void a(@NonNull ww wwVar, int i2, @NonNull je jeVar, @NonNull com.yandex.metrica.impl.ob.i.a aVar, @NonNull er erVar) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("number", Long.valueOf(jeVar.c()));
        contentValues.put("global_number", Integer.valueOf(af.e(i2) ? erVar.a() : 0));
        contentValues.put("number_of_type", Integer.valueOf(erVar.a(i2)));
        contentValues.put(LocationConst.TIME, Long.valueOf(jeVar.d()));
        contentValues.put("session_id", Long.valueOf(jeVar.a()));
        contentValues.put("session_type", Integer.valueOf(jeVar.b().a()));
        new y(this.h).a(this.i.i()).a(contentValues).a(wwVar, aVar);
        b(contentValues);
    }

    private long e() {
        this.b.lock();
        long j2 = 0;
        try {
            SQLiteDatabase readableDatabase = this.d.getReadableDatabase();
            if (readableDatabase != null) {
                j2 = vl.a(readableDatabase, "reports");
            }
        } catch (Throwable th) {
            this.b.unlock();
            throw th;
        }
        this.b.unlock();
        return j2;
    }

    public void a(ContentValues contentValues) {
        c(contentValues);
    }

    public void b(ContentValues contentValues) {
        synchronized (this.f) {
            this.g.add(contentValues);
        }
        synchronized (this.e) {
            this.e.notifyAll();
        }
    }

    public int a(long j2) {
        this.c.lock();
        int i2 = 0;
        try {
            if (lq.f4973a.booleanValue()) {
                f();
            }
            SQLiteDatabase writableDatabase = this.d.getWritableDatabase();
            if (writableDatabase != null) {
                i2 = writableDatabase.delete("sessions", g.d, new String[]{String.valueOf(j2)});
            }
        } catch (Throwable th) {
            this.c.unlock();
            throw th;
        }
        this.c.unlock();
        return i2;
    }

    private void f() {
        Cursor cursor;
        Cursor cursor2;
        this.b.lock();
        Cursor cursor3 = null;
        try {
            SQLiteDatabase readableDatabase = this.d.getReadableDatabase();
            if (readableDatabase != null) {
                cursor = readableDatabase.rawQuery(" SELECT DISTINCT id From sessions order by id asc ", new String[0]);
                try {
                    StringBuffer stringBuffer = new StringBuffer();
                    stringBuffer.append("All sessions in db: ");
                    while (cursor.moveToNext()) {
                        stringBuffer.append(cursor.getString(0));
                        stringBuffer.append(", ");
                    }
                    cursor2 = readableDatabase.rawQuery(" SELECT DISTINCT session_id From reports order by session_id asc ", new String[0]);
                } catch (Throwable th) {
                    th = th;
                    this.b.unlock();
                    cx.a(cursor);
                    cx.a(cursor3);
                    throw th;
                }
                try {
                    StringBuffer stringBuffer2 = new StringBuffer();
                    stringBuffer2.append("All sessions in reports db: ");
                    while (cursor2.moveToNext()) {
                        stringBuffer2.append(cursor2.getString(0));
                        stringBuffer2.append(", ");
                    }
                    cursor3 = cursor;
                } catch (Throwable th2) {
                    Cursor cursor4 = cursor2;
                    th = th2;
                    cursor3 = cursor4;
                    this.b.unlock();
                    cx.a(cursor);
                    cx.a(cursor3);
                    throw th;
                }
            } else {
                cursor2 = null;
            }
            this.b.unlock();
            cx.a(cursor3);
            cx.a(cursor2);
        } catch (Throwable th3) {
            th = th3;
            cursor = null;
            this.b.unlock();
            cx.a(cursor);
            cx.a(cursor3);
            throw th;
        }
    }

    public void c() {
        try {
            this.c.lock();
            if (this.j.get() > this.i.i().W()) {
                SQLiteDatabase writableDatabase = this.d.getWritableDatabase();
                if (writableDatabase != null) {
                    this.j.addAndGet((long) (-a(writableDatabase)));
                }
            }
        } catch (Throwable th) {
            this.c.unlock();
            throw th;
        }
        this.c.unlock();
    }

    private int a(SQLiteDatabase sQLiteDatabase) {
        try {
            Integer[] numArr = new Integer[af.f4635a.size()];
            Iterator it = af.f4635a.iterator();
            int i2 = 0;
            while (it.hasNext()) {
                int i3 = i2 + 1;
                numArr[i2] = Integer.valueOf(((com.yandex.metrica.impl.ob.af.a) it.next()).a());
                i2 = i3;
            }
            return this.l.a(sQLiteDatabase, "reports", String.format("%1$s NOT IN (%2$s) AND (%3$s IN (SELECT %3$s FROM %4$s ORDER BY %5$s, %6$s LIMIT (SELECT count() FROM %4$s) / %7$s ))", new Object[]{"type", TextUtils.join(",", numArr), "id", "reports", "session_id", "number", Integer.valueOf(10)}), b.DB_OVERFLOW, this.i.b().a(), true).b;
        } catch (Throwable th) {
            tl.a(this.h).reportError("deleteExcessiveReports exception", th);
            return 0;
        }
    }

    public void a(long j2, int i2, int i3, boolean z) throws SQLiteException {
        if (i3 > 0) {
            this.c.lock();
            try {
                String format = String.format(Locale.US, "%1$s = %2$s AND %3$s = %4$s AND %5$s <= (SELECT %5$s FROM %6$s WHERE %1$s = %2$s AND %3$s = %4$s ORDER BY %5$s ASC LIMIT %7$s, 1)", new Object[]{"session_id", Long.toString(j2), "session_type", Integer.toString(i2), "id", "reports", Integer.toString(i3 - 1)});
                SQLiteDatabase writableDatabase = this.d.getWritableDatabase();
                if (writableDatabase != null) {
                    a a2 = this.l.a(writableDatabase, "reports", format, b.BAD_REQUEST, this.i.b().a(), z);
                    if (a2.f4954a != null) {
                        ArrayList arrayList = new ArrayList();
                        for (ContentValues g2 : a2.f4954a) {
                            arrayList.add(Integer.valueOf(g(g2)));
                        }
                        for (nw b2 : this.k) {
                            b2.b(arrayList);
                        }
                    }
                    if (this.i.l().c() && a2.f4954a != null) {
                        a(a2.f4954a, "Event removed from db");
                    }
                    this.j.addAndGet((long) (-a2.b));
                }
            } catch (Throwable th) {
                this.c.unlock();
                throw th;
            }
            this.c.unlock();
        }
    }

    public Cursor a(Map<String, String> map) {
        this.b.lock();
        Cursor cursor = null;
        try {
            SQLiteDatabase readableDatabase = this.d.getReadableDatabase();
            if (readableDatabase != null) {
                cursor = readableDatabase.query("sessions", null, a("id >= ?", map), a(new String[]{Long.toString(0)}, map), null, null, "id ASC", null);
            }
        } catch (Throwable th) {
            this.b.unlock();
            throw th;
        }
        this.b.unlock();
        return cursor;
    }

    public Cursor a(long j2, jh jhVar) throws SQLiteException {
        this.b.lock();
        Cursor cursor = null;
        try {
            SQLiteDatabase readableDatabase = this.d.getReadableDatabase();
            if (readableDatabase != null) {
                cursor = readableDatabase.query("reports", null, "session_id = ? AND session_type = ?", new String[]{Long.toString(j2), Integer.toString(jhVar.a())}, null, null, "number ASC", null);
            }
        } catch (Throwable th) {
            this.b.unlock();
            throw th;
        }
        this.b.unlock();
        return cursor;
    }

    private void c(ContentValues contentValues) {
        if (contentValues != null) {
            this.c.lock();
            try {
                SQLiteDatabase writableDatabase = this.d.getWritableDatabase();
                if (writableDatabase != null) {
                    writableDatabase.insertOrThrow("sessions", null, contentValues);
                }
            } catch (Throwable th) {
                this.c.unlock();
                throw th;
            }
            this.c.unlock();
        }
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public void a(List<ContentValues> list) {
        SQLiteDatabase sQLiteDatabase;
        if (list != null && !list.isEmpty()) {
            this.c.lock();
            try {
                sQLiteDatabase = this.d.getWritableDatabase();
                if (sQLiteDatabase != null) {
                    try {
                        sQLiteDatabase.beginTransaction();
                        for (ContentValues contentValues : list) {
                            sQLiteDatabase.insertOrThrow("reports", null, contentValues);
                            this.j.incrementAndGet();
                            a(contentValues, "Event saved to db");
                        }
                        sQLiteDatabase.setTransactionSuccessful();
                        this.j.get();
                    } catch (Throwable th) {
                        th = th;
                        cx.a(sQLiteDatabase);
                        this.c.unlock();
                        throw th;
                    }
                }
            } catch (Throwable th2) {
                th = th2;
                sQLiteDatabase = null;
                cx.a(sQLiteDatabase);
                this.c.unlock();
                throw th;
            }
            cx.a(sQLiteDatabase);
            this.c.unlock();
        }
    }

    private void a(ContentValues contentValues, String str) {
        if (af.b(d(contentValues))) {
            StringBuilder sb = new StringBuilder(str);
            sb.append(": ");
            sb.append(e(contentValues));
            String f2 = f(contentValues);
            if (af.c(g(contentValues)) && !TextUtils.isEmpty(f2)) {
                sb.append(" with value ");
                sb.append(f2);
            }
            this.i.l().a(sb.toString());
        }
    }

    private void a(List<ContentValues> list, String str) {
        for (int i2 = 0; i2 < list.size(); i2++) {
            a((ContentValues) list.get(i2), str);
        }
    }

    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:16:0x003d */
    @NonNull
    public List<ContentValues> d() {
        ArrayList arrayList = new ArrayList();
        this.b.lock();
        Cursor cursor = null;
        SQLiteDatabase readableDatabase = this.d.getReadableDatabase();
        if (readableDatabase != null) {
            Cursor rawQuery = readableDatabase.rawQuery(g.c, null);
            while (rawQuery.moveToNext()) {
                try {
                    ContentValues contentValues = new ContentValues();
                    DatabaseUtils.cursorRowToContentValues(rawQuery, contentValues);
                    arrayList.add(contentValues);
                } catch (Throwable th) {
                    th = th;
                    cursor = rawQuery;
                    cx.a(cursor);
                    this.b.unlock();
                    throw th;
                }
            }
            cursor = rawQuery;
        }
        cx.a(cursor);
        this.b.unlock();
        return arrayList;
    }

    public ContentValues b(long j2, jh jhVar) {
        Cursor cursor;
        ContentValues contentValues = new ContentValues();
        this.b.lock();
        Cursor cursor2 = null;
        try {
            SQLiteDatabase readableDatabase = this.d.getReadableDatabase();
            if (readableDatabase != null) {
                cursor = readableDatabase.rawQuery(String.format(Locale.US, "SELECT report_request_parameters FROM sessions WHERE id = %s AND type = %s ORDER BY id DESC LIMIT 1", new Object[]{Long.valueOf(j2), Integer.valueOf(jhVar.a())}), null);
                try {
                    if (cursor.moveToNext()) {
                        ContentValues contentValues2 = new ContentValues();
                        DatabaseUtils.cursorRowToContentValues(cursor, contentValues2);
                        contentValues = contentValues2;
                    }
                } catch (Throwable th) {
                    th = th;
                    cursor2 = cursor;
                    cx.a(cursor2);
                    this.b.unlock();
                    throw th;
                }
            } else {
                cursor = null;
            }
            cx.a(cursor);
        } catch (Throwable th2) {
            th = th2;
            cx.a(cursor2);
            this.b.unlock();
            throw th;
        }
        this.b.unlock();
        return contentValues;
    }

    private static String a(String str, Map<String, String> map) {
        StringBuilder sb = new StringBuilder(str);
        for (String str2 : map.keySet()) {
            sb.append(sb.length() > 0 ? " AND " : "");
            StringBuilder sb2 = new StringBuilder();
            sb2.append(str2);
            sb2.append(" = ? ");
            sb.append(sb2.toString());
        }
        if (TextUtils.isEmpty(sb.toString())) {
            return null;
        }
        return sb.toString();
    }

    private static String[] a(String[] strArr, Map<String, String> map) {
        ArrayList arrayList = new ArrayList();
        arrayList.addAll(Arrays.asList(strArr));
        for (Entry value : map.entrySet()) {
            arrayList.add(value.getValue());
        }
        return (String[]) arrayList.toArray(new String[arrayList.size()]);
    }

    private static int d(ContentValues contentValues) {
        Integer asInteger = contentValues.getAsInteger("type");
        if (asInteger != null) {
            return asInteger.intValue();
        }
        return -1;
    }

    private String e(ContentValues contentValues) {
        return b(contentValues, "name");
    }

    private String f(ContentValues contentValues) {
        return b(contentValues, "value");
    }

    private String b(ContentValues contentValues, String str) {
        return cu.b(contentValues.getAsString(str), "");
    }

    /* access modifiers changed from: private */
    public int g(ContentValues contentValues) {
        return c(contentValues, "type");
    }

    private int c(ContentValues contentValues, String str) {
        return contentValues.getAsInteger(str).intValue();
    }

    public void close() {
        this.g.clear();
        this.e.a();
    }

    /* access modifiers changed from: private */
    public boolean g() {
        boolean isEmpty;
        synchronized (this.f) {
            isEmpty = this.g.isEmpty();
        }
        return isEmpty;
    }
}
