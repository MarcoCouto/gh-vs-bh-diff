package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.net.Uri.Builder;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import java.util.Collection;

public class pd extends bp<sx> {
    @NonNull
    private Context j;
    @NonNull
    private ph k;
    @NonNull
    private final cs l;
    @NonNull
    private oh m;
    @NonNull
    private ly n;
    @NonNull
    private final pf o;
    private long p;
    private pe q;

    public pd(@NonNull Context context, @NonNull ph phVar, @NonNull cs csVar) {
        this(context, phVar, csVar, new ly(ld.a(context).c()), new sx(), new pf(context));
    }

    public boolean a() {
        if (!this.l.d() && !TextUtils.isEmpty(this.k.r()) && !TextUtils.isEmpty(this.k.t()) && !cx.a((Collection) s())) {
            return H();
        }
        return false;
    }

    public boolean b() {
        boolean b = super.b();
        J();
        return b;
    }

    /* access modifiers changed from: protected */
    public void a(@NonNull Builder builder) {
        ((sx) this.i).a(builder, this.k);
    }

    /* access modifiers changed from: protected */
    public void E() {
        G();
    }

    /* access modifiers changed from: protected */
    public void F() {
        G();
    }

    private void G() {
        this.o.a(this.q);
    }

    public boolean t() {
        return super.t() & (400 != k());
    }

    private boolean H() {
        this.q = this.o.a(this.m.h);
        if (!this.q.a()) {
            return c(e.a((e) this.q.c));
        }
        return false;
    }

    private void I() {
        this.p = this.n.b(-1) + 1;
        ((sx) this.i).a(this.p);
    }

    private void J() {
        this.n.c(this.p).q();
    }

    @VisibleForTesting
    pd(@NonNull Context context, @NonNull ph phVar, @NonNull cs csVar, @NonNull ly lyVar, @NonNull sx sxVar, @NonNull pf pfVar) {
        super(sxVar);
        this.j = context;
        this.k = phVar;
        this.l = csVar;
        this.m = this.k.a();
        this.n = lyVar;
        this.o = pfVar;
        I();
        a(this.k.b());
    }
}
