package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.io.Closeable;

public class ge implements et, eu, gj {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Context f4848a;
    @NonNull
    private final ek b;
    @NonNull
    private final ac c;
    @NonNull
    private gf d;
    @NonNull
    private fk e;
    @NonNull
    private final com.yandex.metrica.CounterConfiguration.a f;

    static class a {
        a() {
        }

        public gf a(@NonNull Context context, @NonNull ek ekVar, @NonNull uk ukVar, @NonNull com.yandex.metrica.impl.ob.gi.a aVar) {
            return new gf(new com.yandex.metrica.impl.ob.gi.b(context, ekVar.b()), ukVar, aVar);
        }
    }

    static class b {
        b() {
        }

        public ac<ge> a(@NonNull ge geVar, @NonNull un unVar, @NonNull bl blVar, @NonNull gk gkVar, @NonNull lw lwVar) {
            ac acVar = new ac(geVar, unVar.a(), blVar, gkVar, lwVar);
            return acVar;
        }
    }

    public void a(@NonNull ue ueVar, @Nullable uk ukVar) {
    }

    public ge(@NonNull Context context, @NonNull ek ekVar, @NonNull bl blVar, @NonNull com.yandex.metrica.impl.ob.eg.a aVar, @NonNull uk ukVar, @NonNull un unVar, @NonNull com.yandex.metrica.CounterConfiguration.a aVar2) {
        Context context2 = context;
        this(context2, ekVar, blVar, aVar, ukVar, unVar, aVar2, new gk(), new b(), new a(), new fk(context, ekVar), new lw(ld.a(context).b(ekVar)));
    }

    public ge(@NonNull Context context, @NonNull ek ekVar, @NonNull bl blVar, @NonNull com.yandex.metrica.impl.ob.eg.a aVar, @NonNull uk ukVar, @NonNull un unVar, @NonNull com.yandex.metrica.CounterConfiguration.a aVar2, @NonNull gk gkVar, @NonNull b bVar, @NonNull a aVar3, @NonNull fk fkVar, @NonNull lw lwVar) {
        ek ekVar2 = ekVar;
        uk ukVar2 = ukVar;
        this.f4848a = context;
        this.b = ekVar2;
        this.e = fkVar;
        this.f = aVar2;
        this.c = bVar.a(this, unVar, blVar, gkVar, lwVar);
        synchronized (this) {
            this.e.a(ukVar2.y);
            com.yandex.metrica.impl.ob.eg.a aVar4 = aVar;
            this.d = aVar3.a(context, ekVar, ukVar, new com.yandex.metrica.impl.ob.gi.a(aVar));
        }
    }

    @NonNull
    public ek b() {
        return this.b;
    }

    public void a(@NonNull com.yandex.metrica.impl.ob.eg.a aVar) {
        this.d.a((Object) aVar);
    }

    public void a(@NonNull w wVar) {
        this.c.a(wVar);
    }

    public void a() {
        if (this.e.a(((gi) this.d.d()).c())) {
            a(af.a());
            this.e.a();
        }
    }

    public synchronized void a(@Nullable uk ukVar) {
        this.d.a(ukVar);
        this.e.a(ukVar.y);
    }

    @NonNull
    public gi d() {
        return (gi) this.d.d();
    }

    public void c() {
        cx.a((Closeable) this.c);
    }
}
