package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;

public class iu extends it {
    iu(@NonNull en enVar, @NonNull jc jcVar) {
        this(enVar, jcVar, new jg(enVar.y(), "background"));
    }

    @VisibleForTesting
    iu(@NonNull en enVar, @NonNull jc jcVar, @NonNull jg jgVar) {
        super(enVar, jcVar, jgVar, jb.a(jh.BACKGROUND).a(3600).a());
    }
}
