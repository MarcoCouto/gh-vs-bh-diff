package com.yandex.metrica.impl.ob;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.InputDeviceCompat;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

public final class af {

    /* renamed from: a reason: collision with root package name */
    public static final EnumSet<a> f4635a = EnumSet.of(a.EVENT_TYPE_INIT, new a[]{a.EVENT_TYPE_FIRST_ACTIVATION, a.EVENT_TYPE_REFERRER_RECEIVED_FROM_BROADCAST, a.EVENT_TYPE_SEND_REFERRER, a.EVENT_TYPE_APP_UPDATE, a.EVENT_TYPE_CLEANUP});
    private static final EnumSet<a> b = EnumSet.of(a.EVENT_TYPE_UNDEFINED, new a[]{a.EVENT_TYPE_PURGE_BUFFER, a.EVENT_TYPE_REFERRER_RECEIVED_FROM_BROADCAST, a.EVENT_TYPE_SEND_REFERRER, a.EVENT_TYPE_APP_ENVIRONMENT_UPDATED, a.EVENT_TYPE_APP_ENVIRONMENT_CLEARED, a.EVENT_TYPE_ACTIVATION, a.EVENT_TYPE_PREV_SESSION_NATIVE_CRASH});
    private static final EnumSet<a> c = EnumSet.of(a.EVENT_TYPE_SET_USER_INFO, new a[]{a.EVENT_TYPE_REPORT_USER_INFO, a.EVENT_TYPE_IDENTITY, a.EVENT_TYPE_UNDEFINED, a.EVENT_TYPE_INIT, a.EVENT_TYPE_APP_UPDATE, a.EVENT_TYPE_SEND_REFERRER, a.EVENT_TYPE_ALIVE, a.EVENT_TYPE_STARTUP, a.EVENT_TYPE_APP_ENVIRONMENT_UPDATED, a.EVENT_TYPE_APP_ENVIRONMENT_CLEARED, a.EVENT_TYPE_ACTIVATION, a.EVENT_TYPE_PREV_SESSION_NATIVE_CRASH});
    private static final EnumSet<a> d = EnumSet.of(a.EVENT_TYPE_UPDATE_FOREGROUND_TIME, a.EVENT_TYPE_SET_USER_INFO, a.EVENT_TYPE_REPORT_USER_INFO, a.EVENT_TYPE_EXCEPTION_UNHANDLED_FROM_FILE);
    private static final EnumSet<a> e = EnumSet.of(a.EVENT_TYPE_EXCEPTION_UNHANDLED, new a[]{a.EVENT_TYPE_EXCEPTION_UNHANDLED_FROM_FILE, a.EVENT_TYPE_EXCEPTION_UNHANDLED_FROM_INTENT, a.EVENT_TYPE_EXCEPTION_UNHANDLED_PROTOBUF, a.EVENT_TYPE_EXCEPTION_USER, a.EVENT_TYPE_EXCEPTION_USER_PROTOBUF, a.EVENT_TYPE_PREV_SESSION_NATIVE_CRASH, a.EVENT_TYPE_CURRENT_SESSION_NATIVE_CRASH, a.EVENT_TYPE_REGULAR});
    private static final EnumSet<a> f = EnumSet.of(a.EVENT_TYPE_DIAGNOSTIC, a.EVENT_TYPE_DIAGNOSTIC_STATBOX, a.EVENT_TYPE_DIAGNOSTIC_DISABLE_STAT_SENDING);
    private static final EnumSet<a> g = EnumSet.of(a.EVENT_TYPE_REGULAR);
    private static final EnumSet<a> h = EnumSet.of(a.EVENT_TYPE_PREV_SESSION_NATIVE_CRASH);

    @SuppressLint({"UseSparseArrays"})
    public enum a {
        EVENT_TYPE_UNDEFINED(-1, "Unrecognized action"),
        EVENT_TYPE_INIT(0, "First initialization event"),
        EVENT_TYPE_REGULAR(1, "Regular event"),
        EVENT_TYPE_UPDATE_FOREGROUND_TIME(3, "Update foreground time"),
        EVENT_TYPE_EXCEPTION_USER(5, "Error from developer"),
        EVENT_TYPE_ALIVE(7, "App is still alive"),
        EVENT_TYPE_SET_USER_INFO(9, "User info"),
        EVENT_TYPE_REPORT_USER_INFO(10, "Report user info"),
        EVENT_TYPE_SEND_USER_PROFILE(40961, "Send user profile"),
        EVENT_TYPE_SET_USER_PROFILE_ID(40962, "Set user profile ID"),
        EVENT_TYPE_SEND_REVENUE_EVENT(40976, "Send revenue event"),
        EVENT_TYPE_PURGE_BUFFER(256, "Forcible buffer clearing"),
        EVENT_TYPE_PREV_SESSION_NATIVE_CRASH(768, "Native crash of app, read from file"),
        EVENT_TYPE_CURRENT_SESSION_NATIVE_CRASH(769, "Native crash of app, caught by FileObserver"),
        EVENT_TYPE_STARTUP(1536, "Sending the startup due to lack of data"),
        EVENT_TYPE_IDENTITY(1792, "System identification"),
        EVENT_TYPE_IDENTITY_LIGHT(1793, "Identity light"),
        EVENT_TYPE_DIAGNOSTIC(2048, "Diagnostic event"),
        EVENT_TYPE_DIAGNOSTIC_STATBOX(2049, "Diagnostic statbox event"),
        EVENT_TYPE_DIAGNOSTIC_DISABLE_STAT_SENDING(2050, "Disable stat send"),
        EVENT_TYPE_STATBOX(2304, "Event with statistical data"),
        EVENT_TYPE_REFERRER_RECEIVED_FROM_BROADCAST(4096, "Referrer received"),
        EVENT_TYPE_SEND_REFERRER(FragmentTransaction.TRANSIT_FRAGMENT_OPEN, "Send referrer"),
        EVENT_TYPE_REFERRER_OBTAINED_FROM_SERVICES(InputDeviceCompat.SOURCE_TOUCHSCREEN, "Referrer obtained"),
        EVENT_TYPE_APP_ENVIRONMENT_UPDATED(5376, "App Environment Updated"),
        EVENT_TYPE_APP_ENVIRONMENT_CLEARED(5632, "App Environment Cleared"),
        EVENT_TYPE_EXCEPTION_UNHANDLED(5888, "Crash of App"),
        EVENT_TYPE_EXCEPTION_UNHANDLED_FROM_FILE(5889, "Crash of App, read from file"),
        EVENT_TYPE_EXCEPTION_UNHANDLED_FROM_INTENT(5890, "Crash of App, passed to server via intent"),
        EVENT_TYPE_EXCEPTION_UNHANDLED_PROTOBUF(5891, "Crash of App"),
        EVENT_TYPE_EXCEPTION_USER_PROTOBUF(5892, "Error from developer"),
        EVENT_TYPE_ANR(5968, "ANR"),
        EVENT_TYPE_ACTIVATION(6144, "Activation of metrica"),
        EVENT_TYPE_FIRST_ACTIVATION(6145, "First activation of metrica"),
        EVENT_TYPE_START(6400, "Start of new session"),
        EVENT_TYPE_CUSTOM_EVENT(8192, "Custom event"),
        EVENT_TYPE_APP_OPEN(8208, "App open event"),
        EVENT_TYPE_APP_UPDATE(8224, "App update event"),
        EVENT_TYPE_PERMISSIONS(12288, "Permissions changed event"),
        EVENT_TYPE_APP_FEATURES(12289, "Features, required by application"),
        EVENT_TYPE_UPDATE_PRE_ACTIVATION_CONFIG(16384, "Update pre-activation config"),
        EVENT_TYPE_CLEANUP(12290, "Cleanup database");
        
        static final HashMap<Integer, a> Q = null;
        private final int R;
        private final String S;

        static {
            int i;
            a[] values;
            Q = new HashMap<>();
            for (a aVar : values()) {
                Q.put(Integer.valueOf(aVar.a()), aVar);
            }
        }

        private a(int i, String str) {
            this.R = i;
            this.S = str;
        }

        public int a() {
            return this.R;
        }

        public String b() {
            return this.S;
        }

        public static a a(int i) {
            a aVar = (a) Q.get(Integer.valueOf(i));
            return aVar == null ? EVENT_TYPE_UNDEFINED : aVar;
        }
    }

    public static boolean a(a aVar) {
        return !b.contains(aVar);
    }

    public static boolean b(a aVar) {
        return !c.contains(aVar);
    }

    public static boolean a(int i) {
        return d.contains(a.a(i));
    }

    public static boolean b(int i) {
        return e.contains(a.a(i));
    }

    public static boolean c(int i) {
        return g.contains(a.a(i));
    }

    public static boolean d(int i) {
        return f.contains(a.a(i));
    }

    public static boolean e(int i) {
        return !h.contains(a.a(i));
    }

    public static w a(@NonNull String str, @NonNull vz vzVar) {
        return a(a.EVENT_TYPE_PREV_SESSION_NATIVE_CRASH, str, vzVar);
    }

    public static w b(@NonNull String str, @NonNull vz vzVar) {
        return a(a.EVENT_TYPE_CURRENT_SESSION_NATIVE_CRASH, str, vzVar);
    }

    static w a(a aVar, String str, @NonNull vz vzVar) {
        return new r(str, "", aVar.a(), vzVar);
    }

    public static w a(a aVar, @NonNull vz vzVar) {
        return new r("", aVar.a(), vzVar);
    }

    public static w c(String str, @NonNull vz vzVar) {
        return new r(str, a.EVENT_TYPE_REGULAR.a(), vzVar);
    }

    static w a(String str, String str2, @NonNull vz vzVar) {
        return new r(str2, str, a.EVENT_TYPE_REGULAR.a(), vzVar);
    }

    static w b(String str, String str2, @NonNull vz vzVar) {
        return new r(str2, str, a.EVENT_TYPE_STATBOX.a(), vzVar);
    }

    public static w a() {
        w a2 = new w().a(a.EVENT_TYPE_DIAGNOSTIC_DISABLE_STAT_SENDING.a());
        try {
            a2.c(vq.a().toString());
        } catch (Throwable unused) {
        }
        return a2;
    }

    static w a(byte[] bArr, @NonNull vz vzVar) {
        return new r(bArr, "", a.EVENT_TYPE_ANR.a(), vzVar);
    }

    static w a(String str, byte[] bArr, @NonNull vz vzVar) {
        return new r(bArr, str, a.EVENT_TYPE_EXCEPTION_USER_PROTOBUF.a(), vzVar);
    }

    static w d(String str, @NonNull vz vzVar) {
        return new r(str, a.EVENT_TYPE_START.a(), vzVar);
    }

    static w e(String str, @NonNull vz vzVar) {
        return new r(str, a.EVENT_TYPE_UPDATE_FOREGROUND_TIME.a(), vzVar);
    }

    public static r b(String str, byte[] bArr, @NonNull vz vzVar) {
        return a(bArr, str, a.EVENT_TYPE_EXCEPTION_UNHANDLED_PROTOBUF, vzVar);
    }

    public static w a(String str, byte[] bArr, int i, @NonNull HashMap<com.yandex.metrica.impl.ob.r.a, Integer> hashMap, @NonNull vz vzVar) {
        return a(bArr, str, a.EVENT_TYPE_EXCEPTION_UNHANDLED_FROM_FILE, vzVar).a(hashMap).c(i);
    }

    private static r a(byte[] bArr, String str, a aVar, @NonNull vz vzVar) {
        return new r(bArr, str, aVar.a(), vzVar);
    }

    public static w f(String str, @NonNull vz vzVar) {
        return new r("", str, a.EVENT_TYPE_REFERRER_RECEIVED_FROM_BROADCAST.a(), vzVar);
    }

    public static w a(@Nullable sj sjVar, @NonNull vz vzVar) {
        if (sjVar == null) {
            return new r("", a.EVENT_TYPE_REFERRER_OBTAINED_FROM_SERVICES.a(), vzVar);
        }
        return new r(sjVar.f5159a, a.EVENT_TYPE_REFERRER_OBTAINED_FROM_SERVICES.a(), vzVar).a(sjVar.a());
    }

    static w g(String str, @NonNull vz vzVar) {
        return c("open", str, vzVar);
    }

    @Deprecated
    static w h(String str, @NonNull vz vzVar) {
        return c("referral", str, vzVar);
    }

    static w c(String str, String str2, @NonNull vz vzVar) {
        HashMap hashMap = new HashMap();
        hashMap.put("type", str);
        hashMap.put("link", str2);
        return new r(vq.b((Map) hashMap), "", a.EVENT_TYPE_APP_OPEN.a(), vzVar);
    }

    public static w a(bu buVar, @NonNull vz vzVar) {
        return new r(buVar == null ? "" : buVar.a(), "", a.EVENT_TYPE_ACTIVATION.a(), vzVar);
    }

    public static w i(@NonNull String str, @NonNull vz vzVar) {
        return new r(str, "", a.EVENT_TYPE_CLEANUP.a(), vzVar);
    }

    static w a(int i, String str, String str2, Map<String, Object> map, @NonNull vz vzVar) {
        r rVar = new r(str2, str, a.EVENT_TYPE_CUSTOM_EVENT.a(), i, vzVar);
        return rVar.e(vq.b((Map) map));
    }
}
