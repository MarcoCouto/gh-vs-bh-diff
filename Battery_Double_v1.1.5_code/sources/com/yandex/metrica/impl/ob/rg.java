package com.yandex.metrica.impl.ob;

import java.io.IOException;

public interface rg {

    public static final class a extends e {
        public e b;
        public e[] c;

        public a() {
            d();
        }

        public a d() {
            this.b = null;
            this.c = e.d();
            this.f4803a = -1;
            return this;
        }

        public void a(b bVar) throws IOException {
            if (this.b != null) {
                bVar.a(1, (e) this.b);
            }
            if (this.c != null && this.c.length > 0) {
                for (e eVar : this.c) {
                    if (eVar != null) {
                        bVar.a(2, (e) eVar);
                    }
                }
            }
            super.a(bVar);
        }

        /* access modifiers changed from: protected */
        public int c() {
            int c2 = super.c();
            if (this.b != null) {
                c2 += b.b(1, (e) this.b);
            }
            if (this.c != null && this.c.length > 0) {
                for (e eVar : this.c) {
                    if (eVar != null) {
                        c2 += b.b(2, (e) eVar);
                    }
                }
            }
            return c2;
        }

        /* renamed from: b */
        public a a(a aVar) throws IOException {
            while (true) {
                int a2 = aVar.a();
                if (a2 == 0) {
                    return this;
                }
                if (a2 == 10) {
                    if (this.b == null) {
                        this.b = new e();
                    }
                    aVar.a((e) this.b);
                } else if (a2 == 18) {
                    int b2 = g.b(aVar, 18);
                    int length = this.c == null ? 0 : this.c.length;
                    e[] eVarArr = new e[(b2 + length)];
                    if (length != 0) {
                        System.arraycopy(this.c, 0, eVarArr, 0, length);
                    }
                    while (length < eVarArr.length - 1) {
                        eVarArr[length] = new e();
                        aVar.a((e) eVarArr[length]);
                        aVar.a();
                        length++;
                    }
                    eVarArr[length] = new e();
                    aVar.a((e) eVarArr[length]);
                    this.c = eVarArr;
                } else if (!g.a(aVar, a2)) {
                    return this;
                }
            }
        }
    }

    public static final class b extends e {
        public a b;
        public String c;
        public int d;

        public b() {
            d();
        }

        public b d() {
            this.b = null;
            this.c = "";
            this.d = -1;
            this.f4803a = -1;
            return this;
        }

        public void a(b bVar) throws IOException {
            if (this.b != null) {
                bVar.a(1, (e) this.b);
            }
            if (!this.c.equals("")) {
                bVar.a(2, this.c);
            }
            if (this.d != -1) {
                bVar.a(3, this.d);
            }
            super.a(bVar);
        }

        /* access modifiers changed from: protected */
        public int c() {
            int c2 = super.c();
            if (this.b != null) {
                c2 += b.b(1, (e) this.b);
            }
            if (!this.c.equals("")) {
                c2 += b.b(2, this.c);
            }
            return this.d != -1 ? c2 + b.d(3, this.d) : c2;
        }

        /* renamed from: b */
        public b a(a aVar) throws IOException {
            while (true) {
                int a2 = aVar.a();
                if (a2 == 0) {
                    return this;
                }
                if (a2 == 10) {
                    if (this.b == null) {
                        this.b = new a();
                    }
                    aVar.a((e) this.b);
                } else if (a2 != 18) {
                    if (a2 == 24) {
                        int g = aVar.g();
                        switch (g) {
                            case -1:
                            case 0:
                            case 1:
                                this.d = g;
                                break;
                        }
                    } else if (!g.a(aVar, a2)) {
                        return this;
                    }
                } else {
                    this.c = aVar.i();
                }
            }
        }
    }

    public static final class c extends e {
        public f b;
        public a c;
        public String d;
        public int e;

        public c() {
            d();
        }

        public c d() {
            this.b = null;
            this.c = null;
            this.d = "";
            this.e = -1;
            this.f4803a = -1;
            return this;
        }

        public void a(b bVar) throws IOException {
            if (this.b != null) {
                bVar.a(1, (e) this.b);
            }
            if (this.c != null) {
                bVar.a(2, (e) this.c);
            }
            if (!this.d.equals("")) {
                bVar.a(3, this.d);
            }
            if (this.e != -1) {
                bVar.a(4, this.e);
            }
            super.a(bVar);
        }

        /* access modifiers changed from: protected */
        public int c() {
            int c2 = super.c();
            if (this.b != null) {
                c2 += b.b(1, (e) this.b);
            }
            if (this.c != null) {
                c2 += b.b(2, (e) this.c);
            }
            if (!this.d.equals("")) {
                c2 += b.b(3, this.d);
            }
            return this.e != -1 ? c2 + b.d(4, this.e) : c2;
        }

        /* renamed from: b */
        public c a(a aVar) throws IOException {
            while (true) {
                int a2 = aVar.a();
                if (a2 == 0) {
                    return this;
                }
                if (a2 == 10) {
                    if (this.b == null) {
                        this.b = new f();
                    }
                    aVar.a((e) this.b);
                } else if (a2 == 18) {
                    if (this.c == null) {
                        this.c = new a();
                    }
                    aVar.a((e) this.c);
                } else if (a2 != 26) {
                    if (a2 == 32) {
                        int g = aVar.g();
                        switch (g) {
                            case -1:
                            case 0:
                            case 1:
                                this.e = g;
                                break;
                        }
                    } else if (!g.a(aVar, a2)) {
                        return this;
                    }
                } else {
                    this.d = aVar.i();
                }
            }
        }
    }

    public static final class d extends e {
        private static volatile d[] g;
        public String b;
        public String c;
        public int d;
        public String e;
        public boolean f;

        public static d[] d() {
            if (g == null) {
                synchronized (c.f4711a) {
                    if (g == null) {
                        g = new d[0];
                    }
                }
            }
            return g;
        }

        public d() {
            e();
        }

        public d e() {
            this.b = "";
            this.c = "";
            this.d = 0;
            this.e = "";
            this.f = false;
            this.f4803a = -1;
            return this;
        }

        public void a(b bVar) throws IOException {
            bVar.a(1, this.b);
            if (!this.c.equals("")) {
                bVar.a(2, this.c);
            }
            bVar.c(3, this.d);
            bVar.a(4, this.e);
            bVar.a(5, this.f);
            super.a(bVar);
        }

        /* access modifiers changed from: protected */
        public int c() {
            int c2 = super.c() + b.b(1, this.b);
            if (!this.c.equals("")) {
                c2 += b.b(2, this.c);
            }
            return c2 + b.f(3, this.d) + b.b(4, this.e) + b.b(5, this.f);
        }

        /* renamed from: b */
        public d a(a aVar) throws IOException {
            while (true) {
                int a2 = aVar.a();
                if (a2 == 0) {
                    return this;
                }
                if (a2 == 10) {
                    this.b = aVar.i();
                } else if (a2 == 18) {
                    this.c = aVar.i();
                } else if (a2 == 24) {
                    this.d = aVar.l();
                } else if (a2 == 34) {
                    this.e = aVar.i();
                } else if (a2 == 40) {
                    this.f = aVar.h();
                } else if (!g.a(aVar, a2)) {
                    return this;
                }
            }
        }
    }

    public static final class e extends e {
        private static volatile e[] h;
        public String b;
        public int c;
        public long d;
        public String e;
        public int f;
        public d[] g;

        public static e[] d() {
            if (h == null) {
                synchronized (c.f4711a) {
                    if (h == null) {
                        h = new e[0];
                    }
                }
            }
            return h;
        }

        public e() {
            e();
        }

        public e e() {
            this.b = "";
            this.c = 0;
            this.d = 0;
            this.e = "";
            this.f = 0;
            this.g = d.d();
            this.f4803a = -1;
            return this;
        }

        public void a(b bVar) throws IOException {
            bVar.a(1, this.b);
            bVar.c(2, this.c);
            bVar.c(3, this.d);
            if (!this.e.equals("")) {
                bVar.a(4, this.e);
            }
            if (this.f != 0) {
                bVar.b(5, this.f);
            }
            if (this.g != null && this.g.length > 0) {
                for (d dVar : this.g) {
                    if (dVar != null) {
                        bVar.a(6, (e) dVar);
                    }
                }
            }
            super.a(bVar);
        }

        /* access modifiers changed from: protected */
        public int c() {
            int c2 = super.c() + b.b(1, this.b) + b.f(2, this.c) + b.f(3, this.d);
            if (!this.e.equals("")) {
                c2 += b.b(4, this.e);
            }
            if (this.f != 0) {
                c2 += b.e(5, this.f);
            }
            if (this.g != null && this.g.length > 0) {
                for (d dVar : this.g) {
                    if (dVar != null) {
                        c2 += b.b(6, (e) dVar);
                    }
                }
            }
            return c2;
        }

        /* renamed from: b */
        public e a(a aVar) throws IOException {
            while (true) {
                int a2 = aVar.a();
                if (a2 == 0) {
                    return this;
                }
                if (a2 == 10) {
                    this.b = aVar.i();
                } else if (a2 == 16) {
                    this.c = aVar.l();
                } else if (a2 == 24) {
                    this.d = aVar.m();
                } else if (a2 == 34) {
                    this.e = aVar.i();
                } else if (a2 == 40) {
                    this.f = aVar.k();
                } else if (a2 == 50) {
                    int b2 = g.b(aVar, 50);
                    int length = this.g == null ? 0 : this.g.length;
                    d[] dVarArr = new d[(b2 + length)];
                    if (length != 0) {
                        System.arraycopy(this.g, 0, dVarArr, 0, length);
                    }
                    while (length < dVarArr.length - 1) {
                        dVarArr[length] = new d();
                        aVar.a((e) dVarArr[length]);
                        aVar.a();
                        length++;
                    }
                    dVarArr[length] = new d();
                    aVar.a((e) dVarArr[length]);
                    this.g = dVarArr;
                } else if (!g.a(aVar, a2)) {
                    return this;
                }
            }
        }
    }

    public static final class f extends e {
        private static volatile f[] g;
        public String b;
        public String c;
        public d[] d;
        public f e;
        public f[] f;

        public static f[] d() {
            if (g == null) {
                synchronized (c.f4711a) {
                    if (g == null) {
                        g = new f[0];
                    }
                }
            }
            return g;
        }

        public f() {
            e();
        }

        public f e() {
            this.b = "";
            this.c = "";
            this.d = d.d();
            this.e = null;
            this.f = d();
            this.f4803a = -1;
            return this;
        }

        public void a(b bVar) throws IOException {
            bVar.a(1, this.b);
            if (!this.c.equals("")) {
                bVar.a(2, this.c);
            }
            if (this.d != null && this.d.length > 0) {
                for (d dVar : this.d) {
                    if (dVar != null) {
                        bVar.a(3, (e) dVar);
                    }
                }
            }
            if (this.e != null) {
                bVar.a(4, (e) this.e);
            }
            if (this.f != null && this.f.length > 0) {
                for (f fVar : this.f) {
                    if (fVar != null) {
                        bVar.a(5, (e) fVar);
                    }
                }
            }
            super.a(bVar);
        }

        /* access modifiers changed from: protected */
        public int c() {
            int c2 = super.c() + b.b(1, this.b);
            if (!this.c.equals("")) {
                c2 += b.b(2, this.c);
            }
            if (this.d != null && this.d.length > 0) {
                int i = c2;
                for (d dVar : this.d) {
                    if (dVar != null) {
                        i += b.b(3, (e) dVar);
                    }
                }
                c2 = i;
            }
            if (this.e != null) {
                c2 += b.b(4, (e) this.e);
            }
            if (this.f != null && this.f.length > 0) {
                for (f fVar : this.f) {
                    if (fVar != null) {
                        c2 += b.b(5, (e) fVar);
                    }
                }
            }
            return c2;
        }

        /* renamed from: b */
        public f a(a aVar) throws IOException {
            while (true) {
                int a2 = aVar.a();
                if (a2 == 0) {
                    return this;
                }
                if (a2 == 10) {
                    this.b = aVar.i();
                } else if (a2 == 18) {
                    this.c = aVar.i();
                } else if (a2 == 26) {
                    int b2 = g.b(aVar, 26);
                    int length = this.d == null ? 0 : this.d.length;
                    d[] dVarArr = new d[(b2 + length)];
                    if (length != 0) {
                        System.arraycopy(this.d, 0, dVarArr, 0, length);
                    }
                    while (length < dVarArr.length - 1) {
                        dVarArr[length] = new d();
                        aVar.a((e) dVarArr[length]);
                        aVar.a();
                        length++;
                    }
                    dVarArr[length] = new d();
                    aVar.a((e) dVarArr[length]);
                    this.d = dVarArr;
                } else if (a2 == 34) {
                    if (this.e == null) {
                        this.e = new f();
                    }
                    aVar.a((e) this.e);
                } else if (a2 == 42) {
                    int b3 = g.b(aVar, 42);
                    int length2 = this.f == null ? 0 : this.f.length;
                    f[] fVarArr = new f[(b3 + length2)];
                    if (length2 != 0) {
                        System.arraycopy(this.f, 0, fVarArr, 0, length2);
                    }
                    while (length2 < fVarArr.length - 1) {
                        fVarArr[length2] = new f();
                        aVar.a((e) fVarArr[length2]);
                        aVar.a();
                        length2++;
                    }
                    fVarArr[length2] = new f();
                    aVar.a((e) fVarArr[length2]);
                    this.f = fVarArr;
                } else if (!g.a(aVar, a2)) {
                    return this;
                }
            }
        }
    }
}
