package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import com.yandex.metrica.impl.ob.e;

public class mg<T, P extends e> implements mf<T> {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final String f4992a;
    @NonNull
    private final kx b;
    @NonNull
    private final me<P> c;
    @NonNull
    private final mq<T, P> d;

    public mg(@NonNull String str, @NonNull kx kxVar, @NonNull me<P> meVar, @NonNull mq<T, P> mqVar) {
        this.f4992a = str;
        this.b = kxVar;
        this.c = meVar;
        this.d = mqVar;
    }

    public void a(@NonNull T t) {
        this.b.a(this.f4992a, this.c.a(this.d.b(t)));
    }

    @NonNull
    public T a() {
        try {
            byte[] a2 = this.b.a(this.f4992a);
            if (cx.a(a2)) {
                return this.d.a(this.c.c());
            }
            return this.d.a(this.c.b(a2));
        } catch (Throwable unused) {
            return this.d.a(this.c.c());
        }
    }
}
