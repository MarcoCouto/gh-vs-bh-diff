package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.yandex.metrica.impl.ob.rh.b.a;

public class pa {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final og f5052a;
    @NonNull
    private final oz b;

    public pa(@NonNull og ogVar, @NonNull oz ozVar) {
        this.f5052a = ogVar;
        this.b = ozVar;
    }

    @Nullable
    public a a(long j, @Nullable String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            on b2 = this.f5052a.b(j, str);
            if (b2 != null) {
                return this.b.a(b2);
            }
            return null;
        } catch (Exception unused) {
            return null;
        }
    }
}
