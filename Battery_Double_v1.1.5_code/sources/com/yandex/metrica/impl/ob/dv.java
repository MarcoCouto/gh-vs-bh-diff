package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.metrica.impl.ob.dt;

public class dv<T extends dt> {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final du<T> f4797a;
    @Nullable
    private final ds<T> b;

    public static final class a<T extends dt> {
        @NonNull

        /* renamed from: a reason: collision with root package name */
        final du<T> f4798a;
        @Nullable
        ds<T> b;

        a(@NonNull du<T> duVar) {
            this.f4798a = duVar;
        }

        @NonNull
        public a<T> a(@NonNull ds<T> dsVar) {
            this.b = dsVar;
            return this;
        }

        @NonNull
        public dv<T> a() {
            return new dv<>(this);
        }
    }

    private dv(@NonNull a aVar) {
        this.f4797a = aVar.f4798a;
        this.b = aVar.b;
    }

    public void a(@NonNull dt dtVar) {
        this.f4797a.a(dtVar);
    }

    /* access modifiers changed from: 0000 */
    public final boolean b(@NonNull dt dtVar) {
        if (this.b == null) {
            return false;
        }
        return this.b.a(dtVar);
    }

    @NonNull
    public static <T extends dt> a<T> a(@NonNull du<T> duVar) {
        return new a<>(duVar);
    }
}
