package com.yandex.metrica.impl.ob;

import android.annotation.SuppressLint;
import android.content.Context;
import android.location.Location;
import android.os.Parcel;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import java.util.WeakHashMap;
import java.util.concurrent.TimeUnit;

public class oo implements p {

    /* renamed from: a reason: collision with root package name */
    public static final long f5036a = TimeUnit.MINUTES.toMillis(1);
    @SuppressLint({"StaticFieldLeak"})
    private static volatile oo b;
    private static final Object c = new Object();
    @NonNull
    private final Context d;
    @NonNull
    private xi e;
    @NonNull
    private final WeakHashMap<Object, Object> f;
    private boolean g;
    /* access modifiers changed from: private */
    @Nullable
    public oh h;
    /* access modifiers changed from: private */
    @NonNull
    public uk i;
    /* access modifiers changed from: private */
    @Nullable
    public ot j;
    @NonNull
    private a k;
    private Runnable l;
    @NonNull
    private lh m;
    @NonNull
    private lg n;
    private final pr o;
    private final px p;
    private boolean q;
    private final Object r;
    /* access modifiers changed from: private */
    public final Object s;

    static class a {
        a() {
        }

        @NonNull
        public ot a(@NonNull Context context, @NonNull xi xiVar, @NonNull uk ukVar, @Nullable oh ohVar, @NonNull lh lhVar, @NonNull lg lgVar, @NonNull pr prVar) {
            ot otVar = new ot(context, ukVar, xiVar, ohVar, lhVar, lgVar, prVar);
            return otVar;
        }
    }

    public static oo a(Context context) {
        if (b == null) {
            synchronized (c) {
                if (b == null) {
                    b = new oo(context.getApplicationContext());
                }
            }
        }
        return b;
    }

    private oo(@NonNull Context context) {
        this(context, al.a().j().e(), new a(), ld.a(context).g(), ld.a(context).h(), (uk) com.yandex.metrica.impl.ob.np.a.a(uk.class).a(context).a());
    }

    private void c() {
        this.e.a((Runnable) new Runnable() {
            public void run() {
                try {
                    if (oo.this.j != null) {
                        oo.this.j.a();
                    }
                } catch (Throwable unused) {
                }
            }
        });
    }

    public void a(@Nullable Object obj) {
        synchronized (this.r) {
            this.f.put(obj, null);
            d();
        }
    }

    public void b(@Nullable Object obj) {
        synchronized (this.r) {
            this.f.remove(obj);
            d();
        }
    }

    private void d() {
        if (this.q) {
            if (!this.g || this.f.isEmpty()) {
                e();
                this.q = false;
            }
        } else if (this.g && !this.f.isEmpty()) {
            f();
            this.q = true;
        }
    }

    private void e() {
        if (this.j != null) {
            this.j.f();
        }
        h();
    }

    private void f() {
        if (this.j == null) {
            synchronized (this.s) {
                this.j = this.k.a(this.d, this.e, this.i, this.h, this.m, this.n, this.o);
            }
        }
        this.j.e();
        g();
        c();
    }

    @Nullable
    public Location a() {
        ot otVar = this.j;
        if (otVar == null) {
            return null;
        }
        return otVar.b();
    }

    @Nullable
    public Location b() {
        ot otVar = this.j;
        if (otVar == null) {
            return null;
        }
        return otVar.c();
    }

    private void g() {
        if (this.l == null) {
            this.l = new Runnable() {
                public void run() {
                    ot a2 = oo.this.j;
                    if (a2 != null) {
                        a2.d();
                    }
                    oo.this.i();
                }
            };
            i();
        }
    }

    private void h() {
        if (this.l != null) {
            this.e.b(this.l);
        }
    }

    /* access modifiers changed from: private */
    public void i() {
        this.e.a(this.l, f5036a);
    }

    public static byte[] a(@Nullable Location location) {
        if (location != null) {
            Parcel obtain = Parcel.obtain();
            try {
                obtain.writeValue(location);
                return obtain.marshall();
            } catch (Throwable unused) {
            } finally {
                obtain.recycle();
            }
        }
        return null;
    }

    public static Location a(@NonNull byte[] bArr) {
        if (bArr != null) {
            Parcel obtain = Parcel.obtain();
            try {
                obtain.unmarshall(bArr, 0, bArr.length);
                obtain.setDataPosition(0);
                return (Location) obtain.readValue(Location.class.getClassLoader());
            } catch (Throwable unused) {
            } finally {
                obtain.recycle();
            }
        }
        return null;
    }

    public void a(boolean z) {
        synchronized (this.r) {
            if (this.g != z) {
                this.g = z;
                this.p.a(z);
                this.o.a(this.p.a());
                d();
            }
        }
    }

    public void a(@NonNull uk ukVar, @Nullable oh ohVar) {
        synchronized (this.r) {
            this.i = ukVar;
            this.h = ohVar;
            this.p.a(ukVar);
            this.o.a(this.p.a());
        }
        this.e.a((Runnable) new Runnable() {
            public void run() {
                synchronized (oo.this.s) {
                    if (oo.this.j != null) {
                        oo.this.j.a(oo.this.i, oo.this.h);
                    }
                }
            }
        });
    }

    @VisibleForTesting
    oo(@NonNull Context context, @NonNull xi xiVar, @NonNull a aVar, @NonNull lh lhVar, @NonNull lg lgVar, @NonNull uk ukVar) {
        this.g = false;
        this.p = new px();
        this.q = false;
        this.r = new Object();
        this.s = new Object();
        this.d = context;
        this.e = xiVar;
        this.f = new WeakHashMap<>();
        this.k = aVar;
        this.m = lhVar;
        this.n = lgVar;
        this.i = ukVar;
        this.o = new pr(this.p.a());
    }
}
