package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.util.Pair;

public class i {

    /* renamed from: a reason: collision with root package name */
    private vx f4881a;
    private long b;
    private boolean c;
    @NonNull
    private final xx d;

    public static final class a {

        /* renamed from: a reason: collision with root package name */
        public final String f4882a;
        public final long b;

        public a(String str, long j) {
            this.f4882a = str;
            this.b = j;
        }

        public boolean equals(Object obj) {
            boolean z = true;
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            a aVar = (a) obj;
            if (this.b != aVar.b) {
                return false;
            }
            if (this.f4882a == null ? aVar.f4882a != null : !this.f4882a.equals(aVar.f4882a)) {
                z = false;
            }
            return z;
        }

        public int hashCode() {
            return ((this.f4882a != null ? this.f4882a.hashCode() : 0) * 31) + ((int) (this.b ^ (this.b >>> 32)));
        }
    }

    public i(String str, long j, @NonNull vz vzVar) {
        this(str, j, new xx(vzVar, "App Environment"));
    }

    @VisibleForTesting
    i(String str, long j, @NonNull xx xxVar) {
        this.b = j;
        try {
            this.f4881a = new vx(str);
        } catch (Throwable unused) {
            this.f4881a = new vx();
            this.b = 0;
        }
        this.d = xxVar;
    }

    public synchronized void a() {
        this.f4881a = new vx();
        this.b = 0;
    }

    public synchronized void a(@NonNull Pair<String, String> pair) {
        if (this.d.a(this.f4881a, (String) pair.first, (String) pair.second)) {
            this.c = true;
        }
    }

    public synchronized a b() {
        if (this.c) {
            this.b++;
            this.c = false;
        }
        return new a(this.f4881a.toString(), this.b);
    }

    public synchronized String toString() {
        StringBuilder sb;
        sb = new StringBuilder("Map size ");
        sb.append(this.f4881a.size());
        sb.append(". Is changed ");
        sb.append(this.c);
        sb.append(". Current revision ");
        sb.append(this.b);
        return sb.toString();
    }
}
