package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import com.yandex.metrica.impl.ob.af.a;
import java.util.List;

public abstract class hw<BaseHandler> {

    /* renamed from: a reason: collision with root package name */
    private final ig f4878a;

    public abstract void a(a aVar, @NonNull List<BaseHandler> list);

    public hw(ig igVar) {
        this.f4878a = igVar;
    }

    public ig a() {
        return this.f4878a;
    }
}
