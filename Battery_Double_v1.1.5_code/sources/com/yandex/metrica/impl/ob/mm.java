package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import com.yandex.metrica.impl.ob.rm.a;
import com.yandex.metrica.impl.ob.rm.a.b;
import java.util.ArrayList;

public class mm implements mq<pn, a> {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final mo f4994a;

    public mm() {
        this(new mo());
    }

    @VisibleForTesting
    mm(@NonNull mo moVar) {
        this.f4994a = moVar;
    }

    @NonNull
    /* renamed from: a */
    public a b(@NonNull pn pnVar) {
        a aVar = new a();
        aVar.b = new b[pnVar.f5063a.size()];
        int i = 0;
        int i2 = 0;
        for (pu a2 : pnVar.f5063a) {
            aVar.b[i2] = a(a2);
            i2++;
        }
        if (pnVar.b != null) {
            aVar.c = this.f4994a.b(pnVar.b);
        }
        aVar.d = new String[pnVar.c.size()];
        for (String str : pnVar.c) {
            aVar.d[i] = str;
            i++;
        }
        return aVar;
    }

    @NonNull
    public pn a(@NonNull a aVar) {
        ArrayList arrayList = new ArrayList();
        for (b a2 : aVar.b) {
            arrayList.add(a(a2));
        }
        l lVar = null;
        if (aVar.c != null) {
            lVar = this.f4994a.a(aVar.c);
        }
        ArrayList arrayList2 = new ArrayList();
        for (String add : aVar.d) {
            arrayList2.add(add);
        }
        return new pn(arrayList, lVar, arrayList2);
    }

    private b a(@NonNull pu puVar) {
        b bVar = new b();
        bVar.b = puVar.f5068a;
        bVar.c = puVar.b;
        return bVar;
    }

    private pu a(@NonNull b bVar) {
        return new pu(bVar.b, bVar.c);
    }
}
