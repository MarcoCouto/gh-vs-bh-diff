package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import java.io.Closeable;

public class lc extends SQLiteOpenHelper implements Closeable {

    /* renamed from: a reason: collision with root package name */
    protected final lj f4962a;
    @NonNull
    private final String b;
    private final vz c;
    @NonNull
    private final Context d;

    public lc(Context context, @NonNull String str, lj ljVar) {
        this(context, str, ljVar, vr.a());
    }

    @VisibleForTesting
    lc(Context context, @NonNull String str, lj ljVar, @NonNull vz vzVar) {
        super(context, str, null, lq.b);
        this.d = context;
        this.f4962a = ljVar;
        this.b = str;
        this.c = vzVar;
    }

    public void onCreate(SQLiteDatabase sQLiteDatabase) {
        this.f4962a.b(sQLiteDatabase);
    }

    public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        this.f4962a.a(sQLiteDatabase, i, i2);
    }

    public void onOpen(SQLiteDatabase sQLiteDatabase) {
        super.onOpen(sQLiteDatabase);
        this.f4962a.a(sQLiteDatabase);
    }

    @Nullable
    public SQLiteDatabase getReadableDatabase() {
        try {
            return super.getReadableDatabase();
        } catch (Throwable th) {
            this.c.a(th, "", new Object[0]);
            this.c.c("Could not get readable database %s due to an exception. AppMetrica SDK may behave unexpectedly.", this.b);
            tl.a(this.d).reportError("db_read_error", th);
            return null;
        }
    }

    @Nullable
    public SQLiteDatabase getWritableDatabase() {
        try {
            return super.getWritableDatabase();
        } catch (Throwable th) {
            this.c.a(th, "", new Object[0]);
            this.c.c("Could not get writable database %s due to an exception. AppMetrica SDK may behave unexpectedly.", this.b);
            tl.a(this.d).reportError("db_write_error", th);
            return null;
        }
    }
}
