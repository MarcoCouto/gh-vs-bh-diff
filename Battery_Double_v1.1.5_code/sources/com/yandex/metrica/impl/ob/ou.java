package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import java.util.HashMap;
import java.util.Map;

class ou {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private c f5046a;
    @NonNull
    private a b;
    @NonNull
    private b c;
    @NonNull
    private Context d;
    @Nullable
    private oh e;
    @Nullable
    private ov f;
    @NonNull
    private ow g;
    @NonNull
    private od h;
    @Nullable
    private oi i;
    @NonNull
    private Map<String, op> j;

    public static class a {
        @NonNull
        public oi a(oe oeVar) {
            return new oi(oeVar);
        }
    }

    public static class b {
        @NonNull
        public op a(@Nullable String str, @Nullable oh ohVar, @NonNull ov ovVar, @NonNull ow owVar, @NonNull od odVar) {
            op opVar = new op(str, ohVar, ovVar, owVar, odVar);
            return opVar;
        }
    }

    public static class c {
        @NonNull
        public ov a(@NonNull Context context, @Nullable oe oeVar) {
            return new ov(context, oeVar);
        }
    }

    public ou(@NonNull Context context, @Nullable oh ohVar, @NonNull ow owVar, @NonNull od odVar) {
        this(context, ohVar, new c(), new a(), new b(), owVar, odVar);
    }

    public void a(@NonNull Location location) {
        String provider = location.getProvider();
        op opVar = (op) this.j.get(provider);
        if (opVar == null) {
            opVar = a(provider);
            this.j.put(provider, opVar);
        } else {
            opVar.a(this.e);
        }
        opVar.a(location);
    }

    @Nullable
    public Location a() {
        if (this.i == null) {
            return null;
        }
        return this.i.a();
    }

    @NonNull
    private op a(String str) {
        if (this.f == null) {
            this.f = this.f5046a.a(this.d, null);
        }
        if (this.i == null) {
            this.i = this.b.a(this.f);
        }
        return this.c.a(str, this.e, this.f, this.g, this.h);
    }

    public void a(@Nullable oh ohVar) {
        this.e = ohVar;
    }

    @VisibleForTesting
    ou(@NonNull Context context, @Nullable oh ohVar, @NonNull c cVar, @NonNull a aVar, @NonNull b bVar, @NonNull ow owVar, @NonNull od odVar) {
        this.j = new HashMap();
        this.d = context;
        this.e = ohVar;
        this.f5046a = cVar;
        this.b = aVar;
        this.c = bVar;
        this.g = owVar;
        this.h = odVar;
    }
}
