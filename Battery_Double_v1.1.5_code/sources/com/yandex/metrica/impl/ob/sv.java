package com.yandex.metrica.impl.ob;

import android.net.Uri.Builder;
import android.support.annotation.NonNull;
import com.tapjoy.TapjoyConstants;

public class sv implements sw<gi> {
    public void a(@NonNull Builder builder, @NonNull gi giVar) {
        builder.appendPath("diagnostic");
        builder.appendQueryParameter("deviceid", giVar.r());
        builder.appendQueryParameter("uuid", giVar.t());
        builder.appendQueryParameter("app_platform", giVar.l());
        builder.appendQueryParameter("analytics_sdk_version_name", giVar.i());
        builder.appendQueryParameter("analytics_sdk_build_number", giVar.j());
        builder.appendQueryParameter("analytics_sdk_build_type", giVar.k());
        builder.appendQueryParameter("app_version_name", giVar.q());
        builder.appendQueryParameter("app_build_number", giVar.p());
        builder.appendQueryParameter("model", giVar.m());
        builder.appendQueryParameter("manufacturer", giVar.g());
        builder.appendQueryParameter(TapjoyConstants.TJC_DEVICE_OS_VERSION_NAME, giVar.n());
        builder.appendQueryParameter("os_api_level", String.valueOf(giVar.o()));
        builder.appendQueryParameter("screen_width", String.valueOf(giVar.w()));
        builder.appendQueryParameter("screen_height", String.valueOf(giVar.x()));
        builder.appendQueryParameter("screen_dpi", String.valueOf(giVar.y()));
        builder.appendQueryParameter("scalefactor", String.valueOf(giVar.z()));
        builder.appendQueryParameter("locale", giVar.A());
        builder.appendQueryParameter(TapjoyConstants.TJC_DEVICE_TYPE_NAME, giVar.C());
        builder.appendQueryParameter("app_id", giVar.d());
        builder.appendQueryParameter("api_key_128", giVar.b());
        builder.appendQueryParameter("app_debuggable", giVar.E());
        builder.appendQueryParameter("is_rooted", giVar.u());
        builder.appendQueryParameter("app_framework", giVar.v());
    }
}
