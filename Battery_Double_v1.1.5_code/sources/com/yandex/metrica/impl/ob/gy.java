package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import com.yandex.metrica.impl.ob.eg.a;

public class gy extends gt {

    /* renamed from: a reason: collision with root package name */
    private final cs f4864a;
    @NonNull
    private final ox b;

    public gy(@NonNull ei eiVar, @NonNull cs csVar, @NonNull ox oxVar) {
        super(eiVar);
        this.f4864a = csVar;
        this.b = oxVar;
    }

    public boolean a(@NonNull w wVar, @NonNull fs fsVar) {
        a a2 = fsVar.b().a();
        this.f4864a.a(a2.m);
        a(wk.a(a2.e, true));
        return false;
    }

    private void a(boolean z) {
        this.b.a(z);
    }
}
