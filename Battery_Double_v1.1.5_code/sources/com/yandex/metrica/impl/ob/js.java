package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;

public class js {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Context f4921a;
    @NonNull
    private final yc b;

    public js(@NonNull Context context) {
        this(context, new yc());
    }

    public int a() {
        try {
            return Math.max(1, this.b.b(this.f4921a, new Intent().setAction("com.yandex.metrica.configuration.ACTION_INIT"), 128).size());
        } catch (Throwable unused) {
            return 1;
        }
    }

    @VisibleForTesting
    js(@NonNull Context context, @NonNull yc ycVar) {
        this.f4921a = context;
        this.b = ycVar;
    }
}
