package com.yandex.metrica.impl.ob;

import android.location.Location;
import android.support.annotation.NonNull;
import com.yandex.metrica.impl.ob.oh.a;
import com.yandex.metrica.impl.ob.rh.b.C0111b;
import java.util.concurrent.TimeUnit;

public class pb {
    @NonNull
    public C0111b a(@NonNull os osVar) {
        C0111b bVar = new C0111b();
        Location c = osVar.c();
        bVar.b = osVar.a() == null ? bVar.b : osVar.a().longValue();
        bVar.d = TimeUnit.MILLISECONDS.toSeconds(c.getTime());
        bVar.l = a(osVar.f5044a);
        bVar.c = TimeUnit.MILLISECONDS.toSeconds(osVar.b());
        bVar.m = TimeUnit.MILLISECONDS.toSeconds(osVar.d());
        bVar.e = c.getLatitude();
        bVar.f = c.getLongitude();
        bVar.g = Math.round(c.getAccuracy());
        bVar.h = Math.round(c.getBearing());
        bVar.i = Math.round(c.getSpeed());
        bVar.j = (int) Math.round(c.getAltitude());
        bVar.k = a(c.getProvider());
        return bVar;
    }

    private static int a(@NonNull a aVar) {
        switch (aVar) {
            case FOREGROUND:
                return 0;
            case BACKGROUND:
                return 1;
            default:
                return 0;
        }
    }

    private static int a(@NonNull String str) {
        if ("gps".equals(str)) {
            return 1;
        }
        return "network".equals(str) ? 2 : 0;
    }
}
