package com.yandex.metrica.impl.ob;

import android.location.Location;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.metrica.j;
import com.yandex.metrica.j.a;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

public class aa implements ba {

    /* renamed from: a reason: collision with root package name */
    private Location f4629a;
    private Boolean b;
    private Boolean c;
    private Map<String, String> d = new LinkedHashMap();
    private Map<String, String> e = new LinkedHashMap();
    private boolean f;
    private boolean g;
    private cd h;

    private static boolean a(Object obj) {
        return obj == null;
    }

    public Location a() {
        return this.f4629a;
    }

    public Boolean b() {
        return this.b;
    }

    public void a(boolean z) {
        this.b = Boolean.valueOf(z);
        f();
    }

    public Boolean c() {
        return this.c;
    }

    public void setStatisticsSending(boolean z) {
        this.c = Boolean.valueOf(z);
        f();
    }

    public void a(Location location) {
        this.f4629a = location;
    }

    public boolean d() {
        return this.f;
    }

    private void e() {
        this.f4629a = null;
        this.b = null;
        this.c = null;
        this.d.clear();
        this.e.clear();
        this.f = false;
    }

    public j a(j jVar) {
        if (this.g) {
            return jVar;
        }
        a b2 = b(jVar);
        a(jVar, b2);
        this.g = true;
        e();
        return b2.b();
    }

    private a b(j jVar) {
        a a2 = j.a(jVar.apiKey);
        a2.a(jVar.b, jVar.j);
        a2.c(jVar.f5324a);
        a2.a(jVar.preloadInfo);
        a2.a(jVar.location);
        a2.a(jVar.m);
        a(a2, jVar);
        a(this.d, a2);
        a(jVar.i, a2);
        b(this.e, a2);
        b(jVar.h, a2);
        return a2;
    }

    private void a(@NonNull a aVar, @NonNull j jVar) {
        if (cx.a((Object) jVar.d)) {
            aVar.a(jVar.d);
        }
        if (cx.a((Object) jVar.appVersion)) {
            aVar.a(jVar.appVersion);
        }
        if (cx.a((Object) jVar.f)) {
            aVar.d(jVar.f.intValue());
        }
        if (cx.a((Object) jVar.e)) {
            aVar.b(jVar.e.intValue());
        }
        if (cx.a((Object) jVar.g)) {
            aVar.c(jVar.g.intValue());
        }
        if (cx.a((Object) jVar.logs) && jVar.logs.booleanValue()) {
            aVar.a();
        }
        if (cx.a((Object) jVar.sessionTimeout)) {
            aVar.a(jVar.sessionTimeout.intValue());
        }
        if (cx.a((Object) jVar.crashReporting)) {
            aVar.a(jVar.crashReporting.booleanValue());
        }
        if (cx.a((Object) jVar.nativeCrashReporting)) {
            aVar.b(jVar.nativeCrashReporting.booleanValue());
        }
        if (cx.a((Object) jVar.locationTracking)) {
            aVar.d(jVar.locationTracking.booleanValue());
        }
        if (cx.a((Object) jVar.installedAppCollecting)) {
            aVar.e(jVar.installedAppCollecting.booleanValue());
        }
        if (cx.a((Object) jVar.c)) {
            aVar.b(jVar.c);
        }
        if (cx.a((Object) jVar.firstActivationAsUpdate)) {
            aVar.g(jVar.firstActivationAsUpdate.booleanValue());
        }
        if (cx.a((Object) jVar.statisticsSending)) {
            aVar.f(jVar.statisticsSending.booleanValue());
        }
        if (cx.a((Object) jVar.l)) {
            aVar.c(jVar.l.booleanValue());
        }
        if (cx.a((Object) jVar.maxReportsInDatabaseCount)) {
            aVar.e(jVar.maxReportsInDatabaseCount.intValue());
        }
        if (cx.a((Object) jVar.n)) {
            aVar.a(jVar.n);
        }
    }

    private void a(@Nullable Map<String, String> map, @NonNull a aVar) {
        if (!cx.a((Map) map)) {
            for (Entry entry : map.entrySet()) {
                aVar.b((String) entry.getKey(), (String) entry.getValue());
            }
        }
    }

    private void b(@Nullable Map<String, String> map, @NonNull a aVar) {
        if (!cx.a((Map) map)) {
            for (Entry entry : map.entrySet()) {
                aVar.a((String) entry.getKey(), (String) entry.getValue());
            }
        }
    }

    private void a(j jVar, a aVar) {
        Boolean b2 = b();
        if (a((Object) jVar.locationTracking) && cx.a((Object) b2)) {
            aVar.d(b2.booleanValue());
        }
        Location a2 = a();
        if (a((Object) jVar.location) && cx.a((Object) a2)) {
            aVar.a(a2);
        }
        Boolean c2 = c();
        if (a((Object) jVar.statisticsSending) && cx.a((Object) c2)) {
            aVar.f(c2.booleanValue());
        }
    }

    public void a(cd cdVar) {
        this.h = cdVar;
    }

    private void f() {
        if (this.h != null) {
            this.h.a(this.b, this.c);
        }
    }
}
