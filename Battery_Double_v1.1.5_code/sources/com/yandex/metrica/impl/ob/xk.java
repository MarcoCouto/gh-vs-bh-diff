package com.yandex.metrica.impl.ob;

import android.os.HandlerThread;
import android.support.annotation.NonNull;

public class xk extends HandlerThread implements xj {

    /* renamed from: a reason: collision with root package name */
    private volatile boolean f5302a = true;

    public xk(@NonNull String str) {
        super(str);
    }

    public synchronized boolean c() {
        return this.f5302a;
    }
}
