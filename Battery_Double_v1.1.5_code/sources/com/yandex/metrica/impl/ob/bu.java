package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.yandex.metrica.PreloadInfo;
import org.json.JSONObject;

public class bu {

    /* renamed from: a reason: collision with root package name */
    private PreloadInfo f4700a;

    public bu(PreloadInfo preloadInfo, @NonNull vz vzVar) {
        if (preloadInfo == null) {
            return;
        }
        if (!TextUtils.isEmpty(preloadInfo.getTrackingId())) {
            this.f4700a = preloadInfo;
        } else if (vzVar.c()) {
            vzVar.c("Required field \"PreloadInfo.trackingId\" is empty!\nThis preload info will be skipped.");
        }
    }

    /* access modifiers changed from: 0000 */
    public String a() {
        if (this.f4700a == null) {
            return "";
        }
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("preloadInfo", b());
        } catch (Throwable unused) {
        }
        return jSONObject.toString();
    }

    @Nullable
    public JSONObject b() {
        if (this.f4700a == null) {
            return null;
        }
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("trackingId", this.f4700a.getTrackingId());
            if (!this.f4700a.getAdditionalParams().isEmpty()) {
                jSONObject.put("additionalParams", new JSONObject(this.f4700a.getAdditionalParams()));
            }
        } catch (Throwable unused) {
        }
        return jSONObject;
    }
}
