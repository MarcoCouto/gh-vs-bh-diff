package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;

public class fr {
    @NonNull
    public fq a(@NonNull fn fnVar) {
        switch (fnVar.e()) {
            case COMMUTATION:
                return new fx();
            case MAIN:
                return new fv();
            case SELF_DIAGNOSTIC_MAIN:
                return new gc();
            case SELF_DIAGNOSTIC_MANUAL:
                return new gd();
            case MANUAL:
                return new fz();
            case APPMETRICA:
                return new fm();
            default:
                return new fv();
        }
    }
}
