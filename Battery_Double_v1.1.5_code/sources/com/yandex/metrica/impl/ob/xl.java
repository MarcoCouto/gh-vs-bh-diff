package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;

public class xl extends Thread implements xj {

    /* renamed from: a reason: collision with root package name */
    private volatile boolean f5303a = true;

    public xl() {
    }

    public xl(@NonNull Runnable runnable, @NonNull String str) {
        super(runnable, str);
    }

    public xl(@NonNull String str) {
        super(str);
    }

    public synchronized boolean c() {
        return this.f5303a;
    }

    public synchronized void b() {
        this.f5303a = false;
        interrupt();
    }
}
