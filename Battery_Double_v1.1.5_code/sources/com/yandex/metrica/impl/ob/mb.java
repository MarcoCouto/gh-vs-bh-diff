package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import com.yandex.metrica.impl.ob.e;

public abstract class mb<T extends e> implements me<T> {
    @NonNull
    /* renamed from: b */
    public abstract T c();

    @NonNull
    public byte[] a(@NonNull T t) {
        return e.a((e) t);
    }
}
