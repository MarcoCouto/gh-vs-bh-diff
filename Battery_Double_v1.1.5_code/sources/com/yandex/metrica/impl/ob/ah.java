package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import java.io.File;

public class ah {
    @NonNull
    public File a(@NonNull Context context) {
        if (am.a()) {
            return context.getNoBackupFilesDir();
        }
        return context.getFilesDir();
    }

    @NonNull
    public File b(@NonNull Context context) {
        return new File(a(context), "appmetrica_crashes");
    }

    @NonNull
    public File c(@NonNull Context context) {
        return new File(a(context), "YandexMetricaNativeCrashes");
    }

    @NonNull
    public File a(@NonNull String str) {
        return new File(str);
    }

    @NonNull
    public File a(@NonNull File file, @NonNull String str) {
        return new File(file, str);
    }
}
