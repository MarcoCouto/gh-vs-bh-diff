package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.util.Pair;
import com.yandex.metrica.impl.ob.cq.a;
import com.yandex.metrica.impl.ob.cq.a.C0108a;
import com.yandex.metrica.impl.ob.rr.a.h;
import com.yandex.metrica.impl.ob.rr.a.h.C0127a;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class nb implements mv<a, h> {

    /* renamed from: a reason: collision with root package name */
    private static final Map<Integer, C0108a> f5000a = Collections.unmodifiableMap(new HashMap<Integer, C0108a>() {
        {
            put(Integer.valueOf(1), C0108a.WIFI);
            put(Integer.valueOf(2), C0108a.CELL);
        }
    });
    private static final Map<C0108a, Integer> b = Collections.unmodifiableMap(new HashMap<C0108a, Integer>() {
        {
            put(C0108a.WIFI, Integer.valueOf(1));
            put(C0108a.CELL, Integer.valueOf(2));
        }
    });

    @NonNull
    /* renamed from: a */
    public h b(@NonNull a aVar) {
        h hVar = new h();
        hVar.b = aVar.f4747a;
        hVar.c = aVar.b;
        hVar.d = aVar.c;
        hVar.e = a(aVar.d);
        hVar.f = aVar.e == null ? 0 : aVar.e.longValue();
        hVar.g = b(aVar.f);
        return hVar;
    }

    @NonNull
    public a a(@NonNull h hVar) {
        a aVar = new a(hVar.b, hVar.c, hVar.d, a(hVar.e), Long.valueOf(hVar.f), a(hVar.g));
        return aVar;
    }

    @NonNull
    private C0127a[] a(@NonNull List<Pair<String, String>> list) {
        C0127a[] aVarArr = new C0127a[list.size()];
        int i = 0;
        for (Pair pair : list) {
            C0127a aVar = new C0127a();
            aVar.b = (String) pair.first;
            aVar.c = (String) pair.second;
            aVarArr[i] = aVar;
            i++;
        }
        return aVarArr;
    }

    @NonNull
    private List<Pair<String, String>> a(@NonNull C0127a[] aVarArr) {
        ArrayList arrayList = new ArrayList(aVarArr.length);
        for (C0127a aVar : aVarArr) {
            arrayList.add(new Pair(aVar.b, aVar.c));
        }
        return arrayList;
    }

    @NonNull
    private List<C0108a> a(@NonNull int[] iArr) {
        ArrayList arrayList = new ArrayList(iArr.length);
        for (int valueOf : iArr) {
            arrayList.add(f5000a.get(Integer.valueOf(valueOf)));
        }
        return arrayList;
    }

    @NonNull
    private int[] b(@NonNull List<C0108a> list) {
        int[] iArr = new int[list.size()];
        for (int i = 0; i < list.size(); i++) {
            iArr[i] = ((Integer) b.get(list.get(i))).intValue();
        }
        return iArr;
    }
}
