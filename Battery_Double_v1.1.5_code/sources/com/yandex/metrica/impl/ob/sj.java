package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public final class sj {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    public final String f5159a;
    public final long b;
    public final long c;

    @Nullable
    public static sj a(@NonNull byte[] bArr) throws d {
        if (cx.a(bArr)) {
            return null;
        }
        return new sj(bArr);
    }

    private sj(@NonNull byte[] bArr) throws d {
        rj a2 = rj.a(bArr);
        this.f5159a = a2.b;
        this.b = a2.d;
        this.c = a2.c;
    }

    public sj(@NonNull String str, long j, long j2) {
        this.f5159a = str;
        this.b = j;
        this.c = j2;
    }

    public byte[] a() {
        rj rjVar = new rj();
        rjVar.b = this.f5159a;
        rjVar.d = this.b;
        rjVar.c = this.c;
        return e.a((e) rjVar);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        sj sjVar = (sj) obj;
        if (this.b == sjVar.b && this.c == sjVar.c) {
            return this.f5159a.equals(sjVar.f5159a);
        }
        return false;
    }

    public int hashCode() {
        return (((this.f5159a.hashCode() * 31) + ((int) (this.b ^ (this.b >>> 32)))) * 31) + ((int) (this.c ^ (this.c >>> 32)));
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("ReferrerInfo{installReferrer='");
        sb.append(this.f5159a);
        sb.append('\'');
        sb.append(", referrerClickTimestampSeconds=");
        sb.append(this.b);
        sb.append(", installBeginTimestampSeconds=");
        sb.append(this.c);
        sb.append('}');
        return sb.toString();
    }
}
