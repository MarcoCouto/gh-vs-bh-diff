package com.yandex.metrica.impl.ob;

import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.util.SparseArray;

public class lj {

    /* renamed from: a reason: collision with root package name */
    private final String f4968a;
    private final lr b;
    private final lr c;
    private final SparseArray<lr> d;
    private final lk e;

    public static class a {
        public lj a(@NonNull String str, @NonNull lr lrVar, @NonNull lr lrVar2, @NonNull SparseArray<lr> sparseArray, @NonNull lk lkVar) {
            lj ljVar = new lj(str, lrVar, lrVar2, sparseArray, lkVar);
            return ljVar;
        }
    }

    private lj(String str, lr lrVar, lr lrVar2, SparseArray<lr> sparseArray, lk lkVar) {
        this.f4968a = str;
        this.b = lrVar;
        this.c = lrVar2;
        this.d = sparseArray;
        this.e = lkVar;
    }

    public void a(SQLiteDatabase sQLiteDatabase) {
        try {
            if (this.e != null && !this.e.a(sQLiteDatabase)) {
                c(sQLiteDatabase);
            }
        } catch (Throwable unused) {
        }
    }

    public void b(SQLiteDatabase sQLiteDatabase) {
        a(this.b, sQLiteDatabase);
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public void a(lr lrVar, SQLiteDatabase sQLiteDatabase) {
        try {
            lrVar.a(sQLiteDatabase);
        } catch (Throwable unused) {
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x0022  */
    /* JADX WARNING: Removed duplicated region for block: B:18:? A[RETURN, SYNTHETIC] */
    public void a(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        boolean z;
        if (i2 > i) {
            z = false;
            int i3 = i + 1;
            while (i3 <= i2) {
                try {
                    lr lrVar = (lr) this.d.get(i3);
                    if (lrVar != null) {
                        lrVar.a(sQLiteDatabase);
                    }
                    i3++;
                } catch (Throwable unused) {
                }
            }
            if (!(this.e.a(sQLiteDatabase) ^ true) && !z) {
                c(sQLiteDatabase);
                return;
            }
            return;
        }
        z = true;
        if (!(!this.e.a(sQLiteDatabase)) && !z) {
        }
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public void c(SQLiteDatabase sQLiteDatabase) {
        b(this.c, sQLiteDatabase);
        a(this.b, sQLiteDatabase);
    }

    private void b(lr lrVar, SQLiteDatabase sQLiteDatabase) {
        try {
            lrVar.a(sQLiteDatabase);
        } catch (Throwable unused) {
        }
    }
}
