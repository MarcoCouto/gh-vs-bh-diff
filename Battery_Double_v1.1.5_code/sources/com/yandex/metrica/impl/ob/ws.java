package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import android.util.Base64;

public class ws implements wx {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final wr f5292a;

    ws(@NonNull Context context) {
        this(new wq(context));
    }

    ws(@NonNull wq wqVar) {
        this(new wr("AES/CBC/PKCS5Padding", wqVar.a(), wqVar.b()));
    }

    @VisibleForTesting
    ws(@NonNull wr wrVar) {
        this.f5292a = wrVar;
    }

    @NonNull
    public ww a(@NonNull w wVar) {
        String e = wVar.e();
        String str = null;
        if (!TextUtils.isEmpty(e)) {
            try {
                byte[] a2 = this.f5292a.a(e.getBytes("UTF-8"));
                if (a2 != null) {
                    str = Base64.encodeToString(a2, 0);
                }
            } catch (Throwable unused) {
            }
        }
        return new ww(wVar.c(str), a());
    }

    @NonNull
    public byte[] a(@Nullable byte[] bArr) {
        byte[] bArr2 = new byte[0];
        if (bArr != null && bArr.length > 0) {
            try {
                return this.f5292a.b(Base64.decode(bArr, 0));
            } catch (Throwable unused) {
            }
        }
        return bArr2;
    }

    @NonNull
    public wz a() {
        return wz.AES_VALUE_ENCRYPTION;
    }
}
