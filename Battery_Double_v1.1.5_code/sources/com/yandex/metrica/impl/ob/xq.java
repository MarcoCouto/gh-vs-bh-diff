package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;

abstract class xq<T> implements yb<T> {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    protected final vz f5306a;
    private final int b;
    private final String c;

    public xq(int i, @NonNull String str, @NonNull vz vzVar) {
        this.b = i;
        this.c = str;
        this.f5306a = vzVar;
    }

    @VisibleForTesting(otherwise = 3)
    public int a() {
        return this.b;
    }

    @VisibleForTesting(otherwise = 3)
    @NonNull
    public String b() {
        return this.c;
    }
}
