package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;

public class lg extends kw {
    @NonNull
    public String e() {
        return "lbs_dat";
    }

    lg(@NonNull Context context, @NonNull lc lcVar) {
        this(lcVar, new ly(ld.a(context).c()));
    }

    @VisibleForTesting
    lg(@NonNull lc lcVar, @NonNull ly lyVar) {
        super(lcVar, lyVar);
    }

    /* access modifiers changed from: protected */
    public long c(long j) {
        return c().f(j);
    }

    /* access modifiers changed from: protected */
    @NonNull
    public ly d(long j) {
        return c().g(j);
    }
}
