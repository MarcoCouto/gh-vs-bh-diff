package com.yandex.metrica.impl.ob;

import com.yandex.metrica.impl.ob.af.a;
import java.util.ArrayList;

public class gm extends gr<gt> {

    /* renamed from: a reason: collision with root package name */
    private final hx f4855a;

    public gm(ei eiVar) {
        this.f4855a = new hx(eiVar);
    }

    public go<gt> a(int i) {
        ArrayList arrayList = new ArrayList();
        switch (a.a(i)) {
            case EVENT_TYPE_REFERRER_RECEIVED_FROM_BROADCAST:
                arrayList.add(this.f4855a.a());
                break;
            case EVENT_TYPE_STARTUP:
                arrayList.add(this.f4855a.b());
                break;
            case EVENT_TYPE_REFERRER_OBTAINED_FROM_SERVICES:
                arrayList.add(this.f4855a.c());
                break;
            case EVENT_TYPE_UPDATE_PRE_ACTIVATION_CONFIG:
                arrayList.add(this.f4855a.d());
                break;
        }
        return new gn(arrayList);
    }
}
