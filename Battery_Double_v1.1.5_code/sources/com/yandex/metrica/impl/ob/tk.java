package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import com.yandex.metrica.impl.ob.bt.a;
import com.yandex.metrica.impl.ob.rs.a.C0128a;
import com.yandex.metrica.impl.ob.rs.a.b;
import com.yandex.metrica.impl.ob.rs.a.b.C0129a;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import org.json.JSONException;
import org.json.JSONObject;

public class tk {

    /* renamed from: a reason: collision with root package name */
    private static final Map<C0129a, String> f5191a = Collections.unmodifiableMap(new HashMap<C0129a, String>() {
        {
            put(C0129a.COMPLETE, "complete");
            put(C0129a.ERROR, "error");
            put(C0129a.OFFLINE, "offline");
            put(C0129a.INCOMPATIBLE_NETWORK_TYPE, "incompatible_network_type");
        }
    });
    private static final Map<a, String> b = Collections.unmodifiableMap(new HashMap<a, String>() {
        {
            put(a.WIFI, "wifi");
            put(a.CELL, "cell");
            put(a.OFFLINE, "offline");
            put(a.UNDEFINED, "undefined");
        }
    });

    public String a(@NonNull b bVar) {
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.putOpt("id", bVar.a().f5092a);
            jSONObject.putOpt("url", bVar.a().b);
            jSONObject.putOpt("status", f5191a.get(bVar.b()));
            jSONObject.putOpt("code", bVar.d());
            if (!cx.a(bVar.e())) {
                jSONObject.putOpt(TtmlNode.TAG_BODY, a(bVar.e()));
            } else if (!cx.a(bVar.h())) {
                jSONObject.putOpt(TtmlNode.TAG_BODY, a(bVar.h()));
            }
            jSONObject.putOpt("headers", a(bVar.f()));
            jSONObject.putOpt("error", a(bVar.g()));
            jSONObject.putOpt("network_type", b.get(bVar.c()));
            return jSONObject.toString();
        } catch (Throwable th) {
            return th.toString();
        }
    }

    public String a(@NonNull C0128a aVar) {
        try {
            return new JSONObject().put("id", aVar.f5092a).toString();
        } catch (Throwable th) {
            return th.toString();
        }
    }

    @Nullable
    private JSONObject a(@Nullable Map<String, List<String>> map) throws JSONException {
        if (cx.a((Map) map)) {
            return null;
        }
        JSONObject jSONObject = new JSONObject();
        for (Entry entry : map.entrySet()) {
            String str = (String) entry.getKey();
            if (!cx.a((Collection) entry.getValue())) {
                List<String> a2 = cx.a((List) entry.getValue(), 10);
                ArrayList arrayList = new ArrayList();
                for (String str2 : a2) {
                    if (!TextUtils.isEmpty(str2)) {
                        arrayList.add(cx.a(str2, 100));
                    }
                }
                jSONObject.putOpt(str, TextUtils.join(",", arrayList));
            }
        }
        return jSONObject;
    }

    @Nullable
    private String a(@NonNull byte[] bArr) {
        return Base64.encodeToString(bArr, 0);
    }

    @Nullable
    private String a(@Nullable Throwable th) {
        if (th == null) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(th.toString());
        sb.append("\n");
        sb.append(Log.getStackTraceString(th));
        return sb.toString();
    }
}
