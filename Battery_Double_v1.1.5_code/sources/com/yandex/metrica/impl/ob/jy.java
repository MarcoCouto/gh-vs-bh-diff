package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public abstract class jy {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Context f4929a;

    public abstract void a(@Nullable Bundle bundle, @Nullable jw jwVar);

    public jy(@NonNull Context context) {
        this.f4929a = context;
    }

    @NonNull
    public Context a() {
        return this.f4929a;
    }
}
