package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.text.TextUtils;
import com.yandex.metrica.i;
import com.yandex.metrica.impl.ob.wj.a;

public class hr extends hd {
    public hr(en enVar) {
        super(enVar);
    }

    public boolean a(@NonNull w wVar) {
        b(wVar);
        return true;
    }

    /* access modifiers changed from: 0000 */
    public void b(w wVar) {
        String l = wVar.l();
        i a2 = wj.a(l);
        String h = a().h();
        i a3 = wj.a(h);
        if (!a2.equals(a3)) {
            if (a(a2, a3)) {
                wVar.a(h);
                a(wVar, a.LOGOUT);
            } else if (c(a2, a3)) {
                a(wVar, a.LOGIN);
            } else if (b(a2, a3)) {
                a(wVar, a.SWITCH);
            } else {
                a(wVar, a.UPDATE);
            }
            a().a(l);
        }
    }

    private void a(w wVar, a aVar) {
        wVar.c(wj.a(aVar));
        a().e().e(wVar);
    }

    private boolean a(i iVar, i iVar2) {
        return TextUtils.isEmpty(iVar.a()) && !TextUtils.isEmpty(iVar2.a());
    }

    private boolean b(i iVar, i iVar2) {
        return !TextUtils.isEmpty(iVar.a()) && !iVar.a().equals(iVar2.a());
    }

    private boolean c(i iVar, i iVar2) {
        return !TextUtils.isEmpty(iVar.a()) && TextUtils.isEmpty(iVar2.a());
    }
}
