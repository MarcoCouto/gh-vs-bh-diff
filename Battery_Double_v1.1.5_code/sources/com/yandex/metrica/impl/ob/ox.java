package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;

public class ox {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final oo f5050a;
    @NonNull
    private final vd b;
    @NonNull
    private final cy c;
    private final ly d;

    public ox(@NonNull oo ooVar, @NonNull vd vdVar, @NonNull cy cyVar, @NonNull ly lyVar) {
        this.f5050a = ooVar;
        this.b = vdVar;
        this.c = cyVar;
        this.d = lyVar;
        a();
    }

    private void a() {
        boolean g = this.d.g();
        this.f5050a.a(g);
        this.c.a(g);
        this.b.a(g);
    }

    public void a(@NonNull Object obj) {
        this.f5050a.b(obj);
        this.b.b();
    }

    public void b(@NonNull Object obj) {
        this.f5050a.a(obj);
        this.b.a();
    }

    public void a(boolean z) {
        this.f5050a.a(z);
        this.b.a(z);
        this.c.a(z);
        this.d.b(z);
    }

    public void a(@NonNull uk ukVar, boolean z) {
        this.f5050a.a(ukVar, z ? ukVar.q : ukVar.p);
        this.c.a(ukVar);
        this.b.a(ukVar);
    }
}
