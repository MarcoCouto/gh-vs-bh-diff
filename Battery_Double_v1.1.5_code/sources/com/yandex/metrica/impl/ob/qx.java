package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import com.yandex.metrica.impl.ob.rl.a.C0115a;

public class qx extends qr<Double> {
    public qx(@NonNull String str, double d, @NonNull yk<String> ykVar, @NonNull qo qoVar) {
        super(1, str, Double.valueOf(d), ykVar, qoVar);
    }

    /* access modifiers changed from: protected */
    public void a(@NonNull C0115a aVar) {
        aVar.e.c = ((Double) b()).doubleValue();
    }
}
