package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.v7.widget.helper.ItemTouchHelper.Callback;
import android.util.Pair;
import com.yandex.metrica.Revenue;
import com.yandex.metrica.impl.ob.rk.a;

class cg {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Revenue f4724a;
    private final yb<String> b = new xy(30720, "revenue payload", this.e);
    private final yb<String> c = new ya(new xy(184320, "receipt data", this.e), "<truncated data was not sent, see METRIKALIB-4568>");
    private final yb<String> d = new ya(new xz(1000, "receipt signature", this.e), "<truncated data was not sent, see METRIKALIB-4568>");
    @NonNull
    private final vz e;

    cg(@NonNull Revenue revenue, @NonNull vz vzVar) {
        this.e = vzVar;
        this.f4724a = revenue;
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public Pair<byte[], Integer> a() {
        rk rkVar = new rk();
        rkVar.d = this.f4724a.currency.getCurrencyCode().getBytes();
        if (cx.a((Object) this.f4724a.price)) {
            rkVar.c = this.f4724a.price.doubleValue();
        }
        if (cx.a((Object) this.f4724a.priceMicros)) {
            rkVar.h = this.f4724a.priceMicros.longValue();
        }
        rkVar.e = cu.d(new xz(Callback.DEFAULT_DRAG_ANIMATION_DURATION, "revenue productID", this.e).a(this.f4724a.productID));
        rkVar.b = wk.a(this.f4724a.quantity, 1);
        rkVar.f = cu.d((String) this.b.a(this.f4724a.payload));
        int i = 0;
        if (cx.a((Object) this.f4724a.receipt)) {
            a aVar = new a();
            String str = (String) this.c.a(this.f4724a.receipt.data);
            if (xu.a(this.f4724a.receipt.data, str)) {
                i = this.f4724a.receipt.data.length() + 0;
            }
            String str2 = (String) this.d.a(this.f4724a.receipt.signature);
            aVar.b = cu.d(str);
            aVar.c = cu.d(str2);
            rkVar.g = aVar;
        }
        return new Pair<>(e.a((e) rkVar), Integer.valueOf(i));
    }
}
