package com.yandex.metrica.impl.ob;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Point;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.metrica.impl.ob.v.b;
import org.json.JSONException;
import org.json.JSONObject;

@Deprecated
public class nt {

    /* renamed from: a reason: collision with root package name */
    private final String f5017a;
    private final String b;
    private final String c;
    private final Point d;

    @SuppressLint({"NewApi", "HardwareIds", "ObsoleteSdkInt"})
    public nt(@NonNull Context context, @Nullable String str, @NonNull pr prVar) {
        this.f5017a = Build.MANUFACTURER;
        this.b = Build.MODEL;
        this.c = a(context, str, prVar);
        b bVar = v.a(context).f;
        this.d = new Point(bVar.f5263a, bVar.b);
    }

    @SuppressLint({"HardwareIds", "ObsoleteSdkInt", "MissingPermission", "NewApi"})
    @NonNull
    private String a(@NonNull Context context, @Nullable String str, @NonNull pr prVar) {
        if (cx.a(28)) {
            if (prVar.d(context)) {
                try {
                    return Build.getSerial();
                } catch (Throwable unused) {
                }
            }
            return wk.b(str, "");
        } else if (cx.a(8)) {
            return Build.SERIAL;
        } else {
            return wk.b(str, "");
        }
    }

    @NonNull
    public String a() {
        return this.c;
    }

    public nt(String str) throws JSONException {
        JSONObject jSONObject = new JSONObject(str);
        this.f5017a = jSONObject.getString("manufacturer");
        this.b = jSONObject.getString("model");
        this.c = jSONObject.getString("serial");
        this.d = new Point(jSONObject.getInt("width"), jSONObject.getInt("height"));
    }

    public JSONObject b() throws JSONException {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("manufacturer", this.f5017a);
        jSONObject.put("model", this.b);
        jSONObject.put("serial", this.c);
        jSONObject.put("width", this.d.x);
        jSONObject.put("height", this.d.y);
        return jSONObject;
    }

    public boolean equals(Object obj) {
        boolean z = true;
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        nt ntVar = (nt) obj;
        if (this.f5017a == null ? ntVar.f5017a != null : !this.f5017a.equals(ntVar.f5017a)) {
            return false;
        }
        if (this.b == null ? ntVar.b != null : !this.b.equals(ntVar.b)) {
            return false;
        }
        if (this.d != null) {
            z = this.d.equals(ntVar.d);
        } else if (ntVar.d != null) {
            z = false;
        }
        return z;
    }

    public int hashCode() {
        int i = 0;
        int hashCode = (((this.f5017a != null ? this.f5017a.hashCode() : 0) * 31) + (this.b != null ? this.b.hashCode() : 0)) * 31;
        if (this.d != null) {
            i = this.d.hashCode();
        }
        return hashCode + i;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("DeviceSnapshot{mManufacturer='");
        sb.append(this.f5017a);
        sb.append('\'');
        sb.append(", mModel='");
        sb.append(this.b);
        sb.append('\'');
        sb.append(", mSerial='");
        sb.append(this.c);
        sb.append('\'');
        sb.append(", mScreenSize=");
        sb.append(this.d);
        sb.append('}');
        return sb.toString();
    }
}
