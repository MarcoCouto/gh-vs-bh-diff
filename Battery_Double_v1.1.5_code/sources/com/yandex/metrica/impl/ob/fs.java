package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.os.ResultReceiver;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public class fs implements fp, uh {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Context f4844a;
    @NonNull
    private ei b;
    @Nullable
    private final ResultReceiver c;

    public void a(@NonNull ue ueVar, @Nullable uk ukVar) {
    }

    public fs(@NonNull Context context, @NonNull ei eiVar, @NonNull eg egVar) {
        this.f4844a = context;
        this.b = eiVar;
        this.c = egVar.c;
        this.b.a(this);
    }

    public void a(@NonNull w wVar, @NonNull eg egVar) {
        this.b.a(egVar.b);
        this.b.a(wVar, this);
    }

    public void a(@Nullable uk ukVar) {
        x.a(this.c, ukVar);
    }

    public void a() {
        this.b.b(this);
    }

    @NonNull
    public ei b() {
        return this.b;
    }
}
