package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import android.util.SparseArray;
import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import org.json.JSONObject;

public class cm extends bi {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public final ly f4733a;
    /* access modifiers changed from: private */
    @NonNull
    public final Context b;

    static class a implements a {
        @NonNull

        /* renamed from: a reason: collision with root package name */
        private final mf<Collection<pu>> f4735a;
        @NonNull
        private final mf<uk> b;
        @NonNull
        private final ns c;

        public a(@NonNull mf<Collection<pu>> mfVar, @NonNull mf<uk> mfVar2, @NonNull ns nsVar) {
            this.f4735a = mfVar;
            this.b = mfVar2;
            this.c = nsVar;
        }

        public void a(@NonNull Context context) {
            c(context);
            com.yandex.metrica.impl.ob.uk.a a2 = ((uk) this.b.a()).a();
            a(context, a2);
            a(a2);
            this.b.a(a2.a());
            b(context);
        }

        private void b(@NonNull Context context) {
            context.getSharedPreferences("com.yandex.metrica.configuration", 0).edit().clear().apply();
        }

        private void a(@NonNull com.yandex.metrica.impl.ob.uk.a aVar) {
            aVar.c(true);
        }

        private void a(@NonNull Context context, @NonNull com.yandex.metrica.impl.ob.uk.a aVar) {
            nq a2 = this.c.a(context);
            if (a2 != null) {
                aVar.b(a2.f5014a).d(a2.b);
            }
        }

        private void c(@NonNull Context context) {
            li d = ld.a(context).d();
            List a2 = d.a();
            if (a2 != null) {
                this.f4735a.a(a2);
                d.b();
            }
        }
    }

    static class b implements a {
        @NonNull

        /* renamed from: a reason: collision with root package name */
        private mf f4736a;
        @NonNull
        private lz b;

        public b(@NonNull mf mfVar, @NonNull lz lzVar) {
            this.f4736a = mfVar;
            this.b = lzVar;
        }

        public void a(Context context) {
            this.f4736a.a(this.b.a());
        }
    }

    static class c implements a {
        @NonNull

        /* renamed from: a reason: collision with root package name */
        private final ly f4737a;
        @NonNull
        private final qf b;

        public c(@NonNull ly lyVar, @NonNull qf qfVar) {
            this.f4737a = lyVar;
            this.b = qfVar;
        }

        public void a(Context context) {
            Boolean b2 = this.b.b();
            this.b.d().j();
            if (b2 != null) {
                this.f4737a.a(b2.booleanValue()).q();
            }
        }
    }

    static class d implements a {
        @NonNull

        /* renamed from: a reason: collision with root package name */
        private final mf<Collection<pu>> f4738a;
        @NonNull
        private final mf<pn> b;

        d(@NonNull mf<Collection<pu>> mfVar, @NonNull mf<pn> mfVar2) {
            this.f4738a = mfVar;
            this.b = mfVar2;
        }

        public void a(Context context) {
            this.b.a(new pn(new ArrayList((Collection) this.f4738a.a()), null, new ArrayList()));
        }
    }

    static class e implements a {

        /* renamed from: a reason: collision with root package name */
        private qi f4739a;
        private lz b;

        e(@NonNull Context context) {
            this.f4739a = new qi(context);
            this.b = new lz(ld.a(context).e(), context.getPackageName());
        }

        public void a(Context context) {
            String a2 = this.f4739a.a((String) null);
            if (!TextUtils.isEmpty(a2)) {
                this.b.b(a2).q();
                qi.a(context);
            }
        }
    }

    static class f implements a {
        f() {
        }

        public void a(Context context) {
            qf qfVar = new qf(context, context.getPackageName());
            SharedPreferences a2 = ql.a(context, "_boundentrypreferences");
            String string = a2.getString(qf.d.a(), null);
            long j = a2.getLong(qf.e.a(), -1);
            if (string != null && j != -1) {
                qfVar.a(new com.yandex.metrica.impl.ob.i.a(string, j)).j();
                a2.edit().remove(qf.d.a()).remove(qf.e.a()).apply();
            }
        }
    }

    static class g implements a {
        g() {
        }

        public void a(Context context) {
            ly lyVar = new ly(ld.a(context).c());
            c(context, lyVar);
            b(context, lyVar);
            a(context, lyVar);
            lyVar.q();
            qa qaVar = new qa(context);
            qaVar.a();
            qaVar.b();
            b(context);
        }

        private void b(Context context) {
            new ns().a(context, new nq(wk.b(new lz(ld.a(context).e(), context.getPackageName()).a().b, ""), null), new pr(new pm()));
        }

        private void a(Context context, ly lyVar) {
            qf qfVar = new qf(context, new fb(context.getPackageName(), null).toString());
            Boolean b = qfVar.b();
            qfVar.d();
            if (b != null) {
                lyVar.a(b.booleanValue());
            }
            String b2 = qfVar.b((String) null);
            if (!TextUtils.isEmpty(b2)) {
                lyVar.a(b2);
            }
            qfVar.d().c().j();
        }

        private void b(Context context, ly lyVar) {
            qh qhVar = new qh(context, context.getPackageName());
            long a2 = qhVar.a(0);
            if (a2 != 0) {
                lyVar.a(a2);
            }
            qhVar.a();
        }

        private void c(Context context, ly lyVar) {
            qj qjVar = new qj(context);
            if (qjVar.a()) {
                lyVar.b(true);
                qjVar.b();
            }
        }
    }

    static class h implements a {
        h() {
        }

        public void a(Context context) {
            a(context, new ly(ld.a(context).c()));
        }

        private void a(Context context, ly lyVar) {
            int i = (new lz(ld.a(context).e(), context.getPackageName()).a().t > 0 ? 1 : (new lz(ld.a(context).e(), context.getPackageName()).a().t == 0 ? 0 : -1));
            boolean z = true;
            boolean z2 = i > 0;
            if (lyVar.c(-1) <= 0) {
                z = false;
            }
            if (z2 || z) {
                lyVar.d(false).q();
            }
        }
    }

    static class i implements a {
        i() {
        }

        public void a(Context context) {
            lz lzVar = new lz(ld.a(context).e(), context.getPackageName());
            String i = lzVar.i(null);
            if (i != null) {
                lzVar.a(Collections.singletonList(i));
            }
            String j = lzVar.j(null);
            if (j != null) {
                lzVar.b(Collections.singletonList(j));
            }
        }
    }

    static class j implements a {

        static class a implements FilenameFilter {

            /* renamed from: a reason: collision with root package name */
            final Iterable<FilenameFilter> f4740a;

            a(Iterable<FilenameFilter> iterable) {
                this.f4740a = iterable;
            }

            public boolean accept(File file, String str) {
                for (FilenameFilter accept : this.f4740a) {
                    if (accept.accept(file, str)) {
                        return true;
                    }
                }
                return false;
            }
        }

        static class b implements FilenameFilter {

            /* renamed from: a reason: collision with root package name */
            private final FilenameFilter f4741a;

            b(FilenameFilter filenameFilter) {
                this.f4741a = filenameFilter;
            }

            public boolean accept(File file, String str) {
                if (str.startsWith("db_metrica_")) {
                    try {
                        return this.f4741a.accept(file, j.a(str));
                    } catch (Throwable unused) {
                    }
                }
                return false;
            }
        }

        static class c implements FilenameFilter {
            c() {
            }

            public boolean accept(File file, String str) {
                return str.endsWith("null");
            }
        }

        static class d implements FilenameFilter {

            /* renamed from: a reason: collision with root package name */
            private final String f4742a;

            d(@NonNull String str) {
                this.f4742a = str;
            }

            public boolean accept(File file, String str) {
                return !str.contains(this.f4742a);
            }
        }

        j() {
        }

        public void a(Context context) {
            b(context);
            d(context);
        }

        private void d(Context context) {
            new lz(ld.a(context).e(), context.getPackageName()).r(new qk("LAST_STARTUP_CLIDS_SAVE_TIME").b()).q();
        }

        /* access modifiers changed from: 0000 */
        public void b(@NonNull Context context) {
            File[] listFiles;
            File c2 = c(context);
            b bVar = new b(new d(context.getPackageName()));
            for (File file : c2.listFiles(new a(Arrays.asList(new FilenameFilter[]{bVar, new b(new c())})))) {
                try {
                    if (!file.delete()) {
                        tl.a(context).reportEvent("Can not delete file", new JSONObject().put("fileName", file.getName()).toString());
                    }
                } catch (Throwable th) {
                    tl.a(context).reportError("Can not delete file", th);
                }
            }
        }

        /* access modifiers changed from: 0000 */
        @VisibleForTesting
        public File c(@NonNull Context context) {
            if (cx.a(21)) {
                return context.getNoBackupFilesDir();
            }
            return new File(context.getFilesDir().getParentFile(), "databases");
        }

        @NonNull
        static String a(@NonNull String str) {
            return str.endsWith("-journal") ? str.replace("-journal", "") : str;
        }
    }

    static class k implements a {
        k() {
        }

        public void a(Context context) {
            mf a2 = com.yandex.metrica.impl.ob.np.a.a(uk.class).a(context);
            uk ukVar = (uk) a2.a();
            a2.a(ukVar.a().a(ukVar.t > 0).c(true).a());
        }
    }

    public cm(@NonNull Context context) {
        this.b = context;
        this.f4733a = new ly(ld.a(context).c());
    }

    /* access modifiers changed from: 0000 */
    public SparseArray<a> a() {
        return new SparseArray<a>() {
            {
                put(29, new e(cm.this.b));
                put(39, new f());
                put(47, new g());
                put(60, new h());
                put(62, new i());
                put(66, new j());
                put(67, new b(com.yandex.metrica.impl.ob.np.a.a(uk.class).a(cm.this.b), new lz(ld.a(cm.this.b).e(), cm.this.b.getPackageName())));
                put(68, new k());
                put(72, new a(com.yandex.metrica.impl.ob.np.a.b(pu.class).a(cm.this.b), com.yandex.metrica.impl.ob.np.a.a(uk.class).a(cm.this.b), new ns()));
                put(73, new c(cm.this.f4733a, new qf(cm.this.b, new fb(cm.this.b.getPackageName(), null).toString())));
                put(82, new d(com.yandex.metrica.impl.ob.np.a.b(pu.class).a(cm.this.b), com.yandex.metrica.impl.ob.np.a.a(pn.class).a(cm.this.b)));
            }
        };
    }

    /* access modifiers changed from: protected */
    public int a(qg qgVar) {
        int a2 = qgVar.a();
        return a2 == -1 ? this.f4733a.a(-1) : a2;
    }

    /* access modifiers changed from: protected */
    public void a(qg qgVar, int i2) {
        this.f4733a.b(i2).q();
        qgVar.b().j();
    }
}
