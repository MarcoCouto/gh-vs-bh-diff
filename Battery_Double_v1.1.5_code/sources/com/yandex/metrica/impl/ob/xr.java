package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;

public class xr extends xq<byte[]> {
    @VisibleForTesting(otherwise = 3)
    public /* bridge */ /* synthetic */ int a() {
        return super.a();
    }

    @VisibleForTesting(otherwise = 3)
    @NonNull
    public /* bridge */ /* synthetic */ String b() {
        return super.b();
    }

    public xr(int i, @NonNull String str, @NonNull vz vzVar) {
        super(i, str, vzVar);
    }

    @Nullable
    public byte[] a(@Nullable byte[] bArr) {
        if (bArr.length <= a()) {
            return bArr;
        }
        byte[] bArr2 = new byte[a()];
        System.arraycopy(bArr, 0, bArr2, 0, a());
        if (this.f5306a.c()) {
            this.f5306a.b("\"%s\" %s exceeded limit of %d bytes", b(), bArr, Integer.valueOf(a()));
        }
        return bArr2;
    }
}
