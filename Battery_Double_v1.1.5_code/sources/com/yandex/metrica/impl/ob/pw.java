package com.yandex.metrica.impl.ob;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import java.util.ArrayList;
import java.util.List;

@TargetApi(16)
class pw implements pt {

    /* renamed from: a reason: collision with root package name */
    private final Context f5069a;
    private final yc b;

    public pw(Context context) {
        this(context, new yc());
    }

    @VisibleForTesting
    public pw(Context context, @NonNull yc ycVar) {
        this.f5069a = context;
        this.b = ycVar;
    }

    @NonNull
    public List<pu> a() {
        ArrayList arrayList = new ArrayList();
        PackageInfo a2 = this.b.a(this.f5069a, this.f5069a.getPackageName(), 4096);
        if (a2 != null) {
            for (int i = 0; i < a2.requestedPermissions.length; i++) {
                String str = a2.requestedPermissions[i];
                if ((a2.requestedPermissionsFlags[i] & 2) != 0) {
                    arrayList.add(new pu(str, true));
                } else {
                    arrayList.add(new pu(str, false));
                }
            }
        }
        return arrayList;
    }
}
