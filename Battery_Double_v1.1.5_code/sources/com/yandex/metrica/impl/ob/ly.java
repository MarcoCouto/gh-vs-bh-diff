package com.yandex.metrica.impl.ob;

import android.support.annotation.Nullable;
import android.util.Base64;

public class ly extends lx {

    /* renamed from: a reason: collision with root package name */
    static final qk f4987a = new qk("LOCATION_TRACKING_ENABLED");
    @Deprecated
    public static final qk b = new qk("COLLECT_INSTALLED_APPS");
    public static final qk c = new qk("INSTALLED_APP_COLLECTING");
    static final qk d = new qk("REFERRER");
    static final qk e = new qk("REFERRER_FROM_PLAY_SERVICES");
    static final qk f = new qk("REFERRER_HANDLED");
    static final qk g = new qk("REFERRER_HOLDER_STATE");
    static final qk h = new qk("PREF_KEY_OFFSET");
    static final qk i = new qk("UNCHECKED_TIME");
    static final qk j = new qk("L_REQ_NUM");
    static final qk k = new qk("L_ID");
    static final qk l = new qk("LBS_ID");
    static final qk m = new qk("STATISTICS_RESTRICTED_IN_MAIN");
    static final qk n = new qk("SDKFCE");
    static final qk o = new qk("FST");
    static final qk p = new qk("LSST");
    static final qk r = new qk("FSDKFCO");
    static final qk s = new qk("SRSDKFC");
    static final qk t = new qk("LSDKFCAT");
    static final qk u = new qk("LAST_IDENTITY_LIGHT_SEND_TIME");
    private static final qk v = new qk("LAST_MIGRATION_VERSION");

    public ly(lf lfVar) {
        super(lfVar);
    }

    public boolean a() {
        return b(c.b(), false);
    }

    public String b() {
        return c(d.b(), null);
    }

    public sj c() {
        return b(c(e.b(), null));
    }

    private sj b(String str) {
        if (str == null) {
            return null;
        }
        try {
            return sj.a(Base64.encode(str.getBytes(), 0));
        } catch (d unused) {
            return null;
        }
    }

    public boolean d() {
        return b(f.b(), false);
    }

    public ly a(boolean z) {
        return (ly) a(c.b(), z);
    }

    public ly a(String str) {
        return (ly) b(d.b(), str);
    }

    public ly a(sj sjVar) {
        return (ly) b(e.b(), b(sjVar));
    }

    private String b(@Nullable sj sjVar) {
        if (sjVar == null) {
            return null;
        }
        return new String(Base64.encode(sjVar.a(), 0));
    }

    public ly e() {
        return (ly) a(f.b(), true);
    }

    public ly f() {
        return (ly) r(d.b()).r(e.b());
    }

    public int a(int i2) {
        return b(v.b(), i2);
    }

    public ly b(int i2) {
        return (ly) a(v.b(), i2);
    }

    public void b(boolean z) {
        a(f4987a.b(), z).q();
    }

    public boolean g() {
        return b(f4987a.b(), false);
    }

    public long c(int i2) {
        return b(h.b(), (long) i2);
    }

    public ly a(long j2) {
        return (ly) a(h.b(), j2);
    }

    public long b(long j2) {
        return b(j.b(), j2);
    }

    public ly c(long j2) {
        return (ly) a(j.b(), j2);
    }

    public long d(long j2) {
        return b(k.b(), j2);
    }

    public ly e(long j2) {
        return (ly) a(k.b(), j2);
    }

    public long f(long j2) {
        return b(l.b(), j2);
    }

    public ly g(long j2) {
        return (ly) a(l.b(), j2);
    }

    public boolean c(boolean z) {
        return b(i.b(), z);
    }

    public ly d(boolean z) {
        return (ly) a(i.b(), z);
    }

    public int d(int i2) {
        return b(g.b(), i2);
    }

    public ly e(int i2) {
        return (ly) a(g.b(), i2);
    }

    @Nullable
    public Boolean h() {
        if (t(m.b())) {
            return Boolean.valueOf(b(m.b(), true));
        }
        return null;
    }

    public ly e(boolean z) {
        return (ly) a(m.b(), z);
    }

    public long h(long j2) {
        return b(u.b(), j2);
    }

    public ly i(long j2) {
        return (ly) a(u.b(), j2);
    }
}
