package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import com.yandex.metrica.impl.ob.et;

public class gl<T, C extends et> {

    /* renamed from: a reason: collision with root package name */
    private final gr<T> f4854a;
    private final C b;

    protected interface a<T> {
        boolean a(T t, w wVar);
    }

    protected gl(gr<T> grVar, C c) {
        this.f4854a = grVar;
        this.b = c;
    }

    /* access modifiers changed from: protected */
    public boolean a(@NonNull w wVar, @NonNull a<T> aVar) {
        for (Object a2 : a(wVar).a()) {
            if (aVar.a(a2, wVar)) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: 0000 */
    public go<T> a(w wVar) {
        return this.f4854a.a(wVar.g());
    }
}
