package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.SparseArray;

public class s extends bi {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public final lv f5127a;

    static class a implements a {

        /* renamed from: a reason: collision with root package name */
        private lv f5129a;

        private boolean a(long j, long j2, long j3) {
            return j != j3 && j2 == j3;
        }

        public a(lv lvVar) {
            this.f5129a = lvVar;
        }

        public void a(Context context) {
            qe qeVar = new qe(context);
            if (cx.a(qeVar.c())) {
                return;
            }
            if (this.f5129a.a((String) null) == null || this.f5129a.b((String) null) == null) {
                a(qeVar);
                b(qeVar);
                c(qeVar);
                d(qeVar);
                e(qeVar);
                f(qeVar);
                g(qeVar);
                h(qeVar);
                this.f5129a.q();
                qeVar.b().j();
            }
        }

        private boolean a(String str, String str2) {
            return !TextUtils.isEmpty(str) && TextUtils.isEmpty(str2);
        }

        private void a(@NonNull qe qeVar) {
            String b = qeVar.b((String) null);
            if (a(b, this.f5129a.b((String) null))) {
                this.f5129a.h(b);
            }
        }

        private void b(@NonNull qe qeVar) {
            String a2 = qeVar.a();
            if (a(a2, this.f5129a.a())) {
                this.f5129a.m(a2);
            }
        }

        private void c(@NonNull qe qeVar) {
            String a2 = qeVar.a((String) null);
            if (a(a2, this.f5129a.a((String) null))) {
                this.f5129a.g(a2);
            }
        }

        private void d(@NonNull qe qeVar) {
            String c = qeVar.c(null);
            if (a(c, this.f5129a.d((String) null))) {
                this.f5129a.j(c);
            }
        }

        private void e(@NonNull qe qeVar) {
            String d = qeVar.d(null);
            if (a(d, this.f5129a.e((String) null))) {
                this.f5129a.k(d);
            }
        }

        private void f(@NonNull qe qeVar) {
            String e = qeVar.e(null);
            if (a(e, this.f5129a.f((String) null))) {
                this.f5129a.l(e);
            }
        }

        private void g(@NonNull qe qeVar) {
            long a2 = qeVar.a(-1);
            if (a(a2, this.f5129a.a(-1), -1)) {
                this.f5129a.d(a2);
            }
        }

        private void h(@NonNull qe qeVar) {
            long b = qeVar.b(-1);
            if (a(b, this.f5129a.b(-1), -1)) {
                this.f5129a.e(b);
            }
        }
    }

    private class b implements a {
        private final lv b;

        public b(lv lvVar) {
            this.b = lvVar;
        }

        public void a(Context context) {
            this.b.r(new qk("COOKIE_BROWSERS").b());
            this.b.r(new qk("BIND_ID_URL").b());
            am.a(context, "b_meta.dat");
            am.a(context, "browsers.dat");
        }
    }

    public s(lv lvVar) {
        this.f5127a = lvVar;
    }

    /* access modifiers changed from: 0000 */
    public SparseArray<a> a() {
        return new SparseArray<a>() {
            {
                put(47, new a(s.this.f5127a));
                put(66, new b(s.this.f5127a));
            }
        };
    }

    /* access modifiers changed from: protected */
    public int a(qg qgVar) {
        return (int) this.f5127a.c(-1);
    }

    /* access modifiers changed from: protected */
    public void a(qg qgVar, int i) {
        this.f5127a.f((long) i);
        qgVar.c().j();
    }
}
