package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import java.util.HashSet;
import java.util.Set;

public class ip {

    /* renamed from: a reason: collision with root package name */
    private boolean f4886a;
    @NonNull
    private Set<Integer> b;
    private int c;
    private int d;

    public ip() {
        this(false, 0, 0, (Set<Integer>) new HashSet<Integer>());
    }

    public ip(boolean z, int i, int i2, @NonNull int[] iArr) {
        this(z, i, i2, cx.a(iArr));
    }

    public ip(boolean z, int i, int i2, @NonNull Set<Integer> set) {
        this.f4886a = z;
        this.b = set;
        this.c = i;
        this.d = i2;
    }

    public void a() {
        this.b = new HashSet();
        this.d = 0;
    }

    public boolean b() {
        return this.f4886a;
    }

    public void a(boolean z) {
        this.f4886a = z;
    }

    @NonNull
    public Set<Integer> c() {
        return this.b;
    }

    public int d() {
        return this.d;
    }

    public int e() {
        return this.c;
    }

    public void a(int i) {
        this.c = i;
        this.d = 0;
    }

    public void b(int i) {
        this.b.add(Integer.valueOf(i));
        this.d++;
    }
}
