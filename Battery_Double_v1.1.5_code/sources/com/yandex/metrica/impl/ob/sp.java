package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import com.yandex.metrica.impl.ob.sm;
import com.yandex.metrica.impl.ob.sn;
import com.yandex.metrica.impl.ob.sn.c;
import com.yandex.metrica.impl.ob.sn.d;

public abstract class sp<T extends sn, IA, A extends sm<IA, A>, L extends d<T, c<A>>> {
    @Nullable

    /* renamed from: a reason: collision with root package name */
    private T f5165a;
    @NonNull
    private L b;
    @NonNull
    private c<A> c;

    public sp(@NonNull L l, @NonNull uk ukVar, @NonNull A a2) {
        this.b = l;
        dr.a().a(this, dz.class, dv.a((du<T>) new du<dz>() {
            public void a(dz dzVar) {
                sp.this.a();
            }
        }).a());
        a(new c<>(ukVar, a2));
    }

    public synchronized void a() {
        this.f5165a = null;
    }

    /* access modifiers changed from: protected */
    public synchronized void a(@NonNull c<A> cVar) {
        this.c = cVar;
    }

    public synchronized void a(@NonNull IA ia) {
        if (!((sm) this.c.b).a(ia)) {
            a(new c<>(b(), ((sm) this.c.b).b(ia)));
            a();
        }
    }

    public synchronized void a(@NonNull uk ukVar) {
        a(new c<>(ukVar, c()));
        a();
    }

    @NonNull
    public synchronized uk b() {
        return this.c.f5164a;
    }

    @VisibleForTesting(otherwise = 4)
    @NonNull
    public synchronized A c() {
        return (sm) this.c.b;
    }

    @NonNull
    public synchronized T d() {
        if (this.f5165a == null) {
            this.f5165a = this.b.a(this.c);
        }
        return this.f5165a;
    }
}
