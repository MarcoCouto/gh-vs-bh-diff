package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import com.yandex.metrica.impl.ob.eg.a;
import com.yandex.metrica.impl.ob.et;
import com.yandex.metrica.impl.ob.ew;
import java.util.ArrayList;
import java.util.List;

public class fc<COMPONENT extends ew & et> implements ep, ev, uh {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Context f4834a;
    @NonNull
    private final ek b;
    @NonNull
    private final bl c;
    @NonNull
    private final ft<COMPONENT> d;
    @NonNull
    private final ul e;
    @NonNull
    private final fh f;
    @Nullable
    private COMPONENT g;
    @Nullable
    private eu h;
    private List<uh> i;
    @NonNull
    private final el<fp> j;

    public fc(@NonNull Context context, @NonNull ek ekVar, @NonNull eg egVar, @NonNull ft<COMPONENT> ftVar) {
        this(context, ekVar, egVar, new fh(egVar.b), ftVar, new bm(), new el(), uc.a());
    }

    public fc(@NonNull Context context, @NonNull ek ekVar, @NonNull eg egVar, @NonNull fh fhVar, @NonNull ft<COMPONENT> ftVar, @NonNull bm bmVar, @NonNull el<fp> elVar, @NonNull uc ucVar) {
        this.i = new ArrayList();
        this.f4834a = context;
        this.b = ekVar;
        this.f = fhVar;
        this.c = bmVar.a(this.f4834a, this.b);
        this.d = ftVar;
        this.j = elVar;
        this.e = ucVar.a(this.f4834a, a(), this, egVar.f4809a);
    }

    public void a(@NonNull w wVar, @NonNull eg egVar) {
        ew ewVar;
        b();
        if (af.d(wVar.g())) {
            ewVar = e();
        } else {
            ewVar = d();
        }
        if (!af.a(wVar.g())) {
            a(egVar.b);
        }
        ewVar.a(wVar);
    }

    private void b() {
        e().a();
    }

    public synchronized void a(@NonNull a aVar) {
        this.f.a(aVar);
        if (this.h != null) {
            this.h.a(aVar);
        }
        if (this.g != null) {
            ((et) this.g).a(aVar);
        }
    }

    public synchronized void a(@NonNull fp fpVar) {
        this.j.a(fpVar);
    }

    public synchronized void b(@NonNull fp fpVar) {
        this.j.b(fpVar);
    }

    private COMPONENT d() {
        if (this.g == null) {
            synchronized (this) {
                this.g = this.d.d(this.f4834a, this.b, this.f.a(), this.c, this.e);
                this.i.add(this.g);
            }
        }
        return this.g;
    }

    private eu e() {
        if (this.h == null) {
            synchronized (this) {
                this.h = this.d.c(this.f4834a, this.b, this.f.a(), this.c, this.e);
                this.i.add(this.h);
            }
        }
        return this.h;
    }

    @VisibleForTesting
    @NonNull
    public final ek a() {
        return this.b;
    }

    public synchronized void a(@Nullable uk ukVar) {
        for (uh a2 : this.i) {
            a2.a(ukVar);
        }
    }

    public synchronized void a(@NonNull ue ueVar, @Nullable uk ukVar) {
        for (uh a2 : this.i) {
            a2.a(ueVar, ukVar);
        }
    }

    public void c() {
        if (this.g != null) {
            ((ep) this.g).c();
        }
        if (this.h != null) {
            this.h.c();
        }
    }

    public void a(@NonNull eg egVar) {
        this.e.a(egVar.f4809a);
        a(egVar.b);
    }
}
