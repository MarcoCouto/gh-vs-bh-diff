package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

public class uc {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public final wp<String, uh> f5228a = new wp<>();
    /* access modifiers changed from: private */
    public final HashMap<String, ul> b = new HashMap<>();
    private final uj c = new uj() {
        public void a(@NonNull String str, @NonNull uk ukVar) {
            for (uh a2 : a(str)) {
                a2.a(ukVar);
            }
        }

        public void a(@NonNull String str, @NonNull ue ueVar, @Nullable uk ukVar) {
            for (uh a2 : a(str)) {
                a2.a(ueVar, ukVar);
            }
        }

        @NonNull
        public List<uh> a(@NonNull String str) {
            synchronized (uc.this.b) {
                Collection a2 = uc.this.f5228a.a(str);
                if (a2 == null) {
                    ArrayList arrayList = new ArrayList();
                    return arrayList;
                }
                ArrayList arrayList2 = new ArrayList(a2);
                return arrayList2;
            }
        }
    };

    private static final class a {

        /* renamed from: a reason: collision with root package name */
        static final uc f5230a = new uc();
    }

    public static final uc a() {
        return a.f5230a;
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public ul a(@NonNull Context context, @NonNull ek ekVar, @NonNull com.yandex.metrica.impl.ob.su.a aVar) {
        ul ulVar = (ul) this.b.get(ekVar.b());
        boolean z = true;
        if (ulVar == null) {
            synchronized (this.b) {
                ulVar = (ul) this.b.get(ekVar.b());
                if (ulVar == null) {
                    ul b2 = b(context, ekVar, aVar);
                    this.b.put(ekVar.b(), b2);
                    ulVar = b2;
                    z = false;
                }
            }
        }
        if (z) {
            ulVar.a(aVar);
        }
        return ulVar;
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public ul b(@NonNull Context context, @NonNull ek ekVar, @NonNull com.yandex.metrica.impl.ob.su.a aVar) {
        return new ul(context, ekVar.b(), aVar, this.c);
    }

    @NonNull
    public ul a(@NonNull Context context, @NonNull ek ekVar, @NonNull uh uhVar, @NonNull com.yandex.metrica.impl.ob.su.a aVar) {
        ul a2;
        synchronized (this.b) {
            this.f5228a.a(ekVar.b(), uhVar);
            a2 = a(context, ekVar, aVar);
        }
        return a2;
    }
}
