package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import java.util.HashMap;
import java.util.Map;

public abstract class vr {

    /* renamed from: a reason: collision with root package name */
    private static Map<String, vz> f5274a = new HashMap();
    private static Map<String, vp> b = new HashMap();

    @NonNull
    public static vz a(@Nullable String str) {
        if (TextUtils.isEmpty(str)) {
            return a();
        }
        if (!f5274a.containsKey(str)) {
            f5274a.put(str, new vz(str));
        }
        return (vz) f5274a.get(str);
    }

    @NonNull
    public static vp b(@Nullable String str) {
        if (TextUtils.isEmpty(str)) {
            return b();
        }
        if (!b.containsKey(str)) {
            b.put(str, new vp(str));
        }
        return (vp) b.get(str);
    }

    @NonNull
    public static vz a() {
        return vz.h();
    }

    @NonNull
    public static vp b() {
        return vp.h();
    }
}
