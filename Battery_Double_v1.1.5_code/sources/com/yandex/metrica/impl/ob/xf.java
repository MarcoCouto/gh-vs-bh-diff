package com.yandex.metrica.impl.ob;

import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;

public class xf {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final xe f5300a;
    @Nullable
    private volatile xi b;
    @Nullable
    private volatile xh c;
    @Nullable
    private volatile xh d;
    @Nullable
    private volatile Handler e;

    public xf() {
        this(new xe());
    }

    @NonNull
    public xh a() {
        if (this.c == null) {
            synchronized (this) {
                if (this.c == null) {
                    this.c = this.f5300a.b();
                }
            }
        }
        return this.c;
    }

    @NonNull
    public xi b() {
        if (this.b == null) {
            synchronized (this) {
                if (this.b == null) {
                    this.b = this.f5300a.d();
                }
            }
        }
        return this.b;
    }

    @NonNull
    public xh c() {
        if (this.d == null) {
            synchronized (this) {
                if (this.d == null) {
                    this.d = this.f5300a.c();
                }
            }
        }
        return this.d;
    }

    @NonNull
    public Handler d() {
        if (this.e == null) {
            synchronized (this) {
                if (this.e == null) {
                    this.e = this.f5300a.a();
                }
            }
        }
        return this.e;
    }

    @VisibleForTesting
    xf(@NonNull xe xeVar) {
        this.f5300a = xeVar;
    }
}
