package com.yandex.metrica.impl.ob;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.telephony.CellLocation;
import android.telephony.TelephonyManager;
import android.telephony.gsm.GsmCellLocation;
import android.text.TextUtils;
import android.util.SparseArray;

@SuppressLint({"NewApi"})
public class uw implements ve<uu> {
    /* access modifiers changed from: private */
    @SuppressLint({"InlineApi"})

    /* renamed from: a reason: collision with root package name */
    public static final SparseArray<String> f5244a = new SparseArray<String>() {
        {
            put(0, null);
            put(7, "1xRTT");
            put(4, "CDMA");
            put(2, "EDGE");
            put(14, "eHRPD");
            put(5, "EVDO rev.0");
            put(6, "EVDO rev.A");
            put(12, "EVDO rev.B");
            put(1, "GPRS");
            put(8, "HSDPA");
            put(10, "HSPA");
            put(15, "HSPA+");
            put(9, "HSUPA");
            put(11, "iDen");
            put(3, "UMTS");
            put(12, "EVDO rev.B");
            if (cx.a(11)) {
                put(14, "eHRPD");
                put(13, "LTE");
                if (cx.a(13)) {
                    put(15, "HSPA+");
                }
            }
        }
    };
    /* access modifiers changed from: private */
    @NonNull
    public final ux b;
    /* access modifiers changed from: private */
    @NonNull
    public pr c;

    public uw(@NonNull ux uxVar, @NonNull pr prVar) {
        this.b = uxVar;
        this.c = prVar;
    }

    @Nullable
    /* renamed from: a */
    public uu d() {
        if (!this.b.i()) {
            return null;
        }
        uu uuVar = new uu(f(), g(), c(), b(), i(), h(), null, true, 0, null, null);
        return uuVar;
    }

    @Nullable
    private Integer f() {
        return (Integer) cx.a((wo<T, S>) new wo<TelephonyManager, Integer>() {
            public Integer a(TelephonyManager telephonyManager) throws Throwable {
                String networkOperator = telephonyManager.getNetworkOperator();
                String substring = !TextUtils.isEmpty(networkOperator) ? networkOperator.substring(0, 3) : null;
                if (!TextUtils.isEmpty(substring)) {
                    return Integer.valueOf(Integer.parseInt(substring));
                }
                return null;
            }
        }, this.b.c(), "getting phoneMcc", "TelephonyManager");
    }

    @Nullable
    private Integer g() {
        return (Integer) cx.a((wo<T, S>) new wo<TelephonyManager, Integer>() {
            public Integer a(TelephonyManager telephonyManager) throws Throwable {
                String networkOperator = telephonyManager.getNetworkOperator();
                String substring = !TextUtils.isEmpty(networkOperator) ? networkOperator.substring(3) : null;
                if (!TextUtils.isEmpty(substring)) {
                    return Integer.valueOf(Integer.parseInt(substring));
                }
                return null;
            }
        }, this.b.c(), "getting phoneMnc", "TelephonyManager");
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    @VisibleForTesting
    @SuppressLint({"MissingPermission"})
    public Integer b() {
        return (Integer) cx.a((wo<T, S>) new wo<TelephonyManager, Integer>() {
            public Integer a(TelephonyManager telephonyManager) throws Throwable {
                Integer num = null;
                if (!uw.this.c.c(uw.this.b.d())) {
                    return null;
                }
                GsmCellLocation gsmCellLocation = (GsmCellLocation) telephonyManager.getCellLocation();
                int cid = gsmCellLocation != null ? gsmCellLocation.getCid() : 1;
                if (1 != cid) {
                    num = Integer.valueOf(cid);
                }
                return num;
            }
        }, this.b.c(), "getting phoneCellId", "TelephonyManager");
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    @VisibleForTesting
    @SuppressLint({"MissingPermission"})
    public Integer c() {
        return (Integer) cx.a((wo<T, S>) new wo<TelephonyManager, Integer>() {
            public Integer a(TelephonyManager telephonyManager) throws Throwable {
                Integer num = null;
                if (uw.this.c.c(uw.this.b.d())) {
                    CellLocation cellLocation = telephonyManager.getCellLocation();
                    if (cellLocation != null) {
                        int lac = ((GsmCellLocation) cellLocation).getLac();
                        if (1 != lac) {
                            num = Integer.valueOf(lac);
                        }
                        return num;
                    }
                }
                return null;
            }
        }, this.b.c(), "getting phoneLac", "TelephonyManager");
    }

    @NonNull
    private String h() {
        return (String) cx.a(new wo<TelephonyManager, String>() {
            public String a(TelephonyManager telephonyManager) throws Throwable {
                return (String) uw.f5244a.get(telephonyManager.getNetworkType(), "unknown");
            }
        }, this.b.c(), "getting networkType", "TelephonyManager", "unknown");
    }

    @Nullable
    private String i() {
        return (String) cx.a((wo<T, S>) new wo<TelephonyManager, String>() {
            public String a(TelephonyManager telephonyManager) throws Throwable {
                return telephonyManager.getNetworkOperatorName();
            }
        }, this.b.c(), "getting network operator name", "TelephonyManager");
    }
}
