package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import com.yandex.metrica.impl.ob.rr.a.b.C0123a;
import com.yandex.metrica.impl.ob.tt.a;
import com.yandex.metrica.impl.ob.tt.a.C0130a;
import com.yandex.metrica.impl.ob.tt.a.b;
import com.yandex.metrica.impl.ob.tt.a.c;

public class nj implements mv<a, C0123a> {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final ni f5003a;
    @NonNull
    private final nm b;
    @NonNull
    private final nn c;

    public nj() {
        this(new ni(), new nm(), new nn());
    }

    @NonNull
    /* renamed from: a */
    public C0123a b(@NonNull a aVar) {
        C0123a aVar2 = new C0123a();
        if (!TextUtils.isEmpty(aVar.f5210a)) {
            aVar2.b = aVar.f5210a;
        }
        if (!TextUtils.isEmpty(aVar.b)) {
            aVar2.c = aVar.b;
        }
        if (aVar.c != null) {
            aVar2.d = this.f5003a.b(aVar.c);
        }
        if (aVar.d != null) {
            aVar2.e = this.b.b(aVar.d);
        }
        if (aVar.e != null) {
            aVar2.f = this.c.b(aVar.e);
        }
        return aVar2;
    }

    @NonNull
    public a a(@NonNull C0123a aVar) {
        C0130a aVar2;
        b bVar;
        c cVar;
        String str = TextUtils.isEmpty(aVar.b) ? null : aVar.b;
        String str2 = TextUtils.isEmpty(aVar.c) ? null : aVar.c;
        if (aVar.d == null) {
            aVar2 = null;
        } else {
            aVar2 = this.f5003a.a(aVar.d);
        }
        if (aVar.e == null) {
            bVar = null;
        } else {
            bVar = this.b.a(aVar.e);
        }
        if (aVar.f == null) {
            cVar = null;
        } else {
            cVar = this.c.a(aVar.f);
        }
        a aVar3 = new a(str, str2, aVar2, bVar, cVar);
        return aVar3;
    }

    @VisibleForTesting
    nj(@NonNull ni niVar, @NonNull nm nmVar, @NonNull nn nnVar) {
        this.f5003a = niVar;
        this.b = nmVar;
        this.c = nnVar;
    }
}
