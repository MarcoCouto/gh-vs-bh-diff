package com.yandex.metrica.impl.ob;

import android.support.annotation.Nullable;
import com.yandex.metrica.impl.ob.af.a;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class gp extends gr<hd> {

    /* renamed from: a reason: collision with root package name */
    private final ig f4858a;
    private final Map<a, hz<hd>> b = b();
    @Nullable
    private hw<hd> c = new hv(this.f4858a);
    @Nullable
    private hw<hd> d;

    public gp(en enVar) {
        this.f4858a = new ig(enVar);
    }

    private HashMap<a, hz<hd>> b() {
        HashMap<a, hz<hd>> hashMap = new HashMap<>();
        hashMap.put(a.EVENT_TYPE_ACTIVATION, new hu(this.f4858a));
        hashMap.put(a.EVENT_TYPE_START, new ij(this.f4858a));
        hashMap.put(a.EVENT_TYPE_REGULAR, new id(this.f4858a));
        ib ibVar = new ib(this.f4858a);
        hashMap.put(a.EVENT_TYPE_EXCEPTION_USER, ibVar);
        hashMap.put(a.EVENT_TYPE_EXCEPTION_USER_PROTOBUF, ibVar);
        hashMap.put(a.EVENT_TYPE_SEND_REFERRER, ibVar);
        hashMap.put(a.EVENT_TYPE_STATBOX, ibVar);
        hashMap.put(a.EVENT_TYPE_CUSTOM_EVENT, ibVar);
        hashMap.put(a.EVENT_TYPE_APP_OPEN, new Cif(this.f4858a));
        hashMap.put(a.EVENT_TYPE_PURGE_BUFFER, new ic(this.f4858a));
        hashMap.put(a.EVENT_TYPE_PREV_SESSION_NATIVE_CRASH, new ii(this.f4858a, this.f4858a.l()));
        hashMap.put(a.EVENT_TYPE_CURRENT_SESSION_NATIVE_CRASH, new hy(this.f4858a));
        hashMap.put(a.EVENT_TYPE_EXCEPTION_UNHANDLED_FROM_FILE, new il(this.f4858a));
        ik ikVar = new ik(this.f4858a);
        hashMap.put(a.EVENT_TYPE_EXCEPTION_UNHANDLED, ikVar);
        hashMap.put(a.EVENT_TYPE_EXCEPTION_UNHANDLED_PROTOBUF, ikVar);
        hashMap.put(a.EVENT_TYPE_EXCEPTION_UNHANDLED_FROM_INTENT, ikVar);
        hashMap.put(a.EVENT_TYPE_ANR, ibVar);
        hashMap.put(a.EVENT_TYPE_IDENTITY, new ia(this.f4858a));
        hashMap.put(a.EVENT_TYPE_SET_USER_INFO, new ih(this.f4858a));
        hashMap.put(a.EVENT_TYPE_REPORT_USER_INFO, new ii(this.f4858a, this.f4858a.g()));
        hashMap.put(a.EVENT_TYPE_APP_ENVIRONMENT_UPDATED, new ii(this.f4858a, this.f4858a.i()));
        hashMap.put(a.EVENT_TYPE_APP_ENVIRONMENT_CLEARED, new ii(this.f4858a, this.f4858a.j()));
        hashMap.put(a.EVENT_TYPE_SEND_USER_PROFILE, ibVar);
        hashMap.put(a.EVENT_TYPE_SET_USER_PROFILE_ID, new ii(this.f4858a, this.f4858a.o()));
        hashMap.put(a.EVENT_TYPE_SEND_REVENUE_EVENT, ibVar);
        hashMap.put(a.EVENT_TYPE_IDENTITY_LIGHT, ibVar);
        hashMap.put(a.EVENT_TYPE_CLEANUP, ibVar);
        return hashMap;
    }

    public void a(a aVar, hz<hd> hzVar) {
        this.b.put(aVar, hzVar);
    }

    public ig a() {
        return this.f4858a;
    }

    public go<hd> a(int i) {
        LinkedList linkedList = new LinkedList();
        a a2 = a.a(i);
        if (this.c != null) {
            this.c.a(a2, linkedList);
        }
        hz hzVar = (hz) this.b.get(a2);
        if (hzVar != null) {
            hzVar.a(linkedList);
        }
        if (this.d != null) {
            this.d.a(a2, linkedList);
        }
        return new gn(linkedList);
    }
}
