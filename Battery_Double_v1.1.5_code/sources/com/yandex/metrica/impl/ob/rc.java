package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import com.yandex.metrica.impl.ob.rl.a.C0115a;

public class rc extends qr<String> {

    /* renamed from: a reason: collision with root package name */
    private final yb<String> f5084a;

    public rc(@NonNull String str, @NonNull String str2, @NonNull yb<String> ybVar, @NonNull yk<String> ykVar, @NonNull qo qoVar) {
        super(0, str, str2, ykVar, qoVar);
        this.f5084a = ybVar;
    }

    /* access modifiers changed from: protected */
    public void a(@NonNull C0115a aVar) {
        String str = (String) this.f5084a.a(b());
        aVar.e.b = str == null ? new byte[0] : str.getBytes();
    }
}
