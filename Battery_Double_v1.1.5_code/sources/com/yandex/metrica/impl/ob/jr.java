package com.yandex.metrica.impl.ob;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;

public class jr {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final xh f4919a;

    public static class a implements Runnable {
        @NonNull

        /* renamed from: a reason: collision with root package name */
        private final jy f4920a;
        @Nullable
        private final Bundle b;
        @Nullable
        private final jw c;

        public a(@NonNull jy jyVar, @Nullable Bundle bundle) {
            this(jyVar, bundle, null);
        }

        public a(@NonNull jy jyVar, @Nullable Bundle bundle, @Nullable jw jwVar) {
            this.f4920a = jyVar;
            this.b = bundle;
            this.c = jwVar;
        }

        public void run() {
            try {
                this.f4920a.a(this.b, this.c);
            } catch (Throwable unused) {
                if (this.c != null) {
                    this.c.a();
                }
            }
        }
    }

    public jr() {
        this(al.a().j().f());
    }

    @VisibleForTesting
    jr(@NonNull xh xhVar) {
        this.f4919a = xhVar;
    }

    public void a(@NonNull jy jyVar, @Nullable Bundle bundle) {
        this.f4919a.a((Runnable) new a(jyVar, bundle));
    }

    public void a(@NonNull jy jyVar, @Nullable Bundle bundle, @Nullable jw jwVar) {
        this.f4919a.a((Runnable) new a(jyVar, bundle, jwVar));
    }

    @NonNull
    public xh a() {
        return this.f4919a;
    }
}
