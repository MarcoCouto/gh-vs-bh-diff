package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import com.yandex.metrica.impl.ob.rr.a.e;

public class ms implements mv<oh, e> {
    @NonNull
    /* renamed from: a */
    public e b(@NonNull oh ohVar) {
        e eVar = new e();
        eVar.b = ohVar.e;
        eVar.c = ohVar.f;
        eVar.d = ohVar.g;
        eVar.e = ohVar.h;
        eVar.f = ohVar.i;
        eVar.g = ohVar.j;
        eVar.h = ohVar.k;
        eVar.j = ohVar.l;
        eVar.i = ohVar.m;
        return eVar;
    }

    @NonNull
    public oh a(@NonNull e eVar) {
        oh ohVar = new oh(eVar.b, eVar.c, eVar.d, eVar.e, eVar.f, eVar.g, eVar.h, eVar.j, eVar.i);
        return ohVar;
    }
}
