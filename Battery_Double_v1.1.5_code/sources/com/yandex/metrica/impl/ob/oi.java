package com.yandex.metrica.impl.ob;

import android.location.Location;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import java.util.concurrent.TimeUnit;

class oi extends of {

    /* renamed from: a reason: collision with root package name */
    public static final long f5027a = TimeUnit.MINUTES.toMillis(2);
    public static final long b = TimeUnit.SECONDS.toMillis(10);
    @Nullable
    private com.yandex.metrica.impl.ob.p.a<Location> c;
    @NonNull
    private a d;
    private long e;

    public static class a {

        /* renamed from: a reason: collision with root package name */
        public final long f5028a;
        public final long b;
        public final long c;

        public a(long j, long j2, long j3) {
            this.f5028a = j;
            this.b = j2;
            this.c = j3;
        }
    }

    public oi(@Nullable oe oeVar) {
        a aVar = new a(f5027a, 200, 50);
        this(oeVar, aVar, b);
    }

    public void b(@Nullable String str, @Nullable Location location, @Nullable oh ohVar) {
        if (location != null) {
            a(location);
        }
    }

    private void a(@NonNull Location location) {
        if (this.c == null || this.c.a(this.e) || a(location, (Location) this.c.a())) {
            Location location2 = new Location(location);
            com.yandex.metrica.impl.ob.p.a<Location> aVar = new com.yandex.metrica.impl.ob.p.a<>();
            aVar.a(location2);
            this.c = aVar;
        }
    }

    @Nullable
    public Location a() {
        if (this.c == null) {
            return null;
        }
        return (Location) this.c.d();
    }

    private boolean a(@Nullable Location location, @Nullable Location location2) {
        return a(location, location2, this.d.f5028a, this.d.b);
    }

    public static boolean a(@Nullable Location location, @Nullable Location location2, long j, long j2) {
        if (location2 == null) {
            return true;
        }
        if (location == null) {
            return false;
        }
        long time = location.getTime() - location2.getTime();
        boolean z = time > j;
        boolean z2 = time < (-j);
        boolean z3 = time > 0;
        if (z) {
            return true;
        }
        if (z2) {
            return false;
        }
        int accuracy = (int) (location.getAccuracy() - location2.getAccuracy());
        boolean z4 = accuracy > 0;
        boolean z5 = accuracy < 0;
        boolean z6 = ((long) accuracy) > j2;
        boolean a2 = a(location.getProvider(), location2.getProvider());
        if (z5) {
            return true;
        }
        if (!z3 || z4) {
            return z3 && !z6 && a2;
        }
        return true;
    }

    static boolean a(@Nullable String str, @Nullable String str2) {
        if (str == null) {
            return str2 == null;
        }
        return str.equals(str2);
    }

    @VisibleForTesting
    oi(@Nullable oe oeVar, @NonNull a aVar, long j) {
        super(oeVar);
        this.d = aVar;
        this.e = j;
    }
}
