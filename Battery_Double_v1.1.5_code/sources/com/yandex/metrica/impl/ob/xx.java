package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;

public class xx {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final xw f5310a;
    @NonNull
    private final xv b;

    public xx(@NonNull vz vzVar, @NonNull String str) {
        xw xwVar = new xw(30, 50, 4000, str, vzVar);
        this(xwVar, new xv(4500, str, vzVar));
    }

    @VisibleForTesting
    xx(@NonNull xw xwVar, @NonNull xv xvVar) {
        this.f5310a = xwVar;
        this.b = xvVar;
    }

    public boolean a(@Nullable vx vxVar, @NonNull String str, @Nullable String str2) {
        if (vxVar != null) {
            String a2 = this.f5310a.a().a(str);
            String a3 = this.f5310a.b().a(str2);
            if (vxVar.containsKey(a2)) {
                String str3 = (String) vxVar.get(a2);
                if (a3 == null || !a3.equals(str3)) {
                    return a(vxVar, a2, a3, str3);
                }
            } else if (a3 != null) {
                return a(vxVar, a2, a3, null);
            }
        }
        return false;
    }

    /* access modifiers changed from: 0000 */
    public synchronized boolean a(@NonNull vx vxVar, @NonNull String str, @Nullable String str2, @Nullable String str3) {
        if (vxVar.size() >= this.f5310a.c().a()) {
            if (this.f5310a.c().a() != vxVar.size() || !vxVar.containsKey(str)) {
                this.f5310a.a(str);
                return false;
            }
        }
        if (!this.b.a(vxVar, str, str2)) {
            vxVar.put(str, str2);
            return true;
        }
        this.b.a(str);
        return false;
    }
}
