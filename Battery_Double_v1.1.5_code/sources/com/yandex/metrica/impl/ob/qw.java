package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import com.yandex.metrica.impl.ob.rl.a.C0115a;
import com.yandex.metrica.impl.ob.rl.a.b;
import com.yandex.metrica.impl.ob.rl.a.c;

public abstract class qw implements qm, rf {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final String f5082a;
    private final int b;
    @NonNull
    private final yk<String> c;
    @NonNull
    private final qo d;
    @NonNull
    private vz e = vr.a();

    qw(int i, @NonNull String str, @NonNull yk<String> ykVar, @NonNull qo qoVar) {
        this.b = i;
        this.f5082a = str;
        this.c = ykVar;
        this.d = qoVar;
    }

    @NonNull
    public String c() {
        return this.f5082a;
    }

    public int d() {
        return this.b;
    }

    @NonNull
    public qo e() {
        return this.d;
    }

    @NonNull
    public final C0115a a() {
        C0115a aVar = new C0115a();
        aVar.c = d();
        aVar.b = c().getBytes();
        aVar.e = new c();
        aVar.d = new b();
        return aVar;
    }

    public void a(@NonNull vz vzVar) {
        this.e = vzVar;
    }

    /* access modifiers changed from: protected */
    public boolean f() {
        yi a2 = this.c.a(c());
        if (a2.a()) {
            return true;
        }
        if (this.e.c()) {
            vz vzVar = this.e;
            StringBuilder sb = new StringBuilder();
            sb.append("Attribute ");
            sb.append(c());
            sb.append(" of type ");
            sb.append(rd.a(d()));
            sb.append(" is skipped because ");
            sb.append(a2.b());
            vzVar.b(sb.toString());
        }
        return false;
    }
}
