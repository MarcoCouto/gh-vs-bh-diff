package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import com.yandex.metrica.impl.ob.rr.a.b;

public class nh implements mv<tt, b> {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final nl f5002a;
    @NonNull
    private final nk b;

    public nh() {
        this(new nl(), new nk());
    }

    @NonNull
    /* renamed from: a */
    public b b(@NonNull tt ttVar) {
        b bVar = new b();
        bVar.b = this.f5002a.b(ttVar.f5209a);
        bVar.c = this.b.b(ttVar.b);
        bVar.d = ttVar.c;
        bVar.e = ttVar.d;
        return bVar;
    }

    @NonNull
    public tt a(@NonNull b bVar) {
        tt ttVar = new tt(this.f5002a.a(bVar.b), this.b.a(bVar.c), bVar.d, bVar.e);
        return ttVar;
    }

    @VisibleForTesting
    nh(@NonNull nl nlVar, @NonNull nk nkVar) {
        this.f5002a = nlVar;
        this.b = nkVar;
    }
}
