package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public class ya<T> implements yb<T> {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final yb<T> f5314a;
    @Nullable
    private final T b;

    public ya(@NonNull yb<T> ybVar, @Nullable T t) {
        this.f5314a = ybVar;
        this.b = t;
    }

    @Nullable
    public T a(@Nullable T t) {
        return t != this.f5314a.a(t) ? this.b : t;
    }
}
