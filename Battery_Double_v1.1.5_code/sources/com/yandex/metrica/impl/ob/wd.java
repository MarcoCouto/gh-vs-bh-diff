package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import java.util.concurrent.TimeUnit;

public class wd {

    /* renamed from: a reason: collision with root package name */
    private volatile long f5285a;
    private ly b;
    private wh c;

    private static class a {

        /* renamed from: a reason: collision with root package name */
        static wd f5286a = new wd();
    }

    public static wd a() {
        return a.f5286a;
    }

    private wd() {
    }

    public synchronized long b() {
        return this.f5285a;
    }

    public synchronized void a(@NonNull Context context) {
        a(new ly(ld.a(context).c()), (wh) new wg());
    }

    public synchronized void a(long j, @Nullable Long l) {
        this.f5285a = (j - this.c.a()) / 1000;
        if (this.b.c(true)) {
            boolean z = false;
            if (l != null) {
                long abs = Math.abs(j - this.c.a());
                ly lyVar = this.b;
                if (abs > TimeUnit.SECONDS.toMillis(l.longValue())) {
                    z = true;
                }
                lyVar.d(z);
            } else {
                this.b.d(false);
            }
        }
        this.b.a(this.f5285a);
        this.b.q();
    }

    public synchronized void c() {
        this.b.d(false);
        this.b.q();
    }

    public synchronized boolean d() {
        return this.b.c(true);
    }

    @VisibleForTesting
    public void a(ly lyVar, wh whVar) {
        this.b = lyVar;
        this.f5285a = this.b.c(0);
        this.c = whVar;
    }
}
