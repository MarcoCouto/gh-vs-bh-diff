package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import com.yandex.metrica.impl.ob.eg.a;
import com.yandex.metrica.impl.ob.et;
import com.yandex.metrica.impl.ob.ew;

public interface ft<C extends ew & et> {
    @NonNull
    eu c(@NonNull Context context, @NonNull ek ekVar, @NonNull a aVar, @NonNull bl blVar, @NonNull ul ulVar);

    @NonNull
    C d(@NonNull Context context, @NonNull ek ekVar, @NonNull a aVar, @NonNull bl blVar, @NonNull ul ulVar);
}
