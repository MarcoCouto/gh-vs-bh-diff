package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

class od {
    @Nullable

    /* renamed from: a reason: collision with root package name */
    private oh f5023a;
    @NonNull
    private final lh b;
    @NonNull
    private final lg c;

    public od(@Nullable oh ohVar, @NonNull lh lhVar, @NonNull lg lgVar) {
        this.f5023a = ohVar;
        this.b = lhVar;
        this.c = lgVar;
    }

    public void a() {
        if (this.f5023a != null) {
            b(this.f5023a);
            c(this.f5023a);
        }
    }

    private void b(@NonNull oh ohVar) {
        if (this.b.a() > ((long) ohVar.j)) {
            this.b.c((int) (((float) ohVar.j) * 0.1f));
        }
    }

    private void c(@NonNull oh ohVar) {
        if (this.c.a() > ((long) ohVar.j)) {
            this.c.c((int) (((float) ohVar.j) * 0.1f));
        }
    }

    public void a(@Nullable oh ohVar) {
        this.f5023a = ohVar;
    }
}
