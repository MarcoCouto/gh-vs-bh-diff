package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.util.SparseArray;

class lt {

    /* renamed from: a reason: collision with root package name */
    private static volatile SparseArray<qk> f4982a = new SparseArray<>();

    lt() {
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public synchronized String a(int i) {
        if (f4982a.get(i) == null) {
            SparseArray<qk> sparseArray = f4982a;
            StringBuilder sb = new StringBuilder();
            sb.append("EVENT_NUMBER_OF_TYPE_");
            sb.append(i);
            sparseArray.put(i, new qk(sb.toString()));
        }
        return ((qk) f4982a.get(i)).b();
    }
}
