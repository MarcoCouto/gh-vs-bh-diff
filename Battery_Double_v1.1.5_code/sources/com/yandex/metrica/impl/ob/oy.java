package com.yandex.metrica.impl.ob;

import android.location.Location;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.github.mikephil.charting.utils.Utils;
import com.ironsource.sdk.constants.LocationConst;
import com.yandex.metrica.impl.ob.oh.a;
import org.json.JSONObject;

class oy {
    @Nullable
    public static String a(@NonNull os osVar) {
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.putOpt("collection_mode", osVar.f5044a.toString());
            jSONObject.put("lat", osVar.c().getLatitude());
            jSONObject.put("lon", osVar.c().getLongitude());
            jSONObject.putOpt("timestamp", Long.valueOf(osVar.c().getTime()));
            jSONObject.putOpt("receive_timestamp", Long.valueOf(osVar.b()));
            jSONObject.put("receive_elapsed_realtime_seconds", osVar.d());
            jSONObject.putOpt("precision", osVar.c().hasAccuracy() ? Float.valueOf(osVar.c().getAccuracy()) : null);
            jSONObject.putOpt("direction", osVar.c().hasBearing() ? Float.valueOf(osVar.c().getBearing()) : null);
            jSONObject.putOpt(LocationConst.SPEED, osVar.c().hasSpeed() ? Float.valueOf(osVar.c().getSpeed()) : null);
            jSONObject.putOpt(LocationConst.ALTITUDE, osVar.c().hasAltitude() ? Double.valueOf(osVar.c().getAltitude()) : null);
            jSONObject.putOpt("provider", cu.c(osVar.c().getProvider(), null));
            return jSONObject.toString();
        } catch (Throwable unused) {
            return null;
        }
    }

    @Nullable
    public static os a(long j, @NonNull String str) {
        if (!TextUtils.isEmpty(str)) {
            try {
                JSONObject jSONObject = new JSONObject(str);
                long optLong = jSONObject.optLong("receive_timestamp", 0);
                long optLong2 = jSONObject.optLong("receive_elapsed_realtime_seconds", 0);
                a a2 = a.a(jSONObject.optString("collection_mode"));
                Location location = new Location(jSONObject.optString("provider", null));
                location.setLongitude(jSONObject.optDouble("lon", Utils.DOUBLE_EPSILON));
                location.setLatitude(jSONObject.optDouble("lat", Utils.DOUBLE_EPSILON));
                location.setTime(jSONObject.optLong("timestamp", 0));
                location.setAccuracy((float) jSONObject.optDouble("precision", Utils.DOUBLE_EPSILON));
                location.setBearing((float) jSONObject.optDouble("direction", Utils.DOUBLE_EPSILON));
                location.setSpeed((float) jSONObject.optDouble(LocationConst.SPEED, Utils.DOUBLE_EPSILON));
                location.setAltitude(jSONObject.optDouble(LocationConst.ALTITUDE, Utils.DOUBLE_EPSILON));
                os osVar = new os(a2, optLong, optLong2, location, Long.valueOf(j));
                return osVar;
            } catch (Throwable unused) {
            }
        }
        return null;
    }

    @Nullable
    public static on b(long j, @NonNull String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            on onVar = new on();
            try {
                onVar.a(Long.valueOf(j));
                JSONObject jSONObject = new JSONObject(str);
                onVar.a(jSONObject.optLong("timestamp", 0));
                onVar.b(jSONObject.optLong("elapsed_realtime_seconds", 0));
                onVar.b(jSONObject.optJSONArray("cell_info"));
                onVar.a(jSONObject.optJSONArray("wifi_info"));
            } catch (Throwable unused) {
            }
            return onVar;
        } catch (Throwable unused2) {
            return null;
        }
    }

    @Nullable
    public static String a(@NonNull on onVar) {
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("timestamp", onVar.b());
            jSONObject.put("elapsed_realtime_seconds", onVar.e());
            jSONObject.putOpt("wifi_info", onVar.c());
            jSONObject.putOpt("cell_info", onVar.d());
            return jSONObject.toString();
        } catch (Throwable unused) {
            return null;
        }
    }
}
