package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.os.RemoteException;
import android.os.ResultReceiver;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.util.Base64;
import android.util.Pair;
import com.yandex.metrica.IMetricaService;
import com.yandex.metrica.impl.ac.NativeCrashesHelper;
import com.yandex.metrica.impl.ob.af.a;
import com.yandex.metrica.impl.ob.ce.c;
import com.yandex.metrica.impl.ob.ce.d;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class cd implements ao {

    /* renamed from: a reason: collision with root package name */
    private final Context f4715a;
    private bb b;
    private final NativeCrashesHelper c;
    private ax d;
    @NonNull
    private final u e;
    private ug f;
    private final kt g;
    @NonNull
    private final kn h;
    private final ce i;

    cd(ee eeVar, Context context, xh xhVar) {
        this(eeVar, context, new NativeCrashesHelper(context), new bb(context, xhVar), new kt(), new kn());
    }

    @VisibleForTesting
    cd(ee eeVar, Context context, @NonNull NativeCrashesHelper nativeCrashesHelper, @NonNull bb bbVar, @NonNull kt ktVar, @NonNull kn knVar) {
        this.b = bbVar;
        this.f4715a = context;
        this.c = nativeCrashesHelper;
        this.e = new u(eeVar);
        this.g = ktVar;
        this.h = knVar;
        this.i = new ce(this);
    }

    /* access modifiers changed from: 0000 */
    public void a(@Nullable ax axVar) {
        this.d = axVar;
    }

    /* access modifiers changed from: 0000 */
    public void a(ug ugVar) {
        this.f = ugVar;
        this.e.b(ugVar);
    }

    /* access modifiers changed from: 0000 */
    public void a(boolean z, bz bzVar) {
        this.c.a(z);
    }

    /* access modifiers changed from: 0000 */
    public void a(@Nullable Boolean bool, @Nullable Boolean bool2) {
        if (cx.a((Object) bool)) {
            this.e.h().a(bool.booleanValue());
        }
        if (cx.a((Object) bool2)) {
            this.e.h().g(bool2.booleanValue());
        }
        a(w.t(), (bz) this.e);
    }

    /* access modifiers changed from: 0000 */
    public void a(String str, bz bzVar) {
        a(af.a(a.EVENT_TYPE_PREV_SESSION_NATIVE_CRASH, str, c(bzVar)), bzVar);
    }

    public void c() {
        this.b.g();
    }

    public void d() {
        this.b.h();
    }

    /* access modifiers changed from: private */
    public w b(w wVar, bz bzVar) {
        if (wVar.g() == a.EVENT_TYPE_EXCEPTION_USER.a() || wVar.g() == a.EVENT_TYPE_EXCEPTION_USER_PROTOBUF.a()) {
            wVar.e(bzVar.e());
        }
        return wVar;
    }

    /* access modifiers changed from: 0000 */
    public void a(w wVar, bz bzVar) {
        a(b(wVar, bzVar), bzVar, null);
    }

    public Future<Void> a(w wVar, final bz bzVar, final Map<String, Object> map) {
        this.b.d();
        d dVar = new d(wVar, bzVar);
        if (!cx.a((Map) map)) {
            dVar.a((c) new c() {
                public w a(w wVar) {
                    return cd.this.b(wVar.c(vq.b(map)), bzVar);
                }
            });
        }
        return a(dVar);
    }

    public void a(@NonNull List<String> list, @NonNull ResultReceiver resultReceiver, @Nullable Map<String, String> map) {
        a(af.a(a.EVENT_TYPE_STARTUP, vr.a()).a(new ap(list, map, resultReceiver)), (bz) this.e);
    }

    public void a(String str) {
        a(af.f(str, vr.a()), (bz) this.e);
    }

    public void a(@Nullable sj sjVar) {
        a(af.a(sjVar, vr.a()), (bz) this.e);
    }

    public void a(bz bzVar) {
        a(af.a(bzVar.f(), c(bzVar)), bzVar);
    }

    public void a(List<String> list) {
        this.e.g().a(list);
    }

    public void a(Map<String, String> map) {
        this.e.g().a(map);
    }

    public void b(String str) {
        this.e.g().a(str);
    }

    /* access modifiers changed from: 0000 */
    public void a(@NonNull kh khVar, @NonNull bz bzVar) {
        a(af.a(e.a((e) this.h.b(khVar)), c(bzVar)), bzVar);
    }

    /* access modifiers changed from: 0000 */
    public void a(@NonNull String str, @NonNull kl klVar, @NonNull bz bzVar) {
        a(af.a(str, e.a((e) this.g.b(klVar)), c(bzVar)), bzVar);
    }

    /* access modifiers changed from: 0000 */
    public void a(@NonNull kl klVar, bz bzVar) {
        this.b.d();
        r b2 = af.b(klVar.f4942a, e.a((e) this.g.b(klVar)), c(bzVar));
        b2.e(bzVar.e());
        try {
            a(new d(b2, bzVar).a(b2.a()).a(true)).get();
        } catch (InterruptedException | ExecutionException unused) {
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(n nVar) {
        this.b.d();
    }

    /* access modifiers changed from: 0000 */
    public void b(n nVar) {
        this.b.c();
    }

    public void a(IMetricaService iMetricaService, w wVar, bz bzVar) throws RemoteException {
        b(iMetricaService, wVar, bzVar);
        e();
    }

    public void a(String str, String str2, bz bzVar) {
        a(new d(r.a(str, str2), bzVar));
    }

    public void b(bz bzVar) {
        a(new d(r.b(), bzVar));
    }

    /* access modifiers changed from: 0000 */
    public void a(@NonNull final rl.a aVar, @NonNull bz bzVar) {
        a(new d(r.c(), bzVar).a((c) new c() {
            public w a(w wVar) {
                return wVar.c(new String(Base64.encode(e.a((e) aVar), 0)));
            }
        }));
    }

    /* access modifiers changed from: 0000 */
    public void b(@Nullable final String str, @NonNull bz bzVar) {
        a(new d(r.a(str, c(bzVar)), bzVar).a((c) new c() {
            public w a(w wVar) {
                return wVar.c(str);
            }
        }));
    }

    /* access modifiers changed from: 0000 */
    public void a(@NonNull final cg cgVar, @NonNull bz bzVar) {
        a(new d(r.a(c(bzVar)), bzVar).a((c) new c() {
            public w a(w wVar) {
                Pair a2 = cgVar.a();
                return wVar.c(new String(Base64.encode((byte[]) a2.first, 0))).c(((Integer) a2.second).intValue());
            }
        }));
    }

    private void e() {
        if (this.d == null || this.d.f()) {
            this.b.c();
        }
    }

    private static void b(IMetricaService iMetricaService, w wVar, bz bzVar) throws RemoteException {
        iMetricaService.a(wVar.a(bzVar.b()));
    }

    private Future<Void> a(d dVar) {
        dVar.a().a(this.f);
        return this.i.a(dVar);
    }

    public bb a() {
        return this.b;
    }

    public Context b() {
        return this.f4715a;
    }

    @NonNull
    private vz c(@NonNull bz bzVar) {
        return vr.a(bzVar.h().e());
    }
}
