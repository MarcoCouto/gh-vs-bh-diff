package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import java.util.List;

public class th {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    public final String f5189a;
    @NonNull
    public final List<String> b;

    public th(@NonNull String str, @NonNull List<String> list) {
        this.f5189a = str;
        this.b = list;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("SdkItem{name='");
        sb.append(this.f5189a);
        sb.append('\'');
        sb.append(", classes=");
        sb.append(this.b);
        sb.append('}');
        return sb.toString();
    }
}
