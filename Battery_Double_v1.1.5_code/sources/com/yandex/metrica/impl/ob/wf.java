package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import java.util.concurrent.TimeUnit;

public class wf {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final wg f5287a;

    public wf() {
        this(new wg());
    }

    @VisibleForTesting
    public wf(@NonNull wg wgVar) {
        this.f5287a = wgVar;
    }

    public long a(long j, @NonNull TimeUnit timeUnit) {
        return TimeUnit.MILLISECONDS.toSeconds(b(j, timeUnit));
    }

    public long b(long j, @NonNull TimeUnit timeUnit) {
        return this.f5287a.c() - timeUnit.toMillis(j);
    }

    public long c(long j, @NonNull TimeUnit timeUnit) {
        return this.f5287a.d() - timeUnit.toNanos(j);
    }

    public long d(long j, @NonNull TimeUnit timeUnit) {
        return TimeUnit.NANOSECONDS.toSeconds(c(j, timeUnit));
    }

    public long e(long j, @NonNull TimeUnit timeUnit) {
        return this.f5287a.b() - timeUnit.toSeconds(j);
    }
}
