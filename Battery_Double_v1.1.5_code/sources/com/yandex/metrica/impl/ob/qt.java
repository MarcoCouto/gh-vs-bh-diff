package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;

public final class qt {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final yk<String> f5080a;
    @NonNull
    private final qn b;
    @NonNull
    private final String c;

    public qt(@NonNull String str, @NonNull yk<String> ykVar, @NonNull qn qnVar) {
        this.c = str;
        this.f5080a = ykVar;
        this.b = qnVar;
    }

    @NonNull
    public String a() {
        return this.c;
    }

    @NonNull
    public qn b() {
        return this.b;
    }

    @NonNull
    public yk<String> c() {
        return this.f5080a;
    }
}
