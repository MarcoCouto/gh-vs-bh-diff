package com.yandex.metrica.impl.ob;

import android.annotation.TargetApi;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

@TargetApi(21)
public class dd {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Context f4775a;

    public dd(@NonNull Context context) {
        this.f4775a = context;
    }

    @Nullable
    public BluetoothLeScanner a() {
        BluetoothManager bluetoothManager = (BluetoothManager) this.f4775a.getSystemService("bluetooth");
        if (bluetoothManager == null) {
            return null;
        }
        BluetoothAdapter bluetoothAdapter = (BluetoothAdapter) cx.a((wo<T, S>) new wo<BluetoothManager, BluetoothAdapter>() {
            public BluetoothAdapter a(BluetoothManager bluetoothManager) throws Throwable {
                return bluetoothManager.getAdapter();
            }
        }, bluetoothManager, "getting adapter", "BluetoothManager");
        if (bluetoothAdapter != null && bluetoothAdapter.isEnabled()) {
            return (BluetoothLeScanner) cx.a((wo<T, S>) new wo<BluetoothAdapter, BluetoothLeScanner>() {
                public BluetoothLeScanner a(BluetoothAdapter bluetoothAdapter) throws Throwable {
                    return bluetoothAdapter.getBluetoothLeScanner();
                }
            }, bluetoothAdapter, "Get bluetooth LE scanner", "BluetoothAdapter");
        }
        return null;
    }
}
