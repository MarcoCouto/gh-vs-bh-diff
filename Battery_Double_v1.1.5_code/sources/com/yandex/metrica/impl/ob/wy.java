package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import com.yandex.metrica.impl.ob.af.a;

public class wy {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final vw<wz, wx> f5296a;
    @NonNull
    private final vw<a, wx> b;

    public wy(@NonNull Context context) {
        this(new wv(), new xa(), new ws(context));
    }

    public wy(@NonNull wx wxVar, @NonNull wx wxVar2, @NonNull wx wxVar3) {
        this.f5296a = new vw<>(wxVar);
        this.f5296a.a(wz.NONE, wxVar);
        this.f5296a.a(wz.EXTERNALLY_ENCRYPTED_EVENT_CRYPTER, wxVar2);
        this.f5296a.a(wz.AES_VALUE_ENCRYPTION, wxVar3);
        this.b = new vw<>(wxVar);
        this.b.a(a.EVENT_TYPE_IDENTITY, wxVar3);
    }

    @NonNull
    public wx a(wz wzVar) {
        return (wx) this.f5296a.a(wzVar);
    }

    @NonNull
    public wx a(@NonNull w wVar) {
        return a(a.a(wVar.g()));
    }

    @NonNull
    private wx a(@NonNull a aVar) {
        return (wx) this.b.a(aVar);
    }
}
