package com.yandex.metrica.impl.ob;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;
import com.yandex.metrica.impl.ob.rr.a.b.C0126b;
import com.yandex.metrica.impl.ob.tt.b;
import com.yandex.metrica.impl.ob.tt.b.C0131b;
import com.yandex.metrica.impl.ob.tt.b.a;
import com.yandex.metrica.impl.ob.tt.b.c;
import com.yandex.metrica.impl.ob.tt.b.d;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@SuppressLint({"UseSparseArrays"})
public class nl implements mv<b, C0126b> {

    /* renamed from: a reason: collision with root package name */
    private static final Map<Integer, a> f5005a;
    private static final Map<a, Integer> b;
    private static final Map<Integer, C0131b> c;
    private static final Map<C0131b, Integer> d;
    private static final Map<Integer, c> e;
    private static final Map<c, Integer> f;
    private static final Map<Integer, d> g;
    private static final Map<d, Integer> h;

    static {
        HashMap hashMap = new HashMap();
        hashMap.put(Integer.valueOf(1), a.ALL_MATCHES);
        hashMap.put(Integer.valueOf(2), a.FIRST_MATCH);
        hashMap.put(Integer.valueOf(3), a.MATCH_LOST);
        f5005a = Collections.unmodifiableMap(hashMap);
        HashMap hashMap2 = new HashMap();
        hashMap2.put(a.ALL_MATCHES, Integer.valueOf(1));
        hashMap2.put(a.FIRST_MATCH, Integer.valueOf(2));
        hashMap2.put(a.MATCH_LOST, Integer.valueOf(3));
        b = Collections.unmodifiableMap(hashMap2);
        HashMap hashMap3 = new HashMap();
        hashMap3.put(Integer.valueOf(1), C0131b.AGGRESSIVE);
        hashMap3.put(Integer.valueOf(2), C0131b.STICKY);
        c = Collections.unmodifiableMap(hashMap3);
        HashMap hashMap4 = new HashMap();
        hashMap4.put(C0131b.AGGRESSIVE, Integer.valueOf(1));
        hashMap4.put(C0131b.STICKY, Integer.valueOf(2));
        d = Collections.unmodifiableMap(hashMap4);
        HashMap hashMap5 = new HashMap();
        hashMap5.put(Integer.valueOf(1), c.ONE_AD);
        hashMap5.put(Integer.valueOf(2), c.FEW_AD);
        hashMap5.put(Integer.valueOf(3), c.MAX_AD);
        e = Collections.unmodifiableMap(hashMap5);
        HashMap hashMap6 = new HashMap();
        hashMap6.put(c.ONE_AD, Integer.valueOf(1));
        hashMap6.put(c.FEW_AD, Integer.valueOf(2));
        hashMap6.put(c.MAX_AD, Integer.valueOf(3));
        f = Collections.unmodifiableMap(hashMap6);
        HashMap hashMap7 = new HashMap();
        hashMap7.put(Integer.valueOf(1), d.LOW_POWER);
        hashMap7.put(Integer.valueOf(2), d.BALANCED);
        hashMap7.put(Integer.valueOf(3), d.LOW_LATENCY);
        g = Collections.unmodifiableMap(hashMap7);
        HashMap hashMap8 = new HashMap();
        hashMap8.put(d.LOW_POWER, Integer.valueOf(1));
        hashMap8.put(d.BALANCED, Integer.valueOf(2));
        hashMap8.put(d.LOW_LATENCY, Integer.valueOf(3));
        h = Collections.unmodifiableMap(hashMap8);
    }

    @NonNull
    /* renamed from: a */
    public C0126b b(@NonNull b bVar) {
        C0126b bVar2 = new C0126b();
        bVar2.b = ((Integer) b.get(bVar.f5214a)).intValue();
        bVar2.c = ((Integer) d.get(bVar.b)).intValue();
        bVar2.d = ((Integer) f.get(bVar.c)).intValue();
        bVar2.e = ((Integer) h.get(bVar.d)).intValue();
        bVar2.f = bVar.e;
        return bVar2;
    }

    @NonNull
    public b a(@NonNull C0126b bVar) {
        b bVar2 = new b((a) f5005a.get(Integer.valueOf(bVar.b)), (C0131b) c.get(Integer.valueOf(bVar.c)), (c) e.get(Integer.valueOf(bVar.d)), (d) g.get(Integer.valueOf(bVar.e)), bVar.f);
        return bVar2;
    }
}
