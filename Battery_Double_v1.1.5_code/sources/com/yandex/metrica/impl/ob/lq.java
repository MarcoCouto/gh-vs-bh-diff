package com.yandex.metrica.impl.ob;

import android.content.ContentValues;
import android.util.SparseArray;
import com.ironsource.sdk.constants.LocationConst;
import com.tapjoy.TapjoyConstants;
import com.yandex.metrica.YandexMetrica;
import java.util.List;
import java.util.Locale;

public final class lq {

    /* renamed from: a reason: collision with root package name */
    public static final Boolean f4973a = Boolean.valueOf(false);
    public static final int b = YandexMetrica.getLibraryApiLevel();
    public static final SparseArray<lr> c = e.a();
    public static final SparseArray<lr> d = e.b();
    private static final lb e = new lb();
    private static final le f = new le();
    private static final la g = new la(e, f);

    public interface a {

        /* renamed from: a reason: collision with root package name */
        public static final List<String> f4974a = cx.a("incremental_id", "timestamp", "data");

        /* renamed from: com.yandex.metrica.impl.ob.lq$a$a reason: collision with other inner class name */
        public interface C0109a {

            /* renamed from: a reason: collision with root package name */
            public static final String f4975a = String.format(Locale.US, "CREATE TABLE IF NOT EXISTS %s (incremental_id INTEGER NOT NULL,timestamp INTEGER, data TEXT)", new Object[]{"lbs_dat"});
            public static final String b = String.format(Locale.US, "DROP TABLE IF EXISTS %s", new Object[]{"lbs_dat"});
        }

        public interface b {

            /* renamed from: a reason: collision with root package name */
            public static final String f4976a = String.format(Locale.US, "CREATE TABLE IF NOT EXISTS %s (incremental_id INTEGER NOT NULL,timestamp INTEGER, data TEXT)", new Object[]{"l_dat"});
            public static final String b = String.format(Locale.US, "DROP TABLE IF EXISTS %s", new Object[]{"l_dat"});
        }
    }

    public static final class b {

        /* renamed from: a reason: collision with root package name */
        public static final List<String> f4977a = cx.a("data_key", "value");
    }

    public interface c {

        /* renamed from: a reason: collision with root package name */
        public static final List<String> f4978a = cx.a("key", "value", "type");
    }

    public static final class d implements c {
    }

    public static final class e {
        public static ContentValues a() {
            ContentValues contentValues = new ContentValues();
            contentValues.put("API_LEVEL", Integer.valueOf(YandexMetrica.getLibraryApiLevel()));
            return contentValues;
        }
    }

    public static final class f {

        /* renamed from: a reason: collision with root package name */
        public static final List<String> f4979a = cx.a("id", "number", "global_number", "number_of_type", "name", "value", "type", LocationConst.TIME, "session_id", "wifi_network_info", "cell_info", "location_info", "error_environment", "user_info", "session_type", "app_environment", "app_environment_revision", "truncated", TapjoyConstants.TJC_CONNECTION_TYPE, "cellular_connection_type", "custom_type", "wifi_access_point", "encrypting_mode", "profile_id", "first_occurrence_status");
        public static final String b;

        static {
            StringBuilder sb = new StringBuilder();
            sb.append("CREATE TABLE IF NOT EXISTS reports (id INTEGER PRIMARY KEY,name TEXT,value TEXT,number INTEGER,global_number INTEGER,number_of_type INTEGER,type INTEGER,time INTEGER,session_id TEXT,wifi_network_info TEXT DEFAULT '',cell_info TEXT DEFAULT '',location_info TEXT DEFAULT '',error_environment TEXT,user_info TEXT,session_type INTEGER DEFAULT ");
            sb.append(jh.FOREGROUND.a());
            sb.append(",");
            sb.append("app_environment");
            sb.append(" TEXT DEFAULT '");
            sb.append("{}");
            sb.append("',");
            sb.append("app_environment_revision");
            sb.append(" INTEGER DEFAULT ");
            sb.append(0);
            sb.append(",");
            sb.append("truncated");
            sb.append(" INTEGER DEFAULT 0,");
            sb.append(TapjoyConstants.TJC_CONNECTION_TYPE);
            sb.append(" INTEGER DEFAULT ");
            sb.append(2);
            sb.append(",");
            sb.append("cellular_connection_type");
            sb.append(" TEXT,");
            sb.append("custom_type");
            sb.append(" INTEGER DEFAULT 0, ");
            sb.append("wifi_access_point");
            sb.append(" TEXT, ");
            sb.append("encrypting_mode");
            sb.append(" INTEGER DEFAULT ");
            sb.append(wz.NONE.a());
            sb.append(", ");
            sb.append("profile_id");
            sb.append(" TEXT, ");
            sb.append("first_occurrence_status");
            sb.append(" INTEGER DEFAULT ");
            sb.append(aj.UNKNOWN.d);
            sb.append(" )");
            b = sb.toString();
        }
    }

    public static final class g {

        /* renamed from: a reason: collision with root package name */
        public static final List<String> f4980a = cx.a("id", "start_time", "network_info", "report_request_parameters", "server_time_offset", "type", "obtained_before_first_sync");
        public static final String b;
        public static final String c = String.format(Locale.US, "SELECT DISTINCT %s  FROM %s WHERE %s >=0 AND (SELECT count() FROM %5$s WHERE %5$s.%6$s = %2$s.%3$s AND %5$s.%7$s = %2$s.%4$s) > 0 ORDER BY %3$s LIMIT 1", new Object[]{"report_request_parameters", "sessions", "id", "type", "reports", "session_id", "session_type"});
        public static final String d = String.format(Locale.US, "(select count(%s.%s) from %s where %s.%s = %s.%s) = 0 and cast(%s as integer) < ?", new Object[]{"reports", "id", "reports", "reports", "session_id", "sessions", "id", "id"});

        static {
            StringBuilder sb = new StringBuilder();
            sb.append("CREATE TABLE IF NOT EXISTS sessions (id INTEGER,start_time INTEGER,network_info TEXT,report_request_parameters TEXT,server_time_offset INTEGER,type INTEGER DEFAULT ");
            sb.append(jh.FOREGROUND.a());
            sb.append(",");
            sb.append("obtained_before_first_sync");
            sb.append(" INTEGER DEFAULT 0 )");
            b = sb.toString();
        }
    }

    public static final class h implements c {
    }

    public static la a() {
        return g;
    }
}
