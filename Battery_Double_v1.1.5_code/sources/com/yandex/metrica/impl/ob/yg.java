package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public class yg<T> implements yk<T> {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final yk<T> f5317a;

    public yg(@NonNull yk<T> ykVar) {
        this.f5317a = ykVar;
    }

    public yi a(@Nullable T t) {
        yi a2 = this.f5317a.a(t);
        if (a2.a()) {
            return a2;
        }
        throw new yh(a2.b());
    }
}
