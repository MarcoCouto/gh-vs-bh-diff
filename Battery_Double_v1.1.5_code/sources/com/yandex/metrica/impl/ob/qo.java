package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.metrica.impl.ob.rl.a.C0115a;

public abstract class qo {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final qn f5078a;

    public abstract C0115a a(@NonNull re reVar, @Nullable C0115a aVar, @NonNull qm qmVar);

    qo(@NonNull qn qnVar) {
        this.f5078a = qnVar;
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public qn a() {
        return this.f5078a;
    }

    /* access modifiers changed from: 0000 */
    public final boolean a(@Nullable C0115a aVar) {
        return aVar == null || aVar.d.c;
    }
}
