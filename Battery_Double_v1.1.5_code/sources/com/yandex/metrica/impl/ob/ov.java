package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;

class ov extends of {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private lh f5047a;
    @NonNull
    private og b;
    @NonNull
    private wg c;

    public ov(@NonNull Context context, @Nullable oe oeVar) {
        this(oeVar, ld.a(context).g(), new og(context), new wg());
    }

    public void b(@Nullable String str, @Nullable Location location, @Nullable oh ohVar) {
        if (ohVar != null && location != null) {
            os osVar = new os(ohVar.a(), this.c.a(), this.c.c(), location);
            String a2 = this.b.a(osVar);
            if (!TextUtils.isEmpty(a2)) {
                this.f5047a.b(osVar.b(), a2);
            }
        }
    }

    ov(@Nullable oe oeVar, @NonNull lh lhVar, @NonNull og ogVar, @NonNull wg wgVar) {
        super(oeVar);
        this.f5047a = lhVar;
        this.b = ogVar;
        this.c = wgVar;
    }
}
