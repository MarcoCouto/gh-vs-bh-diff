package com.yandex.metrica.impl.ob;

import android.annotation.TargetApi;
import android.content.pm.FeatureInfo;
import android.support.annotation.NonNull;

public abstract class oa {

    public static class a {
        public static oa a() {
            if (cx.a(24)) {
                return new b();
            }
            return new c();
        }
    }

    public static class b extends oa {
        @TargetApi(24)
        public ob a(@NonNull FeatureInfo featureInfo) {
            return new ob(featureInfo.name, featureInfo.version, c(featureInfo));
        }
    }

    public static class c extends oa {
        public ob a(@NonNull FeatureInfo featureInfo) {
            return new ob(featureInfo.name, c(featureInfo));
        }
    }

    /* access modifiers changed from: protected */
    public abstract ob a(FeatureInfo featureInfo);

    public ob b(@NonNull FeatureInfo featureInfo) {
        if (featureInfo.name != null) {
            return a(featureInfo);
        }
        if (featureInfo.reqGlEsVersion == 0) {
            return a(featureInfo);
        }
        return new ob("openGlFeature", featureInfo.reqGlEsVersion, c(featureInfo));
    }

    /* access modifiers changed from: 0000 */
    public boolean c(FeatureInfo featureInfo) {
        return (featureInfo.flags & 1) != 0;
    }
}
