package com.yandex.metrica.impl.ob;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.os.ResultReceiver;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.yandex.metrica.IParamsCallback;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ap implements Parcelable {
    public static final Creator<ap> CREATOR = new Creator<ap>() {
        /* renamed from: a */
        public ap createFromParcel(Parcel parcel) {
            return new ap(parcel);
        }

        /* renamed from: a */
        public ap[] newArray(int i) {
            return new ap[i];
        }
    };
    @Nullable

    /* renamed from: a reason: collision with root package name */
    private ResultReceiver f4645a;
    @Nullable
    private List<String> b;
    @NonNull
    private Map<String, String> c;

    public int describeContents() {
        return 0;
    }

    public ap(@Nullable List<String> list, @Nullable Map<String, String> map, @Nullable ResultReceiver resultReceiver) {
        this.b = list;
        this.f4645a = resultReceiver;
        this.c = map == null ? new HashMap() : new HashMap(map);
    }

    public boolean a(@Nullable uk ukVar) {
        if (cx.a((Collection) this.b)) {
            return true;
        }
        if (ukVar == null) {
            return false;
        }
        boolean z = true;
        for (String str : this.b) {
            if ("yandex_mobile_metrica_device_id".equals(str)) {
                z &= !TextUtils.isEmpty(ukVar.b);
            } else if ("yandex_mobile_metrica_uuid".equals(str)) {
                z &= !TextUtils.isEmpty(ukVar.f5236a);
            } else if ("appmetrica_device_id_hash".equals(str)) {
                z &= !TextUtils.isEmpty(ukVar.d);
            } else if ("yandex_mobile_metrica_report_ad_url".equals(str)) {
                z &= !TextUtils.isEmpty(ukVar.g);
            } else if ("yandex_mobile_metrica_get_ad_url".equals(str)) {
                z &= !TextUtils.isEmpty(ukVar.f);
            } else if (IParamsCallback.YANDEX_MOBILE_METRICA_CLIDS.equals(str)) {
                z &= this.c.equals(we.a(ukVar.n));
            }
        }
        return z;
    }

    @Nullable
    public List<String> a() {
        return this.b;
    }

    @NonNull
    public Map<String, String> b() {
        return this.c;
    }

    @Nullable
    public ResultReceiver c() {
        return this.f4645a;
    }

    protected ap(Parcel parcel) {
        Bundle readBundle = parcel.readBundle(x.class.getClassLoader());
        if (readBundle != null) {
            this.f4645a = (ResultReceiver) readBundle.getParcelable("com.yandex.metrica.CounterConfiguration.receiver");
            this.b = readBundle.getStringArrayList("com.yandex.metrica.CounterConfiguration.identifiersList");
            this.c = we.a(readBundle.getString("com.yandex.metrica.CounterConfiguration.clidsForVerification"));
            return;
        }
        this.c = new HashMap();
    }

    public void writeToParcel(Parcel parcel, int i) {
        Bundle bundle = new Bundle();
        bundle.putParcelable("com.yandex.metrica.CounterConfiguration.receiver", this.f4645a);
        if (this.b != null) {
            bundle.putStringArrayList("com.yandex.metrica.CounterConfiguration.identifiersList", new ArrayList(this.b));
        }
        if (this.c != null) {
            bundle.putString("com.yandex.metrica.CounterConfiguration.clidsForVerification", we.a(this.c));
        }
        parcel.writeBundle(bundle);
    }
}
