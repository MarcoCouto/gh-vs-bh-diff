package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import com.yandex.metrica.impl.ob.rg.b;

public class kn implements mq<kh, b> {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final km f4944a;
    @NonNull
    private final ko b;

    public kn() {
        this(new km(new kr()), new ko());
    }

    @VisibleForTesting
    kn(@NonNull km kmVar, @NonNull ko koVar) {
        this.f4944a = kmVar;
        this.b = koVar;
    }

    @NonNull
    /* renamed from: a */
    public b b(@NonNull kh khVar) {
        b bVar = new b();
        bVar.b = this.f4944a.b(khVar.f4938a);
        if (khVar.b != null) {
            bVar.c = khVar.b;
        }
        bVar.d = this.b.a(khVar.c).intValue();
        return bVar;
    }

    @NonNull
    public kh a(@NonNull b bVar) {
        throw new UnsupportedOperationException();
    }
}
