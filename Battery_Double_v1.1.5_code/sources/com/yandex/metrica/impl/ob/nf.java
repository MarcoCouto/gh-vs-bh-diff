package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import com.yandex.metrica.impl.ob.rr.a;
import com.yandex.metrica.impl.ob.rr.a.h;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class nf implements mv<uk, a> {

    /* renamed from: a reason: collision with root package name */
    private ms f5001a = new ms();
    private mn b = new mn();
    private ne c = new ne();
    private mz d = new mz();
    private nb e = new nb();
    private mr f = new mr();
    private mx g = new mx();
    private ng h = new ng();
    private nc i = new nc();
    private mt j = new mt();
    private nh k = new nh();

    @NonNull
    /* renamed from: a */
    public a b(@NonNull uk ukVar) {
        a aVar = new a();
        aVar.C = ukVar.D;
        aVar.D = ukVar.E;
        if (ukVar.f5236a != null) {
            aVar.b = ukVar.f5236a;
        }
        if (ukVar.b != null) {
            aVar.y = ukVar.b;
        }
        if (ukVar.c != null) {
            aVar.A = ukVar.c;
        }
        if (ukVar.d != null) {
            aVar.z = ukVar.d;
        }
        if (ukVar.j != null) {
            aVar.h = (String[]) ukVar.j.toArray(new String[ukVar.j.size()]);
        }
        if (ukVar.k != null) {
            aVar.i = (String[]) ukVar.k.toArray(new String[ukVar.k.size()]);
        }
        if (ukVar.e != null) {
            aVar.d = (String[]) ukVar.e.toArray(new String[ukVar.e.size()]);
        }
        if (ukVar.i != null) {
            aVar.g = (String[]) ukVar.i.toArray(new String[ukVar.i.size()]);
        }
        if (ukVar.l != null) {
            aVar.t = (String[]) ukVar.l.toArray(new String[ukVar.l.size()]);
        }
        if (ukVar.p != null) {
            aVar.k = this.f5001a.b(ukVar.p);
        }
        if (ukVar.q != null) {
            aVar.l = this.b.b(ukVar.q);
        }
        if (ukVar.r != null) {
            aVar.m = this.c.b(ukVar.r);
        }
        if (ukVar.z != null) {
            aVar.F = this.d.b(ukVar.z);
        }
        if (ukVar.m != null) {
            aVar.o = ukVar.m;
        }
        if (ukVar.f != null) {
            aVar.e = ukVar.f;
        }
        if (ukVar.g != null) {
            aVar.f = ukVar.g;
        }
        if (ukVar.h != null) {
            aVar.B = ukVar.h;
        }
        if (ukVar.s != null) {
            aVar.r = ukVar.s;
        }
        aVar.j = this.f.b(ukVar.o);
        if (ukVar.n != null) {
            aVar.p = ukVar.n;
        }
        aVar.q = ukVar.v;
        aVar.c = ukVar.t;
        aVar.v = ukVar.u;
        if (ukVar.w != null) {
            aVar.n = a(ukVar.w);
        }
        if (ukVar.x != null) {
            aVar.s = ukVar.x;
        }
        if (ukVar.A != null) {
            aVar.w = this.g.b(ukVar.A);
        }
        if (ukVar.B != null) {
            aVar.x = this.i.b(ukVar.B);
        }
        if (ukVar.y != null) {
            aVar.u = this.h.b(ukVar.y);
        }
        aVar.E = ukVar.F;
        if (ukVar.C != null) {
            aVar.G = this.j.b(ukVar.C);
        }
        if (ukVar.G != null) {
            aVar.H = this.k.b(ukVar.G);
        }
        return aVar;
    }

    @NonNull
    public uk a(@NonNull a aVar) {
        uk.a c2 = new uk.a(this.f.a(aVar.j)).a(aVar.b).b(aVar.y).c(aVar.A).d(aVar.z).h(aVar.o).e(aVar.e).a(Arrays.asList(aVar.d)).b(Arrays.asList(aVar.g)).d(Arrays.asList(aVar.i)).c(Arrays.asList(aVar.h)).f(aVar.f).g(aVar.B).e(Arrays.asList(aVar.t)).j(aVar.r).i(aVar.p).b(aVar.q).a(aVar.c).a(aVar.v).f(a(aVar.n)).b(aVar.C).c(aVar.D).k(aVar.s).c(aVar.E);
        if (aVar.k != null) {
            c2.a(this.f5001a.a(aVar.k));
        }
        if (aVar.l != null) {
            c2.a(this.b.a(aVar.l));
        }
        if (aVar.m != null) {
            c2.a(this.c.a(aVar.m));
        }
        if (aVar.F != null) {
            c2.a(this.d.a(aVar.F));
        }
        if (aVar.w != null) {
            c2.g(this.g.a(aVar.w));
        }
        if (aVar.x != null) {
            c2.a(this.i.a(aVar.x));
        }
        if (aVar.u != null) {
            c2.a(this.h.a(aVar.u));
        }
        if (aVar.G != null) {
            c2.a(this.j.a(aVar.G));
        }
        if (aVar.H != null) {
            c2.a(this.k.a(aVar.H));
        }
        return c2.a();
    }

    @NonNull
    private h[] a(@NonNull List<cq.a> list) {
        h[] hVarArr = new h[list.size()];
        int i2 = 0;
        for (cq.a a2 : list) {
            hVarArr[i2] = this.e.b(a2);
            i2++;
        }
        return hVarArr;
    }

    @NonNull
    private List<cq.a> a(@NonNull h[] hVarArr) {
        ArrayList arrayList = new ArrayList(hVarArr.length);
        for (h a2 : hVarArr) {
            arrayList.add(this.e.a(a2));
        }
        return arrayList;
    }
}
