package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import com.yandex.metrica.impl.ob.rr.a.C0121a.C0122a;
import java.util.ArrayList;
import java.util.List;

public class mu implements mw<oj, C0122a> {
    @NonNull
    /* renamed from: a */
    public C0122a[] b(@NonNull List<oj> list) {
        C0122a[] aVarArr = new C0122a[list.size()];
        for (int i = 0; i < list.size(); i++) {
            aVarArr[i] = a((oj) list.get(i));
        }
        return aVarArr;
    }

    @NonNull
    public List<oj> a(@NonNull C0122a[] aVarArr) {
        ArrayList arrayList = new ArrayList();
        for (C0122a a2 : aVarArr) {
            arrayList.add(a(a2));
        }
        return arrayList;
    }

    @NonNull
    private C0122a a(@NonNull oj ojVar) {
        C0122a aVar = new C0122a();
        aVar.b = ojVar.f5029a;
        aVar.c = ojVar.b;
        return aVar;
    }

    @NonNull
    private oj a(@NonNull C0122a aVar) {
        return new oj(aVar.b, aVar.c);
    }
}
