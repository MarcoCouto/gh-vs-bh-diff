package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import com.yandex.metrica.impl.ob.en;
import java.util.concurrent.TimeUnit;

public class cc<C extends en> extends cv<C> {
    private Runnable d = new Runnable() {
        public void run() {
            cc.this.e();
        }
    };
    private final xh e;

    public cc(@NonNull C c, @NonNull uq uqVar, @NonNull bl blVar, @NonNull xh xhVar) {
        super(c, uqVar, blVar);
        this.e = xhVar;
    }

    /* access modifiers changed from: 0000 */
    public void a() {
        this.e.b(this.d);
    }

    public void b() {
        synchronized (this.f4754a) {
            if (!this.c) {
                a();
                d();
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void c() {
        super.c();
        st i = ((en) g()).i();
        if (i.X() && cx.a(i.c())) {
            try {
                this.b.a((bo) by.J().a((en) g()));
            } catch (Throwable unused) {
            }
        }
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public void d() {
        if (((en) g()).i().T() > 0) {
            this.e.a(this.d, TimeUnit.SECONDS.toMillis((long) ((en) g()).i().T()));
        }
    }
}
