package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import android.util.Pair;
import com.yandex.metrica.impl.ob.cq.a.C0108a;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public class rs {

    /* renamed from: a reason: collision with root package name */
    private static final Map<C0108a, com.yandex.metrica.impl.ob.bt.a> f5087a = Collections.unmodifiableMap(new HashMap<C0108a, com.yandex.metrica.impl.ob.bt.a>() {
        {
            put(C0108a.CELL, com.yandex.metrica.impl.ob.bt.a.CELL);
            put(C0108a.WIFI, com.yandex.metrica.impl.ob.bt.a.WIFI);
        }
    });
    /* access modifiers changed from: private */
    @NonNull
    public final Context b;
    @NonNull
    private final mf<a> c;
    @NonNull
    private final xh d;
    /* access modifiers changed from: private */
    @NonNull
    public final tj e;
    /* access modifiers changed from: private */
    @NonNull
    public final cs f;
    /* access modifiers changed from: private */
    @NonNull
    public final vn g;
    private a h;
    private boolean i;

    public static class a {
        @NonNull

        /* renamed from: a reason: collision with root package name */
        private final List<C0128a> f5091a;
        @NonNull
        private final LinkedHashMap<String, Object> b = new LinkedHashMap<>();

        /* renamed from: com.yandex.metrica.impl.ob.rs$a$a reason: collision with other inner class name */
        public static class C0128a {
            @NonNull

            /* renamed from: a reason: collision with root package name */
            public final String f5092a;
            @NonNull
            public final String b;
            @NonNull
            public final String c;
            @NonNull
            public final wp<String, String> d;
            public final long e;
            @NonNull
            public final List<com.yandex.metrica.impl.ob.bt.a> f;

            public C0128a(@NonNull String str, @NonNull String str2, @NonNull String str3, @NonNull wp<String, String> wpVar, long j, @NonNull List<com.yandex.metrica.impl.ob.bt.a> list) {
                this.f5092a = str;
                this.b = str2;
                this.c = str3;
                this.e = j;
                this.f = list;
                this.d = wpVar;
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (obj == null || getClass() != obj.getClass()) {
                    return false;
                }
                return this.f5092a.equals(((C0128a) obj).f5092a);
            }

            public int hashCode() {
                return this.f5092a.hashCode();
            }
        }

        public static class b {
            @Nullable

            /* renamed from: a reason: collision with root package name */
            byte[] f5093a;
            @Nullable
            byte[] b;
            /* access modifiers changed from: private */
            @NonNull
            public final C0128a c;
            @Nullable
            private C0129a d;
            @Nullable
            private com.yandex.metrica.impl.ob.bt.a e;
            @Nullable
            private Integer f;
            @Nullable
            private Map<String, List<String>> g;
            @Nullable
            private Throwable h;

            /* renamed from: com.yandex.metrica.impl.ob.rs$a$b$a reason: collision with other inner class name */
            public enum C0129a {
                OFFLINE,
                INCOMPATIBLE_NETWORK_TYPE,
                COMPLETE,
                ERROR
            }

            public b(@NonNull C0128a aVar) {
                this.c = aVar;
            }

            @NonNull
            public C0128a a() {
                return this.c;
            }

            @Nullable
            public C0129a b() {
                return this.d;
            }

            public void a(@NonNull C0129a aVar) {
                this.d = aVar;
            }

            @Nullable
            public com.yandex.metrica.impl.ob.bt.a c() {
                return this.e;
            }

            public void a(@Nullable com.yandex.metrica.impl.ob.bt.a aVar) {
                this.e = aVar;
            }

            @Nullable
            public Integer d() {
                return this.f;
            }

            public void a(@Nullable Integer num) {
                this.f = num;
            }

            @Nullable
            public byte[] e() {
                return this.f5093a;
            }

            public void a(@Nullable byte[] bArr) {
                this.f5093a = bArr;
            }

            @Nullable
            public Map<String, List<String>> f() {
                return this.g;
            }

            public void a(@Nullable Map<String, List<String>> map) {
                this.g = map;
            }

            @Nullable
            public Throwable g() {
                return this.h;
            }

            public void a(@Nullable Throwable th) {
                this.h = th;
            }

            @Nullable
            public byte[] h() {
                return this.b;
            }

            public void b(@Nullable byte[] bArr) {
                this.b = bArr;
            }
        }

        public a(@NonNull List<C0128a> list, @NonNull List<String> list2) {
            this.f5091a = list;
            if (!cx.a((Collection) list2)) {
                for (String put : list2) {
                    this.b.put(put, new Object());
                }
            }
        }

        public boolean a(@NonNull C0128a aVar) {
            if (this.b.get(aVar.f5092a) != null || this.f5091a.contains(aVar)) {
                return false;
            }
            this.f5091a.add(aVar);
            return true;
        }

        @NonNull
        public Set<String> a() {
            HashSet hashSet = new HashSet();
            int i = 0;
            for (String add : this.b.keySet()) {
                hashSet.add(add);
                i++;
                if (i > 1000) {
                    break;
                }
            }
            return hashSet;
        }

        @NonNull
        public List<C0128a> b() {
            return this.f5091a;
        }

        public void b(@NonNull C0128a aVar) {
            this.b.put(aVar.f5092a, new Object());
            this.f5091a.remove(aVar);
        }
    }

    public rs(@NonNull Context context, @NonNull mf<a> mfVar, @NonNull cs csVar, @NonNull tj tjVar, @NonNull xh xhVar) {
        this(context, mfVar, csVar, tjVar, xhVar, new vk());
    }

    @VisibleForTesting
    public rs(@NonNull Context context, @NonNull mf<a> mfVar, @NonNull cs csVar, @NonNull tj tjVar, @NonNull xh xhVar, @NonNull vn vnVar) {
        this.i = false;
        this.b = context;
        this.c = mfVar;
        this.f = csVar;
        this.e = tjVar;
        this.h = (a) this.c.a();
        this.d = xhVar;
        this.g = vnVar;
    }

    public synchronized void a() {
        this.d.a((Runnable) new Runnable() {
            public void run() {
                rs.this.b();
            }
        });
    }

    /* access modifiers changed from: private */
    public void b() {
        if (!this.i) {
            this.h = (a) this.c.a();
            c();
            this.i = true;
        }
    }

    private void c() {
        for (C0128a b2 : this.h.b()) {
            b(b2);
        }
    }

    public synchronized void a(@NonNull final uk ukVar) {
        final List<com.yandex.metrica.impl.ob.cq.a> list = ukVar.w;
        this.d.a((Runnable) new Runnable() {
            public void run() {
                rs.this.a(list, ukVar.t);
            }
        });
    }

    /* access modifiers changed from: private */
    public void a(@Nullable List<com.yandex.metrica.impl.ob.cq.a> list, long j) {
        if (!cx.a((Collection) list)) {
            for (com.yandex.metrica.impl.ob.cq.a aVar : list) {
                if (!(aVar.f4747a == null || aVar.b == null || aVar.c == null || aVar.e == null || aVar.e.longValue() < 0 || cx.a((Collection) aVar.f))) {
                    C0128a aVar2 = new C0128a(aVar.f4747a, aVar.b, aVar.c, a(aVar.d), TimeUnit.SECONDS.toMillis(aVar.e.longValue() + j), b(aVar.f));
                    a(aVar2);
                }
            }
        }
    }

    private wp<String, String> a(List<Pair<String, String>> list) {
        wp<String, String> wpVar = new wp<>();
        for (Pair pair : list) {
            wpVar.a(pair.first, pair.second);
        }
        return wpVar;
    }

    private boolean a(@NonNull C0128a aVar) {
        boolean a2 = this.h.a(aVar);
        if (a2) {
            b(aVar);
            this.e.a(aVar);
        }
        d();
        return a2;
    }

    private void b(@NonNull final C0128a aVar) {
        this.d.a(new Runnable() {
            public void run() {
                if (!rs.this.f.c()) {
                    rs.this.e.b(aVar);
                    b bVar = new b(aVar);
                    com.yandex.metrica.impl.ob.bt.a a2 = rs.this.g.a(rs.this.b);
                    bVar.a(a2);
                    if (a2 == com.yandex.metrica.impl.ob.bt.a.OFFLINE) {
                        bVar.a(C0129a.OFFLINE);
                    } else if (!aVar.f.contains(a2)) {
                        bVar.a(C0129a.INCOMPATIBLE_NETWORK_TYPE);
                    } else {
                        bVar.a(C0129a.ERROR);
                        try {
                            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(aVar.b).openConnection();
                            for (Entry entry : aVar.d.b()) {
                                httpURLConnection.setRequestProperty((String) entry.getKey(), TextUtils.join(",", (Iterable) entry.getValue()));
                            }
                            httpURLConnection.setInstanceFollowRedirects(true);
                            httpURLConnection.setRequestMethod(aVar.c);
                            httpURLConnection.setConnectTimeout(com.yandex.metrica.impl.ob.pj.a.f5062a);
                            httpURLConnection.setReadTimeout(com.yandex.metrica.impl.ob.pj.a.f5062a);
                            httpURLConnection.connect();
                            int responseCode = httpURLConnection.getResponseCode();
                            bVar.a(C0129a.COMPLETE);
                            bVar.a(Integer.valueOf(responseCode));
                            am.a(httpURLConnection, bVar, "[ProvidedRequestService]", 102400);
                            bVar.a(httpURLConnection.getHeaderFields());
                        } catch (Throwable th) {
                            bVar.a(th);
                        }
                    }
                    rs.this.a(bVar);
                }
            }
        }, Math.max(h.f4865a, Math.max(aVar.e - System.currentTimeMillis(), 0)));
    }

    /* access modifiers changed from: private */
    public synchronized void a(@NonNull b bVar) {
        this.h.b(bVar.c);
        d();
        this.e.a(bVar);
    }

    private void d() {
        this.c.a(this.h);
    }

    @NonNull
    private List<com.yandex.metrica.impl.ob.bt.a> b(@NonNull List<C0108a> list) {
        ArrayList arrayList = new ArrayList(list.size());
        for (C0108a aVar : list) {
            arrayList.add(f5087a.get(aVar));
        }
        return arrayList;
    }
}
