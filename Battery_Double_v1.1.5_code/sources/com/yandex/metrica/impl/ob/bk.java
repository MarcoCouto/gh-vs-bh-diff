package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;

public class bk {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final ez f4691a;

    public bk(@NonNull ez ezVar) {
        this.f4691a = ezVar;
    }

    public void a(@NonNull String str) {
        this.f4691a.a(af.b(str, vz.h()));
    }

    public void b(@NonNull String str) {
        this.f4691a.a(af.a(str, vz.h()));
    }
}
