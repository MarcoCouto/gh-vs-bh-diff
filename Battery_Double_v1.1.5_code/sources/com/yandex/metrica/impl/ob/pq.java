package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;

class pq implements pp {

    /* renamed from: a reason: collision with root package name */
    private final boolean f5066a;

    pq(boolean z) {
        this.f5066a = z;
    }

    public boolean a(@NonNull String str) {
        if ("android.permission.ACCESS_FINE_LOCATION".equals(str) || "android.permission.ACCESS_COARSE_LOCATION".equals(str)) {
            return this.f5066a;
        }
        return true;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("LocationFlagStrategy{mEnabled=");
        sb.append(this.f5066a);
        sb.append('}');
        return sb.toString();
    }
}
