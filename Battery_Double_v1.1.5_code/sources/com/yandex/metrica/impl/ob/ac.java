package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import com.yandex.metrica.impl.ob.ge;

public class ac<C extends ge> extends cv<C> {
    @NonNull
    private final gk d;
    @NonNull
    private final lw e;
    private boolean f = false;

    public ac(@NonNull C c, @NonNull uq uqVar, @NonNull bl blVar, @NonNull gk gkVar, @NonNull lw lwVar) {
        super(c, uqVar, blVar);
        this.d = gkVar;
        this.e = lwVar;
    }

    public void a(@NonNull w wVar) {
        if (!this.f) {
            super.f();
            this.b.a((bo) new gg((gj) g(), wVar, this.d, this.e));
        }
    }

    public void close() {
        this.f = true;
    }
}
