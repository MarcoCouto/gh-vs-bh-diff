package com.yandex.metrica.impl.ob;

import android.annotation.TargetApi;
import android.app.job.JobInfo;
import android.app.job.JobInfo.Builder;
import android.app.job.JobScheduler;
import android.app.job.JobWorkItem;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import com.yandex.metrica.ConfigurationJobService;

@TargetApi(26)
public class jl implements jm, jp {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Context f4907a;
    @Nullable
    private final JobScheduler b;

    public jl(@NonNull Context context) {
        this(context, (JobScheduler) context.getSystemService("jobscheduler"));
    }

    public void a(long j, boolean z) {
        final Builder minimumLatency = new Builder(1512302345, new ComponentName(this.f4907a.getPackageName(), ConfigurationJobService.class.getName())).setMinimumLatency(j);
        if (z) {
            minimumLatency.setOverrideDeadline(j);
        }
        cx.a((wn<T>) new wn<JobScheduler>() {
            public void a(JobScheduler jobScheduler) throws Throwable {
                jobScheduler.schedule(minimumLatency.build());
            }
        }, this.b, "scheduling wakeup in [ConfigurationJobServiceController]", "JobScheduler");
    }

    public void a() {
        cx.a((wn<T>) new wn<JobScheduler>() {
            public void a(JobScheduler jobScheduler) {
                jobScheduler.cancel(1512302345);
            }
        }, this.b, "cancelling scheduled wakeup in [ConfigurationJobServiceController]", "JobScheduler");
    }

    public void a(@NonNull Bundle bundle) {
        final JobInfo build = new Builder(1512302346, new ComponentName(this.f4907a.getPackageName(), ConfigurationJobService.class.getName())).setTransientExtras(bundle).setOverrideDeadline(10).build();
        cx.a((wn<T>) new wn<JobScheduler>() {
            public void a(JobScheduler jobScheduler) throws Throwable {
                jobScheduler.schedule(build);
            }
        }, this.b, "launching [ConfigurationJobServiceController] command", "JobScheduler");
    }

    public void b(@Nullable Bundle bundle) {
        Intent intent = new Intent("com.yandex.metrica.configuration.service.PLC");
        if (bundle == null) {
            bundle = new Bundle();
        }
        final JobWorkItem jobWorkItem = new JobWorkItem(intent.putExtras(bundle));
        final JobInfo build = new Builder(1512302347, new ComponentName(this.f4907a.getPackageName(), ConfigurationJobService.class.getName())).setOverrideDeadline(10).build();
        cx.a((wn<T>) new wn<JobScheduler>() {
            public void a(JobScheduler jobScheduler) {
                jobScheduler.enqueue(build, jobWorkItem);
            }
        }, this.b, "ble callback", "JobScheduler");
    }

    @VisibleForTesting
    jl(@NonNull Context context, @Nullable JobScheduler jobScheduler) {
        this.f4907a = context;
        this.b = jobScheduler;
    }
}
