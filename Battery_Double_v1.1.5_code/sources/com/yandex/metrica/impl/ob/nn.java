package com.yandex.metrica.impl.ob;

import android.os.ParcelUuid;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import com.yandex.metrica.impl.ob.rr.a.b.C0123a;
import com.yandex.metrica.impl.ob.tt.a.c;

public class nn implements mv<c, C0123a.c> {
    @NonNull
    /* renamed from: a */
    public C0123a.c b(@NonNull c cVar) {
        C0123a.c cVar2 = new C0123a.c();
        cVar2.b = cVar.f5213a.toString();
        if (cVar.b != null) {
            cVar2.c = cVar.b.toString();
        }
        return cVar2;
    }

    @NonNull
    public c a(@NonNull C0123a.c cVar) {
        return new c(ParcelUuid.fromString(cVar.b), TextUtils.isEmpty(cVar.c) ? null : ParcelUuid.fromString(cVar.c));
    }
}
