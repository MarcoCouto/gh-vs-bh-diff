package com.yandex.metrica.impl.ob;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.metrica.impl.ob.cs.b;
import com.yandex.metrica.impl.ob.np.a;

public final class al {
    @SuppressLint({"StaticFieldLeak"})

    /* renamed from: a reason: collision with root package name */
    private static volatile al f4643a;
    @NonNull
    private final Context b;
    private volatile si c;
    private volatile tj d;
    private volatile rs e;
    private volatile cs f;
    private volatile h g;
    @Nullable
    private volatile td h;
    @Nullable
    private volatile ai i;
    @NonNull
    private volatile xo j = new xo();
    @Nullable
    private volatile vd k;
    @Nullable
    private volatile ck l;

    public static void a(@NonNull Context context) {
        if (f4643a == null) {
            synchronized (al.class) {
                if (f4643a == null) {
                    f4643a = new al(context.getApplicationContext());
                }
            }
        }
    }

    public static al a() {
        return f4643a;
    }

    private al(@NonNull Context context) {
        this.b = context;
    }

    @NonNull
    public Context b() {
        return this.b;
    }

    @NonNull
    public si c() {
        if (this.c == null) {
            synchronized (this) {
                if (this.c == null) {
                    this.c = new si(this.b);
                }
            }
        }
        return this.c;
    }

    @NonNull
    public tj d() {
        if (this.d == null) {
            synchronized (this) {
                if (this.d == null) {
                    this.d = new tj(this.b);
                }
            }
        }
        return this.d;
    }

    @NonNull
    public rs e() {
        if (this.e == null) {
            synchronized (this) {
                if (this.e == null) {
                    rs rsVar = new rs(this.b, a.a(rs.a.class).a(this.b), a().h(), d(), this.j.h());
                    this.e = rsVar;
                }
            }
        }
        return this.e;
    }

    @NonNull
    public td f() {
        if (this.h == null) {
            synchronized (this) {
                if (this.h == null) {
                    this.h = new td(this.b, this.j.h());
                }
            }
        }
        return this.h;
    }

    @NonNull
    public ai g() {
        if (this.i == null) {
            synchronized (this) {
                if (this.i == null) {
                    this.i = new ai();
                }
            }
        }
        return this.i;
    }

    @NonNull
    public cs h() {
        if (this.f == null) {
            synchronized (this) {
                if (this.f == null) {
                    this.f = new cs(new b(new ly(ld.a(this.b).c())));
                }
            }
        }
        return this.f;
    }

    @NonNull
    public h i() {
        if (this.g == null) {
            synchronized (this) {
                if (this.g == null) {
                    this.g = new h();
                }
            }
        }
        return this.g;
    }

    public void a(@Nullable uk ukVar) {
        if (this.h != null) {
            this.h.b(ukVar);
        }
        if (this.i != null) {
            this.i.a(ukVar);
        }
    }

    @NonNull
    public synchronized xo j() {
        return this.j;
    }

    @NonNull
    public vd k() {
        if (this.k == null) {
            synchronized (this) {
                if (this.k == null) {
                    this.k = new vd(this.b, j().d());
                }
            }
        }
        return this.k;
    }

    @Nullable
    public synchronized ck l() {
        return this.l;
    }

    public synchronized void a(@NonNull cl clVar) {
        this.l = new ck(this.b, clVar);
    }
}
