package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import java.io.File;

public class kf implements Runnable {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final File f4936a;
    @NonNull
    private final wm<File> b;

    public kf(@NonNull File file, @NonNull wm<File> wmVar) {
        this.f4936a = file;
        this.b = wmVar;
    }

    public void run() {
        if (this.f4936a.exists() && this.f4936a.isDirectory()) {
            File[] listFiles = this.f4936a.listFiles();
            if (listFiles != null) {
                for (File a2 : listFiles) {
                    this.b.a(a2);
                }
            }
        }
    }
}
