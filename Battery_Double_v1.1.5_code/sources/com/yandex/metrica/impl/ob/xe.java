package com.yandex.metrica.impl.ob;

import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;

public class xe {
    @NonNull
    public Handler a() {
        return new Handler(Looper.getMainLooper());
    }

    @NonNull
    public xg b() {
        return new xg("YMM-APT");
    }

    @NonNull
    public xg c() {
        return new xg("YMM-RS");
    }

    @NonNull
    public xg d() {
        return new xg("YMM-YM");
    }
}
