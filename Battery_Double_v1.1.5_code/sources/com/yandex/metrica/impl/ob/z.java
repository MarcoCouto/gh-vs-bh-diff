package com.yandex.metrica.impl.ob;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Application.ActivityLifecycleCallbacks;
import android.os.Bundle;
import android.support.annotation.NonNull;

@TargetApi(14)
public class z implements ActivityLifecycleCallbacks {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public final ax f5321a;
    @NonNull
    private final xh b;

    public void onActivityCreated(Activity activity, Bundle bundle) {
    }

    public void onActivityDestroyed(Activity activity) {
    }

    public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
    }

    public void onActivityStarted(Activity activity) {
    }

    public void onActivityStopped(Activity activity) {
    }

    public z(ax axVar, @NonNull xh xhVar) {
        this.f5321a = axVar;
        this.b = xhVar;
    }

    public void onActivityResumed(final Activity activity) {
        this.b.a((Runnable) new Runnable() {
            public void run() {
                z.this.f5321a.b(activity);
            }
        });
    }

    public void onActivityPaused(final Activity activity) {
        this.b.a((Runnable) new Runnable() {
            public void run() {
                z.this.f5321a.c(activity);
            }
        });
    }
}
