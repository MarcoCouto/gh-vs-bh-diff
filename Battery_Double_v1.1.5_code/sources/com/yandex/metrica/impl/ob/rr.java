package com.yandex.metrica.impl.ob;

import android.support.v7.widget.helper.ItemTouchHelper.Callback;
import com.explorestack.iab.vast.VastError;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.extractor.ts.PsExtractor;
import com.google.android.exoplayer2.extractor.ts.TsExtractor;
import com.google.android.exoplayer2.source.chunk.ChunkedTrackBlacklistUtil;
import java.io.IOException;
import java.util.Arrays;

public interface rr {

    public static final class a extends e {
        public String A;
        public String B;
        public long C;
        public long D;
        public boolean E;
        public g F;
        public d G;
        public b H;
        public String b;
        public long c;
        public String[] d;
        public String e;
        public String f;
        public String[] g;
        public String[] h;
        public String[] i;
        public c j;
        public e k;
        public C0121a l;
        public j m;
        public h[] n;
        public String o;
        public String p;
        public boolean q;
        public String r;
        public String s;
        public String[] t;
        public k u;
        public boolean v;
        public f[] w;
        public i x;
        public String y;
        public String z;

        /* renamed from: com.yandex.metrica.impl.ob.rr$a$a reason: collision with other inner class name */
        public static final class C0121a extends e {
            public e b;
            public long c;
            public long d;
            public boolean e;
            public C0122a[] f;

            /* renamed from: com.yandex.metrica.impl.ob.rr$a$a$a reason: collision with other inner class name */
            public static final class C0122a extends e {
                private static volatile C0122a[] d;
                public long b;
                public long c;

                public static C0122a[] d() {
                    if (d == null) {
                        synchronized (c.f4711a) {
                            if (d == null) {
                                d = new C0122a[0];
                            }
                        }
                    }
                    return d;
                }

                public C0122a() {
                    e();
                }

                public C0122a e() {
                    this.b = 0;
                    this.c = 0;
                    this.f4803a = -1;
                    return this;
                }

                public void a(b bVar) throws IOException {
                    bVar.b(1, this.b);
                    bVar.b(2, this.c);
                    super.a(bVar);
                }

                /* access modifiers changed from: protected */
                public int c() {
                    return super.c() + b.e(1, this.b) + b.e(2, this.c);
                }

                /* renamed from: b */
                public C0122a a(a aVar) throws IOException {
                    while (true) {
                        int a2 = aVar.a();
                        if (a2 == 0) {
                            return this;
                        }
                        if (a2 == 8) {
                            this.b = aVar.f();
                        } else if (a2 == 16) {
                            this.c = aVar.f();
                        } else if (!g.a(aVar, a2)) {
                            return this;
                        }
                    }
                }
            }

            public C0121a() {
                d();
            }

            public C0121a d() {
                this.b = null;
                this.c = ChunkedTrackBlacklistUtil.DEFAULT_TRACK_BLACKLIST_MS;
                this.d = 3600000;
                this.e = false;
                this.f = C0122a.d();
                this.f4803a = -1;
                return this;
            }

            public void a(b bVar) throws IOException {
                if (this.b != null) {
                    bVar.a(1, (e) this.b);
                }
                bVar.b(2, this.c);
                bVar.b(3, this.d);
                bVar.a(4, this.e);
                if (this.f != null && this.f.length > 0) {
                    for (C0122a aVar : this.f) {
                        if (aVar != null) {
                            bVar.a(5, (e) aVar);
                        }
                    }
                }
                super.a(bVar);
            }

            /* access modifiers changed from: protected */
            public int c() {
                int c2 = super.c();
                if (this.b != null) {
                    c2 += b.b(1, (e) this.b);
                }
                int e2 = c2 + b.e(2, this.c) + b.e(3, this.d) + b.b(4, this.e);
                if (this.f != null && this.f.length > 0) {
                    for (C0122a aVar : this.f) {
                        if (aVar != null) {
                            e2 += b.b(5, (e) aVar);
                        }
                    }
                }
                return e2;
            }

            /* renamed from: b */
            public C0121a a(a aVar) throws IOException {
                while (true) {
                    int a2 = aVar.a();
                    if (a2 == 0) {
                        return this;
                    }
                    if (a2 == 10) {
                        if (this.b == null) {
                            this.b = new e();
                        }
                        aVar.a((e) this.b);
                    } else if (a2 == 16) {
                        this.c = aVar.f();
                    } else if (a2 == 24) {
                        this.d = aVar.f();
                    } else if (a2 == 32) {
                        this.e = aVar.h();
                    } else if (a2 == 42) {
                        int b2 = g.b(aVar, 42);
                        int length = this.f == null ? 0 : this.f.length;
                        C0122a[] aVarArr = new C0122a[(b2 + length)];
                        if (length != 0) {
                            System.arraycopy(this.f, 0, aVarArr, 0, length);
                        }
                        while (length < aVarArr.length - 1) {
                            aVarArr[length] = new C0122a();
                            aVar.a((e) aVarArr[length]);
                            aVar.a();
                            length++;
                        }
                        aVarArr[length] = new C0122a();
                        aVar.a((e) aVarArr[length]);
                        this.f = aVarArr;
                    } else if (!g.a(aVar, a2)) {
                        return this;
                    }
                }
            }
        }

        public static final class b extends e {
            public C0126b b;
            public C0123a[] c;
            public long d;
            public long e;

            /* renamed from: com.yandex.metrica.impl.ob.rr$a$b$a reason: collision with other inner class name */
            public static final class C0123a extends e {
                private static volatile C0123a[] g;
                public String b;
                public String c;
                public C0124a d;
                public C0125b e;
                public c f;

                /* renamed from: com.yandex.metrica.impl.ob.rr$a$b$a$a reason: collision with other inner class name */
                public static final class C0124a extends e {
                    public int b;
                    public byte[] c;
                    public byte[] d;

                    public C0124a() {
                        d();
                    }

                    public C0124a d() {
                        this.b = 0;
                        this.c = g.h;
                        this.d = g.h;
                        this.f4803a = -1;
                        return this;
                    }

                    public void a(b bVar) throws IOException {
                        bVar.a(1, this.b);
                        if (!Arrays.equals(this.c, g.h)) {
                            bVar.a(2, this.c);
                        }
                        if (!Arrays.equals(this.d, g.h)) {
                            bVar.a(3, this.d);
                        }
                        super.a(bVar);
                    }

                    /* access modifiers changed from: protected */
                    public int c() {
                        int c2 = super.c() + b.d(1, this.b);
                        if (!Arrays.equals(this.c, g.h)) {
                            c2 += b.b(2, this.c);
                        }
                        return !Arrays.equals(this.d, g.h) ? c2 + b.b(3, this.d) : c2;
                    }

                    /* renamed from: b */
                    public C0124a a(a aVar) throws IOException {
                        while (true) {
                            int a2 = aVar.a();
                            if (a2 == 0) {
                                return this;
                            }
                            if (a2 == 8) {
                                this.b = aVar.g();
                            } else if (a2 == 18) {
                                this.c = aVar.j();
                            } else if (a2 == 26) {
                                this.d = aVar.j();
                            } else if (!g.a(aVar, a2)) {
                                return this;
                            }
                        }
                    }
                }

                /* renamed from: com.yandex.metrica.impl.ob.rr$a$b$a$b reason: collision with other inner class name */
                public static final class C0125b extends e {
                    public String b;
                    public byte[] c;
                    public byte[] d;

                    public C0125b() {
                        d();
                    }

                    public C0125b d() {
                        this.b = "";
                        this.c = g.h;
                        this.d = g.h;
                        this.f4803a = -1;
                        return this;
                    }

                    public void a(b bVar) throws IOException {
                        bVar.a(1, this.b);
                        if (!Arrays.equals(this.c, g.h)) {
                            bVar.a(2, this.c);
                        }
                        if (!Arrays.equals(this.d, g.h)) {
                            bVar.a(3, this.d);
                        }
                        super.a(bVar);
                    }

                    /* access modifiers changed from: protected */
                    public int c() {
                        int c2 = super.c() + b.b(1, this.b);
                        if (!Arrays.equals(this.c, g.h)) {
                            c2 += b.b(2, this.c);
                        }
                        return !Arrays.equals(this.d, g.h) ? c2 + b.b(3, this.d) : c2;
                    }

                    /* renamed from: b */
                    public C0125b a(a aVar) throws IOException {
                        while (true) {
                            int a2 = aVar.a();
                            if (a2 == 0) {
                                return this;
                            }
                            if (a2 == 10) {
                                this.b = aVar.i();
                            } else if (a2 == 18) {
                                this.c = aVar.j();
                            } else if (a2 == 26) {
                                this.d = aVar.j();
                            } else if (!g.a(aVar, a2)) {
                                return this;
                            }
                        }
                    }
                }

                /* renamed from: com.yandex.metrica.impl.ob.rr$a$b$a$c */
                public static final class c extends e {
                    public String b;
                    public String c;

                    public c() {
                        d();
                    }

                    public c d() {
                        this.b = "";
                        this.c = "";
                        this.f4803a = -1;
                        return this;
                    }

                    public void a(b bVar) throws IOException {
                        bVar.a(1, this.b);
                        if (!this.c.equals("")) {
                            bVar.a(2, this.c);
                        }
                        super.a(bVar);
                    }

                    /* access modifiers changed from: protected */
                    public int c() {
                        int c2 = super.c() + b.b(1, this.b);
                        return !this.c.equals("") ? c2 + b.b(2, this.c) : c2;
                    }

                    /* renamed from: b */
                    public c a(a aVar) throws IOException {
                        while (true) {
                            int a2 = aVar.a();
                            if (a2 == 0) {
                                return this;
                            }
                            if (a2 == 10) {
                                this.b = aVar.i();
                            } else if (a2 == 18) {
                                this.c = aVar.i();
                            } else if (!g.a(aVar, a2)) {
                                return this;
                            }
                        }
                    }
                }

                public static C0123a[] d() {
                    if (g == null) {
                        synchronized (c.f4711a) {
                            if (g == null) {
                                g = new C0123a[0];
                            }
                        }
                    }
                    return g;
                }

                public C0123a() {
                    e();
                }

                public C0123a e() {
                    this.b = "";
                    this.c = "";
                    this.d = null;
                    this.e = null;
                    this.f = null;
                    this.f4803a = -1;
                    return this;
                }

                public void a(b bVar) throws IOException {
                    if (!this.b.equals("")) {
                        bVar.a(1, this.b);
                    }
                    if (!this.c.equals("")) {
                        bVar.a(2, this.c);
                    }
                    if (this.d != null) {
                        bVar.a(3, (e) this.d);
                    }
                    if (this.e != null) {
                        bVar.a(4, (e) this.e);
                    }
                    if (this.f != null) {
                        bVar.a(5, (e) this.f);
                    }
                    super.a(bVar);
                }

                /* access modifiers changed from: protected */
                public int c() {
                    int c2 = super.c();
                    if (!this.b.equals("")) {
                        c2 += b.b(1, this.b);
                    }
                    if (!this.c.equals("")) {
                        c2 += b.b(2, this.c);
                    }
                    if (this.d != null) {
                        c2 += b.b(3, (e) this.d);
                    }
                    if (this.e != null) {
                        c2 += b.b(4, (e) this.e);
                    }
                    return this.f != null ? c2 + b.b(5, (e) this.f) : c2;
                }

                /* renamed from: b */
                public C0123a a(a aVar) throws IOException {
                    while (true) {
                        int a2 = aVar.a();
                        if (a2 == 0) {
                            return this;
                        }
                        if (a2 == 10) {
                            this.b = aVar.i();
                        } else if (a2 == 18) {
                            this.c = aVar.i();
                        } else if (a2 == 26) {
                            if (this.d == null) {
                                this.d = new C0124a();
                            }
                            aVar.a((e) this.d);
                        } else if (a2 == 34) {
                            if (this.e == null) {
                                this.e = new C0125b();
                            }
                            aVar.a((e) this.e);
                        } else if (a2 == 42) {
                            if (this.f == null) {
                                this.f = new c();
                            }
                            aVar.a((e) this.f);
                        } else if (!g.a(aVar, a2)) {
                            return this;
                        }
                    }
                }
            }

            /* renamed from: com.yandex.metrica.impl.ob.rr$a$b$b reason: collision with other inner class name */
            public static final class C0126b extends e {
                public int b;
                public int c;
                public int d;
                public int e;
                public long f;

                public C0126b() {
                    d();
                }

                public C0126b d() {
                    this.b = 1;
                    this.c = 2;
                    this.d = 3;
                    this.e = 1;
                    this.f = 300000;
                    this.f4803a = -1;
                    return this;
                }

                public void a(b bVar) throws IOException {
                    if (this.b != 1) {
                        bVar.a(1, this.b);
                    }
                    if (this.c != 2) {
                        bVar.a(2, this.c);
                    }
                    if (this.d != 3) {
                        bVar.a(3, this.d);
                    }
                    if (this.e != 1) {
                        bVar.a(4, this.e);
                    }
                    if (this.f != 300000) {
                        bVar.b(5, this.f);
                    }
                    super.a(bVar);
                }

                /* access modifiers changed from: protected */
                public int c() {
                    int c2 = super.c();
                    if (this.b != 1) {
                        c2 += b.d(1, this.b);
                    }
                    if (this.c != 2) {
                        c2 += b.d(2, this.c);
                    }
                    if (this.d != 3) {
                        c2 += b.d(3, this.d);
                    }
                    if (this.e != 1) {
                        c2 += b.d(4, this.e);
                    }
                    return this.f != 300000 ? c2 + b.e(5, this.f) : c2;
                }

                /* renamed from: b */
                public C0126b a(a aVar) throws IOException {
                    while (true) {
                        int a2 = aVar.a();
                        if (a2 != 0) {
                            if (a2 != 8) {
                                if (a2 != 16) {
                                    if (a2 != 24) {
                                        if (a2 == 32) {
                                            int g = aVar.g();
                                            switch (g) {
                                                case 1:
                                                case 2:
                                                case 3:
                                                    this.e = g;
                                                    break;
                                            }
                                        } else if (a2 == 40) {
                                            this.f = aVar.f();
                                        } else if (!g.a(aVar, a2)) {
                                            return this;
                                        }
                                    } else {
                                        int g2 = aVar.g();
                                        switch (g2) {
                                            case 1:
                                            case 2:
                                            case 3:
                                                this.d = g2;
                                                break;
                                        }
                                    }
                                } else {
                                    int g3 = aVar.g();
                                    switch (g3) {
                                        case 1:
                                        case 2:
                                            this.c = g3;
                                            break;
                                    }
                                }
                            } else {
                                int g4 = aVar.g();
                                switch (g4) {
                                    case 1:
                                    case 2:
                                    case 3:
                                        this.b = g4;
                                        break;
                                }
                            }
                        } else {
                            return this;
                        }
                    }
                }
            }

            public b() {
                d();
            }

            public b d() {
                this.b = null;
                this.c = C0123a.d();
                this.d = 1200000;
                this.e = 259200000;
                this.f4803a = -1;
                return this;
            }

            public void a(b bVar) throws IOException {
                if (this.b != null) {
                    bVar.a(1, (e) this.b);
                }
                if (this.c != null && this.c.length > 0) {
                    for (C0123a aVar : this.c) {
                        if (aVar != null) {
                            bVar.a(2, (e) aVar);
                        }
                    }
                }
                bVar.b(3, this.d);
                bVar.b(4, this.e);
                super.a(bVar);
            }

            /* access modifiers changed from: protected */
            public int c() {
                int c2 = super.c();
                if (this.b != null) {
                    c2 += b.b(1, (e) this.b);
                }
                if (this.c != null && this.c.length > 0) {
                    for (C0123a aVar : this.c) {
                        if (aVar != null) {
                            c2 += b.b(2, (e) aVar);
                        }
                    }
                }
                return c2 + b.e(3, this.d) + b.e(4, this.e);
            }

            /* renamed from: b */
            public b a(a aVar) throws IOException {
                while (true) {
                    int a2 = aVar.a();
                    if (a2 == 0) {
                        return this;
                    }
                    if (a2 == 10) {
                        if (this.b == null) {
                            this.b = new C0126b();
                        }
                        aVar.a((e) this.b);
                    } else if (a2 == 18) {
                        int b2 = g.b(aVar, 18);
                        int length = this.c == null ? 0 : this.c.length;
                        C0123a[] aVarArr = new C0123a[(b2 + length)];
                        if (length != 0) {
                            System.arraycopy(this.c, 0, aVarArr, 0, length);
                        }
                        while (length < aVarArr.length - 1) {
                            aVarArr[length] = new C0123a();
                            aVar.a((e) aVarArr[length]);
                            aVar.a();
                            length++;
                        }
                        aVarArr[length] = new C0123a();
                        aVar.a((e) aVarArr[length]);
                        this.c = aVarArr;
                    } else if (a2 == 24) {
                        this.d = aVar.f();
                    } else if (a2 == 32) {
                        this.e = aVar.f();
                    } else if (!g.a(aVar, a2)) {
                        return this;
                    }
                }
            }
        }

        public static final class c extends e {
            public boolean b;
            public boolean c;
            public boolean d;
            public boolean e;
            public boolean f;
            public boolean g;
            public boolean h;
            public boolean i;
            public boolean j;
            public boolean k;
            public boolean l;
            public boolean m;
            public boolean n;
            public boolean o;
            public boolean p;
            public boolean q;

            public c() {
                d();
            }

            public c d() {
                this.b = false;
                this.c = false;
                this.d = false;
                this.e = false;
                this.f = false;
                this.g = false;
                this.h = false;
                this.i = false;
                this.j = false;
                this.k = false;
                this.l = false;
                this.m = false;
                this.n = false;
                this.o = false;
                this.p = true;
                this.q = false;
                this.f4803a = -1;
                return this;
            }

            public void a(b bVar) throws IOException {
                bVar.a(1, this.b);
                bVar.a(2, this.c);
                bVar.a(3, this.d);
                bVar.a(4, this.e);
                bVar.a(5, this.f);
                bVar.a(6, this.g);
                bVar.a(8, this.h);
                bVar.a(9, this.i);
                bVar.a(10, this.j);
                bVar.a(11, this.k);
                bVar.a(12, this.l);
                bVar.a(13, this.m);
                bVar.a(14, this.n);
                bVar.a(15, this.o);
                bVar.a(16, this.p);
                bVar.a(17, this.q);
                super.a(bVar);
            }

            /* access modifiers changed from: protected */
            public int c() {
                return super.c() + b.b(1, this.b) + b.b(2, this.c) + b.b(3, this.d) + b.b(4, this.e) + b.b(5, this.f) + b.b(6, this.g) + b.b(8, this.h) + b.b(9, this.i) + b.b(10, this.j) + b.b(11, this.k) + b.b(12, this.l) + b.b(13, this.m) + b.b(14, this.n) + b.b(15, this.o) + b.b(16, this.p) + b.b(17, this.q);
            }

            /* renamed from: b */
            public c a(a aVar) throws IOException {
                while (true) {
                    int a2 = aVar.a();
                    switch (a2) {
                        case 0:
                            return this;
                        case 8:
                            this.b = aVar.h();
                            break;
                        case 16:
                            this.c = aVar.h();
                            break;
                        case 24:
                            this.d = aVar.h();
                            break;
                        case 32:
                            this.e = aVar.h();
                            break;
                        case 40:
                            this.f = aVar.h();
                            break;
                        case 48:
                            this.g = aVar.h();
                            break;
                        case 64:
                            this.h = aVar.h();
                            break;
                        case 72:
                            this.i = aVar.h();
                            break;
                        case 80:
                            this.j = aVar.h();
                            break;
                        case 88:
                            this.k = aVar.h();
                            break;
                        case 96:
                            this.l = aVar.h();
                            break;
                        case 104:
                            this.m = aVar.h();
                            break;
                        case 112:
                            this.n = aVar.h();
                            break;
                        case 120:
                            this.o = aVar.h();
                            break;
                        case 128:
                            this.p = aVar.h();
                            break;
                        case 136:
                            this.q = aVar.h();
                            break;
                        default:
                            if (g.a(aVar, a2)) {
                                break;
                            } else {
                                return this;
                            }
                    }
                }
            }
        }

        public static final class d extends e {
            public long b;

            public d() {
                d();
            }

            public d d() {
                this.b = 900;
                this.f4803a = -1;
                return this;
            }

            public void a(b bVar) throws IOException {
                bVar.b(1, this.b);
                super.a(bVar);
            }

            /* access modifiers changed from: protected */
            public int c() {
                return super.c() + b.e(1, this.b);
            }

            /* renamed from: b */
            public d a(a aVar) throws IOException {
                while (true) {
                    int a2 = aVar.a();
                    if (a2 == 0) {
                        return this;
                    }
                    if (a2 == 8) {
                        this.b = aVar.f();
                    } else if (!g.a(aVar, a2)) {
                        return this;
                    }
                }
            }
        }

        public static final class e extends e {
            public long b;
            public float c;
            public int d;
            public int e;
            public long f;
            public int g;
            public boolean h;
            public boolean i;
            public long j;

            public e() {
                d();
            }

            public e d() {
                this.b = DefaultRenderersFactory.DEFAULT_ALLOWED_VIDEO_JOINING_TIME_MS;
                this.c = 10.0f;
                this.d = 20;
                this.e = Callback.DEFAULT_DRAG_ANIMATION_DURATION;
                this.f = ChunkedTrackBlacklistUtil.DEFAULT_TRACK_BLACKLIST_MS;
                this.g = 10000;
                this.h = false;
                this.i = false;
                this.j = ChunkedTrackBlacklistUtil.DEFAULT_TRACK_BLACKLIST_MS;
                this.f4803a = -1;
                return this;
            }

            public void a(b bVar) throws IOException {
                bVar.b(1, this.b);
                bVar.a(2, this.c);
                bVar.a(3, this.d);
                bVar.a(4, this.e);
                bVar.b(5, this.f);
                bVar.a(6, this.g);
                bVar.a(7, this.h);
                bVar.a(8, this.i);
                bVar.b(9, this.j);
                super.a(bVar);
            }

            /* access modifiers changed from: protected */
            public int c() {
                return super.c() + b.e(1, this.b) + b.b(2, this.c) + b.d(3, this.d) + b.d(4, this.e) + b.e(5, this.f) + b.d(6, this.g) + b.b(7, this.h) + b.b(8, this.i) + b.e(9, this.j);
            }

            /* renamed from: b */
            public e a(a aVar) throws IOException {
                while (true) {
                    int a2 = aVar.a();
                    if (a2 == 0) {
                        return this;
                    }
                    if (a2 == 8) {
                        this.b = aVar.f();
                    } else if (a2 == 21) {
                        this.c = aVar.d();
                    } else if (a2 == 24) {
                        this.d = aVar.g();
                    } else if (a2 == 32) {
                        this.e = aVar.g();
                    } else if (a2 == 40) {
                        this.f = aVar.f();
                    } else if (a2 == 48) {
                        this.g = aVar.g();
                    } else if (a2 == 56) {
                        this.h = aVar.h();
                    } else if (a2 == 64) {
                        this.i = aVar.h();
                    } else if (a2 == 72) {
                        this.j = aVar.f();
                    } else if (!g.a(aVar, a2)) {
                        return this;
                    }
                }
            }
        }

        public static final class f extends e {
            private static volatile f[] d;
            public String b;
            public boolean c;

            public static f[] d() {
                if (d == null) {
                    synchronized (c.f4711a) {
                        if (d == null) {
                            d = new f[0];
                        }
                    }
                }
                return d;
            }

            public f() {
                e();
            }

            public f e() {
                this.b = "";
                this.c = false;
                this.f4803a = -1;
                return this;
            }

            public void a(b bVar) throws IOException {
                bVar.a(1, this.b);
                bVar.a(2, this.c);
                super.a(bVar);
            }

            /* access modifiers changed from: protected */
            public int c() {
                return super.c() + b.b(1, this.b) + b.b(2, this.c);
            }

            /* renamed from: b */
            public f a(a aVar) throws IOException {
                while (true) {
                    int a2 = aVar.a();
                    if (a2 == 0) {
                        return this;
                    }
                    if (a2 == 10) {
                        this.b = aVar.i();
                    } else if (a2 == 16) {
                        this.c = aVar.h();
                    } else if (!g.a(aVar, a2)) {
                        return this;
                    }
                }
            }
        }

        public static final class g extends e {
            public long b;
            public long c;

            public g() {
                d();
            }

            public g d() {
                this.b = 86400;
                this.c = 432000;
                this.f4803a = -1;
                return this;
            }

            public void a(b bVar) throws IOException {
                bVar.b(1, this.b);
                bVar.b(2, this.c);
                super.a(bVar);
            }

            /* access modifiers changed from: protected */
            public int c() {
                return super.c() + b.e(1, this.b) + b.e(2, this.c);
            }

            /* renamed from: b */
            public g a(a aVar) throws IOException {
                while (true) {
                    int a2 = aVar.a();
                    if (a2 == 0) {
                        return this;
                    }
                    if (a2 == 8) {
                        this.b = aVar.f();
                    } else if (a2 == 16) {
                        this.c = aVar.f();
                    } else if (!g.a(aVar, a2)) {
                        return this;
                    }
                }
            }
        }

        public static final class h extends e {
            private static volatile h[] h;
            public String b;
            public String c;
            public String d;
            public C0127a[] e;
            public long f;
            public int[] g;

            /* renamed from: com.yandex.metrica.impl.ob.rr$a$h$a reason: collision with other inner class name */
            public static final class C0127a extends e {
                private static volatile C0127a[] d;
                public String b;
                public String c;

                public static C0127a[] d() {
                    if (d == null) {
                        synchronized (c.f4711a) {
                            if (d == null) {
                                d = new C0127a[0];
                            }
                        }
                    }
                    return d;
                }

                public C0127a() {
                    e();
                }

                public C0127a e() {
                    this.b = "";
                    this.c = "";
                    this.f4803a = -1;
                    return this;
                }

                public void a(b bVar) throws IOException {
                    bVar.a(1, this.b);
                    bVar.a(2, this.c);
                    super.a(bVar);
                }

                /* access modifiers changed from: protected */
                public int c() {
                    return super.c() + b.b(1, this.b) + b.b(2, this.c);
                }

                /* renamed from: b */
                public C0127a a(a aVar) throws IOException {
                    while (true) {
                        int a2 = aVar.a();
                        if (a2 == 0) {
                            return this;
                        }
                        if (a2 == 10) {
                            this.b = aVar.i();
                        } else if (a2 == 18) {
                            this.c = aVar.i();
                        } else if (!g.a(aVar, a2)) {
                            return this;
                        }
                    }
                }
            }

            public static h[] d() {
                if (h == null) {
                    synchronized (c.f4711a) {
                        if (h == null) {
                            h = new h[0];
                        }
                    }
                }
                return h;
            }

            public h() {
                e();
            }

            public h e() {
                this.b = "";
                this.c = "";
                this.d = "";
                this.e = C0127a.d();
                this.f = 0;
                this.g = g.f4846a;
                this.f4803a = -1;
                return this;
            }

            public void a(b bVar) throws IOException {
                bVar.a(1, this.b);
                bVar.a(2, this.c);
                bVar.a(3, this.d);
                if (this.e != null && this.e.length > 0) {
                    for (C0127a aVar : this.e) {
                        if (aVar != null) {
                            bVar.a(4, (e) aVar);
                        }
                    }
                }
                bVar.b(5, this.f);
                if (this.g != null && this.g.length > 0) {
                    for (int a2 : this.g) {
                        bVar.a(6, a2);
                    }
                }
                super.a(bVar);
            }

            /* access modifiers changed from: protected */
            public int c() {
                int c2 = super.c() + b.b(1, this.b) + b.b(2, this.c) + b.b(3, this.d);
                if (this.e != null && this.e.length > 0) {
                    int i = c2;
                    for (C0127a aVar : this.e) {
                        if (aVar != null) {
                            i += b.b(4, (e) aVar);
                        }
                    }
                    c2 = i;
                }
                int e2 = c2 + b.e(5, this.f);
                if (this.g == null || this.g.length <= 0) {
                    return e2;
                }
                int i2 = 0;
                for (int d2 : this.g) {
                    i2 += b.d(d2);
                }
                return e2 + i2 + (this.g.length * 1);
            }

            /* renamed from: b */
            public h a(a aVar) throws IOException {
                while (true) {
                    int a2 = aVar.a();
                    if (a2 == 0) {
                        return this;
                    }
                    if (a2 == 10) {
                        this.b = aVar.i();
                    } else if (a2 == 18) {
                        this.c = aVar.i();
                    } else if (a2 == 26) {
                        this.d = aVar.i();
                    } else if (a2 == 34) {
                        int b2 = g.b(aVar, 34);
                        int length = this.e == null ? 0 : this.e.length;
                        C0127a[] aVarArr = new C0127a[(b2 + length)];
                        if (length != 0) {
                            System.arraycopy(this.e, 0, aVarArr, 0, length);
                        }
                        while (length < aVarArr.length - 1) {
                            aVarArr[length] = new C0127a();
                            aVar.a((e) aVarArr[length]);
                            aVar.a();
                            length++;
                        }
                        aVarArr[length] = new C0127a();
                        aVar.a((e) aVarArr[length]);
                        this.e = aVarArr;
                    } else if (a2 == 40) {
                        this.f = aVar.f();
                    } else if (a2 == 48) {
                        int b3 = g.b(aVar, 48);
                        int[] iArr = new int[b3];
                        int i = 0;
                        for (int i2 = 0; i2 < b3; i2++) {
                            if (i2 != 0) {
                                aVar.a();
                            }
                            int g2 = aVar.g();
                            switch (g2) {
                                case 1:
                                case 2:
                                    int i3 = i + 1;
                                    iArr[i] = g2;
                                    i = i3;
                                    break;
                            }
                        }
                        if (i != 0) {
                            int length2 = this.g == null ? 0 : this.g.length;
                            if (length2 == 0 && i == iArr.length) {
                                this.g = iArr;
                            } else {
                                int[] iArr2 = new int[(length2 + i)];
                                if (length2 != 0) {
                                    System.arraycopy(this.g, 0, iArr2, 0, length2);
                                }
                                System.arraycopy(iArr, 0, iArr2, length2, i);
                                this.g = iArr2;
                            }
                        }
                    } else if (a2 == 50) {
                        int d2 = aVar.d(aVar.n());
                        int t = aVar.t();
                        int i4 = 0;
                        while (aVar.r() > 0) {
                            switch (aVar.g()) {
                                case 1:
                                case 2:
                                    i4++;
                                    break;
                            }
                        }
                        if (i4 != 0) {
                            aVar.f(t);
                            int length3 = this.g == null ? 0 : this.g.length;
                            int[] iArr3 = new int[(i4 + length3)];
                            if (length3 != 0) {
                                System.arraycopy(this.g, 0, iArr3, 0, length3);
                            }
                            while (aVar.r() > 0) {
                                int g3 = aVar.g();
                                switch (g3) {
                                    case 1:
                                    case 2:
                                        int i5 = length3 + 1;
                                        iArr3[length3] = g3;
                                        length3 = i5;
                                        break;
                                }
                            }
                            this.g = iArr3;
                        }
                        aVar.e(d2);
                    } else if (!g.a(aVar, a2)) {
                        return this;
                    }
                }
            }
        }

        public static final class i extends e {
            public long b;
            public long c;
            public long d;
            public long e;

            public i() {
                d();
            }

            public i d() {
                this.b = 432000000;
                this.c = 86400000;
                this.d = 10000;
                this.e = 3600000;
                this.f4803a = -1;
                return this;
            }

            public void a(b bVar) throws IOException {
                if (this.b != 432000000) {
                    bVar.b(1, this.b);
                }
                if (this.c != 86400000) {
                    bVar.b(2, this.c);
                }
                if (this.d != 10000) {
                    bVar.b(3, this.d);
                }
                if (this.e != 3600000) {
                    bVar.b(4, this.e);
                }
                super.a(bVar);
            }

            /* access modifiers changed from: protected */
            public int c() {
                int c2 = super.c();
                if (this.b != 432000000) {
                    c2 += b.e(1, this.b);
                }
                if (this.c != 86400000) {
                    c2 += b.e(2, this.c);
                }
                if (this.d != 10000) {
                    c2 += b.e(3, this.d);
                }
                return this.e != 3600000 ? c2 + b.e(4, this.e) : c2;
            }

            /* renamed from: b */
            public i a(a aVar) throws IOException {
                while (true) {
                    int a2 = aVar.a();
                    if (a2 == 0) {
                        return this;
                    }
                    if (a2 == 8) {
                        this.b = aVar.f();
                    } else if (a2 == 16) {
                        this.c = aVar.f();
                    } else if (a2 == 24) {
                        this.d = aVar.f();
                    } else if (a2 == 32) {
                        this.e = aVar.f();
                    } else if (!g.a(aVar, a2)) {
                        return this;
                    }
                }
            }
        }

        public static final class j extends e {
            public long b;
            public String c;
            public int[] d;
            public long e;
            public int f;

            public j() {
                d();
            }

            public j d() {
                this.b = 0;
                this.c = "";
                this.d = g.f4846a;
                this.e = 259200;
                this.f = 10;
                this.f4803a = -1;
                return this;
            }

            public void a(b bVar) throws IOException {
                bVar.b(1, this.b);
                bVar.a(2, this.c);
                if (this.d != null && this.d.length > 0) {
                    for (int a2 : this.d) {
                        bVar.a(3, a2);
                    }
                }
                bVar.b(4, this.e);
                bVar.a(5, this.f);
                super.a(bVar);
            }

            /* access modifiers changed from: protected */
            public int c() {
                int c2 = super.c() + b.e(1, this.b) + b.b(2, this.c);
                if (this.d != null && this.d.length > 0) {
                    int i = 0;
                    for (int d2 : this.d) {
                        i += b.d(d2);
                    }
                    c2 = c2 + i + (this.d.length * 1);
                }
                return c2 + b.e(4, this.e) + b.d(5, this.f);
            }

            /* renamed from: b */
            public j a(a aVar) throws IOException {
                while (true) {
                    int a2 = aVar.a();
                    if (a2 == 0) {
                        return this;
                    }
                    if (a2 == 8) {
                        this.b = aVar.f();
                    } else if (a2 == 18) {
                        this.c = aVar.i();
                    } else if (a2 == 24) {
                        int b2 = g.b(aVar, 24);
                        int length = this.d == null ? 0 : this.d.length;
                        int[] iArr = new int[(b2 + length)];
                        if (length != 0) {
                            System.arraycopy(this.d, 0, iArr, 0, length);
                        }
                        while (length < iArr.length - 1) {
                            iArr[length] = aVar.g();
                            aVar.a();
                            length++;
                        }
                        iArr[length] = aVar.g();
                        this.d = iArr;
                    } else if (a2 == 26) {
                        int d2 = aVar.d(aVar.n());
                        int t = aVar.t();
                        int i = 0;
                        while (aVar.r() > 0) {
                            aVar.g();
                            i++;
                        }
                        aVar.f(t);
                        int length2 = this.d == null ? 0 : this.d.length;
                        int[] iArr2 = new int[(i + length2)];
                        if (length2 != 0) {
                            System.arraycopy(this.d, 0, iArr2, 0, length2);
                        }
                        while (length2 < iArr2.length) {
                            iArr2[length2] = aVar.g();
                            length2++;
                        }
                        this.d = iArr2;
                        aVar.e(d2);
                    } else if (a2 == 32) {
                        this.e = aVar.f();
                    } else if (a2 == 40) {
                        this.f = aVar.g();
                    } else if (!g.a(aVar, a2)) {
                        return this;
                    }
                }
            }
        }

        public static final class k extends e {
            public long b;

            public k() {
                d();
            }

            public k d() {
                this.b = 18000000;
                this.f4803a = -1;
                return this;
            }

            public void a(b bVar) throws IOException {
                bVar.b(1, this.b);
                super.a(bVar);
            }

            /* access modifiers changed from: protected */
            public int c() {
                return super.c() + b.e(1, this.b);
            }

            /* renamed from: b */
            public k a(a aVar) throws IOException {
                while (true) {
                    int a2 = aVar.a();
                    if (a2 == 0) {
                        return this;
                    }
                    if (a2 == 8) {
                        this.b = aVar.f();
                    } else if (!g.a(aVar, a2)) {
                        return this;
                    }
                }
            }
        }

        public a() {
            d();
        }

        public a d() {
            this.b = "";
            this.c = 0;
            this.d = g.f;
            this.e = "";
            this.f = "";
            this.g = g.f;
            this.h = g.f;
            this.i = g.f;
            this.j = null;
            this.k = null;
            this.l = null;
            this.m = null;
            this.n = h.d();
            this.o = "";
            this.p = "";
            this.q = false;
            this.r = "";
            this.s = "";
            this.t = g.f;
            this.u = null;
            this.v = false;
            this.w = f.d();
            this.x = null;
            this.y = "";
            this.z = "";
            this.A = "";
            this.B = "";
            this.C = 0;
            this.D = 0;
            this.E = false;
            this.F = null;
            this.G = null;
            this.H = null;
            this.f4803a = -1;
            return this;
        }

        public void a(b bVar) throws IOException {
            if (!this.b.equals("")) {
                bVar.a(1, this.b);
            }
            bVar.b(3, this.c);
            if (this.d != null && this.d.length > 0) {
                for (String str : this.d) {
                    if (str != null) {
                        bVar.a(4, str);
                    }
                }
            }
            if (!this.e.equals("")) {
                bVar.a(5, this.e);
            }
            if (!this.f.equals("")) {
                bVar.a(6, this.f);
            }
            if (this.g != null && this.g.length > 0) {
                for (String str2 : this.g) {
                    if (str2 != null) {
                        bVar.a(7, str2);
                    }
                }
            }
            if (this.h != null && this.h.length > 0) {
                for (String str3 : this.h) {
                    if (str3 != null) {
                        bVar.a(8, str3);
                    }
                }
            }
            if (this.i != null && this.i.length > 0) {
                for (String str4 : this.i) {
                    if (str4 != null) {
                        bVar.a(9, str4);
                    }
                }
            }
            if (this.j != null) {
                bVar.a(10, (e) this.j);
            }
            if (this.k != null) {
                bVar.a(11, (e) this.k);
            }
            if (this.l != null) {
                bVar.a(12, (e) this.l);
            }
            if (this.m != null) {
                bVar.a(13, (e) this.m);
            }
            if (this.n != null && this.n.length > 0) {
                for (h hVar : this.n) {
                    if (hVar != null) {
                        bVar.a(14, (e) hVar);
                    }
                }
            }
            if (!this.o.equals("")) {
                bVar.a(15, this.o);
            }
            if (!this.p.equals("")) {
                bVar.a(16, this.p);
            }
            bVar.a(17, this.q);
            if (!this.r.equals("")) {
                bVar.a(18, this.r);
            }
            if (!this.s.equals("")) {
                bVar.a(19, this.s);
            }
            if (this.t != null && this.t.length > 0) {
                for (String str5 : this.t) {
                    if (str5 != null) {
                        bVar.a(20, str5);
                    }
                }
            }
            if (this.u != null) {
                bVar.a(21, (e) this.u);
            }
            if (this.v) {
                bVar.a(22, this.v);
            }
            if (this.w != null && this.w.length > 0) {
                for (f fVar : this.w) {
                    if (fVar != null) {
                        bVar.a(23, (e) fVar);
                    }
                }
            }
            if (this.x != null) {
                bVar.a(24, (e) this.x);
            }
            if (!this.y.equals("")) {
                bVar.a(25, this.y);
            }
            if (!this.z.equals("")) {
                bVar.a(26, this.z);
            }
            if (!this.B.equals("")) {
                bVar.a(27, this.B);
            }
            bVar.b(28, this.C);
            bVar.b(29, this.D);
            if (this.E) {
                bVar.a(30, this.E);
            }
            if (!this.A.equals("")) {
                bVar.a(31, this.A);
            }
            if (this.F != null) {
                bVar.a(32, (e) this.F);
            }
            if (this.G != null) {
                bVar.a(33, (e) this.G);
            }
            if (this.H != null) {
                bVar.a(34, (e) this.H);
            }
            super.a(bVar);
        }

        /* access modifiers changed from: protected */
        public int c() {
            int c2 = super.c();
            if (!this.b.equals("")) {
                c2 += b.b(1, this.b);
            }
            int e2 = c2 + b.e(3, this.c);
            if (this.d != null && this.d.length > 0) {
                int i2 = 0;
                int i3 = 0;
                for (String str : this.d) {
                    if (str != null) {
                        i3++;
                        i2 += b.b(str);
                    }
                }
                e2 = e2 + i2 + (i3 * 1);
            }
            if (!this.e.equals("")) {
                e2 += b.b(5, this.e);
            }
            if (!this.f.equals("")) {
                e2 += b.b(6, this.f);
            }
            if (this.g != null && this.g.length > 0) {
                int i4 = 0;
                int i5 = 0;
                for (String str2 : this.g) {
                    if (str2 != null) {
                        i5++;
                        i4 += b.b(str2);
                    }
                }
                e2 = e2 + i4 + (i5 * 1);
            }
            if (this.h != null && this.h.length > 0) {
                int i6 = 0;
                int i7 = 0;
                for (String str3 : this.h) {
                    if (str3 != null) {
                        i7++;
                        i6 += b.b(str3);
                    }
                }
                e2 = e2 + i6 + (i7 * 1);
            }
            if (this.i != null && this.i.length > 0) {
                int i8 = 0;
                int i9 = 0;
                for (String str4 : this.i) {
                    if (str4 != null) {
                        i9++;
                        i8 += b.b(str4);
                    }
                }
                e2 = e2 + i8 + (i9 * 1);
            }
            if (this.j != null) {
                e2 += b.b(10, (e) this.j);
            }
            if (this.k != null) {
                e2 += b.b(11, (e) this.k);
            }
            if (this.l != null) {
                e2 += b.b(12, (e) this.l);
            }
            if (this.m != null) {
                e2 += b.b(13, (e) this.m);
            }
            if (this.n != null && this.n.length > 0) {
                int i10 = e2;
                for (h hVar : this.n) {
                    if (hVar != null) {
                        i10 += b.b(14, (e) hVar);
                    }
                }
                e2 = i10;
            }
            if (!this.o.equals("")) {
                e2 += b.b(15, this.o);
            }
            if (!this.p.equals("")) {
                e2 += b.b(16, this.p);
            }
            int b2 = e2 + b.b(17, this.q);
            if (!this.r.equals("")) {
                b2 += b.b(18, this.r);
            }
            if (!this.s.equals("")) {
                b2 += b.b(19, this.s);
            }
            if (this.t != null && this.t.length > 0) {
                int i11 = 0;
                int i12 = 0;
                for (String str5 : this.t) {
                    if (str5 != null) {
                        i12++;
                        i11 += b.b(str5);
                    }
                }
                b2 = b2 + i11 + (i12 * 2);
            }
            if (this.u != null) {
                b2 += b.b(21, (e) this.u);
            }
            if (this.v) {
                b2 += b.b(22, this.v);
            }
            if (this.w != null && this.w.length > 0) {
                for (f fVar : this.w) {
                    if (fVar != null) {
                        b2 += b.b(23, (e) fVar);
                    }
                }
            }
            if (this.x != null) {
                b2 += b.b(24, (e) this.x);
            }
            if (!this.y.equals("")) {
                b2 += b.b(25, this.y);
            }
            if (!this.z.equals("")) {
                b2 += b.b(26, this.z);
            }
            if (!this.B.equals("")) {
                b2 += b.b(27, this.B);
            }
            int e3 = b2 + b.e(28, this.C) + b.e(29, this.D);
            if (this.E) {
                e3 += b.b(30, this.E);
            }
            if (!this.A.equals("")) {
                e3 += b.b(31, this.A);
            }
            if (this.F != null) {
                e3 += b.b(32, (e) this.F);
            }
            if (this.G != null) {
                e3 += b.b(33, (e) this.G);
            }
            return this.H != null ? e3 + b.b(34, (e) this.H) : e3;
        }

        /* renamed from: b */
        public a a(a aVar) throws IOException {
            while (true) {
                int a2 = aVar.a();
                switch (a2) {
                    case 0:
                        return this;
                    case 10:
                        this.b = aVar.i();
                        break;
                    case 24:
                        this.c = aVar.f();
                        break;
                    case 34:
                        int b2 = g.b(aVar, 34);
                        int length = this.d == null ? 0 : this.d.length;
                        String[] strArr = new String[(b2 + length)];
                        if (length != 0) {
                            System.arraycopy(this.d, 0, strArr, 0, length);
                        }
                        while (length < strArr.length - 1) {
                            strArr[length] = aVar.i();
                            aVar.a();
                            length++;
                        }
                        strArr[length] = aVar.i();
                        this.d = strArr;
                        break;
                    case 42:
                        this.e = aVar.i();
                        break;
                    case 50:
                        this.f = aVar.i();
                        break;
                    case 58:
                        int b3 = g.b(aVar, 58);
                        int length2 = this.g == null ? 0 : this.g.length;
                        String[] strArr2 = new String[(b3 + length2)];
                        if (length2 != 0) {
                            System.arraycopy(this.g, 0, strArr2, 0, length2);
                        }
                        while (length2 < strArr2.length - 1) {
                            strArr2[length2] = aVar.i();
                            aVar.a();
                            length2++;
                        }
                        strArr2[length2] = aVar.i();
                        this.g = strArr2;
                        break;
                    case 66:
                        int b4 = g.b(aVar, 66);
                        int length3 = this.h == null ? 0 : this.h.length;
                        String[] strArr3 = new String[(b4 + length3)];
                        if (length3 != 0) {
                            System.arraycopy(this.h, 0, strArr3, 0, length3);
                        }
                        while (length3 < strArr3.length - 1) {
                            strArr3[length3] = aVar.i();
                            aVar.a();
                            length3++;
                        }
                        strArr3[length3] = aVar.i();
                        this.h = strArr3;
                        break;
                    case 74:
                        int b5 = g.b(aVar, 74);
                        int length4 = this.i == null ? 0 : this.i.length;
                        String[] strArr4 = new String[(b5 + length4)];
                        if (length4 != 0) {
                            System.arraycopy(this.i, 0, strArr4, 0, length4);
                        }
                        while (length4 < strArr4.length - 1) {
                            strArr4[length4] = aVar.i();
                            aVar.a();
                            length4++;
                        }
                        strArr4[length4] = aVar.i();
                        this.i = strArr4;
                        break;
                    case 82:
                        if (this.j == null) {
                            this.j = new c();
                        }
                        aVar.a((e) this.j);
                        break;
                    case 90:
                        if (this.k == null) {
                            this.k = new e();
                        }
                        aVar.a((e) this.k);
                        break;
                    case 98:
                        if (this.l == null) {
                            this.l = new C0121a();
                        }
                        aVar.a((e) this.l);
                        break;
                    case 106:
                        if (this.m == null) {
                            this.m = new j();
                        }
                        aVar.a((e) this.m);
                        break;
                    case 114:
                        int b6 = g.b(aVar, 114);
                        int length5 = this.n == null ? 0 : this.n.length;
                        h[] hVarArr = new h[(b6 + length5)];
                        if (length5 != 0) {
                            System.arraycopy(this.n, 0, hVarArr, 0, length5);
                        }
                        while (length5 < hVarArr.length - 1) {
                            hVarArr[length5] = new h();
                            aVar.a((e) hVarArr[length5]);
                            aVar.a();
                            length5++;
                        }
                        hVarArr[length5] = new h();
                        aVar.a((e) hVarArr[length5]);
                        this.n = hVarArr;
                        break;
                    case 122:
                        this.o = aVar.i();
                        break;
                    case TsExtractor.TS_STREAM_TYPE_HDMV_DTS /*130*/:
                        this.p = aVar.i();
                        break;
                    case 136:
                        this.q = aVar.h();
                        break;
                    case 146:
                        this.r = aVar.i();
                        break;
                    case 154:
                        this.s = aVar.i();
                        break;
                    case 162:
                        int b7 = g.b(aVar, 162);
                        int length6 = this.t == null ? 0 : this.t.length;
                        String[] strArr5 = new String[(b7 + length6)];
                        if (length6 != 0) {
                            System.arraycopy(this.t, 0, strArr5, 0, length6);
                        }
                        while (length6 < strArr5.length - 1) {
                            strArr5[length6] = aVar.i();
                            aVar.a();
                            length6++;
                        }
                        strArr5[length6] = aVar.i();
                        this.t = strArr5;
                        break;
                    case 170:
                        if (this.u == null) {
                            this.u = new k();
                        }
                        aVar.a((e) this.u);
                        break;
                    case 176:
                        this.v = aVar.h();
                        break;
                    case 186:
                        int b8 = g.b(aVar, 186);
                        int length7 = this.w == null ? 0 : this.w.length;
                        f[] fVarArr = new f[(b8 + length7)];
                        if (length7 != 0) {
                            System.arraycopy(this.w, 0, fVarArr, 0, length7);
                        }
                        while (length7 < fVarArr.length - 1) {
                            fVarArr[length7] = new f();
                            aVar.a((e) fVarArr[length7]);
                            aVar.a();
                            length7++;
                        }
                        fVarArr[length7] = new f();
                        aVar.a((e) fVarArr[length7]);
                        this.w = fVarArr;
                        break;
                    case 194:
                        if (this.x == null) {
                            this.x = new i();
                        }
                        aVar.a((e) this.x);
                        break;
                    case VastError.ERROR_CODE_DURATION /*202*/:
                        this.y = aVar.i();
                        break;
                    case 210:
                        this.z = aVar.i();
                        break;
                    case 218:
                        this.B = aVar.i();
                        break;
                    case 224:
                        this.C = aVar.f();
                        break;
                    case 232:
                        this.D = aVar.f();
                        break;
                    case PsExtractor.VIDEO_STREAM_MASK /*240*/:
                        this.E = aVar.h();
                        break;
                    case 250:
                        this.A = aVar.i();
                        break;
                    case 258:
                        if (this.F == null) {
                            this.F = new g();
                        }
                        aVar.a((e) this.F);
                        break;
                    case 266:
                        if (this.G == null) {
                            this.G = new d();
                        }
                        aVar.a((e) this.G);
                        break;
                    case 274:
                        if (this.H == null) {
                            this.H = new b();
                        }
                        aVar.a((e) this.H);
                        break;
                    default:
                        if (g.a(aVar, a2)) {
                            break;
                        } else {
                            return this;
                        }
                }
            }
        }

        public static a a(byte[] bArr) throws d {
            return (a) e.a(new a(), bArr);
        }
    }
}
