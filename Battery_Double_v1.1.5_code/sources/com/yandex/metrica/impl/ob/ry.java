package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import com.yandex.metrica.ReporterConfig;
import com.yandex.metrica.impl.ob.rx;
import java.util.HashMap;

public abstract class ry<T extends rx> {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private HashMap<String, T> f5118a = new HashMap<>();
    /* access modifiers changed from: private */
    @NonNull
    public sa b = new sa();
    @NonNull
    private final xh c;

    /* access modifiers changed from: protected */
    public abstract T a(@NonNull xh xhVar, @NonNull Context context, @NonNull String str);

    protected ry(@NonNull xh xhVar) {
        this.c = xhVar;
    }

    private T b(@NonNull final Context context, @NonNull String str) {
        if (this.b.f() == null) {
            this.c.a((Runnable) new Runnable() {
                public void run() {
                    ry.this.b.a(context);
                }
            });
        }
        T a2 = a(this.c, context, str);
        this.f5118a.put(str, a2);
        return a2;
    }

    public T a(@NonNull Context context, @NonNull String str) {
        T t = (rx) this.f5118a.get(str);
        if (t == null) {
            synchronized (this.f5118a) {
                t = (rx) this.f5118a.get(str);
                if (t == null) {
                    T b2 = b(context, str);
                    b2.a(str);
                    t = b2;
                }
            }
        }
        return t;
    }

    public T a(@NonNull Context context, @NonNull ReporterConfig reporterConfig) {
        T t = (rx) this.f5118a.get(reporterConfig.apiKey);
        if (t == null) {
            synchronized (this.f5118a) {
                t = (rx) this.f5118a.get(reporterConfig.apiKey);
                if (t == null) {
                    T b2 = b(context, reporterConfig.apiKey);
                    b2.a(reporterConfig);
                    t = b2;
                }
            }
        }
        return t;
    }
}
