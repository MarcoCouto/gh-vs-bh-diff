package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;

public class io {

    /* renamed from: a reason: collision with root package name */
    private final int f4885a;
    @NonNull
    private final is b;
    @Nullable
    private ip c;

    public io(@NonNull Context context, @NonNull ek ekVar, int i) {
        this(new is(context, ekVar), i);
    }

    @NonNull
    public aj a(@NonNull String str) {
        if (this.c == null) {
            b();
        }
        int b2 = b(str);
        if (this.c.c().contains(Integer.valueOf(b2))) {
            return aj.NON_FIRST_OCCURENCE;
        }
        aj ajVar = this.c.b() ? aj.FIRST_OCCURRENCE : aj.UNKNOWN;
        if (this.c.d() < 1000) {
            this.c.b(b2);
        } else {
            this.c.a(false);
        }
        c();
        return ajVar;
    }

    public void a() {
        if (this.c == null) {
            b();
        }
        this.c.a();
        this.c.a(true);
        c();
    }

    private void b() {
        this.c = this.b.a();
        if (this.c.e() != this.f4885a) {
            this.c.a(this.f4885a);
            c();
        }
    }

    private void c() {
        this.b.a(this.c);
    }

    private int b(@NonNull String str) {
        return str.hashCode();
    }

    @VisibleForTesting
    io(@NonNull is isVar, int i) {
        this.f4885a = i;
        this.b = isVar;
    }
}
