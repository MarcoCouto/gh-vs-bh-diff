package com.yandex.metrica.impl.ob;

import android.net.Uri.Builder;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import com.facebook.internal.NativeProtocol;
import com.ironsource.sdk.constants.LocationConst;
import com.mintegral.msdk.base.entity.CampaignEx;
import com.tapjoy.TapjoyConstants;
import com.yandex.metrica.impl.ac.a.c;
import io.fabric.sdk.android.services.settings.SettingsJsonConstants;
import java.util.Map;

public class ta implements sw<su> {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final ss f5179a;

    public ta(@NonNull ss ssVar) {
        this.f5179a = ssVar;
    }

    public void a(@NonNull Builder builder, @NonNull su suVar) {
        builder.path("analytics/startup");
        builder.appendQueryParameter(this.f5179a.a("deviceid"), suVar.r());
        builder.appendQueryParameter(this.f5179a.a("deviceid2"), suVar.s());
        a(builder, al.a().h(), suVar);
        builder.appendQueryParameter(this.f5179a.a("app_platform"), suVar.l());
        builder.appendQueryParameter(this.f5179a.a("protocol_version"), suVar.h());
        builder.appendQueryParameter(this.f5179a.a("analytics_sdk_version_name"), suVar.i());
        builder.appendQueryParameter(this.f5179a.a("model"), suVar.m());
        builder.appendQueryParameter(this.f5179a.a("manufacturer"), suVar.g());
        builder.appendQueryParameter(this.f5179a.a(TapjoyConstants.TJC_DEVICE_OS_VERSION_NAME), suVar.n());
        builder.appendQueryParameter(this.f5179a.a("screen_width"), String.valueOf(suVar.w()));
        builder.appendQueryParameter(this.f5179a.a("screen_height"), String.valueOf(suVar.x()));
        builder.appendQueryParameter(this.f5179a.a("screen_dpi"), String.valueOf(suVar.y()));
        builder.appendQueryParameter(this.f5179a.a("scalefactor"), String.valueOf(suVar.z()));
        builder.appendQueryParameter(this.f5179a.a("locale"), suVar.A());
        builder.appendQueryParameter(this.f5179a.a(TapjoyConstants.TJC_DEVICE_TYPE_NAME), suVar.C());
        builder.appendQueryParameter(this.f5179a.a("queries"), String.valueOf(1));
        builder.appendQueryParameter(this.f5179a.a("query_hosts"), String.valueOf(2));
        builder.appendQueryParameter(this.f5179a.a(SettingsJsonConstants.FEATURES_KEY), cu.b(this.f5179a.a("easy_collecting"), this.f5179a.a("package_info"), this.f5179a.a("socket"), this.f5179a.a("permissions_collecting"), this.f5179a.a("features_collecting"), this.f5179a.a("foreground_location_collection"), this.f5179a.a("background_location_collection"), this.f5179a.a("foreground_lbs_collection"), this.f5179a.a("background_lbs_collection"), this.f5179a.a("telephony_restricted_to_location_tracking"), this.f5179a.a(TapjoyConstants.TJC_ANDROID_ID), this.f5179a.a("google_aid"), this.f5179a.a("wifi_around"), this.f5179a.a("wifi_connected"), this.f5179a.a("own_macs"), this.f5179a.a("cells_around"), this.f5179a.a("sim_info"), this.f5179a.a("sim_imei"), this.f5179a.a("access_point"), this.f5179a.a("sdk_list"), this.f5179a.a("identity_light_collecting"), this.f5179a.a("ble_collecting")));
        builder.appendQueryParameter(this.f5179a.a("socket"), String.valueOf(1));
        builder.appendQueryParameter(this.f5179a.a("app_id"), suVar.d());
        builder.appendQueryParameter(this.f5179a.a("foreground_location_collection"), String.valueOf(1));
        builder.appendQueryParameter(this.f5179a.a("app_debuggable"), suVar.E());
        builder.appendQueryParameter(this.f5179a.a("sdk_list"), String.valueOf(1));
        builder.appendQueryParameter(this.f5179a.a("background_location_collection"), String.valueOf(1));
        if (suVar.b()) {
            String K = suVar.K();
            if (!TextUtils.isEmpty(K)) {
                builder.appendQueryParameter(this.f5179a.a("country_init"), K);
            }
        } else {
            builder.appendQueryParameter(this.f5179a.a("detect_locale"), String.valueOf(1));
        }
        Map G = suVar.G();
        String H = suVar.H();
        if (!cx.a(G)) {
            builder.appendQueryParameter(this.f5179a.a("distribution_customization"), String.valueOf(1));
            a(builder, "clids_set", we.a(G));
            if (!TextUtils.isEmpty(H)) {
                builder.appendQueryParameter(this.f5179a.a("install_referrer"), H);
            }
        }
        a(builder, "uuid", suVar.t());
        builder.appendQueryParameter(this.f5179a.a(LocationConst.TIME), String.valueOf(1));
        builder.appendQueryParameter(this.f5179a.a("requests"), String.valueOf(1));
        builder.appendQueryParameter(this.f5179a.a("stat_sending"), String.valueOf(1));
        builder.appendQueryParameter(this.f5179a.a(NativeProtocol.RESULT_ARGS_PERMISSIONS), String.valueOf(1));
        builder.appendQueryParameter(this.f5179a.a("identity_light_collecting"), String.valueOf(1));
        builder.appendQueryParameter(this.f5179a.a("ble_collecting"), String.valueOf(1));
    }

    /* access modifiers changed from: protected */
    public void a(@NonNull Builder builder, @NonNull cs csVar, @NonNull su suVar) {
        c D = suVar.D();
        if (csVar.a()) {
            builder.appendQueryParameter(this.f5179a.a(CampaignEx.JSON_KEY_ADV_ID), "");
        } else if (D == null || TextUtils.isEmpty(D.f4620a)) {
            builder.appendQueryParameter(this.f5179a.a(CampaignEx.JSON_KEY_ADV_ID), "");
        } else {
            builder.appendQueryParameter(this.f5179a.a(CampaignEx.JSON_KEY_ADV_ID), D.f4620a);
        }
    }

    private void a(Builder builder, String str, String str2) {
        if (!TextUtils.isEmpty(str2)) {
            builder.appendQueryParameter(this.f5179a.a(str), str2);
        }
    }
}
