package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public class kh {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    public final kg f4938a;
    @Nullable
    public final String b;
    @Nullable
    public final Boolean c;

    public kh(@NonNull kg kgVar, @Nullable String str, @Nullable Boolean bool) {
        this.f4938a = kgVar;
        this.b = str;
        this.c = bool;
    }
}
