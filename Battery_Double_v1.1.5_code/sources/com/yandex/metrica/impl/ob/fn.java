package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.metrica.CounterConfiguration.a;

public class fn {
    @Nullable

    /* renamed from: a reason: collision with root package name */
    private final String f4840a;
    @NonNull
    private final String b;
    @Nullable
    private final Integer c;
    @Nullable
    private final String d;
    @NonNull
    private final a e;

    public fn(@Nullable String str, @NonNull String str2, @Nullable Integer num, @Nullable String str3, @NonNull a aVar) {
        this.f4840a = str;
        this.b = str2;
        this.c = num;
        this.d = str3;
        this.e = aVar;
    }

    @Nullable
    public String a() {
        return this.f4840a;
    }

    @NonNull
    public String b() {
        return this.b;
    }

    @Nullable
    public Integer c() {
        return this.c;
    }

    @Nullable
    public String d() {
        return this.d;
    }

    @NonNull
    public a e() {
        return this.e;
    }

    @NonNull
    public static fn a(@NonNull ed edVar) {
        fn fnVar = new fn(edVar.h().e(), edVar.g().h(), edVar.g().e(), edVar.g().f(), edVar.h().q());
        return fnVar;
    }

    public boolean equals(Object obj) {
        boolean z = true;
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        fn fnVar = (fn) obj;
        if (this.f4840a == null ? fnVar.f4840a != null : !this.f4840a.equals(fnVar.f4840a)) {
            return false;
        }
        if (!this.b.equals(fnVar.b)) {
            return false;
        }
        if (this.c == null ? fnVar.c != null : !this.c.equals(fnVar.c)) {
            return false;
        }
        if (this.d == null ? fnVar.d != null : !this.d.equals(fnVar.d)) {
            return false;
        }
        if (this.e != fnVar.e) {
            z = false;
        }
        return z;
    }

    public int hashCode() {
        int i = 0;
        int hashCode = (((((this.f4840a != null ? this.f4840a.hashCode() : 0) * 31) + this.b.hashCode()) * 31) + (this.c != null ? this.c.hashCode() : 0)) * 31;
        if (this.d != null) {
            i = this.d.hashCode();
        }
        return ((hashCode + i) * 31) + this.e.hashCode();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("ClientDescription{mApiKey='");
        sb.append(this.f4840a);
        sb.append('\'');
        sb.append(", mPackageName='");
        sb.append(this.b);
        sb.append('\'');
        sb.append(", mProcessID=");
        sb.append(this.c);
        sb.append(", mProcessSessionID='");
        sb.append(this.d);
        sb.append('\'');
        sb.append(", mReporterType=");
        sb.append(this.e);
        sb.append('}');
        return sb.toString();
    }
}
