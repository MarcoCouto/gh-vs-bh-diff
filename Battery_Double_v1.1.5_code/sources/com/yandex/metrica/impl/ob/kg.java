package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.util.Collections;
import java.util.List;

public class kg {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    public final kk f4937a;
    @NonNull
    public final List<kk> b;

    public kg(@NonNull kk kkVar, @Nullable List<kk> list) {
        this.f4937a = kkVar;
        this.b = list == null ? Collections.emptyList() : Collections.unmodifiableList(list);
    }
}
