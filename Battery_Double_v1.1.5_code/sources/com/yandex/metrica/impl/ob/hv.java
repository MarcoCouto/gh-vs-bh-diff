package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import com.yandex.metrica.impl.ob.af.a;
import java.util.List;

public class hv extends hw<hd> {
    public hv(@NonNull ig igVar) {
        super(igVar);
    }

    public void a(@NonNull a aVar, @NonNull List<hd> list) {
        if (a(aVar)) {
            list.add(a().h());
        }
        if (b(aVar)) {
            list.add(a().d());
        }
    }

    private boolean a(@NonNull a aVar) {
        return af.b(aVar);
    }

    private boolean b(@NonNull a aVar) {
        return af.a(aVar);
    }
}
