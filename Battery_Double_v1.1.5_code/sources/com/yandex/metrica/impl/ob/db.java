package com.yandex.metrica.impl.ob;

import android.annotation.SuppressLint;
import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.util.Log;
import com.yandex.metrica.AppMetricaDeviceIDListener;
import com.yandex.metrica.DeferredDeeplinkParametersListener;
import com.yandex.metrica.IIdentifierCallback;
import com.yandex.metrica.IIdentifierCallback.Reason;
import com.yandex.metrica.d;
import com.yandex.metrica.f;
import com.yandex.metrica.impl.ob.x.a;
import com.yandex.metrica.j;
import java.util.Collections;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;

public class db implements a {
    @SuppressLint({"StaticFieldLeak"})

    /* renamed from: a reason: collision with root package name */
    private static db f4769a;
    private static aa b = new aa();
    private static volatile boolean c;
    /* access modifiers changed from: private */
    public static final EnumMap<Reason, AppMetricaDeviceIDListener.Reason> d = new EnumMap<>(Reason.class);
    private static xf e = new xf();
    private final Context f;
    private final ca g;
    private ax h;
    private bh i;
    private final uf j;
    private final bx k;
    private ki l;
    private final ee m;
    /* access modifiers changed from: private */
    public IIdentifierCallback n;
    private cd o = new cd(this.m, this.f, k().b());
    @NonNull
    private t p;

    static {
        d.put(Reason.UNKNOWN, AppMetricaDeviceIDListener.Reason.UNKNOWN);
        d.put(Reason.INVALID_RESPONSE, AppMetricaDeviceIDListener.Reason.INVALID_RESPONSE);
        d.put(Reason.NETWORK, AppMetricaDeviceIDListener.Reason.NETWORK);
    }

    private db(Context context) {
        this.f = context.getApplicationContext();
        lf f2 = ld.a(this.f).f();
        vz.a(context.getApplicationContext());
        ci.a();
        com.yandex.metrica.impl.ac.a.b().a(this.f);
        Handler a2 = k().b().a();
        this.m = new ee(this.f, (ResultReceiver) new x(a2, this));
        lv lvVar = new lv(f2);
        b.a(this.o);
        new s(lvVar).a(this.f);
        this.j = new uf(this.o, lvVar, a2);
        this.o.a((ug) this.j);
        this.k = new bx(this.o, lvVar, k().b());
        ca caVar = new ca(this.f, this.m, this.o, a2, this.j);
        this.g = caVar;
        this.p = new t();
        this.p.a();
    }

    public static xh a() {
        return k().a();
    }

    public static synchronized void a(@NonNull Context context, @NonNull j jVar) {
        synchronized (db.class) {
            c(context, jVar);
            if (wk.a(jVar.crashReporting, true) && d().i == null) {
                d().i = new bh(Thread.getDefaultUncaughtExceptionHandler(), context);
                Thread.setDefaultUncaughtExceptionHandler(d().i);
            }
        }
    }

    public static synchronized void b(@NonNull Context context, @NonNull j jVar) {
        synchronized (db.class) {
            vz a2 = vr.a(jVar.apiKey);
            vp b2 = vr.b(jVar.apiKey);
            boolean d2 = b.d();
            j a3 = b.a(jVar);
            c(context, a3);
            if (f4769a.h == null) {
                f4769a.b(jVar);
                f4769a.j.a(a2);
                f4769a.a(jVar);
                f4769a.m.a(jVar);
                f4769a.a(a3, d2);
                StringBuilder sb = new StringBuilder();
                sb.append("Activate AppMetrica with APIKey ");
                sb.append(cx.b(a3.apiKey));
                Log.i("AppMetrica", sb.toString());
                if (vi.a(a3.logs)) {
                    a2.a();
                    b2.a();
                    vr.a().a();
                    vr.b().a();
                } else {
                    a2.b();
                    b2.b();
                    vr.a().b();
                    vr.b().b();
                }
            } else if (a2.c()) {
                a2.b("Appmetrica already has been activated!");
            }
        }
    }

    public static synchronized void c(Context context, @Nullable j jVar) {
        synchronized (db.class) {
            if (f4769a == null) {
                f4769a = new db(context.getApplicationContext());
                f4769a.m();
            }
        }
    }

    public static void b() {
        c = true;
    }

    public static boolean c() {
        return c;
    }

    public static synchronized db d() {
        db dbVar;
        synchronized (db.class) {
            dbVar = f4769a;
        }
        return dbVar;
    }

    public static db a(Context context) {
        c(context.getApplicationContext(), null);
        return d();
    }

    @Nullable
    public static db e() {
        return d();
    }

    public static synchronized ax f() {
        ax axVar;
        synchronized (db.class) {
            axVar = d().h;
        }
        return axVar;
    }

    static synchronized boolean g() {
        boolean z;
        synchronized (db.class) {
            z = (f4769a == null || f4769a.h == null) ? false : true;
        }
        return z;
    }

    private void m() {
        as.a();
        k().b().a((Runnable) new vv.a(this.f));
    }

    private void a(j jVar) {
        if (jVar != null) {
            this.j.a(jVar.d);
            this.j.a(jVar.b);
            this.j.a(jVar.c);
        }
    }

    public void a(@NonNull f fVar) {
        this.g.a(fVar);
    }

    public d b(@NonNull f fVar) {
        return this.g.b(fVar);
    }

    public void a(String str) {
        this.k.a(str);
    }

    @VisibleForTesting
    static ba h() {
        return g() ? d().h : b;
    }

    public static void a(Location location) {
        h().a(location);
    }

    public static void a(boolean z) {
        h().a(z);
    }

    public void b(boolean z) {
        h().a(z);
    }

    public void c(boolean z) {
        h().setStatisticsSending(z);
    }

    public String i() {
        return this.j.b();
    }

    public String j() {
        return this.j.a();
    }

    public void a(IIdentifierCallback iIdentifierCallback, @NonNull List<String> list) {
        this.j.a(iIdentifierCallback, list, this.m.c());
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public void a(j jVar, boolean z) {
        this.o.a(jVar.locationTracking, jVar.statisticsSending);
        this.h = this.g.a(jVar, z);
        this.j.c();
    }

    public void a(int i2, @NonNull Bundle bundle) {
        this.j.a(i2, bundle);
    }

    public void a(DeferredDeeplinkParametersListener deferredDeeplinkParametersListener) {
        this.k.a(deferredDeeplinkParametersListener);
    }

    public void a(@NonNull final AppMetricaDeviceIDListener appMetricaDeviceIDListener) {
        this.n = new IIdentifierCallback() {
            public void onReceive(Map<String, String> map) {
                db.this.n = null;
                appMetricaDeviceIDListener.onLoaded((String) map.get("appmetrica_device_id_hash"));
            }

            public void onRequestError(Reason reason) {
                db.this.n = null;
                appMetricaDeviceIDListener.onError((AppMetricaDeviceIDListener.Reason) db.d.get(reason));
            }
        };
        this.j.a(this.n, Collections.singletonList("appmetrica_device_id_hash"), this.m.c());
    }

    private void b(@NonNull j jVar) {
        if (this.i != null) {
            this.i.a((ki) new kj(new cb(this.g, "20799a27-fa80-4b36-b2db-0f8141f24180"), new ki.a() {
                public boolean a(Throwable th) {
                    return ci.a(th);
                }
            }, null));
            this.i.a((ki) new kj(new cb(this.g, "0e5e9c33-f8c3-4568-86c5-2e4f57523f72"), new ki.a() {
                public boolean a(Throwable th) {
                    return ci.b(th);
                }
            }, null));
            if (this.l == null) {
                this.l = new kj(new cb(this.g, jVar.apiKey), new ki.a() {
                    public boolean a(Throwable th) {
                        return true;
                    }
                }, jVar.n);
            }
            this.i.a(this.l);
        }
    }

    public static xf k() {
        return e;
    }
}
