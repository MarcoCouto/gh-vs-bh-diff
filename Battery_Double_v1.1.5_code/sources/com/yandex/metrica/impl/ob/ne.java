package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import com.yandex.metrica.impl.ob.rr.a.j;

public class ne implements mv<ub, j> {
    @NonNull
    /* renamed from: a */
    public j b(@NonNull ub ubVar) {
        j jVar = new j();
        jVar.b = ubVar.f5227a;
        jVar.c = ubVar.b;
        jVar.d = vj.a(ubVar.c);
        jVar.e = ubVar.d;
        jVar.f = ubVar.e;
        return jVar;
    }

    @NonNull
    public ub a(@NonNull j jVar) {
        ub ubVar = new ub(jVar.b, jVar.c, vj.a(jVar.d), jVar.e, jVar.f);
        return ubVar;
    }
}
