package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import com.yandex.metrica.impl.ob.sn.c;

public class sq extends sn {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private String f5167a;

    protected static abstract class a<T extends sq, A extends com.yandex.metrica.impl.ob.sn.a> extends b<T, A> {
        private final yc c;

        protected a(@NonNull Context context, @NonNull String str) {
            this(context, str, new yc());
        }

        @VisibleForTesting
        protected a(@NonNull Context context, @NonNull String str, @NonNull yc ycVar) {
            super(context, str);
            this.c = ycVar;
        }

        @NonNull
        /* renamed from: b */
        public T c(@NonNull c<A> cVar) {
            T t = (sq) super.a(cVar);
            a(t);
            return t;
        }

        private void a(@NonNull T t) {
            String packageName = this.f5163a.getPackageName();
            ApplicationInfo b = this.c.b(this.f5163a, this.b, 0);
            if (b != null) {
                t.l(a(b));
            } else if (TextUtils.equals(packageName, this.b)) {
                t.l(a(this.f5163a.getApplicationInfo()));
            } else {
                t.l("0");
            }
        }

        @NonNull
        private String a(@NonNull ApplicationInfo applicationInfo) {
            return (applicationInfo.flags & 2) != 0 ? "1" : "0";
        }
    }

    @NonNull
    public String E() {
        return this.f5167a;
    }

    /* access modifiers changed from: 0000 */
    public void l(@NonNull String str) {
        this.f5167a = str;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("CoreRequestConfig{mAppDebuggable='");
        sb.append(this.f5167a);
        sb.append('\'');
        sb.append(super.toString());
        sb.append('}');
        return sb.toString();
    }
}
