package com.yandex.metrica.impl.ob;

import android.annotation.SuppressLint;
import android.content.ComponentName;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.ServiceInfo;
import com.yandex.metrica.MetricaService;

public class vv {

    /* renamed from: a reason: collision with root package name */
    private static final yc f5279a = new yc();

    public static final class a implements Runnable {

        /* renamed from: a reason: collision with root package name */
        final Context f5280a;

        public a(Context context) {
            this.f5280a = context;
        }

        public void run() {
            vv.a(this.f5280a);
        }
    }

    public static void a(Context context, ComponentName componentName) {
        f5279a.a(context, componentName, 1, 1);
    }

    public static void a(Context context) {
        b(context);
    }

    @SuppressLint({"InlinedApi"})
    public static void b(Context context) {
        ServiceInfo[] serviceInfoArr;
        try {
            PackageInfo a2 = f5279a.a(context, context.getPackageName(), 516);
            if (a2.services != null) {
                for (ServiceInfo serviceInfo : a2.services) {
                    if (MetricaService.class.getName().equals(serviceInfo.name) && !serviceInfo.enabled) {
                        a(context, new ComponentName(context, MetricaService.class));
                    }
                }
            }
        } catch (Throwable unused) {
        }
    }
}
