package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import com.yandex.metrica.CounterConfiguration;
import com.yandex.metrica.impl.ob.eg.a;

class fm extends fz {
    fm() {
    }

    @NonNull
    /* renamed from: a */
    public en d(@NonNull Context context, @NonNull ek ekVar, @NonNull a aVar, @NonNull bl blVar, @NonNull ul ulVar) {
        ef efVar = new ef(context, ulVar.e(), blVar, ekVar, aVar, new fj(al.a().h()), new ur());
        return efVar;
    }

    @NonNull
    /* renamed from: b */
    public ge c(@NonNull Context context, @NonNull ek ekVar, @NonNull a aVar, @NonNull bl blVar, @NonNull ul ulVar) {
        ge geVar = new ge(context, ekVar, blVar, aVar, ulVar.e(), new ur(), CounterConfiguration.a.APPMETRICA);
        return geVar;
    }
}
