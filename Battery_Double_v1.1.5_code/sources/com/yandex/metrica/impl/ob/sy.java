package com.yandex.metrica.impl.ob;

import android.net.Uri.Builder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.metrica.impl.ob.sn;

public abstract class sy<T extends sn> implements sw<T> {
    @Nullable

    /* renamed from: a reason: collision with root package name */
    private xb f5176a;

    public void a(@NonNull Builder builder, @NonNull T t) {
        if (this.f5176a != null && this.f5176a.a() == xc.AES_RSA) {
            builder.appendQueryParameter("encrypted_request", "1");
        }
    }

    public String a(Boolean bool) {
        if (bool == null) {
            return "";
        }
        return String.valueOf(bool.booleanValue() ? "1" : "0");
    }

    public void a(@NonNull xb xbVar) {
        this.f5176a = xbVar;
    }
}
