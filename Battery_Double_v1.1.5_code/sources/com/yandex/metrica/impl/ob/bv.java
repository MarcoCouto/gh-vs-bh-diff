package com.yandex.metrica.impl.ob;

import android.content.ContentValues;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import android.util.Base64;
import android.util.SparseArray;
import com.ironsource.sdk.constants.LocationConst;
import com.tapjoy.TJAdUnitConstants.String;
import com.tapjoy.TapjoyConstants;
import com.yandex.metrica.impl.ob.rh.c.C0112c;
import com.yandex.metrica.impl.ob.rh.c.e.a.C0113a;
import com.yandex.metrica.impl.ob.rh.c.e.a.b.C0114a;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class bv {

    /* renamed from: a reason: collision with root package name */
    private static Map<jh, Integer> f4701a;
    private static SparseArray<jh> b;
    /* access modifiers changed from: private */
    public static Map<aj, Integer> c;

    static class a extends e {
        /* access modifiers changed from: protected */
        public String a() {
            return "";
        }

        a() {
        }
    }

    static class b extends c {
        b() {
        }

        /* access modifiers changed from: protected */
        public byte[] b() {
            return Base64.decode(this.b, 0);
        }
    }

    static class c {
        private static final Map<com.yandex.metrica.impl.ob.af.a, Class<?>> u;
        private static final Map<com.yandex.metrica.impl.ob.af.a, Integer> v;

        /* renamed from: a reason: collision with root package name */
        protected String f4703a;
        protected String b;
        protected int c;
        protected int d;
        protected int e;
        protected long f;
        protected String g;
        protected String h;
        protected String i;
        protected Integer j;
        protected Integer k;
        protected String l;
        protected String m;
        protected int n;
        protected int o;
        protected String p;
        protected String q;
        protected String r;
        protected wz s;
        protected aj t;

        /* access modifiers changed from: protected */
        public int f() {
            return 0;
        }

        static {
            HashMap hashMap = new HashMap();
            hashMap.put(com.yandex.metrica.impl.ob.af.a.EVENT_TYPE_REGULAR, e.class);
            hashMap.put(com.yandex.metrica.impl.ob.af.a.EVENT_TYPE_SEND_REFERRER, k.class);
            hashMap.put(com.yandex.metrica.impl.ob.af.a.EVENT_TYPE_ALIVE, g.class);
            hashMap.put(com.yandex.metrica.impl.ob.af.a.EVENT_TYPE_PREV_SESSION_NATIVE_CRASH, j.class);
            hashMap.put(com.yandex.metrica.impl.ob.af.a.EVENT_TYPE_CURRENT_SESSION_NATIVE_CRASH, j.class);
            hashMap.put(com.yandex.metrica.impl.ob.af.a.EVENT_TYPE_EXCEPTION_USER, e.class);
            hashMap.put(com.yandex.metrica.impl.ob.af.a.EVENT_TYPE_EXCEPTION_USER_PROTOBUF, f.class);
            hashMap.put(com.yandex.metrica.impl.ob.af.a.EVENT_TYPE_IDENTITY, i.class);
            hashMap.put(com.yandex.metrica.impl.ob.af.a.EVENT_TYPE_STATBOX, e.class);
            hashMap.put(com.yandex.metrica.impl.ob.af.a.EVENT_TYPE_SET_USER_INFO, e.class);
            hashMap.put(com.yandex.metrica.impl.ob.af.a.EVENT_TYPE_REPORT_USER_INFO, e.class);
            hashMap.put(com.yandex.metrica.impl.ob.af.a.EVENT_TYPE_EXCEPTION_UNHANDLED, e.class);
            hashMap.put(com.yandex.metrica.impl.ob.af.a.EVENT_TYPE_EXCEPTION_UNHANDLED_PROTOBUF, f.class);
            hashMap.put(com.yandex.metrica.impl.ob.af.a.EVENT_TYPE_EXCEPTION_UNHANDLED_FROM_INTENT, f.class);
            hashMap.put(com.yandex.metrica.impl.ob.af.a.EVENT_TYPE_EXCEPTION_UNHANDLED_FROM_FILE, f.class);
            hashMap.put(com.yandex.metrica.impl.ob.af.a.EVENT_TYPE_ANR, f.class);
            hashMap.put(com.yandex.metrica.impl.ob.af.a.EVENT_TYPE_START, g.class);
            hashMap.put(com.yandex.metrica.impl.ob.af.a.EVENT_TYPE_CUSTOM_EVENT, d.class);
            hashMap.put(com.yandex.metrica.impl.ob.af.a.EVENT_TYPE_APP_OPEN, e.class);
            hashMap.put(com.yandex.metrica.impl.ob.af.a.EVENT_TYPE_PERMISSIONS, a.class);
            hashMap.put(com.yandex.metrica.impl.ob.af.a.EVENT_TYPE_APP_FEATURES, a.class);
            hashMap.put(com.yandex.metrica.impl.ob.af.a.EVENT_TYPE_SEND_USER_PROFILE, b.class);
            hashMap.put(com.yandex.metrica.impl.ob.af.a.EVENT_TYPE_SEND_REVENUE_EVENT, b.class);
            u = Collections.unmodifiableMap(hashMap);
            HashMap hashMap2 = new HashMap();
            hashMap2.put(com.yandex.metrica.impl.ob.af.a.EVENT_TYPE_INIT, Integer.valueOf(1));
            hashMap2.put(com.yandex.metrica.impl.ob.af.a.EVENT_TYPE_REGULAR, Integer.valueOf(4));
            hashMap2.put(com.yandex.metrica.impl.ob.af.a.EVENT_TYPE_SEND_REFERRER, Integer.valueOf(5));
            hashMap2.put(com.yandex.metrica.impl.ob.af.a.EVENT_TYPE_ALIVE, Integer.valueOf(7));
            hashMap2.put(com.yandex.metrica.impl.ob.af.a.EVENT_TYPE_EXCEPTION_UNHANDLED, Integer.valueOf(3));
            hashMap2.put(com.yandex.metrica.impl.ob.af.a.EVENT_TYPE_EXCEPTION_UNHANDLED_PROTOBUF, Integer.valueOf(26));
            hashMap2.put(com.yandex.metrica.impl.ob.af.a.EVENT_TYPE_EXCEPTION_UNHANDLED_FROM_INTENT, Integer.valueOf(26));
            hashMap2.put(com.yandex.metrica.impl.ob.af.a.EVENT_TYPE_EXCEPTION_UNHANDLED_FROM_FILE, Integer.valueOf(26));
            hashMap2.put(com.yandex.metrica.impl.ob.af.a.EVENT_TYPE_ANR, Integer.valueOf(25));
            hashMap2.put(com.yandex.metrica.impl.ob.af.a.EVENT_TYPE_PREV_SESSION_NATIVE_CRASH, Integer.valueOf(3));
            hashMap2.put(com.yandex.metrica.impl.ob.af.a.EVENT_TYPE_CURRENT_SESSION_NATIVE_CRASH, Integer.valueOf(3));
            hashMap2.put(com.yandex.metrica.impl.ob.af.a.EVENT_TYPE_EXCEPTION_USER, Integer.valueOf(6));
            hashMap2.put(com.yandex.metrica.impl.ob.af.a.EVENT_TYPE_EXCEPTION_USER_PROTOBUF, Integer.valueOf(27));
            hashMap2.put(com.yandex.metrica.impl.ob.af.a.EVENT_TYPE_IDENTITY, Integer.valueOf(8));
            hashMap2.put(com.yandex.metrica.impl.ob.af.a.EVENT_TYPE_IDENTITY_LIGHT, Integer.valueOf(28));
            hashMap2.put(com.yandex.metrica.impl.ob.af.a.EVENT_TYPE_STATBOX, Integer.valueOf(11));
            hashMap2.put(com.yandex.metrica.impl.ob.af.a.EVENT_TYPE_SET_USER_INFO, Integer.valueOf(12));
            hashMap2.put(com.yandex.metrica.impl.ob.af.a.EVENT_TYPE_REPORT_USER_INFO, Integer.valueOf(12));
            hashMap2.put(com.yandex.metrica.impl.ob.af.a.EVENT_TYPE_FIRST_ACTIVATION, Integer.valueOf(13));
            hashMap2.put(com.yandex.metrica.impl.ob.af.a.EVENT_TYPE_START, Integer.valueOf(2));
            hashMap2.put(com.yandex.metrica.impl.ob.af.a.EVENT_TYPE_APP_OPEN, Integer.valueOf(16));
            hashMap2.put(com.yandex.metrica.impl.ob.af.a.EVENT_TYPE_APP_UPDATE, Integer.valueOf(17));
            hashMap2.put(com.yandex.metrica.impl.ob.af.a.EVENT_TYPE_PERMISSIONS, Integer.valueOf(18));
            hashMap2.put(com.yandex.metrica.impl.ob.af.a.EVENT_TYPE_APP_FEATURES, Integer.valueOf(19));
            hashMap2.put(com.yandex.metrica.impl.ob.af.a.EVENT_TYPE_SEND_USER_PROFILE, Integer.valueOf(20));
            hashMap2.put(com.yandex.metrica.impl.ob.af.a.EVENT_TYPE_SEND_REVENUE_EVENT, Integer.valueOf(21));
            hashMap2.put(com.yandex.metrica.impl.ob.af.a.EVENT_TYPE_CLEANUP, Integer.valueOf(29));
            v = Collections.unmodifiableMap(hashMap2);
        }

        static c a(int i2, boolean z) {
            c cVar;
            com.yandex.metrica.impl.ob.af.a a2 = com.yandex.metrica.impl.ob.af.a.a(i2);
            Class a3 = a(a2, z);
            Integer num = (Integer) v.get(a2);
            try {
                cVar = (c) a3.getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
            } catch (Throwable unused) {
                cVar = new c();
            }
            return cVar.a(num);
        }

        private static Class<?> a(com.yandex.metrica.impl.ob.af.a aVar, boolean z) {
            switch (aVar) {
                case EVENT_TYPE_INIT:
                case EVENT_TYPE_FIRST_ACTIVATION:
                case EVENT_TYPE_APP_UPDATE:
                    if (z) {
                        return e.class;
                    }
                    return h.class;
                default:
                    return (Class) u.get(aVar);
            }
        }

        /* access modifiers changed from: 0000 */
        public c a(String str) {
            this.f4703a = str;
            return this;
        }

        /* access modifiers changed from: 0000 */
        public c b(String str) {
            this.b = str;
            return this;
        }

        /* access modifiers changed from: 0000 */
        public c a(int i2) {
            this.c = i2;
            return this;
        }

        /* access modifiers changed from: 0000 */
        public c b(int i2) {
            this.d = i2;
            return this;
        }

        /* access modifiers changed from: 0000 */
        public c c(int i2) {
            this.e = i2;
            return this;
        }

        /* access modifiers changed from: 0000 */
        public c a(long j2) {
            this.f = j2;
            return this;
        }

        /* access modifiers changed from: 0000 */
        public c c(String str) {
            this.g = str;
            return this;
        }

        /* access modifiers changed from: 0000 */
        public c d(String str) {
            this.i = str;
            return this;
        }

        /* access modifiers changed from: 0000 */
        public c e(String str) {
            this.h = str;
            return this;
        }

        /* access modifiers changed from: 0000 */
        public c a(Integer num) {
            this.j = num;
            return this;
        }

        /* access modifiers changed from: 0000 */
        public c f(String str) {
            this.q = str;
            return this;
        }

        /* access modifiers changed from: 0000 */
        public c b(Integer num) {
            this.k = num;
            return this;
        }

        /* access modifiers changed from: 0000 */
        public c g(String str) {
            this.l = str;
            return this;
        }

        /* access modifiers changed from: 0000 */
        public c h(String str) {
            this.m = str;
            return this;
        }

        /* access modifiers changed from: 0000 */
        public c d(int i2) {
            this.n = i2;
            return this;
        }

        /* access modifiers changed from: 0000 */
        public c e(int i2) {
            this.o = i2;
            return this;
        }

        /* access modifiers changed from: 0000 */
        public c i(String str) {
            this.p = str;
            return this;
        }

        public c j(String str) {
            this.r = str;
            return this;
        }

        /* access modifiers changed from: 0000 */
        public c a(wz wzVar) {
            this.s = wzVar;
            return this;
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        public c a(@NonNull aj ajVar) {
            this.t = ajVar;
            return this;
        }

        /* access modifiers changed from: protected */
        public String a() {
            return this.f4703a;
        }

        /* access modifiers changed from: protected */
        public byte[] b() {
            return this.b == null ? new byte[0] : cu.c(this.b);
        }

        /* access modifiers changed from: protected */
        public Integer c() {
            return this.j;
        }

        /* access modifiers changed from: protected */
        public String d() {
            return this.l;
        }

        /* access modifiers changed from: 0000 */
        public com.yandex.metrica.impl.ob.rh.c.e.a e() {
            com.yandex.metrica.impl.ob.rh.c.e.a aVar = new com.yandex.metrica.impl.ob.rh.c.e.a();
            com.yandex.metrica.impl.ob.rh.c.e.a.b a2 = bv.a(this.o, this.p, this.i, this.h, this.q);
            com.yandex.metrica.impl.ob.rh.c.b d2 = bv.d(this.g);
            C0113a f2 = bv.f(this.m);
            if (a2 != null) {
                aVar.h = a2;
            }
            if (d2 != null) {
                aVar.g = d2;
            }
            if (a() != null) {
                aVar.e = a();
            }
            if (b() != null) {
                aVar.f = b();
            }
            if (d() != null) {
                aVar.i = d();
            }
            if (f2 != null) {
                aVar.j = f2;
            }
            aVar.d = c().intValue();
            aVar.b = (long) this.c;
            aVar.p = (long) this.d;
            aVar.q = (long) this.e;
            aVar.c = this.f;
            aVar.k = this.n;
            aVar.l = f();
            aVar.m = bv.e(this.g);
            aVar.n = this.r == null ? new byte[0] : this.r.getBytes();
            Integer num = (Integer) bv.c.get(this.t);
            if (num != null) {
                aVar.o = num.intValue();
            }
            return aVar;
        }
    }

    static class d extends e {
        d() {
        }

        /* access modifiers changed from: protected */
        public Integer c() {
            return this.k;
        }
    }

    static class e extends c {
        private wy u;

        e() {
            this(new wy(al.a().b()));
        }

        e(@NonNull wy wyVar) {
            this.u = wyVar;
        }

        /* access modifiers changed from: protected */
        public byte[] b() {
            return this.u.a(this.s).a(super.b());
        }
    }

    static class f extends c {
        private wy u;

        f() {
            this(new wy(al.a().b()));
        }

        f(@NonNull wy wyVar) {
            this.u = wyVar;
        }

        /* access modifiers changed from: protected */
        public byte[] b() {
            return this.u.a(this.s).a(Base64.decode(super.b(), 0));
        }
    }

    static class g extends c {
        /* access modifiers changed from: protected */
        public String a() {
            return "";
        }

        g() {
        }

        /* access modifiers changed from: protected */
        public byte[] b() {
            return new byte[0];
        }
    }

    static class h extends c {
        h() {
        }

        /* access modifiers changed from: protected */
        public byte[] b() {
            return new byte[0];
        }
    }

    static class i extends e {
        i() {
        }

        public int f() {
            return this.s == wz.EXTERNALLY_ENCRYPTED_EVENT_CRYPTER ? 1 : 0;
        }
    }

    static class j extends c {
        j() {
        }

        /* access modifiers changed from: protected */
        public byte[] b() {
            return cu.c(am.c(this.b));
        }
    }

    static class k extends c {
        k() {
        }

        /* access modifiers changed from: protected */
        public byte[] b() {
            try {
                sj a2 = sj.a(Base64.decode(this.b, 0));
                ri riVar = new ri();
                riVar.b = a2.f5159a == null ? new byte[0] : a2.f5159a.getBytes();
                riVar.d = a2.b;
                riVar.c = a2.c;
                return e.a((e) riVar);
            } catch (d unused) {
                return new byte[0];
            }
        }
    }

    public static void a(com.yandex.metrica.impl.ob.rh.c.e eVar) {
    }

    static {
        HashMap hashMap = new HashMap();
        hashMap.put(jh.FOREGROUND, Integer.valueOf(0));
        hashMap.put(jh.BACKGROUND, Integer.valueOf(1));
        f4701a = Collections.unmodifiableMap(hashMap);
        SparseArray<jh> sparseArray = new SparseArray<>();
        sparseArray.put(0, jh.FOREGROUND);
        sparseArray.put(1, jh.BACKGROUND);
        b = sparseArray;
        HashMap hashMap2 = new HashMap();
        hashMap2.put(aj.FIRST_OCCURRENCE, Integer.valueOf(1));
        hashMap2.put(aj.NON_FIRST_OCCURENCE, Integer.valueOf(0));
        hashMap2.put(aj.UNKNOWN, Integer.valueOf(-1));
        c = Collections.unmodifiableMap(hashMap2);
    }

    public static com.yandex.metrica.impl.ob.rh.c.g a(ContentValues contentValues) {
        return a(contentValues.getAsLong("start_time"), contentValues.getAsLong("server_time_offset"), contentValues.getAsBoolean("obtained_before_first_sync"));
    }

    public static com.yandex.metrica.impl.ob.rh.c.f a(vb vbVar) {
        com.yandex.metrica.impl.ob.rh.c.f fVar = new com.yandex.metrica.impl.ob.rh.c.f();
        if (vbVar.a() != null) {
            fVar.b = vbVar.a().intValue();
        }
        if (vbVar.b() != null) {
            fVar.c = vbVar.b().intValue();
        }
        if (!TextUtils.isEmpty(vbVar.d())) {
            fVar.d = vbVar.d();
        }
        fVar.e = vbVar.c();
        if (!TextUtils.isEmpty(vbVar.e())) {
            fVar.f = vbVar.e();
        }
        return fVar;
    }

    public static jh a(int i2) {
        return (jh) b.get(i2);
    }

    public static com.yandex.metrica.impl.ob.rh.d[] a(String str) {
        try {
            return a(new JSONArray(str));
        } catch (Throwable unused) {
            return null;
        }
    }

    public static com.yandex.metrica.impl.ob.rh.d[] a(JSONArray jSONArray) {
        try {
            com.yandex.metrica.impl.ob.rh.d[] dVarArr = new com.yandex.metrica.impl.ob.rh.d[jSONArray.length()];
            int i2 = 0;
            while (i2 < jSONArray.length()) {
                try {
                    dVarArr[i2] = a(jSONArray.getJSONObject(i2));
                    i2++;
                } catch (Throwable unused) {
                    return dVarArr;
                }
            }
            return dVarArr;
        } catch (Throwable unused2) {
            return null;
        }
    }

    public static com.yandex.metrica.impl.ob.rh.d a(JSONObject jSONObject) throws JSONException {
        try {
            com.yandex.metrica.impl.ob.rh.d dVar = new com.yandex.metrica.impl.ob.rh.d();
            dVar.b = jSONObject.getString("mac");
            dVar.c = jSONObject.getInt("signal_strength");
            dVar.d = jSONObject.getString("ssid");
            dVar.e = jSONObject.optBoolean("is_connected");
            dVar.f = jSONObject.optLong("last_visible_offset_seconds", 0);
            return dVar;
        } catch (Throwable unused) {
            com.yandex.metrica.impl.ob.rh.d dVar2 = new com.yandex.metrica.impl.ob.rh.d();
            dVar2.b = jSONObject.getString("mac");
            return dVar2;
        }
    }

    static C0114a b(String str) {
        try {
            if (!TextUtils.isEmpty(str)) {
                JSONObject jSONObject = new JSONObject(str);
                C0114a aVar = new C0114a();
                aVar.b = jSONObject.optString("ssid");
                switch (jSONObject.optInt("state", -1)) {
                    case 0:
                    case 1:
                    case 2:
                    case 4:
                        aVar.c = 1;
                        break;
                    case 3:
                        aVar.c = 2;
                        break;
                }
                return aVar;
            }
        } catch (Throwable unused) {
        }
        return null;
    }

    public static com.yandex.metrica.impl.ob.rh.c.g a(long j2) {
        com.yandex.metrica.impl.ob.rh.c.g gVar = new com.yandex.metrica.impl.ob.rh.c.g();
        gVar.b = j2;
        gVar.c = wi.a(j2);
        return gVar;
    }

    public static com.yandex.metrica.impl.ob.rh.c.g a(Long l, Long l2, Boolean bool) {
        com.yandex.metrica.impl.ob.rh.c.g a2 = a(l.longValue());
        if (l2 != null) {
            a2.d = l2.longValue();
        }
        if (bool != null) {
            a2.e = bool.booleanValue();
        }
        return a2;
    }

    public static com.yandex.metrica.impl.ob.rh.c.e.b a(String str, int i2, com.yandex.metrica.impl.ob.rh.c.g gVar) {
        com.yandex.metrica.impl.ob.rh.c.e.b bVar = new com.yandex.metrica.impl.ob.rh.c.e.b();
        bVar.b = gVar;
        bVar.c = str;
        bVar.d = i2;
        return bVar;
    }

    static int a(jh jhVar) {
        return ((Integer) f4701a.get(jhVar)).intValue();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:?, code lost:
        return new com.yandex.metrica.impl.ob.rh.a[]{b(new org.json.JSONObject(r3))};
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:?, code lost:
        return null;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:6:0x0012 */
    public static com.yandex.metrica.impl.ob.rh.a[] c(String str) {
        if (!TextUtils.isEmpty(str)) {
            return b(new JSONArray(str));
        }
        return null;
    }

    @Nullable
    public static com.yandex.metrica.impl.ob.rh.a[] b(JSONArray jSONArray) {
        try {
            com.yandex.metrica.impl.ob.rh.a[] aVarArr = new com.yandex.metrica.impl.ob.rh.a[jSONArray.length()];
            int i2 = 0;
            while (i2 < jSONArray.length()) {
                try {
                    JSONObject jSONObject = jSONArray.getJSONObject(i2);
                    if (jSONObject != null) {
                        aVarArr[i2] = b(jSONObject);
                    }
                    i2++;
                } catch (Throwable unused) {
                    return aVarArr;
                }
            }
            return aVarArr;
        } catch (Throwable unused2) {
            return null;
        }
    }

    @VisibleForTesting
    @NonNull
    static com.yandex.metrica.impl.ob.rh.a b(JSONObject jSONObject) {
        com.yandex.metrica.impl.ob.rh.a aVar = new com.yandex.metrica.impl.ob.rh.a();
        if (jSONObject.has("signal_strength")) {
            int optInt = jSONObject.optInt("signal_strength", aVar.c);
            if (optInt != -1) {
                aVar.c = optInt;
            }
        }
        if (jSONObject.has("cell_id")) {
            aVar.b = jSONObject.optInt("cell_id", aVar.b);
        }
        if (jSONObject.has("lac")) {
            aVar.d = jSONObject.optInt("lac", aVar.d);
        }
        if (jSONObject.has(TapjoyConstants.TJC_DEVICE_COUNTRY_CODE)) {
            aVar.e = jSONObject.optInt(TapjoyConstants.TJC_DEVICE_COUNTRY_CODE, aVar.e);
        }
        if (jSONObject.has("operator_id")) {
            aVar.f = jSONObject.optInt("operator_id", aVar.f);
        }
        if (jSONObject.has("operator_name")) {
            aVar.g = jSONObject.optString("operator_name", aVar.g);
        }
        if (jSONObject.has("is_connected")) {
            aVar.h = jSONObject.optBoolean("is_connected", aVar.h);
        }
        aVar.i = jSONObject.optInt("cell_type", 0);
        if (jSONObject.has("pci")) {
            aVar.j = jSONObject.optInt("pci", aVar.j);
        }
        if (jSONObject.has("last_visible_time_offset")) {
            aVar.k = jSONObject.optLong("last_visible_time_offset", aVar.k);
        }
        return aVar;
    }

    public static com.yandex.metrica.impl.ob.rh.c.b d(String str) {
        try {
            if (!TextUtils.isEmpty(str)) {
                com.yandex.metrica.impl.ob.vq.a aVar = new com.yandex.metrica.impl.ob.vq.a(str);
                if (aVar.c("lon") && aVar.c("lat")) {
                    com.yandex.metrica.impl.ob.rh.c.b bVar = new com.yandex.metrica.impl.ob.rh.c.b();
                    try {
                        bVar.c = aVar.getDouble("lon");
                        bVar.b = aVar.getDouble("lat");
                        bVar.h = aVar.optInt(LocationConst.ALTITUDE);
                        bVar.f = aVar.optInt("direction");
                        bVar.e = aVar.optInt("precision");
                        bVar.g = aVar.optInt(LocationConst.SPEED);
                        bVar.d = aVar.optLong("timestamp") / 1000;
                        if (aVar.c("provider")) {
                            String a2 = aVar.a("provider");
                            if ("gps".equals(a2)) {
                                bVar.i = 1;
                            } else if ("network".equals(a2)) {
                                bVar.i = 2;
                            }
                        }
                        if (!aVar.c("original_provider")) {
                            return bVar;
                        }
                        bVar.j = aVar.a("original_provider");
                        return bVar;
                    } catch (Throwable unused) {
                        return bVar;
                    }
                }
            }
        } catch (Throwable unused2) {
        }
        return null;
    }

    @VisibleForTesting
    static int e(String str) {
        try {
            return new ko().a(Boolean.valueOf(new com.yandex.metrica.impl.ob.vq.a(str).getBoolean(String.ENABLED))).intValue();
        } catch (Throwable unused) {
            return -1;
        }
    }

    public static com.yandex.metrica.impl.ob.rh.c.e.a.b a(int i2, String str, String str2, String str3, String str4) {
        com.yandex.metrica.impl.ob.rh.c.e.a.b bVar = new com.yandex.metrica.impl.ob.rh.c.e.a.b();
        bVar.d = i2;
        if (str != null) {
            bVar.e = str;
        }
        com.yandex.metrica.impl.ob.rh.a[] c2 = c(str3);
        if (c2 != null) {
            bVar.b = c2;
        }
        bVar.c = a(str2);
        if (!TextUtils.isEmpty(str4)) {
            bVar.f = b(str4);
        }
        return bVar;
    }

    public static C0113a f(String str) {
        try {
            if (!TextUtils.isEmpty(str)) {
                com.yandex.metrica.i a2 = wj.a(str);
                C0113a aVar = new C0113a();
                aVar.b = a2.a();
                if (!TextUtils.isEmpty(a2.b())) {
                    aVar.c = a2.b();
                }
                if (!cx.a(a2.c())) {
                    aVar.d = vq.b(a2.c());
                }
                return aVar;
            }
        } catch (Throwable unused) {
        }
        return null;
    }

    public static C0112c[] a(Context context) {
        List b2 = cy.a(context).b();
        if (cx.a((Collection) b2)) {
            return null;
        }
        C0112c[] cVarArr = new C0112c[b2.size()];
        for (int i2 = 0; i2 < b2.size(); i2++) {
            C0112c cVar = new C0112c();
            com.yandex.metrica.impl.ob.cy.a aVar = (com.yandex.metrica.impl.ob.cy.a) b2.get(i2);
            cVar.b = aVar.f4766a;
            cVar.c = aVar.b;
            cVarArr[i2] = cVar;
        }
        return cVarArr;
    }
}
