package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.os.Build.VERSION;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import java.util.List;

public class pv {
    @Nullable
    public List<pu> a(Context context, @Nullable List<pu> list) {
        List<pu> a2 = a(context).a();
        if (vj.a(a2, list)) {
            return null;
        }
        return a2;
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public pt a(Context context) {
        if (VERSION.SDK_INT >= 16) {
            return new pw(context);
        }
        return new pz(context);
    }
}
