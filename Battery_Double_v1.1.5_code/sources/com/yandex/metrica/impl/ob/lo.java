package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.support.annotation.WorkerThread;
import java.io.Closeable;

public class lo implements lm {

    /* renamed from: a reason: collision with root package name */
    private final Context f4971a;
    private final String b;
    @NonNull
    private final la c;
    @NonNull
    private final ln d;
    private lc e;

    public lo(Context context, String str) {
        this(context, str, new ln(context, str), lq.a());
    }

    @VisibleForTesting
    public lo(@NonNull Context context, @NonNull String str, @NonNull ln lnVar, @NonNull la laVar) {
        this.f4971a = context;
        this.b = str;
        this.d = lnVar;
        this.c = laVar;
    }

    @Nullable
    @WorkerThread
    public synchronized SQLiteDatabase a() {
        try {
            this.d.a();
            this.e = new lc(this.f4971a, this.b, this.c.c());
        } catch (Throwable unused) {
            return null;
        }
        return this.e.getWritableDatabase();
    }

    @WorkerThread
    public synchronized void a(@Nullable SQLiteDatabase sQLiteDatabase) {
        cx.b(sQLiteDatabase);
        cx.a((Closeable) this.e);
        this.d.b();
        this.e = null;
    }
}
