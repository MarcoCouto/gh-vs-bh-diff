package com.yandex.metrica.impl.ob;

public class qk {

    /* renamed from: a reason: collision with root package name */
    private final String f5077a;
    private final String b;

    public qk(String str) {
        this(str, null);
    }

    public qk(String str, String str2) {
        this.f5077a = str;
        this.b = a(str2);
    }

    public String a() {
        return this.f5077a;
    }

    public String b() {
        return this.b;
    }

    public final String a(String str) {
        if (str == null) {
            return this.f5077a;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(this.f5077a);
        sb.append(str);
        return sb.toString();
    }
}
