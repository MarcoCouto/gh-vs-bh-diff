package com.yandex.metrica.impl.ob;

import android.support.annotation.Nullable;
import com.tapjoy.TapjoyConstants;

public class vp extends vs {

    /* renamed from: a reason: collision with root package name */
    private static final vp f5273a = new vp();

    public String f() {
        return "AppMetricaInternal";
    }

    public vp(@Nullable String str) {
        super(str);
    }

    private vp() {
        this("");
    }

    public static vp h() {
        return f5273a;
    }

    /* access modifiers changed from: protected */
    public boolean d() {
        return super.d() && "publicProd".startsWith(TapjoyConstants.LOG_LEVEL_INTERNAL);
    }
}
