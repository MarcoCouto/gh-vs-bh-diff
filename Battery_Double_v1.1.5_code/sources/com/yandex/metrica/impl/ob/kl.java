package com.yandex.metrica.impl.ob;

import android.support.annotation.Nullable;

public class kl {
    @Nullable

    /* renamed from: a reason: collision with root package name */
    public final String f4942a;
    @Nullable
    public final Throwable b;
    @Nullable
    public final kg c;
    @Nullable
    public final String d;
    @Nullable
    public final Boolean e;

    public kl(@Nullable Throwable th, @Nullable kg kgVar, @Nullable String str, @Nullable Boolean bool) {
        this.b = th;
        if (th == null) {
            this.f4942a = "";
        } else {
            this.f4942a = th.getClass().getName();
        }
        this.c = kgVar;
        this.d = str;
        this.e = bool;
    }

    @Nullable
    @Deprecated
    public Throwable a() {
        return this.b;
    }

    public String toString() {
        StackTraceElement[] b2;
        StringBuilder sb = new StringBuilder();
        if (this.b != null) {
            for (StackTraceElement stackTraceElement : cx.b(this.b)) {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("at ");
                sb2.append(stackTraceElement.getClassName());
                sb2.append(".");
                sb2.append(stackTraceElement.getMethodName());
                sb2.append("(");
                sb2.append(stackTraceElement.getFileName());
                sb2.append(":");
                sb2.append(stackTraceElement.getLineNumber());
                sb2.append(")\n");
                sb.append(sb2.toString());
            }
        }
        StringBuilder sb3 = new StringBuilder();
        sb3.append("UnhandledException{errorName='");
        sb3.append(this.f4942a);
        sb3.append('\'');
        sb3.append(", exception=");
        sb3.append(this.b);
        sb3.append("\n");
        sb3.append(sb.toString());
        sb3.append('}');
        return sb3.toString();
    }
}
