package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;

public class hh extends hd {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final io f4873a;

    public hh(@NonNull en enVar) {
        this(enVar, enVar.z());
    }

    public boolean a(@NonNull w wVar) {
        if (!TextUtils.isEmpty(wVar.d())) {
            wVar.a(this.f4873a.a(wVar.d()));
        }
        return false;
    }

    @VisibleForTesting
    hh(@NonNull en enVar, @NonNull io ioVar) {
        super(enVar);
        this.f4873a = ioVar;
    }
}
