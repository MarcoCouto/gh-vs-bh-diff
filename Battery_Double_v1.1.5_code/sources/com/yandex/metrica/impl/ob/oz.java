package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import com.yandex.metrica.impl.ob.rh.b.a;
import java.util.concurrent.TimeUnit;
import org.json.JSONArray;

public class oz {
    @NonNull
    public a a(@NonNull on onVar) {
        a aVar = new a();
        aVar.b = onVar.a() == null ? aVar.b : onVar.a().longValue();
        aVar.c = TimeUnit.MILLISECONDS.toSeconds(onVar.b());
        aVar.f = TimeUnit.MILLISECONDS.toSeconds(onVar.e());
        JSONArray d = onVar.d();
        if (d != null) {
            aVar.d = bv.b(d);
        }
        JSONArray c = onVar.c();
        if (c != null) {
            aVar.e = bv.a(c);
        }
        return aVar;
    }
}
