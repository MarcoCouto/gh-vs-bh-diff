package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import com.yandex.metrica.impl.ob.et;
import java.io.Closeable;

public class cv<C extends et> implements Closeable {

    /* renamed from: a reason: collision with root package name */
    final Object f4754a = new Object();
    @NonNull
    final bl b;
    boolean c = false;
    @NonNull
    private C d;
    @NonNull
    private final uq e;

    /* access modifiers changed from: 0000 */
    public void a() {
    }

    public cv(@NonNull C c2, @NonNull uq uqVar, @NonNull bl blVar) {
        this.d = c2;
        this.e = uqVar;
        this.b = blVar;
    }

    public void close() {
        synchronized (this.f4754a) {
            if (!this.c) {
                a();
                if (this.b.isAlive()) {
                    this.b.a();
                }
                this.c = true;
            }
        }
    }

    public void e() {
        synchronized (this.f4754a) {
            if (!this.c) {
                f();
                a();
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void f() {
        synchronized (this.f4754a) {
            if (!this.c) {
                c();
            }
        }
    }

    /* access modifiers changed from: 0000 */
    public void c() {
        this.e.a(this.b);
    }

    @NonNull
    public C g() {
        return this.d;
    }
}
