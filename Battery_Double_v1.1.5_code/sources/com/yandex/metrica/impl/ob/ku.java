package com.yandex.metrica.impl.ob;

import android.os.FileObserver;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import java.io.File;

public class ku {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final FileObserver f4949a;
    @NonNull
    private final File b;
    @NonNull
    private final wm<File> c;
    @NonNull
    private final xh d;

    public ku(@NonNull File file, @NonNull wm<File> wmVar) {
        this(file, wmVar, al.a().j().i());
    }

    private ku(@NonNull File file, @NonNull wm<File> wmVar, @NonNull xh xhVar) {
        this(new kb(file, wmVar), file, wmVar, xhVar, new kc());
    }

    @VisibleForTesting
    ku(@NonNull FileObserver fileObserver, @NonNull File file, @NonNull wm<File> wmVar, @NonNull xh xhVar, @NonNull kc kcVar) {
        this.f4949a = fileObserver;
        this.b = file;
        this.c = wmVar;
        this.d = xhVar;
        kcVar.a(file);
    }

    public void a() {
        this.d.a((Runnable) new kf(this.b, this.c));
        this.f4949a.startWatching();
    }

    public void b() {
        this.f4949a.stopWatching();
    }
}
