package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import com.yandex.metrica.impl.ob.sw;

public abstract class bs<T extends sw> extends bo<T> {
    @NonNull
    private final bn j;

    public bs(@NonNull bn bnVar, @NonNull T t) {
        super(t);
        this.j = bnVar;
    }

    public boolean b() {
        return this.j.a(k(), l(), m());
    }
}
