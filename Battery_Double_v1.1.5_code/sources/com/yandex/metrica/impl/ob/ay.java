package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import com.yandex.metrica.CounterConfiguration;
import com.yandex.metrica.f;

class ay extends n {
    ay(Context context, ee eeVar, @NonNull f fVar, cd cdVar) {
        this(context, cdVar, new bz(eeVar, new CounterConfiguration(fVar)), new ag(context));
    }

    @VisibleForTesting
    ay(Context context, cd cdVar, bz bzVar, @NonNull ag agVar) {
        super(context, cdVar, bzVar, agVar);
    }
}
