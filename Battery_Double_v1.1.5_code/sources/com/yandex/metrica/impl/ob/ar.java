package com.yandex.metrica.impl.ob;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.ResolveInfo;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.util.Base64;
import com.yandex.metrica.impl.ob.ad.a;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashSet;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class ar {

    /* renamed from: a reason: collision with root package name */
    private final JSONObject f4647a;
    @NonNull
    private final yc b;
    @NonNull
    private final Context c;
    @NonNull
    private final ad d;

    public ar(@NonNull Context context) {
        this(context, new ad(context), new yc());
    }

    @VisibleForTesting
    public ar(@NonNull Context context, @NonNull ad adVar, @NonNull yc ycVar) {
        this.f4647a = new JSONObject();
        this.c = context;
        this.d = adVar;
        this.b = ycVar;
    }

    public ar a() {
        try {
            f();
            d();
        } catch (Throwable unused) {
        }
        b();
        return this;
    }

    /* access modifiers changed from: 0000 */
    public ar b() {
        try {
            Object obj = Class.forName("kotlin.KotlinVersion").getDeclaredField("CURRENT").get(null);
            int intValue = ((Integer) obj.getClass().getDeclaredMethod("getMajor", new Class[0]).invoke(obj, new Object[0])).intValue();
            int intValue2 = ((Integer) obj.getClass().getDeclaredMethod("getMinor", new Class[0]).invoke(obj, new Object[0])).intValue();
            ((JSONObject) a(this.f4647a, "dfid", new JSONObject())).put("kotlin_runtime", new JSONObject().put("major", intValue).put("minor", intValue2).put("patch", ((Integer) obj.getClass().getDeclaredMethod("getPatch", new Class[0]).invoke(obj, new Object[0])).intValue()));
        } catch (Throwable unused) {
        }
        return this;
    }

    /* access modifiers changed from: 0000 */
    public ar c() throws JSONException {
        JSONArray jSONArray;
        JSONObject jSONObject = (JSONObject) a(this.f4647a, "dfid", new JSONObject());
        if (cx.a(21)) {
            jSONArray = h();
        } else {
            jSONArray = i();
        }
        jSONObject.put("cpu_abis", jSONArray);
        return this;
    }

    @TargetApi(21)
    @NonNull
    private JSONArray h() throws JSONException {
        return new JSONArray(Build.SUPPORTED_ABIS);
    }

    @NonNull
    private JSONArray i() {
        ArrayList arrayList = new ArrayList();
        if (!"unknown".equals(Build.CPU_ABI)) {
            arrayList.add(Build.CPU_ABI);
        }
        if (!"unknown".equals(Build.CPU_ABI2)) {
            arrayList.add(Build.CPU_ABI2);
        }
        return new JSONArray(arrayList);
    }

    /* access modifiers changed from: 0000 */
    public ar d() throws JSONException {
        ((JSONObject) a(this.f4647a, "dfid", new JSONObject())).put("boot_time", wi.a() / 1000);
        return this;
    }

    /* access modifiers changed from: 0000 */
    public ar a(boolean z) throws JSONException, UnsupportedEncodingException {
        JSONObject jSONObject = (JSONObject) a((JSONObject) a(this.f4647a, "dfid", new JSONObject()), "au", new JSONObject());
        JSONArray jSONArray = (JSONArray) a(jSONObject, "aun", new JSONArray());
        JSONArray jSONArray2 = (JSONArray) a(jSONObject, "ausf", new JSONArray());
        JSONArray jSONArray3 = (JSONArray) a(jSONObject, "audf", new JSONArray());
        JSONArray jSONArray4 = (JSONArray) a(jSONObject, "aulu", new JSONArray());
        JSONArray jSONArray5 = new JSONArray();
        if (z) {
            a(jSONObject, "aufi", jSONArray5);
        }
        HashSet hashSet = new HashSet();
        for (ResolveInfo resolveInfo : cx.a(this.c, new String(Base64.decode("YW5kcm9pZC5pbnRlbnQuYWN0aW9uLk1BSU4=", 0), "UTF-8"), new String(Base64.decode("YW5kcm9pZC5pbnRlbnQuY2F0ZWdvcnkuTEFVTkNIRVI=", 0), "UTF-8"))) {
            ApplicationInfo applicationInfo = resolveInfo.activityInfo.applicationInfo;
            if (hashSet.add(applicationInfo.packageName)) {
                jSONArray.put(applicationInfo.packageName);
                boolean z2 = (applicationInfo.flags & 1) == 1;
                jSONArray2.put(z2);
                jSONArray4.put(new File(applicationInfo.sourceDir).lastModified());
                jSONArray3.put(true ^ applicationInfo.enabled);
                if (z) {
                    if (z2) {
                        jSONArray5.put(0);
                    } else {
                        PackageInfo a2 = this.b.a(this.c, applicationInfo.packageName);
                        if (a2 == null) {
                            jSONArray5.put(0);
                        } else {
                            jSONArray5.put(a2.firstInstallTime / 1000);
                        }
                    }
                }
            }
        }
        return this;
    }

    /* access modifiers changed from: 0000 */
    public ar e() throws JSONException {
        JSONObject jSONObject = (JSONObject) a((JSONObject) a(this.f4647a, "dfid", new JSONObject()), "apps", new JSONObject());
        JSONArray jSONArray = (JSONArray) a(jSONObject, "names", new JSONArray());
        JSONArray jSONArray2 = (JSONArray) a(jSONObject, "system_flags", new JSONArray());
        JSONArray jSONArray3 = (JSONArray) a(jSONObject, "disabled_flags", new JSONArray());
        JSONArray jSONArray4 = (JSONArray) a(jSONObject, "first_install_time", new JSONArray());
        JSONArray jSONArray5 = (JSONArray) a(jSONObject, "last_update_time", new JSONArray());
        jSONObject.put("version", 0);
        for (PackageInfo packageInfo : cx.a(this.c)) {
            jSONArray.put(packageInfo.packageName);
            jSONArray2.put((packageInfo.applicationInfo.flags & 1) == 1);
            jSONArray3.put(!packageInfo.applicationInfo.enabled);
            a(jSONArray4, packageInfo);
            b(jSONArray5, packageInfo);
        }
        return this;
    }

    private void a(JSONArray jSONArray, PackageInfo packageInfo) {
        jSONArray.put(packageInfo.firstInstallTime / 1000);
    }

    private void b(JSONArray jSONArray, PackageInfo packageInfo) {
        jSONArray.put(packageInfo.lastUpdateTime / 1000);
    }

    /* access modifiers changed from: 0000 */
    public ar f() throws JSONException {
        JSONObject jSONObject = (JSONObject) a(this.f4647a, "dfid", new JSONObject());
        a a2 = this.d.a();
        jSONObject.put("tds", a2.f4633a);
        jSONObject.put("fds", a2.b);
        return this;
    }

    static <T> T a(JSONObject jSONObject, String str, T t) throws JSONException {
        if (!jSONObject.has(str)) {
            jSONObject.put(str, t);
        }
        return jSONObject.get(str);
    }

    public String toString() {
        return this.f4647a.toString();
    }

    public String g() {
        return this.f4647a.toString();
    }
}
