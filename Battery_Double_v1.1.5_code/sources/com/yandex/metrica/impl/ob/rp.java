package com.yandex.metrica.impl.ob;

import java.io.IOException;

public interface rp {

    public static final class a extends e {
        public C0118a[] b;
        public String[] c;

        /* renamed from: com.yandex.metrica.impl.ob.rp$a$a reason: collision with other inner class name */
        public static final class C0118a extends e {
            private static volatile C0118a[] h;
            public String b;
            public String c;
            public String d;
            public C0119a[] e;
            public long f;
            public int[] g;

            /* renamed from: com.yandex.metrica.impl.ob.rp$a$a$a reason: collision with other inner class name */
            public static final class C0119a extends e {
                private static volatile C0119a[] d;
                public String b;
                public String c;

                public static C0119a[] d() {
                    if (d == null) {
                        synchronized (c.f4711a) {
                            if (d == null) {
                                d = new C0119a[0];
                            }
                        }
                    }
                    return d;
                }

                public C0119a() {
                    e();
                }

                public C0119a e() {
                    this.b = "";
                    this.c = "";
                    this.f4803a = -1;
                    return this;
                }

                public void a(b bVar) throws IOException {
                    bVar.a(1, this.b);
                    bVar.a(2, this.c);
                    super.a(bVar);
                }

                /* access modifiers changed from: protected */
                public int c() {
                    return super.c() + b.b(1, this.b) + b.b(2, this.c);
                }

                /* renamed from: b */
                public C0119a a(a aVar) throws IOException {
                    while (true) {
                        int a2 = aVar.a();
                        if (a2 == 0) {
                            return this;
                        }
                        if (a2 == 10) {
                            this.b = aVar.i();
                        } else if (a2 == 18) {
                            this.c = aVar.i();
                        } else if (!g.a(aVar, a2)) {
                            return this;
                        }
                    }
                }
            }

            public static C0118a[] d() {
                if (h == null) {
                    synchronized (c.f4711a) {
                        if (h == null) {
                            h = new C0118a[0];
                        }
                    }
                }
                return h;
            }

            public C0118a() {
                e();
            }

            public C0118a e() {
                this.b = "";
                this.c = "";
                this.d = "";
                this.e = C0119a.d();
                this.f = 0;
                this.g = g.f4846a;
                this.f4803a = -1;
                return this;
            }

            public void a(b bVar) throws IOException {
                bVar.a(1, this.b);
                bVar.a(2, this.c);
                bVar.a(3, this.d);
                if (this.e != null && this.e.length > 0) {
                    for (C0119a aVar : this.e) {
                        if (aVar != null) {
                            bVar.a(4, (e) aVar);
                        }
                    }
                }
                bVar.a(5, this.f);
                if (this.g != null && this.g.length > 0) {
                    for (int a2 : this.g) {
                        bVar.a(6, a2);
                    }
                }
                super.a(bVar);
            }

            /* access modifiers changed from: protected */
            public int c() {
                int c2 = super.c() + b.b(1, this.b) + b.b(2, this.c) + b.b(3, this.d);
                if (this.e != null && this.e.length > 0) {
                    int i = c2;
                    for (C0119a aVar : this.e) {
                        if (aVar != null) {
                            i += b.b(4, (e) aVar);
                        }
                    }
                    c2 = i;
                }
                int d2 = c2 + b.d(5, this.f);
                if (this.g == null || this.g.length <= 0) {
                    return d2;
                }
                int i2 = 0;
                for (int d3 : this.g) {
                    i2 += b.d(d3);
                }
                return d2 + i2 + (this.g.length * 1);
            }

            /* renamed from: b */
            public C0118a a(a aVar) throws IOException {
                while (true) {
                    int a2 = aVar.a();
                    if (a2 == 0) {
                        return this;
                    }
                    if (a2 == 10) {
                        this.b = aVar.i();
                    } else if (a2 == 18) {
                        this.c = aVar.i();
                    } else if (a2 == 26) {
                        this.d = aVar.i();
                    } else if (a2 == 34) {
                        int b2 = g.b(aVar, 34);
                        int length = this.e == null ? 0 : this.e.length;
                        C0119a[] aVarArr = new C0119a[(b2 + length)];
                        if (length != 0) {
                            System.arraycopy(this.e, 0, aVarArr, 0, length);
                        }
                        while (length < aVarArr.length - 1) {
                            aVarArr[length] = new C0119a();
                            aVar.a((e) aVarArr[length]);
                            aVar.a();
                            length++;
                        }
                        aVarArr[length] = new C0119a();
                        aVar.a((e) aVarArr[length]);
                        this.e = aVarArr;
                    } else if (a2 == 40) {
                        this.f = aVar.e();
                    } else if (a2 == 48) {
                        int b3 = g.b(aVar, 48);
                        int[] iArr = new int[b3];
                        int i = 0;
                        for (int i2 = 0; i2 < b3; i2++) {
                            if (i2 != 0) {
                                aVar.a();
                            }
                            int g2 = aVar.g();
                            switch (g2) {
                                case 1:
                                case 2:
                                    int i3 = i + 1;
                                    iArr[i] = g2;
                                    i = i3;
                                    break;
                            }
                        }
                        if (i != 0) {
                            int length2 = this.g == null ? 0 : this.g.length;
                            if (length2 == 0 && i == iArr.length) {
                                this.g = iArr;
                            } else {
                                int[] iArr2 = new int[(length2 + i)];
                                if (length2 != 0) {
                                    System.arraycopy(this.g, 0, iArr2, 0, length2);
                                }
                                System.arraycopy(iArr, 0, iArr2, length2, i);
                                this.g = iArr2;
                            }
                        }
                    } else if (a2 == 50) {
                        int d2 = aVar.d(aVar.n());
                        int t = aVar.t();
                        int i4 = 0;
                        while (aVar.r() > 0) {
                            switch (aVar.g()) {
                                case 1:
                                case 2:
                                    i4++;
                                    break;
                            }
                        }
                        if (i4 != 0) {
                            aVar.f(t);
                            int length3 = this.g == null ? 0 : this.g.length;
                            int[] iArr3 = new int[(i4 + length3)];
                            if (length3 != 0) {
                                System.arraycopy(this.g, 0, iArr3, 0, length3);
                            }
                            while (aVar.r() > 0) {
                                int g3 = aVar.g();
                                switch (g3) {
                                    case 1:
                                    case 2:
                                        int i5 = length3 + 1;
                                        iArr3[length3] = g3;
                                        length3 = i5;
                                        break;
                                }
                            }
                            this.g = iArr3;
                        }
                        aVar.e(d2);
                    } else if (!g.a(aVar, a2)) {
                        return this;
                    }
                }
            }
        }

        public a() {
            d();
        }

        public a d() {
            this.b = C0118a.d();
            this.c = g.f;
            this.f4803a = -1;
            return this;
        }

        public void a(b bVar) throws IOException {
            if (this.b != null && this.b.length > 0) {
                for (C0118a aVar : this.b) {
                    if (aVar != null) {
                        bVar.a(1, (e) aVar);
                    }
                }
            }
            if (this.c != null && this.c.length > 0) {
                for (String str : this.c) {
                    if (str != null) {
                        bVar.a(2, str);
                    }
                }
            }
            super.a(bVar);
        }

        /* access modifiers changed from: protected */
        public int c() {
            int c2 = super.c();
            if (this.b != null && this.b.length > 0) {
                int i = c2;
                for (C0118a aVar : this.b) {
                    if (aVar != null) {
                        i += b.b(1, (e) aVar);
                    }
                }
                c2 = i;
            }
            if (this.c == null || this.c.length <= 0) {
                return c2;
            }
            int i2 = 0;
            int i3 = 0;
            for (String str : this.c) {
                if (str != null) {
                    i3++;
                    i2 += b.b(str);
                }
            }
            return c2 + i2 + (i3 * 1);
        }

        /* renamed from: b */
        public a a(a aVar) throws IOException {
            while (true) {
                int a2 = aVar.a();
                if (a2 == 0) {
                    return this;
                }
                if (a2 == 10) {
                    int b2 = g.b(aVar, 10);
                    int length = this.b == null ? 0 : this.b.length;
                    C0118a[] aVarArr = new C0118a[(b2 + length)];
                    if (length != 0) {
                        System.arraycopy(this.b, 0, aVarArr, 0, length);
                    }
                    while (length < aVarArr.length - 1) {
                        aVarArr[length] = new C0118a();
                        aVar.a((e) aVarArr[length]);
                        aVar.a();
                        length++;
                    }
                    aVarArr[length] = new C0118a();
                    aVar.a((e) aVarArr[length]);
                    this.b = aVarArr;
                } else if (a2 == 18) {
                    int b3 = g.b(aVar, 18);
                    int length2 = this.c == null ? 0 : this.c.length;
                    String[] strArr = new String[(b3 + length2)];
                    if (length2 != 0) {
                        System.arraycopy(this.c, 0, strArr, 0, length2);
                    }
                    while (length2 < strArr.length - 1) {
                        strArr[length2] = aVar.i();
                        aVar.a();
                        length2++;
                    }
                    strArr[length2] = aVar.i();
                    this.c = strArr;
                } else if (!g.a(aVar, a2)) {
                    return this;
                }
            }
        }

        public static a a(byte[] bArr) throws d {
            return (a) e.a(new a(), bArr);
        }
    }
}
