package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import com.yandex.metrica.YandexMetrica;
import com.yandex.metrica.impl.ob.st.d;
import java.io.Closeable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class en implements et, ew, ny {

    /* renamed from: a reason: collision with root package name */
    protected ly f4823a;
    private final Context b;
    private final ek c;
    private final lw d;
    private final lu e;
    private final cc f;
    private final kz g;
    private final gs h;
    private final gp i;
    private final i j;
    @NonNull
    private final a k;
    private volatile jd l;
    private final fe m;
    @NonNull
    private final io n;
    @NonNull
    private final vz o;
    @NonNull
    private final vp p;
    /* access modifiers changed from: private */
    @NonNull
    public final ff q;
    @NonNull
    private final com.yandex.metrica.impl.ob.em.a r;
    @NonNull
    private final nx s;
    @NonNull
    private final nu t;
    @NonNull
    private final nz u;
    @NonNull
    private final q v;
    @NonNull
    private final cw w;

    static class a {

        /* renamed from: a reason: collision with root package name */
        private final HashMap<String, i> f4825a = new HashMap<>();

        a() {
        }

        public synchronized i a(@NonNull ek ekVar, @NonNull vz vzVar, lw lwVar) {
            i iVar;
            iVar = (i) this.f4825a.get(ekVar.toString());
            if (iVar == null) {
                com.yandex.metrica.impl.ob.i.a d = lwVar.d();
                iVar = new i(d.f4882a, d.b, vzVar);
                this.f4825a.put(ekVar.toString(), iVar);
            }
            return iVar;
        }

        public synchronized boolean a(com.yandex.metrica.impl.ob.i.a aVar, lw lwVar) {
            if (aVar.b <= lwVar.d().b) {
                return false;
            }
            lwVar.a(aVar).q();
            return true;
        }

        public synchronized void b(com.yandex.metrica.impl.ob.i.a aVar, lw lwVar) {
            lwVar.a(aVar).q();
        }
    }

    public jd d() {
        return this.l;
    }

    @NonNull
    public ff e() {
        return this.q;
    }

    public en(@NonNull Context context, @NonNull uk ukVar, @NonNull bl blVar, @NonNull ek ekVar, @NonNull com.yandex.metrica.impl.ob.eg.a aVar, @NonNull d dVar, @NonNull un unVar) {
        a aVar2 = new a();
        cw cwVar = new cw();
        Context context2 = context;
        eo eoVar = new eo(context, ekVar, aVar, unVar, ukVar, dVar, blVar, al.a().j().g(), cx.c(context2, ekVar.b()));
        this(context2, ekVar, aVar2, cwVar, eoVar);
    }

    @VisibleForTesting
    en(@NonNull Context context, @NonNull ek ekVar, @NonNull a aVar, @NonNull cw cwVar, @NonNull eo eoVar) {
        this.b = context.getApplicationContext();
        this.c = ekVar;
        this.k = aVar;
        this.w = cwVar;
        this.o = eoVar.a().a();
        this.p = eoVar.a().b();
        this.d = eoVar.b().b();
        this.e = eoVar.b().a();
        this.f4823a = eoVar.b().c();
        this.j = aVar.a(this.c, this.o, this.d);
        this.n = eoVar.c();
        this.g = eoVar.a(this);
        this.f = eoVar.b(this);
        this.m = eoVar.c(this);
        this.r = eoVar.e(this);
        this.u = eoVar.a(this.g, this.m);
        this.t = eoVar.a(this.g);
        ArrayList arrayList = new ArrayList();
        arrayList.add(this.u);
        arrayList.add(this.t);
        this.s = eoVar.a((List<nv>) arrayList, (ny) this);
        D();
        this.l = eoVar.a(this, this.d, new com.yandex.metrica.impl.ob.jd.a() {
            public void a(@NonNull w wVar, @NonNull je jeVar) {
                en.this.q.a(wVar, jeVar);
            }
        });
        if (this.p.c()) {
            this.p.a("Read app environment for component %s. Value: %s", this.c.toString(), this.j.b().f4882a);
        }
        this.q = eoVar.a(this.d, this.l, this.g, this.j, this.f);
        this.i = eoVar.d(this);
        this.h = eoVar.a(this, this.i);
        this.v = eoVar.a(this.d);
        this.g.a();
    }

    /* access modifiers changed from: protected */
    public gp f() {
        return this.i;
    }

    public void a(@NonNull w wVar) {
        if (this.o.c()) {
            this.o.a(wVar, "Event received on service");
        }
        if (cx.a(this.c.a())) {
            this.h.b(wVar);
        }
    }

    public synchronized void a(@NonNull com.yandex.metrica.impl.ob.eg.a aVar) {
        this.m.a(aVar);
        b(aVar);
    }

    public synchronized void g() {
        this.f.e();
    }

    @Nullable
    public String h() {
        return this.d.f();
    }

    @NonNull
    public st i() {
        return (st) this.m.d();
    }

    public kz j() {
        return this.g;
    }

    public ek b() {
        return this.c;
    }

    public synchronized void a(@Nullable uk ukVar) {
        this.m.a(ukVar);
        this.s.a();
    }

    public synchronized void a(@NonNull ue ueVar, @Nullable uk ukVar) {
    }

    public Context k() {
        return this.b;
    }

    @NonNull
    public vz l() {
        return this.o;
    }

    public void m() {
        this.q.b();
    }

    public void b(w wVar) {
        this.j.a(wVar.k());
        com.yandex.metrica.impl.ob.i.a b2 = this.j.b();
        if (this.k.a(b2, this.d) && this.o.c()) {
            this.o.a("Save new app environment for %s. Value: %s", b(), b2.f4882a);
        }
    }

    public void n() {
        this.j.a();
        this.k.b(this.j.b(), this.d);
    }

    public void a(String str) {
        this.d.a(str).q();
    }

    public void o() {
        this.d.d(p() + 1).q();
        this.m.a();
    }

    public int p() {
        return this.d.i();
    }

    public boolean q() {
        return this.q.c() && i().f();
    }

    public boolean r() {
        st i2 = i();
        return i2.H() && i2.f() && this.w.a(this.q.d(), i2.J(), "need to check permissions");
    }

    public boolean s() {
        st i2 = i();
        return i2.H() && this.w.a(this.q.d(), i2.K(), "should force send permissions");
    }

    public boolean t() {
        return this.q.e() && i().I() && i().f();
    }

    public lu u() {
        return this.e;
    }

    @Deprecated
    public final qc v() {
        return new qc(this.b, this.c.a());
    }

    public boolean w() {
        return this.f4823a.a();
    }

    public boolean x() {
        boolean z = false;
        boolean b2 = this.e.b(false);
        boolean z2 = this.m.b().v;
        if (b2 && z2) {
            z = true;
        }
        return !z;
    }

    public void c() {
        cx.a((Closeable) this.f);
        cx.a((Closeable) this.g);
    }

    private void D() {
        long libraryApiLevel = (long) YandexMetrica.getLibraryApiLevel();
        if (this.d.g() < libraryApiLevel) {
            this.r.a(new qb(v())).a();
            this.d.d(libraryApiLevel).q();
        }
    }

    private void b(@NonNull com.yandex.metrica.impl.ob.eg.a aVar) {
        if (vi.a(aVar.l)) {
            this.o.a();
        } else if (vi.c(aVar.l)) {
            this.o.b();
        }
    }

    public lw y() {
        return this.d;
    }

    public void b(@Nullable String str) {
        this.d.d(str).q();
    }

    @NonNull
    public io z() {
        return this.n;
    }

    @NonNull
    public q A() {
        return this.v;
    }

    @Nullable
    public String B() {
        return this.d.h();
    }

    @NonNull
    public nx C() {
        return this.s;
    }

    @NonNull
    public com.yandex.metrica.CounterConfiguration.a a() {
        return com.yandex.metrica.CounterConfiguration.a.MANUAL;
    }
}
