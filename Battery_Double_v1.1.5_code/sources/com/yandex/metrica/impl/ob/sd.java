package com.yandex.metrica.impl.ob;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.metrica.AppMetricaDeviceIDListener;
import com.yandex.metrica.DeferredDeeplinkParametersListener;
import com.yandex.metrica.ReporterConfig;
import com.yandex.metrica.YandexMetricaConfig;

public class sd extends sf {
    private static final yk<YandexMetricaConfig> g = new yg(new yf("Config"));
    private static final yk<String> h = new yg(new ye("Native crash"));
    private static final yk<Activity> i = new yg(new yf("Activity"));
    private static final yk<Application> j = new yg(new yf("Application"));
    private static final yk<Context> k = new yg(new yf("Context"));
    private static final yk<DeferredDeeplinkParametersListener> l = new yg(new yf("Deeplink listener"));
    private static final yk<AppMetricaDeviceIDListener> m = new yg(new yf("DeviceID listener"));
    private static final yk<ReporterConfig> n = new yg(new yf("Reporter Config"));
    private static final yk<String> o = new yg(new ye("Deeplink"));
    @Deprecated
    private static final yk<String> p = new yg(new ye("Referral url"));
    private static final yk<String> q = new yg(new yl());

    public void a(@Nullable Location location) {
    }

    public void a(boolean z) {
    }

    public void a(String str) {
        h.a(str);
    }

    public void a(@NonNull Application application) {
        j.a(application);
    }

    public void a(@NonNull Activity activity) {
        i.a(activity);
    }

    public void b(@NonNull String str) {
        o.a(str);
    }

    @Deprecated
    public void c(@NonNull String str) {
        p.a(str);
    }

    public void a(@NonNull Context context, boolean z) {
        k.a(context);
    }

    public void a(@NonNull DeferredDeeplinkParametersListener deferredDeeplinkParametersListener) {
        l.a(deferredDeeplinkParametersListener);
    }

    public void a(@NonNull AppMetricaDeviceIDListener appMetricaDeviceIDListener) {
        m.a(appMetricaDeviceIDListener);
    }

    public void b(@NonNull Context context, boolean z) {
        k.a(context);
    }

    public void a(@NonNull Context context, @NonNull String str) {
        k.a(context);
        q.a(str);
    }

    public void a(@NonNull Context context, @NonNull ReporterConfig reporterConfig) {
        k.a(context);
        n.a(reporterConfig);
    }

    public void a(@NonNull Context context, @NonNull YandexMetricaConfig yandexMetricaConfig) {
        k.a(context);
        g.a(yandexMetricaConfig);
    }
}
