package com.yandex.metrica.impl.ob;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import java.lang.reflect.Proxy;

public class sl {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Context f5160a;

    public sl(@NonNull Context context) {
        this.f5160a = context.getApplicationContext();
    }

    @SuppressLint({"PrivateApi"})
    public void a(@NonNull final sk skVar) {
        try {
            Class cls = Class.forName("com.android.installreferrer.api.InstallReferrerClient");
            Object invoke = cls.getDeclaredMethod("newBuilder", new Class[]{Context.class}).invoke(cls, new Object[]{this.f5160a});
            Class cls2 = Class.forName("com.android.installreferrer.api.InstallReferrerStateListener");
            final Object invoke2 = invoke.getClass().getDeclaredMethod("build", new Class[0]).invoke(invoke, new Object[0]);
            Object newProxyInstance = Proxy.newProxyInstance(cls2.getClassLoader(), new Class[]{cls2}, new sh(invoke2, new sk() {
                public void a(@NonNull sj sjVar) {
                    skVar.a(sjVar);
                    a();
                }

                public void a(@NonNull Throwable th) {
                    skVar.a(th);
                    a();
                }

                public void a() {
                    if (invoke2 != null) {
                        try {
                            invoke2.getClass().getDeclaredMethod("endConnection", new Class[0]).invoke(invoke2, new Object[0]);
                        } catch (Throwable unused) {
                        }
                    }
                }
            }));
            invoke2.getClass().getDeclaredMethod("startConnection", new Class[]{cls2}).invoke(invoke2, new Object[]{newProxyInstance});
        } catch (Throwable th) {
            skVar.a(th);
        }
    }
}
