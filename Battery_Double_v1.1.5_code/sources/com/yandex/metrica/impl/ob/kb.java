package com.yandex.metrica.impl.ob;

import android.os.FileObserver;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import java.io.File;

public class kb extends FileObserver {

    /* renamed from: a reason: collision with root package name */
    private final wm<File> f4933a;
    private final File b;

    public kb(@NonNull File file, @NonNull wm<File> wmVar) {
        super(file.getAbsolutePath(), 8);
        this.f4933a = wmVar;
        this.b = file;
    }

    public void onEvent(int i, @Nullable String str) {
        if (i == 8 && !TextUtils.isEmpty(str)) {
            this.f4933a.a(new File(this.b, str));
        }
    }
}
