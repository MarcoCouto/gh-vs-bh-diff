package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.yandex.metrica.impl.ob.rr.a.C0121a;
import com.yandex.metrica.impl.ob.rr.a.e;
import com.yandex.metrica.impl.ob.tv.b;
import com.yandex.metrica.impl.ob.uk.a;
import java.util.List;
import org.json.JSONObject;

@Deprecated
public class lz extends lx {

    /* renamed from: a reason: collision with root package name */
    static final qk f4988a = new qk("PREF_KEY_UID_");
    static final qk b = new qk("PREF_KEY_DEVICE_ID_");
    private static final qk c = new qk("PREF_KEY_HOST_URL_");
    private static final qk d = new qk("PREF_KEY_HOST_URLS_FROM_STARTUP");
    private static final qk e = new qk("PREF_KEY_HOST_URLS_FROM_CLIENT");
    @Deprecated
    private static final qk f = new qk("PREF_KEY_REPORT_URL_");
    private static final qk g = new qk("PREF_KEY_REPORT_URLS_");
    @Deprecated
    private static final qk h = new qk("PREF_L_URL");
    private static final qk i = new qk("PREF_L_URLS");
    private static final qk j = new qk("PREF_KEY_GET_AD_URL");
    private static final qk k = new qk("PREF_KEY_REPORT_AD_URL");
    private static final qk l = new qk("PREF_KEY_STARTUP_OBTAIN_TIME_");
    private static final qk m = new qk("PREF_KEY_STARTUP_ENCODED_CLIDS_");
    private static final qk n = new qk("PREF_KEY_DISTRIBUTION_REFERRER_");
    private static final qk o = new qk("STARTUP_CLIDS_MATCH_WITH_APP_CLIDS_");
    @Deprecated
    private static final qk p = new qk("PREF_KEY_PINNING_UPDATE_URL");
    private static final qk r = new qk("PREF_KEY_EASY_COLLECTING_ENABLED_");
    private static final qk s = new qk("PREF_KEY_COLLECTING_PACKAGE_INFO_ENABLED_");
    private static final qk t = new qk("PREF_KEY_PERMISSIONS_COLLECTING_ENABLED_");
    private static final qk u = new qk("PREF_KEY_FEATURES_COLLECTING_ENABLED_");
    private static final qk v = new qk("SOCKET_CONFIG_");
    private static final qk w = new qk("LAST_STARTUP_REQUEST_CLIDS");
    private static final qk x = new qk("FLCC");
    private static final qk y = new qk("BKCC");
    private qk A = q(f4988a.a());
    private qk B = q(c.a());
    private qk C = q(d.a());
    private qk D = q(e.a());
    @Deprecated
    private qk E = q(f.a());
    private qk F = q(g.a());
    @Deprecated
    private qk G = q(h.a());
    private qk H = q(i.a());
    private qk I = q(j.a());
    private qk J = q(k.a());
    private qk K = q(l.a());
    private qk L = q(m.a());
    private qk M = q(n.a());
    private qk N = q(o.a());
    private qk O = q(r.a());
    private qk P = q(s.a());
    private qk Q = q(t.a());
    private qk R = q(u.a());
    private qk S = q(v.a());
    private qk T = q(w.a());
    private qk U = q(x.a());
    private qk V = q(y.a());
    private qk z = new qk(b.a());

    public lz(lf lfVar, String str) {
        super(lfVar, str);
    }

    public lz a(String str) {
        return (lz) b(this.A.b(), str);
    }

    @Deprecated
    public lz b(String str) {
        return (lz) b(this.z.b(), str);
    }

    @Deprecated
    public lz c(String str) {
        return (lz) b(this.E.b(), str);
    }

    public lz a(List<String> list) {
        return (lz) b(this.F.b(), vq.a(list));
    }

    public lz b(List<String> list) {
        return (lz) b(this.H.b(), vq.a(list));
    }

    public lz d(String str) {
        return (lz) b(this.J.b(), str);
    }

    public lz e(String str) {
        return (lz) b(this.I.b(), str);
    }

    public lz f(String str) {
        return (lz) b(this.B.b(), str);
    }

    public lz a(long j2) {
        return (lz) a(this.K.b(), j2);
    }

    public lz g(String str) {
        return (lz) b(this.L.b(), str);
    }

    public lz h(String str) {
        return (lz) b(this.M.b(), str);
    }

    public lz a(boolean z2) {
        return (lz) a(this.N.b(), z2);
    }

    @Deprecated
    @NonNull
    public uk a() {
        return new a(new tv.a().a(b(this.O.b(), b.f5223a)).b(b(this.P.b(), b.b)).c(b(this.Q.b(), b.c)).d(b(this.R.b(), b.d)).a()).a(s(this.A.b())).c(vq.c(s(this.C.b()))).d(vq.c(s(this.D.b()))).h(s(this.L.b())).a(vq.c(s(this.F.b()))).b(vq.c(s(this.H.b()))).e(s(this.I.b())).f(s(this.J.b())).j(c(this.M.b(), null)).a(k(s(this.U.b()))).a(l(s(this.V.b()))).a(ub.a(s(this.S.b()))).i(s(this.T.b())).b(b(this.N.b(), true)).a(b(this.K.b(), -1)).a();
    }

    @Nullable
    private oh k(@Nullable String str) {
        if (!TextUtils.isEmpty(str)) {
            try {
                return new ms().a(a(new JSONObject(str)));
            } catch (Throwable unused) {
            }
        }
        return null;
    }

    static e a(@NonNull JSONObject jSONObject) {
        e eVar = new e();
        eVar.b = wk.a(vq.a(jSONObject, "uti"), eVar.b);
        eVar.c = wk.a(vq.d(jSONObject, "udi"), eVar.c);
        eVar.d = wk.a(vq.b(jSONObject, "rcff"), eVar.d);
        eVar.e = wk.a(vq.b(jSONObject, "mbs"), eVar.e);
        eVar.f = wk.a(vq.a(jSONObject, "maff"), eVar.f);
        eVar.g = wk.a(vq.b(jSONObject, "mrsl"), eVar.g);
        eVar.h = wk.a(vq.c(jSONObject, "ce"), eVar.h);
        return eVar;
    }

    @Nullable
    private oc l(@Nullable String str) {
        if (!TextUtils.isEmpty(str)) {
            try {
                return b(new JSONObject(str));
            } catch (Throwable unused) {
            }
        }
        return null;
    }

    private oc b(@NonNull JSONObject jSONObject) {
        C0121a aVar = new C0121a();
        aVar.b = a(jSONObject);
        aVar.c = wk.a(vq.a(jSONObject, "cd"), aVar.c);
        aVar.d = wk.a(vq.a(jSONObject, "ci"), aVar.d);
        return new mn().a(aVar);
    }

    @Deprecated
    public String i(String str) {
        return c(this.E.b(), str);
    }

    @Deprecated
    public String j(String str) {
        return c(this.G.b(), str);
    }
}
