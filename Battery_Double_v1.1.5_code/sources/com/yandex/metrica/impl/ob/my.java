package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import com.yandex.metrica.impl.ob.ro.a;
import com.yandex.metrica.impl.ob.ro.a.C0117a;
import java.util.ArrayList;
import java.util.List;

public class my implements mq<List<pu>, a> {
    @NonNull
    /* renamed from: a */
    public a b(@NonNull List<pu> list) {
        a aVar = new a();
        aVar.b = new C0117a[list.size()];
        for (int i = 0; i < list.size(); i++) {
            aVar.b[i] = a((pu) list.get(i));
        }
        return aVar;
    }

    @NonNull
    public List<pu> a(@NonNull a aVar) {
        ArrayList arrayList = new ArrayList(aVar.b.length);
        for (C0117a a2 : aVar.b) {
            arrayList.add(a(a2));
        }
        return arrayList;
    }

    @NonNull
    private C0117a a(@NonNull pu puVar) {
        C0117a aVar = new C0117a();
        aVar.b = puVar.f5068a;
        aVar.c = puVar.b;
        return aVar;
    }

    @NonNull
    private pu a(@NonNull C0117a aVar) {
        return new pu(aVar.b, aVar.c);
    }
}
