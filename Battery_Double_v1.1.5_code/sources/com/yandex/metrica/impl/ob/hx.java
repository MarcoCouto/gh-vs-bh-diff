package com.yandex.metrica.impl.ob;

public class hx {

    /* renamed from: a reason: collision with root package name */
    private final gw f4879a;
    private final gx b;
    private final gv c;
    private final gy d;

    public hx(ei eiVar) {
        this.f4879a = new gw(eiVar);
        this.b = new gx(eiVar);
        this.c = new gv(eiVar);
        this.d = new gy(eiVar, al.a().h(), new ox(oo.a(eiVar.d()), al.a().k(), cy.a(eiVar.d()), new ly(ld.a(eiVar.d()).c())));
    }

    public gw a() {
        return this.f4879a;
    }

    public gv b() {
        return this.c;
    }

    public gx c() {
        return this.b;
    }

    public gt d() {
        return this.d;
    }
}
