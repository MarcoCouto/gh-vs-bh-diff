package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import com.yandex.metrica.CounterConfiguration;
import com.yandex.metrica.impl.ob.eg.a;
import com.yandex.metrica.impl.ob.st.d;

public class ef extends en {
    public ef(@NonNull Context context, @NonNull uk ukVar, @NonNull bl blVar, @NonNull ek ekVar, @NonNull a aVar, @NonNull d dVar, @NonNull un unVar) {
        a aVar2 = new a();
        cw cwVar = new cw();
        Context context2 = context;
        eo eoVar = new eo(context, ekVar, aVar, unVar, ukVar, dVar, blVar, al.a().j().g(), cx.c(context2, ekVar.b()));
        this(context2, ekVar, aVar2, cwVar, eoVar);
    }

    @VisibleForTesting
    ef(@NonNull Context context, @NonNull ek ekVar, @NonNull a aVar, @NonNull cw cwVar, @NonNull eo eoVar) {
        super(context, ekVar, aVar, cwVar, eoVar);
    }

    @NonNull
    public CounterConfiguration.a a() {
        return CounterConfiguration.a.APPMETRICA;
    }
}
