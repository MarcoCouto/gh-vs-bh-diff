package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import com.yandex.metrica.impl.ob.st.c;
import com.yandex.metrica.impl.ob.st.d;
import java.util.List;

class eo {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    protected final Context f4826a;
    @NonNull
    private final a b;
    @NonNull
    private final b c;
    @NonNull
    private final ek d;
    @NonNull
    private final com.yandex.metrica.impl.ob.eg.a e;
    @NonNull
    private final un f;
    @NonNull
    private final uk g;
    @NonNull
    private final d h;
    @NonNull
    private final wy i;
    @NonNull
    private final bl j;
    @NonNull
    private final xh k;
    private final int l;

    static class a {
        @Nullable

        /* renamed from: a reason: collision with root package name */
        private final String f4828a;

        a(@Nullable String str) {
            this.f4828a = str;
        }

        /* access modifiers changed from: 0000 */
        public vz a() {
            return vr.a(this.f4828a);
        }

        /* access modifiers changed from: 0000 */
        public vp b() {
            return vr.b(this.f4828a);
        }
    }

    static class b {
        @NonNull

        /* renamed from: a reason: collision with root package name */
        private final ek f4829a;
        @NonNull
        private final ld b;

        b(@NonNull Context context, @NonNull ek ekVar) {
            this(ekVar, ld.a(context));
        }

        @VisibleForTesting
        b(@NonNull ek ekVar, @NonNull ld ldVar) {
            this.f4829a = ekVar;
            this.b = ldVar;
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        public lu a() {
            return new lu(this.b.b(this.f4829a));
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        public lw b() {
            return new lw(this.b.b(this.f4829a));
        }

        /* access modifiers changed from: 0000 */
        @NonNull
        public ly c() {
            return new ly(this.b.c());
        }
    }

    eo(@NonNull Context context, @NonNull ek ekVar, @NonNull com.yandex.metrica.impl.ob.eg.a aVar, @NonNull un unVar, @NonNull uk ukVar, @NonNull d dVar, @NonNull bl blVar, @NonNull xh xhVar, int i2) {
        com.yandex.metrica.impl.ob.eg.a aVar2 = aVar;
        this(context, ekVar, aVar2, unVar, ukVar, dVar, blVar, xhVar, new wy(context), i2, new a(aVar2.d), new b(context, ekVar));
    }

    @VisibleForTesting
    eo(@NonNull Context context, @NonNull ek ekVar, @NonNull com.yandex.metrica.impl.ob.eg.a aVar, @NonNull un unVar, @NonNull uk ukVar, @NonNull d dVar, @NonNull bl blVar, @NonNull xh xhVar, @NonNull wy wyVar, int i2, @NonNull a aVar2, @NonNull b bVar) {
        this.f4826a = context;
        this.d = ekVar;
        this.e = aVar;
        this.f = unVar;
        this.g = ukVar;
        this.h = dVar;
        this.j = blVar;
        this.k = xhVar;
        this.i = wyVar;
        this.l = i2;
        this.b = aVar2;
        this.c = bVar;
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public a a() {
        return this.b;
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public b b() {
        return this.c;
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public io c() {
        return new io(this.f4826a, this.d, this.l);
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public kz a(@NonNull en enVar) {
        return new kz(enVar, ld.a(this.f4826a).a(this.d));
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public cc<en> b(@NonNull en enVar) {
        return new cc<>(enVar, this.f.a(), this.j, this.k);
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public fe c(@NonNull en enVar) {
        return new fe(new c(enVar, this.h), this.g, new com.yandex.metrica.impl.ob.st.a(this.e));
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public jd a(@NonNull en enVar, @NonNull lw lwVar, @NonNull com.yandex.metrica.impl.ob.jd.a aVar) {
        return new jd(enVar, new jc(lwVar), aVar);
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public ff a(@NonNull lw lwVar, @NonNull jd jdVar, @NonNull kz kzVar, @NonNull i iVar, @NonNull final cc ccVar) {
        ff ffVar = new ff(lwVar, jdVar, kzVar, iVar, this.i, this.l, new com.yandex.metrica.impl.ob.ff.a() {
            public void a() {
                ccVar.b();
            }
        });
        return ffVar;
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public gp d(@NonNull en enVar) {
        return new gp(enVar);
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public gs<hd, en> a(@NonNull en enVar, @NonNull gp gpVar) {
        return new gs<>(gpVar, enVar);
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public com.yandex.metrica.impl.ob.em.a e(@NonNull en enVar) {
        return new com.yandex.metrica.impl.ob.em.a(enVar);
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public nu a(@NonNull kz kzVar) {
        return new nu(kzVar);
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public nz a(@NonNull kz kzVar, @NonNull fe feVar) {
        return new nz(kzVar, feVar);
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public nx a(@NonNull List<nv> list, @NonNull ny nyVar) {
        return new nx(list, nyVar);
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public q a(@NonNull lw lwVar) {
        return new q(this.f4826a, lwVar);
    }
}
