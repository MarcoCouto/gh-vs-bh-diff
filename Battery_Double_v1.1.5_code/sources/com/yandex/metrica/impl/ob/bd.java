package com.yandex.metrica.impl.ob;

import android.content.Intent;
import android.os.Bundle;
import android.os.RemoteException;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;

public class bd implements bc {

    /* renamed from: a reason: collision with root package name */
    private xh f4659a;
    /* access modifiers changed from: private */
    public bc b;

    public bd(@NonNull bc bcVar) {
        this(al.a().j().a(), bcVar);
    }

    public void a() {
        this.f4659a.a((Runnable) new wb() {
            public void a() {
                bd.this.b.a();
            }
        });
    }

    public void a(final Intent intent, final int i) {
        this.f4659a.a((Runnable) new wb() {
            public void a() {
                bd.this.b.a(intent, i);
            }
        });
    }

    public void a(final Intent intent, final int i, final int i2) {
        this.f4659a.a((Runnable) new wb() {
            public void a() {
                bd.this.b.a(intent, i, i2);
            }
        });
    }

    public void a(final Intent intent) {
        this.f4659a.a((Runnable) new wb() {
            public void a() {
                bd.this.b.a(intent);
            }
        });
    }

    public void b(final Intent intent) {
        this.f4659a.a((Runnable) new wb() {
            public void a() {
                bd.this.b.b(intent);
            }
        });
    }

    public void c(final Intent intent) {
        this.f4659a.a((Runnable) new wb() {
            public void a() {
                bd.this.b.c(intent);
            }
        });
    }

    public void b() {
        this.f4659a.a((Runnable) new wb() {
            public void a() throws Exception {
                bd.this.b.b();
            }
        });
    }

    public void a(String str, int i, String str2, Bundle bundle) {
        xh xhVar = this.f4659a;
        final String str3 = str;
        final int i2 = i;
        final String str4 = str2;
        final Bundle bundle2 = bundle;
        AnonymousClass8 r1 = new wb() {
            public void a() throws RemoteException {
                bd.this.b.a(str3, i2, str4, bundle2);
            }
        };
        xhVar.a((Runnable) r1);
    }

    public void a(final Bundle bundle) {
        this.f4659a.a((Runnable) new wb() {
            public void a() throws Exception {
                bd.this.b.a(bundle);
            }
        });
    }

    @VisibleForTesting
    bd(@NonNull xh xhVar, @NonNull bc bcVar) {
        this.f4659a = xhVar;
        this.b = bcVar;
    }
}
