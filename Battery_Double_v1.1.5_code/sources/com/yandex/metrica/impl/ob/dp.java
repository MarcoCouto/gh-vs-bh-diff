package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.util.Arrays;
import java.util.List;

public class dp implements wl<Thread, StackTraceElement[], kk> {
    @NonNull
    public kk a(@NonNull Thread thread, @Nullable StackTraceElement[] stackTraceElementArr) {
        List asList;
        String name = thread.getName();
        int priority = thread.getPriority();
        long id = thread.getId();
        String a2 = a(thread);
        Integer valueOf = Integer.valueOf(thread.getState().ordinal());
        if (stackTraceElementArr == null) {
            asList = null;
        } else {
            asList = Arrays.asList(stackTraceElementArr);
        }
        kk kkVar = new kk(name, priority, id, a2, valueOf, asList);
        return kkVar;
    }

    @NonNull
    static String a(@NonNull Thread thread) {
        ThreadGroup threadGroup = thread.getThreadGroup();
        return threadGroup != null ? threadGroup.getName() : "";
    }
}
