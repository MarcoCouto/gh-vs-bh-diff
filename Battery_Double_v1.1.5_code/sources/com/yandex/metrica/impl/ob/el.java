package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import com.yandex.metrica.impl.ob.fp;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class el<CU extends fp> {

    /* renamed from: a reason: collision with root package name */
    private final List<CU> f4815a = new CopyOnWriteArrayList();

    public void a(@NonNull CU cu) {
        this.f4815a.add(cu);
    }

    public void b(@NonNull CU cu) {
        this.f4815a.remove(cu);
    }

    public List<CU> a() {
        return this.f4815a;
    }
}
