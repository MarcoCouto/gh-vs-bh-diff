package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;

class ot {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final oq f5045a;
    @NonNull
    private final ol b;
    @NonNull
    private final ow c;
    @NonNull
    private final od d;

    public ot(@NonNull Context context, @NonNull uk ukVar, @NonNull xi xiVar, @Nullable oh ohVar, @NonNull lh lhVar, @NonNull lg lgVar, @NonNull pr prVar) {
        Context context2 = context;
        LocationManager locationManager = (LocationManager) context.getSystemService("location");
        cy a2 = cy.a(context);
        vd k = al.a().k();
        ow owVar = new ow(context, ukVar, ohVar, lhVar, lgVar, xiVar);
        oh ohVar2 = ohVar;
        this(context, xiVar, locationManager, a2, k, ohVar2, owVar, new od(ohVar2, lhVar, lgVar), prVar);
    }

    private ot(@NonNull Context context, @NonNull xi xiVar, @Nullable LocationManager locationManager, @NonNull cy cyVar, @NonNull vd vdVar, @Nullable oh ohVar, @NonNull ow owVar, @NonNull od odVar, @NonNull pr prVar) {
        oq oqVar = new oq(context, xiVar.b(), locationManager, ohVar, owVar, odVar, prVar);
        ol olVar = new ol(context, cyVar, vdVar, owVar, odVar, xiVar, ohVar);
        this(oqVar, olVar, owVar, odVar);
    }

    public void a() {
        this.f5045a.a();
        this.b.d();
    }

    @Nullable
    public Location b() {
        return this.f5045a.b();
    }

    @Nullable
    public Location c() {
        return this.f5045a.c();
    }

    public void d() {
        this.c.a();
    }

    public void e() {
        this.f5045a.d();
        this.b.a();
    }

    public void f() {
        this.f5045a.e();
        this.b.b();
    }

    @VisibleForTesting
    ot(@NonNull oq oqVar, @NonNull ol olVar, @NonNull ow owVar, @NonNull od odVar) {
        this.f5045a = oqVar;
        this.b = olVar;
        this.c = owVar;
        this.d = odVar;
    }

    public void a(@NonNull uk ukVar, @Nullable oh ohVar) {
        this.c.a(ukVar, ohVar);
        this.d.a(ohVar);
        this.f5045a.a(ohVar);
        this.b.a(ohVar);
    }
}
