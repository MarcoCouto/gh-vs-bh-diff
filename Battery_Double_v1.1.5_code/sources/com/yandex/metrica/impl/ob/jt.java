package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;

public class jt {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final xh f4922a;
    @NonNull
    private final jp b;

    public interface a {
        void a();
    }

    public jt(@NonNull xh xhVar, @NonNull jp jpVar) {
        this.f4922a = xhVar;
        this.b = jpVar;
    }

    public void a(long j, @NonNull final a aVar) {
        this.f4922a.a(new Runnable() {
            public void run() {
                try {
                    aVar.a();
                } catch (Throwable unused) {
                }
            }
        }, j);
    }

    public void a(long j, boolean z) {
        this.b.a(j, z);
    }

    public void a() {
        this.b.a();
    }
}
