package com.yandex.metrica.impl.ob;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import java.util.ArrayList;
import java.util.List;

public class li {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final lm f4967a;
    @NonNull
    private String b;

    public li(lc lcVar, @NonNull String str) {
        this((lm) new lp(lcVar), str);
    }

    @VisibleForTesting
    li(@NonNull lm lmVar, @NonNull String str) {
        this.f4967a = lmVar;
        this.b = str;
    }

    @Nullable
    public List<pu> a() {
        Cursor cursor;
        SQLiteDatabase sQLiteDatabase;
        Throwable th;
        try {
            sQLiteDatabase = this.f4967a.a();
            if (sQLiteDatabase != null) {
                try {
                    cursor = sQLiteDatabase.query(this.b, null, null, null, null, null, null);
                    if (cursor != null) {
                        try {
                            if (cursor.moveToFirst()) {
                                ArrayList arrayList = new ArrayList();
                                do {
                                    arrayList.add(new pu(cursor.getString(cursor.getColumnIndex("name")), cursor.getLong(cursor.getColumnIndex("granted")) == 1));
                                } while (cursor.moveToNext());
                                this.f4967a.a(sQLiteDatabase);
                                cx.a(cursor);
                                return arrayList;
                            }
                        } catch (Throwable th2) {
                            th = th2;
                            this.f4967a.a(sQLiteDatabase);
                            cx.a(cursor);
                            throw th;
                        }
                    }
                } catch (Throwable th3) {
                    Throwable th4 = th3;
                    cursor = null;
                    th = th4;
                    this.f4967a.a(sQLiteDatabase);
                    cx.a(cursor);
                    throw th;
                }
                this.f4967a.a(sQLiteDatabase);
                cx.a(cursor);
                return null;
            }
            cursor = null;
        } catch (Throwable th5) {
            cursor = null;
            th = th5;
            sQLiteDatabase = null;
            this.f4967a.a(sQLiteDatabase);
            cx.a(cursor);
            throw th;
        }
        this.f4967a.a(sQLiteDatabase);
        cx.a(cursor);
        return null;
    }

    public void b() {
        SQLiteDatabase sQLiteDatabase;
        Throwable th;
        SQLiteDatabase sQLiteDatabase2 = null;
        try {
            sQLiteDatabase = this.f4967a.a();
            if (sQLiteDatabase != null) {
                try {
                    sQLiteDatabase.execSQL("DROP TABLE IF EXISTS permissions");
                } catch (Throwable th2) {
                    th = th2;
                    this.f4967a.a(sQLiteDatabase);
                    throw th;
                }
            }
            this.f4967a.a(sQLiteDatabase);
        } catch (Throwable th3) {
            Throwable th4 = th3;
            sQLiteDatabase = null;
            th = th4;
            this.f4967a.a(sQLiteDatabase);
            throw th;
        }
    }
}
