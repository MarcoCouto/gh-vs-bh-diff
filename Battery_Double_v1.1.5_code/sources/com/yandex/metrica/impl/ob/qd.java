package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public abstract class qd {
    private static final qk d = new qk("UNDEFINED_");

    /* renamed from: a reason: collision with root package name */
    protected qk f5076a;
    protected final String b;
    protected final SharedPreferences c;
    private final Map<String, Object> e = new HashMap();
    private boolean f = false;

    /* access modifiers changed from: protected */
    public abstract String f();

    public qd(Context context, String str) {
        this.b = str;
        this.c = a(context);
        this.f5076a = new qk(d.a(), this.b);
    }

    private SharedPreferences a(Context context) {
        return ql.a(context, f());
    }

    /* access modifiers changed from: protected */
    public <T extends qd> T a(String str, Object obj) {
        synchronized (this) {
            if (obj != null) {
                try {
                    this.e.put(str, obj);
                } catch (Throwable th) {
                    throw th;
                }
            }
        }
        return this;
    }

    /* access modifiers changed from: protected */
    public <T extends qd> T h(String str) {
        synchronized (this) {
            this.e.put(str, this);
        }
        return this;
    }

    /* access modifiers changed from: protected */
    public <T extends qd> T h() {
        synchronized (this) {
            this.f = true;
            this.e.clear();
        }
        return this;
    }

    /* access modifiers changed from: protected */
    public String i() {
        return this.b;
    }

    public void j() {
        synchronized (this) {
            a();
            this.e.clear();
            this.f = false;
        }
    }

    private void a() {
        Editor edit = this.c.edit();
        if (this.f) {
            edit.clear();
            a(edit);
            return;
        }
        for (Entry entry : this.e.entrySet()) {
            String str = (String) entry.getKey();
            Object value = entry.getValue();
            if (value == this) {
                edit.remove(str);
            } else if (value instanceof String) {
                edit.putString(str, (String) value);
            } else if (value instanceof Long) {
                edit.putLong(str, ((Long) value).longValue());
            } else if (value instanceof Integer) {
                edit.putInt(str, ((Integer) value).intValue());
            } else if (value instanceof Boolean) {
                edit.putBoolean(str, ((Boolean) value).booleanValue());
            } else if (value != null) {
                throw new UnsupportedOperationException();
            }
        }
        a(edit);
    }

    private void a(Editor editor) {
        editor.apply();
    }
}
