package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import com.yandex.metrica.impl.ob.rr.a.g;

public class mz implements mv<tz, g> {
    @NonNull
    /* renamed from: a */
    public g b(@NonNull tz tzVar) {
        g gVar = new g();
        gVar.b = tzVar.f5225a;
        gVar.c = tzVar.b;
        return gVar;
    }

    @NonNull
    public tz a(@NonNull g gVar) {
        return new tz(gVar.b, gVar.c);
    }
}
