package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import io.fabric.sdk.android.services.common.IdManager;
import java.io.Closeable;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

public final class cx {

    /* renamed from: a reason: collision with root package name */
    private static final yc f4756a = new yc();

    public static boolean a(Object obj) {
        return obj != null;
    }

    public static String a(Context context, String str) {
        return String.valueOf(c(context, str));
    }

    public static String b(Context context, String str) {
        PackageInfo a2 = f4756a.a(context, str);
        if (a2 == null) {
            return IdManager.DEFAULT_VERSION_NAME;
        }
        return a2.versionName;
    }

    public static int c(Context context, String str) {
        PackageInfo a2 = f4756a.a(context, str);
        if (a2 == null) {
            return 0;
        }
        return a2.versionCode;
    }

    public static String a(Throwable th) {
        String str = "";
        if (th == null) {
            return str;
        }
        StringWriter stringWriter = new StringWriter();
        PrintWriter printWriter = new PrintWriter(stringWriter);
        th.printStackTrace(printWriter);
        String obj = stringWriter.toString();
        printWriter.close();
        return obj;
    }

    @NonNull
    public static StackTraceElement[] b(@Nullable Throwable th) {
        if (th != null) {
            try {
                return th.getStackTrace();
            } catch (Throwable unused) {
            }
        }
        return new StackTraceElement[0];
    }

    public static boolean a(int i) {
        return VERSION.SDK_INT >= i;
    }

    public static boolean b(int i) {
        return VERSION.SDK_INT > i;
    }

    public static void a(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (Throwable unused) {
            }
        }
    }

    public static void a(HttpURLConnection httpURLConnection) {
        if (httpURLConnection != null) {
            try {
                httpURLConnection.disconnect();
            } catch (Throwable unused) {
            }
        }
    }

    public static void a(Cursor cursor) {
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
    }

    public static boolean a(Object obj, Object obj2) {
        if (obj == null && obj2 == null) {
            return true;
        }
        if (obj == null || obj2 == null) {
            return false;
        }
        return obj.equals(obj2);
    }

    public static List<ResolveInfo> a(Context context, String str, String str2) {
        try {
            Intent intent = new Intent(str, null);
            intent.addCategory(str2);
            return f4756a.a(context, intent, 0);
        } catch (Throwable unused) {
            return new ArrayList();
        }
    }

    public static List<PackageInfo> a(Context context) {
        return f4756a.a(context, 0);
    }

    public static void a(SQLiteDatabase sQLiteDatabase) {
        if (sQLiteDatabase != null) {
            try {
                sQLiteDatabase.endTransaction();
            } catch (Throwable unused) {
            }
        }
    }

    public static void b(@Nullable SQLiteDatabase sQLiteDatabase) {
        if (sQLiteDatabase != null) {
            try {
                sQLiteDatabase.close();
            } catch (Throwable unused) {
            }
        }
    }

    public static boolean a(Map map) {
        return map == null || map.size() == 0;
    }

    public static boolean a(Collection collection) {
        return collection == null || collection.size() == 0;
    }

    public static <T> boolean a(@Nullable T[] tArr) {
        return tArr == null || tArr.length == 0;
    }

    public static boolean a(@Nullable byte[] bArr) {
        return bArr == null || bArr.length == 0;
    }

    public static boolean b(@Nullable Cursor cursor) {
        return cursor == null || cursor.getCount() == 0;
    }

    public static boolean a(String str) {
        return !TextUtils.isEmpty(str) && !"-1".equals(str);
    }

    @NonNull
    public static String b(@Nullable String str) {
        String str2 = "";
        if (TextUtils.isEmpty(str) || str.length() != 36) {
            return str2;
        }
        StringBuilder sb = new StringBuilder(str);
        sb.replace(8, str.length() - 4, "-xxxx-xxxx-xxxx-xxxxxxxx");
        return sb.toString();
    }

    @NonNull
    public static <K, V> V a(@NonNull Map<K, V> map, @Nullable K k, @NonNull V v) {
        V v2 = map.get(k);
        return v2 == null ? v : v2;
    }

    @NonNull
    public static Set<Integer> a(@NonNull int[] iArr) {
        HashSet hashSet = new HashSet();
        for (int valueOf : iArr) {
            hashSet.add(Integer.valueOf(valueOf));
        }
        return hashSet;
    }

    public static <T> List<T> a(@NonNull List<T> list, int i) {
        if (list.size() <= i) {
            return list;
        }
        ArrayList arrayList = new ArrayList(i);
        for (int i2 = 0; i2 < i; i2++) {
            arrayList.add(list.get(i2));
        }
        return arrayList;
    }

    public static String a(@NonNull String str, int i) {
        return str.length() > i ? str.substring(0, 100) : str;
    }

    @Nullable
    public static <K, V> Map<K, V> b(@Nullable Map<K, V> map) {
        if (a((Map) map)) {
            return null;
        }
        return new HashMap(map);
    }

    @NonNull
    public static List<String> a(@NonNull String... strArr) {
        TreeSet treeSet = new TreeSet();
        Collections.addAll(treeSet, strArr);
        return Collections.unmodifiableList(new ArrayList(treeSet));
    }

    public static <T> void a(@NonNull wn<T> wnVar, @Nullable T t, @NonNull String str, @NonNull String str2) {
        if (t != null) {
            try {
                wnVar.a(t);
            } catch (Throwable unused) {
            }
        }
    }

    @Nullable
    public static <T, S> S a(@NonNull wo<T, S> woVar, @Nullable T t, @NonNull String str, @NonNull String str2) {
        if (t != null) {
            try {
                return woVar.a(t);
            } catch (Throwable unused) {
            }
        }
        return null;
    }

    @NonNull
    public static <T, S> S a(@NonNull wo<T, S> woVar, @Nullable T t, @NonNull String str, @NonNull String str2, @NonNull S s) {
        S a2 = a(woVar, t, str, str2);
        return a2 == null ? s : a2;
    }
}
