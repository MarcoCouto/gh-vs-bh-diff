package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

public interface np<T> {

    public static class a {

        /* renamed from: a reason: collision with root package name */
        private final HashMap<Class<?>, np<?>> f5007a;
        private final np<uk> b;
        private final np<com.yandex.metrica.impl.ob.rs.a> c;
        private final np<List<pu>> d;
        private final np<pn> e;
        private final np<te> f;

        /* renamed from: com.yandex.metrica.impl.ob.np$a$a reason: collision with other inner class name */
        private static final class C0110a {

            /* renamed from: a reason: collision with root package name */
            static final a f5013a = new a();
        }

        public static <T> np<T> a(Class<T> cls) {
            return C0110a.f5013a.c(cls);
        }

        public static <T> np<Collection<T>> b(Class<T> cls) {
            return C0110a.f5013a.d(cls);
        }

        private a() {
            this.f5007a = new HashMap<>();
            this.b = new np<uk>() {
                public mf<uk> a(@NonNull Context context) {
                    return new mg("startup_state", ld.a(context).b(), new no(context).a(), new nf());
                }
            };
            this.c = new np<com.yandex.metrica.impl.ob.rs.a>() {
                public mf<com.yandex.metrica.impl.ob.rs.a> a(@NonNull Context context) {
                    return new mg("provided_request_state", ld.a(context).b(), new no(context).e(), new na());
                }
            };
            this.d = new np<List<pu>>() {
                public mf<List<pu>> a(@NonNull Context context) {
                    return new mg("permission_list", ld.a(context).b(), new no(context).b(), new my());
                }
            };
            this.e = new np<pn>() {
                public mf<pn> a(@NonNull Context context) {
                    return new mg("app_permissions_state", ld.a(context).b(), new no(context).c(), new mm());
                }
            };
            this.f = new np<te>() {
                public mf<te> a(@NonNull Context context) {
                    return new mg("sdk_fingerprinting", ld.a(context).b(), new no(context).d(), new nd());
                }
            };
            this.f5007a.put(uk.class, this.b);
            this.f5007a.put(com.yandex.metrica.impl.ob.rs.a.class, this.c);
            this.f5007a.put(pu.class, this.d);
            this.f5007a.put(pn.class, this.e);
            this.f5007a.put(te.class, this.f);
        }

        /* access modifiers changed from: 0000 */
        public <T> np<T> c(Class<T> cls) {
            return (np) this.f5007a.get(cls);
        }

        /* access modifiers changed from: 0000 */
        public <T> np<Collection<T>> d(Class<T> cls) {
            return (np) this.f5007a.get(cls);
        }
    }

    mf<T> a(@NonNull Context context);
}
