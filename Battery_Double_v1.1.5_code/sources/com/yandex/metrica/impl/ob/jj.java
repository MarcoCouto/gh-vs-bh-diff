package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;

class jj {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    ly f4905a;
    @NonNull
    private final eh b;
    @NonNull
    private String c;
    @NonNull
    private ji d;

    public jj(@NonNull Context context) {
        this(context.getPackageName(), new ly(ld.a(context).c()), new ji());
    }

    @NonNull
    public Bundle a() {
        Bundle bundle = new Bundle();
        this.d.a(bundle, this.c, this.f4905a.g());
        return bundle;
    }

    @VisibleForTesting
    jj(@NonNull String str, @NonNull ly lyVar, @NonNull ji jiVar) {
        this.c = str;
        this.f4905a = lyVar;
        this.d = jiVar;
        this.b = new eh(this.c);
    }
}
