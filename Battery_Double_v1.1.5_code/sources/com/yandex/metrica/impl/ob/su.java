package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.metrica.impl.ob.sn.c;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

public class su extends sq {
    @Nullable

    /* renamed from: a reason: collision with root package name */
    private List<String> f5173a;
    @Nullable
    private List<String> b;
    @Nullable
    private String c;
    @Nullable
    private Map<String, String> d;
    @Nullable
    private List<String> e;
    private boolean f;
    private boolean g;
    private String h;
    private long i;

    public static class a extends com.yandex.metrica.impl.ob.sn.a<a, a> implements sm<a, a> {
        @Nullable

        /* renamed from: a reason: collision with root package name */
        public final String f5174a;
        @Nullable
        public final Map<String, String> b;
        public final boolean f;
        @Nullable
        public final List<String> g;

        /* renamed from: d */
        public boolean a(@NonNull a aVar) {
            return false;
        }

        public a(@NonNull ed edVar) {
            this(edVar.h().d(), edVar.h().g(), edVar.h().h(), edVar.g().d(), edVar.g().c(), edVar.g().a(), edVar.g().b());
        }

        public a(@Nullable String str, @Nullable String str2, @Nullable String str3, @Nullable String str4, @Nullable Map<String, String> map, boolean z, @Nullable List<String> list) {
            super(str, str2, str3);
            this.f5174a = str4;
            this.b = map;
            this.f = z;
            this.g = list;
        }

        public a() {
            this(null, null, null, null, null, false, null);
        }

        /* access modifiers changed from: 0000 */
        public boolean a(@NonNull a aVar) {
            return aVar.f ? aVar.f : this.f;
        }

        /* access modifiers changed from: 0000 */
        public List<String> b(@NonNull a aVar) {
            return aVar.f ? aVar.g : this.g;
        }

        @NonNull
        /* renamed from: c */
        public a b(@NonNull a aVar) {
            a aVar2 = new a((String) wk.a(this.c, aVar.c), (String) wk.a(this.d, aVar.d), (String) wk.a(this.e, aVar.e), (String) wk.a(this.f5174a, aVar.f5174a), (Map) wk.a(this.b, aVar.b), a(aVar), b(aVar));
            return aVar2;
        }
    }

    public static class b extends a<su, a> {
        public b(@NonNull Context context, @NonNull String str) {
            super(context, str);
        }

        /* access modifiers changed from: protected */
        @NonNull
        /* renamed from: a */
        public su b() {
            return new su();
        }

        /* renamed from: a */
        public su c(@NonNull c<a> cVar) {
            su suVar = (su) super.c(cVar);
            a(suVar, cVar.f5164a);
            suVar.m(wk.b(((a) cVar.b).f5174a, cVar.f5164a.s));
            suVar.a(((a) cVar.b).b);
            suVar.b(((a) cVar.b).f);
            suVar.c(((a) cVar.b).g);
            suVar.a(cVar.f5164a.u);
            suVar.a(cVar.f5164a.x);
            suVar.a(cVar.f5164a.E);
            return suVar;
        }

        /* access modifiers changed from: 0000 */
        public void a(@NonNull su suVar, @NonNull uk ukVar) {
            suVar.b(ukVar.j);
            suVar.a(ukVar.k);
        }
    }

    private su() {
        this.i = 0;
    }

    public List<String> a() {
        ArrayList arrayList = new ArrayList();
        if (!cx.a((Collection) this.f5173a)) {
            arrayList.addAll(this.f5173a);
        }
        if (!cx.a((Collection) this.b)) {
            arrayList.addAll(this.b);
        }
        arrayList.add("https://startup.mobile.yandex.net/");
        return arrayList;
    }

    public boolean b() {
        return this.g;
    }

    /* access modifiers changed from: 0000 */
    public void a(boolean z) {
        this.g = z;
    }

    /* access modifiers changed from: 0000 */
    public void a(long j) {
        if (this.i == 0) {
            this.i = j;
        }
    }

    public long c() {
        return this.i;
    }

    public long b(long j) {
        a(j);
        return c();
    }

    public List<String> F() {
        return this.b;
    }

    /* access modifiers changed from: 0000 */
    public void a(@Nullable List<String> list) {
        this.b = list;
    }

    @Nullable
    public Map<String, String> G() {
        return this.d;
    }

    /* access modifiers changed from: 0000 */
    public void a(@Nullable Map<String, String> map) {
        this.d = map;
    }

    /* access modifiers changed from: 0000 */
    public void b(@Nullable List<String> list) {
        this.f5173a = list;
    }

    @Nullable
    public String H() {
        return this.c;
    }

    /* access modifiers changed from: private */
    public void m(@Nullable String str) {
        this.c = str;
    }

    @Nullable
    public List<String> I() {
        return this.e;
    }

    public void c(@Nullable List<String> list) {
        this.e = list;
    }

    @Nullable
    public boolean J() {
        return this.f;
    }

    public void b(boolean z) {
        this.f = z;
    }

    public String K() {
        return this.h;
    }

    public void a(String str) {
        this.h = str;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("StartupRequestConfig{mStartupHostsFromStartup=");
        sb.append(this.f5173a);
        sb.append(", mStartupHostsFromClient=");
        sb.append(this.b);
        sb.append(", mDistributionReferrer='");
        sb.append(this.c);
        sb.append('\'');
        sb.append(", mClidsFromClient=");
        sb.append(this.d);
        sb.append(", mNewCustomHosts=");
        sb.append(this.e);
        sb.append(", mHasNewCustomHosts=");
        sb.append(this.f);
        sb.append(", mSuccessfulStartup=");
        sb.append(this.g);
        sb.append(", mCountryInit='");
        sb.append(this.h);
        sb.append('\'');
        sb.append(", mFirstStartupTime='");
        sb.append(this.i);
        sb.append('\'');
        sb.append('}');
        return sb.toString();
    }
}
