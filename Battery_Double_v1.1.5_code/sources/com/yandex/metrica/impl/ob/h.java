package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import java.util.concurrent.TimeUnit;

public class h {

    /* renamed from: a reason: collision with root package name */
    public static final long f4865a = TimeUnit.SECONDS.toMillis(10);
    private long b;
    @NonNull
    private final wg c;

    public static class a {
        /* access modifiers changed from: private */

        /* renamed from: a reason: collision with root package name */
        public boolean f4867a;
        @NonNull
        private final b b;
        @NonNull
        private final h c;

        public a(@NonNull Runnable runnable) {
            this(runnable, al.a().i());
        }

        @VisibleForTesting
        a(@NonNull final Runnable runnable, @NonNull h hVar) {
            this.f4867a = false;
            this.b = new b() {
                public void a() {
                    a.this.f4867a = true;
                    runnable.run();
                }
            };
            this.c = hVar;
        }

        public void a(long j, @NonNull xh xhVar) {
            if (!this.f4867a) {
                this.c.a(j, xhVar, this.b);
            }
        }
    }

    public interface b {
        void a();
    }

    public h() {
        this(new wg());
    }

    @VisibleForTesting
    h(@NonNull wg wgVar) {
        this.c = wgVar;
    }

    public void a() {
        this.b = this.c.a();
    }

    public void a(long j, @NonNull xh xhVar, @NonNull final b bVar) {
        xhVar.a(new Runnable() {
            public void run() {
                bVar.a();
            }
        }, Math.max(j - (this.c.a() - this.b), 0));
    }
}
