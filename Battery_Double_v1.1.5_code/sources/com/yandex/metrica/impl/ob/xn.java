package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import java.util.concurrent.Executor;

public class xn {
    /* access modifiers changed from: 0000 */
    @NonNull
    public xg a() {
        return new xg("YMM-MC");
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public Executor b() {
        return new xp();
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public xg c() {
        return new xg("YMM-MSTE");
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public xg d() {
        return new xg("YMM-TP");
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public xg e() {
        return new xg("YMM-UH-1");
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public xg f() {
        return new xg("YMM-CSE");
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public xg g() {
        return new xg("YMM-CTH");
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public xg h() {
        return new xg("YMM-SDCT");
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public xg i() {
        return new xg("YMM-DE");
    }

    /* access modifiers changed from: 0000 */
    @NonNull
    public xl a(@NonNull Runnable runnable) {
        return xm.a("YMM-IB", runnable);
    }
}
