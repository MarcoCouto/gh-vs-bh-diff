package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import java.util.Locale;

public class sn {

    /* renamed from: a reason: collision with root package name */
    private String f5162a;
    private v b;
    private final String c = "3.8.0";
    private final String d = "66508";
    private final String e;
    private final String f;
    private final String g;
    @NonNull
    private String h;
    private String i;
    private String j;
    private String k;
    private String l;
    private String m;
    private String n;
    private String o;
    private com.yandex.metrica.impl.ac.a.c p;
    private String q;
    private String r;
    private uk s;

    public static abstract class a<I, O> implements sm<I, O> {
        @Nullable
        public final String c;
        @Nullable
        public final String d;
        @Nullable
        public final String e;

        public a(@Nullable String str, @Nullable String str2, @Nullable String str3) {
            this.c = str;
            this.d = str2;
            this.e = str3;
        }
    }

    protected static abstract class b<T extends sn, A extends a> implements d<T, c<A>> {
        @NonNull

        /* renamed from: a reason: collision with root package name */
        final Context f5163a;
        @NonNull
        final String b;

        /* access modifiers changed from: protected */
        @NonNull
        public abstract T b();

        protected b(@NonNull Context context, @NonNull String str) {
            this.f5163a = context;
            this.b = str;
        }

        @NonNull
        /* renamed from: c */
        public T a(@NonNull c<A> cVar) {
            T b2 = b();
            v a2 = v.a(this.f5163a);
            b2.a(a2);
            b2.a(cVar.f5164a);
            b2.k(a(this.f5163a, ((a) cVar.b).c));
            b2.i(wk.b(a2.a(cVar.f5164a), ""));
            c(b2, cVar);
            a(b2, this.b, ((a) cVar.b).d, this.f5163a);
            b(b2, this.b, ((a) cVar.b).e, this.f5163a);
            b2.b(this.b);
            b2.a(com.yandex.metrica.impl.ac.a.a().c(this.f5163a));
            b2.j(aw.a(this.f5163a).a());
            return b2;
        }

        private void a(@NonNull T t, @NonNull String str, @Nullable String str2, @NonNull Context context) {
            if (TextUtils.isEmpty(str2)) {
                str2 = cx.b(context, str);
            }
            t.d(str2);
        }

        private void b(@NonNull T t, @NonNull String str, @Nullable String str2, @NonNull Context context) {
            if (TextUtils.isEmpty(str2)) {
                str2 = cx.a(context, str);
            }
            t.c(str2);
        }

        private synchronized void c(@NonNull T t, @NonNull c<A> cVar) {
            t.e(a(cVar));
            a(t, cVar);
            b(t, cVar);
        }

        /* access modifiers changed from: 0000 */
        public void a(T t, @NonNull c<A> cVar) {
            t.f(cVar.f5164a.b);
            t.h(cVar.f5164a.d);
        }

        /* access modifiers changed from: 0000 */
        public void b(T t, @NonNull c<A> cVar) {
            t.g(cVar.f5164a.c);
        }

        private String a(@NonNull c<A> cVar) {
            return cVar.f5164a.f5236a;
        }

        /* access modifiers changed from: 0000 */
        @VisibleForTesting
        public String a(@NonNull Context context, @Nullable String str) {
            return str == null ? v.a(context).g : str;
        }
    }

    public static class c<A> {
        @NonNull

        /* renamed from: a reason: collision with root package name */
        public final uk f5164a;
        @NonNull
        public final A b;

        public c(@NonNull uk ukVar, A a2) {
            this.f5164a = ukVar;
            this.b = a2;
        }
    }

    public interface d<T extends sn, D> {
        @NonNull
        T a(D d);
    }

    public String h() {
        return "2";
    }

    public String i() {
        return "3.8.0";
    }

    public String j() {
        return "66508";
    }

    public String l() {
        return "android";
    }

    public sn() {
        this.e = TextUtils.isEmpty("") ? "public" : "public_";
        this.f = "android";
        this.g = "2";
        this.h = ci.b();
        this.q = com.yandex.metrica.b.PHONE.name().toLowerCase(Locale.US);
    }

    public String d() {
        return this.f5162a;
    }

    /* access modifiers changed from: protected */
    public void b(String str) {
        this.f5162a = str;
    }

    /* access modifiers changed from: protected */
    public uk e() {
        return this.s;
    }

    public synchronized boolean f() {
        return !cu.a(t(), r(), this.n);
    }

    /* access modifiers changed from: protected */
    public void a(v vVar) {
        this.b = vVar;
    }

    /* access modifiers changed from: protected */
    public void a(uk ukVar) {
        this.s = ukVar;
    }

    @NonNull
    public String g() {
        return wk.b(this.b.b, "");
    }

    public String k() {
        return this.e;
    }

    @NonNull
    public String m() {
        return this.b.c;
    }

    @NonNull
    public String n() {
        return this.b.d;
    }

    public int o() {
        return this.b.e;
    }

    /* access modifiers changed from: protected */
    public void c(@Nullable String str) {
        if (!TextUtils.isEmpty(str)) {
            this.j = str;
        }
    }

    public String p() {
        return wk.b(this.j, "");
    }

    public String q() {
        return wk.b(this.i, "");
    }

    /* access modifiers changed from: protected */
    public void d(@Nullable String str) {
        if (!TextUtils.isEmpty(str)) {
            this.i = str;
        }
    }

    @NonNull
    public synchronized String r() {
        return wk.b(this.l, "");
    }

    @NonNull
    public synchronized String s() {
        return wk.b(this.m, "");
    }

    @NonNull
    public synchronized String t() {
        return wk.b(this.k, "");
    }

    /* access modifiers changed from: protected */
    public synchronized void e(@Nullable String str) {
        if (!TextUtils.isEmpty(str)) {
            this.k = str;
        }
    }

    /* access modifiers changed from: protected */
    public synchronized void f(@Nullable String str) {
        if (!TextUtils.isEmpty(str)) {
            this.l = str;
        }
    }

    /* access modifiers changed from: protected */
    public synchronized void g(@Nullable String str) {
        if (!TextUtils.isEmpty(str)) {
            this.m = str;
        }
    }

    /* access modifiers changed from: protected */
    public synchronized void h(String str) {
        this.n = str;
    }

    public void i(String str) {
        this.o = str;
    }

    @NonNull
    public String u() {
        return this.b.h;
    }

    @NonNull
    public String v() {
        return this.h;
    }

    public int w() {
        return this.b.f.f5263a;
    }

    public int x() {
        return this.b.f.b;
    }

    public int y() {
        return this.b.f.c;
    }

    public float z() {
        return this.b.f.d;
    }

    @NonNull
    public String A() {
        return wk.b(this.r, "");
    }

    /* access modifiers changed from: 0000 */
    public final void j(String str) {
        this.r = str;
    }

    @NonNull
    public String B() {
        return this.o;
    }

    @NonNull
    public String C() {
        return wk.b(this.q, com.yandex.metrica.b.PHONE.name().toLowerCase(Locale.US));
    }

    /* access modifiers changed from: 0000 */
    public void k(String str) {
        this.q = str;
    }

    public com.yandex.metrica.impl.ac.a.c D() {
        return this.p;
    }

    /* access modifiers changed from: protected */
    public void a(com.yandex.metrica.impl.ac.a.c cVar) {
        this.p = cVar;
    }
}
