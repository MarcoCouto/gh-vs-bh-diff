package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import com.yandex.metrica.impl.ob.ev;

public interface eq<T extends ev> {
    T b(@NonNull Context context, @NonNull ek ekVar, @NonNull eg egVar);
}
