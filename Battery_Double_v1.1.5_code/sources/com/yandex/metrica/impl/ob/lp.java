package com.yandex.metrica.impl.ob;

import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.Nullable;

public class lp implements lm {

    /* renamed from: a reason: collision with root package name */
    private final lc f4972a;

    public void a(@Nullable SQLiteDatabase sQLiteDatabase) {
    }

    public lp(lc lcVar) {
        this.f4972a = lcVar;
    }

    @Nullable
    public SQLiteDatabase a() {
        try {
            return this.f4972a.getWritableDatabase();
        } catch (Throwable unused) {
            return null;
        }
    }
}
