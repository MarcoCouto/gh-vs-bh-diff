package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.support.annotation.NonNull;
import java.util.ArrayList;
import java.util.List;

public class pz implements pt {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Context f5072a;
    @NonNull
    private final String b;
    @NonNull
    private final yc c;

    public pz(@NonNull Context context) {
        this(context, context.getPackageName(), new yc());
    }

    public pz(@NonNull Context context, @NonNull String str, @NonNull yc ycVar) {
        this.f5072a = context;
        this.b = str;
        this.c = ycVar;
    }

    @NonNull
    public List<pu> a() {
        ArrayList arrayList = new ArrayList();
        PackageInfo a2 = this.c.a(this.f5072a, this.b, 4096);
        if (a2 != null) {
            for (String puVar : a2.requestedPermissions) {
                arrayList.add(new pu(puVar, true));
            }
        }
        return arrayList;
    }
}
