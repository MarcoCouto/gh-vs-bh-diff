package com.yandex.metrica.impl.ob;

import android.content.Context;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import java.util.Map;

@Deprecated
public class qe extends qd {
    private static final qk d = new qk(IronSourceConstants.TYPE_UUID);
    private static final qk e = new qk("DEVICEID");
    private static final qk f = new qk("DEVICEID_2");
    private static final qk g = new qk("DEVICEID_3");
    private static final qk h = new qk("AD_URL_GET");
    private static final qk i = new qk("AD_URL_REPORT");
    private static final qk j = new qk("HOST_URL");
    private static final qk k = new qk("SERVER_TIME_OFFSET");
    private static final qk l = new qk("STARTUP_REQUEST_TIME");
    private static final qk m = new qk("CLIDS");
    private qk n = new qk(d.a());
    private qk o = new qk(e.a());
    private qk p = new qk(f.a());
    private qk q = new qk(g.a());
    private qk r = new qk(h.a());
    private qk s = new qk(i.a());
    private qk t = new qk(j.a());
    private qk u = new qk(k.a());
    private qk v = new qk(l.a());
    private qk w = new qk(m.a());

    /* access modifiers changed from: protected */
    public String f() {
        return "_startupinfopreferences";
    }

    public qe(Context context) {
        super(context, null);
    }

    public String a(String str) {
        return this.c.getString(this.n.b(), str);
    }

    public String b(String str) {
        return this.c.getString(this.q.b(), str);
    }

    public String a() {
        return this.c.getString(this.p.b(), this.c.getString(this.o.b(), ""));
    }

    public String c(String str) {
        return this.c.getString(this.r.b(), str);
    }

    public String d(String str) {
        return this.c.getString(this.s.b(), str);
    }

    public long a(long j2) {
        return this.c.getLong(this.u.a(), j2);
    }

    public long b(long j2) {
        return this.c.getLong(this.v.b(), j2);
    }

    public String e(String str) {
        return this.c.getString(this.w.b(), str);
    }

    public qe b() {
        return (qe) h();
    }

    public Map<String, ?> c() {
        return this.c.getAll();
    }
}
