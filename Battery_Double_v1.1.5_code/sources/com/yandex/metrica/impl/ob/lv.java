package com.yandex.metrica.impl.ob;

import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import java.util.List;

public class lv extends lx {

    /* renamed from: a reason: collision with root package name */
    static final qk f4984a = new qk(IronSourceConstants.TYPE_UUID);
    static final qk b = new qk("DEVICE_ID_POSSIBLE");
    static final qk c = new qk("DEVICE_ID");
    static final qk d = new qk("DEVICE_ID_HASH");
    static final qk e = new qk("AD_URL_GET");
    static final qk f = new qk("AD_URL_REPORT");
    static final qk g = new qk("CUSTOM_HOSTS");
    static final qk h = new qk("SERVER_TIME_OFFSET");
    static final qk i = new qk("STARTUP_REQUEST_TIME");
    static final qk j = new qk("CLIDS");
    static final qk k = new qk("CLIENT_CLIDS");
    static final qk l = new qk("REFERRER");
    static final qk m = new qk("DEFERRED_DEEP_LINK_WAS_CHECKED");
    static final qk n = new qk("REFERRER_FROM_PLAY_SERVICES_WAS_CHECKED");
    static final qk o = new qk("DEPRECATED_NATIVE_CRASHES_CHECKED");
    static final qk p = new qk("API_LEVEL");

    public lv(lf lfVar) {
        super(lfVar);
    }

    public String a(String str) {
        return c(f4984a.b(), str);
    }

    public String b(String str) {
        return c(c.b(), str);
    }

    public String c(String str) {
        return c(d.b(), str);
    }

    public String a() {
        return c(b.b(), "");
    }

    public String d(String str) {
        return c(e.b(), str);
    }

    public String e(String str) {
        return c(f.b(), str);
    }

    public List<String> b() {
        String c2 = c(g.b(), null);
        if (TextUtils.isEmpty(c2)) {
            return null;
        }
        return vq.c(c2);
    }

    public long a(long j2) {
        return b(h.a(), j2);
    }

    public long b(long j2) {
        return b(i.b(), j2);
    }

    public String f(String str) {
        return c(j.b(), str);
    }

    public long c(long j2) {
        return b(p.b(), j2);
    }

    public String c() {
        return c(l.b(), null);
    }

    public boolean d() {
        return b(m.b(), false);
    }

    public boolean e() {
        return b(n.b(), false);
    }

    public lv g(String str) {
        return (lv) b(f4984a.b(), str);
    }

    public lv h(String str) {
        return (lv) b(c.b(), str);
    }

    public lv i(String str) {
        return (lv) b(d.b(), str);
    }

    public lv j(String str) {
        return (lv) b(e.b(), str);
    }

    public lv a(List<String> list) {
        return (lv) b(g.b(), vq.a(list));
    }

    public lv k(String str) {
        return (lv) b(f.b(), str);
    }

    public lv d(long j2) {
        return (lv) a(h.b(), j2);
    }

    public lv e(long j2) {
        return (lv) a(i.b(), j2);
    }

    public lv l(String str) {
        return (lv) b(j.b(), str);
    }

    public lv f(long j2) {
        return (lv) a(p.b(), j2);
    }

    public lv m(String str) {
        return (lv) b(b.b(), str);
    }

    public lv n(String str) {
        return (lv) b(l.b(), str);
    }

    public lv f() {
        return (lv) a(m.b(), true);
    }

    public lv g() {
        return (lv) a(n.b(), true);
    }

    public lv o(@Nullable String str) {
        return (lv) b(k.b(), str);
    }

    @Nullable
    public String p(@Nullable String str) {
        return c(k.b(), str);
    }
}
