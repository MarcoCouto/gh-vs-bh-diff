package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;

public class br implements Runnable {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final pi f4696a;
    @NonNull
    private final bo b;
    @NonNull
    private final xj c;
    @NonNull
    private final String d;
    @NonNull
    private final bq e;

    public br(@NonNull pi piVar, @NonNull bo boVar, @NonNull xj xjVar, @NonNull String str) {
        this(piVar, boVar, xjVar, new bq(), str);
    }

    public br(@NonNull pi piVar, @NonNull bo boVar, @NonNull xj xjVar, @NonNull bq bqVar, @NonNull String str) {
        this.f4696a = piVar;
        this.b = boVar;
        this.c = xjVar;
        this.e = bqVar;
        this.d = str;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0024, code lost:
        if (r3.b() == false) goto L_0x0026;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0026, code lost:
        r0 = false;
     */
    public void run() {
        boolean z = true;
        if (this.c.c() && this.f4696a.a()) {
            boolean a2 = this.b.a();
            pj c2 = this.b.c();
            if (a2) {
            }
            while (true) {
                if (this.c.c() && a2) {
                    if (a() || !this.b.t()) {
                        break;
                    }
                    a2 = true;
                } else {
                    break;
                }
            }
        } else {
            z = false;
        }
        if (!z) {
            b();
        }
        this.b.f();
    }

    private boolean a() {
        this.e.a(this.b);
        boolean b2 = this.b.b();
        this.b.a(b2);
        return b2;
    }

    private void b() {
        this.b.g();
    }
}
