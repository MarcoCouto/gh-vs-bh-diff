package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import com.yandex.metrica.impl.ob.rh.b;
import com.yandex.metrica.impl.ob.rh.b.C0111b;
import com.yandex.metrica.impl.ob.rh.b.a;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;
import java.util.Map.Entry;

public class pf {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final lh f5056a;
    @NonNull
    private final lg b;
    @NonNull
    private final pc c;
    @NonNull
    private final pa d;

    public pf(@NonNull Context context) {
        this(ld.a(context).g(), ld.a(context).h(), new og(context), new pb(), new oz());
    }

    public pe a(int i) {
        Map b2 = this.f5056a.b(i);
        Map b3 = this.b.b(i);
        b bVar = new b();
        bVar.b = a(b2);
        bVar.c = b(b3);
        pe peVar = new pe(b2.isEmpty() ? -1 : ((Long) Collections.max(b2.keySet())).longValue(), b3.isEmpty() ? -1 : ((Long) Collections.max(b3.keySet())).longValue(), bVar);
        return peVar;
    }

    public void a(pe peVar) {
        if (peVar.f5055a >= 0) {
            this.f5056a.b(peVar.f5055a);
        }
        if (peVar.b >= 0) {
            this.b.b(peVar.b);
        }
    }

    private C0111b[] a(Map<Long, String> map) {
        ArrayList arrayList = new ArrayList();
        for (Entry entry : map.entrySet()) {
            C0111b a2 = this.c.a(((Long) entry.getKey()).longValue(), (String) entry.getValue());
            if (a2 != null) {
                arrayList.add(a2);
            }
        }
        return (C0111b[]) arrayList.toArray(new C0111b[arrayList.size()]);
    }

    private a[] b(Map<Long, String> map) {
        ArrayList arrayList = new ArrayList();
        for (Entry entry : map.entrySet()) {
            a a2 = this.d.a(((Long) entry.getKey()).longValue(), (String) entry.getValue());
            if (a2 != null) {
                arrayList.add(a2);
            }
        }
        return (a[]) arrayList.toArray(new a[arrayList.size()]);
    }

    @VisibleForTesting
    pf(@NonNull lh lhVar, @NonNull lg lgVar, @NonNull og ogVar, @NonNull pb pbVar, @NonNull oz ozVar) {
        this(lhVar, lgVar, new pc(ogVar, pbVar), new pa(ogVar, ozVar));
    }

    @VisibleForTesting
    pf(@NonNull lh lhVar, @NonNull lg lgVar, @NonNull pc pcVar, @NonNull pa paVar) {
        this.f5056a = lhVar;
        this.b = lgVar;
        this.c = pcVar;
        this.d = paVar;
    }
}
