package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.os.ResultReceiver;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import com.yandex.metrica.impl.ob.si.a;
import java.io.Closeable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ei implements et, ev, uh {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Context f4811a;
    @NonNull
    private final ek b;
    @NonNull
    private uc c;
    /* access modifiers changed from: private */
    @NonNull
    public ul d;
    @NonNull
    private final fh e;
    @NonNull
    private bl f;
    @NonNull
    private gu<gt, ei> g;
    @NonNull
    private cv<ei> h;
    @NonNull
    private List<ap> i;
    private final el<fs> j;
    private si k;
    private final a l;
    @Nullable
    private rs m;
    private final Object n;

    public ei(@NonNull Context context, @NonNull uc ucVar, @NonNull ek ekVar, @NonNull eg egVar, @NonNull si siVar, @NonNull bl blVar) {
        this(context, ucVar, ekVar, egVar, new fh(egVar.b), siVar, blVar);
    }

    @VisibleForTesting
    ei(@NonNull Context context, @NonNull uc ucVar, @NonNull ek ekVar, @NonNull eg egVar, @NonNull fh fhVar, @NonNull si siVar, @NonNull bl blVar) {
        this.j = new el<>();
        this.n = new Object();
        this.f4811a = context.getApplicationContext();
        this.b = ekVar;
        this.c = ucVar;
        this.e = fhVar;
        this.f = blVar;
        this.i = new ArrayList();
        this.g = new gu<>(new gm(this), this);
        this.d = this.c.a(this.f4811a, this.b, this, egVar.f4809a);
        this.h = new cv<>(this, new up(this.d), this.f);
        this.k = siVar;
        this.l = new a() {
            public boolean a(@NonNull sj sjVar) {
                if (!TextUtils.isEmpty(sjVar.f5159a)) {
                    ei.this.d.a(sjVar.f5159a);
                }
                return false;
            }
        };
        this.k.a(this.l);
    }

    public void a(@NonNull eg.a aVar) {
        this.e.a(aVar);
    }

    public synchronized void a(@NonNull fs fsVar) {
        this.j.a(fsVar);
    }

    public synchronized void b(@NonNull fs fsVar) {
        this.j.b(fsVar);
    }

    public void a(@NonNull w wVar, @NonNull fs fsVar) {
        this.g.a(wVar, fsVar);
    }

    @NonNull
    public eg.a a() {
        return this.e.a();
    }

    @NonNull
    public ek b() {
        return this.b;
    }

    public void a(@Nullable uk ukVar) {
        b(ukVar);
        if (ukVar != null) {
            if (this.m == null) {
                this.m = al.a().e();
            }
            this.m.a(ukVar);
        }
    }

    public void a(@NonNull ue ueVar, @Nullable uk ukVar) {
        synchronized (this.n) {
            for (ap c2 : this.i) {
                x.a(c2.c(), ueVar, ukVar);
            }
            this.i.clear();
        }
    }

    private void b(uk ukVar) {
        synchronized (this.n) {
            for (uh a2 : this.j.a()) {
                a2.a(ukVar);
            }
            ArrayList arrayList = new ArrayList();
            for (ap apVar : this.i) {
                if (apVar.a(ukVar)) {
                    x.a(apVar.c(), ukVar);
                } else {
                    arrayList.add(apVar);
                }
            }
            this.i = new ArrayList(arrayList);
            if (!arrayList.isEmpty()) {
                this.h.e();
            }
        }
    }

    public void c() {
        cx.a((Closeable) this.h);
        this.f.b();
    }

    @NonNull
    public Context d() {
        return this.f4811a;
    }

    public void a(@Nullable ap apVar) {
        ResultReceiver resultReceiver;
        Map hashMap = new HashMap();
        List list = null;
        if (apVar != null) {
            list = apVar.a();
            ResultReceiver c2 = apVar.c();
            resultReceiver = c2;
            hashMap = apVar.b();
        } else {
            resultReceiver = null;
        }
        boolean a2 = this.d.a(list, hashMap);
        if (!a2) {
            x.a(resultReceiver, this.d.e());
        }
        if (this.d.c()) {
            synchronized (this.n) {
                if (a2 && apVar != null) {
                    this.i.add(apVar);
                }
            }
            this.h.e();
            return;
        }
        x.a(resultReceiver, this.d.e());
    }

    public si e() {
        return this.k;
    }

    public void a(@NonNull eg egVar) {
        this.d.a(egVar.f4809a);
        a(egVar.b);
    }
}
