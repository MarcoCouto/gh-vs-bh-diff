package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.util.concurrent.TimeUnit;

class or {

    /* renamed from: a reason: collision with root package name */
    static final long f5043a = TimeUnit.SECONDS.toMillis(1);
    @NonNull
    private Context b;
    @NonNull
    private Looper c;
    @Nullable
    private LocationManager d;
    @NonNull
    private LocationListener e;
    @NonNull
    private pr f;

    public or(@NonNull Context context, @NonNull Looper looper, @Nullable LocationManager locationManager, @NonNull LocationListener locationListener, @NonNull pr prVar) {
        this.b = context;
        this.c = looper;
        this.d = locationManager;
        this.e = locationListener;
        this.f = prVar;
    }

    public void a() {
        if (this.f.b(this.b)) {
            a("passive", 0.0f, f5043a, this.e, this.c);
        }
    }

    public void b() {
        if (this.d != null) {
            try {
                this.d.removeUpdates(this.e);
            } catch (Throwable unused) {
            }
        }
    }

    private void a(String str, float f2, long j, @NonNull LocationListener locationListener, @NonNull Looper looper) {
        if (this.d != null) {
            try {
                this.d.requestLocationUpdates(str, j, f2, locationListener, looper);
            } catch (Throwable unused) {
            }
        }
    }
}
