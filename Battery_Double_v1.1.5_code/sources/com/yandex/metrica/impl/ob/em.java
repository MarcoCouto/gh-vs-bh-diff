package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import com.google.android.exoplayer2.offline.DownloadService;
import java.util.LinkedList;
import java.util.List;

public class em {

    /* renamed from: a reason: collision with root package name */
    private final en f4816a;
    private final qb b;
    private List<g> c;

    public static class a {
        @NonNull

        /* renamed from: a reason: collision with root package name */
        private final en f4817a;

        public a(@NonNull en enVar) {
            this.f4817a = enVar;
        }

        /* access modifiers changed from: 0000 */
        public em a(@NonNull qb qbVar) {
            return new em(this.f4817a, qbVar);
        }
    }

    static class b extends g {

        /* renamed from: a reason: collision with root package name */
        private final qf f4818a;
        private final lw b;
        private final ly c;

        b(en enVar) {
            super(enVar);
            this.f4818a = new qf(enVar.k(), enVar.b().toString());
            this.b = enVar.y();
            this.c = enVar.f4823a;
        }

        /* access modifiers changed from: protected */
        public boolean a() {
            return this.f4818a.e();
        }

        /* access modifiers changed from: protected */
        public void b() {
            d();
            c();
            g();
            this.f4818a.g();
        }

        private void g() {
            com.yandex.metrica.impl.ob.i.a a2 = this.f4818a.a();
            if (a2 != null) {
                this.b.a(a2);
            }
            String a3 = this.f4818a.a((String) null);
            if (!TextUtils.isEmpty(a3) && TextUtils.isEmpty(this.b.f())) {
                this.b.a(a3);
            }
            long c2 = this.f4818a.c(Long.MIN_VALUE);
            if (c2 != Long.MIN_VALUE && this.b.a(Long.MIN_VALUE) == Long.MIN_VALUE) {
                this.b.b(c2);
            }
            this.b.q();
        }

        /* access modifiers changed from: 0000 */
        @VisibleForTesting
        public void c() {
            jg jgVar = new jg(this.b, DownloadService.KEY_FOREGROUND);
            if (!jgVar.i()) {
                long d = this.f4818a.d(-1);
                if (-1 != d) {
                    jgVar.d(d);
                }
                boolean booleanValue = this.f4818a.a(true).booleanValue();
                if (booleanValue) {
                    jgVar.a(booleanValue);
                }
                long a2 = this.f4818a.a(Long.MIN_VALUE);
                if (a2 != Long.MIN_VALUE) {
                    jgVar.e(a2);
                }
                long f = this.f4818a.f(0);
                if (f != 0) {
                    jgVar.a(f);
                }
                long h = this.f4818a.h(0);
                if (h != 0) {
                    jgVar.b(h);
                }
                jgVar.h();
            }
        }

        /* access modifiers changed from: 0000 */
        @VisibleForTesting
        public void d() {
            jg jgVar = new jg(this.b, "background");
            if (!jgVar.i()) {
                long e = this.f4818a.e(-1);
                if (e != -1) {
                    jgVar.d(e);
                }
                long b2 = this.f4818a.b(Long.MIN_VALUE);
                if (b2 != Long.MIN_VALUE) {
                    jgVar.e(b2);
                }
                long g = this.f4818a.g(0);
                if (g != 0) {
                    jgVar.a(g);
                }
                long i = this.f4818a.i(0);
                if (i != 0) {
                    jgVar.b(i);
                }
                jgVar.h();
            }
        }
    }

    static class c extends h {
        c(en enVar, qb qbVar) {
            super(enVar, qbVar);
        }

        /* access modifiers changed from: protected */
        public boolean a() {
            return e() instanceof ez;
        }

        /* access modifiers changed from: protected */
        public void b() {
            c().a();
        }
    }

    static class d extends g {

        /* renamed from: a reason: collision with root package name */
        private final qc f4819a;
        private final lu b;

        d(en enVar, qc qcVar) {
            super(enVar);
            this.f4819a = qcVar;
            this.b = enVar.u();
        }

        /* access modifiers changed from: protected */
        public boolean a() {
            return "DONE".equals(this.f4819a.c(null)) || "DONE".equals(this.f4819a.b(null));
        }

        /* access modifiers changed from: protected */
        public void b() {
            if ("DONE".equals(this.f4819a.c(null))) {
                this.b.b();
            }
            String e = this.f4819a.e(null);
            if (!TextUtils.isEmpty(e)) {
                this.b.c(e);
            }
            if ("DONE".equals(this.f4819a.b(null))) {
                this.b.a();
            }
            this.f4819a.d();
            this.f4819a.e();
            this.f4819a.c();
        }
    }

    static class e extends h {
        e(en enVar, qb qbVar) {
            super(enVar, qbVar);
        }

        /* access modifiers changed from: protected */
        public boolean a() {
            return e().u().a((String) null) == null;
        }

        /* access modifiers changed from: protected */
        public void b() {
            qb c = c();
            if (e() instanceof ez) {
                c.c();
            } else {
                c.b();
            }
        }
    }

    static class f extends g {
        @Deprecated

        /* renamed from: a reason: collision with root package name */
        static final qk f4820a = new qk("SESSION_SLEEP_START");
        @Deprecated
        static final qk b = new qk("SESSION_ID");
        @Deprecated
        static final qk c = new qk("SESSION_COUNTER_ID");
        @Deprecated
        static final qk d = new qk("SESSION_INIT_TIME");
        @Deprecated
        static final qk e = new qk("SESSION_IS_ALIVE_REPORT_NEEDED");
        @Deprecated
        static final qk f = new qk("BG_SESSION_ID");
        @Deprecated
        static final qk g = new qk("BG_SESSION_SLEEP_START");
        @Deprecated
        static final qk h = new qk("BG_SESSION_COUNTER_ID");
        @Deprecated
        static final qk i = new qk("BG_SESSION_INIT_TIME");
        @Deprecated
        static final qk j = new qk("BG_SESSION_IS_ALIVE_REPORT_NEEDED");
        private final lw k;

        /* access modifiers changed from: protected */
        public boolean a() {
            return true;
        }

        f(en enVar) {
            super(enVar);
            this.k = enVar.y();
        }

        /* access modifiers changed from: protected */
        public void b() {
            d();
            c();
            g();
        }

        private void g() {
            this.k.r(f4820a.b());
            this.k.r(b.b());
            this.k.r(c.b());
            this.k.r(d.b());
            this.k.r(e.b());
            this.k.r(f.b());
            this.k.r(g.b());
            this.k.r(h.b());
            this.k.r(i.b());
            this.k.r(j.b());
        }

        /* access modifiers changed from: 0000 */
        @VisibleForTesting
        public void c() {
            long b2 = this.k.b(f4820a.b(), -2147483648L);
            if (b2 != -2147483648L) {
                jg jgVar = new jg(this.k, DownloadService.KEY_FOREGROUND);
                if (!jgVar.i()) {
                    if (b2 != 0) {
                        jgVar.b(b2);
                    }
                    long b3 = this.k.b(b.b(), -1);
                    if (-1 != b3) {
                        jgVar.d(b3);
                    }
                    boolean b4 = this.k.b(e.b(), true);
                    if (b4) {
                        jgVar.a(b4);
                    }
                    long b5 = this.k.b(d.b(), Long.MIN_VALUE);
                    if (b5 != Long.MIN_VALUE) {
                        jgVar.e(b5);
                    }
                    long b6 = this.k.b(c.b(), 0);
                    if (b6 != 0) {
                        jgVar.a(b6);
                    }
                    jgVar.h();
                }
            }
        }

        /* access modifiers changed from: 0000 */
        @VisibleForTesting
        public void d() {
            long b2 = this.k.b(g.b(), -2147483648L);
            if (b2 != -2147483648L) {
                jg jgVar = new jg(this.k, "background");
                if (!jgVar.i()) {
                    if (b2 != 0) {
                        jgVar.b(b2);
                    }
                    long b3 = this.k.b(f.b(), -1);
                    if (b3 != -1) {
                        jgVar.d(b3);
                    }
                    boolean b4 = this.k.b(j.b(), true);
                    if (b4) {
                        jgVar.a(b4);
                    }
                    long b5 = this.k.b(i.b(), Long.MIN_VALUE);
                    if (b5 != Long.MIN_VALUE) {
                        jgVar.e(b5);
                    }
                    long b6 = this.k.b(h.b(), 0);
                    if (b6 != 0) {
                        jgVar.a(b6);
                    }
                    jgVar.h();
                }
            }
        }
    }

    private static abstract class g {

        /* renamed from: a reason: collision with root package name */
        private final en f4821a;

        /* access modifiers changed from: protected */
        public abstract boolean a();

        /* access modifiers changed from: protected */
        public abstract void b();

        g(en enVar) {
            this.f4821a = enVar;
        }

        /* access modifiers changed from: 0000 */
        public en e() {
            return this.f4821a;
        }

        /* access modifiers changed from: 0000 */
        public void f() {
            if (a()) {
                b();
            }
        }
    }

    private static abstract class h extends g {

        /* renamed from: a reason: collision with root package name */
        private qb f4822a;

        h(en enVar, qb qbVar) {
            super(enVar);
            this.f4822a = qbVar;
        }

        public qb c() {
            return this.f4822a;
        }
    }

    private em(en enVar, qb qbVar) {
        this.f4816a = enVar;
        this.b = qbVar;
        b();
    }

    private void b() {
        this.c = new LinkedList();
        this.c.add(new c(this.f4816a, this.b));
        this.c.add(new e(this.f4816a, this.b));
        this.c.add(new d(this.f4816a, this.f4816a.v()));
        this.c.add(new b(this.f4816a));
        this.c.add(new f(this.f4816a));
    }

    /* access modifiers changed from: 0000 */
    public void a() {
        if (!a(this.f4816a.b().a())) {
            for (g f2 : this.c) {
                f2.f();
            }
        }
    }

    private boolean a(String str) {
        return qb.f5075a.values().contains(str);
    }
}
