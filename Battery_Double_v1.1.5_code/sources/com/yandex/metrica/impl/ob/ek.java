package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import io.fabric.sdk.android.services.events.EventsFilesManager;

public class ek {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final String f4814a;
    @Nullable
    private final String b;

    public ek(@NonNull String str, @Nullable String str2) {
        this.f4814a = str;
        this.b = str2;
    }

    public String a() {
        return this.b;
    }

    public String b() {
        return this.f4814a;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.f4814a);
        sb.append(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
        sb.append(this.b);
        return sb.toString();
    }

    public String c() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.f4814a);
        sb.append(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
        sb.append(cx.b(this.b));
        return sb.toString();
    }

    public boolean equals(Object obj) {
        boolean z = true;
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        ek ekVar = (ek) obj;
        if (this.f4814a == null ? ekVar.f4814a != null : !this.f4814a.equals(ekVar.f4814a)) {
            return false;
        }
        if (this.b != null) {
            z = this.b.equals(ekVar.b);
        } else if (ekVar.b != null) {
            z = false;
        }
        return z;
    }

    public int hashCode() {
        int i = 0;
        int hashCode = (this.f4814a != null ? this.f4814a.hashCode() : 0) * 31;
        if (this.b != null) {
            i = this.b.hashCode();
        }
        return hashCode + i;
    }
}
