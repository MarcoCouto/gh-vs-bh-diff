package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;

public class tg {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final mf<te> f5188a;
    @NonNull
    private final tf b;
    @NonNull
    private final Context c;

    public tg(@NonNull Context context, @NonNull mf<te> mfVar) {
        this(context, mfVar, new tf());
    }

    public tg(@NonNull Context context, @NonNull mf<te> mfVar, @NonNull tf tfVar) {
        this.f5188a = mfVar;
        this.c = context;
        this.b = tfVar;
    }

    public void a() {
        tl.a(this.c).reportEvent("sdk_list", this.b.b(this.b.a(((te) this.f5188a.a()).f5187a)).toString());
    }
}
