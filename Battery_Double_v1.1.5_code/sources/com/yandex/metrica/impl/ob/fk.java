package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public class fk {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final lw f4838a;
    @NonNull
    private final wh b;
    @NonNull
    private final cw c;
    @Nullable
    private um d;
    private long e;

    public fk(@NonNull Context context, @NonNull ek ekVar) {
        this(new lw(ld.a(context).b(ekVar)), new wg(), new cw());
    }

    public fk(@NonNull lw lwVar, @NonNull wh whVar, @NonNull cw cwVar) {
        this.f4838a = lwVar;
        this.b = whVar;
        this.c = cwVar;
        this.e = this.f4838a.k();
    }

    public boolean a(@Nullable Boolean bool) {
        return vi.c(bool) && this.d != null && this.c.b(this.e, this.d.f5239a, "should report diagnostic");
    }

    public void a() {
        this.e = this.b.a();
        this.f4838a.f(this.e).q();
    }

    public void a(@Nullable um umVar) {
        this.d = umVar;
    }
}
