package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public final class jb {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final jh f4896a;
    @Nullable
    private final Integer b;

    static final class a {
        /* access modifiers changed from: private */
        @NonNull

        /* renamed from: a reason: collision with root package name */
        public jh f4897a;
        /* access modifiers changed from: private */
        @Nullable
        public Integer b;

        private a(jh jhVar) {
            this.f4897a = jhVar;
        }

        public a a(int i) {
            this.b = Integer.valueOf(i);
            return this;
        }

        public jb a() {
            return new jb(this);
        }
    }

    private jb(a aVar) {
        this.f4896a = aVar.f4897a;
        this.b = aVar.b;
    }

    public static final a a(jh jhVar) {
        return new a(jhVar);
    }

    @NonNull
    public jh a() {
        return this.f4896a;
    }

    @Nullable
    public Integer b() {
        return this.b;
    }
}
