package com.yandex.metrica.impl.ob;

public class ty {

    /* renamed from: a reason: collision with root package name */
    public final long f5224a;

    public ty(long j) {
        this.f5224a = j;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("IdentityLightCollectingConfig{mIntervalSeconds=");
        sb.append(this.f5224a);
        sb.append('}');
        return sb.toString();
    }
}
