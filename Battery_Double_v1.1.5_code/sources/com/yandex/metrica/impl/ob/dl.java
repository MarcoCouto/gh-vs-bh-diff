package com.yandex.metrica.impl.ob;

import android.annotation.TargetApi;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanResult;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import java.util.List;

@TargetApi(21)
public class dl extends ScanCallback {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final dm f4788a;

    public dl(@NonNull Context context, long j) {
        this(new dm(context, j));
    }

    public void onScanResult(int i, ScanResult scanResult) {
        super.onScanResult(i, scanResult);
        this.f4788a.a(scanResult, Integer.valueOf(i));
    }

    public void onBatchScanResults(List<ScanResult> list) {
        super.onBatchScanResults(list);
        this.f4788a.a(list);
    }

    public void onScanFailed(int i) {
        super.onScanFailed(i);
        this.f4788a.a(i);
    }

    @VisibleForTesting
    dl(@NonNull dm dmVar) {
        this.f4788a = dmVar;
    }
}
