package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;

public class pu {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    public final String f5068a;
    public final boolean b;

    public pu(@NonNull String str, boolean z) {
        this.f5068a = str;
        this.b = z;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        pu puVar = (pu) obj;
        if (this.b != puVar.b) {
            return false;
        }
        return this.f5068a.equals(puVar.f5068a);
    }

    public int hashCode() {
        return (this.f5068a.hashCode() * 31) + (this.b ? 1 : 0);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("PermissionState{name='");
        sb.append(this.f5068a);
        sb.append('\'');
        sb.append(", granted=");
        sb.append(this.b);
        sb.append('}');
        return sb.toString();
    }
}
