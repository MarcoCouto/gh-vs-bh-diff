package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import com.yandex.metrica.impl.ob.rr.a.C0121a;
import com.yandex.metrica.impl.ob.rr.a.C0121a.C0122a;
import java.util.List;

public class mn implements mv<oc, C0121a> {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final ms f4995a;
    @NonNull
    private final mu b;

    public mn() {
        this(new ms(), new mu());
    }

    @NonNull
    /* renamed from: a */
    public C0121a b(@NonNull oc ocVar) {
        C0122a[] aVarArr;
        C0121a aVar = new C0121a();
        aVar.b = this.f4995a.b((oh) ocVar);
        aVar.d = ocVar.b;
        aVar.c = ocVar.f5022a;
        aVar.e = ocVar.c;
        if (ocVar.d == null) {
            aVarArr = new C0122a[0];
        } else {
            aVarArr = this.b.b(ocVar.d);
        }
        aVar.f = aVarArr;
        return aVar;
    }

    @NonNull
    public oc a(@NonNull C0121a aVar) {
        List a2;
        C0121a aVar2 = aVar;
        oh a3 = this.f4995a.a(aVar2.b);
        long j = a3.e;
        float f = a3.f;
        int i = a3.g;
        int i2 = a3.h;
        long j2 = a3.i;
        int i3 = a3.j;
        boolean z = a3.k;
        long j3 = aVar2.c;
        long j4 = aVar2.d;
        long j5 = a3.l;
        boolean z2 = a3.m;
        boolean z3 = aVar2.e;
        if (cx.a((T[]) aVar2.f)) {
            a2 = null;
        } else {
            a2 = this.b.a(aVar2.f);
        }
        List list = a2;
        oc ocVar = new oc(j, f, i, i2, j2, i3, z, j3, j4, j5, z2, z3, list);
        return ocVar;
    }

    @VisibleForTesting
    mn(@NonNull ms msVar, @NonNull mu muVar) {
        this.f4995a = msVar;
        this.b = muVar;
    }
}
