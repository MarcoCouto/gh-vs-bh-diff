package com.yandex.metrica.impl.ob;

import com.yandex.metrica.impl.ob.af.a;
import java.util.LinkedList;

public class gq extends gr<hd> {

    /* renamed from: a reason: collision with root package name */
    private final hn f4859a;
    private final im b;
    private final hj c;

    public gq(en enVar) {
        this.f4859a = new hn(enVar);
        this.b = new im(enVar);
        this.c = new hj(enVar);
    }

    public go<hd> a(int i) {
        LinkedList linkedList = new LinkedList();
        switch (a.a(i)) {
            case EVENT_TYPE_START:
                linkedList.add(this.b);
                linkedList.add(this.f4859a);
                break;
            case EVENT_TYPE_INIT:
                linkedList.add(this.f4859a);
                break;
            case EVENT_TYPE_UPDATE_FOREGROUND_TIME:
                linkedList.add(this.c);
                break;
        }
        return new gn(linkedList);
    }
}
