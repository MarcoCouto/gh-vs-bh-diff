package com.yandex.metrica.impl.ob;

import android.net.Uri.Builder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.metrica.impl.ob.gh.a;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.util.Collection;

public class gg extends bs<sv> {
    @NonNull
    private final gj j;
    @NonNull
    private final w k;
    @NonNull
    private final gk l;
    @NonNull
    private final a m;
    @NonNull
    private final wh n;
    @NonNull
    private vm o;
    @NonNull
    private final String p;
    @NonNull
    private final lw q;
    @Nullable
    private gi r;

    public gg(@NonNull gj gjVar, @NonNull w wVar, @NonNull gk gkVar, @NonNull lw lwVar) {
        this(gjVar, wVar, gkVar, lwVar, new a(), new wg(), new vm(), new sv());
    }

    public gg(@NonNull gj gjVar, @NonNull w wVar, @NonNull gk gkVar, @NonNull lw lwVar, @NonNull a aVar, @NonNull wh whVar, @NonNull vm vmVar, @NonNull sv svVar) {
        super(new ab(), svVar);
        this.j = gjVar;
        this.k = wVar;
        this.l = gkVar;
        this.q = lwVar;
        this.m = aVar;
        this.n = whVar;
        this.o = vmVar;
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getName());
        sb.append("@");
        sb.append(Integer.toHexString(hashCode()));
        this.p = sb.toString();
    }

    public boolean a() {
        this.r = this.j.d();
        if (!(this.r.f() && !cx.a((Collection) this.r.a()))) {
            return false;
        }
        a(this.r.a());
        byte[] a2 = this.m.a(this.k, this.r, this.l, this.q).a();
        byte[] bArr = null;
        try {
            bArr = this.o.a(a2);
        } catch (Throwable unused) {
        }
        if (!cx.a(bArr)) {
            a(HttpRequest.HEADER_CONTENT_ENCODING, HttpRequest.ENCODING_GZIP);
            a2 = bArr;
        }
        a(a2);
        return true;
    }

    public void d() {
        super.d();
        a(this.n.a());
    }

    /* access modifiers changed from: protected */
    public void a(@NonNull Builder builder) {
        ((sv) this.i).a(builder, this.r);
    }

    public boolean t() {
        return super.t() & (400 != k());
    }

    @NonNull
    public String n() {
        return this.p;
    }
}
