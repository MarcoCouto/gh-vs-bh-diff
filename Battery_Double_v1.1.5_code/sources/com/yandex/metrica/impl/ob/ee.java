package com.yandex.metrica.impl.ob;

import android.content.ContentValues;
import android.content.Context;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.os.Process;
import android.os.ResultReceiver;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.yandex.metrica.j;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class ee implements Parcelable {
    public static final Creator<ee> CREATOR = new Creator<ee>() {
        /* renamed from: a */
        public ee createFromParcel(Parcel parcel) {
            Bundle readBundle = parcel.readBundle(x.class.getClassLoader());
            return new ee((ContentValues) readBundle.getParcelable("CFG_KEY_PROCESS_ENVIRONMENT"), (ResultReceiver) readBundle.getParcelable("CFG_KEY_PROCESS_ENVIRONMENT_RECEIVER"));
        }

        /* renamed from: a */
        public ee[] newArray(int i) {
            return new ee[i];
        }
    };

    /* renamed from: a reason: collision with root package name */
    public static final String f4808a = UUID.randomUUID().toString();
    @NonNull
    private final ContentValues b;
    @Nullable
    private ResultReceiver c;

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        Bundle bundle = new Bundle();
        bundle.putParcelable("CFG_KEY_PROCESS_ENVIRONMENT", this.b);
        bundle.putParcelable("CFG_KEY_PROCESS_ENVIRONMENT_RECEIVER", this.c);
        parcel.writeBundle(bundle);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("ProcessConfiguration{mParamsMapping=");
        sb.append(this.b);
        sb.append(", mDataResultReceiver=");
        sb.append(this.c);
        sb.append('}');
        return sb.toString();
    }

    @Nullable
    public static ee a(Bundle bundle) {
        if (bundle == null) {
            return null;
        }
        try {
            return (ee) bundle.getParcelable("PROCESS_CFG_OBJ");
        } catch (Throwable unused) {
            return null;
        }
    }

    public ee(Context context, @Nullable ResultReceiver resultReceiver) {
        this.b = new ContentValues();
        this.b.put("PROCESS_CFG_PROCESS_ID", Integer.valueOf(Process.myPid()));
        this.b.put("PROCESS_CFG_PROCESS_SESSION_ID", f4808a);
        this.b.put("PROCESS_CFG_SDK_API_LEVEL", Integer.valueOf(85));
        this.b.put("PROCESS_CFG_PACKAGE_NAME", context.getPackageName());
        this.c = resultReceiver;
    }

    public ee(ee eeVar) {
        synchronized (eeVar) {
            this.b = new ContentValues(eeVar.b);
            this.c = eeVar.c;
        }
    }

    public ee(@NonNull ContentValues contentValues, @Nullable ResultReceiver resultReceiver) {
        if (contentValues == null) {
            contentValues = new ContentValues();
        }
        this.b = contentValues;
        this.c = resultReceiver;
    }

    public void a(@Nullable j jVar) {
        if (jVar != null) {
            synchronized (this) {
                b(jVar);
                c(jVar);
                d(jVar);
            }
        }
    }

    private void b(@NonNull j jVar) {
        if (cx.a((Object) jVar.d)) {
            a(jVar.d);
        }
    }

    private void c(@NonNull j jVar) {
        if (cx.a((Object) jVar.b)) {
            a(we.c(jVar.b));
        }
    }

    private void d(@NonNull j jVar) {
        if (cx.a((Object) jVar.c)) {
            a(jVar.c);
        }
    }

    public boolean a() {
        return this.b.containsKey("PROCESS_CFG_CUSTOM_HOSTS");
    }

    @Nullable
    public List<String> b() {
        String asString = this.b.getAsString("PROCESS_CFG_CUSTOM_HOSTS");
        if (TextUtils.isEmpty(asString)) {
            return null;
        }
        return vq.c(asString);
    }

    public synchronized void a(@Nullable List<String> list) {
        this.b.put("PROCESS_CFG_CUSTOM_HOSTS", vq.a(list));
    }

    @Nullable
    public Map<String, String> c() {
        return vq.a(this.b.getAsString("PROCESS_CFG_CLIDS"));
    }

    public synchronized void a(@Nullable Map<String, String> map) {
        this.b.put("PROCESS_CFG_CLIDS", vq.b((Map) map));
    }

    @Nullable
    public String d() {
        return this.b.getAsString("PROCESS_CFG_DISTRIBUTION_REFERRER");
    }

    public synchronized void a(@Nullable String str) {
        this.b.put("PROCESS_CFG_DISTRIBUTION_REFERRER", str);
    }

    @NonNull
    public Integer e() {
        return this.b.getAsInteger("PROCESS_CFG_PROCESS_ID");
    }

    @NonNull
    public String f() {
        return this.b.getAsString("PROCESS_CFG_PROCESS_SESSION_ID");
    }

    public int g() {
        return this.b.getAsInteger("PROCESS_CFG_SDK_API_LEVEL").intValue();
    }

    @NonNull
    public String h() {
        return this.b.getAsString("PROCESS_CFG_PACKAGE_NAME");
    }

    @Nullable
    public ResultReceiver i() {
        return this.c;
    }

    public synchronized void b(@NonNull Bundle bundle) {
        bundle.putParcelable("PROCESS_CFG_OBJ", this);
    }
}
