package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;

public class er {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final lw f4830a;

    public er(@NonNull lw lwVar) {
        this.f4830a = lwVar;
    }

    public int a() {
        int a2 = this.f4830a.a();
        this.f4830a.b(a2 + 1).q();
        return a2;
    }

    public int a(int i) {
        int a2 = this.f4830a.a(i);
        this.f4830a.a(i, a2 + 1).q();
        return a2;
    }
}
