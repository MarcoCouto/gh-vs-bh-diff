package com.yandex.metrica.impl.ob;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.support.v4.app.NotificationCompat;
import com.yandex.metrica.ConfigurationService;

public class jn implements jm, jp {
    /* access modifiers changed from: private */
    @NonNull

    /* renamed from: a reason: collision with root package name */
    public final Context f4912a;
    @Nullable
    private final AlarmManager b;
    /* access modifiers changed from: private */
    @NonNull
    public wh c;

    public jn(@NonNull Context context) {
        this(context, (AlarmManager) context.getSystemService(NotificationCompat.CATEGORY_ALARM), new wg());
    }

    public void a(final long j, boolean z) {
        cx.a((wn<T>) new wn<AlarmManager>() {
            public void a(AlarmManager alarmManager) throws Throwable {
                alarmManager.set(3, jn.this.c.c() + j, jn.this.a(jn.this.f4912a));
            }
        }, this.b, "scheduling wakeup in [ConfigurationServiceController]", "AlarmManager");
    }

    public void a() {
        cx.a((wn<T>) new wn<AlarmManager>() {
            public void a(AlarmManager alarmManager) throws Throwable {
                alarmManager.cancel(jn.this.a(jn.this.f4912a));
            }
        }, this.b, "cancelling scheduled wakeup in [ConfigurationServiceController]", "AlarmManager");
    }

    public void a(@NonNull Bundle bundle) {
        try {
            this.f4912a.startService(new Intent().setComponent(new ComponentName(this.f4912a.getPackageName(), ConfigurationService.class.getName())).setAction("com.yandex.metrica.configuration.ACTION_INIT").putExtras(bundle));
        } catch (Throwable unused) {
        }
    }

    /* access modifiers changed from: private */
    @NonNull
    public PendingIntent a(@NonNull Context context) {
        return PendingIntent.getService(context, 7695435, b(context), 134217728);
    }

    @NonNull
    private Intent b(@NonNull Context context) {
        return new Intent(context, ConfigurationService.class).setAction("com.yandex.metrica.configuration.ACTION_SCHEDULED_START");
    }

    @VisibleForTesting
    jn(@NonNull Context context, @Nullable AlarmManager alarmManager, @NonNull wh whVar) {
        this.f4912a = context;
        this.b = alarmManager;
        this.c = whVar;
    }
}
