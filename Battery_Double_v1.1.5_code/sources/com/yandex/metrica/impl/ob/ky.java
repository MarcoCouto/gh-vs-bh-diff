package com.yandex.metrica.impl.ob;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import com.ironsource.mediationsdk.utils.IronSourceConstants;
import com.tapjoy.TapjoyConstants;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;

public class ky {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Context f4953a;
    @NonNull
    private final com.yandex.metrica.CounterConfiguration.a b;
    @Nullable
    private final ck c;

    static class a {
        @Nullable

        /* renamed from: a reason: collision with root package name */
        public final List<ContentValues> f4954a;
        public final int b;

        a(@Nullable List<ContentValues> list, int i) {
            this.f4954a = list;
            this.b = i;
        }
    }

    public enum b {
        BAD_REQUEST("bad_request"),
        DB_OVERFLOW("db_overflow");
        
        /* access modifiers changed from: private */
        public final String c;

        private b(String str) {
            this.c = str;
        }
    }

    public ky(@NonNull Context context, @NonNull com.yandex.metrica.CounterConfiguration.a aVar) {
        this(context, aVar, al.a().l());
    }

    @VisibleForTesting
    ky(@NonNull Context context, @NonNull com.yandex.metrica.CounterConfiguration.a aVar, @Nullable ck ckVar) {
        this.f4953a = context;
        this.b = aVar;
        this.c = ckVar;
    }

    @Nullable
    private List<ContentValues> a(@Nullable Cursor cursor) {
        if (cursor == null || cursor.getCount() <= 0) {
            return null;
        }
        ArrayList arrayList = new ArrayList(cursor.getCount());
        while (cursor.moveToNext()) {
            ContentValues contentValues = new ContentValues();
            DatabaseUtils.cursorRowToContentValues(cursor, contentValues);
            arrayList.add(contentValues);
        }
        return arrayList;
    }

    @NonNull
    public a a(@NonNull SQLiteDatabase sQLiteDatabase, @NonNull String str, @NonNull String str2, @NonNull b bVar, @Nullable String str3, boolean z) {
        List a2 = a(sQLiteDatabase, str, str2);
        int i = 0;
        if (!cx.a((Collection) a2)) {
            try {
                i = sQLiteDatabase.delete(str, str2, null);
            } catch (Throwable unused) {
            }
            if (z) {
                b(a2, bVar, str3, i);
            }
            int size = a2.size();
        } else {
            HashMap hashMap = new HashMap();
            hashMap.put("table_name", str);
            hashMap.put(TapjoyConstants.TJC_API_KEY, String.valueOf(str3));
            tl.a(this.f4953a).reportEvent("select_rows_to_delete_failed", (Map<String, Object>) hashMap);
        }
        return new a(a2, i);
    }

    @Nullable
    private List<ContentValues> a(@NonNull SQLiteDatabase sQLiteDatabase, @NonNull String str, @NonNull String str2) {
        Throwable th;
        Cursor cursor;
        Cursor cursor2 = null;
        try {
            cursor = sQLiteDatabase.rawQuery(String.format("SELECT %s, %s, %s FROM %s WHERE %s", new Object[]{"global_number", "type", "number_of_type", "reports", str2}), null);
            try {
                List a2 = a(cursor);
                cx.a(cursor);
                return a2;
            } catch (Throwable th2) {
                th = th2;
                try {
                    tl.a(this.f4953a).reportError("select_rows_to_delete_exception", th);
                    cx.a(cursor);
                    return null;
                } catch (Throwable th3) {
                    Throwable th4 = th3;
                    cursor2 = cursor;
                    th = th4;
                    cx.a(cursor2);
                    throw th;
                }
            }
        } catch (Throwable th5) {
            th = th5;
            cx.a(cursor2);
            throw th;
        }
    }

    @Nullable
    private w a(@NonNull List<ContentValues> list, @NonNull b bVar, @Nullable String str, int i) {
        vz vzVar;
        try {
            JSONObject jSONObject = new JSONObject();
            JSONArray jSONArray = new JSONArray();
            JSONArray jSONArray2 = new JSONArray();
            JSONArray jSONArray3 = new JSONArray();
            for (ContentValues contentValues : list) {
                Integer asInteger = contentValues.getAsInteger("global_number");
                Integer asInteger2 = contentValues.getAsInteger("type");
                Integer asInteger3 = contentValues.getAsInteger("number_of_type");
                if (!(asInteger == null || asInteger2 == null || asInteger3 == null)) {
                    jSONArray.put(asInteger);
                    jSONArray2.put(asInteger2);
                    jSONArray3.put(asInteger3);
                }
            }
            jSONObject.put("global_number", jSONArray).put("event_type", jSONArray2).put("number_of_type", jSONArray3);
            JSONObject put = new JSONObject().put("details", new JSONObject().put(IronSourceConstants.EVENTS_ERROR_REASON, bVar.c).put("cleared", jSONObject).put("actual_deleted_number", i));
            if (str == null) {
                vzVar = vr.a();
            } else {
                vzVar = vr.a(str);
            }
            return af.i(put.toString(), vzVar);
        } catch (Throwable unused) {
            return null;
        }
    }

    private void b(@NonNull List<ContentValues> list, @NonNull b bVar, @Nullable String str, int i) {
        if (str != null && this.c != null) {
            cj a2 = this.c.a(str, this.b);
            if (a2 != null) {
                w a3 = a(list, bVar, str, i);
                if (a3 != null) {
                    a2.a(a3);
                }
            }
        }
    }
}
