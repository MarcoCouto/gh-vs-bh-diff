package com.yandex.metrica.impl.ob;

import com.github.mikephil.charting.utils.Utils;
import java.io.IOException;
import java.util.Arrays;

public interface rh {

    public static final class a extends e {
        private static volatile a[] l;
        public int b;
        public int c;
        public int d;
        public int e;
        public int f;
        public String g;
        public boolean h;
        public int i;
        public int j;
        public long k;

        public static a[] d() {
            if (l == null) {
                synchronized (c.f4711a) {
                    if (l == null) {
                        l = new a[0];
                    }
                }
            }
            return l;
        }

        public a() {
            e();
        }

        public a e() {
            this.b = -1;
            this.c = 0;
            this.d = -1;
            this.e = -1;
            this.f = -1;
            this.g = "";
            this.h = false;
            this.i = 0;
            this.j = -1;
            this.k = 0;
            this.f4803a = -1;
            return this;
        }

        public void a(b bVar) throws IOException {
            if (this.b != -1) {
                bVar.b(1, this.b);
            }
            if (this.c != 0) {
                bVar.c(2, this.c);
            }
            if (this.d != -1) {
                bVar.b(3, this.d);
            }
            if (this.e != -1) {
                bVar.b(4, this.e);
            }
            if (this.f != -1) {
                bVar.b(5, this.f);
            }
            if (!this.g.equals("")) {
                bVar.a(6, this.g);
            }
            if (this.h) {
                bVar.a(7, this.h);
            }
            if (this.i != 0) {
                bVar.a(8, this.i);
            }
            if (this.j != -1) {
                bVar.b(9, this.j);
            }
            if (this.k != 0) {
                bVar.a(10, this.k);
            }
            super.a(bVar);
        }

        /* access modifiers changed from: protected */
        public int c() {
            int c2 = super.c();
            if (this.b != -1) {
                c2 += b.e(1, this.b);
            }
            if (this.c != 0) {
                c2 += b.f(2, this.c);
            }
            if (this.d != -1) {
                c2 += b.e(3, this.d);
            }
            if (this.e != -1) {
                c2 += b.e(4, this.e);
            }
            if (this.f != -1) {
                c2 += b.e(5, this.f);
            }
            if (!this.g.equals("")) {
                c2 += b.b(6, this.g);
            }
            if (this.h) {
                c2 += b.b(7, this.h);
            }
            if (this.i != 0) {
                c2 += b.d(8, this.i);
            }
            if (this.j != -1) {
                c2 += b.e(9, this.j);
            }
            return this.k != 0 ? c2 + b.d(10, this.k) : c2;
        }

        /* renamed from: b */
        public a a(a aVar) throws IOException {
            while (true) {
                int a2 = aVar.a();
                switch (a2) {
                    case 0:
                        return this;
                    case 8:
                        this.b = aVar.k();
                        break;
                    case 16:
                        this.c = aVar.l();
                        break;
                    case 24:
                        this.d = aVar.k();
                        break;
                    case 32:
                        this.e = aVar.k();
                        break;
                    case 40:
                        this.f = aVar.k();
                        break;
                    case 50:
                        this.g = aVar.i();
                        break;
                    case 56:
                        this.h = aVar.h();
                        break;
                    case 64:
                        int g2 = aVar.g();
                        switch (g2) {
                            case 0:
                            case 1:
                            case 2:
                            case 3:
                            case 4:
                                this.i = g2;
                                break;
                        }
                    case 72:
                        this.j = aVar.k();
                        break;
                    case 80:
                        this.k = aVar.e();
                        break;
                    default:
                        if (g.a(aVar, a2)) {
                            break;
                        } else {
                            return this;
                        }
                }
            }
        }
    }

    public static final class b extends e {
        public C0111b[] b;
        public a[] c;

        public static final class a extends e {
            private static volatile a[] g;
            public long b;
            public long c;
            public a[] d;
            public d[] e;
            public long f;

            public static a[] d() {
                if (g == null) {
                    synchronized (c.f4711a) {
                        if (g == null) {
                            g = new a[0];
                        }
                    }
                }
                return g;
            }

            public a() {
                e();
            }

            public a e() {
                this.b = 0;
                this.c = 0;
                this.d = a.d();
                this.e = d.d();
                this.f = 0;
                this.f4803a = -1;
                return this;
            }

            public void a(b bVar) throws IOException {
                bVar.a(1, this.b);
                bVar.a(2, this.c);
                if (this.d != null && this.d.length > 0) {
                    for (a aVar : this.d) {
                        if (aVar != null) {
                            bVar.a(3, (e) aVar);
                        }
                    }
                }
                if (this.e != null && this.e.length > 0) {
                    for (d dVar : this.e) {
                        if (dVar != null) {
                            bVar.a(4, (e) dVar);
                        }
                    }
                }
                if (this.f != 0) {
                    bVar.a(5, this.f);
                }
                super.a(bVar);
            }

            /* access modifiers changed from: protected */
            public int c() {
                int c2 = super.c() + b.d(1, this.b) + b.d(2, this.c);
                if (this.d != null && this.d.length > 0) {
                    int i = c2;
                    for (a aVar : this.d) {
                        if (aVar != null) {
                            i += b.b(3, (e) aVar);
                        }
                    }
                    c2 = i;
                }
                if (this.e != null && this.e.length > 0) {
                    for (d dVar : this.e) {
                        if (dVar != null) {
                            c2 += b.b(4, (e) dVar);
                        }
                    }
                }
                return this.f != 0 ? c2 + b.d(5, this.f) : c2;
            }

            /* renamed from: b */
            public a a(a aVar) throws IOException {
                while (true) {
                    int a2 = aVar.a();
                    if (a2 == 0) {
                        return this;
                    }
                    if (a2 == 8) {
                        this.b = aVar.e();
                    } else if (a2 == 16) {
                        this.c = aVar.e();
                    } else if (a2 == 26) {
                        int b2 = g.b(aVar, 26);
                        int length = this.d == null ? 0 : this.d.length;
                        a[] aVarArr = new a[(b2 + length)];
                        if (length != 0) {
                            System.arraycopy(this.d, 0, aVarArr, 0, length);
                        }
                        while (length < aVarArr.length - 1) {
                            aVarArr[length] = new a();
                            aVar.a((e) aVarArr[length]);
                            aVar.a();
                            length++;
                        }
                        aVarArr[length] = new a();
                        aVar.a((e) aVarArr[length]);
                        this.d = aVarArr;
                    } else if (a2 == 34) {
                        int b3 = g.b(aVar, 34);
                        int length2 = this.e == null ? 0 : this.e.length;
                        d[] dVarArr = new d[(b3 + length2)];
                        if (length2 != 0) {
                            System.arraycopy(this.e, 0, dVarArr, 0, length2);
                        }
                        while (length2 < dVarArr.length - 1) {
                            dVarArr[length2] = new d();
                            aVar.a((e) dVarArr[length2]);
                            aVar.a();
                            length2++;
                        }
                        dVarArr[length2] = new d();
                        aVar.a((e) dVarArr[length2]);
                        this.e = dVarArr;
                    } else if (a2 == 40) {
                        this.f = aVar.e();
                    } else if (!g.a(aVar, a2)) {
                        return this;
                    }
                }
            }
        }

        /* renamed from: com.yandex.metrica.impl.ob.rh$b$b reason: collision with other inner class name */
        public static final class C0111b extends e {
            private static volatile C0111b[] n;
            public long b;
            public long c;
            public long d;
            public double e;
            public double f;
            public int g;
            public int h;
            public int i;
            public int j;
            public int k;
            public int l;
            public long m;

            public static C0111b[] d() {
                if (n == null) {
                    synchronized (c.f4711a) {
                        if (n == null) {
                            n = new C0111b[0];
                        }
                    }
                }
                return n;
            }

            public C0111b() {
                e();
            }

            public C0111b e() {
                this.b = 0;
                this.c = 0;
                this.d = 0;
                this.e = Utils.DOUBLE_EPSILON;
                this.f = Utils.DOUBLE_EPSILON;
                this.g = 0;
                this.h = 0;
                this.i = 0;
                this.j = 0;
                this.k = 0;
                this.l = 0;
                this.m = 0;
                this.f4803a = -1;
                return this;
            }

            public void a(b bVar) throws IOException {
                bVar.a(1, this.b);
                bVar.a(2, this.c);
                if (this.d != 0) {
                    bVar.a(3, this.d);
                }
                bVar.a(4, this.e);
                bVar.a(5, this.f);
                if (this.g != 0) {
                    bVar.b(6, this.g);
                }
                if (this.h != 0) {
                    bVar.b(7, this.h);
                }
                if (this.i != 0) {
                    bVar.b(8, this.i);
                }
                if (this.j != 0) {
                    bVar.a(9, this.j);
                }
                if (this.k != 0) {
                    bVar.a(10, this.k);
                }
                if (this.l != 0) {
                    bVar.a(11, this.l);
                }
                if (this.m != 0) {
                    bVar.a(12, this.m);
                }
                super.a(bVar);
            }

            /* access modifiers changed from: protected */
            public int c() {
                int c2 = super.c() + b.d(1, this.b) + b.d(2, this.c);
                if (this.d != 0) {
                    c2 += b.d(3, this.d);
                }
                int b2 = c2 + b.b(4, this.e) + b.b(5, this.f);
                if (this.g != 0) {
                    b2 += b.e(6, this.g);
                }
                if (this.h != 0) {
                    b2 += b.e(7, this.h);
                }
                if (this.i != 0) {
                    b2 += b.e(8, this.i);
                }
                if (this.j != 0) {
                    b2 += b.d(9, this.j);
                }
                if (this.k != 0) {
                    b2 += b.d(10, this.k);
                }
                if (this.l != 0) {
                    b2 += b.d(11, this.l);
                }
                return this.m != 0 ? b2 + b.d(12, this.m) : b2;
            }

            /* renamed from: b */
            public C0111b a(a aVar) throws IOException {
                while (true) {
                    int a2 = aVar.a();
                    switch (a2) {
                        case 0:
                            return this;
                        case 8:
                            this.b = aVar.e();
                            break;
                        case 16:
                            this.c = aVar.e();
                            break;
                        case 24:
                            this.d = aVar.e();
                            break;
                        case 33:
                            this.e = aVar.c();
                            break;
                        case 41:
                            this.f = aVar.c();
                            break;
                        case 48:
                            this.g = aVar.k();
                            break;
                        case 56:
                            this.h = aVar.k();
                            break;
                        case 64:
                            this.i = aVar.k();
                            break;
                        case 72:
                            this.j = aVar.g();
                            break;
                        case 80:
                            int g2 = aVar.g();
                            switch (g2) {
                                case 0:
                                case 1:
                                case 2:
                                    this.k = g2;
                                    break;
                            }
                        case 88:
                            int g3 = aVar.g();
                            switch (g3) {
                                case 0:
                                case 1:
                                    this.l = g3;
                                    break;
                            }
                        case 96:
                            this.m = aVar.e();
                            break;
                        default:
                            if (g.a(aVar, a2)) {
                                break;
                            } else {
                                return this;
                            }
                    }
                }
            }
        }

        public b() {
            d();
        }

        public b d() {
            this.b = C0111b.d();
            this.c = a.d();
            this.f4803a = -1;
            return this;
        }

        public void a(b bVar) throws IOException {
            if (this.b != null && this.b.length > 0) {
                for (C0111b bVar2 : this.b) {
                    if (bVar2 != null) {
                        bVar.a(1, (e) bVar2);
                    }
                }
            }
            if (this.c != null && this.c.length > 0) {
                for (a aVar : this.c) {
                    if (aVar != null) {
                        bVar.a(2, (e) aVar);
                    }
                }
            }
            super.a(bVar);
        }

        /* access modifiers changed from: protected */
        public int c() {
            int c2 = super.c();
            if (this.b != null && this.b.length > 0) {
                int i = c2;
                for (C0111b bVar : this.b) {
                    if (bVar != null) {
                        i += b.b(1, (e) bVar);
                    }
                }
                c2 = i;
            }
            if (this.c != null && this.c.length > 0) {
                for (a aVar : this.c) {
                    if (aVar != null) {
                        c2 += b.b(2, (e) aVar);
                    }
                }
            }
            return c2;
        }

        /* renamed from: b */
        public b a(a aVar) throws IOException {
            while (true) {
                int a2 = aVar.a();
                if (a2 == 0) {
                    return this;
                }
                if (a2 == 10) {
                    int b2 = g.b(aVar, 10);
                    int length = this.b == null ? 0 : this.b.length;
                    C0111b[] bVarArr = new C0111b[(b2 + length)];
                    if (length != 0) {
                        System.arraycopy(this.b, 0, bVarArr, 0, length);
                    }
                    while (length < bVarArr.length - 1) {
                        bVarArr[length] = new C0111b();
                        aVar.a((e) bVarArr[length]);
                        aVar.a();
                        length++;
                    }
                    bVarArr[length] = new C0111b();
                    aVar.a((e) bVarArr[length]);
                    this.b = bVarArr;
                } else if (a2 == 18) {
                    int b3 = g.b(aVar, 18);
                    int length2 = this.c == null ? 0 : this.c.length;
                    a[] aVarArr = new a[(b3 + length2)];
                    if (length2 != 0) {
                        System.arraycopy(this.c, 0, aVarArr, 0, length2);
                    }
                    while (length2 < aVarArr.length - 1) {
                        aVarArr[length2] = new a();
                        aVar.a((e) aVarArr[length2]);
                        aVar.a();
                        length2++;
                    }
                    aVarArr[length2] = new a();
                    aVar.a((e) aVarArr[length2]);
                    this.c = aVarArr;
                } else if (!g.a(aVar, a2)) {
                    return this;
                }
            }
        }
    }

    public static final class c extends e {
        public e[] b;
        public d c;
        public a[] d;
        public C0112c[] e;
        public String[] f;
        public f[] g;
        public String[] h;

        public static final class a extends e {
            private static volatile a[] d;
            public String b;
            public String c;

            public static a[] d() {
                if (d == null) {
                    synchronized (c.f4711a) {
                        if (d == null) {
                            d = new a[0];
                        }
                    }
                }
                return d;
            }

            public a() {
                e();
            }

            public a e() {
                this.b = "";
                this.c = "";
                this.f4803a = -1;
                return this;
            }

            public void a(b bVar) throws IOException {
                bVar.a(1, this.b);
                bVar.a(2, this.c);
                super.a(bVar);
            }

            /* access modifiers changed from: protected */
            public int c() {
                return super.c() + b.b(1, this.b) + b.b(2, this.c);
            }

            /* renamed from: b */
            public a a(a aVar) throws IOException {
                while (true) {
                    int a2 = aVar.a();
                    if (a2 == 0) {
                        return this;
                    }
                    if (a2 == 10) {
                        this.b = aVar.i();
                    } else if (a2 == 18) {
                        this.c = aVar.i();
                    } else if (!g.a(aVar, a2)) {
                        return this;
                    }
                }
            }
        }

        public static final class b extends e {
            public double b;
            public double c;
            public long d;
            public int e;
            public int f;
            public int g;
            public int h;
            public int i;
            public String j;

            public b() {
                d();
            }

            public b d() {
                this.b = Utils.DOUBLE_EPSILON;
                this.c = Utils.DOUBLE_EPSILON;
                this.d = 0;
                this.e = 0;
                this.f = 0;
                this.g = 0;
                this.h = 0;
                this.i = 0;
                this.j = "";
                this.f4803a = -1;
                return this;
            }

            public void a(b bVar) throws IOException {
                bVar.a(1, this.b);
                bVar.a(2, this.c);
                if (this.d != 0) {
                    bVar.a(3, this.d);
                }
                if (this.e != 0) {
                    bVar.b(4, this.e);
                }
                if (this.f != 0) {
                    bVar.b(5, this.f);
                }
                if (this.g != 0) {
                    bVar.b(6, this.g);
                }
                if (this.h != 0) {
                    bVar.a(7, this.h);
                }
                if (this.i != 0) {
                    bVar.a(8, this.i);
                }
                if (!this.j.equals("")) {
                    bVar.a(9, this.j);
                }
                super.a(bVar);
            }

            /* access modifiers changed from: protected */
            public int c() {
                int c2 = super.c() + b.b(1, this.b) + b.b(2, this.c);
                if (this.d != 0) {
                    c2 += b.d(3, this.d);
                }
                if (this.e != 0) {
                    c2 += b.e(4, this.e);
                }
                if (this.f != 0) {
                    c2 += b.e(5, this.f);
                }
                if (this.g != 0) {
                    c2 += b.e(6, this.g);
                }
                if (this.h != 0) {
                    c2 += b.d(7, this.h);
                }
                if (this.i != 0) {
                    c2 += b.d(8, this.i);
                }
                return !this.j.equals("") ? c2 + b.b(9, this.j) : c2;
            }

            /* renamed from: b */
            public b a(a aVar) throws IOException {
                while (true) {
                    int a2 = aVar.a();
                    if (a2 == 0) {
                        return this;
                    }
                    if (a2 == 9) {
                        this.b = aVar.c();
                    } else if (a2 == 17) {
                        this.c = aVar.c();
                    } else if (a2 == 24) {
                        this.d = aVar.e();
                    } else if (a2 == 32) {
                        this.e = aVar.k();
                    } else if (a2 == 40) {
                        this.f = aVar.k();
                    } else if (a2 == 48) {
                        this.g = aVar.k();
                    } else if (a2 != 56) {
                        if (a2 == 64) {
                            int g2 = aVar.g();
                            switch (g2) {
                                case 0:
                                case 1:
                                case 2:
                                    this.i = g2;
                                    break;
                            }
                        } else if (a2 == 74) {
                            this.j = aVar.i();
                        } else if (!g.a(aVar, a2)) {
                            return this;
                        }
                    } else {
                        this.h = aVar.g();
                    }
                }
            }
        }

        /* renamed from: com.yandex.metrica.impl.ob.rh$c$c reason: collision with other inner class name */
        public static final class C0112c extends e {
            private static volatile C0112c[] d;
            public String b;
            public String c;

            public static C0112c[] d() {
                if (d == null) {
                    synchronized (c.f4711a) {
                        if (d == null) {
                            d = new C0112c[0];
                        }
                    }
                }
                return d;
            }

            public C0112c() {
                e();
            }

            public C0112c e() {
                this.b = "";
                this.c = "";
                this.f4803a = -1;
                return this;
            }

            public void a(b bVar) throws IOException {
                bVar.a(1, this.b);
                bVar.a(2, this.c);
                super.a(bVar);
            }

            /* access modifiers changed from: protected */
            public int c() {
                return super.c() + b.b(1, this.b) + b.b(2, this.c);
            }

            /* renamed from: b */
            public C0112c a(a aVar) throws IOException {
                while (true) {
                    int a2 = aVar.a();
                    if (a2 == 0) {
                        return this;
                    }
                    if (a2 == 10) {
                        this.b = aVar.i();
                    } else if (a2 == 18) {
                        this.c = aVar.i();
                    } else if (!g.a(aVar, a2)) {
                        return this;
                    }
                }
            }
        }

        public static final class d extends e {
            public String b;
            public String c;
            public String d;
            public int e;
            public String f;
            public String g;
            public boolean h;
            public int i;
            public String j;
            public String k;
            public String l;
            public int m;
            public a[] n;
            public String o;

            public static final class a extends e {
                private static volatile a[] d;
                public String b;
                public long c;

                public static a[] d() {
                    if (d == null) {
                        synchronized (c.f4711a) {
                            if (d == null) {
                                d = new a[0];
                            }
                        }
                    }
                    return d;
                }

                public a() {
                    e();
                }

                public a e() {
                    this.b = "";
                    this.c = 0;
                    this.f4803a = -1;
                    return this;
                }

                public void a(b bVar) throws IOException {
                    bVar.a(1, this.b);
                    bVar.a(2, this.c);
                    super.a(bVar);
                }

                /* access modifiers changed from: protected */
                public int c() {
                    return super.c() + b.b(1, this.b) + b.d(2, this.c);
                }

                /* renamed from: b */
                public a a(a aVar) throws IOException {
                    while (true) {
                        int a2 = aVar.a();
                        if (a2 == 0) {
                            return this;
                        }
                        if (a2 == 10) {
                            this.b = aVar.i();
                        } else if (a2 == 16) {
                            this.c = aVar.e();
                        } else if (!g.a(aVar, a2)) {
                            return this;
                        }
                    }
                }
            }

            public d() {
                d();
            }

            public d d() {
                this.b = "";
                this.c = "";
                this.d = "";
                this.e = 0;
                this.f = "";
                this.g = "";
                this.h = false;
                this.i = 0;
                this.j = "";
                this.k = "";
                this.l = "";
                this.m = 0;
                this.n = a.d();
                this.o = "";
                this.f4803a = -1;
                return this;
            }

            public void a(b bVar) throws IOException {
                if (!this.b.equals("")) {
                    bVar.a(1, this.b);
                }
                if (!this.c.equals("")) {
                    bVar.a(2, this.c);
                }
                if (!this.d.equals("")) {
                    bVar.a(4, this.d);
                }
                if (this.e != 0) {
                    bVar.b(5, this.e);
                }
                if (!this.f.equals("")) {
                    bVar.a(10, this.f);
                }
                if (!this.g.equals("")) {
                    bVar.a(15, this.g);
                }
                if (this.h) {
                    bVar.a(17, this.h);
                }
                if (this.i != 0) {
                    bVar.b(18, this.i);
                }
                if (!this.j.equals("")) {
                    bVar.a(19, this.j);
                }
                if (!this.k.equals("")) {
                    bVar.a(20, this.k);
                }
                if (!this.l.equals("")) {
                    bVar.a(21, this.l);
                }
                if (this.m != 0) {
                    bVar.b(22, this.m);
                }
                if (this.n != null && this.n.length > 0) {
                    for (a aVar : this.n) {
                        if (aVar != null) {
                            bVar.a(23, (e) aVar);
                        }
                    }
                }
                if (!this.o.equals("")) {
                    bVar.a(24, this.o);
                }
                super.a(bVar);
            }

            /* access modifiers changed from: protected */
            public int c() {
                int c2 = super.c();
                if (!this.b.equals("")) {
                    c2 += b.b(1, this.b);
                }
                if (!this.c.equals("")) {
                    c2 += b.b(2, this.c);
                }
                if (!this.d.equals("")) {
                    c2 += b.b(4, this.d);
                }
                if (this.e != 0) {
                    c2 += b.e(5, this.e);
                }
                if (!this.f.equals("")) {
                    c2 += b.b(10, this.f);
                }
                if (!this.g.equals("")) {
                    c2 += b.b(15, this.g);
                }
                if (this.h) {
                    c2 += b.b(17, this.h);
                }
                if (this.i != 0) {
                    c2 += b.e(18, this.i);
                }
                if (!this.j.equals("")) {
                    c2 += b.b(19, this.j);
                }
                if (!this.k.equals("")) {
                    c2 += b.b(20, this.k);
                }
                if (!this.l.equals("")) {
                    c2 += b.b(21, this.l);
                }
                if (this.m != 0) {
                    c2 += b.e(22, this.m);
                }
                if (this.n != null && this.n.length > 0) {
                    for (a aVar : this.n) {
                        if (aVar != null) {
                            c2 += b.b(23, (e) aVar);
                        }
                    }
                }
                return !this.o.equals("") ? c2 + b.b(24, this.o) : c2;
            }

            /* renamed from: b */
            public d a(a aVar) throws IOException {
                while (true) {
                    int a2 = aVar.a();
                    switch (a2) {
                        case 0:
                            return this;
                        case 10:
                            this.b = aVar.i();
                            break;
                        case 18:
                            this.c = aVar.i();
                            break;
                        case 34:
                            this.d = aVar.i();
                            break;
                        case 40:
                            this.e = aVar.k();
                            break;
                        case 82:
                            this.f = aVar.i();
                            break;
                        case 122:
                            this.g = aVar.i();
                            break;
                        case 136:
                            this.h = aVar.h();
                            break;
                        case 144:
                            this.i = aVar.k();
                            break;
                        case 154:
                            this.j = aVar.i();
                            break;
                        case 162:
                            this.k = aVar.i();
                            break;
                        case 170:
                            this.l = aVar.i();
                            break;
                        case 176:
                            this.m = aVar.k();
                            break;
                        case 186:
                            int b2 = g.b(aVar, 186);
                            int length = this.n == null ? 0 : this.n.length;
                            a[] aVarArr = new a[(b2 + length)];
                            if (length != 0) {
                                System.arraycopy(this.n, 0, aVarArr, 0, length);
                            }
                            while (length < aVarArr.length - 1) {
                                aVarArr[length] = new a();
                                aVar.a((e) aVarArr[length]);
                                aVar.a();
                                length++;
                            }
                            aVarArr[length] = new a();
                            aVar.a((e) aVarArr[length]);
                            this.n = aVarArr;
                            break;
                        case 194:
                            this.o = aVar.i();
                            break;
                        default:
                            if (g.a(aVar, a2)) {
                                break;
                            } else {
                                return this;
                            }
                    }
                }
            }
        }

        public static final class e extends e {
            private static volatile e[] e;
            public long b;
            public b c;
            public a[] d;

            public static final class a extends e {
                private static volatile a[] r;
                public long b;
                public long c;
                public int d;
                public String e;
                public byte[] f;
                public b g;
                public b h;
                public String i;
                public C0113a j;
                public int k;
                public int l;
                public int m;
                public byte[] n;
                public int o;
                public long p;
                public long q;

                /* renamed from: com.yandex.metrica.impl.ob.rh$c$e$a$a reason: collision with other inner class name */
                public static final class C0113a extends e {
                    public String b;
                    public String c;
                    public String d;

                    public C0113a() {
                        d();
                    }

                    public C0113a d() {
                        this.b = "";
                        this.c = "";
                        this.d = "";
                        this.f4803a = -1;
                        return this;
                    }

                    public void a(b bVar) throws IOException {
                        bVar.a(1, this.b);
                        if (!this.c.equals("")) {
                            bVar.a(2, this.c);
                        }
                        if (!this.d.equals("")) {
                            bVar.a(3, this.d);
                        }
                        super.a(bVar);
                    }

                    /* access modifiers changed from: protected */
                    public int c() {
                        int c2 = super.c() + b.b(1, this.b);
                        if (!this.c.equals("")) {
                            c2 += b.b(2, this.c);
                        }
                        return !this.d.equals("") ? c2 + b.b(3, this.d) : c2;
                    }

                    /* renamed from: b */
                    public C0113a a(a aVar) throws IOException {
                        while (true) {
                            int a2 = aVar.a();
                            if (a2 == 0) {
                                return this;
                            }
                            if (a2 == 10) {
                                this.b = aVar.i();
                            } else if (a2 == 18) {
                                this.c = aVar.i();
                            } else if (a2 == 26) {
                                this.d = aVar.i();
                            } else if (!g.a(aVar, a2)) {
                                return this;
                            }
                        }
                    }
                }

                public static final class b extends e {
                    public a[] b;
                    public d[] c;
                    public int d;
                    public String e;
                    public C0114a f;

                    /* renamed from: com.yandex.metrica.impl.ob.rh$c$e$a$b$a reason: collision with other inner class name */
                    public static final class C0114a extends e {
                        public String b;
                        public int c;

                        public C0114a() {
                            d();
                        }

                        public C0114a d() {
                            this.b = "";
                            this.c = 0;
                            this.f4803a = -1;
                            return this;
                        }

                        public void a(b bVar) throws IOException {
                            bVar.a(1, this.b);
                            if (this.c != 0) {
                                bVar.a(2, this.c);
                            }
                            super.a(bVar);
                        }

                        /* access modifiers changed from: protected */
                        public int c() {
                            int c2 = super.c() + b.b(1, this.b);
                            return this.c != 0 ? c2 + b.d(2, this.c) : c2;
                        }

                        /* renamed from: b */
                        public C0114a a(a aVar) throws IOException {
                            while (true) {
                                int a2 = aVar.a();
                                if (a2 == 0) {
                                    return this;
                                }
                                if (a2 != 10) {
                                    if (a2 == 16) {
                                        int g = aVar.g();
                                        switch (g) {
                                            case 0:
                                            case 1:
                                            case 2:
                                                this.c = g;
                                                break;
                                        }
                                    } else if (!g.a(aVar, a2)) {
                                        return this;
                                    }
                                } else {
                                    this.b = aVar.i();
                                }
                            }
                        }
                    }

                    public b() {
                        d();
                    }

                    public b d() {
                        this.b = a.d();
                        this.c = d.d();
                        this.d = 2;
                        this.e = "";
                        this.f = null;
                        this.f4803a = -1;
                        return this;
                    }

                    public void a(b bVar) throws IOException {
                        if (this.b != null && this.b.length > 0) {
                            for (a aVar : this.b) {
                                if (aVar != null) {
                                    bVar.a(1, (e) aVar);
                                }
                            }
                        }
                        if (this.c != null && this.c.length > 0) {
                            for (d dVar : this.c) {
                                if (dVar != null) {
                                    bVar.a(2, (e) dVar);
                                }
                            }
                        }
                        if (this.d != 2) {
                            bVar.a(3, this.d);
                        }
                        if (!this.e.equals("")) {
                            bVar.a(4, this.e);
                        }
                        if (this.f != null) {
                            bVar.a(5, (e) this.f);
                        }
                        super.a(bVar);
                    }

                    /* access modifiers changed from: protected */
                    public int c() {
                        int c2 = super.c();
                        if (this.b != null && this.b.length > 0) {
                            int i = c2;
                            for (a aVar : this.b) {
                                if (aVar != null) {
                                    i += b.b(1, (e) aVar);
                                }
                            }
                            c2 = i;
                        }
                        if (this.c != null && this.c.length > 0) {
                            for (d dVar : this.c) {
                                if (dVar != null) {
                                    c2 += b.b(2, (e) dVar);
                                }
                            }
                        }
                        if (this.d != 2) {
                            c2 += b.d(3, this.d);
                        }
                        if (!this.e.equals("")) {
                            c2 += b.b(4, this.e);
                        }
                        return this.f != null ? c2 + b.b(5, (e) this.f) : c2;
                    }

                    /* renamed from: b */
                    public b a(a aVar) throws IOException {
                        while (true) {
                            int a2 = aVar.a();
                            if (a2 == 0) {
                                return this;
                            }
                            if (a2 == 10) {
                                int b2 = g.b(aVar, 10);
                                int length = this.b == null ? 0 : this.b.length;
                                a[] aVarArr = new a[(b2 + length)];
                                if (length != 0) {
                                    System.arraycopy(this.b, 0, aVarArr, 0, length);
                                }
                                while (length < aVarArr.length - 1) {
                                    aVarArr[length] = new a();
                                    aVar.a((e) aVarArr[length]);
                                    aVar.a();
                                    length++;
                                }
                                aVarArr[length] = new a();
                                aVar.a((e) aVarArr[length]);
                                this.b = aVarArr;
                            } else if (a2 != 18) {
                                if (a2 == 24) {
                                    int g = aVar.g();
                                    switch (g) {
                                        case 0:
                                        case 1:
                                        case 2:
                                        case 3:
                                        case 4:
                                        case 5:
                                        case 6:
                                        case 7:
                                        case 8:
                                        case 9:
                                        case 10:
                                        case 11:
                                        case 12:
                                            this.d = g;
                                            break;
                                    }
                                } else if (a2 == 34) {
                                    this.e = aVar.i();
                                } else if (a2 == 42) {
                                    if (this.f == null) {
                                        this.f = new C0114a();
                                    }
                                    aVar.a((e) this.f);
                                } else if (!g.a(aVar, a2)) {
                                    return this;
                                }
                            } else {
                                int b3 = g.b(aVar, 18);
                                int length2 = this.c == null ? 0 : this.c.length;
                                d[] dVarArr = new d[(b3 + length2)];
                                if (length2 != 0) {
                                    System.arraycopy(this.c, 0, dVarArr, 0, length2);
                                }
                                while (length2 < dVarArr.length - 1) {
                                    dVarArr[length2] = new d();
                                    aVar.a((e) dVarArr[length2]);
                                    aVar.a();
                                    length2++;
                                }
                                dVarArr[length2] = new d();
                                aVar.a((e) dVarArr[length2]);
                                this.c = dVarArr;
                            }
                        }
                    }
                }

                public static a[] d() {
                    if (r == null) {
                        synchronized (c.f4711a) {
                            if (r == null) {
                                r = new a[0];
                            }
                        }
                    }
                    return r;
                }

                public a() {
                    e();
                }

                public a e() {
                    this.b = 0;
                    this.c = 0;
                    this.d = 0;
                    this.e = "";
                    this.f = g.h;
                    this.g = null;
                    this.h = null;
                    this.i = "";
                    this.j = null;
                    this.k = 0;
                    this.l = 0;
                    this.m = -1;
                    this.n = g.h;
                    this.o = -1;
                    this.p = 0;
                    this.q = 0;
                    this.f4803a = -1;
                    return this;
                }

                public void a(b bVar) throws IOException {
                    bVar.a(1, this.b);
                    bVar.a(2, this.c);
                    bVar.b(3, this.d);
                    if (!this.e.equals("")) {
                        bVar.a(4, this.e);
                    }
                    if (!Arrays.equals(this.f, g.h)) {
                        bVar.a(5, this.f);
                    }
                    if (this.g != null) {
                        bVar.a(6, (e) this.g);
                    }
                    if (this.h != null) {
                        bVar.a(7, (e) this.h);
                    }
                    if (!this.i.equals("")) {
                        bVar.a(8, this.i);
                    }
                    if (this.j != null) {
                        bVar.a(9, (e) this.j);
                    }
                    if (this.k != 0) {
                        bVar.b(10, this.k);
                    }
                    if (this.l != 0) {
                        bVar.a(12, this.l);
                    }
                    if (this.m != -1) {
                        bVar.a(13, this.m);
                    }
                    if (!Arrays.equals(this.n, g.h)) {
                        bVar.a(14, this.n);
                    }
                    if (this.o != -1) {
                        bVar.a(15, this.o);
                    }
                    if (this.p != 0) {
                        bVar.a(16, this.p);
                    }
                    if (this.q != 0) {
                        bVar.a(17, this.q);
                    }
                    super.a(bVar);
                }

                /* access modifiers changed from: protected */
                public int c() {
                    int c2 = super.c() + b.d(1, this.b) + b.d(2, this.c) + b.e(3, this.d);
                    if (!this.e.equals("")) {
                        c2 += b.b(4, this.e);
                    }
                    if (!Arrays.equals(this.f, g.h)) {
                        c2 += b.b(5, this.f);
                    }
                    if (this.g != null) {
                        c2 += b.b(6, (e) this.g);
                    }
                    if (this.h != null) {
                        c2 += b.b(7, (e) this.h);
                    }
                    if (!this.i.equals("")) {
                        c2 += b.b(8, this.i);
                    }
                    if (this.j != null) {
                        c2 += b.b(9, (e) this.j);
                    }
                    if (this.k != 0) {
                        c2 += b.e(10, this.k);
                    }
                    if (this.l != 0) {
                        c2 += b.d(12, this.l);
                    }
                    if (this.m != -1) {
                        c2 += b.d(13, this.m);
                    }
                    if (!Arrays.equals(this.n, g.h)) {
                        c2 += b.b(14, this.n);
                    }
                    if (this.o != -1) {
                        c2 += b.d(15, this.o);
                    }
                    if (this.p != 0) {
                        c2 += b.d(16, this.p);
                    }
                    return this.q != 0 ? c2 + b.d(17, this.q) : c2;
                }

                /* renamed from: b */
                public a a(a aVar) throws IOException {
                    while (true) {
                        int a2 = aVar.a();
                        switch (a2) {
                            case 0:
                                return this;
                            case 8:
                                this.b = aVar.e();
                                break;
                            case 16:
                                this.c = aVar.e();
                                break;
                            case 24:
                                this.d = aVar.k();
                                break;
                            case 34:
                                this.e = aVar.i();
                                break;
                            case 42:
                                this.f = aVar.j();
                                break;
                            case 50:
                                if (this.g == null) {
                                    this.g = new b();
                                }
                                aVar.a((e) this.g);
                                break;
                            case 58:
                                if (this.h == null) {
                                    this.h = new b();
                                }
                                aVar.a((e) this.h);
                                break;
                            case 66:
                                this.i = aVar.i();
                                break;
                            case 74:
                                if (this.j == null) {
                                    this.j = new C0113a();
                                }
                                aVar.a((e) this.j);
                                break;
                            case 80:
                                this.k = aVar.k();
                                break;
                            case 96:
                                int g2 = aVar.g();
                                switch (g2) {
                                    case 0:
                                    case 1:
                                        this.l = g2;
                                        break;
                                }
                            case 104:
                                int g3 = aVar.g();
                                switch (g3) {
                                    case -1:
                                    case 0:
                                    case 1:
                                        this.m = g3;
                                        break;
                                }
                            case 114:
                                this.n = aVar.j();
                                break;
                            case 120:
                                int g4 = aVar.g();
                                switch (g4) {
                                    case -1:
                                    case 0:
                                    case 1:
                                        this.o = g4;
                                        break;
                                }
                            case 128:
                                this.p = aVar.e();
                                break;
                            case 136:
                                this.q = aVar.e();
                                break;
                            default:
                                if (g.a(aVar, a2)) {
                                    break;
                                } else {
                                    return this;
                                }
                        }
                    }
                }
            }

            public static final class b extends e {
                public g b;
                public String c;
                public int d;

                public b() {
                    d();
                }

                public b d() {
                    this.b = null;
                    this.c = "";
                    this.d = 0;
                    this.f4803a = -1;
                    return this;
                }

                public void a(b bVar) throws IOException {
                    if (this.b != null) {
                        bVar.a(1, (e) this.b);
                    }
                    bVar.a(2, this.c);
                    if (this.d != 0) {
                        bVar.a(5, this.d);
                    }
                    super.a(bVar);
                }

                /* access modifiers changed from: protected */
                public int c() {
                    int c2 = super.c();
                    if (this.b != null) {
                        c2 += b.b(1, (e) this.b);
                    }
                    int b2 = c2 + b.b(2, this.c);
                    return this.d != 0 ? b2 + b.d(5, this.d) : b2;
                }

                /* renamed from: b */
                public b a(a aVar) throws IOException {
                    while (true) {
                        int a2 = aVar.a();
                        if (a2 == 0) {
                            return this;
                        }
                        if (a2 == 10) {
                            if (this.b == null) {
                                this.b = new g();
                            }
                            aVar.a((e) this.b);
                        } else if (a2 != 18) {
                            if (a2 == 40) {
                                int g = aVar.g();
                                switch (g) {
                                    case 0:
                                    case 1:
                                    case 2:
                                        this.d = g;
                                        break;
                                }
                            } else if (!g.a(aVar, a2)) {
                                return this;
                            }
                        } else {
                            this.c = aVar.i();
                        }
                    }
                }
            }

            public static e[] d() {
                if (e == null) {
                    synchronized (c.f4711a) {
                        if (e == null) {
                            e = new e[0];
                        }
                    }
                }
                return e;
            }

            public e() {
                e();
            }

            public e e() {
                this.b = 0;
                this.c = null;
                this.d = a.d();
                this.f4803a = -1;
                return this;
            }

            public void a(b bVar) throws IOException {
                bVar.a(1, this.b);
                if (this.c != null) {
                    bVar.a(2, (e) this.c);
                }
                if (this.d != null && this.d.length > 0) {
                    for (a aVar : this.d) {
                        if (aVar != null) {
                            bVar.a(3, (e) aVar);
                        }
                    }
                }
                super.a(bVar);
            }

            /* access modifiers changed from: protected */
            public int c() {
                int c2 = super.c() + b.d(1, this.b);
                if (this.c != null) {
                    c2 += b.b(2, (e) this.c);
                }
                if (this.d != null && this.d.length > 0) {
                    for (a aVar : this.d) {
                        if (aVar != null) {
                            c2 += b.b(3, (e) aVar);
                        }
                    }
                }
                return c2;
            }

            /* renamed from: b */
            public e a(a aVar) throws IOException {
                while (true) {
                    int a2 = aVar.a();
                    if (a2 == 0) {
                        return this;
                    }
                    if (a2 == 8) {
                        this.b = aVar.e();
                    } else if (a2 == 18) {
                        if (this.c == null) {
                            this.c = new b();
                        }
                        aVar.a((e) this.c);
                    } else if (a2 == 26) {
                        int b2 = g.b(aVar, 26);
                        int length = this.d == null ? 0 : this.d.length;
                        a[] aVarArr = new a[(b2 + length)];
                        if (length != 0) {
                            System.arraycopy(this.d, 0, aVarArr, 0, length);
                        }
                        while (length < aVarArr.length - 1) {
                            aVarArr[length] = new a();
                            aVar.a((e) aVarArr[length]);
                            aVar.a();
                            length++;
                        }
                        aVarArr[length] = new a();
                        aVar.a((e) aVarArr[length]);
                        this.d = aVarArr;
                    } else if (!g.a(aVar, a2)) {
                        return this;
                    }
                }
            }
        }

        public static final class f extends e {
            private static volatile f[] g;
            public int b;
            public int c;
            public String d;
            public boolean e;
            public String f;

            public static f[] d() {
                if (g == null) {
                    synchronized (c.f4711a) {
                        if (g == null) {
                            g = new f[0];
                        }
                    }
                }
                return g;
            }

            public f() {
                e();
            }

            public f e() {
                this.b = 0;
                this.c = 0;
                this.d = "";
                this.e = false;
                this.f = "";
                this.f4803a = -1;
                return this;
            }

            public void a(b bVar) throws IOException {
                if (this.b != 0) {
                    bVar.b(1, this.b);
                }
                if (this.c != 0) {
                    bVar.b(2, this.c);
                }
                if (!this.d.equals("")) {
                    bVar.a(3, this.d);
                }
                if (this.e) {
                    bVar.a(4, this.e);
                }
                if (!this.f.equals("")) {
                    bVar.a(5, this.f);
                }
                super.a(bVar);
            }

            /* access modifiers changed from: protected */
            public int c() {
                int c2 = super.c();
                if (this.b != 0) {
                    c2 += b.e(1, this.b);
                }
                if (this.c != 0) {
                    c2 += b.e(2, this.c);
                }
                if (!this.d.equals("")) {
                    c2 += b.b(3, this.d);
                }
                if (this.e) {
                    c2 += b.b(4, this.e);
                }
                return !this.f.equals("") ? c2 + b.b(5, this.f) : c2;
            }

            /* renamed from: b */
            public f a(a aVar) throws IOException {
                while (true) {
                    int a2 = aVar.a();
                    if (a2 == 0) {
                        return this;
                    }
                    if (a2 == 8) {
                        this.b = aVar.k();
                    } else if (a2 == 16) {
                        this.c = aVar.k();
                    } else if (a2 == 26) {
                        this.d = aVar.i();
                    } else if (a2 == 32) {
                        this.e = aVar.h();
                    } else if (a2 == 42) {
                        this.f = aVar.i();
                    } else if (!g.a(aVar, a2)) {
                        return this;
                    }
                }
            }
        }

        public static final class g extends e {
            public long b;
            public int c;
            public long d;
            public boolean e;

            public g() {
                d();
            }

            public g d() {
                this.b = 0;
                this.c = 0;
                this.d = 0;
                this.e = false;
                this.f4803a = -1;
                return this;
            }

            public void a(b bVar) throws IOException {
                bVar.a(1, this.b);
                bVar.c(2, this.c);
                if (this.d != 0) {
                    bVar.b(3, this.d);
                }
                if (this.e) {
                    bVar.a(4, this.e);
                }
                super.a(bVar);
            }

            /* access modifiers changed from: protected */
            public int c() {
                int c2 = super.c() + b.d(1, this.b) + b.f(2, this.c);
                if (this.d != 0) {
                    c2 += b.e(3, this.d);
                }
                return this.e ? c2 + b.b(4, this.e) : c2;
            }

            /* renamed from: b */
            public g a(a aVar) throws IOException {
                while (true) {
                    int a2 = aVar.a();
                    if (a2 == 0) {
                        return this;
                    }
                    if (a2 == 8) {
                        this.b = aVar.e();
                    } else if (a2 == 16) {
                        this.c = aVar.l();
                    } else if (a2 == 24) {
                        this.d = aVar.f();
                    } else if (a2 == 32) {
                        this.e = aVar.h();
                    } else if (!g.a(aVar, a2)) {
                        return this;
                    }
                }
            }
        }

        public c() {
            d();
        }

        public c d() {
            this.b = e.d();
            this.c = null;
            this.d = a.d();
            this.e = C0112c.d();
            this.f = g.f;
            this.g = f.d();
            this.h = g.f;
            this.f4803a = -1;
            return this;
        }

        public void a(b bVar) throws IOException {
            if (this.b != null && this.b.length > 0) {
                for (e eVar : this.b) {
                    if (eVar != null) {
                        bVar.a(3, (e) eVar);
                    }
                }
            }
            if (this.c != null) {
                bVar.a(4, (e) this.c);
            }
            if (this.d != null && this.d.length > 0) {
                for (a aVar : this.d) {
                    if (aVar != null) {
                        bVar.a(7, (e) aVar);
                    }
                }
            }
            if (this.e != null && this.e.length > 0) {
                for (C0112c cVar : this.e) {
                    if (cVar != null) {
                        bVar.a(8, (e) cVar);
                    }
                }
            }
            if (this.f != null && this.f.length > 0) {
                for (String str : this.f) {
                    if (str != null) {
                        bVar.a(9, str);
                    }
                }
            }
            if (this.g != null && this.g.length > 0) {
                for (f fVar : this.g) {
                    if (fVar != null) {
                        bVar.a(10, (e) fVar);
                    }
                }
            }
            if (this.h != null && this.h.length > 0) {
                for (String str2 : this.h) {
                    if (str2 != null) {
                        bVar.a(11, str2);
                    }
                }
            }
            super.a(bVar);
        }

        /* access modifiers changed from: protected */
        public int c() {
            int c2 = super.c();
            if (this.b != null && this.b.length > 0) {
                int i = c2;
                for (e eVar : this.b) {
                    if (eVar != null) {
                        i += b.b(3, (e) eVar);
                    }
                }
                c2 = i;
            }
            if (this.c != null) {
                c2 += b.b(4, (e) this.c);
            }
            if (this.d != null && this.d.length > 0) {
                int i2 = c2;
                for (a aVar : this.d) {
                    if (aVar != null) {
                        i2 += b.b(7, (e) aVar);
                    }
                }
                c2 = i2;
            }
            if (this.e != null && this.e.length > 0) {
                int i3 = c2;
                for (C0112c cVar : this.e) {
                    if (cVar != null) {
                        i3 += b.b(8, (e) cVar);
                    }
                }
                c2 = i3;
            }
            if (this.f != null && this.f.length > 0) {
                int i4 = 0;
                int i5 = 0;
                for (String str : this.f) {
                    if (str != null) {
                        i5++;
                        i4 += b.b(str);
                    }
                }
                c2 = c2 + i4 + (i5 * 1);
            }
            if (this.g != null && this.g.length > 0) {
                int i6 = c2;
                for (f fVar : this.g) {
                    if (fVar != null) {
                        i6 += b.b(10, (e) fVar);
                    }
                }
                c2 = i6;
            }
            if (this.h == null || this.h.length <= 0) {
                return c2;
            }
            int i7 = 0;
            int i8 = 0;
            for (String str2 : this.h) {
                if (str2 != null) {
                    i8++;
                    i7 += b.b(str2);
                }
            }
            return c2 + i7 + (i8 * 1);
        }

        /* renamed from: b */
        public c a(a aVar) throws IOException {
            while (true) {
                int a2 = aVar.a();
                if (a2 == 0) {
                    return this;
                }
                if (a2 == 26) {
                    int b2 = g.b(aVar, 26);
                    int length = this.b == null ? 0 : this.b.length;
                    e[] eVarArr = new e[(b2 + length)];
                    if (length != 0) {
                        System.arraycopy(this.b, 0, eVarArr, 0, length);
                    }
                    while (length < eVarArr.length - 1) {
                        eVarArr[length] = new e();
                        aVar.a((e) eVarArr[length]);
                        aVar.a();
                        length++;
                    }
                    eVarArr[length] = new e();
                    aVar.a((e) eVarArr[length]);
                    this.b = eVarArr;
                } else if (a2 == 34) {
                    if (this.c == null) {
                        this.c = new d();
                    }
                    aVar.a((e) this.c);
                } else if (a2 == 58) {
                    int b3 = g.b(aVar, 58);
                    int length2 = this.d == null ? 0 : this.d.length;
                    a[] aVarArr = new a[(b3 + length2)];
                    if (length2 != 0) {
                        System.arraycopy(this.d, 0, aVarArr, 0, length2);
                    }
                    while (length2 < aVarArr.length - 1) {
                        aVarArr[length2] = new a();
                        aVar.a((e) aVarArr[length2]);
                        aVar.a();
                        length2++;
                    }
                    aVarArr[length2] = new a();
                    aVar.a((e) aVarArr[length2]);
                    this.d = aVarArr;
                } else if (a2 == 66) {
                    int b4 = g.b(aVar, 66);
                    int length3 = this.e == null ? 0 : this.e.length;
                    C0112c[] cVarArr = new C0112c[(b4 + length3)];
                    if (length3 != 0) {
                        System.arraycopy(this.e, 0, cVarArr, 0, length3);
                    }
                    while (length3 < cVarArr.length - 1) {
                        cVarArr[length3] = new C0112c();
                        aVar.a((e) cVarArr[length3]);
                        aVar.a();
                        length3++;
                    }
                    cVarArr[length3] = new C0112c();
                    aVar.a((e) cVarArr[length3]);
                    this.e = cVarArr;
                } else if (a2 == 74) {
                    int b5 = g.b(aVar, 74);
                    int length4 = this.f == null ? 0 : this.f.length;
                    String[] strArr = new String[(b5 + length4)];
                    if (length4 != 0) {
                        System.arraycopy(this.f, 0, strArr, 0, length4);
                    }
                    while (length4 < strArr.length - 1) {
                        strArr[length4] = aVar.i();
                        aVar.a();
                        length4++;
                    }
                    strArr[length4] = aVar.i();
                    this.f = strArr;
                } else if (a2 == 82) {
                    int b6 = g.b(aVar, 82);
                    int length5 = this.g == null ? 0 : this.g.length;
                    f[] fVarArr = new f[(b6 + length5)];
                    if (length5 != 0) {
                        System.arraycopy(this.g, 0, fVarArr, 0, length5);
                    }
                    while (length5 < fVarArr.length - 1) {
                        fVarArr[length5] = new f();
                        aVar.a((e) fVarArr[length5]);
                        aVar.a();
                        length5++;
                    }
                    fVarArr[length5] = new f();
                    aVar.a((e) fVarArr[length5]);
                    this.g = fVarArr;
                } else if (a2 == 90) {
                    int b7 = g.b(aVar, 90);
                    int length6 = this.h == null ? 0 : this.h.length;
                    String[] strArr2 = new String[(b7 + length6)];
                    if (length6 != 0) {
                        System.arraycopy(this.h, 0, strArr2, 0, length6);
                    }
                    while (length6 < strArr2.length - 1) {
                        strArr2[length6] = aVar.i();
                        aVar.a();
                        length6++;
                    }
                    strArr2[length6] = aVar.i();
                    this.h = strArr2;
                } else if (!g.a(aVar, a2)) {
                    return this;
                }
            }
        }
    }

    public static final class d extends e {
        private static volatile d[] g;
        public String b;
        public int c;
        public String d;
        public boolean e;
        public long f;

        public static d[] d() {
            if (g == null) {
                synchronized (c.f4711a) {
                    if (g == null) {
                        g = new d[0];
                    }
                }
            }
            return g;
        }

        public d() {
            e();
        }

        public d e() {
            this.b = "";
            this.c = 0;
            this.d = "";
            this.e = false;
            this.f = 0;
            this.f4803a = -1;
            return this;
        }

        public void a(b bVar) throws IOException {
            bVar.a(1, this.b);
            if (this.c != 0) {
                bVar.c(2, this.c);
            }
            if (!this.d.equals("")) {
                bVar.a(3, this.d);
            }
            if (this.e) {
                bVar.a(4, this.e);
            }
            if (this.f != 0) {
                bVar.a(5, this.f);
            }
            super.a(bVar);
        }

        /* access modifiers changed from: protected */
        public int c() {
            int c2 = super.c() + b.b(1, this.b);
            if (this.c != 0) {
                c2 += b.f(2, this.c);
            }
            if (!this.d.equals("")) {
                c2 += b.b(3, this.d);
            }
            if (this.e) {
                c2 += b.b(4, this.e);
            }
            return this.f != 0 ? c2 + b.d(5, this.f) : c2;
        }

        /* renamed from: b */
        public d a(a aVar) throws IOException {
            while (true) {
                int a2 = aVar.a();
                if (a2 == 0) {
                    return this;
                }
                if (a2 == 10) {
                    this.b = aVar.i();
                } else if (a2 == 16) {
                    this.c = aVar.l();
                } else if (a2 == 26) {
                    this.d = aVar.i();
                } else if (a2 == 32) {
                    this.e = aVar.h();
                } else if (a2 == 40) {
                    this.f = aVar.e();
                } else if (!g.a(aVar, a2)) {
                    return this;
                }
            }
        }
    }
}
