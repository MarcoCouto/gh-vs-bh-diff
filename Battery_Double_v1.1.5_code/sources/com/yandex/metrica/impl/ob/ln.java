package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;

public class ln {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Context f4970a;
    @NonNull
    private final String b;
    private File c;
    private FileLock d;
    private RandomAccessFile e;
    private FileChannel f;

    public ln(@NonNull Context context, @NonNull String str) {
        this.f4970a = context;
        this.b = str;
    }

    public synchronized void a() throws IOException {
        File filesDir = this.f4970a.getFilesDir();
        StringBuilder sb = new StringBuilder();
        sb.append(new File(this.b).getName());
        sb.append(".lock");
        this.c = new File(filesDir, sb.toString());
        this.e = new RandomAccessFile(this.c, "rw");
        this.f = this.e.getChannel();
        this.d = this.f.lock();
    }

    public synchronized void b() {
        String str = "";
        if (this.c != null) {
            str = this.c.getAbsolutePath();
        }
        am.a(str, this.d);
        cx.a((Closeable) this.e);
        cx.a((Closeable) this.f);
        this.e = null;
        this.d = null;
        this.f = null;
    }
}
