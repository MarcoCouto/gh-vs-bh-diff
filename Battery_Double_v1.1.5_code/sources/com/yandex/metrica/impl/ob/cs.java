package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.util.HashSet;

public class cs {

    /* renamed from: a reason: collision with root package name */
    private final a f4751a;
    @Nullable
    private Boolean b;
    private final HashSet<String> c = new HashSet<>();
    private final HashSet<String> d = new HashSet<>();

    public interface a {
        @Nullable
        Boolean a();

        void a(boolean z);
    }

    public static class b implements a {

        /* renamed from: a reason: collision with root package name */
        private final ly f4752a;

        public b(@NonNull ly lyVar) {
            this.f4752a = lyVar;
        }

        public void a(boolean z) {
            this.f4752a.e(z).q();
        }

        @Nullable
        public Boolean a() {
            return this.f4752a.h();
        }
    }

    public cs(@NonNull a aVar) {
        this.f4751a = aVar;
        this.b = this.f4751a.a();
    }

    public synchronized void a(@Nullable Boolean bool) {
        if (cx.a((Object) bool) || this.b == null) {
            this.b = Boolean.valueOf(vi.c(bool));
            this.f4751a.a(this.b.booleanValue());
        }
    }

    public synchronized void a(@NonNull String str, @Nullable Boolean bool) {
        if (cx.a((Object) bool) || (!this.d.contains(str) && !this.c.contains(str))) {
            if (wk.a(bool, true)) {
                this.d.add(str);
                this.c.remove(str);
            } else {
                this.c.add(str);
                this.d.remove(str);
            }
        }
    }

    public synchronized boolean a() {
        boolean z;
        z = this.b == null ? this.d.isEmpty() && this.c.isEmpty() : this.b.booleanValue();
        return z;
    }

    public synchronized boolean b() {
        return this.b == null ? this.d.isEmpty() : this.b.booleanValue();
    }

    public synchronized boolean c() {
        return e();
    }

    public synchronized boolean d() {
        return e();
    }

    private boolean e() {
        if (this.b == null) {
            return !this.c.isEmpty() || this.d.isEmpty();
        }
        return this.b.booleanValue();
    }
}
