package com.yandex.metrica.impl.ob;

import android.net.Uri;
import android.support.annotation.NonNull;
import java.io.BufferedOutputStream;
import java.io.Closeable;
import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.util.Map;
import java.util.Map.Entry;

public abstract class to {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    tr f5195a;
    @NonNull
    Uri b;
    @NonNull
    private Socket c;

    /* access modifiers changed from: 0000 */
    public abstract void a();

    to(@NonNull Socket socket, @NonNull Uri uri, @NonNull tr trVar) {
        this.c = socket;
        this.b = uri;
        this.f5195a = trVar;
    }

    /* access modifiers changed from: 0000 */
    public void a(@NonNull String str, @NonNull Map<String, String> map, @NonNull byte[] bArr) {
        BufferedOutputStream bufferedOutputStream = null;
        try {
            BufferedOutputStream bufferedOutputStream2 = new BufferedOutputStream(this.c.getOutputStream());
            try {
                bufferedOutputStream2.write(str.getBytes());
                a(bufferedOutputStream2);
                for (Entry entry : map.entrySet()) {
                    a((OutputStream) bufferedOutputStream2, (String) entry.getKey(), (String) entry.getValue());
                }
                a(bufferedOutputStream2);
                bufferedOutputStream2.write(bArr);
                bufferedOutputStream2.flush();
                this.f5195a.a(this.c.getLocalPort());
                cx.a((Closeable) bufferedOutputStream2);
            } catch (IOException e) {
                e = e;
                bufferedOutputStream = bufferedOutputStream2;
                try {
                    this.f5195a.a("io_exception_during_sync", (Throwable) e);
                    cx.a((Closeable) bufferedOutputStream);
                } catch (Throwable th) {
                    th = th;
                    bufferedOutputStream2 = bufferedOutputStream;
                    cx.a((Closeable) bufferedOutputStream2);
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                cx.a((Closeable) bufferedOutputStream2);
                throw th;
            }
        } catch (IOException e2) {
            e = e2;
            this.f5195a.a("io_exception_during_sync", (Throwable) e);
            cx.a((Closeable) bufferedOutputStream);
        }
    }

    private void a(@NonNull OutputStream outputStream, @NonNull String str, @NonNull String str2) throws IOException {
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(": ");
        sb.append(str2);
        outputStream.write(sb.toString().getBytes());
        a(outputStream);
    }

    private void a(@NonNull OutputStream outputStream) throws IOException {
        outputStream.write("\r\n".getBytes());
    }
}
