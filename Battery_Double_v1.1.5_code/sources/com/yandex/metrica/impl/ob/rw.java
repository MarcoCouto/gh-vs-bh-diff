package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import com.yandex.metrica.d;
import com.yandex.metrica.f;

public class rw extends rx<sf> implements d {
    public rw(@NonNull xh xhVar, @NonNull Context context, @NonNull String str) {
        this(xhVar, context, str, new sf(), new sa(), new sc());
    }

    @VisibleForTesting
    public rw(@NonNull xh xhVar, @NonNull Context context, @NonNull String str, @NonNull sf sfVar, @NonNull sa saVar, @NonNull sc scVar) {
        super(xhVar, context, str, sfVar, saVar, scVar);
    }

    public void a(@NonNull final f fVar) {
        this.b.a((Runnable) new Runnable() {
            public void run() {
                rw.this.b(rw.this.c.a(fVar));
            }
        });
    }

    public void sendEventsBuffer() {
        ((sf) this.f5103a).sendEventsBuffer();
        this.b.a((Runnable) new Runnable() {
            public void run() {
                rw.this.a().sendEventsBuffer();
            }
        });
    }

    public void a(@Nullable final String str, @Nullable final String str2) {
        ((sf) this.f5103a).a(str, str2);
        this.b.a((Runnable) new Runnable() {
            public void run() {
                rw.this.a().a(str, str2);
            }
        });
    }
}
