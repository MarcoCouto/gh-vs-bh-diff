package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.content.Intent;
import android.os.RemoteException;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import com.yandex.metrica.CounterConfiguration;
import com.yandex.metrica.IMetricaService;
import java.io.BufferedOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;

public class ce implements a {
    /* access modifiers changed from: private */

    /* renamed from: a reason: collision with root package name */
    public final ao f4720a;
    /* access modifiers changed from: private */
    public final bb b;
    /* access modifiers changed from: private */
    public final Object c;
    private final xh d;
    /* access modifiers changed from: private */
    @NonNull
    public final ah e;

    @VisibleForTesting
    class a extends b {
        private final kc d;
        private final vu e;

        a(ce ceVar, @NonNull d dVar) {
            this(dVar, new kc(), new vu());
        }

        @VisibleForTesting
        a(d dVar, @NonNull kc kcVar, @NonNull vu vuVar) {
            super(dVar);
            this.d = kcVar;
            this.e = vuVar;
        }

        /* renamed from: a */
        public Void call() {
            if (this.e.a("Metrica")) {
                b(this.b);
                return null;
            }
            ce.this.b.c();
            return super.call();
        }

        /* access modifiers changed from: 0000 */
        public boolean b() {
            a(this.b);
            return false;
        }

        /* access modifiers changed from: 0000 */
        @VisibleForTesting
        public void a(@NonNull d dVar) {
            if (dVar.d().o() == 0) {
                Context b = ce.this.f4720a.b();
                Intent b2 = cn.b(b);
                dVar.d().a(com.yandex.metrica.impl.ob.af.a.EVENT_TYPE_EXCEPTION_UNHANDLED_FROM_INTENT.a());
                b2.putExtras(dVar.d().a(dVar.a().b()));
                try {
                    b.startService(b2);
                } catch (Throwable unused) {
                    b(dVar);
                }
            } else {
                b(dVar);
            }
        }

        /* access modifiers changed from: 0000 */
        public void b(@NonNull d dVar) {
            PrintWriter printWriter;
            File b = ce.this.e.b(ce.this.b.a());
            if (this.d.a(b)) {
                ee g = dVar.a().g();
                Integer e2 = g.e();
                String f = g.f();
                ah d2 = ce.this.e;
                StringBuilder sb = new StringBuilder();
                sb.append(e2);
                sb.append("-");
                sb.append(f);
                try {
                    printWriter = new PrintWriter(new BufferedOutputStream(new FileOutputStream(d2.a(b, sb.toString()))));
                    try {
                        printWriter.write(new kv(dVar.f4722a, dVar.a(), dVar.e).j());
                    } catch (Throwable th) {
                        th = th;
                        cx.a((Closeable) printWriter);
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    printWriter = null;
                    cx.a((Closeable) printWriter);
                    throw th;
                }
                cx.a((Closeable) printWriter);
            }
        }
    }

    private class b implements Callable<Void> {
        final d b;

        private b(d dVar) {
            this.b = dVar;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:18:0x003a, code lost:
            r0 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
            r0 = r4.b;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:22:0x0045, code lost:
            return null;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:23:0x0046, code lost:
            com.yandex.metrica.impl.ob.dr.a().a((java.lang.Object) r4);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:24:0x004d, code lost:
            throw r0;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:19:0x003c */
        /* renamed from: a */
        public Void call() {
            int i = 0;
            do {
                IMetricaService f = ce.this.b.f();
                if (f == null || !a(f, this.b)) {
                    i++;
                    if (!b() || bh.f4686a.get()) {
                        dr.a().a((Object) this);
                    }
                } else {
                    dr.a().a((Object) this);
                    return null;
                }
            } while (i < 20);
            dr.a().a((Object) this);
            return null;
        }

        /* access modifiers changed from: 0000 */
        public boolean b() {
            ce.this.b.b();
            c();
            return true;
        }

        private boolean a(IMetricaService iMetricaService, d dVar) {
            try {
                ce.this.f4720a.a(iMetricaService, dVar.b(), dVar.b);
                return true;
            } catch (RemoteException unused) {
                return false;
            }
        }

        /* JADX WARNING: Can't wrap try/catch for region: R(4:5|6|7|8) */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x0020 */
        private void c() {
            synchronized (ce.this.c) {
                if (!ce.this.b.e()) {
                    ce.this.c.wait(500, 0);
                    ce.this.c.notifyAll();
                }
            }
        }
    }

    public interface c {
        w a(w wVar);
    }

    public static class d {
        /* access modifiers changed from: private */

        /* renamed from: a reason: collision with root package name */
        public w f4722a;
        /* access modifiers changed from: private */
        public bz b;
        private boolean c = false;
        private c d;
        /* access modifiers changed from: private */
        @Nullable
        public HashMap<com.yandex.metrica.impl.ob.r.a, Integer> e;

        d(w wVar, bz bzVar) {
            this.f4722a = wVar;
            this.b = new bz(new ee(bzVar.g()), new CounterConfiguration(bzVar.h()));
        }

        /* access modifiers changed from: 0000 */
        public d a(c cVar) {
            this.d = cVar;
            return this;
        }

        /* access modifiers changed from: 0000 */
        public d a(@NonNull HashMap<com.yandex.metrica.impl.ob.r.a, Integer> hashMap) {
            this.e = hashMap;
            return this;
        }

        /* access modifiers changed from: 0000 */
        public d a(boolean z) {
            this.c = z;
            return this;
        }

        /* access modifiers changed from: 0000 */
        public bz a() {
            return this.b;
        }

        /* access modifiers changed from: 0000 */
        public w b() {
            return this.d != null ? this.d.a(this.f4722a) : this.f4722a;
        }

        /* access modifiers changed from: 0000 */
        public boolean c() {
            return this.c;
        }

        /* access modifiers changed from: 0000 */
        @VisibleForTesting
        public w d() {
            return this.f4722a;
        }

        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append("ReportToSend{mReport=");
            sb.append(this.f4722a);
            sb.append(", mEnvironment=");
            sb.append(this.b);
            sb.append(", mCrash=");
            sb.append(this.c);
            sb.append(", mAction=");
            sb.append(this.d);
            sb.append(", mTrimmedFields=");
            sb.append(this.e);
            sb.append('}');
            return sb.toString();
        }
    }

    public void b() {
    }

    public ce(ao aoVar) {
        this(aoVar, db.k().c(), new ah());
    }

    public ce(@NonNull ao aoVar, @NonNull xh xhVar, @NonNull ah ahVar) {
        this.c = new Object();
        this.f4720a = aoVar;
        this.d = xhVar;
        this.e = ahVar;
        this.b = aoVar.a();
        this.b.a((a) this);
    }

    public Future<Void> a(d dVar) {
        return this.d.a(dVar.c() ? new a<>(this, dVar) : new b<>(dVar));
    }

    public void a() {
        synchronized (this.c) {
            this.c.notifyAll();
        }
    }
}
