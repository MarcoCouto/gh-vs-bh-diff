package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;

public class om {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final lg f5034a;
    @NonNull
    private final og b;

    public om(@NonNull Context context) {
        this(ld.a(context).h(), new og(context));
    }

    public void a(@NonNull on onVar) {
        String a2 = this.b.a(onVar);
        if (!TextUtils.isEmpty(a2)) {
            this.f5034a.b(onVar.b(), a2);
        }
    }

    @VisibleForTesting
    om(@NonNull lg lgVar, @NonNull og ogVar) {
        this.f5034a = lgVar;
        this.b = ogVar;
    }
}
