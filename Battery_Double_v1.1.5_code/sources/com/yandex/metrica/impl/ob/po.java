package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;

public class po implements pp {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final pp f5064a;
    @NonNull
    private final pp b;

    public static class a {
        @NonNull

        /* renamed from: a reason: collision with root package name */
        private pp f5065a;
        @NonNull
        private pp b;

        private a() {
        }

        public a(@NonNull pp ppVar, @NonNull pp ppVar2) {
            this.f5065a = ppVar;
            this.b = ppVar2;
        }

        public a a(@NonNull uk ukVar) {
            this.b = new py(ukVar.A);
            return this;
        }

        public a a(boolean z) {
            this.f5065a = new pq(z);
            return this;
        }

        public po a() {
            return new po(this.f5065a, this.b);
        }
    }

    @VisibleForTesting
    po(@NonNull pp ppVar, @NonNull pp ppVar2) {
        this.f5064a = ppVar;
        this.b = ppVar2;
    }

    public boolean a(@NonNull String str) {
        return this.b.a(str) && this.f5064a.a(str);
    }

    public a a() {
        return new a(this.f5064a, this.b);
    }

    public static a b() {
        return new a(new pq(false), new py(null));
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("AskForPermissionsStrategy{mLocationFlagStrategy=");
        sb.append(this.f5064a);
        sb.append(", mStartupStateStrategy=");
        sb.append(this.b);
        sb.append('}');
        return sb.toString();
    }
}
