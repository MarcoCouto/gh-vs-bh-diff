package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.support.annotation.WorkerThread;
import com.yandex.metrica.IReporter;
import com.yandex.metrica.ReporterConfig;
import com.yandex.metrica.Revenue;
import com.yandex.metrica.d;
import com.yandex.metrica.f;
import com.yandex.metrica.impl.ob.se;
import com.yandex.metrica.profile.UserProfile;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class rx<B extends se> implements IReporter {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    final B f5103a;
    @NonNull
    final xh b;
    @NonNull
    final sc c;
    @NonNull
    private final sa d;
    @NonNull
    private final Context e;
    @NonNull
    private final f f;

    rx(@NonNull xh xhVar, @NonNull Context context, @NonNull String str, @NonNull B b2) {
        this(xhVar, context, str, b2, new sa(), new sc());
    }

    @VisibleForTesting
    rx(@NonNull xh xhVar, @NonNull Context context, @NonNull String str, @NonNull B b2, @NonNull sa saVar, @NonNull sc scVar) {
        this.b = xhVar;
        this.e = context;
        this.f = f.a(str).b();
        this.f5103a = b2;
        this.d = saVar;
        this.c = scVar;
    }

    /* access modifiers changed from: 0000 */
    @WorkerThread
    public final d a() {
        return this.d.a(this.e).b(this.f);
    }

    public void a(@NonNull final String str) {
        this.b.a((Runnable) new Runnable() {
            public void run() {
                rx.this.b(f.a(str).b());
            }
        });
    }

    public void a(@NonNull final ReporterConfig reporterConfig) {
        this.b.a((Runnable) new Runnable() {
            public void run() {
                rx.this.b(rx.this.c.a(f.a(reporterConfig)));
            }
        });
    }

    /* access modifiers changed from: protected */
    @WorkerThread
    public void b(@NonNull f fVar) {
        this.d.a(this.e).a(fVar);
    }

    public void reportEvent(@NonNull final String str) {
        this.f5103a.reportEvent(str);
        this.b.a((Runnable) new Runnable() {
            public void run() {
                rx.this.a().reportEvent(str);
            }
        });
    }

    public void reportEvent(@NonNull final String str, @Nullable final String str2) {
        this.f5103a.reportEvent(str, str2);
        this.b.a((Runnable) new Runnable() {
            public void run() {
                rx.this.a().reportEvent(str, str2);
            }
        });
    }

    public void reportEvent(@NonNull final String str, @Nullable Map<String, Object> map) {
        final List list;
        this.f5103a.reportEvent(str, map);
        if (map == null) {
            list = null;
        } else {
            list = new ArrayList(map.entrySet());
        }
        this.b.a((Runnable) new Runnable() {
            public void run() {
                LinkedHashMap linkedHashMap = new LinkedHashMap();
                if (list != null) {
                    for (Entry entry : list) {
                        linkedHashMap.put(entry.getKey(), entry.getValue());
                    }
                }
                rx.this.a().reportEvent(str, (Map<String, Object>) linkedHashMap);
            }
        });
    }

    public void reportError(@NonNull final String str, @Nullable final Throwable th) {
        this.f5103a.reportError(str, th);
        this.b.a((Runnable) new Runnable() {
            public void run() {
                rx.this.a().reportError(str, th);
            }
        });
    }

    public void reportUnhandledException(@NonNull final Throwable th) {
        this.f5103a.reportUnhandledException(th);
        this.b.a((Runnable) new Runnable() {
            public void run() {
                rx.this.a().reportUnhandledException(th);
            }
        });
    }

    public void resumeSession() {
        this.f5103a.resumeSession();
        this.b.a((Runnable) new Runnable() {
            public void run() {
                rx.this.a().resumeSession();
            }
        });
    }

    public void pauseSession() {
        this.f5103a.pauseSession();
        this.b.a((Runnable) new Runnable() {
            public void run() {
                rx.this.a().pauseSession();
            }
        });
    }

    public void setUserProfileID(@Nullable final String str) {
        this.f5103a.setUserProfileID(str);
        this.b.a((Runnable) new Runnable() {
            public void run() {
                rx.this.a().setUserProfileID(str);
            }
        });
    }

    public void reportUserProfile(@NonNull final UserProfile userProfile) {
        this.f5103a.reportUserProfile(userProfile);
        this.b.a((Runnable) new Runnable() {
            public void run() {
                rx.this.a().reportUserProfile(userProfile);
            }
        });
    }

    public void reportRevenue(@NonNull final Revenue revenue) {
        this.f5103a.reportRevenue(revenue);
        this.b.a((Runnable) new Runnable() {
            public void run() {
                rx.this.a().reportRevenue(revenue);
            }
        });
    }

    public void setStatisticsSending(final boolean z) {
        this.f5103a.setStatisticsSending(z);
        this.b.a((Runnable) new Runnable() {
            public void run() {
                rx.this.a().setStatisticsSending(z);
            }
        });
    }

    public void sendEventsBuffer() {
        this.f5103a.sendEventsBuffer();
        this.b.a((Runnable) new Runnable() {
            public void run() {
                rx.this.a().sendEventsBuffer();
            }
        });
    }
}
