package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import com.yandex.metrica.IIdentifierCallback;
import com.yandex.metrica.IIdentifierCallback.Reason;
import com.yandex.metrica.IParamsCallback;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

public class tu implements tx {

    /* renamed from: a reason: collision with root package name */
    private static final IIdentifierCallback f5219a = new IIdentifierCallback() {
        public void onReceive(Map<String, String> map) {
        }

        public void onRequestError(Reason reason) {
        }
    };
    @NonNull
    private final AtomicReference<IIdentifierCallback> b;

    public tu(@NonNull IIdentifierCallback iIdentifierCallback) {
        this.b = new AtomicReference<>(iIdentifierCallback);
    }

    public void a(Map<String, String> map) {
        ((IIdentifierCallback) this.b.getAndSet(f5219a)).onReceive(map);
    }

    public void a(@NonNull IParamsCallback.Reason reason, Map<String, String> map) {
        ((IIdentifierCallback) this.b.getAndSet(f5219a)).onRequestError(a(reason));
    }

    @NonNull
    private Reason a(@NonNull IParamsCallback.Reason reason) {
        switch (reason) {
            case NETWORK:
                return Reason.NETWORK;
            case INVALID_RESPONSE:
                return Reason.INVALID_RESPONSE;
            default:
                return Reason.UNKNOWN;
        }
    }
}
