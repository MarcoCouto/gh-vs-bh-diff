package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

public class nz implements nv, nw {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final fe f5020a;
    private AtomicLong b;

    public nz(@NonNull kz kzVar, @NonNull fe feVar) {
        this.f5020a = feVar;
        this.b = new AtomicLong(kzVar.b());
        kzVar.a((nw) this);
    }

    public boolean a() {
        return this.b.get() >= ((long) ((st) this.f5020a.d()).U());
    }

    public void a(@NonNull List<Integer> list) {
        this.b.addAndGet((long) list.size());
    }

    public void b(@NonNull List<Integer> list) {
        this.b.addAndGet((long) (-list.size()));
    }
}
