package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.metrica.impl.ob.rl.a.C0115a;

public abstract class qr<T> extends qw {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final T f5079a;

    /* access modifiers changed from: protected */
    public abstract void a(@NonNull C0115a aVar);

    qr(int i, @NonNull String str, @NonNull T t, @NonNull yk<String> ykVar, @NonNull qo qoVar) {
        super(i, str, ykVar, qoVar);
        this.f5079a = t;
    }

    @NonNull
    public T b() {
        return this.f5079a;
    }

    public void a(@NonNull re reVar) {
        if (f()) {
            C0115a b = b(reVar);
            if (b != null) {
                a(b);
            }
        }
    }

    @Nullable
    private C0115a b(@NonNull re reVar) {
        return e().a(reVar, reVar.a(d(), c()), this);
    }
}
