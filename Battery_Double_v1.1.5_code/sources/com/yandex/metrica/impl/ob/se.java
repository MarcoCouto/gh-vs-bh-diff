package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.metrica.IReporter;
import com.yandex.metrica.Revenue;
import com.yandex.metrica.profile.UserProfile;
import java.util.Map;

public class se implements IReporter {

    /* renamed from: a reason: collision with root package name */
    static final yk<String> f5154a = new yg(new ye("Event name"));
    static final yk<String> b = new yg(new ye("Error message"));
    static final yk<Throwable> c = new yg(new yf("Unhandled exception"));
    static final yk<UserProfile> d = new yg(new yf("User profile"));
    static final yk<Revenue> e = new yg(new yf("Revenue"));

    public void pauseSession() {
    }

    public void resumeSession() {
    }

    public void sendEventsBuffer() {
    }

    public void setStatisticsSending(boolean z) {
    }

    public void setUserProfileID(@Nullable String str) {
    }

    public void reportEvent(@NonNull String str) throws yh {
        f5154a.a(str);
    }

    public void reportEvent(@NonNull String str, @Nullable String str2) throws yh {
        f5154a.a(str);
    }

    public void reportEvent(@NonNull String str, @Nullable Map<String, Object> map) throws yh {
        f5154a.a(str);
    }

    public void reportError(@NonNull String str, @Nullable Throwable th) throws yh {
        b.a(str);
    }

    public void reportUnhandledException(@NonNull Throwable th) throws yh {
        c.a(th);
    }

    public void reportUserProfile(@NonNull UserProfile userProfile) throws yh {
        d.a(userProfile);
    }

    public void reportRevenue(@NonNull Revenue revenue) throws yh {
        e.a(revenue);
    }
}
