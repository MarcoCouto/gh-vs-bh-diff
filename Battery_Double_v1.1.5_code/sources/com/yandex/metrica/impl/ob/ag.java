package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;

public class ag {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final ct f4637a;
    @NonNull
    private final o b;

    public ag(@NonNull Context context) {
        this(new ct(context, "com.yandex.android.appmetrica.build_id"), new o(context, "com.yandex.android.appmetrica.is_offline"));
    }

    @VisibleForTesting
    ag(@NonNull ct ctVar, @NonNull o oVar) {
        this.f4637a = ctVar;
        this.b = oVar;
    }

    @Nullable
    public String a() {
        return (String) this.f4637a.a();
    }

    @Nullable
    public Boolean b() {
        return (Boolean) this.b.a();
    }
}
