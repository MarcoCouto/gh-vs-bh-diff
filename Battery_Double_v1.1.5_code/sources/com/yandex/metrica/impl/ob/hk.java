package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import com.yandex.metrica.impl.ob.np.a;
import java.util.List;

public class hk extends hd {

    /* renamed from: a reason: collision with root package name */
    private final pv f4875a;
    @NonNull
    private final mf<pn> b;
    @NonNull
    private final m c;
    @NonNull
    private final j d;
    @NonNull
    private final k e;

    public hk(en enVar, pv pvVar) {
        this(enVar, pvVar, a.a(pn.class).a(enVar.k()), new m(enVar.k()), new j(), new k(enVar.k()));
    }

    @VisibleForTesting
    hk(en enVar, pv pvVar, @NonNull mf<pn> mfVar, @NonNull m mVar, @NonNull j jVar, @NonNull k kVar) {
        super(enVar);
        this.f4875a = pvVar;
        this.b = mfVar;
        this.c = mVar;
        this.d = jVar;
        this.e = kVar;
    }

    public boolean a(@NonNull w wVar) {
        en a2 = a();
        a2.b().toString();
        if (a2.u().d() && a2.r()) {
            pn pnVar = (pn) this.b.a();
            pn a3 = a(pnVar);
            if (a3 != null) {
                a(a3, wVar, a2.e());
                this.b.a(a3);
            } else if (a2.s()) {
                a(pnVar, wVar, a2.e());
            }
        }
        return false;
    }

    @Nullable
    private pn a(@NonNull pn pnVar) {
        List<pu> list = pnVar.f5063a;
        l lVar = pnVar.b;
        l a2 = this.c.a();
        List<String> list2 = pnVar.c;
        List a3 = this.e.a();
        List<pu> a4 = this.f4875a.a(a().k(), list);
        if (a4 == null && cx.a((Object) lVar, (Object) a2) && vj.a(list2, a3)) {
            return null;
        }
        if (a4 != null) {
            list = a4;
        }
        return new pn(list, a2, a3);
    }

    private void a(@NonNull pn pnVar, @NonNull w wVar, @NonNull ff ffVar) {
        ffVar.c(w.a(wVar, pnVar.f5063a, pnVar.b, this.d, pnVar.c));
    }
}
