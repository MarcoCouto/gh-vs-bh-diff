package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.os.ResultReceiver;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.metrica.CounterConfiguration;
import com.yandex.metrica.CounterConfiguration.a;

public class cj {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final String f4729a;
    @NonNull
    private final Context b;
    @Nullable
    private final a c;
    @NonNull
    private final cl d;

    public cj(@NonNull String str, @NonNull Context context, @NonNull a aVar, @NonNull cl clVar) {
        this.f4729a = str;
        this.b = context;
        switch (aVar) {
            case MAIN:
                this.c = a.SELF_DIAGNOSTIC_MAIN;
                break;
            case MANUAL:
                this.c = a.SELF_DIAGNOSTIC_MANUAL;
                break;
            default:
                this.c = null;
                break;
        }
        this.d = clVar;
    }

    public void a(@NonNull w wVar) {
        if (this.c != null) {
            try {
                CounterConfiguration counterConfiguration = new CounterConfiguration(this.f4729a);
                counterConfiguration.a(this.c);
                this.d.a(wVar.a(new bz(new ee(this.b, (ResultReceiver) null), counterConfiguration).b()));
            } catch (Throwable unused) {
            }
        }
    }
}
