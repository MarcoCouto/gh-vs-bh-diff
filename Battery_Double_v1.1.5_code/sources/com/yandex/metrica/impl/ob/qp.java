package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import com.yandex.metrica.impl.ob.rl.a.C0115a;

public class qp extends qr<Boolean> {
    public qp(@NonNull String str, boolean z, @NonNull yk<String> ykVar, @NonNull qo qoVar) {
        super(3, str, Boolean.valueOf(z), ykVar, qoVar);
    }

    /* access modifiers changed from: protected */
    public void a(@NonNull C0115a aVar) {
        aVar.e.e = ((Boolean) b()).booleanValue();
    }
}
