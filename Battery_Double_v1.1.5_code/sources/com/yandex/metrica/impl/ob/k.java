package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.location.LocationManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import java.util.ArrayList;
import java.util.List;

public class k {
    @Nullable

    /* renamed from: a reason: collision with root package name */
    private final LocationManager f4931a;

    public k(@NonNull Context context) {
        this((LocationManager) context.getSystemService("location"));
    }

    @VisibleForTesting
    k(@Nullable LocationManager locationManager) {
        this.f4931a = locationManager;
    }

    @NonNull
    public List<String> a() {
        return this.f4931a != null ? this.f4931a.getProviders(true) : new ArrayList();
    }
}
