package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.yandex.metrica.Revenue;
import com.yandex.metrica.impl.ob.rl.a;
import com.yandex.metrica.profile.UserProfile;
import com.yandex.metrica.profile.UserProfileUpdate;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;

public abstract class n implements an {
    private static final Collection<Integer> f = new HashSet(Arrays.asList(new Integer[]{Integer.valueOf(14), Integer.valueOf(15)}));
    private static final yk<a> g = new yk<a>() {
        public yi a(@NonNull a aVar) {
            if (cx.a((T[]) aVar.b)) {
                return yi.a(this, "attributes list is empty");
            }
            return yi.a(this);
        }
    };
    private static final yk<Revenue> h = new yo();

    /* renamed from: a reason: collision with root package name */
    protected final Context f4998a;
    protected final bz b;
    @NonNull
    protected vz c = vr.a(this.b.h().e());
    @NonNull
    protected vp d;
    protected final cd e;
    private at i;
    @NonNull
    private final ag j;

    n(Context context, cd cdVar, @NonNull bz bzVar, @NonNull ag agVar) {
        this.f4998a = context.getApplicationContext();
        this.e = cdVar;
        this.b = bzVar;
        this.j = agVar;
        this.b.a(new xx(this.c, "Crash Environment"));
        this.c = vr.a(this.b.h().e());
        this.d = vr.b(this.b.h().e());
        if (vi.a(this.b.h().j())) {
            this.c.a();
            this.d.a();
        }
    }

    /* access modifiers changed from: protected */
    public void a() {
        this.e.a(this.b);
    }

    /* access modifiers changed from: 0000 */
    public void a(ug ugVar) {
        this.b.b(ugVar);
    }

    /* access modifiers changed from: 0000 */
    public void a(at atVar) {
        this.i = atVar;
    }

    public void b(String str, String str2) {
        if (!TextUtils.isEmpty(str)) {
            this.b.a(str, str2);
        } else if (this.c.c()) {
            this.c.b("Invalid Error Environment (key,value) pair: (%s,%s).", str, str2);
        }
    }

    public void a(Map<String, String> map) {
        if (!cx.a((Map) map)) {
            for (Entry entry : map.entrySet()) {
                b((String) entry.getKey(), (String) entry.getValue());
            }
        }
    }

    public void b(Map<String, String> map) {
        if (!cx.a((Map) map)) {
            for (Entry entry : map.entrySet()) {
                c((String) entry.getKey(), (String) entry.getValue());
            }
        }
    }

    public void c(String str, String str2) {
        if (!TextUtils.isEmpty(str)) {
            this.e.a(str, str2, this.b);
        } else if (this.c.c()) {
            this.c.b("Invalid App Environment (key,value) pair: (%s,%s).", str, str2);
        }
    }

    public void b() {
        this.e.b(this.b);
    }

    public void resumeSession() {
        a((String) null);
        if (this.c.c()) {
            this.c.a("Resume session");
        }
    }

    /* access modifiers changed from: 0000 */
    public void a(String str) {
        this.e.a(this);
        this.i.b();
        this.e.a(af.d(str, this.c), this.b);
        a(this.b.d());
    }

    private void a(boolean z) {
        if (z) {
            this.e.a(af.a(af.a.EVENT_TYPE_PURGE_BUFFER, this.c), this.b);
        }
    }

    public void pauseSession() {
        if (this.c.c()) {
            this.c.a("Pause session");
        }
        b((String) null);
    }

    /* access modifiers changed from: 0000 */
    public void b(String str) {
        if (!this.b.a()) {
            this.e.b(this);
            this.i.a();
            this.b.c();
            this.e.a(af.e(str, this.c), this.b);
        }
    }

    public void reportEvent(@NonNull String str) {
        if (this.c.c()) {
            e(str);
        }
        a(af.c(str, this.c));
    }

    public void reportEvent(@NonNull String str, String str2) {
        if (this.c.c()) {
            d(str, str2);
        }
        a(af.a(str, str2, this.c));
    }

    public void reportEvent(@NonNull String str, @Nullable Map<String, Object> map) {
        String str2;
        Map b2 = cx.b(map);
        this.e.a(af.c(str, this.c), d(), b2);
        if (this.c.c()) {
            if (b2 == null) {
                str2 = null;
            } else {
                str2 = b2.toString();
            }
            d(str, str2);
        }
    }

    private void e(String str) {
        if (this.c.c()) {
            StringBuilder sb = new StringBuilder("Event received: ");
            sb.append(d(str));
            this.c.a(sb.toString());
        }
    }

    private void d(String str, String str2) {
        if (this.c.c()) {
            StringBuilder sb = new StringBuilder("Event received: ");
            sb.append(d(str));
            sb.append(". With value: ");
            sb.append(d(str2));
            this.c.a(sb.toString());
        }
    }

    private void e(@Nullable String str, @Nullable String str2) {
        if (this.c.c()) {
            StringBuilder sb = new StringBuilder("Statbox event received ");
            sb.append(" with name: ");
            sb.append(d(str));
            sb.append(" with value: ");
            String d2 = d(str2);
            if (d2.length() > 100) {
                sb.append(d2.substring(0, 100));
                sb.append("...");
            } else {
                sb.append(d2);
            }
            this.c.a(sb.toString());
        }
    }

    public void a(int i2, String str, String str2, Map<String, String> map) {
        Map map2;
        if (!a(i2)) {
            if (map == null) {
                map2 = null;
            } else {
                map2 = new HashMap(map);
            }
            a(af.a(i2, str, str2, map2, this.c));
        }
    }

    private boolean a(int i2) {
        return !f.contains(Integer.valueOf(i2)) && i2 >= 1 && i2 <= 99;
    }

    public void reportError(@NonNull String str, Throwable th) {
        this.e.a(str, new kl(th, null, this.j.a(), this.j.b()), this.b);
        if (this.c.c()) {
            this.c.a("Error received: %s", d(str));
        }
    }

    public void sendEventsBuffer() {
        this.e.a(af.a(af.a.EVENT_TYPE_PURGE_BUFFER, this.c), this.b);
    }

    public void a(@Nullable String str, @Nullable String str2) {
        a(af.b(str, str2, this.c));
        e(str, str2);
    }

    /* access modifiers changed from: 0000 */
    public boolean c() {
        boolean z = !f();
        if (z) {
            this.e.a(af.e("", this.c), this.b);
        }
        return z;
    }

    /* access modifiers changed from: 0000 */
    public bz d() {
        return this.b;
    }

    private void a(w wVar) {
        this.e.a(wVar, this.b);
    }

    public void c(@Nullable String str) {
        f(str);
    }

    private void f(String str) {
        this.e.a(am.b(str), this.b);
        if (this.c.c()) {
            this.c.a("Error received: native");
        }
    }

    public void reportUnhandledException(@NonNull Throwable th) {
        b(new kl(th, null, this.j.a(), this.j.b()));
    }

    public void a(@NonNull kl klVar) {
        b(klVar);
    }

    private void b(@NonNull kl klVar) {
        this.e.a(klVar, this.b);
        c(klVar);
    }

    private void c(@NonNull kl klVar) {
        if (this.c.c()) {
            StringBuilder sb = new StringBuilder("Unhandled exception received: ");
            sb.append(klVar.toString());
            this.c.a(sb.toString());
        }
    }

    public void reportRevenue(@NonNull Revenue revenue) {
        a(revenue);
    }

    private void a(@NonNull Revenue revenue) {
        yi a2 = h.a(revenue);
        if (a2.a()) {
            this.e.a(new cg(revenue, this.c), this.b);
            b(revenue);
        } else if (this.c.c()) {
            vz vzVar = this.c;
            StringBuilder sb = new StringBuilder();
            sb.append("Passed revenue is not valid. Reason: ");
            sb.append(a2.b());
            vzVar.b(sb.toString());
        }
    }

    private void b(@NonNull Revenue revenue) {
        if (this.c.c()) {
            StringBuilder sb = new StringBuilder("Revenue received ");
            sb.append("for productID: ");
            sb.append(d(revenue.productID));
            sb.append(" of quantity: ");
            if (revenue.quantity != null) {
                sb.append(revenue.quantity);
            } else {
                sb.append("<null>");
            }
            sb.append(" with price: ");
            sb.append(revenue.price);
            sb.append(" ");
            sb.append(revenue.currency);
            this.c.a(sb.toString());
        }
    }

    public void reportUserProfile(@NonNull UserProfile userProfile) {
        a(userProfile);
    }

    private void a(@NonNull UserProfile userProfile) {
        re reVar = new re();
        for (UserProfileUpdate userProfileUpdatePatcher : userProfile.getUserProfileUpdates()) {
            rf userProfileUpdatePatcher2 = userProfileUpdatePatcher.getUserProfileUpdatePatcher();
            userProfileUpdatePatcher2.a(this.c);
            userProfileUpdatePatcher2.a(reVar);
        }
        a c2 = reVar.c();
        yi a2 = g.a(c2);
        if (a2.a()) {
            this.e.a(c2, this.b);
            g();
        } else if (this.c.c()) {
            vz vzVar = this.c;
            StringBuilder sb = new StringBuilder();
            sb.append("UserInfo wasn't sent because ");
            sb.append(a2.b());
            vzVar.b(sb.toString());
        }
    }

    private void g() {
        if (this.c.c()) {
            this.c.a(new StringBuilder("User profile received").toString());
        }
    }

    public void setUserProfileID(@Nullable String str) {
        this.e.b(str, this.b);
        if (this.c.c()) {
            StringBuilder sb = new StringBuilder("Set user profile ID: ");
            sb.append(d(str));
            this.c.a(sb.toString());
        }
    }

    public void setStatisticsSending(boolean z) {
        this.b.h().g(z);
    }

    public void a(@NonNull kg kgVar) {
        this.e.a(new kh(kgVar, this.j.a(), this.j.b()), this.b);
    }

    public void e() {
        this.e.a(w.a(this.f4998a), this.b);
    }

    public boolean f() {
        return this.b.a();
    }

    /* access modifiers changed from: protected */
    @NonNull
    public String d(@Nullable String str) {
        if (str == null) {
            return "<null>";
        }
        return str.isEmpty() ? "<empty>" : str;
    }
}
