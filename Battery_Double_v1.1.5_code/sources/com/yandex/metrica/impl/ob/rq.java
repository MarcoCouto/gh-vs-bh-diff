package com.yandex.metrica.impl.ob;

import java.io.IOException;

public interface rq {

    public static final class a extends e {
        public C0120a[] b;
        public String c;
        public long d;
        public boolean e;
        public boolean f;

        /* renamed from: com.yandex.metrica.impl.ob.rq$a$a reason: collision with other inner class name */
        public static final class C0120a extends e {
            private static volatile C0120a[] d;
            public String b;
            public String[] c;

            public static C0120a[] d() {
                if (d == null) {
                    synchronized (c.f4711a) {
                        if (d == null) {
                            d = new C0120a[0];
                        }
                    }
                }
                return d;
            }

            public C0120a() {
                e();
            }

            public C0120a e() {
                this.b = "";
                this.c = g.f;
                this.f4803a = -1;
                return this;
            }

            public void a(b bVar) throws IOException {
                bVar.a(1, this.b);
                if (this.c != null && this.c.length > 0) {
                    for (String str : this.c) {
                        if (str != null) {
                            bVar.a(2, str);
                        }
                    }
                }
                super.a(bVar);
            }

            /* access modifiers changed from: protected */
            public int c() {
                int c2 = super.c() + b.b(1, this.b);
                if (this.c == null || this.c.length <= 0) {
                    return c2;
                }
                int i = 0;
                int i2 = 0;
                for (String str : this.c) {
                    if (str != null) {
                        i2++;
                        i += b.b(str);
                    }
                }
                return c2 + i + (i2 * 1);
            }

            /* renamed from: b */
            public C0120a a(a aVar) throws IOException {
                while (true) {
                    int a2 = aVar.a();
                    if (a2 == 0) {
                        return this;
                    }
                    if (a2 == 10) {
                        this.b = aVar.i();
                    } else if (a2 == 18) {
                        int b2 = g.b(aVar, 18);
                        int length = this.c == null ? 0 : this.c.length;
                        String[] strArr = new String[(b2 + length)];
                        if (length != 0) {
                            System.arraycopy(this.c, 0, strArr, 0, length);
                        }
                        while (length < strArr.length - 1) {
                            strArr[length] = aVar.i();
                            aVar.a();
                            length++;
                        }
                        strArr[length] = aVar.i();
                        this.c = strArr;
                    } else if (!g.a(aVar, a2)) {
                        return this;
                    }
                }
            }
        }

        public a() {
            d();
        }

        public a d() {
            this.b = C0120a.d();
            this.c = "";
            this.d = 0;
            this.e = false;
            this.f = false;
            this.f4803a = -1;
            return this;
        }

        public void a(b bVar) throws IOException {
            if (this.b != null && this.b.length > 0) {
                for (C0120a aVar : this.b) {
                    if (aVar != null) {
                        bVar.a(1, (e) aVar);
                    }
                }
            }
            bVar.a(2, this.c);
            bVar.b(3, this.d);
            bVar.a(4, this.e);
            bVar.a(5, this.f);
            super.a(bVar);
        }

        /* access modifiers changed from: protected */
        public int c() {
            int c2 = super.c();
            if (this.b != null && this.b.length > 0) {
                for (C0120a aVar : this.b) {
                    if (aVar != null) {
                        c2 += b.b(1, (e) aVar);
                    }
                }
            }
            return c2 + b.b(2, this.c) + b.e(3, this.d) + b.b(4, this.e) + b.b(5, this.f);
        }

        /* renamed from: b */
        public a a(a aVar) throws IOException {
            while (true) {
                int a2 = aVar.a();
                if (a2 == 0) {
                    return this;
                }
                if (a2 == 10) {
                    int b2 = g.b(aVar, 10);
                    int length = this.b == null ? 0 : this.b.length;
                    C0120a[] aVarArr = new C0120a[(b2 + length)];
                    if (length != 0) {
                        System.arraycopy(this.b, 0, aVarArr, 0, length);
                    }
                    while (length < aVarArr.length - 1) {
                        aVarArr[length] = new C0120a();
                        aVar.a((e) aVarArr[length]);
                        aVar.a();
                        length++;
                    }
                    aVarArr[length] = new C0120a();
                    aVar.a((e) aVarArr[length]);
                    this.b = aVarArr;
                } else if (a2 == 18) {
                    this.c = aVar.i();
                } else if (a2 == 24) {
                    this.d = aVar.f();
                } else if (a2 == 32) {
                    this.e = aVar.h();
                } else if (a2 == 40) {
                    this.f = aVar.h();
                } else if (!g.a(aVar, a2)) {
                    return this;
                }
            }
        }

        public static a a(byte[] bArr) throws d {
            return (a) e.a(new a(), bArr);
        }
    }
}
