package com.yandex.metrica.impl.ob;

import android.content.Context;

@Deprecated
public class qg extends qd {
    private static final qk d = new qk("SERVICE_API_LEVEL");
    private static final qk e = new qk("CLIENT_API_LEVEL");
    private qk f = new qk(d.a());
    private qk g = new qk(e.a());

    /* access modifiers changed from: protected */
    public String f() {
        return "_migrationpreferences";
    }

    public qg(Context context) {
        super(context, null);
    }

    public int a() {
        return this.c.getInt(this.f.b(), -1);
    }

    public qg b() {
        h(this.f.b());
        return this;
    }

    public qg c() {
        h(this.g.b());
        return this;
    }
}
