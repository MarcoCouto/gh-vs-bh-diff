package com.yandex.metrica.impl.ob;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.ironsource.sdk.constants.Constants.RequestParameters;
import com.tapjoy.TapjoyConstants;
import org.json.JSONObject;

public class hc extends hd {
    public hc(en enVar) {
        super(enVar);
    }

    public boolean a(@NonNull w wVar) {
        String e = wVar.e();
        if (!TextUtils.isEmpty(e)) {
            try {
                JSONObject jSONObject = new JSONObject(e);
                if ("open".equals(jSONObject.optString("type")) && a(jSONObject.optString("link"))) {
                    b();
                }
            } catch (Throwable unused) {
            }
        }
        return false;
    }

    private void b() {
        a().o();
        a().z().a();
    }

    private boolean a(@Nullable String str) {
        String[] split;
        if (!TextUtils.isEmpty(str)) {
            try {
                String queryParameter = Uri.parse(str).getQueryParameter(TapjoyConstants.TJC_REFERRER);
                if (!TextUtils.isEmpty(queryParameter)) {
                    for (String str2 : queryParameter.split(RequestParameters.AMPERSAND)) {
                        int indexOf = str2.indexOf(RequestParameters.EQUAL);
                        if (indexOf >= 0 && "reattribution".equals(Uri.decode(str2.substring(0, indexOf))) && "1".equals(Uri.decode(str2.substring(indexOf + 1)))) {
                            return true;
                        }
                    }
                }
            } catch (Throwable unused) {
            }
        }
        return false;
    }
}
