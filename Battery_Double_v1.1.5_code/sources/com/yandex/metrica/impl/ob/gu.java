package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import com.yandex.metrica.impl.ob.ei;
import com.yandex.metrica.impl.ob.gt;

public class gu<T extends gt, C extends ei> extends gl<T, C> {
    public gu(@NonNull gr<T> grVar, @NonNull C c) {
        super(grVar, c);
    }

    public boolean a(@NonNull w wVar, @NonNull final fs fsVar) {
        return a(wVar, new a<T>() {
            public boolean a(T t, w wVar) {
                return t.a(wVar, fsVar);
            }
        });
    }
}
