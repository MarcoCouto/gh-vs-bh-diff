package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import java.util.List;

public abstract class hz<BaseHandler> {

    /* renamed from: a reason: collision with root package name */
    private final ig f4880a;

    public abstract void a(@NonNull List<BaseHandler> list);

    public hz(ig igVar) {
        this.f4880a = igVar;
    }

    public ig a() {
        return this.f4880a;
    }
}
