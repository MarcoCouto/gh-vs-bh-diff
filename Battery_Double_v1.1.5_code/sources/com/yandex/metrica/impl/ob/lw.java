package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.metrica.impl.ob.i.a;
import java.util.Collections;
import java.util.List;

public class lw extends lx {

    /* renamed from: a reason: collision with root package name */
    public static final String f4985a = null;
    @Deprecated
    public static final qk b = new qk("COLLECT_INSTALLED_APPS");
    static final qk c = new qk("DEPRECATED_NATIVE_CRASHES_CHECKED");
    private static final qk d = new qk("IDENTITY_SEND_TIME");
    private static final qk e = new qk("PERMISSIONS_CHECK_TIME");
    private static final qk f = new qk("USER_INFO");
    private static final qk g = new qk("PROFILE_ID");
    private static final qk h = new qk("APP_ENVIRONMENT");
    private static final qk i = new qk("APP_ENVIRONMENT_REVISION");
    private static final qk j = new qk("LAST_MIGRATION_VERSION");
    private static final qk k = new qk("LAST_APP_VERSION_WITH_FEATURES");
    private static final qk l = new qk("APPLICATION_FEATURES");
    private static final qk m = new qk("CURRENT_SESSION_ID");
    private static final qk n = new qk("ATTRIBUTION_ID");
    private static final qk o = new qk("LAST_STAT_SENDING_DISABLED_REPORTING_TIMESTAMP");
    private static final qk p = new qk("NEXT_EVENT_GLOBAL_NUMBER");
    private static final qk r = new qk("LAST_REQUEST_ID");
    private static final qk s = new qk("CERTIFICATES_SHA1_FINGERPRINTS");
    private static final lt t = new lt();

    public lw(lf lfVar) {
        super(lfVar);
    }

    public int a() {
        return b(p.b(), 0);
    }

    public int a(int i2) {
        return b(t.a(i2), 0);
    }

    public long a(long j2) {
        return b(d.b(), j2);
    }

    public long b() {
        return b(e.b(), 0);
    }

    public int c() {
        return b(k.b(), -1);
    }

    public a d() {
        a aVar;
        synchronized (this) {
            aVar = new a(c(h.b(), "{}"), b(i.b(), 0));
        }
        return aVar;
    }

    public String e() {
        return c(l.b(), "");
    }

    public String f() {
        return c(f.b(), f4985a);
    }

    public lw b(int i2) {
        return (lw) a(p.b(), i2);
    }

    public lw a(int i2, int i3) {
        return (lw) a(t.a(i2), i3);
    }

    public lw a(a aVar) {
        synchronized (this) {
            b(h.b(), aVar.f4882a);
            a(i.b(), aVar.b);
        }
        return this;
    }

    public lw b(long j2) {
        return (lw) a(d.b(), j2);
    }

    public lw c(long j2) {
        return (lw) a(e.b(), j2);
    }

    public lw a(String str) {
        return (lw) b(f.b(), str);
    }

    public long g() {
        return b(j.b(), 0);
    }

    public lw d(long j2) {
        return (lw) a(j.b(), j2);
    }

    public lw c(int i2) {
        return (lw) a(k.b(), i2);
    }

    public lw b(String str) {
        return (lw) b(l.b(), str);
    }

    public lw a(String str, String str2) {
        return (lw) b(new qk("SESSION_", str).b(), str2);
    }

    public String c(String str) {
        return c(new qk("SESSION_", str).b(), "");
    }

    @Nullable
    public String h() {
        return s(g.b());
    }

    public lw d(@Nullable String str) {
        return (lw) b(g.b(), str);
    }

    @NonNull
    public lw d(int i2) {
        return (lw) a(n.b(), i2);
    }

    public int i() {
        return b(n.b(), 1);
    }

    public long j() {
        return b(m.b(), -1);
    }

    @NonNull
    public lw e(long j2) {
        return (lw) a(m.b(), j2);
    }

    public long k() {
        return b(o.b(), 0);
    }

    public lw f(long j2) {
        return (lw) a(o.b(), j2);
    }

    public int l() {
        return b(r.b(), -1);
    }

    public lw e(int i2) {
        return (lw) a(r.b(), i2);
    }

    public boolean m() {
        return b(c.b(), false);
    }

    public lw n() {
        return (lw) a(c.b(), true);
    }

    @NonNull
    public List<String> o() {
        return b(s.b(), Collections.emptyList());
    }

    public lw a(List<String> list) {
        return (lw) a(s.b(), list);
    }
}
