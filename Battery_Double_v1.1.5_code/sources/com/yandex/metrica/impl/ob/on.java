package com.yandex.metrica.impl.ob;

import android.support.annotation.Nullable;
import org.json.JSONArray;

public class on {
    @Nullable

    /* renamed from: a reason: collision with root package name */
    private Long f5035a;
    private long b;
    private long c;
    @Nullable
    private JSONArray d;
    @Nullable
    private JSONArray e;

    @Nullable
    public Long a() {
        return this.f5035a;
    }

    public void a(@Nullable Long l) {
        this.f5035a = l;
    }

    public long b() {
        return this.b;
    }

    public void a(long j) {
        this.b = j;
    }

    @Nullable
    public JSONArray c() {
        return this.d;
    }

    public void a(@Nullable JSONArray jSONArray) {
        this.d = jSONArray;
    }

    @Nullable
    public JSONArray d() {
        return this.e;
    }

    public void b(@Nullable JSONArray jSONArray) {
        this.e = jSONArray;
    }

    public long e() {
        return this.c;
    }

    public void b(long j) {
        this.c = j;
    }
}
