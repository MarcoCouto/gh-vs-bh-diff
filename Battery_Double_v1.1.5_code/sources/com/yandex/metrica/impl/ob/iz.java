package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public class iz {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final jh f4892a;
    @Nullable
    private final Long b;
    @Nullable
    private final Long c;
    @Nullable
    private final Integer d;
    @Nullable
    private final Long e;
    @Nullable
    private final Boolean f;
    @Nullable
    private final Long g;
    @Nullable
    private final Long h;

    static final class a {
        @Nullable

        /* renamed from: a reason: collision with root package name */
        public Long f4893a;
        /* access modifiers changed from: private */
        @NonNull
        public jh b;
        /* access modifiers changed from: private */
        @Nullable
        public Long c;
        /* access modifiers changed from: private */
        @Nullable
        public Long d;
        /* access modifiers changed from: private */
        @Nullable
        public Integer e;
        /* access modifiers changed from: private */
        @Nullable
        public Long f;
        /* access modifiers changed from: private */
        @Nullable
        public Boolean g;
        /* access modifiers changed from: private */
        @Nullable
        public Long h;

        private a(jb jbVar) {
            this.b = jbVar.a();
            this.e = jbVar.b();
        }

        public a a(Long l) {
            this.c = l;
            return this;
        }

        public a b(Long l) {
            this.d = l;
            return this;
        }

        public a c(Long l) {
            this.f = l;
            return this;
        }

        public a a(Boolean bool) {
            this.g = bool;
            return this;
        }

        public a d(Long l) {
            this.h = l;
            return this;
        }

        public a e(Long l) {
            this.f4893a = l;
            return this;
        }

        public iz a() {
            return new iz(this);
        }
    }

    private iz(a aVar) {
        this.f4892a = aVar.b;
        this.d = aVar.e;
        this.b = aVar.c;
        this.c = aVar.d;
        this.e = aVar.f;
        this.f = aVar.g;
        this.g = aVar.h;
        this.h = aVar.f4893a;
    }

    public static final a a(jb jbVar) {
        return new a(jbVar);
    }

    public jh a() {
        return this.f4892a;
    }

    public long a(long j) {
        return this.b == null ? j : this.b.longValue();
    }

    public long b(long j) {
        return this.c == null ? j : this.c.longValue();
    }

    public int a(int i) {
        return this.d == null ? i : this.d.intValue();
    }

    public long c(long j) {
        return this.e == null ? j : this.e.longValue();
    }

    public boolean a(boolean z) {
        return this.f == null ? z : this.f.booleanValue();
    }

    public long d(long j) {
        return this.g == null ? j : this.g.longValue();
    }

    public long e(long j) {
        return this.h == null ? j : this.h.longValue();
    }
}
