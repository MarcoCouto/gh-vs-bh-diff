package com.yandex.metrica.impl.ob;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Point;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;
import java.io.File;
import java.lang.reflect.Method;
import java.util.Locale;

public final class bt {

    /* renamed from: a reason: collision with root package name */
    private static final yc f4697a = new yc();
    private static final vw<Integer, a> b = new vw<Integer, a>(a.UNDEFINED) {
        {
            a(Integer.valueOf(1), a.WIFI);
            a(Integer.valueOf(0), a.CELL);
            if (cx.a(13)) {
                a(Integer.valueOf(7), a.BLUETOOTH);
                a(Integer.valueOf(9), a.ETHERNET);
            }
            a(Integer.valueOf(4), a.MOBILE_DUN);
            a(Integer.valueOf(5), a.MOBILE_HIPRI);
            a(Integer.valueOf(2), a.MOBILE_MMS);
            a(Integer.valueOf(3), a.MOBILE_SUPL);
            a(Integer.valueOf(6), a.WIMAX);
            if (cx.a(21)) {
                a(Integer.valueOf(17), a.VPN);
            }
        }
    };
    @TargetApi(23)
    private static final vw<Integer, a> c = new vw<Integer, a>(a.UNDEFINED) {
        {
            a(Integer.valueOf(1), a.WIFI);
            a(Integer.valueOf(0), a.CELL);
            a(Integer.valueOf(3), a.ETHERNET);
            a(Integer.valueOf(2), a.BLUETOOTH);
            a(Integer.valueOf(4), a.VPN);
            if (cx.a(27)) {
                a(Integer.valueOf(6), a.LOWPAN);
            }
            if (cx.a(26)) {
                a(Integer.valueOf(5), a.WIFI_AWARE);
            }
        }
    };
    private static final vw<a, Integer> d = new vw<a, Integer>(Integer.valueOf(2)) {
        {
            a(a.CELL, Integer.valueOf(0));
            a(a.WIFI, Integer.valueOf(1));
            a(a.BLUETOOTH, Integer.valueOf(3));
            a(a.ETHERNET, Integer.valueOf(4));
            a(a.MOBILE_DUN, Integer.valueOf(5));
            a(a.MOBILE_HIPRI, Integer.valueOf(6));
            a(a.MOBILE_MMS, Integer.valueOf(7));
            a(a.MOBILE_SUPL, Integer.valueOf(8));
            a(a.VPN, Integer.valueOf(9));
            a(a.WIMAX, Integer.valueOf(10));
            a(a.LOWPAN, Integer.valueOf(11));
            a(a.WIFI_AWARE, Integer.valueOf(12));
        }
    };

    public enum a {
        WIFI,
        CELL,
        ETHERNET,
        BLUETOOTH,
        VPN,
        LOWPAN,
        WIFI_AWARE,
        MOBILE_DUN,
        MOBILE_HIPRI,
        MOBILE_MMS,
        MOBILE_SUPL,
        WIMAX,
        OFFLINE,
        UNDEFINED
    }

    public static final class b {

        /* renamed from: a reason: collision with root package name */
        private static final String[] f4699a = {"/sbin/", "/system/bin/", "/system/xbin/", "/data/local/xbin/", "/data/local/bin/", "/system/sd/xbin/", "/system/bin/failsafe/", "/data/local/"};

        public static boolean a() {
            try {
                if (new File("/system/app/Superuser.apk").exists()) {
                    return true;
                }
            } catch (Throwable unused) {
            }
            return false;
        }

        public static boolean b() {
            String[] strArr = f4699a;
            int length = strArr.length;
            int i = 0;
            while (i < length) {
                String str = strArr[i];
                try {
                    StringBuilder sb = new StringBuilder();
                    sb.append(str);
                    sb.append("su");
                    if (new File(sb.toString()).exists()) {
                        return true;
                    }
                    i++;
                } catch (Throwable unused) {
                }
            }
            return false;
        }

        public static int c() {
            return (a() || b()) ? 1 : 0;
        }
    }

    @NonNull
    public static com.yandex.metrica.b a(@NonNull Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        Point b2 = b(context);
        int i = b2.x;
        int i2 = b2.y;
        float f = displayMetrics.density;
        float f2 = (float) i;
        float f3 = (float) i2;
        float min = Math.min(f2 / f, f3 / f);
        float f4 = f * 160.0f;
        float f5 = f2 / f4;
        float f6 = f3 / f4;
        double sqrt = Math.sqrt((double) ((f5 * f5) + (f6 * f6)));
        if (a(context, sqrt)) {
            return com.yandex.metrica.b.TV;
        }
        if (sqrt >= 7.0d || min >= 600.0f) {
            return com.yandex.metrica.b.TABLET;
        }
        return com.yandex.metrica.b.PHONE;
    }

    private static boolean a(Context context, double d2) {
        return d2 >= 15.0d && !f4697a.b(context, "android.hardware.touchscreen");
    }

    @TargetApi(17)
    @NonNull
    public static Point b(Context context) {
        return (Point) cx.a(new wo<Display, Point>() {
            public Point a(Display display) throws Throwable {
                int i;
                int i2;
                if (VERSION.SDK_INT >= 17) {
                    DisplayMetrics displayMetrics = new DisplayMetrics();
                    display.getRealMetrics(displayMetrics);
                    int i3 = displayMetrics.widthPixels;
                    int i4 = displayMetrics.heightPixels;
                    i2 = i3;
                    i = i4;
                } else if (VERSION.SDK_INT >= 14) {
                    try {
                        Method method = Display.class.getMethod("getRawHeight", new Class[0]);
                        int intValue = ((Integer) Display.class.getMethod("getRawWidth", new Class[0]).invoke(display, new Object[0])).intValue();
                        i = ((Integer) method.invoke(display, new Object[0])).intValue();
                        i2 = intValue;
                    } catch (Throwable unused) {
                        i2 = display.getWidth();
                        i = display.getHeight();
                    }
                } else {
                    i2 = display.getWidth();
                    i = display.getHeight();
                }
                return new Point(i2, i);
            }
        }, (Display) cx.a((wo<T, S>) new wo<WindowManager, Display>() {
            public Display a(WindowManager windowManager) throws Throwable {
                return windowManager.getDefaultDisplay();
            }
        }, (WindowManager) context.getSystemService("window"), "getting display", "WindowManager"), "getting display metrics", "Display", new Point(0, 0));
    }

    public static Integer c(Context context) {
        Intent registerReceiver = context.registerReceiver(null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
        if (registerReceiver == null) {
            return null;
        }
        int intExtra = registerReceiver.getIntExtra("level", -1);
        int intExtra2 = registerReceiver.getIntExtra("scale", -1);
        if (intExtra <= -1 || intExtra2 <= 0) {
            return null;
        }
        return Integer.valueOf(Math.round((((float) intExtra) / ((float) intExtra2)) * 100.0f));
    }

    @NonNull
    public static String a(@NonNull Locale locale) {
        String language = locale.getLanguage();
        String country = locale.getCountry();
        StringBuilder sb = new StringBuilder(language);
        if (cx.a(21)) {
            String script = locale.getScript();
            if (!TextUtils.isEmpty(script)) {
                sb.append('-');
                sb.append(script);
            }
        }
        if (!TextUtils.isEmpty(country)) {
            sb.append('_');
            sb.append(country);
        }
        return sb.toString();
    }

    @NonNull
    public static a d(@NonNull Context context) {
        return (a) cx.a(new wo<ConnectivityManager, a>() {
            public a a(ConnectivityManager connectivityManager) throws Throwable {
                if (cx.a(23)) {
                    return bt.c(connectivityManager);
                }
                return bt.d(connectivityManager);
            }
        }, (ConnectivityManager) context.getSystemService("connectivity"), "getting connection type", "ConnectivityManager", a.UNDEFINED);
    }

    /* access modifiers changed from: private */
    @TargetApi(23)
    @NonNull
    public static a c(@NonNull ConnectivityManager connectivityManager) {
        a aVar = a.UNDEFINED;
        Network activeNetwork = connectivityManager.getActiveNetwork();
        if (activeNetwork == null) {
            return a.OFFLINE;
        }
        NetworkInfo networkInfo = connectivityManager.getNetworkInfo(activeNetwork);
        if (networkInfo != null && !networkInfo.isConnected()) {
            return a.OFFLINE;
        }
        NetworkCapabilities networkCapabilities = connectivityManager.getNetworkCapabilities(activeNetwork);
        if (networkCapabilities == null) {
            return aVar;
        }
        for (Integer num : c.a()) {
            if (networkCapabilities.hasTransport(num.intValue())) {
                return (a) c.a(num);
            }
        }
        return aVar;
    }

    /* access modifiers changed from: private */
    @NonNull
    public static a d(@NonNull ConnectivityManager connectivityManager) {
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        if (activeNetworkInfo == null || !activeNetworkInfo.isConnected()) {
            return a.OFFLINE;
        }
        return (a) b.a(Integer.valueOf(activeNetworkInfo.getType()));
    }

    public static int e(@NonNull Context context) {
        return a(d(context));
    }

    @VisibleForTesting
    static int a(@Nullable a aVar) {
        return ((Integer) d.a(aVar)).intValue();
    }
}
