package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import java.util.concurrent.TimeUnit;

public class wk {
    @Nullable
    public static <T> T a(@Nullable T t, @Nullable T t2) {
        return t == null ? t2 : t;
    }

    @NonNull
    private static <T> T b(@Nullable T t, @NonNull T t2) {
        return t == null ? t2 : t;
    }

    @NonNull
    public static String a(@Nullable String str, @NonNull String str2) {
        return TextUtils.isEmpty(str) ? str2 : str;
    }

    @NonNull
    public static String b(@Nullable String str, @NonNull String str2) {
        return (String) b((T) str, (T) str2);
    }

    public static boolean a(@Nullable Boolean bool, boolean z) {
        return ((Boolean) b((T) bool, (T) Boolean.valueOf(z))).booleanValue();
    }

    public static long a(@Nullable Long l, long j) {
        return ((Long) b((T) l, (T) Long.valueOf(j))).longValue();
    }

    public static int a(@Nullable Integer num, int i) {
        return ((Integer) b((T) num, (T) Integer.valueOf(i))).intValue();
    }

    public static float a(@Nullable Float f, float f2) {
        return ((Float) b((T) f, (T) Float.valueOf(f2))).floatValue();
    }

    public static long a(@Nullable Long l, @NonNull TimeUnit timeUnit, long j) {
        return l == null ? j : timeUnit.toMillis(l.longValue());
    }
}
