package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import java.lang.Thread.UncaughtExceptionHandler;
import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

class bh implements UncaughtExceptionHandler {

    /* renamed from: a reason: collision with root package name */
    public static final AtomicBoolean f4686a = new AtomicBoolean();
    private final CopyOnWriteArrayList<ki> b;
    private final UncaughtExceptionHandler c;
    private final dq d;
    @NonNull
    private final ag e;

    public bh(UncaughtExceptionHandler uncaughtExceptionHandler, @NonNull Context context) {
        this(uncaughtExceptionHandler, new ag(context));
    }

    @VisibleForTesting
    bh(UncaughtExceptionHandler uncaughtExceptionHandler, @NonNull ag agVar) {
        this.d = new dq();
        this.b = new CopyOnWriteArrayList<>();
        this.c = uncaughtExceptionHandler;
        this.e = agVar;
    }

    public void uncaughtException(Thread thread, Throwable th) {
        try {
            f4686a.set(true);
            a(new kl(th, new kg(new Cdo().a(thread), this.d.a(thread)), this.e.a(), this.e.b()));
        } finally {
            if (this.c != null) {
                this.c.uncaughtException(thread, th);
            }
        }
    }

    /* access modifiers changed from: 0000 */
    @VisibleForTesting
    public void a(@NonNull kl klVar) {
        Iterator it = this.b.iterator();
        while (it.hasNext()) {
            ((ki) it.next()).a(klVar);
        }
    }

    public void a(ki kiVar) {
        this.b.add(kiVar);
    }
}
