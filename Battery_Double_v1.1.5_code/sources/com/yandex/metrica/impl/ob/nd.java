package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import com.yandex.metrica.impl.ob.rq.a;
import com.yandex.metrica.impl.ob.rq.a.C0120a;
import java.util.ArrayList;
import java.util.List;

public class nd implements mq<te, a> {
    @NonNull
    /* renamed from: a */
    public a b(@NonNull te teVar) {
        a aVar = new a();
        aVar.b = new C0120a[teVar.f5187a.size()];
        for (int i = 0; i < teVar.f5187a.size(); i++) {
            aVar.b[i] = a((th) teVar.f5187a.get(i));
        }
        aVar.c = teVar.b;
        aVar.d = teVar.c;
        aVar.e = teVar.d;
        aVar.f = teVar.e;
        return aVar;
    }

    @NonNull
    public te a(@NonNull a aVar) {
        ArrayList arrayList = new ArrayList(aVar.b.length);
        for (C0120a a2 : aVar.b) {
            arrayList.add(a(a2));
        }
        te teVar = new te(arrayList, aVar.c, aVar.d, aVar.e, aVar.f);
        return teVar;
    }

    @NonNull
    private C0120a a(@NonNull th thVar) {
        C0120a aVar = new C0120a();
        aVar.b = thVar.f5189a;
        List<String> list = thVar.b;
        aVar.c = new String[list.size()];
        int i = 0;
        for (String str : list) {
            aVar.c[i] = str;
            i++;
        }
        return aVar;
    }

    @NonNull
    private th a(@NonNull C0120a aVar) {
        ArrayList arrayList = new ArrayList();
        if (aVar.c != null && aVar.c.length > 0) {
            arrayList = new ArrayList(aVar.c.length);
            for (String add : aVar.c) {
                arrayList.add(add);
            }
        }
        return new th(cu.a(aVar.b), arrayList);
    }
}
