package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.util.Map;

public class ym implements yk<String> {

    /* renamed from: a reason: collision with root package name */
    private final Map<String, ?> f5319a;

    public ym(@NonNull Map<String, ?> map) {
        this.f5319a = map;
    }

    public yi a(@Nullable String str) {
        if (!this.f5319a.containsKey(str)) {
            return yi.a(this);
        }
        return yi.a(this, String.format("Failed to activate AppMetrica with provided apiKey ApiKey %s has already been used by another reporter.", new Object[]{str}));
    }
}
