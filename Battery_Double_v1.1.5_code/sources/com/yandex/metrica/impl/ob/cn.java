package com.yandex.metrica.impl.ob;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.net.Uri.Builder;
import android.os.Bundle;
import android.os.Process;
import com.yandex.metrica.IMetricaService;

public final class cn {

    /* renamed from: a reason: collision with root package name */
    private static final yc f4743a = new yc();

    private static Uri c(Context context) {
        return new Builder().scheme("metrica").authority(context.getPackageName()).build();
    }

    public static Intent a(Context context) {
        Intent intent = new Intent(IMetricaService.class.getName(), c(context));
        a(intent);
        return intent;
    }

    public static Intent b(Context context) {
        Intent putExtras = a(context).putExtras(d(context));
        putExtras.setData(putExtras.getData().buildUpon().path("client").appendQueryParameter("pid", String.valueOf(Process.myPid())).appendQueryParameter("psid", ee.f4808a).build());
        return putExtras.setPackage(context.getApplicationContext().getPackageName());
    }

    @SuppressLint({"NewApi"})
    public static void a(Intent intent) {
        if (cx.b(11)) {
            intent.addFlags(32);
        }
    }

    private static Bundle d(Context context) {
        try {
            Bundle bundle = f4743a.b(context, context.getPackageName(), 128).metaData;
            if (bundle == null) {
                bundle = new Bundle();
            }
            return bundle;
        } catch (Throwable unused) {
            return new Bundle();
        }
    }
}
