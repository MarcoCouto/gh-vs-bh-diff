package com.yandex.metrica.impl.ob;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.PendingIntent;
import android.bluetooth.le.BluetoothLeScanner;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import com.yandex.metrica.ConfigurationServiceReceiver;

@TargetApi(26)
public class dg implements dh {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final dd f4782a;
    /* access modifiers changed from: private */
    @NonNull
    public final dk b;
    /* access modifiers changed from: private */
    @NonNull
    public final di c;
    /* access modifiers changed from: private */
    @NonNull
    public final PendingIntent d;

    public dg(@NonNull Context context) {
        this(new dd(context), new dk(), new di(), PendingIntent.getBroadcast(context.getApplicationContext(), 7695436, new Intent("com.yandex.metrica.configuration.service.PLC").setClass(context, ConfigurationServiceReceiver.class), 134217728));
    }

    @SuppressLint({"MissingPermission"})
    public synchronized void a(@NonNull final tt ttVar) {
        BluetoothLeScanner a2 = this.f4782a.a();
        if (a2 != null) {
            a();
            Integer num = (Integer) cx.a((wo<T, S>) new wo<BluetoothLeScanner, Integer>() {
                public Integer a(BluetoothLeScanner bluetoothLeScanner) throws Exception {
                    return Integer.valueOf(bluetoothLeScanner.startScan(dg.this.c.a(ttVar.b), dg.this.b.a(ttVar.f5209a), dg.this.d));
                }
            }, a2, "startScan", "BluetoothLeScanner");
        }
    }

    @SuppressLint({"MissingPermission"})
    public synchronized void a() {
        BluetoothLeScanner a2 = this.f4782a.a();
        if (a2 != null) {
            cx.a((wn<T>) new wn<BluetoothLeScanner>() {
                public void a(BluetoothLeScanner bluetoothLeScanner) {
                    bluetoothLeScanner.stopScan(dg.this.d);
                }
            }, a2, "stopScan", "BluetoothLeScanner");
        }
    }

    @VisibleForTesting
    public dg(@NonNull dd ddVar, @NonNull dk dkVar, @NonNull di diVar, @NonNull PendingIntent pendingIntent) {
        this.f4782a = ddVar;
        this.b = dkVar;
        this.c = diVar;
        this.d = pendingIntent;
    }
}
