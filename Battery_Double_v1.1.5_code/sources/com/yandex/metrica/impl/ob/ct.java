package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public class ct extends cf<String> {
    public ct(@NonNull Context context, @NonNull String str) {
        super(context, str, "string");
    }

    /* access modifiers changed from: protected */
    @Nullable
    /* renamed from: a */
    public String b(int i) {
        return this.f4723a.getString(i);
    }
}
