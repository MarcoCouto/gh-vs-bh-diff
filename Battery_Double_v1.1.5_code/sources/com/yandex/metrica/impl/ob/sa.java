package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.WorkerThread;
import com.yandex.metrica.j;

public class sa {
    @WorkerThread
    public db a(@NonNull Context context) {
        return db.a(context);
    }

    @Nullable
    @WorkerThread
    public db a() {
        return db.e();
    }

    public void a(@NonNull Context context, @NonNull j jVar) {
        db.a(context, jVar);
    }

    public void b(@NonNull Context context, @NonNull j jVar) {
        db.b(context, jVar);
    }

    public void b() {
        db.b();
    }

    public boolean c() {
        return db.c();
    }

    public boolean d() {
        return db.e() != null;
    }

    public ax e() {
        return db.f();
    }

    public void a(@Nullable Location location) {
        db.a(location);
    }

    public void a(boolean z) {
        db.a(z);
    }

    public void a(@NonNull Context context, boolean z) {
        db.a(context).b(z);
    }

    public void b(@NonNull Context context, boolean z) {
        db.a(context).c(z);
    }

    public db f() {
        return db.d();
    }
}
