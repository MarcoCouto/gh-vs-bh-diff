package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ub {

    /* renamed from: a reason: collision with root package name */
    public final long f5227a;
    public final String b;
    public final List<Integer> c;
    public final long d;
    public final int e;

    public ub(long j, @NonNull String str, @NonNull List<Integer> list, long j2, int i) {
        this.f5227a = j;
        this.b = str;
        this.c = Collections.unmodifiableList(list);
        this.d = j2;
        this.e = i;
    }

    @Nullable
    public static ub a(@Nullable String str) {
        try {
            if (TextUtils.isEmpty(str)) {
                return null;
            }
            JSONObject jSONObject = new JSONObject(str);
            ub ubVar = new ub(jSONObject.getLong("seconds_to_live"), jSONObject.getString("token"), a(jSONObject.getJSONArray("ports")), jSONObject.getLong("first_delay_seconds"), jSONObject.getInt("launch_delay_seconds"));
            return ubVar;
        } catch (Throwable unused) {
            return null;
        }
    }

    private static ArrayList<Integer> a(JSONArray jSONArray) throws JSONException {
        ArrayList<Integer> arrayList = new ArrayList<>(jSONArray.length());
        for (int i = 0; i < jSONArray.length(); i++) {
            arrayList.add(Integer.valueOf(jSONArray.getInt(i)));
        }
        return arrayList;
    }

    public boolean equals(Object obj) {
        boolean z = true;
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        ub ubVar = (ub) obj;
        if (this.f5227a != ubVar.f5227a || this.d != ubVar.d || this.e != ubVar.e) {
            return false;
        }
        if (this.b == null ? ubVar.b != null : !this.b.equals(ubVar.b)) {
            return false;
        }
        if (this.c != null) {
            z = this.c.equals(ubVar.c);
        } else if (ubVar.c != null) {
            z = false;
        }
        return z;
    }

    public int hashCode() {
        int i = 0;
        int hashCode = ((((int) (this.f5227a ^ (this.f5227a >>> 32))) * 31) + (this.b != null ? this.b.hashCode() : 0)) * 31;
        if (this.c != null) {
            i = this.c.hashCode();
        }
        return ((((hashCode + i) * 31) + ((int) (this.d ^ (this.d >>> 32)))) * 31) + this.e;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("SocketConfig{secondsToLive=");
        sb.append(this.f5227a);
        sb.append(", token='");
        sb.append(this.b);
        sb.append('\'');
        sb.append(", ports=");
        sb.append(this.c);
        sb.append(", firstDelaySeconds=");
        sb.append(this.d);
        sb.append(", launchDelaySeconds=");
        sb.append(this.e);
        sb.append('}');
        return sb.toString();
    }
}
