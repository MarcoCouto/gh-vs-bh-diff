package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.yandex.metrica.impl.ob.rh.b.C0111b;

public class pc {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final og f5054a;
    @NonNull
    private final pb b;

    public pc(@NonNull og ogVar, @NonNull pb pbVar) {
        this.f5054a = ogVar;
        this.b = pbVar;
    }

    @Nullable
    public C0111b a(long j, @Nullable String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            os a2 = this.f5054a.a(j, str);
            if (a2 != null) {
                return this.b.a(a2);
            }
            return null;
        } catch (Exception unused) {
            return null;
        }
    }
}
