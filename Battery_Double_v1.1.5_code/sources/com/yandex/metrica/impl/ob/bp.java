package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import com.yandex.metrica.impl.ob.sy;
import java.io.IOException;

public abstract class bp<T extends sy> extends bs<T> {
    @NonNull
    private final xb j;
    @NonNull
    private final vm k;
    @NonNull
    private final wh l;

    /* access modifiers changed from: protected */
    public abstract void E();

    /* access modifiers changed from: protected */
    public abstract void F();

    public bp(@NonNull T t) {
        this(new ab(), new wu(), new vm(), new wg(), t);
    }

    public bp(@NonNull bn bnVar, @NonNull xb xbVar, @NonNull vm vmVar, @NonNull wh whVar, @NonNull T t) {
        super(bnVar, t);
        this.j = xbVar;
        this.k = vmVar;
        this.l = whVar;
        t.a(this.j);
    }

    public void d() {
        super.d();
        a(this.l.a());
    }

    public boolean b() {
        boolean b = super.b();
        if (b) {
            E();
        } else if (p()) {
            F();
        }
        return b;
    }

    public boolean c(@NonNull byte[] bArr) {
        boolean z = false;
        try {
            byte[] a2 = this.k.a(bArr);
            if (a2 != null) {
                byte[] a3 = this.j.a(a2);
                if (a3 != null) {
                    a(a3);
                    z = true;
                }
            }
            return z;
        } catch (IOException unused) {
            return false;
        }
    }

    public void a(byte[] bArr) {
        super.a(bArr);
    }

    /* access modifiers changed from: protected */
    public boolean p() {
        return k() == 400;
    }
}
