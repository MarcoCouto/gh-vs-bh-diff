package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import com.yandex.metrica.impl.ob.fp;

public interface fq<C extends fp> {
    @NonNull
    C a(@NonNull Context context, @NonNull fu fuVar, @NonNull fn fnVar, @NonNull eg egVar);
}
