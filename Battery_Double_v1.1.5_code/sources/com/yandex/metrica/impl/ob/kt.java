package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import com.yandex.metrica.impl.ob.rg.c;

public class kt implements mq<kl, c> {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final ks f4948a;
    @NonNull
    private final km b;
    @NonNull
    private final ko c;

    public kt() {
        this(new ks(), new km(new kr()), new ko());
    }

    @VisibleForTesting
    kt(@NonNull ks ksVar, @NonNull km kmVar, @NonNull ko koVar) {
        this.b = kmVar;
        this.f4948a = ksVar;
        this.c = koVar;
    }

    @NonNull
    /* renamed from: a */
    public c b(@NonNull kl klVar) {
        c cVar = new c();
        if (klVar.b != null) {
            cVar.b = this.f4948a.b(klVar.b);
        }
        if (klVar.c != null) {
            cVar.c = this.b.b(klVar.c);
        }
        if (klVar.d != null) {
            cVar.d = klVar.d;
        }
        cVar.e = this.c.a(klVar.e).intValue();
        return cVar;
    }

    @NonNull
    public kl a(@NonNull c cVar) {
        throw new UnsupportedOperationException();
    }
}
