package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class qa {

    /* renamed from: a reason: collision with root package name */
    private Context f5074a;

    public qa(Context context) {
        this.f5074a = context;
    }

    public void a() {
        SharedPreferences a2 = ql.a(this.f5074a, "_bidoptpreferences");
        if (a2.getAll().size() > 0) {
            b(a2);
            a(a2);
            a2.edit().clear().apply();
        }
    }

    private void a(SharedPreferences sharedPreferences) {
        Map all = sharedPreferences.getAll();
        if (all.size() > 0) {
            for (String str : a(all, qi.e.a())) {
                String string = sharedPreferences.getString(new qk(qi.e.a(), str).b(), null);
                qi qiVar = new qi(this.f5074a, str);
                if (!TextUtils.isEmpty(string) && TextUtils.isEmpty(qiVar.b(null))) {
                    qiVar.i(string).j();
                }
            }
        }
    }

    private List<String> a(Map<String, ?> map, String str) {
        ArrayList arrayList = new ArrayList();
        for (String str2 : map.keySet()) {
            if (str2.startsWith(str)) {
                arrayList.add(str2.replace(str, ""));
            }
        }
        return arrayList;
    }

    private void b(SharedPreferences sharedPreferences) {
        String string = sharedPreferences.getString(qi.d.a(), null);
        qi qiVar = new qi(this.f5074a);
        if (!TextUtils.isEmpty(string) && TextUtils.isEmpty(qiVar.a((String) null))) {
            qiVar.j(string).j();
            sharedPreferences.edit().remove(qi.d.a()).apply();
        }
    }

    public void b() {
        lf e = ld.a(this.f5074a).e();
        SharedPreferences a2 = ql.a(this.f5074a, "_startupserviceinfopreferences");
        b(e, a2);
        c(e, a2);
        a(e, this.f5074a.getPackageName());
        a(e, a2);
    }

    private void a(lf lfVar, SharedPreferences sharedPreferences) {
        for (String a2 : a(sharedPreferences.getAll(), qi.e.a())) {
            a(lfVar, a2);
        }
    }

    private void a(lf lfVar, String str) {
        lz lzVar = new lz(lfVar, str);
        qi qiVar = new qi(this.f5074a, str);
        String b = qiVar.b(null);
        if (!TextUtils.isEmpty(b)) {
            lzVar.a(b);
        }
        String a2 = qiVar.a();
        if (!TextUtils.isEmpty(a2)) {
            lzVar.h(a2);
        }
        String d = qiVar.d(null);
        if (!TextUtils.isEmpty(d)) {
            lzVar.g(d);
        }
        String f = qiVar.f(null);
        if (!TextUtils.isEmpty(f)) {
            lzVar.e(f);
        }
        String g = qiVar.g(null);
        if (!TextUtils.isEmpty(g)) {
            lzVar.d(g);
        }
        String c = qiVar.c(null);
        if (!TextUtils.isEmpty(c)) {
            lzVar.f(c);
        }
        long a3 = qiVar.a(-1);
        if (a3 != -1) {
            lzVar.a(a3);
        }
        String e = qiVar.e(null);
        if (!TextUtils.isEmpty(e)) {
            lzVar.c(e);
        }
        lzVar.q();
        qiVar.b();
    }

    private void b(lf lfVar, SharedPreferences sharedPreferences) {
        lz lzVar = new lz(lfVar, null);
        String string = sharedPreferences.getString(qi.d.a(), null);
        if (!TextUtils.isEmpty(string) && TextUtils.isEmpty(lzVar.a().b)) {
            lzVar.b(string).q();
            sharedPreferences.edit().remove(qi.d.a()).apply();
        }
    }

    private void c(lf lfVar, SharedPreferences sharedPreferences) {
        lz lzVar = new lz(lfVar, this.f5074a.getPackageName());
        boolean z = sharedPreferences.getBoolean(qi.f.a(), false);
        if (z) {
            lzVar.a(z).q();
        }
    }
}
