package com.yandex.metrica.impl.ob;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import com.yandex.metrica.impl.ob.ai.b;
import java.util.Collection;

public class dc {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final Context f4774a;
    @NonNull
    private final px b;
    @NonNull
    private final pr c;
    @NonNull
    private final ly d;
    @NonNull
    private final b e;

    public dc(@NonNull Context context) {
        this(context, new px());
    }

    private dc(@NonNull Context context, @NonNull px pxVar) {
        this(context, pxVar, new pr(pxVar.a()), new ly(ld.a(context).c()), new b());
    }

    public boolean a(@NonNull uk ukVar, @NonNull tt ttVar) {
        if (!this.e.a(ukVar.E, ukVar.D, ttVar.d)) {
            return false;
        }
        a(ukVar);
        if (this.c.c(this.f4774a) && this.c.i(this.f4774a)) {
            return true;
        }
        return false;
    }

    public boolean b(@NonNull uk ukVar, @NonNull tt ttVar) {
        a(ukVar);
        return ukVar.o.f && !cx.a((Collection) ttVar.b);
    }

    private void a(@NonNull uk ukVar) {
        this.b.a(this.d.g());
        this.b.a(ukVar);
        this.c.a(this.b.a());
    }

    @VisibleForTesting
    public dc(@NonNull Context context, @NonNull px pxVar, @NonNull pr prVar, @NonNull ly lyVar, @NonNull b bVar) {
        this.f4774a = context;
        this.b = pxVar;
        this.c = prVar;
        this.d = lyVar;
        this.e = bVar;
    }
}
