package com.yandex.metrica.impl.ob;

import android.content.Context;

@Deprecated
public class qh extends qd {
    private static final qk d = new qk("PREF_KEY_OFFSET");
    private qk e = new qk(d.a(), null);

    /* access modifiers changed from: protected */
    public String f() {
        return "_servertimeoffset";
    }

    public qh(Context context, String str) {
        super(context, str);
    }

    public long a(int i) {
        return this.c.getLong(this.e.b(), (long) i);
    }

    public void a() {
        h(this.e.b()).j();
    }
}
