package com.yandex.metrica.impl.ob;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public enum ue {
    UNKNOWN(0),
    NETWORK(1),
    PARSE(2);
    
    private int d;

    private ue(int i) {
        this.d = i;
    }

    public int a() {
        return this.d;
    }

    public Bundle a(Bundle bundle) {
        bundle.putInt("startup_error_key_code", a());
        return bundle;
    }

    @Nullable
    public static ue b(Bundle bundle) {
        if (bundle.containsKey("startup_error_key_code")) {
            return a(bundle.getInt("startup_error_key_code"));
        }
        return null;
    }

    @NonNull
    private static ue a(int i) {
        ue ueVar = UNKNOWN;
        switch (i) {
            case 1:
                return NETWORK;
            case 2:
                return PARSE;
            default:
                return ueVar;
        }
    }
}
