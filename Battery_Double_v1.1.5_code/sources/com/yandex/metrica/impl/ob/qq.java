package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.yandex.metrica.impl.ob.rl.a.C0115a;
import com.yandex.metrica.impl.ob.rl.a.b;

public class qq extends qo {
    public qq(@NonNull qn qnVar) {
        super(qnVar);
    }

    public C0115a a(@NonNull re reVar, @Nullable C0115a aVar, @NonNull qm qmVar) {
        if (a(aVar)) {
            return a().a(reVar, qmVar.a());
        }
        aVar.d = new b();
        return aVar;
    }
}
