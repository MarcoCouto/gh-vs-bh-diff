package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.util.List;

public class pn {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    public final List<pu> f5063a;
    @Nullable
    public final l b;
    @NonNull
    public final List<String> c;

    public pn(@NonNull List<pu> list, @Nullable l lVar, @NonNull List<String> list2) {
        this.f5063a = list;
        this.b = lVar;
        this.c = list2;
    }
}
