package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import com.yandex.metrica.impl.ob.lq.b;
import com.yandex.metrica.impl.ob.lq.d;
import com.yandex.metrica.impl.ob.lq.f;
import com.yandex.metrica.impl.ob.lq.g;
import java.util.HashMap;
import java.util.List;

public class le {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final HashMap<String, List<String>> f4964a = new HashMap<>();

    public le() {
        this.f4964a.put("reports", f.f4979a);
        this.f4964a.put("sessions", g.f4980a);
        this.f4964a.put("preferences", d.f4978a);
        this.f4964a.put("binary_data", b.f4977a);
    }

    @NonNull
    public HashMap<String, List<String>> a() {
        return this.f4964a;
    }
}
