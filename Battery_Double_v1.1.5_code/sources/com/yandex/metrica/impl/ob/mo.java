package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import com.yandex.metrica.impl.ob.l.a;
import com.yandex.metrica.impl.ob.rm.a.C0116a;

public class mo implements mq<l, C0116a> {
    @NonNull
    /* renamed from: a */
    public C0116a b(@NonNull l lVar) {
        C0116a aVar = new C0116a();
        if (lVar.f4958a != null) {
            switch (lVar.f4958a) {
                case ACTIVE:
                    aVar.b = 1;
                    break;
                case WORKING_SET:
                    aVar.b = 2;
                    break;
                case FREQUENT:
                    aVar.b = 3;
                    break;
                case RARE:
                    aVar.b = 4;
                    break;
            }
        }
        if (lVar.b != null) {
            if (lVar.b.booleanValue()) {
                aVar.c = 1;
            } else {
                aVar.c = 0;
            }
        }
        return aVar;
    }

    @NonNull
    public l a(@NonNull C0116a aVar) {
        a aVar2;
        Boolean bool = null;
        switch (aVar.b) {
            case 1:
                aVar2 = a.ACTIVE;
                break;
            case 2:
                aVar2 = a.WORKING_SET;
                break;
            case 3:
                aVar2 = a.FREQUENT;
                break;
            case 4:
                aVar2 = a.RARE;
                break;
            default:
                aVar2 = null;
                break;
        }
        switch (aVar.c) {
            case 0:
                bool = Boolean.valueOf(false);
                break;
            case 1:
                bool = Boolean.valueOf(true);
                break;
        }
        return new l(aVar2, bool);
    }
}
