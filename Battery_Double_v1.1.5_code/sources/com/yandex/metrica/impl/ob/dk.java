package com.yandex.metrica.impl.ob;

import android.annotation.TargetApi;
import android.bluetooth.le.ScanSettings;
import android.bluetooth.le.ScanSettings.Builder;
import android.support.annotation.NonNull;
import com.yandex.metrica.impl.ob.tt.b;
import com.yandex.metrica.impl.ob.tt.b.C0131b;
import com.yandex.metrica.impl.ob.tt.b.a;
import com.yandex.metrica.impl.ob.tt.b.c;
import com.yandex.metrica.impl.ob.tt.b.d;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@TargetApi(21)
public class dk {

    /* renamed from: a reason: collision with root package name */
    private static final Map<d, Integer> f4786a;

    static {
        HashMap hashMap = new HashMap();
        hashMap.put(d.LOW_POWER, Integer.valueOf(0));
        hashMap.put(d.BALANCED, Integer.valueOf(1));
        hashMap.put(d.LOW_LATENCY, Integer.valueOf(2));
        f4786a = Collections.unmodifiableMap(hashMap);
    }

    @NonNull
    public ScanSettings a(@NonNull b bVar) {
        Builder builder = new Builder();
        builder.setScanMode(a(bVar.d));
        builder.setReportDelay(bVar.e);
        if (cx.a(23)) {
            builder.setCallbackType(a(bVar.f5214a));
            builder.setMatchMode(a(bVar.b));
            builder.setNumOfMatches(a(bVar.c));
        }
        return builder.build();
    }

    private int a(@NonNull d dVar) {
        Integer num = (Integer) f4786a.get(dVar);
        if (num == null) {
            return 2;
        }
        return num.intValue();
    }

    @TargetApi(23)
    private int a(@NonNull a aVar) {
        switch (aVar) {
            case MATCH_LOST:
                return 4;
            case FIRST_MATCH:
                return 2;
            default:
                return 1;
        }
    }

    @TargetApi(23)
    private int a(@NonNull C0131b bVar) {
        return C0131b.AGGRESSIVE.equals(bVar) ? 1 : 2;
    }

    @TargetApi(23)
    private int a(@NonNull c cVar) {
        switch (cVar) {
            case ONE_AD:
                return 1;
            case FEW_AD:
                return 2;
            default:
                return 3;
        }
    }
}
