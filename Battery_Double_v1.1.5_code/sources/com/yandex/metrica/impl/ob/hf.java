package com.yandex.metrica.impl.ob;

import android.content.pm.FeatureInfo;
import android.content.pm.PackageInfo;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import com.yandex.metrica.impl.ob.oa.a;
import io.fabric.sdk.android.services.settings.SettingsJsonConstants;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONObject;

public class hf extends hd {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final yc f4871a;

    public hf(en enVar) {
        this(enVar, new yc());
    }

    @VisibleForTesting
    public hf(en enVar, @NonNull yc ycVar) {
        super(enVar);
        this.f4871a = ycVar;
    }

    public boolean a(@NonNull w wVar) {
        en a2 = a();
        if (a2.u().d() && a2.t()) {
            lw y = a2.y();
            HashSet b = b();
            try {
                ArrayList c = c();
                if (vj.a(b, c)) {
                    a2.m();
                } else {
                    JSONArray jSONArray = new JSONArray();
                    Iterator it = c.iterator();
                    while (it.hasNext()) {
                        jSONArray.put(((ob) it.next()).a());
                    }
                    a2.e().d(w.a(wVar, new JSONObject().put(SettingsJsonConstants.FEATURES_KEY, jSONArray).toString()));
                    y.b(jSONArray.toString());
                }
            } catch (Throwable unused) {
            }
        }
        return false;
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    @VisibleForTesting
    public HashSet<ob> b() {
        String e = a().y().e();
        if (TextUtils.isEmpty(e)) {
            return null;
        }
        try {
            HashSet<ob> hashSet = new HashSet<>();
            JSONArray jSONArray = new JSONArray(e);
            for (int i = 0; i < jSONArray.length(); i++) {
                hashSet.add(new ob(jSONArray.getJSONObject(i)));
            }
            return hashSet;
        } catch (Throwable unused) {
            return null;
        }
    }

    /* access modifiers changed from: 0000 */
    @Nullable
    @VisibleForTesting
    public ArrayList<ob> c() {
        try {
            en a2 = a();
            PackageInfo a3 = this.f4871a.a(a2.k(), a2.k().getPackageName(), 16384);
            ArrayList<ob> arrayList = new ArrayList<>();
            oa a4 = a.a();
            if (!(a3 == null || a3.reqFeatures == null)) {
                for (FeatureInfo b : a3.reqFeatures) {
                    arrayList.add(a4.b(b));
                }
            }
            return arrayList;
        } catch (Throwable unused) {
            return null;
        }
    }
}
