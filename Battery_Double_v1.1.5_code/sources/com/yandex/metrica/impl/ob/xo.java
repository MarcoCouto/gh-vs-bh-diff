package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import java.util.concurrent.Executor;

public class xo {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private final xn f5305a;
    @Nullable
    private volatile xh b;
    @Nullable
    private volatile Executor c;
    @Nullable
    private volatile xh d;
    @Nullable
    private volatile xh e;
    @Nullable
    private volatile xi f;
    @Nullable
    private volatile xh g;
    @Nullable
    private volatile xh h;
    @Nullable
    private volatile xh i;
    @Nullable
    private volatile xh j;

    public xo() {
        this(new xn());
    }

    @NonNull
    public xh a() {
        if (this.b == null) {
            synchronized (this) {
                if (this.b == null) {
                    this.b = this.f5305a.a();
                }
            }
        }
        return this.b;
    }

    @NonNull
    public Executor b() {
        if (this.c == null) {
            synchronized (this) {
                if (this.c == null) {
                    this.c = this.f5305a.b();
                }
            }
        }
        return this.c;
    }

    @NonNull
    public xh c() {
        if (this.d == null) {
            synchronized (this) {
                if (this.d == null) {
                    this.d = this.f5305a.c();
                }
            }
        }
        return this.d;
    }

    @NonNull
    public xh d() {
        if (this.e == null) {
            synchronized (this) {
                if (this.e == null) {
                    this.e = this.f5305a.d();
                }
            }
        }
        return this.e;
    }

    @NonNull
    public xi e() {
        if (this.f == null) {
            synchronized (this) {
                if (this.f == null) {
                    this.f = this.f5305a.e();
                }
            }
        }
        return this.f;
    }

    @NonNull
    public xh f() {
        if (this.g == null) {
            synchronized (this) {
                if (this.g == null) {
                    this.g = this.f5305a.f();
                }
            }
        }
        return this.g;
    }

    @NonNull
    public xh g() {
        if (this.h == null) {
            synchronized (this) {
                if (this.h == null) {
                    this.h = this.f5305a.g();
                }
            }
        }
        return this.h;
    }

    @NonNull
    public xh h() {
        if (this.i == null) {
            synchronized (this) {
                if (this.i == null) {
                    this.i = this.f5305a.h();
                }
            }
        }
        return this.i;
    }

    @NonNull
    public xh i() {
        if (this.j == null) {
            synchronized (this) {
                if (this.j == null) {
                    this.j = this.f5305a.i();
                }
            }
        }
        return this.j;
    }

    @NonNull
    public xl a(@NonNull Runnable runnable) {
        return this.f5305a.a(runnable);
    }

    @VisibleForTesting
    xo(@NonNull xn xnVar) {
        this.f5305a = xnVar;
    }
}
