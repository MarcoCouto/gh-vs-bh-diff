package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import com.ironsource.sdk.constants.LocationConst;
import com.mansoon.BatteryDouble.Config;
import com.tapjoy.TapjoyConstants;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class vq {

    public static class a extends JSONObject {
        public a() {
        }

        public a(String str) throws JSONException {
            super(str);
        }

        public String a(String str) {
            String str2 = "";
            if (super.has(str)) {
                try {
                    return super.getString(str);
                } catch (Throwable unused) {
                }
            }
            return str2;
        }

        public String b(String str) {
            return super.has(str) ? a(str) : "";
        }

        public Object a(String str, Object obj) {
            try {
                return super.get(str);
            } catch (Throwable unused) {
                return obj;
            }
        }

        public boolean c(String str) {
            boolean z = false;
            try {
                if (NULL != super.get(str)) {
                    z = true;
                }
                return z;
            } catch (Throwable unused) {
                return false;
            }
        }

        @Nullable
        public Long d(String str) {
            try {
                return Long.valueOf(getLong(str));
            } catch (Throwable unused) {
                return null;
            }
        }

        @Nullable
        public Boolean e(String str) {
            try {
                return Boolean.valueOf(getBoolean(str));
            } catch (Throwable unused) {
                return null;
            }
        }
    }

    @VisibleForTesting
    public static Object a(Object obj) {
        if (obj == null) {
            return null;
        }
        try {
            if (obj.getClass().isArray()) {
                int length = Array.getLength(obj);
                ArrayList arrayList = new ArrayList(length);
                for (int i = 0; i < length; i++) {
                    arrayList.add(a(Array.get(obj, i)));
                }
                return new JSONArray(arrayList);
            } else if (obj instanceof Collection) {
                Collection<Object> collection = (Collection) obj;
                ArrayList arrayList2 = new ArrayList(collection.size());
                for (Object a2 : collection) {
                    arrayList2.add(a(a2));
                }
                return new JSONArray(arrayList2);
            } else if (!(obj instanceof Map)) {
                return obj;
            } else {
                Map map = (Map) obj;
                LinkedHashMap linkedHashMap = new LinkedHashMap();
                for (Entry entry : map.entrySet()) {
                    String obj2 = entry.getKey().toString();
                    if (obj2 != null) {
                        linkedHashMap.put(obj2, a(entry.getValue()));
                    }
                }
                return new JSONObject(linkedHashMap);
            }
        } catch (Throwable unused) {
            return null;
        }
    }

    @Nullable
    public static String a(@Nullable Map<String, String> map) {
        if (map == null) {
            return null;
        }
        if (map.isEmpty()) {
            return "";
        }
        return b((Map) map);
    }

    @Nullable
    public static String b(@Nullable Map map) {
        String str;
        if (cx.a(map)) {
            return null;
        }
        if (cx.a(19)) {
            str = new JSONObject(map).toString();
        } else {
            str = a((Object) map).toString();
        }
        return str;
    }

    @Nullable
    public static String a(List<String> list) {
        String str;
        if (cx.a((Collection) list)) {
            return null;
        }
        if (cx.a(19)) {
            str = new JSONArray(list).toString();
        } else {
            str = a((Object) list).toString();
        }
        return str;
    }

    @Nullable
    public static HashMap<String, String> a(String str) {
        if (!TextUtils.isEmpty(str)) {
            try {
                return a(new JSONObject(str));
            } catch (Throwable unused) {
            }
        }
        return null;
    }

    @NonNull
    public static HashMap<String, String> b(@NonNull String str) throws JSONException {
        return a(new JSONObject(str));
    }

    @Nullable
    public static List<String> c(@Nullable String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            JSONArray jSONArray = new JSONArray(str);
            ArrayList arrayList = new ArrayList(jSONArray.length());
            int i = 0;
            while (i < jSONArray.length()) {
                try {
                    arrayList.add(jSONArray.getString(i));
                    i++;
                } catch (Throwable unused) {
                }
            }
            return arrayList;
        } catch (Throwable unused2) {
            return null;
        }
    }

    @Nullable
    public static HashMap<String, String> a(JSONObject jSONObject) {
        if (JSONObject.NULL.equals(jSONObject)) {
            return null;
        }
        HashMap<String, String> hashMap = new HashMap<>();
        Iterator keys = jSONObject.keys();
        while (keys.hasNext()) {
            String str = (String) keys.next();
            String optString = jSONObject.optString(str);
            if (optString != null) {
                hashMap.put(str, optString);
            }
        }
        return hashMap;
    }

    @Nullable
    public static JSONObject a(@NonNull JSONObject jSONObject, @NonNull cz czVar) throws JSONException {
        jSONObject.put("lat", czVar.getLatitude());
        jSONObject.put("lon", czVar.getLongitude());
        jSONObject.putOpt("timestamp", Long.valueOf(czVar.getTime()));
        jSONObject.putOpt("precision", czVar.hasAccuracy() ? Float.valueOf(czVar.getAccuracy()) : null);
        jSONObject.putOpt("direction", czVar.hasBearing() ? Float.valueOf(czVar.getBearing()) : null);
        jSONObject.putOpt(LocationConst.SPEED, czVar.hasSpeed() ? Float.valueOf(czVar.getSpeed()) : null);
        jSONObject.putOpt(LocationConst.ALTITUDE, czVar.hasAltitude() ? Double.valueOf(czVar.getAltitude()) : null);
        jSONObject.putOpt("provider", cu.c(czVar.getProvider(), null));
        jSONObject.putOpt("original_provider", czVar.a());
        return jSONObject;
    }

    @Nullable
    public static Long a(JSONObject jSONObject, String str) {
        if (jSONObject != null && jSONObject.has(str)) {
            try {
                return Long.valueOf(jSONObject.getLong(str));
            } catch (Throwable unused) {
            }
        }
        return null;
    }

    @Nullable
    public static Integer b(JSONObject jSONObject, String str) {
        if (jSONObject != null && jSONObject.has(str)) {
            try {
                return Integer.valueOf(jSONObject.getInt(str));
            } catch (Throwable unused) {
            }
        }
        return null;
    }

    @Nullable
    public static Boolean c(JSONObject jSONObject, String str) {
        if (jSONObject != null && jSONObject.has(str)) {
            try {
                return Boolean.valueOf(jSONObject.getBoolean(str));
            } catch (Throwable unused) {
            }
        }
        return null;
    }

    public static boolean a(JSONObject jSONObject, String str, boolean z) {
        Boolean c = c(jSONObject, str);
        return c == null ? z : c.booleanValue();
    }

    @Nullable
    public static Float d(JSONObject jSONObject, String str) {
        if (jSONObject != null && jSONObject.has(str)) {
            try {
                return Float.valueOf((float) jSONObject.getDouble(str));
            } catch (Throwable unused) {
            }
        }
        return null;
    }

    @Nullable
    public static byte[] a(@NonNull JSONObject jSONObject, @NonNull String str, byte[] bArr) {
        String optString = jSONObject.optString(str, null);
        if (optString != null) {
            try {
                return cu.e(optString);
            } catch (Throwable unused) {
            }
        }
        return bArr;
    }

    public static List<String> a(JSONArray jSONArray) throws JSONException {
        if (jSONArray == null || jSONArray.length() <= 0) {
            return null;
        }
        ArrayList arrayList = new ArrayList(jSONArray.length());
        for (int i = 0; i < jSONArray.length(); i++) {
            arrayList.add(jSONArray.getString(i));
        }
        return arrayList;
    }

    public static JSONArray a(uu[] uuVarArr) {
        JSONArray jSONArray = new JSONArray();
        if (uuVarArr != null) {
            for (uu a2 : uuVarArr) {
                try {
                    jSONArray.put(a(a2));
                } catch (Throwable unused) {
                }
            }
        }
        return jSONArray;
    }

    public static JSONObject a(uu uuVar) throws JSONException {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("cell_id", uuVar.e());
        jSONObject.put("signal_strength", uuVar.a());
        jSONObject.put("lac", uuVar.d());
        jSONObject.put(TapjoyConstants.TJC_DEVICE_COUNTRY_CODE, uuVar.b());
        jSONObject.put("operator_id", uuVar.c());
        jSONObject.put("operator_name", uuVar.f());
        jSONObject.put("is_connected", uuVar.h());
        jSONObject.put("cell_type", uuVar.i());
        jSONObject.put("pci", uuVar.j());
        jSONObject.put("last_visible_time_offset", uuVar.k());
        return jSONObject;
    }

    public static JSONObject a() throws JSONException {
        return new JSONObject().put("stat_sending", new JSONObject().put(Config.IMPORTANCE_DISABLED, true));
    }
}
