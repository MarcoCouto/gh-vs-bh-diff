package com.yandex.metrica.impl.ob;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.Map;

class tq {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private Socket f5196a;
    @NonNull
    private tr b;
    @NonNull
    private Map<String, tp> c;

    tq(@NonNull Socket socket, @NonNull tr trVar, @NonNull Map<String, tp> map) {
        this.f5196a = socket;
        this.b = trVar;
        this.c = map;
    }

    /* JADX WARNING: Removed duplicated region for block: B:23:0x005e A[SYNTHETIC, Splitter:B:23:0x005e] */
    /* JADX WARNING: Removed duplicated region for block: B:28:? A[RETURN, SYNTHETIC] */
    public void a() {
        BufferedReader bufferedReader;
        Throwable th;
        try {
            this.f5196a.setSoTimeout(1000);
            bufferedReader = new BufferedReader(new InputStreamReader(this.f5196a.getInputStream()));
            try {
                String a2 = a(bufferedReader);
                if (a2 != null) {
                    Uri parse = Uri.parse(a2);
                    tp tpVar = (tp) this.c.get(parse.getPath());
                    if (tpVar != null) {
                        tpVar.a(this.f5196a, parse).a();
                    } else {
                        this.b.a("request_to_unknown_path", a2);
                    }
                }
            } catch (Throwable th2) {
                th = th2;
                try {
                    this.b.a("LocalHttpServer exception", th);
                    if (bufferedReader == null) {
                    }
                    bufferedReader.close();
                } catch (Throwable th3) {
                    th = th3;
                    if (bufferedReader != null) {
                    }
                    throw th;
                }
            }
        } catch (Throwable th4) {
            Throwable th5 = th4;
            bufferedReader = null;
            th = th5;
            if (bufferedReader != null) {
                try {
                    bufferedReader.close();
                } catch (Throwable unused) {
                }
            }
            throw th;
        }
        try {
            bufferedReader.close();
        } catch (Throwable unused2) {
        }
    }

    @Nullable
    private String a(@NonNull BufferedReader bufferedReader) throws IOException {
        String readLine = bufferedReader.readLine();
        if (!TextUtils.isEmpty(readLine) && readLine.startsWith("GET /")) {
            int indexOf = readLine.indexOf(47) + 1;
            int indexOf2 = readLine.indexOf(32, indexOf);
            if (indexOf2 > 0) {
                return readLine.substring(indexOf, indexOf2);
            }
        }
        this.b.a("invalid_route", readLine);
        return null;
    }
}
