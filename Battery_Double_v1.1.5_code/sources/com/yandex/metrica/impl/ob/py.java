package com.yandex.metrica.impl.ob;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class py implements pp {
    @NonNull

    /* renamed from: a reason: collision with root package name */
    private Set<String> f5071a;

    public py(@Nullable List<pu> list) {
        if (list == null) {
            this.f5071a = new HashSet();
            return;
        }
        this.f5071a = new HashSet(list.size());
        for (pu puVar : list) {
            if (puVar.b) {
                this.f5071a.add(puVar.f5068a);
            }
        }
    }

    public boolean a(@NonNull String str) {
        return this.f5071a.contains(str);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("StartupBasedPermissionStrategy{mEnabledPermissions=");
        sb.append(this.f5071a);
        sb.append('}');
        return sb.toString();
    }
}
